using System;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

namespace Generico
{
	public class clsAuditoria
	{
		public static void AuditoriaSesionesXUsuario(Objetos.Auditoria objAudi, string mstrConn)
		{
			Interfaces.Importacion.AuditarSesionesXUsuario(objAudi, mstrConn);
		}

		public static void AuditoriaLogueoXUsuario(Objetos.AuditoriaLogUser objAudi, string mstrConn)
		{
			Interfaces.Importacion.AuditarLogueoXUsuario(objAudi, mstrConn);
		}
	}
}
