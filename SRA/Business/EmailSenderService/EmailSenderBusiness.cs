using System;
using System.Data;
using DAO;
using DAO.EmailSenderService;
using Entities.AcusesCorrespondencia;
using System.Collections;
using System.Data.SqlClient;
using Common;

namespace Business.EmailSenderService
{
	/// <summary>
	/// Summary description for EmailSenderBusiness.
	/// </summary>
	public class EmailSenderBusiness
	{
		// objeto que se utiliza para generar transacciones
		static DataBase _DataBase = new DataBase();

		/// <summary>
		/// metodo que reenvia los comprobantes al cliente/s y retorna 
		/// una lista de los comprobantes que no pudo enviar
		/// </summary>
		/// <param name="listIdComprobantes"></param>
		/// <returns></returns>
		public static ArrayList ReenviarDocumentacionPorMail(ArrayList listIdComprobantes)
		{
			// variable de respuesta si esta en 0 item no hay documentos sin enviar
			// aca guardo el id de comprobante que no se pudo mandar
			ArrayList respuesta = new ArrayList();
			try
			{	
				// recorro el ds de aranceles y sobretasas
				foreach(string item in listIdComprobantes)
				{
					int IdComprobante = Convert.ToInt32(item);
					// ejecuto metodo que recupera los datos del cliente y del comprobante
					ArrayList emailsAEnviar = EmailSenderServiceDao.GetDatosComprobanteAEnviar(Convert.ToInt32(item));

					// valido que el cliente tenga mail sino lo agrego a la collecion de la respuesa
					if(emailsAEnviar.Count >0)
					{
						// variable que uso para ver si mando el mail, si esta en false 
						// agrego el id de comprobante a la coleccion de retorno
						bool tieneMail = false;
						// recoro los datos que retornaron de la base 
						foreach(EnviosEmailComprobantesEntity dato in emailsAEnviar)
						{
							// valido que tenga un mail para enviar el documento
							if(dato.varEmailCliente.Trim().Length>0)
							{
								// registro en la base de datos la fila para el nuevo envio del comprobantel		
								Int32 idInsert = EmailSenderServiceDao.CrearMailEnvioDocumentacion(dato);
								// si la operacion termino ok seteo en true tiene mail
								if(idInsert > 0) 
									tieneMail = true;
							}
						}
						// verifico si tengo o no que agregar el id de comprobante
						//  a la coleccion de no enviados
						if(!tieneMail)
							respuesta.Add(item);
					}
					else
					{
						// si no recupero de la base nada de nada no se puede enviar y se agrega a la
						// respuesta
						respuesta.Add(item);
					}
					
				}
			}
			catch(Exception ex)
			{
				string msg = "EmailSenderBusiness.ReenviarDocumentacionPorMail: " + ex.Message.ToString();
				// llamo al metodo del base encargado de manejar los errores
				_DataBase.gManejarError(ex, msg);
				throw new Exception(msg);
			}

			return respuesta;
		}

	}
}
