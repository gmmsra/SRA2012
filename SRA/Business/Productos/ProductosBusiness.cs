using System;
using System.Data;
using DAO;
using DAO.Productos;
using Entities;
using Entities.Facturacion;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;

namespace Business.Productos
{
	/// <summary>
	/// Summary description for ProductosBusiness.
	/// </summary>
	public class ProductosBusiness
	{
		// objeto que se utiliza para generar transacciones
		static DataBase _DataBase = new DataBase();

		/// <summary>
		/// sobrecarga al metodo original se agregaron parametros para el alta del 
		/// animal importado
		/// </summary>
		/// <param name="intRaza"></param>
		/// <param name="intSexo"></param>
		/// <param name="intAsociacion"></param>
		/// <param name="strNroExtranjero"></param>
		/// <param name="strRP"></param>
		/// <param name="strNombre"></param>
		/// <param name="strApodo"></param>
		/// <param name="bitGenHBA"></param>
		/// <param name="Nacionalidad"></param>
		/// <param name="FechaParam"></param>
		/// <param name="TipoProducto"></param>
		/// <param name="intUsuario"></param>
		/// <returns></returns>
		public static DataSet GuardarProductoImportado(int intRaza, int intSexo, int intAsociacion
			, string strNroExtranjero, string strRP, string strNombre
			, string strApodo, bool bitGenHBA, string Nacionalidad
			, string FechaParam, string TipoProducto
			, int intUsuario
			)
		{
			return GuardarProductoImportado( intRaza, intSexo, intAsociacion, strNroExtranjero, strRP, strNombre, strApodo, bitGenHBA
				, Nacionalidad, FechaParam, TipoProducto, string.Empty, string.Empty, -1, -1, -1
				, -1 , -1 , System.DateTime.MinValue, -1	
				, intUsuario);
		}


		public static DataSet GuardarProductoImportado(int intRaza, int intSexo, int intAsociacion
			, string strNroExtranjero, string strRP, string strNombre
			, string strApodo, bool bitGenHBA, string Nacionalidad
			, string FechaParam, string TipoProducto
			, string FechaNaci, string RPNac, int idTipoReg, int idVariedad, int idPelaje
			, int idPadreServicio, int idTipoServicio, DateTime fechaServicio, int idCriadorPropietario 
			, int intUsuario
			)
		{
			// abro la coneccion
			SqlConnection myConnectionSQLPpal = new SqlConnection(_DataBase.mstrConn);
			myConnectionSQLPpal.Open();
			// obtengo una transaccion
			SqlTransaction transPpal = myConnectionSQLPpal.BeginTransaction(IsolationLevel.ReadCommitted);
			Boolean blnOK = false;
			DataSet dsResultado = new DataSet();

			try
			{
				dsResultado = ProductosDao.ConsultaExistenciaProductoImportado(intRaza, intSexo, intAsociacion, strNroExtranjero, strRP,  strNombre);

				// Si NO existe, inserto el nuevo producto importado.
				if (dsResultado.Tables[0].Rows.Count == 0)
				{
					//Insertar.	
					dsResultado = ProductosDao.InsertarProductoImportado(transPpal, intRaza, intSexo, intAsociacion, strNroExtranjero
						, strRP,  strNombre, strApodo, bitGenHBA
						, Nacionalidad, FechaParam, TipoProducto
						, FechaNaci, RPNac, idTipoReg, idVariedad, idPelaje, idCriadorPropietario
						, intUsuario);
					// si informa servicio y es hembra senera la denuncia del servicio
					if(idPadreServicio > 0 && idTipoServicio > 0 && intSexo == 0)
					{
						DenunciaServicioEntity objDenuServ = CargarDatosServicio(Convert.ToInt32(dsResultado.Tables[0].Rows[0]["prdt_id"])
							,intRaza , idPadreServicio, idTipoServicio, fechaServicio
							,idCriadorPropietario, intUsuario);
						// Inserta o Actualiza Cabecera de la denuncia.
						if (DenunciaServiciosDao.InsertarCabecera(transPpal, objDenuServ))
						{
							//Inserta, Borra � Modifica Detalle.
							blnOK = DenunciaServiciosDao.InsertaBorraModificaDetalle(transPpal, objDenuServ);
						}
					}

					transPpal.Commit(); 
				}
			}
			catch (Exception ex)
			{
				string msg = ex.Message.ToString();
				// error rollback
				if (transPpal.Connection != null)
					transPpal.Rollback();

				_DataBase.gManejarError(ex, msg);
				throw new Exception(msg);
			}
			finally
			{
				transPpal.Dispose();
				myConnectionSQLPpal.Close();
			}
			return dsResultado;
		}

		/// <summary>
		/// metodo que se utiliza para modificar productos y productos inspec 
		/// desde el resultado de la carga de productosinspec.aspx
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static bool CreaModificaInspeccionFeno(ProductoInspecionesFenoEntity obj)
		{
			bool respuesta = false;

			// abro la coneccion
			SqlConnection myConnectionSQL = new SqlConnection(_DataBase.mstrConn);
			myConnectionSQL.Open();
			// obtengo una transaccion
			SqlTransaction trans = myConnectionSQL.BeginTransaction(IsolationLevel.ReadCommitted);
			try
			{
				bool operOk = ProductosDao.ModifProductoPorInspecion(trans, obj);
				
				if(operOk)
				{
					if(obj.prin_id > 0)
						operOk = ProductosDao.ModifProductosInspec(trans, obj);
					else
						operOk = ProductosDao.CrearProductosInspec(trans, obj);
				}

				if(operOk)
				{
					trans.Commit(); 
					respuesta = true;
				}
				else
				{
					throw new Exception("Error no se actualizaron los datos");
				}
			}
			catch(Exception ex)	
			{
				trans.Rollback();
				throw new Exception(ex.Message);
			}
			finally
			{
				// termina la transaccion
				trans.Dispose();
				// termino la coneccion
				myConnectionSQL.Close();
			}

			return respuesta;
		}

		
		/// <summary>
		/// metodo que retorna si el nro de control corresponde o al comprador o al vendedor
		/// </summary>
		/// <param name="nroControl"></param>
		/// <param name="idCriadorComp"></param>
		/// <param name="idCriadorVend"></param>
		/// <param name="cantFact"></param>
		/// <param name="cantNotFact"></param>
		/// <returns>string</returns>
		public static string ValidaNumeroControlTramitesPropiedad( int nroControl, int idCriadorComp, int idCriadorVend, int idRaza
															     , int cantFact, int cantNotFact, int codiServiTipo)
		{
			string respuesta = string.Empty	;
			try
			{
				// cargo los objetos con los datos del nro de tramite para cada cliente criador y vendedor
				ControlCobranzasRRGGEntity objComp = ProductosDao.GetControlCobranzasRRGG(nroControl, idCriadorComp, idRaza);
				ControlCobranzasRRGGEntity objVend = ProductosDao.GetControlCobranzasRRGG(nroControl, idCriadorVend, idRaza);
				
				if((objComp.proformaID == 0 && objComp.comprobanteID == 0) && (objVend.proformaID == 0 && objVend.comprobanteID == 0))
				{
					// si todos los nro de comprobantes y proformas de vendedor y comprador == 0 no encontre el nro de control
					respuesta = "No existe el nro de control " + nroControl.ToString() + ".";
				}
				else if(objComp.idClienteBuscado != objComp.idClienteEncontrado && objVend.idClienteBuscado != objVend.idClienteEncontrado)
				{
					// si los clientes buscados (por criador informado) no corrsponde con el cliente del nro de control
					respuesta = "El nro de control " + nroControl.ToString() + " corresponde a un cliente diferente al informado.";
				}
				else if(objComp.idRaza != idRaza && objComp.idCriador != idCriadorComp 
					&& objVend.idRaza != idRaza && objVend.idCriador != idCriadorVend)
				{
					// si la raza recuperada de comprador-vendedor no correponde con la informada en el tramite
					respuesta = "El nro de control " + nroControl.ToString() + " no corresponde a la raza criador";
				}
				else if (objComp.codiServ != codiServiTipo &&  objVend.codiServ != codiServiTipo && codiServiTipo != Convert.ToInt16(Common.CodigosServiciosTiposRRGG.Varios))
				{	
					// tipo de servicio del nro de control diferente  al informado
					respuesta = "El tipo de servicio del nro de control " + nroControl.ToString() + " no concuerda con el que se declara en el tramite";
				}
				else
				{
					if(objVend.cantFacturada > 0 &&  objVend.cantFacturada < cantFact)
					{
						// si la cantidad facturada es > 0 y  menor que la informada en el tramite
						respuesta = "La cantidad facturada del nro de control " + nroControl.ToString() + " no concuerda con la candidad que se declara en el tramite";
					}
					if(objVend.cantNoFacturada > 0 &&  objVend.cantNoFacturada < cantNotFact)
					{
						// si la cantidad no facturada > 0 y es menor que la informada en el tramite
						respuesta = "La cantidad no facturada del nro de control " + nroControl.ToString() + " no concuerda con la candidad que se declara en el tramite";
					}
				}
			}
			catch(Exception ex)
			{
				string msg = ex.Message.ToString();
				_DataBase.gManejarError(ex, msg);
				throw new Exception(msg);
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que completa el objeto DenunciaServicioEntity para pasarlo a dao
		/// </summary>
		/// <param name="idMadre"></param>
		/// <param name="intRaza"></param>
		/// <param name="idPadreServicio"></param>
		/// <param name="idTipoServicio"></param>
		/// <param name="fechaServicio"></param>
		/// <param name="idCriadorPropietario"></param>
		/// <param name="intUsuario"></param>
		/// <returns>DenunciaServicioEntity</returns>
		private static DenunciaServicioEntity CargarDatosServicio(int idMadre, int intRaza , int idPadreServicio, int idTipoServicio
			, DateTime fechaServicio, int idCriadorPropietario, int intUsuario)
		{
			DenunciaServicioEntity respuesta = new DenunciaServicioEntity();
 
			DenunciaServicioDetalleEntity det = new DenunciaServicioDetalleEntity();
		
			respuesta.sede_fecha = System.DateTime.Now;
			respuesta.sede_raza_id = intRaza;
			respuesta.sede_servi_desde = fechaServicio;
			respuesta.sede_servi_hasta = System.DateTime.MinValue;
			respuesta.sede_audi_user = intUsuario;
			respuesta.sede_audi_fecha = System.DateTime.Now;
			respuesta.sede_padre_id = idPadreServicio;
			respuesta.sede_baja_fecha = System.DateTime.MinValue;
			respuesta.sede_seti_id = idTipoServicio;
			respuesta.sede_presen_fecha = System.DateTime.Now;
			respuesta.sede_cria_id = idCriadorPropietario;

			det.sdde_item_nume = 1;
			det.sdde_sede_id = -1;
			det.sdde_padre_id = idPadreServicio;
			det.sdde_madre_id = idMadre;
			det.sdde_audi_user = intUsuario;
			det.sdde_audi_fecha = System.DateTime.Now;
			det.sdde_servi_desde = fechaServicio;
			det.sdde_servi_hasta = System.DateTime.MinValue;
			det.sdde_seti_id = idTipoServicio;
			det.sdde_linea_nume = 1;
			
			respuesta.LstDetallesPlanilla.Add(det);

			return respuesta;
		}


		/// <summary>
		/// metodoq ue reporta datos del producto
		/// </summary>
		/// <param name="idProducto"></param>
		/// <returns>ProductoEntity</returns>
		public static ProductoEntity GetDatosProducto(int idProducto)
		{
			return ProductosDao.GetDatosProducto(idProducto);
		}

		/// <summary>
		/// metodo que genera el nro de macho dador si ya no tiene uno
		/// </summary>
		/// <param name="idProducto"></param>
		/// <returns>bool</returns>
		public static bool SetNroDador(int idProducto)
		{
			return ProductosDao.SetNroDador(idProducto);
		}
		
		/// <summary>
		/// metodo que revierte la baja del producto
		/// </summary>
		/// <param name="idProducto"></param>
		/// <param name="idUsuario"></param>
		/// <returns>bool</returns>
		public static bool RevertirBajaProducto(Int32 idProducto, Int32 idUsuario)
		{
			return ProductosDao.RevertirBajaProducto(idProducto, idUsuario);
		}

		/// <summary>
		/// metodo que nacionaliza un producto extranjero
		/// </summary>
		/// <param name="idProducto"></param>
		/// <param name="idUsuario"></param>
		/// <returns>Int32</returns>
		public static Int32 NacionalizarExtranjero(Int32 idProducto, Int32 idUsuario)
		{
			return ProductosDao.NacionalizarExtranjero(idProducto, idUsuario);
		}

		/// <summary>
		/// metodo que pasando un producto indica que parte del pedegree se puede modificar
		/// esto lo hace por estado del analisisl del mismo producto al que se le quieren 
		/// modificar los padres, si es usuario sra permite cualquier cosa
		/// -1 = no se puede modificar ni padre ni madre
		/// 0 = deja modificar padre y madre
		/// 1 = solo deja modificar padre
		/// 2 = solo deja modificar madre
		/// </summary>
		/// <param name="idProducto"></param>
		/// <param name="idUsuario"></param>
		/// <returns>int</returns>
		public static RespValidaModificarPedregeeEntity ValidaModificarPedregeeProducto(Int32 idProducto, Int32 idUsuario)
		{
			return ProductosDao.ValidaModificarPedregeeProducto(idProducto, idUsuario);
		}
	}
}
