﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Data;
using DAO;
using AccesoBD;

namespace Business
{
    public class TransferenciaAnimalEnPieBusiness
    {
        private static string nameSpace = "Business.TransferenciaAnimalPieBusiness.";

        public static TransferenciaAnimalEnPie_TramiteEntity LimpiarTramite()
        {
            TransferenciaAnimalEnPie_TramiteEntity tramite = new TransferenciaAnimalEnPie_TramiteEntity();

            tramite.Tramites = new List<TransferenciaAnimalEnPie_TramitesEntity>();
            TransferenciaAnimalEnPie_TramitesEntity t = new TransferenciaAnimalEnPie_TramitesEntity();
            tramite.Tramites.Add(t);

            tramite.TramitesDeta = new List<TransferenciaAnimalEnPie_TramitesDetaEntity>();
            //TransferenciaAnimalEnPie_TramitesDetaEntity tdet = new TransferenciaAnimalEnPie_TramitesDetaEntity();
            //tdet.trad_pend = 0;
            //tramite.TramitesDeta.Add(tdet);

            tramite.TramitesDocum = new List<TransferenciaAnimalEnPie_TramitesDocumEntity>();
            //TransferenciaAnimalEnPie_TramitesDocumEntity tdoc = new TransferenciaAnimalEnPie_TramitesDocumEntity();
            //tramite.TramitesDocum.Add(tdoc);

            tramite.TramitesObse = new List<TransferenciaAnimalEnPie_TramitesObseEntity>();
            //TransferenciaAnimalEnPie_TramitesObseEntity to = new TransferenciaAnimalEnPie_TramitesObseEntity();
            //tramite.TramitesObse.Add(to);

            tramite.TramitesProductos = new List<TransferenciaAnimalEnPie_TramitesProductosEntity>();
            //TransferenciaAnimalEnPie_TramitesProductosEntity tp = new TransferenciaAnimalEnPie_TramitesProductosEntity();
            //tramite.TramitesProductos.Add(tp);

            tramite.TramitesEmbrionesFrescos = new List<TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity>();
            //TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity tef = new TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity();
            //tramite.TramitesEmbrionesFrescos.Add(tef);

            return tramite;
        }

        public static TransferenciaAnimalEnPie_ProductoEntity LimpiarProducto()
        {
            TransferenciaAnimalEnPie_ProductoEntity producto = new TransferenciaAnimalEnPie_ProductoEntity();

            producto.Productos  = new List<TransferenciaAnimalEnPie_ProductosEntity>();
            //TransferenciaAnimalEnPie_ProductosEntity p = new TransferenciaAnimalEnPie_ProductosEntity();
            //producto.Productos.Add(p);

            producto.ProductosAnalisis = new List<TransferenciaAnimalEnPie_ProductosAnalisisEntity>();
            //TransferenciaAnimalEnPie_ProductosAnalisisEntity pa = new TransferenciaAnimalEnPie_ProductosAnalisisEntity();
            //producto.ProductosAnalisis.Add(pa);

            producto.ProductosNumeros = new List<TransferenciaAnimalEnPie_ProductosNumerosEntity>();
            //TransferenciaAnimalEnPie_ProductosNumerosEntity pn = new TransferenciaAnimalEnPie_ProductosNumerosEntity();
            //producto.ProductosNumeros.Add(pn);

            producto.ProductosDocum = new List<TransferenciaAnimalEnPie_ProductosDocumEntity>();
            //TransferenciaAnimalEnPie_ProductosDocumEntity pd = new TransferenciaAnimalEnPie_ProductosDocumEntity();
            //producto.ProductosDocum.Add(pd);

            return producto;
        }

        public static List<TransferenciaAnimalEnPie_GridEntity> Tramites_animalEnPie_Busq(int? tram_id = null, int? tram_nume = null, int? tram_raza_id = null, int? sexo = null,
            int? sra_nume = null, int? cria_id = null, int? prod_id = null, string prdt_nombre = "", DateTime? tram_fecha_desde = null, DateTime? tram_fecha_hasta = null,
            DateTime? tram_oper_fecha = null, int? tram_ttra_id = null, int? tram_esta_id = null, int? tram_pais_id = null, int? mostrar_vistos = null, string prod_nacionalidad = "",
            int? cria_vend_id = null)
        {
            List<TransferenciaAnimalEnPie_GridEntity> obj = new List<TransferenciaAnimalEnPie_GridEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.Tramites_animalEnPie_Busq(tram_id, tram_nume, tram_raza_id, sexo, sra_nume, cria_id, prod_id, prdt_nombre, tram_fecha_desde, tram_fecha_hasta, tram_oper_fecha, tram_ttra_id, tram_esta_id, tram_pais_id, mostrar_vistos, prod_nacionalidad, cria_vend_id);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        obj.Add(new TransferenciaAnimalEnPie_GridEntity
                        {
                            tram_ttra_id = Convert.ToInt32(dr["tram_ttra_id"]),
                            tram_visto = dr["tram_visto"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_visto"]),
                            tram_id = Convert.ToInt32(dr["tram_id"]),
                            tram_inic_fecha = dr["tram_inic_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_inic_fecha"]),
                            tram_comp_clie_id = dr["tram_comp_clie_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_comp_clie_id"]),
                            tram_vend_clie_id = dr["tram_vend_clie_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_vend_clie_id"]),
                            tram_comp_cria_id = dr["tram_comp_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_comp_cria_id"]),
                            tram_vend_cria_id = dr["tram_vend_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_vend_cria_id"]),
                            tram_oper_fecha = dr["tram_oper_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_oper_fecha"]),
                            tram_esta_id = Convert.ToInt32(dr["tram_esta_id"]),
                            tram_nume = dr["tram_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_nume"]),
                            esta_desc = dr["esta_desc"].ToString(),
                            prdt_nomb = dr["prdt_nomb"].ToString(),
                            raza = dr["raza"].ToString(),
                            sexo = dr["sexo"].ToString(),
                            prdt_id = dr["prdt_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_id"]),
                            prdt_sra_nume = dr["prdt_sra_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_sra_nume"]),
                            _numero = dr["_numero"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_numero"]),
                            _cria_nume = dr["_cria_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_cria_nume"]),
                            img_visto = dr["img_visto"].ToString(),
                            text_alt = dr["text_alt"].ToString(),
                            prdt_ndad = dr["prdt_ndad"].ToString(),
                            clie_apel = dr["clie_apel"].ToString(),
                            clie_apel_vend = dr["clie_apel_vend"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Tramites_animalEnPie_Busq, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return obj;
        }

        public static ProductosEntity GetProductoByTramiteId(int tram_id)
        {
            ProductosEntity obj = new Entities.ProductosEntity();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.GetProductoByTramiteId(tram_id);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    obj.prdt_id = Convert.ToInt32(dr["prdt_id"]);
                    obj.prdt_rp = dr["prdt_rp"].ToString();
                    obj.prdt_nomb = dr["prdt_nomb"].ToString();
                    obj.tram_id = tram_id;
                    obj.prdt_cria_id = dr["prdt_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_cria_id"]);
                    obj.prdt_raza_id = Convert.ToInt32(dr["prdt_raza_id"]);
                    obj.prdt_sra_nume = dr["prdt_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_cria_id"]);
                    obj.prdt_sexo = dr["prdt_sexo"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_sexo"]);
                    obj.prdt_prop_cria_id = dr["prdt_prop_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_prop_cria_id"]);
                    obj.prdt_te = dr["prdt_te"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_te"]);
                    obj.prdt_rp_nume = dr["prdt_rp_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_rp_nume"]);
                    obj.prdt_tram_nume = dr["prdt_tram_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_tram_nume"]);
                    obj.prdt_tran_fecha = dr["prdt_tran_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_tran_fecha"]);
                    obj.prdt_ndad = dr["prdt_ndad"].ToString();
                    obj.prdt_rp_extr = dr["prdt_rp_extr"].ToString();
                    obj.prdt_vari_id = dr["prdt_vari_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_vari_id"]);
                    obj.prdt_pdin_id = dr["prdt_pdin_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_pdin_id"]);
                    obj.prdt_ori_asoc_id = dr["prdt_ori_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_ori_asoc_id"]);
                    obj.prdt_ori_asoc_nume = dr["prdt_ori_asoc_nume"].ToString();
                    obj.prdt_ori_asoc_nume_lab = dr["prdt_ori_asoc_nume_lab"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_ori_asoc_nume_lab"]);
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "GetProductoByTramiteId, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return obj;
        }

        public static TransferenciaAnimalEnPie_TramiteEntity Tramites_Consul(int? tram_id = null, int? tram_ttra_id = null, string formato = "")
        {
            TransferenciaAnimalEnPie_TramiteEntity tramite = new Entities.TransferenciaAnimalEnPie_TramiteEntity();
            List<TransferenciaAnimalEnPie_TramitesEntity> tramites = new List<Entities.TransferenciaAnimalEnPie_TramitesEntity>();
            List<TransferenciaAnimalEnPie_TramitesDetaEntity> tramitesDeta = new List<Entities.TransferenciaAnimalEnPie_TramitesDetaEntity>();
            List<TransferenciaAnimalEnPie_TramitesDocumEntity> tramitesDocum = new List<Entities.TransferenciaAnimalEnPie_TramitesDocumEntity>();
            List<TransferenciaAnimalEnPie_TramitesObseEntity> tramitesObse = new List<Entities.TransferenciaAnimalEnPie_TramitesObseEntity>();
            List<TransferenciaAnimalEnPie_TramitesProductosEntity> tramitesProductos = new List<Entities.TransferenciaAnimalEnPie_TramitesProductosEntity>();
            List<TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity> tramitesEmbrionesFrescos = new List<Entities.TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.Tramites_Consul(tram_id, tram_ttra_id, formato);
                if (ds != null && ds.Tables.Count > 0)
                {
                    #region Trámites.
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            tramites.Add(new TransferenciaAnimalEnPie_TramitesEntity
                            {
                                tram_id = Convert.ToInt32(dr["tram_id"]),
                                tram_inic_fecha = dr["tram_inic_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_inic_fecha"]),
                                tram_fina_fecha = dr["tram_fina_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_fina_fecha"]),
                                tram_pres_fecha = dr["tram_pres_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_pres_fecha"]),
                                tram_esta_id = Convert.ToInt32(dr["tram_esta_id"]),
                                tram_raza_id = dr["tram_raza_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_raza_id"]),
                                tram_ttra_id = Convert.ToInt32(dr["tram_ttra_id"]),
                                tram_nume = dr["tram_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_nume"]),
                                tram_cria_cant = dr["tram_cria_cant"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_cria_cant"]),
                                tram_dosi_cant = dr["tram_dosi_cant"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_dosi_cant"]),
                                tram_impr_id = dr["tram_impr_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_impr_id"]),
                                tram_audi_fecha = Convert.ToDateTime(dr["tram_audi_fecha"]),
                                tram_audi_user = Convert.ToInt32(dr["tram_audi_user"]),
                                tram_baja_fecha = dr["tram_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_baja_fecha"]),
                                tram_tmsp = dr["tram_tmsp"] == DBNull.Value ? null : (byte[])dr["tram_tmsp"],
                                tram_fina_user = dr["tram_fina_user"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_fina_user"]),
                                tram_pais_id = dr["tram_pais_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_pais_id"]),
                                tram_comp_clie_id = dr["tram_comp_clie_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_comp_clie_id"]),
                                tram_comp_cria_id = dr["tram_comp_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_comp_cria_id"]),
                                tram_vend_clie_id = dr["tram_vend_clie_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_vend_clie_id"]),
                                tram_vend_cria_id = dr["tram_vend_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_vend_cria_id"]),
                                tram_prop_clie_id = dr["tram_prop_clie_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_prop_clie_id"]),
                                tram_prop_cria_id = dr["tram_prop_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_prop_cria_id"]),
                                tram_embr_madre_id = dr["tram_embr_madre_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_embr_madre_id"]),
                                tram_embr_padre_id = dr["tram_embr_padre_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_embr_padre_id"]),
                                tram_embr_padre2_id = dr["tram_embr_padre2_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_embr_padre2_id"]),
                                tram_embr_cant = dr["tram_embr_cant"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_embr_cant"]),
                                tram_embr_tede_comp_id = dr["tram_embr_tede_comp_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_embr_tede_comp_id"]),
                                tram_embr_tede_vend_id = dr["tram_embr_tede_vend_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_embr_tede_vend_id"]),
                                tram_embr_recu_fecha = dr["tram_embr_recu_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_embr_recu_fecha"]),
                                _te_denun_desc = dr["_te_denun_desc"].ToString(),
                                tram_nro_control = dr["tram_nro_control"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_nro_control"]),
                                tram_oper_fecha = dr["tram_oper_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_oper_fecha"]),
                                tram_liber_fecha = dr["tram_liber_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_liber_fecha"]),
                                tram_apob_asoc_fecha = dr["tram_apob_asoc_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_apob_asoc_fecha"]),
                                tram_ressnpsextr = dr["tram_ressnpsextr"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_ressnpsextr"]),
                                esta_abre = dr["esta_abre"].ToString()
                            });
                        }
                    }
                    tramite.Tramites = tramites;
                    #endregion

                    #region Requisitos.
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            tramitesDeta.Add(new TransferenciaAnimalEnPie_TramitesDetaEntity
                            {
                                trad_id = Convert.ToInt32(dr["trad_id"]),
                                trad_tram_id = Convert.ToInt32(dr["trad_tram_id"]),
                                trad_pend = dr["trad_pend"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_pend"]),
                                trad_fina_fecha = dr["trad_fina_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trad_fina_fecha"]),
                                trad_requ_id = dr["trad_requ_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_requ_id"]),
                                trad_obli = dr["trad_requ_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_requ_id"]),
                                trad_manu = dr["trad_manu"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_manu"]),
                                trad_baja_fecha = dr["trad_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trad_baja_fecha"]),
                                _estado = dr["_estado"].ToString(),
                                trad_audi_fecha = Convert.ToDateTime(dr["trad_audi_fecha"]),
                                trad_audi_user = Convert.ToInt32(dr["trad_audi_user"]),
                                trad_tmsp = dr["trad_tmsp"] == DBNull.Value ? null : (byte[])dr["trad_tmsp"],
                                _requ_desc = dr["_requ_desc"].ToString(),
                                _pend = dr["_pend"].ToString(),
                                _obli = dr["_obli"].ToString(),
                                _manu = dr["_manu"].ToString()
                            });
                        }
                    }
                    tramite.TramitesDeta = tramitesDeta ;
                    #endregion

                    #region Documentos.
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            tramitesDocum.Add(new TransferenciaAnimalEnPie_TramitesDocumEntity
                            {
                                trdo_id = Convert.ToInt32(dr["trdo_id"]),
                                trdo_tram_id = Convert.ToInt32(dr["trdo_tram_id"]),
                                trdo_path = dr["trdo_path"].ToString(),
                                trdo_refe = dr["trdo_refe"].ToString(),
                                trdo_audi_fecha = Convert.ToDateTime(dr["trdo_audi_fecha"]),
                                trdo_audi_user = Convert.ToInt32(dr["trdo_audi_user"]),
                                trdo_baja_fecha = dr["trdo_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trdo_baja_fecha"]),
                                trdo_tmsp = dr["trdo_tmsp"] == DBNull.Value ? null : (byte[])dr["trdo_tmsp"],
                                _estado = dr["_estado"].ToString(),
                                _parampath = dr["_parampath"].ToString()
                            });
                        }
                    }
                    tramite.TramitesDocum = tramitesDocum;
                    #endregion

                    #region Observaciones.
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[3].Rows)
                        {
                            tramitesObse.Add(new TransferenciaAnimalEnPie_TramitesObseEntity
                            {
                                trao_id = Convert.ToInt32(dr["trao_id"]),
                                trao_tram_id = Convert.ToInt32(dr["trao_tram_id"]),
                                trao_trad_id = dr["trao_trad_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trao_trad_id"]),
                                _requ_desc = dr["_requ_desc"].ToString(),
                                _trad_requ_id = dr["_trad_requ_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_trad_requ_id"]),
                                trao_obse = dr["trao_obse"].ToString(),
                                trao_fecha = Convert.ToDateTime(dr["trao_fecha"]),
                                trao_audi_fecha = Convert.ToDateTime(dr["trao_audi_fecha"]),
                                trao_audi_user = Convert.ToInt32(dr["trao_audi_user"]),
                                trao_tmsp = dr["trao_tmsp"] == DBNull.Value ? null : (byte[])dr["trao_tmsp"]
                            });
                        }
                    }
                    tramite.TramitesObse = tramitesObse;
                    #endregion

                    #region Productos.
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[4].Rows)
                        {
                            tramitesProductos.Add(new TransferenciaAnimalEnPie_TramitesProductosEntity
                            {
                                trpr_id = Convert.ToInt32(dr["trpr_id"]),
                                trpr_prdt_id = dr["trpr_prdt_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_prdt_id"]),
                                trpr_tram_id = Convert.ToInt32(dr["trpr_tram_id"]),
                                trpr_apro = dr["trpr_apro"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_apro"]),
                                trpr_resu_fecha = dr["trpr_resu_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trpr_resu_fecha"]),
                                trpr_audi_fecha = Convert.ToDateTime(dr["trpr_audi_fecha"]),
                                trpr_audi_user = Convert.ToInt32(dr["trpr_audi_user"]),
                                trpr_tmsp = dr["trpr_tmsp"] == DBNull.Value ? null : (byte[])dr["trpr_tmsp"],
                                trpr_sra_nume = dr["trpr_sra_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_sra_nume"]),
                                trpr_asoc_id = dr["trpr_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_asoc_id"]),
                                trpr_padre_asoc_id = dr["trpr_padre_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_padre_asoc_id"]),
                                trpr_madre_asoc_id = dr["trpr_madre_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_madre_asoc_id"]),
                                trpr_rp = dr["trpr_rp"].ToString(),
                                trpr_sexo = dr["trpr_sexo"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_sexo"]),
                                trpr_baja_fecha = dr["trpr_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trpr_baja_fecha"]),
                                trpr_apro_fecha = dr["trpr_apro_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trpr_apro_fecha"])
                            });
                        }
                    }
                    tramite.TramitesProductos = tramitesProductos;
                    #endregion

                    #region  Embriones frescos.
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[5].Rows)
                        {
                            tramitesEmbrionesFrescos.Add(new TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity
                            {
                                treb_id = Convert.ToInt32(dr["treb_id"]),
                                treb_tram_id = dr["treb_tram_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["treb_tram_id"]),
                                treb_carav = dr["treb_carav"].ToString(),
                                treb_fecha_serv = dr["treb_fecha_serv"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["treb_fecha_serv"]),
                                treb_fecha_recu = dr["treb_fecha_recu"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["treb_fecha_recu"]),
                                treb_fecha_impl = dr["treb_fecha_impl"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["treb_fecha_impl"]),
                                treb_audi_user = dr["treb_audi_user"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["treb_audi_user"]),
                                treb_audi_fecha = dr["treb_audi_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["treb_audi_fecha"])
                            });
                        }
                    }
                    tramite.TramitesEmbrionesFrescos = tramitesEmbrionesFrescos ;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Tramites_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return tramite;
        }

        public static TransferenciaAnimalEnPie_ProductoEntity Productos_Consul(int prdt_id)
        {
            TransferenciaAnimalEnPie_ProductoEntity producto = new TransferenciaAnimalEnPie_ProductoEntity();
            List<TransferenciaAnimalEnPie_ProductosEntity> productos = new List<Entities.TransferenciaAnimalEnPie_ProductosEntity>();
            List<TransferenciaAnimalEnPie_ProductosAnalisisEntity> productosAnalisis = new List<Entities.TransferenciaAnimalEnPie_ProductosAnalisisEntity>();
            List<TransferenciaAnimalEnPie_ProductosNumerosEntity> productosNumeros = new List<Entities.TransferenciaAnimalEnPie_ProductosNumerosEntity>();
            List<TransferenciaAnimalEnPie_ProductosDocumEntity> productosDocum = new List<Entities.TransferenciaAnimalEnPie_ProductosDocumEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.Productos_Consul(prdt_id);
                if (ds != null && ds.Tables.Count > 0)
                {
                    #region Productos
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            productos.Add(new TransferenciaAnimalEnPie_ProductosEntity
                            {
                                prdt_id = prdt_id,
                                prdt_rp = dr["prdt_rp"].ToString(),
                                prdt_nomb = dr["prdt_nomb"].ToString(),
                                _pref_desc = dr["_pref_desc"].ToString(),
                                prdt_naci_fecha = dr["prdt_naci_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_naci_fecha"]),
                                prdt_fall_fecha = dr["prdt_fall_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_fall_fecha"]),
                                prdt_cria_id = dr["prdt_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_cria_id"]),
                                prdt_prop_cria_id = dr["prdt_prop_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_prop_cria_id"]),
                                prdt_raza_id = dr["prdt_raza_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_raza_id"]),
                                prdt_sra_nume = dr["prdt_sra_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_sra_nume"]),

                                prdt_sexo = dr["prdt_sexo"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_sexo"]),
                                prdt_b_pela_id = dr["prdt_b_pela_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_b_pela_id"]),
                                prdt_c_pela_id = dr["prdt_c_pela_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_c_pela_id"]),
                                prdt_d_pela_id = dr["prdt_d_pela_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_d_pela_id"]),
                                prdt_baja_fecha = dr["prdt_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_baja_fecha"]),
                                prdt_tran_fecha = dr["prdt_tran_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_tran_fecha"]),
                                prdt_regt_id = dr["prdt_regt_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_regt_id"]),
                                prdt_px = dr["prdt_px"].ToString(),
                                prdt_a_pela_id = dr["prdt_a_pela_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_a_pela_id"]),
                                prdt_apodo = dr["prdt_apodo"].ToString(),

                                prdt_siete = dr["prdt_siete"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_siete"]),
                                prdt_melli = dr["prdt_melli"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_melli"]),
                                prdt_te = dr["prdt_te"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_te"]),
                                prdt_dona_nume = dr["prdt_dona_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_dona_nume"]),
                                prdt_dona_fecha = dr["prdt_dona_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_dona_fecha"]),
                                prdt_peso_nacer = dr["prdt_peso_nacer"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_peso_nacer"]),
                                prdt_peso_deste = dr["prdt_peso_deste"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_peso_deste"]),
                                prdt_peso_final = dr["prdt_peso_final"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_peso_final"]),
                                prdt_insc_fecha = dr["prdt_insc_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_insc_fecha"]),
                                prdt_esta_id = dr["prdt_esta_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_esta_id"]),

                                prdt_epd_nacer = dr["prdt_epd_nacer"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["prdt_epd_nacer"]),
                                prdt_epd_deste = dr["prdt_epd_deste"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["prdt_epd_deste"]),
                                prdt_epd_final = dr["prdt_epd_final"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["prdt_epd_final"]),
                                prdt_rp_nume = dr["prdt_rp_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_rp_nume"]),
                                prdt_rp_extr = dr["prdt_rp_extr"].ToString(),
                                prdt_audi_user = dr["prdt_audi_user"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_audi_user"]),
                                _esta_desc = dr["_esta_desc"].ToString(),
                                _raza_desc = dr["_raza_desc"].ToString(),
                                _sexo = dr["_sexo"].ToString(),
                                _asoc = dr["_asoc"].ToString(),

                                _nume = dr["_nume"].ToString(),
                                _asoc_id = dr["_asoc_id"].ToString(),
                                _desc = dr["_desc"].ToString(),
                                _Esdonante = dr["_Esdonante"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_Esdonante"]),
                                _espe = dr["_espe"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_espe"]),
                                prdt_ndad = dr["prdt_ndad"].ToString(),
                                _cria_nume = dr["_cria_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_cria_nume"]),
                                _criador = dr["_criador"].ToString(),
                                prdt_bamo_id = dr["prdt_bamo_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_bamo_id"]),
                                prdt_cuig = dr["prdt_cuig"].ToString(),

                                _embargado = dr["_embargado"].ToString(),
                                prdt_tram_nume = dr["prdt_bamo_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_bamo_id"]),
                                _pdin_fecha = dr["_pdin_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["_pdin_fecha"]),
                                _pdin_nro_control = dr["_pdin_nro_control"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_pdin_nro_control"]),
                                _pdin_tede_nume = dr["_pdin_tede_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_pdin_tede_nume"]),
                                _fecha_tramite = dr["_fecha_tramite"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["_fecha_tramite"]),
                                _tram_nume = dr["_tram_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_tram_nume"]),
                                _nro_control_tramite = dr["_nro_control_tramite"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_nro_control_tramite"]),
                                _calif = dr["_calif"].ToString(),
                                _califFenotipica = dr["_califFenotipica"].ToString(),

                                prdt_vari_id = dr["prdt_vari_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_vari_id"]),
                                _receptora = dr["_receptora"].ToString(),
                                _servi_fecha_producto = dr["_servi_fecha_producto"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["_servi_fecha_producto"]),
                                _servi_fecha = dr["_servi_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["_servi_fecha"]),
                                _servi_tipo_TE = dr["_servi_tipo_TE"].ToString(),
                                _servi_tipo = dr["_servi_tipo"].ToString(),
                                _impla_fecha = dr["_impla_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["_impla_fecha"]),
                                _aprobado = dr["_aprobado"].ToString(),
                                _resulSNPSExtranj = dr["_resulSNPSExtranj"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_resulSNPSExtranj"]),
                                prdt_prop_clie_id = dr["prdt_prop_clie_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_prop_clie_id"]),

                                generar_numero = dr["generar_numero"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["generar_numero"]),
                                _asoc_codi = dr["_asoc_codi"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_asoc_codi"]),
                                prdt_ori_asoc_id = dr["prdt_ori_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_ori_asoc_id"]),
                                prdt_ori_asoc_nume = dr["prdt_ori_asoc_nume"].ToString(),
                                prdt_ori_asoc_nume_lab = dr["prdt_ori_asoc_nume_lab"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_ori_asoc_nume_lab"]),
                                prdt_pdin_id = dr["prdt_pdin_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_pdin_id"]),
                                prdt_condicional = dr["prdt_condicional"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_condicional"]),
                                _dona_nro_control = dr["_dona_nro_control"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_dona_nro_control"]),
                                reproceso = dr["reproceso"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["reproceso"]),
                                _reprocesar = dr["_reprocesar"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_reprocesar"]),

                                prdt_dona_sena_nume = dr["prdt_dona_sena_nume"].ToString(),
                                _prin_bitTrim = dr["_prin_bitTrim"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_prin_bitTrim"]),
                                prdt_Fecha_InspecMayor25 = dr["prdt_Fecha_InspecMayor25"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_Fecha_InspecMayor25"]),
                                prdt_Resul_InspecMayor25 = dr["prdt_Resul_InspecMayor25"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_Resul_InspecMayor25"]),
                                prdt_anio_deps_Nac = dr["prdt_anio_deps_Nac"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_anio_deps_Nac"]),
                                prdt_anio_deps_Dest = dr["prdt_anio_deps_Dest"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_anio_deps_Dest"]),
                                prdt_anio_deps_PosDest = dr["prdt_anio_deps_PosDest"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_anio_deps_PosDest"]),
                                prdt_FechaDestete = dr["prdt_FechaDestete"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_FechaDestete"]),
                                prdt_FechaPesoPostDestete = dr["prdt_FechaPesoPostDestete"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_FechaPesoPostDestete"]),
                                prdt_FechaCirEscrotal = dr["prdt_FechaCirEscrotal"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_FechaCirEscrotal"]),

                                prdt_CirEscrotal = dr["prdt_CirEscrotal"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["prdt_CirEscrotal"]),
                                prdt_ManejoDest = dr["prdt_ManejoDest"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_ManejoDest"]),
                                prdt_ManejoPosDest = dr["prdt_ManejoPosDest"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_ManejoPosDest"]),
                                prdt_ObsDestete = dr["prdt_ObsDestete"].ToString(),
                                prdt_ObsPostDestete = dr["prdt_ObsPostDestete"].ToString(),
                                Prdt_Deps_Fecha = dr["Prdt_Deps_Fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["Prdt_Deps_Fecha"]),
                                Prdt_Epd_CirEscrotal = dr["Prdt_Epd_CirEscrotal"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Epd_CirEscrotal"]),
                                Prdt_Epd_Lech = dr["Prdt_Epd_Lech"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Epd_Lech"]),
                                Prdt_Epd_LYC = dr["Prdt_Epd_LYC"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Epd_LYC"]),
                                Prdt_Exa_Nacer = dr["Prdt_Exa_Nacer"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Exa_Nacer"]),

                                Prdt_Exa_Destete = dr["Prdt_Exa_Destete"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Exa_Destete"]),
                                Prdt_Exa_Final = dr["Prdt_Exa_Final"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Exa_Final"]),
                                Prdt_Exa_CirEscrotal = dr["Prdt_Exa_CirEscrotal"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Exa_CirEscrotal"]),
                                Prdt_Exa_Lech = dr["Prdt_Exa_Lech"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Prdt_Exa_Lech"]),
                                _HabilitaNacionalizacionProducto = dr["_HabilitaNacionalizacionProducto"].ToString(),
                                prdt_castrado = dr["prdt_castrado"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_castrado"]),
                                prdt_Descorne = dr["prdt_Descorne"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdt_Descorne"]),
                                prdt_Fecha_castrado = dr["prdt_Fecha_castrado"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdt_Fecha_castrado"]),
                                prdt_Cond_Geneticas = dr["prdt_Cond_Geneticas"].ToString(),
                                prdt_factura = dr["prdt_factura"].ToString()
                            });
                        }
                    }
                    producto.Productos = productos;
                    #endregion

                    #region Análisis.
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            productosAnalisis.Add(new TransferenciaAnimalEnPie_ProductosAnalisisEntity
                            {
                                prta_id = Convert.ToInt32(dr["prta_id"]),
                                prta_prdt_id = dr["prta_prdt_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prta_prdt_id"]),
                                prta_nume = dr["prta_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prta_nume"]),
                                prta_ares_id = dr["prta_ares_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prta_ares_id"]),
                                prta_fecha = dr["prta_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prta_fecha"]),
                                prta_resul_fecha = dr["prta_resul_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prta_resul_fecha"]),
                                prta_audi_user = dr["prta_ares_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prta_audi_user"]),
                                prta_baja_fecha = dr["prta_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prta_baja_fecha"]),
                                _resul_codi = dr["_resul_codi"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_resul_codi"]),
                                _resul = dr["_resul"].ToString(),
                                prta_ndad = dr["prta_ndad"].ToString(),
                                _pureza = dr["_pureza"].ToString(),
                                _tiene_ts = dr["_tiene_ts"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_tiene_ts"]),
                                _results_codi = dr["_results_codi"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_results_codi"]),
                                _results = dr["_results"].ToString(),
                                _tiene_adn = dr["_tiene_adn"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_tiene_adn"]),
                                _resuladn_codi = dr["_resuladn_codi"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_resuladn_codi"]),
                                _resuladn = dr["_resuladn"].ToString(),
                                _tiene_snps = dr["_tiene_snps"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_tiene_snps"]),
                                _resulsnps_codi = dr["_resulsnps_codi"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_resulsnps_codi"]),
                                _resulsnps = dr["_resulsnps"].ToString(),
                                _tiene_pelo = dr["_tiene_pelo"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_tiene_pelo"]),
                                prta_tipo_analisis = dr["prta_tipo_analisis"].ToString(),
                                Prta_Esan_Codi = dr["Prta_Esan_Codi"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["Prta_Esan_Codi"]),
                                _anes_desc = dr["_anes_desc"].ToString(),
                                _EstadoAnalisis = dr["_EstadoAnalisis"].ToString(),
                                _Estudio = dr["_Estudio"].ToString()
                            });
                        }
                    }
                    producto.ProductosAnalisis = productosAnalisis;
                    #endregion

                    #region Productos números
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            productosNumeros.Add(new TransferenciaAnimalEnPie_ProductosNumerosEntity
                            {
                                ptnu_id = Convert.ToInt32(dr["ptnu_id"]),
                                ptnu_asoc_id = dr["ptnu_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["ptnu_asoc_id"]),
                                ptnu_nume = dr["ptnu_nume"].ToString(),
                                _asoc_codi = dr["_asoc_codi"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_asoc_codi"]),
                                _asoc_deno = dr["_asoc_deno"].ToString(),
                                prdt_rp_extr = dr["prdt_rp_extr"].ToString()
                            });
                        }
                    }
                    producto.ProductosNumeros = productosNumeros;
                    #endregion

                    #region Productos documentos
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[3].Rows)
                        {
                            productosDocum.Add(new TransferenciaAnimalEnPie_ProductosDocumEntity
                            {
                                prdo_id = Convert.ToInt32(dr["prdo_id"]),
                                prdo_prdt_id = dr["prdo_prdt_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdo_prdt_id"]),
                                prdo_path = dr["prdo_path"].ToString(),
                                prdo_refe = dr["prdo_refe"].ToString(),
                                prdo_imag = dr["prdo_imag"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdo_imag"]),
                                prdo_audi_user = Convert.ToInt32(dr["prdo_audi_user"]),
                                prdo_baja_fecha = dr["prdo_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["prdo_baja_fecha"]),
                                prdo_tmsp = dr["prdo_tmsp"] == DBNull.Value ? null : (byte[])dr["prdo_tmsp"],
                                prdo_obse = dr["prdo_obse"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["prdo_obse"]),
                                _path_ant = dr["_path_ant"].ToString(),
                                _prdo_imag = dr["_prdo_imag"].ToString(),
                                _estado = dr["_estado"].ToString(),
                                _parampath = dr["_parampath"].ToString()
                            });
                        }
                    }
                    producto.ProductosDocum = productosDocum;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Productos_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return producto;
        }

        public static void Tramites_Modi(TransferenciaAnimalEnPie_TramiteEntity Transferencia)
        {
            try
            {
                int tram_id = Transferencia.Tramites.First().tram_id;
                int tram_audi_user = Transferencia.Tramites.First().tram_audi_user ;
                int trad_id = 0;
                int trdo_id = 0;
                int trao_id = 0;
                int trpr_id = 0;

                #region Actualización tabla tamites_deta

                List<TransferenciaAnimalEnPie_TramitesDetaEntity> tramitesDeta = new List<TransferenciaAnimalEnPie_TramitesDetaEntity>();
                tramitesDeta = Tramites_Deta_Consul(tram_id);

                foreach (TransferenciaAnimalEnPie_TramitesDetaEntity item in Transferencia.TramitesDeta )
                {
                    item.trad_audi_user = tram_audi_user;
                    if (tramitesDeta.Where(x => x.trad_id == item.trad_id).Count() > 0)
                    {
                        // Modificación.
                        Tramites_Deta_Modi(item);
                    }
                    else
                    { 
                        // Alta.
                        trad_id = Convert.ToInt32(Tramites_Deta_Alta(item));
                        // Actualiza el objeto Observaciones con el trad_id dado de alta en la DB.
                        Transferencia.TramitesObse.Where(x => x.trao_trad_id == item.trad_id).First().trao_trad_id = trad_id;
                    }
                }

                // Baja.
                foreach (TransferenciaAnimalEnPie_TramitesDetaEntity item in tramitesDeta)
                {
                    if (!Transferencia.TramitesDeta.Exists(x => x.trad_id == item.trad_id))
                    {
                        Tramites_Deta_Baja(item.trad_id, tram_audi_user);
                    }
                }

                #endregion

                #region Actualización tabla tramites_docum

                List<TransferenciaAnimalEnPie_TramitesDocumEntity> tramitesDocum = new List<TransferenciaAnimalEnPie_TramitesDocumEntity>();
                tramitesDocum = Tramites_Docum_Consul(tram_id);

                foreach (TransferenciaAnimalEnPie_TramitesDocumEntity item in Transferencia.TramitesDocum)
                {
                    item.trdo_audi_user = tram_audi_user;
                    if (tramitesDocum.Where(x => x.trdo_id == item.trdo_id).Count() > 0)
                    {
                        // Modificación.
                        Tramites_Docum_Modi(item);
                    }
                    else
                    {
                        // Alta.
                        trdo_id = Convert.ToInt32(Tramites_Docum_Alta(item));
                    }
                }
                // Baja.
                foreach (TransferenciaAnimalEnPie_TramitesDocumEntity item in tramitesDocum)
                {
                    if (!Transferencia.TramitesDocum.Exists(x => x.trdo_id == item.trdo_id))
                    {
                        Tramites_Docum_Baja(item.trdo_id, tram_audi_user);
                    }
                }

                #endregion

                #region Actualización tabla tramites_obse
                List<TransferenciaAnimalEnPie_TramitesObseEntity> tramitesObse = new List<TransferenciaAnimalEnPie_TramitesObseEntity>();
                tramitesObse = TransferenciaAnimalEnPieBusiness.Tramites_Obse_Consul(tram_id);
                foreach (TransferenciaAnimalEnPie_TramitesObseEntity item in Transferencia.TramitesObse)
                {
                    item.trao_audi_user = tram_audi_user;
                    if (tramitesObse.Where(x => x.trao_id == item.trao_id).Count() > 0)
                    {
                        // Modificación.
                        Tramites_Obse_Modi(item);
                    }
                    else
                    {
                        // Alta.
                        trao_id = Convert.ToInt32(Tramites_Obse_Alta(item));
                    }
                }
                // Baja.
                foreach (TransferenciaAnimalEnPie_TramitesObseEntity item in tramitesObse)
                {
                    if (!Transferencia.TramitesObse.Exists(x => x.trao_id == item.trao_id))
                    {
                        Tramites_Obse_Baja(item.trao_id, item.trao_audi_user.ToString());
                    }
                }
                #endregion

                #region Actualización tabla tramites_productos

                List<TransferenciaAnimalEnPie_TramitesProductosEntity> tramitesProductos = new List<TransferenciaAnimalEnPie_TramitesProductosEntity>();
                tramitesProductos = Tramites_Productos_Consul(tram_id);

                foreach (TransferenciaAnimalEnPie_TramitesProductosEntity item in Transferencia.TramitesProductos)
                {
                    item.trpr_audi_user = tram_audi_user;
                    if (tramitesProductos.Where(x => x.trpr_id == item.trpr_id).Count() > 0)
                    {
                        // Modificación.
                        Tramites_Productos_Modi(item);
                    }
                    else
                    {
                        // Alta.
                        //trpr_id = Convert.ToInt32(Tramites_Docum_Alta(item));
                    }
                }
                // Baja.
                foreach (TransferenciaAnimalEnPie_TramitesProductosEntity item in tramitesProductos)
                {
                    if (!Transferencia.TramitesProductos.Exists(x => x.trpr_id == item.trpr_id))
                    {
                        //Tramites_Docum_Baja(item.trpr_id, tram_audi_user);
                    }
                }

                #endregion

                TransferenciaAnimalEnPieDao.Tramites_Modi(Transferencia);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Tramites_Modi, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static List<TransferenciaAnimalEnPie_TramitesDetaEntity> Tramites_Deta_Consul(int Trad_tram_id)
        {
            List<TransferenciaAnimalEnPie_TramitesDetaEntity> tramitesDeta = new List<Entities.TransferenciaAnimalEnPie_TramitesDetaEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.Tramites_Deta_Consul(Trad_tram_id);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tramitesDeta.Add(new TransferenciaAnimalEnPie_TramitesDetaEntity
                        {
                            trad_id = Convert.ToInt32(dr["trad_id"]),
                            trad_tram_id = Convert.ToInt32(dr["trad_tram_id"]),
                            trad_pend = dr["trad_pend"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_pend"]),
                            trad_fina_fecha = dr["trad_fina_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trad_fina_fecha"]),
                            trad_requ_id = dr["trad_requ_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_requ_id"]),
                            trad_obli = dr["trad_requ_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_requ_id"]),
                            trad_manu = dr["trad_manu"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trad_manu"]),
                            trad_baja_fecha = dr["trad_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trad_baja_fecha"]),
                            _estado = dr["_estado"].ToString(),
                            trad_audi_fecha = Convert.ToDateTime(dr["trad_audi_fecha"]),
                            trad_audi_user = Convert.ToInt32(dr["trad_audi_user"]),
                            trad_tmsp = dr["trad_tmsp"] == DBNull.Value ? null : (byte[])dr["trad_tmsp"],
                            _requ_desc = dr["_requ_desc"].ToString(),
                            _pend = dr["_pend"].ToString(),
                            _obli = dr["_obli"].ToString(),
                            _manu = dr["_manu"].ToString()
                        });
                    } 
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Tram_Deta_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }

            return tramitesDeta;
        }

        public static int Tramites_Deta_Alta(TransferenciaAnimalEnPie_TramitesDetaEntity TramitesDeta)
        {
            return TransferenciaAnimalEnPieDao.Tramites_Deta_Alta(TramitesDeta);
        }
        
        public static void Tramites_Deta_Baja(int trad_id, int trad_audi_user)
        {
            TransferenciaAnimalEnPieDao.Tramites_Deta_Baja(trad_id, trad_audi_user);
        }

        public static void Tramites_Deta_Modi(TransferenciaAnimalEnPie_TramitesDetaEntity TramitesDeta)
        {
            TransferenciaAnimalEnPieDao.Tramites_Deta_Modi(TramitesDeta);
        }

        public static List<TransferenciaAnimalEnPie_TramitesDocumEntity> Tramites_Docum_Consul(int Trdo_tram_id)
        {
            List<TransferenciaAnimalEnPie_TramitesDocumEntity> tramitesDocum = new List<TransferenciaAnimalEnPie_TramitesDocumEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.Tramites_Docum_Consul(Trdo_tram_id);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tramitesDocum.Add(new TransferenciaAnimalEnPie_TramitesDocumEntity
                        {
                            trdo_id = Convert.ToInt32(dr["trdo_id"]),
                            trdo_tram_id = Convert.ToInt32(dr["trdo_tram_id"]),
                            trdo_path = dr["trdo_path"].ToString(),
                            trdo_refe = dr["trdo_refe"].ToString(),
                            trdo_audi_fecha = Convert.ToDateTime(dr["trdo_audi_fecha"]),
                            trdo_audi_user = Convert.ToInt32(dr["trdo_audi_user"]),
                            trdo_baja_fecha = dr["trdo_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trdo_baja_fecha"]),
                            trdo_tmsp = dr["trdo_tmsp"] == DBNull.Value ? null : (byte[])dr["trdo_tmsp"],
                            _estado = dr["_estado"].ToString(),
                            _parampath = dr["_parampath"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Tram_Deta_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return tramitesDocum;
        }

        public static int Tramites_Docum_Alta(TransferenciaAnimalEnPie_TramitesDocumEntity TramitesDocum)
        {
            return TransferenciaAnimalEnPieDao.Tramites_Docum_Alta(TramitesDocum);
        }

        public static void Tramites_Docum_Baja(int trdo_id, int trdo_audi_user)
        {
            TransferenciaAnimalEnPieDao.Tramites_Docum_Baja(trdo_id, trdo_audi_user);
        }

        public static void Tramites_Docum_Modi(TransferenciaAnimalEnPie_TramitesDocumEntity TramitesDocum)
        {
            TransferenciaAnimalEnPieDao.Tramites_Docum_Modi(TramitesDocum);
        }

        public static List<TransferenciaAnimalEnPie_TramitesObseEntity> Tramites_Obse_Consul(int Trao_tram_id)
        {
            List<TransferenciaAnimalEnPie_TramitesObseEntity> tramitesObse = new List<TransferenciaAnimalEnPie_TramitesObseEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.Tramites_Obse_Consul(Trao_tram_id);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tramitesObse.Add(new TransferenciaAnimalEnPie_TramitesObseEntity
                        {
                            trao_id = Convert.ToInt32(dr["trao_id"]),
                            trao_tram_id = Convert.ToInt32(dr["trao_tram_id"]),
                            trao_trad_id = dr["trao_trad_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trao_trad_id"]),
                            _requ_desc = dr["_requ_desc"].ToString(),
                            _trad_requ_id = dr["_trad_requ_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["_trad_requ_id"]),
                            trao_obse = dr["trao_obse"].ToString(),
                            trao_fecha = Convert.ToDateTime(dr["trao_fecha"]),
                            trao_audi_fecha = Convert.ToDateTime(dr["trao_audi_fecha"]),
                            trao_audi_user = Convert.ToInt32(dr["trao_audi_user"]),
                            trao_tmsp = dr["trao_tmsp"] == DBNull.Value ? null : (byte[])dr["trao_tmsp"]
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Tramites_Obse_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return tramitesObse;
        }

        public static int Tramites_Obse_Alta(TransferenciaAnimalEnPie_TramitesObseEntity TramitesObse)
        {
            return TransferenciaAnimalEnPieDao.Tramites_Obse_Alta(TramitesObse);
        }

        public static void Tramites_Obse_Baja(int trao_id, string trao_audi_user)
        {
            TransferenciaAnimalEnPieDao.Tramites_Obse_Baja(trao_id, trao_audi_user);
        }

        public static void Tramites_Obse_Modi(TransferenciaAnimalEnPie_TramitesObseEntity TramitesObse)
        {
            TransferenciaAnimalEnPieDao.Tramites_Obse_Modi(TramitesObse); 
        }

        public static List<TransferenciaAnimalEnPie_TramitesProductosEntity> Tramites_Productos_Consul(int Trpr_tram_id)
        {
            List<TransferenciaAnimalEnPie_TramitesProductosEntity> tramitesProductos = new List<TransferenciaAnimalEnPie_TramitesProductosEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TransferenciaAnimalEnPieDao.Tramites_Productos_Consul(Trpr_tram_id);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tramitesProductos.Add(new TransferenciaAnimalEnPie_TramitesProductosEntity
                        {
                            trpr_id = Convert.ToInt32(dr["trpr_id"]),
                            trpr_prdt_id = dr["trpr_prdt_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_prdt_id"]),
                            trpr_tram_id = Convert.ToInt32(dr["trpr_tram_id"]),
                            trpr_apro = dr["trpr_apro"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_apro"]),
                            trpr_resu_fecha = dr["trpr_resu_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trpr_resu_fecha"]),
                            trpr_audi_fecha = Convert.ToDateTime(dr["trpr_audi_fecha"]),
                            trpr_audi_user = Convert.ToInt32(dr["trpr_audi_user"]),
                            trpr_tmsp = dr["trpr_tmsp"] == DBNull.Value ? null : (byte[])dr["trpr_tmsp"],
                            trpr_sra_nume = dr["trpr_sra_nume"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_sra_nume"]),
                            trpr_asoc_id = dr["trpr_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_asoc_id"]),
                            trpr_padre_asoc_id = dr["trpr_padre_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_padre_asoc_id"]),
                            trpr_madre_asoc_id = dr["trpr_madre_asoc_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_madre_asoc_id"]),
                            trpr_rp = dr["trpr_rp"].ToString(),
                            trpr_sexo = dr["trpr_sexo"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["trpr_sexo"]),
                            trpr_baja_fecha = dr["trpr_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trpr_baja_fecha"]),
                            trpr_apro_fecha = dr["trpr_apro_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["trpr_apro_fecha"])
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Tramites_Productos_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return tramitesProductos;
        }

        public static void Tramites_Productos_Modi(TransferenciaAnimalEnPie_TramitesProductosEntity TramitesProductos)
        {
            TransferenciaAnimalEnPieDao.Tramites_Productos_Modi(TramitesProductos);
        }

        public static int Tramites_Productos_Alta(TransferenciaAnimalEnPie_TramitesProductosEntity TramitesProductos)
        {
            return TransferenciaAnimalEnPieDao.Tramites_Productos_Alta(TramitesProductos);
        }
        
        public static int Tramites_Alta(TransferenciaAnimalEnPie_TramiteEntity Transferencia)
        {
            int tram_id = 0;
            int trad_id = 0;
            int trad_id_old = 0;
            int trdo_trad_id = 0;
            int trao_trad_id = 0;
            int trpr_id = 0;

            // Alta del trámite
            tram_id = TransferenciaAnimalEnPieDao.Tramites_Alta(Transferencia);

            #region Alta tramites_deta
            foreach (TransferenciaAnimalEnPie_TramitesDetaEntity item in Transferencia.TramitesDeta)
            {
                // Valor antes de agregar el item a la DB. Se usa para buscar si existe en el objeto TramitesObse.
                trad_id_old = item.trad_id;

                item.trad_tram_id = tram_id;
                trad_id = Tramites_Deta_Alta(item);

                // Recorre el objeto TramitesObse y asigna en nuevo trad_id al elemento que corresponda.
                foreach (TransferenciaAnimalEnPie_TramitesObseEntity to in Transferencia.TramitesObse)
                {
                    if (to.trao_trad_id == trad_id_old)
                    {
                        to.trao_trad_id = trad_id;
                    }
                }
            }
            #endregion

            #region Alta tramites_docum
            foreach (TransferenciaAnimalEnPie_TramitesDocumEntity item in Transferencia.TramitesDocum)
            {
                item.trdo_tram_id = tram_id;
                trdo_trad_id = Tramites_Docum_Alta(item);
            }
            #endregion

            #region Alta tramites_obse
            foreach (TransferenciaAnimalEnPie_TramitesObseEntity item in Transferencia.TramitesObse)
            {
                item.trao_tram_id = tram_id;
                trao_trad_id = Tramites_Obse_Alta(item);
            }
            #endregion

            #region Alta tramites_producto

            foreach (TransferenciaAnimalEnPie_TramitesProductosEntity item in Transferencia.TramitesProductos)
            {
                item.trpr_tram_id = tram_id;
                trpr_id = Tramites_Productos_Alta(item);
            }

            #endregion

            return tram_id;
        }

        public static string P_rg_ValidarTransferenciaProductosN(int TramiteProdId, int Usua_id, int? Retiene)
        {
            return TransferenciaAnimalEnPieDao.P_rg_ValidarTransferenciaProductosN(TramiteProdId, Usua_id, Retiene);
        }

        public static string Fc_Obtener_Tipo_Regimen(int usua_id, int prdt_id, string fecha_referencia, int seti_id)
        {
            return TransferenciaAnimalEnPieDao.Fc_Obtener_Tipo_Regimen(usua_id, prdt_id, fecha_referencia, seti_id);
        }

        public static string P_rg_TranferenciaProductoValidaCambioFecha(int prdt_id, DateTime fecha, int tram_id)
        {
            return TransferenciaAnimalEnPieDao.P_rg_TranferenciaProductoValidaCambioFecha(prdt_id, fecha, tram_id);
        }
    }
}
