﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Data;
using DAO;
using AccesoBD;
using Common;


namespace Business
{
    public class DatosUsrCtrolCLIEBusiness
    {
        public static DatosUsrCtrolCLIEEntity GetDatosUsrCtrolCLIE(int cria_id)
        {
            DatosUsrCtrolCLIEEntity obj = new DatosUsrCtrolCLIEEntity();
            try
            {
                DataSet ds = new DataSet();
                ds = DatosUsrCtrolCLIEDao.GetDatosUsrCtrolCLIE(cria_id);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    obj.raza_codi_desc = dr["raza_codi_desc"].ToString();
                    obj.cria_nume = Convert.ToInt32(dr["cria_nume"]);
                    obj.clie_fanta = dr["clie_fanta"].ToString();
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return obj;
        }
    }
}
