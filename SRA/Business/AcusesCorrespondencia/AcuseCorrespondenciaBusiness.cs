using System;
using System.Collections;
using DAO.AcusesCorrespondencia;
using Entities.AcusesCorrespondencia;

namespace Business.AcusesCorrespondencia
{
	/// <summary>
	/// Summary description for AcuseCorrespondencia.
	/// </summary>
	public class AcuseCorrespondenciaBusiness
	{
		/// <summary>
		/// metodo que retorna la coleccion de items de envio de comprobantes segun su
		/// varHashCodeEnvio recibido desde el cliente
		/// </summary>
		/// <param name="varHashCodeEnvio"></param>
		/// <returns>ArrayList</returns>
		public static ArrayList GetComprobantesEnviados(string varHashCodeEnvio)
		{
			return AcuseCorrespondenciaDao.GetComprobantesEnviados(varHashCodeEnvio);
		}

		/// <summary>
		/// metodo que registra el acuse de los documentos enviados
		/// </summary>
		/// <param name="varIdEnviosEmail"></param>
		/// <returns>bool</returns>
		public static bool MarcarComprobantesComoRecibidos(string varIdEnviosEmail, string varHostAcuse)
		{
			return AcuseCorrespondenciaDao.MarcarComprobantesComoRecibidos(varIdEnviosEmail, varHostAcuse);
		}

		/// <summary>
		/// metodo que retorna la coleccion de items de envio de comprobantesvtos segun su
		/// varHashCodeEnvio recibido desde el cliente
		/// </summary>
		/// <param name="varHashCodeEnvio"></param>
		/// <returns>ArrayList</returns>
		public static ArrayList GetComprobantesVtosEnviados(string varHashCodeEnvio)
		{
			return AcuseCorrespondenciaDao.GetComprobantesVtosEnviados(varHashCodeEnvio);
		}

		/// <summary>
		/// metodo que registra el acuse de los documentos vtos enviados
		/// </summary>
		/// <param name="varIdEnviosEmail"></param>
		/// <param name="varHostAcuse"></param>
		/// <returns></returns>
		public static bool MarcarComprobantesVtosComoRecibidos(string varIdEnviosEmail, string varHostAcuse)
		{
			return AcuseCorrespondenciaDao.MarcarComprobantesVtosComoRecibidos(varIdEnviosEmail, varHostAcuse);
		}


		/// <summary>
		/// metodo que retorna la coleccion de items de envio de estado de cuentas clientes
		/// varHashCodeEnvio recibido desde el cliente
		/// </summary>
		/// <param name="varHashCodeEnvio"></param>
		/// <returns>ArrayList</returns>
		public static ArrayList GetEstadoDeCuentaClienteEnviados(string varHashCodeEnvio)
		{
			return AcuseCorrespondenciaDao.GetEstadoDeCuentaClienteEnviados(varHashCodeEnvio);
		}

		/// <summary>
		/// metodo que registra el acuse de los estado de cuentas clientes
		/// </summary>
		/// <param name="varIdEnviosEmail"></param>
		/// <param name="varHostAcuse"></param>
		/// <returns></returns>
		public static bool MarcarEstadoDeCuentaClienteComoRecibidos(string varIdEnviosEmail, string varHostAcuse)
		{
			return AcuseCorrespondenciaDao.MarcarEstadoDeCuentaClienteComoRecibidos(varIdEnviosEmail, varHostAcuse);
		}

		/// <summary>
		/// metodo que obtiene el esatdo de cuenta del cliente
		/// actualizado 
		/// </summary>
		/// <param name="intIdCliente"></param>
		/// <returns>EnviosEstadoCuentasClienteEntity</returns>
		public static EnviosEstadoCuentasClienteEntity GetEstadoDeCuentaCliente(int intIdCliente)
		{
			return AcuseCorrespondenciaDao.GetEstadoDeCuentaCliente(intIdCliente);
		}

	}
}
