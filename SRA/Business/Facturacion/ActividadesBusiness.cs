using System;
using System.Data;
using DAO.CobranzasDao;
using DAO.Facturacion;
using DAO;
using Entities.Facturacion;
using Entities;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using Common;

namespace Business.Facturacion
{
	/// <summary>
	/// Summary description for ActividadesBusiness.
	/// </summary>
	public class ActividadesBusiness
	{
		// objeto que se utiliza para generar transacciones
		static DataBase _DataBase = new DataBase();

		/// <summary>
		/// Dario 2013-07-26 
		/// metodo que se utiliza para obtener una actividad por su id
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns>ActividadEntity</returns>
		public static ActividadEntity GetActividad(int IdActividad)
		{
			return ActividadDao.GetActividadPorId(IdActividad);
		}

		/// <summary>
		/// Dario 2013-07-02
		/// metodo que se uriliza para obtener si una actividad acepta generer proformas
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns>bool</returns>
		public static bool GetAdmiteProforma(int IdActividad)
		{
			return ActividadDao.GetActividadPorId(IdActividad).acti_AdmiteProforma;
		}

		/// <summary>
		/// metodo que retorna un bool que indica si la actividad
		/// permite el pago en cuotas
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns>bool</returns>
		public static bool GetPermitePagoEnCuotas(int IdActividad)
		{
			return ActividadDao.GetActividadPorId(IdActividad).acti_PermitePagoEnCuotas;
		}
		/// <summary>
		/// metodo que retorna un bool que indica si la actividad
		/// calcula sobretasas a diferentes vtos
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns>bool</returns>
		public static bool GetAplicaSobreTasaPagoRetrasado(int IdActividad)
		{
			return ActividadDao.GetActividadPorId(IdActividad).acti_AplicaSobreTasaPagoRetrasado;
		}

		/// <summary>
		/// Dario 2013-07-23
		/// metodo que se utiliza para obtener la cantidad de dias a vto de las notas de debito
		/// toma el vencimiento 1 de la actividad pasasa por parametro
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns></returns>
		public static int GetCantidadDiasVtoNotaDebito(int IdActividad)
		{
			return ActividadDao.GetActividadPorId(IdActividad).acti_vcto_cant_dias;
		}

	}
}
