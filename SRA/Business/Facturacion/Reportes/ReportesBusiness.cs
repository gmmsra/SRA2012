using System;
using System.Data;
using DAO;
using DAO.Facturacion.Reportes;

namespace Business.Facturacion.Reportes
{
	/// <summary>
	/// Summary description for ReportesBusiness.
	/// </summary>
	public class ReportesBusiness
	{
		// objeto que se utiliza para generar transacciones
		static DataBase _DataBase = new DataBase();
		
		/// <summary>
		/// metodo que retorna los datos para la exportacion de deuda integral a exel ExportaDeudaIntegral.aspx 
		/// </summary>
		/// <param name="IdCliente"></param>
		/// <param name="FechaFiltro"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetExportaDeudaIntegral(int IdCliente, DateTime FechaFiltro)
		{
			return ReportesDao.GetExportaDeudaIntegral(IdCliente, FechaFiltro);
		}
	}
}
