using System;
using System.Data;
using DAO.AcreditacionBancariaDao;
using DAO;
using Entities;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using Common;


namespace Business.Facturacion
{
	/// <summary>
	/// Summary description for AcreditacionBancariaBusiness.
	/// </summary>
	public class AcreditacionBancariaBusiness
	{
		// objeto que se utiliza para generar transacciones
	    static DataBase _DataBase = new DataBase();
		static string varNumerosComprobantesEnviados = string.Empty;

		public string NumerosComprobantesEnviados
		{
			get {return varNumerosComprobantesEnviados;}
		}

		/// <summary>
		/// metodo que se utiliza para hacer los asientos del ingreso de las cobranzas.
		/// procesa todas las filas que tiene las erroneas las logea y continua.
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="IdUsuario"></param>
		/// <returns>string</returns>
		public static string EnviarMailDeConfirmacionAcreditaciones(string lstrReciIds, string UsuarioEmail)
		{
			try
			{
				// verifico que tenga comprobantes generados correctamente para enviar el alerta
				if(lstrReciIds.Length > 0)
				{
					// envio mail de aviso a acreditacion bancarias
					MailManager mail = new MailManager();
					mail.IsHTMlBody = true;	
			
					// "dvasqueznigro@sra.org.ar;ggarayoa@sra.org.ar";
					// busca los mail segun el tipo de alerta, y envia el mismo tambien al usuario que 
					// realiza la operacion
					string emaildestino = mail.GetVarMailDestino(Constantes.ACREDITACION_BANCARIA_CONFIRMACION);
					if(emaildestino.Trim().Length > 0)
					{
						mail.VarEmail = UsuarioEmail + ";" +  mail.GetVarMailDestino(Constantes.ACREDITACION_BANCARIA_CONFIRMACION);
			
						// valido que tenga mail de destino
						if(mail.VarEmail.Length > 0 && 1 > 0 )
						{
							string asunto = "ACREDITACIONES BANCARIAS CONFIRMADAS. FECHA: " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm");

							// envio de mail
							mail.EnviaMensaje(asunto, GetBodyMail(lstrReciIds));
						}
					}
				}
			}
			catch(Exception ex)
			{
				// llamo al metodo del base encargado de manejar los errores
				_DataBase.gManejarError(ex, ex.Message);
			}
			return varNumerosComprobantesEnviados;
		}

		/// <summary>
		/// metodo que arma el body del mail de alerta para ACREDITACION_BANCARIA_INGRESO operacion ok
		/// </summary>
		/// <returns></returns>
		private static string GetBodyMail(string listIdComprobante)
		{
			// declaro la variable de retorno
			string respuesta = string.Empty;
			// bool 
			bool bitleyendaND = false; 
			// ejecuto la consulta a la base y recupero el detalle del mail
			DataSet ds = AcreditacionBancariaDao.GetAcreditacionesBancariasAInformarAlertaMail(listIdComprobante);
			// verifico que tenga registros
			if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
			{	
				varNumerosComprobantesEnviados = "Se generaron los recibos: ";
				// si hay filas armo el encabezado
				respuesta = getEncabezadoMail(System.DateTime.Now.ToString("dd/MM/yyyy"));
				// recorro los registros y armo el row del detalle
				foreach (DataRow dr in ds.Tables[0].Rows)
				{
					respuesta += "<tr>";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["coti_desc"].ToString().Trim() +"</b></font></td>" +"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='right'><font color='#7893da' face='arial' size='2'><b>" + dr["comp_nume"].ToString().Trim() +"</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["cliente"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b>" + dr["importe"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["cuba_desc"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b>" + dr["paco_nume"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b>" + dr["comp_ingr_fecha"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "</tr>"+"\r\n";

					// veriofico si cambio el tipo de comprobante y si ya no
					// agtregue la leyenda. 
					if(dr["orden"].ToString().Trim() == "2" && !bitleyendaND)
					{
						varNumerosComprobantesEnviados += "\r\n" + "Se generaron las Notas de Debitos: ";		
						bitleyendaND = true;
					}
					// agrego a la respuesta el numero de comprobante.
					varNumerosComprobantesEnviados += dr["comp_nume"].ToString().Trim() + ';';

				}
				// pongo el pie del mail
				respuesta += "</table></td></tr></table><br></body></html>";
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que retorna el encabezado para el mail de alerta
		/// </summary>
		/// <returns></returns>
		private static string getEncabezadoMail(string fecha)
		{
			// variable de respuesta
			string respuesta = string.Empty;
			// obtengo el link del servidor del sistema para pegarlo al mail
			string lnksist = ConfigurationSettings.AppSettings["conLnkSistema"];

			// armo encabezado
			respuesta += "<html><body><font color='#666666' face='arial' size='3'><b>NO RESPONDER ESTE MAIL</b></font>"
				+ "&nbsp;&nbsp;&nbsp;&nbsp; <font color='#666666' face='arial' size='2'>Haga click"
				+ "<a href='" + lnksist + "'>aqu�</a> para ingresar al sistema </font>" 
				+ "<br><br><font style='FONT-WEIGHT: normal;FONT-SIZE: 12pt;COLOR: #0054a3;FONT-FAMILY: Verdana' face='arial' size='2'>"
				+ "<b>Listado de recibos generados por confirmaci�n de acreditaciones bancarias. Fecha: " + fecha + "</b></font><br><br>"
				+ "<table border='1' width='100%' cellpadding='2' cellspacing='0' bordercolor='#003784' ID='Table1'>"
				+ "<tr><td width='100%'><table border='0' width='100%' bordercolor='#003784' ID='Table2'>"
				+ "<tr><td bgcolor='#7893da' valign='top' align='left'><font color='#ffffff' face='arial' size='2'><b>Tipo Comprob.</b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Comp. Nro.</b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Cliente</b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Importe</b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Cuenta</b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Nro.Oper.</b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Fecha </b></font></td>"
				+ "</tr>";

			return respuesta;
		}
	}
}
