using System;
using DAO;
using WSAFIPFacElect;

namespace Business.Facturacion
{
	/// <summary>
	/// Summary description for ClienteBusiness.
	/// </summary>
	public class ClienteBusiness
	{
		// objeto que se utiliza para generar transacciones
		static DataBase _DataBase = new DataBase();
		
		/// <summary>
		/// metodo que retorna la validacion del cliente tipo y nro de docu mas cuil si es empresa
		/// </summary>
		/// <param name="IdCliente"></param>
		/// <returns>string</returns>
		public static string ValidaClienteDatosAFIP(int IdCliente)
		{
			return ClientesDao.ValidaClienteDatosAFIP(IdCliente);
		}

		public static string IntegrarClienteAFinneg(string empresa, string clie_id, string tipoOper)
		{
			string respuesta = string.Empty;

			respuesta = WSFactElet.IntegrarClienteAFinneg(empresa, clie_id, tipoOper);
			
			return respuesta;
		}

	}
}
