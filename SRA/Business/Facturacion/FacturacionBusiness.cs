using System;
using System.Data;
using DAO.Facturacion;
using DAO;
using Entities.Facturacion;
using Entities;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using Common;

namespace Business.Facturacion
{
	/// <summary>
	/// Summary description for FacturacionBusiness.
	/// </summary>
	public class FacturacionBusiness
	{
		// objeto que se utiliza para generar transacciones
		static DataBase _DataBase = new DataBase();
		
		/// <summary>
		/// metodo que recupera una lista de vtos por actividad
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns>int[]</returns>
		public static int[] GetDiasSobreTasaVtosPorIdActividad(SqlTransaction trans,int IdActividad)
		{
			return FacturacionDao.GetDiasSobreTasaVtosPorIdActividad(trans, IdActividad);
		}
		
		/// <summary>
		/// sobrecarga que recupera una lista de vtos por actividad
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns></returns>
		public static int[] GetDiasSobreTasaVtosPorIdActividad(int IdActividad)
		{
			int[] respuesta;
			// abro la coneccion
			SqlConnection myConnectionSQL = new SqlConnection(_DataBase.mstrConn);
			myConnectionSQL.Open();
			// obtengo una transaccion
			SqlTransaction trans = myConnectionSQL.BeginTransaction(IsolationLevel.ReadCommitted);
			// ejecuto el metodo que recupera los datos
			respuesta = GetDiasSobreTasaVtosPorIdActividad(trans, IdActividad);
			// termina la transaccion
			trans.Commit();
			trans.Dispose();
			// termino la coneccion
			myConnectionSQL.Close();

			return respuesta;
		}


		/// <summary>
		/// metodo qu e carga la lista de comprobantes vtos
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="FechaVto"></param>
		/// <param name="RegOriginal"></param>
		/// <param name="idUsuario"></param>
		/// <param name="IdComprobante"></param>
		/// <param name="listComprobanteSobreTasaVtos"></param>
		public static void CargarColeccionComprobanteSobreTasaVtos( DataSet ds, DateTime FechaVto, bool RegOriginal 
																  , Int32 idUsuario, Int32 IdComprobante 
																  , ArrayList listComprobanteSobreTasaVtos)
		{
			try
			{	
				// recorro el ds de aranceles y sobretasas
				foreach(DataRow dr in ds.Tables["comprob_aranc"].Rows)
				{
					listComprobanteSobreTasaVtos.Add(CargarObjetoComprobanteSobreTasaVtosAranceles(dr, FechaVto , RegOriginal, idUsuario, IdComprobante));
				}
				// recorro el ds de conceptos COMPROB_CONCEP
				foreach(DataRow dr in ds.Tables["comprob_concep"].Rows)
				{
					listComprobanteSobreTasaVtos.Add(CargarObjetoComprobanteSobreTasaVtosConceptos(dr, FechaVto , RegOriginal, idUsuario, IdComprobante));
				}
			}
			catch(Exception ex)
			{
				string msg = ex.Message.ToString();
				// llamo al metodo del base encargado de manejar los errores
				_DataBase.gManejarError(ex, msg);
				throw new Exception(msg);
			}
		}


		/// <summary>
		/// metodo que crea las filas en comprob_aran desde las sobretasas calculadas
		/// para una fecha determinada de pago a futuro, son tomadas de la tabla COMPROB_Sobre_Tasa_Vtos
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="listComprobanteSobreTasaVtos"></param>
		/// <param name="idUsuario"></param>
		/// <param name="IdComprobante"></param>
		/// <returns>bool</returns>
		public static bool CrearComprobanteSobreTasaVtos(SqlTransaction trans, ArrayList listComprobanteSobreTasaVtos, Int32 idUsuario, Int32 IdComprobante)
		{
			bool respuesta = true;
			bool RegOriginal = true;
			bool AplicoUltimoCorte = false;
			DateTime fhCorte = System.DateTime.MinValue; 
			
			try
			{	
				// recorro el ds de aranceles y sobretasas
				foreach(ComprobanteSobreTasaVtosEntity item in listComprobanteSobreTasaVtos)
				{
					// controlo si es el primer registro si es asi seteo como fecha de corte
					// la fecha de aplicacion 
					if(fhCorte == System.DateTime.MinValue)
						fhCorte = item.coso_fecha_VtoAplica;
					// controlo que cambio la fecha que aplica 
					if(fhCorte != item.coso_fecha_VtoAplica)
					{
						// controlo que no sea la generacion de los originales de la factura
						if(!RegOriginal)
						{
							// genero las filas de sobretasas originales en cada nuevo calculo de fecha de sobretasas
							// se ingresas con singo negativo
							FacturacionDao.CrearSobreTasasOriginalesEnNuevoVto(trans, fhCorte, IdComprobante);
							// genero las filas de concepto originales en cada nuevo calculo de fecha de conceptos
							// se ingresas con singo negativo
							FacturacionDao.CrearConceptosOriginalesEnNuevoVto(trans, fhCorte, IdComprobante);
						}
						else
						{
							RegOriginal = false;
						}
						// paso la nueva fecha de corte
						fhCorte = item.coso_fecha_VtoAplica;
						AplicoUltimoCorte = false;
					}
					// paso el id de comprobante al item
					item.coso_comp_id = IdComprobante;
					// declaro la variable de respuesta de la operacion, recibe id de de la liena creada
					Int32 resulOper = -1;	
					// ejecuto el metodo que carga el objeto con los datos del item y llamo al 
					// metodo del dao que inserta la nueva fila en la tabla
					resulOper = FacturacionDao.CrearComprobanteSobreTasaVtos(trans, item);
					// indica que hay un nuevo corte en curso y debe de registras los originales
					// si sale del bucle por falta de elementos
					AplicoUltimoCorte = true;
					// si la operacion termina mal id < 0   genero mensaje de error 
					if(resulOper <= 0)
					{
						respuesta = false;
						throw new Exception("Error: FacturacionBusiness.CargarObjetoComprobanteSobreTasaVtosAranceles");
					}
				}
				
				// controlo si hay que generer los registos originanes al salir de foreach
				if(AplicoUltimoCorte)
				{
					// controlo que no sea la generacion de los originales de la factura
					if(!RegOriginal)
					{
						// genero las filas de sobretasas originales en cada nuevo calculo de fecha de sobretasas
						// se ingresas con singo negativo
						FacturacionDao.CrearSobreTasasOriginalesEnNuevoVto(trans, fhCorte, IdComprobante);
						// genero las filas de concepto originales en cada nuevo calculo de fecha de conceptos
						// se ingresas con singo negativo
						FacturacionDao.CrearConceptosOriginalesEnNuevoVto(trans, fhCorte, IdComprobante);
					}
					else
					{
						RegOriginal = false;
					}
				}

			}
			catch(Exception ex)
			{
				string msg = ex.Message.ToString();
				// llamo al metodo del base encargado de manejar los errores
				_DataBase.gManejarError(ex, msg);
				throw new Exception(msg);
			}

			return	respuesta;
		}


		/// <summary>
		/// metodo que registra en la base de datos las sobretasas recalculadas
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="ds"></param>
		/// <param name="FechaVto"></param>
		/// <returns>bool</returns>
		public static bool CrearComprobanteSobreTasaVtos(SqlTransaction trans, DataSet ds, DateTime FechaVto, bool RegOriginal, Int32 idUsuario, Int32 IdComprobante)
		{
			bool respuesta = true;

			try
			{	
				// recorro el ds de aranceles y sobretasas
				foreach(DataRow dr in ds.Tables["comprob_aranc"].Rows)
				{
					// declaro la variable de respuesta de la operacion, recibe id de de la liena creada
					Int32 resulOper = -1;	
					// ejecuto el metodo que carga el objeto con los datos del dr y llamo al 
					// metodo del dao que inserta la nueva fila en la tabla
					resulOper = FacturacionDao.CrearComprobanteSobreTasaVtos(trans, CargarObjetoComprobanteSobreTasaVtosAranceles(dr, FechaVto , RegOriginal, idUsuario, IdComprobante));
					// si la operacion termina mal id < 0   genero mensaje de error 
					if(resulOper <= 0)
					{
						respuesta = false;
						throw new Exception("Error: FacturacionBusiness.CargarObjetoComprobanteSobreTasaVtosAranceles");
					}
				}
				// recorro el ds de conceptos COMPROB_CONCEP
				foreach(DataRow dr in ds.Tables["comprob_concep"].Rows)
				{
					// declaro la variable de respuesta de la operacion, recibe id de de la liena creada
					Int32 resulOper = -1;	
					// ejecuto el metodo que carga el objeto con los datos del dr y llamo al 
					// metodo del dao que inserta la nueva fila en la tabla
					resulOper = FacturacionDao.CrearComprobanteSobreTasaVtos(trans, CargarObjetoComprobanteSobreTasaVtosConceptos(dr, FechaVto , RegOriginal, idUsuario, IdComprobante));
					// si la operacion termina mal id < 0   genero mensaje de error 
					if(resulOper <= 0)
					{
						respuesta = false;
						throw new Exception("Error: FacturacionBusiness.CrearComprobanteSobreTasaVtos comprob_concep");
					}
				}
				// controlo que no sea la generacion de los originales de la factura
				if(!RegOriginal)
				{
					// genero las filas de sobretasas originales en cada nuevo calculo de fecha de sobretasas
					// se ingresas con singo negativo
					FacturacionDao.CrearSobreTasasOriginalesEnNuevoVto(trans, FechaVto, IdComprobante);
					// genero las filas de concepto originales en cada nuevo calculo de fecha de conceptos
					// se ingresas con singo negativo
					FacturacionDao.CrearConceptosOriginalesEnNuevoVto(trans, FechaVto, IdComprobante);
				}

			}
			catch(Exception ex)
			{
				string msg = ex.Message.ToString();
				// llamo al metodo del base encargado de manejar los errores
				_DataBase.gManejarError(ex, msg);
				throw new Exception(msg);
			}

			return	respuesta;
		}


		/// <summary>
		/// metodo que crea las filas en comprob_aran desde las sobretasas calculadas
		/// para una fecha determinada de pago a futuro, son tomadas de la tabla COMPROB_Sobre_Tasa_Vtos
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="FechaPago"></param>
		/// <param name="IdComprobante"></param>
		/// <param name="IdUsuario"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearArancelesDeSobreTasasEnNuevoVto(SqlTransaction trans, DateTime FechaPago, Int32 IdComprobante
			, Int32 IdUsuario , Int32 IdComprobanteNew)
		{
			return FacturacionDao.CrearArancelesDeSobreTasasEnNuevoVto(trans, FechaPago, IdComprobante, IdUsuario, IdComprobanteNew);
		}


		/// <summary>
		/// metodo que recupera si la raza ingresada se registra en la sra
		/// </summary>
		/// <param name="Raza"></param>
		/// <param name="Criador"></param>
		/// <returns>bool</returns>
		public bool GetRazaInscribeEnSRA(string pstrArgs)
		{
			return FacturacionDao.GetRazaInscribeEnSRA(pstrArgs);
		}


		/// <summary>
		/// metodo que retorna los datos del panel de laboratotio para la factura con bonificacion
		/// se usa desde los js
		/// </summary>
		/// <param name="pstrArgs"></param>
		/// <param name="msDatosFact"></param>
		/// <returns>string</returns>
		public string GetDatosPanelLaboratorioAcumEspecie(string pstrArgs , DataSet msDatosFact)
		{	
			string[] parametros = pstrArgs.Split(';');
			int IdActividad = _DataBase.IntDBNull();
			int IdCliente = _DataBase.IntDBNull();
			int IdArancel = _DataBase.IntDBNull();
			DateTime FechaValor = _DataBase.DateTimeDBNull();
			
			if(parametros[0] != string.Empty)
				IdActividad = Convert.ToInt32(parametros[0]);

			if(parametros[1] != string.Empty)
				IdCliente = Convert.ToInt32(parametros[1]);

			if(parametros[2] != string.Empty)
				IdArancel = Convert.ToInt32(parametros[2]);
			
			if(parametros[3] != string.Empty)
				FechaValor = Convert.ToDateTime(parametros[3]);
				
			return this.GetDatosPanelLaboratorioAcumEspecie(IdActividad, IdCliente, IdArancel, FechaValor, msDatosFact);
		}

		/// <summary>
		/// metodo que retorna los datos del panel de laboratotio para la factura con bonificacion
		/// se usa desde los cs
		/// </summary> 
		/// <param name="pstrArgs"></param>
		/// <param name="msDatosFact"></param>
		/// <returns>string</returns>
		public string GetDatosPanelLaboratorioAcumEspecie(int IdActividad , int IdCliente, int IdArancel, DateTime FechaValor, DataSet msDatosFact)		
		{	
			string respuesta = string.Empty; 
			ResultPanelLabBonif datos = FacturacionDao.GetDatosPanelLaboratorioAcumEspecie(IdActividad, IdCliente, IdArancel, FechaValor);	
		
			int cantidadFactura = 0;

			foreach(DataRow dr in msDatosFact.Tables["comprob_aranc_concep"].Select("temp_borra is null and temp_IdEspecie=" + datos.IdEspecie.ToString()))
			{
				cantidadFactura+= Convert.ToInt32(dr["temp_cant"]);
			}

			respuesta = datos.TituAcum + ":;" 
					  + datos.Cantidad.ToString("#,##0")+ ";" 
					  + cantidadFactura.ToString("#,##0") + ";" 
				      + (datos.Cantidad + cantidadFactura).ToString("#,##0") + ";"	
				      + datos.IdEspecie.ToString()+ ";" 
				      + datos.fechaDesde.ToString("dd/MM/yyyy HH:mm") + ";" 
					  + datos.fechaHasta.ToString("dd/MM/yyyy HH:mm") + ";"
					  + datos.hoy + ";"	 
					  + datos.varEspecie.Trim() + ";"
					  + datos.varFacturasBonifLab.Trim(); 

			return respuesta;
		}
		/// <summary>
		/// metodo que retorna los datos del laboratotio para generar el concepto automatico de bonificacion
		/// de laboratorio
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <param name="IdCliente"></param>
		/// <param name="IdEspecie"></param>
		/// <param name="FechaValor"></param>
		/// <param name="msDatosFact"></param>
		/// <returns>ResultPanelLabBonif</returns>
		public ResultPanelLabBonif GetDatosLaboratorioAcumEspecie(int IdActividad , int IdCliente, int IdEspecie, DateTime FechaValor,  DataSet msDatosFact)		
		{	
			// creo que objeto de respuesta
			ResultPanelLabBonif respuesta = new ResultPanelLabBonif();
			// declaro las variables de calculo
			int cantidadFactura = 0;
			decimal importe = 0;
			decimal importeivai = 0;
			int IdArancel = 0;
			// recorro el ds tabla comprob_aranc_concep buscando los que no estan dados de baja y corresponde a la especie del parametro
			foreach(DataRow dr in msDatosFact.Tables["comprob_aranc_concep"].Select("temp_borra is null and temp_IdEspecie=" + IdEspecie.ToString()))
			{
				// acumulo las cantidades, y los importes
				cantidadFactura+= Convert.ToInt32(dr["temp_cant"]);
				importe+= Convert.ToDecimal(dr["temp_impo"]);
				importeivai+= Convert.ToDecimal(dr["temp_impo_ivai"]);
				// tomo el id del arancel para poder buscar el acumulado del cliente
				IdArancel = Convert.ToInt32(dr["temp_aran_concep_id"]);
			}
			// busco el acumulado del cliente para la especie actividad
			ResultPanelLabBonif datos = FacturacionDao.GetDatosPanelLaboratorioAcumEspecie(IdActividad, IdCliente, IdArancel, FechaValor);	
			// cargo el objeto de respuesta con los datos obtenidos
			respuesta.TituAcum = datos.TituAcum; 
			respuesta.Cantidad = datos.Cantidad;
			respuesta.CantidadFactura = cantidadFactura;
			respuesta.IdEspecie =  datos.IdEspecie;
			respuesta.Importe = importe;
			respuesta.ImporteIVAi = importeivai;
			respuesta.fechaDesde = datos.fechaDesde;
			respuesta.fechaHasta = datos.fechaHasta;
			respuesta.hoy = datos.hoy;
			respuesta.varEspecie = datos.varEspecie;
			respuesta.varFacturasBonifLab = datos.varFacturasBonifLab;

			return respuesta;
		}

		/// <summary>
		/// metodo que carga los datos del dr en el objeto ComprobanteSobreTasaVtosEntity
		/// </summary>
		/// <param name="dr"></param>
		/// <param name="FechaVto"></param>
		/// <returns>ComprobanteSobreTasaVtosEntity</returns>
		private static ComprobanteSobreTasaVtosEntity CargarObjetoComprobanteSobreTasaVtosAranceles(DataRow dr, DateTime FechaVto, bool RegOriginal
			, Int32 idUsuario , Int32 IdComprobante)
		{
			ComprobanteSobreTasaVtosEntity respuesta = new ComprobanteSobreTasaVtosEntity();

			respuesta.coso_id =  (dr["coan_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_id"]);
			respuesta.coso_aran_id =(dr["coan_aran_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_aran_id"]);
			respuesta.coso_conc_id = _DataBase.IntDBNull();
			respuesta.coso_comp_id = IdComprobante;
			respuesta.coso_cant	= (dr["coan_cant"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_cant"]);
			respuesta.coso_anim_no_fact	= (dr["coan_anim_no_fact"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_anim_no_fact"]);
			respuesta.coso_unit_impo = (dr["coan_unit_impo"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coan_unit_impo"]);
			respuesta.coso_fecha = (dr["coan_fecha"] == DBNull.Value)? _DataBase.DateTimeDBNull() : Convert.ToDateTime(dr["coan_fecha"]);
			respuesta.coso_fecha_VtoAplica	= FechaVto;
			respuesta.coso_inic_rp = (dr["coan_inic_rp"] == DBNull.Value)? string.Empty : dr["coan_inic_rp"].ToString();
			respuesta.coso_fina_rp = (dr["coan_fina_rp"] == DBNull.Value)? string.Empty : dr["coan_fina_rp"].ToString();
			respuesta.coso_tram	= (dr["coan_tram"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_tram"]);
			respuesta.coso_sin_carg	= (dr["coan_sin_carg"] == DBNull.Value)? false : Convert.ToBoolean(dr["coan_sin_carg"]);
			respuesta.coso_exen	= (dr["coan_exen"] == DBNull.Value)? false : Convert.ToBoolean(dr["coan_exen"]);
			respuesta.coso_impo	= (dr["coan_impo"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coan_impo"]);
			respuesta.coso_tasa_iva	= (dr["coan_tasa_iva"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coan_tasa_iva"]);
			respuesta.coso_impo_ivai = (dr["coan_impo_ivai"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coan_impo_ivai"]);
			respuesta.coso_audi_fecha = (dr["coan_audi_fecha"] == DBNull.Value)? _DataBase.DateTimeDBNull() : Convert.ToDateTime(dr["coan_audi_fecha"]);
			respuesta.coso_audi_user = idUsuario;
			respuesta.coso_ccos_id = (dr["coan_ccos_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_ccos_id"]);
			respuesta.coso_cant_sin_carg = (dr["coan_cant_sin_carg"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_cant_sin_carg"]);
			respuesta.coso_acta	= (dr["coan_acta"] == DBNull.Value)? string.Empty : dr["coan_acta"].ToString();
			respuesta.coso_pran_id = (dr["coan_pran_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_pran_id"]);
			//respuesta.coso_impo_iva	= (dr["coan_impo_iva"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coan_impo_iva"]);
			respuesta.coso_rrgg_aran_id	= (dr["coan_rrgg_aran_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coan_rrgg_aran_id"]);
			respuesta.coso_auto	= (dr["coan_auto"] == DBNull.Value)? false : Convert.ToBoolean(dr["coan_auto"]);
			respuesta.coso_desc_ampl = (dr["coan_desc_ampl"] == DBNull.Value)? string.Empty : dr["coan_desc_ampl"].ToString();
			respuesta.coso_RegOriginal = RegOriginal;

			return respuesta;
		}


		private static ComprobanteSobreTasaVtosEntity CargarObjetoComprobanteSobreTasaVtosConceptos(DataRow dr, DateTime FechaVto, bool RegOriginal
			, Int32 idUsuario , Int32 IdComprobante)
		{
			ComprobanteSobreTasaVtosEntity respuesta = new ComprobanteSobreTasaVtosEntity();

			respuesta.coso_id =  (dr["coco_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coco_id"]);
			respuesta.coso_aran_id =_DataBase.IntDBNull(); 
			respuesta.coso_conc_id = (dr["coco_conc_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coco_conc_id"]);
			respuesta.coso_comp_id = IdComprobante;
			respuesta.coso_cant	=  _DataBase.IntDBNull();
			respuesta.coso_anim_no_fact	= _DataBase.IntDBNull();
			respuesta.coso_unit_impo = (dr["coco_impo"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coco_impo"]);
			respuesta.coso_fecha = _DataBase.DateTimeDBNull();
			respuesta.coso_fecha_VtoAplica	= FechaVto;
			respuesta.coso_inic_rp = string.Empty;
			respuesta.coso_fina_rp = string.Empty;
			respuesta.coso_tram	= _DataBase.IntDBNull();
			respuesta.coso_sin_carg	=  false;
			respuesta.coso_exen	= false;
			respuesta.coso_impo	= (dr["coco_impo"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coco_impo"]);
			respuesta.coso_tasa_iva	= (dr["coco_tasa_iva"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coco_tasa_iva"]);
			//respuesta.coso_impo_ivai = (dr["coco_impo_ivai"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coco_impo_ivai"]);
			respuesta.coso_impo_ivai = (dr["coco_impo"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coco_impo"]);
			respuesta.coso_audi_fecha = (dr["coco_audi_fecha"] == DBNull.Value)? _DataBase.DateTimeDBNull() : Convert.ToDateTime(dr["coco_audi_fecha"]);
			respuesta.coso_audi_user = idUsuario;
			respuesta.coso_ccos_id = (dr["coco_ccos_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coco_ccos_id"]);
			respuesta.coso_cant_sin_carg = _DataBase.IntDBNull();
			respuesta.coso_acta	= string.Empty;
			respuesta.coso_pran_id = _DataBase.IntDBNull();
			//respuesta.coso_impo_iva	= (dr["coco_impo_iva"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coco_impo_iva"]);
			respuesta.coso_rrgg_aran_id	= _DataBase.IntDBNull();
			respuesta.coso_auto	= (dr["coco_auto"] == DBNull.Value)? false : Convert.ToBoolean(dr["coco_auto"]);
			respuesta.coso_desc_ampl = (dr["coco_desc_ampl"] == DBNull.Value)? string.Empty : dr["coco_desc_ampl"].ToString();
			respuesta.coso_coco_porc = (dr["coco_porc"] == DBNull.Value)? _DataBase.DecimalDBnull() : Convert.ToDecimal(dr["coco_porc"]);
			respuesta.coso_coco_prco_id = (dr["coco_prco_id"] == DBNull.Value)? _DataBase.IntDBNull() : Convert.ToInt32(dr["coco_prco_id"]);
			respuesta.coso_RegOriginal = RegOriginal;

			return respuesta;
		}

		
		/// <summary>
		/// metodo que retorna los parametros para la bonificacion del laboratorio
		/// </summary>
		/// <param name="IdRaza"></param>
		/// <returns>ParametosBonifLabEntity</returns>
		public static ParametosBonifLabEntity GetParametosBonifLaboratorio(int IdRaza)
		{
			return FacturacionDao.GetParametosBonifLaboratorio(IdRaza);
		}



	}
}
