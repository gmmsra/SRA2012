using System;
using System.Data;
using DAO.CobranzasDao;
using DAO.Facturacion;
using DAO;
using Entities.Facturacion;
using Entities;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using Common;
using WSAFIPFacElect; 


namespace Business.Facturacion
{
	/// <summary>
	/// Summary description for FacturacionBusiness.
	/// </summary>
	public class CobranzasBusiness
	{
		// objeto que se utiliza para generar transacciones
	    static DataBase _DataBase = new DataBase();

		/// <summary>
		/// metodo que reporta la respuesta de la consulta de cobranzas vs facturacion
		/// </summary>
		/// <param name="dateFechahasta"></param>
		/// <returns>string</returns>
		public static string GetDeudaPorActividadCliente(DateTime dateFechahasta)
		{
            //return CobranzasDao.GetDeudaPorActividadCliente(dateFechahasta);
			DataSet ds = CobranzasDao.GetDeudaPorActividadCliente(dateFechahasta);
			string respuesta = String.Empty;
			if(ds.Tables[0].Rows.Count > 0) 
			{
                respuesta += "<table>";
				// armo titulos
				respuesta += "<tr><td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>NRO.ACT.</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>ACTIVIDAD</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>NRO.CLI.</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>CLIENTE</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>EMAIL</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>Deuda Total</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>Venc_Ant_2013</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>Venc_Hasta_2013</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>Venc_Hasta_2_Ej</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>Vencida actual</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>Sin vencer</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>A Cuenta</td>";
				respuesta += "<td colspan='1' style='TEXT-ALIGN: left; font-size:large; font-weight:bold;background: #DAE9F0; text-decoration: underline'>Revision SRA</td></tr>";
				//

				foreach(DataRow dr in  ds.Tables[0].Rows)
				{
					respuesta += "<tr><td colspan='1'>"+ dr[0].ToString()+"</td>"; // NRO.ACT.
					respuesta += "<td colspan='1'>"+ dr[1].ToString()+"</td>"; // ACTIVIDAD
					respuesta += "<td colspan='1'>"+ dr[2].ToString()+"</td>"; // NRO.CLI.
					respuesta += "<td colspan='1'>"+ dr[3].ToString()+"</td>"; // CLIENTE
					respuesta += "<td colspan='1'>"+ dr[4].ToString()+"</td>"; // EMAIL
					respuesta += "<td colspan='1'>"+ dr[5].ToString().Replace(".", ",") +"</td>"; // Deuda Total
					respuesta += "<td colspan='1'>"+ dr[6].ToString().Replace(".", ",") +"</td>"; // Venc_Ant_2013
					respuesta += "<td colspan='1'>"+ dr[7].ToString().Replace(".", ",") +"</td>"; // Venc_Hasta_2013
					respuesta += "<td colspan='1'>"+ dr[8].ToString().Replace(".", ",") +"</td>"; // Venc_Hasta_2_Ej
					respuesta += "<td colspan='1'>"+ dr[9].ToString().Replace(".", ",") +"</td>"; // Vencida actual
					respuesta += "<td colspan='1'>"+ dr[10].ToString().Replace(".", ",") +"</td>"; // Sin vencer
					respuesta += "<td colspan='1'>"+ dr[11].ToString().Replace(".", ",") +"</td>"; // A Cuenta
					respuesta += "<td colspan='1'>"+ dr[12].ToString().Replace(".", ",") +"</td></tr>"; // Revisi�n SRA

				}
			}

            respuesta += "</table>";

			return respuesta;
		}
	

		/// <summary>
		/// Dario 2013-05-30
		/// metodo que se usa mostrar en la grilla de la pagina
		/// CobranzasRecepcionPagosMisCuentas.aspx
		/// </summary>
		/// <param name="Usuario"></param>
		/// <returns></returns>
		public static DataSet GetCobranzasARecepcionar(string Usuario, int IdEncabezado)
		{
			return CobranzasDao.GetCobranzasARecepcionar(Usuario, IdEncabezado);
		}

		
		/// <summary>
		/// metodo que se utiliza para obtener la cobranza mas antigua que no esta 
		/// para la recepcionada para empresa pasada por parametro
		/// </summary>
		/// <param name="IdEmpCobElect"></param>
		/// <returns></returns>
		public static CobranzasCabEntity GetEncabezadoCobranzasARecepcionar(int IdEmpCobElect)
		{
			return CobranzasDao.GetEncabezadoCobranzasARecepcionar(IdEmpCobElect);
		}
	
		
		/// <summary>
		/// metodo que retorna la listra de empresas cobradoras electronicas
		/// </summary>
		/// <param name="Usuario"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetEmpresasCobranzasElectronicas(string Usuario)
		{
			return CobranzasDao.GetEmpresasCobranzasElectronicas(Usuario);
		}

		
		/// <summary>
		/// metodo que se utiliza para hacer los asientos del ingreso de las cobranzas.
		/// procesa todas las filas que tiene las erroneas las logea y continua.
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="IdUsuario"></param>
		/// <returns></returns>
		public static string RecepcionarCobranzasElectronicas(DataSet ds, CobranzasCabEntity objCobranzasCab, int IdUsuario, string lstrHost)
		{
			string respuesta = string.Empty;
			string listIdComprobante = string.Empty;
			// variable que tiene la cantidad de decimales para aplicar el redondeo
			//int cantDecimales = 2;
			ParametrosEntity objParametros = ParametrosDao.GetAllParametros();
			// obtengo la cotizacion del dolar para el detalle
			decimal code_coti = CobranzasDao.GetUltimaCotizacionDolar(); 

			// abro la coneccion
			SqlConnection myConnectionSQLPpal = new SqlConnection(_DataBase.mstrConn);
			myConnectionSQLPpal.Open();
			// obtengo una transaccion
			SqlTransaction transPpal = myConnectionSQLPpal.BeginTransaction(IsolationLevel.ReadCommitted);

			try
			{	
				// controlo que no se modifico ya esta cobranza
				CobranzasCabEntity tmpObjCobCab = CobranzasDao.GetEncabezadoCobranzasARecepcionar(transPpal, objCobranzasCab.intIdEmpresaCobElec);
				if(tmpObjCobCab.intIdInterCobElecCab != objCobranzasCab.intIdInterCobElecCab)
					throw new Exception("Error: La informacion fue modificada por otro usuario!!!.");

				foreach(DataRow item in ds.Tables[0].Rows)
				{
					// calculo del importe y del descuento
					// tomo el importe pagado por PmC
					decimal importePmC = Convert.ToDecimal(item["Importe"]);
					decimal importe42AcuseConNotaDebito = -1;
					// para el importe, redondeo la comision a dos decimales
					decimal importeARegistrar = 0;
					decimal importeComisionApagar = 0;
					// Dario 2013-06-13 Se comenta por nueva definicion
					// calculo la comisioon 
					//				decimal importeComision = (importePmC * objParametros.para_Porc_ComisionPmC) / 100;
					//				// calculo el iva de la comicion
					//				decimal impIvaComision = (importeComision * objParametros.para_Iva_ComisionPmC) / 100;
					//				// calculo el importe minimo con iva incluido
					//				decimal importeComisionMinimoConIva = objParametros.para_MontoMinimo_ComisionPmC 
					//													+ ((objParametros.para_MontoMinimo_ComisionPmC * objParametros.para_Iva_ComisionPmC ) /100);
					//				// valido que comision me quedo si la minima si no supera el monto minimo o la calculada
					//				if((importeComision + impIvaComision) < importeComisionMinimoConIva)
					//					importeComisionApagar = decimal.Round(importeComisionMinimoConIva, cantDecimales ) * -1 ;
					//				else
					//					importeComisionApagar = decimal.Round((importeComision + impIvaComision),cantDecimales) * -1 ;

					// calculo el importe a registra importe del recibo menos % pagos mis cuentas
					importeARegistrar = importePmC + importeComisionApagar;
				
					#region populado de objeto
					ComprobanteEntity objComprobante = new ComprobanteEntity();
					// variable de comp id que se usara para la busqueda
					Int32 CompId = _DataBase.IntDBNull();
					Int32 CoVtId = _DataBase.IntDBNull();
					// determino que tipo de pago es si cta cte o socios
					string[] tmpIdFact = item["IdFactura"].ToString().Split('-');
					//
					if(tmpIdFact[0] == "C")
					{
						// busco por el covt_id recibido de PmC el comprobante que corresponde cancelar
						objComprobante.ComprobanteVtos = CobranzasDao.GetComprobanteVtosPorCoVtId(Convert.ToInt32(tmpIdFact[1]));
						CompId = objComprobante.ComprobanteVtos.covt_comp_id; 
						CoVtId = objComprobante.ComprobanteVtos.covt_id;
					}
					else
					{
						// es socio busco por el dato que viene del archivo
						CompId = Convert.ToInt32(tmpIdFact[1]); 
					}

					// busco los datos del comprobante a cancelar
					ComprobanteEntity objComprobanteACancelar = ComprobanteDao.GetComprobantePorCompId(CompId);

					//Dario 2013-07-26 Objeto actividad con el id de actividad del comprobante a cancelar
					//cargo el bool si acepta el pago de sobretasas por vtos retrasados
					//cargo el int de la cantidad de dias del vto de la nota de debito
					ActividadEntity _actividad = ActividadesBusiness.GetActividad(objComprobanteACancelar.comp_acti_id);
					bool bitAplicaSobreTasaPagoRetrasado  = _actividad.acti_AplicaSobreTasaPagoRetrasado;
					// Dario 2014-03-05 cambio para que genere recargo admin
					bool bitAplicaInteresORecargo =	_actividad.acti_gene_inte;
					int intCandidadDiasVtos =  _actividad.acti_vcto_cant_dias;
					int intCantidadDiasVtoNotaDebito = _actividad.acti_vcto_fijo_dia;
					decimal decProcInteresORecargo = _actividad.acti_inte_porc;

					decimal impNotaDebito = 0;
					bool bitAplicaNotaDebito = false;

					// Dario 2013-07-26 
					// si la forma de pago es cuenta corriente y la actividad permite el pago en cuotas
					// cerifico que que pago mayor importe por sobretasas por vtos
					// modif Dario 2014-03-05 se agrega bitAplicaInteresORecargo	
					if (objComprobanteACancelar.comp_cpti_id == (int)ComprobPagosTipos.CtaCte 
						&& (bitAplicaSobreTasaPagoRetrasado || bitAplicaInteresORecargo))
					{
						// verifico que se pago importe mayor al comprobante
						// qoriginal que se cancela, es entonces que 
						// se abono sobretasa por pago en diferente vto del original
						if(importePmC > objComprobante.ComprobanteVtos.covt_impo)
						{
							// calculo el importe de la nota de debito
							impNotaDebito = importePmC - objComprobante.ComprobanteVtos.covt_impo;
							// toma el importe de la factura que cancela el 42 para poner este en el acuse
							// del 42
							importe42AcuseConNotaDebito = objComprobante.ComprobanteVtos.covt_impo;
							// seteo el bit en true para que luego de la crear el compriobante tipo 42
							// cree la nota de debito id 32
							bitAplicaNotaDebito = true;
						}
					}

					// cargo el objcomprobante con los datos que coresponda al id 42
					objComprobante = CargarComprobante(objComprobante
													 , objComprobanteACancelar
													 , item
													 , objParametros 
													 , CompId
													 , CoVtId
													 , IdUsuario
													 , code_coti
													 , importePmC		
													 , importeARegistrar
													 , importeComisionApagar
													 , importe42AcuseConNotaDebito
													 , lstrHost
													);
					
					#endregion

					#region base de datos
					// declaro la variable que recibira los datos de los identitys que retornaran
					// los direntes metodos de insercion llamados en esta seccion
					Int32 idInsert = _DataBase.IntDBNull();
					// abro la coneccion
					SqlConnection myConnectionSQL = new SqlConnection(_DataBase.mstrConn);
					myConnectionSQL.Open();
					// obtengo una transaccion
					SqlTransaction trans = myConnectionSQL.BeginTransaction(IsolationLevel.ReadCommitted);
					// se atrapa el error de la fila que genera el error y se continua luego de logear el mimo
					// el RecepcionarCobranzaselectronicas atrapa error y continua con el siguente
					try
					{
						// inserto el comprobante
						objComprobante.SetCompId = ComprobanteDao.CrearComprobante(trans , objComprobante);
						// controlo que inserto sino tiro error
						if(objComprobante.comp_id <= 0)
							throw new Exception("Error: ComprobanteDao.CrearComprobante");

						// inserto el detalle del comprobante
						idInsert = ComprobanteDetalleDao.CrearComprobanteDetalle(trans, objComprobante.ComprobanteDeta);
						// controlo que inserto sino tiro error
						if(idInsert <= 0)
							throw new Exception("Error: ComprobanteDetalleDao.CrearComprobanteDetalle");

						// inserto el pago del comprobante
						idInsert = ComprobantePagoDao.CrearComprobantePago(trans, objComprobante.ComprobantePagos);
						// controlo que inserto sino tiro error
						if(idInsert <= 0)
							throw new Exception("Error: ComprobantePagoDao.CrearComprobantePago");

						// inserto el acuse del comprobante
						idInsert = ComprobanteAcuseDao.CrearComprobanteAcuse(trans, objComprobante.ComprobantesAcuses);
						// controlo que inserto sino tiro error
						if(idInsert <= 0)
							throw new Exception("Error: ComprobanteAcuseDao.CrearComprobanteAcuse");

						// con la operacion concluida en el registro de comprobante pasamos el id del 
						// mismo a la tabla sra_migra.dbo.inter_CobranzasElectronicas_Detalle donde quedara asentado
						int intUpdate =  CobranzasDao.SetIdComprobanteToCobranzasElectDet(Convert.ToInt32(item["Id"])
							, objComprobante.comp_id);
						// controlo que se realizo el update correctamente
						if(intUpdate <= 0)
							throw new Exception("Error: CobranzasDao.SetIdComprobanteToCobranzasElectDet");

						// acumulo todos los id de comprobantes que se generaron ok para el envio de mails de alerta de la operacion
						listIdComprobante +=  objComprobante.comp_id.ToString() + "|";

						if(bitAplicaNotaDebito)
						{
							// modif: Dario 2014-03-06
							if(!bitAplicaInteresORecargo) 
							{
								// Dario 2014-03-06 (entra por el flujo normal de sobre tasas)
								// tomo el id del comprobante 42 recien insertado para el  coac_canc_comp_id
								// de la nota de debito
								Int32 comp_id42 = objComprobante.comp_id;

								// seteo los id de comprobante a generar como null para 
								// usar este objeto como base para generar la nota de debito
								// modificando solo los vales del objeto que sean necesarios
								objComprobante.SetCompId = _DataBase.IntDBNull();
								// cargo la nota de debito sobre el objeto comprobante 42
								// modificando solo los datos que hagan falta
								objComprobante = CargarNotaDebito(objComprobante
																, objComprobanteACancelar
																, impNotaDebito
																, intCantidadDiasVtoNotaDebito 
																);
								// inserto el comprobante 
								objComprobante.SetCompId = ComprobanteDao.CrearComprobante(trans , objComprobante);
								// controlo que inserto sino tiro error
								if(objComprobante.comp_id <= 0)
									throw new Exception("Error: ComprobanteDao.CrearComprobanteND");
								// seteo el valor del id de comprobante 32 ND
								objComprobante.ComprobantesAcuses.coac_comp_id = objComprobante.comp_id; // id del comprobante que se cancela
								// seteo el valor del id del comprobante 42 que se inserto antes
								objComprobante.ComprobantesAcuses.coac_canc_comp_id = comp_id42;

								// inserto el detalle del comprobante
								idInsert = ComprobanteDetalleDao.CrearComprobanteDetalle(trans, objComprobante.ComprobanteDeta);
								// controlo que inserto sino tiro error
								if(idInsert <= 0)
									throw new Exception("Error: ComprobanteDetalleDao.CrearComprobanteDetalleND");

								// inserto el vto del comprobante
								idInsert = ComprobanteVtoDao.CrearComprobanteVto(trans, objComprobante.ComprobanteVtos);
								// controlo que inserto sino tiro error
								if(idInsert <= 0)
									throw new Exception("Error: ComprobanteVtoDao.CrearComprobanteVtoND");

								// seteo el valor del covt_id del comprobante 32 ND
								objComprobante.ComprobantesAcuses.coac_covt_id = idInsert; // covt_id de comprobante a cancelar

								// inserto el acuse del comprobante
								idInsert = ComprobanteAcuseDao.CrearComprobanteAcuse(trans, objComprobante.ComprobantesAcuses);
								// controlo que inserto sino tiro error
								if(idInsert <= 0)
									throw new Exception("Error: ComprobanteAcuseDao.CrearComprobanteAcuseND");

								// inserto el detalle de los aranceles de la nota de debito								
								idInsert = FacturacionDao.CrearArancelesDeSobreTasasEnNuevoVto( trans
																							  , objComprobante.comp_fecha 
																							  , objComprobanteACancelar.comp_id
																							  , IdUsuario
																							  , objComprobante.comp_id
																							  );
							
								// inserto el detalle de los conceptos de la nota de debito								
								idInsert = FacturacionDao.CrearConceptosDeSobreTasasEnNuevoVto( trans
																							  , objComprobante.comp_fecha 
																						      , objComprobanteACancelar.comp_id
																							  , IdUsuario
																							  , objComprobante.comp_id
																							  );
								// solicito cae a la afip para esta nota de debito
								GetCAE(trans, objComprobante.comp_id);
							}
							else // aplica inetreses o recargos admin
							{
								// tomo el id del comprobante 42 recien insertado para el  coac_canc_comp_id
								// de la nota de debito
								Int32 comp_id42 = objComprobante.comp_id;

								// seteo los id de comprobante a generar como null para 
								// usar este objeto como base para generar la nota de debito
								// modificando solo los vales del objeto que sean necesarios
								objComprobante.SetCompId = _DataBase.IntDBNull();
								// cargo la nota de debito sobre el objeto comprobante 42
								// modificando solo los datos que hagan falta
								objComprobante = CargarNotaDebitoRecargoOInteres( objComprobante
																			    , objComprobanteACancelar
																				, impNotaDebito	
																				, intCantidadDiasVtoNotaDebito 
																				);
								// inserto el comprobante 
								objComprobante.SetCompId = ComprobanteDao.CrearComprobante(trans , objComprobante);
								// controlo que inserto sino tiro error
								if(objComprobante.comp_id <= 0)
									throw new Exception("Error: ComprobanteDao.CrearComprobanteND");
								// seteo el valor del id de comprobante 32 ND
								objComprobante.ComprobantesAcuses.coac_comp_id = objComprobante.comp_id; // id del comprobante que se cancela
								// seteo el valor del id del comprobante 42 que se inserto antes
								objComprobante.ComprobantesAcuses.coac_canc_comp_id = comp_id42;

								// inserto el detalle del comprobante
								idInsert = ComprobanteDetalleDao.CrearComprobanteDetalle(trans, objComprobante.ComprobanteDeta);
								// controlo que inserto sino tiro error
								if(idInsert <= 0)
									throw new Exception("Error: ComprobanteDetalleDao.CrearComprobanteDetalleND");

								// inserto el vto del comprobante
								idInsert = ComprobanteVtoDao.CrearComprobanteVto(trans, objComprobante.ComprobanteVtos);
								// controlo que inserto sino tiro error
								if(idInsert <= 0)
									throw new Exception("Error: ComprobanteVtoDao.CrearComprobanteVtoND");

								// seteo el valor del covt_id del comprobante 32 ND
								objComprobante.ComprobantesAcuses.coac_covt_id = idInsert; // covt_id de comprobante a cancelar

								// inserto el acuse del comprobante
								idInsert = ComprobanteAcuseDao.CrearComprobanteAcuse(trans, objComprobante.ComprobantesAcuses);
								// controlo que inserto sino tiro error
								if(idInsert <= 0)
									throw new Exception("Error: ComprobanteAcuseDao.CrearComprobanteAcuseND");

								// inserto el detalle de los conceptos de la nota de debito								
								idInsert = FacturacionDao.CrearConceptosDeRecargoAdminOInteres(trans
																							 , objComprobanteACancelar.comp_id
																							 , IdUsuario
																							 , objComprobante.comp_id
																							 , impNotaDebito
																							 );
								
								if(idInsert <= 0)
									throw new Exception("Error: FacturacionDao.CrearConceptosDeRecargoAdminOInteres");

								// solicito cae a la afip para esta nota de debito
								GetCAE(trans, objComprobante.comp_id);
							}
						}
						// llegamos hasta aca no hubo errores commit y seguimos con otra recepcion de pagos mis cuentas
						trans.Commit(); 
					}
					catch(Exception ex)
					{
						// armo mensaje de error
						string Msg = "CobranzasBusiness.RecepcionarCobranzasPagosMisCuentas: " 
							+ "Recepcion Comprobante nro: " + objComprobante.comp_id + " Erronea. Cliente:" +  item["varDescipcion"].ToString()
							+ " registro el siguiente error: " + ex.Message; 
						// concateno la respuesta del error para retornarla al usuario con todas las recepciones con error si 
						// las hubiera
						respuesta += "Recepcion Comprobante nro: " + objComprobante.comp_id + " Erronea. Cliente:" +  item["varDescipcion"].ToString() + "\\n";

						// errro rollback
						trans.Rollback();

						// llamo al metodo del base encargado de manejar los errores
						_DataBase.gManejarError(ex , Msg);
					}
					finally
					{
						trans.Dispose();
						myConnectionSQL.Close();
					}
					#endregion
				}
				
				// todo termino ok updeteo el encabezado de la cobranza con la fecha de proceso, sino solo updatelo los datos de 
				// nro comprobante y recaudado neto
				if(respuesta.Trim().Length == 0)
					objCobranzasCab.dateFechaProceso = System.DateTime.Now;
				else
					objCobranzasCab.dateFechaProceso = _DataBase.DateTimeDBNull();

				// ejecuto el meto de modificacion de la cabecera
			    int intUpdateModif = CobranzasDao.ModifEncabezadoCobranzasElectronicas(transPpal, objCobranzasCab);
				
				// controlo que se realizo el update correctamente
				if(intUpdateModif <= 0)
					throw new Exception("Error: CobranzasDao.ModifEncabezadoCobranzasElectronicas");

				// llegamos hasta aca no hubo errores commit y seguimos con otra recepcion de pagos mis cuentas
				transPpal.Commit();

				// verifico que tenga comprobantes generados correctamente para enviar el alerta
				if(listIdComprobante.Length > 0)
				{
					// envio mail de aviso a cobranzas
					MailManager mail = new MailManager();
					mail.IsHTMlBody = true;	
					// conLnkSistema
			
					// "dvasqueznigro@sra.org.ar;ggarayoa@sra.org.ar";
					mail.VarEmail = mail.GetVarMailDestino(Constantes.ACREDITACION_BANCARIA_INGRESO) ;
			
					// valido que tenga mail de destino
					if(mail.VarEmail.Length > 0 && listIdComprobante.Length > 0 )
					{
						string asunto = "ACREDITACIONES BANCARIAS INGRESADAS. FECHA:" + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm");

						// envio de mail
						mail.EnviaMensaje(asunto, GetBodyMail(listIdComprobante, objCobranzasCab));
					}
				}
			}
			catch(Exception ex)
			{
				respuesta += ex.Message.ToString();
				// error rollback
				if(transPpal.Connection != null)
					transPpal.Rollback();

				// llamo al metodo del base encargado de manejar los errores
				_DataBase.gManejarError(ex, respuesta);
			}
			finally
			{
				transPpal.Dispose();
				myConnectionSQLPpal.Close();
			}
			// retorno de mensaja de respuesta de la operacion
			return respuesta;
		}

		/// <summary>
		/// metodo que carga el objeto comprobante entity de la nota de debito para sobretasas
		/// </summary>
		/// <param name="objComprobante"></param>
		/// <param name="objComprobanteACancelar"></param>
		/// <param name="impNotaDebito"></param>
		/// <param name="intCantidadDiasVtoNotaDebito"></param>
		/// <returns>ComprobanteEntity</returns>
		private static ComprobanteEntity CargarNotaDebito( ComprobanteEntity objComprobante
													     , ComprobanteEntity objComprobanteACancelar
													  	 , decimal impNotaDebito
														 , int intCantidadDiasVtoNotaDebito
														)
		{
			// acuses
			objComprobante.ComprobantesAcuses.coac_id = _DataBase.IntDBNull();
			objComprobante.ComprobantesAcuses.coac_canc_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobantesAcuses.coac_impor = impNotaDebito; //importePmC;
			
			// comprobante
			objComprobante.comp_id = _DataBase.IntDBNull();
			objComprobante.comp_dh = true;
			objComprobante.comp_neto = impNotaDebito;
			objComprobante.comp_cance = false;
			objComprobante.comp_coti_id = (int)ComprobTipos.ND;
			objComprobante.comp_letra = objComprobanteACancelar.comp_letra;
			objComprobante.comp_cpti_id = (int)ComprobPagosTipos.CtaCte;
			objComprobante.comp_acti_id = objComprobanteACancelar.comp_acti_id;
			objComprobante.comp_modu_id = (int)Modulos.SobreTasa_PagoFuera_termino; // facturacion
		
			// detalle del comprobante
			objComprobante.ComprobanteDeta.code_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_asoc_comp_id = objComprobante.ComprobantesAcuses.coac_comp_id; // id de comprobante que cancela

			// vto del comprobante
			objComprobante.ComprobanteVtos.covt_id	= _DataBase.IntDBNull();
			objComprobante.ComprobanteVtos.covt_comp_id	= _DataBase.IntDBNull();
			objComprobante.ComprobanteVtos.covt_fecha = System.DateTime.Now.AddDays(intCantidadDiasVtoNotaDebito) ;
			objComprobante.ComprobanteVtos.covt_impo = impNotaDebito;
			objComprobante.ComprobanteVtos.covt_porc = 100;
			objComprobante.ComprobanteVtos.covt_cance = false;
			objComprobante.ComprobanteVtos.covt_audi_fecha = System.DateTime.Now;
			objComprobante.ComprobanteVtos.covt_audi_user = objComprobante.comp_audi_user;	
			objComprobante.ComprobanteVtos.covt_compati_1 = _DataBase.IntDBNull();					

			return objComprobante;
		
		}


		/// <summary>
		/// metodo que carga el objeto comprobante entity de la nota de debito para recargos o intereses
		/// </summary>
		/// <param name="objComprobante"></param>
		/// <param name="objComprobanteACancelar"></param>
		/// <param name="impNotaDebito"></param>
		/// <param name="intCantidadDiasVtoNotaDebito"></param>
		/// <returns>ComprobanteEntity</returns>
		private static ComprobanteEntity CargarNotaDebitoRecargoOInteres( ComprobanteEntity objComprobante
																		, ComprobanteEntity objComprobanteACancelar
																		, decimal impNotaDebito
																		, int intCantidadDiasVtoNotaDebito
																		)
		{
			// acuses
			objComprobante.ComprobantesAcuses.coac_id = _DataBase.IntDBNull();
			objComprobante.ComprobantesAcuses.coac_canc_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobantesAcuses.coac_impor = impNotaDebito; //importePmC;
			
			// comprobante
			objComprobante.comp_id = _DataBase.IntDBNull();
			objComprobante.comp_dh = true;
			objComprobante.comp_neto = impNotaDebito;
			objComprobante.comp_cance = false;
			objComprobante.comp_coti_id = (int)ComprobTipos.ND;
			objComprobante.comp_letra = objComprobanteACancelar.comp_letra;
			objComprobante.comp_cpti_id = (int)ComprobPagosTipos.CtaCte;
			objComprobante.comp_acti_id = objComprobanteACancelar.comp_acti_id;
			objComprobante.comp_modu_id = (int)Modulos.Cobranza; // cobranzas
		
			// detalle del comprobante
			objComprobante.ComprobanteDeta.code_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_asoc_comp_id = objComprobante.ComprobantesAcuses.coac_comp_id; // id de comprobante que cancela

			// vto del comprobante
			objComprobante.ComprobanteVtos.covt_id	= _DataBase.IntDBNull();
			objComprobante.ComprobanteVtos.covt_comp_id	= _DataBase.IntDBNull();
			objComprobante.ComprobanteVtos.covt_fecha = System.DateTime.Now.AddDays(intCantidadDiasVtoNotaDebito) ;
			objComprobante.ComprobanteVtos.covt_impo = impNotaDebito;
			objComprobante.ComprobanteVtos.covt_porc = 100;
			objComprobante.ComprobanteVtos.covt_cance = false;
			objComprobante.ComprobanteVtos.covt_audi_fecha = System.DateTime.Now;
			objComprobante.ComprobanteVtos.covt_audi_user = objComprobante.comp_audi_user;	
			objComprobante.ComprobanteVtos.covt_compati_1 = _DataBase.IntDBNull();					

			return objComprobante;
		
		}


		/// <summary>
		/// Dario 2013-07-26
		/// metodo que carga el obteto comprobante
		/// </summary>
		/// <param name="objComprobante"></param>
		/// <param name="objComprobanteACancelar"></param>
		/// <param name="item"></param>
		/// <param name="objParametros"></param>
		/// <param name="CompIdCancela"></param>
		/// <param name="CoVtIdCancela"></param>
		/// <param name="IdUsuario"></param>
		/// <param name="CotizDolar"></param>
		/// <param name="coac_impor"></param>
		/// <param name="importeARegistrar"></param>
		/// <param name="importeComisionApagar"></param>
		/// <param name="lstrHost"></param>
		/// <returns>ComprobanteEntity</returns>
		private static ComprobanteEntity CargarComprobante(ComprobanteEntity objComprobante
														 , ComprobanteEntity objComprobanteACancelar
														 , DataRow item
														 , ParametrosEntity objParametros 
													     , int CompIdCancela
														 , int CoVtIdCancela
														 , int IdUsuario
													     , decimal CotizDolar
													     , decimal coac_impor		
														 , decimal importeARegistrar
														 , decimal importeComisionApagar
													     , decimal importe42AcuseConNotaDebito
														 , string lstrHost
														  )
		{
			// acuses
			objComprobante.ComprobantesAcuses.coac_id = _DataBase.IntDBNull();
			objComprobante.ComprobantesAcuses.coac_comp_id = CompIdCancela; // id del comprobante que se cancela
			objComprobante.ComprobantesAcuses.coac_canc_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobantesAcuses.coac_impor = (importe42AcuseConNotaDebito == -1)? coac_impor: importe42AcuseConNotaDebito; //coac_impor; //importePmC;
			objComprobante.ComprobantesAcuses.coac_covt_id = CoVtIdCancela; // covt_id de comprobante a cancelar
			objComprobante.ComprobantesAcuses.coac_audi_fecha = System.DateTime.Now ;		
			objComprobante.ComprobantesAcuses.coac_audi_user = IdUsuario;
			objComprobante.ComprobantesAcuses.coac_baja_fecha = _DataBase.DateTimeDBNull();
			// comprobante
			objComprobante.comp_id = _DataBase.IntDBNull();
			string[] fecha = item["FechaCobro"].ToString().Split('/');
			DateTime fechacomp = Convert.ToDateTime(fecha[1]+"/"+ fecha[0] + "/" + fecha[2]);
			objComprobante.comp_fecha = fechacomp; //Convert.ToDateTime(item["FechaCobro"]); // fecha de pago informada por Pmc
			objComprobante.comp_clie_id = Convert.ToInt32(item["NroReferencia"]); // id de cliente
			objComprobante.comp_dh = true;
			objComprobante.comp_neto = importeARegistrar;
			objComprobante.comp_neto_soci = _DataBase.DecimalDBnull();
			objComprobante.comp_neto_ctac = _DataBase.DecimalDBnull();
			objComprobante.comp_cance = true;
			objComprobante.comp_cs  = objComprobanteACancelar.comp_cs;
			objComprobante.comp_coti_id = objParametros.para_coti_id;
			objComprobante.comp_ingr_fecha = System.DateTime.Now; // hoy
			objComprobante.comp_mone_id = 1;
			objComprobante.comp_cemi_nume = 1; //administracion central
			objComprobante.comp_nume = _DataBase.IntDBNull();
			objComprobante.comp_esta_id = _DataBase.IntDBNull();
			objComprobante.comp_Baja_fecha = _DataBase.DateTimeDBNull();
			objComprobante.comp_audi_fecha = System.DateTime.Now; // ahora
			objComprobante.comp_audi_user = IdUsuario; // id de usuario
			objComprobante.comp_compati_1 = _DataBase.IntDBNull();
			objComprobante.comp_compati_id = _DataBase.IntDBNull();
			objComprobante.comp_letra = _DataBase.StringDBNull();
			objComprobante.comp_cpti_id = _DataBase.IntDBNull();
			objComprobante.comp_ivar_fecha = _DataBase.DateTimeDBNull();
			objComprobante.comp_acti_id = _DataBase.IntDBNull();
			objComprobante.comp_impre = false;
			objComprobante.comp_host  = lstrHost;
			objComprobante.comp_compati_a = _DataBase.StringDBNull();
			objComprobante.comp_emct_id = 11; // administracion
			objComprobante.comp_modu_id = 9; // facturacion
			objComprobante.comp_compati_b = _DataBase.StringDBNull();
			objComprobante.comp_compati_user = _DataBase.StringDBNull();
			objComprobante.comp_neto_conc = importeComisionApagar;
			objComprobante.comp_clie_ivap_id = objComprobanteACancelar.comp_clie_ivap_id;
			objComprobante.comp_clie_cuit = objComprobanteACancelar.comp_clie_cuit;
			// Dario 2013-06-13 Nueva definicion se comenta
			// concepto comprobante
			//				objComprobante.ComprobanteConcep.coco_id = _DataBase.IntDBNull();
			//				objComprobante.ComprobanteConcep.coco_comp_id = _DataBase.IntDBNull();
			//				objComprobante.ComprobanteConcep.coco_conc_id = objParametros.para_conc_id;
			//				objComprobante.ComprobanteConcep.coco_impo = importeComisionApagar;		
			//				objComprobante.ComprobanteConcep.coco_ccos_id = objComprobanteACancelar.acti_nega_ccos_id;
			//				objComprobante.ComprobanteConcep.coco_impo_ivai = importeComisionApagar;	
			//				objComprobante.ComprobanteConcep.coco_desc_ampl = "Pagos Mis Cuentas";
			//				objComprobante.ComprobanteConcep.coco_porc = _DataBase.DecimalDBnull();
			//				objComprobante.ComprobanteConcep.coco_tasa_iva = _DataBase.DecimalDBnull();
			//				objComprobante.ComprobanteConcep.coco_auto = true;
			//				objComprobante.ComprobanteConcep.coco_audi_fecha  = System.DateTime.Now;
			//				objComprobante.ComprobanteConcep.coco_audi_user = IdUsuario;
			//				objComprobante.ComprobanteConcep.coco_prco_id = _DataBase.IntDBNull();
			//				objComprobante.ComprobanteConcep.coco_impo_iva = _DataBase.DecimalDBnull();
			// fin 2013-06-13
			// detalle del comprobante
			objComprobante.ComprobanteDeta.code_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_conv_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_tarj_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_tarj_cuot = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_tarj_vtat = false;
			objComprobante.ComprobanteDeta.code_obse = "Pagos Mis Cuentas";
			objComprobante.ComprobanteDeta.code_coti = CotizDolar ; //code_coti; // obogatorio ultima cotizacion del dolar
			objComprobante.ComprobanteDeta.code_pers = false;
			objComprobante.ComprobanteDeta.code_raza_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_impo_ivat_tasa = 0;
			objComprobante.ComprobanteDeta.code_impo_ivat_tasa_redu = 0;
			objComprobante.ComprobanteDeta.code_impo_ivat_tasa_sobre = 0;
			objComprobante.ComprobanteDeta.code_audi_fecha  = System.DateTime.Now;
			objComprobante.ComprobanteDeta.code_impo_ivat_perc = 0;
			objComprobante.ComprobanteDeta.code_audi_user = IdUsuario;
			objComprobante.ComprobanteDeta.code_reci_nyap = _DataBase.StringDBNull();
			objComprobante.ComprobanteDeta.code_sistgen = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_opergen = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_insc_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_asoc_comp_id = _DataBase.IntDBNull();
            objComprobante.ComprobanteDeta.code_expo_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_tacl_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_tarj_nume = _DataBase.StringDBNull();
			objComprobante.ComprobanteDeta.code_cria_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_impo_grava = _DataBase.DecimalDBnull();
			objComprobante.ComprobanteDeta.code_gene_inte = false;
			objComprobante.ComprobanteDeta.code_peli_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_reci_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobanteDeta.code_impo_iibb_perc = 0;
			// pago del comprobante
			objComprobante.ComprobantePagos.paco_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_pati_id = 5; // tipo de pagos_tipos Acritacion bancaria
			objComprobante.ComprobantePagos.paco_mone_id = 1; // id de moneda 1 = pesos 2 Dolar
			objComprobante.ComprobantePagos.paco_nume = item["NroControl"].ToString(); // nro de comprobante de pmc
			objComprobante.ComprobantePagos.paco_tarj_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_tarj_cuot = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_tarj_nume = _DataBase.StringDBNull();
			objComprobante.ComprobantePagos.paco_cheq_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_audi_fecha  = System.DateTime.Now;
			objComprobante.ComprobantePagos.paco_impo = importeARegistrar;
			objComprobante.ComprobantePagos.paco_audi_user = IdUsuario;
			objComprobante.ComprobantePagos.paco_cuba_id = objParametros.para_cuba_id;
			objComprobante.ComprobantePagos.paco_orig_impo  = importeARegistrar;
			objComprobante.ComprobantePagos.paco_dine_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_tarj_autori = _DataBase.StringDBNull();
			objComprobante.ComprobantePagos.paco_acre_fecha = _DataBase.DateTimeDBNull();
			objComprobante.ComprobantePagos.paco_acre_audi_fecha = _DataBase.DateTimeDBNull();
			objComprobante.ComprobantePagos.paco_acre_audi_user = IdUsuario;
			//objComprobante.ComprobantePagos.paco_acre_apro = false; no pasarle nada
			objComprobante.ComprobantePagos.paco_tarj_rech_fecha = _DataBase.DateTimeDBNull();
			objComprobante.ComprobantePagos.paco_conc_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_ccos_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_tacl_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_rtip_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_orig_banc_id = objParametros.para_banc_id;
			objComprobante.ComprobantePagos.paco_orig_banc_suc = _DataBase.StringDBNull();
			objComprobante.ComprobantePagos.paco_baja_fecha = _DataBase.DateTimeDBNull();
			objComprobante.ComprobantePagos.paco_baja_moti  = _DataBase.StringDBNull();
			objComprobante.ComprobantePagos.paco_nd_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.paco_compati_a = _DataBase.StringDBNull();
			objComprobante.ComprobantePagos.paco_tarj_cupo = _DataBase.StringDBNull();
			string[] fechaacre = item["FechaAcreditacion"].ToString().Split('/');
			DateTime fechaacretemp = Convert.ToDateTime(fechaacre[1] + "/" + fechaacre[0] + "/" + fechaacre[2]);
			objComprobante.ComprobantePagos.paco_acre_depo_fecha = fechaacretemp; //Convert.ToDateTime(item["FechaAcreditacion"]);  // fecha de acreditacion fecha deposito
			objComprobante.ComprobantePagos.paco_acus_comp_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.cheq_banc_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.cheq_nume = _DataBase.StringDBNull();
			objComprobante.ComprobantePagos.cheq_teor_depo_fecha = _DataBase.DateTimeDBNull();
			objComprobante.ComprobantePagos.cheq_rece_fecha = _DataBase.DateTimeDBNull();
			objComprobante.ComprobantePagos.cheq_plaza = false;
			objComprobante.ComprobantePagos.cheq_clea_id = _DataBase.IntDBNull();
			objComprobante.ComprobantePagos.cheq_chti_id = _DataBase.IntDBNull();

			return objComprobante;
		}
		
		/// <summary>
		/// metodo que arma el body del mail de alerta para ACREDITACION_BANCARIA_INGRESO operacion ok
		/// </summary>
		/// <returns></returns>
		private static string GetBodyMail(string listIdComprobante, CobranzasCabEntity objCobranzasCab)
		{
			// declaro la variable de retorno
			string respuesta = string.Empty;
			// ejecuto la consulta a la base y recupero el detalle del mail
			DataSet ds = CobranzasDao.GetComprobantesAInformarAlertaMail(listIdComprobante);
			// verifico que tenga registros
			if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
			{	
				// si hay filas armo el encabezado
				respuesta = getEncabezadoMail(objCobranzasCab);
				// recorro los registros y armo el row del detalle
				foreach (DataRow dr in ds.Tables[0].Rows)
				{
					respuesta += "<tr>";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["comp_nume"].ToString().Trim() +"</b></font></td>" +"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='right'><font color='#7893da' face='arial' size='2'><b>" + dr["comp_neto"].ToString().Trim() +"</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["cliente"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["banc_desc"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["cuba_desc"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b>" + dr["paco_nume"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + dr["comp_fecha"].ToString().Trim() + "</b></font></td>"+"\r\n";
					respuesta += "</tr>"+"\r\n";
				}
				// pongo el pie del mail
				respuesta += "<tr>";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>Total Recaudado</b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='right'><font color='#7893da' face='arial' size='2'><b>" + objCobranzasCab.decRecaudacion +"</b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b>F. Acreditaci�n</b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>" + objCobranzasCab.dateFechaAcreditacion.ToString("dd/MM/yyyy") + "</b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b>Nro. Comprobante</b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='left' colspan='2'><font color='#7893da' face='arial' size='2'><b>" + objCobranzasCab.varNroComprobante + "</b></font></td>"+"\r\n";
				respuesta += "</tr>";
				respuesta += "<tr>";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b>Total Acreditado</b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='right'><font color='#7893da' face='arial' size='2'><b>" + objCobranzasCab.decRecaudacionNeta +"</b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b></b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='left'><font color='#7893da' face='arial' size='2'><b></b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='center'><font color='#7893da' face='arial' size='2'><b></b></font></td>"+"\r\n";
				respuesta += "<td bgcolor='whitesmoke' valign='top' align='left' colspan='2'><font color='#7893da' face='arial' size='2'><b></b></font></td>"+"\r\n";
				respuesta += "</table></td></tr></table><br></body></html>";
			}
			return respuesta;
		}

		
		/// <summary>
		/// metodo que retorna el encabezado para el mail de alerta
		/// </summary>
		/// <returns></returns>
		private static string getEncabezadoMail(CobranzasCabEntity objCobranzasCab)
		{
			// variable de respuesta
			string respuesta = string.Empty;
			// obtengo el link del servidor del sistema para pegarlo al mail
			string lnksist = ConfigurationSettings.AppSettings["conLnkSistema"];

			// armo encabezado
			respuesta += "<html><body><font color='#666666' face='arial' size='3'><b>NO RESPONDER ESTE MAIL</b></font>"
				+ "&nbsp;&nbsp;&nbsp;&nbsp; <font color='#666666' face='arial' size='2'>Haga click"
				+ "<a href='" + lnksist + "'>aqu�</a> para ingresar al sistema </font>" 
				+ "<br><br><font style='FONT-WEIGHT: normal;FONT-SIZE: 12pt;COLOR: #0054a3;FONT-FAMILY: Verdana' face='arial' size='2'>"
				+ "<b>Aviso de ingreso de acreditaciones bancarias. " + objCobranzasCab.varDesEmpresaCobElect + "</b></font><br><br>"
				+ "<table border='1' width='100%' cellpadding='2' cellspacing='0' bordercolor='#003784' ID='Table1'>"
				+ "<tr><td width='100%'><table border='0' width='100%' bordercolor='#003784' ID='Table2'>"
				+ "<tr><td bgcolor='#7893da' valign='top' align='left'><font color='#ffffff' face='arial' size='2'><b>Nro.Comp. </b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Importe </b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Cliente </b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Entidad </b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Cuenta </b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Nro.Oper. </b></font></td>"
				+ "<td bgcolor='#7893da' valign='top' align='center'><font color='#ffffff' face='arial' size='2'><b>Fecha </b></font></td>"
				+ "</tr>";

			return respuesta;
		}

		/// <summary>
		/// hace el pedido de cae al ws de afip
		/// </summary>
		/// <param name="comp_id"></param>
		/// <returns>bool</returns>
		private static bool GetCAE(SqlTransaction transPpal, Int32 comp_id)
		{
			bool respuesta  = false;
			try
			{
				string respuestaCAE = string.Empty;
                // Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                respuestaCAE = WSFactElet.GetCAE(String.Empty, comp_id.ToString());
				//                 0                1                2              3              4            5
                // respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                //                  6                      7                8 
                //            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
				string[] respArray = respuestaCAE.Split('|');
                // si retorna ok 
                if(respArray[0] == "A")
				{
                  // controlo que el comp id enviado es igual al recibido
				  string compIdresp  = respArray[2];
 	
				  string comp_cemi_nume= string.Empty;
				  string opcional1 = string.Empty;
				  string opcional2 = string.Empty;
   				  
					try { comp_cemi_nume = respArray[6]; } catch(Exception ex ) { }
					try { opcional1 = respArray[7]; } catch(Exception ex ) { }
					try { opcional2 = respArray[8]; } catch (Exception ex) { }

//					if(comp_cemi_nume.Trim().Length == 0){comp_cemi_nume = "null"; }
//					if(opcional1.Trim().Length == 0){opcional1 = "null"; }
//					if(opcional2.Trim().Length == 0){opcional2 = "null"; }

					if(compIdresp.Trim() == comp_id.ToString())
					{
						// obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
						string compNumeresp = respArray[3];
						//string proc = "comprobantes_asigna_comp_nume @comp_id=" + comp_id.ToString() + " ,@comp_nume=" + compNumeresp;
						string proc = "comprobantes_asigna_comp_nume @comp_id=" + comp_id.ToString() + " ,@comp_nume=" + compNumeresp 
							+ " ,@comp_cemi_nume=" + comp_cemi_nume + " ,@opcional1= " + opcional1 + " ,@opcional2= " + opcional2;
						// hago el update del @comp_nume de la tabla de comprobantes
						Int32 intUpdateRest = ComprobanteDao.AsignaCompNumeComprobantes(transPpal, comp_id.ToString(), compNumeresp
																						, comp_cemi_nume,opcional1,opcional2 );
						
						_DataBase.gLoggear("GetCAE 16:: " + proc);
						// si no realizo el update error
						if(intUpdateRest == 0)
						{
							throw new Exception("Error: no Update:: " + proc + " * " + respuestaCAE);
						}
					}
					else
					{
						// si los com id son diferentes error
						throw new Exception("Error compIdresp != lstrId *" + compIdresp + "!=" + comp_id.ToString() + " * " + respuestaCAE);					
					}
				}
				else
				{
					// cualquier cosa fuera de ok error 
					throw new Exception(respuestaCAE);
				}
			}
			catch(Exception ex)
			{
				// llamo al metodo del base encargado de manejar los errores
				_DataBase.gManejarError(ex, ex.Message.ToString());
			}

			return respuesta;
		}
	}
}
