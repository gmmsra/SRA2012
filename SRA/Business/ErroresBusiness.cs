﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Data;
using DAO;
using AccesoBD;
using Common;

namespace Business
{
    public class ErroresBusiness
    {
        private static string nameSpace = "Business.ErroresBusiness.";

        public static List<ErroresEntity.Rg_Denuncias_Errores_Busq> Rg_Denuncias_Errores_Busq(int? Id, int? Proce, string Titulo)
        {
            List<ErroresEntity.Rg_Denuncias_Errores_Busq> obj = new List<ErroresEntity.Rg_Denuncias_Errores_Busq>();
            try
            {
                DataSet ds = new DataSet();
                ds = ErroresDao.Rg_Denuncias_Errores_Busq(Id, Proce,Titulo);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        obj.Add(new ErroresEntity.Rg_Denuncias_Errores_Busq
                        {
                            Id = Convert.ToInt32(dr["id"]),
                            Fecha = dr["Fecha"].ToString(),
                            Codigo = dr["Codigo"].ToString(),
                            Mensaje = dr["Mensaje"].ToString(),
                            Err = Convert.ToInt32(dr["err"]),
                            Manual = dr["manual"].ToString(),
                            Audi_User = dr["audi_user"] == DBNull.Value ? string.Empty : dr["audi_user"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Rg_Denuncias_Errores_Busq, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return obj;
        }

        public static string GetTablaErroresSegunProceso(int IdProceso)
        {
            string tabla = string.Empty;
            switch (IdProceso)
            {
                case 1:
                    tabla = TablaErroresSegunProceso.RG_PRODUCTOS_INSCRIP;
                    break;
                case 2:
                    tabla = TablaErroresSegunProceso.RG_SERVI_DENUNCIAS;
                    break;
                case 5:
                case 10:
                    tabla = TablaErroresSegunProceso.TRAMITES_PRODUCTOS;
                    break;
                case 9:
                    tabla = TablaErroresSegunProceso.IMPORTACION_PRODUCTOS;
                    break;
                case 6:
                    tabla = TablaErroresSegunProceso.TE_DENUN_DETA;
                    break;
                case 12:
                case 13:
                case 14:
                    tabla = TablaErroresSegunProceso.TRAMITES;
                    break;
                case 11:
                case 15:
                case 16:
                    tabla = TablaErroresSegunProceso.SEMEN_STOCK;
                    break;
                case 18:
                    tabla = TablaErroresSegunProceso.PRESTAMOS;
                    break;
            }

            return tabla;
        }

        public static void Cs_Validaciones_Errores_Grabar(int IdProceso, int IdTabla, int? TramiteId, int Rmen_id, int Audi_user, int? SalvadoActivado, int? Manual, string Obser, int BitErroresSalvar)
        {
            try
            {
                string tabla = GetTablaErroresSegunProceso(IdProceso);
                ErroresDao.Cs_Validaciones_Errores_Grabar(tabla, IdTabla, TramiteId, Rmen_id, Audi_user, SalvadoActivado, Manual, Obser, BitErroresSalvar);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Cs_Validaciones_Errores_Grabar, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static void Cs_Validaciones_Errores_Salvar(int IdProceso, int Id, int Err_Id, int Baja_Manual_User, int Salvar, int? BitErroresSalvar)
        {
            try
            {
                string tabla = GetTablaErroresSegunProceso(IdProceso);
                ErroresDao.Cs_Validaciones_Errores_Salvar(tabla, Id, Err_Id, Baja_Manual_User, Salvar, BitErroresSalvar);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "Cs_Validaciones_Errores_Salvar, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}
