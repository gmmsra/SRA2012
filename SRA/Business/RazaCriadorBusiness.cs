﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Data;
using DAO;
using AccesoBD;

namespace Business
{
    public class RazaCriadorBusiness
    {
        private static string nameSpace = "Business.RazaCriadorBusiness.";

        public static RazaCriadorEntity GetDatosRazaCriador(int raza_id, int cria_nume)
        {
            RazaCriadorEntity obj = new RazaCriadorEntity();
            try 
            {
                DataSet ds = new DataSet();
                ds = RazaCriadorDao.GetDatosRazaCriador(raza_id, cria_nume);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    obj.raza_id = Convert.ToInt32(dr["raza_id"]);
                    obj.cria_id = Convert.ToInt32(dr["cria_id"]);
                    obj.clie_id = Convert.ToInt32(dr["clie_id"]);
                    obj.raza_codi = Convert.ToInt32(dr["raza_codi"]);
                    obj.raza_desc = dr["raza_desc"].ToString();
                    obj.raza_codi_desc = dr["raza_codi_desc"].ToString();
                    obj.cria_nume = Convert.ToInt32(dr["cria_nume"]);
                    obj.clie_fanta = dr["clie_fanta"].ToString();
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameSpace + "GetDatosRazaCriador, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return obj;
        }
    }
}
