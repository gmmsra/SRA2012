﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Data;
using DAO;
using AccesoBD;
using Common;

namespace Business
{
    public class TramitesReservasBusiness
    {
        public static TramitesReservasEntity GetTramiteReservas(int tram_id)
        {
            TramitesReservasEntity obj = new TramitesReservasEntity();
            try
            {
                DataSet ds = new DataSet();
                ds = TramitesReservasDao.GetTramiteReservas(tram_id);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    obj.IdTramiteReserva = Convert.ToInt32(dr["IdTramiteReserva"]);
                    obj.Tram_id = Convert.ToInt32(dr["Tram_id"]);
                    obj.ReservaSemen = Convert.ToInt32(dr["ReservaSemen"]);
                    obj.CantReservaSemen = Convert.ToInt32(dr["CantReservaSemen"]);
                    obj.ReservaCria = Convert.ToInt32(dr["ReservaCria"]);
                    obj.CantReservaCria = Convert.ToInt32(dr["CantReservaCria"]);
                    obj.ReservaEmbrion = Convert.ToInt32(dr["ReservaEmbrion"]);
                    obj.CantReservaEmbrion = Convert.ToInt32(dr["CantReservaEmbrion"]);
                    obj.ReservaVientre = Convert.ToInt32(dr["ReservaVientre"]);
                    obj.ReservaMaterialGenetico = Convert.ToInt32(dr["ReservaMaterialGenetico"]);
                    obj.ReservaDerechoClonar = Convert.ToInt32(dr["ReservaDerechoClonar"]);
                    obj.AutorizaCompradorClonar = Convert.ToInt32(dr["AutorizaCompradorClonar"]);
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return obj;
        }

        public static void InsertTramiteReservas(TramitesReservasEntity TramiteReservas)
        {
            try
            {
                TramitesReservasDao.InsertTramitesReservas(TramiteReservas);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
        }

        public static void UpdateTramiteReservas(TramitesReservasEntity TramiteReservas)
        {
            try
            {
                TramitesReservasDao.UpdateTramitesReservas(TramiteReservas);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
        }

    }
}
