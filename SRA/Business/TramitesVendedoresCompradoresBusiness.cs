﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Data;
using DAO;
using AccesoBD;
using Common;
using System.Xml;
using System.Xml.Serialization;

namespace Business
{
    public class TramitesVendedoresCompradoresBusiness
    {
        public static List<TramitesVendedoresCompradoresEntity> ConsTramitesCompradoresVendedores(int tram_id)
        {
            List<TramitesVendedoresCompradoresEntity> obj = new List<TramitesVendedoresCompradoresEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TramitesVendedoresCompradoresDao.ConsTramitesCompradoresVendedores(tram_id);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        obj.Add(new TramitesVendedoresCompradoresEntity {
                            tram_vc_id = Convert.ToInt32(dr["tram_vc_id"]),
                            tram_vc_tram_id = tram_id,
                            tram_vc_tram_audi_fecha = Convert.ToDateTime(dr["tram_vc_tram_audi_fecha"]),
                            tram_vc_tram_audi_user = Convert.ToInt32(dr["tram_vc_tram_audi_user"]),
                            tram_vc_tram_baja_fecha = dr["tram_vc_tram_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_vc_tram_baja_fecha"]),
                            tram_vc_clie_id = Convert.ToInt32(dr["tram_vc_clie_id"]),
                            tram_vc_cria_id = dr["tram_vc_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_vc_cria_id"]),
                            tram_vc_porcentaje = Convert.ToInt32(dr["tram_vc_porcentaje"]),
                            tram_vc_tran_tipo = dr["tram_vc_tran_tipo"].ToString(),
                            tram_vc_obser = dr["tram_vc_obser"].ToString(),
                            raza_codi_desc = dr["raza_codi_desc"].ToString(),
                            cria_nume = Convert.ToInt32(dr["cria_nume"]),
                            clie_fanta = dr["clie_fanta"].ToString(),
                            estado = string.Empty 
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return obj;
        }

        public static TramitesVendedoresCompradoresEntity GetTramitesCompradoresVendedores(int tram_vc_id)
        {
            TramitesVendedoresCompradoresEntity obj = new TramitesVendedoresCompradoresEntity();
            try
            {
                DataSet ds = new DataSet();
                ds = TramitesVendedoresCompradoresDao.GetTramitesCompradoresVendedores(tram_vc_id);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    obj.tram_vc_id = tram_vc_id;
                    obj.tram_vc_tram_id = Convert.ToInt32(dr["tram_vc_tram_id"]);
                    obj.tram_vc_tram_audi_fecha = Convert.ToDateTime(dr["tram_vc_tram_audi_fecha"]);
                    obj.tram_vc_tram_audi_user = Convert.ToInt32(dr["tram_vc_tram_audi_user"]);
                    obj.tram_vc_tram_baja_fecha = dr["tram_vc_tram_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_vc_tram_baja_fecha"]);
                    obj.tram_vc_clie_id = Convert.ToInt32(dr["tram_vc_clie_id"]);
                    obj.tram_vc_cria_id = dr["tram_vc_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_vc_cria_id"]);
                    obj.tram_vc_porcentaje = Convert.ToInt32(dr["tram_vc_porcentaje"]);
                    obj.tram_vc_tran_tipo = dr["tram_vc_tran_tipo"].ToString();
                    obj.tram_vc_obser = dr["tram_vc_obser"].ToString();
                    obj.raza_codi_desc = dr["raza_codi_desc"].ToString();
                    obj.cria_nume = Convert.ToInt32(dr["cria_nume"]);
                    obj.clie_fanta = dr["clie_fanta"].ToString();
                    obj.estado = string.Empty;
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return obj;
        }

        public static void Tramites_Vendedores_Compradores_Actualizar(int Tram_id, List<TramitesVendedoresCompradoresEntity> Vendedores, List<TramitesVendedoresCompradoresEntity> Compradores)
        {
            try 
            {
                foreach (var item in Vendedores)
                {
                    item.tram_vc_tram_id = Tram_id;
                    if (getExisteTramiteCompradorVendedor(Tram_id, item.tram_vc_clie_id, item.tram_vc_cria_id, item.tram_vc_tran_tipo))
                    {
                        UpdateTramiteCompradorVendedor(item);
                    }
                    else
                    {
                        InsertTramiteCompradorVendedor(item);
                    }
                }

                foreach (var item in Compradores)
                {
                    item.tram_vc_tram_id = Tram_id;
                    if (getExisteTramiteCompradorVendedor(Tram_id, item.tram_vc_clie_id, item.tram_vc_cria_id, item.tram_vc_tran_tipo))
                    {
                        UpdateTramiteCompradorVendedor(item);
                    }
                    else
                    {
                        InsertTramiteCompradorVendedor(item);
                    }
                }
                

            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
        }

        private static bool getExisteTramiteCompradorVendedor(int tram_id, int tram_vc_clie_id, int? tram_vc_cria_id, string tram_vc_tran_tipo)
        {
            return TramitesVendedoresCompradoresDao.GetExisteTramiteCompradorVendedor(tram_id, tram_vc_clie_id, tram_vc_cria_id, tram_vc_tran_tipo);
        }

        public static void UpdateTramiteCompradorVendedor(TramitesVendedoresCompradoresEntity TramiteVendedorComprador)
        {
            TramitesVendedoresCompradoresDao.UpdateTramiteCompradorVendedor(TramiteVendedorComprador);
        }

        public static void InsertTramiteCompradorVendedor(TramitesVendedoresCompradoresEntity TramiteVendedorComprador)
        {
            TramitesVendedoresCompradoresDao.InsertTramiteCompradorVendedor(TramiteVendedorComprador);
        }

        public static void Tramites_Vendedores_Compradores_Eliminar(int Tram_id, List<TramitesVendedoresCompradoresEntity> Vendedores, List<TramitesVendedoresCompradoresEntity> Compradores)
        {
            // Obtiene los datos desde la DB según el trámite.
            var objTramitesVC = new List<TramitesVendedoresCompradoresEntity>();
            objTramitesVC = ConsTramitesCompradoresVendedores(Tram_id);

            // Filtra los datos para obtener los vendedores.
            var objVendedoresDB = objTramitesVC.Where(x => x.tram_vc_tran_tipo == "V").ToList();

            foreach (var item in objVendedoresDB)
            {
                // Si el registro de la DB no existe entre los vendendores recibidos como parámetro, lo elimina.
                if (Vendedores.Where(v => v.tram_vc_cria_id == item.tram_vc_cria_id && v.tram_vc_clie_id == item.tram_vc_clie_id).Count() == 0)
                {
                    DeleteTramiteCompradorVendedor(item);
                }
            }

            // Filtra los datos para obtener los compradores.
            var objCompradoresDB = objTramitesVC.Where(x => x.tram_vc_tran_tipo == "C").ToList();

            foreach (var item in objCompradoresDB)
            {
                // Si el registro de la DB no existe entre los compradores recibidos como parámetro, lo elimina.
                if (Compradores.Where(v => v.tram_vc_cria_id == item.tram_vc_cria_id && v.tram_vc_clie_id == item.tram_vc_clie_id).Count() == 0)
                {
                    DeleteTramiteCompradorVendedor(item);
                }
            }            
        }

        public static void DeleteTramiteCompradorVendedor(TramitesVendedoresCompradoresEntity TramiteVendedorComprador)
        {
            TramitesVendedoresCompradoresDao.DeleteTramiteCompradorVendedor(TramiteVendedorComprador);
        }

        public static DataSet ConsTramitesCompradoresVendedoresDS(int tram_id)
        {
            DataSet ds = new DataSet();
            ds = TramitesVendedoresCompradoresDao.ConsTramitesCompradoresVendedores(tram_id);
            return ds;
        }

        public static List<TramitesVendedoresCompradoresEntity> ConsTransferenciaProductoPropietariosQueVenden(int prdt_id, DateTime? operFecha, int solovigente)
        {
            List<TramitesVendedoresCompradoresEntity> obj = new List<TramitesVendedoresCompradoresEntity>();
            try
            {
                DataSet ds = new DataSet();
                ds = TramitesVendedoresCompradoresDao.ConsTransferenciaProductoPropietariosQueVenden(prdt_id, operFecha, solovigente);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        obj.Add(new TramitesVendedoresCompradoresEntity
                        {
                            tram_vc_tram_id = Convert.ToInt32(dr["tram_vc_tram_id"]),
                            tram_vc_tram_audi_fecha = Convert.ToDateTime(dr["tram_vc_tram_audi_fecha"]),
                            tram_vc_tram_audi_user = Convert.ToInt32(dr["tram_vc_tram_audi_user"]),
                            tram_vc_tram_baja_fecha = dr["tram_vc_tram_baja_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["tram_vc_tram_baja_fecha"]),
                            tram_vc_clie_id = Convert.ToInt32(dr["tram_vc_clie_id"]),
                            tram_vc_cria_id = dr["tram_vc_cria_id"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["tram_vc_cria_id"]),
                            tram_vc_porcentaje = Convert.ToInt32(dr["tram_vc_porcentaje"]),
                            tram_vc_tran_tipo = dr["tram_vc_tran_tipo"].ToString(),
                            tram_vc_obser = dr["tram_vc_obser"].ToString(),
                            raza_codi_desc = dr["raza_codi_desc"].ToString(),
                            cria_nume = Convert.ToInt32(dr["cria_nume"]),
                            clie_fanta = dr["clie_fanta"].ToString(),
                            estado = string.Empty
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(Errores.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return obj;

        }

    }
}
