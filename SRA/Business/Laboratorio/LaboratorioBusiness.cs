using System;
using System.Data;
using DAO.Laboratorio;
using DAO;
using Entities.Facturacion;
using Entities;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using Common;


namespace Business.Laboratorio
{
	/// <summary>
	/// Summary description for LaboratorioBusiness.
	/// </summary>
	public class LaboratorioBusiness
	{
		// objeto que se utiliza para generar transacciones
		static DataBase _DataBase = new DataBase();
		
		/// <summary>
		/// metodo que retorna un dataset con los datos de analisis por cliente especie
		/// para el pop up de facturacion
		/// </summary>
		/// <param name="IdCliente"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetAllAnalisisAcumPorActividadCliente(int IdActividad , int IdCliente, DateTime Fecha)
		{
			return LaboratorioDao.GetAllAnalisisAcumPorActividadCliente(IdActividad, IdCliente, Fecha);
		}
	}
}
