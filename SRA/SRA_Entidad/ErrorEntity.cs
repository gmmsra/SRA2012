using System;


namespace SRA_Entidad 
{
    
   public class ErrorEntity
 {

        /// <summary>
        /// Summary description for ErrorDL
        /// </summary>
		#region Private Variables
		private Int32 _errorId;
		private DateTime _errorFecha;
		private string _errorDescripcion;

		#endregion

		#region Public Properties
		public Int32 errorId
		{
			get
			{
				return _errorId;
			}
			set
			{
				_errorId = value;
			}
		}

	
		
		public DateTime errorFecha
		{
			get
			{
				return _errorFecha;
			}
			set
			{
				_errorFecha = value;
			}
		}
	
	   public string errorDescripcion
	   {
		   get
		   {
			   return _errorDescripcion;
		   }
		   set
		   {
			   _errorDescripcion = value;
		   }
	   }
        #endregion

	}
}
