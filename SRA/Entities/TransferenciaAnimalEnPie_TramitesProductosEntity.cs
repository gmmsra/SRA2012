﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_TramitesProductosEntity
    {
        #region Campos de la tabla
        public int trpr_id { get; set; }
        public int? trpr_prdt_id { get; set; }
        public int trpr_tram_id { get; set; }
        public int? trpr_apro { get; set; }
        public DateTime? trpr_resu_fecha { get; set; }
        public DateTime trpr_audi_fecha { get; set; }
        public int trpr_audi_user { get; set; }
        public byte[] trpr_tmsp { get; set; }
        public string trpr_rp { get; set; }
        public int? trpr_sexo { get; set; }
        public int? trpr_sra_nume { get; set; }
        public DateTime? trpr_baja_fecha { get; set; }
        public DateTime? trpr_apro_fecha { get; set; }
        public int? trpr_asoc_id { get; set; }
        public int? trpr_padre_asoc_id { get; set; }
        public int? trpr_madre_asoc_id { get; set; }
        public int? trpr_cria_cant { get; set; }
        public int? trpr_dosi_cant { get; set; }
        public int? trpr_embr_cant { get; set; }
        public DateTime? trpr_embr_recu_fecha { get; set; }
        public int? trpr_embr_madre_id { get; set; }
        public string trpr_embr_madre_rp { get; set; }
        public int? trpr_embr_madre_sra_nume { get; set; }
        public int? trpr_prdt_esta_id_anterior { get; set; }
        public bool trpr_vta_crias { get; set; }
        public bool trpr_vta_emb_semen { get; set; }
        public bool trpr_vta_mat_gen { get; set; }
        public bool trpr_pre_ces { get; set; }
        public DateTime? trpr_a_partir_fecha { get; set; }
        public DateTime? trpr_fin_fecha { get; set; }
        #endregion

        #region Campos extras usados para otros obejetivos por los CU

        #endregion
    }
}
