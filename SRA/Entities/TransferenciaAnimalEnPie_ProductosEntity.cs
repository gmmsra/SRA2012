﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_ProductosEntity
    {
        public int prdt_id { get; set; }
        public string prdt_rp { get; set; }
        public string prdt_nomb { get; set; }
        public string _pref_desc { get; set; }
        public DateTime? prdt_naci_fecha { get; set; }
        public DateTime? prdt_fall_fecha { get; set; }
        public int? prdt_cria_id { get; set; }
        public int? prdt_prop_cria_id { get; set; }
        public int? prdt_raza_id { get; set; }
        public int? prdt_sra_nume { get; set; }
        
        public int? prdt_sexo { get; set; }
        public int? prdt_b_pela_id { get; set; }
        public int? prdt_c_pela_id { get; set; }
        public int? prdt_d_pela_id { get; set; }
        public DateTime? prdt_baja_fecha { get; set; }
        public DateTime? prdt_tran_fecha { get; set; }
        public int? prdt_regt_id { get; set; }
        public string prdt_px { get; set; }
        public int? prdt_a_pela_id { get; set; }
        public string prdt_apodo { get; set; }
        
        public int? prdt_siete { get; set; }
        public int? prdt_melli { get; set; }
        public int? prdt_te { get; set; }
        public int? prdt_dona_nume { get; set; }
        public DateTime? prdt_dona_fecha { get; set; }
        public int? prdt_peso_nacer { get; set; }
        public int? prdt_peso_deste { get; set; }
        public int? prdt_peso_final { get; set; }
        public DateTime? prdt_insc_fecha { get; set; }
        public int? prdt_esta_id { get; set; }
        
        public decimal? prdt_epd_nacer { get; set; }
        public decimal? prdt_epd_deste { get; set; }
        public decimal? prdt_epd_final { get; set; }
        public int? prdt_rp_nume { get; set; }
        public string prdt_rp_extr { get; set; }
        public int? prdt_audi_user { get; set; }
        public string _esta_desc { get; set; }
        public string _raza_desc { get; set; }
        public string _sexo { get; set; }
        public string _asoc { get; set; }

        public string _nume { get; set; }
        public string _asoc_id { get; set; }
        public string _desc { get; set; }
        public int? _Esdonante { get; set; }
        public int? _espe { get; set; }
        public string prdt_ndad { get; set; }
        public int? _cria_nume { get; set; }
        public string _criador { get; set; }
        public int? prdt_bamo_id { get; set; }
        public string prdt_cuig { get; set; }
        
        public string _embargado { get; set; }
        public int? prdt_tram_nume { get; set; }
        public DateTime? _pdin_fecha { get; set; }
        public int? _pdin_nro_control { get; set; }
        public int? _pdin_tede_nume { get; set; }
        public DateTime? _fecha_tramite { get; set; }
        public int? _tram_nume { get; set; }
        public int? _nro_control_tramite { get; set; }
        public string _calif { get; set; }
        public string _califFenotipica { get; set; }
        
        public int? prdt_vari_id { get; set; }
        public string _receptora { get; set; }
        public DateTime? _servi_fecha_producto { get; set; }
        public DateTime? _servi_fecha { get; set; }
        public string _servi_tipo_TE { get; set; }
        public string _servi_tipo { get; set; }
        public DateTime? _impla_fecha { get; set; }
        public string _aprobado { get; set; }
        public int? _resulSNPSExtranj { get; set; }
        public int? prdt_prop_clie_id { get; set; }
        
        public int? generar_numero { get; set; }
        public int? _asoc_codi { get; set; }
        public int? prdt_ori_asoc_id { get; set; }
        public string prdt_ori_asoc_nume { get; set; }
        public int? prdt_ori_asoc_nume_lab { get; set; }
        public int? prdt_pdin_id { get; set; }
        public int? prdt_condicional { get; set; }
        public int? _dona_nro_control { get; set; }
        public int? reproceso { get; set; }
        public int? _reprocesar { get; set; }
        
        public string prdt_dona_sena_nume { get; set; }
        public int? _prin_bitTrim { get; set; }
        public DateTime? prdt_Fecha_InspecMayor25 { get; set; }
        public int? prdt_Resul_InspecMayor25 { get; set; }
        public int? prdt_anio_deps_Nac { get; set; }
        public int? prdt_anio_deps_Dest { get; set; }
        public int? prdt_anio_deps_PosDest { get; set; }
        public DateTime? prdt_FechaDestete { get; set; }
        public DateTime? prdt_FechaPesoPostDestete { get; set; }
        public DateTime? prdt_FechaCirEscrotal { get; set; }

        public decimal? prdt_CirEscrotal { get; set; }
        public int? prdt_ManejoDest { get; set; }
        public int? prdt_ManejoPosDest { get; set; }
        public string prdt_ObsDestete { get; set; }
        public string prdt_ObsPostDestete { get; set; }
        public DateTime? Prdt_Deps_Fecha { get; set; }
        public decimal? Prdt_Epd_CirEscrotal { get; set; }
        public decimal? Prdt_Epd_Lech { get; set; }
        public decimal? Prdt_Epd_LYC { get; set; }
        public decimal? Prdt_Exa_Nacer { get; set; }

        public decimal? Prdt_Exa_Destete { get; set; }
        public decimal? Prdt_Exa_Final { get; set; }
        public decimal? Prdt_Exa_CirEscrotal { get; set; }
        public decimal? Prdt_Exa_Lech { get; set; }
        public string _HabilitaNacionalizacionProducto { get; set; }
        public int? prdt_castrado { get; set; }
        public int? prdt_Descorne { get; set; }
        public DateTime? prdt_Fecha_castrado { get; set; }
        public string prdt_Cond_Geneticas { get; set; }
        public string prdt_factura { get; set; }







        /*
        
        public string prdt_pure_ana_codi { get; set; }
        public int? prdt_compati_1 { get; set; }
        public int? prdt_compati_asop { get; set; }
        public int? prdt_compati_hbap { get; set; }
        public int? prdt_compati_asom { get; set; }
        public int? prdt_compati_hbam { get; set; }
        public int? prdt_Impreso { get; set; }
        public DateTime? prdt_Fecha_Impreso { get; set; }
        public int? prdt_User_Impreso { get; set; }
        public int? prdt_carga_asoc { get; set; }
        public int? prdt_tran_nume { get; set; }
        public decimal? prdt_PesosPostDestete { get; set; }
        public int? prdt_seti_id { get; set; }
        public DateTime? prdt_servi_fecha { get; set; }
        public int? Prdt_Mga { get; set; }
        public int? Prdt_MgaP { get; set; }
        public int? Prdt_MgaM { get; set; }
        public int? prdt_compati_RazP { get; set; }
        public int? prdt_compati_RazM { get; set; }
        public string prdt_compati_RpPd { get; set; }
        public string prdt_compati_RpMd { get; set; }
        public string prdt_compati_RgsP { get; set; }
        public string prdt_compati_RgsM { get; set; }
        public string prdt_compati_VarP { get; set; }
        public string prdt_compati_VarM { get; set; }
        public long? prdt_idnv { get; set; }
        public long? prdt_nro_chip { get; set; }
        public string prdt_Vnro_Chip { get; set; }
        public string prdt_Irregularidades { get; set; }
        */

    }
}
