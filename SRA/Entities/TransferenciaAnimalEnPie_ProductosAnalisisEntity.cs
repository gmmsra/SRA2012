﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_ProductosAnalisisEntity
    {
        #region Campos de la tabla
        public int prta_id { get; set; }
        public int? prta_prdt_id { get; set; }
        public int? prta_nume { get; set; }
        public int? prta_ares_id { get; set; }
        public DateTime? prta_fecha { get; set; }
        public DateTime? prta_resul_fecha { get; set; }
        public int? prta_audi_user { get; set; }
        public DateTime? prta_baja_fecha { get; set; }
        public DateTime? prta_audi_fecha { get; set; }
        public string prta_ndad { get; set; }
        public string prta_tipo_analisis { get; set; }
        public int? Prta_Esan_Codi { get; set; }

        /*
       
        public byte[] prta_tmsp { get; set; }
        public string prta_pureza { get; set; }
        public int? prta_tiene_ts { get; set; }
        public int? prta_ts_ares_id { get; set; }
        public int? prta_tiene_adn { get; set; }
        public int? prta_adn_ares_id { get; set; }
        public int? prta_tiene_snps { get; set; }
        public int? prta_snps_ares_id { get; set; }
        public int? prta_tiene_pelo { get; set; }
        public int? prta_anca_id { get; set; }
        public int? prta_ande_id { get; set; }
        public int? prta_nroref_padre { get; set; }
        public int? prta_prdt_id_padre { get; set; }
        public int? prta_result_padre { get; set; }
        public int? prta_nroref_madre { get; set; }
        public int? prta_prdt_id_madre { get; set; }
        public int? prta_result_madre { get; set; }
        public string prta_varPadre { get; set; }
        public string prta_varMadre { get; set; }
        public int? prta_macrSNP { get; set; }
        */

        #endregion

        #region Campos extras usados para otros obejetivos por los CU
        public int? _resul_codi { get; set; }
        public string _resul { get; set; }
        public string _pureza { get; set; }
        public int? _tiene_ts { get; set; }
        public int? _results_codi { get; set; }
        public string _results { get; set; }
        public int? _tiene_adn { get; set; }
        public int? _resuladn_codi { get; set; }
        public string _resuladn { get; set; }
        public int? _tiene_snps { get; set; }
        public int? _resulsnps_codi { get; set; }
        public string _resulsnps { get; set; }
        public int? _tiene_pelo { get; set; }
        public string _anes_desc { get; set; }
        public string _EstadoAnalisis { get; set; }
        public string _Estudio { get; set; }
        #endregion
    }
}
