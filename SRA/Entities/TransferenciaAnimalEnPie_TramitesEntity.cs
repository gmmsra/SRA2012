﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_TramitesEntity
    {
        public int tram_id { get; set; }
        public DateTime? tram_inic_fecha { get; set; }
        public DateTime? tram_fina_fecha { get; set; }
        public DateTime? tram_pres_fecha { get; set; }
        public int tram_esta_id { get; set; }
        public int? tram_raza_id { get; set; }
        public int tram_ttra_id { get; set; }
        public int? tram_nume { get; set; }
        public int? tram_cria_cant { get; set; }
        public int? tram_dosi_cant { get; set; }
        public int? tram_impr_id { get; set; }
        public DateTime tram_audi_fecha { get; set; }
        public int tram_audi_user { get; set; }
        public DateTime? tram_baja_fecha { get; set; }
        public byte[] tram_tmsp { get; set; }
        public int? tram_fina_user { get; set; }
        public int? tram_pais_id { get; set; }
        public int? tram_comp_clie_id { get; set; }
        public int? tram_comp_cria_id { get; set; }
        public int? tram_prop_clie_id { get; set; }
        public int? tram_prop_cria_id { get; set; }
        public int? tram_embr_madre_id { get; set; }
        public int? tram_embr_padre_id { get; set; }
        public int? tram_embr_padre2_id { get; set; }


        public int? tram_embr_cant { get; set; }
        public int? tram_embr_tede_comp_id { get; set; }
        public int? tram_embr_tede_vend_id { get; set; }
        public DateTime? tram_embr_recu_fecha { get; set; }
        public string _te_denun_desc { get; set; }
        public int? tram_nro_control { get; set; }
        public DateTime? tram_oper_fecha { get; set; }
        public DateTime? tram_liber_fecha { get; set; }
        public DateTime? tram_apob_asoc_fecha { get; set; }
        public int? tram_ressnpsextr { get; set; }
        public int? tram_vend_clie_id { get; set; }
        public int? tram_vend_cria_id { get; set; }

        public string esta_abre { get; set; } // Abreviatura del estado del trámite.

        /*
        public int? tram_vend_prop_id { get; set; }
        public DateTime? tram_embr_apro_fecha { get; set; }
        public int? tram_compati_1 { get; set; }
        public int? tram_visto { get; set; }
        public string tram_tran_tipo { get; set; }
        public int? tram_embr_padre3_id { get; set; }
        public int? tram_bjdd_id { get; set; }
        public string tram_obser { get; set; }
        */
    }
}
