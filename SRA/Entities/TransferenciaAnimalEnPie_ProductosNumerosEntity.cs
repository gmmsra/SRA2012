﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_ProductosNumerosEntity
    {
        public int ptnu_id { get; set; }
        public int? ptnu_asoc_id { get; set; }
        public string ptnu_nume { get; set; }
        public int? _asoc_codi { get; set; }
        public string _asoc_deno { get; set; }
        public string prdt_rp_extr { get; set; }
    }
}
