﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TramitesVendedoresCompradoresEntity
    {
        public int tram_vc_id { get; set; }
        public int tram_vc_tram_id { get; set; }
        public DateTime tram_vc_tram_audi_fecha { get; set; }
        public int tram_vc_tram_audi_user { get; set; }
        public DateTime? tram_vc_tram_baja_fecha { get; set; }
        public int tram_vc_clie_id { get; set; }
        public int? tram_vc_cria_id { get; set; }
        public int tram_vc_porcentaje { get; set; }
        public string tram_vc_tran_tipo { get; set; }
        public string tram_vc_obser { get; set; }

        // Datos para grid.
        public string raza_codi_desc { get; set; }
        public int cria_nume { get; set; }
        public string clie_fanta { get; set; }
        public string estado { get; set; }
    }
}
