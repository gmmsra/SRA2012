﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_TramiteEntity
    {
        public List<TransferenciaAnimalEnPie_TramitesEntity> Tramites { get; set; }
        public List<TransferenciaAnimalEnPie_TramitesDetaEntity> TramitesDeta { get; set; }
        public List<TransferenciaAnimalEnPie_TramitesDocumEntity> TramitesDocum { get; set; }
        public List<TransferenciaAnimalEnPie_TramitesObseEntity> TramitesObse { get; set; }
        public List<TransferenciaAnimalEnPie_TramitesProductosEntity> TramitesProductos { get; set; }
        public List<TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity> TramitesEmbrionesFrescos { get; set; }
    }
}
