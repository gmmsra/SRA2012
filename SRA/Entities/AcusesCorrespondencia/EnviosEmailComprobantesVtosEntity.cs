﻿using System;
using System.Text;

namespace Entities.AcusesCorrespondencia
{
    public class EnviosEmailComprobantesVtosEntity
    {
        public Int32 intIdEnviosEmail;
        public Int32 intIdComprobante;
        public Int32 intIdCliente;
        public string varCliente;
        public Int32 intIdTipoComprobante;
        public string varComprobNume;
        public DateTime dateFecIngreComp;
        public DateTime dateFecVto;
        public decimal decImpNetoComp;
        public decimal decSaldoComp;
        public decimal decImpACtaCte;
        public string varEmailCliente;
        public bool bitEnviado;
        public DateTime dateFechaEnvio;
        public bool bitAcuse;
        public DateTime dateFechaAcuse;
        public string varHostAcuse;
        public string hashCodeEnvio;

    }
}
