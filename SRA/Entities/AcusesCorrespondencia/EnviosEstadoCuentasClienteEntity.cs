﻿using System;
using System.Text;
using System.Collections;

namespace Entities.AcusesCorrespondencia
{
    public class EnviosEstadoCuentasClienteEntity
    {
        public Int32 intIdEnviosEmail;
        public Int32 intIdCliente;
        public string varCliente;
        public string varEmailCliente;
        public decimal decDeudaCtaCte;
        public decimal decDeudaVencida;
        public decimal decCtaCteInteres;
        public decimal decOtrasCtas;
        public decimal decOtrasCtasVenc;
        public decimal decDeudaSocial;
        public decimal decDeudaSocialVencida;
        public decimal decDeudaSocialInteres;
        public decimal decLimiteSaldo;
        public decimal decTotalProforma;
        public decimal decTotalProformaRRGG;
        public decimal decTotalProformaLabo;
        public decimal decTotalProformaExpo;
        public decimal decTotalProformaOtras;
        public bool bitEnviado;
        public DateTime dateFechaEnvio;
        public bool bitAcuse;
        public DateTime dateFechaAcuse;
        public string varHostAcuse;
        public string hashCodeEnvio;
		public decimal decImporteACuenta;
		public ArrayList EstadoCuentasClientePorActividad = new ArrayList();
    }
}
