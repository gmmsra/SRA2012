using System;

namespace Entities.AcusesCorrespondencia
{
	/// <summary>
	/// Summary description for EnviosEstadoCuentasClientePorActividadEntity.
	/// </summary>
	public class EnviosEstadoCuentasClientePorActividadEntity
	{
		public Int32 intIdActividad;
		public string varActividadDesc;
		public decimal decImpoNeto;
		public decimal decImpoNetoVencida;
		public decimal decIntereses;
		public decimal decSobreTasasAlVto;
		public string varDatosSectorActividad;
	}
}
