using System;

namespace Entities.AcusesCorrespondencia
{
	/// <summary>
	/// Summary description for EnviosEmailComprobantesEntity.
	/// </summary>
	public class EnviosEmailComprobantesEntity
	{
		public Int32 intIdEnviosEmail;
		public Int32 intIdComprobante;
		public Int32 intIdCliente;
		public string varCliente;
		public Int32 intIdTipoComprobante;
		public string varComprobNume;
		public DateTime dateFecIngreComp;
		public string varEmailCliente;
		public bool bitEnviado;
		public DateTime dateFechaEnvio;
		public bool bitAcuse;
		public DateTime dateFechaAcuse;
		public string varHostAcuse;
		public string hashCodeEnvio;
	}
}
