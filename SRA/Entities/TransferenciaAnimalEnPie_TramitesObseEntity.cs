﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_TramitesObseEntity
    {
        #region Campos de la tabla
        public string trao_obse { get; set; }
        public int trao_tram_id { get; set; }
        public int? trao_trad_id { get; set; }
        public DateTime trao_fecha { get; set; }
        public int trao_id { get; set; }
        public DateTime trao_audi_fecha { get; set; }
        public int trao_audi_user { get; set; }
        public byte[] trao_tmsp { get; set; }
        #endregion

        #region Campos extras usados para otros obejetivos por los CU
        public string _requ_desc { get; set; }
        public int? _trad_requ_id { get; set; }
        #endregion
    }
}
