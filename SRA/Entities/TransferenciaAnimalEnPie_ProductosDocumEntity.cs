﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_ProductosDocumEntity
    {
        #region Campos de la tabla.
        public int prdo_id { get; set; }
        public int? prdo_prdt_id { get; set; }
        public string prdo_path { get; set; }
        public string prdo_refe { get; set; }
        public int? prdo_imag { get; set; }
        public DateTime prdo_audi_fecha { get; set; }
        public int prdo_audi_user { get; set; }
        public DateTime? prdo_baja_fecha { get; set; }
        public byte[] prdo_tmsp { get; set; }
        public int? prdo_obse { get; set; }
        #endregion

        #region Campos anexos para CU
        public string _path_ant { get; set; }
        public string _prdo_imag { get; set; }
        public string _estado { get; set; }
        public string _parampath { get; set; }
        #endregion
    }
}
