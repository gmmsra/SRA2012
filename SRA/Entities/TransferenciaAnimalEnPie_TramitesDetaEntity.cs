﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_TramitesDetaEntity
    {
        #region Campos de la tabla
        public int trad_id { get; set; }
        public int trad_tram_id { get; set; }
        public int? trad_pend { get; set; }
        public DateTime? trad_fina_fecha { get; set; }
        public int? trad_requ_id { get; set; }
        public int? trad_obli { get; set; }
        public int? trad_manu { get; set; }
        public DateTime? trad_baja_fecha { get; set; }
        public DateTime trad_audi_fecha { get; set; }
        public int trad_audi_user { get; set; }
        public byte[] trad_tmsp { get; set; }
        public string trad_caratula { get; set; }
        public DateTime? trad_fecha_ini { get; set; }
        public DateTime? trad_fecha_fin { get; set; }
        #endregion

        #region Campos extras usados para otros obejetivos por los CU
        public string _estado { get; set; }
        public string _requ_desc { get; set; }
        public string _pend { get; set; }
        public string _obli { get; set; }
        public string _manu { get; set; }
        #endregion
    }
}
