using System;


namespace Entities
{
   public class rgReceptorasTiposEntity
    {

        /// <summary>
        /// Summary description for rg_receptoras_tiposDL
        /// </summary>
		#region Private Variables
		private Int32 _rcti_id;
		private String _rcti_codi;
		private String _rcti_desc;
		private Int16 _rcti_edad;
		private DateTime _rcti_audi_fecha;
		private Int32 _rcti_audi_user;
		private DateTime _rcti_baja_fecha;
		private object _rcti_tmsp;

		#endregion

		#region Public Properties
		public Int32 rcti_id
		{
			get
			{
				return _rcti_id;
			}
			set
			{
				_rcti_id = value;
			}
		}
		public String rcti_codi
		{
			get
			{
				return _rcti_codi;
			}
			set
			{
				_rcti_codi = value;
			}
		}
		public String rcti_desc
		{
			get
			{
				return _rcti_desc;
			}
			set
			{
				_rcti_desc = value;
			}
		}
		public Int16 rcti_edad
		{
			get
			{
				return _rcti_edad;
			}
			set
			{
				_rcti_edad = value;
			}
		}
		public DateTime rcti_audi_fecha
		{
			get
			{
				return _rcti_audi_fecha;
			}
			set
			{
				_rcti_audi_fecha = value;
			}
		}
		public Int32 rcti_audi_user
		{
			get
			{
				return _rcti_audi_user;
			}
			set
			{
				_rcti_audi_user = value;
			}
		}
		public DateTime rcti_baja_fecha
		{
			get
			{
				return _rcti_baja_fecha;
			}
			set
			{
				_rcti_baja_fecha = value;
			}
		}
		public object rcti_tmsp
		{
			get
			{
				return _rcti_tmsp;
			}
			set
			{
				_rcti_tmsp = value;
			}
		}

        #endregion

	}
}
