using System;

namespace Entities
{
	/// <summary>
	/// Summary description for ParametrosEntity.
	/// </summary>
	public class ParametrosEntity
	{
		public int para_pesos_mone_id;
		public int para_dolar_mone_id;
		public string para_clie_docu_path;
		public string para_clie_foto_path;
		public int para_page_size_grilla;
		public int para_iva_cons_final;
		public int para_aprob_calif;
		public int para_soin_vige_dias;
		public int para_soci_anios_vitalicio;
		public int para_cemi_nume;
		public int para_bime_en_curso;
		public int para_cate_acti_id;
		public int para_cunica_nume;
		public int para_menor_edad;
		public int para_menor_ingreso_edad;
		public decimal para_soci_inte;
		public DateTime para_soci_inte_fecha;
		public int para_inst_defa;
		public DateTime para_reva_ulti_fecha;
		public DateTime para_ejer_inic_fecha;
		public DateTime para_ejer_fina_fecha;
		public int para_fact_vto_dias;
		public int para_reci_clie_id;
		public string para_desde_mail;
		public string para_admin_mail;
		public int para_rrgg_tram;
		public decimal para_ctac_inte;
		public decimal para_min_inte;
		public int para_labo_dias_turnos;
		public int para_capi_pcia_id;
		public int para_ult_tram_RRGG;
		public int para_dias_aviso_cai;
		public string para_leyen_dolar;
		public int para_soci_maxi_bime;
		public int para_dias_aviso_cheque;
		public string para_clie_cria_firma_path;
		public int para_sra_asoc_id;
		public string para_clie_raza_path;
		public int para_dias_cierre_arqueo;
		public int para_giro_banc_id;
		public string para_craz_docu_path;
		public string para_prdt_docu_path;
		public string para_nomb_servsql;
		public string para_tram_docu_path;
		public string para_temp_path;
		public int para_nume_cert;
		public int para_soci_ult_nume;
		public int para_soci_apod_tope;
		public int para_soci_apod_firm;
		public int para_servi_bovi_maxi_cant;
		public int para_servi_capr_maxi_cant;
		public int para_soci_fall_prec;
		public int para_recalcu_prof_dias;
		public int para_expo;
		public int para_ejer_cierre_mes;
		public int para_inte_presu_expo_mes;
		public int para_inte_presu_expo_anio;
		public bool para_iibb_perc;
		public bool para_iva_perc;
		public int para_EstableNumeroPmC;
		public int para_EmpresaNumeroPmC;
		public int para_BcoAsignadoNumeroPmC;
		public int para_conc_id;
		public int para_coti_id;
		public int para_cuba_id;
		public int para_banc_id;
		public decimal para_Porc_ComisionPmC;
		public decimal para_MontoMinimo_ComisionPmC;
		public decimal para_Iva_ComisionPmC;
	}
}
