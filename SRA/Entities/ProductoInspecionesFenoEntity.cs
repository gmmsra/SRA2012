using System;

namespace Entities
{
	/// <summary>
	/// Descripción breve de ProductoInspecionesFenoEntity.
	/// </summary>
	public class ProductoInspecionesFenoEntity
	{
		public Int64  prin_id;
		public Int64  prin_prdt_id;
		public DateTime prin_fecha;
		public int prin_resu;
		public string prin_obse;
		public string prin_tipo;
		public Int32 prin_clie_id;
		public Int32 prin_audi_user;
		public bool	prin_bitTrim;
		public string prin_varInspector;
		public bool	prdt_sexo;
		public DateTime prdt_naci_fecha;
		public string prdt_px; 
		public Int32 prdt_bamo_id;
		public DateTime prdt_baja_fecha;
		public Int32 prdt_regt_id;
		public Int32 prdt_vari_id;
	}
}
