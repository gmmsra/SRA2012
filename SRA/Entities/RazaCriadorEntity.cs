﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class RazaCriadorEntity
    {
        public int raza_id { get; set; }
        public int cria_id { get; set; }
        public int clie_id { get; set; }
        public int raza_codi { get; set; }
        public string raza_desc { get; set; }
        public string raza_codi_desc { get; set; }
        public int cria_nume { get; set; }
        public string clie_fanta { get; set; }
    }
}
