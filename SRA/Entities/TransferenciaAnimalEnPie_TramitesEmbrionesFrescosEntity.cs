﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_TramitesEmbrionesFrescosEntity
    {
        #region Campos de la tabla
        public int treb_id { get; set; }
        public int? treb_tram_id { get; set; }
        public string treb_carav { get; set; }
        public DateTime? treb_fecha_serv { get; set; }
        public DateTime? treb_fecha_recu { get; set; }
        public DateTime? treb_fecha_impl { get; set; }
        public int? treb_audi_user { get; set; }
        public DateTime? treb_audi_fecha { get; set; }
        #endregion

        #region Campos extras usados para otros obejetivos por los CU
        #endregion
    }
}
