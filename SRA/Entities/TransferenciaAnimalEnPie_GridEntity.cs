﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_GridEntity
    {
        #region Campos de la tabla Tramites
        public int tram_ttra_id { get; set; }
        public int? tram_visto { get; set; }
        public int tram_id { get; set; }
        public DateTime? tram_inic_fecha { get; set; }
        public int? tram_comp_clie_id { get; set; }
        public int? tram_vend_clie_id { get; set; }
        public int? tram_comp_cria_id { get; set; }
        public int? tram_vend_cria_id { get; set; }
        public DateTime? tram_oper_fecha { get; set; }
        public int tram_esta_id { get; set; }
        public int? tram_nume { get; set; }
        #endregion

        #region Campos anexos para el grid
        public string esta_desc { get; set; }
        public string prdt_nomb { get; set; }
        public string raza { get; set; }
        public string sexo { get; set; }
        public int? prdt_id { get; set; }
        public int? prdt_sra_nume { get; set; }
        public int? _numero { get; set; }
        public int? _cria_nume { get; set; }
        public string img_visto { get; set; }
        public string text_alt { get; set; }
        public string prdt_ndad { get; set; }
        public string clie_apel { get; set; }
        public string clie_apel_vend { get; set; }
        #endregion
    }
}
