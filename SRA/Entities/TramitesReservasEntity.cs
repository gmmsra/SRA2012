﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TramitesReservasEntity
    {
        public int IdTramiteReserva { get; set; }
        public int Tram_id { get; set; }
        public int ReservaSemen { get; set; }
        public int CantReservaSemen { get; set; }
        public int ReservaCria { get; set; }
        public int CantReservaCria { get; set; }
        public int ReservaEmbrion { get; set; }
        public int CantReservaEmbrion { get; set; }
        public int ReservaVientre { get; set; }
        public int ReservaMaterialGenetico { get; set; }
        public int ReservaDerechoClonar { get; set; }
        public int AutorizaCompradorClonar { get; set; }
        public int IdUsuarioAudi { get; set; }
        public DateTime FechaAudi { get; set; }
    }
}
