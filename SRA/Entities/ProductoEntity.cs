using System;

namespace Entities
{
	/// <summary>
	/// Descripción breve de ProductoEntity.
	/// </summary>
	public class ProductoEntity
	{
		public Int64 prdt_id;
		public Int32 idPadre;
		public Int32 idMadre;
		public Int32 idSNPS;
	}
}
