using System;
using System.Collections;

namespace Entities
{
	[Serializable ]
	public class productosFiltrosEntity
	{

	

		/// <summary>
		/// Entidad del control usrProdFiltro
		/// </summary>
		#region Private Variables
		private Int32 _rprf_id;
		private Int32 _rprf_raza_id;
		private String _rprf_sra_nume;
		private Int32 _rprf_sexo;
		private String _rprf_rp;
		private String _rprf_sra_nume_desde="";
		private String _rprf_sra_nume_hasta="";
		private String _rprf_rp_desde="";
		private String _rprf_rp_hasta="";
		private DateTime _rprf_naci_fecha_desde;
		private DateTime _rprf_naci_fecha_hasta;
		private DateTime _rprf_insc_fecha_desde;
		private DateTime _rprf_insc_fecha_hasta;
		private Int32 _rprf_cria_id;
		private Boolean _rprf_opcl;
		private Int32 _rprf_clie_prop_id;
		private Int32 _rprf_cria_prop_id;
		private String _rprf_clie_prop;
	private String _rprf_cria_prop;

		private String _rprf_nomb;
		private String _rprf_apodo;
		private Boolean _rprf_buscar_en;
		private Boolean _rprf_incluir_bajas;
		private Int32 _rprf_asoc_id;
		private String _rprf_asoc_nume;
		private String _rprf_ntra_lab;
		private String _rprf_tram_nume;
		private String _rprf_cuig;
		private Int32 _rprf_esta_id;
		private Int32 _rprf_ndad_id;
		private String _rprf_obse;
		private Int32 _rprf_madre_prdt_id;
		private Int32 _rprf_padre_prdt_id;
		private String _HBAPadreNume;
		private String _HBAMadreNume;
		private String _RPPadreNume;
		private String _RPMadreNume;


		#endregion

		#region Public Properties

		public String HBAPadreNume
		{
			get
			{
				return _HBAPadreNume;
			}
			set
			{
				_HBAPadreNume = value;
			}
		}

		public String HBAMadreNume
		{
			get
			{
				return _HBAMadreNume;
			}
			set
			{
				_HBAMadreNume = value;
			}
		}


		public String RPPadreNume
		{
			get
			{
				return _RPPadreNume;
			}
			set
			{
				_RPPadreNume = value;
			}
		}

		public String RPMadreNume
		{
			get
			{
				return _RPMadreNume;
			}
			set
			{
				_RPMadreNume = value;
			}
		}




		public Int32 rprf_id
		{
			get
			{
				return _rprf_id;
			}
			set
			{
				_rprf_id = value;
			}
		}
		public Int32 rprf_raza_id
		{
			get
			{
				return _rprf_raza_id;
			}
			set
			{
				_rprf_raza_id = value;
			}
		}
		public String rprf_sra_nume
		{
			get
			{
				return _rprf_sra_nume;
			}
			set
			{
				_rprf_sra_nume = value;
			}
		}
		public Int32 rprf_sexo
		{
			get
			{
				return _rprf_sexo;
			}
			set
			{
				_rprf_sexo = value;
			}
		}
		public String rprf_rp
		{
			get
			{
				return _rprf_rp;
			}
			set
			{
				_rprf_rp = value;
			}
		}
		public String rprf_sra_nume_desde
		{
			get
			{
				return _rprf_sra_nume_desde;
			}
			set
			{
				_rprf_sra_nume_desde = value;
			}
		}
		public String rprf_sra_nume_hasta
		{
			get
			{
				return _rprf_sra_nume_hasta;
			}
			set
			{
				_rprf_sra_nume_hasta = value;
			}
		}
		public String rprf_rp_desde
		{
			get
			{
				return _rprf_rp_desde;
			}
			set
			{
				_rprf_rp_desde = value;
			}
		}
		public String rprf_rp_hasta
		{
			get
			{
				return _rprf_rp_hasta;
			}
			set
			{
				_rprf_rp_hasta = value;
			}
		}
		public DateTime rprf_naci_fecha_desde
		{
			get
			{
				return _rprf_naci_fecha_desde;
			}
			set
			{
				_rprf_naci_fecha_desde = value;
			}
		}
		public DateTime rprf_naci_fecha_hasta
		{
			get
			{
				return _rprf_naci_fecha_hasta;
			}
			set
			{
				_rprf_naci_fecha_hasta = value;
			}
		}
		public DateTime rprf_insc_fecha_desde
		{
			get
			{
				return _rprf_insc_fecha_desde;
			}
			set
			{
				_rprf_insc_fecha_desde = value;
			}
		}
		public DateTime rprf_insc_fecha_hasta
		{
			get
			{
				return _rprf_insc_fecha_hasta;
			}
			set
			{
				_rprf_insc_fecha_hasta = value;
			}
		}
		public Int32 rprf_cria_id
		{
			get
			{
				return _rprf_cria_id;
			}
			set
			{
				_rprf_cria_id = value;
			}
		}
		public Boolean rprf_opcl
		{
			get
			{
				return _rprf_opcl;
			}
			set
			{
				_rprf_opcl = value;
			}
		}
		public Int32 rprf_clie_prop_id
		{
			get
			{
				return _rprf_clie_prop_id;
			}
			set
			{
				_rprf_clie_prop_id = value;
			}
		}
		public Int32 rprf_cria_prop_id
		{
			get
			{
				return _rprf_cria_prop_id;
			}
			set
			{
				_rprf_cria_prop_id = value;
			}
		}


		public String rprf_clie_prop
		{
			get
			{
				return _rprf_clie_prop;
			}
			set
			{
				_rprf_clie_prop = value;
			}
		}
		public String rprf_cria_prop
		{
			get
			{
				return _rprf_cria_prop;
			}
			set
			{
				_rprf_cria_prop = value;
			}
		}

		public String rprf_nomb
		{
			get
			{
				return _rprf_nomb;
			}
			set
			{
				_rprf_nomb = value;
			}
		}
		public String rprf_apodo
		{
			get
			{
				return _rprf_apodo;
			}
			set
			{
				_rprf_apodo = value;
			}
		}
		public Boolean rprf_buscar_en
		{
			get
			{
				return _rprf_buscar_en;
			}
			set
			{
				_rprf_buscar_en = value;
			}
		}
		public Boolean rprf_incluir_bajas
		{
			get
			{
				return _rprf_incluir_bajas;
			}
			set
			{
				_rprf_incluir_bajas = value;
			}
		}
		public Int32 rprf_asoc_id
		{
			get
			{
				return _rprf_asoc_id;
			}
			set
			{
				_rprf_asoc_id = value;
			}
		}
		public String rprf_asoc_nume
		{
			get
			{
				return _rprf_asoc_nume;
			}
			set
			{
				_rprf_asoc_nume = value;
			}
		}
		public String rprf_ntra_lab
		{
			get
			{
				return _rprf_ntra_lab;
			}
			set
			{
				_rprf_ntra_lab = value;
			}
		}
		public String rprf_tram_nume
		{
			get
			{
				return _rprf_tram_nume;
			}
			set
			{
				_rprf_tram_nume = value;
			}
		}
		public String rprf_cuig
		{
			get
			{
				return _rprf_cuig;
			}
			set
			{
				_rprf_cuig = value;
			}
		}
		public Int32 rprf_esta_id
		{
			get
			{
				return _rprf_esta_id;
			}
			set
			{
				_rprf_esta_id = value;
			}
		}
		public Int32 rprf_ndad_id
		{
			get
			{
				return _rprf_ndad_id;
			}
			set
			{
				_rprf_ndad_id = value;
			}
		}
		public String rprf_obse
		{
			get
			{
				return _rprf_obse;
			}
			set
			{
				_rprf_obse = value;
			}
		}
		public Int32 rprf_madre_prdt_id
		{
			get
			{
				return _rprf_madre_prdt_id;
			}
			set
			{
				_rprf_madre_prdt_id = value;
			}
		}
		public Int32 rprf_padre_prdt_id
		{
			get
			{
				return _rprf_padre_prdt_id;
			}
			set
			{
				_rprf_padre_prdt_id = value;
			}
		}

		#endregion


		
		

	}
}
