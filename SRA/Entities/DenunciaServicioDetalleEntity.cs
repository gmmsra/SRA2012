﻿using System;
using System.Collections;
using System.Text;

namespace Entities
{
    public class DenunciaServicioDetalleEntity
    {
        public Int32 sdde_id;
        public Int32 sdde_item_nume;
        public Int32 sdde_sede_id;
        public Int32 sdde_padre_id;
        public Int32 sdde_madre_id;
        public String sdde_obser;
        public Int32 sdde_audi_user;
        public DateTime sdde_audi_fecha;
        public DateTime sdde_baja_fecha;
        public DateTime sdde_servi_desde;
        public DateTime sdde_servi_hasta;
        public Int32 sdde_seti_id;
        public String sdde_padre_rp;
        public Int32 sdde_padre_sra_nume;
        public String sdde_madre_rp;
        public Int32 sdde_madre_sra_nume;
        public Int32 sdde_madre_asoc_id;
        public Int32 sdde_padre_asoc_id;
        public DateTime sdde_apro_fecha;
        public Int32 sdde_padre2_id;
        public String sdde_padre2_rp;
        public Int32 sdde_padre2_sra_nume;
        public Int32 sdde_padre3_id;
        public String sdde_padre3_rp;
        public Int32 sdde_padre3_sra_nume;
        public Int32 sdde_linea_nume;
        public Int32 sdde_regt_padre_id;
        public Int32 sdde_regt_padre2_id;
        public Int32 sdde_regt_madre_id;
        public String sdde_padre_nombre;
        public String sdde_padre2_nombre;
        public String sdde_padre3_nombre;
        public String sdde_madre_nombre;
        public String sdde_padre_ori_asoc_nume;
        public String sdde_padre2_ori_asoc_nume;
        public String sdde_padre3_ori_asoc_nume;
        public String sdde_madre_ori_asoc_nume;
        public Int32 sdde_padre2_asoc_id;
        public Int32 sdde_padre3_asoc_id;
        public Int32 sdde_regt_padre3_id;

        // Los siguentes items no pertenecen a la tabla RG_PRODUCTOS_INSCRIP.
        public Boolean IsEdit;
        public Boolean IsDelete;
        public String ErrorReglasValida;

		public DenunciaServicioDetalleEntity()
		{
			sdde_id = -1;
			sdde_item_nume = -1;
			sdde_sede_id = -1;
			sdde_padre_id = -1;
			sdde_madre_id = -1;
			sdde_obser = string.Empty;
			sdde_audi_user = -1;
			sdde_audi_fecha = System.DateTime.MinValue;
			sdde_baja_fecha = System.DateTime.MinValue;
			sdde_servi_desde = System.DateTime.MinValue;
			sdde_servi_hasta = System.DateTime.MinValue;
			sdde_seti_id = -1;
			sdde_padre_rp = string.Empty;
			sdde_padre_sra_nume = -1;
			sdde_madre_rp = string.Empty;
			sdde_madre_sra_nume = -1;
			sdde_madre_asoc_id = -1;
			sdde_padre_asoc_id = -1;
			sdde_apro_fecha = System.DateTime.MinValue;
			sdde_padre2_id = -1;
			sdde_padre2_rp = string.Empty;
			sdde_padre2_sra_nume = -1;
			sdde_padre3_id = -1;
			sdde_padre3_rp = string.Empty;
			sdde_padre3_sra_nume = -1;
			sdde_linea_nume = -1;
			sdde_regt_padre_id = -1;
			sdde_regt_padre2_id = -1;
			sdde_regt_madre_id = -1;
			sdde_padre_nombre = string.Empty;
			sdde_padre2_nombre = string.Empty;
			sdde_padre3_nombre = string.Empty;
			sdde_madre_nombre = string.Empty;
			sdde_padre_ori_asoc_nume = string.Empty;
			sdde_padre2_ori_asoc_nume = string.Empty;
			sdde_padre3_ori_asoc_nume = string.Empty;
			sdde_madre_ori_asoc_nume = string.Empty;
			sdde_padre2_asoc_id = -1;
			sdde_padre3_asoc_id = -1;
			sdde_regt_padre3_id = -1;

			// Los siguentes items no pertenecen a la tabla RG_PRODUCTOS_INSCRIP.
			IsEdit = false;
			IsDelete = false;
			ErrorReglasValida = string.Empty;
		}

        public object Clone()
        {
            return (DenunciaServicioDetalleEntity)this.MemberwiseClone();
        }
    }
}
