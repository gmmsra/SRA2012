﻿using System;
using System.Collections;
using System.Text;

namespace Entities
{
    public class DenunciaServicioEntity
    {
        public Int32 sede_id;
        public DateTime sede_fecha;
        public Int32 sede_clie_id;
        public Int32 sede_cria_id;
        public Int32 sede_folio_nume;
        public Int32 sede_raza_id;
        public Int32 sede_espe_id;
        public DateTime sede_servi_desde;
        public DateTime sede_servi_hasta; 
        public Int32 sede_audi_user; 
        public DateTime sede_audi_fecha;
        public Int32 sede_padre_id;
        public DateTime sede_baja_fecha;
        public Int32 sede_seti_id;
        public String sede_padre_rp;
        public Int32 sede_padre_sra_nume;
        public Int32 sede_regt_id;
        public Int32 sede_compati_1;
        public DateTime sede_presen_fecha;
        public Boolean sede_visto;
        public DateTime sede_apro_fecha;
        public Int32 sede_nro_control;

        public ArrayList LstDetallesPlanilla = new ArrayList();

		public DenunciaServicioEntity()
		{
			sede_id = -1;
			sede_fecha = System.DateTime.MinValue;
			sede_clie_id = -1;
			sede_cria_id = -1;
			sede_folio_nume = -1;
			sede_raza_id = -1;
			sede_espe_id = -1;
			sede_servi_desde = System.DateTime.MinValue;
			sede_servi_hasta = System.DateTime.MinValue;
			sede_audi_user = -1;
			sede_audi_fecha = System.DateTime.MinValue;
			sede_padre_id = -1;
			sede_baja_fecha = System.DateTime.MinValue;
			sede_seti_id = -1;
			sede_padre_rp = string.Empty;
			sede_padre_sra_nume = -1;
			sede_regt_id = -1;
			sede_compati_1 = -1;
			sede_presen_fecha = System.DateTime.MinValue;
			sede_visto = false;
			sede_apro_fecha = System.DateTime.MinValue;
			sede_nro_control = -1;
		}
    }         
}
