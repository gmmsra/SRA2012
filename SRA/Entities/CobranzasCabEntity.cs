using System;


namespace Entities
{
    /// <summary>
    /// objeto de la tabla cobranzas cabecera
    /// </summary>
    public class CobranzasCabEntity 
    {
        public Int64 intIdInterCobElecCab;
		public string varNroComprobante;
        public decimal decRecaudacion;
        public decimal decRecaudacionNeta;
        public Int32 intCantidadDeNovedades;
        public DateTime dateFechaAcreditacion;
        public Int32 intIdEmpresaCobElec;
        public DateTime dateFechaProceso;
		public string varDesEmpresaCobElect;
	}
}
