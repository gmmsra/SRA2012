using System;

namespace Entities
{
	/// <summary>
	/// Summary description for ActividadEntity.
	/// </summary>
	public class ActividadEntity
	{
		public int acti_id;
		public string acti_desc;
		public bool	acti_siste;
		public int acti_cuct_id;
		public DateTime acti_audi_fecha;
		public int acti_audi_user;
		public DateTime acti_baja_fecha;
		public int acti_compati_1;
		public bool	acti_fact;
		public bool	acti_gene_inte;
		public decimal acti_inte_porc;
		public decimal acti_inte_monto;
		public int acti_vcto_cant_dias;
		public int acti_vcto_fijo_dia;
		public int acti_ccos_id;
		public bool	acti_adhe;
		public decimal acti_mora_inte_porc;
		public int acti_nega_ccos_id;
		public int acti_mora_ccos_id;
		public bool	acti_discriminapreciounitario;
		public bool	acti_AdmiteProforma;
		public bool	acti_AplicaSobreTasaPagoRetrasado;
		public bool	acti_PermitePagoEnCuotas;
		public int acti_vcto2_cant_dias;
		public int acti_vcto3_cant_dias;
		public int acti_vcto4_cant_dias;
		public int acti_vcto5_cant_dias;
		public int acti_vcto6_cant_dias;
		public int acti_vcto7_cant_dias;
		public int acti_vcto8_cant_dias;
	}
}
