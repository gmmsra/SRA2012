using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// entidad de la tabla COMPROB_PAGOS
	/// </summary>
	public class ComprobantePagosEntity
	{
		public int paco_id;
		public int paco_comp_id;
		public int paco_pati_id;
		public int paco_mone_id;
		public string paco_nume;
		public int paco_tarj_id;
		public int paco_tarj_cuot;
		public string paco_tarj_nume;
		public int paco_cheq_id;
		public DateTime paco_audi_fecha;
		public decimal paco_impo;
		public int paco_audi_user;
		public int paco_cuba_id;
		public decimal paco_orig_impo;
		public int paco_dine_id;
		public string paco_tarj_autori;
		public DateTime paco_acre_fecha;
		public DateTime paco_acre_audi_fecha;
		public int paco_acre_audi_user;
		public bool paco_acre_apro;
		public DateTime paco_tarj_rech_fecha;
		public int paco_conc_id;
		public int paco_ccos_id;
		public int paco_tacl_id;
		public int paco_rtip_id;
		public int paco_orig_banc_id;
		public string paco_orig_banc_suc;
		public DateTime paco_baja_fecha;
		public string paco_baja_moti;
		public int paco_nd_comp_id;
		public string paco_compati_a;
		public string paco_tarj_cupo;
		public DateTime paco_acre_depo_fecha;
		public int paco_acus_comp_id;

		public int cheq_banc_id;
		public string cheq_nume;
		public DateTime cheq_teor_depo_fecha;
		public DateTime cheq_rece_fecha;
		public bool cheq_plaza;
		public int cheq_clea_id;
		public int cheq_chti_id;

	}
}
