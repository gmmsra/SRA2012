using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// Summary description for ResultPanelLabBonif.
	/// </summary>
	public class ResultPanelLabBonif
	{
		public string TituAcum;
        public int Cantidad;
		public int IdEspecie;
		public int CantidadFactura;
		public decimal Importe;
		public decimal ImporteIVAi;
		public DateTime fechaDesde;
		public DateTime fechaHasta;
		public DateTime hoy;
		public string varEspecie;
		public string varFacturasBonifLab;
	}
}
