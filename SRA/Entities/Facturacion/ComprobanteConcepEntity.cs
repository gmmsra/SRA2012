using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// entidad que corresponde a la tabla COMPROB_CONCEP
	/// </summary>
	public class ComprobanteConcepEntity
	{
		public int coco_id;
		public int coco_comp_id;
		public int coco_conc_id;
		public decimal coco_impo;		
		public int coco_ccos_id;
		public decimal coco_impo_ivai;	
		public string coco_desc_ampl;
		public decimal coco_porc;	
		public decimal coco_tasa_iva;	
		public bool coco_auto;
		public DateTime coco_audi_fecha;
		public int coco_audi_user;
		public int coco_prco_id;
		public decimal coco_impo_iva;	
	}
}
