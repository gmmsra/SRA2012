using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// entidad que resume los valores de los parametros para aplicar la bonificacion
	/// por cantidad de laboratorio
	/// </summary>
	public class ParametosBonifLabEntity
	{
		public int intIdEspecie;
		public int intIdConceptoBonifLab;
		public string varCodigoConcepto;
		public string varConceptoBonifLab;
		public int intIdCuentaContable; 
		public decimal decPorcBonifPorCantidad;
		public int intCantABonificar;
		public decimal decPorcBonifPorCantidad2;
		public int intCantABonificar2;
		public int intCCosConcepBonifLab;
		public string varCCosConcepBonifLab;
	}
}
