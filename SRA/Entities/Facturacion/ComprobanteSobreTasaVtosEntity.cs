using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// Summary description for ComprobanteSobreTasaVtosEntity.
	/// </summary>
	public class ComprobanteSobreTasaVtosEntity
	{
		public Int32 coso_id;
		public Int32 coso_aran_id;
		public Int32 coso_conc_id;
		public Int32 coso_comp_id;
		public Int32 coso_cant;
		public Int32 coso_anim_no_fact;
		public decimal coso_unit_impo;
		public DateTime coso_fecha;
		public DateTime coso_fecha_VtoAplica;
		public string coso_inic_rp;
		public string coso_fina_rp;
		public Int32 coso_tram;
		public bool coso_sin_carg;
		public bool coso_exen;
		public decimal coso_impo;
		public decimal coso_tasa_iva;
		public decimal coso_impo_ivai;
		public DateTime coso_audi_fecha;
		public Int32 coso_audi_user;
		public Int32 coso_ccos_id;
		public Int32 coso_cant_sin_carg;
		public string coso_acta;
		public Int32 coso_pran_id;
		public decimal coso_impo_iva;
		public Int32 coso_rrgg_aran_id;
		public bool coso_auto;
		public string coso_desc_ampl;
		public decimal coso_coco_porc;
		public Int32 coso_coco_prco_id;
		public bool coso_RegOriginal;
	}
}
