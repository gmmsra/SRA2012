using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// entidad de la tabla COMPROB_VTOS
	/// </summary>
	public class ComprobanteVtosEntity
	{
		public int covt_id;
		public int covt_comp_id;
		public DateTime covt_fecha;
		public decimal covt_impo;
		public decimal covt_porc;
		public bool covt_cance;
		public DateTime covt_audi_fecha;
		public int covt_audi_user;
		public int covt_compati_1;
	}
}
