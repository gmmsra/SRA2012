using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// entidad de la tabla COMPROB_ACUSES
	/// </summary>
	public class ComprobanteAcusesEntity
	{
		public int coac_id;
		public int coac_comp_id;
		public int coac_canc_comp_id;
		public decimal coac_impor;
		public int coac_covt_id;
		public DateTime coac_audi_fecha;
		public int coac_audi_user;
		public DateTime coac_baja_fecha;
	}
}
