using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// objeto comprobante con sus propiedades y los objeto de tablas asociadas 
	/// para dar un alta desde pagos mis cuentas
	/// </summary>
	public class ComprobanteEntity
	{
		public Int32 comp_id;
		public DateTime comp_fecha;
		public Int32 comp_clie_id;
		public bool comp_dh;
		public decimal comp_neto;
		public decimal comp_neto_soci;
		public decimal comp_neto_ctac;
		public bool comp_cance;
		public Int32 comp_cs; // 0 socio y 1 cta cte
		public Int32 comp_coti_id;
		public DateTime comp_ingr_fecha;
		public Int32 comp_mone_id;
		public Int32 comp_cemi_nume;
		public Int32 comp_nume;
		public Int32 comp_esta_id;
		public DateTime comp_Baja_fecha;
		public DateTime comp_audi_fecha;
		public Int32 comp_audi_user;
		public Int32 comp_compati_1;
		public Int32 comp_compati_id;
	    public string comp_letra;
		public int comp_cpti_id;
		public DateTime comp_ivar_fecha;
		public Int32 comp_acti_id;
		public bool comp_impre;
		public string comp_host;
		public string comp_compati_a;
		public Int32 comp_emct_id;
		public Int32 comp_modu_id;
		public string comp_compati_b;
		public string comp_compati_user;
		public decimal comp_neto_conc;
		public int comp_clie_ivap_id;
		public Int64 comp_clie_cuit;
		public Int32 acti_nega_ccos_id;

		public ComprobanteConcepEntity ComprobanteConcep = new ComprobanteConcepEntity();
		public ComprobanteDetaEntity ComprobanteDeta = new ComprobanteDetaEntity();
		public ComprobantePagosEntity ComprobantePagos = new ComprobantePagosEntity();
		public ComprobanteAcusesEntity ComprobantesAcuses = new ComprobanteAcusesEntity();
		public ComprobanteVtosEntity ComprobanteVtos = new ComprobanteVtosEntity();

		/// <summary>
		/// propiedad que setea el id del compromabte en 
		/// los demas comp_id de los demas objetos
		/// </summary>
		public Int32 SetCompId
		{
			set{
				comp_id = value;
				ComprobanteConcep.coco_comp_id = comp_id;
				ComprobanteDeta.code_comp_id = comp_id;
				ComprobantePagos.paco_comp_id = comp_id;
				ComprobantePagos.paco_acus_comp_id = comp_id;
				ComprobantesAcuses.coac_canc_comp_id = comp_id;
				ComprobanteVtos.covt_comp_id = comp_id;
			}
		}
	}
}

