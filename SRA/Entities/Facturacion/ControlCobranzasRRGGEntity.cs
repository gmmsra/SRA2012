using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// Descripción breve de ControlCobranzasRRGGEntity.
	/// </summary>
	public class ControlCobranzasRRGGEntity
	{
		public int comprobanteID;
		public int proformaID;
		public string nroFactura;
		public int cantFacturada;
		public int cantNoFacturada;
		public int idRaza;
		public int idCriador;
		public int codiServ;
		public int idClienteBuscado;
		public int idClienteEncontrado;
	}
}
