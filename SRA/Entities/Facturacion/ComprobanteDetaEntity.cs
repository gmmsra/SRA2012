using System;

namespace Entities.Facturacion
{
	/// <summary>
	/// entidad de la tabla COMPROB_DETA
	/// </summary>
	public class ComprobanteDetaEntity
	{
		public int code_id;
		public int code_comp_id;
		public int code_conv_id;
		public int code_tarj_id;
		public int code_tarj_cuot;
		public bool code_tarj_vtat;
		public string code_obse;
		public decimal code_coti;
		public bool code_pers;
		public int code_raza_id;
		public decimal code_impo_ivat_tasa;
		public decimal code_impo_ivat_tasa_redu;
		public decimal code_impo_ivat_tasa_sobre;
		public DateTime code_audi_fecha;
		public decimal code_impo_ivat_perc;
		public int code_audi_user;
		public string code_reci_nyap;
		public int code_sistgen;
		public int code_opergen;
		public int code_insc_id;
		public int code_asoc_comp_id;
		public int code_expo_id;
		public int code_tacl_id;
		public string code_tarj_nume;
		public int code_cria_id;
		public decimal code_impo_grava;
		public bool code_gene_inte;
		public int code_peli_id;
		public int code_reci_comp_id;		
		public decimal code_impo_iibb_perc;
	}
}
