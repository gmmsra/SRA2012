﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_ProductoEntity
    {
        public List<TransferenciaAnimalEnPie_ProductosEntity> Productos { get; set; }
        public List<TransferenciaAnimalEnPie_ProductosAnalisisEntity> ProductosAnalisis { get; set; }
        public List<TransferenciaAnimalEnPie_ProductosNumerosEntity> ProductosNumeros { get; set; }
        public List<TransferenciaAnimalEnPie_ProductosDocumEntity> ProductosDocum { get; set; }
    }
}
