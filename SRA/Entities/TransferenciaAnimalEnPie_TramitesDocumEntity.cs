﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class TransferenciaAnimalEnPie_TramitesDocumEntity
    {
        #region Campos de la tabla
        public int trdo_tram_id { get; set; }
        public int trdo_id { get; set; }
        public string trdo_path { get; set; }
        public string trdo_refe { get; set; }
        public DateTime trdo_audi_fecha { get; set; }
        public int trdo_audi_user { get; set; }
        public DateTime? trdo_baja_fecha { get; set; }
        public byte[] trdo_tmsp { get; set; }
        public string trdo_filename_adjunto { get; set; }
        #endregion

        #region Campos extras usados para otros obejetivos por los CU
        public string _estado { get; set; }
        public string _parampath { get; set; }
        #endregion
    }
}
