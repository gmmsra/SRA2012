﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class ErroresEntity
    {
        public int Id { get; set; }
        public string Fecha { get; set; }
        public string Codigo { get; set; }
        public string Mensaje { get; set; }
        public int Err { get; set; }
        public string Manual { get; set; }
        public string Audi_user { get; set; }


        public class Rg_Denuncias_Errores_Busq
        {
            public int Id { get; set; }
            public string Fecha { get; set; }
            public string Codigo { get; set; }
            public string Mensaje { get; set; }
            public int Err { get; set; }
            public string Manual { get; set; }
            public string Audi_User { get; set; }
        }
    
    
    }

}
