﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AccesoBD;
using Entities;

namespace DAO
{
    public class TransferenciaAnimalEnPieDao
    {
        private static string nameEspace = "DAO.TransferenciaAnimalPieDao.";

        public static DataSet Tramites_animalEnPie_Busq(int? tram_id, int? tram_nume, int? tram_raza_id, int? sexo, int? sra_nume, int? cria_id, int? prod_id,
            string prdt_nombre, DateTime? tram_fecha_desde, DateTime? tram_fecha_hasta, DateTime? tram_oper_fecha, int? tram_ttra_id, int? tram_esta_id,
            int? tram_pais_id, int? mostrar_vistos, string prod_nacionalidad, int? cria_vend_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_animalEnPie_busq");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_id", tram_id));
                cmd.Parameters.Add(new SqlParameter("@tram_nume", tram_nume));
                cmd.Parameters.Add(new SqlParameter("@tram_raza_id", tram_raza_id));
                cmd.Parameters.Add(new SqlParameter("@sexo", sexo));
                cmd.Parameters.Add(new SqlParameter("@sra_nume", sra_nume));
                cmd.Parameters.Add(new SqlParameter("@cria_id", cria_id));
                cmd.Parameters.Add(new SqlParameter("@prod_id", prod_id));
                cmd.Parameters.Add(new SqlParameter("@prdt_nombre", prdt_nombre));
                cmd.Parameters.Add(new SqlParameter("@tram_fecha_desde", tram_fecha_desde));
                cmd.Parameters.Add(new SqlParameter("@tram_fecha_hasta", tram_fecha_hasta));
                cmd.Parameters.Add(new SqlParameter("@tram_oper_fecha", tram_oper_fecha));
                cmd.Parameters.Add(new SqlParameter("@tram_ttra_id", tram_ttra_id));
                cmd.Parameters.Add(new SqlParameter("@tram_esta_id", tram_esta_id));
                cmd.Parameters.Add(new SqlParameter("@tram_pais_id", tram_pais_id));
                cmd.Parameters.Add(new SqlParameter("@mostrar_vistos", mostrar_vistos));
                cmd.Parameters.Add(new SqlParameter("@prod_nacionalidad", prod_nacionalidad));
                cmd.Parameters.Add(new SqlParameter("@cria_vend_id", cria_vend_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_animalEnPie_Busq, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static DataSet GetProductoByTramiteId(int tram_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("GetProductoByTramiteId");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_id", tram_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "GetProductoByTramiteId, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static DataSet Tramites_Consul(int? tram_id, int? tram_ttra_id, string formato)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_consul");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_id", tram_id));
                cmd.Parameters.Add(new SqlParameter("@tram_ttra_id", tram_ttra_id));
                cmd.Parameters.Add(new SqlParameter("@formato", formato));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "GetProductoByTramiteId, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static DataSet Productos_Consul(int prdt_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("productos_consul");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@prdt_id", prdt_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "GetProductoByTramiteId, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static void Tramites_Modi(TransferenciaAnimalEnPie_TramiteEntity Transferencia)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_modi");
                cmd.CommandType = CommandType.StoredProcedure;

                #region Control de parámetros con valores null antes de llamar al SP
                cmd.Parameters.Add(new SqlParameter("@tram_id", Transferencia.Tramites.First().tram_id));
                if (Transferencia.Tramites.First().tram_inic_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_inic_fecha", DateTime.Now));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_inic_fecha", Transferencia.Tramites.First().tram_inic_fecha));
                }
                if (Transferencia.Tramites.First().tram_fina_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_fecha", Transferencia.Tramites.First().tram_fina_fecha));
                }
                if (Transferencia.Tramites.First().tram_pres_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pres_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pres_fecha", Transferencia.Tramites.First().tram_pres_fecha));
                }
                if (Transferencia.Tramites.First().tram_esta_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_esta_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_esta_id", Transferencia.Tramites.First().tram_esta_id));
                }
                if (Transferencia.Tramites.First().tram_raza_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_raza_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_raza_id", Transferencia.Tramites.First().tram_raza_id));
                }
                if (Transferencia.Tramites.First().tram_ttra_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ttra_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ttra_id", Transferencia.Tramites.First().tram_ttra_id));
                }
                if (Transferencia.Tramites.First().tram_impr_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_impr_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_impr_id", Transferencia.Tramites.First().tram_impr_id));
                }
                if (Transferencia.Tramites.First().tram_nume == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nume", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nume", Transferencia.Tramites.First().tram_nume));
                }
                if (Transferencia.Tramites.First().tram_cria_cant == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_cria_cant", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_cria_cant", Transferencia.Tramites.First().tram_cria_cant));
                }
                if (Transferencia.Tramites.First().tram_dosi_cant == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_dosi_cant", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_dosi_cant", Transferencia.Tramites.First().tram_dosi_cant));
                }
                if (Transferencia.Tramites.First().tram_audi_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_audi_user", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_audi_user", Transferencia.Tramites.First().tram_audi_user));
                }
                if (Transferencia.Tramites.First().tram_baja_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_baja_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_baja_fecha", Transferencia.Tramites.First().tram_baja_fecha));
                }
                if (Transferencia.Tramites.First().tram_tmsp == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_tmsp", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_tmsp", Transferencia.Tramites.First().tram_tmsp));
                }
                if (Transferencia.Tramites.First().tram_fina_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_user", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_user", Transferencia.Tramites.First().tram_fina_user));
                }
                if (Transferencia.Tramites.First().tram_pais_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pais_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pais_id", Transferencia.Tramites.First().tram_pais_id));
                }
                if (Transferencia.Tramites.First().tram_comp_clie_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_clie_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_clie_id", Transferencia.Tramites.First().tram_comp_clie_id));
                }
                if (Transferencia.Tramites.First().tram_comp_cria_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_cria_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_cria_id", Transferencia.Tramites.First().tram_comp_cria_id));
                }
                if (Transferencia.Tramites.First().tram_vend_clie_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_clie_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_clie_id", Transferencia.Tramites.First().tram_vend_clie_id));
                }
                if (Transferencia.Tramites.First().tram_vend_cria_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_cria_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_cria_id", Transferencia.Tramites.First().tram_vend_cria_id));
                }
                if (Transferencia.Tramites.First().tram_prop_clie_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_clie_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_clie_id", Transferencia.Tramites.First().tram_prop_clie_id));
                }
                if (Transferencia.Tramites.First().tram_prop_cria_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_cria_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_cria_id", Transferencia.Tramites.First().tram_prop_cria_id));
                }
                if (Transferencia.Tramites.First().tram_embr_madre_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_madre_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_madre_id", Transferencia.Tramites.First().tram_embr_madre_id));
                }
                if (Transferencia.Tramites.First().tram_embr_padre_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre_id", Transferencia.Tramites.First().tram_embr_padre_id));
                }
                if (Transferencia.Tramites.First().tram_embr_padre2_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre2_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre2_id", Transferencia.Tramites.First().tram_embr_padre2_id));
                }
                if (Transferencia.Tramites.First().tram_embr_cant == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_cant", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_cant", Transferencia.Tramites.First().tram_embr_cant));
                }
                if (Transferencia.Tramites.First().tram_embr_tede_comp_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_comp_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_comp_id", Transferencia.Tramites.First().tram_embr_tede_comp_id));
                }
                if (Transferencia.Tramites.First().tram_embr_tede_vend_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_vend_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_vend_id", Transferencia.Tramites.First().tram_embr_tede_vend_id));
                }
                if (Transferencia.Tramites.First().tram_embr_recu_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_recu_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_recu_fecha", Transferencia.Tramites.First().tram_embr_recu_fecha));
                }
                if (Transferencia.Tramites.First().tram_nro_control == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nro_control", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nro_control", Transferencia.Tramites.First().tram_nro_control));
                }
                if (Transferencia.Tramites.First().tram_oper_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_oper_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_oper_fecha", Transferencia.Tramites.First().tram_oper_fecha));
                }
                if (Transferencia.Tramites.First().tram_liber_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_liber_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_liber_fecha", Transferencia.Tramites.First().tram_liber_fecha));
                }
                if (Transferencia.Tramites.First().tram_apob_asoc_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_apob_asoc_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_apob_asoc_fecha", Transferencia.Tramites.First().tram_apob_asoc_fecha));
                }
                if (Transferencia.Tramites.First().tram_ressnpsextr == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ressnpsextr", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ressnpsextr", Transferencia.Tramites.First().tram_ressnpsextr));
                }
                #endregion

                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Modi, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static DataSet Tramites_Deta_Consul(int Trad_tram_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_deta_consul");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trad_tram_id", Trad_tram_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Deta_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static int Tramites_Deta_Alta(TransferenciaAnimalEnPie_TramitesDetaEntity TramitesDeta)
        {
            int trad_id = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_deta_alta");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trad_tram_id", TramitesDeta.trad_tram_id));

                #region trad_pend
                if (TramitesDeta.trad_pend == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_pend", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_pend", TramitesDeta.trad_pend));
                }
                #endregion 

                #region trad_fina_fecha
                if (TramitesDeta.trad_fina_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_fina_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_fina_fecha", TramitesDeta.trad_fina_fecha));
                }
                #endregion

                #region trad_requ_id
                if (TramitesDeta.trad_requ_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_requ_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_requ_id", TramitesDeta.trad_requ_id));
                }
                #endregion

                #region trad_obli
                if (TramitesDeta.trad_obli == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_obli", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_obli", TramitesDeta.trad_obli));
                }
                #endregion

                #region trad_manu
                if (TramitesDeta.trad_manu == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_manu", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_manu", TramitesDeta.trad_manu));
                }
                #endregion

                #region trad_audi_user
                if (TramitesDeta.trad_audi_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_audi_user", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_audi_user", TramitesDeta.trad_audi_user));
                }
                #endregion

                trad_id = Convert.ToInt32(AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd));
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Deta_Alta, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return trad_id;
        }

        public static void Tramites_Deta_Baja(int trad_id, int trad_audi_user)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_deta_baja");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trad_id", trad_id));
                cmd.Parameters.Add(new SqlParameter("@trad_audi_user", trad_audi_user));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Deta_Baja, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static void Tramites_Deta_Modi(TransferenciaAnimalEnPie_TramitesDetaEntity TramitesDeta)
        {
            try 
            {
                SqlCommand cmd = new SqlCommand("tramites_deta_modi");
                cmd.CommandType = CommandType.StoredProcedure;

                #region Control de parámetros con valores null antes de llamar al SP
                cmd.Parameters.Add(new SqlParameter("@trad_tram_id", TramitesDeta.trad_tram_id));
                cmd.Parameters.Add(new SqlParameter("@trad_id", TramitesDeta.trad_id));
                if (TramitesDeta.trad_pend == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_pend", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_pend", TramitesDeta.trad_pend));
                }
                if (TramitesDeta.trad_fina_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_fina_fecha", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_fina_fecha", TramitesDeta.trad_fina_fecha));
                }
                if (TramitesDeta.trad_requ_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_requ_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_requ_id", TramitesDeta.trad_requ_id));
                }
                if (TramitesDeta.trad_obli == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_obli", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_obli", TramitesDeta.trad_obli));
                }
                if (TramitesDeta.trad_manu == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_manu", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_manu", TramitesDeta.trad_manu));
                }
                if (TramitesDeta.trad_baja_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_baja_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_baja_fecha", TramitesDeta.trad_baja_fecha));
                }
                if (TramitesDeta.trad_audi_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_audi_user", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_audi_user", TramitesDeta.trad_audi_user));
                }
                if (TramitesDeta.trad_tmsp == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_tmsp", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trad_tmsp", TramitesDeta.trad_tmsp));
                }
                #endregion

                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Deta_Modi, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static DataSet Tramites_Docum_Consul(int Trdo_tram_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_docum_consul");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trdo_tram_id", Trdo_tram_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Docum_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static int Tramites_Docum_Alta(TransferenciaAnimalEnPie_TramitesDocumEntity TramitesDocum)
        {
            int trdo_id = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_docum_alta");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trdo_tram_id", TramitesDocum.trdo_tram_id));
                cmd.Parameters.Add(new SqlParameter("@trdo_path", TramitesDocum.trdo_path));
                cmd.Parameters.Add(new SqlParameter("@trdo_refe", TramitesDocum.trdo_refe));
                cmd.Parameters.Add(new SqlParameter("@trdo_audi_user", TramitesDocum.trdo_audi_user));
                trdo_id = Convert.ToInt32(AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd));
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Docum_Alta, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return trdo_id;
        }

        public static void Tramites_Docum_Baja(int trdo_id, int trdo_audi_user)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_docum_baja");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trdo_id", trdo_id));
                cmd.Parameters.Add(new SqlParameter("@trdo_audi_user", trdo_audi_user));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Docum_Baja, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static void Tramites_Docum_Modi(TransferenciaAnimalEnPie_TramitesDocumEntity TramitesDocum)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_docum_modi");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trdo_tram_id", TramitesDocum.trdo_tram_id));
                cmd.Parameters.Add(new SqlParameter("@trdo_id", TramitesDocum.trdo_id ));
                cmd.Parameters.Add(new SqlParameter("@trdo_path", TramitesDocum.trdo_path));
                cmd.Parameters.Add(new SqlParameter("@trdo_refe", TramitesDocum.trdo_refe));
                cmd.Parameters.Add(new SqlParameter("@trdo_audi_user", TramitesDocum.trdo_audi_user));
                if (TramitesDocum.trdo_baja_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trdo_baja_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trdo_baja_fecha", TramitesDocum.trdo_baja_fecha));
                }
                if (TramitesDocum.trdo_tmsp == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trdo_tmsp", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trdo_tmsp", TramitesDocum.trdo_tmsp));
                }
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Docum_Modi, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static DataSet Tramites_Obse_Consul(int Trao_tram_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_obse_consul");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trao_tram_id", Trao_tram_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Obse_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static int Tramites_Obse_Alta(TransferenciaAnimalEnPie_TramitesObseEntity TramitesObse)
        {
            int trao_id = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_obse_alta");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trao_obse", TramitesObse.trao_obse));
                cmd.Parameters.Add(new SqlParameter("@trao_tram_id", TramitesObse.trao_tram_id));
                cmd.Parameters.Add(new SqlParameter("@trao_trad_id", TramitesObse.trao_trad_id));
                cmd.Parameters.Add(new SqlParameter("@trao_fecha", TramitesObse.trao_fecha));
                cmd.Parameters.Add(new SqlParameter("@trao_audi_user", TramitesObse.trao_audi_user ));
                trao_id = Convert.ToInt32(AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd));
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Obse_Alta, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return trao_id;
        }

        public static void Tramites_Obse_Baja(int trao_id, string trao_audi_user)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_obse_baja");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trao_id", trao_id));
                cmd.Parameters.Add(new SqlParameter("@trao_audi_user", trao_audi_user));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Obse_Baja, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static void Tramites_Obse_Modi(TransferenciaAnimalEnPie_TramitesObseEntity TramitesObse)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_obse_modi");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trao_obse", TramitesObse.trao_obse));
                cmd.Parameters.Add(new SqlParameter("@trao_tram_id", TramitesObse.trao_tram_id));
                cmd.Parameters.Add(new SqlParameter("@trao_trad_id", TramitesObse.trao_trad_id));
                cmd.Parameters.Add(new SqlParameter("@trao_fecha", TramitesObse.trao_fecha));
                cmd.Parameters.Add(new SqlParameter("@trao_id", TramitesObse.trao_id));
                cmd.Parameters.Add(new SqlParameter("@trao_audi_user", TramitesObse.trao_audi_user));
                cmd.Parameters.Add(new SqlParameter("@trao_tmsp", TramitesObse.trao_tmsp));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Obse_Modi, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static DataSet Tramites_Productos_Consul(int Trpr_tram_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_productos_consul");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trpr_tram_id", Trpr_tram_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Productos_Consul, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static void Tramites_Productos_Modi(TransferenciaAnimalEnPie_TramitesProductosEntity TramitesProductos)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_productos_modi");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trpr_id", TramitesProductos.trpr_id));
                cmd.Parameters.Add(new SqlParameter("@trpr_prdt_id", TramitesProductos.trpr_prdt_id));
                cmd.Parameters.Add(new SqlParameter("@trpr_tram_id", TramitesProductos.trpr_tram_id));

                #region trpr_apro
                if (TramitesProductos.trpr_apro == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro", TramitesProductos.trpr_apro));
                }
                #endregion 

                #region trpr_resu_fecha
                if (TramitesProductos.trpr_resu_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_resu_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_resu_fecha", TramitesProductos.trpr_resu_fecha));
                }
                #endregion

                #region trpr_audi_user
                if (TramitesProductos.trpr_audi_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_audi_user", TramitesProductos.trpr_audi_user));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_audi_user", TramitesProductos.trpr_audi_user));
                }
                #endregion

                #region trpr_sra_nume
                if (TramitesProductos.trpr_sra_nume == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sra_nume", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sra_nume", TramitesProductos.trpr_sra_nume));
                }
                #endregion

                #region trpr_asoc_id
                if (TramitesProductos.trpr_asoc_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_asoc_id",DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_asoc_id", TramitesProductos.trpr_asoc_id));
                }
                #endregion

                #region trpr_padre_asoc_id
                if (TramitesProductos.trpr_padre_asoc_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_padre_asoc_id", DBNull.Value));
                }
                else
                { 
                    cmd.Parameters.Add(new SqlParameter("@trpr_padre_asoc_id", TramitesProductos.trpr_padre_asoc_id));
                }
                #endregion

                #region trpr_madre_asoc_id
                if (TramitesProductos.trpr_madre_asoc_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_madre_asoc_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_madre_asoc_id", TramitesProductos.trpr_madre_asoc_id));
                }
                #endregion

                #region trpr_rp
                if (TramitesProductos.trpr_rp == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_rp", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_rp", TramitesProductos.trpr_rp));
                }
                #endregion

                #region trpr_sexo
                if (TramitesProductos.trpr_sexo == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sexo", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sexo", TramitesProductos.trpr_sexo));
                }
                #endregion

                #region trpr_baja_fecha
                if (TramitesProductos.trpr_baja_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_baja_fecha", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_baja_fecha", TramitesProductos.trpr_baja_fecha));
                }
                #endregion

                #region trpr_apro_fecha
                if (TramitesProductos.trpr_apro_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro_fecha", TramitesProductos.trpr_apro_fecha));
                }
                #endregion

                #region trpr_tmsp
                if (TramitesProductos.trpr_tmsp == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_tmsp", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_tmsp", TramitesProductos.trpr_tmsp));
                }
                #endregion

                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Productos_Modi, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static int Tramites_Productos_Alta(TransferenciaAnimalEnPie_TramitesProductosEntity TramitesProductos)
        {
            int trpr_id = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_productos_alta");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@trpr_prdt_id", TramitesProductos.trpr_prdt_id));
                cmd.Parameters.Add(new SqlParameter("@trpr_tram_id", TramitesProductos.trpr_tram_id));

                #region trpr_apro
                if (TramitesProductos.trpr_apro == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro",DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro", TramitesProductos.trpr_apro));
                }
                #endregion

                #region trpr_resu_fecha
                if (TramitesProductos.trpr_resu_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_resu_fecha", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_resu_fecha", TramitesProductos.trpr_resu_fecha));
                }
                #endregion

                #region trpr_audi_user
                if (TramitesProductos.trpr_audi_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_audi_user", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_audi_user", TramitesProductos.trpr_audi_user));
                }
                #endregion

                #region trpr_sra_nume
                if (TramitesProductos.trpr_sra_nume == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sra_nume", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sra_nume", TramitesProductos.trpr_sra_nume));
                }
                #endregion

                #region trpr_asoc_id
                if (TramitesProductos.trpr_asoc_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_asoc_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_asoc_id", TramitesProductos.trpr_asoc_id));
                }
                #endregion

                #region trpr_padre_asoc_id
                if (TramitesProductos.trpr_padre_asoc_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_padre_asoc_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_padre_asoc_id", TramitesProductos.trpr_padre_asoc_id));
                }
                #endregion

                #region trpr_madre_asoc_id
                if (TramitesProductos.trpr_madre_asoc_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_madre_asoc_id", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_madre_asoc_id", TramitesProductos.trpr_madre_asoc_id));
                }
                #endregion

                #region trpr_rp
                if (TramitesProductos.trpr_rp == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_rp", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_rp", TramitesProductos.trpr_rp));
                }
                #endregion

                #region trpr_sexo
                if (TramitesProductos.trpr_sexo == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sexo", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_sexo", TramitesProductos.trpr_sexo));
                }
                #endregion

                #region trpr_apro_fecha
                if (TramitesProductos.trpr_apro_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro_fecha", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@trpr_apro_fecha", TramitesProductos.trpr_apro_fecha));
                }
                #endregion

                trpr_id = Convert.ToInt32(AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd));
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Productos_Modi, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return trpr_id;
        }

        public static int Tramites_Alta(TransferenciaAnimalEnPie_TramiteEntity Transferencia)
        {
            int tram_id = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("tramites_alta");
                cmd.CommandType = CommandType.StoredProcedure;

                #region tram_inic_fecha
                
                if (Transferencia.Tramites.First().tram_inic_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_inic_fecha", DateTime.Now));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_inic_fecha", Transferencia.Tramites.First().tram_inic_fecha));
                }
                
                #endregion

                #region tram_fina_fecha

                if (Transferencia.Tramites.First().tram_fina_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_fecha", Transferencia.Tramites.First().tram_fina_fecha));
                }

                #endregion

                #region tram_pres_fecha

                if (Transferencia.Tramites.First().tram_pres_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pres_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pres_fecha", Transferencia.Tramites.First().tram_pres_fecha));
                }

                #endregion

                #region tram_esta_id

                if (Transferencia.Tramites.First().tram_esta_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_esta_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_esta_id", Transferencia.Tramites.First().tram_esta_id));
                }

                #endregion

                #region tram_raza_id

                if (Transferencia.Tramites.First().tram_raza_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_raza_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_raza_id", Transferencia.Tramites.First().tram_raza_id));
                }

                #endregion

                #region tram_ttra_id

                if (Transferencia.Tramites.First().tram_ttra_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ttra_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ttra_id", Transferencia.Tramites.First().tram_ttra_id));
                }

                #endregion

                #region tram_impr_id

                if (Transferencia.Tramites.First().tram_impr_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_impr_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_impr_id", Transferencia.Tramites.First().tram_impr_id));
                }

                #endregion

                #region tram_nume

                if (Transferencia.Tramites.First().tram_nume == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nume", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nume", Transferencia.Tramites.First().tram_nume));
                }

                #endregion

                #region tram_cria_cant

                if (Transferencia.Tramites.First().tram_cria_cant == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_cria_cant", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_cria_cant", Transferencia.Tramites.First().tram_cria_cant));
                }

                #endregion

                #region tram_dosi_cant

                if (Transferencia.Tramites.First().tram_dosi_cant == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_dosi_cant", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_dosi_cant", Transferencia.Tramites.First().tram_dosi_cant));
                }

                #endregion 

                #region tram_audi_user

                if (Transferencia.Tramites.First().tram_audi_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_audi_user", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_audi_user", Transferencia.Tramites.First().tram_audi_user));
                }

                #endregion

                #region tram_fina_user

                if (Transferencia.Tramites.First().tram_fina_user == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_user", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_fina_user", Transferencia.Tramites.First().tram_fina_user));
                }

                #endregion 

                #region tram_pais_id

                if (Transferencia.Tramites.First().tram_pais_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pais_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_pais_id", Transferencia.Tramites.First().tram_pais_id));
                }

                #endregion

                #region tram_comp_clie_id

                if (Transferencia.Tramites.First().tram_comp_clie_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_clie_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_clie_id", Transferencia.Tramites.First().tram_comp_clie_id));
                }

                #endregion

                #region tram_comp_cria_id

                if (Transferencia.Tramites.First().tram_comp_cria_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_cria_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_comp_cria_id", Transferencia.Tramites.First().tram_comp_cria_id));
                }

                #endregion

                #region tram_vend_clie_id

                if (Transferencia.Tramites.First().tram_vend_clie_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_clie_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_clie_id", Transferencia.Tramites.First().tram_vend_clie_id));
                }

                #endregion

                #region tram_vend_cria_id

                if (Transferencia.Tramites.First().tram_vend_cria_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_cria_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_vend_cria_id", Transferencia.Tramites.First().tram_vend_cria_id));
                }

                #endregion

                #region tram_prop_clie_id

                if (Transferencia.Tramites.First().tram_prop_clie_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_clie_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_clie_id", Transferencia.Tramites.First().tram_prop_clie_id));
                }

                #endregion 

                #region tram_prop_cria_id

                if (Transferencia.Tramites.First().tram_prop_cria_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_cria_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_prop_cria_id", Transferencia.Tramites.First().tram_prop_cria_id));
                }

                #endregion

                #region tram_embr_madre_id

                if (Transferencia.Tramites.First().tram_embr_madre_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_madre_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_madre_id", Transferencia.Tramites.First().tram_embr_madre_id));
                }

                #endregion

                #region tram_embr_padre_id

                if (Transferencia.Tramites.First().tram_embr_padre_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre_id", Transferencia.Tramites.First().tram_embr_padre_id));
                }

                #endregion

                #region tram_embr_padre2_id

                if (Transferencia.Tramites.First().tram_embr_padre2_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre2_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_padre2_id", Transferencia.Tramites.First().tram_embr_padre2_id));
                }

                #endregion

                #region tram_embr_cant

                if (Transferencia.Tramites.First().tram_embr_cant == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_cant", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_cant", Transferencia.Tramites.First().tram_embr_cant));
                }

                #endregion

                #region tram_embr_tede_comp_id

                if (Transferencia.Tramites.First().tram_embr_tede_comp_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_comp_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_comp_id", Transferencia.Tramites.First().tram_embr_tede_comp_id));
                }

                #endregion

                #region tram_embr_tede_vend_id

                if (Transferencia.Tramites.First().tram_embr_tede_vend_id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_vend_id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_tede_vend_id", Transferencia.Tramites.First().tram_embr_tede_vend_id));
                }

                #endregion

                #region tram_embr_recu_fecha

                if (Transferencia.Tramites.First().tram_embr_recu_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_recu_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_embr_recu_fecha", Transferencia.Tramites.First().tram_embr_recu_fecha));
                }

                #endregion

                #region tram_nro_control

                if (Transferencia.Tramites.First().tram_nro_control == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nro_control", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_nro_control", Transferencia.Tramites.First().tram_nro_control));
                }

                #endregion

                #region tram_oper_fecha

                if (Transferencia.Tramites.First().tram_oper_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_oper_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_oper_fecha", Transferencia.Tramites.First().tram_oper_fecha));
                }

                #endregion

                #region tram_liber_fecha

                if (Transferencia.Tramites.First().tram_liber_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_liber_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_liber_fecha", Transferencia.Tramites.First().tram_liber_fecha));
                }

                #endregion 

                #region tram_apob_asoc_fecha 

                if (Transferencia.Tramites.First().tram_apob_asoc_fecha == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_apob_asoc_fecha", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_apob_asoc_fecha", Transferencia.Tramites.First().tram_apob_asoc_fecha));
                }

                #endregion

                #region tram_ressnpsextr

                if (Transferencia.Tramites.First().tram_ressnpsextr == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ressnpsextr", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@tram_ressnpsextr", Transferencia.Tramites.First().tram_ressnpsextr));
                }

                #endregion

                tram_id = Convert.ToInt32(AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd));
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Tramites_Alta, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return tram_id;
        }

        public static string P_rg_ValidarTransferenciaProductosN(int TramiteProdId, int Usua_id, int? Retiene)
        {
            string result = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand("p_rg_ValidarTransferenciaProductosN");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tramiteProdId", TramiteProdId));
                cmd.Parameters.Add(new SqlParameter("@usua_id", Usua_id));
                cmd.Parameters.Add(new SqlParameter("@retiene", Retiene));
                result = AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd);

            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "P_rg_ValidarTransferenciaProductosN, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return result;
        }

        public static string Fc_Obtener_Tipo_Regimen( int usua_id, int prdt_id, string fecha_referencia, int seti_id )
        {
            string result = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand("fc_Obtener_Tipo_Regimen");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@usua_id", usua_id));
                cmd.Parameters.Add(new SqlParameter("@prdt_id", prdt_id));
                cmd.Parameters.Add(new SqlParameter("@fecha_referencia", fecha_referencia));
                cmd.Parameters.Add(new SqlParameter("@seti_id", seti_id));
                result = AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd);

            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Fc_Obtener_Tipo_Regimen, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return result;
        }

        public static string P_rg_TranferenciaProductoValidaCambioFecha(int prdt_id, DateTime fecha, int tram_id)
        {
            string result = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand("p_rg_TranferenciaProductoValidaCambioFecha");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@prdt_id", prdt_id));
                cmd.Parameters.Add(new SqlParameter("@fecha", fecha));
                cmd.Parameters.Add(new SqlParameter("@tram_id", tram_id));
                result = AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd);

            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Fc_Obtener_Tipo_Regimen, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return result;
        }
    }
}
