using System;
using System.Text;
using System.Data;
using AccesoBD;
using System.Collections;
using Entities.AcusesCorrespondencia;
using System.Data.SqlClient;
using System.Configuration;

namespace DAO.EmailSenderService
{
	/// <summary>
	/// Summary description for EmailSenderServiceDao.
	/// </summary>
	public class EmailSenderServiceDao
	{
		/// <summary>
		/// metodo que recupera los datos para generar el pedio de envio
		/// del comprobante soliccitado a los mails que recupere
		/// y que luego sera enviado por el servicio de mails sender de la sra
		/// </summary>
		/// <param name="IdComprobante"></param>
		/// <returns>ArrayList</returns>
		public static ArrayList GetDatosComprobanteAEnviar(Int32 IdComprobante )
		{
			ArrayList respuesta = new ArrayList();
			
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_FAC_GetDatosComprobanteAEnviar ");
			pstrProc.Append("@IdComprobante=");
			pstrProc.Append(IdComprobante);

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						EnviosEmailComprobantesEntity dato = new EnviosEmailComprobantesEntity();

						if (dr["intIdEnviosEmail"] != DBNull.Value){dato.intIdEnviosEmail = Convert.ToInt32(dr["intIdEnviosEmail"]);}
						dato.intIdComprobante = Convert.ToInt32(dr["intIdComprobante"]);
						dato.intIdCliente = Convert.ToInt32(dr["intIdCliente"]);
						dato.varCliente = dr["varCliente"].ToString();
						dato.intIdTipoComprobante = Convert.ToInt32(dr["intIdTipoComprobante"]);
						dato.varComprobNume = dr["varComprobNume"].ToString();
						if (dr["dateFecIngreComp"] != DBNull.Value) { dato.dateFecIngreComp = Convert.ToDateTime(dr["dateFecIngreComp"]); }
						dato.varEmailCliente = dr["varEmailCliente"].ToString();
						dato.bitEnviado = Convert.ToBoolean(dr["bitEnviado"]);
						if (dr["dateFechaEnvio"] != DBNull.Value) { dato.dateFechaEnvio = Convert.ToDateTime(dr["dateFechaEnvio"]); }
						dato.bitAcuse = Convert.ToBoolean(dr["bitAcuse"]);
						if (dr["dateFechaAcuse"] != DBNull.Value) { dato.dateFechaAcuse = Convert.ToDateTime(dr["dateFechaAcuse"]); }
						dato.varHostAcuse = dr["varHostAcuse"].ToString();
						dato.hashCodeEnvio = dr["hashCodeEnvio"].ToString();

						respuesta.Add(dato);
					}
			}
			catch (Exception ex)
			{
				string Msg = "EmailSenderServiceDao.GetComprobantesEnviados: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		
		/// <summary>
		/// metodo que crea una nueva fila en la tabla ARQ_EnviosEmailComprobantes
		/// para que sea enviado por el servicio emailsendersra
		/// </summary>
		/// <param name="dato"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearMailEnvioDocumentacion(EnviosEmailComprobantesEntity dato)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			
			pstrProc.Append("exec P_FAC_CrearMailEnvioDocumentacion ");
			pstrProc.Append("@intIdComprobante=");
			if (dato.intIdComprobante != _DataBase.IntDBNull() ) { pstrProc.Append(dato.intIdComprobante); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@intIdCliente=");
			if (dato.intIdCliente != _DataBase.IntDBNull() ) { pstrProc.Append(dato.intIdCliente); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@varCliente=");
			if (dato.varCliente.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(dato.varCliente)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@intIdTipoComprobante=");
			if (dato.intIdTipoComprobante != _DataBase.IntDBNull() ) { pstrProc.Append(dato.intIdTipoComprobante); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@varComprobNume=");
			if (dato.varComprobNume.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(dato.varComprobNume)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@dateFecIngreComp=");
			if (dato.dateFecIngreComp != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)dato.dateFecIngreComp).ToString("yyyy/MM/dd HH:mm:ss")));} 	
			else { pstrProc.Append("null"); }
			pstrProc.Append(",@varEmailCliente=");
			if (dato.varEmailCliente.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(dato.varEmailCliente)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@bitEnviado=");
			pstrProc.Append(dato.bitEnviado);
			pstrProc.Append(",@dateFechaEnvio=");
			if (dato.dateFechaEnvio != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)dato.dateFechaEnvio).ToString("yyyy/MM/dd HH:mm:ss")));} 	
			else { pstrProc.Append("null"); }
			pstrProc.Append(",@bitAcuse=");
			pstrProc.Append(dato.bitAcuse); 
			pstrProc.Append(",@dateFechaAcuse=");
			if (dato.dateFechaAcuse != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)dato.dateFechaAcuse).ToString("yyyy/MM/dd HH:mm:ss")));} 	
			else { pstrProc.Append("null"); }
			pstrProc.Append(",@varHostAcuse=");
			if (dato.varHostAcuse.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(dato.varHostAcuse)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@hashCodeEnvio=");
			if (dato.hashCodeEnvio.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(dato.hashCodeEnvio)); } else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "EmailSenderServiceDao.CrearMailEnvioDocumentacion: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}
			return respuesta;
		}
	}
}
