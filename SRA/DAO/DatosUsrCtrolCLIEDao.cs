﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AccesoBD;
using Entities;
using DAO;
//using Common;  

namespace DAO
{
    public class DatosUsrCtrolCLIEDao
    {
        public static DataSet GetDatosUsrCtrolCLIE(int cria_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetDatosUsrCtrolCLIE");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@cria_id", cria_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return ds;
        }
    }
}
