using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for ComprobanteVtoDao.
	/// </summary>
	public class ComprobanteVtoDao
	{
		/// <summary>
		/// Dario 2013-07-26
		/// metodo que crea una nueva fila en la tabla comprob_vto
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearComprobanteVto(SqlTransaction trans, ComprobanteVtosEntity obj)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprob_vtos_alta ");
			pstrProc.Append("@covt_comp_id=");
			if (obj.covt_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.covt_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@covt_fecha=");
			if (obj.covt_fecha != _DataBase.DateTimeDBNull()) {pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.covt_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@covt_impo=");
			if (obj.covt_impo != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.covt_impo.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@covt_porc=");
			if (obj.covt_porc != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.covt_porc.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@covt_cance=");
			pstrProc.Append(obj.covt_cance);
			pstrProc.Append(",@covt_audi_user=");
			if (obj.covt_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.covt_audi_user); } else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["covt_id"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ComprobanteVtoDao.CrearComprobanteVto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
	}
}
