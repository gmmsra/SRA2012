using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for ComprobanteAcuseDao.
	/// </summary>
	public class ComprobanteAcuseDao
	{
		/// <summary>
		/// metodo que crea una nueva fila en la tabla comprob_acuses
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static Int32 CrearComprobanteAcuse(SqlTransaction trans, ComprobanteAcusesEntity obj)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprob_acuses_alta ");
			pstrProc.Append("@coac_comp_id=");
			if (obj.coac_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coac_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coac_canc_comp_id=");
			if (obj.coac_canc_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coac_canc_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coac_impor=");
			if (obj.coac_impor != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coac_impor.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coac_covt_id=");
			if (obj.coac_covt_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coac_covt_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coac_audi_user=");
			if (obj.coac_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.coac_audi_user); } else { pstrProc.Append("null"); }
	
			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["coac_id"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ComprobanteAcuseDao.CrearComprobanteAcuse: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
	}
}
