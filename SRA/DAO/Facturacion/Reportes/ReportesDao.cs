using System;
using System.Data;
using AccesoBD;
using System.Text;
using System.Data.SqlClient;

namespace DAO.Facturacion.Reportes
{
	/// <summary>
	/// Summary description for ReportesDao.
	/// </summary>
	public class ReportesDao
	{
		//@ smalldatetime = null

		/// <summary>
		/// metodo que retorna los datos para la exportacion de deuda integral a exel ExportaDeudaIntegral.aspx 
		/// </summary>
		/// <param name="IdCliente"></param>
		/// <param name="FechaFiltro"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetExportaDeudaIntegral(int IdCliente, DateTime FechaFiltro)
		{
			DataSet respuesta = null;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec rpt_exporta_deuda_integral_consul ");
			pstrProc.Append("@clie_id=");
			if (IdCliente != _DataBase.IntDBNull()) { pstrProc.Append(IdCliente); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@fecha_Hasta=");
			if (FechaFiltro != _DataBase.DateTimeDBNull()){pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)FechaFiltro).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }

			try
			{
				//																				time out maximo
				respuesta = clsSQLServer.gExecuteQuery(_DataBase.mstrConn , pstrProc.ToString(), 999999999);
			}
			catch (Exception ex)
			{
				string Msg = "ReportesDao.GetExportaDeudaIntegral: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

	}
}
