using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using Entities;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for ActividadDao.
	/// </summary>
	public class ActividadDao
	{
		/// <summary>
		/// metodo que retorna una actividad por su id
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <returns></returns>
		public static ActividadEntity GetActividadPorId(Int32 IdActividad)
		{
			ActividadEntity respuesta = new ActividadEntity();
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec actividades_consul ");
			pstrProc.Append("@acti_id=");
			pstrProc.Append(IdActividad); 
            
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["acti_id"] != System.DBNull.Value){respuesta.acti_id = Convert.ToInt32(dr["acti_id"]);}
						if(dr["acti_desc"] != System.DBNull.Value){respuesta.acti_desc = dr["acti_desc"].ToString();}
						if(dr["acti_siste"] != System.DBNull.Value){respuesta.acti_siste = Convert.ToBoolean(dr["acti_siste"]);}
						if(dr["acti_cuct_id"] != System.DBNull.Value){respuesta.acti_cuct_id = Convert.ToInt32(dr["acti_cuct_id"]);}
						if(dr["acti_audi_fecha"] != System.DBNull.Value){respuesta.acti_audi_fecha = Convert.ToDateTime(dr["acti_audi_fecha"]);}
						if(dr["acti_audi_user"] != System.DBNull.Value){respuesta.acti_audi_user = Convert.ToInt32(dr["acti_audi_user"]);}
						if(dr["acti_baja_fecha"] != System.DBNull.Value){respuesta.acti_baja_fecha = Convert.ToDateTime(dr["acti_baja_fecha"]);}
						if(dr["acti_compati_1"] != System.DBNull.Value){respuesta.acti_compati_1 = Convert.ToInt32(dr["acti_compati_1"]);}
						if(dr["acti_fact"] != System.DBNull.Value){respuesta.acti_fact = Convert.ToBoolean(dr["acti_fact"]);}
						if(dr["acti_gene_inte"] != System.DBNull.Value){respuesta.acti_gene_inte = Convert.ToBoolean(dr["acti_gene_inte"]);}
						if(dr["acti_inte_porc"] != System.DBNull.Value){respuesta.acti_inte_porc = Convert.ToDecimal(dr["acti_inte_porc"]);} else{respuesta.acti_inte_porc = 0;}
						if(dr["acti_inte_monto"] != System.DBNull.Value){respuesta.acti_inte_monto = Convert.ToDecimal(dr["acti_inte_monto"]);}
						if(dr["acti_vcto_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto_cant_dias = Convert.ToInt32(dr["acti_vcto_cant_dias"]);}else{respuesta.acti_vcto_cant_dias = 0;}
						if(dr["acti_vcto_fijo_dia"] != System.DBNull.Value){respuesta.acti_vcto_fijo_dia = Convert.ToInt32(dr["acti_vcto_fijo_dia"]);}
						if(dr["acti_ccos_id"] != System.DBNull.Value){respuesta.acti_ccos_id = Convert.ToInt32(dr["acti_ccos_id"]);}
						if(dr["acti_adhe"] != System.DBNull.Value){respuesta.acti_adhe = Convert.ToBoolean(dr["acti_adhe"]);}
						if(dr["acti_mora_inte_porc"] != System.DBNull.Value){respuesta.acti_mora_inte_porc = Convert.ToDecimal(dr["acti_mora_inte_porc"]);}
						if(dr["acti_nega_ccos_id"] != System.DBNull.Value){respuesta.acti_nega_ccos_id = Convert.ToInt32(dr["acti_nega_ccos_id"]);}
						if(dr["acti_mora_ccos_id"] != System.DBNull.Value){respuesta.acti_mora_ccos_id = Convert.ToInt32(dr["acti_mora_ccos_id"]);}
						if(dr["acti_discriminapreciounitario"] != System.DBNull.Value){respuesta.acti_discriminapreciounitario = Convert.ToBoolean(dr["acti_discriminapreciounitario"]);}
						if(dr["acti_AdmiteProforma"] != System.DBNull.Value){respuesta.acti_AdmiteProforma = Convert.ToBoolean(dr["acti_AdmiteProforma"]);}
						if(dr["acti_AplicaSobreTasaPagoRetrasado"] != System.DBNull.Value){respuesta.acti_AplicaSobreTasaPagoRetrasado = Convert.ToBoolean(dr["acti_AplicaSobreTasaPagoRetrasado"]);}
						if(dr["acti_PermitePagoEnCuotas"] != System.DBNull.Value){respuesta.acti_PermitePagoEnCuotas = Convert.ToBoolean(dr["acti_PermitePagoEnCuotas"]);}
						if(dr["acti_vcto2_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto2_cant_dias = Convert.ToInt32(dr["acti_vcto2_cant_dias"]);}
						if(dr["acti_vcto3_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto3_cant_dias = Convert.ToInt32(dr["acti_vcto3_cant_dias"]);}
						if(dr["acti_vcto4_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto4_cant_dias = Convert.ToInt32(dr["acti_vcto4_cant_dias"]);}
						if(dr["acti_vcto5_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto5_cant_dias = Convert.ToInt32(dr["acti_vcto5_cant_dias"]);}
						if(dr["acti_vcto6_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto6_cant_dias = Convert.ToInt32(dr["acti_vcto6_cant_dias"]);}
						if(dr["acti_vcto7_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto7_cant_dias = Convert.ToInt32(dr["acti_vcto7_cant_dias"]);}
						if(dr["acti_vcto8_cant_dias"] != System.DBNull.Value){respuesta.acti_vcto8_cant_dias = Convert.ToInt32(dr["acti_vcto8_cant_dias"]);}
						break;
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "ActividadDao.GetActividadPorId: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

	}
}
