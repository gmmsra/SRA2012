using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for ComprobantePagoDao.
	/// </summary>
	public class ComprobantePagoDao
	{
		/// <summary>
		/// metodo que crea una nueva fila en la tabla comprob_pagos
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static Int32 CrearComprobantePago(SqlTransaction trans, ComprobantePagosEntity obj)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprob_pagos_alta ");
			pstrProc.Append("@paco_comp_id=");
			if (obj.paco_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_pati_id=");
			if (obj.paco_pati_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_pati_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_mone_id=");
			if (obj.paco_mone_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_mone_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_nume=");
			if (obj.paco_nume.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.paco_nume)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_tarj_id=");
			if (obj.paco_tarj_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_tarj_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_tarj_cuot=");
			if (obj.paco_tarj_cuot != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_tarj_cuot); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_tarj_nume=");
			if (obj.paco_tarj_nume.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.paco_tarj_nume)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_cheq_id=");
			if (obj.paco_cheq_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_cheq_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_impo=");
			if (obj.paco_impo != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.paco_impo.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_audi_user=");
			if (obj.paco_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_audi_user); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_cuba_id=");
			if (obj.paco_cuba_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_cuba_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_orig_impo=");
			if (obj.paco_orig_impo != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.paco_orig_impo.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_dine_id=");
			if (obj.paco_dine_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_dine_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_tarj_autori=");
			if (obj.paco_tarj_autori.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.paco_tarj_autori)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_conc_id=");
			if (obj.paco_conc_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_conc_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_ccos_id=");
			if (obj.paco_ccos_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_ccos_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_tarj_rech_fecha=");
			if (obj.paco_tarj_rech_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.paco_tarj_rech_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_acre_fecha=");
			if (obj.paco_acre_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.paco_acre_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_acre_audi_fech=");
			if (obj.paco_acre_audi_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.paco_acre_audi_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_acre_audi_user=");
			if (obj.paco_acre_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_acre_audi_user); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_acre_apro=");
			pstrProc.Append("null");
			pstrProc.Append(",@paco_tacl_id=");
			if (obj.paco_tacl_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_tacl_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_rtip_id=");
			if (obj.paco_rtip_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_rtip_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_orig_banc_id=");
			if (obj.paco_orig_banc_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_orig_banc_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_orig_banc_suc=");
			if (obj.paco_orig_banc_suc.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.paco_orig_banc_suc)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@cheq_banc_id=");
			if (obj.cheq_banc_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.cheq_banc_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@cheq_nume=");
			if (obj.cheq_nume.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.cheq_nume)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@cheq_teor_depo_fecha=");
			if (obj.cheq_teor_depo_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.cheq_teor_depo_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@cheq_rece_fecha=");
			if (obj.cheq_rece_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.cheq_rece_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@cheq_plaza=");
			pstrProc.Append(obj.cheq_plaza);
			pstrProc.Append(",@cheq_clea_id=");
			if (obj.cheq_clea_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.cheq_clea_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@cheq_chti_id=");
			if (obj.cheq_chti_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.cheq_chti_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_nd_comp_id=");
			if (obj.paco_nd_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.paco_nd_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@paco_acre_depo_fecha=");
			if (obj.paco_acre_depo_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.paco_acre_depo_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
	
			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["paco_id"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ComprobantePagoDao.CrearComprobantePago: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
	}
}
