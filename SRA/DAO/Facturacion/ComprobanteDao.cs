using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for ComprobanteDao.
	/// </summary>
	public class ComprobanteDao 
	{
		/// <summary>
		/// metodo que retorna un comprobante por comp_id
		/// </summary>
		/// <param name="CompId"></param>
		/// <returns>ComprobanteEntity</returns>
		public static ComprobanteEntity GetComprobantePorCompId(Int32 CompId )
		{
			ComprobanteEntity respuesta = new ComprobanteEntity();
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprobantes_consul_CE ");
			pstrProc.Append("@comp_id=");
			pstrProc.Append(CompId);

         	try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["comp_id"] != System.DBNull.Value){respuesta.comp_id  = Convert.ToInt32(dr["comp_id"]);}
						if(dr["comp_fecha"] != System.DBNull.Value){respuesta.comp_fecha  = Convert.ToDateTime(dr["comp_fecha"]);}
						if(dr["comp_clie_id"] != System.DBNull.Value){respuesta.comp_clie_id  = Convert.ToInt32(dr["comp_clie_id"]);}
						if(dr["comp_dh"] != System.DBNull.Value){respuesta.comp_dh  = Convert.ToBoolean(dr["comp_dh"]);}						
						if(dr["comp_neto"] != System.DBNull.Value){respuesta.comp_neto  = Convert.ToDecimal(dr["comp_neto"]);}
						if(dr["comp_neto_soci"] != System.DBNull.Value){respuesta.comp_neto_soci  = Convert.ToDecimal(dr["comp_neto_soci"]);}
						if(dr["comp_neto_ctac"] != System.DBNull.Value){respuesta.comp_neto_ctac  = Convert.ToDecimal(dr["comp_neto_ctac"]);}
						if(dr["comp_cance"] != System.DBNull.Value){respuesta.comp_cance  = Convert.ToBoolean(dr["comp_cance"]);}	
						if(dr["comp_cs"] != System.DBNull.Value){respuesta.comp_cs  = Convert.ToInt32(dr["comp_cs"]);}
						if(dr["comp_coti_id"] != System.DBNull.Value){respuesta.comp_coti_id  = Convert.ToInt32(dr["comp_coti_id"]);}
						if(dr["comp_ingr_fecha"] != System.DBNull.Value){respuesta.comp_ingr_fecha  = Convert.ToDateTime(dr["comp_ingr_fecha"]);}
						if(dr["comp_mone_id"] != System.DBNull.Value){respuesta.comp_mone_id  = Convert.ToInt32(dr["comp_mone_id"]);}
						if(dr["comp_cemi_nume"] != System.DBNull.Value){respuesta.comp_cemi_nume  = Convert.ToInt32(dr["comp_cemi_nume"]);}
						if(dr["comp_nume"] != System.DBNull.Value){respuesta.comp_nume  = Convert.ToInt32(dr["comp_nume"]);}
						if(dr["comp_esta_id"] != System.DBNull.Value){respuesta.comp_esta_id  = Convert.ToInt32(dr["comp_esta_id"]);}
						if(dr["comp_Baja_fecha"] != System.DBNull.Value){respuesta.comp_Baja_fecha  = Convert.ToDateTime(dr["comp_Baja_fecha"]);}
						if(dr["comp_audi_fecha"] != System.DBNull.Value){respuesta.comp_audi_fecha  = Convert.ToDateTime(dr["comp_audi_fecha"]);}
						if(dr["comp_audi_user"] != System.DBNull.Value){respuesta.comp_audi_user  = Convert.ToInt32(dr["comp_audi_user"]);}
						if(dr["comp_compati_1"] != System.DBNull.Value){respuesta.comp_compati_1  = Convert.ToInt32(dr["comp_compati_1"]);}
						if(dr["comp_compati_id"] != System.DBNull.Value){respuesta.comp_compati_id  = Convert.ToInt32(dr["comp_compati_id"]);}
						if(dr["comp_letra"] != System.DBNull.Value){respuesta.comp_letra  = dr["comp_letra"].ToString();}
						if(dr["comp_cpti_id"] != System.DBNull.Value){respuesta.comp_cpti_id  = Convert.ToInt32(dr["comp_cpti_id"]);}
						if(dr["comp_ivar_fecha"] != System.DBNull.Value){respuesta.comp_ivar_fecha  = Convert.ToDateTime(dr["comp_ivar_fecha"]);}
						if(dr["comp_acti_id"] != System.DBNull.Value){respuesta.comp_acti_id  = Convert.ToInt32(dr["comp_acti_id"]);}
						if(dr["comp_impre"] != System.DBNull.Value){respuesta.comp_impre  = Convert.ToBoolean(dr["comp_impre"]);}	
						if(dr["comp_host"] != System.DBNull.Value){respuesta.comp_host  = dr["comp_host"].ToString();}
						if(dr["comp_compati_a"] != System.DBNull.Value){respuesta.comp_compati_a  = dr["comp_compati_a"].ToString();}						
						if(dr["comp_emct_id"] != System.DBNull.Value){respuesta.comp_emct_id  = Convert.ToInt32(dr["comp_emct_id"]);}
						if(dr["comp_modu_id"] != System.DBNull.Value){respuesta.comp_modu_id  = Convert.ToInt32(dr["comp_modu_id"]);}
						if(dr["comp_neto_conc"] != System.DBNull.Value){respuesta.comp_neto_conc  = Convert.ToDecimal(dr["comp_neto_conc"]);}
						if(dr["comp_clie_ivap_id"] != System.DBNull.Value){respuesta.comp_clie_ivap_id  = Convert.ToInt32(dr["comp_clie_ivap_id"]);}
						if(dr["comp_clie_cuit"] != System.DBNull.Value){respuesta.comp_clie_cuit  = Convert.ToInt64(dr["comp_clie_cuit"]);}
						if(dr["acti_nega_ccos_id"] != System.DBNull.Value){respuesta.acti_nega_ccos_id  = Convert.ToInt32(dr["acti_nega_ccos_id"]);}
					}
			}
			catch (Exception ex)
			{
				string Msg = "ComprobanteDao.GetComprobantePorCompId: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}
			return respuesta;
		}

		/// <summary>
		/// Metodo que inserta un comprobante
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearComprobante(SqlTransaction trans, ComprobanteEntity obj)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprobantes_alta_CE ");
			pstrProc.Append("@comp_fecha=");
			pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.comp_fecha).ToString("yyyy/MM/dd")));
			pstrProc.Append(",@comp_clie_id=");
			if (obj.comp_clie_id != _DataBase.IntDBNull() ) { pstrProc.Append(obj.comp_clie_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_dh=");
			pstrProc.Append(obj.comp_dh);
			pstrProc.Append(",@comp_neto=");
			if (obj.comp_neto != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.comp_neto.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_neto_soci=");
			if (obj.comp_neto_soci != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.comp_neto_soci.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_neto_ctac=");
			if (obj.comp_neto_ctac != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.comp_neto_ctac.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_neto_conc=");
			if (obj.comp_neto_conc != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.comp_neto_conc.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_cance=");
			pstrProc.Append(obj.comp_cance);
			pstrProc.Append(",@comp_ingr_fecha=");
			pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.comp_ingr_fecha).ToString("yyyy/MM/dd")));
			pstrProc.Append(",@comp_cs=");
			if (obj.comp_cs != -1) { pstrProc.Append(obj.comp_cs); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_coti_id=");
			if (obj.comp_coti_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_coti_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_mone_id=");
			if (obj.comp_mone_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_mone_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_cemi_nume=");
			if (obj.comp_cemi_nume != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_cemi_nume); } 	else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_nume=");
			if (obj.comp_nume != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_nume); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_esta_id=");
			if (obj.comp_esta_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_esta_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_Baja_fecha=");
			if (obj.comp_Baja_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.comp_Baja_fecha).ToString("yyyy/MM/dd HH:mm:ss")));} 	else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_audi_user=");
			if (obj.comp_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_audi_user); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_compati_1=");
			if (obj.comp_compati_1 != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_compati_1); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_compati_id=");
			if (obj.comp_compati_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_compati_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_letra=");
			if (obj.comp_letra.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.comp_letra)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_cpti_id=");
			if (obj.comp_cpti_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_cpti_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_ivar_fecha=");
			if (obj.comp_ivar_fecha != _DataBase.DateTimeDBNull()) 
			{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.comp_ivar_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_acti_id=");
			if (obj.comp_acti_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_acti_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_impre=");
			pstrProc.Append(obj.comp_impre);
			pstrProc.Append(",@comp_host=");
			if (obj.comp_host.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.comp_host)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_compati_a=");
			if (obj.comp_compati_a.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.comp_compati_a)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_emct_id=");
			if (obj.comp_emct_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_emct_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_modu_id=");
			if (obj.comp_modu_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_modu_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_clie_ivap_id=");
			if (obj.comp_clie_ivap_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_clie_ivap_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_clie_cuit=");
			if (obj.comp_clie_cuit != _DataBase.IntDBNull()) { pstrProc.Append(obj.comp_clie_cuit); } else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["comp_id"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ComprobanteDao.CrearComprobante: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que coloca en comp_nume generado por afip cuando se pidio el cae
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="comp_id"></param>
		/// <param name="comp_nume"></param>
		/// <returns>Int32</returns>
		public static Int32 AsignaCompNumeComprobantes(SqlTransaction trans, string comp_id, string comp_nume, string comp_cemi_nume 
												   	  ,string opcional1, string opcional2)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprobantes_asigna_comp_nume ");
			pstrProc.Append("@comp_id=");
			if (comp_id.Trim().Length > 0) { pstrProc.Append(comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_nume=");
			if (comp_nume.Trim().Length >0) { pstrProc.Append(comp_nume); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@comp_cemi_nume=");
			if (comp_cemi_nume.Trim().Length >0) { pstrProc.Append(comp_cemi_nume); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@opcional1=");
			if (opcional1.Trim().Length >0) { pstrProc.Append(opcional1); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@opcional2=");
			if (opcional2.Trim().Length >0) { pstrProc.Append(opcional2); } else { pstrProc.Append("null"); }
			
			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ComprobanteDao.AsignaCompNumeComprobantes: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}
			return respuesta;
		}
	}
}
