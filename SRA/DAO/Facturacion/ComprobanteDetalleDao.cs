using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for ComprobanteDetalle.
	/// </summary>
	public class ComprobanteDetalleDao
	{
		/// <summary>
		/// metodo que crea una nueva fila en la tabla de comprb_deta
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static Int32 CrearComprobanteDetalle(SqlTransaction trans, ComprobanteDetaEntity obj)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprob_deta_alta ");
			pstrProc.Append("@code_comp_id=");
			if (obj.code_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_conv_id=");
			if (obj.code_conv_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_conv_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_tarj_id=");
			if (obj.code_tarj_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_tarj_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_tarj_cuot=");
			if (obj.code_tarj_cuot != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_tarj_cuot); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_tarj_vtat=");
			pstrProc.Append(obj.code_tarj_vtat);
			pstrProc.Append(",@code_obse=");
			if (obj.code_obse.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.code_obse)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_coti=");
			if (obj.code_coti != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.code_coti.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_pers=");
			pstrProc.Append(obj.code_pers);
			pstrProc.Append(",@code_raza_id=");
			if (obj.code_raza_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_raza_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_impo_ivat_tasa=");
			pstrProc.Append(_DataBase.darFormatoDecimal(obj.code_impo_ivat_tasa.ToString()));
			pstrProc.Append(",@code_impo_ivat_tasa_redu=");
			pstrProc.Append(_DataBase.darFormatoDecimal(obj.code_impo_ivat_tasa_redu.ToString()));
			pstrProc.Append(",@code_impo_ivat_tasa_sobre=");
			pstrProc.Append(_DataBase.darFormatoDecimal(obj.code_impo_ivat_tasa_sobre.ToString()));
			pstrProc.Append(",@code_impo_ivat_perc=");
			pstrProc.Append(_DataBase.darFormatoDecimal(obj.code_impo_ivat_perc.ToString()));
			pstrProc.Append(",@code_reci_nyap=");
			if (obj.code_reci_nyap.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.code_reci_nyap)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_asoc_comp_id=");
			if (obj.code_asoc_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_asoc_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_sistgen=");
			if (obj.code_sistgen != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_sistgen); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_opergen=");
			if (obj.code_opergen != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_opergen); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_insc_id=");
			if (obj.code_insc_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_insc_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_expo_id=");
			if (obj.code_expo_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_expo_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_tacl_id=");
			if (obj.code_tacl_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_tacl_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_tarj_nume=");
			if (obj.code_tarj_nume.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.code_tarj_nume)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_audi_user=");
			if (obj.code_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_audi_user); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_cria_id=");
			if (obj.code_cria_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_cria_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@code_peli_id=");
			if (obj.code_peli_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.code_peli_id); } else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ComprobanteDetalleDao.CrearComprobanteDetalle: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
	}
}
