using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for ComprobanteConcepto.
	/// </summary>
	public class ComprobanteConceptoDao
	{
		/// <summary>
		/// metodo que crea una nueva fila en la tabla comprob_conep
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static Int32 CrearComprobanteConcepto(SqlTransaction trans, ComprobanteConcepEntity obj)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec comprob_concep_alta ");
			pstrProc.Append("@coco_comp_id=");
			if (obj.coco_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coco_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_conc_id=");
			if (obj.coco_conc_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coco_conc_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_impo=");
			if (obj.coco_impo != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coco_impo.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_ccos_id=");
			if (obj.coco_ccos_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coco_ccos_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_impo_ivai=");
			if (obj.coco_impo_ivai != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coco_impo_ivai.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_desc_ampl=");
			if (obj.coco_desc_ampl.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.coco_desc_ampl)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_porc=");
			if (obj.coco_porc != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coco_porc.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_tasa_iva=");
			if (obj.coco_tasa_iva != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coco_tasa_iva.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_auto=");
			pstrProc.Append(obj.coco_auto);
			pstrProc.Append(",@coco_audi_user=");
			if (obj.coco_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.coco_audi_user); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coco_prco_id=");
			if (obj.coco_prco_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coco_prco_id); } else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["coco_id"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ComprobanteConceptoDao.CrearComprobanteConcepto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
	}
}
