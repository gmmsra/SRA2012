using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using Entities;
using System.Data.SqlClient;

namespace DAO.Facturacion
{
	/// <summary>
	/// Summary description for FacturacionDao.
	/// </summary>
	public class FacturacionDao
	{

		/// <summary>
		/// metodo que recupera una lista de vtos por actividad
		/// </summary> 
		/// <param name="trans"></param>
		/// <param name="IdActividad"></param>
		/// <returns>int[]</returns>
		public static int[] GetDiasSobreTasaVtosPorIdActividad(SqlTransaction trans, int IdActividad)
		{
			DataSet ds = null;
			int[] respuesta = new int[0];
			int index = 0;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_GetDiasSobreTasaVtosPorIdActividad ");
			pstrProc.Append("@IdActividad=");
			pstrProc.Append(IdActividad); 
            
			try
			{
				ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());
					
				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					// declaro las dimeciones del vector segun los datos recuperados de la base
					respuesta = new int[ds.Tables[0].Rows.Count];

					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						// si viene valor informado cargo el valor en el vector y incremento
                        // el indexador en 1
						if(dr["intCantDias"] != System.DBNull.Value){respuesta[index++] = Convert.ToInt32(dr["intCantDias"]);}
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.GetDiasSobreTasaVtosPorIdActividad: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}


		/// <summary>
		/// metodo que crea una fila en la tabla COMPROB_Sobre_Tasa_Vtos
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearComprobanteSobreTasaVtos(SqlTransaction trans, ComprobanteSobreTasaVtosEntity obj)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_CrearComprobanteSobreTasaVtos ");
			pstrProc.Append("@coso_aran_id=");
			if (obj.coso_aran_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_aran_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_conc_id=");
			if (obj.coso_conc_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_conc_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_comp_id=");
			if (obj.coso_comp_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_comp_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_cant=");
			if (obj.coso_cant != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_cant); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_anim_no_fact=");
			if (obj.coso_anim_no_fact != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_anim_no_fact); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_unit_impo=");
			if (obj.coso_unit_impo != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coso_unit_impo.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_fecha=");
			if (obj.coso_fecha != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.coso_fecha).ToString("yyyy/MM/dd"))); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_fecha_VtoAplica=");
			if (obj.coso_fecha_VtoAplica != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.coso_fecha_VtoAplica).ToString("yyyy/MM/dd"))); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_inic_rp=");
			if (obj.coso_inic_rp.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.coso_inic_rp)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_fina_rp=");
			if (obj.coso_fina_rp.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.coso_fina_rp)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_tram=");
			if (obj.coso_tram != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_tram); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_sin_carg="); 
			pstrProc.Append(obj.coso_sin_carg);
			pstrProc.Append(",@coso_exen="); 
			pstrProc.Append(obj.coso_exen);
			pstrProc.Append(",@coso_impo=");
			if (obj.coso_impo != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coso_impo.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_tasa_iva=");
			if (obj.coso_tasa_iva != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coso_tasa_iva.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_impo_ivai=");
			if (obj.coso_impo_ivai != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coso_impo_ivai.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_audi_fecha=");
			if (obj.coso_audi_fecha != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.coso_audi_fecha).ToString("yyyy/MM/dd HH:mm:ss"))); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_audi_user=");
			if (obj.coso_audi_user != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_audi_user); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_ccos_id=");
			if (obj.coso_ccos_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_ccos_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_cant_sin_carg=");
			if (obj.coso_cant_sin_carg != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_cant_sin_carg); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_acta=");
			if (obj.coso_acta.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.coso_acta)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_pran_id=");
			if (obj.coso_pran_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_pran_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_impo_iva=");
			if (obj.coso_impo_iva != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coso_impo_iva.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_rrgg_aran_id=");
			if (obj.coso_rrgg_aran_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_rrgg_aran_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_auto="); 
			pstrProc.Append(obj.coso_auto);
			pstrProc.Append(",@coso_desc_ampl=");
			if (obj.coso_desc_ampl.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.coso_desc_ampl)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_coco_porc=");
			if (obj.coso_coco_porc != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(obj.coso_coco_porc.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_coco_prco_id=");
			if (obj.coso_coco_prco_id != _DataBase.IntDBNull()) { pstrProc.Append(obj.coso_coco_prco_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@coso_RegOriginal="); 
			pstrProc.Append(obj.coso_RegOriginal);

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.CrearComprobanteSobreTasaVtos: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que clona las sobretasas originales del comprobante si existen 
		/// para el nuevo vencimiento con los importes con signo negativo
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="FechaVto"></param>
		/// <param name="IdComprobante"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearSobreTasasOriginalesEnNuevoVto(SqlTransaction trans, DateTime FechaVto, Int32 IdComprobante)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_CrearSobreTasasOriginalesEnNuevoVto ");
			pstrProc.Append("@IdComprobante=");
			if (IdComprobante != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobante); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@FechaVto=");
			if (FechaVto != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)FechaVto).ToString("yyyy/MM/dd"))); } 	else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.CrearSobreTasasOriginalesEnNuevoVto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}


		/// <summary>
		/// metodo que clona los conceptos originales del comprobante si existen 
		/// para el nuevo vencimiento con los importes con signo negativo
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="FechaVto"></param>
		/// <param name="IdComprobante"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearConceptosOriginalesEnNuevoVto(SqlTransaction trans, DateTime FechaVto, Int32 IdComprobante)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_CrearConceptosOriginalesEnNuevoVto ");
			pstrProc.Append("@IdComprobante=");
			if (IdComprobante != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobante); } 
			else { pstrProc.Append("null"); }
			pstrProc.Append(",@FechaVto=");
			if (FechaVto != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)FechaVto).ToString("yyyy/MM/dd"))); } 	
			else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.CrearConceptosOriginalesEnNuevoVto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que crea las filas en comprob_aran desde las sobretasas calculadas
		/// para una fecha determinada de pago a futuro, son tomadas de la tabla COMPROB_Sobre_Tasa_Vtos
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="FechaPago"></param>
		/// <param name="IdComprobante"></param>
		/// <param name="IdUsuario"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearArancelesDeSobreTasasEnNuevoVto(SqlTransaction trans, DateTime FechaPago, Int32 IdComprobante
																, Int32 IdUsuario, Int32 IdComprobanteNew)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_CrearArancelesDeSobreTasasEnNuevoVto ");
			pstrProc.Append("@IdComprobante=");
			if (IdComprobante != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobante); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@Fecha=");
			if (FechaPago != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)FechaPago).ToString("yyyy/MM/dd"))); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdUsuario=");
			if (IdUsuario != _DataBase.IntDBNull()) { pstrProc.Append(IdUsuario); }	else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdComprobanteNew=");
			if (IdComprobanteNew != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobanteNew); } else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.CrearArancelesDeSobreTasasEnNuevoVto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que recupera si la raza ingresada se registra en la sra
		/// </summary>
		/// <param name="Raza"></param>
		/// <param name="Criador"></param>
		/// <returns>bool</returns>
		public static bool GetRazaInscribeEnSRA(string pstrArgs)
		{
			bool respuesta = false;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_GetRazaInscribeEnSRA " + pstrArgs);

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToBoolean(ds.Tables[0].Rows[0]["respuesta"]);
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.GetRazaInscribeEnSRA: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que recupera datos de la consulta del panel de datos de laboratorio para bonificacion
		/// </summary>
		/// <param name="IdActividad"></param>
		/// <param name="IdCliente"></param>
		/// <param name="IdArancel"></param>
		/// <param name="FechaValor"></param>
		/// <returns>ResultPanelLabBonif</returns>
		public static ResultPanelLabBonif GetDatosPanelLaboratorioAcumEspecie( int IdActividad , int IdCliente, int IdArancel, DateTime FechaValor)
		{
			ResultPanelLabBonif respuesta = new ResultPanelLabBonif();
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_GetAnalisisAcumPorActividadClienteEspecie ");
			pstrProc.Append("@IdActividad=");
			if (IdActividad != _DataBase.IntDBNull()) { pstrProc.Append(IdActividad); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdCliente=");
			if (IdCliente != _DataBase.IntDBNull()) { pstrProc.Append(IdCliente); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdArancel=");
			if (IdArancel != _DataBase.IntDBNull()) { pstrProc.Append(IdArancel); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@FechaValor=");
			if (FechaValor != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)FechaValor).ToString("yyyy/MM/dd"))); } 	else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					respuesta.TituAcum = ds.Tables[0].Rows[0]["TituAcum"].ToString();
					if(ds.Tables[0].Rows[0]["Cantidad"] != System.DBNull.Value){respuesta.Cantidad  = Convert.ToInt32(ds.Tables[0].Rows[0]["Cantidad"]);}
					if(ds.Tables[0].Rows[0]["IdEspecie"] != System.DBNull.Value){respuesta.IdEspecie  = Convert.ToInt32(ds.Tables[0].Rows[0]["IdEspecie"]);}
					if(ds.Tables[0].Rows[0]["fechaDesde"] != System.DBNull.Value){respuesta.fechaDesde  = Convert.ToDateTime(ds.Tables[0].Rows[0]["fechaDesde"]);}
					if(ds.Tables[0].Rows[0]["fechaHasta"] != System.DBNull.Value){respuesta.fechaHasta  = Convert.ToDateTime(ds.Tables[0].Rows[0]["fechaHasta"]);}
					if(ds.Tables[0].Rows[0]["hoy"] != System.DBNull.Value){respuesta.hoy  = Convert.ToDateTime(ds.Tables[0].Rows[0]["hoy"]);}
					if(ds.Tables[0].Rows[0]["varEspecie"] != System.DBNull.Value){respuesta.varEspecie  = ds.Tables[0].Rows[0]["varEspecie"].ToString();}
					if(ds.Tables[0].Rows[0]["varFacturasBonifLab"] != System.DBNull.Value){respuesta.varFacturasBonifLab  = ds.Tables[0].Rows[0]["varFacturasBonifLab"].ToString();} else {respuesta.varFacturasBonifLab = String.Empty;}
				}
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.GetDatosPanelLaboratorioAcumEspecie: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que crea las filas en comprob_concep desde las sobretasas calculadas
		/// para una fecha determinada de pago a futuro, son tomadas de la tabla COMPROB_Sobre_Tasa_Vtos
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="FechaPago"></param>
		/// <param name="IdComprobante"></param>
		/// <param name="IdUsuario"></param>
		/// <param name="IdComprobanteNew"></param>
		/// <returns>Int32</returns>
		public static Int32 CrearConceptosDeSobreTasasEnNuevoVto(SqlTransaction trans, DateTime FechaPago, Int32 IdComprobante
			, Int32 IdUsuario, Int32 IdComprobanteNew)
		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_CrearConceptosDeSobreTasasEnNuevoVto ");
			pstrProc.Append("@IdComprobante=");
			if (IdComprobante != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobante); } 
			else { pstrProc.Append("null"); }
			pstrProc.Append(",@Fecha=");
			if (FechaPago != _DataBase.DateTimeDBNull()) { pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)FechaPago).ToString("yyyy/MM/dd"))); } 
			else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdUsuario=");
			if (IdUsuario != _DataBase.IntDBNull()) { pstrProc.Append(IdUsuario); }	
			else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdComprobanteNew=");
			if (IdComprobanteNew != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobanteNew); } 
			else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.CrearConceptosDeSobreTasasEnNuevoVto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}


		/// <summary>
		/// metodo que registra en un concepto recargo o interes
		/// para nota de debito de pago mis cuentas si corresponde
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="IdComprobante"></param>
		/// <param name="IdUsuario"></param>
		/// <param name="IdComprobanteNew"></param>
		/// <param name="impNotaDebito"></param>
		/// <returns></returns>
		public static Int32 CrearConceptosDeRecargoAdminOInteres( SqlTransaction trans, Int32 IdComprobante
																, Int32 IdUsuario, Int32 IdComprobanteNew
																, decimal impNotaDebito)

		{
			Int32 respuesta = -1;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_CrearConceptosDeRecargoAdminOInteres ");
			pstrProc.Append("@IdComprobante=");
			if (IdComprobante != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobante); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdUsuario=");
			if (IdUsuario != _DataBase.IntDBNull()) { pstrProc.Append(IdUsuario); }	else { pstrProc.Append("null"); }
			pstrProc.Append(",@IdComprobanteNew=");
			if (IdComprobanteNew != _DataBase.IntDBNull()) { pstrProc.Append(IdComprobanteNew); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@impNotaDebito=");
			if (impNotaDebito != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(impNotaDebito.ToString())); } else { pstrProc.Append("null"); }

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"].ToString());
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.CrearConceptosDeRecargoAdminOInteres: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}


		/// <summary>
		/// metodo que rertona los parametros para la bonificacion de laboratorio concepto automatico
		/// </summary>
		/// <param name="IdRaza"></param>
		/// <returns>ParametosBonifLabEntity</returns>
		public static ParametosBonifLabEntity GetParametosBonifLaboratorio(int IdRaza)
		{
			ParametosBonifLabEntity respuesta = new ParametosBonifLabEntity();

			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			pstrProc.Append("exec FAC_GetParametosBonifLab ");
			pstrProc.Append("@IdRaza=");
			pstrProc.Append(IdRaza);
			
			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["intIdEspecie"] != System.DBNull.Value){respuesta.intIdEspecie  = Convert.ToInt32(dr["intIdEspecie"]);}
						if(dr["intIdConceptoBonifLab"] != System.DBNull.Value){respuesta.intIdConceptoBonifLab  = Convert.ToInt32(dr["intIdConceptoBonifLab"]);}
						if(dr["varCodigoConcepto"] != System.DBNull.Value){respuesta.varCodigoConcepto  = dr["varCodigoConcepto"].ToString();}
						if(dr["varConceptoBonifLab"] != System.DBNull.Value){respuesta.varConceptoBonifLab  = dr["varConceptoBonifLab"].ToString();}
						if(dr["intIdCuentaContable"] != System.DBNull.Value){respuesta.intIdCuentaContable  = Convert.ToInt32(dr["intIdCuentaContable"]);}
						if(dr["decPorcBonifPorCantidad"] != System.DBNull.Value){respuesta.decPorcBonifPorCantidad  = Convert.ToDecimal(dr["decPorcBonifPorCantidad"]);}
						if(dr["intCantABonificar"] != System.DBNull.Value){respuesta.intCantABonificar  = Convert.ToInt32(dr["intCantABonificar"]);}
						if(dr["decPorcBonifPorCantidad2"] != System.DBNull.Value){respuesta.decPorcBonifPorCantidad2  = Convert.ToDecimal(dr["decPorcBonifPorCantidad2"]);}
						if(dr["intCantABonificar2"] != System.DBNull.Value){respuesta.intCantABonificar2  = Convert.ToInt32(dr["intCantABonificar2"]);}
						if(dr["intCCosConcepBonifLab"] != System.DBNull.Value){respuesta.intCCosConcepBonifLab  = Convert.ToInt32(dr["intCCosConcepBonifLab"]);}
						if(dr["varCCosConcepBonifLab"] != System.DBNull.Value){respuesta.varCCosConcepBonifLab  = dr["varCCosConcepBonifLab"].ToString();}
					}
			}
			catch (Exception ex)
			{
				string Msg = "FacturacionDao.GetParametosBonifLaboratorio: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}
	}
}
