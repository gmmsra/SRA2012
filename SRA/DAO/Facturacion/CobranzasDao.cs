using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using Entities;
using System.Data.SqlClient;


namespace DAO.CobranzasDao
{
	/// <summary>
	/// Dario 2013-05-29
	/// Summary description for FacturacionDao.
	/// clase que se utiliza para los metodos de acceso a dato de las funcipnes de facturacion
	/// </summary>
	public class CobranzasDao
	{
		/// <summary>
		/// Dario 2013-05-30
		/// metodo que se usa mostrar en la grilla de la pagina
		/// CobranzasRecepcionPagosMisCuentas.aspx
		/// </summary>
		/// <param name="Usuario"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetCobranzasARecepcionar(string Usuario, int IdEncabezado)
		{
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_CE_GetCobranzasARecepcionar ");
			pstrProc.Append("@IdEncabezado=");
			pstrProc.Append(IdEncabezado); 
            
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.GetCobranzasARecepcionar: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return ds;
		}


		/// <summary>
		/// metodo que reporta la respuesta de la consulta de cobranzas vs facturacion
		/// </summary>
		/// <param name="dateFechahasta"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetDeudaPorActividadCliente(DateTime dateFechahasta)
		{
			DataBase _DataBase = new DataBase();
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			if(dateFechahasta == _DataBase.DateTimeDBNull())
			{
				dateFechahasta = System.DateTime.Now;
			}

			pstrProc.Append("exec rpt_Cobranzas_Versus_Facturacion_Agrupado ");
			pstrProc.Append("@fecha_Hasta=");
			pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)dateFechahasta).ToString("yyyy/MM/dd"))); 	

			try
			{
				ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn , pstrProc.ToString(), 9999);
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.GetDeudaPorActividadCliente: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return ds;
		}

		
		/// <summary>
		/// sobrecarga del metodo GetEncabezadoCobranzasARecepcionar 
		/// </summary>
		/// <param name="IdEmpCobElect"></param>
		/// <returns>CobranzasEntity</returns>
		public static CobranzasCabEntity GetEncabezadoCobranzasARecepcionar(int IdEmpCobElect)
		{
			CobranzasCabEntity respuesta = new CobranzasCabEntity();
			// clase base
			DataBase oBase = new DataBase();
			// abro la coneccion
			SqlConnection myConnectionSQL = new SqlConnection(oBase.mstrConn);
			myConnectionSQL.Open();
			// obtengo una transaccion
			SqlTransaction trans = myConnectionSQL.BeginTransaction(IsolationLevel.ReadCommitted);
			try
			{
				respuesta = CobranzasDao.GetEncabezadoCobranzasARecepcionar(trans, IdEmpCobElect);
			}
			catch (Exception ex)
			{
				trans.Rollback();
				throw new Exception(ex.Message.ToString());
			}
			finally
			{
				trans.Dispose();
				myConnectionSQL.Close();
			}

			return respuesta;
		}


		/// <summary>
		/// metodo que recupera el encabezado para antiguo para una empresa cobradora
		/// que se tenga que recepcionar
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="IdEmpCobElect"></param>
		/// <returns>CobranzasCabEntity</returns>
		public static CobranzasCabEntity GetEncabezadoCobranzasARecepcionar(SqlTransaction trans, int IdEmpCobElect)
		{
			DataSet ds = null;
			CobranzasCabEntity respuesta = new CobranzasCabEntity();
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_CE_GetEncabezadoCobranzasARecepcionar ");
			pstrProc.Append("@IdEmpCobElect=");
			pstrProc.Append(IdEmpCobElect); 
            
			try
			{
				ds = clsSQLServer.gExecuteQuery(trans, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["intIdInterCobElecCab"] != System.DBNull.Value){respuesta.intIdInterCobElecCab = Convert.ToInt32(dr["intIdInterCobElecCab"]);}
						if(dr["varNroComprobante"] != System.DBNull.Value){respuesta.varNroComprobante = dr["varNroComprobante"].ToString();}
						if(dr["decRecaudacion"] != System.DBNull.Value){respuesta.decRecaudacion = Convert.ToDecimal(dr["decRecaudacion"]);}
						if(dr["decRecaudacionNeta"] != System.DBNull.Value){respuesta.decRecaudacionNeta = Convert.ToDecimal(dr["decRecaudacionNeta"]);}
						if(dr["intCantidadDeNovedades"] != System.DBNull.Value){respuesta.intCantidadDeNovedades = Convert.ToInt32(dr["intCantidadDeNovedades"]);}
						if(dr["dateFechaAcreditacion"] != System.DBNull.Value){respuesta.dateFechaAcreditacion =  Convert.ToDateTime(dr["dateFechaAcreditacion"]);}
						if(dr["intIdEmpresaCobElec"] != System.DBNull.Value){respuesta.intIdEmpresaCobElec = Convert.ToInt32(dr["intIdEmpresaCobElec"]);}
						if(dr["dateFechaProceso"] != System.DBNull.Value){respuesta.dateFechaProceso = Convert.ToDateTime(dr["dateFechaProceso"]);}
						if(dr["varDesEmpresaCobElect"] != System.DBNull.Value){respuesta.varDesEmpresaCobElect = dr["varDesEmpresaCobElect"].ToString();}
						
						break;
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.GetEncabezadoCobranzasARecepcionar: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		
		/// <summary>
		/// metodo que retorna la listra de empresas cobradoras electronicas
		/// </summary>
		/// <param name="Usuario"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetEmpresasCobranzasElectronicas(string Usuario)
		{
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec SP_FAC_CE_GetEmpresasCobranzasElectronicas ");
            
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.GetEmpresasCobranzasElectronicas: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return ds;
		}

		
		/// <summary>
		/// metodo que se utiliza para registrar en la tabla de recibos de pagos mis cuentas
		/// el nro de comprobante generado con el pago ingormado por PmC
		/// </summary>
		/// <param name="IdPMCTASCob"></param>
		/// <param name="IdComp"></param>
		/// <returns>int</returns>
		public static int SetIdComprobanteToCobranzasElectDet(Int32 intIdInterCobElecDet , Int32 IdComp)
		{
			int respuesta = -1;
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_CE_SetIdComprobanteToCobranzasElectDet ");
			pstrProc.Append("@intIdInterCobElecDet=");
			pstrProc.Append(intIdInterCobElecDet); 
			pstrProc.Append(",@intNroComprobante=");
			pstrProc.Append(IdComp); 
            
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
					respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"]);
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.SetIdComprobanteToCobranzasElectDet: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}


		public static int ModifEncabezadoCobranzasElectronicas(SqlTransaction transPpal, CobranzasCabEntity objCobranzasCab)
		{
			int respuesta = -1;
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec FAC_CE_ModifEncabezadoCobranzasElectronicas ");
			pstrProc.Append("@intIdInterCobElecCab=");
			pstrProc.Append(objCobranzasCab.intIdInterCobElecCab); 
			pstrProc.Append(",@varNroComprobante=");
			if (objCobranzasCab.varNroComprobante.Trim().Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(objCobranzasCab.varNroComprobante)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@decRecaudacion=");
			if (objCobranzasCab.decRecaudacion != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(objCobranzasCab.decRecaudacion.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@decRecaudacionNeta=");
			if (objCobranzasCab.decRecaudacionNeta != _DataBase.DecimalDBnull()) { pstrProc.Append(_DataBase.darFormatoDecimal(objCobranzasCab.decRecaudacionNeta.ToString())); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@intCantidadDeNovedades=");
			if (objCobranzasCab.intCantidadDeNovedades != _DataBase.IntDBNull()) { pstrProc.Append(objCobranzasCab.intCantidadDeNovedades); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@dateFechaAcreditacion=");
			if (objCobranzasCab.dateFechaAcreditacion != _DataBase.DateTimeDBNull())	{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)objCobranzasCab.dateFechaAcreditacion).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@intIdEmpresaCobElec=");
			if (objCobranzasCab.intIdEmpresaCobElec != _DataBase.IntDBNull()) { pstrProc.Append(objCobranzasCab.intIdEmpresaCobElec); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@dateFechaProceso=");
			if (objCobranzasCab.dateFechaProceso != _DataBase.DateTimeDBNull())	{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)objCobranzasCab.dateFechaProceso).ToString("yyyy/MM/dd HH:mm:ss")));} else { pstrProc.Append("null"); }

			try
			{
				ds = clsSQLServer.gExecuteQuery(transPpal , pstrProc.ToString());
				respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"]);
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.ModifEncabezadoCobranzasElectronicas: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
		
		/// <summary>
		/// metodo que busca un comprobante vto por covt_id
		/// </summary>
		/// <param name="covt_id"></param>
		/// <returns>ComprobanteVtosEntity</returns>
		public static ComprobanteVtosEntity GetComprobanteVtosPorCoVtId(int covt_id)
		{
			DataSet ds = null;
			ComprobanteVtosEntity respuesta = new ComprobanteVtosEntity();
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec comprob_vtos_consul ");
			pstrProc.Append("@covt_id=");
			pstrProc.Append(covt_id); 
            
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						respuesta.covt_id = Convert.ToInt32(dr["covt_id"]);
						respuesta.covt_comp_id = Convert.ToInt32(dr["covt_comp_id"]);
						respuesta.covt_fecha = Convert.ToDateTime(dr["covt_fecha"]);
						respuesta.covt_impo = Convert.ToDecimal(dr["covt_impo"]);
						respuesta.covt_porc = Convert.ToDecimal(dr["covt_porc"]);
						respuesta.covt_cance = Convert.ToBoolean(dr["covt_cance"]);
						respuesta.covt_audi_fecha = Convert.ToDateTime(dr["covt_audi_fecha"]);
						respuesta.covt_audi_user =  Convert.ToInt32(dr["covt_audi_user"]);
						respuesta.covt_compati_1 = -1;
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.GetComprobanteVtosPorCoVtId: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		
		/// <summary>
		/// metodo que retorna la ultima cotizacion de dolar
		/// </summary>
		/// <returns></returns>
		public static decimal GetUltimaCotizacionDolar()
		{
			DataSet ds = null;
			decimal respuesta = -1;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec dbo.cotiza_monedas_consul @como_mone_id = 2");
			
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				    respuesta = Convert.ToDecimal(ds.Tables[0].Rows[0]["como_impo_cotiza"]);
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.GetUltimaCotizacionDolar: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		
		/// <summary>
		/// metodo que recupera la lista de comprobantes para 
		/// armar el detalle del mail de alerta del ingreso bancario 
		/// </summary>
		/// <param name="listIdComprobante"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetComprobantesAInformarAlertaMail(string listIdComprobante)
		{
			DataSet ds = null;

			DataBase _DataBase = new DataBase();

			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_CE_GetComprobantesAInformarAlertaMail ");
			pstrProc.Append("@listIdComprobante=");
			pstrProc.Append(_DataBase.darFormatoSQl(listIdComprobante));

			try
			{
				ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn , pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "CobranzasDao.GetComprobantesAInformarAlertaMail: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return ds;
		}

	}
}
