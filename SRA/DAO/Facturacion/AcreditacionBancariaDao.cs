using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using Entities;
using System.Data.SqlClient;


namespace DAO.AcreditacionBancariaDao
{
	/// <summary>
	/// Dario 2013-05-29
	/// Summary description for AcreditacionBancariaDao.
	/// clase que se utiliza para los metodos de acceso a dato de las funciones de las acreditaciones 
	/// </summary>
	public class AcreditacionBancariaDao
	{
		/// <summary>
		/// metodo que recupera la lista de comprobantes para 
		/// armar el detalle del mail de alerta de la confirmacion de ingreso acridictacion 
		/// </summary>
		/// <param name="listIdComprobante"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetAcreditacionesBancariasAInformarAlertaMail(string listIdComprobante)
		{
			DataSet ds = null;

			DataBase _DataBase = new DataBase();

			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_GetAcreditacionesBancariasAInformarAlertaMail ");
			pstrProc.Append("@listIdComprobante=");
			pstrProc.Append(_DataBase.darFormatoSQl(listIdComprobante));

			try
			{
				ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn , pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "AcreditacionBancariaDao.GetAcreditacionesBancariasAInformarAlertaMail: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return ds;
		}

	}
}
