using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities.Facturacion;
using Entities;
using System.Data.SqlClient;

namespace DAO.Laboratorio
{
	/// <summary>
	/// Summary description for LaboratorioDao.
	/// </summary>
	public class LaboratorioDao 
	{
		/// <summary>
		/// metodo que retorna un dataset con los datos de analisis por cliente especie
		/// para el pop up de facturacion
		/// </summary>
		/// <param name="IdCliente"></param>
		/// <returns>DataSet</returns>
		public static DataSet GetAllAnalisisAcumPorActividadCliente(int IdActividad , int IdCliente, DateTime Fecha)
		{
			DataSet ds = null;
			
			DataBase _DataBase = new DataBase();

			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_GetAllAnalisisAcumPorActividadCliente ");
			pstrProc.Append("@IdActividad=");
			pstrProc.Append(IdActividad); 
			pstrProc.Append(",@IdCliente=");
			pstrProc.Append(IdCliente); 
			pstrProc.Append(",@FechaValor=");
			if (Fecha != _DataBase.DateTimeDBNull()){pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)Fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
            
			try
			{
				ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn , pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "LaboratorioDao.GetAllAnalisisAcumPorActividadCliente: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return ds;
		}

	}
}
