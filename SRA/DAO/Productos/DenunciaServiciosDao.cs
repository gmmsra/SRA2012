using System;
using System.Text;
using DAO;
using System.Data;
using AccesoBD;
using System.Data.SqlClient;
using Entities;

namespace DAO.Productos
{
	/// <summary>
	/// Descripci�n breve de DenunciaServiciosDao.
	/// </summary>
	public class DenunciaServiciosDao
	{

		/// <summary>
		/// Inserta en: rg_servi_denuncias. (Cabecera)
		/// </summary>
		/// <param name="objPreInscripcion"></param>
		/// <returns>Devuelve true si insert� correctamente.</returns>
		public static Boolean InsertarCabecera(SqlTransaction transPpal, DenunciaServicioEntity objServicio)
		{
			StringBuilder pstrProc = new StringBuilder();
			Boolean blnOK = false;

			try
			{
				pstrProc.Append("exec p_RG_servi_denuncias_actu ");

				//ID.
				pstrProc.Append("@sede_id=");
				pstrProc.Append(objServicio.sede_id.ToString());

				//Fecha Proceso.
				pstrProc.Append(",@sede_fecha=");
				pstrProc.Append((new DataBase()).darFormatoSQl((Convert.ToDateTime(objServicio.sede_fecha)).ToString("yyyyMMdd")));

				//Fecha Recepci�n.
				pstrProc.Append(",@sede_presen_fecha=");
				pstrProc.Append((new DataBase()).darFormatoSQl((Convert.ToDateTime(objServicio.sede_presen_fecha)).ToString("yyyyMMdd")));

				//Raza.
				pstrProc.Append(",@sede_raza_id=");
				pstrProc.Append(objServicio.sede_raza_id.ToString());

				//Criador.
				pstrProc.Append(",@sede_cria_id=");
				if (objServicio.sede_cria_id != -1) { pstrProc.Append(objServicio.sede_cria_id.ToString()); } else { pstrProc.Append("null"); }

				//Folio.
				pstrProc.Append(",@sede_folio_nume=");
				if (objServicio.sede_folio_nume != -1) { pstrProc.Append(objServicio.sede_folio_nume.ToString()); } else { pstrProc.Append("null"); }

				//Especie.
				pstrProc.Append(",@sede_espe_id=");
				if (objServicio.sede_espe_id != -1 ) { pstrProc.Append(objServicio.sede_espe_id.ToString()); } else { pstrProc.Append("null"); }

				//N�mero Control.
				pstrProc.Append(",@sede_nro_control=");
				if (objServicio.sede_nro_control != -1) { pstrProc.Append(objServicio.sede_nro_control.ToString()); } else { pstrProc.Append("null"); }

				//Usuario.
				pstrProc.Append(",@sede_audi_user=");
				pstrProc.Append(objServicio.sede_audi_user); // usuariop

				//Ejecuto el Query.
				DataSet ds = clsSQLServer.gExecuteQuery(transPpal, pstrProc.ToString());

				//Si trae datos el DataSet, insert� correctamente y devuelve true.
				//Caso contrario, devuelve false.
				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					objServicio.sede_id = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
					blnOK = true;
				}
			}
			catch (Exception ex)
			{
				string Msg = "DenunciaServiciosDao.InsertarCabecera: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}

			return blnOK;
		}

		/// <summary>
		/// Inserta en: rg_servi_denuncias_deta. (Detalle)
		/// </summary>
		/// <param name="objPreInscripcion"></param>
		/// <returns>Devuelve true si insert� correctamente.</returns>
		public static Boolean InsertaBorraModificaDetalle(SqlTransaction transPpal, DenunciaServicioEntity objServicio)
		{
			StringBuilder pstrProc = new StringBuilder();
			Boolean blnOK = false;

			try
			{
				foreach (DenunciaServicioDetalleEntity item in objServicio.LstDetallesPlanilla)
				{
					// Verifico si es Borrar. Si lo es, borro el registro en cuesti�n.
					if (item.IsDelete)
					{
						pstrProc = new StringBuilder();
						pstrProc.Append("exec p_RG_servi_denuncias_baja ");

						//ID.
						pstrProc.Append("@sdde_id=");
						pstrProc.Append(item.sdde_id.ToString());

						//Ejecuto el Query.
						DataSet ds = clsSQLServer.gExecuteQuery(transPpal, pstrProc.ToString(), 9999);

						if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
						{
							blnOK = true;
						}
					}
					else
					{
						// Verifico si es Editado o si el ID es < 0 (Nuevo). Si se cumple, inserto el registro.
						if (item.IsEdit || item.sdde_id < 0)
						{
							pstrProc = new StringBuilder();
							pstrProc.Append("exec p_RG_servi_denuncias_deta_actu ");

							// ID. Si es mayor que cero, es Update, sino le paso 0 y es Insert.
							pstrProc.Append("@sdde_id=");
							if (item.sdde_id > 0) { pstrProc.Append(item.sdde_id.ToString()); } 
							else { pstrProc.Append("0"); }

							// ID Cabecera.
							pstrProc.Append(",@sdde_sede_id=");
							if (objServicio.sede_id != -1) { pstrProc.Append(objServicio.sede_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// L�nea.
							pstrProc.Append(",@sdde_linea_nume=");
							if (item.sdde_linea_nume != -1) { pstrProc.Append(item.sdde_linea_nume.ToString()); } 
							else { pstrProc.Append("null"); }

							// Fecha Entrada.
							pstrProc.Append(",@sdde_servi_desde=");
							if (item.sdde_servi_desde != System.DateTime.MinValue) { pstrProc.Append((new DataBase()).darFormatoSQl((Convert.ToDateTime(item.sdde_servi_desde)).ToString("yyyyMMdd"))); } 
							else { pstrProc.Append("null"); }

							// Fecha Salida.
							pstrProc.Append(",@sdde_servi_hasta=");
							if (item.sdde_servi_hasta != System.DateTime.MinValue) { pstrProc.Append((new DataBase()).darFormatoSQl((Convert.ToDateTime(item.sdde_servi_hasta)).ToString("yyyyMMdd"))); } 
							else { pstrProc.Append("null"); }

							// Tipo Servicio.
							pstrProc.Append(",@sdde_seti_id=");
							if (item.sdde_seti_id != -1 ) { pstrProc.Append(item.sdde_seti_id.ToString()); } else { pstrProc.Append("null"); }

							// ID Padre 1.
							pstrProc.Append(",@sdde_padre_id=");
							if (item.sdde_padre_id != -1) { pstrProc.Append(item.sdde_padre_id.ToString()); } else { pstrProc.Append("null"); }

							// Asociaci�n Padre 1.
							pstrProc.Append(",@sdde_padre_asoc_id=");
							if (item.sdde_padre_asoc_id != -1) { pstrProc.Append(item.sdde_padre_asoc_id.ToString()); } else { pstrProc.Append("null"); }

							// RP Padre 1.
							pstrProc.Append(",@sdde_padre_rp=");
							if (item.sdde_padre_rp.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre_rp.ToString())); } 
							else { pstrProc.Append("null"); }

							// HBA Padre 1.
							pstrProc.Append(",@sdde_padre_sra_nume=");
							if (item.sdde_padre_sra_nume != -1) { pstrProc.Append(item.sdde_padre_sra_nume.ToString()); } else { pstrProc.Append("null"); }

							// Registro Padre 1.
							pstrProc.Append(",@sdde_regt_padre_id=");
							if (item.sdde_regt_padre_id != -1 && item.sdde_regt_padre_id >= 0) { pstrProc.Append(item.sdde_regt_padre_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// Nombre Padre 1.
							pstrProc.Append(",@sdde_padre_nombre=");
							if (item.sdde_padre_nombre.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre_nombre.ToString())); } 
							else { pstrProc.Append("null"); }

							// Nro Extranjero Padre 1.
							pstrProc.Append(",@sdde_padre_ori_asoc_nume=");
							if (item.sdde_padre_ori_asoc_nume.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre_ori_asoc_nume.ToString())); } 
							else { pstrProc.Append("null"); }

							// ID Padre 2.
							pstrProc.Append(",@sdde_padre2_id=");
							if (item.sdde_padre2_id != -1) { pstrProc.Append(item.sdde_padre2_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// Asociaci�n Padre 2.
							pstrProc.Append(",@sdde_padre2_asoc_id=");
							if (item.sdde_padre2_asoc_id != -1) { pstrProc.Append(item.sdde_padre2_asoc_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// RP Padre 2.
							pstrProc.Append(",@sdde_padre2_rp=");
							if (item.sdde_padre2_rp != null) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre2_rp.ToString())); } 
							else { pstrProc.Append("null"); }

							// HBA Padre 2.
							pstrProc.Append(",@sdde_padre2_sra_nume=");
							if (item.sdde_padre2_sra_nume != -1) { pstrProc.Append(item.sdde_padre2_sra_nume.ToString()); } 
							else { pstrProc.Append("null"); }

							// Registro Padre 2.
							pstrProc.Append(",@sdde_regt_padre2_id=");
							if (item.sdde_regt_padre2_id != -1 && item.sdde_regt_padre2_id >= 0) { pstrProc.Append(item.sdde_regt_padre2_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// Nombre Padre 2.
							pstrProc.Append(",@sdde_padre2_nombre=");
							if (item.sdde_padre2_nombre.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre2_nombre.ToString())); } 
							else { pstrProc.Append("null"); }

							// Nro Extranjero Padre 2.
							pstrProc.Append(",@sdde_padre2_ori_asoc_nume=");
							if (item.sdde_padre2_ori_asoc_nume.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre2_ori_asoc_nume.ToString())); } 
							else { pstrProc.Append("null"); }

							// ID Padre 3.
							pstrProc.Append(",@sdde_padre3_id=");
							if (item.sdde_padre3_id != -1) { pstrProc.Append(item.sdde_padre3_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// Asociaci�n Padre 3.
							pstrProc.Append(",@sdde_padre3_asoc_id=");
							if (item.sdde_padre3_asoc_id != -1) { pstrProc.Append(item.sdde_padre3_asoc_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// RP Padre 3.
							pstrProc.Append(",@sdde_padre3_rp=");
							if (item.sdde_padre3_rp.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre3_rp.ToString())); } 
							else { pstrProc.Append("null"); }

							// HBA Padre 3.
							pstrProc.Append(",@sdde_padre3_sra_nume=");
							if (item.sdde_padre3_sra_nume != -1) { pstrProc.Append(item.sdde_padre3_sra_nume.ToString()); } 
							else { pstrProc.Append("null"); }

							// Registro Padre 3.
							pstrProc.Append(",@sdde_regt_padre3_id=");
							if (item.sdde_regt_padre3_id != -1 && item.sdde_regt_padre3_id >= 0) { pstrProc.Append(item.sdde_regt_padre3_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// Nombre Padre 3.
							pstrProc.Append(",@sdde_padre3_nombre=");
							if (item.sdde_padre3_nombre.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre3_nombre.ToString())); } 
							else { pstrProc.Append("null"); }

							// Nro Extranjero Padre 3.
							pstrProc.Append(",@sdde_padre3_ori_asoc_nume=");
							if (item.sdde_padre3_ori_asoc_nume.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_padre3_ori_asoc_nume.ToString())); } 
							else { pstrProc.Append("null"); }

							// ID Madre.
							pstrProc.Append(",@sdde_madre_id=");
							if (item.sdde_madre_id != -1) { pstrProc.Append(item.sdde_madre_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// Asociaci�n Madre.
							pstrProc.Append(",@sdde_madre_asoc_id=");
							if (item.sdde_madre_asoc_id != -1) { pstrProc.Append(item.sdde_madre_asoc_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// RP Madre.
							pstrProc.Append(",@sdde_madre_rp=");
							if (item.sdde_madre_rp.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_madre_rp.ToString())); } 
							else { pstrProc.Append("null"); }

							// HBA Madre.
							pstrProc.Append(",@sdde_madre_sra_nume=");
							if (item.sdde_madre_sra_nume != -1) { pstrProc.Append(item.sdde_madre_sra_nume.ToString()); } 
							else { pstrProc.Append("null"); }

							// Registro Madre.
							pstrProc.Append(",@sdde_regt_madre_id=");
							if (item.sdde_regt_madre_id != -1 && item.sdde_regt_madre_id >= 0) { pstrProc.Append(item.sdde_regt_madre_id.ToString()); } 
							else { pstrProc.Append("null"); }

							// Nombre Madre.
							pstrProc.Append(",@sdde_madre_nombre=");
							if (item.sdde_madre_nombre.Trim().Length > 0) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_madre_nombre.ToString())); } 
							else { pstrProc.Append("null"); }

							// Nro Extranjero Madre.
							pstrProc.Append(",@sdde_madre_ori_asoc_nume=");
							if (item.sdde_madre_ori_asoc_nume != null) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_madre_ori_asoc_nume.ToString())); } 
							else { pstrProc.Append("null"); }

							// Observaci�n.
							pstrProc.Append(",@sdde_obser=");
							if (item.sdde_obser != null) { pstrProc.Append((new DataBase()).darFormatoSQl(item.sdde_obser.ToString())); } 
							else { pstrProc.Append("null"); }

							// Usuario.
							pstrProc.Append(",@sdde_audi_user=");
							pstrProc.Append(item.sdde_audi_user ); // usuaripo

							// Ejecuto el Query.
							DataSet ds = clsSQLServer.gExecuteQuery(transPpal, pstrProc.ToString(), 9999);

							if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
							{
								item.sdde_id = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
								blnOK = true;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "DenunciaServiciosDao.InsertaBorraModificaDetalle: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}

			return blnOK;
		}
	}
}
