using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities;
using Entities.Facturacion;
using System.Data.SqlClient;

namespace DAO.Productos
{
	/// <summary>
	/// Summary description for ProductosDao.
	/// </summary>
	public class ProductosDao
	{
			/// <summary>
			/// Consulta si existe el producto importado que voy a ingresar.
			/// </summary>
			/// <param name="intRaza">Raza</param>
			/// <param name="intSexo">Sexo: 0 - Hembra // 1 - Macho</param>
			/// <param name="intAsociacion">Asociación</param>
			/// <param name="strNroExtranjero">Nro. Extanjero</param>
			/// <param name="strRP">RP</param>
			/// <param name="strNombre">Nombre</param>
			/// <returns>Devuelve un DataSet</returns>
		public static DataSet ConsultaExistenciaProductoImportado(int intRaza, int intSexo, int intAsociacion
															    , string strNroExtranjero, string strRP, string strNombre)
			{
				DataSet dsResultadoConsul = new DataSet();		
				StringBuilder pstrProc = new StringBuilder();
			// Conexión.
			DataBase oBase = new DataBase();

				pstrProc.Append("exec p_rg_ObtenerProductoImportado_consul ");
				// Raza
				pstrProc.Append("@prdt_raza_id=");
				pstrProc.Append(intRaza); 
				// Sexo
				pstrProc.Append(",@prdt_sexo=");
				pstrProc.Append(intSexo); 
				// Asociación
				pstrProc.Append(",@prdt_ori_asoc_id=");
				pstrProc.Append(intAsociacion); 
				// Nro. Extranjero
				pstrProc.Append(",@prdt_ori_asoc_nume=");
				pstrProc.Append("'" + strNroExtranjero + "'");
				// RP Extranjero
				pstrProc.Append(",@prdt_rp_extr=");
				if (strRP != string.Empty) { pstrProc.Append("'" + strRP + "'"); } else { pstrProc.Append("null"); }
				// Nombre
				pstrProc.Append(",@prdt_nomb=");
				if (strNombre != string.Empty) { pstrProc.Append("'" + strNombre + "'"); } else { pstrProc.Append("null"); }

				try
				{
					
					// Ejecuto el Query
					dsResultadoConsul = clsSQLServer.gExecuteQuery(oBase.mstrConn, pstrProc.ToString());
				}
				catch (Exception ex)
				{
					string Msg = "ProductosDao.ConsultaExistenciaProductoImportado: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
					clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
					throw new Exception(ex.Message.ToString());
				}

				return dsResultadoConsul;
			}
		public static DataSet InsertarProductoImportado(int intRaza, int intSexo, int intAsociacion, string strNroExtranjero
			, string strRP, string strNombre, string strApodo, bool bitGenHBA 
			, string strNdad,string FechaParam,string strTipoProducto
			, string FechaNaci, string RPNac, int idTipoReg, int idVariedad, int idPelaje, int idCriadorPropietario
			, int intUsuario)
		{
			DataBase oBase = new DataBase();
			// abro la coneccion
			SqlConnection myConnectionSQLPpal = new SqlConnection(oBase.mstrConn);
			myConnectionSQLPpal.Open();
			// obtengo una transaccion
			SqlTransaction transPpal = myConnectionSQLPpal.BeginTransaction(IsolationLevel.ReadCommitted);

			return InsertarProductoImportado(transPpal,  intRaza,  intSexo,  intAsociacion,  strNroExtranjero
											, strRP,  strNombre,  strApodo,  bitGenHBA 
											, strNdad, FechaParam, strTipoProducto
											, FechaNaci,  RPNac,  idTipoReg,  idVariedad,  idPelaje, idCriadorPropietario
											, intUsuario);
		}


		/// <summary>
		/// Inserta un nuevo producto como importado.
		/// </summary>
		/// <param name="intRaza">Raza</param>
		/// <param name="intSexo">Sexo: 0 - Hembra // 1 - Macho</param>
		/// <param name="intAsociacion">Asociación</param>
		/// <param name="strNroExtranjero">Nro. Extanjero</param>
		/// <param name="strRP">RP</param>
		/// <param name="strNombre">Nombre</param>
		/// <param name="strTipoControl">"producto" // "madre" // "padre"</param>
		/// <returns>Devuelve un DataSet</returns>
		public static DataSet InsertarProductoImportado(SqlTransaction transPpal, int intRaza, int intSexo, int intAsociacion, string strNroExtranjero
													  , string strRP, string strNombre, string strApodo, bool bitGenHBA 
													  , string strNdad,string FechaParam,string strTipoProducto
													  , string FechaNaci, string RPNac, int idTipoReg, int idVariedad, int idPelaje, int idCriadorPropietario
													  , int intUsuario)
		{
			string strProductoID = string.Empty;
			DataSet dsResultadoIns = new DataSet();		
			StringBuilder pstrProc = new StringBuilder();

			// Conexión.
			DataBase oBase = new DataBase();

			pstrProc.Append("exec p_rg_ObtenerProductoImportado_alta ");
			// Raza
			pstrProc.Append("@prdt_raza_id=");
			pstrProc.Append(intRaza); 
			// Sexo
			pstrProc.Append(",@prdt_sexo=");
			pstrProc.Append(intSexo); 
			// Asociación
			pstrProc.Append(",@prdt_ori_asoc_id=");
			pstrProc.Append(intAsociacion); 
			// Nro. Extranjero
			pstrProc.Append(",@prdt_ori_asoc_nume=");
			pstrProc.Append(oBase.darFormatoSQl(strNroExtranjero.ToUpper()));
			// RP Extranjero
			pstrProc.Append(",@prdt_rp_extr=");
			if (strRP != string.Empty) { pstrProc.Append(oBase.darFormatoSQl(strRP.ToUpper())); } 
			else { pstrProc.Append("null"); }
			// Nombre
			pstrProc.Append(",@prdt_nomb=");
			if (strNombre != string.Empty) { pstrProc.Append(oBase.darFormatoSQl(strNombre.ToUpper())); } 
			else { pstrProc.Append("null"); }
			// Apodo
			pstrProc.Append(",@prdt_apodo=");
			if (strNombre != string.Empty) { pstrProc.Append(oBase.darFormatoSQl(strApodo.ToUpper())); } 
			else { pstrProc.Append("null"); }
			// ndad
			pstrProc.Append(",@prdt_ndad=");
			if (strNdad.Trim().Length > 0){pstrProc.Append(oBase.darFormatoSQl(strNdad.ToUpper()));} else{pstrProc.Append(oBase.darFormatoSQl("E"));}
			// bit que indica si auto genera nro de hba y nro de macho dador 
			pstrProc.Append(",@booAutoGeneraHBA=");
			pstrProc.Append(bitGenHBA);
			// fecha de ingreso del trammite 
			pstrProc.Append(",@dateFechaImportacion=");
			if (FechaParam.Trim().Length > 0){
				string[] fechaImport = FechaParam.Split('/');
				pstrProc.Append(oBase.darFormatoSQl(fechaImport[2] + fechaImport[1] + fechaImport[0]));
				//pstrProc.Append(oBase.darFormatoSQl(Convert.ToDateTime(FechaParam).ToString("yyyy/MM/dd HH:ss")));
			} else{pstrProc.Append("null");}
			// id prop criador
			pstrProc.Append(",@idCriadorPropietario=");
			if (idCriadorPropietario > 0) { pstrProc.Append(idCriadorPropietario); } else { pstrProc.Append("null"); }

			// Tipo Producto S , E (semen o Embrion)
			pstrProc.Append(",@TipoProducto=");
			pstrProc.Append("'"+strTipoProducto+"'"); 

			// Usuario
			pstrProc.Append(",@prdt_audi_user=");
			pstrProc.Append(intUsuario);
			// nuevos datos Dario 2014-07-03
			// , 
			// Fecha de nacimiento
			pstrProc.Append(",@dateFechaNacimiento=");
			if (FechaNaci.Trim().Length > 0) {
				string[] fecha = FechaNaci.Split('/');
				pstrProc.Append(oBase.darFormatoSQl(fecha[2]+fecha[1]+fecha[0]));
			} else { pstrProc.Append("null"); }
			//if (FechaNaci.Trim().Length > 0) { pstrProc.Append(oBase.darFormatoSQl(Convert.ToDateTime(FechaNaci).ToString("yyyyMMdd"))); } else { pstrProc.Append("null"); }
			// RP nacional
			pstrProc.Append(",@rpNacional=");
			if (RPNac != string.Empty) { pstrProc.Append(oBase.darFormatoSQl(RPNac.ToUpper())); } else { pstrProc.Append("null"); }
			// tipo registro
			pstrProc.Append(",@idTipoReg=");
			if (idTipoReg != -1) { pstrProc.Append(idTipoReg); } else { pstrProc.Append("null"); }
			// id de variedad
			pstrProc.Append(",@idVariedad=");
			if (idVariedad != -1) { pstrProc.Append(idVariedad); } else { pstrProc.Append("null"); }
			// id de variedad
			pstrProc.Append(",@idPelajeA=");
			if (idPelaje != -1) { pstrProc.Append(idPelaje); } else { pstrProc.Append("null"); }

			try
			{
				// Ejecuto el Query
				dsResultadoIns = clsSQLServer.gExecuteQuery(transPpal, pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.InsertarProductoImportado: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}

			return dsResultadoIns;
		}

		public static DataSet ValidarRPRepetido(string razaID, string criaID, string strRP)
		{
			DataSet dsRPRepetido = new DataSet();
			StringBuilder pstrProc = new StringBuilder();
			DataBase oBase = new DataBase(); // Conexión.

			try
			{			
				pstrProc.Append("exec p_RG_valida_rp_repetido ");

				//Raza.
				pstrProc.Append("@raza_id=");
				pstrProc.Append(razaID);

				//Criador.
				pstrProc.Append(",@cria_id=");
				pstrProc.Append(criaID);

				//RP.
				pstrProc.Append(",@rp='");
				pstrProc.Append(strRP);
				pstrProc.Append("'");

				// Ejecuto el Query
				dsRPRepetido = clsSQLServer.gExecuteQuery(oBase.mstrConn, pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.ValidarRPRepetido: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}

			return dsRPRepetido;
		}

		/// <summary>
		/// metodo que se utiliza para modificar datos del producto solo desde inspecion fenotipica
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns>bool</returns>
		public static bool ModifProductoPorInspecion(SqlTransaction trans, ProductoInspecionesFenoEntity obj)
		{
			bool respuesta = false;
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_RG_ModificarProductoPorInspecionFenotipica ");
			pstrProc.Append("@prdt_id=");
			pstrProc.Append(obj.prin_prdt_id); 
			pstrProc.Append(",@prdt_sexo=");
			pstrProc.Append((obj.prdt_sexo == true)? 1:0 ); 
			pstrProc.Append(",@prdt_naci_fecha=");
			if (obj.prdt_naci_fecha != _DataBase.DateTimeDBNull()){pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.prdt_naci_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@prdt_px=");
			if (obj.prdt_px.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.prdt_px)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prdt_bamo_id=");
			if (obj.prdt_bamo_id != -1) { pstrProc.Append(obj.prdt_bamo_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prdt_baja_fecha=");
			if (obj.prdt_baja_fecha != _DataBase.DateTimeDBNull()){pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.prdt_baja_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@prdt_regt_id=");
			if (obj.prdt_regt_id != -1) { pstrProc.Append(obj.prdt_regt_id); } 	else { pstrProc.Append("null"); }
			pstrProc.Append(",@prdt_vari_id=");
			if (obj.prdt_vari_id != -1) { pstrProc.Append(obj.prdt_vari_id); } 	else { pstrProc.Append("null"); }

			try
			{
				ds = clsSQLServer.gExecuteQuery(trans , pstrProc.ToString()); 
				respuesta = Convert.ToBoolean(ds.Tables[0].Rows[0]["respuesta"]);
			}
			catch (Exception ex)
			{
				string Msg = "Productos.ModifProductoPorInspecion: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que modifica una inspeccion
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns>bool</returns>
		public static bool ModifProductosInspec(SqlTransaction trans, ProductoInspecionesFenoEntity obj)
		{
			bool respuesta = false;
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			
			pstrProc.Append("exec productos_inspec_modi ");
			pstrProc.Append("@prin_id=");
			pstrProc.Append(obj.prin_id); 
			pstrProc.Append(",@prin_prdt_id=");
			pstrProc.Append(obj.prin_prdt_id); 
			pstrProc.Append(",@prin_fecha=");
			if (obj.prin_fecha != _DataBase.DateTimeDBNull()){pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.prin_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_resu=");
			if (obj.prin_resu != -1){ pstrProc.Append(obj.prin_resu);} else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_obse=");
			if (obj.prin_obse.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.prin_obse)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_tipo=");
			if (obj.prin_tipo.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.prin_tipo)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_clie_id=");
			if (obj.prin_clie_id != -1) { pstrProc.Append(obj.prin_clie_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_audi_user=");
			if (obj.prin_audi_user != -1) { pstrProc.Append(obj.prin_audi_user); } 	else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_tmsp=null");
			pstrProc.Append(",@prin_baja_fecha=null");
			pstrProc.Append(",@prin_bitTrim=");
			pstrProc.Append(obj.prin_bitTrim);
			pstrProc.Append(",@prin_varInspector=");
			if (obj.prin_varInspector.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.prin_varInspector)); } else { pstrProc.Append("null"); }

			try
			{
				ds = clsSQLServer.gExecuteQuery(trans , pstrProc.ToString());
				respuesta = Convert.ToBoolean(ds.Tables[0].Rows[0]["respuesta"]);
			}
			catch (Exception ex)
			{
				string Msg = "Productos.ModifProductosInspec: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que crea una inspeccion para un producto
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static bool CrearProductosInspec(SqlTransaction trans, ProductoInspecionesFenoEntity obj)
		{
			bool respuesta = false;
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();
			
			pstrProc.Append("exec productos_inspec_alta ");
			pstrProc.Append("@prin_prdt_id=");
			pstrProc.Append(obj.prin_prdt_id); 
			pstrProc.Append(",@prin_fecha=");
			if (obj.prin_fecha != _DataBase.DateTimeDBNull()){pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)obj.prin_fecha).ToString("yyyy/MM/dd")));} else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_resu=");
			if (obj.prin_resu != -1){ pstrProc.Append(obj.prin_resu);} else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_obse=");
			if (obj.prin_obse.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.prin_obse)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_tipo=");
			if (obj.prin_tipo.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.prin_tipo)); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_clie_id=");
			if (obj.prin_clie_id != -1) { pstrProc.Append(obj.prin_clie_id); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_audi_user=");
			if (obj.prin_audi_user != -1) { pstrProc.Append(obj.prin_audi_user); } else { pstrProc.Append("null"); }
			pstrProc.Append(",@prin_bitTrim=");
			pstrProc.Append(obj.prin_bitTrim);
			pstrProc.Append(",@prin_varInspector=");
			if (obj.prin_varInspector.Length > 0) { pstrProc.Append(_DataBase.darFormatoSQl(obj.prin_varInspector)); } else { pstrProc.Append("null"); }

			try
			{
				ds = clsSQLServer.gExecuteQuery(trans , pstrProc.ToString());
				Int32 idInsert = Convert.ToInt32(ds.Tables[0].Rows[0]["prin_id"]);

				if(idInsert>0)
					respuesta = true;
				else
					respuesta = false;

			}
			catch (Exception ex)
			{
				string Msg = "Productos.ModifProductosInspec: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que obtiene el detalle de comprobante para un nro de control y criador
		/// para la validacion de los tramites de propiedad
		/// </summary>
		/// <param name="nroControl"></param>
		/// <param name="idCriador"></param>
		/// <returns>ControlCobranzasRRGGEntity</returns>
		public static ControlCobranzasRRGGEntity GetControlCobranzasRRGG(int nroControl, int idCriador, int idRaza )
		{
			ControlCobranzasRRGGEntity respuesta = new ControlCobranzasRRGGEntity();
			DataSet ds = null;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec p_rg_ControlCobranzas_rrgg ");
			pstrProc.Append("@nroControl=");
			pstrProc.Append(nroControl); 
			pstrProc.Append(",@idCriador=");
			if (idCriador != -1){ pstrProc.Append(idCriador);} else { pstrProc.Append("null"); }
			pstrProc.Append(",@idRaza=");
			if (idRaza != -1){ pstrProc.Append(idRaza);} else { pstrProc.Append("null"); }

			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["comprobanteID"] != System.DBNull.Value){respuesta.comprobanteID  = Convert.ToInt32(dr["comprobanteID"]);}
						if(dr["proformaID"] != System.DBNull.Value){respuesta.proformaID  = Convert.ToInt32(dr["proformaID"]);}
						if(dr["nroFactura"] != System.DBNull.Value){respuesta.nroFactura  = dr["nroFactura"].ToString();}
						if(dr["cantFacturada"] != System.DBNull.Value){respuesta.cantFacturada  = Convert.ToInt32(dr["cantFacturada"]);}
						if(dr["cantNoFacturada"] != System.DBNull.Value){respuesta.cantNoFacturada  = Convert.ToInt32(dr["cantNoFacturada"]);}
						if(dr["idRaza"] != System.DBNull.Value){respuesta.idRaza  = Convert.ToInt32(dr["idRaza"]);}
						if(dr["idCriador"] != System.DBNull.Value){respuesta.idCriador  = Convert.ToInt32(dr["idCriador"]);}
						if(dr["codiServ"] != System.DBNull.Value){respuesta.codiServ  = Convert.ToInt32(dr["codiServ"]);}
						if(dr["idClienteBuscado"] != System.DBNull.Value){respuesta.idClienteBuscado  = Convert.ToInt32(dr["idClienteBuscado"]);}
						if(dr["idClienteEncontrado"] != System.DBNull.Value){respuesta.idClienteEncontrado  = Convert.ToInt32(dr["idClienteEncontrado"]);}		
						break;
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.GetControlCobranzasRRGG: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
		
		/// <summary>
		/// metodo que retorna los datos de un producto
		/// </summary>
		/// <param name="idProducto"></param>
		/// <returns>ProductoEntity</returns>
		public static ProductoEntity GetDatosProducto(int idProducto)
		{
			ProductoEntity respuesta = new ProductoEntity();
			DataSet ds = null;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec p_rg_GetDatosProducto ");
			pstrProc.Append("@idProducto=");
			pstrProc.Append(idProducto); 
			
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["prdt_id"] != System.DBNull.Value){respuesta.prdt_id  = Convert.ToInt32(dr["prdt_id"]);}
						if(dr["idPadre"] != System.DBNull.Value){respuesta.idPadre  = Convert.ToInt32(dr["idPadre"]);}
						if(dr["idMadre"] != System.DBNull.Value){respuesta.idMadre  = Convert.ToInt32(dr["idMadre"]);}
						if(dr["idSNPS"] != System.DBNull.Value){respuesta.idSNPS  = Convert.ToInt32(dr["idSNPS"]);}
						break;
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.GetDatosProductos: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}


		/// <summary>
		/// metodo que genera el nro de dador para el producto si 
		/// no tiene ya uno
		/// </summary>
		/// <param name="idProducto"></param>
		/// <returns>bool</returns>
		public static bool SetNroDador(int idProducto)
		{
			bool respuesta = false;
			DataSet ds = null;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec SetNroDador ");
			pstrProc.Append("@idProducto=");
			pstrProc.Append(idProducto); 
			
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
				
				respuesta = true;
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.SetNroDador: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}


		/// <summary>
		/// metodo que revierte la baja del producto
		/// </summary>
		/// <param name="idProducto"></param>
		/// <param name="idUsuario"></param>
		/// <returns>bool</returns>
		public static bool RevertirBajaProducto(Int32 idProducto, Int32 idUsuario)
		{
			bool respuesta = false;
			DataSet ds = null;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec p_rg_RevertirBajaProducto ");
			pstrProc.Append("@idProducto=");
			pstrProc.Append(idProducto); 
			pstrProc.Append(",@idUsuario=");
			pstrProc.Append(idUsuario); 
			
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
				respuesta = Convert.ToBoolean(ds.Tables[0].Rows[0]["respuesta"]);
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.RevertirBajaProducto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que nacionaliza el producto informado
		/// </summary>
		/// <param name="idProducto"></param>
		/// <param name="idUsuario"></param>
		/// <returns></returns>
		public static Int32 NacionalizarExtranjero(Int32 idProducto, Int32 idUsuario)
		{
			Int32 respuesta = -1;
			DataSet ds = null;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec p_rg_NacionalizarExtranjero ");
			pstrProc.Append("@idProducto=");
			pstrProc.Append(idProducto); 
			pstrProc.Append(",@idUsuario=");
			pstrProc.Append(idUsuario); 
			
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
				respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["respuesta"]);
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.NacionalizarExtranjero: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que pasando un producto indica que parte del pedegree se puede modificar
		/// esto lo hace por estado del analisisl del mismo producto al que se le quieren 
		/// modificar los padres, si es usuario sra permite cualquier cosa
		/// -1 = no se puede modificar ni padre ni madre
		/// 0 = deja modificar padre y madre
		/// 1 = solo deja modificar padre
		/// 2 = solo deja modificar madre
		/// </summary>
		/// <param name="idProducto"></param>
		/// <param name="idUsuario"></param>
		/// <returns>Int16</returns>
		public static RespValidaModificarPedregeeEntity ValidaModificarPedregeeProducto(Int32 idProducto, Int32 idUsuario)
		{
			RespValidaModificarPedregeeEntity respuesta = new RespValidaModificarPedregeeEntity();
			DataSet ds = null;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec p_rg_ValidaModificarPedregeeProducto ");
			pstrProc.Append("@idProducto=");
			pstrProc.Append(idProducto); 
			pstrProc.Append(",@idUsuario=");
			pstrProc.Append(idUsuario); 
			
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
				
				foreach (DataRow dr in ds.Tables[0].Rows)
				{
					if(dr["respuesta"] != System.DBNull.Value){respuesta.respuesta_ID  = Convert.ToInt32(dr["respuesta"]);}
					if(dr["descrespuesta"] != System.DBNull.Value){respuesta.respuesta_Desc = dr["descrespuesta"].ToString();}else{respuesta.respuesta_Desc = string.Empty;}
					break;
				}
			}
			catch (Exception ex)
			{
				string Msg = "ProductosDao.ValidaModificarPedregeeProducto: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

	}
}

