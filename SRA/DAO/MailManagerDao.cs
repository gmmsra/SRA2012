using System;
using System.Data;
using AccesoBD;
using System.Text;


namespace DAO
{
	/// <summary>
	/// Summary description for MailManagerDao.
	/// </summary>
	public class MailManagerDao
	{
		/// <summary>
		/// metodo que retorna los mail y los usuarios
		/// que deben de recibir la alarta parada por parametro
		/// </summary>
		/// <param name="IdAlerta"></param>
		/// <returns></returns>
		public static DataSet GetMailsAlertaEnviar(int IdAlerta)
		{
			DataSet ds = null;
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec FAC_CE_GetMailsAlertaEnviar ");
			pstrProc.Append("@alti_id=");
			pstrProc.Append(IdAlerta); 
            
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());
			}
			catch (Exception ex)
			{
				string Msg = "MailManagerDao.GetMailsAlertaEnviar: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return ds;
		}
	}
}
