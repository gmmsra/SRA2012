using System;
using System.Data;
using AccesoBD;
using System.Text;
using Entities;

namespace DAO
{
	/// <summary>
	/// Summary description for ParametrosDao.
	/// </summary>
	public class ParametrosDao
	{
		/// <summary>
		/// metodo que carga el objeto parametos desde la base de datos.
		/// </summary>
		/// <returns>ParametrosEntity</returns>
		public static ParametrosEntity GetAllParametros()
		{
			ParametrosEntity respuesta = new ParametrosEntity();

			DataSet ds = null;
			
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec parametros_consul ");
			
			DataBase oBase = new DataBase();

			try
			{
				ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["para_pesos_mone_id"] != System.DBNull.Value){respuesta.para_pesos_mone_id  = Convert.ToInt32(dr["para_pesos_mone_id"]);}
						if(dr["para_dolar_mone_id"] != System.DBNull.Value){respuesta.para_dolar_mone_id  = Convert.ToInt32(dr["para_dolar_mone_id"]);}
						if(dr["para_clie_docu_path"] != System.DBNull.Value){respuesta.para_clie_docu_path = dr["para_clie_docu_path"].ToString();}
						if(dr["para_clie_foto_path"] != System.DBNull.Value){respuesta.para_clie_foto_path = dr["para_clie_foto_path"].ToString();}
						if(dr["para_page_size_grilla"] != System.DBNull.Value){respuesta.para_page_size_grilla  = Convert.ToInt32(dr["para_page_size_grilla"]);}
						if(dr["para_iva_cons_final"] != System.DBNull.Value){respuesta.para_iva_cons_final  = Convert.ToInt32(dr["para_iva_cons_final"]);}
						if(dr["para_soin_vige_dias"] != System.DBNull.Value){respuesta.para_soin_vige_dias  = Convert.ToInt32(dr["para_soin_vige_dias"]);}
						if(dr["para_soin_vige_dias"] != System.DBNull.Value){respuesta.para_soci_anios_vitalicio  = Convert.ToInt32(dr["para_soin_vige_dias"]);}
						if(dr["para_cemi_nume"] != System.DBNull.Value){respuesta.para_cemi_nume  = Convert.ToInt32(dr["para_cemi_nume"]);}
						if(dr["para_bime_en_curso"] != System.DBNull.Value){respuesta.para_bime_en_curso  = Convert.ToInt32(dr["para_bime_en_curso"]);}
						if(dr["para_cate_acti_id"] != System.DBNull.Value){respuesta.para_cate_acti_id  = Convert.ToInt32(dr["para_cate_acti_id"]);}
						if(dr["para_cunica_nume"] != System.DBNull.Value){respuesta.para_cunica_nume  = Convert.ToInt32(dr["para_cunica_nume"]);}
						if(dr["para_menor_edad"] != System.DBNull.Value){respuesta.para_menor_edad  = Convert.ToInt32(dr["para_menor_edad"]);}
						if(dr["para_menor_ingreso_edad"] != System.DBNull.Value){respuesta.para_menor_ingreso_edad = Convert.ToInt32(dr["para_menor_ingreso_edad"]);}
						if(dr["para_soci_inte"] != System.DBNull.Value){respuesta.para_soci_inte =  Convert.ToDecimal(dr["para_soci_inte"]);}
						if(dr["para_soci_inte_fecha"] != System.DBNull.Value){respuesta.para_soci_inte_fecha = Convert.ToDateTime(dr["para_soci_inte_fecha"]);}
						if(dr["para_inst_defa"] != System.DBNull.Value){respuesta.para_inst_defa  = Convert.ToInt32(dr["para_inst_defa"]);}
						if(dr["para_reva_ulti_fecha"] != System.DBNull.Value){respuesta.para_reva_ulti_fecha  = Convert.ToDateTime(dr["para_reva_ulti_fecha"]);}
						if(dr["para_ejer_cierre_mes"] != System.DBNull.Value){respuesta.para_ejer_cierre_mes  = Convert.ToInt32(dr["para_ejer_cierre_mes"]);}
						if(dr["para_fact_vto_dias"] != System.DBNull.Value){respuesta.para_fact_vto_dias  = Convert.ToInt32(dr["para_fact_vto_dias"]);}
						if(dr["para_reci_clie_id"] != System.DBNull.Value){respuesta.para_reci_clie_id  = Convert.ToInt32(dr["para_reci_clie_id"]);}
						if(dr["para_desde_mail"] != System.DBNull.Value){respuesta.para_desde_mail  = dr["para_desde_mail"].ToString();}
						if(dr["para_admin_mail"] != System.DBNull.Value){respuesta.para_admin_mail  = dr["para_admin_mail"].ToString();}
						if(dr["para_rrgg_tram"] != System.DBNull.Value){respuesta.para_rrgg_tram  = Convert.ToInt32(dr["para_rrgg_tram"]);}
						if(dr["para_ctac_inte"] != System.DBNull.Value){respuesta.para_ctac_inte  = Convert.ToDecimal(dr["para_ctac_inte"]);}
						if(dr["para_min_inte"] != System.DBNull.Value){respuesta.para_min_inte  = Convert.ToDecimal(dr["para_min_inte"]);}
						if(dr["para_labo_dias_turnos"] != System.DBNull.Value){respuesta.para_labo_dias_turnos  = Convert.ToInt32(dr["para_labo_dias_turnos"]);}
						if(dr["para_capi_pcia_id"] != System.DBNull.Value){respuesta.para_capi_pcia_id  = Convert.ToInt32(dr["para_capi_pcia_id"]);}
						if(dr["para_ult_tram_RRGG"] != System.DBNull.Value){respuesta.para_ult_tram_RRGG  = Convert.ToInt32(dr["para_ult_tram_RRGG"]);}
						if(dr["para_dias_aviso_cai"] != System.DBNull.Value){respuesta.para_dias_aviso_cai  = Convert.ToInt32(dr["para_dias_aviso_cai"]);}
						if(dr["para_EstableNumeroPmC"] != System.DBNull.Value){respuesta.para_leyen_dolar  = dr["para_EstableNumeroPmC"].ToString();}
						if(dr["para_soci_maxi_bime"] != System.DBNull.Value){respuesta.para_soci_maxi_bime  = Convert.ToInt32(dr["para_soci_maxi_bime"]);}
						if(dr["para_dias_aviso_cheque"] != System.DBNull.Value){respuesta.para_dias_aviso_cheque  = Convert.ToInt32(dr["para_dias_aviso_cheque"]);}
						if(dr["para_clie_cria_firma_path"] != System.DBNull.Value){respuesta.para_clie_cria_firma_path  = dr["para_clie_cria_firma_path"].ToString();}
						if(dr["para_clie_raza_path"] != System.DBNull.Value){respuesta.para_clie_raza_path  = dr["para_clie_raza_path"].ToString();}
						if(dr["para_sra_asoc_id"] != System.DBNull.Value){respuesta.para_sra_asoc_id  = Convert.ToInt32(dr["para_sra_asoc_id"]);}
						if(dr["para_dias_cierre_arqueo"] != System.DBNull.Value){respuesta.para_dias_cierre_arqueo  = Convert.ToInt32(dr["para_dias_cierre_arqueo"]);}
						if(dr["para_giro_banc_id"] != System.DBNull.Value){respuesta.para_giro_banc_id  = Convert.ToInt32(dr["para_giro_banc_id"]);}
						if(dr["para_craz_docu_path"] != System.DBNull.Value){respuesta.para_craz_docu_path  = dr["para_craz_docu_path"].ToString();}
						if(dr["para_prdt_docu_path"] != System.DBNull.Value){respuesta.para_prdt_docu_path  = dr["para_prdt_docu_path"].ToString();}
						if(dr["para_tram_docu_path"] != System.DBNull.Value){respuesta.para_tram_docu_path  = dr["para_tram_docu_path"].ToString();}
						if(dr["para_temp_path"] != System.DBNull.Value){respuesta.para_temp_path  = dr["para_temp_path"].ToString();}
						if(dr["para_nume_cert"] != System.DBNull.Value){respuesta.para_nume_cert  = Convert.ToInt32(dr["para_nume_cert"]);}
						if(dr["para_soci_ult_nume"] != System.DBNull.Value){respuesta.para_soci_ult_nume  = Convert.ToInt32(dr["para_soci_ult_nume"]);}
						if(dr["para_soci_apod_tope"] != System.DBNull.Value){respuesta.para_soci_apod_tope  = Convert.ToInt32(dr["para_soci_apod_tope"]);}
						if(dr["para_soci_apod_firm"] != System.DBNull.Value){respuesta.para_soci_apod_firm  = Convert.ToInt32(dr["para_soci_apod_firm"]);}
						if(dr["para_servi_bovi_maxi_cant"] != System.DBNull.Value){respuesta.para_servi_bovi_maxi_cant  = Convert.ToInt32(dr["para_servi_bovi_maxi_cant"]);}
						if(dr["para_servi_capr_maxi_cant"] != System.DBNull.Value){respuesta.para_servi_capr_maxi_cant  = Convert.ToInt32(dr["para_servi_capr_maxi_cant"]);}
						if(dr["para_soci_fall_prec"] != System.DBNull.Value){respuesta.para_soci_fall_prec  = Convert.ToInt32(dr["para_soci_fall_prec"]);}
						if(dr["para_recalcu_prof_dias"] != System.DBNull.Value){respuesta.para_recalcu_prof_dias  = Convert.ToInt32(dr["para_recalcu_prof_dias"]);}
						if(dr["para_expo"] != System.DBNull.Value){respuesta.para_expo = Convert.ToInt32(dr["para_expo"]);}
						if(dr["_para_inte_presu_expo_mes"] != System.DBNull.Value){respuesta.para_inte_presu_expo_mes  = Convert.ToInt32(dr["_para_inte_presu_expo_mes"]); }
						if(dr["_para_inte_presu_expo_anio"] != System.DBNull.Value){respuesta.para_inte_presu_expo_anio  = Convert.ToInt32(dr["_para_inte_presu_expo_anio"]);}
						if(dr["para_iibb_perc"] != System.DBNull.Value){respuesta.para_iibb_perc  = Convert.ToBoolean(dr["para_iibb_perc"]);}
						if(dr["para_iva_perc"] != System.DBNull.Value){respuesta.para_iva_perc  = Convert.ToBoolean(dr["para_iva_perc"]);}
						if(dr["para_EstableNumeroPmC"] != System.DBNull.Value){respuesta.para_EstableNumeroPmC = Convert.ToInt32(dr["para_EstableNumeroPmC"]);}
						if(dr["para_EmpresaNumeroPmC"] != System.DBNull.Value){respuesta.para_EmpresaNumeroPmC = Convert.ToInt32(dr["para_EmpresaNumeroPmC"]);}
						if(dr["para_BcoAsignadoNumeroPmC"] != System.DBNull.Value){respuesta.para_BcoAsignadoNumeroPmC = Convert.ToInt32(dr["para_BcoAsignadoNumeroPmC"]);}
						if(dr["para_conc_id"] != System.DBNull.Value){respuesta.para_conc_id = Convert.ToInt32(dr["para_conc_id"]);}
						if(dr["para_coti_id"] != System.DBNull.Value){respuesta.para_coti_id = Convert.ToInt32(dr["para_coti_id"]);}
						if(dr["para_cuba_id"] != System.DBNull.Value){respuesta.para_cuba_id = Convert.ToInt32(dr["para_cuba_id"]);}
						if(dr["para_banc_id"] != System.DBNull.Value){respuesta.para_banc_id = Convert.ToInt32(dr["para_banc_id"]);}
						if(dr["para_Porc_ComisionPmC"] != System.DBNull.Value){respuesta.para_Porc_ComisionPmC  = Convert.ToDecimal(dr["para_Porc_ComisionPmC"]);}
						if(dr["para_MontoMinimo_ComisionPmC"] != System.DBNull.Value){respuesta.para_MontoMinimo_ComisionPmC  = Convert.ToDecimal(dr["para_MontoMinimo_ComisionPmC"]);}
						if(dr["para_Iva_ComisionPmC"] != System.DBNull.Value){respuesta.para_Iva_ComisionPmC  = Convert.ToDecimal(dr["para_Iva_ComisionPmC"]);}
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "ParametrosDao.GetAllParametros: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
	}
}
