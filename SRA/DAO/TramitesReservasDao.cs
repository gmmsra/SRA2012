﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AccesoBD;
using Entities;
using DAO;
//using Common;

namespace DAO
{
    public class TramitesReservasDao
    {        
        public static DataSet GetTramiteReservas(int tram_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetTramiteReservas");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_id", tram_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return ds;
        }

        public static void InsertTramitesReservas(TramitesReservasEntity TramiteReservas)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_InsertTramiteReservas");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Tram_id", TramiteReservas.Tram_id));
                cmd.Parameters.Add(new SqlParameter("@ReservaSemen", TramiteReservas.ReservaSemen));
                cmd.Parameters.Add(new SqlParameter("@CantReservaSemen", TramiteReservas.CantReservaSemen));
                cmd.Parameters.Add(new SqlParameter("@ReservaCria", TramiteReservas.ReservaCria));
                cmd.Parameters.Add(new SqlParameter("@CantReservaCria", TramiteReservas.CantReservaCria));
                cmd.Parameters.Add(new SqlParameter("@ReservaEmbrion", TramiteReservas.ReservaEmbrion));
                cmd.Parameters.Add(new SqlParameter("@CantReservaEmbrion", TramiteReservas.CantReservaEmbrion));
                cmd.Parameters.Add(new SqlParameter("@ReservaVientre", TramiteReservas.ReservaVientre));
                cmd.Parameters.Add(new SqlParameter("@ReservaMaterialGenetico", TramiteReservas.ReservaMaterialGenetico));
                cmd.Parameters.Add(new SqlParameter("@ReservaDerechoClonar", TramiteReservas.ReservaDerechoClonar));
                cmd.Parameters.Add(new SqlParameter("@AutorizaCompradorClonar", TramiteReservas.AutorizaCompradorClonar));
                cmd.Parameters.Add(new SqlParameter("@IdUsuarioAudi", TramiteReservas.IdUsuarioAudi));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
        }

        public static void UpdateTramitesReservas(TramitesReservasEntity TramiteReservas)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_UpdateTramiteReservas");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Tram_id", TramiteReservas.Tram_id));
                cmd.Parameters.Add(new SqlParameter("@ReservaSemen", TramiteReservas.ReservaSemen));
                cmd.Parameters.Add(new SqlParameter("@CantReservaSemen", TramiteReservas.CantReservaSemen));
                cmd.Parameters.Add(new SqlParameter("@ReservaCria", TramiteReservas.ReservaCria));
                cmd.Parameters.Add(new SqlParameter("@CantReservaCria", TramiteReservas.CantReservaCria));
                cmd.Parameters.Add(new SqlParameter("@ReservaEmbrion", TramiteReservas.ReservaEmbrion));
                cmd.Parameters.Add(new SqlParameter("@CantReservaEmbrion", TramiteReservas.CantReservaEmbrion));
                cmd.Parameters.Add(new SqlParameter("@ReservaVientre", TramiteReservas.ReservaVientre));
                cmd.Parameters.Add(new SqlParameter("@ReservaMaterialGenetico", TramiteReservas.ReservaMaterialGenetico));
                cmd.Parameters.Add(new SqlParameter("@ReservaDerechoClonar", TramiteReservas.ReservaDerechoClonar));
                cmd.Parameters.Add(new SqlParameter("@AutorizaCompradorClonar", TramiteReservas.AutorizaCompradorClonar));
                cmd.Parameters.Add(new SqlParameter("@IdUsuarioAudi", TramiteReservas.IdUsuarioAudi));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
        }
    }
}
