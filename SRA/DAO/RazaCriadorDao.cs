﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AccesoBD;
using Entities;

namespace DAO
{
    public class RazaCriadorDao
    {
        private static string nameEspace = "DAO.RazaCriadorDao.";

        public static DataSet GetDatosRazaCriador(int raza_id, int cria_nume)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetDatosRazaCriador");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@raza_id", raza_id));
                cmd.Parameters.Add(new SqlParameter("@cria_nume", cria_nume));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "GetDatosRazaCriador, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }
    }
}
