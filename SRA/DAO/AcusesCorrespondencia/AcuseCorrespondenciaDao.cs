using System;
using System.Text;
using System.Data;
using AccesoBD;
using System.Collections;
using Entities.AcusesCorrespondencia;
using System.Data.SqlClient;
using System.Configuration;

namespace DAO.AcusesCorrespondencia
{
	/// <summary>
	/// Summary description for AcuseCorrespondenciaDao.
	/// </summary>
	public class AcuseCorrespondenciaDao
	{
		/// <summary>
		/// metodo que retorna un array list de objetos EnviosEmailComprobantesEntity
		/// </summary>
		/// <param name="varHashCodeEnvio"></param>
		/// <returns>ArrayList<EnviosEmailComprobantesEntity></returns>
		public static ArrayList GetComprobantesEnviados(string varHashCodeEnvio)
		{
			ArrayList respuesta = new ArrayList();
			
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_FAC_GetComprobantesEnviados ");
			pstrProc.Append("@varHashCodeEnvio=");
			pstrProc.Append(_DataBase.darFormatoSQl(varHashCodeEnvio));

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						EnviosEmailComprobantesEntity dato = new EnviosEmailComprobantesEntity();

						dato.intIdEnviosEmail = Convert.ToInt32(dr["intIdEnviosEmail"]);
						dato.intIdComprobante = Convert.ToInt32(dr["intIdComprobante"]);
						dato.intIdCliente = Convert.ToInt32(dr["intIdCliente"]);
						dato.varCliente = dr["varCliente"].ToString();
						dato.intIdTipoComprobante = Convert.ToInt32(dr["intIdTipoComprobante"]);
						dato.varComprobNume = dr["varComprobNume"].ToString();
						if (dr["dateFecIngreComp"] != DBNull.Value) { dato.dateFecIngreComp = Convert.ToDateTime(dr["dateFecIngreComp"]); }
						dato.varEmailCliente = dr["varEmailCliente"].ToString();
						dato.bitEnviado = Convert.ToBoolean(dr["bitEnviado"]);
						if (dr["dateFechaEnvio"] != DBNull.Value) { dato.dateFechaEnvio = Convert.ToDateTime(dr["dateFechaEnvio"]); }
						dato.bitAcuse = Convert.ToBoolean(dr["bitAcuse"]);
						if (dr["dateFechaAcuse"] != DBNull.Value) { dato.dateFechaAcuse = Convert.ToDateTime(dr["dateFechaAcuse"]); }
						dato.varHostAcuse = dr["varHostAcuse"].ToString();
						dato.hashCodeEnvio = dr["hashCodeEnvio"].ToString();

						respuesta.Add(dato);
					}
			}
			catch (Exception ex)
			{
				string Msg = "AcuseCorrespondenciaDao.GetComprobantesEnviados: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que marca en la base de datos los envios como recibidos por el 
		/// cliente
		/// </summary>
		/// <param name="varIdEnviosEmail"></param>
		/// <returns></returns>
		public static bool MarcarComprobantesComoRecibidos(string varIdEnviosEmail, string varHostAcuse)
		{
			bool respuesta = false;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_FAC_MarcarComprobantesComoRecibidos ");
			pstrProc.Append("@varIdEnviosEmail=");
			pstrProc.Append(_DataBase.darFormatoSQl(varIdEnviosEmail));
			pstrProc.Append(",@varHostAcuse=");
			pstrProc.Append(_DataBase.darFormatoSQl(varHostAcuse));
			
			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToBoolean(ds.Tables[0].Rows[0]["resultado"]);
			}
			catch (Exception ex)
			{
				string[] Msg = { "AcuseCorrespondenciaDao.MarcarComprobantesComoRecibidos: ", pstrProc.ToString()
								   , " registro el siguiente error: " + ex.Message };
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}


		/// <summary>
		/// metodo que retorna un array list de objetos EnviosEmailComprobantesVtosEntity
		/// </summary>
		/// <param name="varHashCodeEnvio"></param>
		/// <returns>ArrayList<EnviosEmailComprobantesEntity></returns>
		public static ArrayList GetComprobantesVtosEnviados(string varHashCodeEnvio)
		{
			ArrayList respuesta = new ArrayList();
			
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_FAC_GetComprobantesVtosEnviados ");
			pstrProc.Append("@varHashCodeEnvio=");
			pstrProc.Append(_DataBase.darFormatoSQl(varHashCodeEnvio));

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						EnviosEmailComprobantesVtosEntity dato = new EnviosEmailComprobantesVtosEntity();

						dato.intIdEnviosEmail = Convert.ToInt32(dr["intIdEnviosEmail"]);
						dato.intIdComprobante = Convert.ToInt32(dr["intIdComprobante"]);
						dato.intIdCliente = Convert.ToInt32(dr["intIdCliente"]);
						dato.varCliente = dr["varCliente"].ToString();
						dato.intIdTipoComprobante = Convert.ToInt32(dr["intIdTipoComprobante"]);
						dato.varComprobNume = dr["varComprobNume"].ToString();
						if (dr["dateFecIngreComp"] != DBNull.Value) { dato.dateFecIngreComp = Convert.ToDateTime(dr["dateFecIngreComp"]); }
						if (dr["dateFecVto"] != DBNull.Value) { dato.dateFecVto = Convert.ToDateTime(dr["dateFecVto"]); }
						dato.decImpNetoComp = Convert.ToDecimal(dr["decImpNetoComp"]);
						dato.decSaldoComp = Convert.ToDecimal(dr["decSaldoComp"]);
						dato.decImpACtaCte = Convert.ToDecimal(dr["decImpACtaCte"]);
						dato.varEmailCliente = dr["varEmailCliente"].ToString();
						dato.bitEnviado = Convert.ToBoolean(dr["bitEnviado"]);
						if (dr["dateFechaEnvio"] != DBNull.Value) { dato.dateFechaEnvio = Convert.ToDateTime(dr["dateFechaEnvio"]); }
						dato.bitAcuse = Convert.ToBoolean(dr["bitAcuse"]);
						if (dr["dateFechaAcuse"] != DBNull.Value) { dato.dateFechaAcuse = Convert.ToDateTime(dr["dateFechaAcuse"]); }
						dato.varHostAcuse = dr["varHostAcuse"].ToString();
						dato.hashCodeEnvio = dr["hashCodeEnvio"].ToString();

						respuesta.Add(dato);
					}
			}
			catch (Exception ex)
			{
				string Msg = "AcuseCorrespondenciaDao.GetComprobantesVtosEnviados: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que marca en la base de datos los envios como recibidos por el 
		/// cliente de ARQ_EnviosEmailComprobantesVtos
		/// </summary>
		/// <param name="varIdEnviosEmail"></param>
		/// <returns></returns>
		public static bool MarcarComprobantesVtosComoRecibidos(string varIdEnviosEmail, string varHostAcuse)
		{
			bool respuesta = false;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_FAC_MarcarComprobantesVtosComoRecibidos ");
			pstrProc.Append("@varIdEnviosEmail=");
			pstrProc.Append(_DataBase.darFormatoSQl(varIdEnviosEmail));
			pstrProc.Append(",@varHostAcuse=");
			pstrProc.Append(_DataBase.darFormatoSQl(varHostAcuse));
			
			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToBoolean(ds.Tables[0].Rows[0]["resultado"]);
			}
			catch (Exception ex)
			{
				string[] Msg = { "AcuseCorrespondenciaDao.MarcarComprobantesVtosComoRecibidos: ", pstrProc.ToString()
								   , " registro el siguiente error: " + ex.Message };
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}


		/// <summary>
		/// metodo que retorna un array list de objetos EnviosEstadoCuentasClienteEntity
		/// </summary>
		/// <param name="varHashCodeEnvio"></param>
		/// <returns>ArrayList<EnviosEmailComprobantesEntity></returns>
		public static ArrayList GetEstadoDeCuentaClienteEnviados(string varHashCodeEnvio)
		{
			ArrayList respuesta = new ArrayList();
			
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_FAC_GetEstadoDeCuentaClienteEnviados ");
			pstrProc.Append("@varHashCodeEnvio=");
			pstrProc.Append(_DataBase.darFormatoSQl(varHashCodeEnvio));

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						EnviosEstadoCuentasClienteEntity dato = new EnviosEstadoCuentasClienteEntity();

						dato.intIdEnviosEmail = Convert.ToInt32(dr["intIdEnviosEmail"]);
						dato.intIdCliente = Convert.ToInt32(dr["intIdCliente"]);
						dato.varCliente = dr["varCliente"].ToString();
						dato.varEmailCliente = dr["varEmailCliente"].ToString();
						dato.decDeudaCtaCte = Convert.ToDecimal(dr["decDeudaCtaCte"]);
						dato.decDeudaVencida = Convert.ToDecimal(dr["decDeudaVencida"]);
						dato.decCtaCteInteres = Convert.ToDecimal(dr["decCtaCteInteres"]);
						dato.decOtrasCtas = Convert.ToDecimal(dr["decOtrasCtas"]);
						dato.decOtrasCtasVenc = Convert.ToDecimal(dr["decOtrasCtasVenc"]);
						dato.decDeudaSocial = Convert.ToDecimal(dr["decDeudaSocial"]);
						dato.decDeudaSocialVencida = Convert.ToDecimal(dr["decDeudaSocialVencida"]);
						dato.decDeudaSocialInteres = Convert.ToDecimal(dr["decDeudaSocialInteres"]);
						dato.decLimiteSaldo = Convert.ToDecimal(dr["decLimiteSaldo"]);
						dato.decTotalProforma = Convert.ToDecimal(dr["decTotalProforma"]);
						dato.decTotalProformaRRGG = Convert.ToDecimal(dr["decTotalProformaRRGG"]);
						dato.decTotalProformaLabo = Convert.ToDecimal(dr["decTotalProformaLabo"]);
						dato.decTotalProformaExpo = Convert.ToDecimal(dr["decTotalProformaExpo"]);
						dato.decTotalProformaOtras = Convert.ToDecimal(dr["decTotalProformaOtras"]);
						dato.bitEnviado = Convert.ToBoolean(dr["bitEnviado"]);
						if (dr["dateFechaEnvio"] != DBNull.Value) { dato.dateFechaEnvio = Convert.ToDateTime(dr["dateFechaEnvio"]); }
						dato.bitAcuse = Convert.ToBoolean(dr["bitAcuse"]);
						if (dr["dateFechaAcuse"] != DBNull.Value) { dato.dateFechaAcuse = Convert.ToDateTime(dr["dateFechaAcuse"]); }
						dato.varHostAcuse = dr["varHostAcuse"].ToString();
						dato.hashCodeEnvio = dr["hashCodeEnvio"].ToString();
        
						respuesta.Add(dato);
					}
			}
			catch (Exception ex)
			{
				string Msg = "AcuseCorrespondenciaDao.GetEstadoDeCuentaClienteEnviados: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que marca en la base de datos los envios como recibidos por el 
		/// cliente de ARQ_EnviosEmailEstadoDeCuentaCliente
		/// </summary>
		/// <param name="varIdEnviosEmail"></param>
		/// <returns></returns>
		public static bool MarcarEstadoDeCuentaClienteComoRecibidos(string varIdEnviosEmail, string varHostAcuse)
		{
			bool respuesta = false;
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec p_FAC_MarcarEstadoDeCuentaClienteComoRecibidos ");
			pstrProc.Append("@varIdEnviosEmail=");
			pstrProc.Append(_DataBase.darFormatoSQl(varIdEnviosEmail));
			pstrProc.Append(",@varHostAcuse=");
			pstrProc.Append(_DataBase.darFormatoSQl(varHostAcuse));
			
			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
					respuesta = Convert.ToBoolean(ds.Tables[0].Rows[0]["resultado"]);
			}
			catch (Exception ex)
			{
				string[] Msg = { "AcuseCorrespondenciaDao.MarcarEstadoDeCuentaClienteComoRecibidos: ", pstrProc.ToString()
								   , " registro el siguiente error: " + ex.Message };
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		/// <summary>
		/// Sobrecarga
		/// metodo que obtiene el esatdo de cuenta del cliente
		/// actualizado 
		/// </summary>
		/// <param name="intIdCliente"></param>
		/// <returns>EnviosEstadoCuentasClienteEntity</returns>
		public static EnviosEstadoCuentasClienteEntity GetEstadoDeCuentaCliente(int intIdCliente )
		{
			return GetEstadoDeCuentaCliente(intIdCliente, System.DateTime.Now);
		}

		/// <summary>
		/// metodo que obtiene el esatdo de cuenta del cliente
		/// actualizado 
		/// </summary>
		/// <param name="intIdCliente"></param>
		/// <returns>EnviosEstadoCuentasClienteEntity</returns>
		public static EnviosEstadoCuentasClienteEntity GetEstadoDeCuentaCliente(int intIdCliente , DateTime dateFechaEstado)
		{
			EnviosEstadoCuentasClienteEntity respuesta = new EnviosEstadoCuentasClienteEntity();
			
			StringBuilder pstrProc = new StringBuilder();

			DataBase _DataBase = new DataBase();

			pstrProc.Append("exec clientes_estado_de_saldos_consul ");
			pstrProc.Append("@clie_id=");
			pstrProc.Append(intIdCliente);

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{	
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						respuesta.intIdCliente = intIdCliente;
						respuesta.decDeudaCtaCte =(dr["saldo_cta_cte"] != DBNull.Value)? Convert.ToDecimal(dr["saldo_cta_cte"]): 0;
						respuesta.decDeudaVencida = (dr["saldo_cta_cte_venc"] != DBNull.Value)? Convert.ToDecimal(dr["saldo_cta_cte_venc"]): 0;
						respuesta.decCtaCteInteres = (dr["Interes_Cte"] != DBNull.Value)? Convert.ToDecimal(dr["Interes_Cte"]): 0;
						respuesta.decOtrasCtas = (dr["OtrasCuentas"] != DBNull.Value)? Convert.ToDecimal(dr["OtrasCuentas"]): 0;
						respuesta.decOtrasCtasVenc = (dr["OtrasCuentasVenc"] != DBNull.Value)? Convert.ToDecimal(dr["OtrasCuentasVenc"]): 0;
						respuesta.decDeudaSocial = (dr["saldo_social"] != DBNull.Value)? Convert.ToDecimal(dr["saldo_social"]): 0;
						respuesta.decDeudaSocialVencida = (dr["saldo_social_venc"] != DBNull.Value)? Convert.ToDecimal(dr["saldo_social_venc"]): 0;
						respuesta.decDeudaSocialInteres = (dr["Interes_Soc"] != DBNull.Value)? Convert.ToDecimal(dr["Interes_Soc"]): 0;
						respuesta.decLimiteSaldo = (dr["lim_saldo"] != DBNull.Value)? Convert.ToDecimal(dr["lim_saldo"]): 0;
						respuesta.decTotalProforma = (dr["TotalProforma"] != DBNull.Value)? Convert.ToDecimal(dr["TotalProforma"]): 0;
						respuesta.decTotalProformaRRGG = (dr["TotalProformaRRGG"] != DBNull.Value)? Convert.ToDecimal(dr["TotalProformaRRGG"]): 0;
						respuesta.decTotalProformaLabo = (dr["TotalProformaLabo"] != DBNull.Value)? Convert.ToDecimal(dr["TotalProformaLabo"]): 0;
						respuesta.decTotalProformaExpo = (dr["TotalProformaExpo"] != DBNull.Value)? Convert.ToDecimal(dr["TotalProformaExpo"]): 0;
						respuesta.decTotalProformaOtras = (dr["TotalProformaOtras"] != DBNull.Value)? Convert.ToDecimal(dr["TotalProformaOtras"]): 0;
						respuesta.decImporteACuenta = ((dr["imp_a_cta_cta_cte"] != DBNull.Value)? Convert.ToDecimal(dr["imp_a_cta_cta_cte"]): 0)
							+ ((dr["imp_a_cta_cta_social"] != DBNull.Value)?Convert.ToDecimal(dr["imp_a_cta_cta_social"]):0);
						break;
					}
				}
				// Dario 2013-11-19 Detalle de deuda separado por actividad
				pstrProc = new StringBuilder();
				pstrProc.Append("exec p_FAC_GetEstadoDeCuentaClientePorActividad ");
				pstrProc.Append("@intIdCliente=");
				pstrProc.Append(intIdCliente);
				pstrProc.Append(",@Fecha=");
				if (dateFechaEstado != _DataBase.DateTimeDBNull()) 
				{pstrProc.Append(_DataBase.darFormatoSQl(((DateTime)dateFechaEstado).ToString("yyyy/MM/dd")));} 	
				
				ds = clsSQLServer.gExecuteQuery(_DataBase.mstrConn, pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{	
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						EnviosEstadoCuentasClientePorActividadEntity item = new EnviosEstadoCuentasClientePorActividadEntity();
						
						item.intIdActividad = (dr["intIdActividad"] != DBNull.Value)? Convert.ToInt32 (dr["intIdActividad"]): 0;;
						item.varActividadDesc = dr["varActividadDesc"].ToString();;
						item.decImpoNeto =  (dr["decImpoNeto"] != DBNull.Value)? Convert.ToDecimal(dr["decImpoNeto"]): 0;;
						item.decImpoNetoVencida =  (dr["decImpoNetoVencida"] != DBNull.Value)? Convert.ToDecimal(dr["decImpoNetoVencida"]): 0;;
						item.decIntereses =  (dr["decIntereses"] != DBNull.Value)? Convert.ToDecimal(dr["decIntereses"]): 0;;
						item.decSobreTasasAlVto =  (dr["decSobreTasasAlVto"] != DBNull.Value)? Convert.ToDecimal(dr["decSobreTasasAlVto"]): 0;;
						item.varDatosSectorActividad = dr["varDatosSectorActividad"].ToString();;
						
						respuesta.EstadoCuentasClientePorActividad.Add(item);
					}
				}


			}
			catch (Exception ex)
			{
				string Msg = "AcuseCorrespondenciaDao.GetEstadoDeCuentaCliente: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message);
			}

			return respuesta;
		}

		
	}
}
