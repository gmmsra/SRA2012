﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AccesoBD;
using Entities;
//using Common;   

namespace DAO
{
    public class TramitesVendedoresCompradoresDao
    {
        public static DataSet ConsTramitesCompradoresVendedores(int tram_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_ConsTramitesCompradoresVendedores");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_id", tram_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return ds;
        }

        public static DataSet GetTramitesCompradoresVendedores(int tram_vc_id)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetTramitesCompradoresVendedores");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_vc_id", tram_vc_id));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return ds; 
        }

        public static bool GetExisteTramiteCompradorVendedor(int tram_id, int tram_vc_clie_id, int? tram_vc_cria_id, string tram_vc_tran_tipo)
        {
            bool existe = false;
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetExisteTramiteCompradorVendedor");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tram_id", tram_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_clie_id", tram_vc_clie_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_cria_id", tram_vc_cria_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tran_tipo", tram_vc_tran_tipo));
                existe = Convert.ToInt32(AccesoBD.clsSQLServer.ExecuteCommandScalar(cmd)) > 0 ? true : false;
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            } 
            return existe;
        }

        public static void UpdateTramiteCompradorVendedor(TramitesVendedoresCompradoresEntity TramiteVendedorComprador)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_UpdateTramiteCompradorVendedor");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tram_audi_fecha", TramiteVendedorComprador.tram_vc_tram_audi_fecha));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tram_audi_user", TramiteVendedorComprador.tram_vc_tram_audi_user));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_porcentaje", TramiteVendedorComprador.tram_vc_porcentaje));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_obser", TramiteVendedorComprador.tram_vc_obser));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tram_id", TramiteVendedorComprador.tram_vc_tram_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_clie_id", TramiteVendedorComprador.tram_vc_clie_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_cria_id", TramiteVendedorComprador.tram_vc_cria_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tran_tipo", TramiteVendedorComprador.tram_vc_tran_tipo));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            } 
        }

        public static void InsertTramiteCompradorVendedor(TramitesVendedoresCompradoresEntity TramiteVendedorComprador)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_InsertTramiteCompradorVendedor");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tram_id", TramiteVendedorComprador.tram_vc_tram_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tram_audi_fecha", TramiteVendedorComprador.tram_vc_tram_audi_fecha));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tram_audi_user", TramiteVendedorComprador.tram_vc_tram_audi_user));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_clie_id", TramiteVendedorComprador.tram_vc_clie_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_cria_id", TramiteVendedorComprador.tram_vc_cria_id));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_porcentaje", TramiteVendedorComprador.tram_vc_porcentaje));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_tran_tipo", TramiteVendedorComprador.tram_vc_tran_tipo));
                cmd.Parameters.Add(new SqlParameter("@tram_vc_obser", TramiteVendedorComprador.tram_vc_obser));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
        }

        public static void DeleteTramiteCompradorVendedor(TramitesVendedoresCompradoresEntity TramiteVendedorComprador)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_DeleteTramiteCompradorVendedor");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tram_vc_id", TramiteVendedorComprador.tram_vc_id));
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
        }

        public static DataSet ConsTransferenciaProductoPropietariosQueVenden(int prdt_id, DateTime? operFecha, int solovigente)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("p_rg_transferencia_producto_propietarios_q_venden");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@prdt_id", prdt_id));
                cmd.Parameters.Add(new SqlParameter("@operFecha", operFecha));
                cmd.Parameters.Add(new SqlParameter("@solovigente", solovigente));
                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(BaseDAO.ReturnExceptionMessage(System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType.Namespace, System.Reflection.MethodBase.GetCurrentMethod().Name.ToString(), ex.Message)));
            }
            return ds; 
        }



    }
}
