using System;
using System.Data;
using AccesoBD;
using System.Text;
using System.Data.SqlClient;

namespace DAO
{

	public class ClientesDao
	{
		/// <summary>
		/// metodo que retorna la validacion del cliente tipo y nro de docu mas cuil si es empresa
		/// </summary>
		public static string ValidaClienteDatosAFIP(int IdCliente)
		{
			string respuesta = string.Empty;
			StringBuilder pstrProc = new StringBuilder();

			pstrProc.Append("exec p_FAC_ValidaClienteDatosAFIP ");
			pstrProc.Append("@IdCliente=");
			pstrProc.Append(IdCliente); 
            
			DataBase oBase = new DataBase();

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					foreach (DataRow dr in ds.Tables[0].Rows)
					{
						if(dr["varRespuesta"] != System.DBNull.Value){respuesta = dr["varRespuesta"].ToString();}
						
						break;
					}
				}
			}
			catch (Exception ex)
			{
				string Msg = "ClientesDao.ValidaClienteDatosAFIP: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}

		/// <summary>
		/// metodo que registra le solicitud de debito auto de un socio
		/// </summary>
		/// <param name="varNroSocio"></param>
		/// <param name="varNomYApe"></param>
		/// <param name="vareMail"></param>
		/// <param name="varTelefono"></param>
		/// <param name="varObserv"></param>
		/// <param name="varHostAcuse"></param>
		/// <returns></returns>
		public static bool RegistarSolicitudDebitoAutoSocio(string varNroSocio, string varNomYApe, string vareMail, string varTelefono, string varObserv,string varHost)
		{
			DataBase oBase = new DataBase();
			bool respuesta = false;
			StringBuilder pstrProc = new StringBuilder();
			//@varNroSocio nvarchar(50),@varNomYApe nvarchar(250),@vareMail nvarchar(250),@varTelefono nvarchar(250),@varObserv nvarchar(250),@varHost nvarchar(250)
			pstrProc.Append("exec p_SOC_RegistarSolicitudDebitoAutoSocio ");
			pstrProc.Append("@varNroSocio=");
			pstrProc.Append((new DataBase()).darFormatoSQl(varNroSocio)); 
			pstrProc.Append(",@varNomYApe=");
			pstrProc.Append((new DataBase()).darFormatoSQl(varNomYApe)); 
			pstrProc.Append(",@vareMail=");
			pstrProc.Append((new DataBase()).darFormatoSQl(vareMail)); 
			pstrProc.Append(",@varTelefono=");
			pstrProc.Append((new DataBase()).darFormatoSQl(varTelefono)); 
			pstrProc.Append(",@varObserv=");
			pstrProc.Append((new DataBase()).darFormatoSQl(varObserv)); 
			pstrProc.Append(",@varHost=");
			pstrProc.Append((new DataBase()).darFormatoSQl(varHost)); 

			try
			{
				DataSet ds = clsSQLServer.gExecuteQuery(oBase.mstrConn , pstrProc.ToString());

				if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
				{
					Int32 idInsert = Convert.ToInt32(ds.Tables[0].Rows[0]["idInsert"].ToString());	
					if(idInsert > 0)
						respuesta = true;
				}
			}
			catch (Exception ex)
			{
				string Msg = "ClientesDao.RegistarSolicitudDebitoAutoSocio: " + pstrProc.ToString() + " registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
			}
			return respuesta;
		}
	}
}
