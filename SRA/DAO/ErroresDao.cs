﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AccesoBD;
using Entities;

namespace DAO
{
    public class ErroresDao
    {
        private static string nameEspace = "DAO.ErroresDao.";

        public static DataSet Rg_Denuncias_Errores_Busq(int? Id, int? Proce, string Titulo)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("rg_denuncias_errores_busq");
                cmd.CommandType = CommandType.StoredProcedure;
                if (Id == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@id", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@id", Id));
                }
                if (Proce == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@proce", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@proce", Proce));
                }
                cmd.Parameters.Add(new SqlParameter("@titulo", Titulo));

                ds = AccesoBD.clsSQLServer.GetDataSetSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "ConsErrores, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
            return ds;
        }

        public static void Cs_Validaciones_Errores_Grabar(string Tabla, int? Id, int? TramiteId, int Rmen_id, int Audi_user, int? SalvadoActivado, int? Manual, string Obser, int BitErroresSalvar)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("cs_validaciones_errores_grabar");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tabla", Tabla));
                cmd.Parameters.Add(new SqlParameter("@id", Id));
                if (TramiteId == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@TramiteId", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@TramiteId", TramiteId));
                }
                cmd.Parameters.Add(new SqlParameter("@rmen_id", Rmen_id));
                cmd.Parameters.Add(new SqlParameter("@audi_user", Audi_user));
                if (SalvadoActivado == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@SalvadoActivado", DBNull.Value ));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@SalvadoActivado", SalvadoActivado));
                }
                if (Manual == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@manual", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@manual", Manual));
                }
                cmd.Parameters.Add(new SqlParameter("@obser", Obser));
                cmd.Parameters.Add(new SqlParameter("@bitErroresSalvar", BitErroresSalvar));

                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Cs_Validaciones_Errores_Grabar, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }

        public static void Cs_Validaciones_Errores_Salvar(string Tabla, int Id, int Err_Id, int Baja_Manual_User, int Salvar, int? BitErroresSalvar)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("cs_validaciones_errores_salvar");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@tabla", Tabla));
                cmd.Parameters.Add(new SqlParameter("@id", Id));
                cmd.Parameters.Add(new SqlParameter("@err_id", Err_Id));
                cmd.Parameters.Add(new SqlParameter("@baja_manual_user", Baja_Manual_User));
                cmd.Parameters.Add(new SqlParameter("@salvar", Salvar));
                if (BitErroresSalvar == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@bitErroresSalvar", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@bitErroresSalvar", BitErroresSalvar));
                }
                AccesoBD.clsSQLServer.ExecuteCommandNonQuery(cmd);
            }
            catch (Exception ex)
            {
                clsError.gManejarError(new Exception(nameEspace + "Cs_Validaciones_Errores_Salvar, registró el siguiente error: " + ex.Message));
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}
