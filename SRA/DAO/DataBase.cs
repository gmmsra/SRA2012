﻿using System;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using AccesoBD;

namespace DAO
{
	public class DataBase
	{
		static string dstAppCode = ConfigurationSettings.AppSettings["AppCode"];
		static string gstrServ = clsFormatear.gDesencriptar(ConfigurationSettings.AppSettings["conServ"],dstAppCode);
		static string gstrBase = clsFormatear.gDesencriptar(ConfigurationSettings.AppSettings["conBase"],dstAppCode);
		static string gstrUsua = clsFormatear.gDesencriptar(ConfigurationSettings.AppSettings["conUser"],dstAppCode);
		static string gstrPass = clsFormatear.gDesencriptar(ConfigurationSettings.AppSettings["conPass"],dstAppCode);
//		static string gstrServ = ConfigurationSettings.AppSettings["conServ"];
//		static string gstrBase = ConfigurationSettings.AppSettings["conBase"];
//		static string gstrUsua = ConfigurationSettings.AppSettings["conUser"];
//		static string gstrPass = ConfigurationSettings.AppSettings["conPass"];		
		static string gstrTimeOut = ConfigurationSettings.AppSettings["conTimeOut"];		
		
		public String mstrConn
		{
			get
			{
				return   "server=" + gstrServ + ";database=" + gstrBase + ";"
					+ "uid=" + gstrUsua + ";"
					+ "pwd=" + gstrPass + ";"
					+ "Connection Timeout=" + gstrTimeOut;
			}
		}

		/// <summary>
		/// Permite formatear un string que se envia por parametro a SQL Server
		/// </summary>
		/// <param name="valor"></param>
		/// <returns></returns>
		public string darFormatoSQl(string valor)
		{
			if (valor != null)
				valor = valor.Replace("'", " "); // elimito ' en los nombres y direcciones

			return "'" + valor + "'";
		}

		/// <summary>
		/// permite poner el formato decimal al valor
		/// </summary>
		/// <param name="valor"></param>
		/// <returns></returns>
		public string darFormatoDecimal(string valor)
		{
			return valor.Replace(',', '.');
		}


		/// <summary>
		/// metodo que retorna el valor datetime dbnull
		/// </summary>
		/// <returns></returns>
		public DateTime DateTimeDBNull()
		{
			return System.DateTime.MinValue;
		}

		/// <summary>
		/// metodo que retorna el Int 32 dbnull
		/// </summary>
		/// <returns></returns>
		public Int32 IntDBNull()
		{
			return 0;
		}

		/// <summary>
		/// metodo que retorna el string dbnull
		/// </summary>
		/// <returns></returns>
		public string StringDBNull()
		{
			return string.Empty;
		}
		/// <summary>
		/// metodo que retorna el decimal dbnull
		/// </summary>
		/// <returns></returns>
		public Decimal DecimalDBnull()
		{
			return -1;
		}
		/// <summary>
		/// metodo wrapper de la clase AccesoBD.gManejarError para no refereniar
		/// la clase en el business 
		/// </summary>
		/// <param name="ex"></param>
		/// <param name="Msg"></param>
		public void gManejarError(Exception ex, string Msg)
		{
			clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
		}
		public void gManejarErrorSinMsgBox(Exception ex, string Msg)
		{
			clsError.gManejarErrorSinMsgBox(new Exception(ex.Message + " - " + Msg , ex));
		}

		/// <summary>
		/// matodo que logea sin error por pantalla
		/// </summary>
		/// <param name="Msg"></param>
		public void gLoggear(string Msg)
		{
			clsError.gLoggear( Msg);
		}
	}
}
     
 
