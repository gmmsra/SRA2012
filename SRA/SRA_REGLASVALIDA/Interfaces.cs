using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Interfaces
{ 
	public class TipoArchivoWingest
	{
		private string tipoArchivo;

		public string TipoArchivo
		{
			get{return tipoArchivo;}
			set{tipoArchivo = value;}
		}
	}
	
	public class Importacion
	{
		static DataSet mdsValiNaci;   
		static DataSet mdsValiServ;
		static DataSet mdsValiTeDenu;
		static string mstrCmd;
		static string mstrConn;
		static string mstrUserId; 
		const string mstrTablaServiDenuncias = "rg_servi_denuncias";
		const string mstrTablaServiDenunciasDeta = "rg_servi_denuncias_deta";
		const string mstrTablaProductosInscrip = "rg_productos_inscrip";
		const string mstrTablaProductosBaja = "rg_productos";
		const string mstrTablaImportaciones = "rg_importaciones_wingest";
		const string mstrTablaTeDenu = "te_denun";
		const string mstrTablaTeDenuDeta = "te_denun_deta";

		// posiciones de los campos
		// static int mintTNOV = 0; 
		// static int mintNPRO = 1;
		static int mintRAZA = 2;
		static int mintPROP = 3;
		// static int mintLNEA = 4;
		// static int mintFOLI = 5;
		// static int mintRGST = 6;
		//static int mintVARI = 7;
		// static int mintCDPX = 8;
		static int mintHBAE = 9;
		// static int mintNROC = 10;
		static int mintRPEX = 11;
		static int mintSEXO = 12;
		static int mintASOC = 13;
		static int mintPREF = 14;
		static int mintNOMB = 15;
		static int mintFNAC = 16;
		static int mintRAZP = 17;
		// static int mintASOP = 18;
		static int mintRGSP = 19;
		// static int mintVARP = 20;
		static int mintHBAP = 21;
		static int mintRPPD = 22;
		static int mintRAZM = 23;
		// static int mintASOM = 24;
		static int mintRGSM = 25;
		// static int mintVARM = 26;
		static int mintHBAM = 27;
		static int mintRPMD = 28;
		//static int mintANLS = 29;
		//static int mintRANA = 30;
		//static int mintOBSI = 31;
		//static int mintFOBS = 32;
		static int mintCBAJ = 33;
		static int mintFBAJ = 34;
		static int mintCRIA = 35;
		//static int mintTRAN = 36;
		//static int mintFTRF = 37;
		//static int mintCDOS = 38;
		//static int mintSDOS = 39;
		//static int mintCCRI = 40;
		//static int mintSCRI = 41;
		//static int mintDONA = 42;
		//static int mintDADO = 43;
		//static int mintSIET = 44;
		//static int mintMELL = 45;
		//static int mintTRNS = 46;
		static int mintFREC = 47;
		static int mintFIMP = 48;
		static int mintTSER = 49;
		static int mintFSER = 50;
		static int mintFSE2 = 51;
		//static int mintFANA = 52;
		//static int mintEMBA = 53;
		static int mintCOLO = 54;
		static int mintFMOV = 55;
		//static int mintDENU = 56;
		//static int mintCPEL = 57;
		//static int mintCCUE = 58;
		//static int mintCCAB = 59;
		//static int mintCMIE = 60;
		static int mintRPNU = 61;
		//static int mintPROVA = 62;
		//static int mintUSADO = 63;
		//static int mintPENA = 64;
		//static int mintPEDE = 65;
		//static int mintPEFI = 66;
		//static int mintEPNA = 67;
		//static int mintEPDE = 68;
		//static int mintEPFI = 69;
		//static int mintFIDX = 70;
		//static int mintPART = 71;
		//static int mintLCRI = 72;
		static int mintSFOL = 73;
		static int mintSLIN = 74;
		//static int mintSPRO = 75;
		static int mintRAPO = 76;
		static int mintRPPO = 77;
		//static int mintANTE = 78;
		//static int mintPURE = 79;
		//static int mintTANA = 80;
		//static int mintSIGLA = 81;
		//static int mintSIGPA = 82;
		//static int mintSIGMA = 83;
		//static int mintNROT = 84;
		//static int mintRGTT = 85;
		//static int mintNLOT = 86;
		//static int mintAPPD = 87;			

		public static void Inicializar(string pstrConn, string pstrUserId, DataSet pdsValiNaci, DataSet pdsValiServ, DataSet pdsValiTeDenu)
		{
			mstrConn = pstrConn;
			mstrUserId = pstrUserId;
			mdsValiNaci = pdsValiNaci;
			mdsValiServ = pdsValiServ;
			mdsValiTeDenu = pdsValiTeDenu;
		}
		   
		private static string mFormatFecha(string pstrFecha) 
		{
            string lstrFecha = pstrFecha.Replace(" 12:00:00 AM", "").Replace(" 12:00:00 a.m.", "");
			return (lstrFecha);
		} 

		public static string ValidarRaza(DataRow odrDato, string pstrRaza ) 
		{
			string lstrMsg  = "";
			if (!(odrDato[mintRAZA].ToString() == odrDato[mintRAZP].ToString() && odrDato[mintRAZP].ToString() == pstrRaza))
				lstrMsg = "Error al informar las razas: Madre=" + odrDato[mintRAZA].ToString() + " - Padre: " + odrDato[mintRAZP].ToString() + " - Archivo: " + pstrRaza;
			return (lstrMsg);
		} 

		public static string ValidarPropietario(DataRow odrDato, string pstrProp ) 
		{
			string lstrMsg  = "";
			if (odrDato[mintPROP].ToString() != pstrProp)
				lstrMsg = "Error al informar el propietario: " + odrDato[mintPROP].ToString() + " <> " + pstrProp;
			return (lstrMsg);
		} 

		public static string FechaProcesoImplante(DataRow odrDato) 
		{
			string lstrFecha = mFormatFecha(odrDato[mintFREC].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			return(lstrFecha);
		}

		public static string FechaProcesoServicio(DataRow odrDato) 
		{
			string lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			return(lstrFecha);
		} 

		public static string FechaProcesoNacimiento(DataRow odrDato) 
		{
			string lstrFecha = mFormatFecha(odrDato[mintFNAC].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			return(lstrFecha);
		} 

		public static string ProcesarCabecera(string pstrArchivo, string pstrFechaRecep,string pTipoArch)
		{
			string lstrCmd = "";
			string lstrId = "";
			string lstrFecha = "";
			//esto es una prueba para ver si trago bien la version del SourSafe
			lstrCmd = "exec rg_importaciones_alta";
			lstrCmd = lstrCmd + " '" + pstrArchivo + "'";
			lstrFecha = mFormatFecha(pstrFechaRecep);
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			lstrCmd = lstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
			lstrCmd = lstrCmd + "," + mFormatArg(mstrUserId, SqlDbType.Int);
			lstrCmd = lstrCmd + ",'" + pTipoArch + "'";

			lstrId = mEjecutar(mstrConn, lstrCmd);



			return (lstrId);
		}

		public static int AuditarSesionesXUsuario(Objetos.Auditoria objAudi, string mstrConnexion)
		{
			string lstrCmd = "";
			
			lstrCmd = " exec auditoria_sesiones_x_usuario_alta ";
			lstrCmd += " @ausu_user_id=" +  mFormatArg(objAudi.UserID.ToString(), SqlDbType.Int);
			lstrCmd += ",@ausu_nomb_form=" +  mFormatArg(objAudi.NombreForm, SqlDbType.VarChar);
			lstrCmd += ",@ausu_modulo=" +  mFormatArg(objAudi.Modulo, SqlDbType.VarChar);
			lstrCmd += ",@ausu_sistema=" +  mFormatArg(objAudi.Sistema, SqlDbType.VarChar);

			SqlConnection myConnection = new SqlConnection(mstrConnexion);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = lstrCmd;
			cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;

			myConnection.Open();
			int lintRecAff = cmdExecCommand.ExecuteNonQuery();
			myConnection.Close();
			
			return(lintRecAff);
		}

		public static int AuditarLogueoXUsuario(Objetos.AuditoriaLogUser objAudi, string mstrConnexion)
		{
			string lstrCmd = "";
			
			lstrCmd = " exec auditoria_logueo_x_usuario_alta ";
			lstrCmd += " @aulu_user_id=" +  mFormatArg(objAudi.UserID.ToString(), SqlDbType.Int);
			lstrCmd += ",@aulu_IP=" +  mFormatArg(objAudi.IP, SqlDbType.VarChar);

			SqlConnection myConnection = new SqlConnection(mstrConnexion);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = lstrCmd;
			cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;

			myConnection.Open();
			int lintRecAff = cmdExecCommand.ExecuteNonQuery();
			myConnection.Close();
			
			return(lintRecAff);
		}

		public static int EliminarCabecera(string lstrId)
		{
			string lstrCmd = "";
			
			lstrCmd = "exec rg_importaciones_baja";
			lstrCmd = lstrCmd + " @impo_id = " + lstrId;

			SqlConnection myConnection = new SqlConnection(mstrConn);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = lstrCmd;
			cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;

			myConnection.Open();
			int lintRecAff = cmdExecCommand.ExecuteNonQuery();
			myConnection.Close();
			
			return(lintRecAff);
		}

		public static string ProcesarImplante(DataRow odrDato) 
		{
				string lstrId  = "";
				string lstrFecha  = "";

				mstrCmd = "exec cs_" + mstrTablaTeDenu + "_wingest_actu";
				lstrFecha = mFormatFecha(odrDato[mintFREC].ToString());
				if (!mValidarFecha(lstrFecha))
					lstrFecha = "";
				mstrCmd = mstrCmd + " " + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @tede_recu_fecha 
				mstrCmd = mstrCmd + "," + "null";                                                                     // @tede_gest_dias 
				mstrCmd = mstrCmd + "," + "null";																  	  // @tede_embr_cong				
				mstrCmd = mstrCmd + "," + "null";																	  // @tede_embr_fres 
				lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
				if (!mValidarFecha(lstrFecha))
					lstrFecha = "";
				mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @tede_serv_fecha 
				lstrFecha = mFormatFecha(odrDato[mintFIMP].ToString());
				if (!mValidarFecha(lstrFecha))
					lstrFecha = "";
				mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @tede_impl_fecha 
				mstrCmd = mstrCmd + ",0";																	          // @tede_impl_embr_cong
				mstrCmd = mstrCmd + ",1";																		      // @tede_impl_embr_fres
				mstrCmd = mstrCmd + "," + mFormatArg(mstrUserId, SqlDbType.Int);                                      // @tede_audi_user
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintPROP].ToString(), SqlDbType.Int);					  // @prop_nume
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAPO].ToString(), SqlDbType.Int);					  // @raza_codi_rece
				mstrCmd = mstrCmd + ",null";																		  // @tedd_carv
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPPO].ToString(), SqlDbType.VarChar);					  // @tedd_tatu
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAZA].ToString(), SqlDbType.Int);					  // @raza_codi
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPPD].ToString(), SqlDbType.VarChar);					  // @rp_padre
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAP].ToString(), SqlDbType.Int);					  // @sra_nume_padre
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPEX].ToString(), SqlDbType.VarChar);					  // @rp_madre
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAE].ToString(), SqlDbType.Int);					  // @sra_nume_madre
				mstrCmd = mstrCmd + ",null";																		  // @tede_imcr_id
				mstrCmd = mstrCmd + ",null";																		  // @tede_auim_id 
				mstrCmd = mstrCmd + ",null";																		  // @tedd_rcti_id
								
				lstrId = mEjecutar(mstrConn, mstrCmd);

				return (lstrId);
		} 

		public static int ProcesarBaja(DataRow odrDato) 
		{
			string lstrSexo = "";
			string lstrFecha = "";

			mstrCmd = "exec " + mstrTablaImportaciones + "_productos_baja";
			//Raza.
			mstrCmd += " @raza_nume=" + mFormatArg(odrDato[mintRAZA].ToString(), SqlDbType.Int);
			//HBA.
			mstrCmd += ",@prdt_sra_nume=" + mFormatArg(odrDato[mintHBAE].ToString(), SqlDbType.Int);		
			//Sexo.
			lstrSexo = odrDato[mintSEXO].ToString();
			switch (lstrSexo)
			{
				case "1":	// macho
				{ lstrSexo = "1"; break; }
				case "2":	// hembra	
				{ lstrSexo = "0"; break; }
				default:
				{ lstrSexo = ""; break; }
			}
			mstrCmd += ",@prdt_sexo=" + mFormatArg(lstrSexo, SqlDbType.Int);
			//RP.
			mstrCmd += ",@prdt_rp='" + mFormatArg(odrDato[mintRPEX].ToString(), SqlDbType.Int) + "'";
			//Usuario.
			mstrCmd += ",@prdt_audi_user=" + mFormatArg(mstrUserId, SqlDbType.Int);
			//Baja Motivo.
			mstrCmd += ",@prdt_bamo_id=" + mFormatArg(odrDato[mintCBAJ].ToString(), SqlDbType.Int);
			//Fecha Baja.
			lstrFecha = mFormatFecha(odrDato[mintFBAJ].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			mstrCmd += ",@prdt_baja_fecha=" + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);

			//Ejecuta el Stored Procedure.
			SqlConnection myConnection = new SqlConnection(mstrConn);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = mstrCmd;
			cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;

			myConnection.Open();
			int lintRecAff = cmdExecCommand.ExecuteNonQuery();
			myConnection.Close();
			
			return(lintRecAff);
		} 


		public static string ProcesarRecuperacion(DataRow odrDato) 
		{
			string lstrId  = "";
			string lstrFecha  = "";

			mstrCmd = "exec cs_" + mstrTablaTeDenu + "_wingest_actu";
			lstrFecha = mFormatFecha(odrDato[mintFREC].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			mstrCmd = mstrCmd + " " + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @tede_recu_fecha 
			mstrCmd = mstrCmd + "," + "null";                                                                     // @tede_gest_dias 
			mstrCmd = mstrCmd + "," + "null";																  	  // @tede_embr_cong				
			mstrCmd = mstrCmd + "," + "null";																	  // @tede_embr_fres 
			lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @tede_serv_fecha 
			lstrFecha = mFormatFecha(odrDato[mintFIMP].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @tede_impl_fecha 
			mstrCmd = mstrCmd + ",0";																	          // @tede_impl_embr_cong
			mstrCmd = mstrCmd + ",1";																		      // @tede_impl_embr_fres
			mstrCmd = mstrCmd + "," + mFormatArg(mstrUserId, SqlDbType.Int);                                      // @tede_audi_user
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintPROP].ToString(), SqlDbType.Int);					  // @prop_nume
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAPO].ToString(), SqlDbType.Int);					  // @raza_codi_rece
			mstrCmd = mstrCmd + ",null";																		  // @tedd_carv
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPPO].ToString(), SqlDbType.VarChar);					  // @tedd_tatu
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAZA].ToString(), SqlDbType.Int);					  // @raza_codi
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPPD].ToString(), SqlDbType.VarChar);					  // @rp_padre
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAP].ToString(), SqlDbType.Int);					  // @sra_nume_padre
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPEX].ToString(), SqlDbType.VarChar);					  // @rp_madre
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAE].ToString(), SqlDbType.Int);					  // @sra_nume_madre
			mstrCmd = mstrCmd + ",null";																		  // @tede_imcr_id
			mstrCmd = mstrCmd + ",null";																		  // @tede_auim_id 
			mstrCmd = mstrCmd + ",null";																		  // @tedd_rcti_id
								
			lstrId = mEjecutar(mstrConn, mstrCmd);

			return (lstrId);
		} 

		public static string ProcesarServicio(DataRow odrDato, string pstrFechaRecep) 
		{
			string lstrServId  = "";
			string lstrFecha  = "";

			mstrCmd = "exec cs_" + mstrTablaServiDenuncias + "_wingest_actu";
			mstrCmd = mstrCmd + " " + "null";                                                                                                        // @sede_id
			lstrFecha = mFormatFecha(odrDato[mintFMOV].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @sede_fecha
			lstrFecha = mFormatFecha(pstrFechaRecep);
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
			mstrCmd = mstrCmd + "," + "null";                                                                     // @sede_clie_id
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintPROP].ToString(), SqlDbType.Int);                    // @cria_nume
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintSFOL].ToString(), SqlDbType.Int);                    // @sede_folio_nume
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAZA].ToString(), SqlDbType.Int);                    // @raza_codi
			mstrCmd = mstrCmd + "," + mFormatArg(mstrUserId, SqlDbType.Int);                                      // @sede_audi_user
			mstrCmd = mstrCmd + "," + "null";                                                                     // @sede_espe_id
			lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
			if (!mValidarFecha(lstrFecha))
				lstrFecha = "";
			mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);;                            // @sede_servi_desde
			lstrFecha = mFormatFecha(odrDato[mintFSE2].ToString());
			if (!mValidarFecha(lstrFecha)) 
				lstrFecha = "";
			mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                             // @sede_servi_hasta
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintTSER].ToString(), SqlDbType.Int);                    // @sede_seti_id
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPPD].ToString(), SqlDbType.VarChar);                // @sede_padre_rp
			mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAP].ToString(), SqlDbType.VarChar);                // @sede_padre_sra_nume
			mstrCmd = mstrCmd + "," + "null";                                                                                                        // @sede_padre_id
			mstrCmd = mstrCmd + "," + "null";                                                                                                        // @sede_baja_fecha

			lstrServId = mEjecutar(mstrConn, mstrCmd);

			return (lstrServId);
		} 

		public static string  ProcesarServicioDetalle(DataRow odrDato, string pstrServId ) 
		{
				string lstrServDetaId  = "";
				string lstrFecha  = "";

				mstrCmd = "exec cs_" + mstrTablaServiDenunciasDeta + "_wingest_actu";
				mstrCmd = mstrCmd + " " + "null";                                                                                                   // @sdde_id 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintSLIN].ToString(), SqlDbType.Int);                    // @sdde_item_nume
				mstrCmd = mstrCmd + "," + mFormatArg(pstrServId, SqlDbType.Int);                                                            // @sdde_sede_id
				lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
				if (!mValidarFecha(lstrFecha)) 
					lstrFecha = "";
				mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                                                   // @sdde_servi_desde
				lstrFecha = mFormatFecha(odrDato[mintFSE2].ToString());
				if (!mValidarFecha(lstrFecha)) 
					lstrFecha = "";
				mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                                                   // @sdde_servi_hasta
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintTSER].ToString(), SqlDbType.Int);                    // @sdde_seti_id
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAP].ToString(), SqlDbType.Int);                    // @sdde_padre_sra_nume
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPPD].ToString(), SqlDbType.VarChar);                // @sdde_padre_rp
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAM].ToString(), SqlDbType.Int);                    // @sdde_madre_sra_nume
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPMD].ToString(), SqlDbType.VarChar);                // @sdde_madre_rp
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @sdde_madre_asoc_id
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @sdde_padre_asoc_id
				mstrCmd = mstrCmd + "," + "''";                                                                                                         // @sdde_obser
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @sdde_baja_fecha
				mstrCmd = mstrCmd + "," + mFormatArg(mstrUserId, SqlDbType.Int);                                                            // @sdde_audi_user
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @prdt_cria_id
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @espe
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAZA].ToString(), SqlDbType.Int);                    // @raza_codi

				lstrServDetaId = mEjecutar(mstrConn, mstrCmd);

				return (lstrServDetaId);
		}

		public static string ValidarDatosImportados(DataRow odrDato) 
		{
			bool lbooRet  = false;
			string lstrCmd = "";
			string lstrMsg = "";
			string lstrFecha = "";
			
			lstrCmd = "exec " + mstrTablaImportaciones + "_busq";			
			lstrCmd = lstrCmd +" '"+ odrDato["tnov"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["npro"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["raza"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["prop"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["lnea"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["foli"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["rgst"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["hbae"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["nroc"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["rpex"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["sexo"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["asoc"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["pref"].ToString();
			lstrCmd = lstrCmd +"','"+ replaceCharSpec(odrDato["nomb"].ToString()) + "'";
			lstrFecha = mFormatFecha(odrDato["fnac"].ToString());
			if (lstrFecha=="") 
				lstrCmd = lstrCmd +",''";
			else
				lstrCmd = lstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);

			lstrCmd = lstrCmd +",'"+ odrDato["razp"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["asop"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["rgsp"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["hbap"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["rppd"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["razm"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["asom"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["rgsm"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["hbam"].ToString();
			lstrCmd = lstrCmd +"','"+ odrDato["rpmd"].ToString() + "'";
			lstrFecha = mFormatFecha(odrDato["FREC"].ToString());
			if (lstrFecha=="") 
				lstrCmd = lstrCmd +",''";
			else
				lstrCmd = lstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);

			lstrCmd = lstrCmd +",'"+ odrDato["TSER"].ToString() + "'";
			lstrFecha = mFormatFecha(odrDato["FSER"].ToString());
			if (lstrFecha=="") 
				lstrCmd = lstrCmd +",''";
			else
				lstrCmd = lstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);

			lstrFecha = mFormatFecha(odrDato["FSE2"].ToString());
			if (lstrFecha=="") 
				lstrCmd = lstrCmd +",''";
			else
				lstrCmd = lstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
		
			lstrFecha = mFormatFecha(odrDato["FMOV"].ToString());
			if (lstrFecha=="") 
				lstrCmd = lstrCmd +",''";
			else
				lstrCmd = lstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
			
			lbooRet = mBuscarRegiImportacion(mstrConn, lstrCmd);
			if (lbooRet)
			{
				lstrMsg = "Registro Duplicado";
				return (lstrMsg);
			}
			else
			{
				return (lstrMsg);
			}
		}

		public static string  ProcesarNacimiento(DataRow odrDato, string pstrServDetaId, string pstrFechaValor) 
		{
				string lstrNaciId  = "";
				string lstrFecha  = "";
				string lstrSexo = "";
				string lstrNomb = "";

				mstrCmd = "exec cs_" + mstrTablaProductosInscrip + "_wingest_actu";
				mstrCmd = mstrCmd + " " + "null";                                                                                                       // @pdin_id
                lstrFecha = mFormatFecha(odrDato[mintFMOV].ToString());
                if (!mValidarFecha(lstrFecha)) 
                    lstrFecha = "";
                mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
				lstrFecha = mFormatFecha(pstrFechaValor);
				if (!mValidarFecha(lstrFecha)) 
					lstrFecha = "";
				mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);// @pdin_insc_fecha 
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @pdin_pipl_id 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintCRIA].ToString(), SqlDbType.Int);                    // @cria_nume
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @pdin_esbl_id 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAZA].ToString(), SqlDbType.Int);                    // @raza_codi 
				lstrFecha = mFormatFecha(odrDato[mintFNAC].ToString());
				if (!mValidarFecha(lstrFecha)) 
					lstrFecha = "";
				mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                                                   // @pdin_naci_fecha 
				lstrNomb = odrDato[mintPREF].ToString().Trim();
				if (lstrNomb != "")
					lstrNomb = lstrNomb + " ";
				lstrNomb = lstrNomb + odrDato[mintNOMB].ToString().Trim();        // prefijio + nombre
				if (lstrNomb.Length > 35) 
					lstrNomb = lstrNomb.Substring(0, 35);
				mstrCmd = mstrCmd + "," + mFormatArg(lstrNomb, SqlDbType.VarChar);                                                          // @pdin_nomb 
				lstrSexo = odrDato[mintSEXO].ToString();
				switch (lstrSexo)
				{
					case "1":	// macho
						{ lstrSexo = "1"; break; }
					case "2":	// hembra	
						{ lstrSexo = "0"; break; }
					default:
						{ lstrSexo = ""; break; }
				}
				mstrCmd = mstrCmd + "," + mFormatArg(lstrSexo, SqlDbType.Int);                                                              // @pdin_sexo 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPNU].ToString(), SqlDbType.Int);                    // @pdin_rp_nume 
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @pdin_baja_fecha 
				lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
				if (!mValidarFecha(lstrFecha)) 
					lstrFecha = "";
				mstrCmd = mstrCmd + "," + mFormatArg(lstrFecha, SqlDbType.SmallDateTime);                                                   // @pdin_servi_fecha 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPPD].ToString(), SqlDbType.VarChar);                // @pdin_padre_rp 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAP].ToString(), SqlDbType.Int);                    // @pdin_padre_sra_nume 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRPMD].ToString(), SqlDbType.VarChar);                // @pdin_madre_rp 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintHBAM].ToString(), SqlDbType.Int);                    // @pdin_madre_sra_nume 
				if (pstrServDetaId != "") 
					mstrCmd = mstrCmd + "," + mFormatArg(pstrServDetaId, SqlDbType.Int);                                                    // @pdin_sdde_id 
				else
					mstrCmd = mstrCmd + "," + "null";                                                                                                    // @pdin_sdde_id 
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @pdin_cria 
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @pdin_parto 
				mstrCmd = mstrCmd + "," + "''";                                                                                                          // @pdin_obser 
				mstrCmd = mstrCmd + "," + mFormatArg(mstrUserId, SqlDbType.Int);                                                            // @pdin_audi_user 
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @espe 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintCOLO].ToString(), SqlDbType.Int);                    // @pdin_a_pela_id 
				mstrCmd = mstrCmd + "," + "null";                                                                                                        // @pdin_ticr_id 
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintPROP].ToString(), SqlDbType.Int);                    // @prop_nume
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintPREF].ToString(), SqlDbType.VarChar);                // @pref_desc
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintASOC].ToString(), SqlDbType.Int);                    // @asoc_codi
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAZP].ToString(), SqlDbType.Int);					  // Raza_padre
				mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRAZM].ToString(), SqlDbType.Int);
                mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRGSP].ToString(), SqlDbType.VarChar);
                mstrCmd = mstrCmd + "," + mFormatArg(odrDato[mintRGSM].ToString(), SqlDbType.VarChar);
			
			
				lstrNaciId = mEjecutar(mstrConn, mstrCmd);

				return (lstrNaciId);
		} 

		public static string ProcesarImportacion(DataRow odrDato, string pstrIdCabe, string pstrTipoArch) 
		{
			string lstrImpo  = "";
			string lstrFecha = "";
			string lstrFechaImpl = "";
			string lstrFechaBaja = "";
						
			mstrCmd = "exec " + mstrTablaImportaciones + "_alta";
			mstrCmd = mstrCmd + " " + mFormatArg(pstrIdCabe, SqlDbType.Int);
			mstrCmd = mstrCmd +",'" + odrDato["tnov"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["npro"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["raza"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["prop"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["lnea"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["foli"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["rgst"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["hbae"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["nroc"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["rpex"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["sexo"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["asoc"].ToString();
			mstrCmd = mstrCmd +"',''"+ mFormatArg(odrDato["pref"].ToString(), SqlDbType.VarChar); //odrDato["pref"].ToString();
			mstrCmd = mstrCmd +"'','"+ replaceCharSpec(odrDato["nomb"].ToString()) + "'";
			lstrFecha = mFormatFecha(odrDato["fnac"].ToString());
			if (lstrFecha=="") 
				mstrCmd = mstrCmd +",''";
			else
				mstrCmd = mstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
			
			mstrCmd = mstrCmd +",'"+ odrDato["razp"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["asop"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["rgsp"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["hbap"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["rppd"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["razm"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["asom"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["rgsm"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["hbam"].ToString();
			mstrCmd = mstrCmd +"','"+ odrDato["rpmd"].ToString() + "'";
			lstrFecha = mFormatFecha(odrDato["FREC"].ToString());
			if (lstrFecha=="") 
				mstrCmd = mstrCmd +",''";
			else
				mstrCmd = mstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);

			//Si el tipo de Archivo es "R":
			//Si est� vac�o o contiene un cero, guardo un 6 por defecto.
			if (pstrTipoArch != "R")
				mstrCmd = mstrCmd +",'"+ odrDato["TSER"].ToString() + "'";
			else
			{
				if (odrDato["TSER"].ToString() != "0")
					mstrCmd = mstrCmd +",'"+ odrDato["TSER"].ToString() + "'";
				else
					mstrCmd = mstrCmd +",'6'";
			}

			lstrFecha = mFormatFecha(odrDato["FSER"].ToString());
			if (lstrFecha=="") 
				mstrCmd = mstrCmd +",''";
			else
				mstrCmd = mstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
			
			lstrFecha = mFormatFecha(odrDato["FSE2"].ToString());
			if (lstrFecha=="") 
				mstrCmd = mstrCmd +",''";
			else 
				mstrCmd = mstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
			
			lstrFecha = mFormatFecha(odrDato["FMOV"].ToString());
			if (lstrFecha=="") 
				mstrCmd = mstrCmd +",''";
			else
				mstrCmd = mstrCmd +","+ mFormatArg(lstrFecha, SqlDbType.SmallDateTime);
			
			mstrCmd = mstrCmd + "," + mFormatArg(mstrUserId, SqlDbType.Int);                                                            // @pdin_audi_user 

			//Fecha Implante.
			lstrFechaImpl = mFormatFecha(odrDato["FIMP"].ToString());
			if (lstrFechaImpl=="") 
				mstrCmd = mstrCmd +",''";
			else
				mstrCmd = mstrCmd +","+ mFormatArg(lstrFechaImpl, SqlDbType.SmallDateTime);
			
			//RP de la Receptora.
			mstrCmd = mstrCmd +",'"+ odrDato["RPPO"].ToString() + "'";
			//LCRI.
			mstrCmd = mstrCmd +",'"+ odrDato["LCRI"].ToString() + "'";
			//Variedad.
			mstrCmd = mstrCmd +",'"+ odrDato["VARI"].ToString() + "'";
			//C�digo Baja.
			if (odrDato["CBAJ"].ToString() == "")
				mstrCmd = mstrCmd +",''";
			else
				mstrCmd = mstrCmd +","+ odrDato["CBAJ"].ToString();
			
			//Fecha Baja.
			lstrFechaBaja = mFormatFecha(odrDato["FBAJ"].ToString());
			if (lstrFechaBaja=="") 
				mstrCmd = mstrCmd +",''";
			else
				mstrCmd = mstrCmd +","+ mFormatArg(lstrFechaBaja, SqlDbType.SmallDateTime);

			//Variedad Padre.
			mstrCmd = mstrCmd +",'"+ odrDato["VARP"].ToString() + "'";
			//CDOS.
			mstrCmd = mstrCmd +","+ odrDato["CDOS"].ToString();
			//DENU.
			mstrCmd = mstrCmd +","+ odrDato["DENU"].ToString();
			
			//Si el tipo de Archivo es "R":
			if(pstrTipoArch == "R")
			{
				//NENV.
				mstrCmd = mstrCmd +","+ odrDato["NENV"].ToString();
				//Raza Padre Dos.
				mstrCmd = mstrCmd +","+ odrDato["RAZP2"].ToString();
				//Asoc. Padre Dos.
				mstrCmd = mstrCmd +","+ odrDato["ASOP2"].ToString();
				//Registro Padre Dos.
				mstrCmd = mstrCmd +",'"+ odrDato["RGSP2"].ToString() + "'";
				//Variedad Padre Dos.
				mstrCmd = mstrCmd +",'"+ odrDato["VARP2"].ToString() + "'";
				//HBA Padre Dos.
				mstrCmd = mstrCmd +","+ odrDato["HBAP2"].ToString();
				//RP Padre Dos.
				mstrCmd = mstrCmd +",'"+ odrDato["RPPD2"].ToString() + "'";		
			}

			lstrImpo = mEjecutar(mstrConn, mstrCmd);

			return (lstrImpo);
		} 

		public static string  ValidarFechas(DataRow odrDato, string pstrTabla ) 
		{
				string lstrMsg  = "";
				string lstrFecha  = "";

				switch (pstrTabla)
				{
					case mstrTablaServiDenuncias:
					{
						lstrFecha = mFormatFecha(odrDato[mintFMOV].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFSE2].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						break;
					}
					case mstrTablaServiDenunciasDeta:
					{
						lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFSE2].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
            			break;
					}
					case mstrTablaProductosInscrip:
					{
						lstrFecha = mFormatFecha(odrDato[mintFMOV].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFNAC].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						break;
					}
					case mstrTablaTeDenuDeta:
					{
						lstrFecha = mFormatFecha(odrDato[mintFMOV].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFREC].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFIMP].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						lstrFecha = mFormatFecha(odrDato[mintFSER].ToString());
						lstrMsg = FormatMensaje(lstrMsg, lstrFecha);
						break;
					}
				}

				return (lstrMsg);
		} 

		public static string  ValidarNombre(DataRow odrDato) 
		{
				string lstrMsg  = "";
				string lstrNomb  = "";

				lstrNomb = odrDato[mintPREF].ToString().Trim();
				if (lstrNomb != "")
					lstrNomb = lstrNomb + " ";
				lstrNomb = lstrNomb + odrDato[mintNOMB].ToString().Trim();
				if (lstrNomb.Length > 35)
					lstrMsg = "Nombre con m�s de 35 caracteres: " + lstrNomb;

				return (lstrMsg);
		} 

		private static string FormatMensaje(string pstrMsg , string pstrFecha ) 
		{
			string lstrMsg  = "";
			if (!mValidarFecha(pstrFecha)) 
			{
				if (pstrMsg == "")
					lstrMsg = "Fecha/s inv�lida/s: ";
				else
					lstrMsg = pstrMsg + " - ";
				lstrMsg = lstrMsg + pstrFecha;
			}
			return (lstrMsg);
		}
		private static string mFormatArg(string pstrParam, SqlDbType pstrTipo)
		{
			if (pstrParam == null)
			{
				return("null");
			}
			string lstrAux = pstrParam.Trim();
			switch (pstrTipo)
			{
				case SqlDbType.VarChar:
					if(lstrAux.IndexOf("'")==-1)
						lstrAux = "'" + lstrAux + "'";
					else
						lstrAux = "[" + lstrAux + "]";
					return(lstrAux);
				case SqlDbType.SmallDateTime:
					if (lstrAux != "")
					{
						if (lstrAux.IndexOf(" ") != -1)
							lstrAux = lstrAux.Substring(0, lstrAux.IndexOf(" "));
						System.IFormatProvider format = new System.Globalization.CultureInfo("fr-FR", true);
						DateTime datFech = System.DateTime.Parse(lstrAux, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
						lstrAux = datFech.Month.ToString()+"/"+datFech.Day.ToString()+"/"+datFech.Year.ToString();
						if (datFech.Hour != 0 || datFech.Minute != 0)
						{
							lstrAux = lstrAux + " " + datFech.Hour.ToString() + ":" + datFech.Minute.ToString();
						} 
						lstrAux = "'" + lstrAux + "'";
					}
					else
					{
						lstrAux = "null";
					}
					return(lstrAux);
				case SqlDbType.Int:
					if (lstrAux == "")
					{
						lstrAux = "0";
					}
					return(lstrAux);
				case SqlDbType.Decimal:
					if (lstrAux == "")
					{
						lstrAux = "0";
					}
					lstrAux = lstrAux.Replace(",",".");
					return(lstrAux);
				default:
					return(lstrAux);
			}
		}
		private static bool mValidarFecha(string expression)
		{
			if (expression == null || expression == "") { return true; }
			try
			{
				DateTime dateTime = DateTime.Parse(expression);
			}
			catch (FormatException)
			{
				return false;
			}

			return true;
		}	
		private static string mEjecutar(string pstrConn, string pstrProc)
		{
			if (pstrConn.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrConn");
			}
			if (pstrProc.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrProc");
			}
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = pstrProc;
			cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
			myConnection.Open();
			string lstrRet = cmdExecCommand.ExecuteScalar().ToString();
			myConnection.Close();
			return(lstrRet);
		}

		/// <summary>
		/// Este metodo ejecuta un Scalar con Transaction
		/// </summary>
		/// <param name="pTrans"></param>
		/// <param name="pstrConn"></param>
		/// <param name="pstrProc"></param>
		/// <returns>Devuelve en String el Id del Registro dado de alta</returns>
		private static string mEjecutarconTrans(SqlTransaction pTrans, SqlConnection pstrConn, string pstrProc)
		{
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = pstrConn;
			cmdExecCommand.CommandText = pstrProc;
			cmdExecCommand.CommandTimeout = pstrConn.ConnectionTimeout;
			cmdExecCommand.Transaction = pTrans;
			string lstrRet = cmdExecCommand.ExecuteScalar().ToString();
			return(lstrRet);
		}
		
		private static bool mBuscarRegiImportacion(string pstrConn, string pstrProc)
		{
			bool lbooResult = false;
			if (pstrConn.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrConn");
			}
			if (pstrProc.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrProc");
			}
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = pstrProc;
			cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
			myConnection.Open();
			SqlDataReader dr = cmdExecCommand.ExecuteReader();

			if (dr.Read())
				lbooResult = true;            
			dr.Close();
			myConnection.Close();
			return (lbooResult);
			
		}

		private static string replaceCharSpec(string pstrParam) 
		{

			StringBuilder b = new StringBuilder(pstrParam);
			b.Replace("  ", string.Empty);
			b.Replace(Environment.NewLine, string.Empty);
			b.Replace("\\t", string.Empty);
			b.Replace("'", "~");
			
			return b.ToString();

		}
	}
}
