using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using AccesoBD;


namespace ReglasValida
{
	public class Validaciones
	{
		static object _lock = new object();
		static object _lockmValidar = new object();
		static object _lockmGrabarAprobacion = new object();
		static object _lockmGrabarErrorTramite = new object();
		static object _lockgErrorSalvado = new object();
		static object _lockgLimpiarErrores = new object();
		static object _lockmGrabarError = new object();
		static object _lockmExecute = new object();
		static object _lockmObtenerEspecie = new object();
        
        
        
		//static string mstrCmd = "";
		// Nombres de tablas.
		public static string gstrTabProd = "productos";
		public static string gstrTabTram = "tramites";
		public static string gstrTabTramProd = "tramites_productos";
		public static string gstrTabProdInscPlan = "rg_productos_inscrip_planilla";
		public static string gstrTabProdInsc = "rg_productos_inscrip";
		public static string gstrTabServDenu = "rg_servi_denuncias";
		public static string gstrTabServDenuDeta = "rg_servi_denuncias_deta";
		public static string gstrTabEMBRIONES_STOCK = "embriones_stock";
		public static string gstrTabSEMEN_STOCK = "semen_stock";
		public static string gstrTabTeDenu = "te_denun";
		public static string gstrTabTeDenuDeta = "te_denun_deta";
		public static string gstrTabNaciDeclara = "rg_nacimientos_declara";
		public static string gstrTabNaciDeclaraDeta = "rg_nacimientos_declara_deta";
		public static string gstrTabPlantelMovi = "rg_plantel_movi";
		public static string gstrTabPlantelMoviDeta = "rg_plantel_movi_deta";

		const string constTabProd = "productos";
		const string constTabTram = "tramites";
		const string constTabTramProd = "tramites_productos";
		const string constTabProdInscPlan = "rg_productos_inscrip_planilla";
		const string constTabProdInsc = "rg_productos_inscrip";
		const string constTabServDenu = "rg_servi_denuncias";
		const string constTabServDenuDeta = "rg_servi_denuncias_deta";
		const string constTabEMBRIONES_STOCK = "embriones_stock";
		const string constTabSEMEN_STOCK = "semen_stock";
		const string constTabTeDenu = "te_denun";
		const string constTabTeDenuDeta = "te_denun_deta";
		const string constTabNaciDeclara = "rg_nacimientos_declara";
		const string constTabNaciDeclaraDeta = "rg_nacimientos_declara_deta";
		const string constTabPlantelMovi = "rg_plantel_movi";
		const string constTabPlantelMoviDeta = "rg_plantel_movi_deta";


		public class TiposValidacion
		{
			public static string CapturaTxt = "T";
			public static string Modulo2005 = "M";
			public static string AplicacionWeb = "W";
		}
		

		public class Procesos
		{
			public static int Nacimientos = 1;
			public static int Servicios = 2;
			public static int TransferenciaProductos = 5;
			public static int ImplantesRecuperaEmbriones = 6;
			public static int DeclaraPorcinos = 7;
			public static int PlantelPorcinos = 8;
			public static int ImportacionProductos = 9;
			public static int ExportacionProductos = 10;
			public static int ExportacionSemen = 11;
			public static int ExportacionEmbriones = 12;
			public static int ImportacionEmbriones = 13;
			public static int TransferenciaEmbriones = 14;
			public static int ImportacionSemen = 15;
			public static int TransferenciaSemen = 16;
			public static int PlantelBase = 17;
			public static int Prestamos = 18;
			public static int Embargos = 19;
			public static int ImplantesEmbriones = 20;
		}


		public class ProcesosConst
		{
			public const string Nacimientos = "1";
			public const string Servicios = "2";
			public const string TransferenciaProductos = "5";
			public const string ImplantesRecuperaEmbriones = "6";
			public const string DeclaraPorcinos = "7";
			public const string PlantelPorcinos = "8";
			public const string ImportacionProductos = "9";
			public const string ExportacionProductos = "10";
			public const string ExportacionSemen = "11";
			public const string ExportacionEmbriones = "12";
			public const string ImportacionEmbriones = "13";
			public const string TransferenciaEmbriones = "14";
			public const string ImportacionSemen = "15";
			public const string TransferenciaSemen = "16";
			public const string PlantelBase = "17";
			public const string Prestamos = "18";
			public const string Embargos = "19";
			public const string ImplantesEmbriones = "20";
		}

		public static DataSet gDeterminarReglas(string pstrConn, string pstrProce)
		{
			return (gDeterminarReglas(pstrConn, pstrProce, TiposValidacion.AplicacionWeb, "", "", "")); 
		}
		
		public static DataSet gDeterminarReglas(string pstrConn, string pstrProce, string pstrTipo)
		{
			return (gDeterminarReglas(pstrConn, pstrProce, pstrTipo, "", "", "")); 
		}

		public static DataSet gDeterminarReglas(string pstrConn, string pstrProce, string pstrTipo, string pstrFechaDesde, string pstrFechaHasta)
		{
			return (gDeterminarReglas(pstrConn, pstrProce, pstrTipo, pstrFechaDesde, pstrFechaHasta, "")); 
		}

		public static DataSet gDeterminarReglas(string pstrConn, string pstrProce, string pstrTipo, string pstrFechaDesde, string pstrFechaHasta, string varIdRaza)
		{
			string mstrCmd = "exec cs_validaciones_consul " + pstrProce + ",'" + pstrTipo + "'";
			if (pstrFechaDesde != "")
				mstrCmd = mstrCmd + "," + gFormatArg(pstrFechaDesde, SqlDbType.SmallDateTime);
			else
				mstrCmd = mstrCmd + ",null";
			if (pstrFechaHasta != "")
				mstrCmd = mstrCmd + "," + gFormatArg(pstrFechaHasta, SqlDbType.SmallDateTime);
			else
				mstrCmd = mstrCmd + ",null";

			if (varIdRaza.Trim().Length > 0)
				mstrCmd = mstrCmd + "," + varIdRaza;
			else
				mstrCmd = mstrCmd + ",null";

			SqlDataAdapter cmdExecCommand = new SqlDataAdapter(mstrCmd, pstrConn);
			DataSet ds = new DataSet();
			SqlConnection myConnection = new SqlConnection(pstrConn);
			myConnection.Open();
			cmdExecCommand.Fill(ds);
			myConnection.Close();
			return (ds);
		}

		public static string CrLf()
		{
			byte[] Bytes;
			Bytes = new byte[2];
			Bytes[0] = 13;
			Bytes[1] = 10;
			return(System.Text.Encoding.ASCII.GetString(Bytes));
		}
		
		public static void mGrabarErrorLog(Exception pExcep, string pstrSql)
		{
			lock (_lock)
			{
				string lstrArchLog = System.Configuration.ConfigurationSettings.AppSettings["conArchLog"].ToString();
				if (lstrArchLog != "")
				{
					FileStream fs = new FileStream(lstrArchLog, FileMode.OpenOrCreate, FileAccess.ReadWrite);

					StreamWriter w = new StreamWriter(fs);
					w.BaseStream.Seek(0, SeekOrigin.End);

					w.Write(CrLf() + "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + CrLf());
					w.Write(pExcep.Message + CrLf());
					w.Write(pExcep.StackTrace + CrLf());
					w.Write("RG_FORMULAS" + CrLf());
					w.Write(pstrSql + CrLf());
					w.Write("------------------------------------------------------------------------" + CrLf());

					w.Flush();
					w.Close();
				}
			}
		}
		
		public static string gFormatArg(string pstrParam, SqlDbType pstrTipo)
		{
			if (pstrParam == null)
			{
				return("null");
			}
			string lstrAux = pstrParam.Trim();
			switch (pstrTipo)
			{
				case SqlDbType.VarChar:
					if(lstrAux.IndexOf("'")==-1)
						lstrAux = "'" + lstrAux + "'";
					else
						lstrAux = "[" + lstrAux + "]";
					return(lstrAux);
				case SqlDbType.SmallDateTime:
					if (lstrAux != "")
					{
						IFormatProvider format = new System.Globalization.CultureInfo("fr-FR", true);
						if (lstrAux.Trim() == "/  /")
						{
							lstrAux = "null";
						}
						else
						{
							DateTime datFech = DateTime.Parse(lstrAux, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
							lstrAux = datFech.Month.ToString()+"/"+datFech.Day.ToString()+"/"+datFech.Year.ToString();
							lstrAux = "'" + lstrAux + "'";
						}
					}
					else
					{
						lstrAux = "null";
					}
					return(lstrAux);
				case SqlDbType.Int:
					if (lstrAux == "")
					{
						lstrAux = "0";
					}
					return(lstrAux);
				case SqlDbType.Decimal:
					if (lstrAux == "")
					{
						lstrAux = "0";
					}
					lstrAux = lstrAux.Replace(",",".");
					return(lstrAux);
				default:
					return(lstrAux);
			}
		}
		
		public static bool mValidar(string pstrConn, string pstrSql, string pstrFormId)
		{
			bool lbooVali = true;
			lock (_lockmValidar)
			{
				try
				{
					//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mValidar -: pstrSql= " + pstrSql + " * pstrFormId " + pstrFormId));
					//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception(pstrSql+";"));
					clsError.gManejarErrorSinMsgBox(new Exception("error : mValidar -: pstrSql= " + pstrSql + " * pstrFormId " + pstrFormId));
					SqlConnection myConnection = new SqlConnection(pstrConn);
					SqlCommand cmdExec = new SqlCommand();
					myConnection.Open();
					cmdExec.Connection = myConnection;
					cmdExec.CommandType = CommandType.Text;
					cmdExec.CommandText = pstrSql;
					SqlDataReader dr = cmdExec.ExecuteReader();
					while (dr.Read())
					{
						lbooVali = false;
					}
					dr.Close();
					myConnection.Close();
					return (lbooVali);
				}
				catch (Exception ex)
				{
					mGrabarErrorLog(new Exception("Se ha producido un error en la formula " + pstrFormId + ": " + ex.Message, ex), pstrSql);
					throw new Exception("Se ha producido un error en la formula " + pstrFormId + ": " + ex.Message);
					//return (lbooVali);
				}
			}
		}

		public static DataTable mGetErrorByFormulaId(string pstrConn, int pFormulaId)
		{
			DataTable dt = new DataTable("dtError");
			string mstrCmd="" ;
			try
			{
				lock (_lock)
				{
					mstrCmd = "exec GetMensajeByFormulaId " + clsSQLServer.gFormatArg(pFormulaId.ToString(), SqlDbType.VarChar);
					dt = clsSQLServer.gExecuteQuery(pstrConn, mstrCmd).Tables[0];
				}
				return dt;
			}
			catch (Exception ex)
			{
				mGrabarErrorLog(new Exception("mGetErrorByFormulaId :Se ha producido un error en la formula " + pFormulaId.ToString() + ": " + ex.Message, ex), mstrCmd);
			}
			
			return dt;
      
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="dsVali"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrProductoId"></param>
		/// <param name="pstrRaza"></param>
		/// <param name="pstrSexo"></param>
		/// <param name="pstrUsua"></param>
		/// <param name="pbooGrabarError"></param>
		/// <param name="pProceID"></param>
		/// <param name="pstrTramiteId"></param>
		/// <param name="pstrTramProdId"></param>
		/// <returns></returns>
		public static string gValidarReglas(string pstrConn, DataSet dsVali, string pstrTabla,  
			string pstrProductoId, string pstrRaza, string pstrSexo, string pstrUsua, bool pbooGrabarError,  
			string pProceID,Int32 pstrTramiteId,Int32 pstrTramProdId)
		{
			string lstrErrorId;
			string lstrErrorDesc = "";
			string lstrEspe = mObtenerEspecie(pstrConn, pstrRaza);
			string pTipoTabla="";
			//string strId = "";

			Int32 pintTramiteId = 0;
		
			/// Se setea por defecto el tramite id para el where.
			/// MF 30/07/2013
			//strId =pstrProductoId;
			
			if (pProceID.ToString()== Procesos.ExportacionProductos.ToString()||
				pProceID.ToString() == Procesos.TransferenciaProductos.ToString() ||
				pProceID.ToString() == Procesos.ImportacionProductos.ToString()) 
			{
				//strId = pstrTramProdId.ToString();
			}



			if (pProceID.ToString() == Procesos.ImplantesRecuperaEmbriones.ToString() ||
				pProceID.ToString() == Procesos.ExportacionEmbriones.ToString() ||
				pProceID.ToString() == Procesos.ImportacionEmbriones.ToString())
			{
				//strId = pstrTramiteId.ToString();
			}

			if (pProceID.ToString() == Procesos.ExportacionSemen.ToString() ||
				pProceID.ToString() == Procesos.ImportacionSemen.ToString() ||
				pProceID.ToString() == Procesos.TransferenciaSemen.ToString())
			{
				//strId = pstrTramiteId.ToString();
			}
			//Los unicos que tienen tramites productos son los que son animal en pie
			//embriones  y semen no tienen tramites productos
			//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mArmarReglaSQL -: pstrFormId " + pstrProductoId));

			lstrErrorId = Validaciones.mArmarReglaSQL(pstrConn, dsVali, pstrTabla, pstrProductoId, pstrRaza, 
				pstrSexo, ref lstrErrorDesc, pbooGrabarError, pstrUsua, lstrEspe, pProceID, 
				pstrTramiteId);

			
			if (pbooGrabarError && lstrErrorId == "")
			{
				Validaciones.mGrabarAprobacion(pstrConn, pstrTabla, pstrProductoId, pstrUsua);
				return ("");
			}
			else
			{	
				pTipoTabla=pstrTabla;
				pintTramiteId = Convert.ToInt32(pstrTramiteId);
				Validaciones.mGrabarErrorTramite(pstrConn, pTipoTabla, pstrProductoId, pintTramiteId, lstrErrorId, pstrUsua);
				return (lstrErrorDesc);
			}
		}

		public static object ConvertDataSetToXML(DataSet pDs, string pLogFile) 
		{
			string strFileName;
			string modoEjecucion;
 
			if( pDs == null )
			{
				return false;
 
			}
 
			modoEjecucion = ConfigurationSettings.AppSettings["ModoEjecucion"].ToString();
			if( (modoEjecucion == "DEBUG") )
			{
				strFileName = @"C:\TEMP\" + pLogFile + ".XML";
				if (File.Exists(strFileName))
				{
					File.Delete(strFileName);
				}

				FileStream fs = new FileStream(strFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
				StreamWriter writer = new StreamWriter(fs, Encoding.UTF8);
				pDs.WriteXml(writer, XmlWriteMode.WriteSchema);
				fs.Close();
			}
 
 
 
			return true;
		}


		public static bool gErrorSalvado(string pstrConn, string pstrTabla, string pstrId,
			string pstrErrorId, int strProcId)
		{
			bool lbooError = false;
			int lintProce = 0;

			switch (pstrTabla.ToLower())
			{
				case "rg_productos_inscrip":
				{ lintProce = Procesos.Nacimientos; break; }
				case "rg_servi_denuncias":
				{ lintProce = Procesos.Servicios; break; }
					//				case "tramites_productos":
					//				{ lintProce = Procesos.TransferenciaProductos; break; }
				case "te_denun_deta":
				{ lintProce = Procesos.ImplantesRecuperaEmbriones; break; }

				default:
				{
					lintProce = strProcId; break;
				}
			}

			if (lintProce == 0)
				return (false);

			string mstrCmd = "exec rg_denuncias_errores_busq " + pstrId + "," + lintProce.ToString();

			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = mstrCmd;
			lock (_lockgErrorSalvado)
			{
				SqlDataReader dr = cmdExec.ExecuteReader();
				while (dr.Read())
				{
					if (Convert.ToInt32(dr.GetValue(dr.GetOrdinal("err"))) == Convert.ToInt32(pstrErrorId))
						lbooError = (dr.GetValue(dr.GetOrdinal("audi_user")) != DBNull.Value && dr.GetValue(dr.GetOrdinal("audi_user")).ToString() != "");
					if (lbooError)
						return (lbooError);
				}
				dr.Close();
				myConnection.Close();
			}
			return (lbooError);
		}
		

		/// <summary>
		/// sobrecarga con transaccion
		/// </summary>
		/// <param name="lTransac"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrId"></param>
		/// <param name="pstrErrorId"></param>
		/// <param name="strProcId"></param>
		/// <returns></returns>
		public static bool gErrorSalvado(SqlTransaction lTransac , string pstrTabla, string pstrId,
			string pstrErrorId, int strProcId)
		{
			bool lbooError = false;
			int lintProce = 0;

			switch (pstrTabla.ToLower())
			{
				case "rg_productos_inscrip":
				{ lintProce = Procesos.Nacimientos; break; }
				case "rg_servi_denuncias":
				{ lintProce = Procesos.Servicios; break; }
					//				case "tramites_productos":
					//				{ lintProce = Procesos.TransferenciaProductos; break; }
				case "te_denun_deta":
				{ lintProce = Procesos.ImplantesRecuperaEmbriones; break; }

				default:
				{
					lintProce = strProcId; break;
				}
			}

			if (lintProce == 0)
				return (false);

			string mstrCmd = "exec rg_denuncias_errores_busq " + pstrId + "," + lintProce.ToString();

			SqlCommand cmdExec = new SqlCommand(mstrCmd, lTransac.Connection, lTransac);
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = mstrCmd;
			lock (_lockgErrorSalvado)
			{
				SqlDataReader dr = cmdExec.ExecuteReader();
				while (dr.Read())
				{
					if (Convert.ToInt32(dr.GetValue(dr.GetOrdinal("err"))) == Convert.ToInt32(pstrErrorId))
						lbooError = (dr.GetValue(dr.GetOrdinal("audi_user")) != DBNull.Value && dr.GetValue(dr.GetOrdinal("audi_user")).ToString() != "");
					if (lbooError)
						return (lbooError);
				}
				dr.Close();
			}
			return (lbooError);
		}
		
		public static string mObtenerEspecie(string pstrConn, string pstrRaza)
		{		
			string lstrEspe = "";

			string mstrCmd = "exec razas_consul " + pstrRaza;
			lock (_lockmObtenerEspecie)
			{
				SqlConnection myConnection = new SqlConnection(pstrConn);
				SqlCommand cmdExec = new SqlCommand();
				myConnection.Open();
				cmdExec.Connection = myConnection;
				cmdExec.CommandType = CommandType.Text;
				cmdExec.CommandText = mstrCmd;
				SqlDataReader dr = cmdExec.ExecuteReader();
				while (dr.Read())
				{
					lstrEspe = dr.GetValue(dr.GetOrdinal("raza_espe_id")).ToString();
				}
				dr.Close();
				myConnection.Close();
			}
			return (lstrEspe);
		}

		public static void gLimpiarErrores(string pstrConn, string pstrTabla, string pstrId)
		{
			string mstrCmd = "exec cs_validaciones_errores_limpiar ";
			mstrCmd = mstrCmd + " '" + pstrTabla + "'";
			mstrCmd = mstrCmd + "," + pstrId;
			mExecute(pstrConn, mstrCmd);
		}

		/// <summary>
		/// Dario 2014-06-12
		/// metodo sobrecarga con SqlTransaction
		/// </summary>
		/// <param name="lTransac"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrId"></param>
		public static void gLimpiarErrores(SqlTransaction lTransac , string pstrTabla, string pstrId)
		{
			bool bitdeadlockedDetected = true;
			while (bitdeadlockedDetected)
			{
				try
				{
					string mstrCmd = "exec cs_validaciones_errores_limpiar ";
					mstrCmd = mstrCmd + " '" + pstrTabla + "'";
					mstrCmd = mstrCmd + "," + pstrId;
					mExecute(lTransac, mstrCmd);

					bitdeadlockedDetected = false;
				}
				catch (Exception ex)
				{
					string msg = ex.InnerException + ex.Message;
					if (msg.IndexOf("deadlocked")>0)
						bitdeadlockedDetected = true;
					else
						throw new Exception(msg);
				}
			}
		}

		private static void mGrabarAprobacion(string pstrConn, string pstrTabla, string pstrId,
			string pstrUsua)
		{
			//lock (_lockmGrabarAprobacion)
		{
			string mstrCmd = "exec cs_validaciones_aprobacion_grabar ";
			mstrCmd = mstrCmd + " '" + pstrTabla + "'";
			mstrCmd = mstrCmd + "," + pstrId;
			mstrCmd = mstrCmd + "," + pstrUsua;
			mExecute(pstrConn, mstrCmd);
		}
		}
		/// <summary>
		/// sobrecarga con transaccion
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrId"></param>
		/// <param name="pstrUsua"></param>
		private static void mGrabarAprobacion(SqlTransaction lTransac, string pstrTabla, string pstrId,
			string pstrUsua)
		{
			//lock (_lockmGrabarAprobacion)
			{
				string mstrCmd = "exec cs_validaciones_aprobacion_grabar ";
				mstrCmd = mstrCmd + " '" + pstrTabla + "'";
				mstrCmd = mstrCmd + "," + pstrId;
				mstrCmd = mstrCmd + "," + pstrUsua;
				mExecute(lTransac, mstrCmd);
			}
		}

		public static string mArmarReglaSQL(string pstrConn, DataSet dsVali, string pstrTabla,  
			string pstrId, string pstrRaza, string pstrSexo, ref string pstrErrorDesc, 
			bool pbooGrabarError, string pstrUsua, string pstrEspe, string pProceID, 
			Int32 pTramiteId)
		{
			string lstrPK = "";
			string lstrForm = "";
			string lstrRaza = "";
			string lstrEspe = "";
			string lstrSexo = "";
			string lstrMiniValor = "";
			string lstrMiniTabla = "";
			string lstrMiniFK = "";
			string lstrMiniTablaFK = "";
			string lstrMaxiValor = "";
			string lstrMaxiTabla = "";
			string lstrMaxiFK = "";
			string lstrMaxiTablaFK = "";
			string lstrSql = "";
			string lstrErrorId = "";
			string lstrErrorDesc = "";
			string lstrCampo = "";
			string lstrFrom = "";
			string lstrWhere = "";
			bool lbooObli = false;
			bool lbooManual = false;
			string lstrResuErrorId = "";
			string lstrAgru = "";
			string lstrOrde = "";
			string lstrUltiAgruError = "";
			string lstrRegla = "";
			string lstrAlias = "";
			string lstrFormId = "";
			int intProceID = 0;

			lstrPK = "prdt_id";
			lstrFrom = pstrTabla;
			lstrWhere = "";
			intProceID = Convert.ToInt16(pProceID);
			pstrErrorDesc = "";

			#region tablas
			if (pstrTabla == gstrTabServDenu)
			{
				lstrAlias = "c.";
				lstrPK = "sede_id";
				lstrFrom = gstrTabServDenu + " c (nolock) inner join " + gstrTabServDenuDeta + " d (nolock) on c.sede_id = d.sdde_sede_id ";
				//lstrWhere = "1=1";
			}
			if (pstrTabla == gstrTabEMBRIONES_STOCK )
			{
				lstrAlias = "c.";
				lstrPK = "tesk_tram_id";
				lstrFrom = pstrTabla + " c (nolock) inner join " + " TRAMITES d (nolock) on c.tesk_tram_id = d.tram_id ";
				//lstrWhere = "1=1";
			}
			if (pstrTabla == gstrTabSEMEN_STOCK )
			{
				lstrAlias = "c.";
				lstrPK = "sest_tram_id";
				lstrFrom = pstrTabla + " c (nolock) inner join " + " TRAMITES d (nolock) on c.sest_tram_id = d.tram_id ";
				//lstrWhere = "1=1";
			}

			if (pstrTabla == gstrTabServDenuDeta)
			{
				lstrAlias = "d."; 
				lstrPK = "sdde_id";
				lstrFrom = gstrTabServDenu + " c (nolock) inner join " + gstrTabServDenuDeta + " d (nolock) on c.sede_id = d.sdde_sede_id ";
				//lstrWhere = "1=1";
			}
			if (pstrTabla == gstrTabTram)
			{
				lstrAlias = "c."; 
				lstrPK = "tram_id";
				lstrFrom = gstrTabTram + " c (nolock) inner join rg_tramites_tipos t (nolock) on c.tram_ttra_id = t.ttra_id inner join tramites_productos tp (nolock) on tram_id = tp.trpr_tram_id ";
				//lstrWhere = "1=1";

			}
			if (pstrTabla == gstrTabTramProd)
			{
				lstrAlias = "p."; 
				lstrPK = "trpr_id";
				lstrFrom = gstrTabTramProd + " p (nolock) inner join " + gstrTabTram + " t (nolock) on p.trpr_tram_id = t.tram_id  inner join " + gstrTabProd + " prod (nolock) on prod.prdt_id=p.trpr_prdt_id ";
				//lstrWhere = "1=1" ;
			}
			if (pstrTabla == gstrTabProdInsc)
			{
				lstrAlias = "p."; 
				lstrPK = "pdin_id";
				lstrFrom = gstrTabProdInsc + " i (nolock) inner join " + gstrTabProd + " p (nolock) on i.pdin_id = p.prdt_pdin_id ";
				//lstrWhere = "1=1";
			}
			if (pstrTabla == gstrTabTeDenu)
			{
				lstrAlias = "c."; 
				lstrPK = "tede_id";
				lstrFrom = gstrTabTeDenu + " c (nolock) left join " + gstrTabTeDenuDeta + " d (nolock) on  c.tede_id = d.tedd_tede_id ";
				//lstrWhere = "1=1";
			}
			if (pstrTabla == gstrTabTeDenuDeta)
			{
				lstrAlias = "d."; 
				lstrPK = "tedd_id";
				lstrFrom = gstrTabTeDenu + " c (nolock) inner join " + gstrTabTeDenuDeta + " d (nolock) on c.tede_id = d.tedd_tede_id ";
				//lstrWhere = "1=1";
			}			
			if (pstrTabla == gstrTabNaciDeclara)
			{
				lstrAlias = "c."; 
				lstrPK = "nade_id";
				lstrFrom = gstrTabNaciDeclara + " c (nolock) left join " + gstrTabNaciDeclaraDeta + " d (nolock) on c.nade_id = d.nadd_nade_id ";
				//lstrWhere = "";
			}
			if (pstrTabla == gstrTabNaciDeclaraDeta)
			{
				lstrAlias = "d."; 
				lstrPK = "nadd_id";
				lstrFrom = gstrTabNaciDeclara + " c (nolock) inner join " + gstrTabNaciDeclaraDeta + " d (nolock) on c.nade_id = d.nadd_nade_id ";
				//lstrWhere = "1=1";
			}			
			if (pstrTabla == gstrTabPlantelMovi)
			{
				lstrAlias = "c."; 
				lstrPK = "plmo_id";
				lstrFrom = gstrTabPlantelMovi + " c (nolock) left join " + gstrTabPlantelMoviDeta + " d (nolock) on c.plmo_id = d.plmd_plmo_id ";
				//lstrWhere = "1=1";
			}
			if (pstrTabla == gstrTabPlantelMoviDeta)
			{
				lstrAlias = "d."; 
				lstrPK = "plmd_id";
				lstrFrom = gstrTabPlantelMovi + " c (nolock) inner join " + gstrTabPlantelMoviDeta + " d (nolock) on c.plmo_id = d.plmd_plmo_id ";
				//lstrWhere = "1=1";
			}

			#endregion

			int cantidad = 0;

			foreach (DataRow odrDeta in dsVali.Tables[0].Rows)
			{
				lstrForm = mFormatCadena(odrDeta.ItemArray[0].ToString());
				lstrForm = lstrForm.Replace("[PROCE]",pProceID);
				lstrSexo = mFormatCadena(odrDeta.ItemArray[1].ToString());
				lstrCampo = mFormatCadena(odrDeta.ItemArray[2].ToString());
				lbooObli = mFormatBolean(odrDeta.ItemArray[3].ToString());
				lstrMiniValor = mFormatCadena(odrDeta.ItemArray[4].ToString());
				lstrMiniTabla = mFormatCadena(odrDeta.ItemArray[5].ToString());
				lstrMiniFK = mFormatCadena(odrDeta.ItemArray[6].ToString());
				lstrMiniTablaFK = mFormatCadena(odrDeta.ItemArray[7].ToString());
				lstrMaxiValor = mFormatCadena(odrDeta.ItemArray[8].ToString());
				lstrMaxiTabla = mFormatCadena(odrDeta.ItemArray[9].ToString());
				lstrMaxiFK = mFormatCadena(odrDeta.ItemArray[10].ToString());
				lstrMaxiTablaFK = mFormatCadena(odrDeta.ItemArray[11].ToString());
				lstrRaza = ";" + mFormatCadena(odrDeta.ItemArray[12].ToString()) + ";";
				lstrErrorId = mFormatCadena(odrDeta.ItemArray[13].ToString());
				lstrErrorDesc = mFormatCadena(odrDeta.ItemArray[14].ToString());
				lbooManual = mFormatBolean(odrDeta.ItemArray[15].ToString());
				lstrAgru = mFormatCadena(odrDeta.ItemArray[16].ToString());
				lstrOrde = mFormatCadena(odrDeta.ItemArray[17].ToString());
				lstrEspe = ";" + mFormatCadena(odrDeta.ItemArray[18].ToString()) + ";";
				lstrFormId = mFormatCadena(odrDeta.ItemArray[19].ToString());
                
				// no seguir validando si ya se produjo un error del mismo grupo de errores.
				if (lstrUltiAgruError != lstrAgru || lstrAgru == "0" || lstrAgru == "")
				{
					#region consulta Reglas

					lstrSql = "select * from " + lstrFrom + " ";
					lstrRegla = "";
					// tablas
					if (lstrMiniTabla != "")
						lstrSql = lstrSql + "," + lstrMiniTabla + " mi" + " (nolock)";
					if (lstrMaxiTabla != "")
						lstrSql = lstrSql + "," + lstrMaxiTabla + " ma" + " (nolock)";
					// condiciones
					lstrSql = lstrSql + " where " + lstrPK + " = " + pstrId;
					if (lstrWhere != "")
						lstrSql = lstrSql  + " and " + lstrWhere;
					if (lstrMiniTabla != "")
						lstrSql = lstrSql + " and " + lstrMiniFK + " = mi." + lstrMiniTablaFK;
					if (lstrMaxiTabla != "")
						lstrSql = lstrSql + " and " + lstrMaxiFK + " = ma." + lstrMaxiTablaFK;
					// campo obligatorio.
					if (lbooObli)
					{
						lstrRegla = lstrRegla + lstrCampo + " is not null";
					}
					// valores minimo y maximo.
					if (lstrMiniValor != "")
					{
						if (lstrRegla!= "")
							lstrRegla = lstrRegla + " and ";
						lstrRegla = lstrRegla + lstrAlias + lstrCampo + " >= ";
						if (lstrMiniTabla != "")
							lstrRegla = lstrRegla + "mi.";
						lstrRegla = lstrRegla + lstrMiniValor;
					}
					if (lstrMaxiValor != "")
					{
						if (lstrRegla != "")
							lstrRegla = lstrRegla + " and ";
						lstrRegla = lstrRegla + lstrAlias + lstrCampo + " <= ";
						if (lstrMaxiTabla != "")
							lstrRegla = lstrRegla + "ma.";
						lstrRegla = lstrRegla + lstrMaxiValor;
					}
					// reglas y formulas
					if (lstrRegla != "" && lstrForm != "")
					{
						lstrSql = lstrSql + " and ";
						lstrSql = lstrSql + "(";
						lstrSql = lstrSql + " not (" + lstrRegla + ")";
						lstrSql = lstrSql + " or (" + lstrForm + ")";
						lstrSql = lstrSql + ")";
					}
					else
					{
						// solo reglas
						if (lstrRegla != "")
							lstrSql = lstrSql + " and not (" + lstrRegla + ")";
						// solo formulas
						if (lstrForm != "")
							lstrSql = lstrSql + " and " + lstrForm;
					}
					#endregion
					#region realizar la validacion
					if (lstrForm != "" || lstrRegla != "" || lbooManual)
					{

						if (! lbooManual || lstrForm != "" )
						{
							if (lstrForm.Substring(0,7).ToLower() =="sra_neg" && 
								(pstrTabla == gstrTabSEMEN_STOCK  ||pstrTabla == gstrTabEMBRIONES_STOCK))
							{

								lstrForm=lstrForm.Substring(8, lstrForm.Length -8);

								lstrSql = "select * from tramites (nolock) where tram_id= " +
									pTramiteId.ToString() + " and "   + lstrForm;
							}
						
						}
						
					
						bool vlEntro=false;
						// validacion manual, siempre rechazar.
						if ( !mValidar(pstrConn, lstrSql, lstrFormId)|| lbooManual)
						{
							if (lstrErrorId.ToString()=="" || lbooManual )
							{  
								vlEntro=true;
								lstrErrorId = mFormatCadena(odrDeta.ItemArray[13].ToString());

								DataTable dtError= new DataTable("ErrorMensaje");
								dtError = mGetErrorByFormulaId(pstrConn,Convert.ToInt16(lstrFormId));
								lstrErrorId=dtError.Rows[0]["rmen_id"].ToString();
								lstrErrorDesc=dtError.Rows[0]["rmen_codi"].ToString()+ "-" + dtError.Rows[0]["rmen_desc"].ToString();
							}

							if ( lbooManual && !vlEntro)
							{  
								lstrErrorId = mFormatCadena(odrDeta.ItemArray[13].ToString());

								DataTable dtError= new DataTable("ErrorMensaje");
								dtError = mGetErrorByFormulaId(pstrConn,Convert.ToInt16(lstrFormId));
								lstrErrorId=dtError.Rows[0]["rmen_id"].ToString();
								lstrErrorDesc=dtError.Rows[0]["rmen_codi"].ToString()+ "-" + dtError.Rows[0]["rmen_desc"].ToString();
							}

							if (pstrTabla == gstrTabEMBRIONES_STOCK && (intProceID == Procesos.ExportacionEmbriones || intProceID == Procesos.TransferenciaEmbriones || intProceID == Procesos.ImportacionEmbriones) ||
								pstrTabla == gstrTabSEMEN_STOCK && (intProceID == Procesos.ExportacionSemen || intProceID == Procesos.TransferenciaSemen || intProceID == Procesos.ImportacionSemen))
							{
								pstrId=pTramiteId.ToString();
							
							}

							if (gErrorSalvado(pstrConn, pstrTabla, pstrId, lstrErrorId,intProceID))
							{
								
								lstrResuErrorId = "";
								pstrErrorDesc = "";
							}
							else
							{
								
								lstrResuErrorId = lstrErrorId;
								pstrErrorDesc = lstrErrorDesc;
							}
									 
							lstrUltiAgruError = lstrAgru;
							if (pbooGrabarError)
							{
								if ( pstrTabla.ToUpper()=="TRAMITES_PRODUCTOS" || pstrTabla.ToUpper()=="SEMEN_STOCK"  || pstrTabla.ToUpper()=="EMBRIONES_STOCK")
								{
									mGrabarErrorTramite(pstrConn, pstrTabla, pstrId, pTramiteId,lstrErrorId, pstrUsua);
								}
								else
								{
									mGrabarError(pstrConn, pstrTabla, pstrId,lstrErrorId, pstrUsua);
						
								}
							}	
						}
							
					}
					#endregion
				}
				cantidad++;
			}

			cantidad++;	
				
			return (lstrResuErrorId);
		}

		public static void mGrabarError(string pstrConn, string pstrTabla, string pstrId, 
			string lstrErrorId, string pstrUsua)
		{
			//lock (_lockmGrabarError)
		{
			string mstrCmd = "exec cs_validaciones_errores_grabar ";
			mstrCmd = mstrCmd + "@tabla= '" + pstrTabla + "'";
			mstrCmd = mstrCmd + ",@id =" + pstrId;
			mstrCmd = mstrCmd + ",@rmen_id=" + lstrErrorId;
			mstrCmd = mstrCmd + ",@audi_user=" + pstrUsua;

			//try
			//{
			mExecute(pstrConn, mstrCmd);
			//}
			//catch (Exception ex)
			//{
			//    clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : Validaciones.mGrabarError -:  pstrFormId " + pstrId + " -:- sqlcmd: " + mstrCmd));
			//}
		} 
		}

		/// <summary>
		/// sobrecarga con trasaccion
		/// </summary>
		/// <param name="pstrConn"></param> 
		/// <param name="pstrTabla"></param>
		/// <param name="pstrId"></param>
		/// <param name="lstrErrorId"></param>
		/// <param name="pstrUsua"></param>
		public static void mGrabarError(SqlTransaction lTransac, string pstrTabla, string pstrId, 
			string lstrErrorId, string pstrUsua)
		{
			//lock (_lockmGrabarError)
		{
			string mstrCmd = "exec cs_validaciones_errores_grabar ";
			mstrCmd = mstrCmd + "@tabla= '" + pstrTabla + "'";
			mstrCmd = mstrCmd + ",@id =" + pstrId;
			mstrCmd = mstrCmd + ",@rmen_id=" + lstrErrorId;
			mstrCmd = mstrCmd + ",@audi_user=" + pstrUsua;

			//try
			//{
			mExecute(lTransac, mstrCmd);
			//}
			//catch (Exception ex)
			//{
			//    clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : Validaciones.mGrabarError -:  pstrFormId " + pstrId + " -:- sqlcmd: " + mstrCmd));
			//}
		}
		}

		public static void mGrabarErrorTramite(string pstrConn, string pstrTabla, string pstrId, Int32 pstrTramiteId,
			string lstrErrorId, string pstrUsua   )
		{
			//lock (_lockmGrabarErrorTramite)
		{
			string mstrCmd = "exec cs_validaciones_errores_grabar ";
			mstrCmd = mstrCmd + "@tabla= '" + pstrTabla + "'";
			mstrCmd = mstrCmd + ",@id =" + pstrId;
			mstrCmd = mstrCmd + ",@TramiteId =" + pstrTramiteId;
			mstrCmd = mstrCmd + ",@rmen_id=" + lstrErrorId;
			mstrCmd = mstrCmd + ",@audi_user=" + pstrUsua;
                
			//try
			//{
			mExecute(pstrConn, mstrCmd);
			//}
			//catch (Exception ex)
			//{
			//    clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : Validaciones.mGrabarErrorTramite -:  pstrFormId " + pstrId + " - " + pstrTramiteId + " -:- sqlcmd: " + mstrCmd));
			//}
		}
		}

		public static void mGrabarErrorPrestamo(string pstrConn, string pstrTabla, string pCabeceraId, Int32 pstrProductoId,
			string lstrErrorId, string pstrUsua   )
		{
			//lock (_lockmGrabarErrorTramite)
		{
			string mstrCmd = "exec cs_validaciones_errores_grabar ";
			mstrCmd = mstrCmd + "@tabla= '" + pstrTabla + "'";
			mstrCmd = mstrCmd + ",@id =" + pCabeceraId;
			mstrCmd = mstrCmd + ",@TramiteId =" + pstrProductoId;
			mstrCmd = mstrCmd + ",@rmen_id=" + lstrErrorId;
			mstrCmd = mstrCmd + ",@audi_user=" + pstrUsua;
                
			//try
			//{
			mExecute(pstrConn, mstrCmd);
			//}
			//catch (Exception ex)
			//{
			//    clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : Validaciones.mGrabarErrorTramite -:  pstrFormId " + pstrId + " - " + pstrTramiteId + " -:- sqlcmd: " + mstrCmd));
			//}
		}
		}


		/// <summary>
		/// sopbrecarga con transaccion
		/// </summary>
		/// <param name="lTransac"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrId"></param>
		/// <param name="pstrTramiteId"></param>
		/// <param name="lstrErrorId"></param>
		/// <param name="pstrUsua"></param>
		public static void mGrabarErrorTramite(SqlTransaction lTransac, string pstrTabla, string pstrId, Int32 pstrTramiteId,
											   string lstrErrorId, string pstrUsua   )
		{
			//lock (_lockmGrabarErrorTramite)
			{
				string mstrCmd = "exec cs_validaciones_errores_grabar ";
				mstrCmd = mstrCmd + "@tabla= '" + pstrTabla + "'";
				mstrCmd = mstrCmd + ",@id =" + pstrId;
				mstrCmd = mstrCmd + ",@TramiteId =" + pstrTramiteId;
				mstrCmd = mstrCmd + ",@rmen_id=" + lstrErrorId;
				mstrCmd = mstrCmd + ",@audi_user=" + pstrUsua;
                
				//try
				//{
					mExecute(lTransac, mstrCmd);
				//}
				//catch (Exception ex)
				//{
				//    clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : Validaciones.mGrabarErrorTramite -:  pstrFormId " + pstrId + " - " + pstrTramiteId + " -:- sqlcmd: " + mstrCmd));
				//}
			}
		}


		public static void mGrabarError(string pstrConn, string pstrTabla, string pstrId, string lstrErrorId, 
			string pstrUsua, string strMensajeError)
		{
			string mstrCmd = "exec cs_validaciones_errores_grabar ";
			mstrCmd = mstrCmd + "@tabla= '" + pstrTabla + "'";
			mstrCmd = mstrCmd + ",@id =" + pstrId;
			mstrCmd = mstrCmd + ",@rmen_id=" + lstrErrorId;
			mstrCmd = mstrCmd + ",@audi_user =" + pstrUsua;
			mstrCmd = mstrCmd + ",@manual=" + strMensajeError;

			//try
			//{
			mExecute(pstrConn, mstrCmd);
			//}
			//catch (Exception ex)
			//{
			//    clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : Validaciones.mGrabarError -:  pstrFormId " + pstrId + " -:- sqlcmd: " + mstrCmd));
			//}
		}

		public static string mFormatCadena(string pstrCade)
		{
			if (pstrCade == null)
			{
				pstrCade = "";
				return (pstrCade);
			}
			pstrCade = pstrCade.Trim();
			pstrCade = pstrCade.Replace("&nbsp;", "");
			return (pstrCade);
		}

		public static bool mFormatBolean(string pstrCade)
		{
			if (pstrCade == null || pstrCade == "")
			{
				return (false);
			}
			return (Convert.ToBoolean(pstrCade));
		}

		public static int mExecute(string pstrConn, string pstrProc)
		{
			int lintExec = -1;

			bool bitdeadlockedDetected = true;
			while (bitdeadlockedDetected)
			{
				try
				{
					lock (_lockmExecute)
					{
						if (pstrConn.Length == 0)
						{
							ArgumentNullException Throw = new ArgumentNullException("pstrConn");
						}
						if (pstrProc.Length == 0)
						{
							ArgumentNullException Throw = new ArgumentNullException("pstrProc");
						}

						SqlConnection myConnection = new SqlConnection(pstrConn);
						SqlCommand cmdExecCommand = new SqlCommand();
						cmdExecCommand.Connection = myConnection;
						cmdExecCommand.CommandText = pstrProc;

						myConnection.Open();
						lintExec = cmdExecCommand.ExecuteNonQuery();
						myConnection.Close();
						bitdeadlockedDetected = false;

						//return (lintExec);
					}

					bitdeadlockedDetected = false;
				}
				catch (Exception ex)
				{
					string msg = ex.InnerException + ex.Message;
					if (msg.IndexOf("deadlocked")>0)
						bitdeadlockedDetected = true;
					else
						throw new Exception(msg);
				}
			}
			return (lintExec);
		}
		/// <summary>
		/// sobrecarga con transaccion
		/// </summary>
		/// <param name="lTransac"></param>
		/// <param name="pstrProc"></param>
		/// <returns></returns>
		public static int mExecute(SqlTransaction lTransac, string pstrProc)
		{
			int lintExec = -1;

			bool bitdeadlockedDetected = true;
			while (bitdeadlockedDetected)
			{
				try
				{
					lock (_lockmExecute)
					{
						if (pstrProc.Length == 0)
						{
							ArgumentNullException Throw = new ArgumentNullException("pstrProc");
						}

						SqlCommand cmdExecCommand = new SqlCommand(pstrProc, lTransac.Connection, lTransac);
						cmdExecCommand.CommandType = CommandType.Text;
						cmdExecCommand.CommandText = pstrProc;

						lintExec = cmdExecCommand.ExecuteNonQuery();
						return (lintExec);
					}

				//	bitdeadlockedDetected = false;
				}
				catch (Exception ex)
				{
					string msg = ex.InnerException + ex.Message;
					if (msg.IndexOf("deadlocked")>0)
						bitdeadlockedDetected = true;
					else
						throw new Exception(msg);
				}
			}
			return (lintExec);
		}

		public static string mQuitarComillas(string pstrCade)
		{
			if (pstrCade == null)
			{
				pstrCade = "";
				return (pstrCade);
			}
			pstrCade = pstrCade.Trim();
			pstrCade = pstrCade.Replace("'", "");
			return (pstrCade);
		}

		public static string EmptyToCero(string stringToCheck)
		{
			if (stringToCheck == null || stringToCheck.Length == 0)
				return "0";
			return stringToCheck;
		}

		/// <summary>
		/// Valida que el campo sea distinto de null
		/// </summary>
		/// <param name="pstrCampo">Campo a evaluar</param>
		/// <param name="pboolnull">Si es true devuelve Null devuelve "",por false devuelve "0"</param>
		/// <returns>Devuelve el valor sin nulos</returns>
		public static string ValidarNulos(object pstrCampo, bool pboolnull) 
		{
			string lstrRt;

			if( pstrCampo.ToString() == "" || pstrCampo == null ||  pstrCampo== DBNull.Value )
			{ 
				if( pboolnull )
				{ 
					lstrRt = ""; 
				}
				else
				{
					lstrRt = "0";
				}
			}
			else
			{
				lstrRt = pstrCampo.ToString();

			}
			return lstrRt;
		}

		public static string ReplaceCharEspPorComilla(string pCadena) 
		{
			string Retorno;
			Retorno=  pCadena.Replace("~","'");
	
			return Retorno;
		}

		
		/*Metodos nuevos que validan por sp las reglas*/
		public static string gValidarReglas(string pstrConn,  string pstrTabla,
			string pstrProductoId, string pstrRaza, string pstrSexo, string pstrUsua, bool pbooGrabarError, 
			string pProceID, Int32 pstrTramiteId, Int32 pstrTramProdId)
		{
			string lstrErrorId;
			string lstrErrorDesc = "";
			string lstrEspe = ""; // mObtenerEspecie(pstrConn, pstrRaza);
			string pTipoTabla = "";
			string strId = "";

			Int32 pintTramiteId = 0;

			/// Se setea por defecto el tramite id para el where.
			/// MF 30/07/2013
			strId =pstrProductoId;

			if (pProceID.ToString() == Procesos.ExportacionProductos.ToString() ||
				pProceID.ToString() == Procesos.TransferenciaProductos.ToString() ||
				pProceID.ToString() == Procesos.ImportacionProductos.ToString())
			{
				strId = pstrTramProdId.ToString();
			}

			if (pProceID.ToString() == Procesos.ImplantesRecuperaEmbriones.ToString() ||
				pProceID.ToString() == Procesos.ExportacionEmbriones.ToString() ||
				pProceID.ToString() == Procesos.ImportacionEmbriones.ToString()|| 
				pProceID.ToString() == Procesos.TransferenciaEmbriones.ToString())
			{
				strId = pstrTramiteId.ToString();
			}

			if (pProceID.ToString() == Procesos.ExportacionSemen.ToString() ||
				pProceID.ToString() == Procesos.ImportacionSemen.ToString() ||
				pProceID.ToString() == Procesos.TransferenciaSemen.ToString())
			{
				strId = pstrTramiteId.ToString();
			}
			//Los unicos que tienen tramites productos son los que son animal en pie
			//embriones  y semen no tienen tramites productos
			//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mArmarReglaSQL -: pstrFormId " + pstrProductoId));
			try
			{
				lstrErrorId = Validaciones.mEjecutaValidacionReglas(pstrConn, pstrTabla, strId, pstrRaza,
					pstrSexo, ref lstrErrorDesc, pbooGrabarError, pstrUsua, lstrEspe, pProceID,
					pstrTramiteId);
			}
			catch( Exception ex  )
			{
				string p = ex.StackTrace; 
				lstrErrorId = "198";
			}

			if (pbooGrabarError && lstrErrorId == "")
			{
				Validaciones.mGrabarAprobacion(pstrConn, pstrTabla, strId, pstrUsua);
				return ("");
			}
			else 
			{
				pTipoTabla = pstrTabla;
				pintTramiteId = Convert.ToInt32(pstrTramiteId);
				Validaciones.mGrabarErrorTramite(pstrConn, pTipoTabla, strId, pintTramiteId, lstrErrorId, pstrUsua);
				return (lstrErrorId);
			}
		}

		/// <summary>
		/// sobrecarga con transaccion
		/// </summary>
		/// <param name="lTransac"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrProductoId"></param>
		/// <param name="pstrRaza"></param>
		/// <param name="pstrSexo"></param>
		/// <param name="pstrUsua"></param>
		/// <param name="pbooGrabarError"></param>
		/// <param name="pProceID"></param>
		/// <param name="pstrTramiteId"></param>
		/// <param name="pstrTramProdId"></param>
		/// <returns></returns>
		public static string gValidarReglas(SqlTransaction lTransac,  string pstrTabla,
			string pstrProductoId, string pstrRaza, string pstrSexo, string pstrUsua, bool pbooGrabarError, 
			string pProceID, Int32 pstrTramiteId, Int32 pstrTramProdId)
		{
			string lstrErrorId;
			string lstrErrorDesc = "";
			string lstrEspe = ""; // mObtenerEspecie(pstrConn, pstrRaza);
			string pTipoTabla = "";
			string strId = "";

			Int32 pintTramiteId = 0;

			/// Se setea por defecto el tramite id para el where.
			/// MF 30/07/2013
			strId =pstrProductoId;

			if (pProceID.ToString() == Procesos.ExportacionProductos.ToString() ||
				pProceID.ToString() == Procesos.TransferenciaProductos.ToString() ||
				pProceID.ToString() == Procesos.ImportacionProductos.ToString())
			{
				strId = pstrTramProdId.ToString();
			}

			if (pProceID.ToString() == Procesos.ImplantesRecuperaEmbriones.ToString() ||
				pProceID.ToString() == Procesos.ExportacionEmbriones.ToString() ||
				pProceID.ToString() == Procesos.ImportacionEmbriones.ToString()||
				pProceID.ToString() == Procesos.TransferenciaEmbriones.ToString())
			{
				strId = pstrTramiteId.ToString();
			}

			if (pProceID.ToString() == Procesos.ExportacionSemen.ToString() ||
				pProceID.ToString() == Procesos.ImportacionSemen.ToString() ||
				pProceID.ToString() == Procesos.TransferenciaSemen.ToString())
			{
				strId = pstrTramiteId.ToString();
			}
			//Los unicos que tienen tramites productos son los que son animal en pie
			//embriones  y semen no tienen tramites productos
			//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mArmarReglaSQL -: pstrFormId " + pstrProductoId));
	
			try
			{
				lstrErrorId = Validaciones.mEjecutaValidacionReglas(lTransac, pstrTabla, strId, pstrRaza,
					pstrSexo, ref lstrErrorDesc, pbooGrabarError, pstrUsua, 
					lstrEspe, pProceID,	pstrTramiteId);
			}
			catch( Exception ex  )
			{
				string p = ex.StackTrace; 
				lstrErrorId = "198";
			}


			if (pbooGrabarError && lstrErrorId == "")
			{
				Validaciones.mGrabarAprobacion(lTransac, pstrTabla, strId, pstrUsua);
				return ("");
			}
			else
			{
				pTipoTabla = pstrTabla;
				pintTramiteId = Convert.ToInt32(pstrTramiteId);
				Validaciones.mGrabarErrorTramite(lTransac, pTipoTabla, strId, pintTramiteId, lstrErrorId, pstrUsua);
				return (lstrErrorId);
			}
		}


		public static string gValidarReglas(SqlTransaction lTransac,  string pstrTabla,
			string pstrCabeceraID,   string pstrUsua, bool pbooGrabarError, 
			string pProceID,  DataSet dsDatos)
		{
			string lstrErrorId;
			string lstrTieneErrorId="";
			string lstrErrorDesc = "";
			string lstrEspe = ""; // mObtenerEspecie(pstrConn, pstrRaza);
			string pTipoTabla = "";
			string strSexo="";
			string strRaza="";
			Int32 intProductoId;

			lstrErrorId="";

			
			if (pstrTabla.ToLower()== "prestamos")
			{
				foreach (DataRow dr in dsDatos.Tables["Prestamos_deta"].Rows)
				{
					intProductoId= Convert.ToInt32(dr["pstd_prdt_id"].ToString());
					strRaza= dr["_RazaId"].ToString();
					strSexo= dr["_sexo"].ToString();
					lstrErrorId = Validaciones.mEjecutaValidacionReglas(lTransac, pstrTabla, pstrCabeceraID,strRaza,
						strSexo, ref lstrErrorDesc, pbooGrabarError, pstrUsua, 
						lstrEspe, pProceID,	intProductoId);	



					if (lstrErrorId!="")
					{
						lstrTieneErrorId = lstrErrorId;
						pTipoTabla = pstrTabla;
						Validaciones.mGrabarErrorTramite(lTransac, pTipoTabla, pstrCabeceraID, intProductoId, lstrErrorId, pstrUsua);
					}
				}
			}

			if (pstrTabla.ToLower() == "embargos")
			{
				
				foreach (DataRow dr in dsDatos.Tables["embargos_deta"].Rows)
				{
					intProductoId= Convert.ToInt32(dr["emde_prdt_id"].ToString());
					strRaza= dr["_RazaId"].ToString();
					strSexo= dr["_sexo"].ToString();

					lstrErrorId = Validaciones.mEjecutaValidacionReglas(lTransac, pstrTabla, pstrCabeceraID, strRaza,
						strSexo, ref lstrErrorDesc, pbooGrabarError, pstrUsua, 
						lstrEspe, pProceID,	intProductoId);	

					if (lstrErrorId!="")
					{
						lstrTieneErrorId = lstrErrorId;
						pTipoTabla = pstrTabla;
						Validaciones.mGrabarErrorTramite(lTransac, pTipoTabla, pstrCabeceraID, intProductoId, lstrErrorId, pstrUsua);
					}
				}
			}


			if (pbooGrabarError && lstrTieneErrorId == "")
			{
				Validaciones.mGrabarAprobacion(lTransac, pstrTabla, pstrCabeceraID, pstrUsua);
				
			}
	
			return (lstrTieneErrorId);
		}

		public static string mEjecutaValidacionReglas(string pstrConn, string pstrTabla, string pstrId, string pstrRaza, string pstrSexo, ref string pstrErrorDesc,
			bool pbooGrabarError, string pstrUsua, string pstrEspe, string pProceID, Int32 pTramiteId)
		{
    			string lstrResuErrorId = "";
				string ResutError = "198";
				int intProceID = 0;

				intProceID = Convert.ToInt16(pProceID);
				pstrErrorDesc = "";

				//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mArmarReglaSQLEntrar -:  pstrFormId " + pstrId));
				//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 1 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
				DataSet ds = null;

				ArrayList lstErrorId = new ArrayList();
				/*
				public const string  = "16";
				*/

				#region consultas
				switch (pProceID)
				{
					case ProcesosConst.Nacimientos:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarProdInsc", pstrId);
						break;
					case ProcesosConst.Servicios:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarServicios", pstrId);
						break;
					case ProcesosConst.TransferenciaProductos:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarTransferenciaProductos", pstrId);
						break;
					case ProcesosConst.ImplantesRecuperaEmbriones:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarImplantesRecuperaEmbriones", pstrId);
						break;
					case ProcesosConst.DeclaraPorcinos:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarProdInsc", pstrId);
						break;
					case ProcesosConst.PlantelPorcinos:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarPlantelPorcinos", pstrId);
						break;
					case ProcesosConst.ImportacionProductos:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarImportacionProductos", pstrId);
						break;
					case ProcesosConst.ExportacionProductos:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarExportacionProductos", pstrId);
						break;
					case ProcesosConst.ExportacionSemen:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarExportacionSemen", pstrId);
						break;
					case ProcesosConst.ExportacionEmbriones:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarExportacionEmbriones", pstrId);
						break;
					case ProcesosConst.ImportacionEmbriones:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarImportacionEmbriones", pstrId);
						break;
					case ProcesosConst.TransferenciaEmbriones:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarTransferenciaEmbriones", pstrId);
						break;
					case ProcesosConst.ImportacionSemen:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarImportacionSemen", pstrId);
						break;
					case ProcesosConst.TransferenciaSemen:
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarTransferenciaSemen", pstrId);
						break;
					case ProcesosConst.PlantelBase:  // Dario 2014-06-26
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarPlantelBase", pstrId);
						break;
					case ProcesosConst.Prestamos:  // Dario 2014-07-21
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarPrestamos", pstrId);
						break;
					case ProcesosConst.Embargos:  // Dario 2014-07-21
						ds = gEjecutaSPValidacion(pstrConn, "p_rg_ValidarEmbargos", pstrId);
						break;
				}
				#endregion

				//int cantidad = 0;

			if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
			{
				if (ds.Tables[0].Columns.Count > 0)
				{
					foreach(DataColumn dc in ds.Tables[0].Columns)
					{
						Int32 idForm = Convert.ToInt32(dc.ColumnName.Replace("pstrFormId", "").Split('_')[1]);
						string varError = ds.Tables[0].Rows[0][dc].ToString();

						// validacion manual, siempre rechazar.
						if (varError != string.Empty)
						{
							lstrResuErrorId = idForm.ToString();
							//List<MensajeByFormulaIdEntity> mensaje = (from dato in MensajeByFormulaId
							//                                         where dato.rfor_id == idForm
							//                                        select dato).ToList<MensajeByFormulaIdEntity>();

							//if(mensaje.Count> 0)
							//{
							//lstErrorId.Add(mensaje.First().rmen_id);
							//lstrErrorId = mensaje.First().rmen_id.ToString();
							//lstrErrorDesc = mensaje.First().rmen_codi.ToString() + "-" + mensaje.First().rmen_desc.ToString();
							//}

							if (pstrTabla == gstrTabEMBRIONES_STOCK && (intProceID == Procesos.ExportacionEmbriones || intProceID == Procesos.TransferenciaEmbriones || intProceID == Procesos.ImportacionEmbriones) ||
								pstrTabla == gstrTabSEMEN_STOCK && (intProceID == Procesos.ExportacionSemen || intProceID == Procesos.TransferenciaSemen || intProceID == Procesos.ImportacionSemen))
							{
								pstrId = pTramiteId.ToString();

							}

							if (gErrorSalvado(pstrConn, pstrTabla, pstrId, lstrResuErrorId, intProceID))
							{
								lstrResuErrorId = "";
								//pstrErrorDesc = "";
							} 
							else
							{
								lstErrorId.Add(idForm);
								//    lstrResuErrorId = lstrErrorId;
								//pstrErrorDesc = lstrResuErrorId;
							}

							//lstrUltiAgruError = lstrAgru;
						}
					}
				}
				else
				{
					lstErrorId.Add(198);
				}
			}
			else
			{
				lstErrorId.Add(198);
			}

				//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 4 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));

				if (pbooGrabarError)
				{
					if (pstrTabla.ToUpper() == "TRAMITES_PRODUCTOS" || pstrTabla.ToUpper() == "SEMEN_STOCK" || pstrTabla.ToUpper() == "EMBRIONES_STOCK"
						|| pstrTabla.ToUpper() == "EMBARGOS" || pstrTabla.ToUpper() == "PRESTAMOS" )
					{
						//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 5 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
						foreach (Int32 item in lstErrorId)
						{
							mGrabarErrorTramite(pstrConn, pstrTabla, pstrId, pTramiteId, item.ToString(), pstrUsua);
							pstrErrorDesc = item.ToString();
						}
						//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 6 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
                    
					}
					else
					{
						//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 5 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
						foreach (Int32 item in lstErrorId)
						{
							mGrabarError(pstrConn, pstrTabla, pstrId, item.ToString(), pstrUsua);
							pstrErrorDesc = item.ToString();
						}
						//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 6 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));

					}
				}

				//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mArmarReglaSQLSalir -:  pstrFormId " + pstrId));
            
			
				// verifico que tenga errores asi retorno el primero 
				if (lstErrorId.Count == 0)
				{
					// Dario 2014-10-21 cambio para que no me libere el producto/tramite si tiene errores
					// manuales sin salvar
					int erroresCant = gCantidadErroresSinSalvar(pstrConn, pstrTabla, pstrId, pTramiteId);

					if (erroresCant == 0)
					{
						ResutError = "";
						pstrErrorDesc = "";
					}
					else
					{
						lstrResuErrorId = erroresCant.ToString();
						ResutError = lstrResuErrorId;
						pstrErrorDesc = erroresCant.ToString();
					}
				}
				else
				{
					lstrResuErrorId = lstErrorId[0].ToString();
					ResutError = lstrResuErrorId;
					pstrErrorDesc = lstrResuErrorId;
				}

				return (ResutError);
		}


		/// <summary>
		/// sobrecarga con transaccion
		/// </summary>
		/// <param name="lTransac"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrId"></param>
		/// <param name="pstrRaza"></param>
		/// <param name="pstrSexo"></param>
		/// <param name="pstrErrorDesc"></param>
		/// <param name="pbooGrabarError"></param>
		/// <param name="pstrUsua"></param>
		/// <param name="pstrEspe"></param>
		/// <param name="pProceID"></param>
		/// <param name="pTramiteId"></param>
		/// <returns></returns>
		public static string mEjecutaValidacionReglas(SqlTransaction lTransac, string pstrTabla, string pstrId, string pstrRaza, string pstrSexo, ref string pstrErrorDesc,
			bool pbooGrabarError, string pstrUsua, string pstrEspe, string pProceID, Int32 pTramiteId)
		{
			string lstrResuErrorId = "";
			string ResutError = "198";
			int intProceID = 0;

			intProceID = Convert.ToInt16(pProceID);
			pstrErrorDesc = "";

			//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mArmarReglaSQLEntrar -:  pstrFormId " + pstrId));
			//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 1 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
			DataSet ds = null;

			ArrayList lstErrorId = new ArrayList();
			/*
			public const string  = "16";
			*/

			#region consultas
			switch (pProceID)
			{
				case ProcesosConst.Nacimientos:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarProdInsc", pstrId);
					break;
				case ProcesosConst.Servicios:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarServicios", pstrId);
					break;
				case ProcesosConst.TransferenciaProductos:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarTransferenciaProductos", pstrId);
					break;
				case ProcesosConst.ImplantesRecuperaEmbriones:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarRecuperaEmbriones", pstrId);
					break;
				case ProcesosConst.ImplantesEmbriones:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarImplantesEmbriones", pstrId);
					break;
				case ProcesosConst.DeclaraPorcinos:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarProdInsc", pstrId);
					break; 
				case ProcesosConst.PlantelPorcinos:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarPlantelPorcinos", pstrId);
					break;
				case ProcesosConst.ImportacionProductos:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarImportacionProductos", pstrId);
					break;
				case ProcesosConst.ExportacionProductos:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarExportacionProductos", pstrId);
					break;
				case ProcesosConst.ExportacionSemen:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarExportacionSemen", pstrId);
					break;
				case ProcesosConst.ExportacionEmbriones:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarExportacionEmbriones", pstrId);
					break;
				case ProcesosConst.ImportacionEmbriones:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarImportacionEmbriones", pstrId);
					break;
				case ProcesosConst.TransferenciaEmbriones:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarTransferenciaEmbriones", pstrId);
					break;
				case ProcesosConst.ImportacionSemen:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarImportacionSemen", pstrId);
					break;
				case ProcesosConst.TransferenciaSemen:
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarTransferenciaSemen", pstrId);
					break;
				case ProcesosConst.PlantelBase:  // Dario 2014-06-26
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarPlantelBase", pstrId);
					break;
				case ProcesosConst.Prestamos:  // Dario 2014-07-21
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarPrestamos", pstrId);
					break;
				case ProcesosConst.Embargos:  // Dario 2014-07-21
					ds = gEjecutaSPValidacion(lTransac, "p_rg_ValidarEmbargos", pstrId);
					break;
			}
			#endregion

			//int cantidad = 0;

			if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
			{
				if (ds.Tables[0].Columns.Count > 0)
				{
					foreach(DataColumn dc in ds.Tables[0].Columns)
					{
						Int32 idForm = Convert.ToInt32(dc.ColumnName.Replace("pstrFormId", "").Split('_')[1]);
						string varError = ds.Tables[0].Rows[0][dc].ToString();

						// validacion manual, siempre rechazar.
						if (varError != string.Empty)
						{
							lstrResuErrorId = idForm.ToString();
							//List<MensajeByFormulaIdEntity> mensaje = (from dato in MensajeByFormulaId
							//                                         where dato.rfor_id == idForm
							//                                        select dato).ToList<MensajeByFormulaIdEntity>();

							//if(mensaje.Count> 0)
							//{
							//lstErrorId.Add(mensaje.First().rmen_id);
							//lstrErrorId = mensaje.First().rmen_id.ToString();
							//lstrErrorDesc = mensaje.First().rmen_codi.ToString() + "-" + mensaje.First().rmen_desc.ToString();
							//}

							if (pstrTabla == gstrTabEMBRIONES_STOCK && (intProceID == Procesos.ExportacionEmbriones || intProceID == Procesos.TransferenciaEmbriones || intProceID == Procesos.ImportacionEmbriones) ||
								pstrTabla == gstrTabSEMEN_STOCK && (intProceID == Procesos.ExportacionSemen || intProceID == Procesos.TransferenciaSemen || intProceID == Procesos.ImportacionSemen))
							{
								pstrId = pTramiteId.ToString();
							}

							if (gErrorSalvado(lTransac, pstrTabla, pstrId, lstrResuErrorId, intProceID))
							{
								lstrResuErrorId = "";
								//    pstrErrorDesc = "";
							}
							else
							{
								lstErrorId.Add(idForm);
								//    lstrResuErrorId = lstrErrorId;
								//    pstrErrorDesc = lstrErrorDesc;
							}

							//lstrUltiAgruError = lstrAgru;
						}
					}
				}
				else
				{
					lstErrorId.Add(198);
				}
			}
			else
			{
				lstErrorId.Add(198);
			}

			//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 4 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));

			if (pbooGrabarError)
			{
				if (pstrTabla.ToUpper() == "TRAMITES_PRODUCTOS" || pstrTabla.ToUpper() == "SEMEN_STOCK" || pstrTabla.ToUpper() == "EMBRIONES_STOCK"
					|| pstrTabla.ToUpper() == "EMBARGOS" || pstrTabla.ToUpper() == "PRESTAMOS" )
				{
					//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 5 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
					foreach (Int32 item in lstErrorId)
					{
						mGrabarErrorTramite(lTransac, pstrTabla, pstrId, pTramiteId, item.ToString(), pstrUsua);
						pstrErrorDesc = item.ToString();
					}
					//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 6 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
                    
				}
				else
				{
					//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 5 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
					foreach (Int32 item in lstErrorId)
					{
						mGrabarError(lTransac, pstrTabla, pstrId, item.ToString(), pstrUsua);
						pstrErrorDesc = item.ToString();
					}
					//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("paso: 6 -: " + System.DateTime.Now.ToString("HH:mm:ss.fff")));
				}
			}

			//clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception("error : mArmarReglaSQLSalir -:  pstrFormId " + pstrId));
            
			// verifico que tenga errores asi retorno el primero 
			if (lstErrorId.Count == 0)
			{
				// Dario 2014-10-21 cambio para que no me libere el producto/tramite si tiene errores
				// manuales sin salvar
				int erroresCant = gCantidadErroresSinSalvar(lTransac, pstrTabla, pstrId, pTramiteId);

				if (erroresCant == 0)
				{
					ResutError = "";
					pstrErrorDesc = "";
				}
				else
				{
					lstrResuErrorId = erroresCant.ToString();
					ResutError = lstrResuErrorId;
					pstrErrorDesc = erroresCant.ToString();
				}
			}
			else
			{
				lstrResuErrorId = lstErrorId[0].ToString();
				ResutError = lstrResuErrorId;
				pstrErrorDesc = lstrResuErrorId;
			}
			return (ResutError);
		}


		/// <summary>
		/// metodo que verifica si hay erroes
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="tabla"></param>
		/// <param name="pstrId"></param>
		/// <returns>int</returns>
		public static int gCantidadErroresSinSalvar(string pstrConn, string tabla, string pstrId, int pTramiteId)
		{
			int respuesta = 0;
 
			bool bitdeadlockedDetected = true;
			while (bitdeadlockedDetected)
			{
				try
				{
					StringBuilder pstrProc = new StringBuilder();

					pstrProc.Append("exec cs_validaciones_errores_Count ");
					pstrProc.Append("@tabla=");
					if (tabla.Trim().Length > 0) { pstrProc.Append(tabla); } 
					else { pstrProc.Append("null"); }
					pstrProc.Append(",@id=");
					if (pstrId.Trim().Length > 0) { pstrProc.Append(pstrId); } 
					else { pstrProc.Append("null"); }
					pstrProc.Append(",@TramiteId=");
					if (pTramiteId > 0) { pstrProc.Append(pTramiteId); } 
					else { pstrProc.Append("null"); }

					DataSet ds = clsSQLServer.gExecuteQuery(pstrConn, pstrProc.ToString());

					if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
						respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["cantidad"]);
					
					bitdeadlockedDetected = false;
				}
				catch (Exception ex)
				{
					string msg = ex.InnerException + ex.Message;
					if (msg.IndexOf("deadlocked")>0)
						bitdeadlockedDetected = true;
					else
						throw new Exception(msg);
				}
			}

			return respuesta;
		}

		/// <summary>
		/// metodo que verifica si hay erroes
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="tabla"></param>
		/// <param name="pstrId"></param>
		/// <returns>int</returns>
		public static int gCantidadErroresSinSalvar(SqlTransaction lTransac, string tabla, string pstrId, int pTramiteId)
		{
			int respuesta = 0;

			bool bitdeadlockedDetected = true;
			while (bitdeadlockedDetected)
			{
				try
				{
					StringBuilder pstrProc = new StringBuilder();

					pstrProc.Append("exec cs_validaciones_errores_Count ");
					pstrProc.Append("@tabla=");
					if (tabla.Trim().Length > 0) { pstrProc.Append(tabla); } 
					else { pstrProc.Append("null"); }
					pstrProc.Append(",@id=");
					if (pstrId.Trim().Length > 0) { pstrProc.Append(pstrId); } 
					else { pstrProc.Append("null"); }
					pstrProc.Append(",@TramiteId=");
					if (pTramiteId > 0) { pstrProc.Append(pTramiteId); } 
					else { pstrProc.Append("null"); }

					DataSet ds = clsSQLServer.gExecuteQuery(lTransac, pstrProc.ToString());

					if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
						respuesta = Convert.ToInt32(ds.Tables[0].Rows[0]["cantidad"]);
					
					bitdeadlockedDetected = false;
				}
				catch (Exception ex)
				{
					string msg = ex.InnerException + ex.Message;
					if (msg.IndexOf("deadlocked")>0)
						bitdeadlockedDetected = true;
					else
						throw new Exception(msg);
				}
			}

			return respuesta;
		}


		public static DataSet gEjecutaSPValidacion(string pstrConn, string pstrProce, string pstrId)
		{
			DataSet ds = new DataSet();

			bool bitdeadlockedDetected = true;
			while (bitdeadlockedDetected)
			{
				try
				{
					string mstrCmd = "exec " + pstrProce + " " + pstrId;

					SqlDataAdapter cmdExecCommand = new SqlDataAdapter(mstrCmd, pstrConn);
					cmdExecCommand.SelectCommand.CommandTimeout = 99999;
					
					SqlConnection myConnection = new SqlConnection(pstrConn);
					myConnection.Open();
					
					cmdExecCommand.Fill(ds);
					
					myConnection.Close();
					bitdeadlockedDetected = false;
					return (ds);
					
					
				}
				catch (Exception ex)
				{
					string msg = ex.InnerException + ex.Message;
					if (msg.IndexOf("deadlocked")>0)
						bitdeadlockedDetected = true;
					else
						throw new Exception(msg);
				}
			}
			return (ds);
		}

		/// <summary>
		/// sobrecarga con transaccion
		/// </summary>
		/// <param name="lTransac"></param>
		/// <param name="pstrProce"></param>
		/// <param name="pstrId"></param>
		/// <returns></returns>
		public static DataSet gEjecutaSPValidacion(SqlTransaction lTransac, string pstrProce, string pstrId)
		{
			DataSet ds = new DataSet();

			bool bitdeadlockedDetected = true;
			while (bitdeadlockedDetected)
			{
				try
				{
					string mstrCmd = "exec " + pstrProce + " " + pstrId;

					SqlDataAdapter cmdExecCommand = new SqlDataAdapter(mstrCmd, lTransac.Connection);
					cmdExecCommand.SelectCommand.CommandTimeout = 99999;
					cmdExecCommand.SelectCommand.Transaction = lTransac;
					
					cmdExecCommand.Fill(ds);
					bitdeadlockedDetected = false;
					return (ds);
					
					
				}
				catch (Exception ex)
				{
					string msg = ex.InnerException + ex.Message;
					if (msg.IndexOf("deadlocked")>0)
						bitdeadlockedDetected = true;
					else
						throw new Exception(msg);
				}
			}
			return (ds);
		}
	}

}
