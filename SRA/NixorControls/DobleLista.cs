using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace NixorControls
{
	/// <summary>
	/// El control DobleLista permite seleccionar varios items pasandolos de una lista a otra.
	/// Las propiedades específicas son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// - AutoPostBack  : especifica si hace postback al clickear los botones, por default = "False"
	/// - lstOri		: accede al combo origen
	/// - lstDest		: accede al combo destino
	/// - TituloOri		: el titulo de la lista origen
	/// - TituloDes		: el titulo de la lista destino
	/// - ClassTitulos  : el estilo de los titulos
	/// - AltaVisible   : especifican si los botones son visibles
	/// - AltaMulVisible 
	/// - BajaVisible 
	/// - BajaMulVisible

	/// Los metodos publicos son:
	/// - ObtenerIDs	: obtiene los ids del combo destino, concatenados con ","
	/// - SeleccionarItems: selecciona los items en el combo elegido, pasando los ids concatenados con ","
	/// Los metodos publicos son:
	/// - ObtenerIDs	: obtiene los ids del combo destino, concatenados con ","
	/// </summary>

	public delegate void DobleListaEventHandler(object sender, string pstrIds);

	public class DobleLista:System.Web.UI.WebControls.WebControl,System.Web.UI.IPostBackEventHandler 
	{
		private string mstrIncludesUrl = "Includes/";
		private string mstrCambiaValor = "False";
		private string mstrTituloOri = "";
		private string mstrTituloDes = "";
		private string mstrClassTitulos = "";
		private string mstrAltaToolTip = "Agregar";
		private string mstrBajaToolTip = "Sacar";
		private bool mbooAutoPostBack = false;
		private bool mbooAltaVisible = true;
		private bool mbooAltaMulVisible = true;
		private bool mbooBajaVisible = true;
		private bool mbooBajaMulVisible = true;
		private bool mbooOrdenarListas = false;

		private ListBox mlstOri;
		private ListBox mlstDest;

		public event DobleListaEventHandler Alta_Click;
		public event DobleListaEventHandler AltaMul_Click;
		public event DobleListaEventHandler Baja_Click;
		public event DobleListaEventHandler BajaMul_Click;

		public string IncludesUrl
		{
			get 
			{
				return (Utils.gPath(Page) + mstrIncludesUrl); 
			}
			set 
			{
				mstrIncludesUrl = value; 
			}
		}

		public string CambiaValor
		{
			get 
			{
				return mstrCambiaValor; 
			}
			set 
			{
				mstrCambiaValor = value; 
			}
		}

		public string TituloOri
		{
			get 
			{
				return mstrTituloOri; 
			}
			set 
			{
				mstrTituloOri = value; 
			}
		}
		public string TituloDes
		{
			get 
			{
				return mstrTituloDes;
			}
			set 
			{
				mstrTituloDes = value; 
			}
		}
		public string ClassTitulos
		{
			get 
			{
				return mstrClassTitulos;
			}
			set 
			{
				mstrClassTitulos = value; 
			}
		}
		
		public string AltaToolTip
		{
			get 
			{
				return mstrAltaToolTip; 
			}
			set 
			{
				mstrAltaToolTip = value; 
			}
		}

		public string BajaToolTip
		{
			get 
			{
				return mstrBajaToolTip; 
			}
			set 
			{
				mstrBajaToolTip = value; 
			}
		}

		public bool AutoPostBack
		{
			get 
			{
				return mbooAutoPostBack;
			}
			set 
			{
				mbooAutoPostBack = value; 
			}
		}

		public bool AltaVisible
		{
			get 
			{
				return mbooAltaVisible;
			}
			set 
			{
				mbooAltaVisible = value; 
			}
		}

		public bool AltaMulVisible
		{
			get 
			{
				return mbooAltaMulVisible;
			}
			set 
			{
				mbooAltaMulVisible = value; 
			}
		}

		public bool BajaVisible
		{
			get 
			{
				return mbooBajaVisible;
			}
			set 
			{
				mbooBajaVisible = value; 
			}
		}

		public bool BajaMulVisible
		{
			get 
			{
				return mbooBajaMulVisible;
			}
			set 
			{
				mbooBajaMulVisible = value; 
			}
		}

		public bool OrdenarListas
		{
			get 
			{
				return mbooOrdenarListas;
			}
			set 
			{
				mbooOrdenarListas = value; 
			}
		}
		
		public ListBox lstOri
		{
			get 
			{
				return mlstOri; 
			}
		}

		public ListBox lstDest
		{
			get 
			{
				return mlstDest; 
			}
		}



		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);
			if(!base.Page.IsClientScriptBlockRegistered("NixorDobleLista"))
				Page.RegisterClientScriptBlock("NixorDobleLista", String.Format("<SCRIPT language=\"javascript\" src=\"{0}doblelista.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
		
			if (bool.Parse(mstrCambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			string lstrCambios="";
			string lstrClick="";

			if (bool.Parse(mstrCambiaValor))
				lstrCambios="mCambioValor();";

			StringBuilder lsbCtrls;
			lsbCtrls = new StringBuilder();

			lsbCtrls.Append(string.Format("<TABLE id='Tab{0}' style='WIDTH: {1}; HEIGHT: {2}' border=0>", base.ClientID, base.Width, base.Height));
			if((mstrTituloOri!="")||(mstrTituloDes!=""))
			{
				lsbCtrls.Append(string.Format("<TR><TD><div class='{2}'>{0}</div></TD><TD></TD><TD><div class='{2}'>{1}</div></TD></TR>", mstrTituloOri, mstrTituloDes,mstrClassTitulos));
			}
			lsbCtrls.Append("<TR>");
			lsbCtrls.Append("<TD style='WIDTH: 50%; HEIGHT: 100%' vAlign='center' rowspan=6>");

			pWriter.Write(lsbCtrls.ToString());
			mlstOri.RenderControl(pWriter);

			lsbCtrls.Length=0;
			lsbCtrls.Append("<TD>");
			lsbCtrls.Append("</TD>");
			lsbCtrls.Append("<TD style='WIDTH: 50%; HEIGHT: 100%' vAlign='center' rowspan=6>");
			pWriter.Write(lsbCtrls.ToString());
			mlstDest.RenderControl(pWriter);
			lsbCtrls.Length=0;
			lsbCtrls.Append("</TD>");
			lsbCtrls.Append("</TR>");
			lsbCtrls.Append("<TR>");
			lsbCtrls.Append("<TD>");

			if (AutoPostBack)
				lstrClick=Page.GetPostBackEventReference(this, "Alta");
			else
				lstrClick="dlAgregar('" + base.ClientID + "', '"+mbooOrdenarListas+"')";
			lsbCtrls.Append(string.Format("<input type='button' name='btn{0}Alta' value='>' id='btn{0}Alta' title='"+mstrAltaToolTip+"' onclick=\"{1};{2}\" style='width:30px;' /></P>", base.ClientID, lstrClick, lstrCambios));
			
			lsbCtrls.Append("</TD>");
			lsbCtrls.Append("</TR>");
			lsbCtrls.Append("<TR>");
			lsbCtrls.Append("<TD>");
			
			if (AutoPostBack)
				lstrClick=Page.GetPostBackEventReference(this, "Baja");
			else
				lstrClick="dlSacar('" + base.ClientID + "', '"+mbooOrdenarListas+"')";
			lsbCtrls.Append(string.Format("<input type='button' name='btn{0}Baja' value='<' id='btn{0}Baja' title='"+mstrBajaToolTip+"' onclick=\"{1};{2}\" style='width:30px;' /></P>", base.ClientID, lstrClick, lstrCambios));
			
			lsbCtrls.Append("</TD>");
			lsbCtrls.Append("</TR>");
			if (AltaMulVisible) 
			{
				lsbCtrls.Append("<TR>");
				lsbCtrls.Append("<TD>");
			
				if (AutoPostBack)
					lstrClick=Page.GetPostBackEventReference(this, "AltaMul");
				else
					lstrClick="dlAgregarMul('" + base.ClientID + "', '"+mbooOrdenarListas+"')";
				

				lsbCtrls.Append(string.Format("<input type='button' name='btn{0}AltaMul' value='>>' id='btn{0}AltaMul' title='Agregar Todos' onclick=\"{1};{2}\" style='width:30px;' /></P>", base.ClientID, lstrClick, lstrCambios));
				lsbCtrls.Append("</TD>");
				lsbCtrls.Append("</TR>");
			}
			if (BajaMulVisible) 
			{
				lsbCtrls.Append("<TR>");
				lsbCtrls.Append("<TD>");
			
				if (AutoPostBack)
					lstrClick=Page.GetPostBackEventReference(this, "BajaMul");
				else
					lstrClick="dlSacarMul('" + base.ClientID + "', '"+mbooOrdenarListas+"')";

				lsbCtrls.Append(string.Format("<input type='button' name='btn{0}BajaMul' value='<<' id='btn{0}BajaMul' title='Sacar Todos' onclick=\"{1};{2}\" style='width:30px;' /></P>", base.ClientID, lstrClick, lstrCambios));
				lsbCtrls.Append("</TD>");
				lsbCtrls.Append("</TR>");
			}
			lsbCtrls.Append("</TABLE>");
			lsbCtrls.Append("<div style='display:none'>");
			lsbCtrls.Append(string.Format("<input name='hdn{0}'>", base.ClientID));
			lsbCtrls.Append("</div>");
			pWriter.Write(lsbCtrls.ToString());
		}

		protected override void OnInit(System.EventArgs e)
		{
			base.OnInit(e);
			mlstOri=new ListBox();
			mlstDest=new ListBox();
			mlstOri.ID = base.ID + "_ORI";
			mlstDest.ID = base.ID + "_DES";
			mlstOri.SelectionMode=ListSelectionMode.Multiple;
			mlstDest.SelectionMode=ListSelectionMode.Multiple;
			mlstOri.CssClass=base.CssClass;
			mlstDest.CssClass=base.CssClass;
			mlstOri.Width=new System.Web.UI.WebControls.Unit("100%");
			mlstDest.Width=new System.Web.UI.WebControls.Unit("100%");
			mlstOri.Height=new System.Web.UI.WebControls.Unit("100%");
			mlstDest.Height=new System.Web.UI.WebControls.Unit("100%");
		}

		protected override object SaveViewState ()
		{
			mGuardarEstado(mlstOri);			
			mGuardarEstado(mlstDest);			
			return(base.SaveViewState());
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			mCargarEstado(mlstOri);
			mCargarEstado(mlstDest);
			mActualizarEstado();
		}

		private void mGuardarEstado(ListBox pListBox)
		{
			DataSet ds = new DataSet();
			DataRow dr;

			ds.Tables.Add(new System.Data.DataTable());
			ds.Tables[0].Columns.Add("id");
			ds.Tables[0].Columns.Add("desc");

			foreach(ListItem loItem in pListBox.Items)
			{
				dr=ds.Tables[0].NewRow();
				dr[0]=loItem.Value;
				dr[1]=loItem.Text;
				ds.Tables[0].Rows.Add(dr);
			}
			ViewState.Add(pListBox.ID,ds);
		}

		private void mCargarEstado(ListBox pListBox)
		{
			DataSet ds = (DataSet) ViewState[pListBox.ID];
			ListItem li;

			foreach(DataRow lDr in ds.Tables[0].Rows)
			{
				li=new ListItem(lDr[1].ToString(),lDr[0].ToString());
				pListBox.Items.Add(li);
			}
		}

		private void mActualizarEstado()
		{
			if(Page.Request.Form["hdn" + base.ClientID]!=null)
			{
				string[] lstrCambios = Page.Request.Form["hdn" + base.ClientID].Split(char.Parse(","));
				for(int i=0; i<lstrCambios.GetUpperBound(0); i++)
				{
					if(lstrCambios[i].StartsWith("+"))
						mMoverItem(mlstOri, mlstDest, lstrCambios[i].Substring(1));
					else
						mMoverItem(mlstDest, mlstOri, lstrCambios[i].Substring(1));
				}
			}
		}

		private void mMoverItem(ListBox plbOri, ListBox plbDes, string pstrValor)
		{
			ListItem li = plbOri.Items.FindByValue(pstrValor);
			plbOri.Items.Remove(li);
			plbDes.Items.Add(li);
		}

		public string ObtenerIDs()
		{
			return(ObtenerIDs(mlstDest));
		}

		public string ObtenerIDs(ListBox plbOri)
		{
			StringBuilder lsbCtrls;
			lsbCtrls = new StringBuilder();
			foreach(ListItem loItem in plbOri.Items)
			{
				if(lsbCtrls.Length!=0)
					lsbCtrls.Append(",");
				lsbCtrls.Append(loItem.Value.Trim());
			}
			return(lsbCtrls.ToString());
		}

		public void SeleccionarItems(ListBox plbOri, string pstrIds)
		{
			if (pstrIds!=null)
			{
				string[] lvstrIds = pstrIds.Split(char.Parse(","));
				for(int i=0;i<=lvstrIds.GetUpperBound(0);i++)
					plbOri.Items.FindByValue(lvstrIds[i]).Selected=true;
			}
		}
		
		void System.Web.UI.IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			SeleccionarItems(mlstOri, Page.Request.Form[base.ClientID + "_ORI"]);
			SeleccionarItems(mlstDest, Page.Request.Form[base.ClientID + "_DES"]);

			switch(eventArgument)
			{
				case "Alta":
					if(Alta_Click!=null)
						Alta_Click(this, Page.Request.Form[base.ClientID + "_ORI"]);
					break;
				case "AltaMul":
					if(AltaMul_Click!=null)
						AltaMul_Click(this, ObtenerIDs(mlstOri));
					break;
				case "Baja":
					if(Baja_Click!=null)
						Baja_Click(this, Page.Request.Form[base.ClientID + "_DES"]);
					break;
				case "BajaMul":
					if(BajaMul_Click!=null)
						BajaMul_Click(this, ObtenerIDs(mlstDest));
					break;
			}
		}
	}
}
