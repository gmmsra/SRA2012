using System;
using System.Data;
using System.Text;

namespace NixorControls
{
	/// <summary>
	/// El control Menu genera un menu desplegaba a partir de un dataset.
	/// Los campos de la primer tabla del dataset deben ser:
	/// nivel       
	/// texto                          
	/// tooltiptext                    
	/// url                                                                                                  
	/// accion      
	/// imagen1         
	/// imagen2 
	/// </summary>
	
	public delegate void CargarMenuDataSetEventHandler(object sender, EventArgs e);

	public class Menu:System.Web.UI.WebControls.WebControl
	{
		public event CargarMenuDataSetEventHandler CargarMenuDataSet;

		public enum MenuModeEnum 
		{
			Horizontal = 1,
			Vertical = 2
		}

		private string mstrIncludesUrl = "Includes/";
		private string mstrImagesUrl = "Images/menu/";
		private DataSet mDataSet = new DataSet();	//para generar menu
		private string mstrMenuName = "";				//Nombre del menu
		private MenuModeEnum _MenuMode = MenuModeEnum.Horizontal;  //Horizontal o vertical
		private string _ArrowPictureForItems  = "arrow_black.gif";
		private string _ArrowPictureForTop  = "arrow_black.gif";
		private string _SeparatorPictureForTop = "gray.gif";
		private bool mbooShowPictureForItems = false;
		private bool mbooShowPictureForTop = false;

		//Estilos menu top
		private string mstrCssClassItem = "M_TopItem";
		private string mstrCssClassItemOver = "M_TopItemOver";
		private string mstrCssClassSeparator = "M_TopSeparator";
		
		//Estilo submenues													
		private string mstrCssClassSubMenu = "M_SubMenu";
		private string mstrCssClassSubMenuItem = "M_SubMenuItem";
		private string mstrCssClassSubMenuItemOver = "M_SubMenuItemOver";
		private string mstrCssClassSubMenuSeparator = "M_SubMenuSeparator";

		private string _miScriptFinal;	//Aca se arma lo que al final va al explorer

		protected override void OnPreRender(System.EventArgs e)
		{
			StringBuilder[] miScript = new StringBuilder[25];
			int[] NivelMenu = new int[25];
			int CuentaMenu;
			StringBuilder miScriptIni;
			StringBuilder miScriptMedio;
			if(mstrMenuName.Trim() != "" )
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorMenu"))
				{
					Page.RegisterHiddenField(HelperID("Url"), "");
					//Aqui guardo el destino del link.. _Self, _Blank, etc..
					Page.RegisterHiddenField(HelperID("Target"), "");
					Page.RegisterClientScriptBlock("NixorMenu", FuncionesJava());
				}

				//Inicio el menu.

				//'miScriptIni: Nunca lo guardo en Cache, ya que un mismo menu, puede estar en 2 paginas en un lugar diferente.
				//Y aqui defino la posicion.
				miScriptIni = new StringBuilder();
				miScriptIni.Append(" <SPAN id='");
				miScriptIni.Append(base.ClientID);
				miScriptIni.Append("'>");

				//Establezco si tengo que armarlo o recuperarlo
				bool ArmoDeNuevo = false;
				if(Page.Session[NombreSession()]==null) 
					ArmoDeNuevo = true;

				//Me fijo si leo del session el menu ya procesado o si lo armo dinamicamente 
				if(ArmoDeNuevo)
				{
					//Armo el menu. Al final lo guardo en session
					CargarMenuDataSet(this,null);

					//Me aseguro que vengan las dos tablas
					if(mDataSet.Tables.Count != 1 || mDataSet.Tables[0].Rows.Count == 0)
						throw new System.Exception("No se pudieron recuperar la tablas desde la base de datos. Revise lo siguiente: 1) Asegurese que se escribio un metodo para capturar el evento LoadMenuDataSet. 2) En ese metodo se debe cargar el DataSet con el menu en la propiedad MenuDataSet  3) El menu '" + this.MenuName + "' este creado en las bases de datos.");
					else
					{
						//En tables(0), tengo el encabezado del menu
						//En tables(1), tengo el detalle del menu

						string Texto, Accion, URL="", Target, ToolTip, PicNormal, PicOver;
						string HagoClick, HagoToolTip;
						int Nivel;
						int Estado;
						bool EsElPrimero =true;
						int CuentaItem;
						int CuentaZindex;

						//Inicializo variables
						Estado = 0;
						CuentaMenu = 1;
						CuentaItem = 1;
						CuentaZindex = 999;
						Target = "_self";     //Todavia no funciona, previsto para abrir el link en otro frame

						//Ya que hay una sola funcion Menu_PopupMenu para todos los
						//menues dentro de una pagina, debo pasar como parametro
						//la referencia. Por eso ahora me fijo a quien tengo que hacer referencia (Ver Menu_PopupMenu)
						string postback = "";
						postback = Page.GetPostBackEventReference(this);
						postback = postback.Substring(14);      //Le saco el __doPostBack('
						postback = postback.Substring(0, postback.IndexOf("'"));

						miScript[0] = new System.Text.StringBuilder();
						StringBuilder miSb = new StringBuilder();
						DataRow dr;
						for (int j=0;j<mDataSet.Tables[0].Rows.Count;j++)
						{
							//Leo los campos de cada item
							dr = mDataSet.Tables[0].Rows[j];
							Texto = "&nbsp;" + dr["Texto"].ToString() + "&nbsp;";
							ToolTip = dr["ToolTipText"].ToString();
							PicNormal = dr["Imagen1"].ToString().Trim();
							PicOver = dr["Imagen2"].ToString().Trim();
							Nivel = Int32.Parse(dr["Nivel"].ToString()) - 1;	//Le resto 1 para que empieze en 0
							Accion = dr["Accion"].ToString();
                    
							switch(Accion.ToString().Trim())
							{
								case "0":
								case "":         // Ir a un URL
									URL = dr["URL"].ToString();
									break;
								case "1":         //Dispara click en server
									URL = "Click:" + dr["ParamClick"].ToString();
									break;
								case "2":         //Funcion Java
									miSb = new StringBuilder();
									miSb.Append("Funcion:");
									miSb.Append(dr["FuncionJava"].ToString());
									miSb.Append("()");
									URL = miSb.ToString();
									break;
							}

							if(URL == "")
								HagoClick = "";
							else
							{
								//URL = System.Web.HttpUtility.HtmlAttributeEncode(URL).ToString
								miSb = new StringBuilder();
								miSb.Append(" onclick='Menu_PopupMenu(\"");
								miSb.Append(URL);
								miSb.Append("\",\"");
								miSb.Append(Target);
								miSb.Append("\",\"");
								miSb.Append(postback);
								miSb.Append("\", \"");
								miSb.Append("\");'");
								HagoClick = miSb.ToString();
							}

							if (ToolTip == "")
								HagoToolTip = "";
							else
							{
								miSb = new StringBuilder();
								miSb.Append(" title='");
								miSb.Append(ToolTip);
								miSb.Append("'");
								HagoToolTip = miSb.ToString();
							}
							CuentaItem += 1;
							CuentaZindex += 1;
							if (Nivel == Estado)
							{
								if (EsElPrimero)
									//Inicio del menu
									if (MenuMode == MenuModeEnum.Vertical)
									{
										//Vertical
										miScript[0].Append("<TABLE class='");
										miScript[0].Append(this.CssClass);
										miScript[0].Append("' id='");
										miScript[0].Append(base.ClientID);
										miScript[0].Append("_group_");
										miScript[0].Append(CuentaMenu.ToString());
										miScript[0].Append("' style='Z-INDEX: ");
										miScript[0].Append(CuentaZindex.ToString());
										miScript[0].Append("' onmouseout=\"if (document.readyState == 'complete') Menu_groupMsOut('");
										miScript[0].Append(base.ClientID);
										miScript[0].Append("_group_");
										miScript[0].Append(CuentaMenu.ToString());
										miScript[0].Append("', null, null, 0);\" cellSpacing='1' cellPadding='0' border='0'><TBODY>");
									}
									else
									{
										//Horizontal
										miScript[0].Append("<TABLE class='");
										miScript[0].Append(this.CssClass);
										miScript[0].Append("' id='");
										miScript[0].Append(base.ClientID);
										miScript[0].Append("_group_");
										miScript[0].Append(CuentaMenu.ToString());
										miScript[0].Append("' style='Z-INDEX: ");
										miScript[0].Append(CuentaZindex.ToString());
										miScript[0].Append("' onmouseout=\"if (document.readyState == 'complete') Menu_groupMsOut('");
										miScript[0].Append(base.ClientID);
										miScript[0].Append("_group_");
										miScript[0].Append(CuentaMenu.ToString());
										miScript[0].Append("', null, null, 0);\" cellSpacing='1' cellPadding='0' border='0'><TBODY><TR>");
									}
								EsElPrimero = false;
							}
							else
							{
								if (Nivel > Estado)
								{
									CuentaMenu += 1;
									NivelMenu[Nivel] = CuentaMenu - 1;
									Estado = Nivel;
								}
								else
									Estado = Nivel;
							}
							//cosas en comun para todos los items
							string PicNormal2, PicOver2, NombreIcono, NombreItem;
							NombreIcono = base.ClientID + "_icon_" + CuentaItem.ToString();
							NombreItem = base.ClientID + "_item_" + CuentaItem.ToString();
							if (PicNormal == "")
								PicNormal2 = "NO";
							else
							{
								miSb = new StringBuilder();
								miSb.Append("'");
								miSb.Append(mstrImagesUrl);
								miSb.Append(PicNormal);
								miSb.Append("'");
								PicNormal2 = miSb.ToString();
							}
							if (PicOver == "")
							{
								miSb = new StringBuilder();
								miSb.Append("'");
								miSb.Append(mstrImagesUrl);
								miSb.Append(PicNormal);
								miSb.Append("'");
								PicOver2 = miSb.ToString();
							}
							else
							{
								miSb = new StringBuilder();
								miSb.Append("'");
								miSb.Append(mstrImagesUrl);
								miSb.Append(PicOver);
								miSb.Append("'");
								PicOver2 = miSb.ToString();
							}

							//Me fijo que es
							if (Texto == "&nbsp;--&nbsp;")
							{
								//Es un separador
								if (Nivel == 0)
								{
									//Es el top
									miScript[NivelMenu[Nivel]].Append("<TR><TD id='");
									miScript[NivelMenu[Nivel]].Append(NombreItem);
									miScript[NivelMenu[Nivel]].Append("' width='100%' height='1'><IMG class='");
									miScript[NivelMenu[Nivel]].Append(mstrCssClassSeparator);
									miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmouseover=\"this.className='");
									miScript[NivelMenu[Nivel]].Append(mstrCssClassSeparator);
									miScript[NivelMenu[Nivel]].Append("';\" onmouseout=\"this.className='");
									miScript[NivelMenu[Nivel]].Append(mstrCssClassSeparator);
									miScript[NivelMenu[Nivel]].Append("';\" height='1' src='");
									miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
									miScript[NivelMenu[Nivel]].Append(_SeparatorPictureForTop);
									miScript[NivelMenu[Nivel]].Append("' width='100%' border='0'></TD></TR>");
								}
								else
								{
									//Es un item comun
									miScript[NivelMenu[Nivel]].Append("<TR><TD id='");
									miScript[NivelMenu[Nivel]].Append(NombreItem);
									miScript[NivelMenu[Nivel]].Append("' width='100%' height='1'><HR></TD></TR>");
								}
							}
							else
							{
								//Es un item

								if (Nivel == 0)
								{
									//Es el menu TOP
									if (MenuMode == MenuModeEnum.Vertical)
									{
										//Vertical
										if (j < mDataSet.Tables[0].Rows.Count - 1 && (int.Parse(mDataSet.Tables[0].Rows[j + 1]["Nivel"].ToString()) - 1) > Nivel)
										{
											//tiene hijo
											if (mbooShowPictureForTop)
											{
												//Dejo espacio para iconos
												if (PicNormal2 == "NO")
												{
													//no tiene icono, dejo espacio vacio
													miScript[NivelMenu[Nivel]].Append("<TR");
													miScript[NivelMenu[Nivel]].Append(HagoToolTip);
													miScript[NivelMenu[Nivel]].Append(" onmousemove='return false;'><TD><TABLE onmouseup='' class='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("' ");
													miScript[NivelMenu[Nivel]].Append(HagoClick);
													miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
													miScript[NivelMenu[Nivel]].Append("', null, '', '");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("', '', 'over');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
													miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOver('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_group_");
													miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
													miScript[NivelMenu[Nivel]].Append("', 'rightdown', 0, 0, 0, null);\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
													miScript[NivelMenu[Nivel]].Append("', null, '', '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_icon_a");
													miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
													miScript[NivelMenu[Nivel]].Append(_ArrowPictureForTop);
													miScript[NivelMenu[Nivel]].Append("', 'out');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOut('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_group_");
													miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel] + 1).ToString());
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_group_");
													miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
													miScript[NivelMenu[Nivel]].Append("', 0, null);\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'></TD><TD>");
													miScript[NivelMenu[Nivel]].Append(Texto);
													miScript[NivelMenu[Nivel]].Append("</TD><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' align='right' width='15'><IMG id='");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_icon_a");
													miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
													miScript[NivelMenu[Nivel]].Append("' src='");
													miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
													miScript[NivelMenu[Nivel]].Append(_ArrowPictureForTop);
													miScript[NivelMenu[Nivel]].Append("' border='0'></TD></TR></TBODY></TABLE></TD></TR>");
												}
												else
												{
													//tiene icono
													miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
													miScript[NivelMenu[Nivel]].Append(HagoToolTip);
													miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("' ");
													miScript[NivelMenu[Nivel]].Append(HagoClick);
													miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("', ");
													miScript[NivelMenu[Nivel]].Append(PicOver2);
													miScript[NivelMenu[Nivel]].Append(", '");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("', '', 'over');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
													miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOver('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_group_");
													miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
													miScript[NivelMenu[Nivel]].Append("', 'rightdown', 0, 0, 0, null);\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("', ");
													miScript[NivelMenu[Nivel]].Append(PicNormal2);
													miScript[NivelMenu[Nivel]].Append(", '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_icon_a");
													miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
													miScript[NivelMenu[Nivel]].Append(_ArrowPictureForTop);
													miScript[NivelMenu[Nivel]].Append("', 'out');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOut('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_group_");
													miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel] + 1).ToString());
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_group_");
													miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
													miScript[NivelMenu[Nivel]].Append("', 0, null);\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'><IMG id='");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("' src='");
													miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
													miScript[NivelMenu[Nivel]].Append(PicNormal);
													miScript[NivelMenu[Nivel]].Append("' border='0'></TD><TD>");
													miScript[NivelMenu[Nivel]].Append(Texto);
													miScript[NivelMenu[Nivel]].Append("</TD><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' align='right' width='15'><IMG id='");
													miScript[NivelMenu[Nivel]].Append(base.ClientID);
													miScript[NivelMenu[Nivel]].Append("_icon_a");
													miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
													miScript[NivelMenu[Nivel]].Append("' src='");
													miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
													miScript[NivelMenu[Nivel]].Append(_ArrowPictureForTop);
													miScript[NivelMenu[Nivel]].Append("' border='0'></TD></TR></TBODY></TABLE></TD></TR>");
												}
											}
											else
											{
												//No Dejo espacio para iconos
												miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
												miScript[NivelMenu[Nivel]].Append("', null, '', '");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("', '', 'over');this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
												miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOver('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 'rightdown', 0, 0, 0, null);\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("', null, '', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_icon_a");
												miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
												miScript[NivelMenu[Nivel]].Append(_ArrowPictureForTop);
												miScript[NivelMenu[Nivel]].Append("', 'out');this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOut('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel] + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 0, null);\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' align='right' width='15'><IMG id='");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_icon_a");
												miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
												miScript[NivelMenu[Nivel]].Append("' src='");
												miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
												miScript[NivelMenu[Nivel]].Append(_ArrowPictureForTop);
												miScript[NivelMenu[Nivel]].Append("' border='0'></TD></TR></TBODY></TABLE></TD></TR>");
											}
										}
										else
										{
											//no tiene hijo
											if (mbooShowPictureForTop)
											{
												//Dejo espacio para iconos
												if (PicNormal2 == "NO")
												{
													//no tiene icono
													miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
													miScript[NivelMenu[Nivel]].Append(HagoToolTip);
													miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("' ");
													miScript[NivelMenu[Nivel]].Append(HagoClick);
													miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
													miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'over');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
													miScript[NivelMenu[Nivel]].Append("';\"");
													miScript[NivelMenu[Nivel]].Append(HagoClick);
													miScript[NivelMenu[Nivel]].Append(" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
													miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'out');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("';\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'></TD><TD>");
													miScript[NivelMenu[Nivel]].Append(Texto);
													miScript[NivelMenu[Nivel]].Append("</TD></TR></TBODY></TABLE></TD></TR>");
												}
												else
												{
													//tiene icono
													miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
													miScript[NivelMenu[Nivel]].Append(HagoToolTip);
													miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("' ");
													miScript[NivelMenu[Nivel]].Append(HagoClick);
													miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("', ");
													miScript[NivelMenu[Nivel]].Append(PicOver2);
													miScript[NivelMenu[Nivel]].Append(", null, '', 'over');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
													miScript[NivelMenu[Nivel]].Append("TopMenuItemOver';\"");
													miScript[NivelMenu[Nivel]].Append(HagoClick);
													miScript[NivelMenu[Nivel]].Append(" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
													miScript[NivelMenu[Nivel]].Append(NombreItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
													miScript[NivelMenu[Nivel]].Append("', '");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("', ");
													miScript[NivelMenu[Nivel]].Append(PicNormal2);
													miScript[NivelMenu[Nivel]].Append(", null, '', 'out');this.className='");
													miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
													miScript[NivelMenu[Nivel]].Append("';\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'><IMG id='");
													miScript[NivelMenu[Nivel]].Append(NombreIcono);
													miScript[NivelMenu[Nivel]].Append("' src=");
													miScript[NivelMenu[Nivel]].Append(PicNormal2);
													miScript[NivelMenu[Nivel]].Append(" border='0'></TD><TD>");
													miScript[NivelMenu[Nivel]].Append(Texto);
													miScript[NivelMenu[Nivel]].Append("</TD></TR></TBODY></TABLE></TD></TR>");
												}
											}
											else
											{
												//No Dejo espacio para iconos
												miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
												miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'over');this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
												miScript[NivelMenu[Nivel]].Append("';\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'out');this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("';\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD></TR></TBODY></TABLE></TD></TR>");
											}
										}
									}
									else
									{
										//Horizontal
										if (j < mDataSet.Tables[0].Rows.Count - 1 && (int.Parse(mDataSet.Tables[0].Rows[j + 1]["Nivel"].ToString()) - 1) > Nivel)
										{
											//tiene hijo
											if (mbooShowPictureForTop == true && PicNormal2 != "NO")
											{
												//Con Icono
												miScript[NivelMenu[Nivel]].Append("<TD><TABLE ");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
												miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOver('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 'belowleft', 0, 0, 0, null);\" onmouseout=\"this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOut('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel] + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 0, null);\"><TBODY><TR><TD width='12'><IMG id='");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("' src=");
												miScript[NivelMenu[Nivel]].Append(PicNormal2);
												miScript[NivelMenu[Nivel]].Append(" border='0'></TD><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD><TD></TD></TR></TBODY></TABLE></TD>");
											}
											else
											{
												//Sin icono
												miScript[NivelMenu[Nivel]].Append("<TD");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
												miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOver('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 'belowleft', 0, 0, 0, null);\" onmouseout=\"this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("';if (document.readyState == 'complete') Menu_itemMsOut('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel] + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 0, null);\">");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD>");
											}
										}
										else
										{
											//no tiene hijo
											if (mbooShowPictureForTop && PicNormal2 != "NO")
											{
												//Con icocno
												miScript[NivelMenu[Nivel]].Append("<TD><TABLE ");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem + "' onmousemove='return false;' id='" + NombreItem + "' " + HagoClick + " onmouseover=\"this.className='" + mstrCssClassItemOver + "';\" onmouseout=\"this.className='" + mstrCssClassItem + "';\"><TBODY><TR><TD width='12'><IMG id='" + NombreIcono + "' src=" + PicNormal2 + " border='0'></TD><TD>" + Texto + "</TD><TD></TD></TR></TBODY></TABLE></TD>");
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
												miScript[NivelMenu[Nivel]].Append("';\" onmouseout=\"this.className=\"");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("';'><TBODY><TR><TD width='12'><IMG id='");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("' src=");
												miScript[NivelMenu[Nivel]].Append(PicNormal2);
												miScript[NivelMenu[Nivel]].Append(" border='0'></TD><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD><TD></TD></TR></TBODY></TABLE></TD>");
											}
											else
											{
												//Sin icono, sin hijo, horizontal
												miScript[NivelMenu[Nivel]].Append("<TD");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItemOver);
												miScript[NivelMenu[Nivel]].Append("';\" onmouseout=\"this.className='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassItem);
												miScript[NivelMenu[Nivel]].Append("';\">");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD>");
											}
										}
									}
								}
								else
								{
									//Todos los otros Menues hijos del top

									//Me fijo si ya esta empezado (si no preguntar por ""
									if (miScript[NivelMenu[Nivel]] ==null)
									{
										//Creo un nuevo string builder
										miScript[NivelMenu[Nivel]] = new System.Text.StringBuilder();
										miScript[NivelMenu[Nivel]].Append("<TABLE class='");
										miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenu);
										miScript[NivelMenu[Nivel]].Append("' id='");
										miScript[NivelMenu[Nivel]].Append(base.ClientID);
										miScript[NivelMenu[Nivel]].Append("_group_");
										miScript[NivelMenu[Nivel]].Append(CuentaMenu.ToString());
										miScript[NivelMenu[Nivel]].Append("' onmouseover=\"Menu_groupMsOver('");
										miScript[NivelMenu[Nivel]].Append(base.ClientID);
										miScript[NivelMenu[Nivel]].Append("_group_");
										miScript[NivelMenu[Nivel]].Append(CuentaMenu.ToString());
										miScript[NivelMenu[Nivel]].Append("')\" style='Z-INDEX: ");
										miScript[NivelMenu[Nivel]].Append(CuentaZindex.ToString());
										miScript[NivelMenu[Nivel]].Append("; LEFT: 0px; VISIBILITY: hidden; POSITION: absolute; TOP: 0px' onmouseout=\"if (document.readyState == 'complete') Menu_groupMsOut('");
										miScript[NivelMenu[Nivel]].Append(base.ClientID);
										miScript[NivelMenu[Nivel]].Append("_group_");
										miScript[NivelMenu[Nivel]].Append(CuentaMenu.ToString());
										miScript[NivelMenu[Nivel]].Append("', '");
										miScript[NivelMenu[Nivel]].Append(base.ClientID);
										miScript[NivelMenu[Nivel]].Append("_item_");
										miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
										miScript[NivelMenu[Nivel]].Append("', '");
										miScript[NivelMenu[Nivel]].Append(base.ClientID);
										miScript[NivelMenu[Nivel]].Append("_group_");
										miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel - 1] + 1).ToString());
										miScript[NivelMenu[Nivel]].Append("', 0, null);\" cellSpacing='1' cellPadding='0' border='0'><TBODY>");
									}

									if (j < mDataSet.Tables[0].Rows.Count - 1 && (int.Parse(mDataSet.Tables[0].Rows[j + 1]["Nivel"].ToString()) - 1) > Nivel)
									{
										//tiene hijo
										if (mbooShowPictureForItems)
										{
											//Dejo espacio para iconos
											if (PicNormal2 == "NO")
											{
												//no tiene icono, dejo espacio vacio
												miScript[NivelMenu[Nivel]].Append("<TR");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" onmousemove='return false;'><TD><TABLE onmouseup='' class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
												miScript[NivelMenu[Nivel]].Append("', null, '', '");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("', '', 'over');if (document.readyState == 'complete') Menu_itemMsOver('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 'rightdown', 0, 0, 0, null);\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("', null, '', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_icon_a");
												miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
												miScript[NivelMenu[Nivel]].Append(_ArrowPictureForItems);
												miScript[NivelMenu[Nivel]].Append("', 'out');if (document.readyState == 'complete') Menu_itemMsOut('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel - 1] + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 0, null);\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'></TD><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' align='right' width='15'><IMG id='");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_icon_a");
												miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
												miScript[NivelMenu[Nivel]].Append("' src='");
												miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
												miScript[NivelMenu[Nivel]].Append(_ArrowPictureForItems);
												miScript[NivelMenu[Nivel]].Append("' border='0'></TD></TR></TBODY></TABLE></TD></TR>");
											}
											else
											{
												//tiene icono
												miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("', ");
												miScript[NivelMenu[Nivel]].Append(PicOver2);
												miScript[NivelMenu[Nivel]].Append(", '");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("', '', 'over');if (document.readyState == 'complete') Menu_itemMsOver('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 'rightdown', 0, 0, 0, null);\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("', ");
												miScript[NivelMenu[Nivel]].Append(PicNormal2);
												miScript[NivelMenu[Nivel]].Append(", '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_icon_a");
												miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
												miScript[NivelMenu[Nivel]].Append(_ArrowPictureForItems);
												miScript[NivelMenu[Nivel]].Append("', 'out');if (document.readyState == 'complete') Menu_itemMsOut('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel - 1] + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_group_");
												miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
												miScript[NivelMenu[Nivel]].Append("', 0, null);\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'><IMG id='");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("' src='");
												miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
												miScript[NivelMenu[Nivel]].Append(PicNormal);
												miScript[NivelMenu[Nivel]].Append("' border='0'></TD><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' align='right' width='15'><IMG id='");
												miScript[NivelMenu[Nivel]].Append(base.ClientID);
												miScript[NivelMenu[Nivel]].Append("_icon_a");
												miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
												miScript[NivelMenu[Nivel]].Append("' src='");
												miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
												miScript[NivelMenu[Nivel]].Append(_ArrowPictureForItems);
												miScript[NivelMenu[Nivel]].Append("' border='0'></TD></TR></TBODY></TABLE></TD></TR>");
											}
										}
										else
										{
											//No hay espacio para iconos
											miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
											miScript[NivelMenu[Nivel]].Append(HagoToolTip);
											miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
											miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
											miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("' ");
											miScript[NivelMenu[Nivel]].Append(HagoClick);
											miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
											miScript[NivelMenu[Nivel]].Append("', null, '', '");
											miScript[NivelMenu[Nivel]].Append(NombreIcono);
											miScript[NivelMenu[Nivel]].Append("', '', 'over');if (document.readyState == 'complete') Menu_itemMsOver('");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(base.ClientID);
											miScript[NivelMenu[Nivel]].Append("_group_");
											miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
											miScript[NivelMenu[Nivel]].Append("', 'rightdown', 0, 0, 0, null);\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
											miScript[NivelMenu[Nivel]].Append("', null, '', '");
											miScript[NivelMenu[Nivel]].Append(base.ClientID);
											miScript[NivelMenu[Nivel]].Append("_icon_a");
											miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
											miScript[NivelMenu[Nivel]].Append(_ArrowPictureForItems);
											miScript[NivelMenu[Nivel]].Append("', 'out');if (document.readyState == 'complete') Menu_itemMsOut('");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(base.ClientID);
											miScript[NivelMenu[Nivel]].Append("_group_");
											miScript[NivelMenu[Nivel]].Append((NivelMenu[Nivel] + 1).ToString());
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(base.ClientID);
											miScript[NivelMenu[Nivel]].Append("_group_");
											miScript[NivelMenu[Nivel]].Append((CuentaMenu + 1).ToString());
											miScript[NivelMenu[Nivel]].Append("', 0, null);\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD>");
											miScript[NivelMenu[Nivel]].Append(Texto);
											miScript[NivelMenu[Nivel]].Append("</TD><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' align='right' width='15'><IMG id='");
											miScript[NivelMenu[Nivel]].Append(base.ClientID);
											miScript[NivelMenu[Nivel]].Append("_icon_a");
											miScript[NivelMenu[Nivel]].Append(CuentaItem.ToString());
											miScript[NivelMenu[Nivel]].Append("' src='");
											miScript[NivelMenu[Nivel]].Append(mstrImagesUrl);
											miScript[NivelMenu[Nivel]].Append(_ArrowPictureForItems);
											miScript[NivelMenu[Nivel]].Append("' border='0'></TD></TR></TBODY></TABLE></TD></TR>");
										}
									}
									else
									{
										//no tiene hijo
										if (mbooShowPictureForItems)
										{
											//Dejo espacio para iconos
											if (PicNormal2 == "NO")
											{
												//no tiene icono
												miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
												miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'over');'");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'out');\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'></TD><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD></TR></TBODY></TABLE></TD></TR>");
											}
											else
											{
												//tiene icono
												miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
												miScript[NivelMenu[Nivel]].Append(HagoToolTip);
												miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("' ");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("', ");
												miScript[NivelMenu[Nivel]].Append(PicOver2);
												miScript[NivelMenu[Nivel]].Append(", null, '', 'over');'");
												miScript[NivelMenu[Nivel]].Append(HagoClick);
												miScript[NivelMenu[Nivel]].Append(" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
												miScript[NivelMenu[Nivel]].Append(NombreItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
												miScript[NivelMenu[Nivel]].Append("', '");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("', ");
												miScript[NivelMenu[Nivel]].Append(PicNormal2);
												miScript[NivelMenu[Nivel]].Append(", null, '', 'out');\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD style='PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px' width='20'><IMG id='");
												miScript[NivelMenu[Nivel]].Append(NombreIcono);
												miScript[NivelMenu[Nivel]].Append("' src=");
												miScript[NivelMenu[Nivel]].Append(PicNormal2);
												miScript[NivelMenu[Nivel]].Append(" border='0'></TD><TD>");
												miScript[NivelMenu[Nivel]].Append(Texto);
												miScript[NivelMenu[Nivel]].Append("</TD></TR></TBODY></TABLE></TD></TR>");
											}
										}
										else
										{
											//No hay espacio para iconos
											miScript[NivelMenu[Nivel]].Append("<TR><TD><TABLE");
											miScript[NivelMenu[Nivel]].Append(HagoToolTip);
											miScript[NivelMenu[Nivel]].Append(" onmouseup='' class='");
											miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
											miScript[NivelMenu[Nivel]].Append("' onmousemove='return false;' onmousedown='' id='");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("' ");
											miScript[NivelMenu[Nivel]].Append(HagoClick);
											miScript[NivelMenu[Nivel]].Append(" onmouseover=\"if (document.readyState == 'complete') Menu_updateCell('");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItemOver);
											miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'over');\" onmouseout=\"if (document.readyState == 'complete') Menu_updateCell('");
											miScript[NivelMenu[Nivel]].Append(NombreItem);
											miScript[NivelMenu[Nivel]].Append("', '");
											miScript[NivelMenu[Nivel]].Append(mstrCssClassSubMenuItem);
											miScript[NivelMenu[Nivel]].Append("', null, '', null, '', 'out');\" height='100%' cellSpacing='0' cellPadding='0' width='100%' border='0'><TBODY><TR><TD>");
											miScript[NivelMenu[Nivel]].Append(Texto);
											miScript[NivelMenu[Nivel]].Append("</TD></TR></TBODY></TABLE></TD></TR>");
										}
									}
								}
							}

							if (j == mDataSet.Tables[0].Rows.Count - 1)
							{
								//Se acabo el menu top
								miScript[0].Append("</TBODY></TABLE></SPAN>");
							}
						}


						//Finalizo el menu. Saco de la matriz todos los menues y armo un solo script
						int i;
						miScriptMedio = new StringBuilder(miScript[0].ToString());
						miScriptMedio.Append("<SPAN>");
						for (i = 1;i<CuentaMenu;i++)
						{
							miScriptMedio.Append(miScript[i].ToString());
							miScriptMedio.Append("</TBODY></TABLE>");
						}
						miScriptMedio.Append("</SPAN>");
						this.Page.Session[NombreSession()] = miScriptMedio;
					}
				}
				else
					// Recupero el script del session
					miScriptMedio = new System.Text.StringBuilder(this.Page.Session[NombreSession()].ToString());



				//Armo el script que finalmente va para el browser (Render)
				try
				{
					miScriptIni.Append(miScriptMedio.ToString());
					_miScriptFinal = miScriptIni.ToString();
				}
				catch
				{
					//Si da error queda vacio y da error mas adelante
				}
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			//Verifico si estoy en modo dise�o

			if (this.Site == null)
			{
				//Si estoy en modo usuario, verifico que no este vacio el dataset
				//Si no hay tablas, pongo mensaje
				if (mstrMenuName.Trim() == "")
					throw new Exception(this.UniqueID + ": La propiedad <MenuName> esta vacia.");
				else
				{
					//Si esta todo ok dibujo el menu
					pWriter.Write(_miScriptFinal);
				}
			}
		}

		public string FuncionesJava()
		{
			StringBuilder miSb = new StringBuilder();
			miSb.Append("<Script language='javascript' src='");
			miSb.Append(mstrIncludesUrl);
			miSb.Append("Menu.js");
			miSb.Append("' type='text/javascript'></Script>"); miSb.Append(Utils.CrLf());

			return (miSb.ToString());
		}

		//Usado por dar nombre a los campos ocultos
		private string HelperID(string NombreCampo)
		{
			return("__Menu_PopupMenu_" + NombreCampo + "_State");
		}

		public string NombreSession()
		{
			return("Menu:" + this.UniqueID + mstrMenuName.Trim());
		}

		#region Propiedades Publicas
		public string ImagesUrl
		{
			get 
			{
				return mstrImagesUrl; 
			}
			set 
			{
				mstrImagesUrl = value; 
			}
		}

		public string IncludesUrl
		{
			get 
			{
				return (Utils.gPath(Page) + mstrIncludesUrl); 
			}
			set 
			{
				mstrIncludesUrl = value; 
			}
		}

		public string CssClassItem
		{
			get 
			{
				return mstrCssClassItem; 
			}
			set 
			{
				mstrCssClassItem = value; 
			}
		}
		public string CssClassItemOver
		{
			get 
			{
				return mstrCssClassItemOver; 
			}
			set 
			{
				mstrCssClassItemOver = value; 
			}
		}

		public string CssClassSeparator
		{
			get 
			{
				return mstrCssClassSeparator; 
			}
			set 
			{
				mstrCssClassSeparator = value; 
			}
		}

		public string CssClassSubMenu
		{
			get 
			{
				return mstrCssClassSubMenu; 
			}
			set 
			{
				mstrCssClassSubMenu = value; 
			}
		}

		public string CssClassSubMenuItem
		{
			get 
			{
				return mstrCssClassSubMenuItem; 
			}
			set 
			{
				mstrCssClassSubMenuItem = value; 
			}
		}

		public string CssClassSubMenuItemOver
		{
			get 
			{
				return mstrCssClassSubMenuItemOver; 
			}
			set 
			{
				mstrCssClassSubMenuItemOver = value; 
			}
		}

		public string CssClassSubMenuSeparator
		{
			get 
			{
				return mstrCssClassSubMenuSeparator; 
			}
			set 
			{
				mstrCssClassSubMenuSeparator = value; 
			}
		}

		public MenuModeEnum MenuMode
		{
			get
			{
				return _MenuMode;
			}
			set
			{
				_MenuMode=value;			   
			}
		}

		public string MenuName
		{
			get
			{
				return mstrMenuName;
			}
			set
			{
				mstrMenuName=value;			   
			}
		}

		public DataSet DataSource
		{
			get
			{
				return mDataSet;
			}
			set
			{
				mDataSet=value;			   
			}
		}

		public bool ShowPictureForTop
		{
			get
			{
				return mbooShowPictureForTop;
			}
			set
			{
				mbooShowPictureForTop=value;			   
			}
		}

		public bool ShowPictureForItems
		{
			get
			{
				return mbooShowPictureForItems;
			}
			set
			{
				mbooShowPictureForItems =value;			   
			}
		}
		#endregion 
	}
}