using System;

namespace NixorControls
{
	/// <summary>
	/// El control CUITBox hereda la funcionalidad del textbox y
	/// le agrega la validacion del NRO de CUIT (solo acepta numeros).
	/// Contiene la mascara (xx-xxxxxxxx-x)
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// </summary>
	public class CUITBox:TextBoxControls
	{
	
//		private string mstrIncludesUrl = "Includes/";
//		private string mstrEnterPorTab = "True";
//		private string mstrCambiaValor = "False";
//		private bool mbooAceptaNull = true;
//		private bool mbooObligatorio = true;
//		private string mstrCampoVal = "";
		
        /* 11/05/2021 GM
		public object TextoPlano
		{
			get 
			{
				if(base.Text==String.Empty)
				{
					if(AceptaNull)
						return(DBNull.Value.ToString());
					else
						return("0");
				}
				else
					return(ObtenerTextoPlano(base.Text));
			}
			set 
			{
				if(value.ToString() == string.Empty )
					base.Text = String.Empty;
				else
				{
					if(value.ToString().Length>9)
						base.Text = value.ToString().Insert(2,"-").Insert(11,"-"); 
				}
			}
		}
        */

        public string TextoPlano
        {
            get
            {
                if (base.Text == String.Empty)
                {
                    if (AceptaNull)
                        return (DBNull.Value.ToString());
                    else
                        return ("0");
                }
                else
                    return (ObtenerTextoPlano(base.Text));
            }
            set
            {
                if (value.ToString() == string.Empty)
                    base.Text = String.Empty;
                else
                {
                    if (value.ToString().Length > 9)
                        base.Text = value.ToString().Insert(2, "-").Insert(11, "-");
                }
            }
        }

		private string ObtenerTextoPlano(string stexto)
		{
			if(stexto.Length>9)
			{
				stexto = stexto.Remove(2, 1);
				stexto = stexto.Remove(10, 1);
			}
			return(stexto);
		}
	
		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

			if(!base.Page.IsClientScriptBlockRegistered("NixorCUITBox"))
				Page.RegisterClientScriptBlock("NixorCUITBox", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCUIT.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));

			if (bool.Parse(EnterPorTab) || bool.Parse(CambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			
			if (EnterPorTab.Trim()=="true")//(bool.Parse(EnterPorTab))
				base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

			if (EnterPorTab.Trim()=="true") //(bool.Parse(CambiaValor))
				base.Attributes.Add("onchange", "javascript:mCambioValor();");
			
			base.Attributes.Add("onblur", "javascript:gValCUIT(this, "+ (base.AutoPostBack ? "1" : "0") +");");
			base.Attributes.Add("onkeypress", "javascript:gMascaraCUIT(this);");

			if(Obligatorio)
				base.Attributes.Add("Obligatorio", "true");

			base.AutoPostBack = false; 

			base.MaxLength=13;
			base.Render(pWriter);
		}
	}	
}

