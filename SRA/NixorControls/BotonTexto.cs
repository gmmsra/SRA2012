using System;
using System.Text;

namespace NixorControls
{
	/// <summary>
	/// El control BotonTexto hereda la funcionalidad del BotonImagen
	/// Las propiedades agregadas al ImageButton son:
	/// - Texto			: especifica el texto a mostrar.
	/// - ControlTexto	: especifica el control desde el cual tomar el texto.
	/// - ComboItem		: especifica el menor indice del combo con el texto a mostrar, por default "0" (solo si ControlTexto es de tipo combo)
	/// </summary>
	public class BotonTexto:BotonImagen 
	{
		private string mstrTexto = "";
		private string mstrControlTexto = "";
		private string mstrComboItem = "0";
		
		public string ComboItem
		{
			get 
			{
				return mstrComboItem; 
			}
			set 
			{
				mstrComboItem = value; 
			}
		}

		public string ControlTexto
		{
			get 
			{
				return mstrControlTexto; 
			}
			set 
			{
				mstrControlTexto = value; 
			}
		}

		public string Texto
		{
			get 
			{
				return mstrTexto; 
			}
			set 
			{
				mstrTexto = value; 
			}
		}
		
		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

			if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
				Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}paneles.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			base.ImageAnim = "False";	
		
			string lstrAccion = "javascript:";
			if (mstrControlTexto.Trim() == "")
				lstrAccion = lstrAccion + string.Format("document.all['lbl{0}'].innerText = '"+mstrTexto+"';", base.ClientID);
			else
			{
				if (mstrControlTexto.Substring(0,3)=="cmb")
				{
					lstrAccion = lstrAccion + string.Format(" document.all['lbl{0}'].innerText = document.all['"+mstrControlTexto+"'].options[document.all['"+mstrControlTexto+"'].selectedIndex].text; ", base.ClientID);
					lstrAccion = lstrAccion + " if (document.all['"+mstrControlTexto+"'].selectedIndex >= "+ComboItem.ToString()+")";
					lstrAccion = lstrAccion + string.Format("gMostrarPanel('pan{0}',event.x-280,event.y,'visible');", base.ClientID);
				}
				else
				{
					lstrAccion = lstrAccion + string.Format("document.all['lbl{0}'].innerText = ", base.ClientID);
					lstrAccion = lstrAccion + "document.all['"+mstrControlTexto+"'].value;";
					lstrAccion = lstrAccion + string.Format("gMostrarPanel('pan{0}',event.x-280,event.y,'visible');", base.ClientID);
				}
			}			
			
			base.Attributes.Add("onmouseover", lstrAccion);
			base.Attributes.Add("onmouseout", string.Format("javascript:gMostrarPanel('pan{0}',0,0,'hidden');", base.ClientID));
							
			base.Render(pWriter);

			StringBuilder lsbCtrls;
			lsbCtrls = new StringBuilder();
			  
			lsbCtrls.Append(string.Format("<div class=paneldetalle id=pan{0} style='WIDTH: 300; LEFT: 50px; VISIBILITY: hidden; POSITION: absolute; TOP: 150px'>", base.ClientID));
			lsbCtrls.Append("<TABLE id=Tabla border=0 height=100% width=100%>");			
			lsbCtrls.Append("<TR><TD width=10></TD>");
			lsbCtrls.Append("<TD align=left valign=middle>");			
			lsbCtrls.Append(string.Format("<span class=textodetalle id=lbl{0}></span>", base.ClientID));
			lsbCtrls.Append("</TD><TD width=10></TD><TR></TR></TABLE></div>");

			pWriter.Write(lsbCtrls.ToString());

		}
	}
}
