using System;

namespace NixorControls
{
	/// <summary>
	/// El control NumberBox hereda la funcionalidad del textbox y
	/// le agrega validaciones numericas.
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// - EsDecimal		: especifica si el contenido puede ser un numero decimal, por default "False"
	/// - EsPorcen		: especifica si el contenido puede ser un numero decimal, por default "False"
	/// - CantMax		: especifica la cantidad maxima de posiciones de la parte entera, por default "8"
	/// - MaxValor		: especifica el Valor Maximo que puede tener el control, por defecto es 1000000
	
	/// </summary>
	public class TextBoxControls:System.Web.UI.WebControls.TextBox
	{
		private string mstrIncludesUrl = "Includes/";
		private string mstrEnterPorTab = "True";
		private string mstrCambiaValor = "False";
		private bool mbooAceptaNull = true;
		private bool mbooObligatorio = false;
		private string mstrCampoVal = "";
		private string mstrPanel = "";

		public string IncludesUrl
		{
			get 
			{
				return (Utils.gPath(Page) + mstrIncludesUrl); 
			}
			set 
			{
				mstrIncludesUrl = value; 
			}
		}

		public string EnterPorTab
		{
			get 
			{
				return mstrEnterPorTab; 
			}
			set 
			{
				mstrEnterPorTab = value; 
			}
		}

		public string CambiaValor
		{
			get 
			{
				return mstrCambiaValor; 
			}
			set 
			{
				mstrCambiaValor = value; 
			}
		}

		public string CampoVal
		{
			get 
			{
				return mstrCampoVal; 
			}
			set 
			{
				mstrCampoVal = value; 
			}
		}

		public string Panel 
		{
			get 
			{
				return mstrPanel; 
			}
			set 
			{
				mstrPanel = value; 
			}
		}


		public bool AceptaNull
		{
			get 
			{
				return mbooAceptaNull; 
			}
			set 
			{
				mbooAceptaNull = value; 
			}
		}

		public bool Obligatorio
		{
			get 
			{
				return mbooObligatorio; 
			}
			set 
			{
				mbooObligatorio = value; 
			}
		}

	}
}
