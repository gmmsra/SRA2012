using System;

namespace NixorControls
{
	/// <summary>
	/// El control TextBox hereda la funcionalidad del textbox y
	/// le agrega el comportamiento del ENTER como TAB.
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// </summary>
public class CheckBoxSimple:System.Web.UI.WebControls.CheckBox 
{
private string mstrIncludesUrl = "Includes/";
private string mstrEnterPorTab = "True";
private string mstrCambiaValor = "False";

public string IncludesUrl
{
get 
{
return (Utils.gPath(Page) + mstrIncludesUrl); 
}
set 
{
mstrIncludesUrl = value; 
}
}

public string EnterPorTab
{
get 
{
return mstrEnterPorTab; 
}
set 
{
mstrEnterPorTab = value; 
}
}

public string CambiaValor
{
get 
{
return mstrCambiaValor; 
}
set 
{
mstrCambiaValor = value; 
}
}

protected override void OnPreRender(System.EventArgs e)
{
base.OnPreRender(e);

if (bool.Parse(mstrEnterPorTab) || bool.Parse(mstrCambiaValor))
{
if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
}

}

protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
{
	string lstrEvento = "";

	if(bool.Parse(mstrEnterPorTab))
		lstrEvento = "mEnterPorTab();";

	if(bool.Parse(mstrCambiaValor))
		lstrEvento = lstrEvento + "mCambioValor();";

	if (lstrEvento != "")
		base.Attributes.Add("onclick", "javascript:"+lstrEvento);

	if(bool.Parse(mstrEnterPorTab))
		base.Attributes.Add("onkeydown", "javascript:mEnterPorTab()");

	base.Render(pWriter);
}
}
}
