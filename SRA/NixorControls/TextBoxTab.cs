using System;

namespace NixorControls
{
	/// <summary>
	/// El control TextBox hereda la funcionalidad del textbox y
	/// le agrega el comportamiento del ENTER como TAB.
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// - EsMail		: especifica si el contenido debe tener el formato de una direccion de mail
	/// </summary>
	public class TextBoxTab:TextBoxControls
	{
//		private string mstrIncludesUrl = "Includes/";
//		private string mstrEnterPorTab = "True";
//		private string mstrCambiaValor = "False";
		private string mstrEsMail = "False";
		private string mstrTag;
//		private bool mbooAceptaNull = true;
//		private bool mbooObligatorio = false;
//		private string mstrCampoVal = "";

		public string Tag
		{
			get 
			{
				return mstrTag; 
			}
			set 
			{
				mstrTag = value; 
			}
		}

		public string EsMail
		{
			get 
			{
				return mstrEsMail; 
			}
			set 
			{
				mstrEsMail = value; 
			}
		}

        /* GM 05/05/2021 
		public object Valor
		{
			get 
			{
				if(Text=="")
				{
					if(AceptaNull)
						return(DBNull.Value);
					else
						return("");
				}
				else
					return(Text.Trim());
			}
			set 
			{
				if(value == DBNull.Value)
				{
					Text="";
				}
				else
				{
					Text = value.ToString().Trim();
				}
			}
		}
        */

        public string Valor
        {
            get
            {
                if (Text == string.Empty)
                {
                    if (AceptaNull)
                        return (DBNull.Value.ToString());
                    else
                        return (string.Empty);
                }
                else
                    return (Text.Trim());
            }
            set
            {
                if (value == DBNull.Value.ToString())
                {
                    Text = string.Empty;
                }
                else
                {
                    Text = value.ToString().Trim();
                }
            }
        }

		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

			if (bool.Parse(EnterPorTab) || bool.Parse(CambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}

			if(bool.Parse(mstrEsMail))
			{
					if(!base.Page.IsClientScriptBlockRegistered("NixorMail"))
					Page.RegisterClientScriptBlock("NixorMail", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valMail.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
            if (EnterPorTab.Trim() == "true") // (bool.Parse(EnterPorTab))
				base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

            if (CambiaValor.Trim() == "true") // (bool.Parse(CambiaValor))
				base.Attributes.Add("onchange", "javascript:mCambioValor();");

            if (mstrEsMail.Trim() == "true") // (bool.Parse(mstrEsMail))
			{
				base.Attributes.Add("onblur", "javascript:gValMail(this);");
			}
			else
			{
				if(this.TextMode == System.Web.UI.WebControls.TextBoxMode.MultiLine && this.MaxLength != 0)
					base.Attributes.Add("onblur", "javascript:if ("+this.MaxLength.ToString()+"<this.value.length) { alert('Se han ingresado '+this.value.length+' caracteres en este campo y la longitud m�xima permitida es de "+this.MaxLength.ToString()+" caracteres.'); this.focus(); }");
			}
			
			if(this.TextMode == System.Web.UI.WebControls.TextBoxMode.MultiLine && this.MaxLength != 0)
				base.Attributes.Add("onkeypress", "javascript:if ("+this.MaxLength.ToString()+"<=this.value.length) { event.keyCode = 27 }");

			if(Obligatorio)
				base.Attributes.Add("Obligatorio", "true");

			base.Render(pWriter);
		}
	}
}