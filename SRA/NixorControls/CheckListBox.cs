using System;
using System.Web.UI.WebControls;
using System.Text;
using System.ComponentModel;

namespace NixorControls
{
	/// <summary>
	/// El control CheckList hereda la funcionalidad del CheckBoxList y
	/// le agrega una columna template con un checkbox.
	/// Las propiedades agregadas al CheckBoxList son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	
	/// Las propiedades publicas son:
	/// - ObtenerIds	: Obtiene los ids de los itemes seleccionados, concatenados con ","
	/// - SeleccionarIds: Selecciona los items indicados
	/// - TodosSelec	: Devuelve true si estan todos los items seleccionados y permite seleccionarlos
	/// </summary>

	public class CheckListBox:CheckBoxList
	{
		protected string mstrIncludesUrl = "Includes/";
		protected string mstrEnterPorTab = "True";
		protected string mstrCambiaValor = "False";
		protected bool mbooMuestraBotones = false;
		protected bool mbooAceptaNull = true;
		protected bool mbooObligatorio = false;
		protected int mintMinScroll = 0;
		protected string mstrCampoVal = "";
		protected string mstrPanel = "";
		private string mBotonTodosText = "Todos";
		private string mBotonNingunoText = "Ninguno";

		[DefaultValue("Todos")]
		[Browsable(true)]
		public string BotonTodosText
		{
			get
			{
				return mBotonTodosText;
			}
			set
			{
				mBotonTodosText = value;
			}
		}

		[DefaultValue("Ninguno")]
		[Browsable(true)]
		public string BotonNingunoText
		{
			get
			{
				return mBotonNingunoText;
			}
			set
			{
				mBotonNingunoText = value;
			}
		}

		public string IncludesUrl
		{
			get 
			{
				return (Utils.gPath(Page) + mstrIncludesUrl); 
			}
			set 
			{
				mstrIncludesUrl = value; 
			}
		}

		public string EnterPorTab
		{
			get 
			{
				return mstrEnterPorTab; 
			}
			set 
			{
				mstrEnterPorTab = value; 
			}
		}

		public string CambiaValor
		{
			get 
			{
				return mstrCambiaValor; 
			}
			set 
			{
				mstrCambiaValor = value; 
			}
		}

		public string CampoVal
		{
			get 
			{
				return mstrCampoVal; 
			}
			set 
			{
				mstrCampoVal = value; 
			}
		}

		public string Panel 
		{
			get 
			{
				return mstrPanel; 
			}
			set 
			{
				mstrPanel = value; 
			}
		}


		public bool AceptaNull
		{
			get 
			{
				return mbooAceptaNull; 
			}
			set 
			{
				mbooAceptaNull = value; 
			}
		}

		public bool Obligatorio
		{
			get 
			{
				return mbooObligatorio; 
			}
			set 
			{
				mbooObligatorio = value; 
			}
		}

		
		public bool MuestraBotones
		{
			get 
			{
				return mbooMuestraBotones; 
			}
			set 
			{
				mbooMuestraBotones= value; 
			}
		}

		public bool TodosSelec
		{
			get 
			{
				foreach(ListItem loItem in base.Items)
				{
					if (!loItem.Selected)
					{
						return(false);
					}
				}
				return(true);
			}
			set 
			{
				foreach(ListItem loItem in base.Items)
					loItem.Selected=value;
			}
		}

		public int MinScroll
		{
			get 
			{
				return mintMinScroll; 
			}
			set 
			{
				mintMinScroll = value; 
			}
		}

		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

			if(!base.Page.IsClientScriptBlockRegistered("NixorCheckListBox"))
				Page.RegisterClientScriptBlock("NixorCheckListBox", String.Format("<SCRIPT language=\"javascript\" src=\"{0}checklist.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));

			if (bool.Parse(mstrEnterPorTab) || bool.Parse(mstrCambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			StringBuilder lsbCtrls;
			if(bool.Parse(mstrEnterPorTab))
				base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

			if(bool.Parse(mstrCambiaValor))
				base.Attributes.Add("onclick", "javascript:mCambioValor();");

			if (mintMinScroll >= Items.Count) 
				base.Height = Unit.Parse("");

			lsbCtrls = new StringBuilder();
			lsbCtrls.Append(string.Format("<TABLE width='{0}' border=0><tr><td colspan=2>", base.Width));
			lsbCtrls.Append(string.Format("<SPAN style='OVERFLOW-Y: auto;WIDTH:{0};HEIGHT:{1}'>", base.Width, base.Height));
			pWriter.Write(lsbCtrls.ToString());
			base.Render(pWriter);

			lsbCtrls.Length=0;
			lsbCtrls.Append("</SPAN></td></tr>");
			
			if(MuestraBotones)
			{
				lsbCtrls.Append("<tr><td width=50% align=center>");
				lsbCtrls.Append(string.Format("<input type='button' value='{0}' onclick='return chlstTodos(\"{1}\");'", mBotonTodosText, base.ClientID));
				if (base.CssClass!="")
					lsbCtrls.Append(string.Format(" class={0}", base.CssClass));
				if (!base.Enabled)
					lsbCtrls.Append(" DISABLED ");
				lsbCtrls.Append("></td><td width=50% align=center>");
				lsbCtrls.Append(string.Format("<input type='button' value='{0}' onclick='return chlstNinguno(\"{1}\");'", mBotonNingunoText, base.ClientID));
				if (base.CssClass!="")
					lsbCtrls.Append(string.Format(" class={0}", base.CssClass));
				if (!base.Enabled)
					lsbCtrls.Append(" DISABLED ");
				lsbCtrls.Append("></td></tr>");
			}
			lsbCtrls.Append("</table>");
			pWriter.Write(lsbCtrls.ToString());

			if(base.Enabled==false) 
			{
				base.Page.Session[base.ClientID + "_disab"] = ObtenerIDs();
			}
		}

		protected override void OnLoad(System.EventArgs e)
		{
			if(base.Enabled==false) 
			{
				string lstrIds = ObtenerIDs();
				if(lstrIds=="" && base.Page.Session[base.ClientID + "_disab"]!=null) 
					SeleccionarIds(base.Page.Session[base.ClientID + "_disab"].ToString());
			}

			base.OnLoad(e);
		}

		public void Limpiar() 
		{
			foreach(ListItem loItem in base.Items)
				loItem.Selected = false;
		}

		public string ObtenerIDs()
		{
			StringBuilder lsbCtrls;
			lsbCtrls = new StringBuilder();
			foreach(ListItem loItem in base.Items)
			{
				if (loItem.Selected)
				{
					if(lsbCtrls.Length!=0)
						lsbCtrls.Append(",");
					lsbCtrls.Append(loItem.Value);
				}
			}
			return(lsbCtrls.ToString());
		}

		public void SeleccionarIds(string pstrIds)
		{
			string[] lvsIds = pstrIds.Split(char.Parse(","));
			ListItem lItem;

			Limpiar();

			for(int i=0;i<=lvsIds.GetUpperBound(0);i++)
			{
				lItem = base.Items.FindByValue(lvsIds[i]);
				if (lItem!=null)
					lItem.Selected=true;
			}
		}

		public void SeleccionarIds(System.Data.DataTable pdtTabla, string pstrCampoId)
		{
			ListItem lItem;

			Limpiar();

			foreach(System.Data.DataRow lDr in pdtTabla.Rows)
			{
				 lItem = base.Items.FindByValue(lDr[pstrCampoId].ToString());
				 if (lItem!=null)
					 lItem.Selected=true;
			}
		}
	}
}