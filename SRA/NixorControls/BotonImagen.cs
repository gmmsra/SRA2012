using System;

namespace NixorControls
{
	/// <summary>
	/// El control BotonImagen hereda la funcionalidad del ImageButton
	/// Las propiedades agregadas al ImageButton son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - ImagesUrl		: especifica el directorio donde estan las imagenes, por default "Images/"
	/// - ImageBoton	: especifica el nombre de la imagen del boton.
	/// - ImageAnim		: especifica si se cambia la imagen al pasar el mouse sobre el boton, por default "True".
	/// - ImageOver		: especifica el nombre de la imagen al pasar el mouse sobre el boton.
	/// - Accion		: especifica el nombre de la funcion javascript que se ejecuta al clickear sobre el boton.
	/// </summary>
	public class BotonImagen:System.Web.UI.WebControls.ImageButton 
	{
		private string mstrIncludesUrl = "Includes/";
		private string mstrImagesUrl = "Images/";
		private string mstrImageBoton = "";
		private string mstrImageOver = "";
		private string mstrImageDisable = "";
		private string mstrImageAnim = "True";
		private string mstrAccion = "";

		public string ImageAnim
		{
			get 
			{
				return mstrImageAnim; 
			}
			set 
			{
				mstrImageAnim = value; 
			}
		}
		
		public string Accion
		{
			get 
			{
				return mstrAccion; 
			}
			set 
			{
				mstrAccion = value; 
			}
		}
		
		public string IncludesUrl
		{
			get 
			{
				return (Utils.gPath(Page) + mstrIncludesUrl); 
			}
			set 
			{
				mstrIncludesUrl = value; 
			}
		}

		public string ImagesUrl
		{
			get 
			{
				return mstrImagesUrl; 
			}
			set 
			{
				mstrImagesUrl = value; 
			}
		}

		public string ImageBoton
		{
			get 
			{
				return mstrImageBoton; 
			}
			set 
			{
				mstrImageBoton = value; 
			}
		}

		public string ImageOver
		{
			get 
			{
				return mstrImageOver; 
			}
			set 
			{
				mstrImageOver = value; 
			}
		}

		public string ImageDisable
		{
			get 
			{
				return mstrImageDisable; 
			}
			set 
			{
				mstrImageDisable = value; 
			}
		}
		
		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			if (mstrImageAnim == "True")

			{
				if (Enabled)
				{
					base.Attributes.Add("onmouseover", "javascript:this.src='"+mstrImagesUrl+mstrImageOver+"';");
					base.Attributes.Add("onmouseout", "javascript:this.src='"+mstrImagesUrl+mstrImageBoton+"';");
					base.ImageUrl = mstrImagesUrl + mstrImageBoton;
				}
				else
				{
					base.ImageUrl = mstrImagesUrl + mstrImageDisable;
				}
			}

			if (mstrAccion.Trim() != "")
				base.Attributes.Add("onclick", mstrAccion);
							
			base.Render(pWriter);
		}
	}
}
