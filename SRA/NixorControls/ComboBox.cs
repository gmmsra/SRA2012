using System;
using System.Text;

namespace NixorControls
{
	/// <summary>
	/// El control ComboBox hereda la funcionalidad del combo y
	/// le agrega la busqueda por un texto usando xmlhttp.
	/// Las propiedades agregadas al combo son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// - TextSize		: especifica el tamanio del textbox de filtro, por default "5"
	/// - Filtra		: especifica si tiene filtro previo, por default "False"
	/// - NomOper		: especifica el nombre de la operacion de carga XMLHTTP
	/// - MostrarBotones: especifica si se muestran los botones de b�squeda en el combo, por default "True"
	/// - BuscAvan		: especifica si se muestra el boton de busqueda avanzada, por default "False" (debe indicarse la propiedad Filtra en "True")
	/// - ParaValores   : especifica los valores de las opciones de b�squeda avanzada, por default "C,D"
	/// - Parametros	: especifica las descripciones de las opciones de b�squeda avanzada, por default "C�digo,Descripci�n"
	/// - ParaSepara	: especifica el separador para los parametros, por default ","
	/// 
	/// Las propiedades publicas son:
	/// - Valor	: obtiene el valor del item seleccionado, o null si no hay ninguno seleccionado y el combo acepta null
	/// - ValorBool	: obtiene el valor del item seleccionado transformado a booleano, para cuando es 0 o 1, o null si no hay ninguno seleccionado y el combo acepta null
	/// </summary>
	public class ComboBox:System.Web.UI.WebControls.DropDownList,System.Web.UI.IPostBackEventHandler
	{
		private string mstrIncludesUrl = "Includes/";
		private string mstrImagesUrl = "Images/";
		private string mstrEnterPorTab = "True";
		private string mstrCambiaValor = "False";
		private string mstrTextSize = "5";
		private string mstrMaxLength = "";
		private string mstrFiltroDefault = "1";
		private string mstrFiltra = "False";
		private string mstrNomOper = "";
		private bool mbooAceptaNull = true;
		private bool mbooObligatorio = false;
		private string mstrCampoVal = "";
		//private bool mstrDefaultValue = true;
		private string mstrBuscAvan = "False";
		private string mstrMostrarBoton = "True";
		private string mstrParaValores = "C,D";
		private string mstrParametros = "C�digo,Descripci�n";
		private string mstrParaSepara = ",";
		private string mstrChangeEvent = "";
		private bool mbooDisparaEvento = true;

  	  //  public string DefaultValue
	//	{
//			get 
//			{
//				return mstrDefaultValue; 
//			}
//			set 
//			{
//				mstrDefaultValue = value; 
//			}
//	}

		public string onchange
		{
			get 
			{
				return mstrChangeEvent; 
			}
			set 
			{
				mstrChangeEvent = value; 
			}
		}

		public string MostrarBotones
		{
			get 
			{
				return mstrMostrarBoton; 
			}
			set 
			{
				mstrMostrarBoton = value; 
			}
		}
				
		public string ParaFiltroDefault
		{
			get 
			{
				return mstrFiltroDefault; 
			}
			set 
			{
				mstrFiltroDefault = value; 
			}
		}

		public string ParaSepara
		{
			get 
			{
				return mstrParaSepara; 
			}
			set 
			{
				mstrParaSepara = value; 
			}
		}

		public string Parametros
		{
			get 
			{
				return mstrParametros; 
			}
			set 
			{
				mstrParametros = value; 
			}
		}
		
		public string ParaValores
		{
			get 
			{
				return mstrParaValores; 
			}
			set 
			{
				mstrParaValores = value; 
			}		
		}

		public string BusquedaAvanzada
		{
			get 
			{
				return mstrBuscAvan; 
			}
			set 
			{
				mstrBuscAvan = value; 
			}
		}

		public string ImagesUrl
		{
			get 
			{
				return mstrImagesUrl; 
			}
			set 
			{
				mstrImagesUrl = value; 
			}
		}

		public string IncludesUrl
		{
			get 
			{
				return (Utils.gPath(Page) + mstrIncludesUrl); 
			}
			set 
			{
				mstrIncludesUrl = value; 
			}
		}

		public string EnterPorTab
		{
			get 
			{
				return (Utils.gPath(Page) + mstrIncludesUrl); 
			}
			set 
			{
				mstrEnterPorTab = value; 
			}
		}

		public string CambiaValor
		{
			get 
			{
				return mstrCambiaValor; 
			}
			set 
			{
				mstrCambiaValor = value; 
			}
		}

		public string Filtra
		{
			get 
			{
				return mstrFiltra;
			}
			set 
			{
				mstrFiltra = value; 
			}
		}

		public string NomOper
		{
			get 
			{
				return mstrNomOper;
			}
			set 
			{
				mstrNomOper = value; 
			}
		}

		public string TextSize
		{
			get 
			{
				return mstrTextSize;
			}
			set 
			{
				mstrTextSize = value; 
			}
		}

		public string TextMaxLength
		{
			get 
			{
				return mstrMaxLength;
			}
			set 
			{
				mstrMaxLength = value; 
			}
		}

		public string CampoVal
		{
			get 
			{
				return mstrCampoVal; 
			}
			set 
			{
				mstrCampoVal = value; 
			}
		}
		
		public bool AceptaNull
		{
			get 
			{
				return mbooAceptaNull; 
			}
			set 
			{
				mbooAceptaNull = value; 
			}
		}

		public bool Obligatorio
		{
			get 
			{
				return mbooObligatorio; 
			}
			set 
			{
				mbooObligatorio = value; 
			}
		}

        /* GM 05/05/2021 Se reemplaza porque no castea correctamente.
		public object Valor
		{
			get 
			{
				if(SelectedValue=="")
				{
					if(AceptaNull)
						return(DBNull.Value);
					else
						return(0);
				}
				else
					return(SelectedValue);
			}
			set 
			{
				mLimpiarInactivos();

				if(value == DBNull.Value)
				{
					SelectedIndex=-1;
				}
				else
				{
					SelectedIndex = Items.IndexOf(Items.FindByValue(value.ToString()));
					if(SelectedIndex==0&&SelectedValue!=value.ToString()&&value.ToString()!=""&&value.ToString()!="0"&&value.ToString()!="-1")
						mCargarValorInactivo(value.ToString());
				}
			}
		}

		public object ValorBool
		{
			get 
			{
				if(SelectedValue=="")
				{
					if(AceptaNull)
						return(DBNull.Value);
					else
						return(false);
				}
				else
					return(Convert.ToBoolean(int.Parse(SelectedValue)));
			}
			set 
			{
				if(value == DBNull.Value)
				{
					SelectedIndex=-1;
				}
				else
				{
					if((bool)value)
						Valor="1";
					else
						Valor="0";
				}
			}
		}
        */

        public string Valor
        {
            get
            {
                if (SelectedValue == string.Empty)
                {
                    if (AceptaNull)
                        return (DBNull.Value.ToString());
                    else
                        return ("0");
                }
                else
                    return (SelectedValue);
            }
            set
            {
                mLimpiarInactivos();

                if (value == DBNull.Value.ToString())
                {
                    SelectedIndex = -1;
                }
                else
                {
                    SelectedIndex = Items.IndexOf(Items.FindByValue(value.ToString()));
                    if (SelectedIndex == 0 && SelectedValue != value.ToString() && value.ToString() != "" && value.ToString() != "0" && value.ToString() != "-1")
                        mCargarValorInactivo(value.ToString());
                }
            }
        }

        public bool ValorBool
        {
            get
            {
                if (SelectedValue == "")
                {
                    if (AceptaNull)
                        return (Convert.ToBoolean(DBNull.Value));
                    else
                        return (false);
                }
                else
                    return (Convert.ToBoolean(int.Parse(SelectedValue)));
            }
            set
            {
                if ( String.IsNullOrEmpty(value.ToString())) // value == Convert.ToBoolean(DBNull.Value))
                {
                    SelectedIndex = -1;
                }
                else
                {
                    if ((bool)value)
                        Valor = "1";
                    else
                        Valor = "0";
                }
            }
        }

		//public string TraerValorPorDefault()
		//{
		//	return(ObtenerValorPorDefault(mstrDefault));
		//}

	//	private string ObtenerTraerValorPorDefault(string sTabla)
//		{
			
		//	stexto = stexto.Remove(2, 1);
		//	stexto = stexto.Remove(10, 1);
	//		return(stexto);
//		}

		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

			if (bool.Parse(mstrEnterPorTab) || bool.Parse(mstrCambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
            if (mstrBuscAvan.Trim().ToUpper() == "TRUE") //(mstrBuscAvan == "True")
			{
				Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}paneles.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}		
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			if(mstrChangeEvent != "" )
				base.Attributes.Add("onchange", mstrChangeEvent);

			if (mstrEnterPorTab.Trim().ToUpper() == "TRUE") //(mstrEnterPorTab.Trim() == "true") //(bool.Parse(mstrEnterPorTab))
				base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

			if (mstrCambiaValor.Trim().ToUpper() == "TRUE") //(mstrCambiaValor.Trim() == "true") //(bool.Parse(mstrCambiaValor))
				base.Attributes.Add("onchange", "javascript:mCambioValor();");

			if (!base.Enabled || ((Parent.GetType().Name=="Panel")&& !((System.Web.UI.WebControls.Panel)Parent).Enabled))
				base.Enabled=false;

            if (mstrFiltra.Trim().ToUpper() == "TRUE") //(mstrFiltra.Trim() == "true") //(bool.Parse(mstrFiltra))
			{
				StringBuilder lsbCtrls;
				lsbCtrls = new StringBuilder();
				lsbCtrls.Append(string.Format("<table width={0} cellpadding=0 cellspacing=0 border=0><tr><td>", base.Width));
				lsbCtrls.Append(string.Format("<input type='text' id='txt{0}'", base.UniqueID));
				if (base.CssClass!="")
					lsbCtrls.Append(string.Format(" class={0}", base.CssClass));

				if (!base.Enabled || ((Parent.GetType().Name=="Panel")&& !((System.Web.UI.WebControls.Panel)Parent).Enabled))
					lsbCtrls.Append(" disabled");

                if (mstrMostrarBoton.Trim().ToUpper() == "FALSE" && mstrNomOper != string.Empty) //(mstrMostrarBoton == "False" && mstrNomOper!="")
				{
					lsbCtrls.Append(" onkeydown='javascript:mEnterPorTab();'");
					lsbCtrls.Append(string.Format(" onchange='javascript:mBuscarIdXCodi(\"{0}\", \"{1}\", \"Id\");'", base.UniqueID,mstrNomOper));
				
					if(base.SelectedValue!="")
						lsbCtrls.Append(string.Format(" value='{0}'", mObtenerCodiXId(SelectedValue)));

					base.Attributes.Add("onchange", string.Format("javascript:mBuscarIdXCodi('{0}', '{1}', 'Codi');" +mstrChangeEvent, base.UniqueID,mstrNomOper));
				}
				
				lsbCtrls.Append(string.Format(" size={0}", mstrTextSize));
				lsbCtrls.Append(string.Format(" MaxLength={0}>", mstrMaxLength));
				lsbCtrls.Append("</td><td valign=bottom>");

                if (mstrMostrarBoton.Trim().ToUpper() == "TRUE")//(mstrMostrarBoton == "True")
				{
					lsbCtrls.Append(string.Format("<IMG language='javascript' onclick='return Busc{0}_click();'", base.UniqueID));
					lsbCtrls.Append(string.Format(" src='{0}Buscar16.gif'", mstrImagesUrl));

					lsbCtrls.Append(" STYLE='border-right: thin outset;");
					lsbCtrls.Append("border-top: thin outset;");
					lsbCtrls.Append("border-left: thin outset;");
					lsbCtrls.Append("border-bottom: thin outset;");
					lsbCtrls.Append("cursor: hand'>");
				}

				// Busqueda Avanzada
                if (mstrBuscAvan.Trim().ToUpper() == "TRUE" && mstrMostrarBoton.Trim().ToUpper() == "TRUE") //(mstrBuscAvan == "True" && mstrMostrarBoton == "True")
				{
					string[] mstrPara;
					string[] mstrParaValor;
					string lstrValor = "";
					mstrPara = mObtenerValores(mstrParametros);
					mstrParaValor = mObtenerValores(mstrParaValores);
					if (mstrFiltroDefault != "1" && mstrFiltroDefault != "2" && mstrFiltroDefault != "3" && mstrFiltroDefault != "4")
						mstrFiltroDefault = "1";
					if (mstrParaValor.Length < Convert.ToInt32(mstrFiltroDefault))
						mstrFiltroDefault = "1";
					if (mstrParaValor.Length > 0)
						lstrValor = mstrParaValor[Convert.ToInt32(mstrFiltroDefault)-1];
					lsbCtrls.Append("<img border=0 ");
					lsbCtrls.Append(string.Format(" src='" + mstrImagesUrl + "buscavan.gif' alt='B�squeda Avanzada' onclick=javascript:mBuscMostrar('panBuscAvan{0}','visible',event.x,event.y);", base.UniqueID));
					lsbCtrls.Append(" STYLE='cursor: hand'>&nbsp;");
					lsbCtrls.Append(string.Format("<div class=paneledicion style='width=200;' id=panBuscAvan{0} style='Z-INDEX: 101; LEFT: 200px; POSITION: absolute; TOP: 280px; visibility=hidden'>", base.UniqueID));
					lsbCtrls.Append("<table border=0 align=center valign=middle width=100%>");
					lsbCtrls.Append("<tr><td colspan=2 align=center valign=middle height=34><span class=titulo>B�squeda Avanzada</span>");
					lsbCtrls.Append(string.Format("<input type=hidden size=1 name=hdnBusc{0} value=" + lstrValor + ">", base.UniqueID));
					lsbCtrls.Append("</td></tr>");
					lsbCtrls.Append("<tr><td width=20></td>");
					string lstrChecked = "";
					if (mstrParaValor.Length > 0)
					{
						if (mstrFiltroDefault == "1")
							lstrChecked = " checked ";
						else
							lstrChecked = "";
						lsbCtrls.Append(string.Format("<td><input type=radio value=" + mstrParaValor[0] + " name=grpBuscAvan{0} id=opt{0}1 onclick=javascript:mBuscSele(this.value,'hdnBusc{0}'); " + lstrChecked + "><span class=titulo>" + mstrPara[0] + "</span>", base.UniqueID));
					}
					if (mstrParaValor.Length > 1)
					{
						if (mstrFiltroDefault == "2")
							lstrChecked = " checked ";
						else
							lstrChecked = "";
						lsbCtrls.Append(string.Format("<br><input type=radio value=" + mstrParaValor[1] + " name=grpBuscAvan{0} id=opt{0}2 onclick=javascript:mBuscSele(this.value,'hdnBusc{0}'); " + lstrChecked + "><span class=titulo>" + mstrPara[1] + "</span>", base.UniqueID));
					}
					if (mstrParaValor.Length > 2)
					{
						if (mstrFiltroDefault == "3")
							lstrChecked = " checked ";
						else
							lstrChecked = "";
						lsbCtrls.Append(string.Format("<br><input type=radio value=" + mstrParaValor[2] + " name=grpBuscAvan{0} id=opt{0}3 onclick=javascript:mBuscSele(this.value,'hdnBusc{0}'); " + lstrChecked + "><span class=titulo>" + mstrPara[2] + "</span>", base.UniqueID));
					}
					if (mstrParaValor.Length > 3)
					{
						if (mstrFiltroDefault == "4")
							lstrChecked = " checked ";
						else
							lstrChecked = "";
						lsbCtrls.Append(string.Format("<br><input type=radio value=" + mstrParaValor[3] + " name=grpBuscAvan{0} id=opt{0}4 onclick=javascript:mBuscSele(this.value,'hdnBusc{0}'); " + lstrChecked + "><span class=titulo>" + mstrPara[3] + "</span>", base.UniqueID));
					}
					lsbCtrls.Append("</td></tr>");
					lsbCtrls.Append(string.Format("<tr><td colspan=2 align=center valign=middle height=40><input type=button class=boton value=Aceptar onclick=javascript:mBuscMostrar('panBuscAvan{0}','hidden',0,0); id=btnBuscAcep{0}></td></tr>", base.UniqueID));
					lsbCtrls.Append("</table>");
					lsbCtrls.Append("</div>");
				}

				lsbCtrls.Append("</td><td width=100%>");

				pWriter.Write(lsbCtrls.ToString());
				//base.Width=new System.Web.UI.WebControls.Unit("100%");
				base.Render(pWriter);
				pWriter.Write("</td></tr></table>");
			}
			else
				base.Render(pWriter);

		}

		protected override void OnLoad(System.EventArgs e)
		{
			mCargarControlesSession(true);
			base.OnLoad(e);
		}

		protected override void OnInit(System.EventArgs e)
		{
			base.OnInit(e);
			mCargarControlesSession(false);
		}

		protected override void OnSelectedIndexChanged(System.EventArgs e)
		{
			mbooDisparaEvento=false;
			base.OnSelectedIndexChanged(e);
		}

		protected override object SaveViewState ()
		{
			mGuardarEstado();			
			return(base.SaveViewState());
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			mCargarEstado();
		}

		void System.Web.UI.IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (mbooDisparaEvento)
				base.OnSelectedIndexChanged(null);
		}

		private void mCargarControlesSession(bool pbooBorraSession)
		{
			try
			{
				if (Page.IsPostBack && (mstrNomOper!=""))
				{
					System.Data.DataSet ds;
					string sValor;

					ds = (System.Data.DataSet) Page.Session["-" + base.UniqueID + '_' + mstrNomOper];
					if(ds!=null)
					{
						base.DataValueField = "id";
						base.DataTextField = "descrip";
						base.DataSource = ds.Tables[0];
						base.DataBind();

						if(pbooBorraSession)
							Page.Session.Remove("-" + base.UniqueID + '_' + mstrNomOper);

						sValor = Page.Request.Form[base.UniqueID];

						if(sValor != "" && sValor != null)
							base.SelectedIndex = Items.IndexOf(Items.FindByValue(sValor));
					}
				}
			}
			catch
			{
				//this.Items[0].Selected = false;
			}
		}

		private void mGuardarEstado()
		{
			ViewState.Add("NomOper", NomOper);
		}

		private void mCargarEstado()
		{
			NomOper = ViewState["NomOper"].ToString();
		}

		public void CargarControlesSession()
		{
			mCargarControlesSession(true);
		}

		public void Limpiar()
		{
			mLimpiarInactivos();

			if(base.Page.Session[base.UniqueID + "_defa"]!=null)
				base.SelectedIndex = base.Items.IndexOf(base.Items.FindByValue(base.Page.Session[base.UniqueID + "_defa"].ToString()));
			else
				base.SelectedIndex=-1;
		}

		private string[] mObtenerValores(string pstrValores)
		{
			string[] lstrValores;
			if (mstrParaSepara == "")
				mstrParaSepara = ",";
			lstrValores = pstrValores.Split(Convert.ToChar(mstrParaSepara));
			return(lstrValores);
		}

		public void BuscarId (string pstrId) 
		{
			/* Espe metodo permite buscar el id clave y si lo encuentra
			   lo selecciona.
			*/
		
			int lintI;

			if(pstrId.Trim() == "" || pstrId == null) return;

			for (lintI=0; lintI<Items.Count; lintI++) 
			{
				if (Convert.ToString(Items[lintI].Value).Trim() == pstrId.Trim()) 
				{
					Items[lintI].Selected = true;
					return;
				}
			}
		}

		private System.Data.DataSet mExecuteQuery(string pstrProc)
		{
			System.Data.SqlClient.SqlDataAdapter cmdExecCommand = new System.Data.SqlClient.SqlDataAdapter(pstrProc, Page.Session["sConn"].ToString());
			System.Data.DataSet ds = new System.Data.DataSet();

			cmdExecCommand.Fill(ds);
			return (ds);
		}

		private void mCargarValorInactivo(string pValor)
		{
			try
			{
				if(NomOper=="")
					return;

				System.Data.DataSet ds = mExecuteQuery("exec " + NomOper + " @id_baja=" + pValor);

				if(ds.Tables[0].Rows.Count>0)
				{
					base.Items.Add( ds.Tables[0].Rows[0][1].ToString());
					//base.Items.Add("**INACTIVO**" + ds.Tables[0].Rows[0][1]);
					base.Items[base.Items.Count - 1].Value = ds.Tables[0].Rows[0][0].ToString();
					base.SelectedIndex = base.Items.IndexOf(base.Items.FindByValue(pValor));
				}
			}
			catch{
			}
		}

		private void mLimpiarInactivos()
		{
			for (int lintI=Items.Count-1; lintI>=0; lintI--) 
			{
				if (Items[lintI].Text.IndexOf("**INACTIVO**")==0)
					base.Items.Remove(Items[lintI]);
			}
		}

		private string mObtenerCodiXId(string pValor)
		{
			try
			{
				System.Data.DataSet ds = mExecuteQuery("exec " + NomOper + " @id=" + pValor);

				if(ds.Tables[0].Rows.Count>0)
					return(ds.Tables[0].Rows[0]["codi"].ToString().Trim());
				else
					return("");
			}
			catch
			{
			return("");
			}
		}
	}
}