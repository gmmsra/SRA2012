using System;

namespace NixorControls
{
	/// <summary>
	/// El control CodPostalBox hereda la funcionalidad del textbox y
	/// le agrega la validacion del codigo Postal.
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"

	///  
	/// </summary>
	public class CodPostalBox:TextBoxControls
	{
		//		private string mstrIncludesUrl = "Includes/";
		//		private string mstrEnterPorTab = "True";
		//		private string mstrCambiaValor = "False"; 
		//		private bool mbooAceptaNull = true;
		//		private bool mbooObligatorio = false;
		//		private string mstrCampoVal = "";
		private string mstrValidarPaisDefault = "";
	
		public string ValidarPaisDefault
		{
			get 
			{
				return mstrValidarPaisDefault; 
			}
			set 
			{
				mstrValidarPaisDefault = value; 
			}
		}

        /* 11/05/2021 GM
		public object Valor
		{
			get 
			{
				if(Text=="")
				{
					if(AceptaNull)
						return(DBNull.Value);
					else
						return("");
				}
				else
					return(Text.Trim());
			}
			set 
			{
				if(value == DBNull.Value)
				{
					Text="";
				}
				else
				{
					Text = value.ToString().Trim();
				}
			}
		}
        */

        public string Valor
        {
            get
            {
                if (Text == string.Empty)
                {
                    if (AceptaNull)
                        return (DBNull.Value.ToString());
                    else
                        return (string.Empty);
                }
                else
                    return (Text.Trim());
            }
            set
            {
                if (value == DBNull.Value.ToString())
                {
                    Text = string.Empty;
                }
                else
                {
                    Text = value.ToString().Trim();
                }
            }
        }

		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

			if(!base.Page.IsClientScriptBlockRegistered("NixorCodPostalBox"))
				Page.RegisterClientScriptBlock("NixorCodPostalBox", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCodPostal.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));

			if (bool.Parse(EnterPorTab) || bool.Parse(CambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			
			if (EnterPorTab.Trim() == "true") //(bool.Parse(EnterPorTab))
				base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

			if (CambiaValor.Trim() == "true") //(bool.Parse(CambiaValor))
				base.Attributes.Add("onchange", "javascript:mCambioValor();");
			
			base.Attributes.Add("onblur", "javascript:gValCodPostal(this);");
		
			if(Obligatorio)
				base.Attributes.Add("Obligatorio", "true");

			base.MaxLength=8;
			pWriter.Write("<input type=hidden name=hdnValCodPostalPais value=" + mstrValidarPaisDefault + ">");
			base.Render(pWriter);
		}

	}

}
