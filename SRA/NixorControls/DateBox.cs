using System;
using System.Text;
using System.Globalization;

namespace NixorControls
{
	/// <summary>
	/// El control DateBox hereda la funcionalidad del textbox y
	/// le agrega validaciones de fecha.
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - ImagesUrl		: especifica el directorio donde estan las imagenes, por default "Images/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// - NombreForm	: especifica el nombre del form en cual esta contenido el control, por default "frmABM"
	/// - MostrarBoton	: especifica si se muestra el boton para seleccion en el calendario, por default "True"
	/// </summary>
	public class DateBox:TextBoxControls
	{
//		private string mstrIncludesUrl = "Includes/";
		private string mstrImagesUrl = "Images/";
//		private string mstrEnterPorTab = "True";
//		private string mstrCambiaValor = "False";
		private string mstrNombreForm = "frmABM";
		private string mstrCultureName = "es-AR";
		private string mstrMostrarBoton = "True";
//		private bool mbooAceptaNull = true;
//		private bool mbooObligatorio = false;
//		private string mstrCampoVal = "";
        private string metodoPostValid = "";
        private string esTransferencia = "False";

        public string MetodoPostValid
        {
            get
            {
                return metodoPostValid;
            }
            set
            {
                metodoPostValid = value;
            }
        }

        public string EsTransferencia
        {
            get
            {
                return esTransferencia;
            }
            set
            {
                esTransferencia = value;
            }
        }

		public string ImagesUrl
		{
			get 
			{
				return( Utils.gPath(Page) + mstrImagesUrl); 
			}
			set 
			{
				mstrImagesUrl = value; 
			}
		}

		public string MostrarBoton
		{
			get 
			{
				return mstrMostrarBoton; 
			}
			set 
			{
				mstrMostrarBoton = value; 
			}
		}
		
		public string NombreForm
		{
			get 
			{
				return mstrNombreForm; 
			}
			set 
			{
				mstrNombreForm = value; 
			}
		}

        /* 11/05/2021 GM
		public object Fecha
		{
			get 
			{
				if(Text==String.Empty)
				{
					if(AceptaNull)
						return(DBNull.Value.ToString());
					else
						return("0");
				}
				else
				{
					try
					{
						System.IFormatProvider format = new System.Globalization.CultureInfo("ES-AR", true);
						DateTime datFech = System.DateTime.Parse(Text, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);

						return(datFech);
					}
					catch
					{
						Text=String.Empty;
						return(DBNull.Value.ToString());
					}
				}
			}
			set 
			{
				if(value == DBNull.Value.ToString())
				{
					Text=String.Empty;
				}
				else
				{
					CultureInfo culture = new CultureInfo(mstrCultureName);
					string lstrFecha = Convert.ToString(value, culture);
					Text = lstrFecha.Substring(0,10);
				}
			}
		}
        */

        public string Fecha
        {
            get
            {
                if (Text == string.Empty)
                {
                    if (AceptaNull)
                        return (DBNull.Value.ToString());
                    else
                        return ("0");
                }
                else
                {
                    try
                    {
                        System.IFormatProvider format = new System.Globalization.CultureInfo("ES-AR", true);
                        DateTime datFech = System.DateTime.Parse(Text, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);

                        return (datFech.ToString());
                    }
                    catch
                    {
                        Text = string.Empty;
                        return (DBNull.Value.ToString());
                    }
                }
            }
            set
            {
                if (value == DBNull.Value.ToString())
                {
                    Text = string.Empty;
                }
                else
                {
                    CultureInfo culture = new CultureInfo(mstrCultureName);
                    DateTime fechaing = Convert.ToDateTime(value);
                    int dia = fechaing.Day;
                    int mes = fechaing.Month;
                    int anio = fechaing.Year;
                    string tempfecha = ((dia.ToString().Length == 1) ? "0" + dia.ToString() : dia.ToString())
                                     + "/" + ((mes.ToString().Length == 1) ? "0" + mes.ToString() : mes.ToString())
                                     + "/" + anio.ToString()
                                     ;
                    string lstrFecha = Convert.ToString(tempfecha, culture); //culture);
                    
                    //string lstrFecha = Convert.ToString(value, culture); //culture);
                    Text = lstrFecha.Substring(0, 10);
                }
            }
        }
		
		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);
			if(!base.Page.IsClientScriptBlockRegistered("NixorDateBox"))
			{
				Page.RegisterClientScriptBlock("NixorDateBoxPick", String.Format("<SCRIPT language=\"javascript\" src=\"{0}datepicker.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
				Page.RegisterClientScriptBlock("NixorDateBox", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valdate.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}

			if (bool.Parse(EnterPorTab) || bool.Parse(CambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
		}

        protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
        {
            //if (Transferencias.Trim() == "true")
            //{
            //string parametro = string.Format("javascript:gValDateTransferencia(this,{0})", metodoPostValid.Length == 0 ? "False" : metodoPostValid);

            //base.Attributes.Add("onblur", "javascript:gValDateTransferencia(this)");
            
            //base.Attributes.Add("onblur", parametro);
            //}
            //else
            //{
            //    base.Attributes.Add("onblur", "javascript:gValDate(this);");
            //}

            if (EsTransferencia.ToUpper() == "TRUE")
            {
                if (MetodoPostValid.Length > 0)
                {
                    string parametro = string.Format("javascript:gValDateTransferencia(this,{0})", metodoPostValid.Length == 0 ? "False" : metodoPostValid);
                    base.Attributes.Add("onblur", parametro);
                }
                else
                {
                    base.Attributes.Add("onblur", "javascript:gValDate(this);");
                }
            }
            else
            {
                base.Attributes.Add("onblur", "javascript:gValDate(this);");
            }

            base.Attributes.Add("onkeypress", "javascript:gMascaraFecha(this);");

            if (EnterPorTab.Trim() == "true")   //(bool.Parse(EnterPorTab))
                base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

            if (CambiaValor.Trim() == "true") //(bool.Parse(CambiaValor))
                base.Attributes.Add("onchange", "javascript:mCambioValor();");

            if (Obligatorio)
                base.Attributes.Add("Obligatorio", "true");


            base.MaxLength = 10;
            base.Render(pWriter);

            StringBuilder lsbCtrls;
            lsbCtrls = new StringBuilder();
            lsbCtrls.Append("&nbsp;");

            if (mstrMostrarBoton == "True")
            {
                lsbCtrls.Append("<A href=\"javascript:");
                if (!Enabled)
                    lsbCtrls.Append("if(false)");
                else
                    lsbCtrls.Append("if(true)");
                lsbCtrls.Append("show_calendar('");
                //lsbCtrls.Append(((System.Web.UI.Control)(this)).Parent.ClientID);
                lsbCtrls.Append(mstrNombreForm);
                lsbCtrls.Append(".");
                lsbCtrls.Append(base.ClientID);
                lsbCtrls.Append("');\" tabindex=-1>");
                lsbCtrls.Append("<IMG onmouseover=\"window.status=''\"");
                lsbCtrls.Append(" onmouseout=\"window.status=''\"");
                lsbCtrls.Append(" height=\"15\"");
                if (Enabled)
                    lsbCtrls.Append(String.Format(" src=\"{0}icodate.gif\"", ImagesUrl));
                else
                    lsbCtrls.Append(String.Format(" src=\"{0}icodate_des.gif\"", ImagesUrl));
                lsbCtrls.Append(" width=\"15\" border=\"0\" tabindex=-1>");
                lsbCtrls.Append("</A>");
            }
            pWriter.Write(lsbCtrls.ToString());
        }
	}
}
