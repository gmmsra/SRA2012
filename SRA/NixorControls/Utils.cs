using System;
using System.Web.UI;
using System.Text;

namespace NixorControls
{
	/// <summary>
	/// Descripción breve de Utils.
	/// </summary>
	public class Utils
	{
		public static void gLimpiarCambios(Page pPage)
		{
			System.Web.UI.WebControls.TextBox lTextBox = (System.Web.UI.WebControls.TextBox) pPage.FindControl("hdnNixModi");
			lTextBox.Text="";
		}

		internal static void gAgregarControlLimpiar(Page pPage, System.Web.UI.HtmlTextWriter pWriter)
		{
			System.Web.UI.WebControls.TextBox lTextBox = new System.Web.UI.WebControls.TextBox();
			lTextBox.ID= "hdnNixModi";
			StringBuilder lsbCtrls = new StringBuilder();
			pWriter.Write("<div style='display:none'>");
			lTextBox.RenderControl(pWriter);
			pWriter.Write("</div>");
		}

		internal static string CrLf()
		{
			byte[] Bytes;
			Bytes = new byte[2];
			Bytes[0] = 13;
			Bytes[1] = 10;
			return(Encoding.ASCII.GetString(Bytes));
		}

		internal static string gPath(Page pPage)
		{
			if(pPage.TemplateSourceDirectory.ToLower().IndexOf("reportes")!=-1)
			{
				int lintSub = pPage.TemplateSourceDirectory.Length - pPage.TemplateSourceDirectory.Replace("/", "").Length - 1;
				if(lintSub==1)
					return("../");
				if(lintSub==2)
					return("../../");
			}
			return("");
		}

	}
}
