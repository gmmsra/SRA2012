using System;

namespace NixorControls
{
	/// <summary>
	/// El control HourBox hereda la funcionalidad del textbox y
	/// le agrega la validacion de las horas.
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// </summary>
	public class HourBox:TextBoxControls
	{
//		private string mstrIncludesUrl = "Includes/";
//		private string mstrEnterPorTab = "True";
//		private string mstrCambiaValor = "False";
		private string mstrChangeEvent = "";
		private string mstrBlurEvent = "";

		public string BlurEvent
		{
			get 
			{
				return mstrBlurEvent; 
			}
			set 
			{
				mstrBlurEvent = value; 
			}
		}

		public string ChangeEvent
		{
			get 
			{
				return mstrChangeEvent; 
			}
			set 
			{
				mstrChangeEvent = value; 
			}
		}

        /* 11/05/2021 GM
		public object Valor
		{
			get 
			{
				if(Text==String.Empty)
				{
					if(AceptaNull)
						return(DBNull.Value.ToString());
					else
						return("0");
				}
				else
				{
					try
					{
						System.IFormatProvider format = new System.Globalization.CultureInfo("fr-FR", true);
						DateTime datFech = System.DateTime.Parse(Text, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);

						return(datFech).ToString();
					}
					catch
					{
						Text=String.Empty;
						return(DBNull.Value.ToString());
					}
				}
			}
			set 
			{
				if(value == DBNull.Value.ToString())
				{
					Text=String.Empty;
				}
				else
				{
                    Text = Convert.ToDateTime(value).ToString("HH:mm");
                        //((DateTime) value).ToString("HH:mm");
				}
			}
		}
        */

        public string Valor
        {
            get
            {
                if (Text == string.Empty)
                {
                    if (AceptaNull)
                        return (DBNull.Value.ToString());
                    else
                        return ("0");
                }
                else
                {
                    try
                    {
                        System.IFormatProvider format = new System.Globalization.CultureInfo("fr-FR", true);
                        DateTime datFech = System.DateTime.Parse(Text, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);

                        return (datFech).ToString();
                    }
                    catch
                    {
                        Text = string.Empty;
                        return (DBNull.Value.ToString());
                    }
                }
            }
            set
            {
                if (value == DBNull.Value.ToString())
                {
                    Text = string.Empty;
                }
                else
                {
                    Text = Convert.ToDateTime(value).ToString("HH:mm"); // ((DateTime)value).ToString("HH:mm");
                }
            }
        }

		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

			if(!base.Page.IsClientScriptBlockRegistered("NixorHourBox"))
				Page.RegisterClientScriptBlock("NixorHourBox", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valhora.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));

			if (bool.Parse(EnterPorTab) || bool.Parse(CambiaValor))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{
			if (EnterPorTab.Trim()=="true") //(bool.Parse(EnterPorTab))
				base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

			if (CambiaValor.ToString()=="true")  //(bool.Parse(CambiaValor))
				base.Attributes.Add("onchange", "javascript:mCambioValor();"+mstrChangeEvent+";");
			else
				if (mstrChangeEvent.Trim() != "") 
				base.Attributes.Add("onchange", mstrChangeEvent);
			
			base.Attributes.Add("onblur", "javascript:gValHora(this);"+mstrBlurEvent+";");
			base.Attributes.Add("onkeypress", "javascript:gMascaraHora(this);");

			if(Obligatorio)
				base.Attributes.Add("Obligatorio", "true");

			base.MaxLength=5;
			base.Render(pWriter);
		}
	}
}