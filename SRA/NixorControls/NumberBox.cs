using System;

namespace NixorControls
{
	/// <summary>
	/// El control NumberBox hereda la funcionalidad del textbox y
	/// le agrega validaciones numericas.
	/// Las propiedades agregadas al textbox son:
	/// - IncludesUrl	: especifica el directorio donde estan los includes, por default "Includes/"
	/// - EnterPorTab	: especifica si el enter se comporta como un tab, por default "True"
	/// - CambiaValor	: especifica que se debe avisar cuando se van a perder los datos del control, por default "False"
	/// - EsDecimal		: especifica si el contenido puede ser un numero decimal, por default "False"
	/// - EsPorcen		: especifica si el contenido puede ser un numero decimal, por default "False"
	/// - CantMax		: especifica la cantidad maxima de posiciones de la parte entera, por default "8"
	/// - ChangeEvent	: especifica las funciones javascript a ejecutar en el evento change del control, por default "".
	/// - MaxValor		: especifica el Valor Maximo que puede tener el control, por defecto es 1000000
	//  - EsTarjeta     : especifica si el contenido puede ser el nro de una tarjeta (solo nros y el caracter '-'),por default "False" 
  	/// </summary>
	public class NumberBox:TextBoxControls
	{
//		private string mstrIncludesUrl = "Includes/";
//		private string mstrEnterPorTab = "True";
//		private string mstrCambiaValor = "False";
		private string mstrEsDecimal = "False";
		private string mstrEsPorcen = "False";
		private string mstrCantMax = "16";
		
		private string mstrChangeEvent = "";
		private string mstrMaxValor = "900000000";
		private string mstrEsTarjeta = "False";
//		private bool mbooAceptaNull = false;
//		private bool mbooObligatorio = false;
//		private string mstrCampoVal = "";

		public string ChangeEvent
		{
			get 
			{
				return mstrChangeEvent; 
			}
			set 
			{
				mstrChangeEvent = value; 
			}
		}

		public string EsTarjeta
		{
			get 
			{
				return mstrEsTarjeta; 
			}
			set 
			{
				mstrEsTarjeta = value; 
			}
		}

		public string EsDecimal
		{
			get 
			{
				return mstrEsDecimal; 
			}
			set 
			{
				mstrEsDecimal = value; 
			}
		}

		public string EsPorcen
		{
			get 
			{
				return mstrEsPorcen; 
			}
			set 
			{
				mstrEsPorcen = value; 
			}
		}

		public string CantMax
		{
			get 
			{
				return mstrCantMax; 
			}
			set 
			{
				mstrCantMax = value; 
			}
		}
		
		public string MaxValor
		{
			get 
			{
				return mstrMaxValor; 
			}
			set 
			{
				mstrMaxValor = value; 
			}
		}

        /* 11/05/2021 GM
		public object Valor
		{
			get 
			{
				if(Text=="")
				{
					if(AceptaNull)
						return(DBNull.Value);
					else
						return(0);
				}
				else
					return(Text);
			}
			set 
			{
				if(value == DBNull.Value)
				{
					Text="";
				}
				else
				{
					Text = value.ToString().Trim();
				}
			}
		}
        */

        public string Valor
        {
            get
            {
                if (Text == string.Empty)
                {
                    if (AceptaNull)
                        return (DBNull.Value.ToString());
                    else
                        return ("0");
                }
                else
                    return (Text);
            }
            set
            {
                if (value == DBNull.Value.ToString())
                {
                    Text = string.Empty;
                }
                else
                {
                    Text = value.ToString().Trim();
                }
            }
        }

		protected override void OnPreRender(System.EventArgs e)
		{
			base.OnPreRender(e);

		

			if(bool.Parse(mstrEsDecimal))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorDecimal"))
					Page.RegisterClientScriptBlock("NixorDecimal", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valDecimal.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
			}
			else
			{

				if(bool.Parse(mstrEsPorcen))
				{
					if(!base.Page.IsClientScriptBlockRegistered("NixorPorcen"))
						Page.RegisterClientScriptBlock("NixorPorcen", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valporcen.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
				}
				else
				{
				
					if(bool.Parse(mstrEsTarjeta))
					{
						if(!base.Page.IsClientScriptBlockRegistered("NixorTarjeta"))
							Page.RegisterClientScriptBlock("NixorTarjeta", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valNroTarjeta.js\" type=\"text/javascript\"></SCRIPT>", IncludesUrl));
					}
					else
					{
						if(!base.Page.IsClientScriptBlockRegistered("NixorNumber"))
							Page.RegisterClientScriptBlock("NixorNumber", String.Format("<SCRIPT language='javascript' src='{0}valNumber.js' type='text/javascript'></SCRIPT>", IncludesUrl));
					}

				}
				
			}




			if(bool.Parse(EnterPorTab))
			{
				if(!base.Page.IsClientScriptBlockRegistered("NixorCambios"))
					Page.RegisterClientScriptBlock("NixorCambios", String.Format("<SCRIPT language=\"javascript\" src=\"{0}valCambios.js\"></SCRIPT>", IncludesUrl));
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter pWriter)
		{

            if (mstrEsDecimal.ToString().ToUpper() == "TRUE") // (bool.Parse(mstrEsDecimal))
				base.Attributes.Add("onblur", "javascript:gValDecimal(this," + mstrCantMax + ","+mstrMaxValor + ","+ (base.AutoPostBack ? "1" : "0") +");");
			else
			{
                if (mstrEsPorcen.ToString().ToUpper() == "TRUE") // (bool.Parse(mstrEsPorcen))
					base.Attributes.Add("onblur", "javascript:gValPorcen(this,"+mstrMaxValor+","+ (base.AutoPostBack ? "1" : "0") +");");
				else
				{
                    if (mstrEsTarjeta.ToString().ToUpper() == "TRUE") // (bool.Parse(mstrEsTarjeta))
					{
						
						base.Attributes.Add("onblur", "javascript:gValNroTarjeta(this,"+mstrMaxValor+","+ (base.AutoPostBack ? "1" : "0") +");");
					}
					else
						base.Attributes.Add("onblur", "javascript:gValNumber(this,"+mstrMaxValor+","+ (base.AutoPostBack ? "1" : "0") +");");
				}
					
			}

			//base.Attributes.Add("onblur", "javascript:gValMaxValor(this, " + mstrMaxValor + ");");

            if (EnterPorTab.ToString().ToUpper() == "TRUE") // (bool.Parse(EnterPorTab))
				base.Attributes.Add("onkeydown", "javascript:mEnterPorTab();");

            if (CambiaValor.ToString().ToUpper() == "TRUE") // (bool.Parse(CambiaValor))
				base.Attributes.Add("onchange", "javascript:mCambioValor();"+mstrChangeEvent);
			else
				if (mstrChangeEvent.Trim() != "") 
					base.Attributes.Add("onchange", mstrChangeEvent);
				
			if(Obligatorio)
				base.Attributes.Add("Obligatorio", "true");

			base.AutoPostBack = false; 

			base.Render(pWriter);
		}
	}
}
