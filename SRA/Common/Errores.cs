﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Errores
    {
        public static string ReturnExceptionMessage(string NameSpace, string MethodName, string ExceptionMessage)
        {
            return DateTime.Now.ToShortTimeString() + " - " + NameSpace + "." + MethodName + ", registró el siguiente error: " + ExceptionMessage;
        }
    }
}
