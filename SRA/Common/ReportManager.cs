using System;

namespace Common
{
	/// <summary>
	/// Summary description for ReportManager.
	/// </summary>
	public class ReportManager
	{
			public static string gReportServer()
			{
				string lstrRepo = System.Configuration.ConfigurationSettings.AppSettings["conRepo"].ToString();
				return(lstrRepo);
			}
			private static string gReportFolder()
			{
				string lstrRepo = System.Configuration.ConfigurationSettings.AppSettings["conRepoDire"].ToString();
				return(lstrRepo);
			}
			public static string gIniciarReporte(System.Web.HttpRequest oRequest, string pstrRptName)
			{
				string lstrRpt = "";

				lstrRpt = "http://" + gReportServer() ;
				lstrRpt += "/rptproxy.aspx?%2f" + gReportFolder() + "%2f" + pstrRptName;
				lstrRpt += "&rs%3aClearSession=true&rs%3aFormat=HTML4.0&rs%3aCommand=Render";
			
				return(lstrRpt);
			}
			public static string gSetearOpcionesReporte(string pstrRpt)
			{

				return(gSetearOpcionesReporte(pstrRpt,true));
			}

			public static string gSetearOpcionesReporte(string pstrRpt, bool pbooTolbar)
			{

				string lstrRpt = pstrRpt;
				lstrRpt += "&rc%3aParameters=false&rc%3aLinkTarget=_top";
				lstrRpt += "&rc%3aToolbar=" + (pbooTolbar?"True" : "False") + "&rc%3aReplacementRoot=http%3a%2f%2f";
				lstrRpt += gReportServer() + "%2fReports%2fPages%2fReport.aspx%3fServerUrl%3d";
				return(lstrRpt);
			}
	}
}
