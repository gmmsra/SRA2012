using System;
using System.Net;
using System.Web.Mail;
using AccesoBD;
using System.Configuration;
using System.IO;
using System.Text;
using System.Data;
using DAO;

namespace Common
{
    /// <summary>
    /// �tiles de la aplicaci�n
    /// </summary>
    public class MailManager
    {
        private string _SmtpServidor ;
        private string _SmtpUsuario ;
        private string _SmtpPassword;
		private string _SmtpAuthenticate;
		public int smtpPuerto; 
		public bool EnableSsl; 
        public  string VarEmail;
		public string VarEmailCC;
		public  string VarEmailCCO;
		public bool IsHTMlBody; 
		private string _CuentaOrigen = string.Empty;
		public string varDisplayName;
		
		public MailManager() 
        {
			// cargo las variable con los valores del web config
            this._SmtpServidor = ConfigurationSettings.AppSettings["conMailServ"];
            this._SmtpUsuario  = ConfigurationSettings.AppSettings["sendusername"]; 
            this._SmtpPassword = ConfigurationSettings.AppSettings["sendpassword"];
			this._SmtpAuthenticate = ConfigurationSettings.AppSettings["smtpauthenticate"];
            this._CuentaOrigen = ConfigurationSettings.AppSettings["conMailFrom"];
			this.smtpPuerto = Convert.ToInt16(ConfigurationSettings.AppSettings["smtpPuerto"]);
			this.EnableSsl = Convert.ToBoolean(ConfigurationSettings.AppSettings["EnableSsl"]);
        }

        /// <summary>
        /// Env�o de un correo electr�nico
        /// </summary>
        /// <param name="subject">Asunto del mensaje</param>
        /// <param name="body">cuerpo del mensaje</param>
        public void EnviaMensaje(string subject, string body)
        {
            try
            {
                string smtpServidor = this._SmtpServidor; //"192.168.123.7";
                string smtpUsuario = this._SmtpUsuario; //"procesos";
                string smtpPassword = this._SmtpPassword; //"procesos";
                string cuentaDestino = this.VarEmail;
                string cuentaOrigen = this._CuentaOrigen; //"procesos@sra.org.ar";
				// creo un objeto mailmessage
				MailMessage msg = new MailMessage();
				// set del destinatario/s
				msg.To = this.VarEmail;
				// set del destinatario/s en CCO
				if(this.VarEmailCCO != null && this.VarEmailCCO.Trim().Length > 0)
					msg.Bcc = this.VarEmailCCO;
				// set email de origen
				msg.From = this._CuentaOrigen;
				// set asunto
				msg.Subject = subject;
				// set cuerpo mail
				msg.Body = body;
				// set priodidad
				msg.Priority = MailPriority.High;
				// verifico que tipo de formato tiene el cuerpo del mail
				// y lo seteo deacuerdo
				  
				if(IsHTMlBody)
				{
					msg.BodyEncoding = Encoding.UTF8;
					msg.BodyFormat = MailFormat.Html; //MailFormat.Text ;  o MailFormat.Html;
				}
				else
				{
					msg.BodyEncoding = Encoding.UTF8;
					msg.BodyFormat = MailFormat.Text ;
				}

				// Activar Autenticaci�n
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", this._SmtpAuthenticate);
				// Nombre de usuario
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", this._SmtpUsuario);
				// Contrase�a
				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", this._SmtpPassword);

				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", this.smtpPuerto );

				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");

				msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "true");

				// set servidor de correo
				SmtpMail.SmtpServer = this._SmtpServidor;// El servidor de correo
				// envio de mail
				SmtpMail.Send(msg); 

				string Msg = "MailManager.EnviaMensaje:  Asunto" + subject 
						   + " Body: " + body ; 
				clsError.gManejarError(new Exception(Msg));

            }
            catch (Exception ex)
			{
				string Msg = "MailManager.EnviaMensaje:  registro el siguiente error: " + ex.Message; 
				clsError.gManejarError(new Exception(ex.Message + " - " + Msg , ex));
				throw new Exception(ex.Message.ToString());
            }
        }

		/*
		public void EnviaMensaje1(string subject, string body) //, List<Attachment> attachments)
        {
			try
			{
				string smtpServidor = this._SmtpServidor; //"192.168.123.7";
				string smtpUsuario = this._SmtpUsuario; //"procesos";
				string smtpPassword = this._SmtpPassword; //"procesos";
				string cuentaDestino = this.VarEmail;
				string cuentaDestinoCC = this.VarEmailCC;
				string cuentaDestinoCCO = this.VarEmailCCO;
                
				string cuentaOrigen = this._CuentaOrigen; //"procesos@sra.org.ar";

				string _displayname = (varDisplayName.Trim().Length == 0) ? "Sociedad Rural Argentina. Env�o" : varDisplayName;

                
				// Permite a las aplicaciones enviar mensajes de correo electr�nico mediante el protocolo SMTP 
				SmtpClient cliente = new SmtpClient(smtpServidor, smtpPuerto);
				//cliente.Port = smtpPuerto;
				// Credenciales de autenticaci�n
				NetworkCredential credenciales = new NetworkCredential(smtpUsuario, smtpPassword);
				// Credenciales de autenticaci�n
				cliente.Credentials = credenciales;
				// Se descartan las credenciales por defecto
				//cliente.UseDefaultCredentials = false;

				// Los mensajes de correo electr�nico se env�an a un servidor SMTP a trav�s de la red
				//cliente.DeliveryMethod = SmtpDeliveryMethod.Network;

				cliente.EnableSsl = EnableSsl;
				// Mensaje
				MailMessage mensaje = new MailMessage();

				// Direcci�n de origen
				MailAddress direccionOrigen = new MailAddress(cuentaOrigen, _displayname);
				mensaje.From = direccionOrigen;

				// Direcci�n de destino
				if (cuentaDestino != null)
				{
					string[] cuentas = cuentaDestino.Split(';');
					foreach (string cuenta in cuentas)
					{
						MailAddress direccionDestino = new MailAddress(cuenta);
						mensaje.To.Add(direccionDestino);
						direccionDestino = null;
					}
				}

				// Direcci�n de destino
				if (cuentaDestinoCC != null)
				{
					string[] cuentas = cuentaDestinoCC.Split(';');
					foreach (string cuenta in cuentas)
					{
						MailAddress direccionDestino = new MailAddress(cuenta);
						mensaje.CC.Add(direccionDestino);
						direccionDestino = null;
					}
				}

				// Direcci�n de destinoCCO
				if (cuentaDestinoCCO != null)
				{
					string[] cuentasCCO = cuentaDestinoCCO.Split(';');
					foreach (string cuentaCCO in cuentasCCO)
					{
						MailAddress direccionDestinoCCO = new MailAddress(cuentaCCO);
						mensaje.Bcc.Add(direccionDestinoCCO);
						direccionDestinoCCO = null;
					}
				}

				mensaje.Subject = subject + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

//				if (attachments != null)
//				{
//					foreach (Attachment itemAttach in attachments)
//					{
//						mensaje.Attachments.Add(itemAttach);
//					}
//				}

				mensaje.BodyEncoding = Encoding.UTF8;
				mensaje.Body = body;
				mensaje.IsBodyHtml = isBodyHtml;
                
				// Env�o del mensaje
				cliente.Send(mensaje);

				// Limpieza
				mensaje = null;
				direccionOrigen = null;
                
				credenciales = null;
				cliente = null;
			}
			catch (Exception ex)
			{
				string Msg = "MailManager.EnviaMensaje: registro el siguiente error: " + ex.Message;
				throw new Exception(Msg);
			}
        }
		*/
		/// <summary>
		/// metodo que recupera los mail de destino para la alerta informada
		/// </summary>
		/// <param name="IdAlerta"></param>
		/// <returns>string</returns>
		public string GetVarMailDestino(int IdAlerta)
		{
			// declaro la variable de retorno
			string respuesta = string.Empty;

			// ejecuto la consulta a la base de datos para obtener los mails
			DataSet ds = MailManagerDao.GetMailsAlertaEnviar(IdAlerta);
			// verifico que tenga registros
			if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
			{	
				// recorro los registros y concateno los mails de los destinatarios
				foreach (DataRow dr in ds.Tables[0].Rows)
				{
					respuesta += dr["usua_email"].ToString() + ";"; 
				}
			}
			return respuesta;
		}
    }
}


