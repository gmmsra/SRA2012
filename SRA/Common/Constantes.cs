using System;

namespace Common
{
	/// <summary>
	/// Summary description for Constantes. 
	/// </summary>
	public class Constantes
	{
		public static int PROFORMAS_FACTURABLES_PENDIENTES_POR_SALDO_DEUDOR = 1;
		public static int CHEQUES_POSTDATADOS = 2;
		public static int AJUSTES_DE_CUENTA_CORRIENTE_PENDIENTES_DE_APROBACION = 3;
		public static int LISTA_DE_CLIENTES_INCOBRABLES = 4;
		public static int PROFORMAS_EXPOSICIONES_PENDIENTES_DE_FACTURACION = 5;
		public static int NUMEROS_DE_CAI_PROXIMOS_A_VENCER = 6;
		public static int ACREDITACION_BANCARIA_CONFIRMACION = 7;
		public static int ACREDITACION_BANCARIA_INGRESO = 8;
		public static int ACREDITACION_BANCARIA_ANULACION = 9;
		public static int RECHAZO_DE_CHEQUES = 10;
		public static int RECHAZO_DE_CUPONES = 11;		


		/* contantes tipo de envio email sender*/
		public const string EnvioDocumentacion = "ED";		
		public const string EnvioAvisoVtosFactura = "EAV";		
		public const string EnvioEstadoDeCuenta = "EEC";		
	}
	public class ComprobantesTipos
	{
		public const int Cuota_Social = 1;
		public const int Interes_CS_Dev = 2;
		public const int Ajuste = 7;
		public const int Interes_Cuota_Social = 8 ;
		public const int SolicitudIngreso = 10 ;
		public const int CambioCategoria = 11;
		public const int Transferencias = 13;
		public const int Proforma = 28;
		public const int Factura = 29;
		public const int Recibo = 30;
		public const int NC = 31;
		public const int ND = 32;
		public const int Recibo_Acuse = 33;
		public const int AplicacionCreditos = 34 ;
		public const int Transf_CC_a_CS = 36 ;
		public const int Emer_Agrop = 37;
		public const int Deudores_Incobrables = 38;
		public const int Interes_CtaCte = 41 ;
		public const int Recibo_Acreditacion = 42  ;
		public const int SobreTasasAlVto_CtaCte = 43;
		public const int Depositos = 50;
		public const int CancelacionFC = 100;
		public const int PlanFacilidades = 101;
	}

	public enum ComprobTipos
	{
		 Cuota_Social = 1
		,Interes_CS_Dev = 2
		,Ajuste = 7
		,Interes_Cuota_Social = 8
		,SolicitudIngreso = 10
		,CambioCategoria = 11
		,Transferencias = 13
		,Proforma = 28
		,Factura = 29
		,Recibo = 30
		,NC = 31
		,ND = 32
		,Recibo_Acuse = 33
		,AplicacionCreditos = 34
		,Transf_CC_a_CS = 36
		,Emer_Agrop = 37
		,Deudores_Incobrables = 38
		,Interes_CtaCte = 41
		,Recibo_Acreditacion = 42
		,SobreTasasAlVto_CtaCte = 43 // Dario 2013-07-22 
		,Depositos = 50
		,CancelacionFC = 100
		,PlanFacilidades = 101
	}

	public enum PagosTipos
	{
		 Efectivo = 1
		,Cheque = 2
		,Tarjeta = 3
		,DineroElec = 4
		,AcredBancaria = 5
		,AplicCredi = 6
		,Retenciones = 7
	}

	public enum ComprobPagosTipos
	{
		 Contado = 1
		,CtaCte = 2
		,Tarjeta = 3
		,PagoDiferido = 4
	}

	public enum Modulos
	{
		 Devengamientodecuotas = 1
		,SolicituddeIngreso = 2
		,CambiodeCategoría = 3
		,EmergenciaAgropecuaria = 4
		,DeudoresIncobrables = 5
		,DebitoAutomatico = 6
		,DebitoAutomatico_Devolucion = 7
		,Facturacion = 8
		,Cobranza = 9
		,ChequesPostdatados = 10
		,PlanPagos = 11
		,AplicacionCreditos = 12
		,AplicacionCreditosSocios = 13
		,Alumnos_Inscripción = 14
		,Alumnos_FacturacionCuotas = 15
		,FacturacionExterna = 16
		,ChequesRechazo = 17
		,ChequesVenta = 18
		,CuponesRechazo = 19
		,AcreditacionesConfirmación = 20
		,AjusteCtaCte = 21
		,Alumnos_FacturacionManual = 22
		,Alumnos_Débito_Automático = 23
		,Facturación_Proformas = 24
		,Alumnos_Examen = 25
		,SobreTasa_PagoFuera_termino = 26
	}

	public  enum Actividades
	{
		 Varios = 1 // administracion por ejemplo, pero en relidad son todos los demas
		,RRGG = 3
		,Laboratorio = 4
		,AlumnosISEA = 6
		,Expo = 7
		,EstPreLimAntecGen = 8
		,AlumnosCEIDA = 10
	}

	public enum CodigosServiciosTiposRRGG
	{
	    DenunciadeNacimiento	=	1
	   ,DenunciadeServicio	=	2
	   ,Implantes	=	3
	   ,DeclaraciondeDonantes	=	4
	   ,DeclaraciondeMochos	=	5
	   ,DenunciadeDadores	=	6
	   ,NacimientoPorTE	=	7
	   ,ProcesoPendientes	=	8
	   ,ProcesoCondicionales	=	9
	   ,CargaPlanillaPlantelBase	=	10
	   ,TransferenciaAnimalesPie	=	101
	   ,ImportacionAnimalesPie	=	102
	   ,Embargos	=	103
	   ,Prestamos	=	104
	   ,PrestamosPorLote	=	105
	   ,Copropiedad	=	106
	   ,TransferenciaSemen	=	201
	   ,ImportacionSemen	=	202
	   ,RecuperacionSemen	=	204
	   ,Condicionales	=	205
	   ,TransferenciaEmbriones	=	301
	   ,ImportacionEmbriones	=	302
	   ,RecuperacionEmbriones	=	304
	   ,UsoEmbriones	=	305
	   ,CertificadoPedrigreePlaneado	=	306
	   ,RegistrodePorcinos	=	401
	   ,EspeciesMenores	=	504
	   ,Varios	=	999

	}

    public static class TablaErroresSegunProceso
    {
        public const string RG_PRODUCTOS_INSCRIP = "RG_PRODUCTOS_INSCRIP";
        public const string RG_SERVI_DENUNCIAS = "RG_SERVI_DENUNCIAS";
        public const string TRAMITES_PRODUCTOS = "TRAMITES_PRODUCTOS";
        public const string IMPORTACION_PRODUCTOS = "IMPORTACION_PRODUCTOS";
        public const string TE_DENUN_DETA = "TE_DENUN_DETA";
        public const string TRAMITES = "TRAMITES";
        public const string SEMEN_STOCK = "SEMEN_STOCK";
        public const string PRESTAMOS = "PRESTAMOS";
    }
}
