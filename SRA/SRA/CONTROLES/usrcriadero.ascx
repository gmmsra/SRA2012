<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrCriadero" CodeFile="usrCriadero.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0"> 
	<TR>
		<TD noWrap align="left" style="HEIGHT: 11px" vAlign="top">
			<cc1:numberbox id="txtCodi" runat="server" cssclass="cuadrotexto" Width="78px"></cc1:numberbox>&nbsp;<asp:label id="lblCriaNomb" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:label>
			<cc1:textboxtab id="txtCriaNomb" runat="server" cssclass="cuadrotexto" Width="60%"></cc1:textboxtab></TD>
		<TD style="WIDTH: 2px; HEIGHT: 11px" noWrap align="right" width="2"></TD>
		<TD noWrap align="left" width="80" style="HEIGHT: 11px" vAlign="top">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="94%" border="0">
				<TR>
					<TD><IMG language="javascript" id="imgBusc" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
							src="..\imagenes\Buscar16.gif" runat="server"></TD>
					<TD><IMG language="javascript" id="imgBuscAvan" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
							alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif" runat="server"></TD>
					<TD><IMG language="javascript" id="imgLimp" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
							alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp" runat="server"></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD colSpan="4">
			<div class="paneledicion" style='Z-INDEX: 101; VISIBILITY: hidden; WIDTH: 360px; POSITION: absolute; HEIGHT: 160px'
				id=panBuscAvan<%=mstrCtrlId%>>
				<table border="0" cellpadding="0" cellspacing="0" align="center" valign="middle" width="100%">
					<tr>
						<td colspan="2" align="center" valign="middle" height="34"><span class="titulo">B�squeda 
								Avanzada</span><input type="hidden" size="1" name=hdnBusc<%=mstrCtrlId%> value="D">
						</td>
					</tr>
					<tr>
						<td width="20"></td>
						<td>
							<SPAN id="spRaza" style="CLEAR: none; DISPLAY: inline" runat="server">
								<asp:label id="lblRaza" runat="server" cssclass="titulo">&nbsp;Raza:</asp:label>&nbsp;<cc1:combobox class="combo" id="cmbRaza" tag="N" runat="server" AceptaNull="True" Width="160px" Obligatorio="False" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
							</SPAN>
							<span runat="server" visible="true" id="br1"><br></span>
							<span id="spLoca" style="CLEAR: none; DISPLAY: inline" runat="server">
								<asp:label id="lblPais" runat="server" cssclass="titulo">&nbsp;Pais:</asp:label>&nbsp;
                                <cc1:combobox class="combo" id="cmbPais" tag="N" runat="server" Width="180px" onchange="mCargarProvinciasFil(this, 'cmbProv')"></cc1:combobox>&nbsp;
								<br><asp:label id="lblProv" runat="server" cssclass="titulo">&nbsp;Provincia:</asp:label>&nbsp;
								<cc1:combobox class="combo" id="cmbProv" tag="N" runat="server" Width="180px" onchange="mCargarLocalidadesFil(this, 'cmbLoca')"></cc1:combobox>&nbsp;
								<br><asp:label id="lblLoca" runat="server" cssclass="titulo">&nbsp;Localidad:</asp:label>&nbsp;
								<cc1:combobox class="combo" id="cmbLoca" tag="N" runat="server" Width="180px" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
							</span>
							<span runat="server" visible="false" id="br2"><br></span>
							<br>
							&nbsp;<span> </span>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" valign="middle" height="40">
							<input type=button class=boton value=Aceptar onclick=javascript:mAceptarCria('<%=mstrCtrlId%>') id=btnBuscAcep<%=mstrCtrlId%>>&nbsp;&nbsp;
							<input type=button class=boton value=Cancelar onclick=javascript:mBuscMostrarCriaUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>'); id=btnBuscCanc<%=mstrCtrlId%>>
						</td>
					</tr>
				</table>
			</div>
			<DIV style="DISPLAY: none"><asp:label id="lblId" runat="server">Cliente</asp:label><cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab><asp:label id="lblIdAnt" runat="server"></asp:label></DIV>
		</TD>
	</TR>
	<TR>
		<TD colSpan="4">
		</TD>
	</TR>
	<TR>
		<TD align="right" colSpan="4"><asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Rows="2" TextMode="MultiLine"
				Width="100%" Enabled="False"></asp:textbox></TD>
	</TR>
</TABLE>
<script language="javascript">
function mValidaConsulta()
{
    return(true)	
}
function mAceptarCria(pCtrol)
{
    if (mValidaConsulta())
		mBuscMostrarCriaUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1',pCtrol);
}
function mCargarProvinciasFil(pPais,pcmbPcia)
{
	var sFiltro = pPais.value;
	LoadComboXML("provincias_cargar", sFiltro, '<%=mstrCtrlId%>:'+pcmbPcia, "S");
}
function mCargarLocalidadesFil(pPcia, pcmbLoca)
{
	var sFiltro = pPcia.value 
	if (sFiltro != '')		    
		LoadComboXML("localidades_cargar", sFiltro, '<%=mstrCtrlId%>:'+pcmbLoca, "S");
}	
</script>
