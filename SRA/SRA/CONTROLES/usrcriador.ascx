<%--<%@ Reference Page="~/Copropiedad.aspx" %>--%>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrCriador" CodeFile="usrCriador.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
	<TR>
		<TD align="left">
			<table border="0" cellSpacing="0" cellPadding="0">
				<tr>
					<td align="left"><cc1:numberbox id="txtCodi" name="txtCodi" autocomplete="off" MaxLength="9" MaxValor="999999999"
							runat="server" cssclass="cuadrotexto" Width="50px"></cc1:numberbox></td>
				</tr>
			</table>
		</TD>
		<TD align="right">&nbsp;<asp:label id="lblApel" runat="server" cssclass="titulo">Apel/R.Soc.:</asp:label>&nbsp;</TD>
		<TD style="WIDTH: 100%" align="left"><cc1:textboxtab id="txtApel" runat="server" cssclass="textolibre" width="98%" Rows="1" TextMode="MultiLine"></cc1:textboxtab></TD>
		<TD width="80" align="left">
			<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							id="imgBusc" language="javascript" src="..\imagenes\Buscar16.gif" runat="server">&nbsp;</TD>
					<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							id="imgBuscAvan" language="javascript" alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif"
							runat="server">&nbsp;</TD>
					<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							id="imgLimp" language="javascript" alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp"
							runat="server">&nbsp;</TD>
				</TR>
			</TABLE>
		</TD>
		<td id="tdBtnCopropiedad" width="230" align="right" runat="server"><asp:button id="btnCopropiedad" runat="server" cssclass="boton" Width="90px" CausesValidation="False"
				Text="Copropiedad" Enabled="False"></asp:button></td>
	</TR>
	<TR>
		<TD colSpan="4">
			<div 
      style="Z-INDEX: 101; POSITION: absolute; WIDTH: 380px; VISIBILITY: hidden" 
      id="panBuscAvan<%=mstrCtrlId%>" class=paneledicion>
				<table border="0" width="96%" align="center" valign="middle">
					<TR>
						<TD style="HEIGHT: 34px" height="34" vAlign="middle" colSpan="2" align="center"><span class="titulo">B�squeda 
								Avanzada</span><input value=D size=1 type=hidden name="hdnBusc<%=mstrCtrlId%>">
						</TD>
					</TR>
					<TR>
						<TD width="20"></TD>
						<TD><SPAN id="spNomb" runat="server"><asp:label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;<cc1:textboxtab id="txtNomb" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab>
							</SPAN><span id="br2" runat="server" visible="false">
								<br>
							</span><SPAN id="spClieNume" runat="server">
								<asp:label id="lblClieNume" runat="server" cssclass="titulo">Nro.Cliente:</asp:label>&nbsp;<cc1:numberbox id="txtClieNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN><span id="br3" runat="server" visible="false">
								<br>
							</span><SPAN id="spSociNume" runat="server">
								<asp:label id="lblSociNume" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;<cc1:numberbox id="txtSociNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN><span id="br4" runat="server" visible="false">
								<br>
							</span><SPAN style="DISPLAY: inline; CLEAR: none" id="spCriaNume" runat="server">
								<table border="0" cellSpacing="0" cellPadding="0" width="100%">
									<tr>
										<td width="5%" align="right"><asp:label id="lblCriaNumeRaza" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;</td>
										<td><cc1:combobox id="cmbCriaNumeRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
												tag="N" AceptaNull="False" Height="20px" NomOper="razas_cargar" MostrarBotones="False" filtra="true"
												onchange="Combo_change(this)"></cc1:combobox></td>
									</tr>
								</table>
								<table border="0" cellSpacing="0" cellPadding="0" width="100%">
									<tr>
										<td width="15%" align="right"><asp:label id="lblCriaNume" runat="server" cssclass="titulo">Nro.Criador:</asp:label>&nbsp;</td>
										<td><cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></td>
									</tr>
								</table>
							</SPAN><span id="br5" runat="server" visible="false">
								<br>
							</span><span style="DISPLAY: inline; CLEAR: none" id="spLegaNume" runat="server">
								<asp:label id="lblLegaNume" runat="server" cssclass="titulo">Nro.Legajo:</asp:label>&nbsp;<cc1:combobox id="cmbInse" class="combo" runat="server" Width="65px" tag="N" onchange="Combo_change(this)"></cc1:combobox>
								&nbsp;<cc1:numberbox id="txtLegaNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox><br>
							</span><span id="br6" runat="server" visible="false">
								<br>
							</span><SPAN style="DISPLAY: inline" id="spNoCriaNume" runat="server">
								<asp:label id="lblNoCriaNume" runat="server" cssclass="titulo">Nro.No Criador:</asp:label>&nbsp;<cc1:numberbox id="txtNoCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN><span id="br7" runat="server" visible="false">
								<br>
							</span><span id="spCUIT" runat="server" visible="false">
								<asp:label id="lblCUIT" runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;<cc1:cuitbox id="txtCUIT" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:cuitbox>&nbsp;
							</span><span id="br8" runat="server" visible="false">
								<br>
							</span><SPAN style="DISPLAY: inline" id="spDocuNume" runat="server">
								<br>
								<asp:label id="lblDocuNumeTido" runat="server" cssclass="titulo">Tipo Doc.:</asp:label>&nbsp;<cc1:combobox id="cmbDocuNumeTido" class="combo" runat="server" Width="65px" tag="N" onchange="Combo_change(this)"></cc1:combobox>&nbsp;<asp:label id="lblDocuNume" runat="server" cssclass="titulo">Nro.Doc:</asp:label>&nbsp;<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox>&nbsp;
							</SPAN><span id="br1" runat="server" visible="false">
								<br>
							</span><SPAN style="DISPLAY: inline" id="spTarjNume" runat="server">
								<br>
								<asp:label id="lblTarjNume" runat="server" cssclass="titulo">Tarjeta:</asp:label>&nbsp;<cc1:combobox id="cmbTarjTipo" class="combo" runat="server" Width="110px" tag="N" onchange="Combo_change(this)"></cc1:combobox>&nbsp;<asp:label id="Label2" runat="server" cssclass="titulo">Nro.:</asp:label>&nbsp;<cc1:numberbox id="txtTarjNume" runat="server" cssclass="cuadrotexto" Width="90px" EsTarjeta="True"></cc1:numberbox>&nbsp;
							</SPAN><span id="br9" runat="server" visible="false">
								<br>
							</span><SPAN style="DISPLAY: inline" id="spEntidad" runat="server">
								<asp:label id="lblEntidad" runat="server" cssclass="titulo">Entidad:</asp:label>&nbsp;<cc1:combobox id="cmbEntidad" class="combo" runat="server" Width="104px" tag="N"></cc1:combobox>&nbsp;
							</SPAN><span id="br10" runat="server" visible="false">
								<br>
							</span><SPAN id="spMedioPago" runat="server">
								<asp:label id="lblMedioPago" runat="server" cssclass="titulo">Medio de Pago:</asp:label>&nbsp;<cc1:combobox id="cmbMedioPago" class="combo" runat="server" Width="120px" tag="N" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
							</SPAN><span id="br11" runat="server" visible="false">
								<br>
							</span><span id="br12" runat="server" visible="false">
								<br>
							</span><SPAN style="DISPLAY: inline" id="spEmpresa" runat="server">
								<asp:label id="lblEmpresa" runat="server" cssclass="titulo">Empresa:</asp:label>&nbsp;<cc1:combobox id="cmbEmpresa" class="combo" runat="server" Width="120px" tag="N" onchange="Combo_change(this)"></cc1:combobox>
							</SPAN><span id="br13" runat="server" visible="false">
								<br>
							</span><SPAN style="DISPLAY: inline" id="spClaveUnica" runat="server">
								<asp:label id="lblClaveUnica" runat="server" cssclass="titulo">Clave:</asp:label>&nbsp;<cc1:textboxtab id="txtClave" onkeypress="gMascaraCunica(this)" runat="server" cssclass="cuadrotexto"
									Width="100px" maxlength="13"></cc1:textboxtab>&nbsp; </SPAN><SPAN style="DISPLAY: inline; CLEAR: none" id="spExpo" runat="server">
								<table border="0" cellSpacing="0" cellPadding="0" width="100%">
									<tr>
										<td width="5%" align="center"><asp:label id="lblExpo" runat="server" cssclass="titulo">Nro.Expositor:</asp:label>&nbsp;</td>
										<td><cc1:numberbox id="txtExpo" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></td>
									</tr>
								</table>
							</SPAN><SPAN style="DISPLAY: inline" id="spAgrupa" runat="server">
								<br>
								<asp:checkbox id="chkAgrupa" cssclass="titulo" Text="Incluir Agrupaciones" Checked="True" Runat="server"></asp:checkbox>&nbsp;
							</SPAN>
						</TD>
					</TR>
					<TR>
						<td style="DISPLAY: none" id="tdRazaCria" width="15%" align="center" runat="server">
							<cc1:combobox id="cmbRazaCria" class="combo" Width="99%" cssclass="cuadrotexto" runat="server"
								name="cmbRazaCria" onchange="cmbRaza_change(this)" filtra="true" MostrarBotones="False" NomOper="razas_cargar"
								Height="10px" AceptaNull="False" Visible="False"></cc1:combobox></td>
					</TR>
					<TR>
						<td height="40" vAlign="middle" colSpan="2" align="center"><input id="btnBuscAcep<%=mstrCtrlId%>" class=boton onclick="javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1','<%=mstrCtrlId%>');"value=Aceptar type=button>&nbsp;&nbsp;
							<input id="btnBuscCanc<%=mstrCtrlId%>" class=boton onclick="javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>');"value=Cancelar type=button>
						</td>
					</TR>
				</table>
			</div>
			<DIV style="DISPLAY: none"><asp:label id="lblId" runat="server">Criador</asp:label><cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab><asp:label id="lblIdAnt" runat="server"></asp:label><cc1:textboxtab id="txtRazaSessId" runat="server"></cc1:textboxtab><asp:textbox id="txtRazaId" runat="server"></asp:textbox></DIV>
		</TD>
	</TR>
	<TR>
		<TD colSpan="4" align="right"><asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Width="100%" Rows="2" TextMode="MultiLine"
				Enabled="False"></asp:textbox></TD>
	</TR>
</TABLE>
<script language="javascript">
	if (document.all('chkAgrupa')!=null)
		document.all('chkAgrupa').checked = true;
		
	function mCriaCopropiedad(strId)
	{
		gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Copropiedad&tabla=criadores_por_cliente_raza&Rpt=L&filtros="+ strId, 7, "900","300","50","100");
	}
</script>
