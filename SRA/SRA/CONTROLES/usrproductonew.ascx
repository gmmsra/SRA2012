<%@ Reference Control="~/controles/usrcliederivnew.ascx" %>
<%@ Reference Page="~/Px.aspx" %>
<%@ Reference Control="~/controles/usrcliederivnew.ascx" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="usrClieDerivNew.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrProductoNew" CodeFile="usrProductoNew.ascx.vb" %>
<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
	<TR>
		<TD style="HEIGHT: 9px" vAlign="top" width="100%" colSpan="3" align="left">
			<TABLE border="0" cellSpacing="0" cellPadding="0">
				<TR id="trRazaCriador" runat="server">
					<TD align="right"><asp:label id="lblCriaOrProp" runat="server" cssclass="titulo">Propietario:</asp:label>&nbsp;</TD>
					<TD width="820" align="left"><UC1:CLIE id="usrCriadorFil" runat="server" MuestraBotonBusquedaAvanzada="False" Criador="True"
							AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="False" Ancho="820" FilDocuNume="True" Saltos="1,2"
							Tabla="Criadores" AceptaNull="false"></UC1:CLIE></TD>
				</TR>
				<TR id="trSexo" runat="server">
					<TD align="right"><asp:label id="Label3" runat="server" cssclass="titulo">Sexo:</asp:label>&nbsp;</TD>
					<TD align="left"><anthem:panel id="PnlSraNume" runat="server">
					<asp:DropDownList id="cmbProdSexo" runat="server" CssClass="combo" AutoPostBack="True"></asp:DropDownList>
<asp:label id="lblRP" runat="server" cssclass="titulo">RP:</asp:label>&nbsp; 
<cc1:textboxtab id="txtRP" runat="server" cssclass="cuadrotexto" Width="150" MaxLength="20"></cc1:textboxtab>&nbsp; 
<asp:label id="lblSraNume" runat="server" cssclass="titulo">Nro:</asp:label>&nbsp; 
<cc1:numberbox id="txtSraNume" runat="server" cssclass="cuadrotexto" Width="80" MaxLength="9"></cc1:numberbox></anthem:panel></TD>
				</TR>
				<TR id="trFechaNaci" runat="server">
					<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
						<asp:Label id="lblFechaNaci" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
					<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
						<cc1:DateBox id="txtFechaNaci" runat="server" cssclass="cuadrotexto" width="68px"></cc1:DateBox>&nbsp;
						<asp:checkbox id="chkFechaNaciTrim" Runat="server" CssClass="titulo" Text="Es Trimrestre"></asp:checkbox>&nbsp;
						<asp:Label id="lblPx" runat="server" cssclass="titulo">PX:</asp:Label>&nbsp;
						<cc1:textboxtab id="txtPx" runat="server" cssclass="cuadrotexto" Width="50px" MaxLength="4"></cc1:textboxtab>&nbsp;
					</TD>
				</TR>
				<TR id="trAsocExtranjera" runat="server">
					<TD align="right"><asp:label id="lblNume" runat="server" cssclass="titulo">Asoc.Extr.:</asp:label>&nbsp;</TD>
					<TD align="left"><anthem:panel id="PnlcmbProdAsoc" runat="server">
							<cc1:combobox id="cmbProdAsoc" class="combo" runat="server" cssclass="cuadrotexto" AceptaNull="True"
								onchange="Combo_change(this)" Width="500" MostrarBotones="False" Filtra="True"></cc1:combobox></TD>
					</anthem:panel></TR>
				<TR id="trNroExtr" runat="server">
					<TD align="right"><asp:label id="Label1" runat="server" cssclass="titulo">Nro.Extr.:</asp:label>&nbsp;</TD>
					<TD align="left"><cc1:textboxtab id="txtCodi" runat="server" cssclass="cuadrotexto" Width="150" MaxLength="20"></cc1:textboxtab></TD>
				</TR>
				<TR id="trTipoRegistro" runat="server">
					<TD align="right"><asp:label id="lblTipoRegistro" runat="server" cssclass="titulo">Tipo Reg.:</asp:label>&nbsp;</TD>
					<td><anthem:panel id="PnlTipoRegistro" runat="server">
							<TABLE>
								<TR>
									<TD id="tdRazaCria" width="50" noWrap align="left" runat="server">
										<asp:TextBox id="txtTipoRegistroCodi" runat="server" cssclass="cuadrotexto" AutoPostBack="True"
											Width="50px"></asp:TextBox></TD>
									<TD id="tdRazaCria1" width="120" noWrap align="left" runat="server">
										<asp:DropDownList id="cmbTipoRegistro" runat="server" cssclass="combo" AutoPostBack="True" Width="120px"></asp:DropDownList></TD>
									<TD align="right">
										<asp:label id="lblVariadad" runat="server" cssclass="titulo">Variedad:</asp:label>&nbsp;</TD>
									<TD id="Td1" width="50" noWrap align="left" runat="server">
										<asp:TextBox id="txtVariadadCodi" runat="server" cssclass="cuadrotexto" AutoPostBack="True" Width="50px"></asp:TextBox></TD>
									<TD id="Td2" width="120" noWrap align="left" runat="server">
										<asp:DropDownList id="cmbVariadad" runat="server" cssclass="combo" AutoPostBack="True" Width="120px"></asp:DropDownList></TD>
								</TR>
							</TABLE>
						</anthem:panel></td>
				</TR>
				<TR id="trNroCondicional" runat="server">
					<TD align="right"><asp:label id="lblNroCondicional" runat="server" cssclass="titulo">Nro.Condicional:</asp:label>&nbsp;</TD>
					<TD align="left"><cc1:numberbox id="txtNroCondicional" runat="server" cssclass="cuadrotexto" Width="80" MaxLength="9"></cc1:numberbox></TD>
				</TR>
				<TR id="trProdNomb" runat="server">
					<TD align="right"><asp:label id="lblApel" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
					<TD align="left"><cc1:textboxtab id="txtProdNomb" runat="server" cssclass="textolibre" Width="500" TextMode="MultiLine"
							Height="20" Rows="1"></cc1:textboxtab><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							id="imgBusc" language="javascript" src="..\imagenes\Buscar16.gif" runat="server">
						<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							id="imgLimp" language="javascript" alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp"
							runat="server"> <IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							id="imgBuscAvan" language="javascript" alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif" runat="server">
					</TD>
				</TR>
				<TR id="trDescrip" runat="server">
					<TD align="right"><asp:label id="lblDescrip" runat="server" cssclass="titulo">Descrip.:</asp:label>&nbsp;</TD>
					<TD align="left"><asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Width="95%" TextMode="MultiLine"
							Rows="2"></asp:textbox></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD colSpan="3">
			<DIV 
      style="Z-INDEX: 101; POSITION: absolute; WIDTH: 360px; HEIGHT: 231px; VISIBILITY: hidden" 
      id="panBuscAvan<%=mstrCtrlId%>" class=paneledicion>
				<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
					<TR>
						<TD height="34" vAlign="middle" colSpan="2" align="center"><SPAN class="titulo">B�squeda 
								Avanzada</SPAN><INPUT 
            name="hdnBusc<%=mstrCtrlId%>" value=D size=1 type=hidden 
            ></TD>
					</TR>
					<TR>
						<TD width="20"></TD>
						<TD><SPAN id="spRpNume" runat="server"><asp:label id="lblRpNume" runat="server" cssclass="titulo">&nbsp;Nro.RP:</asp:label>&nbsp;
								<cc1:numberbox id="txtRpNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;</SPAN>
							<SPAN id="br1" runat="server">
								<BR>
							</SPAN><SPAN style="DISPLAY: inline; CLEAR: none" id="spCriaNume" runat="server">
								<asp:label id="lblCriaRaza" runat="server" cssclass="titulo" Visible="true">&nbsp;Raza:</asp:label>&nbsp;
								<cc1:combobox id="cmbCriaNumeRaza" class="combo" runat="server" onchange="Combo_change(this)"
									Width="197px" tag="N"></cc1:combobox><asp:label id="lblCriaNume" runat="server" cssclass="titulo">&nbsp;Nro.Criador:&nbsp;</asp:label><cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></SPAN><SPAN id="br2" runat="server"><BR>
							</SPAN><SPAN style="DISPLAY: inline" id="spLaboNume" runat="server">
								<asp:label id="lblLaboNume" runat="server" cssclass="titulo">&nbsp;Nro.Tr�m.Laborat.:&nbsp;</asp:label><cc1:numberbox id="txtLaboNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox></SPAN><SPAN id="br3" runat="server"><BR>
							</SPAN><SPAN id="spNaciFecha" runat="server">
								<asp:label id="lblNaciFecha" runat="server" cssclass="titulo">&nbsp;F.Nacim.:&nbsp;</asp:label><cc1:datebox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
									ImagesUrl="Images/"></cc1:datebox></SPAN><SPAN id="br4" runat="server"></SPAN></TD>
					</TR>
					<TR>
						<TD height="40" vAlign="middle" colSpan="2" align="center"><INPUT id="btnBuscAcep<%=mstrCtrlId%>" class=boton onclick="javascript:mAceptarProd(<%=mstrCtrlId%>)" value=Aceptar type=button>&nbsp;&nbsp;
							<INPUT id="btnBuscCanc<%=mstrCtrlId%>" class=boton onclick="javascript:mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>');"value=Cancelar type=button>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV style="DISPLAY: none"><cc1:textboxtab id="hdnRazaCruza" runat="server" autopostback="true"></cc1:textboxtab><cc1:textboxtab id="hdnValiExis" runat="server" autopostback="true"></cc1:textboxtab><asp:textbox id="hdnCriaNume" runat="server"></asp:textbox><asp:textbox id="hdnCriaOrProp" runat="server"></asp:textbox><asp:textbox id="txtRegtId" runat="server"></asp:textbox><asp:label id="lblId" runat="server">Cliente</asp:label><cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab><cc1:textboxtab id="txtSexoId" runat="server" size="4"></cc1:textboxtab><cc1:textboxtab id="txtRazaSessId" runat="server"></cc1:textboxtab><asp:label id="lblIdAnt" runat="server"></asp:label><asp:textbox id="txtCriaId" runat="server"></asp:textbox><asp:textbox id="txtIgnoraInexist" runat="server"></asp:textbox><asp:textbox id="hdnRazaId" runat="server"></asp:textbox><cc1:textboxtab id="txtIdEvent" runat="server" autopostback="true"></cc1:textboxtab></DIV>
		</TD>
	</TR>
</TABLE>
<script language="javascript">
	/*function mValida(pCtrl)
	{
		var oper = document.all(pCtrl + "_lblCriaOrProp").innerHTML //Obtengo el texto del label lblCriaOrProp.
		var sCriaOrProp = oper.replace(/:/gi,""); //Reemplazo el ":" por "" en toda la cadena.	
		
		//Valido que haya completado el control de Raza - Criador/Propietario.
		if (document.all(pCtrl + ":usrCriadorFil:txtId").value == "")
			{
				alert("Debe indicar la Raza y el " + sCriaOrProp + ".");
				return(false);
			}
			
		return(true);
	}
	*/
	function mValida(pCtrl)
	{
		var oper = document.all(pCtrl + "_lblCriaOrProp").innerHTML //Obtengo el texto del label lblCriaOrProp.
		var sCriaOrProp = oper.replace(/:/gi,""); //Reemplazo el ":" por "" en toda la cadena.	
		
		//Valido que haya completado el control de Raza - Criador/Propietario.
		if (document.all(pCtrl + ":usrCriadorFil:cmbRazaCria").value == "")
			{
				alert("Debe indicar la Raza .");
				return(false);
			}
			
		return(true);
	}

	function mAceptarProd(pCtrl)
	{
		if (mValidaConsultaProd(pCtrl))
			mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1',pCtrl);
	}
</script>
