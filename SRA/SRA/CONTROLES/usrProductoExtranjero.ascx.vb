Namespace SRA

Partial Class usrProductoExtranjero
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCriaOrProp As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents txtTipoRegistro As System.Web.UI.WebControls.TextBox

    'Protected WithEvents lblEsperePorFavor As System.Web.UI.WebControls.Label



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Event Cambio(ByVal sender As System.Object)
    Protected mstrCtrlId As String

#Region "Variables privadas"
    Public mstrConn As String

    Private mstrTabla As String
    Private mstrCampoCodi As String
    Private mintAncho As Integer = 600
    Private mintAlto As Integer = 450
    Private mbooAceptaNull As Boolean = True
    Private mbooMuestraDesc As Boolean = False
    Private mbooAutoGeneraHBA As Boolean = False
    Private mbooMuestraBotonAgregarImportado As Boolean = True
    Private mstrTipoControl As String = "producto" 'puede ser: producto, madre o padre.
    Private mstrTipoProducto As String = "" 'puede ser: S (Semen) , E (Embrion)

    Private mstrApodo As String = ""
    Private mstrNacionalidad As String = "E" 'puede ser: producto, madre o padre.
    Private strFechaParametro As String = ""
    Private mbooAutoPostback As Boolean = False
    Private mbooPermiModi As Boolean = False
    Private mbooSoloBusq As Boolean = False
    Private mbooIncluirDeshabilitados As Boolean = False
    Private mintValiExis As Integer = 1
    Private mstrCampoVal As String = "Producto"
    Private mstrPagina As String
    Private mstrIdControlPropietario As String = ""

    Private mstrFiltroRazas As String = ""
    Private mstrRazaId As String = ""

    Private mbooFilProp As Boolean = False
    Private mbooFilSraNume As Boolean = True
    Private mbooFilRpNume As Boolean = False
    Private mbooFilCriaNume As Boolean = True
    Private mbooFilLaboNume As Boolean = True
    Private mbooFilAsocNume As Boolean = False
    Private mbooFilNaciFecha As Boolean = True
    Private mbooFilMadre As Boolean = False
    Private mbooFilPadre As Boolean = False
    Private mbooFilRece As Boolean = False
    Private mbooFilSexo As Boolean = True
    Private mbooFilNumExtr As Boolean = True
    Private mstrTipo As String = ""
    Private mstrEstaId As String = ""

    Private mbooColNomb As Boolean = True
    Private mbooColSraNume As Boolean = True
    Private mbooColRpNume As Boolean = True
    Private mbooColCriaNume As Boolean = True
    Private mbooColLaboNume As Boolean = False
    Private mbooColAsocNume As Boolean = True
    Private mbooColNaciFecha As Boolean = True
    Private mbooColRazaSoloCodi As Boolean = False

    Private mbooInscrip As Boolean = False

    Private mstrSaltos As String = ""
    Private mstrSexo As String = ""
    Private mstrCriaId As String = ""
    Private mstrRaza As String = ""
    Private mbooBloqCriaFil As Boolean = False

    Private mbooIgnoraInexistencia As Boolean = False
#End Region

#Region "Metodos privados"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lstrSRA As String
        Dim lstrSP As String = "razas_cargar "

        mstrCtrlId = Me.ID
        mstrConn = clsWeb.gVerificarConexion(Me.Page)

        If mbooFilSexo Then
            cmbProdSexo.Enabled = True
        Else
            cmbProdSexo.Enabled = False
        End If

        If txtSexoId.Text <> "" Then
            mstrSexo = txtSexoId.Text
        End If

        If txtApodo.Text <> "" Then
            mstrApodo = txtApodo.Text
        End If
        If txtCriaId.Text <> "" Then
            mstrCriaId = txtCriaId.Text
        End If

        If (hdnIdCtrlPropietario.Text <> "") Then
            mstrIdControlPropietario = hdnIdCtrlPropietario.Text()
        End If

        If mbooMuestraDesc Then
            trDescrip.Style.Add("display", "inline")
        Else
            trDescrip.Style.Add("display", "none")
        End If

        If mbooMuestraBotonAgregarImportado Then
            imgAdd.Style.Add("display", "inline")
        Else
            imgAdd.Style.Add("display", "none")
        End If

        If (AutoGeneraHBA() = True) Then
            txtAutoGeneraHBA.Text = "True"
        End If

        txtNacionalidad.Text = Nacionalidad()

        txtFechaParametro.Text = FechaParametro()

        imgBuscAvan.Style.Add("display", "none")

        'DivEsperePorFavor.Style.Add("display", "none")

        cmbProdSexo.Valor = mstrSexo
        txtSexoId.Text = mstrSexo
        txtCriaId.Text = mstrCriaId

        hdnValiExis.Text = mintValiExis.ToString
        txtTipoProducto.Text = mstrTipoProducto

        ' Dario 2014-07-01    
        Me.cmbProdSexo.Enabled = False

        If (mstrTipoControl = "padre") Then
            Me.cmbProdSexo.Valor = "1"
        ElseIf (mstrTipoControl = "madre") Then
            Me.cmbProdSexo.Valor = "0"
        Else
            Me.cmbProdSexo.Valor = mstrSexo
            Me.cmbProdSexo.Enabled = True
        End If

        If Not Page.IsPostBack Then
            mSetearEventos()
            lstrSRA = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbProdAsoc, "id", "descrip", "T")

            If hdnRazaCruza.Text <> "" And txtSexoId.Text <> "" Then
                lstrSP = "razas_padres_cargar " & hdnRazaCruza.Text & "," & txtSexoId.Text
            End If
            clsWeb.gCargarCombo(mstrConn, lstrSP, cmbCriaNumeRaza, "id", "descrip", "T")

            SRA_Neg.Utiles.gSetearRaza(cmbCriaNumeRaza)

            If mbooInscrip Then lstrSP = "razas_inscrip_cargar"
            If hdnRazaCruza.Text <> "" And txtSexoId.Text <> "" Then
                lstrSP = "razas_padres_cargar " & hdnRazaCruza.Text & "," & txtSexoId.Text
            End If

            lstrSP += IIf(lstrSP.IndexOf(",") > 0 And mstrFiltroRazas <> "", ",", " ") & mstrFiltroRazas

            clsWeb.gCargarCombo(mstrConn, lstrSP, cmbProdRaza, "id", "descrip", "T")
            cmbProdRaza.SelectedIndex = cmbProdRaza.Items.IndexOf(cmbProdRaza.Items.FindByText(txtRazaId.Text))

            If cmbProdRaza.Valor.ToString = "" Then
                SRA_Neg.Utiles.gSetearRaza(cmbProdRaza)
            End If
        Else
            If lblIdAnt.Text <> txtId.Text Then
                'para refrescar el estado del textbox
                Valor = txtId.Text
            End If
        End If

        'If (txtId.Text = "") Then
        '    Me.imgAdd.Style.Add("display", "none")
        'Else
        '    Me.imgAdd.Style.Add("display", "inline")
        'End If

        txtId.CampoVal = CampoVal
        txtRazaSessId.Text = Session("sRazaId")
    End Sub

    Private Sub mSetearEventos()
        Dim lstrEstilo As String
        Dim lstrParams As New StringBuilder
        Dim lstrParamsProductoImportado As New StringBuilder
        Dim lstrSraAsocId As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
        Dim lstrInclDesha As String = IIf(mbooIncluirDeshabilitados, "1", "0")

        txtIgnoraInexist.Text = IIf(mbooIgnoraInexistencia, "1", "")

        'el bot�n de b�squeda
        lstrEstilo = String.Format("location=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width={0}px,height={1}px", Ancho.ToString, Alto.ToString)

        lstrParams.Append("FilSraNume=" & Math.Abs(CInt(FilSraNume)).ToString)
        lstrParams.Append("&FilRpNume=" & Math.Abs(CInt(FilRpNume)).ToString)
        lstrParams.Append("&FilProp=" & Math.Abs(CInt(FilProp)).ToString)
        lstrParams.Append("&FilLaboNume=" & Math.Abs(CInt(FilLaboNume)).ToString)
        lstrParams.Append("&FilCriaNume=" & Math.Abs(CInt(FilCriaNume)).ToString)
        lstrParams.Append("&FilNaciFecha=" & Math.Abs(CInt(FilNaciFecha)).ToString)
        lstrParams.Append("&FilMadre=" & Math.Abs(CInt(FilMadre)).ToString)
        lstrParams.Append("&FilPadre=" & Math.Abs(CInt(FilPadre)).ToString)
        lstrParams.Append("&FilRece=" & Math.Abs(CInt(FilRece)).ToString)
        lstrParams.Append("&FilSexo=" & Math.Abs(CInt(FilSexo)).ToString)

        lstrParams.Append("&ColNomb=" & Math.Abs(CInt(ColNomb)).ToString)
        lstrParams.Append("&ColSraNume=" & Math.Abs(CInt(ColSraNume)).ToString)
        lstrParams.Append("&ColRpNume=" & Math.Abs(CInt(ColRpNume)).ToString)
        lstrParams.Append("&ColLaboNume=" & Math.Abs(CInt(ColLaboNume)).ToString)
        lstrParams.Append("&ColCriaNume=" & Math.Abs(CInt(ColCriaNume)).ToString)
        lstrParams.Append("&ColAsocNume=" & Math.Abs(CInt(ColAsocNume)).ToString)
        lstrParams.Append("&ColNaciFecha=" & Math.Abs(CInt(ColNaciFecha)).ToString)

        lstrParams.Append("&FilTipo=" & FilTipo)
        lstrParams.Append("&SoloBusq=" & Math.Abs(CInt(mbooSoloBusq)))
        lstrParams.Append("&InclDeshab=" & Math.Abs(CInt(mbooIncluirDeshabilitados)))
        lstrParams.Append("&ValiExis=" & Math.Abs(CInt(mintValiExis)))
        lstrParams.Append("&EstaId=" & EstaId)
        lstrParams.Append("&Inscrip=" & Math.Abs(CInt(mbooInscrip)))
        lstrParams.Append("&ColRazaSoloCodi=" & Math.Abs(CInt(mbooColRazaSoloCodi)))
        lstrParams.Append("&BloqCriaFil=" & Math.Abs(CInt(mbooBloqCriaFil)))
        If hdnRazaCruza.Text <> "" Then
            lstrParams.Append("&RazaCruza=" & Math.Abs(CInt(hdnRazaCruza.Text)))
        End If
        lstrParams.Append("&EsExtr=1")

        'Par�metros Producto Importado.
        lstrParamsProductoImportado.Append("TipoControl=" & mstrTipoControl)
        lstrParamsProductoImportado.Append("&TipoProducto=" & mstrTipoProducto)
        lstrParamsProductoImportado.Append("&SexoAnimal=" & mstrTipoProducto)


        txtRPExtr.Attributes.Add("onchange", "CodiProductoExtranjero_change('" & UniqueID & "', '" & Math.Abs(CInt(AutoPostback)).ToString & "', '" & Tabla.ToString.ToLower & "_control" & ";" & "prdt_sra_nume" & ";" & ";" & ";" & EstaId & ";" & lstrInclDesha & "', '" & CampoVal & "', '" & lstrSraAsocId & "','sra_nume','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "');")
        imgBusc.Attributes.Add("onclick", "if (mValidaRaza('" & UniqueID & "')) {imgBuscProductoExtranjero_click('" & UniqueID & "','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "','false');}")
        imgBuscAvan.Attributes.Add("onclick", "mBuscMostrarProdUsr('panBuscAvan" & mstrCtrlId & "','visible','','" & mstrCtrlId & "');")
        imgLimp.Attributes.Add("onclick", "imgLimpProductoExtranjero_click('" & UniqueID & "');")
        imgAdd.Attributes.Add("onclick", "imgAddProductoImportado_click('" & UniqueID & "','" & lstrParamsProductoImportado.ToString & "', '" & mstrIdControlPropietario & "' );")

        spNaciFecha.Visible = False   'FilNaciFecha
        spRpNume.Visible = False      ' FilRpNume
        spCriaNume.Visible = False    ' FilCriaNume
        spLaboNume.Visible = FilLaboNume

    End Sub

    Private Sub txtId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtId.TextChanged
        RaiseEvent Cambio(Me)
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        If Saltos <> "" Then
            Dim vsSaltos() As String = Saltos.Split(",")
            Dim lstrCtrl As String
            Dim j, k As Integer

            For i As Integer = 0 To vsSaltos.GetUpperBound(0)
                j = 0
                While k < 4
                    k += 1
                    If Me.FindControl(mObtenerSpFil(k)).Visible Then
                        j += 1
                    End If
                End While
            Next
        End If
    End Sub

    Private Function mObtenerSpFil(ByVal pintSpanFil) As String
        Select Case pintSpanFil
            Case 1
                Return ("spRpNume")
            Case 2
                Return ("spCriaNume")
            Case 3
                Return ("spLaboNume")
            Case 4
                Return ("spNaciFecha")
        End Select
    End Function

#End Region

#Region "Propiedades publicas"

    Public Property CriaId() As String
        Get
            Return mstrCriaId
        End Get
        Set(ByVal Value As String)
            mstrCriaId = Value
        End Set
    End Property

    Public Property Raza() As String
        Get
            Return mstrRaza
        End Get
        Set(ByVal Value As String)
            mstrRaza = Value
        End Set
    End Property

    Public Property FilSexo() As Boolean
        Get
            Return mbooFilSexo
        End Get
        Set(ByVal Value As Boolean)
            mbooFilSexo = Value
        End Set
    End Property

    Public Property FilNumExtr() As Boolean
        Get
            Return mbooFilNumExtr
        End Get
        Set(ByVal Value As Boolean)
            mbooFilNumExtr = Value
        End Set
    End Property

    Public Property Sexo() As String
        Get
            Return mstrSexo
        End Get
        Set(ByVal Value As String)
            mstrSexo = Value
        End Set
    End Property

    Public Property FiltroRazas() As String
        Get
            Return mstrFiltroRazas
        End Get
        Set(ByVal Value As String)
            mstrFiltroRazas = Value
        End Set
    End Property

    Public Property Tabla() As String
        Get
            Return mstrTabla
        End Get
        Set(ByVal Value As String)
            mstrTabla = Value
        End Set
    End Property

    Public Property Pagina() As String
        Get
            If mstrPagina <> "" Then
                Return mstrPagina
            Else
                Dim lstrPagina As New StringBuilder
                Dim lintSub As Integer = Len(Page.TemplateSourceDirectory) - Len(Replace(Page.TemplateSourceDirectory, "/", "")) - 1
                If lintSub = 1 Then
                    lstrPagina.Append("../")
                ElseIf lintSub = 2 Then
                    lstrPagina.Append("../../")
                End If

                Select Case mstrTabla.ToLower
                    Case "productos"
                        lstrPagina.Append("productos.aspx")
                End Select
                Return (lstrPagina.ToString)
            End If
        End Get
        Set(ByVal Value As String)
            mstrPagina = Value
        End Set
    End Property

    Public Property CampoCodi() As String
        Get
            If mstrCampoCodi <> "" Then
                Return mstrCampoCodi
            Else
                Select Case mstrTabla.ToLower
                    Case "productos"
                        Return ("prdt_sra_nume")
                End Select
            End If
        End Get
        Set(ByVal Value As String)
            mstrCampoCodi = Value
        End Set
    End Property

    Public Property MuestraDesc() As Boolean
        Get
            Return mbooMuestraDesc
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraDesc = Value
        End Set
    End Property

    Public Property MuestraBotonAgregaImportado() As Boolean
        Get
            Return mbooMuestraBotonAgregarImportado
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraBotonAgregarImportado = Value
        End Set
    End Property

    Public Property TipoControl() As String
        Get
            Return mstrTipoControl
        End Get

        Set(ByVal Value As String)
            mstrTipoControl = Value
        End Set
    End Property

    Public Property TipoProducto() As String
        Get
            Return mstrTipoProducto
        End Get

        Set(ByVal Value As String)
            mstrTipoProducto = Value
        End Set
    End Property

    Public Property AutoGeneraHBA() As Boolean
        Get
            Return mbooAutoGeneraHBA
        End Get

        Set(ByVal Value As Boolean)
            mbooAutoGeneraHBA = Value
        End Set
    End Property

    Public Property Nacionalidad() As String
        Get
            Return mstrNacionalidad
        End Get

        Set(ByVal Value As String)
            mstrNacionalidad = Value
        End Set
    End Property

    Public Property FechaParametro() As String
        Get
            Return strFechaParametro
        End Get

        Set(ByVal Value As String)
            strFechaParametro = Value
        End Set
    End Property

    Public Property AceptaNull() As Boolean
        Get
            Return mbooAceptaNull
        End Get

        Set(ByVal Value As Boolean)
            mbooAceptaNull = Value
        End Set
    End Property

    Public Property AutoPostback() As Boolean
        Get
            Return mbooAutoPostback
        End Get

        Set(ByVal AutoPostback As Boolean)
            mbooAutoPostback = AutoPostback
        End Set
    End Property

    Public Property IncluirDeshabilitados() As Boolean
        Get
            Return mbooIncluirDeshabilitados
        End Get

        Set(ByVal IncluirDeshabilitados As Boolean)
            mbooIncluirDeshabilitados = IncluirDeshabilitados
        End Set
    End Property

    Public Property Obligatorio() As Boolean
        Get
            Return txtId.Obligatorio
        End Get

        Set(ByVal Value As Boolean)
            txtId.Obligatorio = Value
        End Set
    End Property

    Public Property CampoVal() As String
        Get
            Return mstrCampoVal
        End Get

        Set(ByVal CampoVal As String)
            mstrCampoVal = CampoVal
        End Set
    End Property

    Public Property IdControlPropietario() As String
        Get
            Return mstrIdControlPropietario
        End Get

        Set(ByVal IdControlPropietario As String)
            mstrIdControlPropietario = IdControlPropietario
            Me.hdnIdCtrlPropietario.Text = IdControlPropietario
        End Set
    End Property

    Public Property Valor() As Object
        Get
            If txtId.Text = "" Then
                If AceptaNull Then
                    Return (DBNull.Value)
                Else
                    Return (0)
                End If
            Else
                Return (txtId.Text)
            End If
        End Get

        Set(ByVal Valor As Object)
            If (Valor Is DBNull.Value) OrElse (Valor.ToString = "") OrElse (Valor.ToString = "0") Then
                txtId.Text = ""
                txtProdNomb.Text = ""
                txtCodi.Text = ""
                txtDesc.Text = ""
                txtApodo.Text = ""
                txtRPExtr.Text = ""
                hdnRazaCruza.Text = ""
                'If mstrSexo <> "" Then
                cmbProdSexo.Valor = mstrSexo
                txtSexoId.Text = mstrSexo
                'End If
                txtCriaId.Text = mstrCriaId
                If mstrConn Is Nothing Then
                    mstrConn = clsWeb.gVerificarConexion(Me.Page)
                End If
                clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbProdAsoc, "id", "descrip", "T")
                'cmbProdAsoc.Valor = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
                'If usrCriadorFil.Activo Then
                '    usrCriadorFil.Limpiar()
                '    If mstrRaza <> "" Then
                '        usrCriadorFil.RazaId = mstrRaza
                '    End If
                'End If

                'mSetearDeno()
                If cmbProdRaza.Enabled Then
                    cmbProdRaza.Limpiar()
                    If mstrRaza <> "" Then
                        cmbProdRaza.Valor = mstrRaza
                    Else
                        SRA_Neg.Utiles.gSetearRaza(cmbProdRaza)
                    End If
                End If
                ' Dario 2014-07-03
                Me.hidFechaNaci.Text = ""
                Me.hidRPNac.Text = ""
                Me.hidTipoRegistro.Text = ""
                Me.hidVariedad.Text = ""
                Me.hidPelaAPeli.Text = ""
                'Me.imgAdd.Style.Add("display", "inline")
            Else
                txtId.Text = Valor

                Dim vsRet As String()
                vsRet = SRA_Neg.Utiles.BuscarProdDeriv(Page.Session("sConn").ToString, txtId.Text & ";" & Tabla & "_control" & ";id;" & ";" & ";" & EstaId & ";" & IIf(mbooIncluirDeshabilitados, "1", "0")).Split("|")

                mstrConn = Page.Session("sConn").ToString
                If vsRet.GetUpperBound(0) > 0 Then
                    cmbProdRaza.Valor = vsRet(1)
                    txtId.Text = vsRet(0)
                    txtRazaId.Text = vsRet(1)
                    txtProdNomb.Text = vsRet(3)
                    cmbProdSexo.ValorBool = CType(IIf(vsRet(8) = "", "True", vsRet(8)), Boolean)
                    txtSexoId.Text = cmbProdSexo.Valor.ToString
                    txtDesc.Text = "HBA: " + vsRet(9) + " RP:" + vsRet(6) + " ** " + vsRet(5)
                    txtRPExtr.Text = vsRet(11)
                    txtApodo.Text = vsRet(14)
                    txtRegtId.Text = vsRet(10)
                    If vsRet(7) <> clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id") Then
                        cmbProdAsoc.Valor = vsRet(7)
                        txtCodi.Text = vsRet(2)
                    Else
                        cmbProdAsoc.Limpiar()
                        txtCodi.Text = ""
                    End If
                    ' Dario 2014-07-03
                    If (vsRet(17) <> "") Then
                        Me.hidFechaNaci.Text = Convert.ToDateTime(vsRet(17)).ToString("dd/MM/yyyy")
                    Else
                        Me.hidFechaNaci.Text = ""
                    End If

                    Me.hidRPNac.Text = vsRet(6)
                    Me.hidTipoRegistro.Text = vsRet(10)
                    Me.hidVariedad.Text = vsRet(15)
                    Me.hidPelaAPeli.Text = vsRet(21)
                    'Me.imgAdd.Style.Add("display", "none")
                Else
                    cmbProdRaza.Limpiar()
                    SRA_Neg.Utiles.gSetearRaza(cmbProdRaza)
                    cmbProdAsoc.Valor = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
                    txtCodi.Text = ""
                    txtProdNomb.Text = ""
                    cmbProdSexo.Valor = mstrSexo
                    txtCriaId.Text = mstrCriaId
                    txtSexoId.Text = mstrSexo
                    txtDesc.Text = ""
                    txtApodo.Text = ""
                    txtRPExtr.Text = ""
                    ' Dario 2014-07-03
                    Me.hidFechaNaci.Text = ""
                    Me.hidRPNac.Text = ""
                    Me.hidTipoRegistro.Text = ""
                    Me.hidVariedad.Text = ""
                    Me.hidPelaAPeli.Text = ""
                    'Me.imgAdd.Style.Add("display", "inline")
                End If

                If PermiModi Then
                    Activo = Activo
                End If
            End If
            lblIdAnt.Text = txtId.Text
        End Set
    End Property

    Public Property Activo() As Boolean
        Get
            Return txtCodi.Enabled
        End Get

        Set(ByVal Activo As Boolean)
            cmbProdRaza.Enabled = Activo
            If mstrSexo = "" Then cmbProdSexo.Enabled = Activo
            cmbProdAsoc.Enabled = Activo
            txtCodi.Enabled = Activo
            txtRPExtr.Enabled = Activo
            txtProdNomb.Enabled = Activo
            imgBusc.Disabled = Not Activo And (Not PermiModi Or Valor Is DBNull.Value)  'si est� inactivo, permite entrar solo a modicar
            imgBuscAvan.Disabled = Not Activo
            imgLimp.Disabled = Not Activo
            'usrCriadorFil.Activo = Activo
        End Set
    End Property

    Public Property SoloBusq() As Boolean
        Get
            Return mbooSoloBusq
        End Get

        Set(ByVal SoloBusq As Boolean)
            mbooSoloBusq = SoloBusq
        End Set
    End Property

    Public Property ValiExis() As Integer
        Get
            Return mintValiExis
        End Get

        Set(ByVal ValiExis As Integer)
            mintValiExis = ValiExis
        End Set
    End Property

    Public Property BloqCriaFil() As Boolean
        Get
            Return mbooBloqCriaFil
        End Get

        Set(ByVal value As Boolean)
            mbooBloqCriaFil = value
        End Set
    End Property

    Public Property PermiModi() As Boolean
        Get
            Return mbooPermiModi
        End Get

        Set(ByVal PermiModi As Boolean)
            mbooPermiModi = PermiModi
        End Set
    End Property

    Public Property Ancho() As Integer
        Get
            Return mintAncho
        End Get

        Set(ByVal Ancho As Integer)
            mintAncho = Ancho
        End Set
    End Property

    Public Property Alto() As Integer
        Get
            Return mintAlto
        End Get

        Set(ByVal Alto As Integer)
            mintAlto = Alto
        End Set
    End Property

    Public Property Saltos() As String
        Get
            Return mstrSaltos
        End Get

        Set(ByVal Value As String)
            mstrSaltos = Value
        End Set
    End Property

    Public Property IgnogaInexistente() As Boolean
        Get
            Return mbooIgnoraInexistencia
        End Get
        Set(ByVal Value As Boolean)
            mbooIgnoraInexistencia = Value
        End Set
    End Property

    Public ReadOnly Property ProdProp() As String
        Get
            Return (txtProdNomb.Text)
        End Get
    End Property

    Public ReadOnly Property Codi() As String
        Get
            Return (txtCodi.Text)
        End Get
    End Property

    Public ReadOnly Property ProdId() As Object
        Get
            Dim oRet As Object
            Dim lDsDatos As DataSet

            oRet = DBNull.Value

            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, Tabla, Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    Select Case Tabla.ToLower
                        Case "productos"
                            oRet = lDsDatos.Tables(0).Rows(0).Item("prdt_id")
                    End Select
                End If
            End If

            If Not AceptaNull And oRet Is DBNull.Value Then
                oRet = "0"
            End If

            Return (oRet)
        End Get
    End Property

#Region "Propiedades para seteo de columnas"

    Public Property ColNomb() As Boolean
        Get
            Return mbooColNomb
        End Get
        Set(ByVal Value As Boolean)
            mbooColNomb = Value
        End Set
    End Property

    Public Property ColSraNume() As Boolean
        Get
            Return mbooColSraNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColSraNume = Value
        End Set
    End Property

    Public Property ColRpNume() As Boolean
        Get
            Return mbooColRpNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColRpNume = Value
        End Set
    End Property

    Public Property ColLaboNume() As Boolean
        Get
            Return mbooColLaboNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColLaboNume = Value
        End Set
    End Property

    Public Property ColRazaSoloCodi() As Boolean
        Get
            Return mbooColRazaSoloCodi
        End Get
        Set(ByVal Value As Boolean)
            mbooColRazaSoloCodi = Value
        End Set
    End Property

    Public Property ColCriaNume() As Boolean
        Get
            Return mbooColCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColCriaNume = Value
        End Set
    End Property

    Public Property ColAsocNume() As Boolean
        Get
            Return mbooColAsocNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColAsocNume = Value
        End Set
    End Property

    Public Property ColNaciFecha() As Boolean
        Get
            Return mbooColNaciFecha
        End Get
        Set(ByVal Value As Boolean)
            mbooColNaciFecha = Value
        End Set
    End Property

#End Region

#Region "Propiedades para seteo de filtros"

    Public Property FilMadre() As Boolean
        Get
            Return mbooFilMadre
        End Get
        Set(ByVal Value As Boolean)
            mbooFilMadre = Value
        End Set
    End Property

    Public Property FilPadre() As Boolean
        Get
            Return mbooFilPadre
        End Get
        Set(ByVal Value As Boolean)
            mbooFilPadre = Value
        End Set
    End Property

    Public Property FilRece() As Boolean
        Get
            Return mbooFilRece
        End Get
        Set(ByVal Value As Boolean)
            mbooFilRece = Value
        End Set
    End Property

    Public Property FilProp() As Boolean
        Get
            Return mbooFilProp
        End Get
        Set(ByVal Value As Boolean)
            mbooFilProp = Value
        End Set
    End Property

    Public Property FilSraNume() As Boolean
        Get
            Return mbooFilSraNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilSraNume = Value
        End Set
    End Property

    Public Property FilRpNume() As Boolean
        Get
            Return mbooFilRpNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilRpNume = Value
        End Set
    End Property

    Public Property FilLaboNume() As Boolean
        Get
            Return mbooFilLaboNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilLaboNume = Value
        End Set
    End Property

    Public Property FilCriaNume() As Boolean
        Get
            Return mbooFilCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilCriaNume = Value
        End Set
    End Property

    Public Property FilAsocNume() As Boolean
        Get
            Return mbooFilAsocNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilAsocNume = Value
        End Set
    End Property

    Public Property FilNaciFecha() As Boolean
        Get
            Return mbooFilNaciFecha
        End Get
        Set(ByVal Value As Boolean)
            mbooFilNaciFecha = Value
        End Set
    End Property

    Public Property Inscrip() As Boolean
        Get
            'si es inscripciones, carga razas con un SP especial 
            Return mbooInscrip

        End Get
        Set(ByVal Value As Boolean)
            mbooInscrip = Value
        End Set
    End Property

    Public Property FilTipo() As String
        Get
            Return mstrTipo
        End Get
        Set(ByVal Value As String)
            mstrTipo = Value
        End Set
    End Property

    Public Property EstaId() As String
        Get
            Return mstrEstaId
        End Get
        Set(ByVal Value As String)
            mstrEstaId = Value
        End Set
    End Property

    Public ReadOnly Property txtIdExt() As NixorControls.TextBoxTab
        Get
            Return txtId
        End Get
    End Property

    Public ReadOnly Property txtCodiExt() As NixorControls.TextBoxTab
        Get
            Return txtCodi
        End Get
    End Property

    Public ReadOnly Property txtProdNombExt() As NixorControls.TextBoxTab
        Get
            Return txtProdNomb
        End Get
    End Property

    Public ReadOnly Property txtSexoIdExt() As NixorControls.TextBoxTab
        Get
            Return txtSexoId
        End Get
    End Property

    Public ReadOnly Property cmbProdRazaExt() As NixorControls.ComboBox
        Get
            Return (cmbProdRaza)
        End Get
    End Property

    Public ReadOnly Property hdnRegtId() As System.Web.UI.WebControls.TextBox
        Get
            Return (txtRegtId)
        End Get
    End Property

    Public ReadOnly Property cmbSexoProdExt() As NixorControls.ComboBox
        Get
            Return (cmbProdSexo)
        End Get
    End Property

    Public ReadOnly Property cmbProdAsocExt() As NixorControls.ComboBox
        Get
            Return (cmbProdAsoc)
        End Get
    End Property

    Public ReadOnly Property txtRPExt() As NixorControls.TextBoxControls
        Get
            Return (txtRPExtr)
        End Get
    End Property

    Public ReadOnly Property hdnRazaCruzaExt() As NixorControls.TextBoxTab
        Get
            Return (hdnRazaCruza)
        End Get
    End Property

#End Region

#End Region

#Region "Metodos publicos"

    Public Sub Limpiar()
        Valor = DBNull.Value
    End Sub

#End Region

End Class

End Namespace
