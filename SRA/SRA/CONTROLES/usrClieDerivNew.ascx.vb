Namespace SRA

Partial Class usrClieDerivNew
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkFanta As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents spFanta As System.Web.UI.HtmlControls.HtmlGenericControl

    Protected WithEvents ps1 As System.Web.UI.HtmlControls.HtmlGenericControl



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Event Cambio(ByVal sender As System.Object)
    Event RazaChange(ByVal sender As System.Web.UI.WebControls.DropDownList)
    Protected mstrCtrlId As String

#Region "Variables privadas"
    Public mstrConn As String

    Private mstrTabla As String
    'Private mbooHabilitarRaza As Boolean = True
    Private mstrRazaId As String = ""
    Private mstrCampoCodi As String
    Private mintAncho As Integer = 790
    Private mintAlto As Integer = 450
    Private mbooAceptaNull As Boolean = True
    Private mbooAutoPostback As Boolean = False
    Private mbooPermiModi As Boolean = False
    Private mbooSoloBusq As Boolean = False
    Private mstrCampoVal As String = "Cliente"
    Private mbooMuestraBotonBusquedaAvanzada As Boolean = True
    Public mbooCriador As Boolean = False
    Public mbooRaza As Boolean = False
    Public mbooIncluirDeshabilitados As Boolean = False
    Private mstrPagina As String
    Private mstrFiltroRazas As String = ""

    Private mbooFilNomb As Boolean = False
    Private mbooFilFanta As Boolean = False
    Private mbooFilClieNume As Boolean = False
    Private mbooFilSociNume As Boolean = False
    Private mbooFilExpo As Boolean = True
    Private mbooFilCriaNume As Boolean = True
    Private mbooFilNoCriaNume As Boolean = False
    Private mbooFilLegaNume As Boolean = False
    Private mbooFilCUIT As Boolean = False
    Private mbooFilDocuNume As Boolean = False
    Private mbooFilTarjNume As Boolean = True
    Private mbooFilEnti As Boolean = False
    Private mbooFilMedioPago As Boolean = False
    Private mbooFilAgru As Boolean = True
    Private mbooFilEmpr As Boolean = False
    Private mbooFilClaveUnica As Boolean = False
    Private mstrTipo As String = ""
    Private mstrAutoSelect As String = ""

    Private mstrInclBloq As String = "T"
    Private mstrInseId As String = ""
    Private mstrCateTitu As String = ""
    Private mstrCatePode As String = ""
    Private mstrEstaId As String = ""

    Private mbooColNomb As Boolean = False
    Private mbooColFanta As Boolean = False
    Private mbooColClieNume As Boolean = False
    Private mbooColSociNume As Boolean = True
    Private mbooColCriaNume As Boolean = False
    Private mbooColNoCriaNume As Boolean = False
    Private mbooColLegaNume As Boolean = False
    Private mbooColCUIT As Boolean = False
    Private mbooColDocuNume As Boolean = False
    Private mbooColEnti As Boolean = False
    Private mbooColMedioPago As Boolean = False
    Private mbooColEmpr As Boolean = False
    Private mbooColClaveUnica As Boolean = False
    Private mbooInscrip As Boolean = False


    Private mstrSaltos As String = ""
#End Region

#Region "Metodos privados"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mstrCtrlId = Me.ID
        mstrConn = clsWeb.gVerificarConexion(Me.Page)
        chkAgrupa.Checked = True

        Me.txtRazaCodi.Visible = mbooCriador
        cmbRazaCria.Visible = mbooCriador
        tdRazaCria.Style.Add("Width", IIf(mbooCriador, 50, 0))
        tdRazaCria1.Style.Add("Width", IIf(mbooCriador, 180, 0))

        If mbooMuestraBotonBusquedaAvanzada Then
            imgBuscAvan.Style.Add("display", "inline")
        Else
            imgBuscAvan.Style.Add("display", "none")
        End If

        Dim lstrSP As String = "razas_cargar"

        If Not Page.IsPostBack Then
            mSetearEventos()
            If mbooInscrip Then lstrSP = "razas_inscrip_cargar"
            If FilCriaNume Then

                If FilAutoSelect <> "" And FilTipo = "S" Then
                    clsWeb.gCargarCombo(mstrConn, lstrSP & " " & FilAutoSelect, cmbCriaNumeRaza, "id", "descrip", "S")
                Else
                    clsWeb.gCargarCombo(mstrConn, lstrSP, cmbCriaNumeRaza, "id", "descrip", "T")
                End If
                SRA_Neg.Utiles.gSetearRaza(cmbRazaCria, Me.txtRazaCodi, mstrConn)
            End If
            If mbooCriador Then
                lstrSP += IIf(lstrSP.IndexOf(",") > 0 And mstrFiltroRazas <> "", ",", " ") & mstrFiltroRazas

                If FilAutoSelect <> "" And FilTipo = "S" Then
                    clsWeb.gCargarCombo(mstrConn, lstrSP & " ," & FilAutoSelect, cmbRazaCria, "id", "descrip", "S")
                Else
                    clsWeb.gCargarCombo(mstrConn, lstrSP, cmbRazaCria, "id", "descrip", "T")
                End If

                'cmbRazaCriaNew.SelectedIndex = cmbRazaCriaNew.Items.IndexOf(cmbRazaCriaNew.Items.FindByValue(txtRazaId.Text))
                'If cmbRazaCriaNew.SelectedValue.ToString = "0" Then
                SRA_Neg.Utiles.gSetearRaza(cmbRazaCria, Me.txtRazaCodi, mstrConn)
                'End If

            End If
            RazaId = cmbRazaCria.SelectedValue
            If FilDocuNume Then clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuNumeTido, "T")
            If FilEnti Then clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEntidad, "T")
            If FilEmpr Then clsWeb.gCargarRefeCmb(mstrConn, "empresas", cmbEmpresa, "T")
            If FilTarjNume Then clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjTipo, "T")
            If FilMedioPago Then clsWeb.gCargarRefeCmb(mstrConn, "pago_medios", cmbMedioPago, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "institutos", cmbInse, "T")
            If mstrInseId <> "" Then
                cmbInse.Visible = False
                cmbInse.Valor = mstrInseId
            Else
                cmbInse.Visible = True
                cmbInse.Limpiar()
            End If
        Else
            'gsz  cmbRazaCria.SelectedIndex = cmbRazaCria.Items.IndexOf(cmbRazaCria.Items.FindByValue(txtRazaId.Text))
            RazaId = cmbRazaCria.SelectedValue
            'If cmbRazaCria.Valor.ToString = "0" Then
            '    SRA_Neg.Utiles.gSetearRaza(cmbRazaCria)
            'End If

            If lblIdAnt.Text <> txtId.Text Then
                'para refrescar el estado del textbox
                Valor = txtId.Text
            End If
        End If
        txtId.CampoVal = CampoVal
        txtRazaSessId.Text = Session("sRazaId")

        'RaiseEvent RazaChange(Me.cmbRazaCria)

        'Me.Panel1.UpdateAfterCallBack = True
    End Sub

    Private Sub mSetearEventos()
        Dim lstrEstilo As String
        Dim lstrParams As New StringBuilder
        Dim lstrparam As String
        Dim lstrInclDesha As String

        lstrInclDesha = IIf(mbooIncluirDeshabilitados, "1", "0")

        txtCodi.Attributes.Add("onchange", "CodiClieDeriv_change('" & UniqueID & "', '" & Math.Abs(CInt(AutoPostback)).ToString & "', '" & Tabla.ToString.ToLower & ";" & CampoCodi.ToLower & ";" & FilInseId & ";" & CateTitu & ";" & CatePode & ";" & EstaId & ";" & lstrInclDesha & "', '" & CampoVal & "');")

        'el bot�n de b�squeda
        lstrEstilo = String.Format("location=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width={0}px,height={1}px", Ancho.ToString, Alto.ToString)

        lstrParams.Append("FilClieNume=" & Math.Abs(CInt(FilClieNume)).ToString)
        lstrParams.Append("&FilSociNume=" & Math.Abs(CInt(FilSociNume)).ToString)
        lstrParams.Append("&FilExpo=" & Math.Abs(CInt(FilExpo)).ToString)
        lstrParams.Append("&FilNomb=" & Math.Abs(CInt(FilNomb)).ToString)
        lstrParams.Append("&FilLegaNume=" & Math.Abs(CInt(FilLegaNume)).ToString)
        lstrParams.Append("&FilCriaNume=" & Math.Abs(CInt(FilCriaNume)).ToString)
        lstrParams.Append("&FilNoCriaNume=" & Math.Abs(CInt(FilNoCriaNume)).ToString)
        lstrParams.Append("&FilCUIT=" & Math.Abs(CInt(FilCUIT)).ToString)
        lstrParams.Append("&FilDocuNume=" & Math.Abs(CInt(FilDocuNume)).ToString)
        lstrParams.Append("&FilTarjNume=" & Math.Abs(CInt(FilTarjNume)).ToString)
        lstrParams.Append("&FilEnti=" & Math.Abs(CInt(FilEnti)).ToString)
        lstrParams.Append("&FilMedioPago=" & Math.Abs(CInt(FilMedioPago)).ToString)
        lstrParams.Append("&FilEmpr=" & Math.Abs(CInt(FilEmpr)).ToString)
        lstrParams.Append("&FilClaveUnica=" & Math.Abs(CInt(FilClaveUnica)).ToString)
        lstrParams.Append("&FilAgru=" & Math.Abs(CInt(FilAgru)).ToString)
        lstrParams.Append("&FilFanta=" & Math.Abs(CInt(FilFanta)).ToString)

        lstrParams.Append("&ColNomb=" & Math.Abs(CInt(ColNomb)).ToString)
        lstrParams.Append("&ColFanta=" & Math.Abs(CInt(ColFanta)).ToString)
        lstrParams.Append("&ColClieNume=" & Math.Abs(CInt(ColClieNume)).ToString)
        lstrParams.Append("&ColSociNume=" & Math.Abs(CInt(ColSociNume)).ToString)
        lstrParams.Append("&ColLegaNume=" & Math.Abs(CInt(ColLegaNume)).ToString)
        lstrParams.Append("&ColCriaNume=" & Math.Abs(CInt(ColCriaNume)).ToString)
        lstrParams.Append("&ColNoCriaNume=" & Math.Abs(CInt(ColNoCriaNume)).ToString)
        lstrParams.Append("&ColCUIT=" & Math.Abs(CInt(ColCUIT)).ToString)
        lstrParams.Append("&ColDocuNume=" & Math.Abs(CInt(ColDocuNume)).ToString)
        lstrParams.Append("&ColEnti=" & Math.Abs(CInt(ColEnti)).ToString)
        lstrParams.Append("&ColMedioPago=" & Math.Abs(CInt(ColMedioPago)).ToString)
        lstrParams.Append("&ColEmpr=" & Math.Abs(CInt(ColEmpr)).ToString)
        lstrParams.Append("&ColClaveUnica=" & Math.Abs(CInt(ColClaveUnica)).ToString)

        lstrParams.Append("&FilTipo=" & FilTipo)
        lstrParams.Append("&FilInseId=" & FilInseId)
        lstrParams.Append("&SoloBusq=" & Math.Abs(CInt(mbooSoloBusq)))
        lstrParams.Append("&InclDeshab=" & Math.Abs(CInt(mbooIncluirDeshabilitados)))
        lstrParams.Append("&InclBloq=" & mstrInclBloq)
        lstrParams.Append("&CateTitu=" & CateTitu)
        lstrParams.Append("&CatePode=" & CatePode)
        lstrParams.Append("&EstaId=" & EstaId)
        lstrParams.Append("&FilHabiRaza=" & IIf(cmbCriaRazaExt.Enabled, "1", "0"))
        lstrParams.Append("&FilAutoSelect=" & FilAutoSelect)
        lstrParams.Append("&pstNewClieDeriv=" & 1)
        imgBusc.Attributes.Add("onclick", "imgBuscClieDerivNew_click('" & IIf(mbooCriador, "S", "N") & "','" & UniqueID & "','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "');")
        imgBuscAvan.Attributes.Add("onclick", "mBuscMostrarUsr('panBuscAvan" & mstrCtrlId & "','visible','','" & mstrCtrlId & "');")
        imgLimp.Attributes.Add("onclick", "imgLimpClieDerivNew_click('" & UniqueID & "');")

        spDocuNume.Visible = FilDocuNume
        spTarjNume.Visible = FilTarjNume
        spEntidad.Visible = FilEnti
        spMedioPago.Visible = FilMedioPago
        spAgrupa.Visible = FilAgru
        spEmpresa.Visible = FilEmpr
        spClaveUnica.Visible = FilClaveUnica
        spNoCriaNume.Visible = FilNoCriaNume
        spNomb.Visible = FilNomb
        'spFanta.Visible = FilFanta
        spClieNume.Visible = FilClieNume
        spSociNume.Visible = FilSociNume
        spLegaNume.Visible = FilLegaNume
        spExpo.Visible = FilExpo
        spCriaNume.Visible = (FilCriaNume And Not mbooCriador)
        spCUIT.Visible = FilCUIT
    End Sub

    Private Sub txtId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtId.TextChanged
        RaiseEvent Cambio(Me)
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        If Saltos <> "" Then
            Dim vsSaltos() As String = Saltos.Split(",")
            Dim lstrCtrl As String
            Dim j, k As Integer

            For i As Integer = 0 To vsSaltos.GetUpperBound(0)
                j = 0
                While k < 11
                    k += 1
                    If Me.FindControl(mObtenerSpFil(k)).Visible Then
                        j += 1
                    End If
                    If j = vsSaltos(i) Then
                        Me.FindControl("br" & k).Visible = True
                        Exit While
                    End If
                End While
            Next
        End If
    End Sub

    Private Function mObtenerSpFil(ByVal pintSpanFil) As String
        Select Case pintSpanFil
            Case 1
                Return ("spTarjNume")
            Case 2
                Return ("spNomb")
            Case 3
                Return ("spClieNume")
            Case 4
                Return ("spSociNume")
            Case 5
                Return ("spCriaNume")
            Case 6
                Return ("spLegaNume")
            Case 7
                Return ("spNoCriaNume")
            Case 8
                Return ("spCUIT")
            Case 9
                Return ("spDocuNume")
            Case 10
                Return ("spEntidad")
            Case 11
                Return ("spMedioPago")
            Case 12
                Return ("spAgrupa")
            Case 13
                Return ("spEmpresa")
            Case 14
                Return ("spClaveUnica")
            Case 15
                Return ("spExpo")
        End Select
    End Function
#End Region

#Region "Propiedades publicas"
    Public Property Tabla() As String
        Get
            Return mstrTabla
        End Get
        Set(ByVal Value As String)
            mstrTabla = Value
        End Set
    End Property

    Public Property RazaId() As String
        Get
            Return mstrRazaId
        End Get
        Set(ByVal Value As String)
            mstrRazaId = Value
            cmbRazaCria.SelectedValue = mstrRazaId
        End Set
    End Property
    Public Property Inscrip() As Boolean
        Get
            'si es inscripciones, carga razas con un SP especial 
            Return mbooInscrip

        End Get
        Set(ByVal Value As Boolean)
            mbooInscrip = Value
        End Set
    End Property

    Public Property FiltroRazas() As String
        Get
            Return mstrFiltroRazas
        End Get
        Set(ByVal Value As String)
            mstrFiltroRazas = Value
        End Set
    End Property

    Public Property Pagina() As String
        Get
            If mstrPagina <> "" Then
                Return mstrPagina
            Else
                Dim lstrPagina As New StringBuilder
                Dim lintSub As Integer = Len(Page.TemplateSourceDirectory) - Len(Replace(Page.TemplateSourceDirectory, "/", "")) - 1
                If lintSub = 1 Then
                    lstrPagina.Append("../")
                ElseIf lintSub = 2 Then
                    lstrPagina.Append("../../")
                End If

                Select Case mstrTabla.ToLower
                    Case "clientes"
                        lstrPagina.Append("clientes.aspx")
                    Case "alumnos"
                        lstrPagina.Append("alumnos.aspx")
                    Case "socios"
                        lstrPagina.Append("socios.aspx")
                    Case "criadores"
                        lstrPagina.Append("criadores.aspx")
                End Select
                Return (lstrPagina.ToString)
            End If
        End Get
        Set(ByVal Value As String)
            mstrPagina = Value
        End Set
    End Property

    Public Property CampoCodi() As String
        Get
            If mstrCampoCodi <> "" Then
                Return mstrCampoCodi
            Else
                Select Case mstrTabla.ToLower
                    Case "clientes"
                        Return ("clie_id")
                    Case "alumnos"
                        Return ("alum_lega")
                    Case "socios"
                        Return ("soci_nume")
                    Case "criadores"
                        Return ("cria_nume")
                End Select
            End If
        End Get
        Set(ByVal Value As String)
            mstrCampoCodi = Value
        End Set
    End Property

    Public Property MuestraBotonBusquedaAvanzada() As Boolean
        Get
            Return mbooMuestraBotonBusquedaAvanzada
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraBotonBusquedaAvanzada = Value
        End Set
    End Property

    Public Property MuestraDesc() As Boolean
        Get
            Return txtDesc.Visible
        End Get

        Set(ByVal Value As Boolean)
            txtDesc.Visible = Value
        End Set
    End Property

    Public Property AceptaNull() As Boolean
        Get
            Return mbooAceptaNull
        End Get

        Set(ByVal Value As Boolean)
            mbooAceptaNull = Value
        End Set
    End Property

    Public Property AutoPostback() As Boolean
        Get
            Return mbooAutoPostback
        End Get

        Set(ByVal AutoPostback As Boolean)
            mbooAutoPostback = AutoPostback
        End Set
    End Property

    Public Property Obligatorio() As Boolean
        Get
            Return txtId.Obligatorio
        End Get

        Set(ByVal Value As Boolean)
            txtId.Obligatorio = Value
        End Set
    End Property

    Public Property CampoVal() As String
        Get
            Return mstrCampoVal
        End Get

        Set(ByVal CampoVal As String)
            mstrCampoVal = CampoVal
        End Set
    End Property

    Public Property Valor() As Object
        Get
            If txtId.Text = "" Then
                If AceptaNull Then
                    Return (DBNull.Value)
                Else
                    Return (0)
                End If
            Else
                Return (txtId.Text)
            End If
        End Get

        Set(ByVal Valor As Object)
            If (Valor Is DBNull.Value) OrElse (Valor.ToString = "") OrElse (Valor.ToString = "0") Then
                txtId.Text = ""
                txtApel.Text = ""
                txtCodi.Text = ""
                txtDesc.Text = ""
                If mbooCriador And cmbRazaCria.Enabled Then
                    'cmbRazaCria.Limpiar()
                    SRA_Neg.Utiles.gSetearRaza(cmbRazaCria, Me.txtRazaCodi, mstrConn)
                    RazaId = cmbRazaCria.SelectedValue
                End If
            Else
                txtId.Text = Valor

                Dim vsRet As String()
                vsRet = SRA_Neg.Utiles.BuscarClieDeriv(Page.Session("sConn").ToString, txtId.Text & ";" & Tabla & ";id;" & FilInseId & ";" & CateTitu & ";" & CatePode & ";" & EstaId & ";" & IIf(mbooIncluirDeshabilitados, "1", "0")).Split("|")

                If vsRet.GetUpperBound(0) > 0 Then
                    txtCodi.Text = vsRet(1)
                    txtApel.Text = vsRet(2)
                    txtDesc.Text = vsRet(3)
                    If (mbooCriador Or cmbRazaCria.Visible) And vsRet.GetUpperBound(0) >= 4 Then
                        txtRazaId.Text = vsRet(4)
                        cmbRazaCria.SelectedIndex = cmbRazaCria.Items.IndexOf(cmbRazaCria.Items.FindByValue(txtRazaId.Text))
                    End If
                Else
                    txtId.Text = ""
                    txtApel.Text = ""
                    txtCodi.Text = ""
                    txtDesc.Text = ""
                    If mbooCriador And cmbRazaCria.Enabled Then
                        'cmbRazaCria.Limpiar()
                        SRA_Neg.Utiles.gSetearRaza(cmbRazaCria, Me.txtRazaCodi, mstrConn)
                    End If
                End If

                If PermiModi Then
                    Activo = Activo
                End If
            End If

            lblIdAnt.Text = txtId.Text
        End Set
    End Property

    Public Property IncluirDeshabilitados() As Boolean
        Get
            Return mbooIncluirDeshabilitados
        End Get

        Set(ByVal IncluirDeshabilitados As Boolean)
            mbooIncluirDeshabilitados = IncluirDeshabilitados
        End Set
    End Property

    Public Property Criador() As Boolean
        Get
            Return mbooCriador
        End Get

        Set(ByVal Criador As Boolean)
            mbooCriador = Criador
        End Set
    End Property
    Public Property Raza() As Boolean
        Get
            Return mbooRaza
        End Get

        Set(ByVal Raza As Boolean)
            mbooRaza = Raza
        End Set
    End Property

    Public Property Activo() As Boolean
        Get
            Return txtCodi.Enabled
        End Get

        Set(ByVal Activo As Boolean)
            Me.txtRazaCodi.Enabled = Activo
            cmbRazaCria.Enabled = Activo
            txtCodi.Enabled = Activo
            txtApel.Enabled = Activo
            imgBusc.Disabled = Not Activo And (Not PermiModi Or Valor Is DBNull.Value)  'si est� inactivo, permite entrar solo a modicar
            imgBuscAvan.Disabled = Not Activo
            imgLimp.Disabled = Not Activo
            Me.Panel1.UpdateAfterCallBack = True
        End Set
    End Property

    Public Property SoloBusq() As Boolean
        Get
            Return mbooSoloBusq
        End Get

        Set(ByVal SoloBusq As Boolean)
            mbooSoloBusq = SoloBusq
        End Set
    End Property

    Public Property PermiModi() As Boolean
        Get
            Return mbooPermiModi
        End Get

        Set(ByVal PermiModi As Boolean)
            mbooPermiModi = PermiModi
        End Set
    End Property

    Public Property Ancho() As Integer
        Get
            Return mintAncho
        End Get

        Set(ByVal Ancho As Integer)
            mintAncho = Ancho
        End Set
    End Property

    Public Property Alto() As Integer
        Get
            Return mintAlto
        End Get

        Set(ByVal Alto As Integer)
            mintAlto = Alto
        End Set
    End Property

    Public Property Saltos() As String
        Get
            Return mstrSaltos
        End Get

        Set(ByVal Value As String)
            mstrSaltos = Value
        End Set
    End Property

    Public ReadOnly Property Apel() As String
        Get
            Return (txtApel.Text)
        End Get
    End Property

    Public ReadOnly Property Codi() As String
        Get
            Return (txtCodi.Text)
        End Get
    End Property
    Public Property RazaCodi() As String
        Get
            Return (txtRazaCodi.Text)
        End Get

        Set(ByVal Value As String)
            txtRazaCodi.Text = Value
        End Set
    End Property

    Public ReadOnly Property ClieId(Optional ByVal pstrConn As String = "") As Object
        Get
            Dim oRet As Object
            Dim lDsDatos As DataSet

            If pstrConn <> "" Then mstrConn = pstrConn

            oRet = DBNull.Value

            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, Tabla, Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    Select Case Tabla.ToLower
                        Case "alumnos"
                            oRet = lDsDatos.Tables(0).Rows(0).Item("alum_clie_id")
                        Case "socios"
                            oRet = lDsDatos.Tables(0).Rows(0).Item("soci_clie_id")
                        Case "criadores"
                            oRet = lDsDatos.Tables(0).Rows(0).Item("cria_clie_id")
                    End Select
                End If
            End If

            If Not AceptaNull And oRet Is DBNull.Value Then
                oRet = "0"
            End If

            Return (oRet)
        End Get
    End Property

    Public ReadOnly Property TipoIVAId() As Object
        Get
            Dim oRet As Object
            Dim lDsDatos As DataSet

            oRet = DBNull.Value
            ' tipo IVA del cliente

            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    oRet = lDsDatos.Tables(0).Rows(0).Item("clie_ivap_id")

                End If
            End If

            If Not AceptaNull And oRet Is DBNull.Value Then
                oRet = "0"
            End If

            Return (oRet)
        End Get
    End Property
    Public ReadOnly Property SociId(Optional ByVal pstrConn As String = "") As Object
        Get
            Dim oRet As Object
            Dim lDsDatos As DataSet
            If pstrConn <> "" Then mstrConn = pstrConn
            oRet = DBNull.Value
            'clientes_soci_consul
            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, Tabla + "_soci", Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    oRet = lDsDatos.Tables(0).Rows(0).Item("soci_id")

                End If
            End If

            If Not AceptaNull And oRet Is DBNull.Value Then
                oRet = "0"
            End If

            Return (oRet)
        End Get
    End Property
    Public ReadOnly Property SociNro() As Object
        Get
            Dim oRet As Object
            Dim lDsDatos As DataSet

            oRet = DBNull.Value
            'clientes_soci_consul
            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, Tabla + "_soci", Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    oRet = lDsDatos.Tables(0).Rows(0).Item("soci_nume")

                End If
            End If

            If Not AceptaNull And oRet Is DBNull.Value Then
                oRet = "0"
            End If

            Return (oRet)
        End Get
    End Property
    Public ReadOnly Property DiscrimIVA() As Boolean
        Get
            Dim oRet As Boolean
            Dim lDsDatos As DataSet

            oRet = False
            'clientes_posic_iva_consul
            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "clientes_posic_iva", Valor.ToString)
                oRet = lDsDatos.Tables(0).Rows(0).Item("ivap_discri")
            End If
            Return (oRet)
        End Get
    End Property
    Public ReadOnly Property SobretasaIVA() As Boolean
        Get
            'para inscriptos no responsables 
            'representa al 50% del total de iva normal
            Dim oRet As Boolean
            Dim lDsDatos As DataSet

            oRet = False
            'clientes_posic_iva_consul
            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "clientes_posic_iva", Valor.ToString)
                oRet = lDsDatos.Tables(0).Rows(0).Item("ivap_sobre")
            End If
            Return (oRet)
        End Get
    End Property

    Public ReadOnly Property Exento() As Boolean
        Get
            Dim oRet As Boolean
            Dim lDsDatos As DataSet

            oRet = False
            'clientes_posic_iva_consul
            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    oRet = IIf(lDsDatos.Tables(0).Rows(0).IsNull("clie_fact_exen"), 0, lDsDatos.Tables(0).Rows(0).Item("clie_fact_exen"))
                End If
            End If

            Return (oRet)
        End Get
    End Property

    Public ReadOnly Property ClienteGenerico(Optional ByVal pstrConn As String = "") As Boolean
        Get
            Dim oRet As Boolean
            Dim lDsDatos As DataSet
            If pstrConn <> "" Then mstrConn = pstrConn
            oRet = False
            'clientes_consul
            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    oRet = IIf(lDsDatos.Tables(0).Rows(0).Item("clie_gene") Is DBNull.Value, False, lDsDatos.Tables(0).Rows(0).Item("clie_gene"))
                End If
            End If

            Return (oRet)
        End Get
    End Property

#Region "Propiedades para seteo de columnas"
    Public Property ColFanta() As Boolean
        Get
            Return mbooColFanta
        End Get

        Set(ByVal Value As Boolean)
            mbooColFanta = Value
        End Set
    End Property
    Public Property ColNomb() As Boolean
        Get
            Return mbooColNomb
        End Get
        Set(ByVal Value As Boolean)
            mbooColNomb = Value
        End Set
    End Property
    Public Property ColClieNume() As Boolean
        Get
            Return mbooColClieNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColClieNume = Value
        End Set
    End Property
    Public Property ColSociNume() As Boolean
        Get
            Return mbooColSociNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColSociNume = Value
        End Set
    End Property
    Public Property ColLegaNume() As Boolean
        Get
            Return mbooColLegaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColLegaNume = Value
        End Set
    End Property
    Public Property ColCriaNume() As Boolean
        Get
            Return mbooColCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColCriaNume = Value
        End Set
    End Property
    Public Property ColNoCriaNume() As Boolean
        Get
            Return mbooColNoCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColNoCriaNume = Value
        End Set
    End Property
    Public Property ColCUIT() As Boolean
        Get
            Return mbooColCUIT
        End Get
        Set(ByVal Value As Boolean)
            mbooColCUIT = Value
        End Set
    End Property
    Public Property ColDocuNume() As Boolean
        Get
            Return mbooColDocuNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColDocuNume = Value
        End Set
    End Property
    Public Property ColEnti() As Boolean
        Get
            Return mbooColEnti
        End Get
        Set(ByVal Value As Boolean)
            mbooColEnti = Value
        End Set
    End Property
    Public Property ColMedioPago() As Boolean
        Get
            Return mbooColMedioPago
        End Get
        Set(ByVal Value As Boolean)
            mbooColMedioPago = Value
        End Set
    End Property

    Public Property ColEmpr() As Boolean
        Get
            Return mbooColEmpr
        End Get
        Set(ByVal Value As Boolean)
            mbooColEmpr = Value
        End Set
    End Property
    Public Property ColClaveUnica() As Boolean
        Get
            Return mbooColClaveUnica

        End Get
        Set(ByVal Value As Boolean)
            mbooColClaveUnica = Value
        End Set
    End Property
#End Region

#Region "Propiedades para seteo de filtros"
    Public Property FilFanta() As Boolean
        Get
            Return mbooFilFanta
        End Get

        Set(ByVal Value As Boolean)
            mbooFilFanta = Value
        End Set
    End Property
    Public Property FilNomb() As Boolean
        Get
            Return mbooFilNomb
        End Get
        Set(ByVal Value As Boolean)
            mbooFilNomb = Value
        End Set
    End Property
    Public Property FilClieNume() As Boolean
        Get
            Return mbooFilClieNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilClieNume = Value
        End Set
    End Property
    Public Property FilSociNume() As Boolean
        Get
            Return mbooFilSociNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilSociNume = Value
        End Set
    End Property
    Public Property FilExpo() As Boolean
        Get
            Return mbooFilExpo
        End Get
        Set(ByVal Value As Boolean)
            mbooFilExpo = Value
        End Set
    End Property
    Public Property FilLegaNume() As Boolean
        Get
            Return mbooFilLegaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilLegaNume = Value
        End Set
    End Property
    Public Property FilCriaNume() As Boolean
        Get
            Return mbooFilCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilCriaNume = Value
        End Set
    End Property
    Public Property FilNoCriaNume() As Boolean
        Get
            Return mbooFilNoCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilNoCriaNume = Value
        End Set
    End Property
    Public Property FilCUIT() As Boolean
        Get
            Return mbooFilCUIT
        End Get
        Set(ByVal Value As Boolean)
            mbooFilCUIT = Value
        End Set
    End Property
    Public Property FilDocuNume() As Boolean
        Get
            Return mbooFilDocuNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilDocuNume = Value
        End Set
    End Property
    Public Property FilTarjNume() As Boolean
        Get
            Return mbooFilTarjNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilTarjNume = Value
        End Set
    End Property
    Public Property FilEnti() As Boolean
        Get
            Return mbooFilEnti
        End Get
        Set(ByVal Value As Boolean)
            mbooFilEnti = Value
        End Set
    End Property
    Public Property FilMedioPago() As Boolean
        Get
            Return mbooFilMedioPago
        End Get
        Set(ByVal Value As Boolean)
            mbooFilMedioPago = Value
        End Set
    End Property
    Public Property FilAgru() As Boolean
        Get
            Return mbooFilAgru
        End Get
        Set(ByVal Value As Boolean)
            mbooFilAgru = Value
        End Set
    End Property
    Public Property FilEmpr() As Boolean
        Get
            Return mbooFilEmpr
        End Get
        Set(ByVal Value As Boolean)
            mbooFilEmpr = Value
        End Set
    End Property
    Public Property FilClaveUnica() As Boolean
        Get
            Return mbooFilClaveUnica

        End Get
        Set(ByVal Value As Boolean)
            mbooFilClaveUnica = Value
        End Set
    End Property

    Public Property FilTipo() As String
        Get
            Return mstrTipo
        End Get
        Set(ByVal Value As String)
            mstrTipo = Value
        End Set
    End Property
    Public Property FilAutoSelect() As String
        Get
            Return mstrAutoSelect
        End Get
        Set(ByVal Value As String)
            mstrAutoSelect = Value
        End Set
    End Property

    Public Property InclBloq() As String
        Get
            Return mstrInclBloq
        End Get
        Set(ByVal Value As String)
            mstrInclBloq = Value
        End Set
    End Property

    Public Property FilInseId() As String
        Get
            Return mstrInseId
        End Get
        Set(ByVal Value As String)
            mstrInseId = Value
        End Set
    End Property

    Public Property CateTitu() As String
        Get
            Return mstrCateTitu
        End Get
        Set(ByVal Value As String)
            mstrCateTitu = Value
        End Set
    End Property

    Public Property CatePode() As String
        Get
            Return mstrCatePode
        End Get
        Set(ByVal Value As String)
            mstrCatePode = Value
        End Set
    End Property

    Public Property EstaId() As String
        Get
            Return mstrEstaId
        End Get
        Set(ByVal Value As String)
            mstrEstaId = Value
        End Set
    End Property

    Public ReadOnly Property txtIdExt() As NixorControls.TextBoxTab
        Get
            Return txtId
        End Get
    End Property

    Public ReadOnly Property cmbCriaRazaExt() As DropDownList
        Get
            Return (cmbRazaCria)
        End Get
    End Property

    Public ReadOnly Property txtCodiExt() As NixorControls.NumberBox
        Get
            Return txtCodi
        End Get
    End Property

    Public ReadOnly Property txtApelExt() As NixorControls.TextBoxTab
        Get
            Return txtApel
        End Get
    End Property
#End Region
#End Region

#Region "Metodos publicos"
    Public Sub Limpiar()
        Valor = DBNull.Value
        mstrRazaId = ""
        cmbRazaCria.SelectedValue = ""
        Me.txtCodi.Text = ""
        SRA_Neg.Utiles.gSetearRaza(cmbRazaCria, Me.txtRazaCodi, mstrConn)
        Me.Panel1.UpdateAfterCallBack = True
    End Sub
    Public Sub mCargarRazas()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar " & mstrFiltroRazas, cmbRazaCria, "id", "descrip", "T")
    End Sub
#End Region

#Region "Metodos que ejecutan el RaiseEvent RazaChange(sender)"
    Private Sub txtRazaCodi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRazaCodi.TextChanged
        If (txtRazaCodi.Text <> "") Then
            Dim idRaza As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "RAZAS", "raza_id", " @raza_codi=" & txtRazaCodi.Text)

            If (idRaza = "") Then
                SRA_Neg.Utiles.gSetearRaza(cmbRazaCria, Me.txtRazaCodi, mstrConn)
            Else
                cmbRazaCria.SelectedValue = idRaza
            End If

            If (txtVieneDeCriadores.Text = "si") Then
                txtVieneDeCriadores.Text = ""
            Else
                Me.txtApel.Text = ""
                Me.txtCodi.Text = ""

                Me.Panel1.UpdateAfterCallBack = True

                Me.RazaId = idRaza

                RaiseEvent RazaChange(Me.cmbRazaCria)
            End If
        Else
            Me.txtApel.Text = ""
            Me.txtCodi.Text = ""
            cmbRazaCria.SelectedValue = ""
            Me.Panel1.UpdateAfterCallBack = True
        End If
    End Sub

    Private Sub cmbRazaCria_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRazaCria.SelectedIndexChanged
        If (Me.cmbRazaCria.SelectedValue <> "") Then
            Dim codiTmp = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "RAZAS", "raza_codi", " @raza_id=" & Me.cmbRazaCria.SelectedValue)

            If (codiTmp <> Me.txtRazaCodi.Text) Then
                Me.txtRazaCodi.Text = codiTmp

                If (txtVieneDeCriadores.Text = "si") Then
                    txtVieneDeCriadores.Text = ""
                Else
                    Me.txtApel.Text = ""
                    Me.txtCodi.Text = ""
                    Me.RazaId = Me.cmbRazaCria.SelectedValue

                    Me.Panel1.UpdateAfterCallBack = True

                    RaiseEvent RazaChange(Me.cmbRazaCria)
                End If
            End If
        Else
            Me.txtRazaCodi.Text = ""
            Me.txtApel.Text = ""
            Me.txtCodi.Text = ""
            Me.Panel1.UpdateAfterCallBack = True
        End If
    End Sub

    Private Sub txtCodi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodi.TextChanged
        RaiseEvent RazaChange(Me.cmbRazaCria)
    End Sub
#End Region
End Class
End Namespace
