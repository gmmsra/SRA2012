<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Productos.aspx.vb" Inherits="Productos" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="~/controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="~/controles/usrProdDeriv.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Productos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="~/stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="~/includes/utiles.js"></script>
		<script language="JavaScript" src="~/includes/paneles.js"></script>
		<script language="JavaScript">
		var gstrCria = '';
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
				
		function btnTramites_click()
		{
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=TRAMITES&tabla=productos_tramites&filtros=" + document.all("hdnId").value, 1, "600","400","100","50");
		}
		function btnServicios_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=SERVICIOS&tabla=productos_servicios&filtros=" + document.all("hdnId").value, 2, "600","400","100","50");
		}
		function btnParejas_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PAREJAS&tabla=productos_parejas&filtros=" + document.all("hdnId").value, 3, "700","400","50","50");
		}
		function btnPremios_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PREMIOS&tabla=productos_premios&filtros=" + document.all("hdnId").value, 4, "600","400","100","50");
		}
		function btnEstados_click()
		{
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=ESTADOS&tabla=productos_estados&filtros=" + document.all("hdnId").value, 5, "600","400","100","50");
		}
		function btnPerform_click()
		{
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PERFORMANCE&tabla=productos_performance&filtros=" + document.all("hdnId").value, 6, "600","400","100","50");
		}
		function btnPedigree_click()
		{
			gAbrirVentanas("consulta_arbol.aspx?titulo=PEDIGREE&tabla=productos_pedigree&filtros=" + document.all("hdnId").value, 7, "700","400","50","50");
		}
																
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 				else
	 					document.all('lblNumeFil').innerHTML = 'Nro.:';
	 			}
 			}
		}
		function mAsociaciones()
	   	{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Asociaciones&tabla=productos_numeros&filtros='"+ document.all("hdnId").value +"'", 7, "600","300","50","100");
		}
		function mPropietarios()
	   	{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Propietarios&tabla=productos_prop&filtros='"+ document.all("hdnId").value +"'", 7, "600","300","50","100");
		}
		function mRelaciones()
	   	{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Relaciones&tabla=productos_relaciones&filtros='"+ document.all("hdnId").value +"'", 7, "600","300","50","100");
		}
	    function expandir()
		{
			parent.frames("menu").CambiarExp();
		}
		function mCargarPelajes(pRaza)
		{
		    var sFiltro = pRaza.value;
		    mNumeDenoConsul(pRaza,false);
		    
		    if (sFiltro!='')
		    {
				LoadComboXML("rg_registros_tipos", sFiltro, "cmbRegiTipo", "N");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",1", "cmbPelaA", "S");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",2", "cmbPelaB", "S");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",3", "cmbPelaC", "S");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",4", "cmbPelaD", "S");
			}
			else
			{
				document.all('cmbRegiTipo').selectedIndex = 0;
				document.all('cmbPelaA').selectedIndex = 0;
				document.all('cmbPelaB').selectedIndex = 0;
				document.all('cmbPelaC').selectedIndex = 0;
				document.all('cmbPelaD').selectedIndex = 0;
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="~/imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="~/imagenes/recsup.jpg"><IMG height="10" src="~/imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="~/imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="~/imagenes/reciz.jpg"><IMG height="10" src="~/imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Productos</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" width="99%" BorderWidth="1px" BorderStyle="none">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="94%" border="0">
												<TR>
													<TD colSpan="4" style="height: 74px">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="4">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="2" background="imagenes/formiz.jpg" colSpan="3"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg" height="6"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="imagenes/formfdofields.jpg"
																				height="6"></TD>
																			<TD style="WIDTH: 88%" background="imagenes/formfdofields.jpg" height="6"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo"> Nro.:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCriaFil" runat="server" cssclass="titulo">Criador:&nbsp;</asp:Label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
																					MuestraDesc="False" FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="Criadores"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 6px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																			<TD noWrap background="imagenes/formfdofields.jpg">
																				<TABLE cellSpacing="0" cellPadding="0" border="0">
																					<TR>
																						<TD>
																							<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="110px"
																								AceptaNull="False" filtra="true" MostrarBotones="False" NomOper="razas_cargar" Height="20px"
																								onchange="javascript:mNumeDenoConsul(this,true);"></cc1:combobox></TD>
																						<TD width="10"></TD>
																						<TD>
																							<asp:Label id="lblSexoFil" runat="server" cssclass="titulo">&nbsp;&nbsp;Sexo:&nbsp;</asp:Label></TD>
																						<TD width="4"></TD>
																						<TD>
																							<cc1:combobox class="combo" id="cmbSexoFil" runat="server" Width="113px" AceptaNull="False"><asp:ListItem Selected="True" value="">(Todos)</asp:ListItem><asp:ListItem Value="0">Hembra</asp:ListItem><asp:ListItem Value="1">Macho</asp:ListItem></cc1:combobox></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" height="24"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="imagenes/formfdofields.jpg"
																				height="24">
																				<asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" height="24">
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" height="24"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="imagenes/formfdofields.jpg"
																				height="24">
																				<asp:label id="lblApodo" runat="server" cssclass="titulo">Apodo:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" height="24">
																				<CC1:TEXTBOXTAB id="txtApodoFil" runat="server" cssclass="cuadrotexto" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<asp:checkbox id="chkBusc" Text="Buscar en..." CssClass="titulo" Runat="server"></asp:checkbox>&nbsp;&nbsp;&nbsp;
																				<asp:checkbox id="chkBaja" Text="Incluir Dados de Baja" CssClass="titulo" Runat="server"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" style="height: 45px"></TD>
																			<TD style="WIDTH: 181px; height: 45px;" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblNaciFechaFil" runat="server" cssclass="titulo">Fecha Nacimiento:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg" style="height: 45px">
																				<cc1:DateBox id="txtNaciFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtNaciFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblRPNumeFil" runat="server" cssclass="titulo">Nro. RP:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtRPNumeFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" vAlign="top" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblMadreFil" runat="server" cssclass="titulo"><br>Madre:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<UC1:PROD id="usrMadreFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
																					MuestraDesc="True" FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="productos"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" vAlign="top" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblPadreFil" runat="server" cssclass="titulo"><br>Padre:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
																					MuestraDesc="True" FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="productos"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" vAlign="top" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblReceFil" runat="server" cssclass="titulo"><br>Receptora:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<UC1:PROD id="usrReceFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
																					MuestraDesc="True" FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="productos"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="~/imagenes/formfdofields.jpg">
																				<asp:label id="lblPropFil" runat="server" cssclass="titulo">Propietario:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrPropFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
																					MuestraDesc="False" FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="Clientes"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="~/imagenes/formfdofields.jpg">
																				<asp:label id="lblAsoNumeFil" runat="server" cssclass="titulo">Asociaci�n:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<P>
																					<cc1:combobox class="combo" id="cmbAsocFil" runat="server" Width="200px" AceptaNull="False"></cc1:combobox>
																					<CC1:TEXTBOXTAB id="txtAsoNumeFil" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"></CC1:TEXTBOXTAB></P>
																			</TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="~/imagenes/formfdofields.jpg">
																				<asp:label id="lblCtroFil" runat="server" cssclass="titulo" Visible="False">Centro de Transplante:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbCtroFil" runat="server" Width="200px" AceptaNull="False" Visible="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="~/imagenes/formfdofields.jpg">
																				<asp:label id="lblCtrolFactuNumeFil" runat="server" cssclass="titulo" Visible="False">Nro. Ctrol Fact.:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtCtrolFactuNumeFil" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"
																					Visible="False" esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD background="~/imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 181px" noWrap align="right" background="~/imagenes/formfdofields.jpg">
																				<asp:label id="lblLaboNumeFil" runat="server" cssclass="titulo">Nro. Tr�mite Laboratorio:&nbsp;</asp:label></TD>
																			<TD background="~/imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtLaboNumeFil" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"
																					esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" background="~/imagenes/formfdofields.jpg" height="4"></TD>
																			<TD style="WIDTH: 19.08%" noWrap align="right" background="~/imagenes/formfdofields.jpg"
																				height="8"></TD>
																			<TD style="WIDTH: 88%" background="~/imagenes/formfdofields.jpg" height="4"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="~/imagenes/formdivfin.jpg" height="2"><IMG height="2" src="~/imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 19.08%" align="right" background="~/imagenes/formdivfin.jpg" height="2"><IMG height="2" src="~/imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="~/imagenes/formdivfin.jpg" height="2"><IMG height="2" src="~/imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="~/imagenes/formde.jpg"><IMG height="2" src="~/imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="right" colSpan="2"><br>
										<asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnEditCommand="mSeleccionarProducto"
											OnPageIndexChanged="DataGrid_Page" OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False"
											CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton3" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
															<img src='~/images/sele.gif' border="0" alt="Seleccionar Producto" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='~/images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="prdt_id" ReadOnly="True" HeaderText="Id Producto"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_raza" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_sexo" ReadOnly="True" HeaderText="Sexo"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_nomb" ReadOnly="True" HeaderText="Nombre"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_asoc" ReadOnly="True" HeaderText="Asociaci�n"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_nume" ReadOnly="True" HeaderText="Nro."></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_desc" ReadOnly="True" HeaderText="Descripci�n"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_rp" ReadOnly="True" HeaderText="Nro. RP"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_criador" ReadOnly="True" HeaderText="Criador"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="prdt_naci_fecha" ReadOnly="True" HeaderText="F. Nacim."></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="prdt_raza_id" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_raza_codi" HeaderText="Raza"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle" align="right">
										<TABLE id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<TR>
												<TD width="1"><IMG height="28" src="~/imagenes/tab_a.bmp" width="9" border="0"></TD>
												<TD background="~/imagenes/tab_b.bmp"><IMG height="28" src="~/imagenes/tab_b.bmp" width="8" border="0"></TD>
												<TD width="1"><IMG height="28" src="~/imagenes/tab_c.bmp" width="31" border="0"></TD>
												<TD width="1" background="~/imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Producto </asp:linkbutton></TD>
												<TD width="1" background="~/imagenes/tab_fondo.bmp"></TD>
												<TD width="27"><IMG height="28" src="~/imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="~/imagenes/tab_fondo.bmp"></TD>
												<TD width="1" background="~/imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkAna" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" CausesValidation="False" Height="21px"> An�lisis </asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="~/imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
											</TR>
										</TABLE>
									</TD>
									<TD align="right"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderWidth="1px" BorderStyle="Solid"
												Visible="False" Height="116px">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="~/images\Close.bmp" CausesValidation="False"
																ToolTip="Cerrar"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" align="center" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="232px" Obligatorio="true"
																				AceptaNull="True" filtra="true" MostrarBotones="False" NomOper="razas_cargar" Height="20px"
																				onchange="javascript:mCargarPelajes(this);"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" vAlign="middle" align="right">
																			<asp:Label id="lblSraNume" runat="server" cssclass="titulo">Nro.:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:numberbox id="txtSraNume" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="false"
																				esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" vAlign="middle" align="right">
																			<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="365px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" vAlign="middle" align="right">
																			<asp:Label id="lblApod" runat="server" cssclass="titulo">Apodo:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtApod" runat="server" cssclass="cuadrotexto" Width="365px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" vAlign="middle" align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">Nro.RP:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtRP" runat="server" cssclass="cuadrotexto" Width="170px" Obligatorio="true"></CC1:TEXTBOXTAB>&nbsp;
																			<cc1:numberbox id="txtRPNume" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="true"
																				esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbSexo" runat="server" Width="113px" Obligatorio="True" AceptaNull="true"><asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem><asp:ListItem Value="0">Hembra</asp:ListItem><asp:ListItem Value="1">Macho</asp:ListItem></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px; HEIGHT: 15px" align="right">
																			<asp:Label id="lblPelaA" runat="server" cssclass="titulo">Color:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 15px">
																			<cc1:combobox class="combo" id="cmbPelaA" runat="server" Width="200px" AceptaNull="True"></cc1:combobox>&nbsp;&nbsp;&nbsp;
																			<asp:Label id="lblPelaB" runat="server" cssclass="titulo">Cabeza:&nbsp;</asp:Label>
																			<cc1:combobox class="combo" id="cmbPelaB" runat="server" Width="200px" AceptaNull="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblPelaC" runat="server" cssclass="titulo">Cuerpo:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbPelaC" runat="server" Width="200px" AceptaNull="True"></cc1:combobox>&nbsp;&nbsp;&nbsp;
																			<asp:Label id="lblPelaD" runat="server" cssclass="titulo">Miembros:&nbsp;</asp:Label>
																			<cc1:combobox class="combo" id="cmbPelaD" runat="server" Width="200px" AceptaNull="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right"></TD>
																		<TD>
																			<asp:Label id="lblSiete" runat="server" cssclass="titulo">Sietemesino:&nbsp;</asp:Label>
																			<cc1:numberbox id="txtSiete" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
																				MaxValor="9" MaxLength="1" CantMax="1"></cc1:numberbox>
																			<asp:Label id="lblMelli" runat="server" cssclass="titulo">Mellizo:&nbsp;</asp:Label>
																			<cc1:numberbox id="txtMelli" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
																				MaxValor="99" MaxLength="2" CantMax="2"></cc1:numberbox>
																			<asp:CheckBox id="chkTranEmbr" Text="Transpl. Embrionario" CssClass="titulo" Runat="server"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblDona" runat="server" cssclass="titulo">Nro.Donante:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:numberbox id="txtDona" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="false"
																				esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblPesoNacer" runat="server" cssclass="titulo">Peso al Nacer:&nbsp;</asp:Label></TD>
																		<TD noWrap>
																			<cc1:numberbox id="txtPesoNacer" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="True"
																				esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox>
																			<asp:Label id="lblPesoDeste" runat="server" cssclass="titulo">Peso al Dest.:&nbsp;</asp:Label>
																			<cc1:numberbox id="txtPesoDeste" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="True"
																				esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox>
																			<asp:Label id="lblPesoFinal" runat="server" cssclass="titulo">Peso Final:&nbsp;</asp:Label>
																			<cc1:numberbox id="txtPesoFinal" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="True"
																				esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblEpdNacer" runat="server" cssclass="titulo">DEP al Nacer:&nbsp;</asp:Label></TD>
																		<TD noWrap>
																			<cc1:numberbox id="txtEpdNacer" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="True"
																				esdecimal="True" MaxValor="9999999999999" MaxLength="12" CantMax="3"></cc1:numberbox>
																			<asp:Label id="lblEpdDeste" runat="server" cssclass="titulo">DEP al Dest.:&nbsp;</asp:Label>
																			<cc1:numberbox id="txtEpdDeste" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="True"
																				esdecimal="True" MaxValor="9999999999999" MaxLength="12" CantMax="3"></cc1:numberbox>
																			<asp:Label id="lblEpdFinal" runat="server" cssclass="titulo">DEP Final:&nbsp;</asp:Label>
																			<cc1:numberbox id="txtEpdFinal" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="True"
																				esdecimal="True" MaxValor="9999999999999" MaxLength="12" CantMax="3"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:&nbsp;</asp:Label></TD>
																		<TD>
																			<UC1:CLIE id="usrCria" runat="server" Obligatorio="True" AceptaNull="true" Ancho="800" FilDocuNume="True"
																				MuestraDesc="False" FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="Criadores"
																				CampoVal="Criador"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblRegiTipo" runat="server" cssclass="titulo">Tipo de Registro:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbRegiTipo" runat="server" Width="200px" NomOper="rg_registros_tipos_cargar"></cc1:combobox>&nbsp;&nbsp;&nbsp;
																			<asp:Label id="lblPX" runat="server" cssclass="titulo">PX:&nbsp;</asp:Label>
																			<CC1:TEXTBOXTAB id="txtPX" runat="server" cssclass="cuadrotexto" Width="40px" MaxLength="4"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px; HEIGHT: 3px" noWrap align="right">
																			<asp:Label id="lblInscFecha" runat="server" cssclass="titulo">Fecha Inscripci�n:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 3px" noWrap>
																			<cc1:DateBox id="txtInscFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox>&nbsp;&nbsp; 
																			&nbsp;
																			<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Nacim.:&nbsp;</asp:Label>
																			<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox>&nbsp; 
																			&nbsp;
																			<asp:Label id="lblFalleFecha" runat="server" cssclass="titulo">Fecha Fallecim.:</asp:Label>
																			<cc1:DateBox id="txtFalleFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 161px" align="right">
																			<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD>
																			<asp:Label id="txtEsta" runat="server" cssclass="titulo" Width="100px"></asp:Label>&nbsp;&nbsp;
																			<asp:Label id="lblTranFecha" runat="server" cssclass="titulo">Fecha Transf.:&nbsp;</asp:Label>
																			<asp:Label id="txtTranFecha" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="~/imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="~/imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="30">
																			<asp:Button id="btnProp" runat="server" cssclass="boton" Width="90px" Text="Propietarios" CausesValidation="False"></asp:Button>&nbsp;
																			<asp:Button id="btnRela" runat="server" cssclass="boton" Width="90px" Text="Relaciones" CausesValidation="False"></asp:Button>&nbsp;
																			<asp:Button id="btnAsoc" runat="server" cssclass="boton" Width="90px" Text="Asociaciones" CausesValidation="False"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="30"><BUTTON class="boton" id="btnTramites" style="WIDTH: 75px" onclick="btnTramites_click();"
																				type="button" runat="server" value="Tr�mites">Tr�mites</BUTTON>&nbsp;<BUTTON class="boton" id="btnServicios" style="WIDTH: 75px" onclick="btnServicios_click();"
																				type="button" runat="server" value="Servicios">Servicios</BUTTON>&nbsp;<BUTTON class="boton" id="btnParejas" style="WIDTH: 75px" onclick="btnParejas_click();"
																				type="button" runat="server" value="Parejas">Parejas</BUTTON>&nbsp;<BUTTON class="boton" id="btnPremios" style="WIDTH: 75px" onclick="btnPremios_click();"
																				type="button" runat="server" value="Premios">Premios</BUTTON>&nbsp;<BUTTON class="boton" id="btnEstados" style="WIDTH: 75px" onclick="btnEstados_click();"
																				type="button" runat="server" value="Estados">Estados</BUTTON>&nbsp;<BUTTON class="boton" id="btnPerform" style="WIDTH: 85px" onclick="btnPerform_click();"
																				type="button" runat="server" value="Performance">Performance</BUTTON>&nbsp;<BUTTON class="boton" id="btnPedigree" style="WIDTH: 75px" onclick="btnPedigree_click();"
																				type="button" runat="server" value="Pedigree">Pedigree</BUTTON></TD>
																	</TR>
																</TABLE>
															</asp:panel>&nbsp;&nbsp;</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="center" colSpan="3">
															<asp:panel id="panAna" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="center" colSpan="2">
																			<asp:datagrid id="grdAna" runat="server" BorderStyle="None" BorderWidth="1px" width="95%" Visible="true"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				AutoGenerateColumns="False" OnPageIndexChanged="grdAna_PageChanged" OnEditCommand="mEditarDatosAnalisis">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='~/images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="false" DataField="prta_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prta_fecha" HeaderText="Fecha" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prta_nume" ReadOnly="True" HeaderText="N�mero" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_tipo" ReadOnly="True" HeaderText="Tipo" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_resul" HeaderText="Resultado">
																						<HeaderStyle Width="100%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="prta_resul_fecha" HeaderText="F.Resultado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prta_baja_fecha" HeaderText="Fecha Baja" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnaNume" runat="server" cssclass="titulo">Nro.An�lsis:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtAnaNume" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"
																				Visible="true" esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnaTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:combobox class="combo" id="cmbAnaTipo" runat="server" Width="150px"><asp:listitem value="" selected="true">(Seleccione)</asp:listitem><asp:listitem value="1">ADN</asp:listitem><asp:listitem value="0">Tipificaci�n</asp:listitem></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnaFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtAnaFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnaResul" runat="server" cssclass="titulo">Resultado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:combobox class="combo" id="cmbAnaResul" runat="server" Width="320px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnaFechaResul" runat="server" cssclass="titulo">Fecha Resultado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:DateBox id="txtAnaFechaResul" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="34">
																			<asp:Button id="btnAltaAna" runat="server" cssclass="boton" Width="110px" Text="Agregar An�lisis"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaAna" runat="server" cssclass="boton" Width="110px" Text="Eliminar An�lisis"
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiAna" runat="server" cssclass="boton" Width="110px" Text="Modificar An�lisis"
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpAna" runat="server" cssclass="boton" Width="110px" Text="Limpiar An�lisis"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta" Visible="false"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" Visible="false"
																CausesValidation="False"></asp:Button>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
														</TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="~/imagenes/recde.jpg"><IMG height="10" src="~/imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="~/imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="~/imagenes/recinf.jpg"><IMG height="15" src="~/imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="~/imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnAnaId" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnSRA" runat="server"></asp:textbox>
				<asp:textbox id="hdnEspe" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		if (document.all["usrClieFil_txtCodi"]!= null && !document.all["usrClieFil_txtCodi"].disabled)
			document.all["usrClieFil_txtCodi"].focus();		
		if (document.all('cmbRazaFil')!=null)
			mNumeDenoConsul(document.all('cmbRazaFil'),true);
		if (document.all('cmbRaza')!=null)
			mNumeDenoConsul(document.all('cmbRaza'),false);
		
		gSetearTituloFrame('Productos');

		</SCRIPT>
	</BODY>
</HTML>
