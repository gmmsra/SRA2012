<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="usrClieDeriv.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrProdFiltro" CodeFile="usrProdFiltro.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD noWrap align="right" width="15%" height="2"></TD>
		<TD align="left"></TD>
		<TD align="right" width="15%"></TD>
		<TD align="left"></TD>
	</TR>
	<tr>
		<td colSpan="4">
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<div class="panelediciontransp" id="panProdBuscAvan1" style="WIDTH: 100%">
							<table width="100%" align="center" border="0">
								<TR>
									<TD style="WIDTH: 14.83%; HEIGHT: 10px" align="right"><asp:label id="lblRazaFil" cssclass="titulo" runat="server">Raza:&nbsp;</asp:label></TD>
									<TD colspan="3">
										<TABLE cellSpacing="0" cellPadding="0" border="0">
											<TR>
												<TD><cc1:combobox class="combo" id="cmbRazaFil" cssclass="cuadrotexto" runat="server" filtra="true"
														MostrarBotones="False" NomOper="razas_cargar" Height="20px" onchange="javascript:Combo_change(this);mSetearRazas();mNumeDenoConsul(this,true);"
														AceptaNull="False" Width="250px"></cc1:combobox></TD>
												<TD align="right" width="50"><asp:label id="lblNumeFil" cssclass="titulo" runat="server"> Nro.:&nbsp;</asp:label></TD>
												<TD><CC1:TEXTBOXTAB id="txtNumeFil" cssclass="cuadrotexto" runat="server" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblCriador" cssclass="titulo" runat="server">Criador:&nbsp;</asp:label></TD>
									<TD colspan="3"><UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
											FilDocuNume="True" MuestraDesc="False" FilTipo="T" FilSociNume="True" Saltos="1,2" Tabla="Criadores"
											criador="true"></UC1:CLIE></TD>
								</TR>
								<TR>
									<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:label id="lblSexoFil" cssclass="titulo" runat="server">&nbsp;&nbsp;Sexo:&nbsp;</asp:label></TD>
									<TD><cc1:combobox class="combo" id="cmbSexoFil" runat="server" AceptaNull="True" Width="113px">
											<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
											<asp:ListItem Value="0">Hembra</asp:ListItem>
											<asp:ListItem Value="1">Macho</asp:ListItem>
										</cc1:combobox>&nbsp;<asp:label id="lblRPNumeFil" cssclass="titulo" runat="server">RP:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtRPNumeFil" cssclass="cuadrotexto" runat="server" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
								</TR>
								<TR>
									<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:label id="lblNumeDesdeFil" cssclass="titulo" runat="server">Nro. Desde:&nbsp;</asp:label></TD>
									<TD><CC1:TEXTBOXTAB id="txtNumeDesdeFil" cssclass="cuadrotexto" runat="server" Width="140px" Obligatorio="false"
											MaxLength="8"></CC1:TEXTBOXTAB>&nbsp;<asp:label id="lblNumeHastaFil" cssclass="titulo" runat="server">Nro. Hasta:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtNumeHastaFil" cssclass="cuadrotexto" runat="server" Width="140px" Obligatorio="false"
											MaxLength="8"></CC1:TEXTBOXTAB></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:label id="lblRPDesdeFil" cssclass="titulo" runat="server">RP Desde:&nbsp;</asp:label></TD>
									<TD><CC1:TEXTBOXTAB id="txtRPDesdeFil" cssclass="cuadrotexto" runat="server" Width="140px" Obligatorio="false"
											MaxLength="8"></CC1:TEXTBOXTAB>&nbsp;<asp:label id="lblRPHastaFil" cssclass="titulo" runat="server">RP Hasta:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtRPHastaFil" cssclass="cuadrotexto" runat="server" Width="140px" Obligatorio="false"
											style="Z-INDEX: 0" MaxLength="8"></CC1:TEXTBOXTAB></TD>
								</TR>
								<TR>
									<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:label id="lblNaciFechaFil" cssclass="titulo" runat="server">Fecha Nacimiento:&nbsp;</asp:label></TD>
									<TD><cc1:datebox id="txtNaciFechaDesdeFil" cssclass="cuadrotexto" runat="server" Width="70px"></cc1:datebox>&nbsp;<asp:label id="lblFechaHastaFil" cssclass="titulo" runat="server">Hasta:&nbsp;</asp:label><cc1:datebox id="txtNaciFechaHastaFil" cssclass="cuadrotexto" runat="server" Width="70px"></cc1:datebox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:label id="lblInscFechaFil" cssclass="titulo" runat="server">Fecha Inscripci�n:&nbsp;</asp:label></TD>
									<TD><cc1:datebox id="txtInscFechaDesdeFil" cssclass="cuadrotexto" runat="server" Width="70px"></cc1:datebox>&nbsp;<asp:label id="lblFechaInscHastaFil" cssclass="titulo" runat="server">Hasta:&nbsp;</asp:label><cc1:datebox id="txtInscFechaHastaFil" cssclass="cuadrotexto" runat="server" Width="70px"></cc1:datebox></TD>
								</TR>
								<TR id="trPropiedad" style='VISIBILITY: hidden' runat="server">
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:label id="lblPropFil" cssclass="titulo" runat="server">Propietario:&nbsp;</asp:label></TD>
									<TD colspan="3"><asp:radiobutton id="optClie" runat="server" AutoPostBack="True" Checked="True" GroupName="grpTipo"
											Text="Cliente" CssClass="titulo"></asp:radiobutton><asp:radiobutton id="optCria" runat="server" AutoPostBack="True" GroupName="grpTipo" Text="Expediente"
											CssClass="titulo"></asp:radiobutton>
										<DIV id="panPropClie"><UC1:CLIE id="usrPropClieFil" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
												FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Clientes"></UC1:CLIE></DIV>
										<DIV id="panPropCria"><UC1:CLIE id="usrPropCriaFil" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
												FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Criadores" Visible="False"></UC1:CLIE></DIV>
									</TD>
								</TR>
								<TR id="TrNombre" style='VISIBILITY: hidden' runat="server">
									<TD style="WIDTH: 98px; HEIGHT: 10px" align="right"><asp:label id="lblNombFil" cssclass="titulo" runat="server" Width="16px">Nombre:&nbsp;</asp:label></TD>
									<TD style="WIDTH: 374px"><CC1:TEXTBOXTAB id="txtNombFil" cssclass="cuadrotexto" runat="server" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
								</TR>
								<TR id="TrApodo" style='VISIBILITY: hidden' runat="server">
									<TD style="WIDTH: 98px; HEIGHT: 10px" align="right"><asp:label id="lblApodoFil" cssclass="titulo" runat="server">Apodo:&nbsp;</asp:label></TD>
									<TD style="WIDTH: 374px"><CC1:TEXTBOXTAB id="txtApodoFil" cssclass="cuadrotexto" runat="server" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:Label id="lblHBAPadre" runat="server" cssclass="titulo">HBA Padre:</asp:Label>&nbsp;</TD>
									<TD>
										<CC1:TEXTBOXTAB id="txtHBAPadre" runat="server" cssclass="cuadrotexto"></CC1:TEXTBOXTAB>&nbsp;
										<asp:Label id="lblRPPadre" runat="server" cssclass="titulo" style="Z-INDEX: 0">RP Padre:</asp:Label>
										<CC1:TEXTBOXTAB id="txtRPPadre" runat="server" cssclass="cuadrotexto" Width="170px" style="Z-INDEX: 0"></CC1:TEXTBOXTAB>
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 10px" align="right"><asp:Label id="lblHBAMadre" runat="server" cssclass="titulo">HBA Madre:</asp:Label>&nbsp;</TD>
									<TD>
										<CC1:TEXTBOXTAB id="txtHBAMadre" runat="server" cssclass="cuadrotexto"></CC1:TEXTBOXTAB>&nbsp;
										<asp:Label id="lblRPMadre" runat="server" cssclass="titulo" style="Z-INDEX: 0">RP Madre:</asp:Label>
										<CC1:TEXTBOXTAB id="txtRPMadre" runat="server" cssclass="cuadrotexto" Width="170px" style="Z-INDEX: 0"></CC1:TEXTBOXTAB>
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px; HEIGHT: 13px" align="right"></TD>
									<TD style="HEIGHT: 13px"><asp:checkbox id="chkBusc" Text="Buscar en..." CssClass="titulo" Runat="server"></asp:checkbox>&nbsp;&nbsp;&nbsp;
										<asp:checkbox id="chkBaja" Text="Incluir Dados de Baja" CssClass="titulo" Runat="server"></asp:checkbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 10px" align="right"><asp:label id="lblAsoNumeFil" cssclass="titulo" runat="server">Asociaci�n:&nbsp;</asp:label></TD>
									<TD style="WIDTH: 530px"><cc1:combobox class="combo" id="cmbAsocFil" cssclass="cuadrotexto" runat="server" filtra="True"
											onchange="Combo_change(this)" AceptaNull="False" Width="100%" mostrarbotones="False"></cc1:combobox></TD>
									<td style="WIDTH: 10px"><asp:label id="lblAsocNumeFil" cssclass="titulo" runat="server">Nro:</asp:label></td>
									<td><CC1:TEXTBOXTAB id="txtAsoNumeFil" cssclass="cuadrotexto" runat="server" Width="104px" Obligatorio="false"></CC1:TEXTBOXTAB></td>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblLaboNumeFil" cssclass="titulo" runat="server">Nro Tr�mite Laboratorio:&nbsp;</asp:label></TD>
									<TD><cc1:numberbox id="txtLaboNumeFil" cssclass="cuadrotexto" runat="server" Width="100px" MaxLength="12"
											MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblTramNumeFil" cssclass="titulo" runat="server">N� Tr�mite:&nbsp;</asp:label></TD>
									<TD><cc1:numberbox id="txtTramNumeFil" cssclass="cuadrotexto" runat="server" Width="160px" MaxLength="12"
											MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblCuigFil" cssclass="titulo" runat="server">CUIG:&nbsp;</asp:label></TD>
									<TD><CC1:TEXTBOXTAB id="txtCuigFil" cssclass="cuadrotexto" runat="server" Width="160px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblDonaFil" cssclass="titulo" runat="server" Visible="False">Nro.Donante/Dador:&nbsp;</asp:label></TD>
									<TD><cc1:numberbox id="txtDonaFil" cssclass="cuadrotexto" runat="server" Width="106px" Obligatorio="false"
											MaxLength="12" MaxValor="9999999999999" esdecimal="False" Visible="False"></cc1:numberbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblCtroFil" cssclass="titulo" runat="server" Visible="False">Centro de Transplante:&nbsp;</asp:label></TD>
									<TD><cc1:combobox class="combo" id="cmbCtroFil" runat="server" AceptaNull="False" Width="200px" Visible="False"></cc1:combobox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblCtrolFactuNumeFil" cssclass="titulo" runat="server" Visible="False">N� Ctrol Fact.:&nbsp;</asp:label></TD>
									<TD><cc1:numberbox id="txtCtrolFactuNumeFil" cssclass="cuadrotexto" runat="server" Width="160px" Obligatorio="false"
											MaxLength="12" MaxValor="9999999999999" esdecimal="False" Visible="False"></cc1:numberbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblEstaFil" cssclass="titulo" runat="server">Estado:&nbsp;</asp:label></TD>
									<TD><cc1:combobox class="combo" id="cmbEstaFil" runat="server" AceptaNull="False" Width="140px"></cc1:combobox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 136px" align="right"><asp:label id="lblNaciFil" cssclass="titulo" runat="server">Nacionalidad:&nbsp;</asp:label></TD>
									<TD><cc1:combobox class="combo" id="cmbNaciFil" runat="server" AceptaNull="False" Width="140px">
											<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
											<asp:ListItem Value="I">Importado</asp:ListItem>
											<asp:ListItem Value="E">Extranjero</asp:ListItem>
											<asp:ListItem Value="N">Nacional</asp:ListItem>
										</cc1:combobox></TD>
								</TR>
							</table>
						</div>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
							</TR>
							<TR id="divBusqAvanzada" style='VISIBILITY: hidden' runat="server">
								<TD align="right"><SPAN id="spExp" style="FONT-FAMILY: Verdana; HEIGHT: 4px; COLOR: black; FONT-SIZE: 14px; CURSOR: hand; FONT-WEIGHT: bold"
										onclick="CambiarExp()">+</SPAN></TD>
								<TD width="100%"><asp:label id="lblExp" cssclass="titulo" runat="server">B�squeda avanzada</asp:label></TD>
							</TR>
							<TR id="divBusqAvanzadaDetalle" style='VISIBILITY: hidden' runat="server">
								<TD colSpan="2">
									<DIV class="panelediciontransp" id="panBuscAvan" style="Z-INDEX: 101; WIDTH: 100%; DISPLAY: none">
										<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
											<TR>
												<TD style="HEIGHT: 10px" align="right" colSpan="2"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 10px" align="right"><asp:label id="lblObservacionFil" cssclass="titulo" runat="server">Observaci�n:</asp:label>&nbsp;</TD>
												<TD><CC1:TEXTBOXTAB id="txtObservacionFil" cssclass="cuadrotexto" runat="server" Height="46px" Width="80%"
														EnterPorTAb="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 15%" align="right"><asp:label id="lblPadreFil" cssclass="titulo" runat="server">Padre:</asp:label>&nbsp;</TD>
												<TD><UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
														FilDocuNume="True" MuestraDesc="True" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos"></UC1:PROD></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD align="right"><asp:label id="lblMadreFil" cssclass="titulo" runat="server">Madre:</asp:label>&nbsp;</TD>
												<TD><UC1:PROD id="usrMadreFil" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
														FilDocuNume="True" MuestraDesc="True" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos"></UC1:PROD></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD align="right"><asp:label id="lblReceFil" cssclass="titulo" runat="server" Visible="False">Receptora:</asp:label>&nbsp;</TD>
												<TD><UC1:PROD id="usrReceFil" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
														FilDocuNume="True" MuestraDesc="True" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos"
														Visible="False"></UC1:PROD></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 10px" align="right" colSpan="2"></TD>
											</TR>
										</TABLE>
									</DIV>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
</TABLE>
<script language="JavaScript" src="includes/utiles.js"></script>
<script language="JavaScript">
function CambiarExp()
{
	if(document.all("spExp").innerText=="+")
	{
		document.all("panBuscAvan").style.display = "inline";
		document.all("spExp").innerText="-";
	}
	
	else
	{
		document.all("panBuscAvan").style.display = "none";
		document.all("spExp").innerText="+";
	}

}

		function mSetearRazas()
		{
			if (document.all("txtusrProdFil:cmbRazaFil")!=null && document.all("txtusrProdFil:usrMadreFil:cmbProdRaza")!= null)
			{
				document.all("txtusrProdFil:usrMadreFil:cmbProdRaza").value = document.all("txtusrProdFil:cmbRazaFil").value;
				mBuscarIdXCodi("usrProdFil:usrMadreFil:cmbProdRaza", "razas_cargar", "Id");
				document.all("txtusrProdFil:usrPadreFil:cmbProdRaza").value = document.all("txtusrProdFil:cmbRazaFil").value;
				mBuscarIdXCodi("usrProdFil:usrPadreFil:cmbProdRaza", "razas_cargar", "Id");
			}
		}
function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (pFiltro)
		   	{
 				if (document.all('usrProdFil_lblNumeFil')!=null)
				{
					if (strRet!='')
					{
	 					document.all('usrProdFil_lblNumeFil').innerHTML = strRet+':';
	 					document.all('usrProdFil_lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('usrProdFil_lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					document.all('usrProdFil_lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('usrProdFil_lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('usrProdFil_lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
		}	
		
		 function cmbRazaFil_onchange(pstrNomb)
		{
			if(document.all(pstrNomb)!=null && document.all("txtusrProdFil_usrCriaFil_cmbRazaCria")!=null)
		    {
				document.all("usrProdFil:usrCriaFil:cmbRazaCria").value = document.all(pstrNomb).value;
				document.all("usrProdFil:usrCriaFil:cmbRazaCria").onchange();
				document.all("usrProdFil:usrCriaFil_cmbRazaCria").disabled = !(document.all(pstrNomb).value=="");
				document.all("txtusrProdFil_usrCriaFil_cmbRazaCria").disabled = !(document.all(pstrNomb).value=="");
			}
		}
	   		
</script>
