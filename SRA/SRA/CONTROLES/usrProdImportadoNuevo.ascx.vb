Namespace SRA

Partial Class usrProdImportadoNuevo
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

   Protected WithEvents panImpo As System.Web.UI.WebControls.Panel


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"

    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrTablaAsociaciones As String = SRA_Neg.Constantes.gTab_ProductosNumeros
   Private mstrTablaDocum As String = SRA_Neg.Constantes.gTab_ProductosDocum
    Private mstrTablaRelaciones As String = SRA_Neg.Constantes.gTab_ProductosRelaciones
    Private mstrTablaEmbriones As String = SRA_Neg.Constantes.gTab_EmbrionesStock
    Private mstrTablaTramitesProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
    Private mstrTablaSemen As String = SRA_Neg.Constantes.gTab_SemenStock
    Private mstrTablaTramitesDeta As String = SRA_Neg.Constantes.gTab_Tramites_Deta



   Private mstrConn As String
   Private mstrNdadProd As String = ""
   Private mstrNdadPadres As String = ""
   Private mintPropietario As Integer = 0
   Private mintTramite As Integer

   Private mbooAceptaNull As Boolean = True
   Private mbooAdicionales As Boolean = False
   Private mbooPrefijoRP As Boolean = False
   Private mbooHabilitar As Boolean = True
   Private mbooMostrarNumeroExtranjero As Boolean = True
   Private mbooMostrarFechaNacimiento As Boolean = False
   Private mbooGenerarSRANume As Boolean = False
   Public mbooMostrarPadre As Boolean = False
   Public mbooMostrarMadre As Boolean = False
   Public mbooMostrarPedigree As Boolean = False
   Public mbooMostrarPedigreePadre As Boolean = False
   Public mbooMostrarPedigreeMadre As Boolean = False
   Public mbooMostrarCriador As Boolean = True
   Public mbooMostrarProducto As Boolean = True
    Public mbooMostrarResultado As Boolean = True
    Public mbooMostrarEmbriones As Boolean = False

#End Region

#Region "Propiedades publicas"
   Public Property NdadProd() As String
      Get
         Return mstrNdadProd
      End Get
      Set(ByVal Value As String)
         mstrNdadProd = Value
      End Set
   End Property
   Public Property NdadPadres() As String
      Get
         Return mstrNdadPadres
      End Get
      Set(ByVal Value As String)
         mstrNdadPadres = Value
      End Set
   End Property

   Public ReadOnly Property usrProductoExt() As usrProdDeriv
      Get
         Return (usrProducto)
      End Get
   End Property
   Public ReadOnly Property usrCriaExt() As usrClieDeriv
      Get
         Return (usrCria)
      End Get
   End Property
   Public ReadOnly Property usrPadreExt() As usrProdDeriv
      Get
         Return (usrPadre)
      End Get
   End Property
   Public ReadOnly Property usrMadreExt() As usrProdDeriv
      Get
         Return (usrMadre)
      End Get
   End Property
   Public ReadOnly Property hdnProdIdExt() As System.Web.UI.WebControls.TextBox
      Get
         Return (hdnProdId)
      End Get
   End Property
   Public Property Propietario() As Integer
      Get
         Return mintPropietario
      End Get
      Set(ByVal Value As Integer)
         mintPropietario = Value
      End Set
   End Property
   Public Property Tramite() As Integer
      Get
         Return mintTramite
      End Get
      Set(ByVal Value As Integer)
         mintTramite = Value
      End Set
   End Property
   Public Property AceptaNull() As Boolean
      Get
         Return mbooAceptaNull
      End Get

      Set(ByVal Value As Boolean)
         mbooAceptaNull = Value
      End Set
   End Property
   Public Property Adicionales() As Boolean
      Get
         Return mbooAdicionales
      End Get
      Set(ByVal Value As Boolean)
         mbooAdicionales = Value
      End Set
   End Property
   Public Property PrefijoRP() As Boolean
      Get
         Return mbooPrefijoRP
      End Get
      Set(ByVal Value As Boolean)
         mbooPrefijoRP = Value
      End Set
   End Property
   Public Property MostrarCriador() As Boolean
      Get
         Return mbooMostrarCriador
      End Get
      Set(ByVal Value As Boolean)
         mbooMostrarCriador = Value
      End Set
   End Property
   Public Property MostrarProducto() As Boolean
      Get
         Return mbooMostrarProducto
      End Get
      Set(ByVal Value As Boolean)
         mbooMostrarProducto = Value
      End Set
   End Property
   Public Property MostrarResultado() As Boolean
      Get
         Return mbooMostrarResultado
      End Get
      Set(ByVal Value As Boolean)
         mbooMostrarResultado = Value
      End Set
   End Property
   Public Property MostrarPedigree() As Boolean
      Get
         Return mbooMostrarPedigree
      End Get
      Set(ByVal Value As Boolean)
         mbooMostrarPedigree = Value
      End Set
   End Property
   Public Property MostrarPadre() As Boolean
      Get
         Return mbooMostrarPadre
      End Get
      Set(ByVal Value As Boolean)
         mbooMostrarPadre = Value
      End Set
    End Property
    Public Property MostrarEmbriones() As Boolean
        Get
            Return mbooMostrarEmbriones
        End Get
        Set(ByVal Value As Boolean)
            mbooMostrarEmbriones = Value
        End Set
    End Property
    Public Property MostrarPedigreePadre() As Boolean
        Get
            Return mbooMostrarPedigreePadre
        End Get
        Set(ByVal Value As Boolean)
            mbooMostrarPedigreePadre = Value
        End Set
    End Property
    Public Property MostrarMadre() As Boolean
        Get
            Return mbooMostrarMadre
        End Get
        Set(ByVal Value As Boolean)
            mbooMostrarMadre = Value
        End Set
    End Property
    Public Property MostrarPedigreeMadre() As Boolean
        Get
            Return mbooMostrarPedigreeMadre
        End Get
        Set(ByVal Value As Boolean)
            mbooMostrarPedigreeMadre = Value
        End Set
    End Property
    Public Property MostrarNumeroExtranjero() As Boolean
        Get
            Return mbooMostrarNumeroExtranjero
        End Get
        Set(ByVal Value As Boolean)
            mbooMostrarNumeroExtranjero = Value
        End Set
    End Property
    Public Property MostrarFechaNacimiento() As Boolean
        Get
            Return mbooMostrarFechaNacimiento
        End Get
        Set(ByVal Value As Boolean)
            mbooMostrarFechaNacimiento = Value
        End Set
    End Property
    Public Property GenerarSRANume() As Boolean
        Get
            Return mbooGenerarSRANume
        End Get
        Set(ByVal Value As Boolean)
            mbooGenerarSRANume = Value
        End Set
    End Property
    Public Property Habilitar() As Boolean
        Get
            Return mbooHabilitar
        End Get
        Set(ByVal Value As Boolean)
            mbooHabilitar = Value
        End Set
    End Property
    Public Property Valor() As Object
        Get
            If hdnProdId.Text = "" Then
                If AceptaNull Then
                    Return (DBNull.Value)
                Else
                    Return (0)
                End If
            Else
                Return (hdnProdId.Text)
            End If
        End Get

        Set(ByVal Valor As Object)
            If (Valor Is DBNull.Value) OrElse (Valor.ToString = "") OrElse (Valor.ToString = "0") Then
                Dim lstrSRA As String
                hdnProdId.Text = ""
                hdnPropId.Text = ""
                usrMadre.cmbProdRazaExt.Enabled = True
                usrPadre.cmbProdRazaExt.Enabled = True
                usrPadre.Limpiar()
                usrPadre.Activo = True
                usrMadre.Limpiar()
                usrMadre.Activo = True
                'If mbooGenerarSRANume Then
                '    usrProducto.txtSraNumeExt.Enabled = False
                'Else
                '    usrProducto.txtSraNumeExt.Enabled = True
                'End If

                usrProducto.Sexo = ""
                usrProducto.Limpiar()
                If usrProducto.cmbProdRazaExt.Valor.ToString <> "" Or Not Habilitar Then
                    usrPadre.cmbProdRazaExt.Enabled = False
                    usrMadre.cmbProdRazaExt.Enabled = False
                End If
                txtRPExtr.Text = ""
                txtPX.Text = ""
                usrCria.Limpiar()

            Else
                hdnProdId.Text = Valor

                Dim vsRet As String()
                vsRet = SRA_Neg.Utiles.BuscarProdDeriv(Page.Session("sConn").ToString, hdnProdId.Text & _
                ";productoxSraNume;id;" & mintTramite.ToString & ";" & ";" & ";" & ";").Split("|")

                If vsRet.GetUpperBound(0) > 0 Then
                    usrProducto.Valor = vsRet(0)
                    usrProducto.cmbProdAsocExt.Valor = vsRet(13)
                    usrProducto.txtCodiExt.Valor = vsRet(14)

                    txtPX.Text = vsRet(4)
                    txtRPExtr.Valor = vsRet(6)

                    usrPadre.Valor = vsRet(7)
                    usrPadre.cmbProdAsocExt.Valor = vsRet(16)
                    usrPadre.txtCodiExt.Valor = vsRet(17)
                    'usrPadre.Activo = False

                    usrMadre.Valor = vsRet(8)
                    usrMadre.cmbProdAsocExt.Valor = vsRet(18)
                    usrMadre.txtCodiExt.Valor = vsRet(19)
                    'usrMadre.Activo = False

                    usrCria.Valor = vsRet(9)

                    usrCria.cmbCriaRazaExt.Enabled = False
                    usrPadre.cmbProdRazaExt.Enabled = False
                    usrMadre.cmbProdRazaExt.Enabled = False


                    If mstrConn Is Nothing Then
                        mstrConn = clsWeb.gVerificarConexion(Me.Page)
                    End If

                    hdnPropId.Text = vsRet(15)
                Else
                    hdnPropId.Text = ""
                    txtPX.Text = ""
                    txtRPExtr.Text = ""
                    usrMadre.cmbProdRazaExt.Enabled = True
                    usrPadre.cmbProdRazaExt.Enabled = True
                    usrPadre.Limpiar()
                    usrPadre.Activo = True
                    usrMadre.Limpiar()
                    usrMadre.Activo = True

                    usrProducto.Limpiar()
                    If usrProducto.cmbProdRazaExt.Valor.ToString <> "" Or Not Habilitar Then
                        usrPadre.cmbProdRazaExt.Enabled = False
                        usrMadre.cmbProdRazaExt.Enabled = False
                    End If
                    usrCria.Limpiar()

                End If
            End If
        End Set
    End Property
#End Region

#Region "Operaciones sobre el control"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      mstrConn = clsWeb.gVerificarConexion(Me.Page)

      usrProducto.FilNumExtr = mbooMostrarNumeroExtranjero

      rowProdPedigree.Style.Add("display", IIf(mbooMostrarPedigree, "inline", "none"))
      rowPadrePedigree.Style.Add("display", IIf(mbooMostrarPedigreePadre, "inline", "none"))
      rowMadrePedigree.Style.Add("display", IIf(mbooMostrarPedigreeMadre, "inline", "none"))
      lblFechaNac.Visible = mbooMostrarFechaNacimiento
      txtFechaNac.Visible = mbooMostrarFechaNacimiento

      If (Not Page.IsPostBack) Then
         mSetearMaxLength()
         If mbooAdicionales Then
            rowRPExtPX.Style.Add("display", "inline")
         End If
         usrPadre.IgnogaInexistente = True
         usrMadre.IgnogaInexistente = True
      Else
         txtRPExtr.Enabled = mbooHabilitar
         txtPX.Enabled = mbooHabilitar
            usrProducto.txtSraNumeExt.Enabled = False

         usrCria.Activo = mbooHabilitar
         usrPadre.Activo = mbooHabilitar
         usrMadre.Activo = mbooHabilitar
      End If
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtRPExtr.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_rp_extr")
      txtPX.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_px")



    End Sub

#End Region

#Region "Opciones de ABM"
   Public Function GuardarDatos() As DataSet
        Dim ldsDatos As New DataSet
      Dim ldrProd As DataRow

        'ldsDatos = mCrearDataSet(usrProducto.Valor.ToString)



        mCrearDataSet(usrProducto.Valor.ToString, "productosUnico", mstrTabla, ldsDatos, True)
        mCrearDataSet(usrProducto.Valor.ToString, mstrTablaAsociaciones, mstrTablaAsociaciones, ldsDatos, True)
        mCrearDataSet(usrProducto.Valor.ToString, mstrTablaDocum, mstrTablaDocum, ldsDatos, True)
        mCrearDataSet(usrProducto.Valor.ToString, mstrTablaRelaciones, mstrTablaRelaciones, ldsDatos, True)
        mCrearDataSet(usrProducto.Valor.ToString, mstrTablaEmbriones, mstrTablaEmbriones, ldsDatos, True)
        mCrearDataSet(usrProducto.Valor.ToString, mstrTablaTramitesProductos, mstrTablaTramitesProductos, ldsDatos, True)
        mCrearDataSet(usrProducto.Valor.ToString, mstrTablaSemen, mstrTablaSemen, ldsDatos, True)
        mCrearDataSet(usrProducto.Valor.ToString, mstrTablaTramitesDeta, mstrTablaTramitesDeta, ldsDatos, True)



      If mbooMostrarProducto Then
         If ldsDatos.Tables(mstrTabla).Select.GetUpperBound(0) = -1 Then
            ldrProd = ldsDatos.Tables(mstrTabla).NewRow
            ldrProd.Table.Rows.Add(ldrProd)
         Else
            ldrProd = ldsDatos.Tables(mstrTabla).Select()(0)
         End If

         With ldrProd
            .Item("prdt_id") = clsSQLServer.gFormatArg(hdnProdId.Text, SqlDbType.Int)
            .Item("prdt_raza_id") = usrProducto.cmbProdRazaExt.Valor
            .Item("prdt_sexo") = usrProducto.cmbSexoProdExt.ValorBool
                .Item("prdt_nomb") = usrProducto.txtProdNombExt.Valor
            .Item("prdt_px") = txtPX.Text
            .Item("prdt_naci_fecha") = txtFechaNac.Fecha
                '.Item("prdt_rp_nume") = IIf(IsNumeric(Trim(usrProducto.txtRPExt.Text)), Trim(usrProducto.txtRPExt.Text), DBNull.Value)
            .Item("prdt_rp") = IIf(mintTramite = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen, IIf(Trim(usrProducto.txtRPExt.Text) = "", DBNull.Value, IIf(usrProducto.txtRPExt.Text.IndexOf("SI") = 0, "", "SI ") & Trim(usrProducto.txtRPExt.Text)), Trim(usrProducto.txtRPExt.Text))
            .Item("prdt_rp_extr") = txtRPExtr.Valor
                .Item("prdt_ndad") = NdadProd
                .Item("prdt_ori_asoc_id") = usrProducto.cmbProdAsocExt.Valor

                .Item("prdt_ori_asoc_nume") = usrProducto.txtCodiExt.Valor

                .Item("prdt_prop_clie_id") = IIf(mintPropietario > 0, mintPropietario, .Item("prdt_prop_clie_id"))
                .Item("generar_numero") = IIf(mbooGenerarSRANume, True, False)
                'If mintPropietario > 0 Then
                '.Item("prdt_cria_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "criadores", "@cria_clie_id = " & mintPropietario.ToString & ",@cria_raza_id = " & .Item("prdt_raza_id"), "cria_id")
                'End If
                '   .Item("prdt_cria_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "criadores", "@cria_nume = " & usrProducto.txtCodiExt.Valor & ",@cria_raza_id = " & .Item("prdt_raza_id"), "cria_id")
            End With


            If mbooMostrarNumeroExtranjero Then
                mGuardarAsociaciones(ldsDatos, ldrProd.Item("prdt_id").ToString, usrProducto)
            End If
        End If

        If mbooMostrarPadre Then mGuardarRelacionados(ldsDatos, ldrProd, _
        CType(SRA_Neg.Constantes.ProductosRelacion.Padre, String), usrPadre)

        If mbooMostrarMadre Then mGuardarRelacionados(ldsDatos, ldrProd, _
        CType(SRA_Neg.Constantes.ProductosRelacion.Madre, String), usrMadre)

        If mbooMostrarPadre Then
            If ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Padre).GetUpperBound(0) > -1 Then
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Padre)(0).Item("ptre_supe_prdt_id").ToString, usrPadre)
            Else
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTabla).Select("prdt_sexo = 1")(0).Item("prdt_id").ToString, usrPadre)
            End If
        End If

        If mbooMostrarMadre Then
            If ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Madre).GetUpperBound(0) > -1 Then
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Madre)(0).Item("ptre_supe_prdt_id").ToString, usrMadre)
            Else
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTabla).Select("prdt_sexo = 0")(0).Item("prdt_id").ToString, usrMadre)
            End If
        End If



        Return (ldsDatos)
    End Function

    Public Function GuardarDatos(ByVal mint As Integer) As DataSet
        Dim ldsDatos As New DataSet
        Dim ldrProd As DataRow

        'ldsDatos = mCrearDataSet(usrProducto.Valor.ToString)


        mCrearDataSet("", "productosUnico", mstrTabla, ldsDatos, True)
        mCrearDataSet("", mstrTablaAsociaciones, mstrTablaAsociaciones, ldsDatos, True)
        mCrearDataSet("", mstrTablaDocum, mstrTablaDocum, ldsDatos, True)
        mCrearDataSet("", mstrTablaRelaciones, mstrTablaRelaciones, ldsDatos, True)
        mCrearDataSet("", mstrTablaEmbriones, mstrTablaEmbriones, ldsDatos, True)
        mCrearDataSet("", mstrTablaTramitesProductos, mstrTablaTramitesProductos, ldsDatos, True)
        mCrearDataSet("", mstrTablaSemen, mstrTablaSemen, ldsDatos, True)


        If mbooMostrarProducto Then
            If ldsDatos.Tables(mstrTabla).Select.GetUpperBound(0) = -1 Then
                ldrProd = ldsDatos.Tables(mstrTabla).NewRow
                ldrProd.Table.Rows.Add(ldrProd)
            Else
                ldrProd = ldsDatos.Tables(mstrTabla).Select()(0)
            End If

            With ldrProd
                .Item("prdt_id") = clsSQLServer.gFormatArg(hdnProdId.Text, SqlDbType.Int)
                .Item("prdt_raza_id") = usrProducto.cmbProdRazaExt.Valor
                .Item("prdt_sexo") = usrProducto.cmbSexoProdExt.ValorBool
                .Item("prdt_nomb") = usrProducto.txtProdNombExt.Valor
                .Item("prdt_px") = txtPX.Text
                .Item("prdt_naci_fecha") = txtFechaNac.Fecha
                '.Item("prdt_rp_nume") = IIf(IsNumeric(Trim(usrProducto.txtRPExt.Text)), Trim(usrProducto.txtRPExt.Text), DBNull.Value)
                .Item("prdt_rp") = IIf(mintTramite = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen, IIf(Trim(usrProducto.txtRPExt.Text) = "", DBNull.Value, IIf(usrProducto.txtRPExt.Text.IndexOf("SI") = 0, "", "SI ") & Trim(usrProducto.txtRPExt.Text)), Trim(usrProducto.txtRPExt.Text))
                .Item("prdt_rp_extr") = txtRPExtr.Valor
                .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", NdadProd, .Item("prdt_ndad"))
                .Item("prdt_ori_asoc_id") = usrProducto.cmbProdAsocExt.Valor

                .Item("prdt_ori_asoc_nume") = usrProducto.txtCodiExt.Valor
                .Item("prdt_prop_clie_id") = IIf(mintPropietario > 0, mintPropietario, .Item("prdt_prop_clie_id"))
                .Item("generar_numero") = IIf(mbooGenerarSRANume, True, False)
                'If mintPropietario > 0 Then
                '   .Item("prdt_cria_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "criadores", "@cria_clie_id = " & mintPropietario.ToString & ",@cria_raza_id = " & .Item("prdt_raza_id"), "cria_id")
                'End If
                .Item("prdt_cria_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "criadores", "@cria_id = " & usrProducto.txtCodiExt.Valor & ",@cria_raza_id = " & .Item("prdt_raza_id"), "cria_id")

            End With


            If mbooMostrarNumeroExtranjero Then
                mGuardarAsociaciones(ldsDatos, ldrProd.Item("prdt_id").ToString, usrProducto)
            End If
        End If

        If mbooMostrarPadre Then mGuardarRelacionados(ldsDatos, ldrProd, _
        CType(SRA_Neg.Constantes.ProductosRelacion.Padre, String), usrPadre)

        If mbooMostrarMadre Then mGuardarRelacionados(ldsDatos, ldrProd, _
        CType(SRA_Neg.Constantes.ProductosRelacion.Madre, String), usrMadre)

        If mbooMostrarPadre Then
            If ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Padre).GetUpperBound(0) > -1 Then
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Padre)(0).Item("ptre_supe_prdt_id").ToString, usrPadre)
            Else
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTabla).Select("prdt_sexo = 1")(0).Item("prdt_id").ToString, usrPadre)
            End If
        End If

        If mbooMostrarMadre Then
            If ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Madre).GetUpperBound(0) > -1 Then
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Madre)(0).Item("ptre_supe_prdt_id").ToString, usrMadre)
            Else
                'clsWeb.ConvertDsToXML(ldsDatos, "ldsDatos Madre")
                mGuardarAsociaciones(ldsDatos, ldsDatos.Tables(mstrTabla).Select("prdt_sexo = 0")(0).Item("prdt_id").ToString, usrMadre)
            End If
        End If
       
        Return (ldsDatos)
    End Function





    Private Sub ActualizarId(ByVal SRow As DataRow, ByVal Campo As String, ByVal valor As Integer)
        SRow.Item(Campo) = valor
    End Sub

    Private Sub mGuardarAsociaciones(ByVal pdtsDatos As DataSet, _
    ByVal pstrPrdtId As String, ByVal pusrProd As usrProdDeriv)
        Dim ldrDatos As DataRow
        Dim lbooAlta As Boolean = False

        If pusrProd.cmbProdAsocExt.Valor.ToString <> "" And _
            pusrProd.txtCodiExt.Valor.ToString <> "" Then
            'Borro los existentes
            If pusrProd.Valor.ToString <> "" Then
                If pdtsDatos.Tables(mstrTablaAsociaciones).Select("ptnu_prdt_id=" & pusrProd.Valor.ToString & " and ptnu_asoc_id=" & pusrProd.cmbProdAsocExt.Valor.ToString).GetUpperBound(0) > -1 Then
                    ldrDatos = pdtsDatos.Tables(mstrTablaAsociaciones).Select("ptnu_prdt_id=" & pusrProd.Valor.ToString & " and ptnu_asoc_id=" & pusrProd.cmbProdAsocExt.Valor.ToString)(0)
                End If
            Else
                lbooAlta = True
                ldrDatos = pdtsDatos.Tables(mstrTablaAsociaciones).NewRow
                ldrDatos.Item("ptnu_id") = clsSQLServer.gObtenerId(pdtsDatos.Tables(mstrTablaAsociaciones), "ptnu_id")
            End If


            If ldrDatos Is Nothing Then
                clsError.gGenerarMensajes("", "DEBUG", "ldrdatos Is Nothing em  asociaciones")
                lbooAlta = True
                ldrDatos = pdtsDatos.Tables(mstrTablaAsociaciones).NewRow
                ldrDatos.Item("ptnu_id") = clsSQLServer.gObtenerId(pdtsDatos.Tables(mstrTablaAsociaciones), "ptnu_id")

            End If

            If Not ldrDatos Is Nothing Then
                With ldrDatos
                    .Item("ptnu_asoc_id") = pusrProd.cmbProdAsocExt.Valor
                    .Item("ptnu_prdt_id") = pstrPrdtId
                    .Item("ptnu_nume") = pusrProd.txtCodiExt.Valor

                    If lbooAlta Then
                        .Table.Rows.Add(ldrDatos)
                    End If
                End With
            End If



        End If
    End Sub


    Private Sub mGuardarRelacionados(ByVal pdsDatos As DataSet, ByVal pdrHijo As DataRow, _
    ByVal pstrRetiId As String, ByVal pusrRela As usrProdDeriv)

        Dim ldrPtre As DataRow 'FILA DE PRODUCTOS RELACIONES
        Dim ldrRela As DataRow 'FILA DEL PRODUCTO RELACIONADO
        Dim ldsDatosRela As New DataSet

        'ˇ     ldsDatos = mCrearDataSet(pusrRela.Valor.ToString)

        mCrearDataSet(pusrRela.Valor.ToString, "productosUnico", mstrTabla, ldsDatosRela, True)
        mCrearDataSet(pusrRela.Valor.ToString, mstrTablaAsociaciones, mstrTablaAsociaciones, ldsDatosRela, True)
        mCrearDataSet(pusrRela.Valor.ToString, mstrTablaDocum, mstrTablaDocum, ldsDatosRela, True)
        mCrearDataSet(pusrRela.Valor.ToString, mstrTablaRelaciones, mstrTablaRelaciones, ldsDatosRela, True)




        If pusrRela.Valor.ToString <> "" Then
            If ldsDatosRela.Tables(0).Select("prdt_id=" & pusrRela.Valor.ToString).GetUpperBound(0) < 0 Then
                ldrRela = ldsDatosRela.Tables(0).NewRow
                ldrRela.Item("prdt_id") = clsSQLServer.gObtenerId(pdsDatos.Tables(mstrTabla), "prdt_id")
            Else
                ldrRela = ldsDatosRela.Tables(0).Select("prdt_id=" & pusrRela.Valor.ToString)(0)
            End If
        Else
            ldrRela = ldsDatosRela.Tables(0).NewRow
            ldrRela.Item("prdt_id") = clsSQLServer.gObtenerId(pdsDatos.Tables(mstrTabla), "prdt_id")
        End If


        If Not pdrHijo Is Nothing And mintTramite <> SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then 'CUANDO HAY PRODUCTO(NO EMBRIONES)



            With pdsDatos.Tables(mstrTablaRelaciones).Select("ptre_reti_id=" & pstrRetiId)
                If .GetUpperBound(0) > -1 Then
                    ldrPtre = .GetValue(0)
                Else
                    ldrPtre = pdsDatos.Tables(mstrTablaRelaciones).NewRow
                    ldrPtre.Item("ptre_id") = clsSQLServer.gObtenerId(ldrPtre.Table, "ptre_id")
                End If

                With ldrPtre
                    .Item("ptre_reti_id") = pstrRetiId
                    .Item("ptre_prdt_id") = pdrHijo.Item("prdt_id")
                    .Item("ptre_supe_prdt_id") = ldrRela.Item("prdt_id")

                    If .Item("ptre_id") <= 0 Then
                        .Table.Rows.Add(ldrPtre)
                    End If
                End With
            End With

        End If

        With ldrRela
            .Item("prdt_raza_id") = IIf(pusrRela.cmbProdRazaExt.Valor.ToString() = "", _
                        usrProducto.cmbProdRazaExt.Valor, _
                        pusrRela.cmbProdRazaExt.Valor)


            .Item("prdt_sexo") = pusrRela.cmbSexoProdExt.ValorBool
            .Item("prdt_nomb") = pusrRela.txtProdNombExt.Valor
            .Item("prdt_rp") = IIf(Trim(pusrRela.txtRPExt.Text) = "", DBNull.Value, Trim(pusrRela.txtRPExt.Text))
            '.Item("prdt_rp_nume") = IIf(IsNumeric(Trim(pusrRela.txtRPExt.Text)), Trim(pusrRela.txtRPExt.Text), DBNull.Value)
            .Item("prdt_ndad") = IIf(NdadPadres = "", DBNull.Value, NdadPadres)
            .Item("prdt_ori_asoc_id") = pusrRela.cmbProdAsocExt.Valor
            .Item("prdt_ori_asoc_nume") = pusrRela.txtCodiExt.Valor
            .Item("generar_numero") = IIf(mbooGenerarSRANume, True, False)

            pdsDatos.Tables(mstrTabla).Rows.Add(ldrRela.ItemArray)
        End With

    End Sub
    

    Private Function mCrearDataSet(ByVal pstrId As String) As DataSet
        Dim ldsDatos As DataSet

        If pstrId = "" Then
            pstrId = "-1"
        End If

        ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, "@tramite='S'")

        With ldsDatos
            .Tables(0).TableName = mstrTabla
            '            .Tables(1).TableName = mstrTablaAnalisis
            .Tables(2).TableName = mstrTablaAsociaciones
            .Tables(3).TableName = mstrTablaDocum
            .Tables(4).TableName = mstrTablaRelaciones
        End With

        Return ldsDatos
    End Function

    Private Function mCrearDataSet(ByVal pstrId As String, ByVal pSpEstructura As String, _
              ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
              ByVal pOpcionAdd As Boolean) As Boolean


        Dim ldsDatosTransf As New DataSet

        If pstrId = "" Then
            pstrId = "-1"
        End If

        Dim tblDatos As New DataTable(pTableName)
        ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, pstrId)
        tblDatos = ldsDatosTransf.Tables(0)
        tblDatos.TableName = pTableName

        dsDatosNewDataSet.Tables.Add(tblDatos.Copy())



        'With ldsDatos
        '    .Tables(0).TableName = mstrTabla
        '    .Tables(1).TableName = mstrTablaAsociaciones
        '    .Tables(2).TableName = mstrTablaDocum
        '    .Tables(3).TableName = mstrTablaRelaciones
        'End With

        Return True
    End Function



#End Region

#Region "Metodos publicos"
   Public Sub Limpiar()
      Valor = DBNull.Value
   End Sub
#End Region
End Class

End Namespace
