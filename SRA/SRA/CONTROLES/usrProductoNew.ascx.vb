Namespace SRA

Partial Class usrProductoNew
    Inherits System.Web.UI.UserControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hdnProp As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblCriaNumeRaza As System.Web.UI.WebControls.Label
    Protected WithEvents spProp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spSraNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ps1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblProp As System.Web.UI.WebControls.Label
    Protected WithEvents txtProp As NixorControls.TextBoxTab
    Protected WithEvents br5 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br10 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br11 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br12 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtSociNume As NixorControls.NumberBox
    Protected WithEvents lblAsoc As System.Web.UI.WebControls.Label
    Protected WithEvents txtAsocNume As NixorControls.NumberBox
    Protected WithEvents spAsocNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br6 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spClieNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spSociNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spLegaNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spAgrupa As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spAsoc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spNachMedioPago As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents cmbSexo As NixorControls.ComboBox

    Protected WithEvents trRPExtr As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents txtRPExtr As NixorControls.TextBoxTab
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents PnlAll As Anthem.Panel




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Event Cambio(ByVal sender As System.Object)
    Event RazaChange(ByVal sender As System.Web.UI.WebControls.DropDownList)
    Event IdProdSelected(ByVal sender As String)
    Protected mstrCtrlId As String

#Region "Variables privadas"
    Public mstrConn As String

    Private mstrTabla As String
    Private mstrCampoCodi As String
    Private mintAncho As Integer = 600
    Private mintAlto As Integer = 450
    Private mbooAceptaNull As Boolean = True
    Private mbooMuestraDesc As Boolean = False
    Private mbooMuestraProdNomb As Boolean = False
    Private mbooMuestraTipoRegistroVariedad As Boolean = False
    Private mbooAsocExtr As Boolean = True
    Private mbooFechaNaci As Boolean = False
    Private mbooNroAsocExtr As Boolean = True
    Private mbooMuestraNroCondicional As Boolean = False
    Private mbooMuestraBotonAgregarImportado As Boolean = False
    Private mstrTipoControl As String = "producto" 'puede ser: producto, madre, padre � pedigree.
    Private mbooAutoPostback As Boolean = False
    Private mbooPermiModi As Boolean = False
    Private mbooSoloBusq As Boolean = False
    Private mbooIncluirDeshabilitados As Boolean = False
    Private mintValiExis As Integer = 1
    Private mstrCampoVal As String = "Producto"
    Private mbooEsPropiertario As Boolean = True
    Private mstrPagina As String

    Private mstrFiltroRazas As String = ""
    'Private mstrRazaId As String = ""

    Private mbooFilProp As Boolean = False
    Private mbooFilSraNume As Boolean = True
    Private mbooFilRpNume As Boolean = False
    Private mbooFilCriaNume As Boolean = True
    Private mbooFilLaboNume As Boolean = True
    Private mbooFilAsocNume As Boolean = False
    Private mbooFilNaciFecha As Boolean = True
    Private mbooFilMadre As Boolean = False
    Private mbooFilPadre As Boolean = False
    Private mbooFilRece As Boolean = False
    Private mbooFilSexo As Boolean = True
    Private mbooFilNumExtr As Boolean = True
    Private mstrTipo As String = ""
    Private mstrEstaId As String = ""

    Private mbooColNomb As Boolean = True
    Private mbooColSraNume As Boolean = True
    Private mbooColRpNume As Boolean = True
    Private mbooColCriaNume As Boolean = True
    Private mbooColLaboNume As Boolean = False
    Private mbooColAsocNume As Boolean = True
    Private mbooColNaciFecha As Boolean = True
    Private mbooColRazaSoloCodi As Boolean = False
    Private mbooColCondicional As Boolean = False

    Private mbooInscrip As Boolean = False

    Private mstrSaltos As String = ""
    Private mstrSexo As String = ""
    Private mstrCriaId As String = ""
    Private mstrCondicional As String = ""
    Private mstrRaza As String = ""
    Private mbooBloqCriaFil As Boolean = False

    Private mbooIgnoraInexistencia As Boolean = False
#End Region

#Region "Metodos privados"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lstrSRA As String
        Dim lstrSP As String = "razas_cargar "



        mstrCtrlId = Me.ID
        mstrConn = clsWeb.gVerificarConexion(Me.Page)

        If mbooFilSexo Then
            cmbProdSexo.Enabled = True
        Else
            cmbProdSexo.Enabled = False
        End If

        If txtSexoId.Text <> "" Then
            mstrSexo = txtSexoId.Text
        End If
        If txtCriaId.Text <> "" Then
            mstrCriaId = txtCriaId.Text
        End If

        'If mbooEsPropiertario Then
        '    lblCriaOrProp.Text = "Propietario:"
        'Else
        '    lblCriaOrProp.Text = "Criador:"
        'End If
        lblCriaOrProp.Text = "Raza/Criador:"

        If mbooMuestraDesc Then
            trDescrip.Style.Add("display", "inline")
        Else
            trDescrip.Style.Add("display", "none")
        End If

        If mbooMuestraProdNomb Then
            trProdNomb.Style.Add("display", "inline")
        Else
            trProdNomb.Style.Add("display", "none")
        End If

        If mbooMuestraTipoRegistroVariedad Then
            trTipoRegistro.Style.Add("display", "inline")
        Else
            trTipoRegistro.Style.Add("display", "none")
        End If

        If mbooAsocExtr Then
            trAsocExtranjera.Style.Add("display", "inline")
        Else
            trAsocExtranjera.Style.Add("display", "none")
        End If

        If mbooFechaNaci Then
            trFechaNaci.Style.Add("display", "inline")
        Else
            trFechaNaci.Style.Add("display", "none")
        End If

        If mbooNroAsocExtr Then
            trNroExtr.Style.Add("display", "inline")
        Else
            trNroExtr.Style.Add("display", "none")
        End If

        If mbooMuestraNroCondicional Then
            trNroCondicional.Style.Add("display", "inline")
        Else
            trNroCondicional.Style.Add("display", "none")
        End If

        imgBuscAvan.Style.Add("display", "none")

        hdnValiExis.Text = mintValiExis.ToString

        If Not Page.IsPostBack Then
            mInicializarControles()
            mSetearEventos()
            If (cmbProdSexo.Items.Count = 0) Then
                cmbProdSexo.Items.Insert(0, "(Sexo)")
                cmbProdSexo.Items.Insert(1, "Hembra")
                cmbProdSexo.Items.Insert(2, "Macho")
            End If

            If (mstrSexo = "") Then
                cmbProdSexo.SelectedIndex = 0
            ElseIf (mstrSexo = "0") Then
                cmbProdSexo.SelectedIndex = 1
            Else
                cmbProdSexo.SelectedIndex = 2
            End If

            lstrSRA = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbProdAsoc, "id", "descrip", "T")

            If hdnRazaCruza.Text <> "" And txtSexoId.Text <> "" Then
                lstrSP = "razas_padres_cargar " & hdnRazaCruza.Text & "," & txtSexoId.Text
            End If
            clsWeb.gCargarCombo(mstrConn, lstrSP, cmbCriaNumeRaza, "id", "descrip", "T")

            SRA_Neg.Utiles.gSetearRaza(cmbCriaNumeRaza)

            If mbooInscrip Then lstrSP = "razas_inscrip_cargar"
            If hdnRazaCruza.Text <> "" And txtSexoId.Text <> "" Then
                lstrSP = "razas_padres_cargar " & hdnRazaCruza.Text & "," & txtSexoId.Text
            End If

            lstrSP += IIf(lstrSP.IndexOf(",") > 0 And mstrFiltroRazas <> "", ",", " ") & mstrFiltroRazas

            'clsWeb.gCargarCombo(mstrConn, lstrSP, cmbProdRaza, "id", "descrip", "T")
            'cmbProdRaza.SelectedIndex = cmbProdRaza.Items.IndexOf(cmbProdRaza.Items.FindByText(txtRazaId.Text))

            'If cmbProdRaza.Valor.ToString = "" Then
            'SRA_Neg.Utiles.gSetearRaza(cmbProdRaza)
            'End If
            mSetearDeno()
        Else
            If lblIdAnt.Text <> txtId.Text Then
                'para refrescar el estado del textbox
                Valor = txtId.Text
            End If
        End If

        txtSexoId.Text = mstrSexo
        txtCriaId.Text = mstrCriaId
        txtId.CampoVal = CampoVal
        txtRazaSessId.Text = Session("sRazaId")

    End Sub

    Private Sub mInicializarControles()
        usrCriadorFil.Tabla = "Criadores"
        usrCriadorFil.AutoPostback = False
        usrCriadorFil.FilClaveUnica = False
        usrCriadorFil.ColClaveUnica = False
        usrCriadorFil.Ancho = 820
        usrCriadorFil.Alto = 510
        usrCriadorFil.ColDocuNume = False
        usrCriadorFil.ColCUIT = False
        usrCriadorFil.ColClaveUnica = False
        usrCriadorFil.FilClaveUnica = True
        usrCriadorFil.FilAgru = False
        usrCriadorFil.FilCriaNume = True
        usrCriadorFil.FilCUIT = False
        usrCriadorFil.FilDocuNume = True
        usrCriadorFil.FilTarjNume = False
        usrCriadorFil.Criador = True
    End Sub

    Private Function mSetearDeno()
        mstrConn = clsWeb.gVerificarConexion(Me.Page)
        'If cmbProdRaza.Valor.ToString <> "" Then
        If usrCriadorFil.cmbCriaRazaExt.SelectedValue <> "" Then
            Me.usrCriadorFil.RazaCodi = Trim(clsSQLServer.gObtenerValorCampo(mstrConn, "razas", usrCriadorFil.cmbCriaRazaExt.SelectedValue, "raza_codi").ToString)
            'lblSraNume.Text = Trim(clsSQLServer.gObtenerValorCampo(mstrConn, "razas_especie", cmbProdRaza.Valor.ToString, "espe_nomb_nume").ToString) & ":"
            lblSraNume.Text = Trim(clsSQLServer.gObtenerValorCampo(mstrConn, "razas_especie", usrCriadorFil.cmbCriaRazaExt.SelectedValue.ToString, "espe_nomb_nume").ToString) & ":"
        Else
            lblSraNume.Text = "Nro.:"
            Me.usrCriadorFil.RazaCodi = ""
        End If
        Me.PnlSraNume.UpdateAfterCallBack = True
    End Function

    Private Sub mSetearEventos()
        Dim lstrEstilo As String
        Dim lstrParams As New StringBuilder
        Dim lstrSraAsocId As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
        Dim lstrInclDesha As String = IIf(mbooIncluirDeshabilitados, "1", "0")

        txtIgnoraInexist.Text = IIf(mbooIgnoraInexistencia, "1", "")

        'el bot�n de b�squeda
        lstrEstilo = String.Format("location=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width={0}px,height={1}px", Ancho.ToString, Alto.ToString)

        lstrParams.Append("FilSraNume=" & Math.Abs(CInt(FilSraNume)).ToString)
        lstrParams.Append("&FilRpNume=" & Math.Abs(CInt(FilRpNume)).ToString)
        lstrParams.Append("&FilProp=" & Math.Abs(CInt(FilProp)).ToString)
        lstrParams.Append("&FilLaboNume=" & Math.Abs(CInt(FilLaboNume)).ToString)
        lstrParams.Append("&FilCriaNume=" & Math.Abs(CInt(FilCriaNume)).ToString)
        lstrParams.Append("&FilNaciFecha=" & Math.Abs(CInt(FilNaciFecha)).ToString)
        lstrParams.Append("&FilMadre=" & Math.Abs(CInt(FilMadre)).ToString)
        lstrParams.Append("&FilPadre=" & Math.Abs(CInt(FilPadre)).ToString)
        lstrParams.Append("&FilRece=" & Math.Abs(CInt(FilRece)).ToString)
        lstrParams.Append("&FilSexo=" & Math.Abs(CInt(FilSexo)).ToString)

        lstrParams.Append("&ColNomb=" & Math.Abs(CInt(ColNomb)).ToString)
        lstrParams.Append("&ColSraNume=" & Math.Abs(CInt(ColSraNume)).ToString)
        lstrParams.Append("&ColRpNume=" & Math.Abs(CInt(ColRpNume)).ToString)
        lstrParams.Append("&ColLaboNume=" & Math.Abs(CInt(ColLaboNume)).ToString)
        lstrParams.Append("&ColCriaNume=" & Math.Abs(CInt(ColCriaNume)).ToString)
        lstrParams.Append("&ColAsocNume=" & Math.Abs(CInt(ColAsocNume)).ToString)
        lstrParams.Append("&ColNaciFecha=" & Math.Abs(CInt(ColNaciFecha)).ToString)

        lstrParams.Append("&FilTipo=" & FilTipo)
        lstrParams.Append("&SoloBusq=" & Math.Abs(CInt(mbooSoloBusq)))
        lstrParams.Append("&InclDeshab=" & Math.Abs(CInt(mbooIncluirDeshabilitados)))
        lstrParams.Append("&ValiExis=" & Math.Abs(CInt(mintValiExis)))
        lstrParams.Append("&EstaId=" & EstaId)
        lstrParams.Append("&Inscrip=" & Math.Abs(CInt(mbooInscrip)))
        lstrParams.Append("&ColRazaSoloCodi=" & Math.Abs(CInt(mbooColRazaSoloCodi)))
        lstrParams.Append("&BloqCriaFil=" & Math.Abs(CInt(mbooBloqCriaFil)))
        If hdnRazaCruza.Text <> "" Then
            lstrParams.Append("&RazaCruza=" & Math.Abs(CInt(hdnRazaCruza.Text)))
        End If
        lstrParams.Append("&EsProp=" & Math.Abs(CInt(mbooEsPropiertario)))

        lstrParams.Append("&pstNewCtrlProducto=" & 1)

        'txtCodi.Attributes.Add("onchange", "CodiProducto_change('" & UniqueID & "', '" & Math.Abs(CInt(AutoPostback)).ToString & "', '" & Tabla.ToString.ToLower & ";" & "prdt_sra_nume" & ";" & ";" & ";" & EstaId & ";" & lstrInclDesha & "', '" & CampoVal & "', '" & lstrSraAsocId & "','asoc_nume','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "');")
        'txtSraNume.Attributes.Add("onchange", "CodiProducto_change('" & UniqueID & "', '" & Math.Abs(CInt(AutoPostback)).ToString & "', '" & Tabla.ToString.ToLower & ";" & "prdt_sra_nume" & ";" & ";" & ";" & EstaId & ";" & lstrInclDesha & "', '" & CampoVal & "', '" & lstrSraAsocId & "','sra_nume','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "');")
        'txtRP.Attributes.Add("onchange", "CodiProducto_change('" & UniqueID & "', '" & Math.Abs(CInt(AutoPostback)).ToString & "', '" & Tabla.ToString.ToLower & ";" & "prdt_sra_nume" & ";" & ";" & ";" & EstaId & ";" & lstrInclDesha & "', '" & CampoVal & "', '" & lstrSraAsocId & "','sra_nume','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "');")
        imgBusc.Attributes.Add("onclick", "if (mValida('" & UniqueID & "')) {imgBuscProductoNew_click('" & UniqueID & "','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "','false');}")
        imgBuscAvan.Attributes.Add("onclick", "mBuscMostrarProdUsr('panBuscAvan" & mstrCtrlId & "','visible','','" & mstrCtrlId & "');")
        imgLimp.Attributes.Add("onclick", "imgLimpProductoNew_click('" & UniqueID & "');")

        spNaciFecha.Visible = False   'FilNaciFecha
        spRpNume.Visible = False      ' FilRpNume
        spCriaNume.Visible = False    ' FilCriaNume
        spLaboNume.Visible = FilLaboNume

    End Sub

    Private Sub txtId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtId.TextChanged
        RaiseEvent Cambio(Me)
    End Sub

    Private Sub txtTipoRegistroCodi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTipoRegistroCodi.TextChanged
        If (Me.txtTipoRegistroCodi.Text = "") Then
            cmbTipoRegistro.SelectedValue = ""
            Me.txtTipoRegistroCodi.Text = ""
        Else
            Dim idRegT As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_id" _
                                         , " @regt_raza_id=" & usrCriadorFil.cmbCriaRazaExt.SelectedValue & ",@regt_desc=" & Me.txtTipoRegistroCodi.Text)

            If (idRegT = "") Then
                cmbTipoRegistro.SelectedValue = idRegT
                Me.txtTipoRegistroCodi.Text = ""
            Else
                cmbTipoRegistro.SelectedValue = idRegT

            End If
        End If

        Me.PnlTipoRegistro.UpdateAfterCallBack = True
    End Sub

    Private Sub cmbTipoRegistro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoRegistro.SelectedIndexChanged
        If (Me.cmbTipoRegistro.SelectedValue <> "") Then
            Dim codiRegT As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_desc", " @regt_id=" & Me.cmbTipoRegistro.SelectedValue)

            If (codiRegT = "") Then
                Me.txtTipoRegistroCodi.Text = ""
            Else
                Me.txtTipoRegistroCodi.Text = codiRegT
            End If
        Else
            Me.txtTipoRegistroCodi.Text = ""
        End If
        Me.PnlTipoRegistro.UpdateAfterCallBack = True
    End Sub
    Private Sub txtVariadadCodi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVariadadCodi.TextChanged
        If (Me.txtVariadadCodi.Text = "") Then
            cmbVariadad.SelectedValue = ""
            Me.txtVariadadCodi.Text = ""
        Else
            Dim idVaridedad As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_variedades", "vari_id" _
                                         , " @vari_raza_id=" & usrCriadorFil.cmbCriaRazaExt.SelectedValue & ",@vari_codi=" & Me.txtVariadadCodi.Text)

            If (idVaridedad = "") Then
                cmbVariadad.SelectedValue = idVaridedad
                Me.txtVariadadCodi.Text = ""
            Else
                cmbVariadad.SelectedValue = idVaridedad

            End If
        End If
        Me.PnlTipoRegistro.UpdateAfterCallBack = True
    End Sub

    Private Sub cmbVariadad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVariadad.SelectedIndexChanged
        If (Me.cmbVariadad.SelectedValue <> "") Then
            Dim codiVaridedad As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_variedades", "vari_codi", " @vari_id=" & Me.cmbVariadad.SelectedValue)

            If (codiVaridedad = "") Then
                Me.txtVariadadCodi.Text = ""
            Else
                Me.txtVariadadCodi.Text = codiVaridedad
            End If
        Else
            Me.txtVariadadCodi.Text = ""
        End If
        Me.PnlTipoRegistro.UpdateAfterCallBack = True
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        If Saltos <> "" Then
            Dim vsSaltos() As String = Saltos.Split(",")
            Dim lstrCtrl As String
            Dim j, k As Integer

            For i As Integer = 0 To vsSaltos.GetUpperBound(0)
                j = 0
                While k < 4
                    k += 1
                    If Me.FindControl(mObtenerSpFil(k)).Visible Then
                        j += 1
                    End If
                End While
            Next
        End If
    End Sub

    Private Function mObtenerSpFil(ByVal pintSpanFil) As String
        Select Case pintSpanFil
            Case 1
                Return ("spRpNume")
            Case 2
                Return ("spCriaNume")
            Case 3
                Return ("spLaboNume")
            Case 4
                Return ("spNaciFecha")
        End Select
    End Function
#End Region

#Region "Propiedades publicas"
    Public Property CriaId() As String
        Get
            Return mstrCriaId
        End Get
        Set(ByVal Value As String)
            mstrCriaId = Value
        End Set
    End Property
    Public Property Raza() As String
        Get
            Return mstrRaza
        End Get
        Set(ByVal Value As String)
            mstrRaza = Value
        End Set
    End Property
    Public Property FilSexo() As Boolean
        Get
            Return mbooFilSexo
        End Get
        Set(ByVal Value As Boolean)
            mbooFilSexo = Value
        End Set
    End Property
    Public Property FilNumExtr() As Boolean
        Get
            Return mbooFilNumExtr
        End Get
        Set(ByVal Value As Boolean)
            mbooFilNumExtr = Value
        End Set
    End Property
    Public Property Sexo() As String
        Get
            Return mstrSexo
        End Get
        Set(ByVal Value As String)
            mstrSexo = Value
        End Set
    End Property
    Public Property GetSexoProd() As String
        Get
            If (cmbProdSexo.SelectedIndex = 0) Then
                mstrSexo = ""
            ElseIf (cmbProdSexo.SelectedIndex = 1) Then
                mstrSexo = "0"
            Else
                mstrSexo = "1"
            End If
            Return mstrSexo
        End Get
        Set(ByVal Value As String)
            mstrSexo = Value
        End Set
    End Property
    Public Property ColCondicional() As String
        Get
            Return mstrCondicional
        End Get
        Set(ByVal Value As String)
            mstrCondicional = Value
        End Set
    End Property
    Public Property FiltroRazas() As String
        Get
            Return mstrFiltroRazas
        End Get
        Set(ByVal Value As String)
            mstrFiltroRazas = Value
        End Set
    End Property
    Public Property Tabla() As String
        Get
            Return mstrTabla
        End Get
        Set(ByVal Value As String)
            mstrTabla = Value
        End Set
    End Property
    Public Property Pagina() As String
        Get
            If mstrPagina <> "" Then
                Return mstrPagina
            Else
                Dim lstrPagina As New StringBuilder
                Dim lintSub As Integer = Len(Page.TemplateSourceDirectory) - Len(Replace(Page.TemplateSourceDirectory, "/", "")) - 1
                If lintSub = 1 Then
                    lstrPagina.Append("../")
                ElseIf lintSub = 2 Then
                    lstrPagina.Append("../../")
                End If

                Select Case mstrTabla.ToLower
                    Case "productos"
                        lstrPagina.Append("productos.aspx")
                End Select
                Return (lstrPagina.ToString)
            End If
        End Get
        Set(ByVal Value As String)
            mstrPagina = Value
        End Set
    End Property
    Public Property CampoCodi() As String
        Get
            If mstrCampoCodi <> "" Then
                Return mstrCampoCodi
            Else
                Select Case mstrTabla.ToLower
                    Case "productos"
                        Return ("prdt_sra_nume")
                End Select
            End If
        End Get
        Set(ByVal Value As String)
            mstrCampoCodi = Value
        End Set
    End Property
    Public Property MuestraDesc() As Boolean
        Get
            Return mbooMuestraDesc
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraDesc = Value
        End Set
    End Property
    Public Property MuestraProdNomb() As Boolean
        Get
            Return mbooMuestraProdNomb
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraProdNomb = Value
        End Set
    End Property
    Public Property MuestraTipoRegistroVariedad() As Boolean
        Get
            Return mbooMuestraTipoRegistroVariedad
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraTipoRegistroVariedad = Value
        End Set
    End Property
    Public Property MuestraNroCondicional() As Boolean
        Get
            Return mbooMuestraNroCondicional
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraNroCondicional = Value
        End Set
    End Property
    Public Property MuestraNroAsocExtr() As Boolean
        Get
            Return mbooNroAsocExtr
        End Get

        Set(ByVal Value As Boolean)
            mbooNroAsocExtr = Value
        End Set
    End Property
    Public Property MuestraAsocExtr() As Boolean
        Get
            Return mbooAsocExtr
        End Get

        Set(ByVal Value As Boolean)
            mbooAsocExtr = Value
        End Set
    End Property
    Public Property MuestraFechaNaci() As Boolean
        Get
            Return mbooFechaNaci
        End Get

        Set(ByVal Value As Boolean)
            mbooFechaNaci = Value
        End Set
    End Property

    Public Property MuestraBotonAgregaImportado() As Boolean
        Get
            Return mbooMuestraBotonAgregarImportado
        End Get

        Set(ByVal Value As Boolean)
            mbooMuestraBotonAgregarImportado = Value
        End Set
    End Property
    Public Property TipoControl() As String
        Get
            Return mstrTipoControl
        End Get

        Set(ByVal Value As String)
            mstrTipoControl = Value
        End Set
    End Property
    Public Property AceptaNull() As Boolean
        Get
            Return mbooAceptaNull
        End Get

        Set(ByVal Value As Boolean)
            mbooAceptaNull = Value
        End Set
    End Property
    Public Property AutoPostback() As Boolean
        Get
            Return mbooAutoPostback
        End Get

        Set(ByVal AutoPostback As Boolean)
            mbooAutoPostback = AutoPostback
        End Set
    End Property
    Public Property IncluirDeshabilitados() As Boolean
        Get
            Return mbooIncluirDeshabilitados
        End Get

        Set(ByVal IncluirDeshabilitados As Boolean)
            mbooIncluirDeshabilitados = IncluirDeshabilitados
        End Set
    End Property
    Public Property Obligatorio() As Boolean
        Get
            Return txtId.Obligatorio
        End Get

        Set(ByVal Value As Boolean)
            txtId.Obligatorio = Value
        End Set
    End Property
    Public Property CampoVal() As String
        Get
            Return mstrCampoVal
        End Get

        Set(ByVal CampoVal As String)
            mstrCampoVal = CampoVal
        End Set
    End Property
    Public Property EsPropietario() As Boolean
        Get
            Return mbooEsPropiertario
        End Get

        Set(ByVal EsPropietario As Boolean)
            mbooEsPropiertario = EsPropietario
        End Set
    End Property
    Public Property Valor() As Object
        Get
            If txtId.Text = "" Then
                If AceptaNull Then
                    Return (DBNull.Value)
                Else
                    Return (0)
                End If
            Else
                Return (txtId.Text)
            End If
        End Get

        Set(ByVal Valor As Object)
            If (Valor Is DBNull.Value) OrElse (Valor.ToString = "") OrElse (Valor.ToString = "0") Then
                txtId.Text = ""
                txtProdNomb.Text = ""
                txtCodi.Text = ""
                txtSraNume.Text = ""
                txtDesc.Text = ""
                txtRP.Text = ""
                txtNroCondicional.Text = ""
                txtFechaNaci.Text = ""
                chkFechaNaciTrim.Checked = False
                hdnRazaCruza.Text = ""
                txtPx.Text = ""
                'If mstrSexo <> "" Then
                If (mstrSexo = "") Then
                    cmbProdSexo.SelectedIndex = 0
                ElseIf (mstrSexo = "0") Then
                    cmbProdSexo.SelectedIndex = 1
                Else
                    cmbProdSexo.SelectedIndex = 2
                End If
                txtSexoId.Text = mstrSexo
                'End If
                txtCriaId.Text = mstrCriaId
                If mstrConn Is Nothing Then
                    mstrConn = clsWeb.gVerificarConexion(Me.Page)
                End If
                ' cargo todas las asociaciones
                clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbProdAsoc, "id", "descrip", mstrTipo)
                ' mando a refescar el panel del combo de asociaciones
                Me.PnlcmbProdAsoc.UpdateAfterCallBack = True

                ' cargo los combos de tipo registro y variedad
                clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = -1", cmbVariadad, "id", "descrip", mstrTipo, False, True)
                ' seteo la variedad en el combo
                cmbVariadad.SelectedValue = ""
                ' cargo el txt codi de variedad
                Me.cmbVariadad_SelectedIndexChanged(Nothing, Nothing)
                clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =-1", cmbTipoRegistro, "id", "descrip", mstrTipo, False, True)
                ' seteo el tipo registro en el combo
                cmbTipoRegistro.SelectedValue = ""
                ' cargo el txt codi de tipo reg
                Me.cmbTipoRegistro_SelectedIndexChanged(Nothing, Nothing)
                ' refresco el panel de tipo de registro y variedad
                Me.PnlTipoRegistro.UpdateAfterCallBack = True
                usrCriadorFil.Limpiar()
                usrCriadorFil.cmbCriaRazaExt.SelectedValue = ""
                'cmbProdAsoc.Valor = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
                If usrCriadorFil.Activo Then
                    usrCriadorFil.Limpiar()
                    If mstrRaza <> "" Then
                        usrCriadorFil.RazaId = mstrRaza
                    End If
                End If

                mSetearDeno()
                'If cmbProdRaza.Enabled Then
                '    cmbProdRaza.Limpiar()
                '    If mstrRaza <> "" Then
                '        cmbProdRaza.Valor = mstrRaza
                '    Else
                '        SRA_Neg.Utiles.gSetearRaza(cmbProdRaza)
                '    End If
                '    mSetearDeno()
                'End If
                lblIdAnt.Text = txtId.Text
                RaiseEvent IdProdSelected(Me.txtId.Text)
            Else
                txtId.Text = Valor
                If (lblIdAnt.Text <> txtId.Text) Then
                    Dim vsRet As String()
                    vsRet = SRA_Neg.Utiles.BuscarProdDeriv(Page.Session("sConn").ToString, txtId.Text & ";" & Tabla & ";id;" & ";" & ";" & EstaId & ";" & IIf(mbooIncluirDeshabilitados, "1", "0")).Split("|")

                    If vsRet.GetUpperBound(0) > 0 Then
                        usrCriadorFil.RazaId = vsRet(1)
                        ' cargo las asociaciones de acuerdo a la raza
                        clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= " & usrCriadorFil.RazaId, cmbProdAsoc, "id", "descrip", mstrTipo)
                        ' mando a refescar el panel del combo de asociaciones
                        Me.PnlcmbProdAsoc.UpdateAfterCallBack = True
                        ' cargo los combos de tipo registro y variedad de acuerdo a la raza y ejecuto evento para cambiar los txtbox asociados
                        clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id =" & usrCriadorFil.RazaId, cmbVariadad, "id", "descrip", mstrTipo, False, True)
                        ' seteo la variedad en el combo
                        cmbVariadad.SelectedValue = vsRet(15)
                        ' cargo el txt codi de variedad
                        Me.cmbVariadad_SelectedIndexChanged(Nothing, Nothing)
                        clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =" & usrCriadorFil.RazaId, cmbTipoRegistro, "id", "descrip", mstrTipo, False, True)
                        ' seteo el tipo registro en el combo
                        cmbTipoRegistro.SelectedValue = vsRet(10)
                        ' cargo el txt codi de tipo reg
                        Me.cmbTipoRegistro_SelectedIndexChanged(Nothing, Nothing)
                        ' refresco el panel de tipo de registro y variedad
                        Me.PnlTipoRegistro.UpdateAfterCallBack = True
                        'cmbProdRaza.Valor = vsRet(1)
                        mSetearDeno()
                        txtId.Text = vsRet(0)
                        'txtRazaId.Text = vsRet(1)
                        txtSraNume.Text = vsRet(9)
                        txtProdNomb.Text = vsRet(3)

                        If (vsRet(8) = "") Then
                            cmbProdSexo.SelectedIndex = 0
                            mstrSexo = ""
                        ElseIf (vsRet(8) = "True") Then
                            cmbProdSexo.SelectedIndex = 2
                            mstrSexo = "1"
                        Else
                            cmbProdSexo.SelectedIndex = 1
                            mstrSexo = "0"
                        End If
                        txtSexoId.Text = mstrSexo
                        txtDesc.Text = vsRet(5)
                        txtRP.Text = vsRet(6)
                        txtRegtId.Text = vsRet(10)
                        txtNroCondicional.Text = vsRet(13)
                        If vsRet(7) <> clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id") Then
                            cmbProdAsoc.Valor = vsRet(7)
                            txtCodi.Text = vsRet(2)
                        Else
                            cmbProdAsoc.Limpiar()
                            txtCodi.Text = ""
                        End If
                        chkFechaNaciTrim.Checked = vsRet(16)
                        If (vsRet(17) <> "") Then
                            txtFechaNaci.Text = IIf(vsRet(17) = "", "", Convert.ToDateTime(vsRet(17)).ToString("dd/MM/yyyy"))
                        Else
                            txtFechaNaci.Text = ""
                        End If

                        txtPx.Text = vsRet(18)
                        usrCriadorFil.txtCodiExt.Text = vsRet(19)
                        usrCriadorFil.txtApelExt.Text = vsRet(20)

                    Else
                        'cmbProdRaza.Limpiar()
                        usrCriadorFil.Limpiar()
                        'SRA_Neg.Utiles.gSetearRaza(cmbProdRaza)
                        mSetearDeno()
                        cmbProdAsoc.Valor = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
                        txtCodi.Text = ""
                        txtSraNume.Text = ""
                        txtProdNomb.Text = ""
                        If (mstrSexo = "") Then
                            cmbProdSexo.SelectedIndex = 0
                        ElseIf (mstrSexo = "0") Then
                            cmbProdSexo.SelectedIndex = 1
                        Else
                            cmbProdSexo.SelectedIndex = 2
                        End If
                        txtCriaId.Text = mstrCriaId
                        txtSexoId.Text = mstrSexo
                        txtDesc.Text = ""
                        txtRP.Text = ""
                        txtNroCondicional.Text = ""
                    End If

                    If PermiModi Then
                        Activo = Activo
                    End If
                    lblIdAnt.Text = txtId.Text
                    RaiseEvent IdProdSelected(Me.txtId.Text)
                End If
            End If
        End Set
    End Property
    Public Property Activo() As Boolean
        Get
            Return txtCodi.Enabled
        End Get

        Set(ByVal Activo As Boolean)
            'cmbProdRaza.Enabled = Activo
            If mstrSexo = "" Then cmbProdSexo.Enabled = Activo
            cmbProdAsoc.Enabled = Activo
            txtCodi.Enabled = Activo
            txtRP.Enabled = Activo
            txtSraNume.Enabled = Activo
            txtProdNomb.Enabled = Activo
            imgBusc.Disabled = Not Activo And (Not PermiModi Or Valor Is DBNull.Value)  'si est� inactivo, permite entrar solo a modicar
            imgBuscAvan.Disabled = Not Activo
            imgLimp.Disabled = Not Activo
            'usrCriadorFil.Activo = Activo
        End Set
    End Property

    Public Property SetInspecciones() As Boolean
        Get
            Return False
        End Get

        Set(ByVal SetInspecciones As Boolean)
            usrCriadorFil.Activo = SetInspecciones
            cmbProdAsoc.Enabled = SetInspecciones
            txtCodi.Enabled = SetInspecciones
            txtRP.Enabled = SetInspecciones
            txtSraNume.Enabled = SetInspecciones
            txtProdNomb.Enabled = SetInspecciones
            txtDesc.Enabled = SetInspecciones
            txtNroCondicional.Enabled = SetInspecciones
            'usrCriadorFil.Activo = Activo
        End Set
    End Property

    Public Property ControlCriador_Activo() As Boolean
        Get
            Return usrCriadorFil.Activo
        End Get

        Set(ByVal Activo As Boolean)
            usrCriadorFil.Activo = Activo
        End Set
    End Property
    Public Property SoloBusq() As Boolean
        Get
            Return mbooSoloBusq
        End Get

        Set(ByVal SoloBusq As Boolean)
            mbooSoloBusq = SoloBusq
        End Set
    End Property
    Public Property ValiExis() As Integer
        Get
            Return mintValiExis
        End Get

        Set(ByVal ValiExis As Integer)
            mintValiExis = ValiExis
        End Set
    End Property
    Public Property BloqCriaFil() As Boolean
        Get
            Return mbooBloqCriaFil
        End Get

        Set(ByVal value As Boolean)
            mbooBloqCriaFil = value
        End Set
    End Property
    Public Property PermiModi() As Boolean
        Get
            Return mbooPermiModi
        End Get

        Set(ByVal PermiModi As Boolean)
            mbooPermiModi = PermiModi
        End Set
    End Property
    Public Property Ancho() As Integer
        Get
            Return mintAncho
        End Get

        Set(ByVal Ancho As Integer)
            mintAncho = Ancho
        End Set
    End Property
    Public Property Alto() As Integer
        Get
            Return mintAlto
        End Get

        Set(ByVal Alto As Integer)
            mintAlto = Alto
        End Set
    End Property
    Public Property Saltos() As String
        Get
            Return mstrSaltos
        End Get

        Set(ByVal Value As String)
            mstrSaltos = Value
        End Set
    End Property
    Public Property IgnogaInexistente() As Boolean
        Get
            Return mbooIgnoraInexistencia
        End Get
        Set(ByVal Value As Boolean)
            mbooIgnoraInexistencia = Value
        End Set
    End Property
    Public ReadOnly Property ProdProp() As String
        Get
            Return (txtProdNomb.Text)
        End Get
    End Property
    Public ReadOnly Property Codi() As String
        Get
            Return (txtCodi.Text)
        End Get
    End Property
    Public ReadOnly Property ProdId() As Object
        Get
            Dim oRet As Object
            Dim lDsDatos As DataSet

            oRet = DBNull.Value

            If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, Tabla, Valor.ToString)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    Select Case Tabla.ToLower
                        Case "productos"
                            oRet = lDsDatos.Tables(0).Rows(0).Item("prdt_id")
                    End Select
                End If
            End If

            If Not AceptaNull And oRet Is DBNull.Value Then
                oRet = "0"
            End If

            Return (oRet)
        End Get
    End Property
    Public Property RazaId() As String
        Get
            Return usrCriadorFil.RazaId
        End Get
        Set(ByVal Value As String)
            usrCriadorFil.cmbCriaRazaExt.SelectedValue = Value
            mSetearDeno()
        End Set
    End Property
    Public Property CriaOrPropId() As String
        Get
            Return usrCriadorFil.Valor
        End Get
        Set(ByVal Value As String)
            usrCriadorFil.Valor = Value
        End Set
    End Property
    Public ReadOnly Property cmbTipoRegistroExt() As DropDownList
        Get
            Return (cmbTipoRegistro)
        End Get
    End Property
    Public ReadOnly Property cmbVariadadExt() As DropDownList
        Get
            Return (cmbVariadad)
        End Get
    End Property

    Public ReadOnly Property FechaNaci() As String
        Get
            Return (Me.txtFechaNaci.Text)
        End Get
    End Property
    Public ReadOnly Property Trimestre() As Boolean
        Get
            Return (Me.chkFechaNaciTrim.Checked)
        End Get
    End Property
    Public ReadOnly Property PX() As String
        Get
            Return (Me.txtPx.Text)
        End Get
    End Property



#Region "Propiedades para seteo de columnas"
    Public Property ColNomb() As Boolean
        Get
            Return mbooColNomb
        End Get
        Set(ByVal Value As Boolean)
            mbooColNomb = Value
        End Set
    End Property

    Public Property ColSraNume() As Boolean
        Get
            Return mbooColSraNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColSraNume = Value
        End Set
    End Property

    Public Property ColRpNume() As Boolean
        Get
            Return mbooColRpNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColRpNume = Value
        End Set
    End Property

    Public Property ColLaboNume() As Boolean
        Get
            Return mbooColLaboNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColLaboNume = Value
        End Set
    End Property

    Public Property ColRazaSoloCodi() As Boolean
        Get
            Return mbooColRazaSoloCodi
        End Get
        Set(ByVal Value As Boolean)
            mbooColRazaSoloCodi = Value
        End Set
    End Property
    Public Property ColCriaNume() As Boolean
        Get
            Return mbooColCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColCriaNume = Value
        End Set
    End Property

    Public Property ColAsocNume() As Boolean
        Get
            Return mbooColAsocNume
        End Get
        Set(ByVal Value As Boolean)
            mbooColAsocNume = Value
        End Set
    End Property

    Public Property ColNaciFecha() As Boolean
        Get
            Return mbooColNaciFecha
        End Get
        Set(ByVal Value As Boolean)
            mbooColNaciFecha = Value
        End Set
    End Property

#End Region

#Region "Propiedades para seteo de filtros"
    Public Property FilMadre() As Boolean
        Get
            Return mbooFilMadre
        End Get
        Set(ByVal Value As Boolean)
            mbooFilMadre = Value
        End Set
    End Property
    Public Property FilPadre() As Boolean
        Get
            Return mbooFilPadre
        End Get
        Set(ByVal Value As Boolean)
            mbooFilPadre = Value
        End Set
    End Property
    Public Property FilRece() As Boolean
        Get
            Return mbooFilRece
        End Get
        Set(ByVal Value As Boolean)
            mbooFilRece = Value
        End Set
    End Property
    Public Property FilProp() As Boolean
        Get
            Return mbooFilProp
        End Get
        Set(ByVal Value As Boolean)
            mbooFilProp = Value
        End Set
    End Property
    Public Property FilSraNume() As Boolean
        Get
            Return mbooFilSraNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilSraNume = Value
        End Set
    End Property
    Public Property FilRpNume() As Boolean
        Get
            Return mbooFilRpNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilRpNume = Value
        End Set
    End Property
    Public Property FilLaboNume() As Boolean
        Get
            Return mbooFilLaboNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilLaboNume = Value
        End Set
    End Property
    Public Property FilCriaNume() As Boolean
        Get
            Return mbooFilCriaNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilCriaNume = Value
        End Set
    End Property
    Public Property FilAsocNume() As Boolean
        Get
            Return mbooFilAsocNume
        End Get
        Set(ByVal Value As Boolean)
            mbooFilAsocNume = Value
        End Set
    End Property
    Public Property FilNaciFecha() As Boolean
        Get
            Return mbooFilNaciFecha
        End Get
        Set(ByVal Value As Boolean)
            mbooFilNaciFecha = Value
        End Set
    End Property
    Public Property Inscrip() As Boolean
        Get
            'si es inscripciones, carga razas con un SP especial 
            Return mbooInscrip
        End Get
        Set(ByVal Value As Boolean)
            mbooInscrip = Value
        End Set
    End Property
    Public Property FilTipo() As String
        Get
            Return mstrTipo
        End Get
        Set(ByVal Value As String)
            mstrTipo = Value
        End Set
    End Property
    Public Property EstaId() As String
        Get
            Return mstrEstaId
        End Get
        Set(ByVal Value As String)
            mstrEstaId = Value
        End Set
    End Property

    Public ReadOnly Property txtIdExt() As NixorControls.TextBoxTab
        Get
            Return txtId
        End Get
    End Property
    Public ReadOnly Property txtCodiExt() As NixorControls.TextBoxTab
        Get
            Return txtCodi
        End Get
    End Property
    Public ReadOnly Property txtSraNumeExtranjero() As NixorControls.TextBoxTab
        Get
            Return txtCodi
        End Get
    End Property
    Public ReadOnly Property txtSraNumeExt() As NixorControls.NumberBox
        Get
            Return txtSraNume
        End Get
    End Property
    Public ReadOnly Property txtProdNombExt() As NixorControls.TextBoxTab
        Get
            Return txtProdNomb
        End Get
    End Property
    Public ReadOnly Property txtSexoIdExt() As NixorControls.TextBoxTab
        Get
            Return txtSexoId
        End Get
    End Property
    Public ReadOnly Property cmbProdRazaExt() As NixorControls.ComboBox
        Get
            'Return (cmbProdRaza)
        End Get
    End Property
    Public ReadOnly Property usrClieDerivNewExt() As usrClieDerivNew
        Get
            Return (usrCriadorFil)
        End Get
    End Property
    Public ReadOnly Property hdnProdProp() As System.Web.UI.WebControls.TextBox
        Get
            Return (hdnProp)
        End Get
    End Property

    Public ReadOnly Property hdnRegtId() As System.Web.UI.WebControls.TextBox
        Get
            Return (txtRegtId)
        End Get
    End Property

    Public ReadOnly Property cmbSexoProdExt() As DropDownList
        Get
            Return (cmbProdSexo)
        End Get
    End Property
    Public ReadOnly Property cmbProdAsocExt() As NixorControls.ComboBox
        Get
            Return (cmbProdAsoc)
        End Get
    End Property
    Public ReadOnly Property txtRPExt() As NixorControls.TextBoxControls
        Get
            Return (txtRP)
        End Get
    End Property

    Public ReadOnly Property txtNroCondicionalExt() As NixorControls.NumberBox
        Get
            Return txtNroCondicional
        End Get
    End Property
    Public ReadOnly Property hdnRazaCruzaExt() As NixorControls.TextBoxTab
        Get
            Return (hdnRazaCruza)
        End Get
    End Property
#End Region
#End Region

#Region "Metodos publicos"
    Public Sub Limpiar()
        usrCriadorFil.Limpiar()
        usrCriadorFil.cmbCriaRazaExt.SelectedValue = ""
        Valor = DBNull.Value
        Me.txtFechaNaci.Text = ""
        Me.cmbProdSexo.SelectedIndex = 0
    End Sub

#End Region
#Region "Metodos que ejecutan el RaiseEvent RazaChange(sender)"
    Private Sub usrCriadorFil_Cambio(ByVal sender As DropDownList) Handles usrCriadorFil.RazaChange
        If (Me.hdnRazaId.Text <> sender.SelectedValue) Then
            Me.hdnRazaId.Text = sender.SelectedValue

            If (sender.SelectedValue <> "") Then
                clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= " & sender.SelectedValue, cmbProdAsoc, "id", "descrip", mstrTipo, False, True)
                mSetearDeno()

                Me.PnlcmbProdAsoc.UpdateAfterCallBack = True

                Me.txtVariadadCodi.Text = ""
                clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = " & sender.SelectedValue, cmbVariadad, "id", "descrip", mstrTipo, False, True)

                Me.txtTipoRegistroCodi.Text = ""
                clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =" & sender.SelectedValue, cmbTipoRegistro, "id", "descrip", mstrTipo, False, True)

                Me.PnlTipoRegistro.UpdateAfterCallBack = True

                RaiseEvent RazaChange(sender)
            End If
        End If
    End Sub
#End Region

    Private Sub cmbProdSexo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProdSexo.SelectedIndexChanged

        If (cmbProdSexo.SelectedIndex = 0) Then
            mstrSexo = ""
        ElseIf (cmbProdSexo.SelectedIndex = 1) Then
            mstrSexo = "0"
        Else
            mstrSexo = "1"
        End If
        Me.txtRP.TabIndex = 0
        Me.PnlSraNume.UpdateAfterCallBack = True
    End Sub

    Private Sub txtIdEvent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtIdEvent.TextChanged
        'RaiseEvent IdProdSelected(Me.txtId.Text)
    End Sub
End Class

End Namespace
