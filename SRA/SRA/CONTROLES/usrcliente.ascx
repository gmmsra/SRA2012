<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrCliente" CodeFile="usrCliente.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD align="left" width="15%"><cc1:numberbox id="txtCodi" runat="server" cssclass="cuadrotexto" Width="60px"></cc1:numberbox>
		<TD align="right" width="15%"><asp:label id="lblApel" runat="server" cssclass="titulo">Apel/R.Soc.:</asp:label>&nbsp;</TD>
		<TD align="left" width="25%">
			<cc1:textboxtab id="txtApel" runat="server" cssclass="cuadrotexto" Width="95%"></cc1:textboxtab>&nbsp;</TD>
		<TD align="left" width="25%">
			<IMG language="javascript" id="imgBusc" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
				src="..\imagenes\Buscar16.gif" runat="server">
			<img language="javascript" id="imgBuscAvan"  src='..\imagenes\buscavan.gif'  runat="server" alt='B�squeda Avanzada' style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset">
		</TD>
	</TR>
	<TR>
		<TD colSpan="4">
			<div class=paneledicion style='width=300;' id=panBuscAvan<%=mstrCtrlId%> style='Z-INDEX: 101; POSITION: absolute; visibility=hidden'>
				<table border=0 align=center valign=middle width=100%>
					<tr>
						<td colspan=2 align=center valign=middle height=34><span class=titulo>B�squeda Avanzada</span><input type=hidden size=1 name=hdnBusc<%=mstrCtrlId%> value=D>
						</td>
					</tr>
					<tr>
						<td width=20></td>
						<td>
							<SPAN id="spNomb" runat="server">
								<asp:label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;<cc1:textboxtab id="txtNomb" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab>
							</SPAN>
							<span runat=server visible=false id="br1"> <br></span>
							<SPAN id="spFanta" runat="server">
								<asp:checkbox id="chkFanta" cssclass="titulo" Width="90px" Text="Fantas�a" Runat="server"></asp:checkbox></SPAN>
							<span runat=server visible=false id="br2"> <br></span>
							<SPAN id="spSociNume" runat="server">
								<asp:label id="lblSociNume" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;<cc1:numberbox id="txtSociNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN>
							<span runat=server visible=false id="br3"> <br></span>
							<SPAN id="spCriaNume" style="CLEAR: none; DISPLAY: inline" runat="server">
								<asp:label id="lblCriaNumeRaza" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;<cc1:combobox tag="N" class="combo" id="cmbCriaNumeRaza" runat="server" Width="160px"></cc1:combobox>&nbsp;<asp:label id="lblCriaNume" runat="server" cssclass="titulo">Nro.Criador:</asp:label><cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>
							</SPAN>
							<span runat=server visible=false id="br4"> <br></span>
							<SPAN id="spLegaNume" style="CLEAR: none; DISPLAY: inline" runat="server">
								<asp:label id="lblLegaNume" runat="server" cssclass="titulo">Nro.Legajo:</asp:label>&nbsp;<cc1:numberbox id="txtLegaNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox>
							</SPAN>
							<span runat=server visible=false id="br5"> <br></span>
							<SPAN id="spNoCriaNume" style="DISPLAY: inline" runat="server">
								<asp:label id="lblNoCriaNume" runat="server" cssclass="titulo">Nro.No Criador:</asp:label>&nbsp;<cc1:numberbox id="txtNoCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN>
							<span runat=server visible=false id="br6"> <br></span>
							<SPAN id="spCUIT" runat="server">
								<asp:label id="lblCUIT" runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;<cc1:cuitbox id="txtCUIT" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:cuitbox>&nbsp;
							</SPAN>
							<span runat=server visible=false id="br7"> <br></span>
							<SPAN id="spDocuNume" style="DISPLAY: inline" runat="server">
								<asp:label id="lblDocuNumeTido" runat="server" cssclass="titulo">Tipo Doc.:</asp:label><cc1:combobox tag="N" class="combo" id="cmbDocuNumeTido" runat="server" Width="60px"></cc1:combobox>&nbsp;<asp:label id="lblDocuNume" runat="server" cssclass="titulo">Nro.Doc:</asp:label>&nbsp;<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox>&nbsp;
							</SPAN>
							<span runat=server visible=false id="br8"> <br></span>
							<SPAN id="spEntidad" style="DISPLAY: inline" runat="server">
								<asp:label id="lblEntidad" runat="server" cssclass="titulo">Entidad:</asp:label><cc1:combobox tag="N" class="combo" id="cmbEntidad" runat="server" Width="104px"></cc1:combobox>&nbsp;
							</SPAN>
							<span runat=server visible=false id="br9"> <br></span>
							<SPAN id="spMedioPago" runat="server">
								<asp:label id="lblMedioPago" runat="server" cssclass="titulo">Medio de Pago:</asp:label><cc1:combobox tag="N" class="combo" id="cmbMedioPago" runat="server" Width="120px"></cc1:combobox>&nbsp;
							</SPAN>
							<span runat=server visible=false id="br10"> <br></span>
							<SPAN id="spAgrupa" style="DISPLAY: inline" runat="server">
								<asp:checkbox id="chkAgrupa" cssclass="titulo" Text="Incluir Agrupaciones" Runat="server"></asp:checkbox>&nbsp;
							</SPAN>
							<span runat=server visible=false id="br11"> <br></span>
							<SPAN id="spEmpresa" style="DISPLAY: inline" runat="server">
								<asp:label id="lblEmpresa" runat="server" cssclass="titulo">Empresa:</asp:label><cc1:combobox tag="N" class="combo" id="cmbEmpresa" runat="server" Width="120px"></cc1:combobox>
							</SPAN>
							<span runat=server visible=false id="br12"> <br></span>
							<SPAN id="spClaveUnica" style="DISPLAY: inline" runat="server">
								<asp:label id="lblClaveUnica" runat="server" cssclass="titulo">Clave:</asp:label><cc1:numberbox id="txtClave" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN>
						</td>
					</tr>
					<tr>
						<td colspan=2 align=center valign=middle height=40>
							<input type=button class=boton value=Aceptar onclick=javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1','<%=mstrCtrlId%>'); id=btnBuscAcep<%=mstrCtrlId%>>&nbsp;&nbsp;
							<input type=button class=boton value=Cancelar onclick=javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>'); id=btnBuscCanc<%=mstrCtrlId%>>
						</td>
					</tr>
					</table>
				</div>
			<DIV style="DISPLAY: none"><asp:label id="lblId" runat="server">Cliente</asp:label><cc1:textboxtab id="txtId" runat="server" autopostback=true></cc1:textboxtab><asp:label id="lblIdAnt" runat="server"></asp:label></DIV>
		</TD>
	</TR>
	<TR>
		<TD colSpan="4">
			
		</TD>
	</TR>
	<TR>
		<TD align="right" colSpan="4"><asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Rows="2" TextMode="MultiLine"
				Width="100%" Enabled="False"></asp:textbox></TD>
	</TR>
</TABLE>
