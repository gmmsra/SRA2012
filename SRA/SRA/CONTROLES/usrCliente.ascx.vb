Namespace SRA

Partial Class usrCliente
   Inherits System.Web.UI.UserControl

   ' Las propiedades agregadas al combo son:
   ' - Valor	      : id del cliente seleccionado, o null si no hay ninguno seleccionado y el control acepta null, si no acepta null devuelve 0 (para los filtros)
   ' - Apel	         : apellido del cliente seleccionado
   ' - MuestraDesc   : si muestra la descripcion
   ' - AutoPostback	: si hace postback o no, si True => evento en servidor <ID>_Cambio, si False=> evento en cliente <ID>_onchange, ambos no obligatorios
   ' - Obligatorio	: si es obligatorio (da error al validar)
   ' - CampoVal		: campo que se muestra en el mensaje de error al validar
   ' - Activo		   : si est� activo
   ' - Ancho, Alto	: ancho y alto de la ventana de seleccion de clientes

   ' Las propiedades que definen filtros son:

   ' - FilNomb          : especifica si filtra por nombre
   ' - FilFanta         : especifica si la b�squeda por descripci�n incluye el apellido
   ' - FilSociNume      : especifica si filtra por nro de socio
   ' - FilCriaNume          : especifica si filtra por nro. de criador
   ' - FilNoCriaNume    : especifica si filtra por nro. de no-criador
   ' - FilCUIT          : especifica si filtra por nro. de cuit
   ' - FilDocuNume      : especifica si filtra por nro. de doc.
   ' - FilEnti          : especifica si filtra por entidad
   ' - FilMedioPago     : especifica si filtra por medio de pago
   ' - FilAgru          : especifica si filtra por agrupaci�n de clientes
   ' - FilEmpr          : especifica si filtra por empresa
   ' - FilNroClave      : especifica si filtra por clave �nica
   ' - FilLegaNume      : especifica si filtra por nro. de legajo
   ' - FilInseId        : especifica el instituto

   ' - Saltos           : indica cada cu�ntos controles corta

   ' 
   ' Las m�todos publicos son:

   ' - Limpiar	      : vac�a de contenido el control

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents ps1 As System.Web.UI.HtmlControls.HtmlGenericControl


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

   Event Cambio(ByVal sender As System.Object)
   Protected mstrCtrlId As String

#Region "Variables privadas"
   Private mstrConn As String

   Private mintAncho As Integer = 600
   Private mintAlto As Integer = 450
   Private mbooAceptaNull As Boolean = True
   Private mbooAutoPostback As Boolean = False
   Private mstrCampoVal As String = "Cliente"

   Private mbooFilNomb As Boolean = False
   Private mbooFilFanta As Boolean = False
   Private mbooFilSociNume As Boolean = False
   Private mbooFilCriaNume As Boolean = False
   Private mbooFilNoCriaNume As Boolean = False
   Private mbooFilLegaNume As Boolean = False
   Private mbooFilCUIT As Boolean = False
   Private mbooFilDocuNume As Boolean = False
   Private mbooFilEnti As Boolean = False
   Private mbooFilMedioPago As Boolean = False
   Private mbooFilAgru As Boolean = False
   Private mbooFilEmpr As Boolean = False
   Private mbooFilClaveUnica As Boolean = False
   Private mstrTipo As String = ""
   Private mstrInseId As String = ""

   Private mbooColNomb As Boolean = False
   Private mbooColFanta As Boolean = False
   Private mbooColSociNume As Boolean = False
   Private mbooColCriaNume As Boolean = False
   Private mbooColNoCriaNume As Boolean = False
   Private mbooColLegaNume As Boolean = False
   Private mbooColCUIT As Boolean = False
   Private mbooColDocuNume As Boolean = False
   Private mbooColEnti As Boolean = False
   Private mbooColMedioPago As Boolean = False
   Private mbooColEmpr As Boolean = False
   Private mbooColClaveUnica As Boolean = False

   Private mstrSaltos As String = ""
#End Region

#Region "Metodos privados"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      mstrCtrlId = Me.ID
      mstrConn = clsWeb.gVerificarConexion(Me.Page)

      If Not Page.IsPostBack Then
         mSetearEventos()
         clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbCriaNumeRaza, "T")
         clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuNumeTido, "T")
         clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEntidad, "T")
         clsWeb.gCargarRefeCmb(mstrConn, "empresas", cmbEmpresa, "T")
         clsWeb.gCargarRefeCmb(mstrConn, "pago_medios", cmbMedioPago, "T")

      Else
         If lblIdAnt.Text <> txtId.Text Then
            'para refrescar el estado del textbox
            Valor = txtId.Text
         End If
      End If
      txtId.CampoVal = CampoVal
   End Sub

   Private Sub mSetearEventos()
      Dim lstrEstilo As String
      Dim lstrParams As New StringBuilder

      txtCodi.Attributes.Add("onchange", "CodiClie_change('" & UniqueID & "', '" & Math.Abs(CInt(AutoPostback)).ToString & "');")

      'el bot�n de b�squeda
      lstrEstilo = String.Format("location=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width={0}px,height={1}px", Ancho.ToString, Alto.ToString)

      lstrParams.Append("FilSociNume=" & Math.Abs(CInt(FilSociNume)).ToString)
      lstrParams.Append("&FilNomb=" & Math.Abs(CInt(FilNomb)).ToString)
      lstrParams.Append("&FilLegaNume=" & Math.Abs(CInt(FilLegaNume)).ToString)
      lstrParams.Append("&FilCriaNume=" & Math.Abs(CInt(FilCriaNume)).ToString)
      lstrParams.Append("&FilNoCriaNume=" & Math.Abs(CInt(FilNoCriaNume)).ToString)
      lstrParams.Append("&FilCUIT=" & Math.Abs(CInt(FilCUIT)).ToString)
      lstrParams.Append("&FilDocuNume=" & Math.Abs(CInt(FilDocuNume)).ToString)
      lstrParams.Append("&FilEnti=" & Math.Abs(CInt(FilEnti)).ToString)
      lstrParams.Append("&FilMedioPago=" & Math.Abs(CInt(FilMedioPago)).ToString)
      lstrParams.Append("&FilEmpr=" & Math.Abs(CInt(FilEmpr)).ToString)
      lstrParams.Append("&FilClaveUnica=" & Math.Abs(CInt(FilClaveUnica)).ToString)
      lstrParams.Append("&FilAgru=" & Math.Abs(CInt(FilAgru)).ToString)
      lstrParams.Append("&FilFanta=" & Math.Abs(CInt(FilFanta)).ToString)

      lstrParams.Append("&ColNomb=" & Math.Abs(CInt(ColNomb)).ToString)
      lstrParams.Append("&ColFanta=" & Math.Abs(CInt(ColFanta)).ToString)
      lstrParams.Append("&ColSociNume=" & Math.Abs(CInt(ColSociNume)).ToString)
      lstrParams.Append("&ColLegaNume=" & Math.Abs(CInt(ColLegaNume)).ToString)
      lstrParams.Append("&ColCriaNume=" & Math.Abs(CInt(ColCriaNume)).ToString)
      lstrParams.Append("&ColNoCriaNume=" & Math.Abs(CInt(ColNoCriaNume)).ToString)
      lstrParams.Append("&ColCUIT=" & Math.Abs(CInt(ColCUIT)).ToString)
      lstrParams.Append("&ColDocuNume=" & Math.Abs(CInt(ColDocuNume)).ToString)
      lstrParams.Append("&ColEnti=" & Math.Abs(CInt(ColEnti)).ToString)
      lstrParams.Append("&ColMedioPago=" & Math.Abs(CInt(ColMedioPago)).ToString)
      lstrParams.Append("&ColEmpr=" & Math.Abs(CInt(ColEmpr)).ToString)
      lstrParams.Append("&ColClaveUnica=" & Math.Abs(CInt(ColClaveUnica)).ToString)

      lstrParams.Append("&FilTipo=" & FilTipo)
      lstrParams.Append("&FilInseId=" & FilInseId)

      imgBusc.Attributes.Add("onclick", "imgBuscCliente_click('" & UniqueID & "','" & lstrParams.ToString & "','" & Page.Session.SessionID & "','" & lstrEstilo & "');")
      imgBuscAvan.Attributes.Add("onclick", "mBuscMostrarUsr('panBuscAvan" & mstrCtrlId & "','visible','','" & mstrCtrlId & "');")

      spDocuNume.Visible = FilDocuNume
      spEntidad.Visible = FilEnti
      spMedioPago.Visible = FilMedioPago
      spAgrupa.Visible = FilAgru
      spEmpresa.Visible = FilEmpr
      spClaveUnica.Visible = FilClaveUnica
      spNoCriaNume.Visible = FilNoCriaNume
      spNomb.Visible = FilNomb
      spFanta.Visible = FilFanta
      spSociNume.Visible = FilSociNume
      spLegaNume.Visible = FilLegaNume
      spCriaNume.Visible = FilCriaNume
      spCUIT.Visible = FilCUIT
   End Sub

   Private Sub txtId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtId.TextChanged
      RaiseEvent Cambio(Me)
   End Sub

   Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
      If Saltos <> "" Then
         Dim vsSaltos() As String = Saltos.Split(",")
         Dim lstrCtrl As String
         Dim j, k As Integer

         For i As Integer = 0 To vsSaltos.GetUpperBound(0)
            j = 0
            While k < 11
               k += 1
               If Me.FindControl(mObtenerSpFil(k)).Visible Then
                  j += 1
               End If
               If j = vsSaltos(i) Then
                  Me.FindControl("br" & k).Visible = True
                  Exit While
               End If
            End While
         Next
      End If
   End Sub

   Private Function mObtenerSpFil(ByVal pintSpanFil) As String
      Select Case pintSpanFil
         Case 1
            Return ("spNomb")
         Case 2
            Return ("spFanta")
         Case 3
            Return ("spSociNume")
         Case 4
            Return ("spCriaNume")
         Case 5
            Return ("spLegaNume")
         Case 6
            Return ("spNoCriaNume")
         Case 7
            Return ("spCUIT")
         Case 8
            Return ("spDocuNume")
         Case 9
            Return ("spEntidad")
         Case 10
            Return ("spMedioPago")
         Case 11
            Return ("spAgrupa")
         Case 12
            Return ("spEmpresa")
         Case 13
            Return ("spClaveUnica")
      End Select
   End Function
#End Region

#Region "Propiedades publicas"
   Public Property MuestraDesc() As Boolean
      Get
         Return txtDesc.Visible
      End Get

      Set(ByVal Value As Boolean)
         txtDesc.Visible = Value
      End Set
   End Property

   Public Property AceptaNull() As Boolean
      Get
         Return mbooAceptaNull
      End Get

      Set(ByVal Value As Boolean)
         mbooAceptaNull = Value
      End Set
   End Property

   Public Property AutoPostback() As Boolean
      Get
         Return mbooAutoPostback
      End Get

      Set(ByVal AutoPostback As Boolean)
         mbooAutoPostback = AutoPostback
      End Set
   End Property

   Public Property Obligatorio() As Boolean
      Get
         Return txtId.Obligatorio
      End Get

      Set(ByVal Value As Boolean)
         txtId.Obligatorio = Value
      End Set
   End Property

   Public Property CampoVal() As String
      Get
         Return mstrCampoVal
      End Get

      Set(ByVal CampoVal As String)
         mstrCampoVal = CampoVal
      End Set
   End Property

   Public Property Valor() As Object
      Get
         If txtId.Text = "" Then
            If AceptaNull Then
               Return (DBNull.Value)
            Else
               Return (0)
            End If
         Else
            Return (txtId.Text)
         End If
      End Get

      Set(ByVal Valor As Object)
         If (Valor Is DBNull.Value) OrElse (Valor.ToString = "") OrElse (Valor.ToString = "0") Then
            txtId.Text = ""
            txtApel.Text = ""
            txtCodi.Text = ""
            txtDesc.Text = ""
         Else
            txtId.Text = Valor

            Dim vsRet As String()
            txtCodi.Text = txtId.Text
            vsRet = SRA_Neg.Clientes.BuscarClie(Page.Session("sConn").ToString, txtId.Text).Split("|")
            txtApel.Text = vsRet(0)
            txtDesc.Text = vsRet(1)
         End If

         lblIdAnt.Text = txtId.Text
      End Set
   End Property

   Public Property Activo() As Boolean
      Get
         Return txtCodi.Enabled
      End Get

      Set(ByVal Activo As Boolean)
         txtCodi.Enabled = Activo
         txtApel.Enabled = Activo
         imgBusc.Disabled = Not Activo
      End Set
   End Property

   Public Property Ancho() As Integer
      Get
         Return mintAncho
      End Get

      Set(ByVal Ancho As Integer)
         mintAncho = Ancho
      End Set
   End Property

   Public Property Alto() As Integer
      Get
         Return mintAlto
      End Get

      Set(ByVal Alto As Integer)
         mintAlto = Alto
      End Set
   End Property

   Public Property Saltos() As String
      Get
         Return mstrSaltos
      End Get

      Set(ByVal Value As String)
         mstrSaltos = Value
      End Set
   End Property

   Public ReadOnly Property Apel() As String
      Get
         Return (txtApel.Text)
      End Get
   End Property

#Region "Propiedades para seteo de columnas"
   Public Property ColFanta() As Boolean
      Get
         Return mbooColFanta
      End Get

      Set(ByVal Value As Boolean)
         mbooColFanta = Value
      End Set
   End Property
   Public Property ColNomb() As Boolean
      Get
         Return mbooColNomb
      End Get
      Set(ByVal Value As Boolean)
         mbooColNomb = Value
      End Set
   End Property
   Public Property ColSociNume() As Boolean
      Get
         Return mbooColSociNume
      End Get
      Set(ByVal Value As Boolean)
         mbooColSociNume = Value
      End Set
   End Property
   Public Property ColLegaNume() As Boolean
      Get
         Return mbooColLegaNume
      End Get
      Set(ByVal Value As Boolean)
         mbooColLegaNume = Value
      End Set
   End Property
   Public Property ColCriaNume() As Boolean
      Get
         Return mbooColCriaNume
      End Get
      Set(ByVal Value As Boolean)
         mbooColCriaNume = Value
      End Set
   End Property
   Public Property ColNoCriaNume() As Boolean
      Get
         Return mbooColNoCriaNume
      End Get
      Set(ByVal Value As Boolean)
         mbooColNoCriaNume = Value
      End Set
   End Property
   Public Property ColCUIT() As Boolean
      Get
         Return mbooColCUIT
      End Get
      Set(ByVal Value As Boolean)
         mbooColCUIT = Value
      End Set
   End Property
   Public Property ColDocuNume() As Boolean
      Get
         Return mbooColDocuNume
      End Get
      Set(ByVal Value As Boolean)
         mbooColDocuNume = Value
      End Set
   End Property
   Public Property ColEnti() As Boolean
      Get
         Return mbooColEnti
      End Get
      Set(ByVal Value As Boolean)
         mbooColEnti = Value
      End Set
   End Property
   Public Property ColMedioPago() As Boolean
      Get
         Return mbooColMedioPago
      End Get
      Set(ByVal Value As Boolean)
         mbooColMedioPago = Value
      End Set
   End Property
   
   Public Property ColEmpr() As Boolean
      Get
         Return mbooColEmpr
      End Get
      Set(ByVal Value As Boolean)
         mbooColEmpr = Value
      End Set
   End Property
   Public Property ColClaveUnica() As Boolean
      Get
         Return mbooColClaveUnica

      End Get
      Set(ByVal Value As Boolean)
         mbooColClaveUnica = Value
      End Set
   End Property
#End Region

#Region "Propiedades para seteo de filtros"
   Public Property FilFanta() As Boolean
      Get
         Return mbooFilFanta
      End Get

      Set(ByVal Value As Boolean)
         mbooFilFanta = Value
      End Set
   End Property
   Public Property FilNomb() As Boolean
      Get
         Return mbooFilNomb
      End Get
      Set(ByVal Value As Boolean)
         mbooFilNomb = Value
      End Set
   End Property
   Public Property FilSociNume() As Boolean
      Get
         Return mbooFilSociNume
      End Get
      Set(ByVal Value As Boolean)
         mbooFilSociNume = Value
      End Set
   End Property
   Public Property FilLegaNume() As Boolean
      Get
         Return mbooFilLegaNume
      End Get
      Set(ByVal Value As Boolean)
         mbooFilLegaNume = Value
      End Set
   End Property
   Public Property FilCriaNume() As Boolean
      Get
         Return mbooFilCriaNume
      End Get
      Set(ByVal Value As Boolean)
         mbooFilCriaNume = Value
      End Set
   End Property
   Public Property FilNoCriaNume() As Boolean
      Get
         Return mbooFilNoCriaNume
      End Get
      Set(ByVal Value As Boolean)
         mbooFilNoCriaNume = Value
      End Set
   End Property
   Public Property FilCUIT() As Boolean
      Get
         Return mbooFilCUIT
      End Get
      Set(ByVal Value As Boolean)
         mbooFilCUIT = Value
      End Set
   End Property
   Public Property FilDocuNume() As Boolean
      Get
         Return mbooFilDocuNume
      End Get
      Set(ByVal Value As Boolean)
         mbooFilDocuNume = Value
      End Set
   End Property
   Public Property FilEnti() As Boolean
      Get
         Return mbooFilEnti
      End Get
      Set(ByVal Value As Boolean)
         mbooFilEnti = Value
      End Set
   End Property
   Public Property FilMedioPago() As Boolean
      Get
         Return mbooFilMedioPago
      End Get
      Set(ByVal Value As Boolean)
         mbooFilMedioPago = Value
      End Set
   End Property
   Public Property FilAgru() As Boolean
      Get
         Return mbooFilAgru
      End Get
      Set(ByVal Value As Boolean)
         mbooFilAgru = Value
      End Set
   End Property
   Public Property FilEmpr() As Boolean
      Get
         Return mbooFilEmpr
      End Get
      Set(ByVal Value As Boolean)
         mbooFilEmpr = Value
      End Set
   End Property
   Public Property FilClaveUnica() As Boolean
      Get
         Return mbooFilClaveUnica

      End Get
      Set(ByVal Value As Boolean)
         mbooFilClaveUnica = Value
      End Set
   End Property

   Public Property FilTipo() As String
      Get
         Return mstrTipo
      End Get
      Set(ByVal Value As String)
         mstrTipo = Value
      End Set
   End Property

   Public Property FilInseId() As String
      Get
         Return mstrInseId
      End Get
      Set(ByVal Value As String)
         mstrInseId = Value
      End Set
   End Property
#End Region
#End Region

#Region "Metodos publicos"
   Public Sub Limpiar()
      Valor = DBNull.Value
   End Sub
#End Region
End Class
End Namespace
