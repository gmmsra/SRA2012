Namespace SRA

Partial Class usrSociosFiltro
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mbooMostrarOrden As Boolean = True
   Private mbooMostrarDatosOpcionales As Boolean = True
#End Region

#Region "Definición de Propiedades"
   Public ReadOnly Property chkTelefonoExt() As System.Web.UI.WebControls.CheckBox
      Get
         Return (chkTelefono)
      End Get
   End Property
   Public ReadOnly Property chkInteExt() As System.Web.UI.WebControls.CheckBox
      Get
         Return (chkInte)
      End Get
   End Property
   Public ReadOnly Property chkDeudaExt() As System.Web.UI.WebControls.CheckBox
      Get
         Return (chkDeuda)
      End Get
   End Property
   Public ReadOnly Property chkCateFechaExt() As System.Web.UI.WebControls.CheckBox
      Get
         Return (chkCateFecha)
      End Get
   End Property
   Public ReadOnly Property chkIngreFechaExt() As System.Web.UI.WebControls.CheckBox
      Get
         Return (chkIngreFecha)
      End Get
   End Property
   Public Property MostrarDatosOpcionales() As Boolean
      Get
         Return mbooMostrarDatosOpcionales
      End Get

      Set(ByVal MostrarDatosOpcionales As Boolean)
         mbooMostrarDatosOpcionales = MostrarDatosOpcionales
      End Set
   End Property
   Public Property MostrarOrden() As Boolean
      Get
         Return mbooMostrarOrden
      End Get

      Set(ByVal MostrarOrden As Boolean)
         mbooMostrarOrden = MostrarOrden
      End Set
   End Property
#End Region

   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me.Page)
         mInicializar()

         If (Not Page.IsPostBack) Then
            txtCateFecha.Fecha = Date.Today
            mCargarCombos()
            mSetearEventos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub mInicializar()
        lblOrden.Visible = mbooMostrarOrden
        cmbOrden.Visible = mbooMostrarOrden
        panDatoOpci.Visible = mbooMostrarDatosOpcionales
   End Sub

   Private Sub mSetearEventos()
      btnPais.Attributes.Add("onclick", "mAgregar('P');return false;")
      btnProv.Attributes.Add("onclick", "mAgregar('R');return false;")
      btnLoca.Attributes.Add("onclick", "mAgregar('L');return false;")
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", lstCategorias, "")
      clsWeb.gCargarRefeCmb(mstrConn, "estados", lstEstados, "", SRA_Neg.Constantes.EstaTipos.EstaTipo_Socios)
      clsWeb.gCargarRefeCmb(mstrConn, "entidades", lstEntidades, "")
      clsWeb.gCargarRefeCmb(mstrConn, "instituciones", lstInstituciones, "")
      clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsam, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "promociones", cmbPromTipo, "T")
      lstInstituciones.Items.Add(New ListItem("( NINGUNA )", "0"))
   End Sub

   Public Function ObtenerIdFiltro() As String

      Dim lstrId As String
      Dim lstrCmd As New StringBuilder

      lstrCmd.Append("exec rpt_filtros_alta")
      lstrCmd.Append(" @rptf_apel_desde=" & clsSQLServer.gFormatArg(txtApelDesde.Text, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_apel_hasta=" & clsSQLServer.gFormatArg(txtApelHasta.Text, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_nume_desde=" & clsSQLServer.gFormatArg(txtNumeDesde.Text, SqlDbType.Int))
      lstrCmd.Append(",@rptf_nume_hasta=" & clsSQLServer.gFormatArg(txtNumeHasta.Text, SqlDbType.Int))
      lstrCmd.Append(",@rptf_cate_fecha=" & clsSQLServer.gFormatArg(txtCateFecha.Text, SqlDbType.SmallDateTime))
      lstrCmd.Append(",@rptf_cate_ids=" & clsSQLServer.gFormatArg(lstCategorias.ObtenerIDs, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_esta_ids=" & clsSQLServer.gFormatArg(lstEstados.ObtenerIDs, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_inst_ids=" & clsSQLServer.gFormatArg(lstInstituciones.ObtenerIDs, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_enti_ids=" & clsSQLServer.gFormatArg(lstEntidades.ObtenerIDs, SqlDbType.VarChar))
        lstrCmd.Append(",@rptf_dist_desde=" & IIf(txtDistritoDesde.Text.Trim.Length = 0, "null", clsSQLServer.gFormatArg(txtDistritoDesde.Text, SqlDbType.Int)))
        lstrCmd.Append(",@rptf_dist_hasta=" & IIf(txtDistritoHasta.Text.Trim.Length = 0, "null", clsSQLServer.gFormatArg(txtDistritoHasta.Text, SqlDbType.Int)))
      lstrCmd.Append(",@rptf_cpos_desde=" & clsSQLServer.gFormatArg(txtCodPostDesde.Text, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_cpos_hasta=" & clsSQLServer.gFormatArg(txtCodPostHasta.Text, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_mail=" & IIf(cmbMail.Valor.ToString = "", "null", clsSQLServer.gFormatArg(cmbMail.Valor.ToString, SqlDbType.Bit)))
      lstrCmd.Append(",@rptf_cate_desde_fecha=" & clsSQLServer.gFormatArg(txtCateFechaDesde.Text, SqlDbType.SmallDateTime))
      lstrCmd.Append(",@rptf_cate_hasta_fecha=" & clsSQLServer.gFormatArg(txtCateFechaHasta.Text, SqlDbType.SmallDateTime))
      lstrCmd.Append(",@rptf_esta_desde_fecha=" & clsSQLServer.gFormatArg(txtEstaFechaDesde.Text, SqlDbType.SmallDateTime))
      lstrCmd.Append(",@rptf_esta_hasta_fecha=" & clsSQLServer.gFormatArg(txtEstaFechaHasta.Text, SqlDbType.SmallDateTime))
      lstrCmd.Append(",@rptf_ingr_desde_fecha=" & clsSQLServer.gFormatArg(txtIngrFechaDesde.Text, SqlDbType.SmallDateTime))
      lstrCmd.Append(",@rptf_ingr_hasta_fecha=" & clsSQLServer.gFormatArg(txtIngrFechaHasta.Text, SqlDbType.SmallDateTime))
      If txtCuotDesd.Text = "" Then
         lstrCmd.Append(",@rptf_cuotas_desde=null")
      Else
         lstrCmd.Append(",@rptf_cuotas_desde=" & clsSQLServer.gFormatArg(txtCuotDesd.Valor.ToString, SqlDbType.Int))
      End If
      If txtCuotHast.Text = "" Then
         lstrCmd.Append(",@rptf_cuotas_hasta=null")
      Else
         lstrCmd.Append(",@rptf_cuotas_hasta=" & clsSQLServer.gFormatArg(txtCuotHast.Valor.ToString, SqlDbType.Int))
      End If
      lstrCmd.Append(",@rptf_asam_id=" & clsSQLServer.gFormatArg(cmbAsam.Valor.ToString, SqlDbType.Int))
      lstrCmd.Append(",@rptf_correo=" & IIf(cmbCorr.Valor.ToString = "T", "null", clsSQLServer.gFormatArg(cmbCorr.Valor.ToString, SqlDbType.Bit)))
      lstrCmd.Append(",@rptf_prom=" & IIf(cmbProm.Valor.ToString = "T", "null", clsSQLServer.gFormatArg(cmbProm.Valor.ToString, SqlDbType.Bit)))
      lstrCmd.Append(",@rptf_prom_id=" & clsSQLServer.gFormatArg(cmbPromTipo.Valor.ToString, SqlDbType.Int))
      lstrCmd.Append(",@rptf_daut=" & IIf(cmbDA.Valor.ToString = "T", "null", clsSQLServer.gFormatArg(cmbDA.Valor.ToString, SqlDbType.Bit)))
      lstrCmd.Append(",@rptf_tarj_id=" & clsSQLServer.gFormatArg(cmbTarj.Valor.ToString, SqlDbType.Int))
      lstrCmd.Append(",@rptf_pais_ids=" & clsSQLServer.gFormatArg(hdnSelePais.Text, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_prov_ids=" & clsSQLServer.gFormatArg(hdnSeleProv.Text, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_loca_ids=" & clsSQLServer.gFormatArg(hdnSeleLoca.Text, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_deuda_desde_anio=" & clsSQLServer.gFormatArg(txtDeudAnio.Text, SqlDbType.Int))
      lstrCmd.Append(",@rptf_pago_anual=" & clsSQLServer.gFormatArg(txtPagoAnual.Text, SqlDbType.Int))
      lstrCmd.Append(",@rptf_orden=" & clsSQLServer.gFormatArg(cmbOrden.Valor.ToString, SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_cate_fecha_mostrar=" & clsSQLServer.gFormatArg(IIf(chkCateFecha.Checked, "S", "N"), SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_ingre_fecha_mostrar=" & clsSQLServer.gFormatArg(IIf(chkIngreFecha.Checked, "S", "N"), SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_con_telefono=" & clsSQLServer.gFormatArg(IIf(chkTelefono.Checked, "S", "N"), SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_deuda=" & clsSQLServer.gFormatArg(IIf(chkDeuda.Checked, "S", "N"), SqlDbType.VarChar))
      lstrCmd.Append(",@rptf_interes=" & clsSQLServer.gFormatArg(IIf(chkInte.Checked, "S", "N"), SqlDbType.VarChar))

      lstrId = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd.ToString)
      Return (lstrId)
   End Function

   Public Sub Limpiar()
      txtApelDesde.Text = ""
      txtApelHasta.Text = ""
      txtNumeDesde.Text = ""
      txtNumeHasta.Text = ""
      txtDistritoDesde.Text = ""
      txtDistritoHasta.Text = ""
      txtCodPostDesde.Text = ""
      txtCodPostHasta.Text = ""
      txtCateFecha.Fecha = Date.Today
      lstCategorias.Limpiar()
      lstEstados.Limpiar()
      lstEntidades.Limpiar()
      lstInstituciones.Limpiar()
      lisPais.Limpiar()
      lisProv.Limpiar()
      lisLoca.Limpiar()
      cmbPagoAnual.Limpiar()
      cmbDeudAnio.Limpiar()
      txtPagoAnual.Text = ""
      txtDeudAnio.Text = ""
      hdnSelePais.Text = ""
      hdnSeleProv.Text = ""
      hdnSeleLoca.Text = ""
      chkCateFecha.Checked = False
      chkIngreFecha.Checked = False
      cmbMail.Limpiar()
      cmbAsam.Limpiar()
      cmbTarj.Limpiar()
      cmbProm.Limpiar()
      cmbPromTipo.Limpiar()
      cmbDA.Limpiar()
      cmbCorr.Limpiar()
      txtCateFechaDesde.Text = ""
      txtCateFechaHasta.Text = ""
      txtEstaFechaDesde.Text = ""
      txtEstaFechaHasta.Text = ""
      txtIngrFechaDesde.Text = ""
      txtIngrFechaHasta.Text = ""
      txtCuotDesd.Text = ""
      txtCuotHast.Text = ""
   End Sub
End Class
End Namespace
