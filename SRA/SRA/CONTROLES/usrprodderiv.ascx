<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrProdDeriv" CodeFile="usrProdDeriv.ascx.vb" %>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD align="left" colspan="3" width="100%" vAlign="top" style="HEIGHT: 9px">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">
						<asp:label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;</td>
					<td align="left" Width="230" nowrap>
						<cc1:combobox class="combo" Obligatorio="false" id="cmbProdRaza" onchange="cmbRaza_change(this)"
							runat="server" cssclass="cuadrotexto" Width="99%" AceptaNull="True" Height="20px" NomOper="razas_cargar"
							MostrarBotones="False" filtra="true"></cc1:combobox></td>
					<td align="left"><cc1:textboxtab id="txtRazaId" Visible="False" runat="server"></cc1:textboxtab></td>
					<td align="left">
						<asp:Panel id="panSexo" Width="65px" runat="server">
							<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
								<TR>
									<TD>
										<cc1:combobox id="cmbProdSexo" class="combo" runat="server" AceptaNull="true" Width="97%" onchange="cmbProdSexo_change(this)">
											<asp:ListItem Value="" Selected="True">(Sexo)</asp:ListItem>
											<asp:ListItem Value="0">Hembra</asp:ListItem>
											<asp:ListItem Value="1">Macho</asp:ListItem>
										</cc1:combobox></TD>
								</TR>
							</TABLE>
						</asp:Panel>
					</td>

				</tr>
			</table>
		</TD>
	</TR>
	<TR>
		<td width="100%">
			<table id="tabNumExtranjero" runat="server" style="DISPLAY: inline" cellSpacing="0" cellPadding="0"
				width="100%" border="0">
				<TR width="100%">
					<td align="left" width="20%"><asp:label id="lblNume" runat="server" cssclass="titulo">Asoc.Extranjera:</asp:label></td>
					<td align="left" Width="90%">
						<cc1:combobox class="combo" id="cmbProdAsoc" onchange="Combo_change(this)" Width="98%" AceptaNull="True"
							cssclass="cuadrotexto" MostrarBotones="False" Filtra="True" runat="server"></cc1:combobox></td>
				</TR>
				<TR width="100%">
					<td align="left" width="20%"><asp:label id="Label1" runat="server" cssclass="titulo">Nro.Extr.:</asp:label></td>
					<td align="left" Width="5%" valign="top">
						<cc1:TEXTBOXTAB id="txtCodi" MaxLength="20" runat="server" cssclass="cuadrotexto" Width="170"></cc1:TEXTBOXTAB></td>
				</TR>
			</table>
		</td>
	<TR>
		<TD align="left" width="100%" style="HEIGHT: 11px" vAlign="top">
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<td align="left">
						<asp:Panel Visible="False" id="panRP" Width="100px" runat="server">
							<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
								<TR>
									<TD>
										<asp:label id="lblRP" cssclass="titulo" runat="server">RP:</asp:label></TD>
									<TD width="100%">
										<cc1:textboxtab id="txtRP" cssclass="cuadrotexto" runat="server" Width="95%" MaxLength="20"></cc1:textboxtab></TD>
								</TR>
							</TABLE>
						</asp:Panel>
					</td>
					<TD align="right" nowrap>
						<asp:label id="lblSraNume" runat="server" cssclass="titulo">Nro:</asp:label>
					</TD>
					<TD align="left" Width="15%">
						<cc1:numberbox MaxLength="9" id="txtSraNume" runat="server" cssclass="cuadrotexto" Width="60"></cc1:numberbox>
					</TD>
					<TD align="right"><asp:label id="lblApel" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
					<TD align="left" Width="85%"><cc1:textboxtab id="txtProdNomb" runat="server" cssclass="textolibre" Rows="1" TextMode="MultiLine"
							Width="99%"></cc1:textboxtab></TD>
				</TR>
			</TABLE>
		</TD>
		<TD align="right" style="WIDTH: 2px; HEIGHT: 11px"></TD>
		<TD align="right" style="WIDTH: 100px; HEIGHT: 11px" vAlign="top">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><IMG language="javascript" id="imgBusc" style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							src="..\imagenes\Buscar16.gif" runat="server">&nbsp;</TD>
					<TD><IMG language="javascript" id="imgBuscAvan" style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif" runat="server">&nbsp;</TD>
					<TD><IMG language="javascript" id="imgLimp" style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp" runat="server">&nbsp;</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD colSpan="3">
			<div class="paneledicion" style='Z-INDEX: 101; POSITION: absolute; WIDTH: 360px; HEIGHT: 231px; VISIBILITY: hidden'
				id="panBuscAvan<%=mstrCtrlId%>">
				<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
					<tr>
						<td colspan="2" align="center" valign="middle" height="34"><span class="titulo">B�squeda 
								Avanzada</span><input type="hidden" size="1" name="hdnBusc<%=mstrCtrlId%>" value="D"></td>
					</tr>
					<tr>
						<td width="20"></td>
						<td>
							<SPAN id="spRpNume" runat="server">
								<asp:label id="lblRpNume" runat="server" cssclass="titulo">&nbsp;Nro.RP:</asp:label>&nbsp;<cc1:numberbox id="txtRpNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;</SPAN>
							<span runat="server" id="br1">
								<br>
							</span><SPAN id="spCriaNume" style="DISPLAY: inline; CLEAR: none" runat="server">
								<asp:label id="lblCriaRaza" Visible="true" runat="server" cssclass="titulo">&nbsp;Raza:</asp:label>&nbsp;<cc1:combobox tag="N" class="combo" id="cmbCriaNumeRaza" runat="server" Width="197px" onchange="Combo_change(this)"></cc1:combobox>
								<asp:label id="lblCriaNume" runat="server" cssclass="titulo">&nbsp;Nro.Criador:&nbsp;</asp:label><cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></SPAN>
							<span runat="server" id="br2">
								<br>
							</span><SPAN id="spLaboNume" style="DISPLAY: inline" runat="server">
								<asp:label id="lblLaboNume" runat="server" cssclass="titulo">&nbsp;Nro.Tr�m.Laborat.:&nbsp;</asp:label><cc1:numberbox id="txtLaboNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox></SPAN>
							<span runat="server" id="br3">
								<br>
							</span><SPAN id="spNaciFecha" runat="server">
								<asp:label id="lblNaciFecha" runat="server" cssclass="titulo">&nbsp;F.Nacim.:&nbsp;</asp:label>
								<cc1:DateBox id="txtNaciFecha" Width="70px" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
									IncludesUrl="Includes/"></cc1:DateBox></SPAN> <span runat="server" id="br4">
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" valign="middle" height="40">
							<input type=button class=boton value=Aceptar onclick="javascript:mAceptarProd(<%=mstrCtrlId%>)" id="btnBuscAcep<%=mstrCtrlId%>">&nbsp;&nbsp;
							<input type=button class=boton value=Cancelar onclick="javascript:mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>');"id="btnBuscCanc<%=mstrCtrlId%>">
						</td>
					</tr>
				</table>
			</div>
			<DIV style="DISPLAY: none">
				<cc1:textboxtab id="hdnRazaCruza" runat="server" autopostback="true"></cc1:textboxtab>
				<cc1:textboxtab id="hdnValiExis" runat="server" autopostback="true"></cc1:textboxtab>
				<asp:textbox id="hdnCriaNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnProp" runat="server"></asp:textbox>
				<asp:textbox id="txtRegtId" runat="server"></asp:textbox>
				<asp:label id="lblId" runat="server">Cliente</asp:label>
				<cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab>
				<cc1:textboxtab id="txtSexoId" size="4" runat="server"></cc1:textboxtab>
				<cc1:textboxtab id="txtRazaSessId" runat="server"></cc1:textboxtab>
				<asp:label id="lblIdAnt" runat="server"></asp:label>
				<asp:textbox id="txtCriaId" runat="server"></asp:textbox>
				<asp:textbox id="txtIgnoraInexist" runat="server"></asp:textbox>
			</DIV>
		</TD>
	</TR>
	<TR>
		<TD align="right" colSpan="3">
			<asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Rows="2" TextMode="MultiLine"
				Width="100%" Enabled="False"></asp:textbox></TD>
	</TR>
</TABLE>
<script language="javascript">
	function mValidaConsultaProd(pCtrl)
	{
		if (document.all(pCtrl+':cmbCriaNumeRaza')!=null)
		{
			var lstrCriaNumeRaza = document.all(pCtrl+':cmbCriaNumeRaza').value;
			var lstrCriaNume = document.all(pCtrl+':txtCriaNume').value;
			if ((lstrCriaNumeRaza == '' && lstrCriaNume != '')  || (lstrCriaNumeRaza != '' && lstrCriaNume == '') )
			{
				alert("Debe indicar la raza y el numero de criador.")
				return(false)
			}
		}
		return(true)	
	}

	function mAceptarProd(pCtrl)
	{
		if (mValidaConsultaProd(pCtrl))
			mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1',pCtrl);
	}
	
	function cmbProdRaza_onchange(pCtrl)
	{	
		mSetearDeno(document.all(pCtrl));
	}
	
	function mSetearDeno(pRaza)
   	{   	
   		var strFiltro = pRaza.value;
	    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
	    var strCtrol = pRaza.id.replace('cmbProdRaza','lblSraNume');
	    if (document.all(strCtrol)!=null)
		{
			if (strRet!='')
	 			document.all(strCtrol).innerText = strRet+":";
 			else
 				document.all(strCtrol).innerText = "Nro.:";
		}
	}
</script>
