<%--<%@ Reference Control="~/controles/usrcliederiv.ascx" %>--%>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrProducto" CodeFile="usrProducto.ascx.vb" %>
<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
	<TBODY>
		<TR>
			<TD style="HEIGHT: 9px" vAlign="top" width="100%" colSpan="3" align="left">
				<table border="0" cellSpacing="0" cellPadding="0">
					<TBODY>
						<tr id="trRazaCriador" runat="server">
							<td align="right"><asp:label id="lblCriaOrProp" runat="server" cssclass="titulo">Propietario:</asp:label>&nbsp;</td>
							<td width="820" align="left">
								<UC1:CLIE id="usrCriadorFil" runat="server" AceptaNull="false" Tabla="Criadores" Saltos="1,2"
									FilDocuNume="True" Ancho="820" MuestraDesc="False" FilTipo="S" FilSociNume="True" AutoPostBack="False"
									Criador="True" MuestraBotonBusquedaAvanzada="False"></UC1:CLIE></td>
						</tr>
						<tr id="trSexo" runat="server">
							<td align="right">
								<asp:label id="Label3" runat="server" cssclass="titulo">Sexo:</asp:label>&nbsp;</td>
							<td align="left">
								<cc1:combobox id="cmbProdSexo" class="combo" runat="server" AceptaNull="true" Width="70px" onchange="cmbProdSexo_change(this)">
									<asp:ListItem Value="" Selected="True">(Sexo)</asp:ListItem>
									<asp:ListItem Value="0">Hembra</asp:ListItem>
									<asp:ListItem Value="1">Macho</asp:ListItem>
								</cc1:combobox>&nbsp;
								<asp:label id="lblRP" runat="server" cssclass="titulo">RP:</asp:label>&nbsp;
								<cc1:textboxtab id="txtRP" runat="server" cssclass="cuadrotexto" Width="150" MaxLength="20"></cc1:textboxtab>&nbsp;
								<asp:label id="lblSraNume" runat="server" cssclass="titulo">Nro:</asp:label>&nbsp;
								<cc1:numberbox id="txtSraNume" runat="server" cssclass="cuadrotexto" Width="80" MaxLength="9"></cc1:numberbox></td>
						</tr>
						<tr id="trAsocExtranjera" runat=server>
							<td align="right">
								<asp:label id="lblNume" runat="server" cssclass="titulo">Asoc.Extr.:</asp:label>&nbsp;</td>
							<td align="left">
								<cc1:combobox id="cmbProdAsoc" class="combo" runat="server" cssclass="cuadrotexto" AceptaNull="True"
									Width="500" onchange="Combo_change(this)" Filtra="True" MostrarBotones="False"></cc1:combobox></td>
						</tr>
						<tr id="trNroExtr"  runat=server>
							<td align="right">
								<asp:label id="Label1" runat="server" cssclass="titulo">Nro.Extr.:</asp:label>&nbsp;</td>
							<td align="left">
								<cc1:textboxtab id="txtCodi" runat="server" cssclass="cuadrotexto" Width="150" MaxLength="20"></cc1:textboxtab></td>
						</tr>
						<tr id="trNroCondicional" runat="server">
							<td align="right">
								<asp:label id="lblNroCondicional" runat="server" cssclass="titulo">Nro.Condicional:</asp:label>&nbsp;</td>
							<td align="left">
								<cc1:numberbox id="txtNroCondicional" runat="server" cssclass="cuadrotexto" Width="80" MaxLength="9"></cc1:numberbox></td>
						</tr>
						<tr>
							<td align="right">
								<asp:label id="lblApel" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</td>
							<td align="left">
								<cc1:textboxtab id="txtProdNomb" runat="server" cssclass="textolibre" Width="500" Rows="1" Height="20"
									TextMode="MultiLine"></cc1:textboxtab>
								<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
									id="imgBusc" language="javascript" src="..\imagenes\Buscar16.gif" runat="server">
								<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
									id="imgLimp" language="javascript" alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp"
									runat="server"> <IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
									id="imgBuscAvan" language="javascript" alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif" runat="server">
							</td>
						</tr>
						<tr id="trDescrip" runat="server">
							<td align="right"><asp:label id="lblDescrip" runat="server" cssclass="titulo">Descrip.:</asp:label>&nbsp;</td>
							<td align="left"><asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Width="95%" Rows="2" TextMode="MultiLine"></asp:textbox></td>
						</tr>
					</TBODY>
				</table>
			</TD>
		</TR>
		<TR>
			<TD colSpan="3">
				<div 
      style="Z-INDEX: 101; POSITION: absolute; WIDTH: 360px; HEIGHT: 231px; VISIBILITY: hidden" 
      id="panBuscAvan<%=mstrCtrlId%>" class=paneledicion>
					<table border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
						<tr>
							<td height="34" vAlign="middle" colSpan="2" align="center"><span class="titulo">B�squeda 
									Avanzada</span><input value=D size=1 
            type=hidden name="hdnBusc<%=mstrCtrlId%>"></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td><SPAN id="spRpNume" runat="server"><asp:label id="lblRpNume" runat="server" cssclass="titulo">&nbsp;Nro.RP:</asp:label>&nbsp;<cc1:numberbox id="txtRpNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;</SPAN>
								<span id="br1" runat="server">
									<br>
								</span><SPAN style="DISPLAY: inline; CLEAR: none" id="spCriaNume" runat="server">
									<asp:label id="lblCriaRaza" runat="server" cssclass="titulo" Visible="true">&nbsp;Raza:</asp:label>&nbsp;<cc1:combobox id="cmbCriaNumeRaza" class="combo" runat="server" Width="197px" onchange="Combo_change(this)"
										tag="N"></cc1:combobox>
									<asp:label id="lblCriaNume" runat="server" cssclass="titulo">&nbsp;Nro.Criador:&nbsp;</asp:label><cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></SPAN><span id="br2" runat="server"><br>
								</span><SPAN style="DISPLAY: inline" id="spLaboNume" runat="server">
									<asp:label id="lblLaboNume" runat="server" cssclass="titulo">&nbsp;Nro.Tr�m.Laborat.:&nbsp;</asp:label><cc1:numberbox id="txtLaboNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox></SPAN><span id="br3" runat="server"><br>
								</span><SPAN id="spNaciFecha" runat="server">
									<asp:label id="lblNaciFecha" runat="server" cssclass="titulo">&nbsp;F.Nacim.:&nbsp;</asp:label><cc1:datebox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
										IncludesUrl="Includes/"></cc1:datebox></SPAN><span id="br4" runat="server"></span></td>
						</tr>
						<tr>
							<td height="40" vAlign="middle" colSpan="2" align="center"><input id="btnBuscAcep<%=mstrCtrlId%>" class=boton onclick="javascript:mAceptarProd(<%=mstrCtrlId%>)" value=Aceptar type=button>&nbsp;&nbsp;
								<input id="btnBuscCanc<%=mstrCtrlId%>" class=boton onclick="javascript:mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>');"value=Cancelar type=button>
							</td>
						</tr>
					</table>
				</div>
				<DIV style="DISPLAY: none">
					<cc1:textboxtab id="hdnRazaCruza" runat="server" autopostback="true"></cc1:textboxtab>
					<cc1:textboxtab id="hdnValiExis" runat="server" autopostback="true"></cc1:textboxtab>
					<asp:textbox id="hdnCriaNume" runat="server"></asp:textbox>
					<asp:textbox id="hdnCriaOrProp" runat="server"></asp:textbox>
					<asp:textbox id="txtRegtId" runat="server"></asp:textbox>
					<asp:label id="lblId" runat="server">Cliente</asp:label>
					<cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab>
					<cc1:textboxtab id="txtSexoId" runat="server" size="4"></cc1:textboxtab>
					<cc1:textboxtab id="txtRazaSessId" runat="server"></cc1:textboxtab>
					<asp:label id="lblIdAnt" runat="server"></asp:label>
					<asp:textbox id="txtCriaId" runat="server"></asp:textbox>
					
					<asp:textbox id="txtIgnoraInexist" runat="server"></asp:textbox>
				</DIV>
			</TD>
		</TR>
	</TBODY>
</TABLE>
<script language="javascript">
	/*function mValida(pCtrl)
	{
		var oper = document.all(pCtrl + "_lblCriaOrProp").innerHTML //Obtengo el texto del label lblCriaOrProp.
		var sCriaOrProp = oper.replace(/:/gi,""); //Reemplazo el ":" por "" en toda la cadena.	
		
		//Valido que haya completado el control de Raza - Criador/Propietario.
		if (document.all(pCtrl + ":usrCriadorFil:txtId").value == "")
			{
				alert("Debe indicar la Raza y el " + sCriaOrProp + ".");
				return(false);
			}
			
		return(true);
	}
	*/
	function mValida(pCtrl)
	{
		var oper = document.all(pCtrl + "_lblCriaOrProp").innerHTML //Obtengo el texto del label lblCriaOrProp.
		var sCriaOrProp = oper.replace(/:/gi,""); //Reemplazo el ":" por "" en toda la cadena.	
		
		//Valido que haya completado el control de Raza - Criador/Propietario.
		if (document.all(pCtrl + ":usrCriadorFil:cmbRazaCria").value == "")
			{
				alert("Debe indicar la Raza .");
				return(false);
			}
			
		return(true);
	}

	function mAceptarProd(pCtrl)
	{
		if (mValidaConsultaProd(pCtrl))
			mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1',pCtrl);
	}
</script>
