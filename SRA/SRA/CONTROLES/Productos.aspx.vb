Imports System.Data.SqlClient
Imports SRA

Public Class Productos
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    '   Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
    '   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    '   Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton
    '   Protected WithEvents btnAlta As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnModi As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnLimp As System.Web.UI.WebControls.Button
    '   Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    '   Protected WithEvents panBotones As System.Web.UI.WebControls.Panel
    '   Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents usrCria As usrClieDeriv
    '   Protected WithEvents usrCriaFil As usrClieDeriv
    '   Protected WithEvents usrPropFil As usrClieDeriv
    '   Protected WithEvents usrMadreFil As usrProdDeriv
    '   Protected WithEvents usrPadreFil As usrProdDeriv
    '   Protected WithEvents usrReceFil As usrProdDeriv
    '   Protected WithEvents hdnId As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents hdnSRA As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents hdnValorId As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    '   Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    '   Protected WithEvents panFiltros As System.Web.UI.WebControls.Panel
    '   Protected WithEvents lblNombFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtNombFil As NixorControls.TextBoxTab
    '   Protected WithEvents lblBaja As System.Web.UI.WebControls.Label
    '   Protected WithEvents btnBuscar As NixorControls.BotonImagen
    '   Protected WithEvents btnLimpiarFil As NixorControls.BotonImagen
    '   Protected WithEvents cmbActiProp As NixorControls.ComboBox
    '   Protected WithEvents chkBusc As System.Web.UI.WebControls.CheckBox
    '   Protected WithEvents chkBaja As System.Web.UI.WebControls.CheckBox
    '   Protected WithEvents hdnModi As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    '   Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents hdnDatosPop As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblNumeFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbSexoFil As NixorControls.ComboBox
    '   Protected WithEvents lblSexoFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblNaciFechaFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblRPNumeFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblCtroFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbCtroFil As NixorControls.ComboBox
    '   Protected WithEvents txtNaciFechaDesdeFil As NixorControls.DateBox
    '   Protected WithEvents txtNaciFechaHastaFil As NixorControls.DateBox
    '   Protected WithEvents lblFechaHastaFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblLaboNumeFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblAsoNumeFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblCtrolFactuNumeFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtCtrolFactuNumeFil As NixorControls.NumberBox
    '   Protected WithEvents txtLaboNumeFil As NixorControls.NumberBox
    '   Protected WithEvents lblMadreFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblPadreFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblPropFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblReceFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    '   Protected WithEvents txtsraNumeFil As NixorControls.TextBoxTab
    '   Protected WithEvents txtRPNumeFil As NixorControls.TextBoxTab
    '   Protected WithEvents txtAsoNumeFil As NixorControls.TextBoxTab
    '   Protected WithEvents txtNumeFil As NixorControls.TextBoxTab
    '   Protected WithEvents cmbAsocFil As NixorControls.ComboBox
    '   Protected WithEvents panCabecera As System.Web.UI.WebControls.Panel
    '   Protected WithEvents lblSraNume As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtSraNume As NixorControls.NumberBox
    '   Protected WithEvents lblNomb As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtNomb As NixorControls.TextBoxTab
    '   Protected WithEvents lblApod As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtApod As NixorControls.TextBoxTab
    '   Protected WithEvents lblRP As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtRP As NixorControls.TextBoxTab
    '   Protected WithEvents txtRPNume As NixorControls.NumberBox
    '   Protected WithEvents lblRaza As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbRaza As NixorControls.ComboBox
    '   Protected WithEvents lblSexo As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbSexo As NixorControls.ComboBox
    '   Protected WithEvents lblPelaA As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbPelaA As NixorControls.ComboBox
    '   Protected WithEvents lblPelaB As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbPelaB As NixorControls.ComboBox
    '   Protected WithEvents lblPelaC As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbPelaC As NixorControls.ComboBox
    '   Protected WithEvents lblPelaD As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbPelaD As NixorControls.ComboBox
    '   Protected WithEvents chkTranEmbr As System.Web.UI.WebControls.CheckBox
    '   Protected WithEvents lblDona As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtDona As NixorControls.NumberBox
    '   Protected WithEvents lblPesoNacer As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtPesoNacer As NixorControls.NumberBox
    '   Protected WithEvents lblPesoDeste As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtPesoDeste As NixorControls.NumberBox
    '   Protected WithEvents lblPesoFinal As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtPesoFinal As NixorControls.NumberBox
    '   Protected WithEvents lblEpdNacer As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtEpdNacer As NixorControls.NumberBox
    '   Protected WithEvents lblEpdDeste As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtEpdDeste As NixorControls.NumberBox
    '   Protected WithEvents lblEpdFinal As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtEpdFinal As NixorControls.NumberBox
    '   Protected WithEvents lblCria As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblTranFecha As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtTranFecha As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblRegiTipo As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbRegiTipo As NixorControls.ComboBox
    '   Protected WithEvents lblPX As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtPX As NixorControls.TextBoxTab
    '   Protected WithEvents lblMelli As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtMelli As NixorControls.NumberBox
    '   Protected WithEvents lblSiete As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtSiete As NixorControls.NumberBox
    '   Protected WithEvents lblInscFecha As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtInscFecha As NixorControls.DateBox
    '   Protected WithEvents lblNaciFecha As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtNaciFecha As NixorControls.DateBox
    '   Protected WithEvents lblFalleFecha As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtFalleFecha As NixorControls.DateBox
    '   Protected WithEvents lblEsta As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtEsta As System.Web.UI.WebControls.Label
    '   Protected WithEvents lnkAna As System.Web.UI.WebControls.LinkButton
    '   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
    '   Protected WithEvents btnAltaAna As System.Web.UI.WebControls.Button
    '   Protected WithEvents cmbAnaResul As NixorControls.ComboBox
    '   Protected WithEvents lblAnaResul As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblAnaFecha As System.Web.UI.WebControls.Label
    '   Protected WithEvents lblAnaNume As System.Web.UI.WebControls.Label
    '   Protected WithEvents grdAna As System.Web.UI.WebControls.DataGrid
    '   Protected WithEvents panAna As System.Web.UI.WebControls.Panel
    '   Protected WithEvents lblAnaFechaResul As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtAnaFechaResul As NixorControls.DateBox
    '   Protected WithEvents txtAnaNume As NixorControls.NumberBox
    '   Protected WithEvents hdnAnaId As System.Web.UI.WebControls.TextBox
    '   Protected WithEvents txtAnaFecha As NixorControls.DateBox
    '   Protected WithEvents btnBajaAna As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnModiAna As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnLimpAna As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnAsoc As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnRela As System.Web.UI.WebControls.Button
    '   Protected WithEvents btnProp As System.Web.UI.WebControls.Button
    '   Protected WithEvents lblApodo As System.Web.UI.WebControls.Label
    '   Protected WithEvents txtApodoFil As NixorControls.TextBoxTab
    '   Protected WithEvents lblAnaTipo As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbAnaTipo As NixorControls.ComboBox
    '   Protected WithEvents btnTramites As System.Web.UI.HtmlControls.HtmlButton
    '   Protected WithEvents btnServicios As System.Web.UI.HtmlControls.HtmlButton
    '   Protected WithEvents btnParejas As System.Web.UI.HtmlControls.HtmlButton
    '   Protected WithEvents btnPremios As System.Web.UI.HtmlControls.HtmlButton
    '   Protected WithEvents btnEstados As System.Web.UI.HtmlControls.HtmlButton
    '   Protected WithEvents btnPerform As System.Web.UI.HtmlControls.HtmlButton
    '   Protected WithEvents btnPedigree As System.Web.UI.HtmlControls.HtmlButton
    'Protected WithEvents hdnEspe As System.Web.UI.WebControls.TextBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mdsDatos As DataSet
    Private mstrds As String
    Private mdsDatosAux As DataSet
    Private mstrConn As String
    Public mstrProdCtrl As String
    Private mstrProdId As String
    Private mstrValorId As String
    Public mbooActi As Boolean
    Private mbooSoloBusq As Boolean
    'campos a filtrar
    Private mstrFilProp As String
    Private mstrFilSraNume As String
    Private mstrFilCriaNume As String
    Private mstrFilRpNume As String
    Private mstrFilNoCriaNume As String
    Private mstrFilCuit As String
    Private mstrFilLaboNume As String
    Private mstrFilTarj As String
    Private mstrFilEnti As String
    Private mstrFilMedioPago As String
    Private mstrFilAgru As String
    Private mstrFilEmpr As String
    Private mstrFilNaciFecha As String
    Private mstrFilRespa As String
    Private mstrFilProd As String
    Private mstrFilMadre As String
    Private mstrFilPadre As String
    Private mstrFilRece As String
    Private mstrFilSexo As String
    Private mstrFilHabiRaza As String
    'valores de los filtros 
    Private mstrClieNume As String
    Private mstrNume As String
    Private mstrNomb As String
    Private mstrRaza As String
    Private mstrCriaNume As String
    Private mstrSexo As String
    'columnas a mostrar 
    Private mstrColNomb As String
    Private mstrColSraNume As String
    Private mstrColRpNume As String
    Private mstrColCriaNume As String
    Private mstrColLaboNume As String
    Private mstrColAsocNume As String
    Private mstrColNaciFecha As String
    'columnas de la grilla de resultados
    Private mintGrdProdEdit As Integer = 0
    Private mintGrdProdSele As Integer = 1
    Private mintGrdProdId As Integer = 2
    Private mintGrdProdRaza As Integer = 3
    Private mintGrdProdSexo As Integer = 4
    Private mintGrdProdNomb As Integer = 5
    Private mintGrdProdAsoc As Integer = 6
    Private mintGrdProdNume As Integer = 7
    Private mintGrdProdRpNume As Integer = 8
    Private mintGrdProdCria As Integer = 9
    Private mintGrdProdNaci As Integer = 10
    Private mintGrdProdEsta As Integer = 11

    Private Enum Columnas As Integer
        ProdSele = 0
        ProdEdit = 1
        ProdId = 2
        ProdRazaDesc = 3
        ProdSexo = 4
        ProdNomb = 5
        ProdAsoc = 6
        ProdNume = 7
        ProdDesc = 8
        ProdRaza = 13
        ProdRazaCodi = 14
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lstrFil As String
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializarControl()
            mInicializar()
            If (Not Page.IsPostBack) Then
                If mstrProdCtrl = "" Then
                    Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                End If

                Session(mSess(mstrTabla)) = Nothing

                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()

                If mstrProdId <> "" Then
                    mCargarDatos(mstrProdId)
                Else
                    If mstrProdCtrl <> "" Then
                        mMostrarPanel(False)
                        lstrFil = usrCriaFil.Valor & txtNombFil.Text & txtAsoNumeFil.Text & txtRPNumeFil.Text & txtNaciFechaDesdeFil.Text & txtNaciFechaHastaFil.Text & txtLaboNumeFil.Text & cmbRazaFil.SelectedValue.ToString & cmbSexoFil.SelectedValue.ToString & cmbAsocFil.SelectedValue.ToString
                        If lstrFil <> "" And lstrFil <> "0" Then
                            mConsultar()
                        End If
                    Else
                        mMostrarPanel(False)
                    End If
                End If
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mSess(mstrTabla))
                    Dim x As String = Session.SessionID
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()

        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "T")
        SRA_Neg.Utiles.gSetearRaza(cmbRaza)
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
        clsWeb.gCargarRefeCmb(mstrConn, "implante_ctros", cmbCtroFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "asociaciones", cmbAsocFil, "T")
        cmbAsocFil.Valor = hdnSRA.Text


        clsWeb.gCargarRefeCmb(mstrConn, "rg_analisis_resul", cmbAnaResul, "S")

        'inicializar valores
        If mValorParametro(Request.QueryString("Raza")) <> "" Then
            cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
        End If
        'If mValorParametro(Request.QueryString("Asoc")) <> "" Then
        cmbAsocFil.Valor = mValorParametro(Request.QueryString("Asoc"))
        'End If
        If mValorParametro(Request.QueryString("Sexo")) <> "" Then
            cmbSexoFil.Valor = mValorParametro(Request.QueryString("Sexo"))
        End If

    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnProp.Attributes.Add("onclick", "mPropietarios();return false;")
        btnRela.Attributes.Add("onclick", "mRelaciones();return false;")
        btnAsoc.Attributes.Add("onclick", "mAsociaciones();return false;")
    End Sub

    Private Sub mSetearMaxLength()

        Dim lstrProdLong As Object
        Dim lintCol As Integer

        lstrProdLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtSraNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_nomb")
        txtApod.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_apodo")
        txtRP.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_rp")
        txtRPNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtDona.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtPesoNacer.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtPesoDeste.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtPesoFinal.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtEpdNacer.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_epd_nacer")
        txtEpdDeste.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_epd_deste")
        txtEpdFinal.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_epd_final")

        txtNombFil.MaxLength = txtNomb.MaxLength
        txtNumeFil.MaxLength = txtSraNume.MaxLength
        txtRPNumeFil.MaxLength = txtRP.MaxLength
        txtLaboNumeFil.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtAsoNumeFil.MaxLength = txtRP.MaxLength
        txtAnaNume.MaxLength = txtLaboNumeFil.MaxLength

    End Sub

#End Region

#Region "Inicializacion de Variables"

    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        hdnSRA.Text = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")

        usrCriaFil.Tabla = mstrCriadores
        usrCriaFil.AutoPostback = False
        usrCriaFil.FilClaveUnica = False
        usrCriaFil.ColClaveUnica = False
        usrCriaFil.Ancho = 790
        usrCriaFil.Alto = 510
        usrCriaFil.ColDocuNume = False
        usrCriaFil.ColCUIT = False
        usrCriaFil.ColClaveUnica = False
        usrCriaFil.FilClaveUnica = True
        usrCriaFil.FilAgru = False
        usrCriaFil.FilCriaNume = True
        usrCriaFil.FilCUIT = False
        usrCriaFil.FilDocuNume = True
        usrCriaFil.FilTarjNume = False
        usrCriaFil.Criador = True

        usrPropFil.Tabla = mstrClientes
        usrPropFil.AutoPostback = False
        usrPropFil.FilClaveUnica = False
        usrPropFil.ColClaveUnica = False
        usrPropFil.Ancho = 790
        usrPropFil.Alto = 510
        usrPropFil.ColDocuNume = True
        usrPropFil.ColCUIT = True
        usrPropFil.ColClaveUnica = True
        usrPropFil.FilClaveUnica = True
        usrPropFil.FilCUIT = True
        usrPropFil.FilDocuNume = True
        usrPropFil.FilTarjNume = False
        usrPropFil.Criador = False
        usrPropFil.FilTipo = "T"

        usrCria.Tabla = mstrCriadores
        usrCria.AutoPostback = False
        usrCria.FilClaveUnica = False
        usrCria.ColClaveUnica = False
        usrCria.Ancho = 790
        usrCria.Alto = 510
        usrCria.ColDocuNume = False
        usrCria.ColCUIT = False
        usrCria.ColClaveUnica = False
        usrCria.FilClaveUnica = False
        usrCria.FilCUIT = False
        usrCria.FilDocuNume = False
        usrCria.FilTarjNume = False
        usrCria.Criador = True

        usrMadreFil.Tabla = mstrTabla
        usrMadreFil.AutoPostback = False
        usrMadreFil.Ancho = 790
        usrMadreFil.Alto = 510
        usrMadreFil.FilCriaNume = True
        usrMadreFil.ColCriaNume = True
        usrMadreFil.FilAsocNume = False
        usrMadreFil.ColAsocNume = False
        usrMadreFil.FilNaciFecha = False
        usrMadreFil.ColNaciFecha = False
        usrMadreFil.FilProp = True
        usrMadreFil.ColNomb = True
        usrMadreFil.FilRpNume = False
        usrMadreFil.ColRpNume = False
        usrMadreFil.FilSraNume = False
        usrMadreFil.ColSraNume = False
        usrMadreFil.FilLaboNume = True
        usrMadreFil.FilMadre = False
        usrMadreFil.FilPadre = False
        usrMadreFil.FilRece = False
        usrMadreFil.FilSexo = False
        usrMadreFil.Sexo = 0

        usrPadreFil.Tabla = mstrTabla
        usrPadreFil.AutoPostback = False
        usrPadreFil.Ancho = 790
        usrPadreFil.Alto = 510
        usrPadreFil.FilCriaNume = True
        usrPadreFil.ColCriaNume = True
        usrPadreFil.FilAsocNume = True
        usrPadreFil.ColAsocNume = True
        usrPadreFil.FilNaciFecha = True
        usrPadreFil.ColNaciFecha = True
        usrPadreFil.FilProp = True
        usrPadreFil.ColNomb = True
        usrPadreFil.FilRpNume = True
        usrPadreFil.ColRpNume = True
        usrPadreFil.FilSraNume = True
        usrPadreFil.ColSraNume = True
        usrPadreFil.FilLaboNume = True
        usrPadreFil.FilMadre = False
        usrPadreFil.FilPadre = False
        usrPadreFil.FilRece = False
        usrPadreFil.FilSexo = False
        usrPadreFil.Sexo = 1

        usrReceFil.Tabla = mstrTabla
        usrReceFil.AutoPostback = False
        usrReceFil.Ancho = 790
        usrReceFil.Alto = 510
        usrReceFil.FilCriaNume = True
        usrReceFil.ColCriaNume = True
        usrReceFil.FilAsocNume = True
        usrReceFil.ColAsocNume = True
        usrReceFil.FilNaciFecha = True
        usrReceFil.ColNaciFecha = True
        usrReceFil.FilProp = True
        usrReceFil.ColNomb = True
        usrReceFil.FilRpNume = True
        usrReceFil.ColRpNume = True
        usrReceFil.FilSraNume = True
        usrReceFil.ColSraNume = True
        usrReceFil.FilLaboNume = True
        usrReceFil.FilMadre = False
        usrReceFil.FilPadre = False
        usrReceFil.FilRece = False
        usrReceFil.FilSexo = False
        usrReceFil.Sexo = 0

    End Sub

    Public Sub mInicializarControl()

        Dim lstrCriaId As String

        Try
            mstrProdCtrl = mValorParametro(Request.QueryString("ctrlId"))
            mbooSoloBusq = mValorParametro(Request.QueryString("SoloBusq")) = "1"
            mstrValorId = mValorParametro(Request.QueryString("ValorId"))
            mstrProdId = mValorParametro(Request.QueryString("ProdId"))

            If hdnValorId.Text = "" Then
                hdnValorId.Text = mstrValorId 'pone el id seleccionado en el control de b�squeda
            ElseIf hdnValorId.Text = "-1" Then  'si ya se limpiaron los filtros, ignora el id con el que vino
                mstrValorId = ""
            End If

            mbooActi = mValorParametro(Request.QueryString("a") = "s")

            'campos a filtrar
            If mstrProdCtrl <> "" Then
                mstrFilProp = mValorParametro(Request.QueryString("FilNomb"))
                mstrFilSraNume = mValorParametro(Request.QueryString("FilSraNume"))
                mstrFilCriaNume = mValorParametro(Request.QueryString("FilCriaNume"))
                mstrFilRpNume = mValorParametro(Request.QueryString("FilRpNume"))
                mstrFilLaboNume = mValorParametro(Request.QueryString("FilLaboNume"))
                mstrFilNaciFecha = mValorParametro(Request.QueryString("FilNaciFecha"))
                mstrFilMadre = mValorParametro(Request.QueryString("FilMadre"))
                mstrFilPadre = mValorParametro(Request.QueryString("FilPadre"))
                mstrFilRece = mValorParametro(Request.QueryString("FilRece"))
                mstrFilSexo = mValorParametro(Request.QueryString("FilSexo"))
                mstrFilHabiRaza = mValorParametro(Request.QueryString("FilHabiRaza"))
            Else
                mstrFilProp = 1
                mstrFilSraNume = 1
                mstrFilCriaNume = 1
                mstrFilRpNume = 1
                mstrFilLaboNume = 1
                mstrFilNaciFecha = 1
                mstrFilMadre = 1
                mstrFilPadre = 1
                mstrFilRece = 1
                mstrFilSexo = 1
                mstrFilHabiRaza = 1
            End If

            'valores de los filtros
            If Not Page.IsPostBack Then
                If mValorParametro(Request.QueryString("Raza")) <> "" And mValorParametro(Request.QueryString("CriaNume")) <> "" Then
                    lstrCriaId = clsSQLServer.gCampoValorConsul(mstrConn, "criadores_busq @clie_raza=" + mValorParametro(Request.QueryString("Raza")) + ", @clie_cria_nume=" + mValorParametro(Request.QueryString("CriaNume")), "cria_id")
                    usrCriaFil.Valor = lstrCriaId
                End If
                txtNombFil.Text = mValorParametro(Request.QueryString("Nomb"))
                txtAsoNumeFil.Text = mValorParametro(Request.QueryString("CodiNume"))
                txtRPNumeFil.Text = mValorParametro(Request.QueryString("RpNume"))
                txtNaciFechaDesdeFil.Text = mValorParametro(Request.QueryString("NaciFecha"))
                txtNaciFechaHastaFil.Text = mValorParametro(Request.QueryString("NaciFecha"))
                cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
                cmbSexoFil.Valor = mValorParametro(Request.QueryString("Sexo"))
                cmbAsocFil.Valor = mValorParametro(Request.QueryString("Asoc"))
                txtLaboNumeFil.Text = mValorParametro(Request.QueryString("LaboNume"))
            End If

            'columnas a mostrar
            mstrColNomb = mValorParametro(Request.QueryString("ColNomb"))
            mstrColSraNume = mValorParametro(Request.QueryString("ColSraNume"))
            mstrColRpNume = mValorParametro(Request.QueryString("ColRpNume"))
            mstrColCriaNume = mValorParametro(Request.QueryString("ColCriaNume"))
            mstrColLaboNume = mValorParametro(Request.QueryString("ColLaboNume"))
            mstrColAsocNume = mValorParametro(Request.QueryString("ColAsocNume"))
            mstrColNaciFecha = mValorParametro(Request.QueryString("ColNaciFecha"))

            If mbooSoloBusq Then
                grdDato.Columns(1).Visible = False
                'btnAgre.Enabled = False
            End If

            'seteo de controles segun filtros
            grdDato.Columns(0).Visible = (mstrProdCtrl <> "")

            If mstrProdCtrl <> "" Then
                grdDato.Columns(mintGrdProdNomb).Visible = False '(mstrColNomb = 1)
                grdDato.Columns(mintGrdProdNume).Visible = (mstrColSraNume = 1)
                grdDato.Columns(mintGrdProdRpNume).Visible = (mstrColRpNume = 1)
                grdDato.Columns(mintGrdProdCria).Visible = (mstrColCriaNume = 1)
                grdDato.Columns(mintGrdProdAsoc).Visible = (mstrColAsocNume = 1)
                grdDato.Columns(mintGrdProdNaci).Visible = (mstrColNaciFecha = 1)
                grdDato.Columns(mintGrdProdEsta).Visible = (mstrProdCtrl = "")
            End If

            If mstrProdCtrl <> "" Then
                usrPropFil.Visible = (mstrFilProp = "1")
                lblPropFil.Visible = (mstrFilProp = "1")
                txtNumeFil.Visible = (mstrFilSraNume = "1")
                lblNumeFil.Visible = (mstrFilSraNume = "1")
                usrCriaFil.Visible = (mstrFilCriaNume = "1")
                lblCriaFil.Visible = (mstrFilCriaNume = "1")
                txtRPNumeFil.Visible = (mstrFilRpNume = "1")
                lblRPNumeFil.Visible = (mstrFilRpNume = "1")
                txtLaboNumeFil.Visible = (mstrFilLaboNume = "1")
                lblLaboNumeFil.Visible = (mstrFilLaboNume = "1")
                txtNaciFechaDesdeFil.Visible = (mstrFilNaciFecha = "1")
                txtNaciFechaHastaFil.Visible = (mstrFilNaciFecha = "1")
                lblNaciFechaFil.Visible = (mstrFilNaciFecha = "1")
                lblFechaHastaFil.Visible = (mstrFilNaciFecha = "1")
                usrMadreFil.Visible = (mstrFilMadre = "1")
                lblMadreFil.Visible = (mstrFilMadre = "1")
                usrPadreFil.Visible = (mstrFilPadre = "1")
                lblPadreFil.Visible = (mstrFilPadre = "1")
                usrReceFil.Visible = (mstrFilRece = "1")
                lblReceFil.Visible = (mstrFilRece = "1")
                cmbSexoFil.Enabled = (mstrFilSexo = "1")
                lblSexoFil.Visible = (mstrFilSexo = "1")
                cmbRazaFil.Enabled = (mstrFilHabiRaza = "1")
            Else
                'mstrFilCriaNume = "1"
                'mstrFilRpNume = "1"
                usrPropFil.Visible = True
                lblPropFil.Visible = True
                txtNumeFil.Visible = True
                lblNumeFil.Visible = True
                usrCriaFil.Visible = True
                lblCriaFil.Visible = True
                txtRPNumeFil.Visible = True
                lblRPNumeFil.Visible = True
                txtLaboNumeFil.Visible = True
                lblLaboNumeFil.Visible = True
                txtNaciFechaDesdeFil.Visible = True
                txtNaciFechaHastaFil.Visible = True
                lblNaciFechaFil.Visible = True
                lblFechaHastaFil.Visible = True
                usrMadreFil.Visible = True
                lblMadreFil.Visible = True
                usrPadreFil.Visible = True
                lblPadreFil.Visible = True
                usrReceFil.Visible = True
                lblReceFil.Visible = True
                cmbSexoFil.Enabled = True
                lblSexoFil.Visible = True
                cmbRazaFil.Enabled = True
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mValorParametro(ByVal pstrPara As String) As String
        Dim lstrPara As String
        If pstrPara Is Nothing Then
            lstrPara = ""
        Else
            If pstrPara Is System.DBNull.Value Then
                lstrPara = ""
            Else
                lstrPara = pstrPara
            End If
        End If
        Return (lstrPara)
    End Function

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As String

            lstrCmd = ObtenerSql(Session("sUserId").ToString(), IIf(chkBusc.Checked, 1, 0), usrCriaFil.Valor, _
                                 IIf(chkBaja.Checked, 1, 0), mstrProdCtrl, txtNombFil.Text, cmbRazaFil.SelectedValue, _
                                 txtNumeFil.Text, txtRPNumeFil.Text, cmbSexoFil.SelectedValue, txtNaciFechaDesdeFil.Text, _
                                 txtNaciFechaHastaFil.Text, cmbAsocFil.SelectedValue, txtAsoNumeFil.Text, txtLaboNumeFil.Text, _
                                 usrMadreFil.Valor, usrPadreFil.Valor, usrReceFil.Valor, txtApodoFil.Text)

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

            grdDato.Visible = True

            If Not Page.IsPostBack And grdDato.Items.Count = 1 And mstrProdCtrl <> "" Then
                If mstrValorId = "" Then
                    mProducto(grdDato.Items(0).Cells(Columnas.ProdId).Text, grdDato.Items(0).Cells(Columnas.ProdRaza).Text, grdDato.Items(0).Cells(Columnas.ProdSexo).Text, grdDato.Items(0).Cells(Columnas.ProdAsoc).Text, grdDato.Items(0).Cells(Columnas.ProdNume).Text, grdDato.Items(0).Cells(Columnas.ProdNomb).Text, grdDato.Items(0).Cells(Columnas.ProdDesc).Text, grdDato.Items(0).Cells(Columnas.ProdRazaCodi).Text)
                Else
                    mCargarDatos(mstrValorId)
                    Return
                End If
            End If
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pstrCriaId As String, _
                                        ByVal pintBaja As Integer, ByVal pstrCriaCtrl As String, ByVal pstrNomb As String, _
                                        ByVal pstrRaza As String, ByVal pstrSraNume As String, ByVal pstrRPNume As String, _
                                        ByVal pstrSexo As String, ByVal pstrNaciDesde As String, ByVal pstrNaciHasta As String, _
                                        ByVal pstrAsoc As String, ByVal pstrAsocNume As String, ByVal pstrLaboNume As String, _
                                        ByVal pstrMadre As String, ByVal pstrPadre As String, ByVal pstrRece As String, ByVal pstrApodo As String) As String

        Dim lstrCmd As New StringBuilder

        'esta funci�n se llama tambi�n desde clsXMLHTTP
        lstrCmd.Append("exec productos_busq")
        lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
        lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
        lstrCmd.Append(" , @prdt_cria_id=" + clsSQLServer.gFormatArg(pstrCriaId, SqlDbType.Int))
        lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)

        lstrCmd.Append(" , @prdt_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
        lstrCmd.Append(" , @prdt_apodo=" + clsSQLServer.gFormatArg(pstrApodo, SqlDbType.VarChar))
        lstrCmd.Append(" , @prdt_sra_nume=" + clsSQLServer.gFormatArg(pstrSraNume, SqlDbType.VarChar))
        lstrCmd.Append(" , @prdt_rp=" + clsSQLServer.gFormatArg(pstrRPNume, SqlDbType.VarChar))

        If pstrSexo = "" Then
            lstrCmd.Append(" , @prdt_sexo=null")
        Else
            If pstrSexo.IndexOf(",") <> -1 Then
                pstrSexo = pstrSexo.Substring(pstrSexo.IndexOf(",") + 1)
            End If
            lstrCmd.Append(" , @prdt_sexo=" + clsSQLServer.gFormatArg(pstrSexo, SqlDbType.Int))
        End If

        lstrCmd.Append(" , @prdt_naci_fecha_desde=" + clsSQLServer.gFormatArg(pstrNaciDesde, SqlDbType.SmallDateTime))
        lstrCmd.Append(" , @prdt_naci_fecha_hasta=" + clsSQLServer.gFormatArg(pstrNaciHasta, SqlDbType.SmallDateTime))
        lstrCmd.Append(" , @prdt_asoc_id=" + clsSQLServer.gFormatArg(pstrAsoc, SqlDbType.Int))
        lstrCmd.Append(" , @prdt_asoc_nume=" + clsSQLServer.gFormatArg(pstrAsocNume, SqlDbType.VarChar))
        lstrCmd.Append(" , @prta_nume=" + clsSQLServer.gFormatArg(pstrLaboNume, SqlDbType.Int))
        lstrCmd.Append(" , @madre_prdt_id=" + clsSQLServer.gFormatArg(pstrMadre, SqlDbType.Int))
        lstrCmd.Append(" , @padre_prdt_id=" + clsSQLServer.gFormatArg(pstrPadre, SqlDbType.Int))
        lstrCmd.Append(" , @rece_prdt_id=" + clsSQLServer.gFormatArg(pstrRece, SqlDbType.Int))
        lstrCmd.Append(" , @prdt_raza_id=" + clsSQLServer.gFormatArg(pstrRaza, SqlDbType.Int))

        Return (lstrCmd.ToString)
    End Function
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTabla
                btnAlta.Enabled = pbooAlta
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnProp.Enabled = Not (pbooAlta)
                btnRela.Enabled = Not (pbooAlta)
                btnAsoc.Enabled = Not (pbooAlta)
                btnTramites.Disabled = pbooAlta
                btnServicios.Disabled = pbooAlta
                btnParejas.Disabled = pbooAlta
                btnPremios.Disabled = pbooAlta
                btnPerform.Disabled = pbooAlta
                btnEstados.Disabled = pbooAlta
                Select Case hdnEspe.Text
                    Case SRA_Neg.Constantes.Especies.Bovinos, SRA_Neg.Constantes.Especies.Camelidos, SRA_Neg.Constantes.Especies.Caprinos, SRA_Neg.Constantes.Especies.Equinos, SRA_Neg.Constantes.Especies.Ovinos
                        btnPedigree.Disabled = pbooAlta
                    Case Else
                        btnPedigree.Disabled = True
                End Select
            Case mstrTablaAnalisis
                btnAltaAna.Enabled = pbooAlta
                btnBajaAna.Enabled = Not (pbooAlta)
                btnModiAna.Enabled = Not (pbooAlta)
        End Select
    End Sub

    Public Sub mSeleccionarProducto(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lstrProdId As String, lstrProdRaza As String, lstrProdAsoc As String, lstrProdDesc As String
        Dim lstrProdNume As String, lstrProdNomb As String, lstrProdSexo As String, lstrProdRazaCodi As String
        lstrProdId = E.Item.Cells(Columnas.ProdId).Text
        lstrProdRaza = E.Item.Cells(Columnas.ProdRaza).Text
        lstrProdAsoc = E.Item.Cells(Columnas.ProdAsoc).Text
        lstrProdNume = E.Item.Cells(Columnas.ProdNume).Text
        lstrProdSexo = E.Item.Cells(Columnas.ProdSexo).Text
        lstrProdNomb = E.Item.Cells(Columnas.ProdNomb).Text
        lstrProdDesc = E.Item.Cells(Columnas.ProdDesc).Text
        lstrProdRazaCodi = E.Item.Cells(Columnas.ProdRazaCodi).Text
        mProducto(lstrProdId, lstrProdRaza, lstrProdSexo, lstrProdAsoc, lstrProdNume, lstrProdNomb, lstrProdDesc, lstrProdRazaCodi)
    End Sub

    Private Sub mProducto(ByVal pstrProdId As String, ByVal pstrProdRaza As String, ByVal pstrProdSexo As String, ByVal pstrProdAsoc As String, ByVal pstrProdNume As String, ByVal pstrProdNomb As String, ByVal pstrProdDesc As String, ByVal pstrRazaCodi As String)
        Dim lsbMsg As New StringBuilder
        lsbMsg.Append("<SCRIPT language='javascript'>")
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtId'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdId)))
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdNume)))
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtProdNomb'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdNomb)))
        lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:txtDesc']!=null) window.opener.document.all['{0}:txtDesc'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdDesc)))
        lsbMsg.Append(String.Format("var pCombo = window.opener.document.all['{0}:cmbProdRaza']; ", Request.QueryString("ctrlId")))
        lsbMsg.Append("for (var lintIndi = 0; lintIndi < pCombo.length; lintIndi++) { ")
        lsbMsg.Append("  if (pCombo.options[lintIndi].value == '" & clsFormatear.gFormatCadena(pstrProdRaza.Trim) & "') {")
        lsbMsg.Append("    pCombo.selectedIndex = lintIndi; } } ")
        lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdRaza'].value='{1}';", Request.QueryString("ctrlId"), pstrRazaCodi))
        lsbMsg.Append("if (window.opener.document.all['" & Request.QueryString("ctrlId") & ":cmbProdSexo']!=null) { ")
        lsbMsg.Append("var pCombo = window.opener.document.all['" & Request.QueryString("ctrlId") & ":cmbProdSexo']; ")
        lsbMsg.Append("for (var lintIndi = 0; lintIndi < pCombo.length; lintIndi++) { ")
        lsbMsg.Append("  if (pCombo.options[lintIndi].text == '" & clsFormatear.gFormatCadena(pstrProdSexo.Trim) & "') {")
        lsbMsg.Append("    pCombo.selectedIndex = lintIndi; } } ")
        lsbMsg.Append("}")
        lsbMsg.Append("var pCombo = window.opener.document.all['" & Request.QueryString("ctrlId") & ":cmbProdAsoc']; ")
        lsbMsg.Append("for (var lintIndi = 0; lintIndi < pCombo.length; lintIndi++) { ")
        lsbMsg.Append("  if (pCombo.options[lintIndi].text == '" & clsFormatear.gFormatCadena(pstrProdAsoc.Trim) & "') {")
        lsbMsg.Append("    pCombo.selectedIndex = lintIndi; } } ")
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(Columnas.ProdId).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarPelajes(ByVal pRaza As String)
        clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=1", cmbPelaA, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=2", cmbPelaB, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=3", cmbPelaC, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=4", cmbPelaD, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id=" & pRaza, cmbRegiTipo, "id", "descrip", "N")
    End Sub

    Public Sub mCargarDatos(ByVal pstrId As String)

        Try

            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(pstrId)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    cmbRaza.Valor = .Item("prdt_raza_id")
                    txtRP.Valor = .Item("prdt_rp")
                    txtNomb.Valor = .Item("prdt_nomb")
                    txtNaciFecha.Fecha = .Item("prdt_naci_fecha")
                    txtFalleFecha.Fecha = .Item("prdt_fall_fecha")
                    usrCria.Valor = .Item("prdt_cria_id")
                    txtSraNume.Valor = .Item("prdt_sra_nume")
                    cmbSexo.Valor = IIf(.Item("prdt_sexo"), "1", "0")
                    mCargarPelajes(cmbRaza.Valor)
                    cmbPelaA.Valor = .Item("prdt_a_pela_id")
                    cmbPelaB.Valor = .Item("prdt_b_pela_id")
                    cmbPelaC.Valor = .Item("prdt_c_pela_id")
                    cmbPelaD.Valor = .Item("prdt_d_pela_id")
                    If .IsNull("prdt_tran_fecha") Then
                        txtTranFecha.Text = ""
                    Else
                        txtTranFecha.Text = CDate(.Item("prdt_tran_fecha")).ToString("dd/MM/yyyy")
                    End If

                    cmbRegiTipo.Valor = .Item("prdt_regt_id")
                    txtPX.Valor = .Item("prdt_px")
                    txtApod.Valor = .Item("prdt_apodo")
                    txtDona.Valor = .Item("prdt_dona_nume")
                    txtPesoNacer.Valor = .Item("prdt_peso_nacer")
                    txtPesoDeste.Valor = .Item("prdt_peso_deste")
                    txtPesoFinal.Valor = .Item("prdt_peso_final")
                    txtInscFecha.Fecha = .Item("prdt_insc_fecha")
                    txtEpdNacer.Valor = .Item("prdt_epd_nacer")
                    txtEpdDeste.Valor = .Item("prdt_epd_deste")
                    txtEpdFinal.Valor = .Item("prdt_epd_final")
                    txtRPNume.Valor = .Item("prdt_rp_nume")
                    txtMelli.Valor = .Item("prdt_melli")
                    txtSiete.Valor = .Item("prdt_siete")
                    hdnEspe.Text = .Item("_espe")

                    If Not .IsNull("prdt_te") Then
                        chkTranEmbr.Checked = clsWeb.gFormatCheck(.Item("prdt_te"), "True")
                    Else
                        chkTranEmbr.Checked = False
                    End If
                    If Not .IsNull("_esta_desc") Then
                        txtEsta.Text = .Item("_esta_desc")
                    Else
                        txtEsta.Text = ""
                    End If
                    If Not .IsNull("prdt_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prdt_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If
                    lblTitu.Text = "Datos del Producto: " & "  Raza: " & .Item("_raza_desc") & " - " & .Item("_sexo") & " - SRA Nro.: " & .Item("prdt_sra_nume")
                End With

                mSetearEditor("", False)
                mMostrarPanel(True)

            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiarFiltros()

        hdnValorId.Text = "-1"  'para que ignore el id que vino del control
        mstrValorId = ""

        txtNumeFil.Text = ""
        txtNombFil.Text = ""
        txtApodoFil.Text = ""
        txtAsoNumeFil.Text = ""
        txtRPNumeFil.Text = ""
        txtLaboNumeFil.Text = ""
        txtCtrolFactuNumeFil.Text = ""
        txtNaciFechaDesdeFil.Text = ""
        txtNaciFechaHastaFil.Text = ""
        usrCriaFil.Valor = ""
        usrCriaFil.Limpiar()
        usrMadreFil.Limpiar()
        usrPadreFil.Limpiar()
        usrReceFil.Limpiar()

        chkBusc.Checked = False
        chkBaja.Checked = False

        If cmbRazaFil.Enabled Then
            cmbRazaFil.Limpiar()
        End If
        cmbAsocFil.Limpiar()
        cmbSexo.Valor = mValorParametro(Request.QueryString("Sexo"))
        cmbCtroFil.Limpiar()

        grdDato.Visible = False

    End Sub

    Public Sub grdAna_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAna.EditItemIndex = -1
            If (grdAna.CurrentPageIndex < 0 Or grdAna.CurrentPageIndex >= grdAna.PageCount) Then
                grdAna.CurrentPageIndex = 0
            Else
                grdAna.CurrentPageIndex = E.NewPageIndex
            End If
            grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
            grdAna.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarAnalisis()
        hdnAnaId.Text = ""
        txtAnaNume.Text = ""
        cmbAnaTipo.Limpiar()
        txtAnaFecha.Text = ""
        txtAnaFechaResul.Text = ""
        cmbAnaResul.Limpiar()
        mSetearEditor(mstrTablaAnalisis, True)
    End Sub

    Private Sub mLimpiar()

        hdnId.Text = ""
        lblBaja.Text = ""
        txtEsta.Text = ""
        hdnEspe.Text = ""
        txtSraNume.Text = ""
        txtRPNume.Text = ""
        txtRP.Text = ""
        txtNomb.Text = ""
        txtApod.Text = ""
        txtDona.Text = ""
        txtTranFecha.Text = ""
        txtNaciFecha.Text = ""
        txtInscFecha.Text = ""
        txtFalleFecha.Text = ""
        txtPesoNacer.Text = ""
        txtPesoDeste.Text = ""
        txtPesoFinal.Text = ""
        txtEpdNacer.Text = ""
        txtEpdDeste.Text = ""
        txtEpdFinal.Text = ""
        txtPX.Text = ""
        txtMelli.Text = ""
        txtSiete.Text = ""

        cmbRaza.Limpiar()
        cmbSexo.Limpiar()
        cmbPelaA.Limpiar()
        cmbPelaB.Limpiar()
        cmbPelaC.Limpiar()
        cmbPelaD.Limpiar()
        cmbRegiTipo.Limpiar()

        chkTranEmbr.Checked = False

        usrCria.Limpiar()

        mCrearDataSet("")

        lblTitu.Text = ""
        mLimpiarAnalisis()

        mSetearEditor("", True)

    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)

        lnkCabecera.Font.Bold = True
        lnkAna.Font.Bold = False

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panAna.Visible = False

        tabLinks.Visible = pbooVisi
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        Dim lstrTitu As String
        If lblTitu.Text <> "" Then
            lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
        End If

        panDato.Visible = True
        panBotones.Visible = True
        panAna.Visible = False
        panCabecera.Visible = False

        lnkCabecera.Font.Bold = False
        lnkAna.Font.Bold = False

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Visible = False
            Case 2
                panAna.Visible = True
                lnkAna.Font.Bold = True
                lblTitu.Text = "Producto: " & lstrTitu
                lblTitu.Visible = True
                grdAna.Visible = True
        End Select

    End Sub

#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim lstrProdId As String

            mGuardarDatos()
            Dim lobjProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaAnalisis, mdsDatos)

            lstrProdId = lobjProducto.Alta()

            If mstrProdCtrl <> "" Then
                mProducto(lstrProdId, "", "", "", "", "", "", "")
                Return
            End If

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try

            mGuardarDatos()
            Dim lobjProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaAnalisis, mdsDatos)

            lobjProducto.Modi()

            If mstrValorId <> "" Then
                mProducto(hdnId.Text, "", "", "", "", "", "", "")
                Return
            End If

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaAnalisis, mdsDatos)
            lobjProducto.Baja(hdnId.Text)

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatosAnalisis(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrAna As DataRow
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lbooGene As Boolean

            hdnAnaId.Text = E.Item.Cells(1).Text
            ldrAna = mdsDatos.Tables(mstrTablaAnalisis).Select("prta_id=" & hdnAnaId.Text)(0)

            With ldrAna
                txtAnaNume.Valor = .Item("prta_nume")
                cmbAnaTipo.Valor = .Item("prta_tipo")
                cmbAnaResul.Valor = .Item("prta_ares_id")
                txtAnaFecha.Fecha = .Item("prta_fecha")
                txtAnaFechaResul.Fecha = .Item("prta_resul_fecha")
            End With

            mSetearEditor(mstrTablaAnalisis, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarAnalisis(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosAnalisis(pbooAlta)

            mLimpiarAnalisis()
            grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
            grdAna.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosAnalisis(ByVal pbooAlta As Boolean)

        Dim ldrDatos As DataRow

        If txtAnaNume.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el N� de An�lisis.")
        End If

        If cmbAnaTipo.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el Tipo de An�lisis.")
        End If

        If txtAnaFecha.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de An�lisis.")
        End If

        If cmbAnaResul.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el Resultado.")
        End If

        If txtAnaFechaResul.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de Resultado.")
        End If

        If hdnAnaId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaAnalisis).NewRow
            ldrDatos.Item("prta_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaAnalisis), "prta_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaAnalisis).Select("prta_id=" & hdnAnaId.Text)(0)
        End If

        With ldrDatos
            .Item("prta_prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("prta_nume") = txtAnaNume.Valor
            .Item("prta_tipo") = cmbAnaTipo.Valor
            .Item("_tipo") = cmbAnaTipo.SelectedItem.Text
            .Item("prta_ares_id") = cmbAnaResul.Valor
            .Item("_resul") = cmbAnaResul.SelectedItem.Text
            .Item("prta_fecha") = txtAnaFecha.Fecha
            .Item("prta_baja_fecha") = DBNull.Value
            .Item("prta_resul_fecha") = txtAnaFechaResul.Fecha
        End With

        If hdnAnaId.Text = "" Then
            mdsDatos.Tables(mstrTablaAnalisis).Rows.Add(ldrDatos)
        End If

    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Private Function mGuardarDatos() As DataSet

        mValidarDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("prdt_rp") = txtRP.Valor
            .Item("prdt_nomb") = txtNomb.Valor
            .Item("prdt_naci_fecha") = txtNaciFecha.Fecha
            .Item("prdt_fall_fecha") = txtFalleFecha.Fecha
            .Item("prdt_cria_id") = usrCria.Valor
            .Item("prdt_raza_id") = cmbRaza.Valor
            .Item("prdt_sra_nume") = txtSraNume.Valor
            .Item("prdt_sexo") = IIf(cmbSexo.Valor = "1", True, False)
            .Item("prdt_a_pela_id") = cmbPelaA.Valor
            .Item("prdt_b_pela_id") = cmbPelaB.Valor
            .Item("prdt_c_pela_id") = cmbPelaC.Valor
            .Item("prdt_d_pela_id") = cmbPelaD.Valor

            .Item("prdt_regt_id") = cmbRegiTipo.Valor
            .Item("prdt_px") = txtPX.Valor
            .Item("prdt_apodo") = txtApod.Text
            .Item("prdt_siete") = txtSiete.Valor
            .Item("prdt_melli") = txtMelli.Valor
            .Item("prdt_te") = chkTranEmbr.Checked
            .Item("prdt_dona_nume") = txtDona.Valor
            .Item("prdt_peso_nacer") = txtPesoNacer.Valor
            .Item("prdt_peso_deste") = txtPesoDeste.Valor
            .Item("prdt_peso_final") = txtPesoFinal.Valor
            .Item("prdt_insc_fecha") = txtInscFecha.Fecha
            .Item("prdt_epd_nacer") = txtEpdNacer.Valor
            .Item("prdt_epd_deste") = txtEpdDeste.Valor
            .Item("prdt_epd_final") = txtEpdFinal.Valor
            .Item("prdt_rp_nume") = txtRPNume.Valor
        End With

        Return mdsDatos
    End Function

    Public Sub mCrearDataSet(ByVal pstrId As String)

        Dim lstrOpci As String

        lstrOpci = "@audi_user=" & Session("sUserId").ToString
        mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, lstrOpci)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaAnalisis

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        Session(mSess(mstrTabla)) = mdsDatos

        grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
        grdAna.DataBind()
        grdAna.Visible = True

    End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

#End Region

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub

    Private Sub lnkAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAna.Click
        mShowTabs(2)
    End Sub

    Private Sub btnAltaAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAna.Click
        mActualizarAnalisis(True)
    End Sub

    Private Sub btnLimpAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAna.Click
        mLimpiarAnalisis()
    End Sub

    Private Sub btnBajaAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAna.Click
        Try
            With mdsDatos.Tables(mstrTablaAnalisis).Select("prta_id=" & hdnAnaId.Text)(0)
                .Item("prta_baja_fecha") = System.DateTime.Now.ToString
            End With
            grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
            grdAna.DataBind()
            mLimpiarAnalisis()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnModiAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAna.Click
        mActualizarAnalisis(False)
    End Sub

End Class