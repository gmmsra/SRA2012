<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrSociosFiltro" CodeFile="usrSociosFiltro.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<script language="JavaScript" src="includes/utiles.js"></script>
<script language="JavaScript">
function mSetearTxtAnio()
{
	if (document.all('usrSocFil:txtPagoAnual')!=null)
	{
		document.all('usrSocFil:txtPagoAnual').disabled=(document.all('usrSocFil:cmbPagoAnual').selectedIndex==0);
		if (document.all('usrSocFil:txtPagoAnual').disabled)
				document.all('usrSocFil:txtPagoAnual').value='';
		document.all('usrSocFil:txtDeudAnio').disabled=(document.all('usrSocFil:cmbDeudAnio').selectedIndex==0);
		if (document.all('usrSocFil:txtDeudAnio').disabled)
				document.all('usrSocFil:txtDeudAnio').value='';
	}
}
function chkDeudaClick()
{
	document.all("usrSocFil:chkInte").checked=false;
	document.all("usrSocFil:chkInte").parentElement.disabled=!document.all("usrSocFil:chkDeuda").checked;
	document.all("usrSocFil:chkInte").disabled=!document.all("usrSocFil:chkDeuda").checked;
}
	
function CambiarExp(pstrNum)
{
	if(document.all("spExp" + pstrNum).innerText=="+")
	{
		document.all("panBuscAvan" + pstrNum).style.display = "inline";
		document.all("spExp" + pstrNum).innerText="-";
	}
	
	else
	{
		document.all("panBuscAvan" + pstrNum).style.display = "none";
		document.all("spExp" + pstrNum).innerText="+";
	}

}
function mDeterminarSeleccion(pCombo)
{
	var lstrSele = '';
	var combo = document.all(pCombo);
	for (j=0;j<combo.length;j++)
	{
		if (lstrSele != '')
			lstrSele = lstrSele + ',';
		lstrSele = lstrSele + combo.options[j].value;		
	}
	return(lstrSele);
}
function mActualizarSeleccion(pSele,pTabla)
{
	var lstrResu = document.all(pSele).value;
	var pstrCombo = "";
	switch(pTabla)
	{
		case "paises":		{ pstrCombo="lisPais"; break;}
		case "provincias":	{ pstrCombo="lisProv"; break;}
		case "localidades":	{ pstrCombo="lisLoca"; break;}
	}
	if (lstrResu != '')
	{
		var sFiltro = "'" + lstrResu + "'";
		LoadComboXML(pTabla+"_multi_cargar", sFiltro, "usrSocFil:"+pstrCombo, "");		
		switch(pTabla)
		{
			case "paises":
			{ 
				sFiltro = "'"+document.all("usrSocFil:hdnSeleProv").value+"','"+document.all("usrSocFil:hdnSelePais").value+"'";
				if (document.all("usrSocFil:hdnSeleProv").value!='')
				{
					LoadComboXML("provincias_multi_cargar", sFiltro, "usrSocFil:lisProv", "");		
					document.all("usrSocFil:hdnSeleProv").value = mDeterminarSeleccion("usrSocFil:lisProv");
				}
				sFiltro = "'"+document.all("usrSocFil:hdnSeleLoca").value+"','"+document.all("usrSocFil:hdnSeleProv").value+"'";
				if (document.all("usrSocFil:hdnSeleLoca").value!='')
				{
					LoadComboXML("localidades_multi_cargar", sFiltro, "usrSocFil:lisLoca", "");		
					document.all("usrSocFil:hdnSeleLoca").value = mDeterminarSeleccion("usrSocFil:lisLoca");
				}
				break;
			}
			case "provincias":
			{ 
				sFiltro = "'"+document.all("usrSocFil:hdnSeleLoca").value+"','"+document.all("usrSocFil:hdnSeleProv").value+"'";
				if (document.all("usrSocFil:hdnSeleLoca").value!='')
				{
					LoadComboXML("localidades_multi_cargar", sFiltro, "usrSocFil:lisLoca", "");		
					document.all("usrSocFil:hdnSeleLoca").value = mDeterminarSeleccion("usrSocFil:lisLoca");
				}
				break;
			}
		}	
	}
	else
	{
		LimpiarCombo(document.all("usrSocFil:"+pstrCombo));
		switch(pTabla)
		{
			case "paises":
			{ 
				LimpiarCombo(document.all("usrSocFil:lisProv")); 
				LimpiarCombo(document.all("usrSocFil:lisLoca")); 
				document.all('usrSocFil:hdnSelePais').value = '';
				document.all('usrSocFil:hdnSeleProv').value = '';
				document.all('usrSocFil:hdnSeleLoca').value = '';
				break;
			}
			case "provincias":
			{ 
				LimpiarCombo(document.all("usrSocFil:lisLoca")); 
				document.all('usrSocFil:hdnSeleProv').value = '';
				document.all('usrSocFil:hdnSeleLoca').value = '';
				break;
			}
			case "localidades":
			{ 
				document.all('usrSocFil:hdnSeleLoca').value = '';
				break;
			}
		}		
	}
}
function mAgregar(pTipo)
{
	switch(pTipo)
	{
		case "P":	// paises
		{ gAbrirVentanas("../MultiSeleGene.aspx?ctrl=usrSocFil:hdnSelePais&tit=Paises&tab=paises&sel="+document.all('usrSocFil:hdnSelePais').value, 0, "640","380","50"); break; }
		case "R":	// provincias
		{ gAbrirVentanas("../MultiSeleGene.aspx?ctrl=usrSocFil:hdnSeleProv&tit=Provincias&tab=provincias&sel="+document.all('usrSocFil:hdnSeleProv').value+"&fil="+document.all('usrSocFil:hdnSelePais').value, 0, "640","380","50"); break; }
		case "L":	// localidades
		{ gAbrirVentanas("../MultiSeleGene.aspx?ctrl=usrSocFil:hdnSeleLoca&tit=Localidades&tab=localidades&sel="+document.all('usrSocFil:hdnSeleLoca').value+"&fil="+document.all('usrSocFil:hdnSeleProv').value, 0, "640","380","50"); break; }
	}
}
</script>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD noWrap align="right" width="15%" height="2"></TD>
		<TD align="left"></TD>
		<TD align="right" width="15%"></TD>
		<TD align="left"></TD>
	</TR>
	<tr>
	<tr>
	<tr>
	<tr>
		<td colSpan="4">
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td width="12"><span id="spExp1" style="FONT-FAMILY: Verdana; HEIGHT: 4px; COLOR: black; FONT-SIZE: 14px; CURSOR: hand; FONT-WEIGHT: bold"
							onclick="CambiarExp('1')">+</span></td>
					<TD width="98%"><asp:label id="lblExp1" runat="server" cssclass="titulo">B�squeda Avanzada</asp:label></TD>
				</tr>
				<TR>
					<TD colSpan="2">
						<div class="panelediciontransp" id="panBuscAvan1" style="WIDTH: 100%; DISPLAY: none">
							<table width="100%" align="center" border="0">
								<TR>
									<TD vAlign="top" align="right"><asp:label id="lblOrden" runat="server" cssclass="titulo">Orden:</asp:label>&nbsp;</TD>
									<TD vAlign="top" width="100%"><cc1:combobox class="combo" id="cmbOrden" runat="server" Width="119px">
											<asp:ListItem Value="A" Selected="True">Apellido</asp:ListItem>
											<asp:ListItem Value="N">Nro.Socio</asp:ListItem>
											<asp:ListItem Value="C">C�digo Postal</asp:ListItem>
										</cc1:combobox></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right"><asp:label id="lblEstados" runat="server" cssclass="titulo">Estados:</asp:label>&nbsp;</TD>
									<TD vAlign="top" width="100%"><cc1:checklistbox id="lstEstados" runat="server" CellPadding="0" CellSpacing="0" width="100%" height="100px"
											RepeatColumns="2" CssClass="chklst" MuestraBotones="True"></cc1:checklistbox></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right"><asp:label id="lblCategorias" runat="server" cssclass="titulo">Categor�as:</asp:label>&nbsp;</TD>
									<TD vAlign="top" width="100%"><cc1:checklistbox id="lstCategorias" runat="server" CellPadding="0" CellSpacing="0" width="100%" height="100px"
											RepeatColumns="2" CssClass="chklst" MuestraBotones="True"></cc1:checklistbox></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="2">&nbsp;&nbsp;
										<asp:label id="lblCateFecha" runat="server" cssclass="titulo">Categor�as y Estados al:</asp:label>&nbsp;
										<cc1:datebox id="txtCateFecha" runat="server" Width="100px"></cc1:datebox></TD>
								</TR>
								<TR>
									<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
								</TR>
								<tr>
									<td colSpan="2">
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD colSpan="2">
													<table width="100%" align="center" border="0">
														<TR>
															<TD align="right"><asp:label id="lblCateFechaDesde" runat="server" cssclass="titulo">Fecha Categor�a Desde:</asp:label>&nbsp;</TD>
															<TD><cc1:datebox id="txtCateFechaDesde" runat="server" Width="100px"></cc1:datebox></TD>
															<TD align="right"><asp:label id="lblCateFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;</TD>
															<TD><cc1:datebox id="txtCateFechaHasta" runat="server" Width="100px"></cc1:datebox></TD>
														</TR>
														<TR>
															<TD align="right"><asp:label id="lblEstaFechaDesde" runat="server" cssclass="titulo">Fecha Estado Desde:</asp:label>&nbsp;</TD>
															<TD><cc1:datebox id="txtEstaFechaDesde" runat="server" Width="100px"></cc1:datebox></TD>
															<TD align="right"><asp:label id="lblEstaFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;</TD>
															<TD><cc1:datebox id="txtEstaFechaHasta" runat="server" Width="100px"></cc1:datebox></TD>
														</TR>
														<TR>
															<TD align="right"><asp:label id="lblIngrFechaDesde" runat="server" cssclass="titulo">Fecha Ingreso Desde:</asp:label>&nbsp;</TD>
															<TD><cc1:datebox id="txtIngrFechaDesde" runat="server" Width="100px"></cc1:datebox></TD>
															<TD align="right"><asp:label id="lblIngrFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;</TD>
															<TD><cc1:datebox id="txtIngrFechaHasta" runat="server" Width="100px"></cc1:datebox></TD>
														</TR>
														<TR>
															<TD align="right"><asp:label id="lblDistritoDesde" runat="server" cssclass="titulo">Distrito de Voto Desde:</asp:label></TD>
															<TD><cc1:numberbox id="txtDistritoDesde" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:numberbox></TD>
															<TD align="right"><asp:label id="lblDistritoHasta" runat="server" cssclass="titulo"> Hasta:</asp:label>&nbsp;</TD>
															<TD><cc1:numberbox id="txtDistritoHasta" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right"><asp:label id="lblCodPostDesde" runat="server" cssclass="titulo">C�digo Postal Desde:</asp:label>&nbsp;</TD>
															<TD><cc1:textboxtab id="txtCodPostDesde" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:textboxtab></TD>
															<TD align="right"><asp:label id="lblCodPostHasta" runat="server" cssclass="titulo"> Hasta:</asp:label>&nbsp;</TD>
															<TD><cc1:textboxtab id="txtCodPostHasta" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:textboxtab></TD>
														</TR>
														<TR>
															<TD align="right"><asp:label id="lblNumeDesde" runat="server" cssclass="titulo">Nro.Socio: Desde:</asp:label>&nbsp;</TD>
															<TD><cc1:numberbox id="txtNumeDesde" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:numberbox></TD>
															<TD align="right"><asp:label id="lblNumeHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;</TD>
															<TD><cc1:numberbox id="txtNumeHasta" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right"><asp:label id="lblApel" runat="server" cssclass="titulo">Apel/R.Soc.:</asp:label><asp:label id="lblApelDesde" runat="server" cssclass="titulo"> Desde:</asp:label>&nbsp;</TD>
															<TD><cc1:textboxtab id="txtApelDesde" runat="server" cssclass="cuadrotexto" Width="200px"></cc1:textboxtab></TD>
															<TD align="right" width="50"><asp:label id="lblApelHasta" runat="server" cssclass="titulo"> Hasta:</asp:label>&nbsp;</TD>
															<TD><cc1:textboxtab id="txtApelHasta" runat="server" cssclass="cuadrotexto" Width="200px"></cc1:textboxtab></TD>
														</TR>
													</table>
												</TD>
											</TR>
										</TABLE>
									</td>
								</tr>
								<TR>
									<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
								</TR>
								<TR>
									<TD vAlign="top" colSpan="2">
										<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD style="WIDTH: 140px" align="right" height="24"><asp:label id="lblAsam" runat="server" cssclass="titulo">Asamblea:</asp:label>&nbsp;</TD>
												<TD colSpan="3" height="24">
													<cc1:combobox class="combo" id="cmbAsam" runat="server" Width="300px"></cc1:combobox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 140px" align="right" height="24"><asp:label id="lblMail" runat="server" cssclass="titulo">e-Mail:</asp:label>&nbsp;</TD>
												<TD style="WIDTH: 74px" height="24"><cc1:combobox class="combo" id="cmbMail" runat="server" Width="100px">
														<asp:listitem value="" selected="true">(Todos)</asp:listitem>
														<asp:listitem value="1">Con</asp:listitem>
														<asp:listitem value="0">Sin</asp:listitem>
													</cc1:combobox></TD>
												<TD align="right" height="24"><asp:label id="lblCorr" runat="server" cssclass="titulo">Correo:</asp:label>&nbsp;</TD>
												<TD height="24"><cc1:combobox class="combo" id="cmbCorr" runat="server" Width="100px">
														<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
														<asp:ListItem Value="0">Con</asp:ListItem>
														<asp:ListItem Value="1">Sin</asp:ListItem>
													</cc1:combobox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 140px" align="right" height="24"><asp:label id="lblProm" runat="server" cssclass="titulo">Promoci�n:</asp:label>&nbsp;</TD>
												<TD style="WIDTH: 74px" height="24"><cc1:combobox class="combo" id="cmbProm" runat="server" Width="100px">
														<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
														<asp:ListItem Value="1">Si</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</cc1:combobox></TD>
												<TD align="left" colSpan="2" height="24">&nbsp;<cc1:combobox class="combo" id="cmbPromTipo" runat="server" Width="218px"></cc1:combobox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 140px" align="right" height="24"><asp:label id="lblPagoAnual" runat="server" cssclass="titulo">Pago Anualidad:</asp:label>&nbsp;</TD>
												<TD style="WIDTH: 74px" height="24"><cc1:combobox class="combo" id="cmbPagoAnual" runat="server" Width="100px" onchange="javascript:mSetearTxtAnio();">
														<asp:ListItem Value="T" Selected="True">Todos</asp:ListItem>
														<asp:ListItem Value="A">A&#241;o</asp:ListItem>
													</cc1:combobox></TD>
												<TD align="left" colSpan="2" height="24">&nbsp;
													<cc1:numberbox id="txtPagoAnual" runat="server" cssclass="cuadrotexto" Width="90px" MaxValor="2050"
														MaxLength="4"></cc1:numberbox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 140px" align="right" height="24"><asp:label id="lblDeudAnio" runat="server" cssclass="titulo">Deuda desde el A�o:</asp:label></TD>
												<TD style="WIDTH: 74px" height="24"><cc1:combobox class="combo" id="cmbDeudAnio" runat="server" Width="100px" onchange="javascript:mSetearTxtAnio();">
														<asp:ListItem Value="T" Selected="True">Todos</asp:ListItem>
														<asp:ListItem Value="A">A&#241;o</asp:ListItem>
													</cc1:combobox></TD>
												<TD align="left" colSpan="2" height="24">&nbsp;
													<cc1:numberbox id="txtDeudAnio" runat="server" cssclass="cuadrotexto" Width="90px" MaxValor="2050"
														MaxLength="4"></cc1:numberbox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 140px" align="right" height="24"><asp:label id="lblDA" runat="server" cssclass="titulo">D�bito Autom�tico:</asp:label>&nbsp;</TD>
												<TD style="WIDTH: 74px" height="24"><cc1:combobox class="combo" id="cmbDA" runat="server" Width="100px">
														<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
														<asp:ListItem Value="1">Si</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</cc1:combobox></TD>
												<TD align="right" height="24"><asp:label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:label>&nbsp;</TD>
												<TD height="24"><cc1:combobox class="combo" id="cmbTarj" runat="server" Width="218px"></cc1:combobox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 140px" align="right" height="24"><asp:label id="lblCuotas" runat="server" cssclass="titulo">Adeuda:</asp:label>&nbsp;<asp:label id="lblCuotDesd" runat="server" cssclass="titulo">Desde:</asp:label>&nbsp;</TD>
												<TD style="WIDTH: 74px" height="24"><cc1:numberbox id="txtCuotDesd" runat="server" cssclass="cuadrotexto" Width="60px"></cc1:numberbox></TD>
												<TD align="right" height="24"><asp:label id="lblCuotHast" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;</TD>
												<TD height="24"><cc1:numberbox id="txtCuotHast" runat="server" cssclass="cuadrotexto" Width="60px"></cc1:numberbox><asp:label id="lblCuot" runat="server" cssclass="titulo">Cuotas</asp:label></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right"><asp:label id="lblInstituciones" runat="server" cssclass="titulo">Instituciones:</asp:label>&nbsp;</TD>
									<TD vAlign="top"><cc1:checklistbox id="lstInstituciones" runat="server" CellPadding="0" CellSpacing="0" width="100%"
											RepeatColumns="2" CssClass="chklst" MuestraBotones="True"></cc1:checklistbox></TD>
								</TR>
								<TR>
									<TD vAlign="top" colspan="2">
										<div class="panelediciontransp" style="WIDTH:420px" id="panDatoOpci" runat="server">
											<table border="0" cellSpacing="0" cellPadding="0">
												<tr>
													<TD style="WIDTH: 140px" align="right" noWrap>
														<asp:label id="lblOpciDato" cssclass="titulo" runat="server">Datos Opcionales:&nbsp;</asp:label></TD>
													<td style="WIDTH: 109px">
														<asp:CheckBox id="chkTelefono" CssClass="titulo" Text="Tel�fono" Runat="server" Checked="false"></asp:CheckBox>
													</td>
													<TD></TD>
													<TD>
														<asp:CheckBox id="chkCateFecha" CssClass="titulo" Text="Fecha Categor�a" Runat="server" Checked="false"></asp:CheckBox></TD>
												</tr>
												<tr>
													<TD style="WIDTH: 96px"></TD>
													<td style="WIDTH: 109px">
														<asp:CheckBox id="chkDeuda" onclick="chkDeudaClick();" CssClass="titulo" Text="Deuda" Runat="server"
															Checked="false"></asp:CheckBox>
													</td>
													<TD></TD>
													<TD>
														<asp:CheckBox id="chkIngreFecha" CssClass="titulo" Text="Fecha Ingreso" Runat="server" Checked="false"></asp:CheckBox></TD>
												</tr>
												<tr>
													<TD style="WIDTH: 96px"></TD>
													<td style="WIDTH: 109px">
														<asp:CheckBox id="chkInte" CssClass="titulo" Text="Intereses" Runat="server" Checked="false" Enabled="False"></asp:CheckBox>
													</td>
													<TD>&nbsp;&nbsp;
													</TD>
													<TD></TD>
												</tr>
											</table>
										</div>
									</TD>
								<TR>
									<TD colSpan="2">
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD width="12"><SPAN id="spExp2" style="WIDTH: 10px; FONT-FAMILY: Verdana; HEIGHT: 4px; COLOR: black; FONT-SIZE: 14px; CURSOR: hand; FONT-WEIGHT: bold"
														onclick="CambiarExp('2')">+</SPAN></TD>
												<TD width="98%">
													<asp:label id="lblExp2" cssclass="titulo" runat="server">B�squeda Por Entidades</asp:label></TD>
											</TR>
											<TR>
												<TD colSpan="2">
													<DIV class="panelediciontransp" id="panBuscAvan2" style="Z-INDEX: 101; WIDTH: 100%; DISPLAY: none">
														<TABLE width="100%" align="center" border="0">
															<TR>
																<TD vAlign="top" align="right">
																	<asp:label id="lblEntidades" cssclass="titulo" runat="server">Entidades:</asp:label>&nbsp;</TD>
																<TD vAlign="top">
																	<cc1:checklistbox id="lstEntidades" runat="server" MuestraBotones="True" CssClass="chklst" RepeatColumns="2"
																		height="100px" width="100%" CellSpacing="0" CellPadding="0"></cc1:checklistbox></TD>
															</TR>
														</TABLE>
													</DIV>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD colSpan="2">
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD width="12"><SPAN id="spExp3" style="FONT-FAMILY: Verdana; HEIGHT: 4px; COLOR: black; FONT-SIZE: 14px; CURSOR: hand; FONT-WEIGHT: bold"
														onclick="CambiarExp('3')">+</SPAN></TD>
												<TD width="98%">
													<asp:label id="lblExp3" cssclass="titulo" runat="server">B�squeda Por Paises/Provincias/Localidades</asp:label></TD>
											</TR>
											<TR>
												<TD colSpan="2">
													<DIV class="panelediciontransp" id="panBuscAvan3" style="Z-INDEX: 101; WIDTH: 100%; DISPLAY: none">
														<TABLE width="100%" align="center" border="0">
															<TR>
																<TD vAlign="top" align="center">
																	<asp:label id="lblPais" cssclass="titulo" runat="server">Paises:</asp:label></TD>
																<TD vAlign="top" align="center"></TD>
																<TD vAlign="top" align="center">
																	<asp:label id="lblProv" cssclass="titulo" runat="server">Provincias:</asp:label></TD>
																<TD vAlign="top"></TD>
																<TD vAlign="top" align="center">
																	<asp:label id="lblLoca" cssclass="titulo" runat="server">Localidades:</asp:label></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="center" width="32%">
																	<cc1:lista id="lisPais" onclick="this.selectedIndex=-1;" runat="server" width="98%" classtitulos="titulo"
																		AutoPostBack="false" Height="120px" Font-Size="XX-Small"></cc1:lista></TD>
																<TD vAlign="top" align="center"></TD>
																<TD vAlign="top" align="center" width="32%">
																	<cc1:lista id="lisProv" onclick="this.selectedIndex=-1;" runat="server" width="98%" classtitulos="titulo"
																		AutoPostBack="false" Height="120px" Font-Size="XX-Small"></cc1:lista></TD>
																<TD vAlign="top"></TD>
																<TD vAlign="top" align="center" width="32%">
																	<cc1:lista id="lisLoca" onclick="this.selectedIndex=-1;" runat="server" width="98%" classtitulos="titulo"
																		AutoPostBack="false" Height="120px" Font-Size="XX-Small"></cc1:lista></TD>
															</TR>
															<TR>
																<TD vAlign="bottom" align="center" width="32%" height="30">
																	<asp:button id="btnPais" cssclass="boton" runat="server" Width="130px" Text="Agregar Paises"></asp:button></TD>
																<TD vAlign="top" align="center" height="30"></TD>
																<TD vAlign="bottom" align="center" width="32%" height="30">
																	<asp:button id="btnProv" cssclass="boton" runat="server" Width="130px" Text="Agregar Provincias"></asp:button></TD>
																<TD vAlign="top" height="30"></TD>
																<TD vAlign="bottom" align="center" width="32%" height="30">
																	<asp:button id="btnLoca" cssclass="boton" runat="server" Width="130px" Text="Agregar Localidades"></asp:button></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<DIV style="DISPLAY: none">
																		<cc1:textboxtab id="hdnSelePais" cssclass="cuadrotexto" runat="server" Width="100"></cc1:textboxtab>
																		<cc1:textboxtab id="hdnSeleProv" cssclass="cuadrotexto" runat="server" Width="100"></cc1:textboxtab>
																		<cc1:textboxtab id="hdnSeleLoca" cssclass="cuadrotexto" runat="server" Width="100"></cc1:textboxtab></DIV>
																</TD>
																<TD vAlign="top"></TD>
																<TD vAlign="top"></TD>
																<TD vAlign="top"></TD>
																<TD vAlign="top"></TD>
															</TR>
														</TABLE>
													</DIV>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</table>
						</div>
					</TD>
				</TR>
			</TABLE>
		</td>
	</tr>
</TABLE>
<SCRIPT language="JavaScript">
mSetearTxtAnio();
</SCRIPT>
