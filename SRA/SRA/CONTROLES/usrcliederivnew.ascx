<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrClieDerivNew" CodeFile="usrClieDerivNew.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<anthem:panel id="Panel1" runat="server">
	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
		<TR>
			<TD style="WIDTH: 230px" align="left">
				<TABLE border="0" cellSpacing="0" cellPadding="0">
					<TR>
						<TD id="tdRazaCria" width="50" noWrap align="left" runat="server">
							<asp:TextBox id="txtRazaCodi" runat="server" cssclass="cuadrotexto" AutoPostBack="True" Width="50px"></asp:TextBox></TD>
						<TD id="tdRazaCria1" width="180" noWrap align="left" runat="server">
							<asp:DropDownList id="cmbRazaCria" runat="server" cssclass="combo" AutoPostBack="True" Width="180px"></asp:DropDownList></TD>
					</TR>
				</TABLE>
			</TD>
			<TD align="left">
				<cc1:numberbox id="txtCodi" runat="server" cssclass="cuadrotexto" AutoPostBack="True" Width="50px"
					MaxValor="999999999" MaxLength="9" autocomplete="off" name="txtCodi"></cc1:numberbox></TD>
			<TD align="right">
				<asp:label id="lblApel" runat="server" cssclass="titulo">Apel/R.Soc.:</asp:label>&nbsp;</TD>
			<TD style="WIDTH: 70%" align="left">
				<cc1:textboxtab id="txtApel" runat="server" cssclass="textolibre" TextMode="MultiLine" Rows="1"
					width="98%"></cc1:textboxtab></TD>
			<TD width="80" align="left">
				<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%">
					<TR>
						<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
								id="imgBusc" language="javascript" src="..\imagenes\Buscar16.gif" runat="server">&nbsp;</TD>
						<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
								id="imgLimp" language="javascript" alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp"
								runat="server">&nbsp;</TD>
						<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
								id="imgBuscAvan" language="javascript" alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif"
								runat="server">&nbsp;</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD colSpan="4">
				<DIV 
      style="Z-INDEX: 101; POSITION: absolute; WIDTH: 380px; VISIBILITY: hidden" 
      id="panBuscAvan<%=mstrCtrlId%>" class=paneledicion>
					<TABLE border="0" width="96%" align="center" valign="middle">
						<TR>
							<TD height="34" vAlign="middle" colSpan="2" align="center"><SPAN class="titulo">B�squeda 
									Avanzada</SPAN><INPUT 
            name="hdnBusc<%=mstrCtrlId%>" value=D size=1 type=hidden>
							</TD>
						</TR>
						<TR>
							<TD width="20"></TD>
							<TD><SPAN id="spNomb" runat="server">
									<asp:label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;
									<cc1:textboxtab id="txtNomb" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab></SPAN><SPAN id="br2" runat="server" visible="false"><BR>
								</SPAN><SPAN id="spClieNume" runat="server">
									<asp:label id="lblClieNume" runat="server" cssclass="titulo">Nro.Cliente:</asp:label>&nbsp;
									<cc1:numberbox id="txtClieNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
								</SPAN><SPAN id="br3" runat="server" visible="false">
									<BR>
								</SPAN><SPAN id="spSociNume" runat="server">
									<asp:label id="lblSociNume" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;
									<cc1:numberbox id="txtSociNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
								</SPAN><SPAN id="br4" runat="server" visible="false">
									<BR>
								</SPAN><SPAN style="DISPLAY: inline; CLEAR: none" id="spCriaNume" runat="server">
									<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
										<TR>
											<TD width="5%" align="right">
												<asp:label id="lblCriaNumeRaza" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;</TD>
											<TD>
												<cc1:combobox id="cmbCriaNumeRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
													tag="N" onchange="Combo_change(this)" filtra="true" MostrarBotones="False" NomOper="razas_cargar"
													Height="20px" AceptaNull="False"></cc1:combobox></TD>
										</TR>
									</TABLE>
									<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
										<TR>
											<TD width="15%" align="right">
												<asp:label id="lblCriaNume" runat="server" cssclass="titulo">Nro.Criador:</asp:label>&nbsp;</TD>
											<TD>
												<cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></TD>
										</TR>
									</TABLE>
								</SPAN><SPAN id="br5" runat="server" visible="false">
									<BR>
								</SPAN><SPAN style="DISPLAY: inline; CLEAR: none" id="spLegaNume" runat="server">
									<asp:label id="lblLegaNume" runat="server" cssclass="titulo">Nro.Legajo:</asp:label>&nbsp;
									<cc1:combobox id="cmbInse" class="combo" runat="server" Width="65px" tag="N" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
									<cc1:numberbox id="txtLegaNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox><BR>
								</SPAN><SPAN id="br6" runat="server" visible="false">
									<BR>
								</SPAN><SPAN style="DISPLAY: inline" id="spNoCriaNume" runat="server">
									<asp:label id="lblNoCriaNume" runat="server" cssclass="titulo">Nro.No Criador:</asp:label>&nbsp;
									<cc1:numberbox id="txtNoCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
								</SPAN><SPAN id="br7" runat="server" visible="false">
									<BR>
								</SPAN><SPAN id="spCUIT" runat="server" visible="false">
									<asp:label id="lblCUIT" runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;
									<cc1:cuitbox id="txtCUIT" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:cuitbox>&nbsp;
								</SPAN><SPAN id="br8" runat="server" visible="false">
									<BR>
								</SPAN><SPAN style="DISPLAY: inline" id="spDocuNume" runat="server">
									<BR>
									<asp:label id="lblDocuNumeTido" runat="server" cssclass="titulo">Tipo Doc.:</asp:label>&nbsp;
									<cc1:combobox id="cmbDocuNumeTido" class="combo" runat="server" Width="65px" tag="N" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
									<asp:label id="lblDocuNume" runat="server" cssclass="titulo">Nro.Doc:</asp:label>&nbsp;
									<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox>&nbsp;
								</SPAN><SPAN id="br1" runat="server" visible="false">
									<BR>
								</SPAN><SPAN style="DISPLAY: inline" id="spTarjNume" runat="server">
									<BR>
									<asp:label id="lblTarjNume" runat="server" cssclass="titulo">Tarjeta:</asp:label>&nbsp;
									<cc1:combobox id="cmbTarjTipo" class="combo" runat="server" Width="110px" tag="N" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
									<asp:label id="Label2" runat="server" cssclass="titulo">Nro.:</asp:label>&nbsp;
									<cc1:numberbox id="txtTarjNume" runat="server" cssclass="cuadrotexto" Width="90px" EsTarjeta="True"></cc1:numberbox>&nbsp;
								</SPAN><SPAN id="br9" runat="server" visible="false">
									<BR>
								</SPAN><SPAN style="DISPLAY: inline" id="spEntidad" runat="server">
									<asp:label id="lblEntidad" runat="server" cssclass="titulo">Entidad:</asp:label>&nbsp;
									<cc1:combobox id="cmbEntidad" class="combo" runat="server" Width="104px" tag="N"></cc1:combobox>&nbsp;
								</SPAN><SPAN id="br10" runat="server" visible="false">
									<BR>
								</SPAN><SPAN id="spMedioPago" runat="server">
									<asp:label id="lblMedioPago" runat="server" cssclass="titulo">Medio de Pago:</asp:label>&nbsp;
									<cc1:combobox id="cmbMedioPago" class="combo" runat="server" Width="120px" tag="N" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
								</SPAN><SPAN id="br11" runat="server" visible="false">
									<BR>
								</SPAN><SPAN id="br12" runat="server" visible="false">
									<BR>
								</SPAN><SPAN style="DISPLAY: inline" id="spEmpresa" runat="server">
									<asp:label id="lblEmpresa" runat="server" cssclass="titulo">Empresa:</asp:label>&nbsp;
									<cc1:combobox id="cmbEmpresa" class="combo" runat="server" Width="120px" tag="N" onchange="Combo_change(this)"></cc1:combobox></SPAN><SPAN id="br13" runat="server" visible="false"><BR>
								</SPAN><SPAN style="DISPLAY: inline" id="spClaveUnica" runat="server">
									<asp:label id="lblClaveUnica" runat="server" cssclass="titulo">Clave:</asp:label>&nbsp;
									<cc1:textboxtab id="txtClave" onkeypress="gMascaraCunica(this)" runat="server" cssclass="cuadrotexto"
										Width="100px" maxlength="13"></cc1:textboxtab>&nbsp; </SPAN><SPAN style="DISPLAY: inline; CLEAR: none" id="spExpo" runat="server">
									<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
										<TR>
											<TD width="5%" align="right">
												<asp:label id="lblExpo" runat="server" cssclass="titulo">Nro.Expositor:</asp:label>&nbsp;</TD>
											<TD>
												<cc1:numberbox id="txtExpo" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></TD>
										</TR>
									</TABLE>
								</SPAN><SPAN style="DISPLAY: inline" id="spAgrupa" runat="server">
									<BR>
									<asp:checkbox id="chkAgrupa" cssclass="titulo" Runat="server" Text="Incluir Agrupaciones" Checked="True"></asp:checkbox>&nbsp;
								</SPAN>
							</TD>
						</TR>
						<TR>
							<TD height="40" vAlign="middle" colSpan="2" align="center"><INPUT id="btnBuscAcep<%=mstrCtrlId%>" class=boton onclick="javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1','<%=mstrCtrlId%>');" value=Aceptar type=button>&nbsp;&nbsp;
								<INPUT id="btnBuscCanc<%=mstrCtrlId%>" class=boton onclick="javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>');" value=Cancelar type=button>
							</TD>
						</TR>
					</TABLE>
				</DIV>
				<DIV style="DISPLAY: none">
					<asp:label id="lblId" runat="server">Cliente</asp:label>
					<cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab>
					<asp:label id="lblIdAnt" runat="server"></asp:label>
					<cc1:textboxtab id="txtRazaSessId" runat="server"></cc1:textboxtab>
					<asp:textbox id="txtVieneDeCriadores" runat="server"></asp:textbox>
					<asp:textbox id="txtRazaId" runat="server"></asp:textbox>
					<asp:textbox id="hdnRazaCodi" runat="server"></asp:textbox></DIV>
			</TD>
		</TR>
		<TR>
			<TD colSpan="4" align="right">
				<asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Width="100%" TextMode="MultiLine"
					Rows="2" Enabled="False"></asp:textbox></TD>
		</TR>
	</TABLE>
</anthem:panel>
<script language="javascript">
	if (document.all('chkAgrupa')!=null)
		document.all('chkAgrupa').checked = true;
</script>
