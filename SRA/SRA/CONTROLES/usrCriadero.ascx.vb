Namespace SRA

Partial Class usrCriadero
   Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents lblNomb As System.Web.UI.WebControls.Label
    Protected WithEvents txtNomb As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRpNume As NixorControls.NumberBox
    Protected WithEvents lblCriaNumeRaza As System.Web.UI.WebControls.Label
    Protected WithEvents lblCriaNume As System.Web.UI.WebControls.Label
    Protected WithEvents txtCriaNume As NixorControls.NumberBox
    Protected WithEvents spProp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spRPNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spCriaNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spNaciFecha As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ps1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblProp As System.Web.UI.WebControls.Label
    Protected WithEvents txtProp As NixorControls.TextBoxTab
    Protected WithEvents br3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br4 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br5 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br10 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br11 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents br12 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblNume As System.Web.UI.WebControls.Label
    Protected WithEvents txtNume As NixorControls.NumberBox
    Protected WithEvents lblRpNume As System.Web.UI.WebControls.Label
    Protected WithEvents txtSociNume As NixorControls.NumberBox
    Protected WithEvents lblAsoc As System.Web.UI.WebControls.Label
    Protected WithEvents cmbAsoc As NixorControls.ComboBox
    Protected WithEvents txtAsocNume As NixorControls.NumberBox
    Protected WithEvents spAsocNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblNaciFecha As System.Web.UI.WebControls.Label
    Protected WithEvents br6 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spNomb As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spClie As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtNaciFecha As NixorControls.DateBox
    Protected WithEvents spClieNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spSociNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spLegaNume As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spAgrupa As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spAsoc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents spNachMedioPago As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents cmbSexo As NixorControls.ComboBox


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

   Event Cambio(ByVal sender As System.Object)
   Protected mstrCtrlId As String

#Region "Variables privadas"
   Private mstrConn As String

   Private mstrTabla As String
   Private mstrCampoCodi As String
   Private mintAncho As Integer = 600
   Private mintAlto As Integer = 450
   Private mbooAceptaNull As Boolean = True
   Private mbooAutoPostback As Boolean = False
   Private mbooPermiModi As Boolean = False
   Private mbooSoloBusq As Boolean = False
   Private mstrCampoVal As String = "Criadero"
   Private mstrPagina As String

   Private mbooFilNume As Boolean = False
   Private mbooFilNomb As Boolean = False
   Private mbooFilClie As Boolean = False
   Private mbooFilRaza As Boolean = False
   Private mbooFilLoca As Boolean = False

   Private mstrNume As String = ""
   Private mstrNomb As String = ""
   Private mstrClie As String = ""
   Private mstrRaza As String = ""
   Private mstrLoca As String = ""
   Private mstrEstaId As String = ""

   Private mbooColNume As Boolean = False
   Private mbooColNomb As Boolean = False

   Private mstrSaltos As String = ""
#End Region

#Region "Metodos privados"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      mstrCtrlId = Me.ID
      mstrConn = clsWeb.gVerificarConexion(Me.Page)

      If Not Page.IsPostBack Then
         mSetearEventos()
         clsWeb.gCargarCombo(mstrConn, "razas_cargar @espe_genealo=0", cmbRaza, "id", "descrip_codi", "T")
         clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbpais, "T")
      Else
         If lblIdAnt.Text <> txtId.Text Then
            'para refrescar el estado del textbox
            Valor = txtId.Text
         End If
      End If
      txtId.CampoVal = CampoVal
   End Sub

   Private Sub mSetearEventos()
      Dim lstrEstilo As String
      Dim lstrParams As New StringBuilder

      txtCodi.Attributes.Add("onchange", "CodiCriaDeriv_change('" & UniqueID & "', '" & Math.Abs(CInt(AutoPostback)).ToString & "', '" & Tabla.ToString.ToLower & ";" & CampoCodi.ToLower & ";" & ";" & ";" & EstaId & "', '" & CampoVal & "');")

      'el bot�n de b�squeda
      lstrEstilo = String.Format("location=no,menubar=no,resizable=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width={0}px,height={1}px", Ancho.ToString, Alto.ToString)

      lstrParams.Append("FilNume=" & Math.Abs(CInt(FilNume)).ToString)
      lstrParams.Append("&FilNomb=" & Math.Abs(CInt(FilNomb)).ToString)
      lstrParams.Append("&FilRaza=" & Math.Abs(CInt(FilRaza)).ToString)
      lstrParams.Append("&FilLoca=" & Math.Abs(CInt(FilLoca)).ToString)

      lstrParams.Append("&ColNume=" & Math.Abs(CInt(ColNume)).ToString)
      lstrParams.Append("&ColNomb=" & Math.Abs(CInt(ColNomb)).ToString)
      lstrParams.Append("&SoloBusq=" & Math.Abs(CInt(mbooSoloBusq)))

      imgBusc.Attributes.Add("onclick", "imgBuscCriaDeriv_click('" & UniqueID & "','" & lstrParams.ToString & "','" & Page.Session.SessionID & Now.Millisecond.ToString & "','" & lstrEstilo & "','" & Pagina & "');")
      imgBuscAvan.Attributes.Add("onclick", "mBuscMostrarUsr('panBuscAvan" & mstrCtrlId & "','visible','','" & mstrCtrlId & "');")
      imgLimp.Attributes.Add("onclick", "imgLimpClieDeriv_click('" & UniqueID & "');")

      spRaza.Visible = FilRaza
      spLoca.Visible = FilLoca

   End Sub

   Private Sub txtId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtId.TextChanged
      RaiseEvent Cambio(Me)
   End Sub

   Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
      If Saltos <> "" Then
         Dim vsSaltos() As String = Saltos.Split(",")
         Dim lstrCtrl As String
         Dim j, k As Integer

         For i As Integer = 0 To vsSaltos.GetUpperBound(0)
            j = 0
            While k < 2
               k += 1
               If Me.FindControl(mObtenerSpFil(k)).Visible Then
                  j += 1
               End If
            End While
         Next
      End If
   End Sub

   Private Function mObtenerSpFil(ByVal pintSpanFil) As String
      Select Case pintSpanFil
         Case 1
            Return ("spRaza")
         Case 2
            Return ("spLoca")
      End Select
   End Function
#End Region

#Region "Propiedades publicas"
   Public Property Tabla() As String
      Get
         Return mstrTabla
      End Get
      Set(ByVal Value As String)
         mstrTabla = Value
      End Set
   End Property

   Public Property Pagina() As String
      Get
         If mstrPagina <> "" Then
            Return mstrPagina
         Else
            Dim lstrPagina As New StringBuilder
            Dim lintSub As Integer = Len(Page.TemplateSourceDirectory) - Len(Replace(Page.TemplateSourceDirectory, "/", "")) - 1
            If lintSub = 1 Then
               lstrPagina.Append("../")
            ElseIf lintSub = 2 Then
               lstrPagina.Append("../../")
            End If

            Select Case mstrTabla.ToLower
               Case "criaderos"
                  lstrPagina.Append("criaderos.aspx")
            End Select
            Return (lstrPagina.ToString)
         End If
      End Get
      Set(ByVal Value As String)
         mstrPagina = Value
      End Set
   End Property

   Public Property CampoCodi() As String
      Get
         Return (mstrCampoCodi)
      End Get
      Set(ByVal Value As String)
         mstrCampoCodi = Value
      End Set
   End Property

   Public Property MuestraDesc() As Boolean
      Get
         Return txtDesc.Visible
      End Get

      Set(ByVal Value As Boolean)
         txtDesc.Visible = Value
      End Set
   End Property

   Public Property AceptaNull() As Boolean
      Get
         Return mbooAceptaNull
      End Get

      Set(ByVal Value As Boolean)
         mbooAceptaNull = Value
      End Set
   End Property

   Public Property AutoPostback() As Boolean
      Get
         Return mbooAutoPostback
      End Get

      Set(ByVal AutoPostback As Boolean)
         mbooAutoPostback = AutoPostback
      End Set
   End Property

   Public Property Obligatorio() As Boolean
      Get
         Return txtId.Obligatorio
      End Get

      Set(ByVal Value As Boolean)
         txtId.Obligatorio = Value
      End Set
   End Property

   Public Property CampoVal() As String
      Get
         Return mstrCampoVal
      End Get

      Set(ByVal CampoVal As String)
         mstrCampoVal = CampoVal
      End Set
   End Property

   Public Property Valor() As Object
      Get
         If txtId.Text = "" Then
            If AceptaNull Then
               Return (DBNull.Value)
            Else
               Return (0)
            End If
         Else
            Return (txtId.Text)
         End If
      End Get

      Set(ByVal Valor As Object)
         If (Valor Is DBNull.Value) OrElse (Valor.ToString = "") OrElse (Valor.ToString = "0") Then
            txtId.Text = ""
            txtCriaNomb.Text = ""
            txtCodi.Text = ""
            txtDesc.Text = ""
         Else
            txtId.Text = Valor

            Dim vsRet As String()
            vsRet = SRA_Neg.Utiles.BuscarCriaDeriv(Page.Session("sConn").ToString, txtId.Text & ";" & Tabla & ";id;" & ";" & ";" & ";" & EstaId).Split("|")

            If vsRet.GetUpperBound(0) > 0 Then
               txtCodi.Text = vsRet(1)
               txtCriaNomb.Text = vsRet(2)
               txtDesc.Text = vsRet(3)
            Else
               txtCodi.Text = ""
               txtCriaNomb.Text = ""
               txtDesc.Text = ""
            End If

            If PermiModi Then
               Activo = Activo
            End If
         End If

         lblIdAnt.Text = txtId.Text
      End Set
   End Property

   Public Property Activo() As Boolean
      Get
         Return txtCodi.Enabled
      End Get

      Set(ByVal Activo As Boolean)
         txtCodi.Enabled = Activo
         txtCriaNomb.Enabled = Activo
         imgBusc.Disabled = Not Activo And (Not PermiModi Or Valor Is DBNull.Value)  'si est� inactivo, permite entrar solo a modicar
         imgBuscAvan.Disabled = Not Activo
         imgLimp.Disabled = Not Activo
      End Set
   End Property

   Public Property SoloBusq() As Boolean
      Get
         Return mbooSoloBusq
      End Get

      Set(ByVal SoloBusq As Boolean)
         mbooSoloBusq = SoloBusq
      End Set
   End Property

   Public Property PermiModi() As Boolean
      Get
         Return mbooPermiModi
      End Get

      Set(ByVal PermiModi As Boolean)
         mbooPermiModi = PermiModi
      End Set
   End Property

   Public Property Ancho() As Integer
      Get
         Return mintAncho
      End Get

      Set(ByVal Ancho As Integer)
         mintAncho = Ancho
      End Set
   End Property

   Public Property Alto() As Integer
      Get
         Return mintAlto
      End Get

      Set(ByVal Alto As Integer)
         mintAlto = Alto
      End Set
   End Property

   Public Property Saltos() As String
      Get
         Return mstrSaltos
      End Get

      Set(ByVal Value As String)
         mstrSaltos = Value
      End Set
   End Property

   Public ReadOnly Property ProdProp() As String
      Get
         Return (txtCriaNomb.Text)
      End Get
   End Property

   Public ReadOnly Property Codi() As String
      Get
         Return (txtCodi.Text)
      End Get
   End Property

   Public ReadOnly Property CriaId() As Object
      Get
         Dim oRet As Object
         Dim lDsDatos As DataSet

         oRet = DBNull.Value

         If (Not Valor Is DBNull.Value) AndAlso Valor <> 0 Then
            lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, Tabla, Valor.ToString)
            If lDsDatos.Tables(0).Rows.Count > 0 Then
               Select Case Tabla.ToLower
                  Case "criaderos"
                     oRet = lDsDatos.Tables(0).Rows(0).Item("crdr_id")
               End Select
            End If
         End If

         If Not AceptaNull And oRet Is DBNull.Value Then
            oRet = "0"
         End If

         Return (oRet)
      End Get
   End Property

#Region "Propiedades para seteo de columnas"
   Public Property ColNume() As Boolean
      Get
         Return mbooColNume
      End Get
      Set(ByVal Value As Boolean)
         mbooColNume = Value
      End Set
   End Property

   Public Property ColNomb() As Boolean
      Get
         Return mbooColNomb
      End Get
      Set(ByVal Value As Boolean)
         mbooColNomb = Value
      End Set
   End Property

#End Region

#Region "Propiedades para seteo de filtros"

   Public Property FilNume() As Boolean
      Get
         Return mbooFilNume
      End Get
      Set(ByVal Value As Boolean)
         mbooFilNume = Value
      End Set
   End Property

   Public Property FilNomb() As Boolean
      Get
         Return mbooFilNomb
      End Get
      Set(ByVal Value As Boolean)
         mbooFilNomb = Value
      End Set
   End Property

   Public Property FilLoca() As Boolean
      Get
         Return mbooFilLoca
      End Get
      Set(ByVal Value As Boolean)
         mbooFilLoca = Value
      End Set
   End Property

   Public Property FilClie() As Boolean
      Get
         Return mbooFilClie
      End Get
      Set(ByVal Value As Boolean)
         mbooFilClie = Value
      End Set
   End Property

   Public Property FilRaza() As Boolean
      Get
         Return mbooFilRaza
      End Get
      Set(ByVal Value As Boolean)
         mbooFilRaza = Value
      End Set
   End Property

   Public Property Nomb() As String
      Get
         If txtCriaNomb.Text = "" Then
            Return ("")
         Else
            Return (txtCriaNomb.Text)
         End If
      End Get
      Set(ByVal Value As String)
         mstrNomb = Value
      End Set
   End Property

   Public Property Raza() As String
      Get
         Return mstrRaza
      End Get
      Set(ByVal Value As String)
         mstrRaza = Value
      End Set
   End Property

   Public Property Clie() As String
      Get
         Return mstrClie
      End Get
      Set(ByVal Value As String)
         mstrClie = Value
      End Set
   End Property

   Public Property Nume() As String
      Get
         Return mstrNume
      End Get
      Set(ByVal Value As String)
         mstrNume = Value
      End Set
   End Property

   Public Property EstaId() As String
      Get
         Return mstrEstaId
      End Get
      Set(ByVal Value As String)
         mstrEstaId = Value
      End Set
   End Property

   Public ReadOnly Property txtIdExt() As NixorControls.TextBoxTab
      Get
         Return txtId
      End Get
   End Property

   Public ReadOnly Property txtCodiExt() As NixorControls.NumberBox
      Get
         Return txtCodi
      End Get
   End Property

   Public ReadOnly Property txtCriaNombExt() As NixorControls.TextBoxTab
      Get
         Return txtCriaNomb
      End Get
   End Property
#End Region
#End Region

#Region "Metodos publicos"
   Public Sub Limpiar()
      Valor = DBNull.Value
   End Sub
#End Region

Protected Overrides Sub Finalize()
MyBase.Finalize()
End Sub

        'Private Function cmbpais() As ListControl
        '    Throw New NotImplementedException
        'End Function

    End Class
End Namespace
