<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrProductoExtranjero" CodeFile="usrProductoExtranjero.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
	<TBODY>
		<TR>
			<TD style="HEIGHT: 9px" vAlign="top" width="100%" colSpan="3" align="left">
				<table border="0" cellSpacing="0" cellPadding="0">
					<TBODY>
						<!--<div id="DivEsperePorFavor" runat="server" align="left" style="FONT-FAMILY: Arial; COLOR: red; FONT-WEIGHT: bold"></div>-->
						<tr>
							<td align="right"><asp:label id="lblRaza" cssclass="titulo" runat="server">Raza:</asp:label>&nbsp;</td>
							<td width="820" align="left"><cc1:combobox id="cmbProdRaza" class="combo" cssclass="cuadrotexto" runat="server" filtra="true"
									MostrarBotones="False" NomOper="razas_cargar" Height="20px" AceptaNull="True" Width="60%" onchange="cmbRaza_change(this)"
									Obligatorio="false"></cc1:combobox></td>
						</tr>
						<tr style="DISPLAY: none" id="trRazaId" runat="server">
							<td align="right"><asp:label id="Label2" cssclass="titulo" runat="server" Visible="False">RazaID:</asp:label>&nbsp;</td>
							<td><cc1:textboxtab id="txtRazaId" runat="server" Visible="False"></cc1:textboxtab></td>
						</tr>
						<tr id="trSexo" runat="server">
							<td align="right"><asp:label id="Label3" cssclass="titulo" runat="server">Sexo:</asp:label>&nbsp;</td>
							<td align="left"><cc1:combobox id="cmbProdSexo" class="combo" runat="server" AceptaNull="true" Width="70px" onchange="cmbProdSexo_change(this)">
									<asp:ListItem Value="" Selected="True">(Sexo)</asp:ListItem>
									<asp:ListItem Value="0">Hembra</asp:ListItem>
									<asp:ListItem Value="1">Macho</asp:ListItem>
								</cc1:combobox>&nbsp;
							</td>
						</tr>
						<tr>
							<td align="right"><asp:label id="lblNume" cssclass="titulo" runat="server">Asoc.Extr.:</asp:label>&nbsp;</td>
							<td align="left"><cc1:combobox id="cmbProdAsoc" class="combo" cssclass="cuadrotexto" runat="server" MostrarBotones="False"
									AceptaNull="True" Width="500" onchange="Combo_change(this)" Filtra="True"></cc1:combobox></td>
						</tr>
						<tr id="trNroExtr">
							<td align="right"><asp:label id="Label1" cssclass="titulo" runat="server">Nro.Extr.:</asp:label>&nbsp;</td>
							<td align="left"><cc1:textboxtab id="txtCodi" cssclass="cuadrotexto" runat="server" Width="150" MaxLength="20"></cc1:textboxtab>&nbsp;
								<asp:label id="lblRPExtr" cssclass="titulo" runat="server">RP Extr.:</asp:label>&nbsp;
								<cc1:textboxtab id="txtRPExtr" cssclass="cuadrotexto" runat="server" Width="75" MaxLength="8"></cc1:textboxtab></td>
						</tr>
						<tr>
							<td align="right"><asp:label id="lblApel" cssclass="titulo" runat="server">Nombre:</asp:label>&nbsp;</td>
							<td align="left"><cc1:textboxtab id="txtProdNomb" cssclass="textolibre" runat="server" Height="20" Width="500" TextMode="MultiLine"
									Rows="1"></cc1:textboxtab><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
									id="imgBusc" language="javascript" src="..\imagenes\Buscar16.gif" runat="server">
								<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
									id="imgLimp" language="javascript" alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp"
									runat="server"> <IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
									id="imgAdd" language="javascript" alt="Agregar Importado" src="..\imagenes\add_16.png" runat="server">
								<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
									id="imgBuscAvan" language="javascript" alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif"
									runat="server">
							</td>
						</tr>
						<tr>
							<td align="right"><asp:label id="lblApodo" cssclass="titulo" runat="server">Apodo:</asp:label>&nbsp;</td>
							<td align="left"><cc1:textboxtab id="txtApodo" cssclass="textolibre" runat="server" Height="20" Width="500" TextMode="MultiLine"
									Rows="1"></cc1:textboxtab></td>
						</tr>
						<tr id="trDescrip" runat="server">
							<td align="right"><asp:label id="lblDescrip" cssclass="titulo" runat="server">Descrip.:</asp:label>&nbsp;</td>
							<td align="left"><cc1:textboxtab id="txtDesc" cssclass="textolibredeshab" runat="server" Width="95%" TextMode="MultiLine"
									Rows="2"></cc1:textboxtab></td>
						</tr>
					</TBODY>
				</table>
			</TD>
		</TR>
		<TR>
			<TD colSpan="3">
				<div 
      style="Z-INDEX: 101; POSITION: absolute; WIDTH: 360px; HEIGHT: 231px; VISIBILITY: hidden" 
      id="panBuscAvan<%=mstrCtrlId%>" class=paneledicion>
					<table border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
						<tr>
							<td height="34" vAlign="middle" colSpan="2" align="center"><span class="titulo">B�squeda 
									Avanzada</span><input 
            name="hdnBusc<%=mstrCtrlId%>" value=D size=1 type=hidden 
            ></td>
						</tr>
						<tr>
							<td width="20"></td>
							<td><SPAN id="spRpNume" runat="server"><asp:label id="lblRpNume" cssclass="titulo" runat="server">&nbsp;Nro.RP:</asp:label>&nbsp;<cc1:numberbox id="txtRpNume" cssclass="cuadrotexto" runat="server" Width="100px"></cc1:numberbox>&nbsp;</SPAN>
								<span id="br1" runat="server">
									<br>
								</span><SPAN style="DISPLAY: inline; CLEAR: none" id="spCriaNume" runat="server">
									<asp:label id="lblCriaRaza" cssclass="titulo" runat="server" Visible="true">&nbsp;Raza:</asp:label>&nbsp;<cc1:combobox id="cmbCriaNumeRaza" class="combo" runat="server" Width="197px" onchange="Combo_change(this)"
										tag="N"></cc1:combobox>
									<asp:label id="lblCriaNume" cssclass="titulo" runat="server">&nbsp;Nro.Criador:&nbsp;</asp:label><cc1:numberbox id="txtCriaNume" cssclass="cuadrotexto" runat="server" Width="100px"></cc1:numberbox></SPAN><span id="br2" runat="server"><br>
								</span><SPAN style="DISPLAY: inline" id="spLaboNume" runat="server">
									<asp:label id="lblLaboNume" cssclass="titulo" runat="server">&nbsp;Nro.Tr�m.Laborat.:&nbsp;</asp:label><cc1:numberbox id="txtLaboNume" cssclass="cuadrotexto" runat="server" Width="70px"></cc1:numberbox></SPAN><span id="br3" runat="server"><br>
								</span><SPAN id="spNaciFecha" runat="server">
									<asp:label id="lblNaciFecha" cssclass="titulo" runat="server">&nbsp;F.Nacim.:&nbsp;</asp:label><cc1:datebox id="txtNaciFecha" cssclass="cuadrotexto" runat="server" Width="70px" IncludesUrl="Includes/"
										ImagesUrl="Images/"></cc1:datebox></SPAN><span id="br4" runat="server"></span></td>
						</tr>
						<tr>
							<td height="40" vAlign="middle" colSpan="2" align="center"><input id="btnBuscAcep<%=mstrCtrlId%>" class=boton onclick="javascript:mAceptarProd(<%=mstrCtrlId%>)" value=Aceptar type=button>&nbsp;&nbsp;
								<input id="btnBuscCanc<%=mstrCtrlId%>" class=boton onclick="javascript:mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>');"value=Cancelar type=button>
							</td>
						</tr>
					</table>
				</div>
				<DIV style="DISPLAY: none">
					<cc1:textboxtab id="hdnRazaCruza" runat="server" autopostback="true"></cc1:textboxtab>
					<cc1:textboxtab id="hdnValiExis" runat="server" autopostback="true"></cc1:textboxtab>
					<asp:textbox id="hdnCriaNume" runat="server"></asp:textbox>
					<asp:textbox id="hdnCriaOrProp" runat="server"></asp:textbox>
					<asp:textbox id="txtTipoProducto" runat="server"></asp:textbox>
					<asp:textbox id="txtRegtId" runat="server"></asp:textbox>
					<asp:label id="lblId" runat="server">Cliente</asp:label>
					<cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab>
					<cc1:textboxtab id="txtSexoId" runat="server" size="4"></cc1:textboxtab>
					<cc1:textboxtab id="txtRazaSessId" runat="server"></cc1:textboxtab>
					<asp:label id="lblIdAnt" runat="server"></asp:label>
					<asp:textbox id="txtCriaId" runat="server"></asp:textbox>
					<asp:textbox id="txtIgnoraInexist" runat="server"></asp:textbox>
					<asp:textbox id="txtAutoGeneraHBA" runat="server"></asp:textbox>
					<asp:textbox id="txtNacionalidad" runat="server"></asp:textbox>
					<asp:textbox id="txtFechaParametro" runat="server"></asp:textbox>
					<cc1:textboxtab id="hidFechaNaci" runat="server"></cc1:textboxtab>
					<cc1:textboxtab id="hidRPNac" runat="server"></cc1:textboxtab>
					<cc1:textboxtab id="hidTipoRegistro" runat="server"></cc1:textboxtab>
					<cc1:textboxtab id="hidVariedad" runat="server"></cc1:textboxtab>
					<cc1:textboxtab id="hidPelaAPeli" runat="server"></cc1:textboxtab>
					<asp:textbox id="hdnIdCtrlPropietario" runat="server"></asp:textbox>
				</DIV>
			</TD>
		</TR>
	</TBODY>
</TABLE>
<script language="javascript">
	function mValidaRaza(pCtrl)
	{
		//Valido que haya completado la Raza.
		if (document.all(pCtrl + ":cmbProdRaza").value == "")
			{
				alert("Debe indicar la Raza.");
				return(false);
			}
			
		return(true);
	}
	
	function mAceptarProd(pCtrl)
	{
		if (mValidaConsultaProd(pCtrl))
			mBuscMostrarProdUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1',pCtrl);
	}
</script>
