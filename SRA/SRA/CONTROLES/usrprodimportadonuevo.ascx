<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="usrClieDeriv.ascx" %>
<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrProdImportadoNuevo" CodeFile="usrProdImportadoNuevo.ascx.vb" %>
<% If mbooMostrarProducto Then %>
<TR>
	<TD vAlign="middle" align="right">
		<asp:label id="lblProducto" cssclass="titulo" runat="server">Producto:</asp:label>&nbsp;</TD>
	<TD>
		<UC1:PROD id="usrProducto" runat="server" FilDocuNume="True" MuestraDesc="True" FilTipo="S"
			FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="productos" AceptaNull="true"></UC1:PROD></TD>
</TR>
<TR id="rowRPExtPX" style="DISPLAY: none" runat="server">
	<TD vAlign="middle" align="right">
		<asp:label id="lblRPExtr" cssclass="titulo" runat="server">RP Extranjero:&nbsp;</asp:label></TD>
	<TD>
		<CC1:TEXTBOXTAB id="txtRPExtr" cssclass="cuadrotexto" runat="server" Width="120px"></CC1:TEXTBOXTAB>&nbsp;&nbsp;
		<asp:label id="lblPX" cssclass="titulo" runat="server">PX:</asp:label>
		<CC1:TEXTBOXTAB id="txtPX" cssclass="cuadrotexto" runat="server" Width="40px" MaxLength="4"></CC1:TEXTBOXTAB>&nbsp;&nbsp;
		<asp:label id="lblFechaNac" cssclass="titulo" runat="server">F.Nacimiento:</asp:label>
		<cc1:DateBox id="txtFechaNac" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
</TR>
<TR id="rowProdPedigree" runat="server">
	<TD></TD>
	<TD>
		<BUTTON class="boton" id="btnProdPedigree" style="WIDTH: 75px" onclick="btnPedigree_click('usrProducto');"
			runat="server" value="Pedigree">Pedigree</BUTTON>
	</TD>
</TR>
<TR>
	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<% End If %>
<% If mbooMostrarCriador Then %>
<TR>
	<TD align="right">
		<asp:label id="lblCria" cssclass="titulo" runat="server">Criador:&nbsp;</asp:label></TD>
	<TD>
		<UC1:CLIE id="usrCria" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True" Saltos="1,2"
			Criador="true" Tabla="Criadores"></UC1:CLIE></TD>
</TR>
<TR>
	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>

</TR>
<% End If %>
<% If mbooMostrarResultado Then %>
<!--aaaaa<TR>
	<TD align="right">&nbsp;</TD>
	&nbsp;
	<TD>&nbsp;&nbsp;&nbsp;</TD>
</TR>
-->
<TR>
	<TD vAlign="middle" align="right"></TD>
	<TD style="HEIGHT: 14px" vAlign="middle"></TD>
</TR><!--aaaaa
<TR>
	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>-->
<% End If %>
<% If mbooMostrarPadre Then %>
<TR>
	<TD vAlign="middle" align="right">
		<asp:label id="lblPadre" cssclass="titulo" runat="server">Padre:&nbsp;</asp:label></TD>
	<TD style="HEIGHT: 14px" vAlign="middle">
		<UC1:PROD id="usrPadre" runat="server" Ancho="800" FilDocuNume="True" MuestraDesc="True" FilTipo="S"
			FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="productos" FilSexo="false"
			Sexo="1"></UC1:PROD></TD>
</TR>
<TR id="rowPadrePedigree" runat="server">
	<TD vAlign="middle" align="right"></TD>
	<TD>
		<BUTTON class="boton" id="btnPadrePedigree" style="WIDTH: 75px" onclick="btnPedigree_click('usrPadre');"
			runat="server" value="Pedigree">Pedigree</BUTTON>
	</TD>
</TR>
<TR>
	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<% End If %>
<% If mbooMostrarMadre Then %>
<TR>
	<TD vAlign="middle" align="right">
		<asp:label id="lblMadre" cssclass="titulo" runat="server">Madre:&nbsp;</asp:label></TD>
	<TD vAlign="middle">
		<UC1:PROD id="usrMadre" runat="server" Ancho="800" FilDocuNume="True" MuestraDesc="True" FilTipo="S"
			FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="productos" FilSexo="false"
			Sexo="0"></UC1:PROD></TD>
</TR>
<TR id="rowMadrePedigree" runat="server">
	<TD vAlign="middle" align="right"></TD>
	<TD>
		<BUTTON class="boton" id="btnMadrePedigree" style="WIDTH: 75px" onclick="btnPedigree_click('usrMadre');"
			runat="server" value="Pedigree">Pedigree</BUTTON>
	</TD>
</TR>
<TR>
	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<% End If %>
<TR>
	<TD colSpan="2">
		<DIV style="DISPLAY: none">
			<asp:textbox id="hdnProdId" runat="server"></asp:textbox>
			<asp:textbox id="hdnPropId" runat="server"></asp:textbox>
		</DIV>
	</TD>
</TR>
<script language="javascript">
	function mSetearBotonesPedigree()
	{
		if (document.all("usrProd_btnProdPedigree")!=null)
			document.all("usrProd_btnProdPedigree").disabled = (document.all("usrProd:usrProducto:txtId").value == "");
		
		if (document.all("usrProd_btnPadrePedigree")!=null)
			document.all("usrProd_btnPadrePedigree").disabled = (document.all("usrProd:usrPadre:txtId").value == "");
		
		if (document.all("usrProd_btnMadrePedigree")!=null)
			document.all("usrProd_btnMadrePedigree").disabled = (document.all("usrProd:usrMadre:txtId").value == "");
	}
	
	function mSetearBotonesSRADisabled()
	{
		if (document.all("usrProd_btnProdPedigree")!=null)
			document.all("usrProd_btnProdPedigree").disabled = (document.all("usrProd:usrProducto:txtId").value == "");
		
			}
	

	function btnPedigree_click(pstrControl)
	{	
		gAbrirVentanas("pedigree.aspx?prod=" + document.all("usrProd:" + pstrControl + ":txtId").value + "&raza=" + document.all("usrProd:" + pstrControl + ":cmbProdRaza").value + "&codi=" + document.all("txtusrProd:" + pstrControl + ":cmbProdRaza").value, 7, "","","","",true);
	}

	function usrProd_usrPadre_cmbProdRaza_onchange(pRaza)
	{
		mSetearRazasPadres(pRaza,'Madre');
	}
	
	function usrProd_usrMadre_cmbProdRaza_onchange(pRaza)
	{
		mSetearRazasPadres(pRaza,'Padre');
	}
	
	function mSetearRazasPadres(pRaza,pControl)
	{
		if (document.all('usrProd:usr' + pControl + ':cmbProdRaza').value != document.all(pRaza).value)
		{
			document.all('usrProd:usr' + pControl + ':cmbProdRaza').value =document.all(pRaza).value;
			document.all('usrProd:usr' + pControl + ':cmbProdRaza').onchange();
		}
	}

    function usrProd_usrProducto_cmbProdRaza_onchange(pRaza)
	{
		var sFiltro = document.all(pRaza).value;
		if(sFiltro != "")
		{
			if (document.all("usrProd:cmbAres")!=null)
				LoadComboXML("rg_analisis_resul_cargar", "@raza_id=" + sFiltro, "usrProd:cmbAres", "S", true);

			if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
			{
				document.all("usrProd:usrCria:cmbRazaCria").value = document.all(pRaza).value;
				document.all("usrProd:usrCria:cmbRazaCria").onchange();
				document.all("usrProd:usrCria:cmbRazaCria").disabled = true;
				document.all("txtusrProd:usrCria:cmbRazaCria").disabled = true;
			}
			if (document.all("usrProd:usrPadre:cmbProdRaza")!=null)
			{
				document.all("usrProd:usrPadre:cmbProdRaza").value = document.all(pRaza).value;
				document.all("usrProd:usrPadre:cmbProdRaza").onchange();
				document.all("usrProd:usrPadre:cmbProdRaza").disabled = true;
				document.all("txtusrProd:usrPadre:cmbProdRaza").disabled = true;
			}
			if (document.all("usrProd:usrMadre:cmbProdRaza")!=null)
			{
				document.all("usrProd:usrMadre:cmbProdRaza").value = document.all(pRaza).value;
				document.all("usrProd:usrMadre:cmbProdRaza").onchange();
				document.all("usrProd:usrMadre:cmbProdRaza").disabled = true;
				document.all("txtusrProd:usrMadre:cmbProdRaza").disabled = true;
			}
		}
		else
		{
			if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
			{
				document.all("usrProd:usrCria:cmbRazaCria").value = "";
				document.all("usrProd:usrCria:cmbRazaCria").onchange();
			}
			if (document.all("usrProd:usrMadre:cmbProdRaza")!=null)
			{
				document.all("usrProd:usrMadre:cmbProdRaza").value = "";
				document.all("usrProd:usrMadre:cmbProdRaza").onchange();
			}
			if (document.all("usrProd:usrPadre:cmbProdRaza")!=null)
			{	
				document.all("usrProd:usrPadre:cmbProdRaza").value = "";
				document.all("usrProd:usrPadre:cmbProdRaza").onchange();
				
				if (document.all("usrProd:usrPadre:cmbProdAsoc").disabled==false)
				{
					document.all("usrProd:usrCria:cmbRazaCria").disabled = false;
					document.all("txtusrProd:usrCria:cmbRazaCria").disabled = false;
					
					document.all("usrProd:usrPadre:cmbProdRaza").disabled = false;
					document.all("txtusrProd:usrPadre:cmbProdRaza").disabled = false;
					document.all("usrProd:usrMadre:cmbProdRaza").disabled = false;
					document.all("txtusrProd:usrMadre:cmbProdRaza").disabled = false;
				}
				
				//document.all("usrProd:cmbAres").innerText="";
			}
		}
	}
		
	function mLimpiarAdicionales()
	{
		document.all("usrProd:hdnProdId").value = "";
						
		document.all("usrProd:hdnPropId").value = "";
		
		if (document.all("usrClieProp_txtCodi")!=null)
			document.all("usrClieProp_imgLimp").onclick();
		
		document.all("usrProd:txtPX").value = "";
		document.all("usrProd:txtRPExtr").value = "";
							
		if (document.all("usrProd_usrCria_imgLimp")!=null)
			document.all("usrProd_usrCria_imgLimp").onclick();
		if (document.all("usrProd_usrPadre_imgLimp")!=null)
			document.all("usrProd_usrPadre_imgLimp").onclick();
		if (document.all("usrProd_usrMadre_imgLimp")!=null)
			document.all("usrProd_usrMadre_imgLimp").onclick();
	}

	function usrProducto_onchange()
	{
		usrProducto_onchange('');
	}


function mCargarPelajes(strFiltro)
{
	mNumeDenoConsul(strFiltro,false);
	if (strFiltro!='')
	{
       vsRet=EjecutarMetodoXML("Utiles.EspeRazaJs", strFiltro).split(";");
      
       switch (vsRet[0])
		{
			case EspePeliferos:
			   {
			    if (document.all('cmbPelaAPeli').value == '')
				 {
					LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbPelaAPeli", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",2", "cmbPelaBPeli", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",3", "cmbPelaCPeli", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",4", "cmbPelaDPeli", "S");
				 }
				break;
			   }
	        case RazaHolando:	
  // 2 - Bovino Holando
			   {
			     if (document.all('cmbHolanPelaA').value == null)
				 {
				  LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbHolanPelaA", "S");
				 }
				break;
			   }
		    case EspeBovinosNOHolando:	 
 // 2 - Bovino  - NO Holando -
			   {
			   if (document.all('cmbBovPelaA').value == '' )
			    {
			    	LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbBovPelaA", "S");
			    }
				break;
			   } 
		    case RazaCriolla:	
  // 2 - Equino Criolla
			   {
			   if (document.all('cmbCrioPelaA').value == '')
			    {
					LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbCrioPelaA", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",2", "cmbCrioPelaB", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",3", "cmbCrioPelaC", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",4", "cmbCrioPelaD", "S");
			 	}
			 	break;
		       } 
		    default:
		      {
		       if (document.all('cmbPelaA').value == '')
			    {
				
					LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbPelaA", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",2", "cmbPelaB", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",3", "cmbPelaC", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",4", "cmbPelaD", "S");
			    }
			   break;
		      }
		}

	}
}

	function usrProducto_onchange(pTipo)
	{
		if (document.all('usrProd:usrProducto:txtId').value != "")
		{
			var sFiltro = "@prdt_id=" + document.all('usrProd:usrProducto:txtId').value;
			
			if (document.all('hdnId')!=null && document.all('hdnId').value!="") //solo para saber de que tramite vino el producto cuando se dio de alta
				sFiltro = sFiltro + ",@trpr_tram_id=" + document.all('hdnId').value;
				                                                                                                            /*   5    */                                                                                                                        /*  16 */
			var vstrRet = LeerCamposXML("productoxSraNume", sFiltro, "prdt_id,prdt_px,prdt_rp_extr,padre_id,padre_raza_id,NumeroPadre,padre_asoc_id,prdt_sra_nume,prdt_rp,madre_id,madre_raza_id,NumeroMadre,madre_asoc_id,prdt_sra_nume,prdt_rp,prdt_cria_id,prta_nume,prta_tipo,prta_ares_id,_cria_nume,prdt_prop_clie_id").split("|");
			if(vstrRet=="")
			{
				if (document.all["usrProd:usrProducto:txtIgnoraInexist"].value != "1" || pTipo != "asoc_nume")
					mLimpiarAdicionales();
			}
			else
			{
				if (document.all("usrProd:hdnProdId").value != vstrRet[0])
				{
					document.all("usrProd:hdnProdId").value = vstrRet[0];
					
					if (document.all("usrClieProp_txtCodi")!=null)
					{
						document.all("usrClieProp_txtCodi").value = vstrRet[20];
						document.all("usrClieProp_txtCodi").onchange();
					}
					else
					{
						
					}
					
					document.all("usrProd:txtPX").value = vstrRet[1];
					document.all("usrProd:txtRPExtr").value = vstrRet[2];
					 
					if (document.all("usrProd:usrCria:txtCodi")!=null)
					{
						document.all("usrProd:usrCria:txtCodi").value = vstrRet[18];
						document.all("usrProd:usrCria:txtCodi").onchange();
					}
					if (document.all("usrProd:usrPadre:cmbProdRaza")!=null)
					{
						document.all("usrProd:usrPadre:cmbProdRaza").value = vstrRet[4];
						document.all("usrProd:usrPadre:cmbProdRaza").onchange();
						document.all("usrProd:usrPadre:cmbProdAsoc").value = vstrRet[6];
						document.all("usrProd:usrPadre:txtCodi").value = vstrRet[5];

						if (document.all("usrProd:usrPadre:txtRP")!=null)
							document.all("usrProd:usrPadre:txtRP").value = vstrRet[8];
						document.all("usrProd:usrPadre:txtSraNume").value = vstrRet[7];
						document.all("usrProd:usrPadre:txtSraNume").onchange();
					}
					
					if (document.all("usrProd:usrMadre:cmbProdRaza")!=null)
					{
						document.all("usrProd:usrMadre:cmbProdRaza").value = vstrRet[10];
						document.all("usrProd:usrMadre:cmbProdRaza").onchange();
						document.all("usrProd:usrMadre:cmbProdAsoc").value = vstrRet[12];
						document.all("usrProd:usrMadre:txtCodi").value = vstrRet[11];

						if (document.all("usrProd:usrMadre:txtRP")!=null)
							document.all("usrProd:usrMadre:txtRP").value = vstrRet[14];
						document.all("usrProd:usrMadre:txtSraNume").value = vstrRet[13];
						document.all("usrProd:usrMadre:txtSraNume").onchange();
					}
				}
			}
		}
		else
			mLimpiarAdicionales();
		
		mSetearBotonesPedigree();
	}
	
	if (document.all('txtusrProd:usrProducto:cmbProdRaza')!=null)
	{
		if (document.all('txtusrProd:usrPadre:cmbProdRaza')!=null)
		{
			document.all('txtusrProd:usrPadre:cmbProdRaza').value = document.all('txtusrProd:usrProducto:cmbProdRaza').value;
			document.all('txtusrProd:usrPadre:cmbProdRaza').onchange();
			document.all('txtusrProd:usrPadre:cmbProdRaza').disabled = (document.all('txtusrProd:usrProducto:cmbProdRaza').value!='');
			document.all('usrProd:usrPadre:cmbProdRaza').disabled = (document.all('txtusrProd:usrProducto:cmbProdRaza').value!='');
		}	
		if (document.all('txtusrProd:usrMadre:cmbProdRaza')!=null)
		{
			document.all('txtusrProd:usrMadre:cmbProdRaza').value = document.all('txtusrProd:usrProducto:cmbProdRaza').value;
			document.all('txtusrProd:usrMadre:cmbProdRaza').onchange();
			document.all('txtusrProd:usrMadre:cmbProdRaza').disabled = (document.all('txtusrProd:usrProducto:cmbProdRaza').value!='');
			document.all('usrProd:usrMadre:cmbProdRaza').disabled = (document.all('txtusrProd:usrProducto:cmbProdRaza').value!='');
		}
		if (document.all('txtusrProd:usrCria:cmbRazaCria')!=null)
		{
			document.all('txtusrProd:usrCria:cmbRazaCria').value = document.all('txtusrProd:usrProducto:cmbProdRaza').value;
			document.all('txtusrProd:usrCria:cmbRazaCria').onchange();
			document.all('txtusrProd:usrCria:cmbRazaCria').disabled = (document.all('txtusrProd:usrProducto:cmbProdRaza').value!='');
			document.all('usrProd:usrCria:cmbRazaCria').disabled = (document.all('txtusrProd:usrProducto:cmbProdRaza').value!='');				
		}
	}
	
	mSetearBotonesPedigree();
</script>
