<%@ Control Language="vb" AutoEventWireup="false" Inherits="SRA.usrClieDeriv" CodeFile="usrClieDeriv.ascx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD align="left">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right" id="tdRazaCria" runat="server" Width="230" nowrap>
						<cc1:combobox class="combo" name="cmbRazaCria" id="cmbRazaCria" runat="server" cssclass="cuadrotexto"
							Width="99%" AceptaNull="False" Height="20px" NomOper="razas_cargar" MostrarBotones="False"
							filtra="true" onchange="cmbRaza_change(this)"></cc1:combobox></td>
					<td align="left" id="tdtxtCodi" runat="server">
						<cc1:numberbox id="txtCodi" name="txtCodi" autocomplete="off" MaxLength="9" MaxValor="999999999"
							runat="server" cssclass="cuadrotexto" Width="50px"></cc1:numberbox></td>
				</tr>
			</table>
		</TD>
		<TD align="right" id="tdlblApel" runat="server">
			&nbsp;<asp:label id="lblApel" runat="server" cssclass="titulo">Apel/R.Soc.:</asp:label>&nbsp;</TD>
		<TD align="left" style="WIDTH: 100%" id="tdtxtApel" runat="server">
			<cc1:textboxtab id="txtApel" width="98%" runat="server" cssclass="textolibre" Rows="1" TextMode="MultiLine"></cc1:textboxtab></TD>
		<TD align="left" width="80" id="tdTable1" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><IMG language="javascript" id="imgBusc" style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							src="..\imagenes\Buscar16.gif" runat="server">&nbsp;</TD>
					<TD><IMG language="javascript" id="imgLimp" style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							alt="Limpiar Datos" src="..\imagenes\Limpiar16.bmp" runat="server">&nbsp;</TD>
					<TD><IMG language="javascript" id="imgBuscAvan" style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
							alt="B�squeda Avanzada" src="..\imagenes\buscavan.gif" runat="server">&nbsp;</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD colSpan="4">
			<div class=paneledicion style='Z-INDEX: 101; POSITION: absolute; WIDTH: 380px; VISIBILITY: hidden' id="panBuscAvan<%=mstrCtrlId%>" >
				<table border="0" align="center" valign="middle" width="96%">
					<tr>
						<td colspan="2" align="center" valign="middle" height="34"><span class="titulo">B�squeda 
								Avanzada</span><input type=hidden size=1 name="hdnBusc<%=mstrCtrlId%>" value=D>
						</td>
					</tr>
					<tr>
						<td width="20"></td>
						<td>
							<SPAN id="spNomb" runat="server">
								<asp:label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;<cc1:textboxtab id="txtNomb" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab>
							</SPAN><span runat="server" visible="false" id="br2">
								<br>
							</span><SPAN id="spClieNume" runat="server">
								<asp:label id="lblClieNume" runat="server" cssclass="titulo">Nro.Cliente:</asp:label>&nbsp;<cc1:numberbox id="txtClieNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN><span runat="server" visible="false" id="br3">
								<br>
							</span><SPAN id="spSociNume" runat="server">
								<asp:label id="lblSociNume" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;<cc1:numberbox id="txtSociNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN><span runat="server" visible="false" id="br4">
								<br>
							</span><SPAN id="spCriaNume" style="DISPLAY: inline; CLEAR: none" runat="server">
								<table cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td align="right" width="5%"><asp:label id="lblCriaNumeRaza" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;</td>
										<td><cc1:combobox tag="N" class="combo" id="cmbCriaNumeRaza" runat="server" cssclass="cuadrotexto"
												Width="100%" AceptaNull="False" Height="20px" NomOper="razas_cargar" MostrarBotones="False"
												filtra="true" onchange="Combo_change(this)"></cc1:combobox></td>
									</tr>
								</table>
								<table cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td align="right" width="15%"><asp:label id="lblCriaNume" runat="server" cssclass="titulo">Nro.Criador:</asp:label>&nbsp;</td>
										<td><cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></td>
									</tr>
								</table>
							</SPAN><span runat="server" visible="false" id="br5">
								<br>
							</span><span id="spLegaNume" style="DISPLAY: inline; CLEAR: none" runat="server">
								<asp:label id="lblLegaNume" runat="server" cssclass="titulo">Nro.Legajo:</asp:label>
								&nbsp;<cc1:combobox tag="N" class="combo" id="cmbInse" runat="server" Width="65px" onchange="Combo_change(this)"></cc1:combobox>
								&nbsp;<cc1:numberbox id="txtLegaNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox><br>
							</span><span runat="server" visible="false" id="br6">
								<br>
							</span><SPAN id="spNoCriaNume" style="DISPLAY: inline" runat="server">
								<asp:label id="lblNoCriaNume" runat="server" cssclass="titulo">Nro.No Criador:</asp:label>&nbsp;<cc1:numberbox id="txtNoCriaNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox>&nbsp;
							</SPAN><span runat="server" visible="false" id="br7">
								<br>
							</span><span id="spCUIT" visible="false" runat="server">
								<asp:label id="lblCUIT" runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;<cc1:cuitbox id="txtCUIT" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:cuitbox>&nbsp;
							</span><span runat="server" visible="false" id="br8">
								<br>
							</span><SPAN id="spDocuNume" style="DISPLAY: inline" runat="server">
								<br>
								<asp:label id="lblDocuNumeTido" runat="server" cssclass="titulo">Tipo Doc.:</asp:label>&nbsp;<cc1:combobox tag="N" class="combo" id="cmbDocuNumeTido" runat="server" Width="65px" onchange="Combo_change(this)"></cc1:combobox>&nbsp;<asp:label id="lblDocuNume" runat="server" cssclass="titulo">Nro.Doc:</asp:label>&nbsp;<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:numberbox>&nbsp;
							</SPAN><span runat="server" visible="false" id="br1">
								<br>
							</span><SPAN id="spTarjNume" style="DISPLAY: inline" runat="server">
								<br>
								<asp:label id="lblTarjNume" runat="server" cssclass="titulo">Tarjeta:</asp:label>&nbsp;<cc1:combobox tag="N" class="combo" id="cmbTarjTipo" runat="server" Width="110px" onchange="Combo_change(this)"></cc1:combobox>&nbsp;<asp:label id="Label2" runat="server" cssclass="titulo">Nro.:</asp:label>&nbsp;<cc1:numberbox id="txtTarjNume" EsTarjeta="True" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:numberbox>&nbsp;
							</SPAN><span runat="server" visible="false" id="br9">
								<br>
							</span><SPAN id="spEntidad" style="DISPLAY: inline" runat="server">
								<asp:label id="lblEntidad" runat="server" cssclass="titulo">Entidad:</asp:label>&nbsp;<cc1:combobox tag="N" class="combo" id="cmbEntidad" runat="server" Width="104px"></cc1:combobox>&nbsp;
							</SPAN><span runat="server" visible="false" id="br10">
								<br>
							</span><SPAN id="spMedioPago" runat="server">
								<asp:label id="lblMedioPago" runat="server" cssclass="titulo">Medio de Pago:</asp:label>&nbsp;<cc1:combobox tag="N" class="combo" id="cmbMedioPago" runat="server" Width="120px" onchange="Combo_change(this)"></cc1:combobox>&nbsp;
							</SPAN><span runat="server" visible="false" id="br11">
								<br>
							</span><span runat="server" visible="false" id="br12">
								<br>
							</span><SPAN id="spEmpresa" style="DISPLAY: inline" runat="server">
								<asp:label id="lblEmpresa" runat="server" cssclass="titulo">Empresa:</asp:label>&nbsp;<cc1:combobox tag="N" class="combo" id="cmbEmpresa" runat="server" Width="120px" onchange="Combo_change(this)"></cc1:combobox>
							</SPAN><span runat="server" visible="false" id="br13">
								<br>
							</span><SPAN id="spClaveUnica" style="DISPLAY: inline" runat="server">
								<asp:label id="lblClaveUnica" runat="server" cssclass="titulo">Clave:</asp:label>&nbsp;<cc1:textboxtab id="txtClave" onkeypress="gMascaraCunica(this)" runat="server" cssclass="cuadrotexto"
									maxlength="13" Width="100px"></cc1:textboxtab>&nbsp; </SPAN><SPAN id="spExpo" style="DISPLAY: inline; CLEAR: none" runat="server">
								<table cellSpacing="0" cellPadding="0" width="100%" border="0">
									<tr>
										<td align="right" width="5%"><asp:label id="lblExpo" runat="server" cssclass="titulo">Nro.Expositor:</asp:label>&nbsp;</td>
										<td><cc1:numberbox id="txtExpo" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></td>
									</tr>
								</table>
							</SPAN><SPAN id="spAgrupa" style="DISPLAY: inline" runat="server">
								<br>
								<asp:checkbox id="chkAgrupa" cssclass="titulo" Checked="True" Text="Incluir Agrupaciones" Runat="server"></asp:checkbox>&nbsp;
							</SPAN>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" valign="middle" height="40">
							<input type=button class=boton value=Aceptar onclick="javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','1','<%=mstrCtrlId%>');"id="btnBuscAcep<%=mstrCtrlId%>">&nbsp;&nbsp;
							<input type=button class=boton value=Cancelar onclick="javascript:mBuscMostrarUsr('panBuscAvan<%=mstrCtrlId%>','hidden','0','<%=mstrCtrlId%>');"id="btnBuscCanc<%=mstrCtrlId%>">
						</td>
					</tr>
				</table>
			</div>
			<DIV style="DISPLAY: none">
				<asp:label id="lblId" runat="server">Cliente</asp:label>
				<cc1:textboxtab id="txtId" runat="server" autopostback="true"></cc1:textboxtab>
				<asp:label id="lblIdAnt" runat="server"></asp:label>
				<cc1:textboxtab id="txtRazaSessId" runat="server"></cc1:textboxtab>
				<asp:textbox id="txtRazaId" runat="server"></asp:textbox>
			</DIV>
		</TD>
	</TR>
	<TR>
		<TD align="right" colSpan="4"><asp:textbox id="txtDesc" runat="server" cssclass="textolibredeshab" Rows="2" TextMode="MultiLine"
				Width="100%" Enabled="False"></asp:textbox></TD>
	</TR>
</TABLE>
<script language="javascript">
	if (document.all('chkAgrupa')!=null)
		document.all('chkAgrupa').checked = true;
</script>
