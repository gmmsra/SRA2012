Imports Entities
Imports ReglasValida.Validaciones
Imports AccesoBD


Namespace SRA


Partial Class usrProdFiltro
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents usrProdFil As usrProducto


   




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Dim mstrCriadores As String = "criadores"
    Dim mstrClientes As String = "clientes"
    Dim mbooSexoObliga As Boolean = False
    Private mbooVisibleAdicionales As Boolean = True

#End Region

#Region "Definición de Propiedades"
    Public Property VisibleAdicionales() As Boolean
        Get
            Return mbooVisibleAdicionales
        End Get
        Set(ByVal Value As Boolean)
            mbooVisibleAdicionales = Value
        End Set
    End Property

    Public ReadOnly Property chkBuscarEn() As System.Web.UI.WebControls.CheckBox
        Get
            Return (chkBusc)
        End Get
    End Property
    Public ReadOnly Property chkDadosBaja() As System.Web.UI.WebControls.CheckBox
        Get
            Return (chkBaja)
        End Get
    End Property
    Public ReadOnly Property optExpediente() As System.Web.UI.WebControls.RadioButton
        Get
            Return (optCria)
        End Get
    End Property
    Public ReadOnly Property optCliente() As System.Web.UI.WebControls.RadioButton
        Get
            Return (optClie)
        End Get
    End Property

    Public ReadOnly Property optSexo() As String
        Get
            Return (cmbSexoFil.SelectedValue.ToString)
        End Get
    End Property

    Public Property SexoObligatorio() As Boolean
        Get
            Return (mbooSexoObliga)
        End Get
        Set(ByVal Value As Boolean)
            mbooSexoObliga = Value
            If mbooSexoObliga Then
                cmbSexoFil.Items.Clear()
                cmbSexoFil.Items.Add("(Seleccione)")
                cmbSexoFil.Items(cmbSexoFil.Items.Count - 1).Value = ""
                cmbSexoFil.Items.Add("Hembra")
                cmbSexoFil.Items(cmbSexoFil.Items.Count - 1).Value = "0"
                cmbSexoFil.Items.Add("Macho")
                cmbSexoFil.Items(cmbSexoFil.Items.Count - 1).Value = "1"
            End If

        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me.Page)
            mInicializar()

            If (VisibleAdicionales = False) Then
                cmbRazaFil.Visible = False
                lblRazaFil.Visible = False

                lblNumeFil.Visible = False
                txtNumeFil.Visible = False
                trPropiedad.Visible = False
                TrNombre.Visible = False
                TrApodo.Visible = False

                cmbEstaFil.Visible = False
                cmbNaciFil.Visible = False
                txtCtrolFactuNumeFil.Visible = False
                cmbCtroFil.Visible = False
                txtDonaFil.Visible = False
                txtCuigFil.Visible = False
                txtTramNumeFil.Visible = False
                txtLaboNumeFil.Visible = False
                cmbAsocFil.Visible = False
                txtAsoNumeFil.Visible = False
                chkBaja.Visible = False
                chkBusc.Visible = False
                txtApodoFil.Visible = False
                txtNombFil.Visible = False
                txtRPNumeFil.Visible = False
                lblRPNumeFil.Visible = False
                lblEstaFil.Visible = False
                lblNaciFil.Visible = False
                lblCtrolFactuNumeFil.Visible = False
                lblCtroFil.Visible = False
                lblDonaFil.Visible = False
                lblCuigFil.Visible = False
                lblTramNumeFil.Visible = False
                lblLaboNumeFil.Visible = False
                lblAsocNumeFil.Visible = False
                lblAsoNumeFil.Visible = False
                lblApodoFil.Visible = False
                lblNombFil.Visible = False



            End If

            If Not Page.IsPostBack Then
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "productos_estados", cmbEstaFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "asociaciones", cmbAsocFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazaFil, "S")
    End Sub
    Private Sub mInicializar()

        usrPropCriaFil.Tabla = mstrCriadores
        usrPropCriaFil.AutoPostback = False
        usrPropCriaFil.FilClaveUnica = False
        usrPropCriaFil.ColClaveUnica = False
        usrPropCriaFil.Ancho = 790
        usrPropCriaFil.Alto = 510
        usrPropCriaFil.ColDocuNume = True
        usrPropCriaFil.ColCUIT = True
        usrPropCriaFil.ColClaveUnica = True
        usrPropCriaFil.FilClaveUnica = True
        usrPropCriaFil.FilCUIT = True
        usrPropCriaFil.FilDocuNume = True
        usrPropCriaFil.FilTarjNume = False
        usrPropCriaFil.Criador = True
        usrPropCriaFil.FilTipo = "T"

        usrPropClieFil.Tabla = mstrClientes
        usrPropClieFil.AutoPostback = False
        usrPropClieFil.FilClaveUnica = False
        usrPropClieFil.ColClaveUnica = False
        usrPropClieFil.Ancho = 790
        usrPropClieFil.Alto = 510
        usrPropClieFil.ColDocuNume = True
        usrPropClieFil.ColCUIT = True
        usrPropClieFil.ColClaveUnica = True
        usrPropClieFil.FilClaveUnica = True
        usrPropClieFil.FilCUIT = True
        usrPropClieFil.FilDocuNume = True
        usrPropClieFil.FilTarjNume = False
        usrPropClieFil.Criador = False
        usrPropClieFil.FilTipo = "T"
        '   usrPropClieFil.Inscrip = mintInscrip

        usrMadreFil.Tabla = "productos"
        usrMadreFil.AutoPostback = False
        usrMadreFil.Ancho = 790
        usrMadreFil.Alto = 510
        usrMadreFil.FilSexo = False
        usrMadreFil.Sexo = 0
        'usrMadreFil.Inscrip = mintInscrip

        usrPadreFil.Tabla = "productos"
        usrPadreFil.AutoPostback = False
        usrPadreFil.Ancho = 790
        usrPadreFil.Alto = 510
        usrPadreFil.FilSexo = False
        usrPadreFil.Sexo = 1
    End Sub

    Public Function ObtenerIdFiltro() As String
        Dim pstrsexo As String
        Dim lstrId As String
        Dim lstrCmd As New StringBuilder
        Dim lstrPropClie As String
        Dim lstrPropCria As String
        ' GSZ 12/12/2014 Se incorporaron datos del Padre y Madre a los Filtros

        Try
            If optClie.Checked Then
                lstrPropClie = usrPropClieFil.Valor.ToString
            Else
                lstrPropClie = ""
            End If
            If optCria.Checked Then
                lstrPropCria = usrPropCriaFil.Valor.ToString
            Else
                lstrPropCria = ""
            End If

            lstrCmd.Append("exec rpt_productos_filtros_alta")
            '  lstrCmd.Append(" @rprf_raza_id=" & clsSQLServer.gFormatArg(cmbRazaFil.Valor.ToString, SqlDbType.Int))
            'GSZ 11/12/2014  se modifico

            lstrCmd.Append(" @rprf_raza_id=" & clsSQLServer.gFormatArg(usrCriaFil.RazaId.ToString(), SqlDbType.Int))

            lstrCmd.Append(" , @rprf_sra_nume=" & clsSQLServer.gFormatArg(txtNumeFil.Text, SqlDbType.VarChar))

                If cmbSexoFil.Valor.Length = 0 Then
                    lstrCmd.Append("  ")
                Else
                    pstrsexo = cmbSexoFil.Valor.ToString
                    lstrCmd.Append(" , @rprf_sexo=" + clsSQLServer.gFormatArg(pstrsexo, SqlDbType.Int))
                End If

            lstrCmd.Append(" , @rprf_rp=" & clsSQLServer.gFormatArg(txtRPNumeFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_sra_nume_desde=" & clsSQLServer.gFormatArg(txtNumeDesdeFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_sra_nume_hasta=" & clsSQLServer.gFormatArg(txtNumeHastaFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_rp_desde=" & clsSQLServer.gFormatArg(txtRPDesdeFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_rp_hasta=" & clsSQLServer.gFormatArg(txtRPHastaFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_naci_fecha_desde=" & clsSQLServer.gFormatArg(txtNaciFechaDesdeFil.Text, SqlDbType.SmallDateTime))
            lstrCmd.Append(" , @rprf_naci_fecha_hasta=" & clsSQLServer.gFormatArg(txtNaciFechaHastaFil.Text, SqlDbType.SmallDateTime))
            lstrCmd.Append(" , @rprf_insc_fecha_desde=" & clsSQLServer.gFormatArg(txtInscFechaDesdeFil.Text, SqlDbType.SmallDateTime))
            lstrCmd.Append(" , @rprf_insc_fecha_hasta=" & clsSQLServer.gFormatArg(txtInscFechaHastaFil.Text, SqlDbType.SmallDateTime))
            lstrCmd.Append(" , @rprf_cria_id=" & clsSQLServer.gFormatArg(usrCriaFil.Valor.ToString, SqlDbType.Int))
            lstrCmd.Append(" , @rprf_opcl=" & clsSQLServer.gFormatArg(IIf(optClie.Checked, 1, IIf(optCria.Checked, 1, 0)), SqlDbType.Bit))
            lstrCmd.Append(" , @rprf_clie_prop_id=" & clsSQLServer.gFormatArg(lstrPropClie, SqlDbType.Int))
            lstrCmd.Append(" , @rprf_cria_prop_id=" & clsSQLServer.gFormatArg(lstrPropCria, SqlDbType.Int))
            lstrCmd.Append(" , @rprf_nomb=" & clsSQLServer.gFormatArg(txtNombFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_apodo=" & clsSQLServer.gFormatArg(txtApodoFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_buscar_en=" & clsSQLServer.gFormatArg(IIf(chkBusc.Checked, 1, 0), SqlDbType.Bit))
            'lstrCmd.Append(" , @rprf_incluir_bajas=" & clsSQLServer.gFormatArg(IIf(chkBaja.Checked, 1, 0), SqlDbType.Bit))
            lstrCmd.Append(" , @rprf_incluir_bajas=" & clsSQLServer.gFormatArg(1, SqlDbType.Bit))
            'GSZ 16/12/2014  se puso mostrar siempre los que estan de baja
            lstrCmd.Append(" , @rprf_asoc_id=" & clsSQLServer.gFormatArg(cmbAsocFil.Valor.ToString, SqlDbType.Int))
            lstrCmd.Append(" , @rprf_asoc_nume=" & clsSQLServer.gFormatArg(txtAsoNumeFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_ntra_lab=" & clsSQLServer.gFormatArg(txtLaboNumeFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_tram_nume=" & clsSQLServer.gFormatArg(txtTramNumeFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_cuig=" & clsSQLServer.gFormatArg(txtCuigFil.Text, SqlDbType.VarChar))
            lstrCmd.Append(" , @rprf_esta_id=" & clsSQLServer.gFormatArg(cmbEstaFil.Valor.ToString, SqlDbType.Int))
            If cmbNaciFil.Valor <> "T" Then
                lstrCmd.Append(" , @rprf_ndad=" & clsSQLServer.gFormatArg(cmbNaciFil.Valor.ToString, SqlDbType.VarChar))
            End If
            lstrCmd.Append(" , @rprf_observacion=" & clsSQLServer.gFormatArg(txtObservacionFil.Text, SqlDbType.VarChar))


            Dim oProductos As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim dtProductoPadre As DataTable
            Dim dtProductoMadre As DataTable
            Dim dtProductoPadreRP As DataTable
            Dim dtProductoMadreRP As DataTable





            Dim PadreId As Int32
            Dim MadreId As Int32

            If txtHBAPadre.Text <> "" Or txtRPPadre.Text <> "" Then
                dtProductoPadre = oProductos.GetProductoByHBARazaSexoRP(txtHBAPadre.Text, usrCriaFil.RazaId.ToString(), "1", txtRPPadre.Text, "")

            End If
            If txtHBAMadre.Text <> "" Or txtRPMadre.Text <> "" Then
                dtProductoMadre = oProductos.GetProductoByHBARazaSexoRP(txtHBAMadre.Text, usrCriaFil.RazaId.ToString(), "0", txtRPMadre.Text, "")
            End If


            If Not dtProductoPadre Is Nothing Then
                If dtProductoPadre.Rows.Count > 0 Then
                    PadreId = ValidarNulos(dtProductoPadre.Rows(0).Item("prdt_id"), False)

                    If dtProductoPadre.Rows.Count > 1 Then
                        Throw New AccesoBD.clsErrNeg("Debe de informar  el HBA y RP de la Padre")
                    End If
                End If
            End If


            If Not dtProductoMadre Is Nothing Then
                If dtProductoMadre.Rows.Count > 0 Then
                    MadreId = ValidarNulos(dtProductoMadre.Rows(0).Item("prdt_id"), False)

                    If dtProductoMadre.Rows.Count > 1 Then
                        Throw New AccesoBD.clsErrNeg("Debe de informar tambien el HBA y RP de la Madre")

                    End If
                End If
            End If


            'lstrCmd.Append(" , @rprf_madre_prdt_id=" & clsSQLServer.gFormatArg(usrMadreFil.Valor.ToString, SqlDbType.Int))
            'lstrCmd.Append(" , @rprf_padre_prdt_id=" & clsSQLServer.gFormatArg(usrPadreFil.Valor.ToString, SqlDbType.Int))

            lstrCmd.Append(" , @rprf_madre_prdt_id=" & clsSQLServer.gFormatArg(MadreId, SqlDbType.Int))
            lstrCmd.Append(" , @rprf_padre_prdt_id=" & clsSQLServer.gFormatArg(PadreId, SqlDbType.Int))



            lstrId = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd.ToString)
            Return (lstrId)
        Catch ex As Exception
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function ObtenerParametros(ByRef productosFiltrosEntity) As Boolean
        ' GSZ 04/12/2014 Se agrego para pasar los parametros a la pantala con usrFiltroProd
        ' GSZ 12/12/2014 Se incorporaron datos del Padre y Madre a los Filtros
        Dim pstrsexo As String
        Dim lstrId As String
        Dim lstrCmd As New StringBuilder
        Dim lstrPropClie As String
        Dim lstrPropCria As String


            Try

                ' GSZ 11/12/2014 Se agrego para pasar los parametros a la pantala con usrFiltroProd

                Dim oProductos As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim dtProductoPadre As DataTable
                Dim dtProductoMadre As DataTable
                Dim PadreId As Int32
                Dim MadreId As Int32



                If optClie.Checked Then
                    lstrPropClie = usrPropClieFil.Valor.ToString + "-" + usrPropClieFil.txtApelExt.Text
                    If lstrPropClie = "0-" Then
                        lstrPropClie = ""
                    End If
                Else
                    lstrPropClie = ""
                End If
                If optCria.Checked Then
                    lstrPropCria = usrPropCriaFil.Valor.ToString + "-" + usrPropCriaFil.txtApelExt.Text
                    If lstrPropCria = "0-" Then
                        lstrPropCria = ""
                    End If
                Else
                    lstrPropCria = ""
                End If

                productosFiltrosEntity.rprf_raza_id = usrCriaFil.RazaId.ToString()  'cmbRazaFil.Valor.ToString
                productosFiltrosEntity.rprf_sra_nume = txtNumeFil.Text
                'If Not cmbSexoFil.Valor Is DBNull.Value Then
                '    productosFiltrosEntity.rprf_sexo = cmbSexoFil.Valor
                'End If

                'If Not IsDBNull(cmbSexoFil.Valor) And IsNumeric(cmbSexoFil.Valor) Then
                '    productosFiltrosEntity.rprf_sexo = Convert.ToInt32(cmbSexoFil.Valor)
                'End If

                'GM 17/09/2022
                If cmbSexoFil.Valor.ToString().Trim.Length > 0 Then
                    productosFiltrosEntity.rprf_sexo = cmbSexoFil.Valor.ToString()
                End If

                productosFiltrosEntity.rprf_rp = txtRPNumeFil.Text
                productosFiltrosEntity.rprf_sra_nume_desde = txtNumeDesdeFil.Text
                productosFiltrosEntity.rprf_sra_nume_hasta = txtNumeHastaFil.Text
                productosFiltrosEntity.rprf_rp_desde = txtRPDesdeFil.Text
                productosFiltrosEntity.rprf_rp_hasta = txtRPHastaFil.Text

                If txtNaciFechaDesdeFil.Text.Trim().Length > 0 Then
                    productosFiltrosEntity.rprf_naci_fecha_desde = txtNaciFechaDesdeFil.Fecha
                End If
                ' Dario 2022-10-24 1546
                If txtNaciFechaHastaFil.Text.Trim().Length > 0 Then
                    productosFiltrosEntity.rprf_naci_fecha_hasta = txtNaciFechaHastaFil.Fecha
                End If

                If txtInscFechaDesdeFil.Text.Trim().Length > 0 Then
                    productosFiltrosEntity.rprf_insc_fecha_desde = txtInscFechaDesdeFil.Fecha
                End If

                If txtInscFechaHastaFil.Text.Trim().Length > 0 Then
                    productosFiltrosEntity.rprf_insc_fecha_hasta = txtInscFechaHastaFil.Fecha
                End If

                productosFiltrosEntity.rprf_cria_id = usrCriaFil.Valor.ToString
                productosFiltrosEntity.rprf_opcl = IIf(optClie.Checked, 1, IIf(optCria.Checked, 1, 0))
                productosFiltrosEntity.rprf_clie_prop = lstrPropClie
                productosFiltrosEntity.rprf_cria_prop = lstrPropCria
                productosFiltrosEntity.rprf_nomb = txtNombFil.Text
                productosFiltrosEntity.rprf_apodo = txtApodoFil.Text
                productosFiltrosEntity.rprf_buscar_en = IIf(chkBusc.Checked, 1, 0)

                If txtHBAPadre.Text <> "" Then
                    dtProductoPadre = oProductos.GetProductoByHBARazaSexoRP(txtHBAPadre.Text, usrCriaFil.RazaId.ToString(), "1", txtRPPadre.Text, "")
                End If


                If txtHBAMadre.Text <> "" Then
                    dtProductoMadre = oProductos.GetProductoByHBARazaSexoRP(txtHBAMadre.Text, usrCriaFil.RazaId.ToString(), "0", txtRPMadre.Text, "")
                End If

                If Not dtProductoPadre Is Nothing Then
                    If dtProductoPadre.Rows.Count > 0 Then
                        PadreId = ValidarNulos(dtProductoPadre.Rows(0).Item("prdt_id"), False)
                    End If
                End If

                If Not dtProductoMadre Is Nothing Then
                    If dtProductoMadre.Rows.Count > 0 Then
                        MadreId = ValidarNulos(dtProductoMadre.Rows(0).Item("prdt_id"), False)
                    End If
                End If

                productosFiltrosEntity.rprf_madre_prdt_id = MadreId
                productosFiltrosEntity.rprf_padre_prdt_id = PadreId


                productosFiltrosEntity.HBAPadreNume = txtHBAPadre.Text
                productosFiltrosEntity.HBAMadreNume = txtHBAMadre.Text
                productosFiltrosEntity.RPPadreNume = txtRPPadre.Text
                productosFiltrosEntity.RPMadreNume = txtRPMadre.Text




                '    productosFiltrosEntity.
                'productosFiltrosEntity.rprf_padre_prdt_id = PadreId
                'productosFiltrosEntity.rprf_madre_prdt_id = MadreId
                'productosFiltrosEntity.rprf_padre_prdt_id = PadreId


                '.rprf_incluir_bajas = IIf(chkBaja.Checked, 1, 0)
                '.rprf_asoc_id = cmbAsocFil.Valor.ToString
                '.rprf_asoc_nume = txtAsoNumeFil.Text
                '.rprf_ntra_lab = txtLaboNumeFil.Text
                '.rprf_tram_nume = txtTramNumeFil.Text
                '.rprf_cuig = txtCuigFil.Text
                '.rprf_esta_id = cmbEstaFil.Valor.ToString
                'If cmbNaciFil.Valor <> "T" Then
                '    oParametrosFiltro.rprf_ndad_id = cmbNaciFil.Valor.ToString
                'End If
                'oParametrosFiltro.rprf_obse = txtObservacionFil.Text



                Return True
            Catch ex As Exception
                clsError.gManejarError(ex)
            End Try

        End Function


    Public Function ObtenerRazaIdFiltro() As String
        ' GSZ 04/12/2014 Se agrego para pasar RazaId a la pantala con usrFiltroProd
        Dim RazaId As String

        Try
            RazaId = "0"
            If (usrCriaFil.RazaId.ToString() <> "") Then
                RazaId = usrCriaFil.RazaId.ToString()

            End If
            Return (RazaId)
        Catch ex As Exception
            clsError.gManejarError(ex)
        End Try
    End Function


    Public Function ObtenerCriadorFiltro() As String
        ' GSZ 04/12/2014 Se agrego para pasar Criador pantala con usrFiltroProd
        Dim strCria As String

        Try
            strCria = ""
            If usrPropCriaFil.Valor.ToString() <> "" Then
                strCria = usrCriaFil.Codi + "-" + usrCriaFil.Apel
            End If
            Return (strCria)
        Catch ex As Exception
            clsError.gManejarError(ex)
        End Try
    End Function

    Public Function ObtenerCriadorIdFiltro() As String
        Dim CriadorId As String

        Try
            CriadorId = ""
            If usrPropCriaFil.Valor.ToString() <> "" Then
                CriadorId = usrCriaFil.Valor
            End If
            Return (CriadorId)
        Catch ex As Exception
            clsError.gManejarError(ex)
        End Try
    End Function


    Public Sub Limpiar()
        cmbSexoFil.Limpiar()
        txtRPNumeFil.Text = ""
        txtRPDesdeFil.Text = ""
        txtRPHastaFil.Text = ""
        txtNumeDesdeFil.Text = ""
        txtNumeHastaFil.Text = ""
        txtNaciFechaDesdeFil.Text = ""
        txtNaciFechaHastaFil.Text = ""
        txtInscFechaDesdeFil.Text = ""
        txtInscFechaHastaFil.Text = ""
        usrCriaFil.Limpiar()
        usrPropClieFil.Limpiar()
        usrPropCriaFil.Limpiar()
        txtNombFil.Text = ""
        txtApodoFil.Text = ""
        chkBusc.Checked = False
        chkBaja.Checked = False
        cmbAsocFil.Limpiar()
        txtLaboNumeFil.Text = ""
        txtTramNumeFil.Text = ""
        txtCuigFil.Text = ""
        cmbEstaFil.Limpiar()
        cmbNaciFil.Limpiar()
        txtAsoNumeFil.Text = ""
        txtHBAMadre.Text = ""
        txtHBAPadre.Text = ""
        txtRPMadre.Text = ""
        txtRPPadre.Text = ""


    End Sub

    Private Sub optClie_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optClie.CheckedChanged
        If optClie.Checked Then
            usrPropClieFil.Visible = True
            usrPropCriaFil.Visible = False
        End If
    End Sub

    Private Sub optCria_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optCria.CheckedChanged
        If optCria.Checked Then
            usrPropCriaFil.Visible = True
            usrPropClieFil.Visible = False
        End If
    End Sub
End Class

End Namespace
