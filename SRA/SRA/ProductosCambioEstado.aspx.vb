Namespace SRA

Partial Class ProductosCambioEstado
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents usrCriadorFil As usrClieDeriv


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
	Private mstrTabla As String = "movim_productos"
	Private mstrCmd As String
	Private mstrConn As String
#End Region


#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()
			If (Not Page.IsPostBack) Then
				Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
				mSetearMaxLength()
				mSetearEventos()
				mCargarCombos()
				'mConsultar()
				mMostrarPanel(False)
				clsWeb.gInicializarControles(Me, mstrConn)
            Else

                'usrProductoFil.cmbProdRazaExt.Valor = usrProductoFil.cmbProdRazaExt.Valor
                'usrProductoFil.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")

                'usrProducto.cmbProdRazaExt.Valor = cmbRazaFil.Valor
                'usrProducto.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")
                'txtFecha.Text = CDate(Now()).ToString("dd/MM/yyyy")

            End If
        Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mSetearEventos()
		btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
	End Sub

	Private Sub mSetearMaxLength()
		Dim lstrLongs As Object

		lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

		txtObservaciones.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "mopr_obse")
	End Sub
	Public Sub mInicializar()
        'usrCriadorFil.Criador = True
        'usrCriadorFil.AutoPostback = False
        'usrCriadorFil.FilClaveUnica = False
        'usrCriadorFil.ColClaveUnica = True
        'usrCriadorFil.Ancho = 790
        'usrCriadorFil.Alto = 510
        'usrCriadorFil.ColDocuNume = False
        'usrCriadorFil.ColCUIT = True
        'usrCriadorFil.ColCriaNume = True
        'usrCriadorFil.FilCUIT = True
        'usrCriadorFil.FilDocuNume = True
        'usrCriadorFil.FilAgru = False
        'usrCriadorFil.FilTarjNume = False

        usrProductoFil.EsPropietario = False
        usrProductoFil.AutoPostback = False
		usrProductoFil.Ancho = 790
		usrProductoFil.Alto = 510



		usrProducto.AutoPostback = False
		usrProducto.IncluirDeshabilitados = True
		usrProducto.Ancho = 790
		usrProducto.Alto = 510
		If Not usrProducto.Valor Is DBNull.Value Then
			mCargarDatosSocio(usrProducto.Valor)
		End If
	End Sub
	Private Sub mCargarCombos()
		'clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_cali=1", cmbRazaFil, "id", "descrip_codi", "T")
		clsWeb.gCargarCombo(mstrConn, "movim_productos_estados_cargar", cmbEsta, "id", "descrip", "S")
		clsWeb.gCargarCombo(mstrConn, "baja_motivos_cargar", cmbBamo, "id", "descrip", "S")
		'SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
	End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
	Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdDato.EditItemIndex = -1
			If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
				grdDato.CurrentPageIndex = 0
			Else
				grdDato.CurrentPageIndex = E.NewPageIndex
			End If
			mConsultar()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Public Sub mConsultar()
		Try
			mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + "@prdt_cria_id=" + IIf(usrProductoFil.CriaOrPropId.ToString() = "", "0", usrProductoFil.CriaOrPropId.ToString())
            If usrProductoFil.RazaId Is System.DBNull.Value And usrProductoFil.RazaId.ToString() <> "" Then
                mstrCmd = mstrCmd + ",@prdt_raza_id=" + usrProductoFil.RazaId.ToString()
                'ElseIf Not usrProductoFil.cmbProdRazaExt.Valor Is System.DBNull.Value And usrProductoFil.RazaId.ToString() = "" Then
                '    mstrCmd = mstrCmd + ",@prdt_raza_id=" + usrProductoFil.cmbProdRazaExt.Valor
            Else
                mstrCmd = mstrCmd + ",@prdt_raza_id=0"
            End If
            mstrCmd = mstrCmd + ",@mopr_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)

        Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

#Region "Seteo de Controles"
	Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
		btnBaja.Enabled = Not (pbooAlta)
		btnModi.Enabled = Not (pbooAlta)
		btnAlta.Enabled = pbooAlta
	End Sub
	Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
		Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, E.Item.Cells(1).Text)
		ldsEsta.Tables(0).TableName = mstrTabla
		mSetearEditor(False)
		With ldsEsta.Tables(0).Rows(0)

			mCargarDatosSocio(.Item("mopr_id"))

			hdnId.Text = .Item("mopr_id")
			usrProducto.Valor = .Item("mopr_prdt_id")
			usrProducto.Raza = .Item("_raza_id")
			txtFecha.Fecha = CDate(.Item("mopr_movi_fecha")).ToString("dd/MM/yyyy")
			cmbEsta.Valor = .Item("_esta_desc")
			If Not .Item("mopr_bamo_fecha") Is DBNull.Value Then
				txtFechaBaja.Fecha = CDate(.Item("mopr_bamo_fecha")).ToString("dd/MM/yyyy")
			End If
                cmbBamo.Valor = .Item("_bamo_desc").ToString
                txtObservaciones.Valor = .Item("mopr_obse").ToString
			If Not .IsNull("mopr_baja_fecha") Then
				lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("mopr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
				btnBaja.Enabled = False
			Else
				lblBaja.Text = ""
			End If

		End With


		mMostrarPanel(True)
	End Sub

	Private Sub mAgregar()
		mLimpiar()
		usrProducto.Valor = usrProductoFil.Valor
		If Not usrProducto.Valor Is DBNull.Value Then
			mCargarDatosSocio(usrProducto.Valor)
		End If
		txtFecha.Fecha = CDate(Now())
		btnBaja.Enabled = False
		btnModi.Enabled = False
		btnAlta.Enabled = True
		mMostrarPanel(True)
	End Sub
	Private Sub mLimpiar()
		hdnId.Text = ""
		usrProducto.Limpiar()
		'usrProducto.cmbProdRazaExt.Valor = cmbRazaFil.Valor
		'usrProducto.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")
		txtFechaBaja.Text = ""
		lblEstadoAnterior.Text = ""
		lblFechaAnterior.Text = ""
		lblbajaAnterior.Text = ""
		lblFechaBaja.Text = ""
		cmbEsta.Limpiar()
		cmbBamo.Limpiar()

		txtFecha.Text = ""
		txtObservaciones.Text = ""
		lblBaja.Text = ""

		grdDato.CurrentPageIndex = 0
		mSetearEditor(True)
	End Sub
	Private Sub mLimpiarFiltro()
        'usrCriadorFil.Limpiar()
		'cmbRazaFil.Limpiar()
		usrProductoFil.Limpiar()
		'usrProductoFil.cmbProdRazaExt.Valor = cmbRazaFil.Valor
		'usrProductoFil.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")
		grdDato.CurrentPageIndex = 0
		mConsultar()
	End Sub
	Private Sub mCerrar()
		mMostrarPanel(False)
	End Sub
	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		If (pbooVisi) Then
			hdnPage.Text = " "
		Else
			hdnPage.Text = ""
		End If
		panDato.Visible = pbooVisi
		panBotones.Visible = pbooVisi
		btnAgre.Enabled = Not (panDato.Visible)

	End Sub
#End Region

#Region "Opciones de ABM"
	Private Sub mAlta()
		Try
			Dim ldsDatos As DataSet = mGuardarDatos()

			Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
			lobjGenerica.Alta()

			mConsultar()
			mMostrarPanel(False)
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mModi()
		Try
			Dim ldsDatos As DataSet = mGuardarDatos()

			Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
			lobjGenerica.Modi()

			mConsultar()
			mMostrarPanel(False)
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mBaja()
		Try
			Dim lintPage As Integer = grdDato.CurrentPageIndex

			Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
			lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

			grdDato.CurrentPageIndex = 0

			mConsultar()

			If (lintPage < grdDato.PageCount) Then
				grdDato.CurrentPageIndex = lintPage
			End If

			mMostrarPanel(False)
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mValidarDatos()

		clsWeb.gInicializarControles(Me, mstrConn)
		clsWeb.gValidarControles(Me)
	End Sub

	

	Private Function mGuardarDatos() As DataSet
		Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
            Dim ldsEsta As DataSet

		mValidarDatos()

            If cmbBamo.Valor.ToString.Length = 0 Then
                mstrCmd = "exec estado_baja_consul " & cmbEsta.Valor
            Else
                mstrCmd = "exec estado_baja_consul " & cmbEsta.Valor & ", " & cmbBamo.Valor
            End If


		ldsEsta = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

		With ldsDatos.Tables(0).Rows(0)

			If ldsEsta.Tables(0).Rows(0).Item("esta_baja") And cmbBamo.Valor Is DBNull.Value Then
				'Indicamos el mensaje
				Throw New AccesoBD.clsErrNeg("Debe selecionar un Motivo de Baja")
			Else
                If ldsEsta.Tables(0).Rows(0).Item("esta_baja") And txtFechaBaja.Text = "" Then
                    'Indicamos el mensaje
                    Throw New AccesoBD.clsErrNeg("Debe indicar fecha para el Motivo de Baja")
                End If
			End If

			.Item("mopr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
			.Item("mopr_prdt_id") = usrProducto.ProdId
			.Item("mopr_movi_fecha") = txtFecha.Fecha
                .Item("mopr_esta_id") = cmbEsta.Valor
                If cmbBamo.Valor.ToString.Length > 0 Then
                    .Item("mopr_bamo_fecha") = txtFechaBaja.Fecha
                    .Item("mopr_bamo_id") = cmbBamo.Valor
                End If
                .Item("mopr_obse") = txtObservaciones.Valor
            End With
		Return ldsDatos
	End Function
#End Region

#Region "Eventos de Controles"
	Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
		mCerrar()
	End Sub
	Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
		mAgregar()
	End Sub
	Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
		grdDato.CurrentPageIndex = 0
		mConsultar()
	End Sub
	Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltro()
	End Sub
	Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
		mAlta()
	End Sub
	Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
		mBaja()
	End Sub
	Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
		mModi()
	End Sub
	Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
		mLimpiar()
	End Sub
#End Region

	Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
		Try
			Dim lstrRptName As String

			lstrRptName = "ProductoCambioEstado"

			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If usrProductoFil.cmbProdRazaExt.Valor Is System.DBNull.Value And usrProductoFil.RazaId.ToString <> "" Then
                lstrRpt += "&prdt_raza_id=" + usrProductoFil.RazaId.ToString()
            ElseIf Not usrProductoFil.cmbProdRazaExt.Valor Is System.DBNull.Value And usrProductoFil.RazaId.ToString = "" Then
                lstrRpt += "&prdt_raza_id=" + usrProductoFil.cmbProdRazaExt.Valor
            Else
                lstrRpt += "&prdt_raza_id=0"
            End If
            lstrRpt += "&mopr_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mCargarDatosSocio(ByVal pintPrdt As Integer)
		Dim ldsDatos As DataSet
		Try
			mstrCmd = "exec productos_movim_esta_consul " & pintPrdt
			ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
			If ldsDatos.Tables(0).Rows.Count <> 0 Then
				With ldsDatos.Tables(0).Rows(0)
					lblEstadoAnterior.Text = .Item("estado")
					lblFechaAnterior.Text = CDate(.Item("esta_fecha")).ToString("dd/MM/yyyy")
					lblbajaAnterior.Text = .Item("bamo_desc")
					If Not .Item("mopr_bamo_fecha") Is DBNull.Value Then
						lblFechaBaja.Text = CDate(.Item("mopr_bamo_fecha")).ToString("dd/MM/yyyy")
					End If
				End With
			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

End Class
End Namespace
