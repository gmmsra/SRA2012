<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CRIA" Src="controles/usrCriadero.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AnillosRecep" CodeFile="AnillosRecep.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Recepci�n de Anillos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
			function mMostrarPanel()
			{
				if (document.all('txtCantiRecep').value != document.all('hdnCantiPedi').value)
				{
					document.all('panNumeros').style.display = '';
					document.all('txtNumeHasta').focus();
				}
				if (document.all('txtCantiRecep').value != document.all('hdnCanti').value)
				{
					document.all('panNumeros').style.display = '';
					document.all('txtNumeHasta').focus();
				}
				if (document.all('txtCantiRecep').value == "")
				{
					document.all('panNumeros').style.display = 'none';
				}
			}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Recepci�n de Anillos</asp:label></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="17" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<asp:checkbox id="chkTodo" Text="Incluir Recibidos" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<tr>
										<TD height="10" colSpan="3"></TD>
									</tr>
									<TR>
										<TD vAlign="top" align="center" colSpan="3">
											<asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
												OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="15px"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="false" DataField="amca_id" ReadOnly="True" HeaderText="Id"></asp:BoundColumn>
													<asp:BoundColumn DataField="amca_soli_fecha" HeaderText="Fecha Encargo" HeaderStyle-HorizontalAlign="Left"
														ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
													<asp:BoundColumn DataField="amca_nume" HeaderText="N� Encargo" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Right"
														HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
													<asp:BoundColumn DataField="amca_desp_nume" HeaderText="N� Despacho" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Right"
														HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
													<asp:BoundColumn DataField="_canti" HeaderText="Cant. Anillos" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado" HeaderStyle-Width="15%"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<tr>
										<TD height="10" colSpan="3"></TD>
									</tr>
									<TR>
										<TD align="right" width="100%"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
											ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
											BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
										<TD align="center" colSpan="2">
											<asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" Height="124%" width="100%">
												<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> Encargo</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkDetalle" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Detalle</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
													</TR>
												</TABLE>
											</asp:panel>
										</TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												Visible="False" Height="116px" width="100%">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
																ToolTip="Cerrar"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label id="lblFechaRecep" runat="server" cssclass="titulo">Fecha de Recepci�n:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%" vAlign="middle">
																			<cc1:DateBox id="txtFechaRecep" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label id="lblNumeroDespacho" runat="server" cssclass="titulo">Numero Despacho:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%" vAlign="middle">
																			<cc1:numberbox id="txtNumeroDespacho" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="false"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 60px" vAlign="top" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 60px" vAlign="middle">
																			<cc1:textboxtab id="txtObse" runat="server" cssclass="textolibre" Width="768px" TextMode="MultiLine"
																				height="53px"></cc1:textboxtab></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panDetalle" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD vAlign="top" align="center" colSpan="2">
																			<asp:datagrid id="grdDetalle" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosDeta" OnPageIndexChanged="DataGridDeta_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%" PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="amde_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_anillo" ReadOnly="True" HeaderText="Anillo" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																					<asp:BoundColumn DataField="amde_cant_pedi" ReadOnly="True" HeaderText="Cant. Encargada" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																					<asp:BoundColumn DataField="amde_cant" ReadOnly="True" HeaderText="Cant. Recibida" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																					<asp:BoundColumn DataField="amde_desde_nume" ReadOnly="True" HeaderText="N� Desde" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																					<asp:BoundColumn DataField="amde_hasta_nume" ReadOnly="True" HeaderText="N� Hasta" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" height="2">
																			<asp:Label id="lblAnilloDeta" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 20px" vAlign="top" align="right">
																			<asp:Label id="lblCantiRecep" runat="server" cssclass="titulo">Cantidad Recibida:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 20px" vAlign="middle">
																			<cc1:numberbox id="txtCantiRecep" runat="server" cssclass="cuadrotexto" Width="100px" OnChange="mMostrarPanel()"></cc1:numberbox>&nbsp;<asp:Label id="lblPedidos" runat="server" cssclass="titulo">Pedidos:</asp:Label><asp:Label id="lblPediCant" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>																	
																	<tr>
																		<td colspan="2" Width="100%">
																		<div id="panNumeros" style="DISPLAY: none" align="center" class="titulo">
																			<table style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																				<TR>
																					<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 25%; HEIGHT: 20px" vAlign="top" align="right">
																						<asp:Label id="lblNumeDesde" runat="server" cssclass="titulo">N� Desde:</asp:Label>&nbsp;</TD>
																					<TD style="WIDTH: 75%; HEIGHT: 20px" vAlign="middle">
																						<cc1:numberbox id="txtNumeDesde" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></TD>
																				</TR>
																				<TR>
																					<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 25%; HEIGHT: 20px" vAlign="top" align="right">
																						<asp:Label id="lblNumeHasta" runat="server" cssclass="titulo">N� Hasta:</asp:Label>&nbsp;</TD>
																					<TD style="WIDTH: 75%; HEIGHT: 20px" vAlign="middle">
																						<cc1:numberbox id="txtNumeHasta" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:numberbox></TD>
																				</TR>
																			</table>
																		</div>																		
																		</td>
																	</tr>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="2">
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCanti" runat="server"></asp:textbox>
				<asp:textbox id="hdnCantiPedi" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
			if (document.all["editar"]!= null)
				document.location='#editar';
			if (document.all('txtCantiRecep').value != document.all('hdnCantiPedi').value && document.all('txtCantiRecep').value != "")
			{
				document.all('panNumeros').style.display = '';
				document.all('txtNumeHasta').focus();
			}
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
