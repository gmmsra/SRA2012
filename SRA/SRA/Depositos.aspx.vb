Namespace SRA

Partial Class Depositos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents hdnImprimio As System.Web.UI.WebControls.TextBox


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String = SRA_Neg.Constantes.gTab_Depositos
   Public mstrTablaDepoCheq As String = SRA_Neg.Constantes.gTab_Depositos_Cheques
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         clsWeb.gInicializarControles(Me, mstrConn)

         If Not Page.IsPostBack Then
            mCargarCombos()
            mSetearEventos()
            mSetearMaxLength()
            Session(mSess(mstrTabla)) = Nothing

            Dim mobj As SRA_Neg.Facturacion
            mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
            mobj.CentroEmisorNro(mstrConn)

            hdnCenEmId.Text = mobj.pCentroEmisorId
            cmbCentroEmisor.Valor = mobj.pCentroEmisorId
            If mobj.ObtenerValorCampo(mstrConn, "EMISORES_CTROS", "emct_central", hdnCenEmId.Text) Then
               cmbCentroEmisor.Visible = True
               lblCentroEmisor.Visible = True
            End If

            txtFechaDesdeFil.Fecha = Now
            txtFechaHastaFil.Fecha = Now
                cmbBancoNuevo.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_banc_id")
                clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaNueva, "S", cmbBancoNuevo.Valor & "," & cmbMoneda.Valor)
                cmbCuentaNueva.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_id")

            mConsultar()
         Else

				If panDato.Visible Then
					mdsDatos = Session(mSess(mstrTabla))
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub
   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      Dim lintCol As Integer

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtNumeroDeposito.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "depo_nume")

   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMoneda, "")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanco, "T", "1")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancoNuevo, "S", "1")
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
	End Sub

	Private Sub mCargarCuentas()
		cmbCuentaNueva.Limpiar()

		If Not cmbBancoNuevo.Valor Is DBNull.Value Then
			clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaNueva, "S", "@banc_id=" + cmbBancoNuevo.Valor + ", @mone_id=" + cmbMoneda.Valor)
		End If
	End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridCheques_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdCheques.EditItemIndex = -1
            If (grdCheques.CurrentPageIndex < 0 Or grdCheques.CurrentPageIndex >= grdCheques.PageCount) Then
                grdCheques.CurrentPageIndex = 0
            Else
                grdCheques.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarCheques()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd = mstrCmd + " @FechaDesde=" + clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            mstrCmd = mstrCmd + ",@FechaHasta=" + clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
            mstrCmd = mstrCmd + ",@Banco=" + clsSQLServer.gFormatArg(cmbBanco.Valor.ToString, SqlDbType.Int)
            mstrCmd = mstrCmd + ",@Cuenta=" + clsSQLServer.gFormatArg(cmbCuenta.Valor.ToString, SqlDbType.Int)
            mstrCmd = mstrCmd + ",@CenEmId=" + clsSQLServer.gFormatArg(cmbCentroEmisor.Valor.ToString, SqlDbType.Int)

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Function mIdCheques()
        Dim lstrIds As String = ""
        For Each oDataItem As DataRow In mdsDatos.Tables(mstrTablaDepoCheq).Select()
            If lstrIds.Length > 0 Then lstrIds += ","
            lstrIds += oDataItem.Item("_cheq_id").ToString
        Next
        Return lstrIds
    End Function
    Private Sub mConsultarCheques()
        txtTotalCheques.Valor = "0"
        For Each oDataItem As DataRow In mdsDatos.Tables(mstrTablaDepoCheq).Select()
            txtTotalCheques.Valor = txtTotalCheques.Valor + oDataItem.Item("_cheq_impo")
        Next

        grdCheques.DataSource = mdsDatos.Tables(mstrTablaDepoCheq)
        grdCheques.DataBind()
    End Sub
    Private Sub mConsultarChequesDisponibles()
        mstrCmd = IIf(hdnId.Text = "", "@formato='a'", "@DepoId=" & hdnId.Text)
        mstrCmd = mstrCmd + ",@Cheques='" + hdnValoresCheque.Text + "'"

        mCrearDataSet(mstrCmd)

        For Each ldrCheq As DataRow In mdsDatos.Tables(mstrTablaDepoCheq).Select()
            If InStr(ldrCheq.Item("_cheq_id").ToString, hdnChequesBorrados.Text) > 0 And hdnChequesBorrados.Text <> "" Then
                mdsDatos.Tables(mstrTablaDepoCheq).Rows.Remove(ldrCheq)
            Else
                If ldrCheq.Item("dech_id").ToString = "" Then
                    ldrCheq.Item("dech_id") = "0"
                    ldrCheq.Item("dech_cheq_id") = ldrCheq.Item("_cheq_id")
                    ldrCheq.Item("dech_clea") = ldrCheq.Item("_clea_id")
                    ldrCheq.Item("dech_audi_user") = Session("sUserId").ToString()
                End If
            End If
        Next

        mConsultarCheques()
    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDepoCheq

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdCheques.CurrentPageIndex = 0

        mConsultarCheques()

        Session(mSess(mstrTabla)) = mdsDatos

    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = False 'Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

            mCrearDataSet(hdnId.Text)

            With mdsDatos.Tables(mstrTabla).Rows(0)
                cmbBancoNuevo.Valor = .Item("_banc_id")
                cmbMoneda.Valor = .Item("depo_mone_id")
                cmbMoneda.Enabled = False
                hdnCenEmId.Text = .Item("depo_emct_id")
                clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaNueva, "S", .Item("_banc_id").ToString + "," + .Item("depo_mone_id").ToString)
                cmbCuentaNueva.Valor = .Item("depo_cuba_id")

                If .Item("_Cheque") = "NO" Then
                    'lblEfectivoMuestra.Text = .Item("depo_monto")
                    txtEfectivo.Valor = "0"
                    'btnConsulCheq.Enabled = False
                Else
                    txtEfectivo.Valor = "0"
                    'lblEfectivoMuestra.Text = ""
                    txtEfectivo.Enabled = False
                    hdnClearingId.Text = mdsDatos.Tables(mstrTablaDepoCheq).Select()(0).Item("_clea_id")
                    hdnBancId.Text = mdsDatos.Tables(mstrTablaDepoCheq).Select()(0).Item("_banc_id")
                End If

                If mdsDatos.Tables(mstrTablaDepoCheq).Rows.Count > 0 Then
                    txtCheques.Valor = .Item("depo_monto")
                Else
                    txtEfectivo.Valor = .Item("depo_monto")
                End If
                ' txtEfectivo.Valor = .Item("depo_monto")
                lblEfectivoMuestra.Text = "( " & CDec(clsSQLServer.gConsultarValor("depositos_cobranzas_efectivo_consul " & hdnCenEmId.Text & "," & cmbCuentaNueva.Valor.ToString, mstrConn, 0)).ToString("########0.00") & " )"
                'lblChequesMuestra.Text = "( " & CDec(clsSQLServer.gConsultarValor("depositos_cobranzas_efectivo_consul " & hdnCenEmId.Text & "," & cmbCuentaNueva.Valor.ToString, mstrConn, 0)).ToString("########0.00") & " )"
                ' lblEfectivoMuestra.Visible = False
                lblChequesMuestra.Visible = False
                btnConsulCheq.Enabled = False
                grdCheques.Columns(0).Visible = False

                txtNumeroDeposito.Valor = .Item("depo_nume")
                txtFechaNueva.Fecha = .Item("depo_fecha")
                If CDate(txtFechaNueva.Fecha).Year * 10 + CDate(txtFechaNueva.Fecha).DayOfYear + 7 < Now.Year * 10 + Now.DayOfYear Then
                    cmbCuentaNueva.Enabled = False
                    cmbBancoNuevo.Enabled = False
                    txtEfectivo.Enabled = False
                    btnConsulCheq.Enabled = False
                End If
                txtFechaNueva.Enabled = False
            End With

            hdnChequesOriginales.Text = mIdCheques()
            mSetearEditor(False)
            mMostrarPanel(True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        cmbBancoNuevo.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_banc_id")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaNueva, "S", cmbBancoNuevo.Valor & "," & cmbMoneda.Valor)
        cmbCuentaNueva.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_id")
        lblEfectivoMuestra.Text = CDec(clsSQLServer.gConsultarValor("depositos_cobranzas_efectivo_consul " & hdnCenEmId.Text & "," & cmbCuentaNueva.Valor.ToString, mstrConn, 0)).ToString("########0.00")
        lblEfectivoMuestra.Visible = True
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        'mCargarCuentas()
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnSess.Text = ""
        btnConsulCheq.Enabled = True
        grdCheques.Columns(0).Visible = True
        hdnDeposId.Text = ""
        txtNumeroDeposito.Text = ""
        txtFechaNueva.Fecha = Now
        txtFechaNueva.Enabled = True
        cmbBancoNuevo.Enabled = True
        'cmbBancoNuevo.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_banc_id")
        ' clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaNueva, "S", cmbBancoNuevo.Valor & "," & cmbMoneda.Valor)
        ' cmbCuentaNueva.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_id")
        cmbCuentaNueva.Enabled = True

        cmbMoneda.Limpiar()
        cmbMoneda.Enabled = True

        If cmbCuentaNueva.Valor.ToString = "" Then
            txtEfectivo.Valor = "0"
            txtCheques.Valor = "0"
        Else
            lblEfectivoMuestra.Text = CDec(clsSQLServer.gConsultarValor("depositos_cobranzas_efectivo_consul " & hdnCenEmId.Text & "," & cmbCuentaNueva.Valor.ToString, mstrConn, 0)).ToString("########0.00")
            'lblChequesMuestra.Text = CDec(clsSQLServer.gConsultarValor("depositos_cobranzas_efectivo_consul " & hdnCenEmId.Text & "," & cmbCuentaNueva.Valor.ToString, mstrConn, 0)).ToString("########0.00")
            txtEfectivo.Valor = "0"
            txtCheques.Valor = "0"
        End If
        'lblEfectivoMuestra.Text = CDec(clsSQLServer.gConsultarValor("depositos_cobranzas_efectivo_consul " & hdnCenEmId.Text & "," & cmbCuentaNueva.Valor.ToString, mstrConn, 0)).ToString("########0.00")
        txtEfectivo.Enabled = True
        hdnClearingId.Text = ""
        hdnBancId.Text = ""
        hdnValoresCheque.Text = ""
        hdnChequesOriginales.Text = ""
        hdnChequesBorrados.Text = ""

        grdCheques.CurrentPageIndex = 0
        mCrearDataSet("")
        mSetearEditor(True)
        mShowTabs(1)
    End Sub
    Private Sub mLimpiarFiltros()
        txtFechaDesdeFil.Fecha = Now
        txtFechaHastaFil.Fecha = Now
        cmbBanco.Limpiar()
        cmbCuenta.Items.Clear()
        cmbCentroEmisor.Valor = hdnCenEmId.Text
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            If (pbooVisi) Then
                hdnPage.Text = " "
            Else
                hdnPage.Text = String.Empty
            End If
            panDato.Visible = pbooVisi
            panSolapas.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            btnAgre.Enabled = Not (panDato.Visible)
            panFiltro.Visible = Not (panDato.Visible)
            grdDato.Visible = Not (panDato.Visible)
            tdNuevo.Visible = Not (panDato.Visible)
        End Sub
#End Region

#Region "Opciones de ABM"
        Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If hdnId.Text <> "" Then
            If CDate(txtFechaNueva.Fecha).Year * 10 + CDate(txtFechaNueva.Fecha).DayOfYear + 7 < Now.Year * 10 + Now.DayOfYear Then
                Throw New AccesoBD.clsErrNeg("No se puede modificar Dep�sitos con m�s de 7 d�as de antig�edad.")
            End If

            If txtEfectivo.Enabled = False And grdCheques.Items.Count = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe haber al menos un Cheque en el Dep�sito.")
            End If

            If txtEfectivo.Enabled = True And CInt(txtEfectivo.Valor).ToString = "0" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Importe de Efectivo para el Dep�sito.")
            End If
        Else
            If txtFechaNueva.Fecha < Today Then
                Throw New AccesoBD.clsErrNeg("La fecha del Dep�sito no puede ser menor a la del d�a.")
            End If

            If CInt(txtEfectivo.Valor).ToString = "0" And grdCheques.Items.Count = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Importe de Efectivo y/o Cheques para el Dep�sito.")
            End If
        End If

        If cmbMoneda.Valor.ToString <> clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", cmbCuentaNueva.Valor.ToString, "cuba_mone_id").ToString Then
            Throw New AccesoBD.clsErrNeg("La moneda de la cuenta no coincide con la del Dep�sito.")
        End If

    End Sub
        Private Sub mGuardarDatos()
            mValidaDatos()

            With mdsDatos.Tables(0).Rows(0)
                .Item("depo_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("depo_fecha") = txtFechaNueva.Fecha
                .Item("depo_cuba_id") = cmbCuentaNueva.SelectedValue
                If txtNumeroDeposito.Valor.ToString.Length > 0 Then
                    .Item("depo_nume") = txtNumeroDeposito.Valor
                End If
                .Item("depo_mone_id") = cmbMoneda.Valor
                .Item("depo_monto") = IIf(txtEfectivo.Text.ToString = "0" Or txtEfectivo.Text.Trim().Length = 0, DBNull.Value, Convert.ToDecimal(txtEfectivo.Valor))
                .Item("depo_emct_id") = hdnCenEmId.Text
                .Item("depo_audi_user") = Session("sUserId").ToString()
                .Item("_nuevo") = IIf(hdnId.Text <> "", False, True)
            End With
        End Sub
        Private Sub mAlta()
        Try
            Dim lstrIds As String
            Dim lstrResu As String = "N"

            mGuardarDatos()

            If txtEfectivo.Valor <> 0 Then
                mstrCmd = "depositos_verificar"
                mstrCmd = mstrCmd & " @depo_fecha=" & clsSQLServer.gFormatArg(txtFechaNueva.Text, SqlDbType.SmallDateTime)
                mstrCmd = mstrCmd & ",@depo_cuba_id=" & clsSQLServer.gFormatArg(cmbCuentaNueva.SelectedValue, SqlDbType.Int)
                mstrCmd = mstrCmd & ",@depo_mone_id=" & clsSQLServer.gFormatArg(cmbMoneda.Valor, SqlDbType.Int)
                mstrCmd = mstrCmd & ",@depo_monto=" & clsSQLServer.gFormatArg(txtEfectivo.Valor, SqlDbType.Decimal)
                mstrCmd = mstrCmd & ",@depo_emct_id=" & clsSQLServer.gFormatArg(hdnCenEmId.Text, SqlDbType.Int)
                mstrCmd = mstrCmd & ",@depo_audi_user=" & clsSQLServer.gFormatArg(Session("sUserId").ToString(), SqlDbType.Int)
                lstrResu = clsSQLServer.gCampoValorConsul(mstrConn, mstrCmd, "existe")
            End If

            If lstrResu = "S" And hdnExiste.Text = "" Then
                hdnExiste.Text = "S"
            Else
                Dim lDepositos As New SRA_Neg.Deposito(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaDepoCheq, mdsDatos.Copy, hdnChequesOriginales.Text)
                lstrIds = lDepositos.Alta()
                hdnDeposId.Text = lstrIds
                hdnExiste.Text = ""

                mConsultar()
                    mMostrarPanel(False)

                    Dim lstrRptName = "BoletasDeposito&DepoId=" + lstrIds

                    Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)

                End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim lstrIds As String

            mGuardarDatos()

            Dim lDepositos As New SRA_Neg.Deposito(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaDepoCheq, mdsDatos.Copy, hdnChequesOriginales.Text)
            lstrIds = lDepositos.Modif()

            hdnDeposId.Text = lstrIds
            'mListar(lstrIds)
            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            lobjGenerica.Baja(hdnId.Text)

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEliminarCheque(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            If CDate(txtFechaNueva.Fecha).Year * 10 + CDate(txtFechaNueva.Fecha).DayOfYear + 7 < Now.Year * 10 + Now.DayOfYear Then
                Throw New AccesoBD.clsErrNeg("No se puede editar Dep�sitos con m�s de 7 d�as de antig�edad.")
            End If

            mdsDatos.Tables(mstrTablaDepoCheq).Select("_cheq_id=" & E.Item.Cells(1).Text)(0).Delete()

            hdnChequesBorrados.Text = ""

            For Each oDataItem As DataRow In mdsDatos.Tables(mstrTablaDepoCheq).Select("", "", DataViewRowState.Deleted)
                oDataItem.RejectChanges()
                If hdnChequesBorrados.Text.Length > 0 Then hdnChequesBorrados.Text += ","
                hdnChequesBorrados.Text += oDataItem.Item("_cheq_id").ToString
                oDataItem.Delete()
            Next

            If (grdCheques.CurrentPageIndex < 0 Or grdCheques.CurrentPageIndex >= grdCheques.PageCount Or grdCheques.Items.Count = 1 And grdCheques.CurrentPageIndex <> 0) Then
                grdCheques.CurrentPageIndex = grdCheques.CurrentPageIndex - 1
            End If

            mConsultarCheques()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Opciones de POP"
    Private Sub btnConsulCheq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulCheq.Click
        mAbrirVentana()
    End Sub
    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            hdnValoresCheque.Text = hdnValoresCheque.Text + "," + hdnDatosPop.Text
            hdnDatosPop.Text = ""
            mConsultarChequesDisponibles()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    

    Private Sub mAbrirVentana()
        Try

            Dim lsbPagina As New System.Text.StringBuilder
            If cmbBancoNuevo.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un Banco en la solapa de Dep�sitos.")
            End If

            If cmbCuentaNueva.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar una Cuenta en la solapa de Dep�sitos.")
            End If

            Dim BancoGiroPostal As Integer = clsSQLServer.gParametroValorConsul(mstrConn, "para_giro_banc_id")

            lsbPagina.Append("ChequesDisponibles_Pop.aspx?cuenta=" & cmbCuentaNueva.Valor & "&ChequesGrilla=" & mIdCheques() & "&cemisor=" & hdnCenEmId.Text & "&clearing=" & hdnClearingId.Text & "&GiroBanc=" & IIf(Trim(hdnBancId.Text) = "", "2", IIf(Trim(hdnBancId.Text) = BancoGiroPostal.ToString, "1", "0")))
            clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 600, 400, 100, 100, True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Opciones de controles"
    Private Sub mShowTabs(ByVal Origen As Byte)
        panCabecera.Visible = False
        lnkCabecera.Font.Bold = False
        lnkDetalle.Font.Bold = False
        panDetalle.Visible = False
        Select Case Origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
            Case 2
                panDetalle.Visible = True
                lnkDetalle.Font.Bold = True
        End Select
    End Sub
    Private Sub mListar(ByVal pstrId As String)
        Try
            Dim lstrRptName As String

            lstrRptName = "BoletasDeposito"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&DepoId=" + pstrId
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
        mShowTabs(2)
    End Sub
#End Region

End Class
End Namespace
