<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Cuotas_Pop" CodeFile="Cuotas_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Generación de cuotas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<asp:panel id="panDato" runat="server" height="100%" cssclass="titulo" BorderStyle="Solid"
				BorderWidth="1px" Width="100%">
				<TABLE class="FdoFld" id="Table1" height="100%" width="100%" border="0">
					<TR>
						<TD align="right" width="40%">
							<asp:label id="lblCuotas" runat="server" cssclass="titulo">Cant. cuotas:</asp:label>&nbsp;</TD>
						<TD>
							<cc1:numberbox id="txtCuotas" style="FONT-SIZE: 8pt" runat="server" Width="55px" cssclass="cuadrotexto"
								height="18px" EsDecimal="False" MaxValor="20"></cc1:numberbox></TD>
					</TR>
					<TR>
						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="1" src="imagenes/formdivmed.jpg" width="1"></TD>
					</TR>
					<TR>
						<TD align="right" width="40%">
							<asp:label id="lblFVto" runat="server" cssclass="titulo">Fecha emisión :</asp:label>&nbsp;</TD>
						<TD>
							<cc1:datebox id="txtFVto" runat="server" Width="68px" cssclass="cuadrotexto" Obligatorio="true"
								AceptaNull="false"></cc1:datebox></TD>
					</TR>
					<TR>
						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="1" src="imagenes/formdivmed.jpg" width="1"></TD>
					</TR>
					<TR>
						<TD align="right" width="40%">
							<asp:label id="lblImp" runat="server" cssclass="titulo">Importe:</asp:label>&nbsp;</TD>
						<TD>
							<cc1:numberbox id="txtImp" style="FONT-SIZE: 8pt" runat="server" Width="55px" cssclass="cuadrotexto"
								height="18px" EsDecimal="True"></cc1:numberbox></TD>
					</TR>
					<TR>
						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="1" src="imagenes/formdivmed.jpg" width="1"></TD>
					</TR>
					<TR>
						<TD vAlign="middle" align="center" colSpan="2" height="30">
							<asp:button id="btnAceptar" runat="server" Width="80px" cssclass="boton" Text="Aceptar"></asp:button>&nbsp;
							<asp:button id="btnCerrar" runat="server" Width="80px" cssclass="boton" Text="Cerrar"></asp:button>&nbsp;
						</TD>
					</TR>
				</TABLE>
			</asp:panel>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		
			if (document.all["txtCuotas"]!= null)
				document.all["txtCuotas"].focus();
		
		</SCRIPT>
	</BODY>
</HTML>
