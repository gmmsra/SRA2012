<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Pelajes" CodeFile="Pelajes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Pelajes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Pelajes</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" BorderStyle="Solid"
										Width="100%">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																	ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																	ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbRazaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="280px"
																				Height="20px" MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="10" colSpan="3"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" colSpan="3" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="pela_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="_raza_desc" HeaderText="Raza">
												<HeaderStyle Width="25%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="pela_codi" ReadOnly="True" HeaderText="C&#243;digo">
												<HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="pela_desc" ReadOnly="True" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="38%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="pela_abre" ReadOnly="True" HeaderText="Abreviatura">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_rgub_desc" ReadOnly="True" HeaderText="Ubicaci&#243;n">
												<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="3">
									<TABLE style="WIDTH: 100%" id="Table3" border="0" cellPadding="0" align="left">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
													ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif"
													ToolTip="Agregar un Nuevo Pelaje"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
													ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
													BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnImpr0.gif" ToolTip="Imprimir Listado"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" align="center">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 224px">
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 224px; HEIGHT: 19px" align="right">
															<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label></TD>
														<TD style="WIDTH: 27.6%; HEIGHT: 19px" background="imagenes/formfdofields.jpg" align="left">
															<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="280px" Height="20px"
																MostrarBotones="False" NomOper="razas_cargar" filtra="true" Obligatorio="True"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 224px; HEIGHT: 16px" align="right">
															<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label></TD>
														<TD style="HEIGHT: 16px" height="16" colSpan="2">
															<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="264px" MaxLength="20"
																Obligatorio="True"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 224px; HEIGHT: 16px" align="right">
															<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label></TD>
														<TD style="HEIGHT: 16px" height="16" colSpan="2">
															<cc1:numberbox id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 224px; HEIGHT: 21px" align="right">
															<asp:Label id="lblImpo" runat="server" cssclass="titulo">Abreviatura:</asp:Label></TD>
														<TD style="HEIGHT: 16px" height="16" colSpan="2">
															<CC1:TEXTBOXTAB id="txtAbreviatura" runat="server" cssclass="cuadrotexto" Width="264px" MaxLength="5"
																Obligatorio="True"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 224px; HEIGHT: 5px" align="right">
															<asp:Label id="lblUbicacion" runat="server" cssclass="titulo">Ubicaci�n:</asp:Label></TD>
														<TD style="HEIGHT: 5px" colSpan="2">
															<cc1:combobox id="cmbUbicacion" class="combo" runat="server" cssclass="cuadrotexto" Width="280px"
																Height="20px" MostrarBotones="False" NomOper="razas_cargar" filtra="true" Obligatorio="True"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 224px" align="right"></TD>
														<TD colSpan="2"></TD>
													</TR>
													<TR>
														<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
			
		function ValorFijoChange()
		{
			if(window.event.srcElement.value!="")
				document.all["cmbTari"].selectedIndex=2;
			else
				document.all["cmbTari"].selectedIndex=1;
		}
		</SCRIPT>
	</BODY>
</HTML>
