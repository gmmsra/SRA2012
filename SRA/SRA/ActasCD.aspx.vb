Namespace SRA

Partial Class ActasCD
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_ActasCD
    Private mstrTablaSoli As String = SRA_Neg.Constantes.gTab_ActasCDSoli

    Private mstrConn As String
    Private mstrParaPageSize As Integer
    Public mbooRegis As Boolean
    Private mdsDatos As New DataSet

    Private Enum Columnas As Integer
        Id = 0
        SoinId = 1
        SocmId = 2
        chkSel = 7
        cmbEsta = 8
    End Enum

    Private Enum Impresion As Integer
        Gral = 0
        Apro = 1
        Pren = 2
        Rein = 3
        CambEsta = 4
        CambCate = 5
    End Enum
#End Region

#Region "Inicialización de Variables"
    Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        If Not Request.QueryString("reg") Is Nothing Then
            mbooRegis = True
        End If

        grdSolicitudes.Columns(Columnas.chkSel).Visible = Not mbooRegis
        grdSolicitudes.Columns(Columnas.cmbEsta).Visible = mbooRegis

        If mbooRegis Then
            txtFecha.Enabled = False
            txtNume.Enabled = False
            btnAgre.Visible = False
            btnTraer.Visible = False
            btnBaja.Visible = False
            btnLimp.Enabled = False
            lblTituAbm.Text = "Registración de Decisiones"
        Else
            btnCerrar.Visible = False
        End If
    End Sub

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearEventos()
                mSetearMaxLength()

                mConsultar()
                hdnEdit.Text = "0"
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                mdsDatos = Session("solicitudes")
                mSetearSoliRegis()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mLlenarDataSet(ByVal pDS As DataSet)

    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "accd_nume")
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnCerrar.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub

    Public Sub mCargarDatos(ByVal pstrId As String)
        Dim ldsEsta As DataSet = CrearDataSet(pstrId)
        Dim ban As Integer = 0

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("accd_id").ToString()
                txtNume.Valor = IIf(.Item("accd_nume").ToString.Trim.Length > 0, .Item("accd_nume"), String.Empty)
                txtFecha.Fecha = IIf(.Item("accd_fecha").ToString.Trim.Length > 0, .Item("accd_fecha"), String.Empty)

            If Not .IsNull("accd_baja_fecha") Then
                lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("accd_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                ban = 1
            Else
                lblBaja.Text = ""
            End If
            If Not .IsNull("accd_cierre_fecha") Then
                lblBaja.Text = "Acta Cerrada en fecha: " & CDate(.Item("accd_cierre_fecha")).ToString("dd/MM/yyyy HH:mm")
                ban = 1
            End If

        End With

        If mbooRegis Then   'si es registración solo muestro las grabadas
            LimpiarTablaSoli(ldsEsta)
            hdnUltSociNume.Text = clsSQLServer.gExecuteScalar(mstrConn, "socios_ult_num_consul")
        End If

        mdsDatos = Nothing
        mdsDatos = New DataSet
        mdsDatos.Tables.Add(ldsEsta.Tables(mstrTablaSoli).Copy())
        Session("solicitudes") = mdsDatos

        grdSolicitudes.DataSource = Nothing
        grdSolicitudes.CurrentPageIndex = 0
        grdSolicitudes.DataSource = mdsDatos.Tables(0)
        grdSolicitudes.DataBind()
        hdnTotalSolis.Text = grdSolicitudes.Items.Count

        mSetearSoliRegis()

        lblTitu.Text = "Registro Seleccionado: " + txtNume.Text + " - " + txtFecha.Fecha

        mChequearGrilla(0, True)
        mSetearEditor("", False)
        mMostrarPanel(True)
        If ban = 1 Then
            btnModi.Enabled = False
            btnBaja.Enabled = False
            btnCerrar.Enabled = False
        End If
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        lblTitu.Text = ""

        txtNume.Text = ""
        txtFecha.Text = ""

        grdSolicitudes.CurrentPageIndex = 0
        grdSolicitudes.DataSource = Nothing
        grdSolicitudes.DataBind()
        hdnTotalSolis.Text = grdSolicitudes.Items.Count

        lblBaja.Text = ""

        mSetearEditor("", True)
    End Sub

    Private Sub mCerrar()
        mLimpiar()
        mConsultar()
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panBotonesImp.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        btnAgre.Visible = Not panDato.Visible And Not mbooRegis
        grdDato.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
            grdDato.DataBind()
        End If
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
                Dim ldsEstruc As DataSet = mGuardarDatos(False)

                Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Alta()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

        Private Sub mModi(ByVal pbooCierre As Boolean)
            Try
                Dim ldsEstruc As DataSet = mGuardarDatos(pbooCierre)
                ldsEstruc.Tables(mstrTablaSoli).DefaultView.Sort = "_soin_titu_clie_id"

                Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
                lobjGenerico.Modi()

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mBaja()
        Try
            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerico.Baja(hdnId.Text)

            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCerrarActa()
        txtFechaCierre.Text = Now
            mModi(True)
        End Sub

        Private Function mGuardarDatos(ByVal pbooCierre As Boolean) As DataSet
            mValidarDatos()
            Dim lDt As DataTable
            Dim i As Integer

            Dim ldsEsta As DataSet = CrearDataSet(hdnId.Text)

            ldsEsta.Tables(0).TableName = mstrTabla
            ldsEsta.Tables(1).TableName = mstrTablaSoli

            With ldsEsta.Tables(0).Rows(0)
                .Item("accd_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("accd_nume") = IIf(txtNume.Valor.Trim.Length > 0, txtNume.Valor, DBNull.Value)
                .Item("accd_fecha") = IIf(txtFecha.Fecha.Trim.Trim.Length > 0, txtFecha.Fecha, DBNull.Value)
                '.Item("accd_cierre_fecha") = IIf(txtFechaCierre.Fecha.Trim.Length > 0, txtFechaCierre.Fecha, DBNull.Value)
                .Item("accd_cierre_fecha") = IIf(pbooCierre = True, Now, DBNull.Value)
                .Item("accd_baja_fecha") = DBNull.Value
            End With

            If Not mbooRegis Then
                mGuardarDatosSoli(ldsEsta)
            Else
                ldsEsta.Tables(mstrTablaSoli).Columns.Add("soci_nume", System.Type.GetType("System.String"))

                mGuardarDatosSoliRegis(ldsEsta)
            End If

            Return ldsEsta
        End Function

        Private Function CrearDataSet(ByVal pstrId As String) As DataSet
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        ldsEsta.Tables(0).TableName = mstrTabla
        ldsEsta.Tables(1).TableName = mstrTablaSoli

        If mbooRegis Then
            With ldsEsta.Tables.Add(mstrTabla & "_proceso")
                .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
                .Columns.Add("proc_accd_id", System.Type.GetType("System.Int32"))
                .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
                .Rows.Add(.NewRow)
                .Rows(0).Item("proc_id") = 0
            End With
        End If

        Return ldsEsta
    End Function

    Private Sub mGuardarDatosSoli(ByVal pdsEsta As DataSet)
        Dim ldrDatos As DataRow
        Dim lbooSelec As Boolean
        Dim lstrFiltro As String

        'si es la generación,le agrego todos y voy descartando
        pdsEsta.Tables.Remove(mstrTablaSoli)
        pdsEsta.Tables.Add(mCrearDsSoli().Tables(0).Copy)
        pdsEsta.Tables(1).TableName = mstrTablaSoli

        For Each oItem As DataGridItem In grdSolicitudes.Items
            lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked

            If oItem.Cells(Columnas.Id).Text <> "0" And Not lbooSelec Then
                'si existe y no este selec, la borra
                pdsEsta.Tables(mstrTablaSoli).Select("sacd_id=" & oItem.Cells(Columnas.Id).Text)(0).Delete()

            ElseIf oItem.Cells(Columnas.Id).Text = "0" And lbooSelec Then
                If IsNumeric(oItem.Cells(Columnas.SoinId).Text) Then
                    lstrFiltro = "sacd_soin_id=" & oItem.Cells(Columnas.SoinId).Text
                Else
                    lstrFiltro = "sacd_socm_id=" & oItem.Cells(Columnas.SocmId).Text
                End If

                ldrDatos = pdsEsta.Tables(mstrTablaSoli).Select(lstrFiltro)(0)

                With ldrDatos
                    .Item("sacd_id") = clsSQLServer.gObtenerId(.Table, "sacd_id")
                    .Item("sacd_accd_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                End With
            End If
        Next
        LimpiarTablaSoli(pdsEsta)
    End Sub

    Private Sub mGuardarDatosSoliRegis(ByVal pdsEsta As DataSet)
        Dim ldrDatos As DataRow
        Dim lcmbEsta As NixorControls.ComboBox
        Dim ltxtSociNume As NixorControls.NumberBox

        For Each oItem As DataGridItem In grdSolicitudes.Items
            lcmbEsta = oItem.FindControl("cmbEsta")
            ltxtSociNume = oItem.FindControl("txtSociNume")
            ldrDatos = pdsEsta.Tables(mstrTablaSoli).Select("sacd_id=" & oItem.Cells(Columnas.Id).Text)(0)

            With ldrDatos
                    .Item("sacd_esta_id") = IIf(lcmbEsta.Valor.Trim.Length > 0, lcmbEsta.Valor, DBNull.Value)
                .Item("sacd_resu") = Not .IsNull("sacd_esta_id")
                    .Item("soci_nume") = IIf(ltxtSociNume.Valor.Trim.Length > 0, ltxtSociNume.Valor, DBNull.Value)
            End With
        Next

            LimpiarTablaSoli(pdsEsta)
        End Sub

    Private Sub mSetearSoliRegis()
        Dim ltxtSociNume As NixorControls.NumberBox
        Dim ltxtSociNumeAnt As HtmlControls.HtmlContainerControl
        Dim ldivSoliNume As HtmlControls.HtmlContainerControl
        Dim ldivSoliTipo As HtmlControls.HtmlContainerControl

        If mbooRegis Then
            For Each oItem As DataGridItem In grdSolicitudes.Items
                ltxtSociNume = oItem.FindControl("txtSociNume")
                ldivSoliNume = oItem.FindControl("divSociNume")
                ltxtSociNumeAnt = oItem.FindControl("divAnteSoci")
                ldivSoliTipo = oItem.FindControl("divSoliTipo")
                ldivSoliNume.Style.Add("display", IIf(ltxtSociNume.Valor Is DBNull.Value, "none", "inline"))
            Next
        End If
    End Sub

    Private Sub LimpiarTablaSoli(ByVal pdsEsta As DataSet)
        For Each oDataRow As DataRow In pdsEsta.Tables(mstrTablaSoli).Select("sacd_id=0")
            pdsEsta.Tables(mstrTablaSoli).Rows.Remove(oDataRow)
        Next
    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub DataGridSoli_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            'If Not mbooRegis Then mGuardarCheck(grdSolicitudes.CurrentPageIndex)

            grdSolicitudes.EditItemIndex = -1
            If (grdSolicitudes.CurrentPageIndex < 0 Or grdSolicitudes.CurrentPageIndex >= grdSolicitudes.PageCount) Then
                grdSolicitudes.CurrentPageIndex = 0
            Else
                grdSolicitudes.CurrentPageIndex = E.NewPageIndex
            End If
            grdSolicitudes.DataSource = mdsDatos
            grdSolicitudes.DataBind()

            If mbooRegis Then mSetearSoliRegis()
            'If Not mbooRegis Then mChequearGrilla(grdSolicitudes.CurrentPageIndex, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarCheck(ByVal pintPage As Integer)
        Dim dr As DataRow
        Dim i As Integer

        i = pintPage * 10
        'Mantengo los check de las paguinas de la grilla
        For Each oDataItem As DataGridItem In grdSolicitudes.Items
            If mdsDatos.Tables(0).Select("").Length <> 0 Then
                dr = mdsDatos.Tables(0).Select("")(oDataItem.ItemIndex + i)
                dr.Item("_chk") = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
            End If
        Next
        Session("solicitudes") = mdsDatos
    End Sub

    Private Sub mChequearGrilla(ByVal pintPage As Integer, ByVal pbooEdit As Boolean)
        Dim dr As DataRow

        'Mantengo los check de las paguinas de la grilla
        For Each oDataItem As DataGridItem In grdSolicitudes.Items
            If Not pbooEdit Then
                If CType(oDataItem.Cells(12).Text, Boolean) Then
                    DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked = True
                Else
                    DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked = False
                End If
            Else
                DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked = True
            End If
        Next
    End Sub

    Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            hdnEdit.Text = "1"
            mCargarDatos(e.Item.Cells(1).Text)

            mHabilitarBotonesImpresion()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi(False)
        End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "_consul")
            lstrCmd.Append(" @accd_nume=" + txtNumeFil.Valor.ToString)
            lstrCmd.Append(", @fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha))
            lstrCmd.Append(", @fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha))

            If mbooRegis Then
                lstrCmd.Append(", @regis=1")
            End If

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultarSoli()
        Try
            mdsDatos = mCrearDsSoli()

            grdSolicitudes.DataSource = mdsDatos.Tables(0)
            grdSolicitudes.DataBind()

            hdnTotalSolis.Text = grdSolicitudes.Items.Count.ToString
            Session("solicitudes") = mdsDatos
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Function mCrearDsSoli() As DataSet
        Dim lstrCmd As New StringBuilder
        lstrCmd.Append("exec " + mstrTablaSoli + "_consul")
        lstrCmd.Append(" @sacd_accd_id=" + clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))
        'If hdnEdit.Text = "0" Then
        lstrCmd.Append(",@todos=1")
        'End If

        Return (clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString))
    End Function

    Public Sub mHabilitarBotonesImpresion()
        Try
            Dim ds As New DataSet
            Dim lstrCmd As New StringBuilder

            'btnSolIngGral.Attributes.Add("onclick", "Imprimir('SolicitudesDeIngreso','Socios a Votar');")
            'btnSolIngApro.Attributes.Add("onclick", "Imprimir('SolicitudesDeIngresoApro','Socios Aprobados');")
            'btnSolIngPren.Attributes.Add("onclick", "Imprimir('SolicitudesDeIngresoPren','Socios a Presentarse');")
            'btnSolIngRein.Attributes.Add("onclick", "Imprimir('SolicitudesDeIngresoRein','Socios Reincorporados');")
            'btnCambEstado.Attributes.Add("onclick", "Imprimir('CambiosDeEstado','Socios Renunciantes');")
            'btnCambCatego.Attributes.Add("onclick", "Imprimir('CambiosDeCategoria','Cambios de Categoría');")

            lstrCmd.Append("exec " + "solicitudes_de_ingresos_counts")
            lstrCmd.Append(" @accd_id=" + hdnId.Text)
            lstrCmd.Append(",@accd_nume=" + txtNume.Text)

            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            '********* ES REGISTRACIÓN *********
            If mbooRegis Then
                'Socios  a Presentarse.
                btnSolIngPren.Enabled = False
                'Socios Reincorporados.
                btnSolIngRein.Enabled = False
                'Socios a Votar.
                btnSolIngGral.Enabled = False

                'Socios Aprobados.
                If ds.Tables(0).Rows(Impresion.Apro).Item("Count") <> 0 Then
                    btnSolIngApro.Enabled = True
                Else
                    btnSolIngApro.Enabled = False
                End If
            Else
                '********* ES GENERACIÓN *********

                'Socios Aprobados.
                btnSolIngApro.Enabled = False

                'Socios a Presentarse.
                If ds.Tables(0).Rows(Impresion.Pren).Item("Count") <> 0 Then
                    btnSolIngPren.Enabled = True
                Else
                    btnSolIngPren.Enabled = False
                End If

                'Socios Reincorporados.
                If ds.Tables(0).Rows(Impresion.Rein).Item("Count") <> 0 Then
                    btnSolIngRein.Enabled = True
                Else
                    btnSolIngRein.Enabled = False
                End If

                'Socios a Votar.
                If ds.Tables(0).Rows(Impresion.Gral).Item("Count") <> 0 Then
                    btnSolIngGral.Enabled = True
                Else
                    btnSolIngGral.Enabled = False
                End If
            End If

            '********* REGISTRACIÓN y GENERACIÓN *********

            'Socios Renunciantes (Cambios de Estado).
            If ds.Tables(0).Rows(Impresion.CambEsta).Item("Count") <> 0 Then
                btnCambEstado.Enabled = True
            Else
                btnCambEstado.Enabled = False
            End If

            'Cambios de Categoría.
            If ds.Tables(0).Rows(Impresion.CambCate).Item("Count") <> 0 Then
                btnCambCatego.Enabled = True
            Else
                btnCambCatego.Enabled = False
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnTraer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTraer.Click
        mConsultarSoli()
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            If lblBaja.Text = "" Then
                    mGuardarDatos(False)
                    mModi(False)
                End If

            Dim lstrRptName As String = "actas_cd_planilla"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&accd_id=" + hdnId.Text
            lstrRpt += "&accd_nume=" + txtNume.Text

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mListar(ByVal lstrRptName As String)
        Try
            If lblBaja.Text = "" Then
                    mGuardarDatos(False)
                    mModi(False)
                End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            lstrRpt += "&accd_id=" + hdnId.Text
            lstrRpt += "&accd_nume=" + txtNume.Text

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub grdSolicitudes_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdSolicitudes.ItemDataBound
        Try
            If e.Item.ItemIndex <> -1 Then
                If Not mbooRegis Then
                    Dim ochkSel As CheckBox = e.Item.FindControl("chkSel")

                    With CType(e.Item.DataItem, DataRowView).Row
                        ochkSel.Checked = .Item("sacd_id") <> 0
                    End With
                Else
                    Dim ocmbEsta As NixorControls.ComboBox = e.Item.FindControl("cmbEsta")
                    Dim ltxtSociNume As NixorControls.NumberBox = e.Item.FindControl("txtSociNume")

                        If e.Item.Cells(14).Text = "4" Then 'Cambio de Categoria
                            Dim importe As Int16 = e.Item.Cells(13).Text
                            If importe = "0" Then  'Verifico si tiene deuda Anterior
                                clsWeb.gCargarRefeCmb(mstrConn, "estados", ocmbEsta, "S", SRA_Neg.Constantes.EstaTipos.EstaTipo_SoliActasCD)
                            Else
                                clsWeb.gCargarRefeCmb(mstrConn, "estados_decisiones", ocmbEsta, "S", SRA_Neg.Constantes.EstaTipos.EstaTipo_SoliActasCD)
                            End If
                        Else
                            If e.Item.Cells(10).Text = "0" And e.Item.Cells(11).Text = "1" Then
                            clsWeb.gCargarRefeCmb(mstrConn, "estados_decisiones", ocmbEsta, "S", SRA_Neg.Constantes.EstaTipos.EstaTipo_SoliActasCD)
                        Else
                            clsWeb.gCargarRefeCmb(mstrConn, "estados", ocmbEsta, "S", SRA_Neg.Constantes.EstaTipos.EstaTipo_SoliActasCD)
                        End If
                    End If
                    With CType(e.Item.DataItem, DataRowView).Row
                        ocmbEsta.Valor = .Item("sacd_esta_id")
                            ltxtSociNume.Valor = .Item("_soci_nume").ToString

                        If Not ocmbEsta.Valor Is DBNull.Value AndAlso (ocmbEsta.Valor = SRA_Neg.Constantes.Estados.SoliActasCD_Aprobada Or ocmbEsta.Valor = SRA_Neg.Constantes.Estados.SoliActasCD_Rechazada Or ocmbEsta.Valor = SRA_Neg.Constantes.Estados.SoliActasCD_Pospuesta) Then
                            ocmbEsta.Enabled = False
                            ltxtSociNume.Enabled = False
                        End If

                        If ocmbEsta.Enabled And Not .IsNull("sacd_soin_id") Then
                            ocmbEsta.Valor = CType(SRA_Neg.Constantes.Estados.SoliActasCD_Presentada, String)
                        End If
                        If ocmbEsta.Enabled And lblBaja.Text <> "" Then
                            ocmbEsta.Enabled = False
                            ltxtSociNume.Visible = False
                        End If
                    End With
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        Try
            hdnEdit.Text = "0"
            mAgregar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAlta_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnAlta.Command

    End Sub

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        mCerrarActa()
    End Sub

    Private Sub btnSolIngGral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolIngGral.Click
        mListar("SolicitudesDeIngreso")
    End Sub

    Private Sub btnSolIngApro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolIngApro.Click
        mListar("SolicitudesDeIngresoApro")
    End Sub

    Private Sub btnSolIngPren_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolIngPren.Click
        mListar("SolicitudesDeIngresoPren")
    End Sub

    Private Sub btnSolIngRein_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolIngRein.Click
        mListar("SolicitudesDeIngresoRein")
    End Sub

    Private Sub btnCambEstado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCambEstado.Click
        mListar("CambiosDeEstado")
    End Sub

    Private Sub btnCambCatego_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCambCatego.Click
        mListar("CambiosDeCategoria")
    End Sub
End Class
End Namespace
