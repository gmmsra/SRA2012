Imports System.Data.SqlClient


Namespace SRA


Partial Class TeServiConsulta
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub 

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo. 
      InitializeComponent()

      If Page.IsPostBack Then
         For i As Integer = 0 To 2
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            grdConsulta.Columns.Add(dgCol)
         Next
      End If
   End Sub

#End Region


#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String
   Public mstrTitulo As String
   Public mstrFiltros As String
   Public mstrSexo As String
   Public mstrRaza As String
   Public mstrNaciFecha As String
   Private mstrConn As String
   Private mbooEsConsul As Boolean
   Private mbooAutopostback As Boolean
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            If mstrNaciFecha <> "" Then
                txtAnioFil.Text = mCalcularFecha(mstrNaciFecha, mstrRaza)
            Else
                txtAnioFil.Text = Today.Year.ToString
            End If
            mConsultar(False)
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrTitulo = Request.QueryString("titulo")
      mstrFiltros = Request.QueryString("filtros")
      mstrSexo = Request.QueryString("prdt_sexo")
      mstrRaza = Request.QueryString("prdt_raza")
      mstrNaciFecha = Request.QueryString("prdt_naci")

      lblTituAbm.Text = mstrTitulo

      If mstrTabla = "rg_servi_denuncias_naci" Then
         'servicios
         lblMadreFil.Text = "Madre-HBA:"
         lblRpFil.Text = "RP:"
         txtReceCaraFil.Visible = False
         txtMadreNumeFil.Visible = True
			txtMadreRpFil.MaxLength = 20
			btnAgre.Text = "Agregar Nuevo Servicio"
      Else
         'transplantes
         lblMadreFil.Text = "Receptora-Caravana:"
         lblRpFil.Text = "Tatuaje:"
         txtReceCaraFil.Visible = True
         txtMadreNumeFil.Visible = False
			txtMadreRpFil.MaxLength = 10
			btnAgre.Text = "Agregar Nuevo T/E"
      End If

      If Not Request.QueryString("EsConsul") Is Nothing Then
         mbooEsConsul = CBool(CInt(Request.QueryString("EsConsul")))
      End If

      If Not Request.QueryString("Autopostback") Is Nothing Then
         mbooAutopostback = CBool(CInt(Request.QueryString("Autopostback")))
      Else
         mbooAutopostback = True
      End If

      If mbooEsConsul Then
         grdConsulta.Columns(0).Visible = False
      End If

      grdConsulta.PageSize = 100 'NO PAGINAR PARA PODER IMPRIMIR TODO EL CONTENIDO
      btnImpri.Attributes.Add("onclick", "mImprimir();return(false);")
      btnAgre.Attributes.Add("onclick", "mAlta('" & mstrTabla & "');return(false);")

   End Sub

   Private Function mCalcularFecha(ByVal pstrFecha As String, ByVal pstrRaza As String) As String
      Try
         Dim lstrCmd As String = "exec razas_consul @raza_id=" & pstrRaza
         Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), lstrCmd)
         Dim lintDias As Integer = 0
         Dim datNaciFecha As DateTime = Convert.ToDateTime(pstrFecha)
         While (dr.Read())
            If Not dr.GetValue(dr.GetOrdinal("raza_gesta_dias")) Is DBNull.Value Then
                lintDias = dr.GetValue(dr.GetOrdinal("raza_gesta_dias"))
            End If
         End While
         dr.Close()
         If lintDias <> 0 Then
            Return (Year(datNaciFecha.AddDays(lintDias * (-1))).ToString())
         Else
            Return (Today.Year.ToString())
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Function

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Dim lsbMsg As New StringBuilder

      lsbMsg.Append("<SCRIPT language='javascript'>")

      Select Case mstrTabla
        Case "te_denun_deta_naci"
          lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosTEPop'].value='{0}';", E.Item.Cells(1).Text))
          If mbooAutopostback Then
             lsbMsg.Append("window.opener.__doPostBack('hdnDatosTEPop','');")
          End If
        Case Else
          lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(1).Text))
          If mbooAutopostback Then
             lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
          End If
      End Select

      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)

   End Sub

   Public Sub mConsultar(ByVal pbooPage As Boolean)
      Try
         mstrCmd = "exec " & mstrTabla & "_busq "
         If cmbAsocFil.Valor <> "T" Then
            mstrCmd += "@asoc=" & cmbAsocFil.Valor
         Else
            mstrCmd += "@asoc=null"
         End If
         If mstrFiltros <> "" Then
            mstrCmd += ",@cria_id=" & mstrFiltros
         End If
         If txtAnioFil.Text <> "" Then
            mstrCmd += ",@anio=" & txtAnioFil.Text
         End If
         If mstrTabla = "rg_servi_denuncias_naci" Then
            'servicios busca por la madre
            If txtMadreRpFil.Text <> "" Then
                mstrCmd += ",@madre_rp=" & clsSQLServer.gFormatArg(txtMadreRpFil.Text, SqlDbType.VarChar)
            End If
            If txtMadreNumeFil.Text <> "" Then
                mstrCmd += ",@madre_sra_nume=" & txtMadreNumeFil.Text
            End If
         Else
            'implantes busca por receptora
            If txtMadreRpFil.Text <> "" Then
                mstrCmd += ",@rece_rp=" & clsSQLServer.gFormatArg(txtMadreRpFil.Text, SqlDbType.VarChar)
            End If
            If txtReceCaraFil.Text <> "" Then
                mstrCmd += ",@rece_carv=" & clsSQLServer.gFormatArg(txtReceCaraFil.Text, SqlDbType.VarChar)
            End If
         End If

         Dim ds As New DataSet
         Dim lintFila As Integer
         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

         If Not pbooPage Then
            Dim i As Integer = grdConsulta.Columns.Count - 1
            While i > 0
               grdConsulta.Columns.Remove(grdConsulta.Columns(i))
               i -= 1
            End While
            For Each dc As DataColumn In ds.Tables(0).Columns
               Dim dgCol As New Web.UI.WebControls.BoundColumn
               dgCol.DataField = dc.ColumnName
               dgCol.HeaderText = dc.ColumnName
               If dc.Ordinal = 0 Then
                  dgCol.Visible = False
               End If
               grdConsulta.Columns.Add(dgCol)
            Next
         End If
         grdConsulta.DataSource = ds
         grdConsulta.DataBind()
         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
  mConsultar(False)
End Sub

Private Sub grdConsulta_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdConsulta.ItemDataBound
    If e.Item.Cells(8).Text.ToUpper = "RECHAZADO" Then
        e.Item.Cells(0).Text = ""
    End If
End Sub

End Class

End Namespace
