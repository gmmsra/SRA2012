<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%--<%@ Page Language="vb" AutoEventWireup="false" Codebehind="InspeccionesFenotipicas.aspx.vb" Inherits="SRA.InspeccionesFenotipicas" %>--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/InspeccionesFenotipicas.aspx.vb" Inherits="InspeccionesFenotipicas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Inspecciones Fenot�picas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Inspecciones Fenot�picas</asp:label></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" Visible="True" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" align="right">
																		<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																					<asp:label id="lblEspecie" runat="server" cssclass="titulo">Especie:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbEspecieFil" runat="server" Width="176px" onchange="mCargarRaza()"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																					<asp:label id="lblRazasFil" runat="server" cssclass="titulo">Razas:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="280px"
																						onchange="setCmbEspecie()" filtra="true" NomOper="razas_cargar" MostrarBotones="False" Height="20px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="raza_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="raza_codi" ReadOnly="True" HeaderText="C&#243;digo">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="raza_abre" HeaderText="Abreviatura"></asp:BoundColumn>
													<asp:BoundColumn DataField="raza_desc" ReadOnly="True" HeaderText="Descripci&#243;n"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 9px" height="9"></TD>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
												ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Nuevo Centro de Implantes"></CC1:BOTONIMAGEN></TD>
										<td align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
												ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnImpr0.gif" ToolTip="Listar"></CC1:BOTONIMAGEN></td>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False" Font-Bold="True"> General</asp:linkbutton></TD>
														<TD style="WIDTH: 23px" width="23"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkGenealogia1" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="100px" Height="21px" CausesValidation="False">Genealog�a(1)</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkGenealogia2" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="100px" Height="21px" CausesValidation="False">Genealog�a(2)</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkAnalisis" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="88px" Height="21px" CausesValidation="False">An�lisis</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkDocumentos" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="88px" Height="21px" CausesValidation="False">Documentos</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" width="100%" Height="116px">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 16px" align="left">
																			<asp:Label id="lblTitu" runat="server" cssclass="titulo">Titulo:</asp:Label></TD>
																		<TD style="HEIGHT: 16px"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDescripcion" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtDescripcion" runat="server" cssclass="cuadrotexto" Width="184px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 18px" align="right">
																			<asp:Label id="lblAbreviatura" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px">
																			<CC1:TEXTBOXTAB id="txtAbreviatura" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="2">
															<asp:panel id="panGenealogia1" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaEspecies1" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" colSpan="2">
																			<asp:Label id="lblTituGen1" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right">
																			<asp:Label id="lblPromVida" runat="server" cssclass="titulo">Prom. vida (a�os)</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:numberbox id="txtPromVida" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEspPromV" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 257px; heigth: 16px" align="right">
																			<asp:Label id="lblDiasGest" runat="server" cssclass="titulo">D�as de Gestaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 22px">
																			<cc1:numberbox id="txtDiasGest" runat="server" cssclass="cuadrotexto" Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEspGestaDias" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="heigth: 16px" align="right">
																			<asp:Label id="lblDiasGestaMax" runat="server" cssclass="titulo">D�as Gestaci�n (M�x):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtDiasGestMax" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																				Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEspGestMax" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDiasGestMin" runat="server" cssclass="titulo">D�as Gestaci�n (Min):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtDiasGestMin" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																				Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEspGestMin" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDiasServCamp" runat="server" cssclass="titulo">D�as entre Servicios a campo (Min):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtDiasServCamp" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																				Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEspDiasCamp" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDiasInsem" runat="server" cssclass="titulo">D�as entre Inseminaciones (Min):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtDiasInsem" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEspDiasInse" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDiasParto" runat="server" cssclass="titulo">D�as entre Partos (Min):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtDiasParto" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEspDiasParto" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblRegPrep" runat="server" cssclass="titulo">Registro Preparatorio:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbRegPrep" runat="server" Width="70px" onchange="SetCmbCtrlReg()"
																				obligatorio="true"></cc1:combobox>&nbsp;
																			<asp:Label id="lblEspRegPrep" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblCtrlReg" runat="server" cssclass="titulo">Controla Registro:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbCtrlReg" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
																			<asp:Label id="lblEspCtrlReg" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="2">
															<asp:panel id="panGenealogia2" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaEspecies2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" colSpan="2">
																			<asp:Label id="lblTituGen2" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblCaract" runat="server" cssclass="titulo">Caracter�stica a Destacar:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtCaract" runat="server" cssclass="cuadrotexto" Width="100px"></CC1:TEXTBOXTAB>&nbsp;
																			<asp:Label id="lblEspCaract" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCaractPrec" runat="server" cssclass="titulo">Caracter�stica Precede al Nro:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbCaractPrec" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
																			<asp:Label id="lblEspCaractPrec" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small"
																				Font-Italic="True">Label</asp:Label></TD>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblAstado" runat="server" cssclass="titulo">Astado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbAstado" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
																			<asp:Label id="lblEspAstado" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblMocho" runat="server" cssclass="titulo">Mocho:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbMocho" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
																			<asp:Label id="lblEspMocho" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCtrlColor" runat="server" cssclass="titulo">Controla Color y Pelaje:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbCtrlColor" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
																			<asp:Label id="lblEspCtrlColor" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblPerf" runat="server" cssclass="titulo">Performance:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbPerf" runat="server" Width="176px">
																				<asp:ListItem Selected="True" Value="">Ninguna</asp:ListItem>
																				<asp:ListItem Value="0">Lechera</asp:ListItem>
																				<asp:ListItem Value="1">Carne</asp:ListItem>
																				<asp:ListItem Value="2">Ambas</asp:ListItem>
																			</cc1:combobox>&nbsp;
																			<asp:Label id="lblEspPerf" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">Label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 15px" align="right">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Requiere Inspecci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 15px">
																			<cc1:combobox class="combo" id="cmbReqInsp" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 21px; heigth: 16px" align="right">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Requiere Diagrama:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 21px">
																			<cc1:combobox class="combo" id="cmbReqDiag" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; HEIGHT: 21px; heigth: 16px" align="right">
																			<asp:Label id="lblCalifica" runat="server" cssclass="titulo">Califica:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 21px">
																			<cc1:combobox class="combo" id="cmbCalifica" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px; heigth: 16px" align="right">
																			<asp:Label id="lblMuestraCrias" runat="server" cssclass="titulo">Muestra Cr�as:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbMuestraCrias" runat="server" Width="70px" onchange="habilitarMuestras()"
																				obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px" align="right">
																			<asp:Label id="lblMuestraCriasPorc" runat="server" cssclass="titulo">Muestra de Cr�as (%):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtMuestraCriasPorc" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																				Width="55px" EsDecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 255px" align="right">
																			<asp:Label id="lblMuestraCriasMin" runat="server" cssclass="titulo">Muestra de Cr�as (M�n):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtMuestraCriasMin" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																				Width="55px" EsDecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="PanAnalisis" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaRazas3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" colSpan="2">
																			<asp:Label id="lblAnalisisTipif" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small"
																				Font-Italic="True">label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px" align="right">
																			<asp:Label id="lblTipifMacho" runat="server" cssclass="titulo">An�lisis Tipificaci�n (M):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbTipifMacho" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px" align="right">
																			<asp:Label id="lblTipifHembra" runat="server" cssclass="titulo">An�lisis Tipificaci�n (H):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbTipifHembra" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px" align="right">
																			<asp:Label id="lblTipifCria" runat="server" cssclass="titulo">An�lisis Tipificaci�n (Cr�a):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbTipifCria" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px" align="right">
																			<asp:Label id="lblTipifCriaTE" runat="server" cssclass="titulo">An�lisis Tipificaci�n (Cr�a TE):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbTipifCriaTE" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 13px" align="right" colSpan="2">
																			<asp:Label id="lblAnalisisADN" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">label</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px" align="right">
																			<asp:Label id="lblADNMacho" runat="server" cssclass="titulo">An�lisis ADN(Macho):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbADNMacho" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 19px" align="right">
																			<asp:Label id="lblADNHembra" runat="server" cssclass="titulo">An�lisis ADN (Hembra):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 19px">
																			<cc1:combobox class="combo" id="cmbADNHembra" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 15px" align="right">
																			<asp:Label id="lblADNCria" runat="server" cssclass="titulo">An�lisis ADN (Cr�a):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 15px">
																			<cc1:combobox class="combo" id="cmbADNCria" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px" align="right">
																			<asp:Label id="lblADNCriaTE" runat="server" cssclass="titulo">An�lisis ADN(Cr�a TE):</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbADNCriaTE" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel>
															<asp:panel id="panDocumentos" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaDocumentos" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="center" colSpan="3">
																			<asp:datagrid id="grdDocum" runat="server" BorderStyle="None" BorderWidth="1px" Visible="True"
																				width="100%" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="DataGridDocum_Page" OnEditCommand="mEditarDatosDocum" AutoGenerateColumns="False">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="razd_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="razd_path" HeaderText="Documento"></asp:BoundColumn>
																					<asp:BoundColumn DataField="razd_refe" HeaderText="Referencia"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_razd_imag" HeaderText="Imagen"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 14px" vAlign="top" align="right">
																			<asp:Label id="lblFoto" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px" colSpan="2" height="14">
																			<CC1:TEXTBOXTAB id="txtFoto" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG id="imgDelFoto" style="CURSOR: hand" onclick="javascript:mLimpiar('txtFoto','btnFotoVer');"
																				alt="Limpiar Foto" src="imagenes\del.gif" runat="server"><BUTTON class="boton" id="btnFotoVer" style="WIDTH: 90px" type="button" runat="server" value="Ver Foto">Ver 
																				Foto</BUTTON><BR>
																			<INPUT id="filDocumDoc" style="WIDTH: 340px; HEIGHT: 22px" type="file" size="49" name="File1"
																				runat="server"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDocumRefer" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtDocumRefer" runat="server" cssclass="cuadrotexto" Width="376px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblEsImg" runat="server" cssclass="titulo">Es Im�gen:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<asp:checkbox id="chkEsImg" CssClass="titulo" Runat="server" Text=""></asp:checkbox></TD>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:Label id="lblBajaDocum" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="3">
																			<asp:Button id="btnAltaDocum" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnAutorId" runat="server"></asp:textbox><asp:textbox id="hdnDocumId" runat="server"></asp:textbox></DIV>
			<div style="DISPLAY: none"><asp:checkbox id="hdnGenealo" runat="server"></asp:checkbox><asp:checkbox id="hdnReqTipi" runat="server"></asp:checkbox><asp:checkbox id="hdnReqADN" runat="server"></asp:checkbox></div>
		</form>
		<SCRIPT language="javascript">
		function habilitarMuestras()
	   {
			document.all("txtMuestraCriasPorc").disabled = !(document.all("cmbMuestraCrias").value == 1);
			document.all("txtMuestraCriasMin").disabled = !(document.all("cmbMuestraCrias").value == 1);
						
			document.all("txtMuestraCriasPorc").value = "";
			document.all("txtMuestraCriasMin").value = "";
		}	
	   function mLimpiar(pCont,pBot)
		{
			document.all(pCont).value = '';
			document.all(pBot).style.visibility="hidden";;
		}
	   function SetCmbCtrlReg()
	   {
				if (document.all("cmbRegPrep").value == 0)
					document.all("cmbCtrlReg").value = 0;
		}
		function mCargarRaza()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecieFil").value != '')
				sFiltro = document.all("cmbEspecieFil").value;
				LoadComboXML("razas_cargar", sFiltro, "cmbRazaFil", "S");
	    }
	    function setCmbEspecie()
	    {
			if (document.all("cmbRazaFil").value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all("cmbRazaFil").value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all("cmbEspecieFil").value = vstrRet[0];
			}		
	    }
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
