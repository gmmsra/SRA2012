Namespace SRA

    Partial Class SociFirmantes
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents rbtnCarr As System.Web.UI.WebControls.RadioButton
        Protected WithEvents rbtnMate As System.Web.UI.WebControls.RadioButton


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_Firmantes

        Private mstrConn As String
        Private mstrSociId As String
        Private mstrParaPageSize As Integer
        Private mstrPath As String

#End Region

#Region "Inicializaci�n de Variables"
        Private Sub mInicializar()
            Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

            mstrSociId = Request.QueryString("soci_id")
        End Sub

#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mstrPath = clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_foto_path")
                mInicializar()
                mSetearFirmante(False)
                If (Not Page.IsPostBack) Then
                    mSetearEventos()
                    mSetearMaxLength()
                    mSetearEditor("", True)

                    mCargarCombos()
                    mConsultar()

                    clsWeb.gInicializarControles(Me, mstrConn)
                End If

                ScriptManager.RegisterStartupScript(Page, Page.GetType, "", "attachEventsJs();", True)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        End Sub

        Private Sub mSetearFirmante(ByVal pbooLimpiar As Boolean)
            If Not pbooLimpiar Then
                If Request.Form(txtNyap.ClientID) <> "" Then
                    txtNyap.Text = Request.Form(txtNyap.UniqueID)
                    txtDocuNume.Text = Request.Form(txtDocuNume.UniqueID)
                    cmbDoti.Valor = Request.Form(cmbDoti.UniqueID)
                    usrSoci.Limpiar()
                ElseIf Request.Form(usrSoci.txtIdExt.UniqueID) <> "" Then
                    usrSoci.Valor = Request.Form(usrSoci.txtIdExt.UniqueID)
                    txtNyap.Text = ""
                    txtDocuNume.Text = ""
                    cmbDoti.Limpiar()
                End If
            End If

            If rbtnFirm.Checked Then
                clsWeb.gActivarControl(txtNyap, False)
                clsWeb.gActivarControl(txtDocuNume, False)
                clsWeb.gActivarFileControl(filFirma, False)
                clsWeb.gActivarFileControl(filFoto, False)
                cmbDoti.Enabled = False
                clsWeb.gActivarControl(usrSoci.txtCodiExt, True)
                clsWeb.gActivarControl(usrSoci.txtApelExt, True)
                imgDelFirm.Disabled = True
                imgDelFoto.Disabled = True
                txtNyap.Text = ""
                txtDocuNume.Text = ""
                txtFoto.Text = ""
                txtFirma.Text = ""
                cmbDoti.Limpiar()
                usrSoci.Activo = True
                clsWeb.gActivarControl(txtFirm_mail, False)
                clsWeb.gActivarControl(txtFirm_celular_pais, False)
                clsWeb.gActivarControl(txtFirm_celular_area, False)
                clsWeb.gActivarControl(txtFirm_celular_numero, False)
            Else
                clsWeb.gActivarControl(txtNyap, True)
                clsWeb.gActivarControl(txtDocuNume, True)
                clsWeb.gActivarFileControl(filFirma, True)
                clsWeb.gActivarFileControl(filFoto, True)
                cmbDoti.Enabled = True
                clsWeb.gActivarControl(usrSoci.txtCodiExt, False)
                clsWeb.gActivarControl(usrSoci.txtApelExt, False)
                imgDelFirm.Disabled = False
                imgDelFoto.Disabled = False
                usrSoci.Limpiar()
                usrSoci.Activo = False
                clsWeb.gActivarControl(txtFirm_mail, True)
                clsWeb.gActivarControl(txtFirm_celular_pais, True)
                clsWeb.gActivarControl(txtFirm_celular_area, True)
                clsWeb.gActivarControl(txtFirm_celular_numero, True)
            End If
        End Sub

        Private Sub mSetearMaxLength()
            Dim lstrLong As Object

            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
            txtNyap.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "firm_nyap")
            txtCargo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "firm_cargo")
        End Sub

        Private Sub mCargarCombos()
            clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDoti, "S")
        End Sub
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
        End Sub

        Public Sub mCargarDatos(ByVal pstrId As String)

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mLimpiar()

            With ldsEsta.Tables(0).Rows(0)
                hdnId.Text = .Item("firm_id").ToString()

                If Not .Item("firm_firm_soci_id") Is DBNull.Value Then
                    usrSoci.Valor = .Item("firm_firm_soci_id")
                    txtIngreFecha.Text = "Fecha Ingreso: " & .Item("_soci_ingr_fecha")
                    txtFoto.Valor = .Item("firm_foto")
                    txtFirma.Valor = .Item("firm_firma")
                    imgFoto.ImageUrl = "fotos/" & txtFoto.Valor
                    imgFirma.ImageUrl = "fotos/" & txtFirma.Valor
                    rbtnFirm.Checked = True
                    rbtnApe.Checked = False
                Else
                    txtNyap.Valor = .Item("firm_nyap")
                    txtIngreFecha.Text = ""
                    cmbDoti.Valor = .Item("firm_doti_id")
                    txtDocuNume.Valor = .Item("firm_docu_nume")
                    txtFoto.Valor = IIf(.Item("firm_foto").ToString.Trim.Length > 0, .Item("firm_foto"), String.Empty)
                    txtFirma.Valor = IIf(.Item("firm_firma").ToString.Trim.Length > 0, .Item("firm_firma"), String.Empty)
                    imgFoto.ImageUrl = "fotos/" & txtFoto.Valor
                    imgFirma.ImageUrl = "fotos/" & txtFirma.Valor
                    rbtnFirm.Checked = False
                    rbtnApe.Checked = True
                End If

                txtCargo.Valor = .Item("firm_cargo")
                txtVencFecha.Fecha = IIf(.Item("firm_venc_fecha").ToString.Trim.Length > 0, .Item("firm_venc_fecha"), String.Empty)
                txtInicFecha.Fecha = IIf(.Item("firm_inic_fecha").ToString.Trim.Length > 0, .Item("firm_inic_fecha"), String.Empty)
                txtFirm_mail.Valor = IIf(.Item("firm_mail").ToString.Trim.Length > 0, .Item("firm_mail"), String.Empty)
                txtFirm_celular_pais.Valor = IIf(.Item("firm_celular_pais").ToString.Trim.Length > 0, .Item("firm_celular_pais"), String.Empty)
                txtFirm_celular_area.Valor = IIf(.Item("firm_celular_area").ToString.Trim.Length > 0, .Item("firm_celular_area"), String.Empty)
                txtFirm_celular_numero.Valor = IIf(.Item("firm_celular_numero").ToString.Trim.Length > 0, .Item("firm_celular_numero"), String.Empty)

                mSetearFirmante(True)
            End With

            mSetearEditor("", False)
        End Sub

        Private Sub mLimpiar()
            hdnId.Text = ""
            lblTitu.Text = ""
            txtNyap.Text = ""
            txtIngreFecha.Text = ""
            txtCargo.Text = ""
            txtVencFecha.Text = ""
            txtInicFecha.Text = ""
            usrSoci.Limpiar()
            cmbDoti.Limpiar()
            txtDocuNume.Text = ""
            rbtnFirm.Checked = True
            rbtnApe.Checked = False
            mSetearFirmante(True)
            imgFirma.ImageUrl = "images/none.jpg"
            imgFoto.ImageUrl = "images/none.jpg"
            mSetearEditor("", True)
            txtFirm_mail.Text = String.Empty
            txtFirm_celular_pais.Text = String.Empty
            txtFirm_celular_area.Text = String.Empty
            txtFirm_celular_numero.Text = String.Empty
        End Sub

        Private Sub mCerrar()
            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)
        End Sub
#End Region

#Region "Opciones de ABM"
        Private Sub mAlta()
            Try
                Dim ldsEstruc As DataSet = mGuardarDatos()

                Dim lobjGenerico As New SRA_Neg.Firmantes(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc, filFoto, filFirma, Server, txtFoto, txtFirma)
                lobjGenerico.Alta()

                mConsultar()

                mLimpiar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mModi()
            Try
                Dim ldsEstruc As DataSet = mGuardarDatos()

                Dim lobjGenerico As New SRA_Neg.Firmantes(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc, filFoto, filFirma, Server, txtFoto, txtFirma)
                lobjGenerico.Modi()

                mConsultar()

                mLimpiar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mBaja()
            Try
                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
                lobjGenerico.Baja(hdnId.Text)

                mConsultar()

                mLimpiar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Function mGuardarDatos() As DataSet

            mValidarDatos()

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

            mGuardarArchivos(hdnId.Text, mstrPath)

            With ldsEsta.Tables(0).Rows(0)
                .Item("firm_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("firm_nyap") = txtNyap.Valor
                .Item("firm_firm_soci_id") = IIf(usrSoci.Valor.ToString.Length > 0, usrSoci.Valor, DBNull.Value)
                .Item("firm_cargo") = txtCargo.Valor
                .Item("firm_venc_fecha") = IIf(txtVencFecha.Fecha.Trim.Length > 0, txtVencFecha.Fecha, DBNull.Value)
                .Item("firm_inic_fecha") = IIf(txtInicFecha.Fecha.Trim.Length > 0, txtInicFecha.Fecha, DBNull.Value)
                .Item("firm_soci_id") = IIf(mstrSociId, mstrSociId, DBNull.Value)
                .Item("firm_doti_id") = IIf(cmbDoti.Valor.Trim.Length > 0, cmbDoti.Valor, DBNull.Value)
                .Item("firm_docu_nume") = IIf(txtDocuNume.Valor.Trim.Length > 0, txtDocuNume.Valor, DBNull.Value)

                If txtFoto.Text <> "" Then
                    .Item("firm_foto") = txtFoto.Text
                    .Item("firm_foto") = .Item("firm_foto").Substring(.Item("firm_foto").LastIndexOf("\") + 1)
                Else
                    .Item("firm_foto") = filFoto.Value
                    .Item("firm_foto") = .Item("firm_foto").Substring(.Item("firm_foto").LastIndexOf("\") + 1)
                    If filFoto.Value <> "" And hdnId.Text <> "" Then
                        .Item("firm_foto") = hdnId.Text & "_" & mstrSociId & "_" & .Item("firm_foto")
                    End If
                End If

                If txtFirma.Text <> "" Then
                    .Item("firm_firma") = txtFirma.Text
                    .Item("firm_firma") = .Item("firm_firma").Substring(.Item("firm_firma").LastIndexOf("\") + 1)
                Else
                    .Item("firm_firma") = filFirma.Value
                    .Item("firm_firma") = .Item("firm_firma").Substring(.Item("firm_firma").LastIndexOf("\") + 1)
                    If filFirma.Value <> "" And hdnId.Text <> "" Then
                        .Item("firm_firma") = hdnId.Text & "_" & mstrSociId & "_" & .Item("firm_firma")
                    End If
                End If

                .Item("firm_mail") = IIf(txtFirm_mail.Valor.Trim.Length > 0, txtFirm_mail.Valor, DBNull.Value)
                .Item("firm_celular_pais") = IIf(txtFirm_celular_pais.Valor.Trim.Length > 0, txtFirm_celular_pais.Valor, DBNull.Value)
                .Item("firm_celular_area") = IIf(txtFirm_celular_area.Valor.Trim.Length > 0, txtFirm_celular_area.Valor, DBNull.Value)
                .Item("firm_celular_numero") = IIf(txtFirm_celular_numero.Valor.Trim.Length > 0, txtFirm_celular_numero.Valor, DBNull.Value)
            End With

            Return ldsEsta
        End Function

        Private Sub mValidarDatos()

            If txtFirm_mail.Valor.Trim.Length > 0 Then
                If EmailValidator.IsValidEmail(txtFirm_mail.Valor) = False Then
                    Throw New AccesoBD.clsErrNeg("Mail incorrecto")
                End If
            End If

            If txtFirm_celular_pais.Valor.Trim.Length > 0 Then
                If Not IsNumeric(txtFirm_celular_pais.Valor.Trim) Then
                    Throw New AccesoBD.clsErrNeg("C�digo area n�mero de celular debe ser num�rico.")
                End If
            End If

            If txtFirm_celular_area.Valor.Trim.Length > 0 Then
                If Not IsNumeric(txtFirm_celular_area.Valor.Trim) Then
                    Throw New AccesoBD.clsErrNeg("C�digo area n�mero de celular debe ser num�rico.")
                End If
                If New Regex("^[1-9][0-9]*$").IsMatch(txtFirm_celular_area.Valor.Trim) = False Then
                    Throw New AccesoBD.clsErrNeg("C�digo area n�mero de celular incorrecto.")
                End If
            End If

            If txtFirm_celular_numero.Valor.Trim.Length > 0 Then
                If Not IsNumeric(txtFirm_celular_numero.Valor.Trim) Then
                    Throw New AccesoBD.clsErrNeg("N�mero de celular debe ser num�rico.")
                End If
                If Regex.IsMatch(txtFirm_celular_numero.Valor.Trim, "^(?!15).*$") = False Then
                    Throw New AccesoBD.clsErrNeg("N�mero de celular incorrecto.")
                End If
            End If

            clsWeb.gInicializarControles(Me, mstrConn)

            clsWeb.gValidarControles(Me)

            If rbtnFirm.Checked Then
                If usrSoci.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el socio.")
                End If
            Else
                If txtNyap.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el nombre del firmante.")
                End If
            End If

        End Sub

        Private Class EmailValidator
            Public Shared Function IsValidEmail(email As String) As Boolean
                Dim emailPattern As String = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$"
                Dim emailExpression As New Regex(emailPattern, RegexOptions.IgnoreCase)
                Return emailExpression.IsMatch(email)
            End Function
        End Class

#End Region

#Region "Operacion Sobre la Grilla"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.CurrentPageIndex = E.NewPageIndex
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Try
                mCargarDatos(e.Item.Cells(1).Text)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

        Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub

        Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub

        Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub

        Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
        End Sub

        Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub

        Public Sub mConsultar()
            Try
                Dim lstrCmd As New StringBuilder
                lstrCmd.Append("exec " + mstrTabla + "_consul")
                lstrCmd.Append(" @firm_soci_id=" + mstrSociId)

                clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mGuardarArchivos(ByVal lstrClieId As String, ByVal lstrPath As String)
            Dim lstrNombre As String
            Dim lstrCarpeta As String

            lstrCarpeta = lstrPath
            If Not (filFoto.PostedFile Is Nothing) AndAlso filFoto.PostedFile.FileName <> "" Then
                lstrNombre = filFoto.PostedFile.FileName
                lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
                lstrNombre = lstrClieId & "_" & lstrNombre
                filFoto.PostedFile.SaveAs(Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
                txtFoto.Text = lstrNombre
            End If

            If Not (filFirma.PostedFile Is Nothing) AndAlso filFirma.PostedFile.FileName <> "" Then
                lstrNombre = filFirma.PostedFile.FileName
                lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
                lstrNombre = lstrClieId & "_" & lstrNombre
                filFirma.PostedFile.SaveAs(Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
                txtFirma.Text = lstrNombre
            End If

        End Sub

    End Class

End Namespace
