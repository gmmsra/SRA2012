<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CtaCteACta_Pop" CodeFile="CtaCteACta_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Saldo a cuenta en Cta. Cte.</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR height="100%">
					<TD vAlign="top" colspan="6"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px"
							PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
							BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								
								<asp:BoundColumn Visible="False" DataField="sact_id"></asp:BoundColumn>
								<asp:BoundColumn DataField="numero" HeaderText="Comprobante"></asp:BoundColumn>
								<asp:BoundColumn DataField="acti_desc" HeaderText="Actividad"></asp:BoundColumn>
					   		    <asp:BoundColumn DataField="sact_impo" HeaderText="Importe Ori."></asp:BoundColumn>
								<asp:BoundColumn DataField="saldo" HeaderText="Saldo"></asp:BoundColumn>
								
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="left" colspan =3>
						<asp:Label id="lblTotal" runat="server" cssclass="titulo">Total:</asp:Label>&nbsp;
					 <CC1:NUMBERBOX id="txtTotal" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="65px"></CC1:NUMBERBOX>
				</TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="right" height="40" colspan="4">
						<asp:button id="btnAceptar" Width="80px" cssclass="boton" runat="server" Text="Aceptar"></asp:button></TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
	function chkSelCheck(pChk)
	{
		var dTotal=0;
		var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
		var lbooChecked = pChk.checked;
		var oText = document.all(pChk.id.replace("chkSel","txtPagado"));
		var sImporte;
		
		ActivarControl(oText, lbooChecked);
		
		if(!lbooChecked)
			oText.value = "0";
		else
		{
			dTotal = eval(document.all("grdConsulta__ctl" + lintFil.toString() + "_divTotal").innerText);
			dTotal = dTotal.toString();
			oText.value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2, dTotal.length );
		}
		
		mCalcularTotales();
	}
	
	function txtPagado_change(pTxt)
	{	
		var sImporte = pTxt.value;
		var iComa;
		
		if(sImporte=="")
			sImporte = "0.00";
			
		sImporte = sImporte.replace(",",".");
		iComa = sImporte.indexOf(".");
		if (iComa==-1)
			sImporte += ".00";
		
		if (iComa==sImporte.length-1)
			sImporte += "00";
			
		if (iComa==sImporte.length-2)
			sImporte += "0";
		pTxt.value = sImporte;

		mCalcularTotales();
	}
	
	function mCalcularTotales()
	{
		var dTotal=0;
		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
					dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value.replace(".",""));
			}
		}
		dTotal = dTotal.toString();

		if (dTotal==0)
			document.all("txtTotalSel").value = "0.00"
		else
			document.all("txtTotalSel").value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2 , dTotal.length );
	}
		</SCRIPT>
	</BODY>
</HTML>
