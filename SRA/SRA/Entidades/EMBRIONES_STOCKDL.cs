using System;
using System.Collections;

namespace DL
{
   [Serializable ]
   public class EMBRIONES_STOCKDL 
    {

        /// <summary>
        /// Summary description for EMBRIONES_STOCKDL
        /// </summary>
		#region Private Variables
		private Int32 _tesk_id;
		private Int32 _tesk_tede_id;
		private Int32 _tesk_cria_id;
		private Int32 _tesk_clie_id;
		private Int32 _tesk_cant;
		private DateTime _tesk_fecha;
		private Boolean _tesk_auto;
		private Int32 _tesk_madr_prdt_id;
		private Int32 _tesk_padr_prdt_id;
		private Int32 _tesk_tipo_movi;
		private DateTime _tesk_audi_fecha;
		private Int32 _tesk_audi_user;
		private object _tesk_tmsp;
		private DateTime _tesk_baja_fecha;
		private Int32 _tesk_tram_id;
		private Int32 _tesk_cria_cant;

		#endregion

		#region Public Properties
		public Int32 tesk_id
		{
			get
			{
				return _tesk_id;
			}
			set
			{
				_tesk_id = value;
			}
		}
		public Int32 tesk_tede_id
		{
			get
			{
				return _tesk_tede_id;
			}
			set
			{
				_tesk_tede_id = value;
			}
		}
		public Int32 tesk_cria_id
		{
			get
			{
				return _tesk_cria_id;
			}
			set
			{
				_tesk_cria_id = value;
			}
		}
		public Int32 tesk_clie_id
		{
			get
			{
				return _tesk_clie_id;
			}
			set
			{
				_tesk_clie_id = value;
			}
		}
		public Int32 tesk_cant
		{
			get
			{
				return _tesk_cant;
			}
			set
			{
				_tesk_cant = value;
			}
		}
		public DateTime tesk_fecha
		{
			get
			{
				return _tesk_fecha;
			}
			set
			{
				_tesk_fecha = value;
			}
		}
		public Boolean tesk_auto
		{
			get
			{
				return _tesk_auto;
			}
			set
			{
				_tesk_auto = value;
			}
		}
		public Int32 tesk_madr_prdt_id
		{
			get
			{
				return _tesk_madr_prdt_id;
			}
			set
			{
				_tesk_madr_prdt_id = value;
			}
		}
		public Int32 tesk_padr_prdt_id
		{
			get
			{
				return _tesk_padr_prdt_id;
			}
			set
			{
				_tesk_padr_prdt_id = value;
			}
		}
		public Int32 tesk_tipo_movi
		{
			get
			{
				return _tesk_tipo_movi;
			}
			set
			{
				_tesk_tipo_movi = value;
			}
		}
		public DateTime tesk_audi_fecha
		{
			get
			{
				return _tesk_audi_fecha;
			}
			set
			{
				_tesk_audi_fecha = value;
			}
		}
		public Int32 tesk_audi_user
		{
			get
			{
				return _tesk_audi_user;
			}
			set
			{
				_tesk_audi_user = value;
			}
		}
		public object tesk_tmsp
		{
			get
			{
				return _tesk_tmsp;
			}
			set
			{
				_tesk_tmsp = value;
			}
		}
		public DateTime tesk_baja_fecha
		{
			get
			{
				return _tesk_baja_fecha;
			}
			set
			{
				_tesk_baja_fecha = value;
			}
		}
		public Int32 tesk_tram_id
		{
			get
			{
				return _tesk_tram_id;
			}
			set
			{
				_tesk_tram_id = value;
			}
		}
		public Int32 tesk_cria_cant
		{
			get
			{
				return _tesk_cria_cant;
			}
			set
			{
				_tesk_cria_cant = value;
			}
		}

        #endregion

	}
}
