<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Temporarios" CodeFile="Temporarios.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Productos Temporarios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
			function expandir()
			{
				try{ parent.frames("menu").CambiarExp();}catch(e){;}
			}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Productos Temporarios</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
											Visible="True" Width="100%">
											<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 50px">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<!--<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbRazaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="250px"
																					MostrarBotones="False" NomOper="razas_cargar" filtra="true" AceptaNull="false"></cc1:combobox></TD>
																		</TR>-->
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblSexoFil" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbSexoFil" class="combo" runat="server" Width="113px" AceptaNull="true">
																					<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																					<asp:ListItem Value="0">Hembra</asp:ListItem>
																					<asp:ListItem Value="1">Macho</asp:ListItem>
																				</cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblNombFil" runat="server" cssclass="titulo">Nombre:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblApodFil" runat="server" cssclass="titulo">Apodo:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtApodFil" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblRPNumeFil" runat="server" cssclass="titulo">N� RP:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtRPNumeFil" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="9999999999999"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrCriadorFil" runat="server" AceptaNull="false" FilTarjNume="false" MuestraDesc="False"
																					Tabla="Criadores" Criador="True" Saltos="1,1,1" Ancho="780" Alto="560"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblProductoFil" runat="server" cssclass="titulo">Producto Relacionado:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<UC1:PROD id="usrProductoFil" runat="server" AceptaNull="false" MuestraDesc="False" Tabla="productos"
																					Saltos="1,2" Ancho="800"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblEstadoFil" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbEstadoFil" class="combo" runat="server" Width="208px" NomOper="estados_cargar"
																					AceptaNull="false"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" AllowPaging="True" HorizontalAlign="Center"
											CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" AutoGenerateColumns="False"
											BorderStyle="None" BorderWidth="1px">
											<FooterStyle CssClass="footer"></FooterStyle>
											<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
											<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="1%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="ptem_id" ReadOnly="True" HeaderText="id"></asp:BoundColumn>
												<asp:BoundColumn DataField="_raza" HeaderText="Raza">
													<HeaderStyle Width="30%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_sexo" HeaderText="Sexo"></asp:BoundColumn>
												<asp:BoundColumn DataField="ptem_nombre" HeaderText="Nombre">
													<HeaderStyle Width="30%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_cria_nume" HeaderText="N�mero"></asp:BoundColumn>
												<asp:BoundColumn DataField="_criador" HeaderText="Criador">
													<HeaderStyle Width="40%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ptem_rp" HeaderText="RP"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD vAlign="middle" colSpan="3"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ToolTip="Agregar un Nuevo Producto Temporal"
											ImageDisable="btnNuev0.gif" ForeColor="Transparent" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif"
											OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
								<TR>
									<TD align="center" colSpan="3">
										<asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Height="116px" Visible="False">
											<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
												align="left">
												<TR>
													<TD vAlign="top" colSpan="2" align="right">
														<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="250px" MostrarBotones="False"
															NomOper="razas_cargar" filtra="true" AceptaNull="True" Height="20px" Obligatorio="true"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbSexo" class="combo" runat="server" Width="113px" AceptaNull="true" Obligatorio="True">
															<asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem>
															<asp:ListItem Value="0">Hembra</asp:ListItem>
															<asp:ListItem Value="1">Macho</asp:ListItem>
														</cc1:combobox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" vAlign="top" background="imagenes/formfdofields.jpg"
														align="right">
														<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<UC1:CLIE id="usrCria" runat="server" FilTarjNume="false" Tabla="Criadores" Criador="True"
															Saltos="1,1,1" Ancho="780" Alto="560" FilClie="True" ColCriaNume="True"></UC1:CLIE></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRPNume" runat="server" cssclass="titulo">N� RP:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtRPNume" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="9999999999999"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblApod" runat="server" cssclass="titulo">Apodo:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtApod" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblEstado" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbEstado" class="combo" runat="server" Width="208px" NomOper="estados_cargar"
															AceptaNull="true"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblProducto" runat="server" cssclass="titulo">Producto Relacionado:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<UC1:PROD id="usrProducto" runat="server" AceptaNull="true" MuestraDesc="False" Tabla="productos"
															Saltos="1,2" Ancho="790" AutoPostBack="False"></UC1:PROD></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblFechaEnganche" runat="server" cssclass="titulo">Fecha de Relaci�n:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:DateBox id="txtFechaEnganche" runat="server" cssclass="cuadrotexto" Width="68px" enabled="false"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2">
														<asp:Label id="lblPadre" runat="server" cssclass="titulo" ForeColor="RoyalBlue" Font-Italic="True">&nbsp;&nbsp;Datos del Padre</asp:Label></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblPadreRP" runat="server" cssclass="titulo">N� RP:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtPadreRP" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="9999999999999"></cc1:numberbox>&nbsp;&nbsp;&nbsp;
														<asp:Label id="lblPadreSRA" runat="server" cssclass="titulo">N� SRA:</asp:Label>&nbsp;
														<cc1:numberbox id="txtPadreSRA" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="9999999999999"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblPadreNombre" runat="server" cssclass="titulo">Nombre:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtPadreNombre" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2">
														<asp:Label id="lblMadre" runat="server" cssclass="titulo" ForeColor="RoyalBlue" Font-Italic="True">&nbsp;&nbsp;Datos de la Madre</asp:Label></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblMadreRP" runat="server" cssclass="titulo">N� RP:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtMadreRP" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="9999999999999"></cc1:numberbox>&nbsp;&nbsp;&nbsp;
														<asp:Label id="lblMadreSRA" runat="server" cssclass="titulo">N� SRA:</asp:Label>&nbsp;
														<cc1:numberbox id="txtMadreSRA" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="9999999999999"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblMadreNombre" runat="server" cssclass="titulo">Nombre:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtMadreNombre" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:textboxtab id="txtObse" runat="server" cssclass="textolibre" Width="100%" height="100px" Rows="10"
															TextMode="MultiLine"></cc1:textboxtab></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblInscFecha" runat="server" cssclass="titulo">Fecha Inscripci�n:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:DateBox id="txtInscFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
														<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;
														<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 20px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblFalleFecha" runat="server" cssclass="titulo">Fecha Baja:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 20px" background="imagenes/formfdofields.jpg">
														<cc1:DateBox id="txtFalleFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
														<asp:Label id="lblFalleCodigo" runat="server" cssclass="titulo">Con C�digo:</asp:Label>&nbsp;
														<cc1:numberbox id="txtFalleCodigo" runat="server" cssclass="cuadrotexto" Width="80px"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR height="30">
													<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
			</DIV>
		</form>
	</BODY>
</HTML>
