Imports System.Data.SqlClient


Namespace SRA


Partial Class MailMode
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    Protected WithEvents hdnValorId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents hdnModi As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_MailsModelos
   Private mstrTablaCate As String = SRA_Neg.Constantes.gTab_MailsModeCate
   Private mstrTablaActi As String = SRA_Neg.Constantes.gTab_MailsModeActi
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "instituciones2", cmbInst, "T")
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "mamo_desc")
      txtAsun.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "mamo_asun")
   End Sub

#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      grdDeta.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.EditItemIndex = -1
         If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
            grdDeta.CurrentPageIndex = 0
         Else
            grdDeta.CurrentPageIndex = E.NewPageIndex
         End If
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaCate)
         grdDeta.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
      btnBajaDeta.Enabled = Not (pbooAlta)
      btnModiDeta.Enabled = Not (pbooAlta)
      btnAltaDeta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtDesc.Valor = .Item("mamo_desc")
            txtAsun.Valor = .Item("mamo_asun")
            If Not .IsNull("mamo_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("mamo_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub
   Public Sub mEditarDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDire As DataRow

         hdnDetaId.Text = E.Item.Cells(1).Text
         ldrDire = mdsDatos.Tables(mstrTablaCate).Select("mmca_id=" & hdnDetaId.Text)(0)

         With ldrDire
            cmbCate.Valor = .Item("mmca_cate_id")
            cmbInst.Valor = .Item("mmca_inst_id")
            cmbActi.Valor = .Item("mmca_acti_id")
            txtCateMens.Text = .Item("mmca_mens")
         End With
         mSetearEditorDeta(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""

      txtAsun.Text = ""
      txtDesc.Text = ""

      lblBaja.Text = ""

      mLimpiarDeta()

      grdDato.CurrentPageIndex = 0
      grdDeta.CurrentPageIndex = 0

      mCrearDataSet("")
      mShowTabs(1)
      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarDeta()
      hdnDetaId.Text = ""
      cmbActi.Limpiar()
      cmbCate.Limpiar()
      cmbInst.Limpiar()
      txtCateMens.Text = ""
      mSetearEditorDeta(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaCate
      'mdsDatos.Tables(2).TableName = mstrTablaActi

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      grdDeta.DataSource = mdsDatos.Tables(mstrTablaCate)
      grdDeta.DataBind()

      Session(mstrTabla) = mdsDatos
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If grdDeta.Items.Count = 0 Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar al menos una actividad/categor�a para el modelo de mail.")
      End If
   End Sub
   Private Sub mGuardarDatos()
      mValidaDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("mamo_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("mamo_desc") = txtDesc.Valor
         .Item("mamo_asun") = txtAsun.Valor
         .Item("mamo_baja_fecha") = DBNull.Value
         .Item("mamo_audi_user") = Session("sUserId").ToString()
      End With
   End Sub
   Private Sub mActualizarDeta(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosDeta(pbooAlta)

         mLimpiarDeta()
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaCate)
         grdDeta.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatosDeta(ByVal pbooAlta As Boolean)
      Dim lstrActi As String = cmbActi.SelectedValue.ToString
      Dim lstrCate As String = cmbCate.SelectedValue.ToString
      Dim lstrInst As String = cmbInst.SelectedValue.ToString

      If cmbActi.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la actividad.")
      End If
      If cmbActi.Valor = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la actividad.")
      End If
      'If cmbActi.Valor = 2 And cmbCate.Valor Is DBNull.Value Then
      '   Throw New AccesoBD.clsErrNeg("Debe indicar la categor�a.")
      'End If
      If pbooAlta Then
        If cmbActi.Valor <> 2 And mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi).GetUpperBound(0) > -1 Then
            Throw New AccesoBD.clsErrNeg("La actividad indicada ya se encuentra asociada.")
        End If
        If Not cmbCate.Valor Is DBNull.Value AndAlso cmbCate.Valor <> "" Then
            If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is null").GetUpperBound(0) > -1 Then
               Throw New AccesoBD.clsErrNeg("Ya existe un mensaje gen�rico para la categor�a.")
            End If
                    If cmbInst.Valor.Length = 0 Then
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is not null ").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    Else
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id=" & cmbInst.Valor.ToString).GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("La categor�a/instituci�n ya se encuentran asociadas.")
                        End If
                    End If
            If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id=" & cmbCate.Valor.ToString & " and mmca_inst_id is null").GetUpperBound(0) > -1 Then
               Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
            End If
                    If cmbInst.Valor.Length = 0 Then
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id=" & cmbCate.Valor.ToString & " and mmca_inst_id is not null ").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    Else
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id=" & cmbCate.Valor.ToString & " and mmca_inst_id=" & cmbInst.Valor.ToString).GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("La categor�a/instituci�n ya se encuentran asociadas.")
                        End If
                    End If
         Else
            If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is null").GetUpperBound(0) > -1 Then
               Throw New AccesoBD.clsErrNeg("Ya existe un mensaje gen�rico para la categor�a.")
            End If
            If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is not null").GetUpperBound(0) > -1 Then
               Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para una categor�a.")
            End If
                    If cmbInst.Valor.Length = 0 Then
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is not null ").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    Else
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id=" & cmbInst.Valor.ToString).GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("La categor�a/instituci�n ya se encuentran asociadas.")
                        End If
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is null").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    End If
         End If
        Else
         If cmbActi.Valor <> 2 And mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi).GetUpperBound(0) > -1 Then
            Throw New AccesoBD.clsErrNeg("La actividad indicada ya se encuentra asociada.")
         End If
                If Not cmbCate.Valor.Length = 0 AndAlso cmbCate.Valor <> "" Then
                    If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is null").GetUpperBound(0) > -1 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe un mensaje gen�rico para la categor�a.")
                    End If
                    If cmbInst.Valor.Length = 0 Then
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is not null ").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    Else
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id=" & cmbInst.Valor.ToString).GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("La categor�a/instituci�n ya se encuentran asociadas.")
                        End If
                    End If
                    If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id=" & cmbCate.Valor.ToString & " and mmca_inst_id is null").GetUpperBound(0) > -1 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                    End If
                    If cmbInst.Valor.Length = 0 Then
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id=" & cmbCate.Valor.ToString & " and mmca_inst_id is not null ").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    Else
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id=" & cmbCate.Valor.ToString & " and mmca_inst_id=" & cmbInst.Valor.ToString).GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("La categor�a/instituci�n ya se encuentran asociadas.")
                        End If
                    End If
                Else
                    If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is null").GetUpperBound(0) > -1 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe un mensaje gen�rico para la categor�a.")
                    End If
                    If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is not null").GetUpperBound(0) > -1 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para una categor�a.")
                    End If
                    If cmbInst.Valor.Length = 0 Then
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is not null ").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    Else
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id=" & cmbInst.Valor.ToString).GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("La categor�a/instituci�n ya se encuentran asociadas.")
                        End If
                        If mdsDatos.Tables(mstrTablaCate).Select("mmca_id <> " & hdnDetaId.Text & " and mmca_acti_id=" & lstrActi & " and mmca_cate_id is null and mmca_inst_id is null").GetUpperBound(0) > -1 Then
                            Throw New AccesoBD.clsErrNeg("Ya existe un mensaje para la categor�a/instituci�n asociadas.")
                        End If
                    End If
                End If
        End If
        If txtCateMens.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el mensaje para la categoria.")
        End If
   End Sub
   Private Sub mBajaDeta()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaCate).Select("mmca_id=" & hdnDetaId.Text)(0)
         row.Delete()
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaCate)
         grdDeta.DataBind()
         mLimpiarDeta()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosDeta(ByVal pbooAlta As Boolean)
      Dim ldrProc As DataRow

      mValidaDatosDeta(pbooAlta)

      If hdnDetaId.Text = "" Then
         ldrProc = mdsDatos.Tables(mstrTablaCate).NewRow
         ldrProc.Item("mmca_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaCate), "mmca_id")
      Else
         ldrProc = mdsDatos.Tables(mstrTablaCate).Select("mmca_id=" & hdnDetaId.Text)(0)
         ldrProc.Item("mmca_id") = clsSQLServer.gFormatArg(hdnDetaId.Text, SqlDbType.Int)
      End If

      With ldrProc
         .Item("mmca_mamo_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("mmca_acti_id") = cmbActi.Valor
         If cmbActi.Valor.ToString() <> "" Then
            .Item("_acti_desc") = cmbActi.SelectedItem.Text
         Else
            .Item("_acti_desc") = ""
         End If
         .Item("mmca_cate_id") = cmbCate.Valor
         If cmbCate.Valor.ToString() <> "" Then
            .Item("_cate_desc") = cmbCate.SelectedItem.Text
            .Item("_cate_subc") = clsSQLServer.gCampoValorConsul(mstrConn, "categorias_consul @cate_id=" & cmbCate.Valor.ToString, "cate_subc")
         Else
            .Item("_cate_desc") = ""
            .Item("_cate_subc") = DBNull.Value
         End If
         .Item("mmca_inst_id") = cmbInst.Valor
         If cmbInst.Valor.ToString() <> "" Then
            .Item("_inst_desc") = cmbInst.SelectedItem.Text
         Else
            .Item("_inst_desc") = ""
         End If
         .Item("mmca_mens") = txtCateMens.Valor
         .Item("mmca_audi_user") = Session("sUserId").ToString()
      End With
      If (hdnDetaId.Text = "") Then
         mdsDatos.Tables(mstrTablaCate).Rows.Add(ldrProc)
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panCabecera.Visible = False
         lnkCabecera.Font.Bold = False
         panDeta.Visible = False
         lnkDeta.Font.Bold = False
         Select Case Tab
            Case 1
               panCabecera.Visible = True
               lnkCabecera.Font.Bold = True
            Case 2
               panDeta.Visible = True
               lnkDeta.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
      mShowTabs(2)
   End Sub
   'Botones generales
   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub btnAltaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDeta(True)
   End Sub
   Private Sub btnBajaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
      mBajaDeta()
   End Sub
   Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDeta(False)
   End Sub
   Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub
#End Region

End Class

End Namespace
