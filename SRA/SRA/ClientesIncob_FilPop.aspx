<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ClientesIncob_FilPop" CodeFile="ClientesIncob_FilPop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Filtro - Busqueda de Incobrables</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
		var vVentanas = new Array(null);
	
		function btnBuscar_click()
		{
			var sFiltro;
			var sFiltroTmp; 

			sFiltroTmp = document.all["txtDeudaDde"].value;
		 	if (sFiltroTmp == "") 
				sFiltroTmp = "0";
			sFiltro = "Deuda=" + sFiltroTmp;
			
			sFiltroTmp = document.all["txtVigenciaDde"].value;
			if (sFiltroTmp == "") 
				sFiltroTmp = "0";
			sFiltro += "&Vigencia=" + sFiltroTmp;
			
		    if (document.all["txtVigenciaDde"].value == ''
				&& document.all["txtDeudaDde"].value == ''
		 		&& document.all("txtComiAnio").value == ''
				&& document.all("cmbSoci").value == ''
				&& document.all("txtPeriDesde").value == ''
				&& document.all("txtPeriHasta").value == ''
				&& document.all["txtPeriFil"].value == ''
				&& document.all["txtAnioFil"].value == ''
				&& document.all("cmbCate").value == 'Todos' 
			    && document.all("cmbEsta").value == 'Todos' )
			{
				alert('Debe ingresar al menos un filtro para la consulta.');
				return;
			}

			if(document.all("hdnTipo").value!="C")
			{
				if (!(document.all["cmbPetiFil"].value=="" && document.all["txtAnioFil"].value=="" && document.all["txtPeriFil"].value=="")&& !(document.all["cmbPetiFil"].value!="" && document.all["txtAnioFil"].value!="" && document.all["txtPeriFil"].value!=""))
				{
					alert("Si selecciona un per�odo debe seleccionar el per�odo, el tipo de per�odo y el a�o")
					return;
				}

				sFiltroTmp = document.all["cmbPetiFil"].value;
				if (sFiltroTmp == "") 
					sFiltroTmp = "0";
				sFiltro += "&peti_id=" + sFiltroTmp;
				
				sFiltroTmp = document.all["txtAnioFil"].value;
				if (sFiltroTmp == "") 
					sFiltroTmp = "0";
				sFiltro += "&anio=" + sFiltroTmp;
				
				sFiltroTmp = document.all["txtPeriFil"].value;
				if (sFiltroTmp == "") 
					sFiltroTmp = "0";
				sFiltro += "&peri=" + sFiltroTmp;
			}
			
			sFiltro += "&lsin_id=" + document.all("hdnId").value;
			sFiltro += "&incl=" + document.all("hdnIncl").value;
			sFiltro += "&orden=" 

			if(document.all("rbtVige").checked)
				sFiltro += "V";
			else
		        sFiltro += "D";
		    
		    sFiltro += "&etapa=" + document.all("hdnEtapa").value;
		    sFiltro += "&tipo=" + document.all("hdnTipo").value;
			sFiltro += "&acti=" + document.all("cmbActi").value;
			sFiltro += "&soci_lsin_id=" + document.all("cmbSoci").value;
			sFiltro += "&comi_anio=" + document.all("txtComiAnio").value;
			sFiltro += "&peri_desde=" + document.all("txtPeriDesde").value;
			sFiltro += "&peri_hasta=" + document.all("txtPeriHasta").value;
			sFiltro += "&cate=" + document.all("cmbCate").value;
			sFiltro += "&esta=" + document.all("cmbEsta").value;
		      
			gAbrirVentanas("ClientesIncob_Pop.aspx?" + sFiltro, 0, "700","550","50");
		}

		function btnCerrar_click()
		{
		
			gCerrarVentanas();
		    window.close();
		}
		
		if (document.all["txtVigenciaDde"]!= null)
			document.all["txtVigenciaDde"].focus();
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onload="body_load();">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%" border="0">
				<TR>
					<TD align="left" colSpan="2" height="30">
						<asp:Label id="lblTitu" runat="server" cssclass="titulo" Font-Size="Small">Clientes Incobrables</asp:Label></TD>
				</TR>
				<TR>
					<TD align="left" colSpan="2" height="10"></TD>
				</TR>
				<TR>
					<TD align="right" style="HEIGHT: 5px">
						<asp:Label id="lblVigenciaDde" runat="server" cssclass="titulo">Vigencia de deuda mayor a:</asp:Label>&nbsp;
					</TD>
					<TD noWrap style="HEIGHT: 5px">
						<CC1:NUMBERBOX id="txtVigenciaDde" runat="server" cssclass="cuadrotexto" Width="35px"></CC1:NUMBERBOX>&nbsp;
						<asp:Label id="Label2" runat="server" cssclass="titulo">a�os</asp:Label></TD>
				</TR>
				<TR runat="server" id="trPerio">
					<TD align="right" style="HEIGHT: 9px">
						<asp:Label id="lblPeriFil" runat="server" cssclass="titulo">Per�odo con Deuda:</asp:Label>&nbsp;</TD>
					<TD nowrap style="HEIGHT: 9px">
						<cc1:numberbox id="txtPeriFil" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
							CantMax="2" MaxValor="12" MaxLength="2"></cc1:numberbox>
						<cc1:combobox class="combo" id="cmbPetiFil" runat="server" Width="114px"></cc1:combobox>&nbsp;&nbsp;
						<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">/</asp:Label>&nbsp;
						<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="50px" AceptaNull="False"
							CantMax="4" MaxValor="2090" MaxLength="4"></cc1:numberbox></TD>
				</TR>
				<TR>
					<TD align="right" style="HEIGHT: 7px">
						<asp:Label id="lblDeudaDde" runat="server" cssclass="titulo">Deuda mayor a:</asp:Label>&nbsp;
					</TD>
					<TD noWrap style="HEIGHT: 7px">
						<CC1:NUMBERBOX id="txtDeudaDde" runat="server" cssclass="cuadrotexto" Width="104px"></CC1:NUMBERBOX></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 7px" align="right">
						<asp:Label id="lblComiAnio" runat="server" cssclass="titulo">Cuya Deuda Comience en el A�o:</asp:Label>&nbsp;</TD>
					<TD style="HEIGHT: 7px" noWrap>
						<cc1:numberbox id="txtComiAnio" runat="server" cssclass="cuadrotexto" Width="50px" MaxLength="4"
							MaxValor="2090" CantMax="4" AceptaNull="False"></cc1:numberbox></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 7px" align="right">
						<asp:Label id="lblPeri" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;
						<asp:Label id="lblPeriDesde" runat="server" cssclass="titulo">Desde:</asp:Label>&nbsp;</TD>
					<TD style="HEIGHT: 7px" noWrap>
						<cc1:DateBox id="txtPeriDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
						<asp:Label id="lblPeriHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
						<cc1:DateBox id="txtPeriHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 7px" align="right">
						<asp:Label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
					<TD style="HEIGHT: 7px" noWrap>
						<cc1:combobox class="combo" id="cmbActi" runat="server" Width="200px"></cc1:combobox></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 7px" align="right">
						<asp:Label id="lblSoci" runat="server" cssclass="titulo">Socios Eliminados:</asp:Label>&nbsp;</TD>
					<TD style="HEIGHT: 7px" noWrap>
						<cc1:combobox class="combo" id="cmbSoci" runat="server" Width="200px"></cc1:combobox></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 7px" align="right">
						<asp:Label id="Label1" runat="server" cssclass="titulo">Categoria:</asp:Label></TD>
					<TD style="HEIGHT: 7px" noWrap>
						<cc1:combobox class="combo" id="cmbCate" runat="server" Width="200px"></cc1:combobox></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 7px" align="right">
						<asp:Label id="Label4" runat="server" cssclass="titulo">Estado:</asp:Label></TD>
					<TD style="HEIGHT: 7px" noWrap>
						<cc1:combobox class="combo" id="cmbEsta" runat="server" Width="200px"></cc1:combobox></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="right" height="4"></TD>
					<TD vAlign="top" noWrap height="4"></TD>
				</TR>
				<TR>
					<TD valign="top" align="right" style="HEIGHT: 29px">
						<asp:Label id="Label3" runat="server" cssclass="titulo" Width="100%">Ordenar Por:</asp:Label>&nbsp;</TD>
					<TD noWrap valign="top" style="HEIGHT: 29px">
						<asp:RadioButton id="rbtVige" runat="server" cssclass="titulo" GroupName="RadioGroup1" Checked="True"
							Text="Vigencia"></asp:RadioButton><BR>
						<asp:RadioButton id="rbtDeuda" runat="server" cssclass="titulo" GroupName="RadioGroup1" Text="Deuda"></asp:RadioButton>
					</TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="center" colspan="2" height="40">
						<BUTTON class="boton" id="btnBuscar" style="WIDTH: 90px" onclick="btnBuscar_click();" type="button"
							runat="server" value="Detalles">Buscar</BUTTON>&nbsp;&nbsp; <BUTTON class="boton" id="btnCerrar" style="WIDTH: 90px" onclick="btnCerrar_click();" type="button"
							runat="server" value="Detalles">Cerrar</BUTTON>
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnIncl" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnEtapa" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnTipo" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<script>
	function body_load(){
	if (document.all["txtVigenciaDde"]!= null)
		document.all["txtVigenciaDde"].focus();
}
		</script>
	</BODY>
</HTML>
