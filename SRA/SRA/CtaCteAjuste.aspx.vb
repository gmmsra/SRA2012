Namespace SRA

Partial Class CtaCteAjuste
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

   Protected WithEvents lblNroCompFil As System.Web.UI.WebControls.Label
   Protected WithEvents txtNroCompFil As NixorControls.NumberBox
Protected WithEvents lblNumeDesdeFil As System.Web.UI.WebControls.Label
Protected WithEvents lblNumeHastaFil As System.Web.UI.WebControls.Label


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Ajustes
   Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_AjustesConcep

   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mbooHayPend As Boolean
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Id = 2
   End Enum

   Private Enum ColumnasDeta As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing
            mSetearMaxLength()
            mSetearEventos()
                    txtFecha.Fecha = Today
                    mCargarCombos()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
            mCargarCentroCostos(False)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnCerrar.Attributes.Add("onclick", "if(!confirm('Confirma el cierre del registro?')) return false;")
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "ajus_obse")
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmctFil, "")
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConc, "S", "@conc_acti_id=1")
    End Sub

    Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
        Dim lstrCta As String = ""
            If cmbConc.Valor.Length > 0 Then
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbConc.Valor)
                lstrCta = lstrCta.Substring(0, 1)
            End If
        If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
            If Not Request.Form("cmbCcos") Is Nothing Or pbooEdit Then
                clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCcos, "id", "descrip", "N")
                If Not Request.Form("cmbCcos") Is Nothing Then
                    cmbCcos.Valor = Request.Form("cmbCcos").ToString
                End If
            End If
        Else
            cmbCcos.Items.Clear()
        End If
    End Sub

#End Region

#Region "Seteo de Controles"
	Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, ByVal pbooCerrado As Boolean, ByVal pbooBaja As Boolean)
		Select Case pstrTabla
			Case mstrTablaDeta
				btnBajaDeta.Enabled = Not pbooAlta
				btnModiDeta.Enabled = Not pbooAlta
				btnAltaDeta.Enabled = pbooAlta
			Case Else
				If pbooBaja Then
					btnBaja.Enabled = Not pbooCerrado
				Else
					btnBaja.Enabled = pbooCerrado
				End If
				btnModi.Enabled = Not pbooAlta And Not pbooCerrado
				btnCerrar.Enabled = Not pbooAlta And Not pbooCerrado
				btnAlta.Enabled = pbooAlta And Not pbooCerrado

				panCabecera.Enabled = Not pbooCerrado
				panDetalle.Enabled = Not pbooCerrado
		End Select
	End Sub

	Public Function mCargarDatos(ByVal pstrId As String) As ResultadoDoble

		Dim resul As ResultadoDoble = New ResultadoDoble

		mCrearDataSet(pstrId)


		With mdsDatos.Tables(mstrTabla).Rows(0)
			hdnId.Text = .Item("ajus_id").ToString()
			txtFecha.Fecha = .Item("ajus_fecha")
			usrClie.Valor = .Item("ajus_clie_id")
			rbtnDebe.Checked = Not .Item("ajus_dh")
			rbtnHaber.Checked = .Item("ajus_dh")
			txtObse.Valor = .Item("ajus_obse")
			txtNume.Valor = .Item("_comp_nume")

			resul.rsBaja = True

			If Not .IsNull("ajus_apro_fecha") Then
				resul.rsCerrado = True
				lblAprob.Text = "Ajuste aprobado en fecha: " & CDate(.Item("ajus_apro_fecha")).ToString("dd/MM/yyyy HH:mm")
				If Not .IsNull("ajus_baja_fecha") Then
					resul.rsBaja = True
					lblBaja.Text = "Registro dado de baja el: " & CDate(.Item("ajus_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
				Else
					resul.rsBaja = False
					lblBaja.Text = ""
				End If
			Else
					resul.rsCerrado = False
					lblAprob.Text = ""
			End If

		End With

		mLimpiarDeta()
		Return resul

	End Function

	Private Sub mAgregar()
		mLimpiar()
		btnBaja.Enabled = False
		btnModi.Enabled = False
		btnCerrar.Enabled = False
		mMostrarPanel(True)
	End Sub

	Private Sub mLimpiar()
		hdnId.Text = ""
		lblTitu.Text = ""
            txtFecha.Fecha = Today
            txtObse.Text = ""
		txtNume.Valor = ""
		usrClie.Limpiar()
		rbtnDebe.Checked = False
		rbtnHaber.Checked = True
            txtImpo.Valor = "" ' DBNull.Value
		cmbConc.Limpiar()
		cmbCcos.Items.Clear()

		lblBaja.Text = ""
		lblAprob.Text = ""
		mCrearDataSet("")

		mLimpiarDeta()

		mSetearEditor("", True, False, True)

		mShowTabs(1)
	End Sub

	Private Sub mCerrar()
		Try
			mLimpiar()
			mConsultar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		Dim lbooVisiOri As Boolean = panDato.Visible

		panDato.Visible = pbooVisi
		panBotones.Visible = pbooVisi
		panFiltro.Visible = Not panDato.Visible
		btnAgre.Visible = Not panDato.Visible
		btnConf.Visible = mbooHayPend And Not panDato.Visible
		grdDato.Visible = Not panDato.Visible

		tabLinks.Visible = pbooVisi

		If pbooVisi Then
			grdDato.DataSource = Nothing
			grdDato.DataBind()

			mShowTabs(1)
		Else
			Session(mstrTabla) = Nothing
			mdsDatos = Nothing
		End If
	End Sub

	Private Sub mShowTabs(ByVal origen As Byte)
		Try
			panDato.Visible = True
			panBotones.Visible = True
			panDetalle.Visible = False
			panCabecera.Visible = False

			lnkCabecera.Font.Bold = False
			lnkDeta.Font.Bold = False

			Select Case origen
				Case 1
					panCabecera.Visible = True
					lnkCabecera.Font.Bold = True
					lblTitu.Text = "Datos del Ajuste"
				Case 2
					panDetalle.Visible = True
					lnkDeta.Font.Bold = True
					lblTitu.Text = "Conceptos del Ajuste"
					grdDetalle.Visible = True
			End Select


		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
		mShowTabs(1)
	End Sub

	Private Sub lnkDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
		mShowTabs(2)
	End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsDatos = mGuardarDatos(False)

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
         lobjGenericoRel.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi(ByVal pbooCierre As Boolean)
      Try
         Dim ldsTmp As DataSet = mGuardarDatos(pbooCierre)

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
         lobjGenericoRel.Modi()
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
			Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenericoRel.Baja(hdnId.Text)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConfirmarMul()
      Try
         Dim ldsTmp As DataSet
         Dim lobjGenericoRel As SRA_Neg.GenericaRel

         For Each lItem As DataGridItem In grdDato.Items
            If DirectCast(lItem.FindControl("chkSel"), CheckBox).Checked Then
               hdnId.Text = lItem.Cells(Columnas.Id).Text
               mCargarDatos(hdnId.Text)
               ldsTmp = mGuardarDatos(True)
               lobjGenericoRel = New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
               lobjGenericoRel.Modi()
            End If
         Next

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos(ByVal pbooCierre As Boolean) As DataSet
      mValidarDatos()

      Dim ldsDatos As DataSet = mdsDatos.Copy

      With ldsDatos.Tables(mstrTabla).Rows(0)
         .Item("ajus_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("ajus_fecha") = txtFecha.Fecha
         .Item("ajus_clie_id") = usrClie.Valor
         .Item("ajus_dh") = rbtnHaber.Checked
         .Item("ajus_obse") = txtObse.Valor
            .Item("ajus_emct_id") = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
         .Item("ajus_impo") = 0

         For Each lDr As DataRow In ldsDatos.Tables(mstrTablaDeta).Select
            .Item("ajus_impo") += lDr.Item("ajco_impo") * IIf(lDr.Item("ajco_dh"), 1, -1)
         Next

         If .Item("ajus_impo") = 0 Then
            Throw New AccesoBD.clsErrNeg("El importe total debe ser distinto de cero.")
         End If

         If pbooCierre Then
            .Item("ajus_apro_fecha") = Today
            .Item("ajus_apro_usua_id") = Session("sUserId").ToString()

            mAgregarTablaProceso(ldsDatos)
         End If
      End With

      Return (ldsDatos)
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaDeta

      mConsultarDeta()

      Session(mstrTabla) = mdsDatos
   End Sub

   Private Sub mValidarDatos()
        Dim ldecHabe As Decimal
        Dim ldecDebe As Decimal

      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      'For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDeta).Select
      '      ldecHabe += IIf(lDr.Item("ajco_dh"), lDr.Item("ajco_impo"), 0)
      '      ldecDebe += IIf(lDr.Item("ajco_dh"), 0, lDr.Item("ajco_impo"))
      'Next

      'If rbtnHaber.Checked Then
      '    If ldecDebe > ldecHabe Then
      '        Throw New AccesoBD.clsErrNeg("El importe de los conceptos del haber debe superar a los conceptos del debe.")
      '    End If
      'Else
      '    If ldecHabe > ldecDebe Then
      '        Throw New AccesoBD.clsErrNeg("El importe de los conceptos del debe debe superar a los conceptos del haber.")
      '    End If
      'End If
    End Sub

    Private Sub mAgregarTablaProceso(ByVal ldsDatos As DataSet)
        Dim lDr As DataRow

        ldsDatos.Tables.Add(mstrTabla & "_conf")

        With ldsDatos.Tables(ldsDatos.Tables.Count - 1)
            .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_ajus_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_cemi_nume", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_emct_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_host", System.Type.GetType("System.String"))
            lDr = .NewRow
            .Rows.Add(lDr)
        End With

        lDr.Item("proc_id") = -1
        lDr.Item("proc_ajus_id") = ldsDatos.Tables(mstrTabla).Rows(0).Item("ajus_id")

        Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
        oFact.CentroEmisorNro(mstrConn)
        lDr.Item("proc_cemi_nume") = oFact.pCentroEmisorNro
        lDr.Item("proc_emct_id") = oFact.pCentroEmisorId
        lDr.Item("proc_host") = oFact.pHost
        oFact.Liberar(Me)
    End Sub

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi(False)
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        mModi(True)
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Public Sub mConsultar()
        Dim lstrFiltros As New System.text.StringBuilder
        mbooHayPend = False
        With lstrFiltros
            .Append("exec " + mstrTabla + "_consul")
            .Append(" @clie_id=")
            .Append(usrClieFil.Valor.ToString())
            .Append(", @fecha_desde=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha))
            .Append(", @fecha_hasta=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha))
            If txtNumeDesdeFil.Text <> "" Then
                .Append(", @nume_desde=")
                .Append(txtNumeDesdeFil.Valor.ToString)
            End If
            If txtNumeHastaFil.Text <> "" Then
                .Append(", @nume_hasta=")
                .Append(txtNumeHastaFil.Valor.ToString)
            End If
            .Append(", @apro=")
            .Append(Math.Abs(CInt(chkApro.Checked)))
            .Append(", @ajus_emct_codi=")
            .Append(cmbEmctFil.Valor.ToString())
            .Append(", @ajus_baja_fecha=")
            .Append(Math.Abs(CInt(ChkAnu.Checked)))

            clsWeb.gCargarDataGrid(mstrConn, .ToString, grdDato)
        End With

        btnConf.Visible = mbooHayPend

        mMostrarPanel(False)
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
		Try

			Dim resul As ResultadoDoble = mCargarDatos(e.Item.Cells(Columnas.Id).Text)

			mSetearEditor("", False, resul.rsCerrado, resul.rsBaja)
			mMostrarPanel(True)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
   End Sub
#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub grdDato_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDato.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 Then
            With CType(e.Item.DataItem, DataRowView).Row
               If Not .IsNull("ajus_apro_fecha") Then
                  e.Item.FindControl("chkSel").Visible = False
               Else
                  mbooHayPend = True
               End If
            End With
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#Region "Detalle"
   Public Sub grdDetalle_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDetalle.EditItemIndex = -1
         If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
            grdDetalle.CurrentPageIndex = 0
         Else
            grdDetalle.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultarDeta()
      With mdsDatos.Tables(mstrTablaDeta)
         grdDetalle.DataSource = .DefaultView
         grdDetalle.DataBind()
      End With
   End Sub

   Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDeta As DataRow

         hdnDetaId.Text = E.Item.Cells(1).Text
         ldrDeta = mdsDatos.Tables(mstrTablaDeta).Select("ajco_id=" & hdnDetaId.Text)(0)

         With ldrDeta
            'rbtnDetaDebe.Checked = Not .Item("ajco_dh")
            'rbtnDetaHaber.Checked = .Item("ajco_dh")
            txtImpo.Valor = .Item("ajco_impo")
            cmbConc.Valor = .Item("ajco_conc_id")
                    mCargarCentroCostos(True)
                    If Not (.Item("ajco_ccos_id") Is DBNull.Value) Then
                        cmbCcos.Valor = .Item("ajco_ccos_id")
                    Else
                        cmbCcos.Valor = ""
                    End If
                    txtDetaObse.Valor = .Item("ajco_obse")
         End With

			mSetearEditor(mstrTablaDeta, False, False, True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarDeta()
      Try
         mGuardarDatosDeta()

         mLimpiarDeta()
         mConsultarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosDeta()
      Dim ldrDatos As DataRow

      If cmbConc.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el Concepto.")
      End If

            If cmbCcos.Valor.Trim().Length = 0 Then
                If clsSQLServer.gObtenerEstruc(mstrConn, "conceptosX", cmbConc.Valor.ToString).Tables(0).Rows(0).Item("cta_resu").ToString() = "1" Then
                    Throw New AccesoBD.clsErrNeg("La cuenta del concepto es de resultado, debe indicar el centro de costo.")
                End If
            End If

            If txtImpo.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el Importe.")
      End If

      If hdnDetaId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrTablaDeta).NewRow
         ldrDatos.Item("ajco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "ajco_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrTablaDeta).Select("ajco_id=" & hdnDetaId.Text)(0)
      End If

      With ldrDatos
         .Item("ajco_dh") = rbtnHaber.Checked
         .Item("ajco_impo") = txtImpo.Valor
                .Item("ajco_conc_id") = cmbConc.Valor
                If (cmbCcos.Valor.Trim().Length > 0) Then
                    .Item("ajco_ccos_id") = cmbCcos.Valor
                Else
                    .Item("ajco_ccos_id") = DBNull.Value
                End If
                .Item("ajco_obse") = txtDetaObse.Valor

         If Not .IsNull("ajco_impo") Then
             .Item("_impo") = .Item("ajco_impo") * IIf(.Item("ajco_dh"), 1, -1)
         End If
                .Item("_conc_desc") = cmbConc.SelectedItem.Text
                If (cmbCcos.Valor.Trim().Length > 0) Then
                    .Item("_ccos_desc") = cmbCcos.SelectedItem.Text
                End If
            End With

      If hdnDetaId.Text = "" Then
         mdsDatos.Tables(mstrTablaDeta).Rows.Add(ldrDatos)
      End If
   End Sub

   Private Sub mLimpiarDeta()
      hdnDetaId.Text = ""
      txtImpo.Text = ""
      cmbConc.Limpiar()
      cmbCcos.Items.Clear()
      'rbtnDetaDebe.Checked = False
      'rbtnDetaHaber.Checked = True
      txtDetaObse.Text = ""

		mSetearEditor(mstrTablaDeta, True, False, True)
   End Sub

   Private Sub btnLimpDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub

   Private Sub btnBajaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
      Try
         mdsDatos.Tables(mstrTablaDeta).Select("ajco_id=" & hdnDetaId.Text)(0).Delete()
         mConsultarDeta()
         mLimpiarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDeta()
   End Sub

   Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDeta()
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "AjustesCtasCtes"
         Dim lstrRpt As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&fecha_desde=" + lintFechaDesde.ToString
         lstrRpt += "&fecha_hasta=" + lintFechaHasta.ToString
         lstrRpt += "&nume_desde=" + IIf(txtNumeDesdeFil.Text = "", "0", txtNumeDesdeFil.Text)
         lstrRpt += "&nume_hasta=" + IIf(txtNumeHastaFil.Text = "", "0", txtNumeHastaFil.Text)
         lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
            lstrRpt += "&apro=" + IIf(chkApro.Checked, "True", "False")
            lstrRpt += "&ajus_baja_fecha=" + IIf(ChkAnu.Checked, "True", "False")
            lstrRpt += "&ajus_emct_id=" + IIf(cmbEmctFil.Valor.ToString = "", 0, cmbEmctFil.Valor.ToString)
   
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Sub btnConf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConf.Click
        Try
           mConfirmarMul()
        Catch ex As Exception
           clsError.gManejarError(Me, ex)
        End Try
    End Sub

	Public Class ResultadoDoble
		Public rsBaja As Boolean
		Public rsCerrado As Boolean
    End Class
    Public Sub mItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            If e.Item.ItemIndex <> -1 Then
                If Not CType(e.Item.DataItem, DataRowView).Row.IsNull("ajus_baja_fecha") Then
                    e.Item.BackColor = System.Drawing.Color.LightSalmon
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

End Class
End Namespace
