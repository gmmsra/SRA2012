' Dario 2013-09-30 cambio para permitir el debito en cuenta
Namespace SRA

Partial Class Tarjetas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "tarjetas"
    Private mstrTablaEstab As String = "tarjetas_estab"
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                mCargarCuentas()
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCuentas()
        If cmbBancSocios.Valor.ToString <> "" Then
            clsWeb.gCargarCombo(mstrConn, "cuentas_bancos_cargar @banc_id=" & cmbBancSocios.Valor.ToString, cmbCuenSocios, "id", "descrip", "S")
            cmbCuenSocios.Valor = Request.Form("hdnCtaSoci")
        Else
            cmbCuenSocios.Items.Clear()
        End If
        If cmbBancISEA.Valor.ToString <> "" Then
            clsWeb.gCargarCombo(mstrConn, "cuentas_bancos_cargar @banc_id=" & cmbBancISEA.Valor.ToString, cmbCuenISEA, "id", "descrip", "S")
            cmbCuenISEA.Valor = Request.Form("hdnCtaIsea")
        Else
            cmbCuenISEA.Items.Clear()
        End If
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
        '   Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        Dim lintCol As Integer

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

        txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_codi")
        txtDescripcion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_desc")
        txtPorcComCupo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_comi_porc")
        txtMontComCupo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_comi_monto")
        txtMaxCuotas.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_max_cuota")
        txtMaxCuotasTel.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_max_cuota_vtat")
        txtNroComSoci.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_da_nuco_soci")
        txtNomArchSoci.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_da_arch_soci")
        txtNroComISEA.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_da_nuco_isea")
        txtNomArchISEA.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_da_arch_isea")
        txtMontoMaxSAPes.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_impo_maxi")
        txtMontoMinPes.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_impo_mini")
        txtMontoMaxSADol.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_dol_impo_maxi")
        txtMontoMinDol.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tarj_dol_impo_mini")

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaEstab)
        txtNumeroEstab.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "taes_esta_nume")
        txtReferEstab.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "taes_esta_refe")

    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuentaContablePes, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuentaContableDol, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancSocios, "S", "@con_cuentas=1")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancISEA, "S", "@con_cuentas=1")
        ' Dario 2013-09-30 debito en cuenta
        clsWeb.gCargarComboBool(Me.cmbDebitoEnCuenta, "S")
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul "
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultarEstab()
        Try
            mLimpiarEstab()
            mstrCmd = "exec " + mstrTablaEstab + "_consul null"
            mstrCmd = mstrCmd + "," + IIf(hdnId.Text = "", "null", hdnId.Text)
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdEstab)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorEstab(ByVal pbooAlta As Boolean)
        btnBajaEstab.Enabled = Not (pbooAlta)
        btnModiEstab.Enabled = Not (pbooAlta)
        btnAltaEstab.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        grdEstab.CurrentPageIndex = 0

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtCodigo.Valor = .Item("tarj_codi")
                txtDescripcion.Valor = .Item("tarj_desc")
                ' Dario 2013-09-30 se agraga nuevo valor para debito en cuentas
                Me.cmbDebitoEnCuenta.Valor = IIf(.Item("tarj_PermiteDebitoEnCuenta"), "1", "0")
                txtPorcComCupo.Valor = .Item("tarj_comi_porc")
                txtMontComCupo.Valor = .Item("tarj_comi_monto")
                txtMaxCuotas.Valor = .Item("tarj_max_cuota")
                txtMaxCuotasTel.Valor = .Item("tarj_max_cuota_vtat")
                    txtNroComSoci.Valor = .Item("tarj_da_nuco_soci").ToString
                    txtNomArchSoci.Valor = .Item("tarj_da_arch_soci").ToString
                    cmbBancSocios.Valor = .Item("tarj_da_banc_id_soci").ToString
                    txtNroComISEA.Valor = .Item("tarj_da_nuco_isea").ToString
                    txtNomArchISEA.Valor = .Item("tarj_da_arch_isea").ToString
                    cmbBancISEA.Valor = .Item("tarj_da_banc_id_isea").ToString
                    txtMontoMaxSAPes.Valor = .Item("tarj_impo_maxi")
                txtMontoMinPes.Valor = .Item("tarj_impo_mini")
                cmbCuentaContablePes.Valor = .Item("tarj_cuct_id")
                chkConPesos.Checked = (.Item("tarj_cuct_id").ToString <> "")
                txtMontoMaxSAPes.Enabled = (.Item("tarj_cuct_id").ToString <> "")
                txtMontoMinPes.Enabled = (.Item("tarj_cuct_id").ToString <> "")
                cmbCuentaContablePes.Enabled = (.Item("tarj_cuct_id").ToString <> "")
                chkConDolares.Checked = (.Item("tarj_dol_cuct_id").ToString <> "")
                txtMontoMaxSADol.Enabled = (.Item("tarj_dol_cuct_id").ToString <> "")
                txtMontoMinDol.Enabled = (.Item("tarj_dol_cuct_id").ToString <> "")
                cmbCuentaContableDol.Enabled = (.Item("tarj_dol_cuct_id").ToString <> "")

                    txtMontoMaxSADol.Valor = .Item("tarj_dol_impo_maxi").ToString
                    txtMontoMinDol.Valor = .Item("tarj_dol_impo_mini").ToString
                    cmbCuentaContableDol.Valor = .Item("tarj_dol_cuct_id").ToString

                If Not .IsNull("tarj_da_cuba_id_soci") Then
                    hdnCtaSoci.Text = .Item("tarj_da_cuba_id_soci")
                Else
                    hdnCtaSoci.Text = ""
                End If
                If Not .IsNull("tarj_da_cuba_id_isea") Then
                    hdnCtaIsea.Text = .Item("tarj_da_cuba_id_isea")
                Else
                    hdnCtaIsea.Text = ""
                End If
                mCargarCuentas()

                If Not .IsNull("tarj_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("tarj_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

            End With
            mSetearEditor(False)
            mSetearEditorEstab(True)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaEstab

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdEstab.DataSource = mdsDatos.Tables(mstrTablaEstab)
        grdEstab.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        txtCodigo.Text = ""
        txtDescripcion.Text = ""
        ' Dario 2013-09-30 por modificacion de debito en cuenta
        Me.cmbDebitoEnCuenta.Limpiar()
        txtPorcComCupo.Text = ""
        txtMontComCupo.Text = ""
        txtMaxCuotas.Text = ""
        txtMaxCuotasTel.Text = ""
        txtNroComSoci.Text = ""
        txtNomArchSoci.Text = ""
        txtNroComISEA.Text = ""
        txtNomArchISEA.Text = ""
        txtMontoMaxSAPes.Text = ""
        txtMontoMinPes.Text = ""
        cmbCuentaContablePes.Limpiar()
        txtMontoMaxSADol.Text = ""
        txtMontoMaxSADol.Enabled = False
        txtMontoMinDol.Text = ""
        txtMontoMinDol.Enabled = False
        cmbCuentaContableDol.Limpiar()
        cmbCuentaContableDol.Enabled = False
        chkConPesos.Checked = True
        chkConDolares.Checked = False
        cmbBancISEA.Limpiar()
        cmbBancSocios.Limpiar()
        cmbCuenISEA.Limpiar()
        cmbCuenSocios.Limpiar()
        hdnCtaSoci.Text = ""
        hdnCtaIsea.Text = ""
        mLimpiarEstab()
        lblBaja.Text = ""
        grdEstab.CurrentPageIndex = 0
        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        mEstablecerPerfil()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        txtMontoMaxSAPes.Enabled = (chkConPesos.Checked = True)
        txtMontoMinPes.Enabled = (chkConPesos.Checked = True)
        cmbCuentaContablePes.Enabled = (chkConPesos.Checked = True)

        txtMontoMaxSADol.Enabled = (chkConDolares.Checked = True)
        txtMontoMinDol.Enabled = (chkConDolares.Checked = True)
        cmbCuentaContableDol.Enabled = (chkConDolares.Checked = True)

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If txtPorcComCupo.Valor.ToString = "" And txtMontComCupo.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Porcentaje y/o el Monto de Comisi�n Cupon.")
        End If

        If txtNroComSoci.Valor.ToString <> "" And txtNomArchSoci.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre de Archivo Socios.")
        End If
        'If txtNroComSoci.Valor.ToString <> "" And cmbBancSocios.Valor.ToString = "" Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el Banco de Dep�sito para Socios.")
        'End If

        If txtNroComISEA.Valor.ToString <> "" And txtNomArchISEA.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre de Archivo ISEA.")
        End If
        'If txtNroComISEA.Valor.ToString <> "" And cmbBancISEA.Valor.ToString = "" Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el Banco de Dep�sito para ISEA.")
        'End If

        If chkConPesos.Checked = False And chkConDolares.Checked = False Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar Moneda/s de operaci�nes.")
        End If

        If chkConPesos.Checked = True And (txtMontoMaxSAPes.Valor.ToString = "" Or txtMontoMinPes.Valor.ToString = "" Or cmbCuentaContablePes.Valor.ToString = "") Then
            Throw New AccesoBD.clsErrNeg("Debe completar todos los datos de Operaci�n con Pesos.")
        End If

        If chkConDolares.Checked = True And (txtMontoMaxSADol.Valor.ToString = "" Or txtMontoMinDol.Valor.ToString = "" Or cmbCuentaContableDol.Valor.ToString = "") Then
            Throw New AccesoBD.clsErrNeg("Debe completar todos los datos de Operaci�n con D�lares.")
        End If

        If grdEstab.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un Establecimiento.")
        End If

    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("tarj_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("tarj_codi") = txtCodigo.Valor
            .Item("tarj_desc") = txtDescripcion.Valor
            ' Dario 2013-09-30 se agraga nuevo valor para el debito en cuenta
            .Item("tarj_PermiteDebitoEnCuenta") = Me.cmbDebitoEnCuenta.ValorBool
            .Item("tarj_comi_porc") = txtPorcComCupo.Valor
            .Item("tarj_comi_monto") = txtMontComCupo.Valor
            .Item("tarj_max_cuota") = txtMaxCuotas.Valor
            .Item("tarj_max_cuota_vtat") = txtMaxCuotasTel.Valor
            .Item("tarj_da_nuco_soci") = txtNroComSoci.Valor
            .Item("tarj_da_arch_soci") = IIf(txtNroComSoci.Valor.ToString <> "", txtNomArchSoci.Valor, DBNull.Value)
            .Item("tarj_da_banc_id_soci") = IIf(txtNroComSoci.Valor.ToString <> "", cmbBancSocios.Valor, DBNull.Value)
            .Item("tarj_da_nuco_isea") = txtNroComISEA.Valor
            .Item("tarj_da_arch_isea") = IIf(txtNroComISEA.Valor.ToString <> "", txtNomArchISEA.Valor, "")
            .Item("tarj_da_banc_id_isea") = IIf(txtNroComISEA.Valor.ToString <> "", cmbBancISEA.Valor, DBNull.Value)
            .Item("tarj_impo_maxi") = IIf(chkConPesos.Checked, txtMontoMaxSAPes.Valor, DBNull.Value)
            .Item("tarj_impo_mini") = IIf(chkConPesos.Checked, txtMontoMinPes.Valor, DBNull.Value)
            .Item("tarj_cuct_id") = IIf(chkConPesos.Checked, cmbCuentaContablePes.Valor, DBNull.Value)
            .Item("tarj_dol_impo_maxi") = IIf(chkConDolares.Checked, txtMontoMaxSADol.Valor, DBNull.Value)
            .Item("tarj_dol_impo_mini") = IIf(chkConDolares.Checked, txtMontoMinDol.Valor, DBNull.Value)
            .Item("tarj_dol_cuct_id") = IIf(chkConDolares.Checked, cmbCuentaContableDol.Valor, DBNull.Value)
            .Item("tarj_da_cuba_id_soci") = IIf(hdnCtaSoci.Text = "", DBNull.Value, hdnCtaSoci.Text)
            .Item("tarj_da_cuba_id_isea") = IIf(hdnCtaIsea.Text = "", DBNull.Value, hdnCtaIsea.Text)
            .Item("tarj_audi_user") = Session("sUserId").ToString()
        End With
    End Sub
    'Metodos de los Establecimientos
    Private Sub mActualizarEstab()
        Try
            mGuardarDatosEstab()

            mLimpiarEstab()
            grdEstab.DataSource = mdsDatos.Tables(mstrTablaEstab)
            grdEstab.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaEstab()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaEstab).Select("taes_id=" & hdnEstabId.Text)(0)
            row.Delete()
            grdEstab.DataSource = mdsDatos.Tables(mstrTablaEstab)
            grdEstab.DataBind()
            mLimpiarEstab()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosEstab()
        Dim ldrCorre As DataRow

        If txtNumeroEstab.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el nro de establecimiento.")
        End If

        If hdnEstabId.Text = "" Then
            ldrCorre = mdsDatos.Tables(mstrTablaEstab).NewRow
            ldrCorre.Item("taes_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaEstab), "taes_id")
        Else
            ldrCorre = mdsDatos.Tables(mstrTablaEstab).Select("taes_id=" & hdnEstabId.Text)(0)
        End If

        With ldrCorre
            .Item("taes_esta_nume") = txtNumeroEstab.Valor
            .Item("taes_esta_refe") = txtReferEstab.Valor
        End With
        If (hdnEstabId.Text = "") Then
            mdsDatos.Tables(mstrTablaEstab).Rows.Add(ldrCorre)
        End If
    End Sub
    Private Sub mLimpiarEstab()
        hdnEstabId.Text = ""
        txtNumeroEstab.Text = ""
        txtReferEstab.Text = ""
        mSetearEditorEstab(True)
    End Sub
    Public Sub mEditarDatosEstab(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrEstab As DataRow

            hdnEstabId.Text = E.Item.Cells(1).Text
            ldrEstab = mdsDatos.Tables(mstrTablaEstab).Select("taes_id=" & hdnEstabId.Text)(0)

            With ldrEstab
                txtNumeroEstab.Valor = .Item("taes_esta_nume")
                txtReferEstab.Valor = .Item("taes_esta_refe")
            End With
            mSetearEditorEstab(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            txtMontoMaxSAPes.Enabled = (chkConPesos.Checked = True)
            txtMontoMinPes.Enabled = (chkConPesos.Checked = True)
            cmbCuentaContablePes.Enabled = (chkConPesos.Checked = True)

            txtMontoMaxSADol.Enabled = (chkConDolares.Checked = True)
            txtMontoMinDol.Enabled = (chkConDolares.Checked = True)
            cmbCuentaContableDol.Enabled = (chkConDolares.Checked = True)

            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panDebito.Visible = False
            lnkDebito.Font.Bold = False
            panMoneda.Visible = False
            lnkMonedas.Font.Bold = False
            panEstab.Visible = False
            lnkEstab.Font.Bold = False
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    panDebito.Visible = True
                    lnkDebito.Font.Bold = True
                Case 3
                    panMoneda.Visible = True
                    lnkMonedas.Font.Bold = True
                Case 4
                    panEstab.Visible = True
                    lnkEstab.Font.Bold = True
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            Dim params As String
            Dim lstrRptName As String = "Tarjetas"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDebito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDebito.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkMonedas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMonedas.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkEstab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEstab.Click
        mShowTabs(4)
    End Sub
    Private Sub btnAltaEstab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaEstab.Click
        mActualizarEstab()
    End Sub
    Private Sub btnBajaEstab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaEstab.Click
        mBajaEstab()
    End Sub
    Private Sub btnModiEstab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiEstab.Click
        mActualizarEstab()
    End Sub
    Private Sub btnLimpEstab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpEstab.Click
        mLimpiarEstab()
    End Sub
#End Region

End Class
End Namespace
