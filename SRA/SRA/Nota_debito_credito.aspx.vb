Namespace SRA

    Partial Class Nota_debito_credito
        Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

        'El Dise�ador de Web Forms requiere esta llamada.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub









        Protected WithEvents hdnDescBusqPop As System.Web.UI.WebControls.TextBox

        Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
        Protected WithEvents btnBusc As NixorControls.BotonImagen
        Protected WithEvents btnList As NixorControls.BotonImagen
        Protected WithEvents lblActiFil As System.Web.UI.WebControls.Label
        Protected WithEvents cmbActiFil As NixorControls.ComboBox
        Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
        Protected WithEvents txtFecha As NixorControls.DateBox
        Protected WithEvents lblClieFil As System.Web.UI.WebControls.Label
        Protected WithEvents lblTipoComp As System.Web.UI.WebControls.Label
        Protected WithEvents cmbTipoComp As NixorControls.ComboBox
        Protected WithEvents lblNro As System.Web.UI.WebControls.Label
        Protected WithEvents txtCEmi As NixorControls.NumberBox
        Protected WithEvents lblSepa As System.Web.UI.WebControls.Label
        Protected WithEvents txtNro As NixorControls.NumberBox

        Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton




        Protected WithEvents lblMail As System.Web.UI.WebControls.Label
        Protected WithEvents txtMail As NixorControls.TextBoxTab
        Protected WithEvents txtMailRefe As NixorControls.TextBoxTab




        Protected WithEvents imgCerrarEnca As System.Web.UI.WebControls.ImageButton
        Protected WithEvents hdnTarjId As System.Web.UI.WebControls.TextBox
        Protected WithEvents hdnEncaId As System.Web.UI.WebControls.TextBox


        Protected WithEvents lblTotIVA As System.Web.UI.WebControls.Label
        Protected WithEvents lblTotIVARedu As System.Web.UI.WebControls.Label
        Protected WithEvents lblTotIVASTasa As System.Web.UI.WebControls.Label
        Protected WithEvents lblSCargoAran As System.Web.UI.WebControls.Label
        Protected WithEvents chkSCargoAran As System.Web.UI.WebControls.CheckBox
        Protected WithEvents lblExentoConcep As System.Web.UI.WebControls.Label
        Protected WithEvents chkExentoConcep As System.Web.UI.WebControls.CheckBox
        Protected WithEvents lblSCargoConcep As System.Web.UI.WebControls.Label
        Protected WithEvents chkSCargoConcep As System.Web.UI.WebControls.CheckBox
        Protected WithEvents lblCtoCostoAranRRGG As System.Web.UI.WebControls.Label
        Protected WithEvents lblTramAranRRGG As System.Web.UI.WebControls.Label

        Protected WithEvents lblCCos As System.Web.UI.WebControls.Label
        Protected WithEvents cmbConcepAux As NixorControls.ComboBox

        Protected WithEvents cmbCcosAran As NixorControls.ComboBox
        Protected WithEvents cmbConcepAux2 As NixorControls.ComboBox
        Protected WithEvents lblAutoriza As System.Web.UI.WebControls.Label
        Protected WithEvents lstAutoriza As NixorControls.CheckListBox
        Protected WithEvents txtCantNoFacAranRRGG As NixorControls.NumberBox
        Protected WithEvents lblCantSCAran As System.Web.UI.WebControls.Label
        Protected WithEvents txtCantSCAran As NixorControls.NumberBox
        Protected WithEvents lblCantNoFacAranRRGG As System.Web.UI.WebControls.Label
        Protected WithEvents hdnProforma As System.Web.UI.WebControls.TextBox


        Protected WithEvents btAltaCuot As System.Web.UI.WebControls.Button
        Protected WithEvents Label3 As System.Web.UI.WebControls.Label
        Protected WithEvents txtAranSTCodi As NixorControls.TextBoxTab
        Protected WithEvents txtAranSTDesc As NixorControls.TextBoxTab
        Protected WithEvents panAranSobretasa As System.Web.UI.WebControls.Panel
        Protected WithEvents cmbComp As NixorControls.ComboBox
        Protected WithEvents lblComp As System.Web.UI.WebControls.Label
        Protected WithEvents panRRGGoLab As System.Web.UI.WebControls.Panel





        Protected WithEvents panClieGene As System.Web.UI.WebControls.Panel
        Protected WithEvents pantblClieGene As System.Web.UI.WebControls.Panel
        Protected WithEvents imgEspec As System.Web.UI.HtmlControls.HtmlAnchor


        Protected WithEvents DivAranSobretasa As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents Button1 As System.Web.UI.WebControls.Button
        Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label

        Protected WithEvents lblCriador As System.Web.UI.WebControls.Label









        'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
        'No se debe eliminar o mover.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
            'No la modifique con el editor de c�digo.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"

        Private mstrTablaProfo As String = SRA_Neg.Constantes.gTab_Proformas

        Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
        Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
        Private mstrTablaDetaAran As String = SRA_Neg.Constantes.gTab_ComprobAranc
        Private mstrTablaDetaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
        Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
        Private mstrTablaAutor As String = SRA_Neg.Constantes.gTab_ComprobAutor
        Private mstrTablaSaldosACta As String = SRA_Neg.Constantes.gTab_SaldosACuenta

        Private mstrTablaTempDeta As String = "comprob_aranc_concep"

        Private mstrParaPageSize As Integer
        Private mstrCmd As String
        Private mstrConn As String
        Private mdsDatos As DataSet
        Private mobj As SRA_Neg.Facturacion

        'forma de pago

        Private Const mintCtaCte As Integer = 2
        Private Const mintContado As Integer = 1

        Public Enum TipoPeriodo As Integer
            mes = 1
            dia = 2
        End Enum

        Public Enum TiposComprobantes As Integer
            NotaCredito = 31
            NotaDebito = 32
        End Enum

        Private Enum OrdenItems As Integer
            arancel = 1
            conceptos = 2
            conceptosAuto = 3
        End Enum
        Private Enum Columnas As Integer

            id = 1
            tipo = 2
            codi_aran_concep = 3
            desc_aran_concep = 4
            cant = 5
            cant_sin_cargo = 6
            anim_no_fact = 7
            impo = 8
            fecha = 9
            inic_rp = 10
            fina_rp = 11
            tram = 12
            acta = 13
            ccos_codi = 14
            exen = 15
            sin_carg = 16
            aran_concep_id = 17
            ccos_id = 18
            impo_bruto = 19
            tipo_IVA = 20
            tasa_iva = 21
            impo_ivai = 22
            autom = 23
        End Enum
        Public Enum Actividades As Integer
            Varios = 1 ' administracion por ejemplo, pero en relidad son todos los demas
            RRGG = 3
            Laboratorio = 4
            AlumnosISEA = 6
            Expo = 7
            AlumnosCEIDA = 10
        End Enum
        Private Enum Panta As Integer
            Encabezado = 1
            Pie = 2
            Cuotas = 3
            Autorizacion = 4
            Arancel = 5
            Concepto = 6
        End Enum

        Private Enum CamposEstado As Integer
            saldo_cta_cte = 0
            saldo_social = 1
            saldo_total = 2
            imp_a_cta_cta_cte = 3
            imp_a_cta_cta_social = 4
            lim_saldo = 5 ' si es nulo, el cliente no opera con Cta. Cte.
            color = 6
            Saldo_excedido = 7
        End Enum
#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()
                mInicializarFacturacion()
                mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())

                If (Not Page.IsPostBack) Then
                    Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                    mCargarCombos()
                    mAgregar()
                    mSetearMaxLength()
                    ' mEstablecerPerfil()
                    Session(mSess(mstrTabla)) = Nothing
                    mdsDatos = Nothing
                    lblFechaIngreso.Text = Today.ToString("dd/MM/yyyy")
                    txtFechaValor.Fecha = Today
                    txtFechaIva.Fecha = Today
                    mobj.CentroEmisorNro(mstrConn)
                    mobj.IVA(mstrConn, txtFechaIva.Text)
                    '  mConsultar()

                    clsWeb.gInicializarControles(Me, mstrConn)

                Else
                    mCargarCentroCostos(False)
                    If panDato.Visible Then
                        mdsDatos = Session(mstrTabla)
                        mSetearControles()
                        mCotizacionDolar()
                    End If
                End If


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        '   Private Sub mEstablecerPerfil()
        '      Dim lbooPermiAlta As Boolean
        '      Dim lbooPermiModi As Boolean

        '      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
        '      'Response.Redirect("noaccess.aspx")
        '      'End If

        '      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        '      'btnAlta.Visible = lbooPermiAlta

        '      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        '      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        '      'btnModi.Visible = lbooPermiModi

        '      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        '      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
        '   End Sub

        Private Sub mSetearMaxLength()
            Dim lstrLongs As Object
            Dim lintCol As Integer

            lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)
            txtObser.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "code_obse")

            lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDetaConcep)
            txtDescAmpliConcep.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "coco_desc_ampl")

            lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDetaAran)
            txtDescAmpliAran.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "coan_desc_ampl")

        End Sub
#End Region

#Region "Inicializacion de Variables"
        Public Sub mInicializar()


            clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)


            usrClie.Tabla = "clientes"
            '  usrClie.AutoPostback = True
            usrClie.Ancho = 790
            usrClie.Alto = 510

            'Clave unica
            usrClie.ColClaveUnica = True
            usrClie.FilClaveUnica = True
            'Numero de cliente
            usrClie.ColClieNume = True
            'muestra desc
            usrClie.MuestraDesc = False

            'Fantasia
            'usrClieFil.ColFanta = False
            'usrClieFil.FilFanta = False
            'Numero de CUIT
            usrClie.ColCUIT = True
            usrClie.FilCUIT = True
            'Numero de documento
            usrClie.ColDocuNume = True
            usrClie.FilDocuNume = True
            'Numero de socio
            usrClie.ColSociNume = True
            usrClie.FilSociNume = True

        End Sub
#End Region

#Region "Seteo de Controles"


        Private Sub mSetearEventoGenerarComprobante()
            hdnComprobante.Text = cmbComprobante.SelectedItem.Text
            ImgbtnGenerar.Attributes.Add("onclick", "if(!confirm('Confirma la generaci�n del comprobante. ')) return false;")
        End Sub


        Private Sub mMostrarSolapaArancel(ByVal pboolArancel As Boolean)
            panAran.Visible = pboolArancel
            If pboolArancel Then
                btnAran.ImageUrl = "imagenes/tabArancel1.gif"
                btnConcep.ImageUrl = "imagenes/tabConcep0.gif"
            Else
                btnAran.ImageUrl = "imagenes/tabArancel0.gif"
                btnConcep.ImageUrl = "imagenes/tabConcep1.gif"
            End If
            panConcep.Visible = Not pboolArancel
        End Sub

        Private Sub mRRGGoLabComprobVisible(ByVal pboolVisible As Boolean)
            panRRGGoLab.Visible = pboolVisible
        End Sub


        Private Sub mShowTabs(ByVal origen As Byte)

            panDato.Visible = True
            panDeta.Visible = False
            panAran.Visible = False
            panConcep.Visible = False
            panPie.Visible = False
            panCuotas.Visible = False
            panAutoriza.Visible = False
            panCabecera.Visible = False
            panEnca.Visible = False
            PanBotonesNavegDeta.Visible = False
            PanBotonesNavegCuot.Visible = False
            panBotonesNavegAutiza.Visible = False
            panBotonesPie.Visible = False
            btnAran.ImageUrl = "imagenes/tabArancel1.gif"
            btnConcep.ImageUrl = "imagenes/tabConcep0.gif"


            Select Case origen
                Case 1
                    'encabezado
                    panCabecera.Visible = True

                Case 5
                    'Detalle / arancel
                    panDeta.Visible = True
                    panAran.Visible = True
                    mControlesIVA()
                    panEnca.Visible = True
                    PanBotonesNavegDeta.Visible = True
                    hdnFechaValor.Text = txtFechaValor.Text

                Case 4
                    'autorizaciones
                    panEnca.Visible = True
                    panAutoriza.Visible = True

                    panBotonesNavegAutiza.Visible = True


                Case 6
                    'Detalle / conceptos


                    btnConcep.ImageUrl = "imagenes/tabConcep1.gif"
                    btnAran.ImageUrl = "imagenes/tabArancel0.gif"
                    panDeta.Visible = True
                    panConcep.Visible = True
                    mControlesIVA()
                    panEnca.Visible = True
                    PanBotonesNavegDeta.Visible = True

                Case 3
                    'cuotas
                    panEnca.Visible = True
                    panCuotas.Visible = True
                    PanBotonesNavegCuot.Visible = True



                Case 2
                    'pie
                    panEnca.Visible = True
                    panPie.Visible = True

                    panBotonesPie.Visible = True




            End Select
        End Sub
        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            Select Case pstrTabla

                Case mstrTablaDetaConcep
                    btnBajaConcep.Enabled = Not (pbooAlta)
                    btnModiConcep.Enabled = Not (pbooAlta)
                    btnAltaConcep.Enabled = pbooAlta

                Case mstrTablaDetaAran
                    btnBajaAran.Enabled = Not (pbooAlta)
                    If mobj.pAranRRGGSobretasa <> 0 Then
                        ' no puede modificarse una sobretasa, solo eliminarse
                        btnModiAran.Enabled = False
                    Else
                        btnModiAran.Enabled = Not (pbooAlta)
                    End If
                    btnAltaAran.Enabled = pbooAlta

                Case mstrTablaVtos
                    btnBajaCuot.Enabled = Not (pbooAlta)
                    btnModiCuot.Enabled = Not (pbooAlta)
                    btnAltaCuot.Enabled = pbooAlta

                Case Else
                    ImgbtnGenerar.Enabled = pbooAlta

            End Select
        End Sub
        Private Function mSess(ByVal pstrTabla As String) As String
            If hdnSess.Text = "" Then
                hdnSess.Text = pstrTabla & Now.Millisecond.ToString
            End If
            Return (hdnSess.Text)
        End Function
        Private Sub mAgregar()
            mLimpiar()
            mInicializaFactura()
            mLlenaComboListaPrecios(txtFechaValor.Fecha, cmbActi.Valor)
            mInicializarVariablesCalculo()
        End Sub
        Private Sub mInicializaFactura()
            mobj.Limpiar()
            mInicializarFacturacion()
            mShowTabs(Panta.Encabezado)
        End Sub

        Private Sub mInicializarFacturacion()
            mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
        End Sub
        Private Sub mCerrar()
            mAgregar()
        End Sub

#End Region

#Region "Operaciones sobre el DataGrid"


        Public Sub grdDetalle_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDetalle.EditItemIndex = -1
                If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
                    grdDetalle.CurrentPageIndex = 0
                Else
                    grdDetalle.CurrentPageIndex = E.NewPageIndex
                End If

                grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta)
                grdDetalle.DataBind()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub



        Private Sub grdAutoriza_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdAutoriza.ItemDataBound
            Try
                If e.Item.ItemIndex <> -1 Then
                    Dim oComen As NixorControls.TextBoxTab = e.Item.FindControl("txtComen")
                    ' Dim ochkApro As NixorControls.CheckBoxSimple = e.Item.FindControl("chkApro")
                    With CType(e.Item.DataItem, DataRowView).Row
                        DirectCast(e.Item.FindControl("chkApro"), CheckBox).Checked = .Item("coau_auto")
                        oComen.Valor = .Item("coau_obse").ToString

                        If .IsNull("coau_id") Then
                            .Item("coau_id") = -1 * e.Item.ItemIndex
                            e.Item.Cells(0).Text = .Item("coau_id")
                        End If


                    End With
                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub




        Public Sub grdCuotas_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdCuotas.EditItemIndex = -1
                If (grdCuotas.CurrentPageIndex < 0 Or grdCuotas.CurrentPageIndex >= grdCuotas.PageCount) Then
                    grdCuotas.CurrentPageIndex = 0
                Else
                    grdCuotas.CurrentPageIndex = E.NewPageIndex
                End If
                grdCuotas.DataSource = mdsDatos.Tables(mstrTablaVtos)
                grdCuotas.DataBind()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosDetalle(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            ' si es un arancel de turnos, llenar los hdl correspondientes (equi / bov)
            '.Item("temp_rela_id")

            Try

                Dim ldrTemp As DataRow
                hdnDetaTempId.Text = E.Item.Cells(1).Text
                ldrTemp = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id=" & hdnDetaTempId.Text)(0)


                With ldrTemp
                    If .Item("temp_tipo") = "A" Or .Item("temp_tipo") = "S" Then  ' aranceles 
                        hdnDetaTempTipo.Text = .Item("temp_tipo")
                        cmbAran.Valor = .Item("temp_aran_concep_id")
                        mSetearDatosArancel()
                        'If mobj.pRRGG Then
                        '   txtAranSTCodi.Valor = mobj.ObtenerValorCampo(mstrConn, "precios_aranX", "_codi", .Item("temp_aran_concep_id").ToString + "," + cmbActi.Valor.ToString)
                        '   txtAranSTDesc.Valor = .Item("temp_desc_aran_concep")
                        '   txtCantNoFacAranRRGG.Valor = .Item("temp_anim_no_fact")
                        '   txtFechaAranRRGG.Fecha = .Item("temp_fecha")
                        '   txtRPAranDdeRRGG.Valor = .Item("temp_inic_rp")
                        '   txtRPAranHtaRRGG.Valor = .Item("temp_fina_rp")
                        '   txtActaAranRRGG.Valor = .Item("temp_acta")
                        '   lblFechaAranRRGG.Text = .Item("temp_desc_fecha")
                        'End If
                        If Not .IsNull("temp_sobre_impo") Then
                            txtTotalSobreTasa1.Text = Format(.Item("temp_sobre_impo"), "###0.00")
                            txtTasaSobreTasa.Text = Format(.Item("temp_sobre_porc"), "###0.00")
                        Else
                            txtTotalSobreTasa1.Text = "0.00"
                            txtTasaSobreTasa.Text = "0.00"
                        End If
                        hdnTotalSobreTasa1.Text = txtTotalSobreTasa1.Text
                        hdnTasaSobreTasa.Text = txtTasaSobreTasa.Text

                        txtCantAran.Valor = .Item("temp_cant")
                        ' txtPUnitAran.Valor = .Item("temp_unit_impo")
                        ' txtImpoAran.Valor = .Item("temp_impo")
                        chkExentoAran.Checked = IIf(.Item("temp_exen") = "Si", 1, 0)

                        ' laboratorios - turnos
                        If mobj.pLabora Then
                            Select Case .Item("temp_turnos").ToString
                                Case "B"
                                    hdnDatosPopTurnoBov.Text = .Item("temp_rela_id")
                                Case "E"
                                    hdnDatosPopTurnoEqu.Text = .Item("temp_rela_id")
                            End Select
                        End If

                        mLimpiarDetaConcep(False)
                        mMostrarSolapaArancel(True)
                        'precio unitario sin iva
                        txtPUnitAranSIVA.Valor = Format(.Item("temp_unit_impo"), "###0.00")
                        'muestra el precio unitario con o sin IVA segun la cond. del cliente 
                        If usrClie.DiscrimIVA Then
                            txtPUnitAran.Valor = Format(.Item("temp_unit_impo"), "###0.00")
                        Else
                            txtPUnitAran.Valor = Format(.Item("temp_unit_c_iva"), "###0.00")
                        End If
                        'tasa iva
                        hdnTasaIVAAran.Text = .Item("temp_tasa_iva")
                        'importe total sin iva
                        hdlImpoIvaAran.Text = CType(.Item("temp_impo_ivai"), Double) - CType(.Item("temp_impo"), Double)
                        If usrClie.DiscrimIVA Then
                            'tasa iva
                            txtTasaIVAAran.Valor = Format(CType(.Item("temp_tasa_iva"), Double), "###0.00")
                            'importe iva
                            txtImpoIvaAran.Valor = Format(CType(hdlImpoIvaAran.Text, Double), "###0.00")
                            'importe total 
                            txtImpoAran.Valor = Format(CType(.Item("temp_impo"), Double), "###0.00")
                        Else
                            'importe total neto
                            txtImpoAran.Valor = Format(CType(.Item("temp_impo_ivai"), Double), "###0.00")

                        End If
                        txtDescAmpliAran.Valor = .Item("temp_desc_ampl")
                        mSetearEditor(mstrTablaDetaAran, False)
                    Else ' concepto
                        hdnDetaTempTipo.Text = .Item("temp_tipo")
                        cmbConcep.Valor = .Item("temp_aran_concep_id")
                        mCargarCentroCostos(True)
                        cmbCCosConcep.Valor = IIf(.Item("temp_ccos_id") Is DBNull.Value, "", .Item("temp_ccos_id"))
                        txtImporteConcep.Valor = IIf(.Item("temp_impo") Is DBNull.Value, "", .Item("temp_impo"))
                        txtDescAmpliConcep.Valor = IIf(.Item("temp_desc_ampl") Is DBNull.Value, "", .Item("temp_desc_ampl"))
                        If Not .IsNull("temp_sobre_impo") Then
                            txtTotalSobreTasaC.Text = Format(.Item("temp_sobre_impo"), "###0.00")
                            txtTasaSobreTasaC.Text = Format(.Item("temp_sobre_porc"), "###0.00")
                        Else
                            txtTotalSobreTasaC.Text = "0.00"
                            txtTasaSobreTasaC.Text = "0.00"
                        End If
                        hdnTotalSobreTasaC.Text = txtTotalSobreTasaC.Text
                        hdnTasaSobreTasaC.Text = txtTasaSobreTasaC.Text
                        'tasa iva
                        hdnTasaIVAConcep.Text = .Item("temp_tasa_iva")
                        'importe total sin iva
                        hdnImpoIvaConcep.Text = CType(.Item("temp_impo_ivai"), Double) - CType(.Item("temp_impo"), Double)
                        If usrClie.DiscrimIVA Then
                            'tasa iva
                            txtTasaIVAConcep.Valor = Format(CType(.Item("temp_tasa_iva"), Double), "###0.00")
                            'importe iva
                            txtImpoIvaConcep.Valor = Format(CType(hdnImpoIvaConcep.Text, Double), "###0.00")
                        End If
                        mCalcularImporteConceptos("P")
                        mLimpiarDetaAran(False)
                        mMostrarSolapaArancel(False)
                        mSetearEditor(mstrTablaDetaConcep, False)
                    End If

                End With


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosCuotas(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try

                Dim ldrCuotas As DataRow
                hdnCuotasId.Text = E.Item.Cells(1).Text
                ldrCuotas = mdsDatos.Tables(mstrTablaVtos).Select("covt_id=" & hdnCuotasId.Text)(0)

                With ldrCuotas
                    txtCuotFecha.Fecha = .Item("covt_fecha")
                    txtCuotPorc.Valor = .Item("covt_porc")
                    txtCuotImporte.Valor = .Item("covt_impo")

                End With

                mSetearEditor(mstrTablaVtos, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


#End Region

#Region "Opciones de ABM"

        Public Sub mCrearDataSet(ByVal pstrId As String)

            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrTablaDeta
            mdsDatos.Tables(2).TableName = mstrTablaDetaAran
            mdsDatos.Tables(3).TableName = mstrTablaDetaConcep
            mdsDatos.Tables(4).TableName = mstrTablaVtos
            mdsDatos.Tables(5).TableName = mstrTablaAutor

            mdsDatos.Tables.Add(mstrTablaTempDeta)
            With mdsDatos.Tables(mstrTablaTempDeta)

                Dim lintCol As DataColumn = .Columns.Add("temp_id", Type.GetType("System.Int32"))
                lintCol.AllowDBNull = False
                lintCol.Unique = True
                lintCol.AutoIncrement = True
                lintCol.AutoIncrementSeed = 1
                lintCol.AutoIncrementStep = 1

                .Columns.Add("temp_tipo", System.Type.GetType("System.String"))
                .Columns.Add("temp_codi_aran_concep", System.Type.GetType("System.String"))
                .Columns.Add("temp_desc_aran_concep", System.Type.GetType("System.String"))
                .Columns.Add("temp_desc_aran_concep_cortada", System.Type.GetType("System.String"))
                .Columns.Add("temp_cant", System.Type.GetType("System.Double"))
                .Columns.Add("temp_cant_sin_cargo", System.Type.GetType("System.Double"))
                .Columns.Add("temp_anim_no_fact", System.Type.GetType("System.Double"))
                .Columns.Add("temp_unit_impo", System.Type.GetType("System.Double"))
                .Columns.Add("temp_impo_Nosocio_aran", System.Type.GetType("System.Double"))
                .Columns.Add("temp_impo", System.Type.GetType("System.Double"))
                .Columns.Add("temp_fecha", System.Type.GetType("System.DateTime"))
                .Columns.Add("temp_inic_rp", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_fina_rp", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_tram", System.Type.GetType("System.String"))
                .Columns.Add("temp_acta", System.Type.GetType("System.String"))
                .Columns.Add("temp_ccos_codi", System.Type.GetType("System.String"))
                .Columns.Add("temp_exen", System.Type.GetType("System.String"))
                .Columns.Add("temp_sin_carg", System.Type.GetType("System.String"))
                .Columns.Add("temp_aran_concep_id", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_ccos_id", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_impo_bruto", System.Type.GetType("System.Double"))
                .Columns.Add("temp_tipo_IVA", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_tasa_iva", System.Type.GetType("System.Double"))
                .Columns.Add("temp_impo_ivai", System.Type.GetType("System.Double"))
                .Columns.Add("temp_desc_ampl", System.Type.GetType("System.String"))
                .Columns.Add("temp_auto", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_desc_fecha", System.Type.GetType("System.String"))
                .Columns.Add("temp_rela_id", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_borra", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_orden", System.Type.GetType("System.Int32"))
                .Columns.Add("temp_turnos", System.Type.GetType("System.String"))
                .Columns.Add("temp_unit_s_iva", System.Type.GetType("System.Double"))
                .Columns.Add("temp_unit_c_iva", System.Type.GetType("System.Double"))
                .Columns.Add("temp_sobre_impo", System.Type.GetType("System.Double"))
                .Columns.Add("temp_sobre_porc", System.Type.GetType("System.Double"))
            End With

            If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
                mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
            End If

            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta)
            grdDetalle.DataBind()

            grdCuotas.DataSource = mdsDatos.Tables(mstrTablaVtos)
            grdCuotas.DataBind()

            Session(mstrTabla) = mdsDatos
        End Sub
        Private Sub mBajaItemTempDeta(ByVal pintId As Integer, Optional ByVal pboolRemove As Boolean = False)
            With mdsDatos.Tables(mstrTablaTempDeta)
                If pboolRemove Then
                    ' elimina definitivamente la fila (es para sobretasas o conceptos eliminados por el programa, no por el usuario)
                    .Rows.Remove(.Select("temp_id=" & pintId)(0))
                Else
                    .Select("temp_id=" & pintId)(0).Item("temp_borra") = 1
                End If
            End With

            mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta).DefaultView
            grdDetalle.DataBind()
            mCalcularTotal()

            mCalcularSaldoCuotas()

        End Sub

        Private Sub mAlta()
            Dim lDsDatos As DataSet
            Dim lstrId As String

            'mCargarAutorizaciones()
            mCargarGrillaArancelesConceptos()
            mSeleccionarTipoPagoParaCalculo()

            lDsDatos = mGuardarDatos()
            If cmbComprobante.Valor = TiposComprobantes.NotaCredito Then
                lDsDatos = mCargarSaldosACuenta(lDsDatos)
            End If
            Dim lobj As New SRA_Neg.FacturacionABM(mstrConn, Session("sUserId").ToString(), mstrTabla, lDsDatos)
            lstrId = lobj.Alta(cmbComprobante.Valor = TiposComprobantes.NotaCredito)
            hdnCompGenera.Text = mobj.NroComprobanteGenerado(mstrConn, lstrId)
            hdnImprimir.Text = lstrId

        End Sub

        Private Function mGuardarDatos() As DataSet
            Dim ldrDatos As DataRow
            Dim ldsDatos As DataSet
            Dim lstrPrefCab As String = "comp"
            Dim lstrPrefDeta As String = "code"
            Dim lstrPrefDetaAran As String = "coan"
            Dim lstrPrefDetaConcep As String = "coco"
            Dim lstrTabla As String = mstrTabla
            Dim lstrTablaDeta As String = mstrTablaDeta
            Dim lstrTablaDetaAran As String = mstrTablaDetaAran
            Dim lstrTablaDetaConcep As String = mstrTablaDetaConcep
            Dim lstrTablaAutor As String = mstrTablaAutor
            Dim lintComprob As Integer

            If mobj.pAutorizaciones Then
                mobj.GuardarAutorizaciones(mdsDatos, grdAutoriza)
            End If

            mValidarDatos()
            mCargarCuotaUnica()

            ldsDatos = mdsDatos.Copy
            ldsDatos.Tables.Remove(mstrTablaTempDeta)

            lintComprob = cmbComprobante.Valor

            With ldsDatos.Tables(lstrTabla).Rows(0)
                .Item(lstrPrefCab + "_id") = -1
                .Item(lstrPrefCab + "_fecha") = txtFechaValor.Fecha
                .Item(lstrPrefCab + "_clie_id") = usrClie.Valor
                .Item(lstrPrefCab + "_dh") = IIf(lintComprob = TiposComprobantes.NotaCredito, 1, 0) ' debe o haber
                .Item(lstrPrefCab + "_neto") = txtTotNeto.Valor
                .Item(lstrPrefCab + "_cance") = IIf(txtTotNeto.Valor = 0, 1, 0)  ' cancelado o no
                .Item(lstrPrefCab + "_cs") = 1
                .Item(lstrPrefCab + "_coti_id") = lintComprob
                .Item(lstrPrefCab + "_mone_id") = 1 ' tipo de moneda $
                .Item(lstrPrefCab + "_cemi_nume") = mobj.pCentroEmisorNro
                .Item(lstrPrefCab + "_emct_id") = mobj.pCentroEmisorId
                mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
                .Item(lstrPrefCab + "_letra") = mobj.pLetraComprob
                .Item(lstrPrefCab + "_cpti_id") = mintCtaCte ' hdnFormaPago.Text si se respeta el tipo de pago que se utiliza para el calculo del arancel
                .Item(lstrPrefCab + "_ivar_fecha") = txtFechaIva.Fecha
                .Item(lstrPrefCab + "_acti_id") = cmbActi.Valor
                .Item(lstrPrefCab + "_impre") = 0 ' impresa o no
                .Item(lstrPrefCab + "_host") = mobj.pHost  ' nombre del host
                .Item(lstrPrefCab + "_modu_id") = SRA_Neg.Constantes.Modulos.Facturacion
            End With

            ldsDatos.Tables(lstrTablaDeta).Select()
            ldrDatos = ldsDatos.Tables(lstrTablaDeta).NewRow()
            ' guarda los datos de la tabla comprob_deta
            With ldrDatos
                .Item(lstrPrefDeta + "_id") = -1
                .Item(lstrPrefDeta + "_" + lstrPrefCab + "_id") = -1
                .Item(lstrPrefDeta + "_conv_id") = IIf(hdnConvId.Text = "", DBNull.Value, hdnConvId.Text)
                .Item(lstrPrefDeta + "_tarj_id") = DBNull.Value
                .Item(lstrPrefDeta + "_tarj_cuot") = DBNull.Value
                .Item(lstrPrefDeta + "_tarj_vtat") = DBNull.Value
                .Item(lstrPrefDeta + "_coti") = txtCotDolar.Valor
                .Item(lstrPrefDeta + "_pers") = DBNull.Value
                .Item(lstrPrefDeta + "_raza_id") = IIf(cmbRaza.Valor = "", DBNull.Value, cmbRaza.Valor)  'raza
                .Item(lstrPrefDeta + "_cria_id") = IIf(hdnCria.Text = "", DBNull.Value, hdnCria.Text)   'criador
                .Item(lstrPrefDeta + "_impo_ivat_tasa") = txtTotIVA.Valor
                .Item(lstrPrefDeta + "_impo_ivat_tasa_redu") = txtTotIVARedu.Valor
                .Item(lstrPrefDeta + "_impo_ivat_tasa_sobre") = txtTotIVASTasa.Valor
                .Item(lstrPrefDeta + "_obse") = txtObser.Valor
                .Item(lstrPrefDeta + "_reci_nyap") = txtNombApel.Valor
                .Item(lstrPrefDeta + "_asoc_comp_id") = cmbComprobanteAsoc.Valor
                If cmbExpo.Valor.Length > 0 Then
                    .Item(lstrPrefDeta + "_expo_id") = cmbExpo.Valor
                End If
                .Table.Rows.Add(ldrDatos)
            End With

            'copia los datos de la tabla temporal en la tabla de comprob_aran y comprob_conceptos segun corresponda
            'no pasa los eliminados 
            mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
            For Each ldr As DataRowView In mdsDatos.Tables(mstrTablaTempDeta).DefaultView
                With ldr
                    If .Item("temp_tipo") = "A" Then  'arancel
                        ldrDatos = ldsDatos.Tables(lstrTablaDetaAran).NewRow()
                        ldrDatos.Item(lstrPrefDetaAran + "_id") = clsSQLServer.gObtenerId(ldrDatos.Table, lstrPrefDetaAran + "_id")
                        ldrDatos.Item(lstrPrefDetaAran + "_" + lstrPrefCab + "_id") = -1
                        ldrDatos.Item(lstrPrefDetaAran + "_aran_id") = .Item("temp_aran_concep_id")
                        ldrDatos.Item(lstrPrefDetaAran + "_ccos_id") = .Item("temp_ccos_id")
                        ldrDatos.Item(lstrPrefDetaAran + "_cant") = .Item("temp_cant")
                        If usrClie.DiscrimIVA Then
                            ldrDatos.Item(lstrPrefDetaAran + "_unit_impo") = .Item("temp_unit_impo")
                        Else
                            ldrDatos.Item(lstrPrefDetaAran + "_unit_impo") = .Item("temp_unit_c_iva")
                        End If
                        ldrDatos.Item(lstrPrefDetaAran + "_impo") = .Item("temp_impo")
                        ldrDatos.Item(lstrPrefDetaAran + "_exen") = IIf(.Item("temp_exen") = "Si", 1, 0)
                        ldrDatos.Item(lstrPrefDetaAran + "_sin_carg") = 0
                        ldrDatos.Item(lstrPrefDetaAran + "_impo_ivai") = .Item("temp_impo_ivai")
                        ldrDatos.Item(lstrPrefDetaAran + "_tasa_iva") = .Item("temp_tasa_iva")
                        ldrDatos.Item(lstrPrefDetaAran + "_desc_ampl") = .Item("temp_desc_ampl")
                        ldrDatos.Table.Rows.Add(ldrDatos)
                    Else
                        ldrDatos = ldsDatos.Tables(lstrTablaDetaConcep).NewRow()
                        ldrDatos.Item(lstrPrefDetaConcep + "_id") = clsSQLServer.gObtenerId(ldrDatos.Table, lstrPrefDetaConcep + "_id")
                        ldrDatos.Item(lstrPrefDetaConcep + "_" + lstrPrefCab + "_id") = -1
                        ldrDatos.Item(lstrPrefDetaConcep + "_conc_id") = .Item("temp_aran_concep_id")
                        ldrDatos.Item(lstrPrefDetaConcep + "_ccos_id") = .Item("temp_ccos_id")
                        ldrDatos.Item(lstrPrefDetaConcep + "_impo") = .Item("temp_impo")
                        ldrDatos.Item(lstrPrefDetaConcep + "_impo_ivai") = .Item("temp_impo_ivai")
                        ldrDatos.Item(lstrPrefDetaConcep + "_tasa_iva") = .Item("temp_tasa_iva")
                        ldrDatos.Item(lstrPrefDetaConcep + "_desc_ampl") = .Item("temp_desc_ampl")
                        ldrDatos.Item(lstrPrefDetaConcep + "_auto") = .Item("temp_auto")
                        ldrDatos.Table.Rows.Add(ldrDatos)
                    End If
                End With
            Next

            Return ldsDatos

        End Function
        Private Sub mAvisoCCostoDesc()
            'si es un descuento
            If txtImporteConcep.Valor < 0 Then
                'si es distinto el cod de c. costo a 06  
                If mobj.CentroCostoDistDesc(cmbCCosConcep.Valor) Then
                    AccesoBD.clsError.gGenerarMensajes(Me, "El Centro de Costo seleccionado no comienza con 06")
                End If
            End If
        End Sub

        Private Sub mGuardarDatosDeta_concep()
            mValidarConceptos("T")
            'mAvisoCCostoDesc()
            Dim lstrConcepto As String = cmbConcep.SelectedItem.ToString
            Dim ldrDatos As DataRow
            Dim lstrCod As String
            If hdnDetaTempId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id = " & hdnDetaTempId.Text)(0)
                mValidarConceptosAutomaticos(CType(IIf(ldrDatos.IsNull("temp_auto"), 0, ldrDatos.Item("temp_auto")), Integer))
            End If
            With ldrDatos
                .Item("temp_tipo") = "C"
                .Item("temp_aran_concep_id") = cmbConcep.Valor
                .Item("temp_codi_aran_concep") = mobj.ObtenerValorCampo(mstrConn, "conceptos", "conc_codi", cmbConcep.Valor.ToString)
                .Item("temp_desc_aran_concep") = cmbConcep.SelectedItem
                lstrConcepto = mobj.CortarCadena(lstrConcepto, 30)
                If mobj.pRRGG Then
                    lstrConcepto = "C" + "-" + Trim(.Item("temp_codi_aran_concep")) + "-" + lstrConcepto
                End If

                .Item("temp_desc_aran_concep_cortada") = lstrConcepto
                .Item("temp_tipo_IVA") = mobj.ObtenerValorCampo(mstrConn, "conceptosX", "tipo_tasa", cmbConcep.Valor.ToString)
                ' importe sin iva
                .Item("temp_impo") = txtImporteConcep.Valor
                'importe con iva
                If usrClie.DiscrimIVA Then
                    .Item("temp_impo_ivai") = (txtImporteConcep.Valor + CType(hdnImpoIvaConcep.Text, Double))
                Else
                    .Item("temp_impo_ivai") = txtImporteConcep.Valor
                End If
                'tasa iva
                .Item("temp_tasa_iva") = CType(hdnTasaIVAConcep.Text, Double)
                If hdnTotalSobreTasaC.Text = "" Then
                    .Item("temp_sobre_impo") = DBNull.Value
                Else
                    .Item("temp_sobre_impo") = hdnTotalSobreTasaC.Text
                End If
                If hdnTasaSobreTasaC.Text = "" Then
                    .Item("temp_sobre_porc") = DBNull.Value
                Else
                    .Item("temp_sobre_porc") = hdnTasaSobreTasaC.Text
                End If
                'If Not cmbCCosConcep.Valor Is DBNull.Value Then
                If (cmbCCosConcep.Text.Trim().Length > 0) Then
                    .Item("temp_ccos_codi") = mobj.ObtenerValorCampo(mstrConn, "centrosc", "ccos_codi", cmbCCosConcep.Valor.ToString)
                    .Item("temp_ccos_id") = cmbCCosConcep.Valor
                End If
                .Item("temp_desc_ampl") = txtDescAmpliConcep.Valor
                .Item("temp_auto") = 0 ' si el desc, es automatico o no (lo trae la formula)
                .Item("temp_orden") = OrdenItems.conceptos
            End With

            If hdnDetaTempId.Text = "" Then
                mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
            End If

        End Sub

        Private Sub mGuardarDatosDeta_aran()
            mValidarAranceles()
            mArancel()
            Dim lstrArancel As String = cmbAran.SelectedItem.ToString
            Dim ldrDatos As DataRow
            Dim lintIdDeta As Integer
            If hdnDetaTempId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id = " & hdnDetaTempId.Text)(0)
                mobj.SobretasasDelArancelNormal(mstrConn, cmbAran.Valor, cmbActi.Valor, txtFechaIva.Fecha)
            End If
            With ldrDatos
                .Item("temp_tipo") = "A"
                .Item("temp_aran_concep_id") = cmbAran.Valor
                .Item("temp_codi_aran_concep") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_codi", cmbAran.Valor.ToString)
                .Item("temp_desc_aran_concep") = cmbAran.SelectedItem
                lstrArancel = mobj.CortarCadena(lstrArancel, 30)
                .Item("temp_desc_aran_concep_cortada") = lstrArancel
                .Item("temp_cant") = txtCantAran.Valor
                .Item("temp_cant_sin_cargo") = 0

                If hdnTotalSobreTasa1.Text = "" Then
                    .Item("temp_sobre_impo") = DBNull.Value
                Else
                    .Item("temp_sobre_impo") = hdnTotalSobreTasa1.Text
                End If
                If hdnTasaSobreTasa.Text = "" Then
                    .Item("temp_sobre_porc") = DBNull.Value
                Else
                    .Item("temp_sobre_porc") = hdnTasaSobreTasa.Text
                End If

                .Item("temp_tipo_IVA") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "tipo_tasa", cmbAran.Valor.ToString)
                'tasa iva
                .Item("temp_tasa_iva") = CType(hdnTasaIVAAran.Text, Double)
                'precio unitario sin iva
                .Item("temp_unit_impo") = txtPUnitAranSIVA.Text
                'precio unitario con iva
                .Item("temp_unit_c_iva") = txtPUnitAranSIVA.Text * (1 + (CType(hdnTasaIVAAran.Text, Double) / 100))
                .Item("temp_desc_ampl") = txtDescAmpliAran.Valor
                If usrClie.DiscrimIVA Then
                    'importe sin iva
                    .Item("temp_impo") = txtImpoAran.Text
                    'importe con iva
                    .Item("temp_impo_ivai") = txtImpoAran.Text + CType(hdlImpoIvaAran.Text, Double)
                Else
                    'importe sin iva
                    '.Item("temp_impo") = txtImpoAran.Valor - CType(hdlImpoIvaAran.Text, Double)
                    'importe con iva
                    .Item("temp_impo_ivai") = txtImpoAran.Valor
                    .Item("temp_impo") = txtImpoAran.Valor
                End If

                .Item("temp_ccos_codi") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "ccos_codi", cmbAran.Valor.ToString)
                .Item("temp_ccos_id") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "ccos_id", cmbAran.Valor.ToString)
                .Item("temp_exen") = IIf(chkExentoAran.Checked(), "Si", "No")
                .Item("temp_orden") = OrdenItems.arancel

                'precio No Socio para luego generar el concepto adicional
                mobj.pPrecioNosocio = True
                Dim lstrFiltro As String
                lstrFiltro = cmbAran.Valor.ToString() + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" + ""
                Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                mobj.pPrecioNosocio = False
                If usrClie.DiscrimIVA Then
                    .Item("temp_impo_Nosocio_aran") = vsRet(1)
                Else
                    .Item("temp_impo_Nosocio_aran") = vsRet(4)
                End If

                If hdnDetaTempId.Text = "" Then
                    mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
                    lintIdDeta = ldrDatos.Item("temp_id")
                End If

                'elimino los conceptos automaticos que representan varios aranceles
                mBorrar_Conceptos()
                mConceptoAutoAdicionalNoSocio()

                'bloquea raza y criador si el arancel tiene raza
                If cmbActi.Valor = Actividades.Laboratorio Or cmbActi.Valor = Actividades.RRGG Then
                    Dim lstrRazaid As String = mobj.ObtenerValorCampo(mstrConn, "aranceles", "aran_raza_id", cmbAran.Valor.ToString)
                    If lstrRazaid <> "" Then mBloqueacontrolesRaza()
                End If
            End With

        End Sub

        Private Sub mBorrar_Conceptos()
            'conceptos para varios aranceles (ej adic por precio No socio)
            For Each ldr As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select
                If ldr.Item("temp_rela_id") Is DBNull.Value And Not (ldr.Item("temp_auto") Is DBNull.Value) Then
                    If ldr.Item("temp_auto") = 1 Then mBajaItemTempDeta(ldr.Item("temp_id"), True)
                End If
            Next
        End Sub

        Private Sub mConceptoAutoAdicionalNoSocio()
            Dim ldrDatos As DataRow
            Dim lbooAdicional As Boolean
            lbooAdicional = (clsSQLServer.gCampoValorConsul(mstrConn, "clientes_alertas_especiales_consul " & usrClie.Valor.ToString(), "adicional") = "S")
            If lbooAdicional And mobj.pEspecialesTodo(10) = "Si" Then
                ' Dario 2013-04-26 (cambio se agrega otro parametro lboolPrecioSocio para ver si en los
                '                   recargos se aplica la diferencia sobre precio no socio o adherente)
                Dim dt As DataTable = mobj.ConceptosAutoma(mdsDatos, mstrConn, usrClie.DiscrimIVA, Convert.ToInt32(cmbActi.Valor), False)
                If dt Is Nothing Then Exit Sub
                mCargarConcepAuto(dt)
            End If
        End Sub

        Private Sub mCargarConcepAuto(ByVal pdt As DataTable)
            'carga el concepto automatico en el ds detalle
            Dim ldrDatos As DataRow
            For Each ldr As DataRow In pdt.Select
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
                With ldrDatos
                    .Item("temp_tipo") = "C"
                    .Item("temp_aran_concep_id") = ldr.Item("temp_aran_concep_id")
                    .Item("temp_codi_aran_concep") = ldr.Item("temp_codi_aran_concep")
                    .Item("temp_desc_aran_concep") = ldr.Item("temp_desc_aran_concep")
                    .Item("temp_desc_aran_concep_cortada") = ldr.Item("temp_desc_aran_concep_cortada")
                    'Roxi: 02/10/2007: los conceptos con formula "Adicional por suspension de servicios" llevan IVA.
                    If clsSQLServer.gObtenerValorCampo(mstrConn, "conceptos", ldr.Item("temp_aran_concep_id"), "conc_form_id") = SRA_Neg.Constantes.Formulas.Adicional_por_suspension_de_servicios Then
                        .Item("temp_tipo_IVA") = 1
                        .Item("temp_impo") = ldr.Item("temp_impo")
                        If hdnTasaIVAAran.Text <> "" Then
                            .Item("temp_impo_ivai") = ldr.Item("temp_impo") * (1 + CDbl(hdnTasaIVAAran.Text) / 100)
                            .Item("temp_tasa_iva") = CDbl(hdnTasaIVAAran.Text)
                        Else
                            .Item("temp_impo_ivai") = 0
                            .Item("temp_tasa_iva") = 0
                        End If
                    Else
                        .Item("temp_tipo_IVA") = ldr.Item("temp_tipo_IVA")
                        .Item("temp_impo") = ldr.Item("temp_impo")
                        .Item("temp_impo_ivai") = ldr.Item("temp_impo_ivai")
                        .Item("temp_tasa_iva") = ldr.Item("temp_tasa_iva")
                    End If
                    .Item("temp_ccos_codi") = ldr.Item("temp_ccos_codi")
                    .Item("temp_ccos_id") = ldr.Item("temp_ccos_id")
                    .Item("temp_desc_ampl") = ldr.Item("temp_desc_ampl")
                    .Item("temp_auto") = ldr.Item("temp_auto")
                    ' .Item("temp_rela_id") =  'ldr.Item("temp_rela_id")
                    .Item("temp_orden") = OrdenItems.conceptosAuto
                    mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
                End With
            Next
        End Sub

        Private Function mValidarNulos(ByVal pstrCampo As String, ByVal pboolnull As Boolean) As String
            Dim lstrRt As String = pstrCampo
            If pstrCampo Is DBNull.Value Then If pboolnull Then lstrRt = "" Else lstrRt = "0"
            Return lstrRt
        End Function


        Private Sub mGuardarDatos_cuotas(Optional ByVal lboolAuto As Boolean = False)

            Dim ldrDatos As DataRow
            If Not lboolAuto Then
                mValidarCuotas()
            End If
            If hdnCuotasId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaVtos).NewRow
                ldrDatos.Item("covt_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaVtos), "covt_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaVtos).Select("covt_id=" & hdnCuotasId.Text)(0)
            End If
            Dim lDsDatos As DataSet

            With ldrDatos
                If lboolAuto Then

                    Dim ldatFecha As Date

                    'NC -> la fecha de vto es la misma que la de ingreso
                    'al deshabilitar las cuotas en NC esto no se usa mas (11/12/06)
                    If cmbComprobante.Valor = TiposComprobantes.NotaCredito Then
                        ldatFecha = Today
                    Else
                        ldatFecha = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, cmbActi.Valor, Today)
                    End If

                    .Item("covt_fecha") = ldatFecha
                    .Item("covt_porc") = 0
                    .Item("covt_impo") = txtTotNeto.Valor
                    .Item("covt_cance") = IIf(txtTotNeto.Valor = 0, 1, 0)
                Else
                    mobj.CalcularValoresCuota(lblImpTotal.Text, IIf(txtCuotPorc.Valor.Length = 0, 0, txtCuotPorc.Valor), IIf(txtCuotImporte.Valor.Length, 0, txtCuotImporte.Valor))
                    'si es cta. cte no puede tener una cuota en cero
                    If mobj.pPagoCtaCte And mobj.pCuotaImporte = 0 Then Exit Sub

                    .Item("covt_fecha") = txtCuotFecha.Fecha
                    .Item("covt_porc") = mobj.pCuotaPorcentaje
                    .Item("covt_impo") = mobj.pCuotaImporte
                    .Item("covt_cance") = 0
                End If

            End With
            If (mEstaEnElDataSetFecha(mdsDatos.Tables(mstrTablaVtos), ldrDatos, "covt_fecha", "covt_id")) Then
                If Not lboolAuto Then Throw New AccesoBD.clsErrNeg("Ya existe una cuota con esa fecha.")
            Else
                If hdnCuotasId.Text = "" Then
                    mdsDatos.Tables(mstrTablaVtos).Rows.Add(ldrDatos)
                End If
            End If



        End Sub




        Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrDatos As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
            Dim filtro As String
            filtro = campo1 + " = " + ldrDatos.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrDatos.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrDatos.Item(campoId).ToString()
            Return (table.Select(filtro).GetLength(0) > 0)
        End Function
        Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrDatos As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
            Return mEstaEnElDataSet(table, ldrDatos, campo1, campo1, campoId)
        End Function
        Private Function mEstaEnElDataSetFecha(ByVal table As System.Data.DataTable, ByVal ldrDatos As System.Data.DataRow, ByVal campofecha As String, ByVal campoId As String) As Boolean
            Dim filtro As String

            ' filtro = campofecha + " = '" + CDate(ldrDatos.Item(campofecha)).ToString("MM/dd/yyyy") + "' AND " + campoId + " <> " + ldrDatos.Item(campoId).ToString()
            filtro = campofecha + " = '" + ldrDatos.Item(campofecha) + "' AND " + campoId + " <> " + ldrDatos.Item(campoId).ToString()

            Return (table.Select(filtro).GetLength(0) > 0)
        End Function

        Public Sub mConsultar(ByVal pOk As Boolean)
            Try
                Dim strFin As String = ""
                If Not pOk Then
                    strFin = ",@ejecuta = N "
                End If
                mstrCmd = "exec " + mstrTabla + "_consul " + strFin
                'clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mActualizarCuotas(Optional ByVal lboolAuto As Boolean = False)

            mGuardarDatos_cuotas(lboolAuto)
            mCalcularSaldoCuotas()

            mLimpiarCuotas()
            With mdsDatos.Tables(mstrTablaVtos)
                .DefaultView.Sort = "covt_fecha"
                grdCuotas.DataSource = .DefaultView
            End With
            grdCuotas.DataBind()

        End Sub

        Private Sub mCargarGrillaArancelesConceptos()
            mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = "temp_orden"
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta).DefaultView
            grdDetalle.DataBind()
            mCalcularTotal()
        End Sub
        Private Sub mActualizarAranceles()
            Dim lintId As Integer
            mGuardarDatosDeta_aran()
            mBloqueacontrolesRaza()
            mLimpiarDetaAran()
            mCargarGrillaArancelesConceptos()

        End Sub
        Private Sub mBloqueacontrolesRaza()
            If cmbActi.Valor = Actividades.RRGG Or cmbActi.Valor = Actividades.Laboratorio Then
                If Not cmbRaza.Valor Is DBNull.Value Then
                    cmbRaza.Enabled = False
                    txtCriaNume.Enabled = False
                End If
            End If
        End Sub

        Dim lintId As Integer

        Private Sub mSeleccionarTipoPagoParaCalculo()
            'si no se selecciona un comprobante, es contado (para el calculo)
            'de lo contrario se toma el tipo de pago del comp seleccionado
            '03/10/06
            If cmbComprobanteAsoc.Valor = 0 Or cmbComprobanteAsoc.Valor Is DBNull.Value Then
                hdnFormaPago.Text = mintContado
            Else
                hdnFormaPago.Text = mobj.ObtenerValorCampo(mstrConn, "comprobantes", "comp_cpti_id", cmbComprobanteAsoc.Valor.ToString)
            End If
            hdnFormaPago.Text = mintContado
        End Sub
        Private Sub mActualizarConceptos()


            mGuardarDatosDeta_concep()
            mLimpiarDetaConcep()
            mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = "temp_orden"
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta).DefaultView
            grdDetalle.DataBind()
            mCalcularTotal()


        End Sub


#End Region

#Region "Eventos de Controles"


        Private Sub mArancel()
            If Not cmbAran.Valor Is DBNull.Value Then
                mSetearDatosArancel()
                mCalcularPrecioUnitArancel()
                mCalcularImporteAranceles()
            End If
        End Sub

        Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
            Try
                ' si no cancela la impresion
                If CBool(CInt(hdnImprimio.Text)) Then
                    mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnImprimir.Text)

                    With mdsDatos.Tables(0)
                        .TableName = mstrTabla
                        .Rows(0).Item("comp_impre") = CBool(CInt(hdnImprimio.Text))
                    End With

                    While mdsDatos.Tables.Count > 1
                        mdsDatos.Tables.Remove(mdsDatos.Tables(1))
                    End While

                    Dim lobj As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

                    lobj.Modi()
                End If
                mAgregar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub btnAran_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAran.Click
            Try
                mShowTabs(Panta.Arancel)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnConcep_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConcep.Click
            Try
                mShowTabs(Panta.Concepto)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub



        Private Sub txtImporteConcep_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporteConcep.TextChanged
            Try
                mCalcularImporteConceptos("P")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub ImgbtnLimpiar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgbtnLimpiar.Click
            Try
                mAgregar()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnAltaCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCuot.Click
            Try
                mActualizarCuotas()
                mCalcularSaldoCuotas()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnModiCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCuot.Click
            Try
                mActualizarCuotas()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnLimpiarCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiarCuot.Click
            Try
                mLimpiarCuotas()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnBajaCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCuot.Click
            Try
                mdsDatos.Tables(mstrTablaVtos).Select("covt_id=" & hdnCuotasId.Text)(0).Delete()
                grdCuotas.DataSource = mdsDatos.Tables(mstrTablaVtos)
                grdCuotas.DataBind()
                mLimpiarCuotas()
                mCalcularSaldoCuotas()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnBajaConcep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaConcep.Click
            Try
                mBajaItemTempDeta(hdnDetaTempId.Text)
                mLimpiarDetaConcep()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnModiConcep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiConcep.Click
            Try
                mActualizarConceptos()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnBajaAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaAran.Click
            Try

                mBajaItemTempDeta(hdnDetaTempId.Text)

                mLimpiarDetaAran()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub ImgbtnGenerar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgbtnGenerar.Click
            Try
                mRecalculaAranceles()
                mAlta()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnLimpAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpAran.Click
            mLimpiarDetaAran()

        End Sub
        Private Sub btnModiAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiAran.Click
            Try
                mActualizarAranceles()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnAltaAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaAran.Click
            Try
                mActualizarAranceles()

                mSetearEditor("", True)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)

            End Try
        End Sub
        Private Sub btnAltaConcep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaConcep.Click
            Try
                mActualizarConceptos()
                mSetearEditor("", True)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnLimpiarConcep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarConcep.Click

            mLimpiarDetaConcep()
        End Sub



        Private Sub mPasarSalopaCab(ByVal pstrPos As String)
            hdnUbic.Text = pstrPos
            hdnActiId.Text = cmbActi.Valor
            hdnCotDolar.Text = txtCotDolar.Valor
            hdnConv.Text = hdnConv.Text
            hdnClieDiscIVA.Text = usrClie.DiscrimIVA
            hdnClieId.Text = usrClie.Valor
            'hdnFormaPago.Text = mintCtaCte

        End Sub



#Region "Paneles"
        Private Sub mNaveg(ByVal pstrPos As String)

            mValidarNavega()
            mPasarSalopaCab(pstrPos)

            If cmbComprobanteAsoc.Valor <> 0 And txtFechaValor.Text <> hdnFValorAnterior.Text Then
                txtFechaValor.Text = hdnFValorAnterior.Text
            End If

            Select Case pstrPos
                Case "P"                                'principal
                    hdnActiId.Text = cmbActi.Valor
                    hdnFechaIva.Text = txtFechaIva.Text
                    mCargarAutorizaciones()
                    mSeleccionarTipoPagoParaCalculo()
                    If mobj.CambioActividad(cmbActi.Valor) Then
                        mLimpiar(True)
                        mCargarArancelesyConceptos()
                    Else
                        mCargarAranceles()
                    End If
                    mPrecioManual()
                    mTituloCabecera()
                    mControlesIVA()
                    mobj.PagoCtaCte(mintCtaCte)

                    mShowTabs(Panta.Arancel)

                    mobj.PrecioSocio_Falle(mstrConn, usrClie.Valor, txtFechaValor.Fecha)

                    mBloquearControlesCabe(True)
                    mRecalculaAranceles()
                Case "D-"
                    mShowTabs(Panta.Encabezado)

                Case "D+"                            'deta posterior
                    mCalcularTotal()
                    'cuotas
                    '11/12/06 solo ND
                    If mobj.pPagoCtaCte And (cmbComprobante.Valor <> TiposComprobantes.NotaCredito) Then
                        mShowTabs(Panta.Cuotas)
                    Else
                        mCargarAutorizaciones()
                        If mobj.pAutorizaciones() Then
                            'tiene autorizaciones
                            mShowTabs(Panta.Autorizacion)
                        Else
                            'va al pie (leyenda)  
                            mShowTabs(Panta.Pie)
                        End If
                    End If
                Case "C-"
                    mShowTabs(Panta.Arancel)
                    ' mCargarAutorizaciones()

                Case "C+"
                    ' mCargarAutorizaciones()
                    If mobj.pAutorizaciones() Then
                        'tiene autorizaciones
                        mShowTabs(Panta.Autorizacion)
                    Else
                        'va al pie (leyenda)  
                        mShowTabs(Panta.Pie)
                    End If
                Case "A+"
                    'va al pie (leyenda)  
                    mShowTabs(Panta.Pie)

                Case "A-"
                    'cuotas
                    '11/12/06 solo ND
                    If mobj.pPagoCtaCte And (cmbComprobante.Valor <> TiposComprobantes.NotaCredito) Then
                        mShowTabs(Panta.Cuotas)
                    Else
                        'detalle 
                        mShowTabs(Panta.Arancel)
                    End If

                Case "P-"                      'pie
                    If mobj.pAutorizaciones() Then
                        'tiene autorizaciones
                        mShowTabs(Panta.Autorizacion)
                    Else
                        'cuotas
                        '11/12/06 solo ND
                        If mobj.pPagoCtaCte And (cmbComprobante.Valor <> TiposComprobantes.NotaCredito) Then
                            mShowTabs(Panta.Cuotas)
                        Else
                            mShowTabs(Panta.Arancel)
                        End If
                    End If
                Case "P--"                     'al principal
                    mShowTabs(Panta.Encabezado)

            End Select

        End Sub

#Region "Eventos botones Naveg"

        Private Sub btnPosteriorDeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorDeta.Click
            Try
                mNaveg("D+")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnAnteriorDeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorDeta.Click
            mNaveg("D-")
        End Sub
        Private Sub btnSigCabe_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSigCabe.Click
            Try
                mNaveg("P")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub btnPosteriorAutoriza_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorAutoriza.Click
            Try
                mobj.GuardarAutorizaciones(mdsDatos, grdAutoriza)
                mNaveg("A+")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnAnteriorAutiza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorAutiza.Click
            Try
                mobj.GuardarAutorizaciones(mdsDatos, grdAutoriza)
                mNaveg("A-")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnAnteriorCuot_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorCuot.Click
            Try
                mNaveg("C-")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnPosteriorCuot_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorCuot.Click
            Try
                mNaveg("C+")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnPosteriorAutoriza_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
            Try
                mNaveg("A+")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub btnAnteriorPie_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorPie.Click
            mNaveg("P-")
        End Sub
        Private Sub btnEncabezadoPie_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEncabezadoPie.Click
            mNaveg("P--")
        End Sub
#End Region

#End Region

#End Region

#Region "Alertas/Ventanas-pop"



        Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
            Try
                If (hdnDatosPop.Text <> "") Then
                    Dim lvrDatos As String = hdnDatosPop.Text
                    txtObser.Valor = mobj.ObtenerValorCampo(mstrConn, "leyendas_factu", "lefa_desc", lvrDatos)
                    hdnDatosPop.Text = ""
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub hdnDatosCPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosCPop.TextChanged
            Try
                If hdnDatosCPop.Text = 1 Then
                    ' selecciona factura o proforma
                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            Finally
                hdnDatosCPop.Text = ""
            End Try
        End Sub


        Private Sub mValidarSeleccionCliente()
            If usrClie.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
            End If
        End Sub
        Private Sub mSetearControles()
            'para que no grabe mas de una vez
            btnAltaAran.Attributes.Add("onclick", "return GrabarUnaVez(this);")
            btnAltaConcep.Attributes.Add("onclick", "return GrabarUnaVez(this);")

            If mobj.pTarjetas Then
                DivimgTarj.Style.Add("display", "inline")
                Dim lstrImg As String = imgTarjTele.Src
                If clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_clientes_consul @tacl_clie_id=" + usrClie.Valor, "tacl_vta_tele") Then
                    lstrImg = lstrImg.Replace("CreditCardTele.gif", "CreditCard.gif")
                Else
                    lstrImg = lstrImg.Replace("CreditCard.gif", "CreditCardTele.gif")
                End If
                imgTarjTele.Src = lstrImg
            Else
                DivimgTarj.Style.Add("display", "none")
            End If
            divClieGene.Style.Add("display", IIf(usrClie.ClienteGenerico(mstrConn), "inline", "none"))

            divExpo.Style.Add("display", IIf(cmbActi.Valor = Actividades.Expo, "inline", "none"))


            mExposiciones()

            mEstadoSaldos()
            mEspeciales()
            mTitulo()
            'If hdnUbic.Text = "P" Then
            mArancel()
            'End If
            hdnFechaVigencia.Text = txtFechaValor.Fecha

            If hdnLoginPop.Text <> "" Then
                lblAutoUsuaTit.Text = clsSQLServer.gObtenerValorCampo(mstrConn, "usuarios", hdnLoginPop.Text, "usua_user")
            Else
                lblAutoUsuaTit.Text = ""
            End If
        End Sub
        Private Sub mExposiciones()
            Dim lstrExpo As String
            If Not cmbActi.Valor Is DBNull.Value Then
                If cmbActi.Valor = Actividades.Expo Then
                    If Not cmbComprobanteAsoc.Valor Is DBNull.Value Then
                        lstrExpo = mobj.ObtenerValorCampo(mstrConn, "comprob_deta", "code_expo_id", cmbComprobanteAsoc.Valor.ToString())
                        If lstrExpo <> "" Then
                            cmbExpo.Valor = lstrExpo
                            cmbExpo.Enabled = False
                        Else
                            cmbExpo.Enabled = True

                        End If
                    End If
                End If
            End If



        End Sub
        Private Sub mTitulo()
            If cmbComprobante.Valor = 0 Then
                lbltitulo.Text = "Nota de D�bito/Cr�dito"
            Else
                If cmbComprobante.Valor = TiposComprobantes.NotaCredito Then
                    lbltitulo.Text = "Nota de Cr�dito"
                Else
                    lbltitulo.Text = "Nota de D�bito"
                End If
            End If

        End Sub
        Private Sub mEstadoDeSaldos()
            mobj.EstadoSaldos(mstrConn, usrClie.Valor, txtFechaValor.Fecha)
        End Sub
        Private Sub mEstadoSaldos()
            DivimgAlertaSaldosAmarillo.Style.Add("display", "none")
            DivimgAlertaSaldosVerde.Style.Add("display", "none")
            DivimgAlertaSaldosRojo.Style.Add("display", "none")


            Select Case mobj.pEstadoSaldos
                Case "A"
                    DivimgAlertaSaldosAmarillo.Style.Add("display", "inline")
                Case "V"
                    DivimgAlertaSaldosVerde.Style.Add("display", "inline")
                Case "R"
                    DivimgAlertaSaldosRojo.Style.Add("display", "inline")
            End Select

        End Sub
        Private Sub mEspeciales()
            DivimgEspecVerde.Style.Add("display", "none")
            DivimgEspecRojo.Style.Add("display", "none")
            Select Case mobj.pEspeciales
                Case "V"
                    DivimgEspecVerde.Style.Add("display", "inline")
                Case "R"
                    DivimgEspecRojo.Style.Add("display", "inline")
            End Select
        End Sub



#End Region

#Region "Funciones-Procedimientos varios"
        Private Sub mCliente()
            txtSocio.Valor = ""
            txtSocio.Valor = usrClie.SociNro
            mEstadoSaldos()
            mEspeciales()

        End Sub

        Private Sub mCalcularSaldoCuotas()
            lblImpoSaldo.Text = mobj.CalcularSaldoCuotas(mdsDatos, lblImpTotal.Text)
        End Sub


        Private Sub mTituloCabecera()
            lblCabeCliente.Text = usrClie.txtApelExt.Text
            lblCabeActividad.Text = cmbActi.SelectedItem.Text
            lblCabeTPago.Text = "cta. cte."
        End Sub
        Private Sub mValidarNavega()
            If cmbComprobante.Valor Is DBNull.Value Or cmbComprobante.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el tipo de comprobante.")
            End If

            If cmbActi.Valor Is DBNull.Value Or cmbActi.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la actividad.")
            End If

            If usrClie.Valor Is DBNull.Value Or Me.usrClie.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente.")
            End If

            If txtFechaIva.Fecha Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de referencia de I.V.A.")
            End If
            If txtFechaValor.Fecha Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha valor")
            End If
        End Sub
        Private Sub mValidarCuotas()
            If txtCuotFecha.Fecha Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha.")
            End If

            If txtCuotImporte.Valor Is DBNull.Value And txtCuotPorc.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el importe o porcentaje de la cuota.")
            End If

            ' toma el importe porque no ingreso el %
            If txtCuotPorc.Valor Is DBNull.Value Then
                If CType(txtCuotImporte.Valor, Double) > CType(lblImpoSaldo.Text, Double) Then
                    Throw New AccesoBD.clsErrNeg("El importe de la cuota no puede superar al saldo.")
                End If
                If CType(txtCuotImporte.Valor, Double) = 0 Then
                    Throw New AccesoBD.clsErrNeg("El importe de la cuota no puede ser cero (0).")
                End If

            End If
            If txtCuotFecha.Fecha < Date.Today Then
                Throw New AccesoBD.clsErrNeg("La fecha no puede ser menor a la fecha de ingreso.")
            End If


        End Sub
        Private Sub mValidarConceptosAutomaticos(ByVal pintauto As Integer)
            If pintauto = 1 Then
                Throw New AccesoBD.clsErrNeg("No pueden modificarse descuentos autom�ticos, solo eliminarlos.")
            End If

        End Sub
        Private Sub mValidarConceptos(ByVal pstrTodo As String)


            If cmbConcep.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el concepto.")
            End If


            If txtImporteConcep.Valor Is DBNull.Value Then
                mLimpiarImportesConceptos()
                Throw New AccesoBD.clsErrNeg("Debe ingresar el valor de concepto.")
            End If

            If pstrTodo = "T" Then
                ' es obligatorio cargar el centro de costo cuando el concepto

                '10/05/2011 - deberia preguntar si es ganadera, entonces volver a verificar con la cuenta secundaria
                If cmbActi.SelectedValue.ToString = "7" Then
                    If mobj.ObtenerValorCampo(mstrConn, "Exposiciones", "expo_desde_fecha", cmbExpo.SelectedValue.ToString) > Today() Then ' Si actividad es Exposicion ganadera
                        If mobj.ObtenerValorCampo(mstrConn, "ConceptosX_GAN", "cta_resu", cmbConcep.Valor) = "1" Then
                            'If cmbCCosConcep.Valor Is DBNull.Value Then
                            If cmbCCosConcep.Valor.Trim().Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("Debe seleccionar el centro de costo (el concepto ingresado se asocia a un a cta. de resultado).")
                            End If

                        Else
                            'If Not cmbCCosConcep.Valor Is DBNull.Value Then
                            If cmbCCosConcep.Valor.Trim().Length > 0 Then
                                    Throw New AccesoBD.clsErrNeg("NO Debe seleccionar el centro de costo (el concepto ingresado NO se asocia a un a cta. de resultado).")
                                End If
                            End If
                    Else
                        If mobj.ObtenerValorCampo(mstrConn, "ConceptosX", "cta_resu", cmbConcep.Valor) = 1 Then
                            ' tiene asociado una cta. contable de resultado
                            'If cmbCCosConcep.Valor Is DBNull.Value Then
                            If cmbCCosConcep.Valor.Trim().Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("Debe seleccionar el centro de costo (el concepto ingresado se asocia a un a cta. de resultado).")
                            End If
                        Else
                            'If Not cmbCCosConcep.Valor Is DBNull.Value Then
                            If cmbCCosConcep.Valor.Trim().Length > 0 Then
                                    Throw New AccesoBD.clsErrNeg("NO Debe seleccionar el centro de costo (el concepto ingresado NO se asocia a un a cta. de resultado).")
                                End If
                            End If
                    End If
                Else
                    If mobj.ObtenerValorCampo(mstrConn, "ConceptosX", "cta_resu", cmbConcep.Valor) = 1 Then
                        ' tiene asociado una cta. contable de resultado
                        'If cmbCCosConcep.Valor Is DBNull.Value Then
                        If cmbCCosConcep.Valor.Trim().Length = 0 Then
                            Throw New AccesoBD.clsErrNeg("Debe seleccionar el centro de costo (el concepto ingresado se asocia a un a cta. de resultado).")
                        End If
                    Else
                        If cmbCCosConcep.Valor.Trim().Length > 0 Then
                            Throw New AccesoBD.clsErrNeg("NO Debe seleccionar el centro de costo (el concepto ingresado NO se asocia a un a cta. de resultado).")
                        End If
                    End If
                End If
                If txtImporteConcep.Valor = 0 Then
                    Throw New AccesoBD.clsErrNeg("No puede ingresar un concepto sin valor.")
                End If

                '' es obligatorio cargar el centro de costo cuando el concepto
                'If mobj.ObtenerValorCampo(mstrConn, "ConceptosX", "cta_resu", cmbConcep.Valor) = 1 Then
                '   ' tiene asociado una cta. contable de resultado
                '   If cmbCCosConcep.Valor Is DBNull.Value Then
                '      Throw New AccesoBD.clsErrNeg("Debe seleccionar el centro de costo (el concepto ingresado se asocia a un a cta. de resultado).")
                '   End If
                'End If
                'If txtImporteConcep.Valor = 0 Then
                '   Throw New AccesoBD.clsErrNeg("No puede ingresar un concepto sin valor.")
                'End If

            End If







        End Sub
        Private Sub mValidarAranceles(Optional ByVal pboolTodo As Boolean = True)
            If pboolTodo Then
                If txtCantAran.Valor Is DBNull.Value OrElse txtCantAran.Valor = 0 Then
                    mLimpiarImportesAranceles(Not pboolTodo)
                    Throw New AccesoBD.clsErrNeg("Debe ingresar la cantidad del arancel.")
                End If
                ' turnos 
                If mobj.pLabora And (hdnDatosPopTurnoBov.Text <> "" Or Me.hdnDatosPopTurnoEqu.Text <> "") Then

                    Dim lintId As Integer
                    Dim lstrTipo As String
                    If (hdnDatosPopTurnoBov.Text <> "") Then
                        lintId = hdnDatosPopTurnoBov.Text
                        lstrTipo = "B"
                    Else
                        lintId = hdnDatosPopTurnoEqu.Text
                        lstrTipo = "E"
                    End If
                    If mobj.TurnosCantFacturados(mdsDatos, txtCantAran.Valor, hdnDetaTempId.Text, lintId, lstrTipo) < 0 Then
                        Throw New AccesoBD.clsErrNeg("La cantidad supera los turnos pendientes a facturar.")
                    End If
                End If

                If Not cmbAran.Valor Is DBNull.Value Then mobj.AranRRGG(mstrConn, cmbAran.Valor)
                If mobj.pRRGG Then
                    If mobj.pAranRRGGRepresAnim And txtCriaNume.Valor Is DBNull.Value And cmbRaza.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe seleccionar el n�mero de criador o raza")
                    End If
                End If
            End If


            If cmbAran.Valor Is DBNull.Value OrElse cmbAran.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el arancel.")
            End If

            If mobj.DistintoIva(mdsDatos, mobj.ObtenerValorCampo(mstrConn, "arancelesX", "tipo_tasa", cmbAran.Valor.ToString)) Then
                Throw New AccesoBD.clsErrNeg("Se esta facturando con un tipo de I.V.A. distinto al del arancel seleccionado.")
            End If

            If txtImpoAran.Text = 0 Then
                Throw New AccesoBD.clsErrNeg("El arancel no tiene valor.")
            End If



        End Sub

        Private Sub mCargarCuotaUnica()
            'Si es cta cte y no cargo las cuotas
            'Si el importe de la factura es 0
            'si no es cta cte y el importe es <> 0 
            If (mobj.pPagoCtaCte And CType(lblImpoSaldo.Text, Double) <> 0) Or _
               txtTotNeto.Valor = 0 Or _
               (Not mobj.pPagoCtaCte And txtTotNeto.Valor <> 0) Or Me.cmbComprobante.Valor = TiposComprobantes.NotaCredito Then
                mBorrarDatosCuotas()
                mActualizarCuotas(True)
            End If
        End Sub
        Private Sub mValidarDatos()
            ' valida los datos de la primer solapa
            mValidarNavega()


            If Not mobj.ExistenItemsEnLaFactura(mdsDatos) Then
                Throw New AccesoBD.clsErrNeg("No existen items cargados.")
            End If

            If cmbActi.Valor = Actividades.Expo Then
                If cmbExpo.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar la exposici�n.")
                End If
            End If

            If mobj.pRRGG Then
                If mobj.ExistenArancRRGGRepresAnim(mstrConn, mdsDatos) And txtCriaNume.Valor Is DBNull.Value And cmbRaza.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar un n�mero de criador o raza. Existen aranceles que representan animales.")

                End If
            End If

            If mobj.ExistenAutorizacionesSinConf(mdsDatos) = 0 Then
                Throw New AccesoBD.clsErrNeg("Existen autorizaciones sin confirmar.")
            End If

        End Sub
        Private Sub mPrecioManual()
            txtPUnitAran.Enabled = True
            cmbListaPrecios.Enabled = True
            'If cmbComprobanteAsoc.Valor = 0 Or cmbActi.Valor = SRA_Neg.Constantes.Actividades.EXPOSICION_GANADERA_PALERMO Then
            '   txtPUnitAran.Enabled = True
            'Else
            '   txtPUnitAran.Enabled = False
            'End If
            'If cmbComprobanteAsoc.Valor = 0 Then
            '   cmbListaPrecios.Enabled = True
            'Else
            '   cmbListaPrecios.Enabled = False
            'End If
        End Sub
        Private Sub mControlesIVA()
            If usrClie.DiscrimIVA Then
                panIVAConcep.Visible = True
                panIVAAran.Visible = True
            Else
                panIVAConcep.Visible = False
                panIVAAran.Visible = False
            End If

        End Sub

        Private Sub mSetearDatosArancel()
            mLimpiarTextRelaAran(False)

        End Sub
        Private Sub mBloquearControlesCabe(ByVal pboolBloq As Boolean)
            cmbActi.Enabled = Not pboolBloq
            'cmbComprobante.Enabled = Not pboolBloq
            'tildar esta linea de abajo si se quiere poder cambiar el comprob. asociado  
            'desp de pasar la primer pantalla, ya esta programado (121206)
            cmbComprobanteAsoc.Enabled = Not pboolBloq
            txtFechaIva.Enabled = Not pboolBloq
            usrClie.Activo = Not pboolBloq
            If cmbComprobanteAsoc.Valor <> 0 Then
                txtFechaValor.Enabled = False
            End If
            'txtCotDolar.Enabled = Not pboolBloq
        End Sub

        Private Function mValorTasaIva(ByVal pstrArancel As String) As Double
            Return mobj.ObtenerValorCampo(mstrConn, "arancelesX", "valor_tasa", pstrArancel + "," + clsFormatear.gFormatFecha2DB(txtFechaIva.Fecha))
        End Function

        Private Function mValorTasaIva(ByVal pstrConcepId As String, ByVal pstrFechaIva As Date) As Double
            Return mobj.ObtenerValorCampo(mstrConn, "conceptosX", "valor_tasa", pstrConcepId + "," + clsFormatear.gFormatFecha2DB(pstrFechaIva))
        End Function

        Private Function mValorArancelAplicFormula(ByVal pintAranId As Integer, ByVal pdoubImporte As Double, ByVal pintCant As Integer) As Double
            Return mobj.AplicarFormula(mstrConn, pintAranId, pdoubImporte, pintCant, 1)
        End Function



        Private Sub mCalcularPrecioUnitArancel()
            
            'If txtCantAran.Valor = 0 Then Exit Sub
            Dim lstrArancelManual As String = ""
            'si el importe puede ser modificado por el usuario    
            If txtPUnitAran.Enabled Then lstrArancelManual = txtPUnitAranSIVA.Valor
            Dim lstrFiltro As String = IIf(cmbAran.Valor.ToString().Length > 0, cmbAran.Valor.ToString, "0") + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" + lstrArancelManual
            Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
            ' vsRet[0]; //PRECIO UNITARIO
            ' vsRet[5];//IMPORTE NETO (PRECIO UNITARIO CON IVA)
            'guarda el importe sin iva  (indistintamente de la condicion IVA del cliente)
            txtPUnitAranSIVA.Valor = vsRet(0)
            'muestra el precio unitario con o sin IVA segun la cond. del cliente 
            If usrClie.DiscrimIVA Then
                txtPUnitAran.Valor = Format(CType(vsRet(0), Double), "###0.00")
            Else
                txtPUnitAran.Valor = Format(CType(vsRet(5), Double), "###0.00")
            End If

        End Sub

        Private Sub mCalcularImporteAranceles()
            'idem mCalcularImporteAranceles() en javascript
            If txtCantAran.Valor > 0 Then

                Dim lstrFiltro As String = cmbAran.Valor.ToString() + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" + txtPUnitAranSIVA.Valor
                Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                ' vsRet[0]; //PRECIO UNITARIO
                ' vsRet[1]; //IMPORTE APLICADA LA FORMULA 
                ' vsRet[2];//TASA IVA
                ' vsRet[3];//IMPORTE IVA
                ' vsRet[4];//IMPORTE NETO 
                ' vsRet[5];//IMPORTE P. UNIT C/IVA
                hdlImpoIvaAran.Text = vsRet(3)
                hdnTasaIVAAran.Text = vsRet(2)

                If usrClie.DiscrimIVA Then
                    'tasa iva
                    txtTasaIVAAran.Valor = Format(CType(vsRet(2), Double), "###0.00")
                    'importe iva
                    txtImpoIvaAran.Valor = Format(CType(vsRet(3), Double), "###0.00")
                    ' valor bruto
                    txtImpoAran.Valor = Format(CType(vsRet(1), Double), "###0.00")
                Else
                    ' valor neto
                    txtImpoAran.Valor = Format(CType(vsRet(4), Double), "###0.00")
                End If

            End If
        End Sub




        Private Sub mCalcularImporteConceptos(ByVal pstrOrigen As String)
            'idem mCalcularImporteConceptos() de javascript
            Dim ldouTasaIVA, ldouImporteiva As Double
            mValidarConceptos(pstrOrigen)

            If Not txtImporteConcep.Valor Is DBNull.Value Then

                Dim lstrFiltro As String = cmbConcep.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + txtImporteConcep.Valor
                Dim vsRet As String() = SRA_Neg.Utiles.CalculoConcepto(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                ' vsRet[0];//TASA IVA   vsRet[1];//IMPORTE DEL IVA  'vsRet[2];//IMPORTE DEL CONCEPTO SIN IVA 'vsRet[3];//IMPORTE DEL CONCEPTO con IVA
                hdnTasaIVAConcep.Text = vsRet(0)
                hdnImpoIvaConcep.Text = vsRet(1)

                If usrClie.DiscrimIVA Then
                    txtTasaIVAConcep.Valor = Format(CType(vsRet(0), Double), "###0.00")
                    'txtImpoIvaConcep.Valor = Format(CType(vsRet(1), Double), "###0.00")
                    'el importe ingresado por el usuario es el neto (incluye iva)

                    '  txtImpoNConcep.Valor = Format(CType(vsRet(3), Double), "###0.00")
                End If
            End If


        End Sub
        Private Sub mCalcularTotal()

            mobj.CalcularTotales(mdsDatos, usrClie.DiscrimIVA, usrClie.SobretasaIVA)
            txtTotBruto.Valor = mobj.pTotalBrutoGral
            'ocultos
            txtTotIVA.Valor = mobj.pTotalIVAGral
            txtTotIVARedu.Valor = mobj.pTotalReduGral
            txtTotIVASTasa.Valor = mobj.pTotalSobreTGral
            '--
            txtTotNeto.Valor = mobj.pTotalNetoGral
            txtTotalSobretasa.Text = mobj.pTotalSobreTGral
            txtTotalIVA.Text = mobj.pTotalesIVA

            lblImpTotal.Text = txtTotNeto.Valor
            lblCabeTotalBruto.Text = txtTotBruto.Valor
            lblCabeTotalIVA.Text = txtTotalIVA.Valor
            lblCabeTotalSobre.Text = txtTotIVASTasa.Valor
            lblCabeTotalNeto.Text = txtTotNeto.Valor
            mCalcularSaldoCuotas()
            mCargarAutorizaciones()
            mSetearEventoGenerarComprobante()
        End Sub

        Private Sub mCotizacionDolar()
            mobj.CotizaDolar(mstrConn, txtFechaValor.Fecha)
            txtCotDolar.Valor = mobj.pCotizaDolar
        End Sub
        Private Sub mCargarAranceles(Optional ByVal pintSobretasa As Integer = 0)
            Dim lintValor As Integer
            Dim lstrRaza As String
            If cmbAran.Valor.Length > 0 Then
                lintValor = cmbAran.Valor
            End If

            Dim lstrFiltro As String = cmbActi.Valor + ", @sobretasa =" + clsSQLServer.gFormatArg(pintSobretasa, SqlDbType.Int)

            If cmbActi.Valor = Actividades.RRGG Or cmbActi.Valor = Actividades.Laboratorio Then
                lstrRaza = mRaza()
                If lstrRaza <> "" Then lstrFiltro = lstrFiltro + ", @aran_raza_id=" + clsSQLServer.gFormatArg(lstrRaza, SqlDbType.Int)
            End If

            ' Dario 2014-01-02 corrige error en la carga de aranceles
            lstrFiltro = lstrFiltro + ", @aran_peli_id=" + clsSQLServer.gFormatArg(hdnListaPrecios.Text, SqlDbType.Int)

            If usrClie.ClienteGenerico() Then lstrFiltro = lstrFiltro + ", @aran_clie_gene= 1"
            clsWeb.gCargarRefeCmb(mstrConn, "precios_aran", cmbAran, "S", lstrFiltro)
            'si el arancel esta en el combo 
            If lintValor <> 0 Then
                lstrFiltro = lstrFiltro + ",@aran_id=" + clsSQLServer.gFormatArg(lintValor, SqlDbType.Int)
                Dim lDsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "precios_aran_cargar", lstrFiltro)
                If lDsDatos.Tables(0).Rows.Count > 0 Then
                    cmbAran.Valor = lintValor
                Else
                    mLimpiarTextRelaAran()
                End If
            Else
                mLimpiarTextRelaAran()
            End If

        End Sub
        Private Function mRaza() As String
            If cmbRaza.Valor.Length > 0 Then
                Return cmbRaza.Valor
                Exit Function
            End If
            If txtCriaNume.Valor.Length > 0 Then
                Return mobj.ObtenerValorCampo(mstrConn, "criadores_razas", "raza_id", txtCriaNume.Valor.ToString())
                Exit Function
            End If
            Return ""

        End Function


        Private Sub mCargarArancelesyConceptos()

            Dim lstrFiltro As String = cmbActi.Valor
            mCargarAranceles()
            lstrFiltro = cmbActi.Valor
            If usrClie.ClienteGenerico() Then lstrFiltro = lstrFiltro + ", @conc_clie_gene= 1"

            clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConcep, "S", lstrFiltro)


            mArancel()
        End Sub

        Private Sub mBorrarDatosCuotas()
            For Each ldr As DataRow In mdsDatos.Tables(mstrTablaVtos).Select
                With ldr
                    ldr.Delete()
                End With
            Next

        End Sub
        Private Sub mCargarAutorizaciones()


            Dim filtro As String
            Dim ldrDatos As DataRow
            Dim ldsDatosAuto As DataSet

            Dim lvacio As Boolean

            ldsDatosAuto = mobj.AutorizacionesNDNC(mstrConn, mdsDatos, txtFechaValor.Fecha, cmbComprobanteAsoc.Valor, cmbListaPrecios.Valor.ToString(), cmbActi.Valor.ToString())

            If mdsDatos Is Nothing Then
                mCrearDataSet("")
            End If

            For Each ldr As DataRow In ldsDatosAuto.Tables(0).Select
                With ldr
                    'verifica que no este la ingresada la autorizacion
                    filtro = "coau_auti_id = " + ldr.Item(0).ToString()
                    If mdsDatos.Tables(mstrTablaAutor).Select.Count = 0 Then
                        lvacio = False

                    Else
                        lvacio = True
                    End If


                    If lvacio And (mdsDatos.Tables(mstrTablaAutor).Select(filtro).GetLength(0) > 0) Then
                        ' If (mEstaEnElDataSet(mdsDatos.Tables(mstrTablaAutor), ldr, "coau_auti_id", "coau_auti_id")) Then
                    Else
                        ' agrega la autotizacion 
                        ldrDatos = mdsDatos.Tables(mstrTablaAutor).NewRow
                        ldrDatos.Item("coau_auti_id") = .Item("id")
                        ldrDatos.Item("coau_auto") = 0
                        ldrDatos.Item("_auti_desc") = .Item("descr")
                        ldrDatos.Table.Rows.Add(ldrDatos)
                        lvacio = True

                    End If

                End With
            Next

            ' elimina las autorizaciones que no estan en el dr 

            For Each ldr As DataRow In mdsDatos.Tables(mstrTablaAutor).Select
                With ldr
                    'verifica que este la autorizacion 
                    filtro = "id = " + ldr.Item(2).ToString()
                    If (ldsDatosAuto.Tables(0).Select(filtro).GetLength(0) > 0) Then
                    Else
                        ldr.Delete()
                    End If
                End With
            Next

            mdsDatos.Tables(mstrTablaAutor).DefaultView.Sort = "_auti_desc"
            grdAutoriza.DataSource = mdsDatos.Tables(mstrTablaAutor).DefaultView
            grdAutoriza.DataBind()

        End Sub

        Private Function mCargarSaldosACuenta(ByVal pDsDatos As DataSet)

            Dim ldrDatos As DataRow
            Dim ldtDatos As DataTable
            Dim ldsDatosSaldosACta As DataSet

            pDsDatos.Tables.Add(mstrTablaSaldosACta)

            With pDsDatos.Tables(mdsDatos.Tables.Count - 1)
                .Columns.Add("sact_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_comp_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_clie_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_acti_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_impo", System.Type.GetType("System.Decimal"))
                .Columns.Add("sact_cance", System.Type.GetType("System.Boolean"))
                .Columns.Add("sact_audi_user", System.Type.GetType("System.Int32"))
                ldrDatos = .NewRow
                .Rows.Add(ldrDatos)
            End With

            ldrDatos.Item("sact_id") = -1
            ldrDatos.Item("sact_comp_id") = -1
            ldrDatos.Item("sact_clie_id") = usrClie.Valor
            ldrDatos.Item("sact_acti_id") = cmbActi.Valor
            ldrDatos.Item("sact_impo") = txtTotNeto.Valor
            ldrDatos.Item("sact_cance") = 0
            ldrDatos.Item("sact_audi_user") = Session("sUserId").ToString()

            Return (pDsDatos)

        End Function

        Private Sub mInicializarVariablesCalculo()
            hdnActivAnterior.Text = cmbActi.Valor
            hdnFValorAnterior.Text = txtFechaValor.Text
            hdnLPrecioAnterior.Text = cmbListaPrecios.Valor
            hdnCotiAnterior.Text = txtCotDolar.Valor
        End Sub
        Private Sub mCargarCombos()

            Dim lstrIDS As String = CType(TiposComprobantes.NotaCredito, String) + "," + CType(TiposComprobantes.NotaDebito, String)
            clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbComprobante, "S", "@coti_ids ='" & lstrIDS & "'")
            clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ")
            clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConcep, "S", " @conc_clie_gene= " & IIf(usrClie.ClienteGenerico(mstrConn), 1, 0))
        End Sub
        Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
            Dim lstrCta As String = ""
            If cmbConcep.Valor.Length > 0 Then
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbConcep.Valor)
                lstrCta = lstrCta.Substring(0, 1)
            End If
            If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
                If Not Request.Form("cmbCCosConcep") Is Nothing Or pbooEdit Then
                    clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCCosConcep, "id", "descrip", "N")
                    If Not Request.Form("cmbCCosConcep") Is Nothing Then
                        cmbCCosConcep.Valor = Request.Form("cmbCCosConcep").ToString
                    End If
                End If
            Else
                cmbCCosConcep.Items.Clear()
            End If
        End Sub

        Private Sub mLlenaComboListaPrecios(ByVal pstrFecha As Date, ByVal pstrActiv As String)
            If pstrFecha.ToString <> "" Then
                Dim lstrFiltro As String

                lstrFiltro = clsFormatear.gFormatFecha2DB(pstrFecha) + ", " + pstrActiv
                clsWeb.gCargarRefeCmb(mstrConn, "precios_lista_fact", cmbListaPrecios, "", lstrFiltro)
                hdnListaPrecios.Text = cmbListaPrecios.Valor
            End If
        End Sub
#End Region

#Region "Limpiar"
        Private Sub mLimpiar()
            mLimpiaCabecera()
        End Sub

        Private Sub mLimpiar(ByVal pboolsolapas As Boolean)

            mBloquearControlesCabe(False)
            hdnId.Text = ""
            txtObser.Text = ""
            lblImpTotal.Text = ""
            lblImpoSaldo.Text = ""
            lblCabeCliente.Text = ""
            lblCabeActividad.Text = ""
            lblCabeTPago.Text = ""
            lblCabeTotalBruto.Text = ""
            lblCabeTotalIVA.Text = ""
            lblCabeTotalNeto.Text = ""
            lblCabeTotalSobre.Text = ""



            mCrearDataSet("")

            mLimpiarDetaAran()

            mLimpiarDetaConcep()
            mLimpiarCuotas()



            hdnImprimio.Text = ""
            hdnImprimir.Text = ""
            hdnComprobante.Text = ""
            hdnDatosPop.Text = ""

            mSetearEditor("", False)


        End Sub
        Private Sub mLimpiaCabecera()
            If cmbActi.Valor = Actividades.RRGG Then
                txtCriaNume.Enabled = True
                cmbRaza.Enabled = True
            End If

            hdnActivAnterior.Text = ""
            hdnFValorAnterior.Text = ""
            hdnLPrecioAnterior.Text = ""
            hdnCotiAnterior.Text = ""
            ''cmbActi.Limpiar()
            txtCriaNume.Valor = ""
            ''cmbComprobante.Limpiar()
            cmbComprobanteAsoc.Valor = ""
            hdnComprobanteAsoc.Text = ""
            cmbRaza.Limpiar()
            hdnFormaPago.Text = ""
            hdnCria.Text = ""
            cmbListaPrecios.Enabled = True
            txtFechaValor.Enabled = True

            txtFechaIva.Fecha = Today
            txtFechaValor.Fecha = Today
            usrClie.Limpiar()
            hdnClieId.Text = ""
            txtNombApel.Valor = ""
            hdnClieDiscIVA.Text = ""
            txtSocio.Valor = ""
            hdnFechaValor.Text = ""
            hdnLoginPop.Text = ""
            lblAutoUsuaTit.Text = ""

            DivimgTarj.Style.Add("display", "none")
            divClieGene.Style.Add("display", "none")
            'lo ven todas las actividades (250407)
            'divCriador.Style.Add("display", "none")

            DivimgAlertaSaldosAmarillo.Style.Add("display", "none")
            DivimgAlertaSaldosVerde.Style.Add("display", "none")
            DivimgAlertaSaldosRojo.Style.Add("display", "none")
            DivimgEspecVerde.Style.Add("display", "none")
            DivimgEspecRojo.Style.Add("display", "none")
            divExpo.Style.Add("display", "none")

            hdnConvId.Text = ""
            txtconv.Valor = ""
            mLimpiaTodo()
            mLimpiar(True)
        End Sub

        Private Sub mLimpiaTodo()
            hdnActiId.Text = ""
            hdnFechaIva.Text = ""
            hdnFechaVigencia.Text = ""

            txtCotDolar.Text = ""

            mCotizacionDolar()

            grdDetalle.CurrentPageIndex = 0
            grdCuotas.CurrentPageIndex = 0

        End Sub
        Private Sub mLimpiarTextRelaAran(Optional ByVal pbooltodo As Boolean = True)
            If pbooltodo Then
                txtCantAran.Text = ""
                txtPUnitAranSIVA.Text = ""
            End If
            txtPUnitAran.Text = ""
            txtImpoIvaAran.Text = ""
            txtTasaIVAAran.Text = ""
            txtImpoAran.Text = ""
            chkExentoAran.Checked = False

        End Sub

        Private Sub mLimpiarDetaAran(Optional ByVal pboolTodo As Boolean = True)
            If pboolTodo Then
                hdnDetaTempId.Text = ""
            End If
            mLimpiarTextRelaAran()
            cmbAran.Limpiar()
            txtDescAmpliAran.Text = ""

            DivAranNormal.Style.Add("display", "inline")
            mSetearEditor(mstrTablaDetaAran, True)
            mSetearEditor("", mobj.ExistenItemsEnLaFactura(mdsDatos))
            txtTasaSobreTasa.Text = ""
            txtTotalSobreTasa1.Text = ""
            hdnTasaSobreTasa.Text = ""
            hdnTotalSobreTasa1.Text = ""


        End Sub

        Private Sub mLimpiarDetaConcep(Optional ByVal pboolTodo As Boolean = True)
            If pboolTodo Then
                hdnDetaTempId.Text = ""
            End If
            cmbConcep.Limpiar()
            cmbCCosConcep.Items.Clear()

            txtImporteConcep.Text = ""
            txtImpoIvaConcep.Text = ""
            txtTasaIVAConcep.Text = ""
            ' txtImpoNConcep.Text = ""
            txtTasaSobreTasaC.Text = ""
            txtTotalSobreTasaC.Text = ""
            hdnTasaSobreTasaC.Text = ""
            hdnTotalSobreTasaC.Text = ""

            txtDescAmpliConcep.Text = ""
            mSetearEditor(mstrTablaDetaConcep, True)
            mSetearEditor("", mobj.ExistenItemsEnLaFactura(mdsDatos))
        End Sub
        Private Sub mLimpiarImportesConceptos()
            'txtImpoIvaConcep.Valor = ""
            'txtImpoNConcep.Valor = ""
            txtTasaIVAConcep.Valor = ""
        End Sub
        Private Sub mLimpiarImportesAranceles(Optional ByVal pboolTodo As Boolean = True)
            If pboolTodo Then
                txtPUnitAran.Valor = ""
                txtPUnitAranSIVA.Valor = ""
            End If
            txtImpoAran.Valor = ""
            txtImpoIvaAran.Valor = ""
            txtTasaIVAAran.Valor = ""
            txtTasaSobreTasa.Valor = ""
            txtTotalSobreTasa1.Valor = ""
        End Sub
        Private Sub mLimpiarCuotas()
            hdnCuotasId.Text = ""
            txtCuotFecha.Text = ""
            txtCuotPorc.Text = ""
            txtCuotImporte.Text = ""

            mSetearEditor(mstrTablaVtos, True)
        End Sub

#End Region





        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Private Sub mModifDatosDeta()
            Dim lintIdDeta As Integer
            Dim ldrTemp As DataRow
            Dim lstrFiltro As String
            Dim lstrArancelManual As String = ""
            mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null and temp_tipo='A'"
            For Each ldr As DataRowView In mdsDatos.Tables(mstrTablaTempDeta).DefaultView
                With ldr
                    'si el importe puede ser modificado por el usuario    
                    If txtPUnitAran.Enabled Then lstrArancelManual = txtPUnitAranSIVA.Valor
                    lstrFiltro = .Item("temp_aran_concep_id").ToString() + ";" + .Item("temp_cant").ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" '+ lstrArancelManual
                    Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                    'precio unitario del arancel
                    .Item("temp_unit_impo") = vsRet(0)
                    'precio unitario con iva
                    .Item("temp_unit_c_iva") = vsRet(5)
                    'tasa iva
                    .Item("temp_tasa_iva") = vsRet(2)
                    'importe total s/iva
                    .Item("temp_impo") = vsRet(1)
                    'importe total c/iva
                    .Item("temp_impo_ivai") = vsRet(4)


                End With
                mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = ""

            Next

        End Sub

        Private Sub mRecalculaAranceles()
            'If hdnLPrecioAnterior.Text <> cmbListaPrecios.Valor Or _
            '   hdnFValorAnterior.Text <> txtFechaValor.Text Or _
            '   hdnCotiAnterior.Text <> txtCotDolar.Text Or _
            '   hdnActivAnterior.Text <> cmbActi.Valor Then

            '   mInicializarVariablesCalculo()
            '   mModifDatosDeta()
            '   mCargarGrillaArancelesConceptos()

            'End If

        End Sub



    End Class

End Namespace




