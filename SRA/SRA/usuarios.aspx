<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.usuarios" CodeFile="usuarios.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Usuarios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Usuarios</asp:label></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="95%" BorderStyle="None" AutoGenerateColumns="False"
										CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page"
										AllowPaging="True" PageSize="15">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="usua_id" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="usua_apel" HeaderText="Apellido"></asp:BoundColumn>
											<asp:BoundColumn DataField="usua_nomb" HeaderText="Nombre"></asp:BoundColumn>
											<asp:BoundColumn DataField="_perfil" HeaderText="Perfil"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="usua_perf_id" HeaderText="id_perfil"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ToolTip="Agregar una Nueva Mesa de Ex�men"
										ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
										CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"></CC1:BOTONIMAGEN></TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Usuario</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkActiv" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" CausesValidation="False" Height="21px"> Actividad</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
											Height="116px" Visible="False">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD width="20%" align="right">
																			<asp:Label id="lblApel" runat="server" cssclass="titulo">Apellido:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:textboxtab id="txtApel" runat="server" cssclass="cuadrotexto" Width="250px" Obligatorio="True"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<cc1:textboxtab id="txtNomb" runat="server" cssclass="cuadrotexto" Width="250px" Obligatorio="True"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblEmail" runat="server" cssclass="titulo">E-mail:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:textboxtab id="txtEmail" runat="server" cssclass="cuadrotexto" Width="250px" EsMail="True"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblCargo" runat="server" cssclass="titulo">Cargo:</asp:Label></TD>
																		<TD>
																			<cc1:textboxtab id="txtCargo" runat="server" cssclass="cuadrotexto" Width="250px"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTele" runat="server" cssclass="titulo">Tel.:</asp:Label></TD>
																		<TD>
																			<cc1:textboxtab id="txtTele" runat="server" cssclass="cuadrotexto" Width="250px"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 5px" align="right">
																			<asp:Label id="lblPerfil" runat="server" cssclass="titulo">Perfil:</asp:Label></TD>
																		<TD style="HEIGHT: 5px">
																			<cc1:combobox id="cmbPerfil" class="combo" runat="server" Width="300px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 5px">
																			<P align="right">
																				<asp:Label id="lblCentroEmisor" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;
																			</P>
																		</TD>
																		<P></P>
																		<TD style="HEIGHT: 5px" height="17" vAlign="bottom" colSpan="2">
																			<cc1:combobox onkeydown="mEnterPorTab();" id="cmbCentroEmisor" class="combo" runat="server" Width="300px"
																				AutoPostBack="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblUser" runat="server" cssclass="titulo">Usuario:</asp:Label></TD>
																		<TD>
																			<cc1:textboxtab id="txtUser" runat="server" cssclass="cuadrotexto" Width="250px" Obligatorio="True"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 152px" align="right">
																			<asp:Label id="lblPass" runat="server" cssclass="titulo">Contrase�a:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:textboxtab id="txtPass" runat="server" cssclass="cuadrotexto" Width="250px" TextMode="Password"></cc1:textboxtab>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 152px" noWrap align="right">
																			<asp:Label id="lblImpr" runat="server" cssclass="titulo">Tipo de Impresi�n:</asp:Label>&nbsp;
																		</TD>
																		<TD colSpan="2">
																			<cc1:combobox id="cmbImpr" class="combo" runat="server" Width="136px" Obligatorio="True">
																				<asp:ListItem Value="C" Selected="True">Cliente</asp:ListItem>
																				<asp:ListItem Value="S">Servidor</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 152px" noWrap align="right">
																			<asp:Label id="lblImprId" runat="server" cssclass="titulo">Impresora:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:combobox id="cmbImprId" class="combo" runat="server" Width="248px" Obligatorio="False"></cc1:combobox></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panActiv" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TableProf" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdActiv" runat="server" width="95%" AllowPaging="True" OnPageIndexChanged="grdActiv_PageChanged"
																				OnEditCommand="mEditarDatosActiv" BorderWidth="1px" HorizontalAlign="Center" CellSpacing="1"
																				GridLines="None" CellPadding="1" AutoGenerateColumns="False" BorderStyle="None" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="usac_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_actividad" HeaderText="Actividad"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="usac_acti_id" HeaderText="id_activ"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_defa" HeaderText="Default">
																						<HeaderStyle Width="60px"></HeaderStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblActiv" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbActiv" class="combo" runat="server" Width="260px"></cc1:combobox>&nbsp;
																			<asp:checkbox id="chkDefa" CssClass="titulo" Runat="server" Text="Default"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaActiv" runat="server" cssclass="boton" Width="100px" Text="Agregar Activ."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaActiv" runat="server" cssclass="boton" Width="100px" Text="Eliminar Activ."></asp:Button>&nbsp;
																			<asp:Button id="btnModiActiv" runat="server" cssclass="boton" Width="100px" Text="Modificar Activ."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpActiv" runat="server" cssclass="boton" Width="100px" Text="Limpiar Activ."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnPass" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnActivId" runat="server"></asp:textbox><asp:textbox id="hdnPage" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtFecha"]!= null)
			document.all["txtFecha"].focus();
		</SCRIPT>
	</BODY>
</HTML>
