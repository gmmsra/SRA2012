'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_Socios_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
' a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'Socios.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Imports Business.Facturacion
Imports ReglasValida.Validaciones


Namespace SRA


    'Partial Class Socios
    Partial Class Migrated_Socios

        Inherits Socios

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Protected WithEvents Label1 As System.Web.UI.WebControls.Label
        Protected WithEvents lblFant As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
#End Region

#Region "Definici�n de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_Socios
        Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
        Private mstrClientesDire As String = SRA_Neg.Constantes.gTab_ClientesDire
        Private mstrClientesTele As String = SRA_Neg.Constantes.gTab_ClientesTele
        Private mstrClientesMails As String = SRA_Neg.Constantes.gTab_ClientesMails
        Private mbooSoloBusq As Boolean
        Private mstrInclBloq As String

        Private mstrConn As String
        Private mstrClieDerivCtrl As String
        Private mstrParaPageSize As Integer
        Private mstrValorId As String
        Private mstrCateTitu As String
        Private mstrCatePode As String
        Private mstrEstaId As String
        Private mdsDatos As DataSet
        Private mbooPermiModi As Boolean
        Private mbooMailAsignadoAuto As Boolean = False
        Private hdnMaclIdNewDefault As Int32

        Private Enum Columnas As Integer
            Sele = 0
            Edit = 1
            Id = 2
            Nume = 3
        End Enum
#End Region

#Region "Inicializaci�n de Variables"
        Private Sub mInicializar()
            Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            mEstablecerPerfil()

            clsWeb.gActivarControl(usrTituSoci.txtApelExt, False)
            clsWeb.gActivarControl(usrTituSoci.txtCodiExt, False)

            mstrClieDerivCtrl = clsWeb.gValorParametro(Request.QueryString("ctrlId"))
            mbooSoloBusq = clsWeb.gValorParametro(Request.QueryString("SoloBusq")) = "1"
            mstrInclBloq = clsWeb.gValorParametro(Request.QueryString("InclBloq"))
            mstrValorId = clsWeb.gValorParametro(Request.QueryString("ValorId"))
            mstrCateTitu = clsWeb.gValorParametro(Request.QueryString("CateTitu"))
            mstrCatePode = clsWeb.gValorParametro(Request.QueryString("CatePode"))
            mstrEstaId = clsWeb.gValorParametro(Request.QueryString("EstaId"))

            usrClie.IncluirDeshabilitados = True
            usrClieFil.IncluirDeshabilitados = True

            If hdnValorId.Text = "" Then
                hdnValorId.Text = mstrValorId 'pone el id seleccionado en el control de b�squeda
            ElseIf hdnValorId.Text = "-1" Then  'si ya se limpiaron los filtros, ignora el id con el que vino
                mstrValorId = ""
            End If
            'usrClie.mstrConn = mstrConn
        End Sub

        Private Sub mEstablecerPerfil()

            If (Not clsSQLServer.gMenuPermi(CType(Opciones.Socios, String), (mstrConn), (Session("sUserId").ToString()))) Then
                Response.Redirect("noaccess.aspx")
            End If

            mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Socios_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
            btnModi.Visible = mbooPermiModi And Page.IsPostBack

        End Sub

        Public Overrides Sub mInicializarControl()
            'valores de los filtros
            If Not Page.IsPostBack Then
                txtApelFil.Text = clsWeb.gValorParametro(Request.QueryString("Apel"))
                txtCuitFil.Text = clsWeb.gValorParametro(Request.QueryString("Cuit"))
                txtDocuNumeFil.Text = clsWeb.gValorParametro(Request.QueryString("DocuNume"))
                txtNumeFil.Text = clsWeb.gValorParametro(Request.QueryString("CodiNume"))

                If clsWeb.gValorParametro(Request.QueryString("SociNume")) <> "" Then
                    txtNumeFil.Valor = clsWeb.gValorParametro(Request.QueryString("SociNume"))
                End If

                If clsWeb.gValorParametro(Request.QueryString("ClieNume")) <> "" Then
                    usrClieFil.Valor = clsWeb.gValorParametro(Request.QueryString("ClieNume"))
                End If

                If mbooSoloBusq Then
                    grdDato.Columns(1).Visible = False
                End If
            End If

            'seteo de controles segun filtros
            grdDato.Columns(0).Visible = (mstrClieDerivCtrl <> "")
            btnLimpiarFil.Visible = (mstrClieDerivCtrl <> "")
        End Sub
#End Region

#Region "Operaciones sobre la Pagina"

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim lstrFil As String

            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()

                If (Not Page.IsPostBack) Then
                    mInicializarControl()

                    Session(mSess(mstrTabla)) = Nothing
                    mdsDatos = Nothing

                    mSetearMaxLength()
                    mCargarCombos()

                    If mstrClieDerivCtrl <> "" Then
                        lstrFil = mstrValorId & txtApelFil.Text & usrClieFil.Valor & txtNumeFil.Valor & txtCuitFil.TextoPlano & cmbDocuTipoFil.Valor & txtDocuNumeFil.Valor & mstrCateTitu & mstrCatePode & mstrEstaId
                        If lstrFil.Replace("0", "") <> "" Then
                            mConsultar()
                        End If
                    Else
                        Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                        'mConsultar()
                    End If

                    If Not Page.IsPostBack And grdDato.Items.Count = 1 And mstrClieDerivCtrl <> "" Then
                        If mstrValorId = "" Then
                            mRetorno(grdDato.Items(0).Cells(Columnas.Nume).Text)
                        Else
                            mCargarDatos(mstrValorId)
                            Return
                        End If
                    End If

                    clsWeb.gInicializarControles(Me, mstrConn)
                Else
                    mdsDatos = Session(mSess(mstrTabla))
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mCargarCombos()
            clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbTipoDocu, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDist, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "iva_posic", cmbIVA, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTeleTipo, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "personas_tipos", cmbTipo, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDefaDirePais, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", "0" + cmbDefaDirePais.Valor.ToString)

            If clsWeb.gValorParametro(Request.QueryString("DocuTipo")) <> "" Then
                cmbDocuTipoFil.Valor = clsWeb.gValorParametro(Request.QueryString("DocuTipo"))
            End If

            clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDirePcia, "S", "0" + "350")

        End Sub

        Private Sub mSetearMaxLength()
            Dim lstrLong As Object

            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
            txtNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "soci_nume")
            txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "soci_obse")
        End Sub
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearDA()
            With mdsDatos.Tables(mstrTabla).Rows(0)
                If .Item("_tacl_id").ToString = "" Then
                    mMostrarDA(False)
                Else
                    mMostrarDA(True)
                    mMostrarDA(True)
                    Dim boolCargaCBU = False
                    ' controlo que fue seleccionad una tarjeta, para realizar la consulta para
                    ' ver si carga CBU o nro de cuenta
                    If (Not .Item("_tacl_tarj_id") Is DBNull.Value) Then
                        boolCargaCBU = clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_consul @tarj_id=" & .Item("_tacl_tarj_id").ToString, "tarj_PermiteDebitoEnCuenta")
                    End If
                    Dim lstrText As New System.Text.StringBuilder
                    lstrText.Append("Tarjeta: ")
                    lstrText.Append(.Item("_tacl_tarj").ToString)
                    ' Dario 2013-10-02
                    Dim texto = IIf(boolCargaCBU, "  CBU.: ", "  Nro.: ")
                    lstrText.Append(texto)
                    ' fin 2013-10-02
                    lstrText.Append(.Item("_tacl_nume").ToString)
                    If Not .IsNull("_tacl_vcto_fecha") Then
                        lstrText.Append(" Fecha Vto.: ")
                        lstrText.Append(CDate(.Item("_tacl_vcto_fecha")).ToString("dd/MM/yyyy"))
                    End If
                    If Not .IsNull("_tacl_vige_fecha") Then
                        lstrText.Append(" Fecha Vigencia: ")
                        lstrText.Append(CDate(.Item("_tacl_vige_fecha")).ToString("dd/MM/yyyy"))
                    End If

                    lblDebAuto.Text = lstrText.ToString
                End If
            End With


        End Sub
        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            btnModi.Enabled = Not (pbooAlta)
            btnModi.Visible = mbooPermiModi And Not (pbooAlta)
        End Sub

        Public Overrides Sub mCargarDatos(ByVal pstrId As String)

            Dim lbooEsFisi As Boolean
            Dim lstrInst As String = " - Com�n"

            mCrearDataSet(pstrId)

            With mdsDatos.Tables(mstrTabla).Rows(0)
                hdnId.Text = .Item("soci_id").ToString()

                Dim soci_dist_id As String = .Item("soci_obse").ToString

                usrClie.Valor = .Item("soci_clie_id").ToString
                cmbDist.Valor = .Item("soci_dist_id").ToString
                txtObse.Valor = .Item("soci_obse").ToString
                hdnClieId.Text = .Item("soci_clie_id").ToString

                cmbDist.Enabled = .Item("_cate_dist")
                If Not cmbDist.Enabled Then
                    cmbDist.Valor = DBNull.Value.ToString
                    cmbDirePcia.Enabled = False
                    cmbDireLoca.Enabled = False
                    cmbDirePcia.Limpiar()
                    cmbDireLoca.Limpiar()
                Else
                    cmbDirePcia.Enabled = True
                    cmbDireLoca.Enabled = True
                    If Not .IsNull("soci_ainfluencia_loca_id") Then
                        Dim pcia_id As String = String.Empty
                        pcia_id = .Item("soci_ainfluencia_loca_id").ToString()
                        Dim dsPcia As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "soli_provincia", pcia_id)
                        With dsPcia.Tables(0).Rows(0)
                            pcia_id = .Item("pcia_id").ToString()
                            clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbDireLoca, "S", "0" + pcia_id)
                        End With
                        cmbDirePcia.Valor = pcia_id
                        cmbDireLoca.Valor = .Item("soci_ainfluencia_loca_id").ToString
                    Else
                        cmbDirePcia.Limpiar()
                        cmbDireLoca.Limpiar()
                    End If


                End If

                lbooEsFisi = Not .IsNull("_peti_fisi") AndAlso .Item("_peti_fisi")
                btnFirmantes.Disabled = lbooEsFisi
                btnDocu.Disabled = Not lbooEsFisi

                btnApoderados.Disabled = Not .Item("_cate_pode")
                btnAcargo.Disabled = Not .Item("_cate_titu")
                btnDA.Disabled = Not .Item("_cate_paga")

                txtNume.Valor = .Item("soci_nume")
                If .Item("_soci_bloq_deve") Then
                    lblDevenga.Text = "S�. " + .Item("_soci_bloq_deve_fecha").ToString
                Else
                    lblDevenga.Text = "No"
                End If

                If Not .IsNull("_institucion") Then
                    If .Item("_institucion") <> "" Then
                        lstrInst = " - " & .Item("_institucion")
                    End If
                End If
                txtCate.Valor = .Item("_cate_desc") & lstrInst
                txtEsta.Valor = .Item("_estado")
                txtCateFecha.Fecha = .Item("soci_cate_fecha").ToString
                txtEstaFecha.Fecha = .Item("soci_esta_fecha").ToString
                txtIngrFecha.Fecha = .Item("soci_ingr_fecha").ToString
                txtEmanFecha.Fecha = .Item("soci_eman_fecha").ToString
                txtObse.Valor = .Item("soci_obse").ToString
                usrTituSoci.Valor = .Item("soci_titu_soci_id").ToString

                clsWeb.gActivarControl(txtEmanFecha, .Item("soci_cate_id") = SRA_Neg.Constantes.Categorias.Menor Or .Item("soci_cate_id") = SRA_Neg.Constantes.Categorias.Menor_F)

                If .Item("_cate_titu") Then
                    usrTituSoci.Limpiar()
                End If

                mCargarDeudaTit()

                clsWeb.gActivarControl(txtNomb, lbooEsFisi)
                clsWeb.gActivarControl(txtNaciFecha, lbooEsFisi)
                cmbTipoDocu.Enabled = lbooEsFisi
                clsWeb.gActivarControl(txtDocuNume, lbooEsFisi)
            End With

            mSetearDA()
            mCargarDatosClie()
            mCargarDatosDire()
            mCargarDatosTele()
            If pstrId <> "" Then
                mCargarDatosMail(pstrId)
            Else
                mCargarDatosMail()
            End If


            'lblTitu.Text = "" '' "Registro Seleccionado: " '+ IIf(txtApel.Text <> "", txtApel.Text + ", ", "") + IIf(txtNomb.Text <> "", txtNomb.Text, "") + " - " + txtNume.Text

            Dim lstrDeuda As String = mCalcularDeuda()
            If lstrDeuda <> "" Then
                lblTitu.Text = "  Deuda: " + lstrDeuda
            Else
                lblTitu.Text = " Socio sin Deuda "

            End If

            'Inicializo la Ruta del Reporte de Ficha de Socio
            Dim lstrRptName As String = "FichaSocios"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            lstrRpt += "&soci_id=" + hdnId.Text
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            hdnRptFicha.Text = lstrRpt

            mMostrarPanel(True)
            mSetearEditor("", False)

            If (mdsDatos.Tables("socios").Rows(0).Item("soci_cate_id") = 7) Or (mdsDatos.Tables("socios").Rows(0).Item("soci_cate_id") = 8) Then usrTituSoci.Activo = True

        End Sub

        Private Function mCalcularDeuda() As String
            Try
                Dim lstrCmd As String = "exec socios_deuda_cuota_fecha_consul "
                'lstrCmd = lstrCmd & " @cate_id=null"
                'lstrCmd = lstrCmd & ",@fecha=" & clsSQLServer.gFormatArg(Date.Today.ToString("dd/MM/yyyy"), SqlDbType.SmallDateTime)
                'lstrCmd = lstrCmd & ",@nro_desde=" & txtNume.Text
                'lstrCmd = lstrCmd & ",@nro_hasta=" & txtNume.Text
                lstrCmd = lstrCmd & " @soci_id=" & hdnId.Text
                lstrCmd = lstrCmd & ",@inte=1"

                Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), lstrCmd)
                Dim lstrDeuda As String = ""
                While (dr.Read())
                    If dr.GetValue(dr.GetOrdinal("deuda_actual")) > 0 Or dr.GetValue(dr.GetOrdinal("deuda_venc")) > 0 Then
                        lstrDeuda = dr.GetValue(dr.GetOrdinal("deuda"))
                        If dr.GetValue(dr.GetOrdinal("inte_fecha")) <> "" Then
                            lstrDeuda = " al " + dr.GetValue(dr.GetOrdinal("inte_fecha")) + " " + lstrDeuda
                        End If
                    End If
                End While

                dr.Close()
                Return lstrDeuda
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                Return ""
            End Try
        End Function

        Private Sub mCargarDeudaTit()
            Dim lstrCmd As String
            Dim ds As DataSet

            If usrTituSoci.Valor Is DBNull.Value Then
                btnDeudaTit.Disabled = True
            Else
                btnDeudaTit.Disabled = False

                lstrCmd = "exec clientes_estado_de_saldos_consul"
                lstrCmd += " @clie_id = " + clsSQLServer.gFormatArg(usrTituSoci.ClieId(mstrConn).ToString, SqlDbType.Int)
                lstrCmd += ",@fecha = " + clsFormatear.gFormatFecha2DB(Today)

                ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

                With ds.Tables(0).Rows(0)
                    btnDeudaTit.Disabled = .Item("saldo_total") = 0
                End With
            End If
        End Sub

        Private Sub mCargarDatosClie()
            With mdsDatos.Tables(mstrClientes).Rows(0)
                txtApel.Valor = .Item("clie_apel").ToString
                txtNomb.Valor = .Item("clie_nomb").ToString
                txtFant.Valor = .Item("clie_fanta").ToString
                txtDocuNume.Valor = .Item("clie_docu_nume").ToString
                If .Item("clie_cuit").ToString Is DBNull.Value.ToString OrElse .Item("clie_cuit") = "0" Then
                    txtCUIT.Text = ""
                Else
                    txtCUIT.Text = Trim(.Item("_cuit")).ToString
                End If
                cmbTipoDocu.Valor = .Item("clie_doti_id").ToString
                cmbIVA.Valor = .Item("clie_ivap_id").ToString
                txtNaciFecha.Fecha = .Item("clie_naci_fecha").ToString
                txtFalleFecha.Fecha = .Item("clie_falle_fecha").ToString
                chkSinCorreo.Checked = .Item("clie_sin_correo").ToString
                cmbTipo.Valor = .Item("clie_peti_id").ToString
            End With
        End Sub

        Private Sub mCargarDatosDire()
            Dim ldrDatos As DataRow

            hdnDiclId.Text = ""
            txtDire.Valor = ""
            txtDireCP.Valor = ""
            cmbDefaDirePais.Limpiar()
            cmbDefaDirePcia.Limpiar()
            cmbDefaDireLoca.Limpiar()
            txtDefaDireRefe.Valor = ""

            ldrDatos = mObtenerDefaultXActi(mstrClientesDire)
            If Not ldrDatos Is Nothing Then
                With ldrDatos
                    hdnDiclId.Text = .Item("dicl_id")
                    txtDire.Valor = .Item("dicl_dire")
                    txtDireCP.Valor = .Item("dicl_cpos")
                    cmbDefaDirePais.Valor = .Item("_dicl_pais_id")
                    clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", "0" + cmbDefaDirePais.Valor.ToString)
                    cmbDefaDirePcia.Valor = .Item("_dicl_pcia_id")
                    clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbDefaDireLoca, "S", "0" + cmbDefaDirePcia.Valor.ToString)
                    cmbDefaDireLoca.Valor = .Item("dicl_loca_id")
                    txtDefaDireRefe.Valor = .Item("dicl_refe")
                End With
            End If
        End Sub

        Private Sub mCargarDatosTele()
            Dim ldrDatos As DataRow

            hdnTeclId.Text = ""
            txtTeleArea.Valor = ""
            txtTeleNume.Valor = ""
            txtTeleRefe.Valor = ""
            cmbTeleTipo.Limpiar()

            ldrDatos = mObtenerDefaultXActi(mstrClientesTele)
            If Not ldrDatos Is Nothing Then
                With ldrDatos
                    hdnTeclId.Text = .Item("tecl_id")
                    txtTeleArea.Valor = .Item("tecl_area_code")
                    txtTeleNume.Valor = .Item("tecl_tele")
                    If (Not .Item("tecl_refe") Is DBNull.Value) Then
                        txtTeleRefe.Valor = .Item("tecl_refe")
                    End If
                    If (Not .Item("tecl_teti_id") Is DBNull.Value) Then
                        cmbTeleTipo.Valor = .Item("tecl_teti_id")
                    End If

                End With
            End If
        End Sub

        Private Sub mCargarDatosMail(ByVal pstrId As String)


            Dim ldrDatos As DataRow
            Dim intSocio As Int32
            hdnMaclId.Text = ""
            txtMail.Valor = ""
            txtMailRefe.Valor = ""
            Dim oSocios As New SRA_Neg.Socios(mstrConn, Session("sUserId").ToString())
            intSocio = ValidarNulos(pstrId, False)
            ldrDatos = oSocios.GetMailDefaultBySocioId(intSocio)


            If Not ldrDatos Is Nothing Then
                With ldrDatos
                    hdnMaclId.Text = .Item("macl_id")
                    txtMail.Valor = .Item("macl_mail")
                    txtMailRefe.Valor = .Item("macl_refe")
                End With
            End If
        End Sub
        Private Sub mCargarDatosMail()
            Dim ldrDatos As DataRow
            Dim intSocio As Int16
            hdnMaclId.Text = ""
            txtMail.Valor = ""
            txtMailRefe.Valor = ""


            ldrDatos = mObtenerDefaultXActi(mstrClientesMails)
            If Not ldrDatos Is Nothing Then
                With ldrDatos
                    hdnMaclId.Text = .Item("macl_id")
                    txtMail.Valor = .Item("macl_mail")
                    txtMailRefe.Valor = .Item("macl_refe")
                End With
            End If
        End Sub

        Private Function mObtenerDefaultXActi(ByVal pstrTabla As String) As DataRow
            Dim ldrDatos As DataRow
            Dim j As Integer

            With mdsDatos.Tables(pstrTabla)
                'por ahora solo muestra la primer direcci�n,despu�s traer la defaul
                j = .Rows.Count - 1
                While j > 0
                    .Rows.RemoveAt(j)
                    j -= 1
                End While

                If .Rows.Count > 0 Then
                    Return (.Rows(0))
                End If
            End With
        End Function

        Private Sub mAgregar()
            mLimpiar()
            btnModi.Enabled = False
            btnModi.Visible = False
            mMostrarPanel(True)
        End Sub

        Private Sub mLimpiar()
            hdnId.Text = ""
            hdnRptFicha.Text = ""
            hdnDiclId.Text = ""
            hdnTeclId.Text = ""
            hdnMaclId.Text = ""
            lblTitu.Text = ""
            txtNume.Text = ""
            lblDebAuto.Text = ""
            txtFalleFecha.Text = ""
            chkSinCorreo.Checked = False
            cmbDefaDireLoca.Limpiar()
            cmbDefaDirePais.Limpiar()
            cmbDefaDirePcia.Limpiar()
            cmbTipo.Limpiar()
            cmbTeleTipo.Limpiar()
            txtDefaDireRefe.Text = ""
            txtTeleRefe.Text = ""
            txtMailRefe.Text = ""

            usrClie.Limpiar()
            usrTituSoci.Limpiar()

            mdsDatos = Nothing
            Session(mSess(mstrTabla)) = Nothing

            mSetearEditor("", True)

            mCargarDeudaTit()

        End Sub

        Private Sub mCerrar()
            Try
                mLimpiar()
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            Dim lbooVisiOri As Boolean = panDato.Visible

            panDato.Visible = pbooVisi
            panFiltro.Visible = Not panDato.Visible
            grdDato.Visible = Not panDato.Visible

            If pbooVisi Then
                grdDato.DataSource = Nothing
                grdDato.DataBind()
            Else
                Session(mSess(mstrTabla)) = Nothing
                mdsDatos = Nothing
            End If

            If lbooVisiOri And Not pbooVisi Then
                mLimpiarFiltros()
            End If
        End Sub

#End Region

#Region "Opciones de ABM"
        Private Sub mModi()
            Try
                mGuardarDatos()
                If Not mdsDatos.Tables(mstrClientesMails) Is Nothing Then
                    mdsDatos.Tables.Remove(mstrClientesMails)
                End If
                If Not mdsDatos.Tables(mstrClientesTele) Is Nothing Then
                    mdsDatos.Tables.Remove(mstrClientesTele)
                End If
                If Not mdsDatos.Tables(mstrClientesDire) Is Nothing Then
                    mdsDatos.Tables.Remove(mstrClientesDire)
                End If

                Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
                lobjGenericoRel.Modi()

                If hdnMaclIdNewDefault <> 0 Then
                    Dim oSocios As New SRA_Neg.Socios(mstrConn, Session("sUserId").ToString())
                    oSocios.SetMailDefaultByMaclId(hdnMaclIdNewDefault)
                End If



                If mstrValorId <> "" Then
                    mRetorno(txtNume.Text)
                    Return
                End If


                ' Dario 2013-05-15 1111 se genera la fila en novnuev en el alta del cliente 
                SRA_Neg.Utiles.NovNuevClientesNovedades(mstrConn, "MODI_CTE", Me.usrClie.Codi)
                ' fin 2013-05-15 1111

                ClienteBusiness.IntegrarClienteAFinneg(String.Empty, hdnClieId.Text, "put")

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mGuardarDatos()
            mValidarDatos()
            With mdsDatos.Tables(mstrTabla).Rows(0)
                If cmbDist.Valor.Length > 0 Then
                    .Item("soci_dist_id") = cmbDist.Valor
                End If
                .Item("soci_obse") = IIf(IsDBNull(txtObse.Valor), "", txtObse.Valor)
                .Item("soci_eman_fecha") = IIf(txtEmanFecha.Text.Trim.Length > 0, txtEmanFecha.Fecha, DBNull.Value)
                If Not (usrTituSoci.Valor Is DBNull.Value) Then .Item("soci_titu_soci_id") = Int(usrTituSoci.Valor)
                If cmbDireLoca.Valor.Length > 0 Then
                    .Item("soci_ainfluencia_loca_id") = cmbDireLoca.Valor
                Else
                    .Item("soci_ainfluencia_loca_id") = DBNull.Value
                End If
            End With

            mGuardarDatosCliente()
            'mGuardarDatosDire()
            'mGuardarDatosTele()
            '  mGuardarDatosMail()
        End Sub


        Private Sub mGuardarDatosCliente()

            Dim oSocios As New SRA_Neg.Socios(mstrConn, Session("sUserId").ToString())
            hdnMaclIdNewDefault = 0


            If hdnTeclId.Text <> "" Then
                If txtTeleNume.Text <> "" Or txtTeleRefe.Text <> "" Then
                    If txtTeleArea.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar un c�digo de �rea.")
                    End If

                    If txtTeleNume.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar el numero de tel�fono.")
                    End If


                    If txtTeleRefe.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar la referencia para el tel�fono.")
                    End If

                    If cmbTeleTipo.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de tel�fono.")
                    End If
                End If
            Else
                If txtTeleNume.Text <> "" Or txtTeleRefe.Text <> "" Then
                    If txtTeleArea.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar un c�digo de �rea.")
                    End If

                    If txtTeleNume.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar el numero de tel�fono.")
                    End If


                    If txtTeleRefe.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar la referencia para el tel�fono.")
                    End If

                    If cmbTeleTipo.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de tel�fono.")
                    End If
                End If
            End If

            If hdnDiclId.Text <> "" Then
                If txtDire.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar una direcci�n.")
                End If
                If txtDireCP.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el c�digo postal.")
                End If
                If cmbDefaDireLoca.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la localidad.")
                End If
                If txtDefaDireRefe.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la referencia para la direcci�n.")
                End If
            End If

            With mdsDatos.Tables(mstrClientes).Rows(0)
                .Item("clie_apel") = txtApel.Valor
                .Item("clie_nomb") = txtNomb.Valor
                .Item("clie_fanta") = txtFant.Valor
                .Item("clie_cuit") = txtCUIT.TextoPlano
                .Item("clie_ivap_id") = cmbIVA.Valor
                .Item("clie_doti_id") = IIf(cmbTipoDocu.Valor.Trim.Length > 0, cmbTipoDocu.Valor, DBNull.Value) '  cmbTipoDocu.Valor
                .Item("clie_docu_nume") = IIf(txtDocuNume.Valor.Trim.Length > 0, txtDocuNume.Valor, DBNull.Value)
                .Item("clie_naci_fecha") = IIf(txtNaciFecha.Fecha.Trim.Length > 0, txtNaciFecha.Fecha, DBNull.Value)
                .Item("clie_falle_fecha") = IIf(txtFalleFecha.Fecha.Trim.Length > 0, txtFalleFecha.Fecha, DBNull.Value)
                .Item("clie_sin_correo") = chkSinCorreo.Checked
                .Item("clie_peti_id") = IIf(cmbTipo.Valor.Trim.Length > 0, cmbTipo.Valor, DBNull.Value)


                'direccion default
                If hdnDiclId.Text = "" Then
                    .Item("dicl_id") = DBNull.Value
                Else
                    .Item("dicl_id") = hdnDiclId.Text
                End If
                .Item("dicl_dire") = txtDire.Valor.ToUpper
                .Item("dicl_refe") = txtDefaDireRefe.Valor
                .Item("dicl_cpos") = txtDireCP.Valor
                .Item("dicl_loca_id") = cmbDefaDireLoca.Valor
                'telefono default
                Dim intSocioId As Integer
                intSocioId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("soci_id"), False)
                If hdnTeclId.Text = "" Then
                    .Item("tecl_id") = DBNull.Value
                Else
                    .Item("tecl_id") = hdnTeclId.Text
                End If

                If txtTeleNume.Text <> "" And txtTeleRefe.Text <> "" Then
                    .Item("tecl_tele") = txtTeleNume.Valor
                    .Item("tecl_area_code") = txtTeleArea.Valor
                    .Item("tecl_refe") = txtTeleRefe.Text
                    .Item("tecl_teti_id") = cmbTeleTipo.Valor
                Else
                    If Not .Item("tecl_id") Is System.DBNull.Value Then
                        oSocios.SetBajaLogicaTelefonoDefaultById(.Item("tecl_id"))
                    End If
                End If

                If txtMail.Valor.ToString <> "" Or txtMailRefe.Text <> "" Then
                    If txtMail.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar la direccion de correo")
                    End If
                    If txtMailRefe.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar la direccion de referencia")
                    End If
                End If

                'mail default
                Dim drMail As DataRow

                If hdnMaclId.Text = "" Then
                    .Item("defa_macl_id") = DBNull.Value
                Else
                    .Item("defa_macl_id") = hdnMaclId.Text
                End If

                If (txtMail.Valor.ToString = "") Then

                    Dim ldrDatos As DataRow
                    ldrDatos = oSocios.GetMailAdicionalBySocioId(intSocioId)
                    If Not ldrDatos Is Nothing Then
                        mbooMailAsignadoAuto = True
                        With ldrDatos
                            hdnMaclIdNewDefault = .Item("macl_id")

                        End With
                    End If

                    .Item("defa_macl_mail") = txtMail.Valor
                    .Item("defa_macl_refe") = txtMailRefe.Text
                Else

                    .Item("defa_macl_mail") = txtMail.Valor
                    .Item("defa_macl_refe") = txtMailRefe.Text
                End If

            End With

        End Sub

        Private Sub mGuardarDatosDire()
            Dim ldrDatos As DataRow

            If hdnDiclId.Text <> "" Then
                If txtDire.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar una direcci�n.")
                End If

                If txtDireCP.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el c�digo postal.")
                End If

                With mdsDatos.Tables(mstrClientesDire).Rows(0)
                    .Item("dicl_dire") = txtDire.Valor
                    .Item("dicl_cpos") = txtDireCP.Valor
                End With
            End If
        End Sub

        Private Sub mGuardarDatosTele()
            Dim ldr As DataRow
            If hdnTeclId.Text <> "" Then
                If txtTeleArea.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar un c�digo de �rea.")
                End If
                If txtTeleNume.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el nro de tel�fono.")
                End If
                If txtTeleRefe.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la referencia para el tel�fono.")
                End If
                If cmbTeleTipo.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de tel�fono.")
                End If
            Else
                ldr = mdsDatos.Tables(mstrClientesTele).NewRow
                ldr.Item("tecl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClientesTele), "tecl_id")
            End If
            With ldr
                .Item("tecl_area_code") = txtTeleArea.Valor
                .Item("tecl_tele") = txtTeleNume.Valor
                .Item("tecl_refe") = txtTeleRefe.Valor
                .Item("tecl_teti_id") = cmbTeleTipo.Valor
            End With
            If (hdnTeclId.Text = "") Then
                mdsDatos.Tables(mstrClientesTele).Rows.Add(ldr)
            End If
        End Sub

        Private Sub mGuardarDatosMail()
            Dim ldr As DataRow
            If hdnMaclId.Text <> "" Then
                If txtMail.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar un mail.")
                End If
                If mdsDatos.Tables(mstrClientesMails).Select("macl_id=" & hdnMaclId.Text).Length > 0 Then
                    ldr = mdsDatos.Tables(mstrClientesMails).Select("macl_id=" & hdnMaclId.Text)(0)
                End If
            Else
                If Not mbooMailAsignadoAuto Then
                    ldr = mdsDatos.Tables(mstrClientesMails).NewRow
                    ldr.Item("macl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClientesMails), "macl_id")
                End If
            End If

            With ldr
                .Item("macl_mail") = txtMail.Valor
                .Item("macl_clie_id") = hdnClieId.Text
            End With
            If (hdnMaclId.Text = "") Then
                If Not mbooMailAsignadoAuto Then
                    mdsDatos.Tables(mstrClientesMails).Rows.Add(ldr)
                End If

            End If
        End Sub
        Private Sub mMostrarDA(ByVal lboolMostrar As Boolean)
            lblDebAuto.Text = ""
            If lboolMostrar Then
                lblDA.Text = ""
                btnDA.InnerHtml = "Modificar"
            Else
                lblDA.Text = "NO"
                btnDA.InnerHtml = "Agregar"
            End If
        End Sub

        Private Sub mValidarDatos()
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)
        End Sub

        Public Overrides Sub mCrearDataSet(ByVal pstrId As String)
            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrClientes
            mdsDatos.Tables(2).TableName = mstrClientesTele
            mdsDatos.Tables(3).TableName = mstrClientesDire
            mdsDatos.Tables(4).TableName = mstrClientesMails

            For j As Integer = 5 To mdsDatos.Tables.Count - 1
                'saco las tablas que no me sirven
                mdsDatos.Tables.RemoveAt(j)
            Next

            Session(mSess(mstrTabla)) = mdsDatos
        End Sub
#End Region

#Region "Operacion Sobre la Grilla"
        Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.CurrentPageIndex = E.NewPageIndex
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Overrides Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                mCargarDatos(E.Item.Cells(Columnas.Id).Text)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Overrides Sub mSeleccionar(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mRetorno(E.Item.Cells(Columnas.Nume).Text)
        End Sub

        Public Overrides Sub mRetorno(ByVal pstrnume As String)
            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", clsWeb.gValorParametro(Request.QueryString("ctrlId")), pstrnume))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].onchange();", clsWeb.gValorParametro(Request.QueryString("ctrlId"))))
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)

            Session(mSess(mstrTabla)) = Nothing
            mdsDatos = Nothing
        End Sub
#End Region

        Private Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub

        Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub

        Private Sub txtNumeFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumeFil.TextChanged
            Try
                mConsultar()

                With DirectCast(grdDato.DataSource, DataSet).Tables(0)
                    If .Rows.Count = 1 Then
                        mCargarDatos(.Rows(0).Item("soci_id"))
                    ElseIf .Rows.Count = 0 Then
                        Throw New AccesoBD.clsErrNeg("Nro. de Socio Inexistente")
                    End If
                End With

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Overrides Sub mConsultar()
            Dim lstrCmd As String
            lstrCmd = ObtenerSql(IIf(chkBaja.Checked, 1, 0), mstrValorId, mstrInclBloq, txtApelFil.Text,
                                 usrClieFil.Valor.ToString, txtNumeFil.Valor.ToString,
                                 txtCuitFil.TextoPlano.ToString, cmbDocuTipoFil.Valor.ToString,
                                 txtDocuNumeFil.Valor.ToString, txtFirmanteNumeFil.Valor.ToString,
                                 txtFirmanteFil.Text, mstrCateTitu, mstrCatePode, mstrEstaId)

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)
            mSetearEditor("", True)
            mMostrarPanel(False)
        End Sub

        '    Public Shared Function ObtenerSql(ByVal pintBaja As Integer, ByVal pstrValorId As String, ByVal pstrInclBloq As String, ByVal pstrApel As String, ByVal pstrClieId As String, ByVal pstrSociNume As String, ByVal pstrCuit As String, ByVal pstrDocuTipo As String, ByVal pstrDocuNume As String, ByVal pstrFirmanteNume As String, ByVal pstrFirmante As String, ByVal pstrCateTitu As String, ByVal pstrCatePode As String, ByVal pstrEstaId As String) As String
        '        Dim lstrCmd As New StringBuilder
        '        lstrCmd.Append("exec socios_busq ")
        '        lstrCmd.Append("@incluir_bajas=" + pintBaja.ToString)
        '
        '        If pstrValorId <> "" Then
        '            lstrCmd.Append(", @soci_id=" + pstrValorId)
        '        Else
        '            lstrCmd.Append(",@bloq=")
        '            Select Case pstrInclBloq
        '                Case "S"
        '                    lstrCmd.Append("1")
        '                Case "N"
        '                    lstrCmd.Append("0")
        '                Case "T", ""
        '                    lstrCmd.Append("NULL")
        '            End Select
        '
        '            lstrCmd.Append(", @soci_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
        '            lstrCmd.Append(", @soci_clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
        '            lstrCmd.Append(", @soci_nume=" + clsSQLServer.gFormatArg(pstrSociNume, SqlDbType.Int))
        '            lstrCmd.Append(", @clie_cuit=" + clsSQLServer.gFormatArg(pstrCuit, SqlDbType.Int))
        '            lstrCmd.Append(", @clie_doti_id=" + clsSQLServer.gFormatArg(pstrDocuTipo, SqlDbType.Int))
        '            lstrCmd.Append(", @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNume, SqlDbType.Int))
        '            lstrCmd.Append(", @firm_soci_nume=" + clsSQLServer.gFormatArg(pstrFirmanteNume, SqlDbType.Int))
        '            lstrCmd.Append(", @firm_nyap=" + clsSQLServer.gFormatArg(pstrFirmante, SqlDbType.VarChar))
        '
        '            If pstrCateTitu <> "" Then
        '                lstrCmd.Append(", @cate_titu=" + pstrCateTitu)
        '            End If
        '
        '            If pstrCatePode <> "" Then
        '                lstrCmd.Append(", @cate_pode=" + pstrCatePode)
        '            End If
        '
        '            If pstrEstaId <> "" Then
        '                lstrCmd.Append(", @esta_id=" + pstrEstaId)
        '            End If
        '        End If
        '
        '        Return (lstrCmd.ToString)
        '    End Function

        Private Sub mLimpiarFiltros()
            hdnValorId.Text = "-1"
            mstrValorId = ""

            txtApelFil.Text = ""
            txtCuitFil.Text = ""
            cmbDocuTipoFil.Limpiar()
            txtDocuNumeFil.Text = ""
            txtFirmanteFil.Text = ""
            txtFirmanteNumeFil.Text = ""
            txtNumeFil.Text = ""
            usrClieFil.Limpiar()
            usrTituSoci.Limpiar()
            chkBaja.Checked = False

            mCargarDeudaTit()
        End Sub

        Private Sub usrClieFil_Cambio(ByVal sender As Object) Handles usrClieFil.Cambio
            Try
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
            Try
                grdDato.CurrentPageIndex = 0
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
            mLimpiarFiltros()
        End Sub

        Private Function mSess(ByVal pstrTabla As String) As String
            If hdnSess.Text = "" Then
                hdnSess.Text = pstrTabla & Now.Millisecond.ToString
            End If
            Return (hdnSess.Text)
        End Function

    End Class
End Namespace
