<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociFirmantes" CodeFile="SociFirmantes.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>FIRMANTES</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultgrupntScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="stylesheet/sra.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/utiles.js"></script>
    <script type="text/javascript" src="includes/paneles.js"></script>

    <script type="text/javascript">

        function usrSoci_onchange()
		{
			if (document.all('usrSoci:txtId').value == '')
			{
				document.all('txtIngreFecha').innerHTML = '';
			}
			else
			{
				var sRet = LeerCamposXML("socios", "@soci_id="+document.all('usrSoci:txtId').value, "soci_ingr_fecha");
				document.all('txtIngreFecha').innerHTML = "Fecha Ingreso: " + sRet;
			}
		}
		function mLimpiarFirma()		
		{
			document.all("txtFirma").value = "";
			mSetearImagenes();
		}
		function mLimpiarFoto()
		{
			document.all("txtFoto").value = "";
			mSetearImagenes();
  		}

            function mSetearImagenes() {
                if (document.all('txtFoto').value != "") {
                    document.all('imgFoto').src = 'fotos/' + document.all('txtFoto').value;
                }
                else {
                    document.all('imgFoto').src = 'images/none.jpg';
                    document.all('btnFotoVer').disabled = true;
                }
                if (document.all('txtFirma').value != "") {
                    document.all('imgFirma').src = 'fotos/' + document.all('txtFirma').value;
                }
                else {
                    document.all('imgFirma').src = 'images/none.jpg';
                    document.all('btnFirmaVer').disabled = true;
                }
            }
            function mVerImagen(pTipo) {
                if (pTipo == 'Foto')
                    gAbrirVentanas('fotos/' + document.all('txtFoto').value);
                else
                    gAbrirVentanas('fotos/' + document.all('txtFirma').value);
            }

            function mSetearFirm(pOri) {
                if (pOri == 'S') {
                    document.all("txtNyap").value = "";
                    document.all("txtDocuNume").value = "";
                    document.all("txtFoto").value = "";
                    document.all("txtFirma").value = "";
                    document.all('imgFoto').src = 'images/none.jpg';
                    document.all('imgFirma').src = 'images/none.jpg';
                    gHabilitarControl(document.all("usrSoci:txtCodi"), true);
                    gHabilitarControl(document.all("usrSoci:txtApel"), true);
                    gHabilitarControl(document.all("txtNyap"), false);
                    gHabilitarControl(document.all("txtDocuNume"), false);
                    gHabilitarControl(document.all("cmbDoti"), false);
                    gHabilitarControl(document.all("filFoto"), false);
                    gHabilitarControl(document.all("filFirma"), false);
                    gHabilitarControl(document.all("txtFirm_mail"), false);
                    gHabilitarControl(document.all("txtFirm_celular_pais"), false);
                    gHabilitarControl(document.all("txtFirm_celular_area"), false);
                    gHabilitarControl(document.all("txtFirm_celular_numero"), false);
                }
                else {
                    gHabilitarControl(document.all("usrSoci:txtCodi"), false);
                    gHabilitarControl(document.all("usrSoci:txtApel"), false);
                    document.all("usrSoci:txtCodi").value = "";
                    document.all("usrSoci:txtApel").value = "";
                    document.all('imgFoto').src = 'images/none.jpg';
                    document.all('imgFirma').src = 'images/none.jpg';
                    document.all("txtFoto").value = "";
                    document.all("txtFirma").value = "";
                    gHabilitarControl(document.all("txtNyap"), true);
                    gHabilitarControl(document.all("txtDocuNume"), true);
                    gHabilitarControl(document.all("cmbDoti"), true);
                    gHabilitarControl(document.all("filFoto"), true);
                    gHabilitarControl(document.all("filFirma"), true);
                    gHabilitarControl(document.all("txtFirm_mail"), true);
                    gHabilitarControl(document.all("txtFirm_celular_pais"), true);
                    gHabilitarControl(document.all("txtFirm_celular_area"), true);
                    gHabilitarControl(document.all("txtFirm_celular_numero"), true);
                }
                mSetearImagenes();
            }
    </script>

    <style type="text/css">
        .form-group {
            display: inline-block;
            margin-right: 10px;
            float: left;
        }
            .form-group label {
                display: block;
            }
    </style>
</head>
<body class="pagina" leftmargin="5" topmargin="5" rightmargin="0">

    <form id="frmABM" method="post" runat="server">

        <table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
            <tr>
                <td width="9">
                    <img height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recsup.jpg">
                    <img height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
                <td width="13">
                    <img height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9" background="imagenes/reciz.jpg">
                    <img height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table id="Table1" style="width: 100%; height: 100%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td>
                                <p></p>
                            </td>
                            <td height="5">
                                <asp:Label ID="lblTitu" runat="server" CssClass="opcion">Firmantes</asp:Label></td>
                            <td valign="top" align="right">&nbsp;
									<asp:ImageButton ID="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="images\Close.bmp"></asp:ImageButton></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td valign="top" align="center" colspan="2">
                                <asp:DataGrid ID="grdDato" runat="server" Width="100%" AllowPaging="True" BorderWidth="1px" HorizontalAlign="Center"
                                    CellSpacing="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
                                    BorderStyle="None">
                                    <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                    <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                    <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                    <FooterStyle CssClass="footer"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderStyle Width="2%"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="firm_id"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_soci_nume" HeaderText="Nro.Socio">
                                            <HeaderStyle Width="60px"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="_soci_ingr_fecha" HeaderText="F.Ingreso" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_firm_desc" HeaderText="Apellido y Nombre"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="firm_cargo" HeaderText="Cargo">
                                            <HeaderStyle Width="140px"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="firm_inic_fecha" HeaderText="F.Inicio" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle Width="70px"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="firm_venc_fecha" HeaderText="F.Vto" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle Width="70px"></HeaderStyle>
                                        </asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr height="10">
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center" colspan="2">
                                <div>
                                    <asp:Panel ID="panDato" runat="server" CssClass="titulo" Width="100%" BorderWidth="1px" BorderStyle="Solid">
                                        <table class="FdoFld" id="Table2" style="width: 100%" cellpadding="0" align="left" border="0">
                                            <tr>
                                                <td style="width: 14px" width="14">
                                                <td colspan="2">
                                                    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                                        <tr>
                                                            <td style="width: 109px" align="left" height="6"></td>
                                                            <td style="width: 10%" align="right" height="6"></td>
                                                            <td style="width: 100%" valign="top" align="left" colspan="4" height="6"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 109px" align="left">
                                                                <asp:RadioButton ID="rbtnFirm" onclick="mSetearFirm('S')" runat="server" Checked="True" GroupName="RadioGroup1"></asp:RadioButton></td>
                                                            <td style="width: 10%" align="right">
                                                                <asp:Label ID="Label2" runat="server" CssClass="titulo">Socio:</asp:Label>&nbsp;</td>
                                                            <td style="width: 100%" valign="top" align="left" colspan="4">
                                                                <uc1:CLIE ID="usrSoci" runat="server" width="100%" FilCUIT="True" Tabla="Socios" SoloBusq="True"
                                                                    Saltos="1,1,1" FilClieNume="True" FilSociNume="True" PermiModi="True" MuestraDesc="False"
                                                                    FilDocuNume="True"></uc1:CLIE>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 109px" align="left"></td>
                                                            <td style="width: 10%" align="right"></td>
                                                            <td style="width: 100%" valign="top" align="left" colspan="4">
                                                                <asp:Label ID="txtIngreFecha" runat="server" CssClass="titulo"></asp:Label>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" background="imagenes/formdivmed.jpg" colspan="8" height="2">
                                                                <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" align="left" height="4"></td>
                                                            <td style="width: 10%" nowrap align="right" height="4"></td>
                                                            <td style="width: 20%" align="left" colspan="3" height="4"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" align="left">
                                                                <asp:RadioButton ID="rbtnApe" onclick="mSetearFirm('A')" runat="server" GroupName="RadioGroup1"></asp:RadioButton></td>
                                                            <td style="width: 10%" nowrap align="right">
                                                                <asp:Label ID="lblApel" runat="server" CssClass="titulo">Apellido y Nombre:</asp:Label>&nbsp;</td>
                                                            <td style="width: 20%" align="left" colspan="3">
                                                                <cc1:TextBoxTab ID="txtNyap" runat="server" CssClass="cuadrotexto" Enabled="False" Obligatorio="False"
                                                                    Width="150px"></cc1:TextBoxTab></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" align="left" height="4"></td>
                                                            <td style="width: 10%" align="right" height="4"></td>
                                                            <td style="width: 20%" align="left" colspan="3" height="4"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" align="left"></td>
                                                            <td style="width: 10%" align="right">
                                                                <asp:Label ID="lblDocu" runat="server" CssClass="titulo">Tipo y Nro.Doc.:</asp:Label>&nbsp;</td>
                                                            <td style="width: 20%" align="left" colspan="3">
                                                                <cc1:ComboBox class="combo" ID="cmbDoti" runat="server" Width="84px"></cc1:ComboBox>
                                                                <cc1:NumberBox ID="txtDocuNume" runat="server" CssClass="cuadrotexto" Enabled="False" Width="112px"
                                                                    MaxLength="9" MaxValor="999999999"></cc1:NumberBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" align="left" height="4"></td>
                                                            <td style="width: 10%" align="right" height="4"></td>
                                                            <td style="width: 20%" align="left" colspan="3" height="4"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" align="left"></td>
                                                            <td style="width: 10%" valign="top" align="right">
                                                                <asp:Label ID="lblFirm" runat="server" CssClass="titulo">Firma:</asp:Label>&nbsp;</td>
                                                            <td style="width: 20%" align="left" colspan="3">
                                                                <cc1:TextBoxTab ID="txtFirma" runat="server" CssClass="cuadrotextodeshab" Width="370px" ReadOnly="false"></cc1:TextBoxTab>
                                                                <img id="imgDelFirm" style="cursor: hand" onclick="javascript:mLimpiarFirma();" alt="Limpiar Firma"
                                                                    src="images\del.gif" runat="server"><br>
                                                                <input id="filFirma" style="width: 400px; height: 22px" type="file" size="26"
                                                                    name="filFirma" runat="server"></td>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 64px" align="left">
                                                                            <asp:Image ID="imgFirma" Width="80px" runat="server" Height="80px" Border="1"></asp:Image></td>
                                                                        <td valign="top" align="left">
                                                                            <button style="z-index: 0; width: 80px" id="btnFotoVer" class="boton" onclick="javascript:mVerImagen('Firma');"
                                                                                runat="server" value="Ver Foto">
                                                                                Ver Firma</button>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 10%" align="left"></td>
                                                            <td style="width: 10%" valign="top" align="right">
                                                                <asp:Label ID="lblFoto" runat="server" CssClass="titulo">Foto:</asp:Label>&nbsp;</td>
                                                            <td style="width: 20%" align="left" colspan="3">
                                                                <cc1:TextBoxTab ID="txtFoto" runat="server" CssClass="cuadrotextodeshab" Width="370px" ReadOnly="false"></cc1:TextBoxTab><img id="imgDelFoto" style="cursor: hand" onclick="javascript:mLimpiarFoto();" alt="Limpiar Foto"
                                                                    src="images\del.gif" runat="server"><br>
                                                                <input id="filFoto" style="width: 400px; height: 22px" type="file" size="26" name="File1"
                                                                    runat="server"></td>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 64px; height: 32px" width="64" align="center">
                                                                            <asp:Image ID="imgFoto" Width="80px" runat="server" Height="80px" Border="1"></asp:Image></td>
                                                                        <td style="height: 32px" valign="top">
                                                                            <button style="z-index: 0; width: 80px" id="btnFirmaVer" class="boton" onclick="javascript:mVerImagen('Foto');"
                                                                                runat="server" value="Ver Firma">
                                                                                Ver Foto</button></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" background="imagenes/formdivmed.jpg" colspan="5" height="2">
                                                                <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            <td align="right">
                                                                <asp:Label ID="lblCargo" runat="server" CssClass="titulo">Cargo:</asp:Label>&nbsp;</td>
                                                            <td align="left" colspan="3">
                                                                <cc1:TextBoxTab ID="txtCargo" runat="server" CssClass="cuadrotexto" Obligatorio="True" Width="150px"></cc1:TextBoxTab>&nbsp;&nbsp;</td>
                                                            <td rowspan="2">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <label class="titulo">Mail:</label></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <cc1:TextBoxTab ID="txtFirm_mail" runat="server" CssClass="cuadrotexto" Obligatorio="False" Width="150px" MaxLength="100"></cc1:TextBoxTab>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td colspan="6">
                                                                                                    <label style="font-size: 10px; color: black" class="titulo">Celular: (C�digo de area sin 0 y N�mero sin 15)</label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <label class="titulo">Pais:</label></td>
                                                                                                <td>
                                                                                                    <cc1:TextBoxTab ID="txtFirm_celular_pais" runat="server" CssClass="cuadrotexto" Obligatorio="False" Width="30px" MaxLength="6"></cc1:TextBoxTab>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <label class="titulo">Area:</label></td>
                                                                                                <td>
                                                                                                    <cc1:TextBoxTab ID="txtFirm_celular_area" runat="server" CssClass="cuadrotexto" Obligatorio="False" Width="30px" MaxLength="5"></cc1:TextBoxTab>
                                                                                                <td>
                                                                                                    <label class="titulo">N�mero:</label></td>
                                                                                                <td>
                                                                                                    <cc1:TextBoxTab ID="txtFirm_celular_numero" runat="server" CssClass="cuadrotexto" Obligatorio="False" Width="120px" MaxLength="10"></cc1:TextBoxTab>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td align="right">
                                                                <asp:Label ID="lblInicFecha" runat="server" CssClass="titulo">Fecha Inicio: </asp:Label>&nbsp;</td>
                                                            <td align="left" colspan="3">
                                                                <cc1:DateBox ID="txtInicFecha" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																	<asp:Label ID="lblVencFecha" runat="server" CssClass="titulo">Fecha Vto.:</asp:Label>&nbsp;
																	<cc1:DateBox ID="txtVencFecha" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="4" height="2">
                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3" height="30"><a id="editar" name="editar"></a>
                                                    <asp:Button ID="btnAlta" runat="server" CssClass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button ID="btnBaja" runat="server" CssClass="boton" CausesValidation="False" Width="80px"
                                                            Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnModi" runat="server" CssClass="boton" CausesValidation="False" Width="80px"
                                                            Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnLimp" runat="server" CssClass="boton" CausesValidation="False" Width="80px"
                                                            Text="Limpiar"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!--- FIN CONTENIDO --->
                </td>
                <td width="13" background="imagenes/recde.jpg">
                    <img height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9">
                    <img height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recinf.jpg">
                    <img height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
                <td width="13">
                    <img height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
            </tr>
        </table>

        <div style="display: none">
            <asp:TextBox ID="lblMens" runat="server"></asp:TextBox><asp:TextBox ID="hdnId" runat="server"></asp:TextBox>
        </div>
    </form>

    <script type="text/javascript">
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
    </script>

</body>
</html>
