Namespace SRA

Partial Class ClientesIncob_FilPop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

   Private mstrConn As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      hdnId.Text = Request("lsin_id").ToString
      hdnIncl.Text = Request("Incl").ToString
      hdnEtapa.Text = Request("Etapa").ToString
      hdnTipo.Text = Request("Tipo").ToString

      mstrConn = clsWeb.gVerificarConexion(Me)

      'trPerio.Visible = (hdnTipo.Text <> "C")
      txtPeriFil.Enabled = (hdnTipo.Text <> "C")
      cmbPetiFil.Enabled = (hdnTipo.Text <> "C")
      txtAnioFil.Enabled = (hdnTipo.Text <> "C")
      txtComiAnio.Enabled = (hdnTipo.Text <> "C")
      cmbActi.Enabled = (hdnTipo.Text = "C")
      cmbSoci.Enabled = (hdnTipo.Text = "C")
      txtPeriDesde.Enabled = (hdnTipo.Text = "C")
      txtPeriHasta.Enabled = (hdnTipo.Text = "C")
      cmbCate.Enabled = (hdnTipo.Text <> "C")
      cmbEsta.Enabled = (hdnTipo.Text <> "C")
      If Not Page.IsPostBack Then
         If hdnTipo.Text = "C" Then
            lblTitu.Text = lblTitu.Text & " - " & "Deuda Cta. Cte."
         Else
            lblTitu.Text = lblTitu.Text & " - " & "Deuda Social"
         End If
         mCargarCombos()
      End If

   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPetiFil, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "T")
      clsWeb.gCargarCombo(mstrConn, "estados_socios_cargar ", cmbEsta, "id", "descrip", "T")

      If hdnTipo.Text = "C" Then 'solo para clientes
         clsWeb.gCargarRefeCmb(mstrConn, "incobrables", cmbSoci, "S")
         If cmbSoci.Items.Count = 1 And cmbSoci.SelectedValue <> "" Then
            cmbSoci.Items.Insert(0, "(Seleccione)")
            cmbSoci.Items(0).Value = ""
            cmbSoci.SelectedIndex = -1
         End If
      End If
   End Sub

    Private Sub cmbActi_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActi.SelectedIndexChanged

    End Sub
End Class
End Namespace
