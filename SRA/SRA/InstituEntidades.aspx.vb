Namespace SRA

Partial Class InstituEntidades
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_InstituEntidades

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String

   Private Enum Columnas As Integer
      InenId = 1
   End Enum

#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then

            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEntiFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEnti, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInstFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInst, "S")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnAlta.Attributes.Add("onclick", "return mConfirmar();")
      btnModi.Attributes.Add("onclick", "return mConfirmar();")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.instntesEntidades, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.instntesEntidades_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.instntesEntidades_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.instntesEntidades_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lDsDatos As DataSet

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.InenId).Text)
      lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With lDsDatos.Tables(0).Rows(0)
         cmbEnti.Valor = .Item("inen_enti_id")
         cmbInst.Valor = .Item("inen_inst_id")
         txtFecha.Fecha = .Item("inen_fecha")
         If Not .IsNull("inen_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("inen_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With
      mSetearEditor("", False)
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()
      cmbEntiFil.Valor = 0
      cmbInstFil.Valor = 0
      grdDato.Visible = False
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      cmbEnti.Limpiar()
      cmbInst.Limpiar()
      txtFecha.Text = ""
      lblBaja.Text = ""

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mConsultarGrilla()
   End Sub

   Private Sub mConsultarGrilla()
      Try
         mConsultar()
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      btnAgre.Visible = Not (panDato.Visible)
      panCabecera.Visible = True
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjinstnEnti.Alta()
         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjinstnEnti.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjinstnEnti.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("inen_enti_id") = cmbEnti.Valor
         .Item("inen_inst_id") = cmbInst.Valor
         .Item("inen_fecha") = txtFecha.Fecha
      End With

      Return ldsEsta
   End Function
#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

#Region "Detalle"

   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
         mstrCmd += " @inen_inst_id = " & cmbInstFil.Valor
         mstrCmd += ",@inen_enti_id = " & cmbEntiFil.Valor

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         mConsultar()
         mMostrarPanel(False)
         btnAgre.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "InstituEntidades"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&inen_inst_id=" + cmbInstFil.Valor.ToString
         lstrRpt += "&inen_enti_id=" + cmbEntiFil.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub
End Class
End Namespace
