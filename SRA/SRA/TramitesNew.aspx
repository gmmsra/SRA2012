﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TramitesNew.aspx.vb" Inherits="TramitesNew" %>

<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="~/controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="~/controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PRON" Src="~/controles/usrProdNuevo.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Trámites</title>
    <link href="stylesheet/SRA.css" rel="stylesheet" />
</head>
<body class="pagina" style="margin: 5px 0px 0px 5px">
    <form id="frmABM" runat="server">
        <table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
            <tr>
                <td style="width: 9px">
                    <img src="imagenes/recsupiz.jpg" alt="" width="9" /></td>
                <td style="background-image: url(imagenes/recsup.jpg)">
                    <img src="imagenes/recsup.jpg" alt="" height="10" width="9" /></td>
                <td style="width: 13px">
                    <img src="imagenes/recsupde.jpg" alt="" height="10" width="13" /></td>
            </tr>

            <tr>
                <td width="9" style="background-image: url(imagenes/reciz.jpg)">
                    <img height="10" src="imagenes/reciz.jpg" width="9" border="0" alt=""></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table id="Table1" style="WIDTH: 100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="HEIGHT: 25px" valign="bottom" colspan="3" height="25">
                                    <asp:Label ID="lblTituAbm" runat="server" CssClass="opcion">Trámites</asp:Label></td>
                            </tr>
                            <tr>
                                <td valign="middle" style="white-space: nowrap" colspan="3">
                                    <asp:Panel ID="panFiltros" runat="server" CssClass="titulo" Width="100%" BorderWidth="1px" BorderStyle="none">
                                        <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="HEIGHT: 8px" width="24"></td>
                                                            <td style="HEIGHT: 8px" width="42"></td>
                                                            <td style="HEIGHT: 8px" width="26"></td>
                                                            <td style="HEIGHT: 8px"></td>
                                                            <td style="HEIGHT: 8px" width="26">
                                                                <cc1:BotonImagen ID="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
                                                                    ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></cc1:BotonImagen></td>
                                                            <td style="HEIGHT: 8px" valign="middle" width="26">
                                                                <cc1:BotonImagen ID="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
                                                                    ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></cc1:BotonImagen></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="HEIGHT: 8px" width="24">
                                                                <img height="25" src="imagenes/formfle.jpg" width="24" border="0" alt=""></td>
                                                            <td style="HEIGHT: 8px" width="42">
                                                                <img height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0" alt=""></td>
                                                            <td style="HEIGHT: 8px" width="26">
                                                                <img height="25" src="imagenes/formcap.jpg" width="26" border="0" alt=""></td>
                                                            <td style="HEIGHT: 8px; background-image: url(imagenes/formfdocap.jpg)" colspan="3">
                                                                <img height="25" src="imagenes/formfdocap.jpg" border="0" alt=""></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="HEIGHT: 50px">
                                                    <table style="height: 100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tr>
                                                            <td width="3" style="background-image: url(imagenes/formiz.jpg)">
                                                                <img height="17" src="imagenes/formiz.jpg" width="3" border="0" alt=""></td>
                                                            <td style="HEIGHT: 100%">
                                                                <!-- FOMULARIO -->
                                                                <table class="FdoFld" style="height: 100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="HEIGHT: 10px; background-image: url(imagenes/formfdofields.jpg)" align="right" colspan="2"></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="HEIGHT: 10px; background-image: url(imagenes/formfdofields.jpg)" align="right" colspan="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="WIDTH: 15%" align="right" height="15">
                                                                            <asp:Label ID="lblNumeFil" runat="server" CssClass="titulo"> Nro.Trámite:</asp:Label>&nbsp;</td>
                                                                        <td style="WIDTH: 85%;text-align:left">
                                                                            <cc1:NumberBox ID="txtNumeFil" runat="server" CssClass="cuadrotexto" MaxValor="999999999" Width="140px"
                                                                                AceptaNull="False"></cc1:NumberBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" height="15">
                                                                            <asp:Label ID="lblEstadoFil" runat="server" CssClass="titulo">Estado:</asp:Label>&nbsp;</td>
                                                                        <td style=";text-align:left">
                                                                            <cc1:ComboBox class="combo" ID="cmbEstadoFil" runat="server" Width="200px" AceptaNull="False"
                                                                                NomOper="estados_cargar">
                                                                            </cc1:ComboBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                    </tr>
                                                                    <tr id="rowPaisFil" style="DISPLAY: inline;text-align:left" runat="server">
                                                                        <td align="right">
                                                                            <asp:Label ID="lblPaisFil" runat="server" CssClass="titulo">País:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:ComboBox class="combo" ID="cmbPaisFil" runat="server" Width="280px" NomOper="paises_cargar" AceptaNull="false">
                                                                            </cc1:ComboBox></td>
                                                                    </tr>
                                                                    <tr id="rowDivPaisFil" style="DISPLAY: inline" runat="server">
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" height="15">
                                                                            <asp:Label ID="lblRazaFil" runat="server" CssClass="titulo">Raza:</asp:Label>&nbsp;</td>
                                                                        <td style="text-align:left">
                                                                            <cc1:ComboBox class="combo" ID="cmbRazaFil" runat="server" CssClass="cuadrotexto" Width="294px"
                                                                                AceptaNull="False" NomOper="razas_cargar" Filtra="true" MostrarBotones="False" onchange="javascript:cmbRazaFil_change();"
                                                                                Height="20px">
                                                                            </cc1:ComboBox></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" height="15">
                                                                            <asp:Label ID="lblSraNumeFil" runat="server" CssClass="titulo">Nro.Producto:</asp:Label>&nbsp;</td>
                                                                        <td style="text-align:left">
                                                                            <cc1:NumberBox ID="txtSraNumeFil" runat="server" CssClass="cuadrotexto" Width="140px" AceptaNull="False"
                                                                                MaxValor="999999999"></cc1:NumberBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" align="right" height="15">
                                                                            <asp:Label ID="lblCriaFil" runat="server" CssClass="titulo">Raza/Criador:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <uc1:CLIE ID="usrCriaFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
                                                                                Saltos="1,2" Criador="true" Tabla="Criadores"></uc1:CLIE>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" height="15">
                                                                            <asp:Label ID="lblFechaDesdeFil" runat="server" CssClass="titulo">Fecha Desde:</asp:Label>&nbsp;</td>
                                                                        <td style="text-align:left">
                                                                            <cc1:DateBox ID="txtFechaDesdeFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:Label ID="lblFechaHastaFil" runat="server" CssClass="titulo">Hasta:&nbsp;</asp:Label>
                                                                            <cc1:DateBox ID="txtFechaHastaFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" height="15">
                                                                            <asp:Label ID="lblProdFil" runat="server" CssClass="titulo">Producto:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <uc1:PROD ID="usrProdFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
                                                                                Saltos="1,2" Tabla="productos" MuestraDesc="True" FilTipo="S" FilSociNume="True" AutoPostback="False"></uc1:PROD>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="white-space: nowrap" align="right" background="imagenes/formfdofields.jpg"></td>
                                                                        <td width="72%" background="imagenes/formfdofields.jpg">
                                                                            <asp:CheckBox ID="chkVisto" Text="Incluir Registros Vistos y/o Aprobados" CssClass="titulo" runat="server"></asp:CheckBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colspan="2"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="HEIGHT: 17px; background-image:url(imagenes/formde.jpg)" width="2">
                                                                <img height="2" src="imagenes/formde.jpg" width="2" border="0"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" colspan="3">
                                    <asp:DataGrid ID="grdDato" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None"
                                        AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                        AllowPaging="True"> <%-- OnUpdateCommand="mEditarDatos" OnPageIndexChanged="grdDato_Page">--%>
                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                        <FooterStyle CssClass="footer"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton5" runat="server" CausesValidation="false" CommandName="Update">
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:pointer" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:pointer;" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="tram_id"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_inic_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha Inic."></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_nume" HeaderText="Nº Trámite" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="raza" HeaderText="Raza" HeaderStyle-Width="20%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="sexo" HeaderText="Sexo"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="prdt_nomb" HeaderText="Nombre" HeaderStyle-Width="30%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="_numero" HeaderText="Nº Producto" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="_cria_nume" HeaderText="Nro. Criador" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="esta_desc" HeaderText="Estado"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td colspan="3" height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" colspan="3">
                                    <cc1:BotonImagen ID="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
                                        ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
                                        ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Agregar un nuevo trámite"></cc1:BotonImagen></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3">
                                    <asp:Panel ID="panLinks" runat="server" CssClass="titulo" Width="100%" Height="124%" Visible="False">
                                        <table id="TablaSolapas" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                                            <tr>
                                                <td style="WIDTH: 0.24%">
                                                    <img height="27" src="imagenes/tab_a.bmp" width="9" border="0" alt=""></td>
                                                <td style="background-image: url(imagenes/tab_b.bmp)">
                                                    <img height="27" src="imagenes/tab_b.bmp" width="8" border="0" alt=""></td>
                                                <td width="1">
                                                    <img height="27" src="imagenes/tab_c.bmp" width="31" border="0" alt=""></td>
                                                <td width="1" style="background-image: url(imagenes/tab_fondo.bmp)">
                                                    <asp:LinkButton ID="lnkCabecera" Style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
                                                        CssClass="solapa" Width="80px" Height="21px" CausesValidation="False"> Trámite </asp:LinkButton></td>
                                                <td width="1">
                                                    <img height="27" src="imagenes/tab_f.bmp" width="27" border="0" alt=""></td>
                                                <td width="1" style="background-image: url(imagenes/tab_fondo.bmp)">
                                                    <asp:LinkButton ID="lnkRequ" Style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Requisitos</asp:LinkButton></td>
                                                <td width="1">
                                                    <img height="27" src="imagenes/tab_f.bmp" width="27" border="0" alt=""></td>
                                                <td width="1" style="background-image: url(imagenes/tab_fondo.bmp)">
                                                    <asp:LinkButton ID="lnkDocu" Style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Documentación </asp:LinkButton></td>
                                                <td width="1">
                                                    <img height="27" src="imagenes/tab_f.bmp" width="27" border="0" alt=""></td>
                                                <td width="1" style="background-image: url(imagenes/tab_fondo.bmp)">
                                                    <asp:LinkButton ID="lnkObse" Style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Comentarios</asp:LinkButton></td>
                                                <td width="0">
                                                    <!--IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"-->
                                                </td>
                                                <td width="1" style="background-image: url(imagenes/tab_fondo.bmp)">
                                                    <asp:LinkButton ID="lnkVend" Style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" CssClass="solapa"
                                                        Width="70px" Visible="False" Height="21px" CausesValidation="False"> Propietarios </asp:LinkButton></td>
                                                <td width="0" style="background-image: url(imagenes/tab_fondo.bmp)">
                                                    <!--IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"-->
                                                </td>
                                                <td width="1" style="background-image: url(imagenes/tab_fondo.bmp)">
                                                    <asp:LinkButton ID="lnkComp" Style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" Visible="False" CausesValidation="False"> Compradores </asp:LinkButton></td>
                                                <td width="1">
                                                    <img height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0" alt=""></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3">
                                    <asp:Panel ID="panDato" runat="server" CssClass="titulo" Width="100%" BorderWidth="1px" BorderStyle="Solid"
                                        Height="116px" Visible="False">
                                        <table class="FdoFld" id="tabDato" style="WIDTH: 100%; HEIGHT: 106px" cellpadding="0" align="left"
                                            border="0">
                                            <tr>
                                                <td height="5" style="text-align:left">
                                                    <asp:Label ID="lblTitu" runat="server" CssClass="titulo" Width="100%"></asp:Label></td>
                                                <td valign="top" align="right">
                                                    <asp:ImageButton ID="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 100%" align="center" colspan="2">
                                                    <asp:Panel ID="panCabecera" runat="server" CssClass="titulo" Width="100%">
                                                        <table id="tabCabecera" style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                            <tr>
                                                                <td style="white-space: nowrap" align="right" width="18%">
                                                                    <asp:Label ID="lblInicFecha" runat="server" CssClass="titulo">Fecha Inicio:&nbsp;</asp:Label></td>
                                                                <td style="text-align:left">
                                                                    <cc1:DateBox ID="txtInicFecha" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																		<asp:Label ID="lblFinaFecha" runat="server" CssClass="titulo">Fecha Cierre:</asp:Label>
                                                                    <cc1:DateBox ID="txtFinaFecha" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="white-space: nowrap" align="right" width="18%">
                                                                    <asp:Label ID="lblFechaTransferencia" runat="server" CssClass="titulo">Fecha Transferencia:&nbsp;</asp:Label></td>
                                                                <td style="text-align:left">
                                                                    <cc1:DateBox ID="txtFechaTransf" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="True"></cc1:DateBox>
                                                                &nbsp;&nbsp;&nbsp;
																		
                                                            </tr>
                                                            <tr>
                                                                <td style="white-space: nowrap" align="right">
                                                                    <asp:Label ID="lblFechaTram" runat="server" CssClass="titulo">Fecha Tramite:&nbsp;</asp:Label></td>
                                                                <td align="left">
                                                                    <cc1:DateBox ID="txtFechaTram" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:Label ID="lblEsta" runat="server" CssClass="titulo">Estado:&nbsp;</asp:Label></td>
                                                                <td align="left">
                                                                    <cc1:ComboBox class="combo" ID="cmbEsta" runat="server" CssClass="cuadrotexto" Width="180px"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" height="15">
                                                                    <asp:Label ID="lblVariedad" runat="server" CssClass="titulo">Variedad:</asp:Label></td>
                                                                <td align="left">
                                                                    <cc1:ComboBox class="combo" ID="cmbVari" runat="server" Width="67px" NomOper="rg_registros_tipos_cargar"
                                                                        DESIGNTIMEDRAGDROP="2076">
                                                                    </cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" height="15">
                                                                    <asp:Label ID="lblTipoRegistro" runat="server" CssClass="titulo">Tipo de Registro:</asp:Label>&nbsp;</td>
                                                                <td style="text-align:left">
                                                                    <cc1:ComboBox class="combo" ID="cmbRegiTipo" runat="server" Width="77px" NomOper="rg_registros_tipos_cargar"
                                                                        DESIGNTIMEDRAGDROP="2076">
                                                                    </cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="white-space: nowrap" align="center" colspan="2">
                                                                    <asp:Label ID="lblTranPlan" runat="server" CssClass="titulo"><br>Se ha detectado más de un producto para este trámite.<br>Puede modificar los mismos desde la opción de Transferencia por Planilla.<br></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <uc1:PRON ID="usrProd" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True" Saltos="1,2"
                                                                Tabla="productos" MuestraDesc="True" FilTipo="S" FilSociNume="True" AutoPostBack="False"></uc1:PRON>
                                                            <tr id="rowDosisCria" style="DISPLAY: none" runat="server">
                                                                <td colspan="2">
                                                                    <table style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                                        <tr>
                                                                            <td align="right" width="18%">
                                                                                <asp:Label ID="lblDosiCant" runat="server" CssClass="titulo">Cantidad Dosis:</asp:Label></td>
                                                                            <td>
                                                                                <cc1:NumberBox ID="txtDosiCant" runat="server" CssClass="cuadrotexto" MaxValor="999999999" Width="81px"
                                                                                    Visible="true" MaxLength="3" EsPorcen="False" EsDecimal="False"></cc1:NumberBox>
                                                                                <asp:Label ID="lblCriaCant" runat="server" CssClass="titulo">&nbsp;&nbsp;Cantidad Crias:&nbsp;</asp:Label>
                                                                                <cc1:NumberBox ID="txtCriaCant" runat="server" CssClass="cuadrotexto" MaxValor="999999999" Width="81px"
                                                                                    Visible="true" EsPorcen="False" EsDecimal="False"></cc1:NumberBox></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                                <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="rowCantEmbr" style="DISPLAY: none" runat="server">
                                                                <td colspan="2">
                                                                    <table style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                                        <tr>
                                                                            <td align="right" width="18%">
                                                                                <asp:Label ID="lblCantEmbr" runat="server" CssClass="titulo">Cantidad:</asp:Label></td>
                                                                            <td style="white-space: nowrap">
                                                                                <cc1:NumberBox ID="txtCantEmbr" runat="server" CssClass="cuadrotexto" MaxValor="999999999" Width="81px"
                                                                                    AceptaNull="false" Visible="true" EsPorcen="False" EsDecimal="False"></cc1:NumberBox>&nbsp;
																					<asp:Label ID="lblRecuFecha" runat="server" CssClass="titulo">Fecha Recup.:</asp:Label>&nbsp;
																					<cc1:DateBox ID="txtRecuFecha" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																					<asp:Button ID="btnTeDenu" runat="server" CssClass="boton" Width="90px" Visible="False" Text="Denuncia TE"></asp:Button>&nbsp;
																					<cc1:TextBoxTab ID="txtTeDesc" runat="server" CssClass="cuadrotexto" Width="176px" Visible="False"
                                                                                        Enabled="False" ReadOnly="True"></cc1:TextBoxTab></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                                <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="rowProp" style="DISPLAY: inline" runat="server">
                                                                <td valign="top" align="right">
                                                                    <asp:Label ID="lblClieProp" runat="server" CssClass="titulo">Propietario Actual:&nbsp;</asp:Label></td>
                                                                <td>
                                                                    <uc1:CLIE ID="usrClieProp" runat="server" AceptaNull="true" FilDocuNume="True" Saltos="1,2"
                                                                        Tabla="Clientes" Activo="False"></uc1:CLIE>
                                                                </td>
                                                            </tr>
                                                            <tr id="rowDivProp" style="DISPLAY: inline" runat="server">
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr id="rowImpo" style="DISPLAY: inline" runat="server">
                                                                <td valign="top" align="right">
                                                                    <asp:Label ID="lblImpo" runat="server" CssClass="titulo">Despachante:&nbsp;</asp:Label></td>
                                                                <td>
                                                                    <cc1:ComboBox class="combo" ID="cmbImpo" runat="server" Width="280px"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr id="rowDivImpo" style="DISPLAY: inline" runat="server">
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr id="rowPais" style="DISPLAY: inline" runat="server">
                                                                <td valign="top" align="right">
                                                                    <asp:Label ID="lblPais" runat="server" CssClass="titulo">País:&nbsp;</asp:Label></td>
                                                                <td>
                                                                    <cc1:ComboBox class="combo" ID="cmbPais" runat="server" Width="280px" NomOper="paises_cargar"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr id="rowDivPais" style="DISPLAY: inline" runat="server">
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="right">
                                                                    <asp:Label ID="lblClieComp" runat="server" CssClass="titulo">Comprador Cliente:&nbsp;</asp:Label></td>
                                                                <td>
                                                                    <uc1:CLIE ID="usrClieComp" runat="server" AceptaNull="true" FilDocuNume="True" Saltos="1,2"
                                                                        Tabla="Clientes"></uc1:CLIE>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="right">
                                                                    <asp:Label ID="lblCriaComp" runat="server" CssClass="titulo">Comprador Criador:&nbsp;</asp:Label></td>
                                                                <td>
                                                                    <cc1:ComboBox class="combo" ID="cmbCriaComp" runat="server" Width="340px" NomOper="paises_cargar"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="right">
                                                                    <asp:Label ID="lblClieVend" runat="server" CssClass="titulo">Vendedor Cliente:&nbsp;</asp:Label></td>
                                                                <td>
                                                                    <uc1:CLIE ID="usrClieVend" runat="server" AceptaNull="True" FilDocuNume="True" Saltos="1,2"
                                                                        Tabla="Clientes"></uc1:CLIE>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="right">
                                                                    <asp:Label ID="lblCriaVend" runat="server" CssClass="titulo">Vendedor Criador:&nbsp;</asp:Label></td>
                                                                <td style="text-align:left">
                                                                    <cc1:ComboBox class="combo" ID="cmbCriaVend" runat="server" Width="340px" NomOper="paises_cargar"></cc1:ComboBox></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 100%" colspan="2">
                                                    <asp:Panel ID="panRequ" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                                        <table id="tabRequ" style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                            <tr>
                                                                <td style="WIDTH: 100%" colspan="2">
                                                                    <asp:DataGrid ID="grdRequ" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None"
                                                                        AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                                                        AllowPaging="True"> <%-- OnEditCommand="mEditarDatosRequ"  OnPageIndexChanged="grdRequ_Page" >--%>
                                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                                        <Columns>
                                                                            <asp:TemplateColumn>
                                                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:pointer" />
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn Visible="False" DataField="trad_id"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
                                                                                <HeaderStyle Width="40%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_pend" HeaderText="Pendiente"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_manu" HeaderText="Manual"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                    </asp:DataGrid></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr style="text-align:left">
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblRequRequ" runat="server" CssClass="titulo">Requisito:</asp:Label>&nbsp;</td>
                                                                <td style="WIDTH: 75%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg);text-align:left">
                                                                    <cc1:ComboBox class="combo" ID="cmbRequRequ" runat="server" CssClass="cuadrotexto" Width="80%"
                                                                        NomOper="rg_requisitos_cargar" Filtra="true" MostrarBotones="False">
                                                                    </cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right"></td>
                                                                <td style="WIDTH: 75%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)">
                                                                    <asp:CheckBox ID="chkRequPend" Text="Pendiente" runat="server" CssClass="titulo" Checked="False"></asp:CheckBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblRequManuTit" runat="server" CssClass="titulo">Manual:</asp:Label>&nbsp;</td>
                                                                <td style="WIDTH: 75%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)">
                                                                    <asp:Label ID="lblRequManu" runat="server" CssClass="Desc" style="text-align:left">Si</asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblRequFinaFechaTit" runat="server" CssClass="titulo">Fecha Finalización:</asp:Label>&nbsp;</td>
                                                                <td style="WIDTH: 75%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)">
                                                                    <asp:Label ID="lblRequFinaFecha" runat="server" CssClass="Desc"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" align="center" colspan="2" height="30">
                                                                    <asp:Button ID="btnAltaRequ" runat="server" CssClass="boton" Width="100px" Text="Agregar Req."></asp:Button>&nbsp;
																		<asp:Button ID="btnBajaRequ" runat="server" CssClass="boton" Width="100px" Text="Eliminar Req."></asp:Button>&nbsp;
																		<asp:Button ID="btnModiRequ" runat="server" CssClass="boton" Width="100px" Text="Modificar Req."></asp:Button>&nbsp;
																		<asp:Button ID="btnLimpRequ" runat="server" CssClass="boton" Width="100px" Text="Limpiar Req."></asp:Button></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 100%" valign="top" align="center" colspan="2">
                                                    <asp:Panel ID="panDocu" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                                        <table id="tabDocu" style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                            <tr>
                                                                <td style="WIDTH: 100%" align="center" colspan="2">
                                                                    <asp:DataGrid ID="grdDocu" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None"
                                                                        AutoGenerateColumns="False" CellPadding="1" GridLines="None"
                                                                        CellSpacing="1" HorizontalAlign="Center" AllowPaging="True"><%--  OnPageIndexChanged="grdDocu_Page" OnUpdateCommand="mEditarDatosDocu" >--%>
                                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                                        <Columns>
                                                                            <asp:TemplateColumn>
                                                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:pointer;" />
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn Visible="False" DataField="trdo_id" HeaderText="trdo_id"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="trdo_path" HeaderText="Documento">
                                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="trdo_refe" HeaderText="Referencia">
                                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                    </asp:DataGrid></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblDocuDocu" runat="server" CssClass="titulo">Documento:</asp:Label>&nbsp;</td>
                                                                <td style="WIDTH: 75%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)">
                                                                    <cc1:TextBoxTab ID="txtDocuDocu" runat="server" CssClass="cuadrotexto" Width="340px" ReadOnly="True"></cc1:TextBoxTab><img id="imgDelDocuDoc" style="CURSOR: pointer" onclick="javascript:mLimpiarPath('txtDocuDocu','imgDelDocuDoc');"
                                                                        alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<br>
                                                                    <input id="filDocuDocu" style="WIDTH: 340px; HEIGHT: 22px" type="file" size="49" name="File1"
                                                                        runat="server">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblDocuObse" runat="server" CssClass="titulo">Referencia:</asp:Label>&nbsp;</td>
                                                                <td style="WIDTH: 75%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)">
                                                                    <cc1:TextBoxTab ID="txtDocuObse" runat="server" CssClass="cuadrotexto" Width="80%" AceptaNull="False"
                                                                        Height="41px" TextMode="MultiLine"></cc1:TextBoxTab></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" align="center" colspan="2" height="34">
                                                                    <asp:Button ID="btnAltaDocu" runat="server" CssClass="boton" Width="110px" Text="Agregar Docum."></asp:Button>&nbsp;
																		<asp:Button ID="btnBajaDocu" runat="server" CssClass="boton" Width="110px" Visible="true" Text="Eliminar Docum."></asp:Button>&nbsp;
																		<asp:Button ID="btnModiDocu" runat="server" CssClass="boton" Width="110px" Visible="true" Text="Modificar Docum."></asp:Button>&nbsp;
																		<asp:Button ID="btnLimpDocu" runat="server" CssClass="boton" Width="110px" Visible="true" Text="Limpiar Docum."></asp:Button>&nbsp;&nbsp;
                                                                    <button class="boton" id="btnDescDocu" style="FONT-WEIGHT: bold; WIDTH: 110px" onclick="javascript:mDescargarDocumento('hdnDocuId','tramites_docum','txtDocuDocu');"
                                                                        type="button" runat="server" value="Descargar">
                                                                        Descargar</button></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 100%" colspan="2">
                                                    <asp:Panel ID="panObse" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                                        <table id="tabObse" style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                            <tr>
                                                                <td style="WIDTH: 100%" colspan="2">
                                                                    <asp:DataGrid ID="grdObse" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None"
                                                                        AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                                                        AllowPaging="True"><%-- OnEditCommand="mEditarDatosObse"  OnPageIndexChanged="grdObse_Page">--%>
                                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                                        <Columns>
                                                                            <asp:TemplateColumn>
                                                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:pointer;" />
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn Visible="False" DataField="trao_id"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="trao_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
                                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="trao_obse" HeaderText="Comentarios">
                                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                    </asp:DataGrid></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblObseFecha" runat="server" CssClass="titulo">Fecha:</asp:Label>&nbsp;</td>
                                                                <td style="HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg);text-align:left">
                                                                    <cc1:DateBox ID="txtObseFecha" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblObseRequ" runat="server" CssClass="titulo">Requisito:</asp:Label>&nbsp;</td>
                                                                <td style="HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg); text-align:left">
                                                                    <cc1:ComboBox class="combo" ID="cmbObseRequ" runat="server" CssClass="cuadrotexto" Width="80%"
                                                                        NomOper="rg_requisitos_cargar" Filtra="true" MostrarBotones="False">
                                                                    </cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 25%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)" align="right">
                                                                    <asp:Label ID="lblObseObse" runat="server" CssClass="titulo">Comentario:</asp:Label>&nbsp;</td>
                                                                <td style="WIDTH: 75%; HEIGHT: 14px; background-image: url(imagenes/formfdofields.jpg)">
                                                                    <cc1:TextBoxTab ID="txtObseObse" runat="server" CssClass="cuadrotexto" Width="80%" Height="41px"
                                                                        TextMode="MultiLine"></cc1:TextBoxTab></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" align="center" colspan="2" height="30">
                                                                    <asp:Button ID="btnAltaObse" runat="server" CssClass="boton" Width="100px" Text="Agregar Com."></asp:Button>&nbsp;
																		<asp:Button ID="btnBajaObse" runat="server" CssClass="boton" Width="100px" Text="Eliminar Com."></asp:Button>&nbsp;
																		<asp:Button ID="btnModiObse" runat="server" CssClass="boton" Width="100px" Text="Modificar Com."></asp:Button>&nbsp;
																		<asp:Button ID="btnLimpObse" runat="server" CssClass="boton" Width="100px" Text="Limpiar Com."></asp:Button></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 100%" colspan="2">
                                                    <asp:Panel ID="panVend" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                                        <table id="tabPers" style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                            <tr>
                                                                <td style="WIDTH: 100%" colspan="3">
                                                                    <asp:DataGrid ID="grdVend" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None" 
                                                                        AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                                                        AllowPaging="True"><%-- OnPageIndexChanged="grdVend_Page">--%>
                                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                                        <Columns>
                                                                            <asp:BoundColumn Visible="False" DataField="trpe_id"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_nombre" HeaderText="Nombre y Apellido/R.Social"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="trpe_porc" HeaderText="Porcentaje"></asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                    </asp:DataGrid></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 100%" colspan="2">
                                                    <asp:Panel ID="panComp" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                                        <table id="Table2" style="WIDTH: 100%" cellpadding="0" align="left" border="0">
                                                            <tr>
                                                                <td style="WIDTH: 100%" colspan="3">
                                                                    <asp:DataGrid ID="grdComp" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None" 
                                                                        AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                                                        AllowPaging="True"> <%-- OnEditCommand="mEditarDatosComp" OnPageIndexChanged="grdComp_Page">--%>
                                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                                        <Columns>
                                                                            <asp:TemplateColumn>
                                                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:pointer;" />
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn Visible="False" DataField="trpe_id"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="_nombre" HeaderText="Nombre y Apellido/R.Social"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="trpe_porc" HeaderText="Porcentaje"></asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                    </asp:DataGrid></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" height="1">
                                                                    <asp:Label ID="lblNombComp" runat="server" CssClass="titulo"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 15%" align="right" height="1">
                                                                    <asp:Label ID="lblPorcComp" runat="server" CssClass="titulo">Porcentaje:</asp:Label></td>
                                                                <td style="text-align:left" >
                                                                    <cc1:NumberBox ID="txtPorcComp" runat="server" CssClass="cuadrotexto" MaxValor="100" Width="60px"
                                                                        MaxLength="3" EsDecimal="False"></cc1:NumberBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="WIDTH: 15%" align="right">
                                                                    <asp:Label ID="lblObseComp" runat="server" CssClass="titulo">Observaciones:</asp:Label></td>
                                                                <td style="text-align:left">
                                                                    <cc1:TextBoxTab ID="txtObseComp" runat="server" CssClass="cuadrotexto" Width="80%" Height="41px"
                                                                        TextMode="MultiLine"></cc1:TextBoxTab></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="background-image: url(imagenes/formdivmed.jpg)" colspan="2" height="2">
                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1" alt=""></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="middle" align="center" colspan="3" height="30">
                                                                    <asp:Button ID="btnModiComp" runat="server" CssClass="boton" Width="100px" Text="Modificar"></asp:Button>&nbsp;
																		<asp:Button ID="btnLimpComp" runat="server" CssClass="boton" Width="100px" Text="Limpiar"></asp:Button></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="panBotones" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblBaja" runat="server" CssClass="titulo" ForeColor="Red"></asp:Label></td>
                                            </tr>
                                            <tr style="height: 30px">
                                                <td valign="middle" align="center"><a id="editar" name="editar"></a>
                                                    <asp:Button ID="btnAlta" runat="server" CssClass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnBaja" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnModi" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnLimp" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Limpiar"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnErr" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Ver Errores"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnStock" runat="server" CssClass="boton" Width="110px" CausesValidation="False"
                                                            Text="Stock Semen" Visible="false"></asp:Button></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--- FIN CONTENIDO --->
                </td>
                <td width="13" style="background-image: url(imagenes/recde.jpg)">
                    <img height="10" src="imagenes/recde.jpg" width="13" border="0" alt=""></td>
            </tr>

        </table>
    </form>
</body>
</html>
