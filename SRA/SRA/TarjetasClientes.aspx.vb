' Dario 2013-10-01 Por cambio en procampo
Namespace SRA

Partial Class TarjetasClientes
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Public mstrCmd As String
    Public mstrTabla As String = SRA_Neg.Constantes.gTab_TarjetasClientes
    Public mstrTablaExten As String = SRA_Neg.Constantes.gTab_TarjetasClientesExten
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If Not Page.IsPostBack Then
                ' Dario 2013-10-02 se desabilita el control para que se habilite cuando el combo tiene algun valor
                Me.txtNumero.Enabled = False

                mCargarCombos()
                mSetearEventos()
                mSetearMaxLength()
                mConsultar()
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        ' Dario 2013-10-01 se comenta ya que ahora el maxlength se 
        ' hace segun el dato seleccionado del combo tarjeta
        ' si la tarjeta tiene en true o false el campo tarj_PermiteDebitoEnCuenta
        ' txtNumero.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacl_nume")
        ' fin cambio

        Me.txtDocuNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacl_docu_nume")

        txtDomicilio.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacl_domi")
        txtCodigoSeg.MaxLength = 4

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaExten)
        txtExtenNombre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacx_exte")
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjetaFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjeta, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBcoEmisor, "N")
        clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipo, "T")
    End Sub
    Private Sub mEstablecerPerfil()
        If (Not clsSQLServer.gMenuPermi(CType(Opciones.Usuarios, String), (mstrConn), (Session("sUserId").ToString()))) Then
            Response.Redirect("noaccess.aspx")
        End If
        btnAlta.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Baja, String), (mstrConn), (Session("sUserId").ToString()))
        btnModi.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        btnLimp.Visible = (btnAlta.Visible Or btnBaja.Visible Or btnModi.Visible)
    End Sub
    Public Sub mInicializar()
        usrClieFil.Tabla = "clientes"
        usrClieFil.AutoPostback = True
        'Clave unica
        usrClieFil.ColClaveUnica = True
        usrClieFil.FilClaveUnica = True
        'Numero de cliente
        usrClieFil.ColClieNume = True
        usrClieFil.ColCUIT = True
        usrClieFil.FilCUIT = True
        'Numero de documento
        usrClieFil.ColDocuNume = True
        usrClieFil.FilDocuNume = True
        'Numero de socio
        usrClieFil.ColSociNume = True
        usrClieFil.FilSociNume = True
        usrClieFil.FilTipo = "T"

        usrCliente.Tabla = "clientes"
        usrCliente.AutoPostback = True
        'Clave unica
        usrCliente.ColClaveUnica = True
        usrCliente.FilClaveUnica = True
        'Numero de cliente
        usrCliente.ColClieNume = True
        usrCliente.ColCUIT = True
        usrCliente.FilCUIT = True
        'Numero de documento
        usrCliente.ColDocuNume = True
        usrCliente.FilDocuNume = True
        'Numero de socio
        usrCliente.ColSociNume = True
        usrCliente.FilSociNume = True
        usrCliente.FilTipo = "T"
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridExten_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdExten.EditItemIndex = -1
            If (grdExten.CurrentPageIndex < 0 Or grdExten.CurrentPageIndex >= grdExten.PageCount) Then
                grdExten.CurrentPageIndex = 0
            Else
                grdExten.CurrentPageIndex = E.NewPageIndex
            End If
            grdExten.DataSource = mdsDatos.Tables(mstrTablaExten)
            grdExten.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd = mstrCmd + " @tacl_clie_id=" + usrClieFil.Valor.ToString
            mstrCmd = mstrCmd + ",@tacl_tarj_id=" + IIf(cmbTarjetaFil.Valor.ToString = "", "0", cmbTarjetaFil.Valor)
            clsWeb.gCargarDataGrid((mstrConn), mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultarExten()
        Try
            mstrCmd = "exec " + mstrTablaExten + "_consul null,"
            mstrCmd = mstrCmd + hdnId.Text
            clsWeb.gCargarDataGrid((mstrConn), mstrCmd, grdExten)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaExten

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdExten.DataSource = mdsDatos.Tables(mstrTablaExten)
        grdExten.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    'Solapa General
                    hdnId.Text = .Item("tacl_id")
                    usrCliente.Valor = .Item("tacl_clie_id")
                    cmbTarjeta.Valor = .Item("tacl_tarj_id")
                    txtNumero.Text = .Item("tacl_nume")
                    txtFechaVenc.Fecha = IIf(.Item("tacl_vcto_fecha").ToString.Length > 0, .Item("tacl_vcto_fecha"), String.Empty)
                    txtTitular.Valor = IIf(.Item("tacl_titu").ToString.Length > 0, .Item("tacl_titu"), String.Empty)
                    'Solapa Detalle
                    hdnCheck.Checked = .Item("tacl_vta_tele")
                    hdnBancoEmisor.Text = .Item("tacl_banc_id").ToString
                    hdnDireccion.Text = .Item("tacl_domi").ToString
                    hdnCodigo.Text = SRA_Neg.Encript.gDesencriptar(.Item("tacl_code_segu").ToString)
                    hdnSocio.Text = .Item("_socio").ToString
                    hdnLegajo.Text = .Item("_alumno").ToString
                    hdnInstituto.Text = .Item("_instituto").ToString

                    chkVentaTelefono.Checked = .Item("tacl_vta_tele")
                    hdnBancoEmisor.Text = .Item("tacl_banc_id").ToString
                    cmbBcoEmisor.Valor = .Item("tacl_banc_id").ToString
                    hdnDireccion.Text = .Item("tacl_domi").ToString
                    txtDomicilio.Text = IIf(.Item("tacl_domi").ToString.Length > 0, .Item("tacl_domi").ToString, String.Empty)
                    hdnCodigo.Text = SRA_Neg.Encript.gDesencriptar(.Item("tacl_code_segu").ToString)
                    hdnSocio.Text = .Item("_socio").ToString
                    hdnLegajo.Text = .Item("_alumno").ToString
                    hdnInstituto.Text = .Item("_instituto").ToString

                    ' Dario 2013-10-01 nuevos campos
                    If Not .IsNull("tacl_cuit") Then
                        Me.txtCuit.Text = Me.FormatearCUIT(.Item("tacl_cuit"))
                    Else
                        Me.txtCuit.Text = ""
                    End If

                    If Not .IsNull("tacl_doti_id") Then
                        Me.cmbDocuTipo.Valor = .Item("tacl_doti_id")
                    Else
                        Me.cmbDocuTipo.Valor = "0"
                    End If

                    If Not .IsNull("tacl_docu_nume") Then
                        Me.txtDocuNume.Valor = .Item("tacl_docu_nume")
                    Else
                        Me.txtDocuNume.Text = ""
                    End If
                    ' Dario 2013-10-01 llamo a la funcion que determina si hay que mostrar o ocultar 
                    ' los controles de documento
                    Me.MostrarOcultarPanelesDocumentos(.Item("tacl_tarj_id"))

                End With

                mSetearEditor(False)
                mMostrarPanel(True)
                mShowTabs(1)
            End If
        End Sub
    Public Sub mEditarDatosExten(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrExten As DataRow

            hdnExtenId.Text = E.Item.Cells(1).Text
            ldrExten = mdsDatos.Tables(mstrTablaExten).Select("tacx_id=" & hdnExtenId.Text)(0)

            With ldrExten
                txtExtenNombre.Valor = .Item("tacx_exte")
            End With
            mSetearEditorExten(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorExten(ByVal pbooAlta As Boolean)
        btnBajaExten.Enabled = Not (pbooAlta)
        btnModiExten.Enabled = Not (pbooAlta)
        btnAltaExten.Enabled = pbooAlta
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        'Solapa General
        hdnId.Text = ""
        usrCliente.Limpiar()
        cmbTarjeta.Limpiar()
        txtCuit.Text = ""
        cmbDocuTipo.Limpiar()
        txtDocuNume.Text = ""
        txtNumero.Text = ""
        txtFechaVenc.Text = ""
        txtTitular.Text = ""
        'Solapa Detalle
        chkVentaTelefono.Checked = False
        cmbBcoEmisor.Limpiar()
        txtDomicilio.Text = ""
        txtCodigoSeg.Text = ""
        txtSocio.Text = ""
        txtLegajo.Text = ""
        hdnCheck.Checked = False
        hdnBancoEmisor.Text = ""
        hdnDireccion.Text = ""
        hdnCodigo.Text = ""
        hdnSocio.Text = ""
        hdnLegajo.Text = ""
        hdnInstituto.Text = ""
        mLimpiarExten()
        mCrearDataSet("")
        mSetearEditor(True)
        mShowTabs(1)
        'Dario 2013-10-01 acomodo los paneles de documentos 
        Me.MostrarOcultarPanelesDocumentos(Me.cmbTarjeta.Valor)
    End Sub
    Private Sub mLimpiarExten()
        hdnExtenId.Text = ""
        txtExtenNombre.Text = ""
        mSetearEditorExten(True)
    End Sub
    Private Sub mLimpiarFil()
        usrClieFil.Limpiar()
        cmbTarjetaFil.Limpiar()

        grdDato.CurrentPageIndex = 0

        mConsultar()
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        mEstablecerPerfil()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If txtFechaVenc.Fecha < Today Then
            Throw New AccesoBD.clsErrNeg("La Fecha de Vencimiento no puede ser menor a la corriente.")
        End If

        If chkVentaTelefono.Checked Then
            If txtDomicilio.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Domicilio Resumen del panel Detalle.")
            End If

            If txtCodigoSeg.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el C�digo de Seguridad.")
            End If
        End If
    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("tacl_id") = clsSQLServer.gFormatArg(Me.hdnId.Text, SqlDbType.Int)
            .Item("tacl_clie_id") = usrCliente.Valor
            '.Item("tacl_soci_id") = IIf(hdnSocio.Text = "", DBNull.Value, hdnSocio.Text)
            .Item("tacl_tarj_id") = Me.cmbTarjeta.Valor
            .Item("tacl_nume") = Me.txtNumero.Valor
            .Item("tacl_vcto_fecha") = Me.txtFechaVenc.Fecha
            .Item("tacl_vta_tele") = Me.chkVentaTelefono.Checked
            .Item("tacl_titu") = txtTitular.Valor
            .Item("tacl_banc_id") = IIf(chkVentaTelefono.Checked, Me.cmbBcoEmisor.Valor, DBNull.Value)
            .Item("tacl_domi") = IIf(chkVentaTelefono.Checked, Me.txtDomicilio.Valor, DBNull.Value)
            .Item("tacl_code_segu") = IIf(chkVentaTelefono.Checked, SRA_Neg.Encript.gEncriptar(txtCodigoSeg.Valor.ToString), "")
            .Item("tacl_audi_user") = Session("sUserId").ToString()

            ' Dario 2013-10-01 nuevos campos
            If (Me.txtCuit.Text = "") Then
                .Item("tacl_cuit") = DBNull.Value
            Else
                .Item("tacl_cuit") = clsSQLServer.gFormatArg(Me.txtCuit.Text.Replace("-", ""), SqlDbType.Int)
            End If

            If (Me.cmbDocuTipo.Valor <> 0) Then
                .Item("tacl_doti_id") = Me.cmbDocuTipo.Valor
            Else
                .Item("tacl_doti_id") = DBNull.Value
            End If

            If (Me.txtDocuNume.Text = "") Then
                .Item("tacl_docu_nume") = DBNull.Value
            Else
                .Item("tacl_docu_nume") = clsSQLServer.gFormatArg(Me.txtDocuNume.Text, SqlDbType.Int)
            End If

        End With
    End Sub
    Private Sub mAlta()
        Try
            mGuardarDatos()
            'el sp tarjetas_clientesABM_altas es el encargado de las altas de tarjetas_clientes
            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), "tarjetas_clientesABM", mdsDatos)
            lobjGenerico.Alta()

            mMostrarPanel(False)
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            lobjGenerico.Modi()

            mMostrarPanel(False)
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerico.Baja(hdnId.Text)

            mMostrarPanel(False)
            grdDato.CurrentPageIndex = 0
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    'Seccion de Extensiones
    Private Sub mActualizarExten()
        Try
            mGuardarDatosExten()

            mLimpiarExten()
            grdExten.DataSource = mdsDatos.Tables(mstrTablaExten)
            grdExten.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaExten()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaExten).Select("tacx_id=" & hdnExtenId.Text)(0)
            row.Delete()
            grdExten.DataSource = mdsDatos.Tables(mstrTablaExten)
            grdExten.DataBind()
            mLimpiarExten()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosExten()
        Dim ldrExten As DataRow

        If txtExtenNombre.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre y/o Apellido.")
        End If

        If hdnExtenId.Text = "" Then
            ldrExten = mdsDatos.Tables(mstrTablaExten).NewRow
            ldrExten.Item("tacx_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaExten), "tacx_id")
        Else
            ldrExten = mdsDatos.Tables(mstrTablaExten).Select("tacx_id=" & hdnExtenId.Text)(0)
        End If

        With ldrExten
            .Item("tacx_exte") = txtExtenNombre.Valor
        End With
        If (hdnExtenId.Text = "") Then
            mdsDatos.Tables(mstrTablaExten).Rows.Add(ldrExten)
        End If
    End Sub
#End Region

#Region "Opciones de controles"
    Private Sub mShowTabs(ByVal origen As Byte)
        Try
            panGeneral.Visible = (origen = 1)
            lnkGeneral.Font.Bold = (origen = 1)
            lnkVtaTelef.Font.Bold = (origen = 2)
            panVtaTelef.Visible = (origen = 2)
            lnkExtenciones.Font.Bold = (origen = 3)
            panExtensiones.Visible = (origen = 3)

            'chkVentaTelefono.Checked = hdnCheck.Checked
            'cmbBcoEmisor.Valor = hdnBancoEmisor.Text
            'txtDomicilio.Text = hdnDireccion.Text
            'txtCodigoSeg.Text = IIf(Len(hdnCodigo.Text) > 4, SRA_Neg.Encript.gDesencriptar(hdnCodigo.Text), hdnCodigo.Text)
            'hdnCodigo.Text = txtCodigoSeg.Text
            'txtSocio.Text = hdnSocio.Text
            'txtLegajo.Text = hdnLegajo.Text
            lblInstituto.Text = hdnInstituto.Text
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones de extensiones
    Private Sub btnAltaExten_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaExten.Click
        mActualizarExten()
    End Sub
    Private Sub btnBajaExten_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaExten.Click
        mBajaExten()
    End Sub
    Private Sub btnModiExten_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiExten.Click
        mActualizarExten()
    End Sub
    Private Sub btnLimpExten_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpExten.Click
        mLimpiarExten()
    End Sub

    Private Sub btnClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub

    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkVtaTelef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkVtaTelef.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkExtenciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExtenciones.Click
        mShowTabs(3)
    End Sub
#End Region

    ' Dario 2013-10-01 metodo que se ejecuta con el cambio de valor de combo de tarjeta
    Public Sub cmbTarjeta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTarjeta.SelectedIndexChanged
        ' obtengo el id de  la tarjeta
        Dim intIdTarjeta As Integer = Convert.ToInt32(Me.cmbTarjeta.Valor)
        ' llamo al la funcion que segun el id de la tarjeta aculta o muestra los controles de documento
        Me.MostrarOcultarPanelesDocumentos(intIdTarjeta)
    End Sub

    ' Dario 2013-10-01 metodo que oculta o muestra los controles de documentos segun el id de tarjeta
    Private Sub MostrarOcultarPanelesDocumentos(ByVal intIdTarjeta As Integer)
        ' Dario 2013-10-02 obtengo el valor de la tabla tarjetas
        ' que determina si habilita la carga de cbu
        Dim boolCargaCBU = False
        ' controlo que fue seleccionad una tarjeta, para realizar la consulta para
        ' ver si carga CBU o nro de cuenta
        If (intIdTarjeta > 0) Then
            boolCargaCBU = clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_consul @tarj_id=" & intIdTarjeta.ToString, "tarj_PermiteDebitoEnCuenta")
        End If

        'Dario 2013-10-01 para ocultar o mostrar los datos de documento
        If (boolCargaCBU) Then
            Me.trCUITCUIL.Style.Add("display", "inline")
            Me.trDocu.Style.Add("display", "inline")
            Me.lblCuenta.Text = "CBU:"
            Me.txtNumero.Enabled = True
            Me.txtNumero.MaxLength = 22
        ElseIf (intIdTarjeta = 0) Then
            Me.trCUITCUIL.Style.Add("display", "none")
            Me.trDocu.Style.Add("display", "none")
            Me.lblCuenta.Text = "N�mero:"
            Me.txtNumero.Text = ""
            Me.txtNumero.Enabled = False
            Me.txtNumero.MaxLength = 1
            Me.txtCuit.Text = ""
            Me.txtDocuNume.Text = ""
        Else
            Me.trCUITCUIL.Style.Add("display", "none")
            Me.trDocu.Style.Add("display", "none")
            Me.lblCuenta.Text = "N�mero:"
            Dim intLength = Me.txtNumero.Text.Length
            Me.txtNumero.Text = Mid(Me.txtNumero.Text, 1, IIf(intLength > 16, 16, intLength))
            Me.txtNumero.Enabled = True
            Me.txtNumero.MaxLength = 16
            Me.txtCuit.Text = ""
            Me.txtDocuNume.Text = ""
        End If
    End Sub
    ' Dario 2013-10-02 Funcion que recupera el CUIT / CUIl con y le pone los guiones
    Private Function FormatearCUIT(ByVal varCUITCUIL As String) As String
        Dim respuesta As String = ""

        If (varCUITCUIL.Length = 11) Then
            respuesta = Mid(varCUITCUIL, 1, 2) & "-" & Mid(varCUITCUIL, 3, 8) & "-" & Mid(varCUITCUIL, 11, 1)
        End If
        Return respuesta
    End Function

End Class

End Namespace
