<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.LeyendaComprobantes" CodeFile="LeyendaComprobantes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Leyendas de Comprobantes</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Leyendas de Comprobantes</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<tr>
										<td align="left" width="100%" colSpan="3"><asp:panel id="panFiltros" runat="server" width="100%" cssclass="titulo" BorderWidth="1px"
												BorderStyle="none">
												<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																			CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																			CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px" align="left">
															<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="17" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE class="FdoFld" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%" align="right" height="15">
																					<asp:Label id="lblActividadFil" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																				<TD>
																					<cc1:combobox class="combo" id="cmbActividadFil" runat="server" AceptaNull="False" NomOper="estados_cargar"
																						Width="200px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" height="15">
																					<asp:label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:label>&nbsp;</TD>
																				<TD>
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																					<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></td>
									</tr>
									<TR height="10">
										<TD></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
												AutoGenerateColumns="False">
												<FooterStyle CssClass="footer"></FooterStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="lefa_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="lefa_abre" HeaderText="Abreviatura">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="lefa_desc" HeaderText="Descripci&#243;n">
														<HeaderStyle Width="40%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_actividad" HeaderText="Descripci&#243;n">
														<HeaderStyle Width="35%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR height="10">
										<TD></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="middle">
											<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
															BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
															BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Leyenda"></CC1:BOTONIMAGEN></TD>
													<TD></TD>
													<TD align="center" width="50"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
															BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
															ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BOTONIMAGEN></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD></TD>
										<TD align="center" colSpan="2">
											<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
													Visible="False" Height="116px">
													<P align="right">
														<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
															border="0">
															<TR>
																<TD>
																	<P></P>
																</TD>
																<TD height="5">
																	<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
																<TD vAlign="top" align="right">&nbsp;
																	<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%" align="center" colSpan="3">
																	<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																		<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																			<TR>
																				<TD style="WIDTH: 99px; HEIGHT: 16px" align="right">
																					<asp:Label id="lblAbre" runat="server" cssclass="titulo" Width="62px">Abreviatura:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 16px" colSpan="2" height="16">
																					<CC1:TEXTBOXTAB id="txtAbre" runat="server" cssclass="cuadrotexto" Width="300px"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																					height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbActi" runat="server" IncludesUrl="../Includes/" ImagesUrl="../Images/"
																						AceptaNull="false" NomOper="actividades_cargar" Width="272px"></cc1:combobox>&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																					height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblFDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																					<asp:Label id="lblFHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																					height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 99px; HEIGHT: 16px" vAlign="top" align="right">
																					<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 16px" colSpan="2" height="16">
																					<P>
																						<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="textolibredeshab" AceptaNull="False" Width="100%"
																							Height="54px" TextMode="MultiLine" Obligatorio="True" EnterPorTab="False"></CC1:TEXTBOXTAB></P>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</asp:panel>
																	<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR height="30">
																<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
																	<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																	<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Baja"></asp:Button>&nbsp;&nbsp;
																	<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Modificar"></asp:Button>&nbsp;&nbsp;
																	<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Limpiar"></asp:Button></TD>
															</TR>
														</TABLE>
													</P>
												</asp:panel></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();

			function VigeChange()
			{
				ActivarFecha("txtInicFecha", document.all("cmbVige").value== "1");
				ActivarFecha("txtFinaFecha", document.all("cmbVige").value == "1");
				if(document.all("cmbVige").value!="1")
				{
					document.all("txtInicFecha").value="";
					document.all("txtFinaFecha").value="";
				}
			}
			
			function cmbConc_Change()
			{
				var lstrRet = LeerCamposXML("conceptos", document.all("cmbConc").value, "conc_form_id");
				document.all("btnPosicDet").disabled = !(lstrRet=="10"||lstrRet=="11"||lstrRet=="12"||lstrRet=="85"||lstrRet=="300"||lstrRet=="400"||lstrRet=="500"||lstrRet=="600"||lstrRet=="700"||lstrRet=="1000");
				/*
				if(lstrRet=="10")
					document.all("btnPosicDet").value = "Posicionamiento";
				if(lstrRet=="11"||lstrRet=="12")
					document.all("btnPosicDet").value = "Recorridos";
				*/
			}
		</SCRIPT>
		</TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
