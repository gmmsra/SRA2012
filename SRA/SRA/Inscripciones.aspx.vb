' Dario 2013-10-03 Por cambio en procampo
Imports System.Data.SqlClient


Namespace SRA


Partial Class Inscripciones
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton

   Protected WithEvents lblCompleto As System.Web.UI.WebControls.Label
   Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label
	Protected WithEvents lblGeneFact As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Inscripciones
   Private mstrInscripCMate As String = SRA_Neg.Constantes.gTab_InscripCMate
   Private mstrTarjetasClientes As String = SRA_Neg.Constantes.gTab_TarjetasClientes
   Private mstrTarjetasClientesExt As String = SRA_Neg.Constantes.gTab_TarjetasClientesExten

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mintInse As Int32

   Private Enum Columnas As Integer
      InscId = 1
   End Enum


#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

                If (Not Page.IsPostBack) Then
                    ' Dario 2013-10-03 se desabilita el control para que se habilite cuando el combo tiene algun valor
                    Me.txtNro.Enabled = False
                    ' oculto el panel de cuit/cuil nro de documento y tipo
                    Me.trDocu.Style.Add("display", "none")
                    ' seteo el maximo de caracteres permitidos para el nro de documento
                    Me.txtDocuNume.MaxLength = 9

                    mSetearEventos()
                    mEstablecerPerfil()
                    mCargarCombos()

                    mConsultar(grdDato)
                    clsWeb.gInicializarControles(Me, mstrConn)
                Else
                    mSetearMaterias()
                    mSetearDA()
                End If

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarMaterias(ByVal pintCiclo As Int32)
      clsWeb.gCargarRefeCmb(mstrConn, "ciclos_materias", cmbMate, "S", "@mate_curs_inte = 1 , @cicl_id =" & pintCiclo)
   End Sub

   Private Sub mCargarMateriasCarrera(ByVal pintCiclo As Int32, ByVal pintPerio As Int32)
      clsWeb.gCargarRefeCmb(mstrConn, "ciclos_materiasXperio", lstMate, "", "@cicl_id =" & pintCiclo & ", @cama_perio =" & pintPerio.ToString)
   End Sub

   Private Sub mCargarCiclos()
      clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCiclo, "S", "@cicl_inse_id =" & mintInse & ",@cicl_anio =" & IIf(txtAnio.Text = "", "0", txtAnio.Text))
   End Sub

   Private Sub mCargarCiclosFil()
      clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCicloFil, "S", "@cicl_inse_id =" & mintInse & ",@cicl_anio =" & IIf(txtAnioFil.Text = "", "0", txtAnioFil.Text))
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "pagos_plan", cmbPlanPago, "S", "@papl_inse_id=" + mintInse.ToString)
        clsWeb.gCargarComboBool(cmbDA, "")
            '  cmbDA.ValorBool = False
            clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "becas", cmbBeca, "N", "@beca_inse_id=" + mintInse.ToString)
        clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipo, "T")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
            'txtAnio.Attributes.Add("onchange", "txtAnio_Change(this,'" & mintInse & "');") Dario 2022-10-11 1236
            txtAnioFil.Attributes.Add("onchange", "txtAnioFil_Change(this,'" & mintInse & "');")

      Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
      lblInstituto.Text = Request.QueryString("fkvalor")
      usrAlum.FilInseId = mintInse
      usrAlumFil.FilInseId = mintInse


      Dim mobj As SRA_Neg.Facturacion
      mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
      mobj.CentroEmisorNro(mstrConn)
      hdnCtroEmis.Text = mobj.pCentroEmisorId

   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(grdDato, 1)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


#End Region

#Region "Seteo de Controles"
   Private Sub mLimpiarFiltros()

      hdnId.Text = ""
      txtAnioFil.Text = ""
      mCargarCiclosFil()
      usrAlumFil.Limpiar()
      mConsultarGrilla()
   End Sub
   Private Sub mHabilitaControles(ByVal pboolHabilita As Boolean)
      txtAnio.Enabled = pboolHabilita
      cmbCiclo.Enabled = pboolHabilita
      txtPerio.Enabled = pboolHabilita
      rbtnCarr.Enabled = pboolHabilita
      rbtnMate.Enabled = pboolHabilita
      lstMate.Enabled = pboolHabilita
      cmbMate.Enabled = pboolHabilita
      usrAlum.Activo = pboolHabilita
      chkGene.Enabled = (pboolHabilita And hdnId.Text = "")
      chkCtaCte.Enabled = (pboolHabilita And hdnId.Text = "")
      If Not chkGene.Enabled Then
         chkGene.Checked = False
      End If
      If Not chkCtaCte.Enabled Then
         chkCtaCte.Checked = False
      End If
   End Sub

   Private Sub mSetearEditor(ByVal pbooAlta As Boolean, ByVal pbooFacturada As Boolean)

      ' panCabecera.Enabled = Not pbooFacturada
      mHabilitaControles(Not pbooFacturada)
      btnBaja.Enabled = Not (pbooAlta) 'And Not pbooFacturada
      btnModi.Enabled = Not (pbooAlta) 'And Not pbooFacturada
      btnAlta.Enabled = pbooAlta
   End Sub

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

            Try
                Dim lbooUsada As Boolean
                mLimpiar()

                hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.InscId).Text)

                Dim ldsEsta As DataSet = mCrearDataSet(hdnId.Text)

                If ldsEsta.Tables(mstrTabla).Rows.Count > 0 Then
                    With ldsEsta.Tables(mstrTabla).Rows(0)
                        lblFechaInsc.Text = CDate(.Item("insc_insc_fecha")).ToString("dd/MM/yyyy")
                        txtAnio.Text = .Item("_anio")
                        mCargarCiclos()
                        cmbCiclo.Valor = .Item("insc_cicl_id")
                        txtPerio.Valor = .Item("insc_perio")
                        mCargarMaterias(cmbCiclo.Valor)
                        mCargarMateriasCarrera(cmbCiclo.Valor, txtPerio.Valor)

                        rbtnCarr.Checked = .Item("insc_carr")
                        rbtnMate.Checked = Not .Item("insc_carr")

                        If rbtnMate.Checked Then
                            If ldsEsta.Tables(mstrInscripCMate).Rows.Count > 0 Then
                                cmbMate.Valor = ldsEsta.Tables(mstrInscripCMate).Rows(0).Item("incm_cima_id")
                            End If
                        Else
                            mEditarDatosInscripCMate(ldsEsta)
                        End If
                        mSetearMaterias()

                        mEditarDatosTarjetasClientes(ldsEsta)
                        mSetearDA()

                        usrAlum.Valor = .Item("insc_alum_id")
                        cmbPlanPago.Valor = .Item("insc_papl_id")
                        usrFactClie.Valor = .Item("insc_fact_clie_id")
                        cmbBeca.Valor = IIf(.Item("insc_beca_id").ToString.Length > 0, .Item("insc_beca_id"), String.Empty)

                        If Not .IsNull("insc_baja_fecha") Then
                            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("insc_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                        Else
                            lblBaja.Text = ""
                        End If
                    End With

                    mSetearEditor(False, mInscripcionUsada)
                    mMostrarPanel(True)
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
   Private Function mInscripcionUsada()
      Dim lbooUsada As Boolean = False
      If hdnId.Text <> "" Then
         With clsSQLServer.gObtenerEstruc(mstrConn, "inscripcionesX", hdnId.Text).Tables(0).Rows(0)
            lbooUsada = Not .IsNull("_comp_id") Or Not .IsNull("_comi_id")
         End With
      End If
      Return lbooUsada
   End Function


   Private Sub mEditarDatosInscripCMate(ByVal pdsDatos As DataSet)
      With pdsDatos.Tables(mstrInscripCMate)
         For Each lItem As ListItem In lstMate.Items
            lItem.Selected = .Select("incm_cima_id=" & lItem.Value.ToString).GetUpperBound(0) > -1
         Next
      End With
   End Sub

   Private Sub mEditarDatosTarjetasClientes(ByVal pdsDatos As DataSet)
      With pdsDatos.Tables(mstrTarjetasClientes).Select("")
         If .GetUpperBound(0) = -1 Then
                    ' cmbDA.ValorBool = False
         Else
            cmbDA.ValorBool = True
            With .GetValue(0)
                    cmbTarj.Valor = .Item("tacl_tarj_id")
                    txtNro.Valor = .Item("tacl_nume")
                        txtVctoFecha.Fecha = IIf(.Item("tacl_vcto_fecha").ToString.Length > 0, .Item("tacl_vcto_fecha"), String.Empty)

                    ' Dario 2013-10-03 nuevos campos
                    If Not .IsNull("tacl_cuit") Then
                        Me.txtCuit.Text = Me.FormatearCUIT(.Item("tacl_cuit"))
                    Else
                        Me.txtCuit.Text = ""
                    End If

                    If Not .IsNull("tacl_doti_id") Then
                        Me.cmbDocuTipo.Valor = .Item("tacl_doti_id")
                    Else
                        Me.cmbDocuTipo.Valor = "0"
                    End If

                    If Not .IsNull("tacl_docu_nume") Then
                        Me.txtDocuNume.Valor = .Item("tacl_docu_nume")
                    Else
                        Me.txtDocuNume.Text = ""
                    End If
                    ' Dario 2013-10-03 llamo a la funcion que determina si hay que mostrar o ocultar 
                    ' los controles de documento
                    Me.MostrarOcultarPanelesDocumentos(.Item("tacl_tarj_id"))
                End With
         End If
      End With
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        txtAnio.Text = ""
        txtPerio.Text = ""
        lblBaja.Text = ""
            'mCargarCiclos() Dario 2022-09-29 1105
            cmbMate.Limpiar()
            mCargarMaterias(-1)
        mCargarMateriasCarrera(-1, -1)
        rbtnMate.Checked = False
        rbtnCarr.Checked = True
        chkGene.Checked = True
        chkCtaCte.Checked = False

        mSetearMaterias()

            cmbDA.ValorBool = False
            mSetearDA()
        cmbBeca.Limpiar()
        cmbPlanPago.Limpiar()
        usrAlum.Limpiar()
        usrFactClie.Limpiar()
        lblTitu.Text = ""
        ' Dario 2013-10-03 
        txtCuit.Text = ""
        cmbDocuTipo.Limpiar()
        txtDocuNume.Text = ""
        ' obtengo el id de  la tarjeta
        Dim intIdTarjeta As Integer = Convert.ToInt32(Me.cmbTarj.Valor)
        ' llamo al la funcion que segun el id de la tarjeta aculta o muestra los controles de documento
        Me.MostrarOcultarPanelesDocumentos(intIdTarjeta)
        mSetearEditor(True, False)
    End Sub

    Private Sub mCerrar()
        mConsultarGrilla()
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        grdDato.Visible = Not panDato.Visible
        btnAgre.Visible = Not (panDato.Visible)
        panFiltro.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
            grdDato.DataBind()
        End If

        panDato.Visible = pbooVisi

        panCabecera.Visible = True
    End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrInscId As String
         Dim ldsDatos As DataSet = mGuardarDatos(True)
         Dim lobjInscripciones As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lstrInscId = lobjInscripciones.Alta()

         If chkGene.Checked Then
            hdnCompId.Text = clsSQLServer.gObtenerEstruc(mstrConn, "inscripcionesX", lstrInscId).Tables(0).Rows(0).Item("_comp_id").ToString
            hdnCompNume.Text = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantesX", hdnCompId.Text).Tables(0).Rows(0).Item("numero").ToString
         Else
            mConsultar(grdDato)
            mMostrarPanel(False)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos(False)
         Dim lobjInscripciones As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjInscripciones.Modi()

         mConsultar(grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjIncripciones As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjIncripciones.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0


         mConsultar(grdDato)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos(ByVal pboolAlta As Boolean, ByVal pdsDatos As DataSet)
      Dim ldateFecha As Date
      Dim ldateFechaInsc As Date

      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      With pdsDatos.Tables(0).Rows(0)
         ldateFecha = mObtenerFechas(cmbCiclo.Valor, "Final_Curso")

         If pboolAlta Then
            ldateFechaInsc = Date.Now
         Else
            ldateFechaInsc = .Item("insc_insc_fecha")
         End If
      End With

      If ldateFechaInsc > ldateFecha Then
         Throw New AccesoBD.clsErrNeg("La fecha de inscripci�n no puede ser mayor a la fecha de finalizaci�n del curso (" + ldateFecha + ").")
      End If

      If rbtnMate.Checked And cmbMate.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar una Materia.")
      End If
   End Sub

   Private Function mGuardarDatos(ByVal pboolAlta As Boolean) As DataSet
      Dim ldsEsta As DataSet = mCrearDataSet(hdnId.Text)

      mValidarDatos(pboolAlta, ldsEsta)

      With ldsEsta.Tables(0).Rows(0)
         .Item("insc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("insc_cicl_id") = cmbCiclo.Valor
         .Item("insc_alum_id") = usrAlum.Valor
         .Item("insc_papl_id") = cmbPlanPago.Valor
         .Item("insc_fact_clie_id") = usrFactClie.Valor
         .Item("insc_baja_fecha") = DBNull.Value
         .Item("insc_carr") = rbtnCarr.Checked
         .Item("insc_peti_id") = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Ciclos, .Item("insc_cicl_id").ToString).Tables(0).Rows(0).Item("_carr_peti_id")
         .Item("insc_perio") = txtPerio.Valor
                .Item("insc_beca_id") = IIf(cmbBeca.Valor.ToString.Length > 0, cmbBeca.Valor, DBNull.Value)

      End With

      mGuardarDatosInscripCMate(ldsEsta)
      mGuardarDatosTarjetasClientes(ldsEsta)

        'SAP 20-07-2007
        If chkGene.Checked And pboolAlta Then
            mAgregarTablaProceso(ldsEsta)
        End If

        Return ldsEsta
   End Function

   Private Sub mGuardarDatosInscripCMate(ByVal ldsEsta As DataSet)
      Dim lDrInsc As DataRow = ldsEsta.Tables(mstrTabla).Rows(0)
      Dim ldrIncm As DataRow

      With ldsEsta.Tables(mstrInscripCMate)
         If lDrInsc.Item("insc_carr") Then   'si cursa una carrera
            For Each lItem As ListItem In lstMate.Items
               If lItem.Selected And .Select("incm_cima_id=" & lItem.Value.ToString).GetUpperBound(0) = -1 Then
                  'si est� seleccionado y no existe en la tabla, lo agrega
                  ldrIncm = .NewRow
                  With ldrIncm
                     .Item("incm_id") = clsSQLServer.gObtenerId(.Table, "incm_id")
                     .Item("incm_cima_id") = lItem.Value
                     .Item("incm_insc_id") = lDrInsc.Item("insc_id")
                  End With
                  .Rows.Add(ldrIncm)
               End If
            Next

            For Each lDr As DataRow In .Select("")
               For Each lItem As ListItem In lstMate.Items
                  If lDr.Item("incm_cima_id") = lItem.Value And Not litem.Selected Then
                     'si not est� seleccionado y existe en la tabla, lo borra
                     lDr.Delete()
                     Exit For
                  End If
               Next
            Next

         Else  'si cursa una materia individual
            If .Select.GetUpperBound(0) = -1 Then
               ldrIncm = .NewRow 'si no existe ning�n registro lo crea y lo agrega

            Else  'si ya existen registros, usa el primero y borra el resto
               With .Select("")
                  ldrIncm = .GetValue(0)

                  For i As Integer = 1 To .GetUpperBound(0)
                     DirectCast(.GetValue(i), DataRow).Delete()
                  Next
               End With
            End If

            With ldrIncm
               If .IsNull("incm_id") Then
                  .Item("incm_id") = clsSQLServer.gObtenerId(.Table, "incm_id")
                  .Table.Rows.Add(ldrIncm)
               End If
                        .Item("incm_cima_id") = IIf(cmbMate.Valor.Length > 0, cmbMate.Valor, DBNull.Value)
               .Item("incm_insc_id") = lDrInsc.Item("insc_id")
            End With
         End If
      End With
   End Sub

   Private Sub mGuardarDatosTarjetasClientes(ByVal ldsEsta As DataSet)
      Dim ldrTacl As DataRow

            With ldsEsta.Tables(mstrTarjetasClientes)

                If cmbDA.ValorBool Then
                    If .Select("").GetUpperBound(0) = -1 Then
                        'si est� seleccionado y no existe en la tabla, lo agrega
                        ldrTacl = .NewRow
                        With ldrTacl
                            .Item("tacl_id") = clsSQLServer.gObtenerId(.Table, "tacl_id")
                        End With
                        .Rows.Add(ldrTacl)
                    Else
                        ldrTacl = .Select("").GetValue(0)
                    End If

                    With ldrTacl
                        .Item("tacl_clie_id") = usrFactClie.Valor
                        .Item("tacl_tarj_id") = cmbTarj.Valor
                        .Item("tacl_nume") = txtNro.Valor
                        .Item("tacl_vcto_fecha") = IIf(txtVctoFecha.Fecha.ToString.Length > 0, txtVctoFecha.Fecha, DBNull.Value)
                        .Item("tacl_insc_id") = ldsEsta.Tables(mstrTabla).Rows(0).Item("insc_id")

                        ' Dario 2013-10-02 nuevos campos
                        If (Me.txtCuit.Text = "") Then
                            .Item("tacl_cuit") = DBNull.Value
                        Else
                            .Item("tacl_cuit") = clsSQLServer.gFormatArg(Me.txtCuit.Text.Replace("-", ""), SqlDbType.Int)
                        End If

                        If (Me.cmbDocuTipo.Valor <> 0) Then
                            .Item("tacl_doti_id") = Me.cmbDocuTipo.Valor
                        Else
                            .Item("tacl_doti_id") = DBNull.Value
                        End If

                        If (Me.txtDocuNume.Text = "") Then
                            .Item("tacl_docu_nume") = DBNull.Value
                        Else
                            .Item("tacl_docu_nume") = clsSQLServer.gFormatArg(Me.txtDocuNume.Text, SqlDbType.Int)
                        End If
                    End With

                Else
                    With .Select("")
                        If .GetUpperBound(0) > -1 Then
                            DirectCast(.GetValue(0), DataRow).Delete()
                        End If
                    End With
                End If
            End With
   End Sub

   Private Function mCrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      With ldsDatos
         .Tables(0).TableName = mstrTabla
         .Tables(1).TableName = mstrInscripCMate
         .Tables(2).TableName = mstrTarjetasClientes
         .Tables(3).TableName = mstrTarjetasClientesExt

         If .Tables(mstrTabla).Rows.Count = 0 Then
            .Tables(mstrTabla).Rows.Add(.Tables(mstrTabla).NewRow)
         End If
      End With

      Return ldsDatos
   End Function

   Private Sub mAgregarTablaProceso(ByVal ldsEsta As DataSet)
      Dim lDr As DataRow
      Dim lintActiId As Integer

      lintActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", mintInse).Tables(0).Rows(0).Item("inse_acti_id")

      ldsEsta.Tables.Add(mstrTabla & "_factura")

      With ldsEsta.Tables(ldsEsta.Tables.Count - 1)
         .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_insc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_cemi_nume", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_emct_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_host", System.Type.GetType("System.String"))
			.Columns.Add("proc_vcto_fecha", System.Type.GetType("System.DateTime"))
			.Columns.Add("cpti_id", System.Type.GetType("System.Int32"))
         lDr = .NewRow
         .Rows.Add(lDr)
      End With

      lDr.Item("proc_id") = -1
      lDr.Item("proc_insc_id") = ldsEsta.Tables(mstrTabla).Rows(0).Item("insc_id")

      Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
      oFact.CentroEmisorNro(mstrConn)
      lDr.Item("proc_cemi_nume") = oFact.pCentroEmisorNro
      lDr.Item("proc_emct_id") = oFact.pCentroEmisorId
      lDr.Item("proc_host") = oFact.pHost
		lDr.Item("proc_vcto_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, lintActiId, Today)

		If chkCtaCte.Checked Then
			lDr.Item("cpti_id") = 2
		Else
			lDr.Item("cpti_id") = 1
		End If

		oFact.Liberar(Me)
   End Sub

   Public Function mObtenerFechas(ByVal pstrCipeId As String, ByVal pstrCampo As String) As Date
      Dim lstrCmd As New StringBuilder
      Dim ds As DataSet

      lstrCmd.Append("exec ciclo_fecha_inscripcion_consul ")
      lstrCmd.Append("@cicl_id=")
      lstrCmd.Append(pstrCipeId)

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      Select Case pstrCampo
         Case "Inicio_Curso"
            Return (ds.Tables(0).Rows(0).Item("curs_inic"))
         Case "Final_Curso"
            Return (ds.Tables(0).Rows(0).Item("curs_fina"))
      End Select
   End Function

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
#End Region

#Region "Detalle"


   Private Sub mConsultarGrilla()
      Try
                mConsultar(grdDato, 1)
                mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Public Sub mConsultar(ByVal grdDato As DataGrid, Optional ByVal pBuscFiltro As Byte = 0)
      Try
         mstrCmd = "exec " + mstrTabla + "_consul" + " @alum_inse_id = " + mintInse.ToString()
         If pBuscFiltro <> 0 Then
            mstrCmd = mstrCmd + ", @insc_alum_id  = " + usrAlumFil.Valor.ToString
            mstrCmd = mstrCmd + ", @cicl_anio  = " + txtAnioFil.Valor.ToString
            mstrCmd = mstrCmd + ", @insc_cicl_id = " + cmbCicloFil.Valor.ToString

         End If
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar(grdDato, 1)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      lblFechaInsc.Text = CDate(Date.Now).ToString("dd/MM/yyyy")
      mAgregar()
   End Sub

   Private Sub txtPerio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPerio.TextChanged
            'If cmbCiclo.Valor Is DBNull.Value Or txtPerio.Valor Is DBNull.Value Then
            If cmbCiclo.Text.Trim().Length = 0 Or txtPerio.Text.Trim().Length = 0 Then
                mCargarMaterias(-1)
                mCargarMateriasCarrera(-1, -1)
            Else
                mCargarMaterias(cmbCiclo.Valor)
                mCargarMateriasCarrera(cmbCiclo.Valor, txtPerio.Valor)
            End If

      mSetearMaterias()

      With DirectCast(lstMate.DataSource, DataSet).Tables(0)
         For Each lItem As ListItem In lstMate.Items
            lItem.Selected = True '.Select("id=" & lItem.Value.ToString)(0).Item("orden") = 0
         Next
      End With
   End Sub


        Private Sub mSetearMaterias()
            If Not mInscripcionUsada() Then
                lstMate.Enabled = rbtnCarr.Checked
                cmbMate.Enabled = rbtnMate.Checked

                If Not lstMate.Enabled Then
                    lstMate.Limpiar()
                Else
                    ' GM 23/04/2021
                    If cmbMate.Items.Count > 0 Then
                        cmbMate.SelectedIndex = 0
                    End If

                End If
            End If
        End Sub

   Private Sub mSetearDA()
      cmbTarj.Enabled = cmbDA.ValorBool
      txtNro.Enabled = cmbDA.ValorBool
      txtVctoFecha.Enabled = cmbDA.ValorBool

      If Not cmbTarj.Enabled Then
         cmbTarj.Limpiar()
         txtNro.Text = ""
         txtVctoFecha.Text = ""
      Else
         If Not Request.Form(cmbTarj.UniqueID) Is Nothing Then
            cmbTarj.Valor = Request.Form(cmbTarj.UniqueID)
         End If

         If Not Request.Form(txtNro.UniqueID) Is Nothing Then
            txtNro.Valor = Request.Form(txtNro.UniqueID)
         End If

         If Not Request.Form(txtVctoFecha.UniqueID) Is Nothing Then
            txtVctoFecha.Text = Request.Form(txtVctoFecha.UniqueID)
         End If
      End If
   End Sub

   Private Sub hdnCompId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnCompId.TextChanged
      Try
         If hdnCompId.Text <> "" Then
            Dim lobj As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), SRA_Neg.Constantes.gTab_Comprobantes)
            lobj.ModiImpreso(Replace(hdnCompId.Text, "-", ""), True)
         End If
         mLimpiar()
         mConsultar(grdDato)
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      Finally
         hdnCompId.Text = ""
         hdnCompNume.Text = ""
      End Try
    End Sub

    ' Dario 2013-10-03 metodo que se ejecuta con el cambio de valor de combo de tarjeta
    Public Sub cmbTarj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTarj.SelectedIndexChanged
        ' obtengo el id de  la tarjeta
        Dim intIdTarjeta As Integer = Convert.ToInt32(Me.cmbTarj.Valor)
        ' llamo al la funcion que segun el id de la tarjeta aculta o muestra los controles de documento
        Me.MostrarOcultarPanelesDocumentos(intIdTarjeta)
    End Sub

    ' Dario 2013-10-03 metodo que oculta o muestra los controles de documentos segun el id de tarjeta
    Private Sub MostrarOcultarPanelesDocumentos(ByVal intIdTarjeta As Integer)
        ' Dario 2013-10-03 obtengo el valor de la tabla tarjetas
        ' que determina si habilita la carga de cbu
        Dim boolCargaCBU = False
        ' controlo que fue seleccionad una tarjeta, para realizar la consulta para
        ' ver si carga CBU o nro de cuenta
        If (intIdTarjeta > 0) Then
            boolCargaCBU = clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_consul @tarj_id=" & intIdTarjeta.ToString, "tarj_PermiteDebitoEnCuenta")
        End If

        'Dario 2013-10-01 para ocultar o mostrar los datos de documento
        If (boolCargaCBU) Then
            Me.trDocu.Style.Add("display", "inline")
            Me.lblNrorunat.Text = "CBU:"
            Me.txtNro.Enabled = True
            Me.txtVctoFecha.Enabled = True
            Me.txtNro.MaxLength = 22
            Me.txtNro.Style.Add("focus", "focus")
        ElseIf (intIdTarjeta = 0) Then
            Me.trDocu.Style.Add("display", "none")
            Me.lblNrorunat.Text = "N�mero:"
            Me.txtNro.Enabled = False
            Me.txtVctoFecha.Enabled = False
            Me.txtNro.MaxLength = 1
            Me.txtCuit.Text = ""
            Me.txtDocuNume.Text = ""
            Me.cmbDocuTipo.Limpiar()
        Else
            Me.trDocu.Style.Add("display", "none")
            Me.lblNrorunat.Text = "N�mero"
            Dim intLength = Me.txtNro.Text.Length
            Me.txtNro.Text = Mid(Me.txtNro.Text, 1, IIf(intLength > 16, 16, intLength))
            Me.txtNro.Enabled = True
            Me.txtVctoFecha.Enabled = True
            Me.txtNro.MaxLength = 16
            Me.txtCuit.Text = ""
            Me.txtDocuNume.Text = ""
            Me.cmbDocuTipo.Limpiar()
        End If
        ' Dario 2013-10-03 pongo el foto el un control de los ultimos para poder quedarme en la zona
        ' en la que cambie el combo tarjeta
        Page.RegisterStartupScript("ClientScript", "<script language='JavaScript'>document.all('btnLimp').focus();</script>")

    End Sub

    ' Dario 2013-10-03 Funcion que recupera el CUIT / CUIl con y le pone los guiones
    Private Function FormatearCUIT(ByVal varCUITCUIL As String) As String
        Dim respuesta As String = ""

        If (varCUITCUIL.Length = 11) Then
            respuesta = Mid(varCUITCUIL, 1, 2) & "-" & Mid(varCUITCUIL, 3, 8) & "-" & Mid(varCUITCUIL, 11, 1)
        End If
        Return respuesta
    End Function


        Protected Sub txtAnio_TextChanged(sender As Object, e As EventArgs) Handles txtAnio.TextChanged
            Me.mCargarCiclos()
        End Sub
    End Class
End Namespace
