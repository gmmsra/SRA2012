<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CRIA" Src="controles/usrCriadero.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Migrated_Criaderos" CodeFile="Criaderos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Criaderos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
		
		function mSelecCP()
		  {
		    document.all["txtDireCP"].value = '';
			document.all["cmbLocaAux"].value = document.all["cmbDireLoca"].value;
			document.all["txtDireCP"].value = document.all["cmbLocaAux"].item(document.all["cmbLocaAux"].selectedIndex).text;
		  }
		   	
		function btnSelecDire_click()
	   	{
		 gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Direciones de los integrantes&tabla=dire_clientesX&filtros='"+ document.all("hdnClieIds").value +"'", 7, "700","300","100");
		}
    
	    function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
			
		
		function mCargarProvincias(pPais,pcmbPcia,pcmbLoca)
		{
		    var sFiltro = pPais.value;
   		    if (sFiltro != '')
			   {
			    LoadComboXML("provincias_cargar", sFiltro, pcmbPcia, "S");
			    document.all(pcmbLoca).innerText='';
				document.all("cmbLocaAux").innerText='';
			   }
			else
			{
				document.all(pcmbPcia).innerText='';
				document.all(pcmbLoca).innerText='';
				document.all("cmbLocaAux").innerText='';
			}
			if (pPais.value == '<%=Session("sPaisDefaId")%>')
				document.all('hdnValCodPostalPais').value = 'S';
			else
				document.all('hdnValCodPostalPais').value = 'N';			
		}
		
		function mCargarLocalidades()
		{
		   
		    var sFiltro =  document.all["cmbDirePcia"].value;
		    if (sFiltro != '')
		    {
		       document.all["txtDireCP"].value = '';
		       LoadComboXML("localidades_cargar", sFiltro, "cmbDireLoca", "S");
		       LoadComboXML("localidades_aux_cargar", sFiltro, "cmbLocaAux", "S");
		    }
		    else
		    {
		    document.all("cmbDireLoca").innerText='';
		    document.all("cmbLocaAux").innerText='';
		    }
		}
		
		function mCargarLocalidadesFiltro()
		{
		    var sFiltro = document.all["cmbProvFil"].value;
		    if (sFiltro != '')
		    {
		       LoadComboXML("localidades_cargar", sFiltro, "cmbLocaFil", "S");
		       LoadComboXML("localidades_aux_cargar", sFiltro, "cmbLocaAux", "S");
		    }
		    else
		    {
		    document.all("cmbLocaFil").innerText='';
		    document.all("cmbLocaAux").innerText='';
		    }
		}
		
			

		function mOtrosDatosAbrir()
	   	{
          gAbrirVentanas("Alertas_pop.aspx?titulo=Otros Datos&amp;origen=O&amp;clieId=" + document.all("hdnId").value+"&amp;"+"FechaValor=&amp;sociId=", 7, "450","300","100");
	 	}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Criaderos</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="100%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="2" background="imagenes/formiz.jpg" colSpan="3"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo">Nro. Criadero:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtNumeFil" runat="server" cssclass="cuadrotexto" esdecimal="False" MaxValor="9999999999999"
																					Width="143px" Obligatorio="True"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblNombFil" runat="server" cssclass="titulo">Descripci�n:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<asp:checkbox id="chkBusc" CssClass="titulo" Runat="server" Text="Buscar en..."></asp:checkbox>&nbsp;&nbsp;&nbsp;
																				<asp:checkbox id="chkBaja" CssClass="titulo" Runat="server" Text="Incluir Dados de Baja"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClieFil" runat="server" Tabla="Clientes" Saltos="1,2" FilSociNume="True"
																					FilTipo="S" MuestraDesc="false" FilDocuNume="True" Ancho="800" AceptaNull="false"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblRazaFil" runat="server" cssclass="titulo">Raza:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="300px"
																					filtra="true" NomOper="razas_cargar" MostrarBotones="False" onchange="mCargarProvincias(this,'cmbProvFil','cmbLocaFil')"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblPaisFil" runat="server" cssclass="titulo">Pa�s:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbPaisFil" runat="server" Width="260px" onchange="mCargarProvincias(this,'cmbProvFil','cmbLocaFil')"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblProvFil" runat="server" cssclass="titulo">Provincia:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbProvFil" runat="server" Width="260px" onchange="mCargarLocalidadesFiltro()"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblLocaFil" runat="server" cssclass="titulo">Localidad:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbLocaFil" runat="server" Width="260px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD height="10"></TD>
									<TD vAlign="top" align="right" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="right" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True" OnEditCommand="mSeleccionarCriadero">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton3" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
															<img src='images/sele.gif' border="0" alt="Seleccionar Criadero" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="crdr_id" ReadOnly="True" HeaderText="Id.Criadero"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="crdr_nume" HeaderText="Nro.Criadero"></asp:BoundColumn>
												<asp:BoundColumn DataField="crdr_desc" HeaderText="Descripci�n"></asp:BoundColumn>
												<asp:BoundColumn DataField="crdr_clie_id" HeaderText="Nro.Cliente"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cliente" ReadOnly="True" HeaderText="Apellido/Raz&#243;n Social"></asp:BoundColumn>
												<asp:BoundColumn DataField="crdr_baja_fecha" ReadOnly="True" HeaderText="Fecha Baja" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
											BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
											ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ToolTip="Agregar un Nuevo Criadero" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Criadero</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkTele" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" CausesValidation="False" Height="21px"> Tel�fonos</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDire" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" CausesValidation="False" Height="21px"> Direcciones</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkMail" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" CausesValidation="False" Height="21px"> Mails</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkRaza" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" CausesValidation="False" Height="21px"> Razas</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												width="99%" Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 121px" vAlign="top" align="right">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label></TD>
																		<TD colSpan="2">
																			<UC1:CLIE id="usrClie" runat="server" Tabla="Clientes" Saltos="1,2" FilSociNume="True" FilTipo="S"
																				MuestraDesc="True" FilDocuNume="True" Ancho="800" AceptaNull="false"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 121px" noWrap align="right">
																			<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro.Criadero:</asp:Label></TD>
																		<TD colSpan="2">
																			<cc1:numberbox id="txtNume" runat="server" cssclass="cuadrotexto" esdecimal="False" MaxValor="9999999999999"
																				Width="143px" Obligatorio="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 121px" align="right">
																			<asp:Label id="lblNomb" runat="server" cssclass="titulo">Descripci�n:</asp:Label></TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="450px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 121px" vAlign="top" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label></TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="450px" Obligatorio="True"
																				Height="50px" MaxLength="20" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="center" colSpan="3"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDire" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table4" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdDire" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosDire"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="grdDire_PageChanged" AutoGenerateColumns="False" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="dicl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_dire_desc" HeaderText="Direcci&#243;n">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="dicl_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="35%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_prop" HeaderText="Prop.">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_defa" HeaderText="Defa."></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotexto" Width="440px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDirePais" runat="server" cssclass="titulo">Pais:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:combobox class="combo" id="cmbDirePais" runat="server" Width="260px" onchange="mCargarProvincias(this,'cmbDirePcia','cmbDireLoca')"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 11px" align="right">
																			<asp:Label id="llbDirePcia" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 11px">
																			<cc1:combobox class="combo" id="cmbDirePcia" runat="server" Width="260px" NomOper="provincias_cargar"
																				onchange="mCargarLocalidades()"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 19px" align="right">
																			<asp:Label id="lblDireLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 19px">
																			<cc1:combobox class="combo" id="cmbDireLoca" runat="server" Width="260px" onchange="mSelecCP();"></cc1:combobox>&nbsp;
																			<asp:Button id="btnLoca" runat="server" cssclass="boton" Width="87px" Text="Localidades" Visible="True"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDireCP" runat="server" cssclass="titulo">C�digo Postal:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:CODPOSTALBOX id="txtDireCP" runat="server" cssclass="cuadrotexto" Width="77px" Obligatorio="True"></CC1:CODPOSTALBOX></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDireRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDireRefe" runat="server" cssclass="cuadrotexto" Width="440px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right"></TD>
																		<TD>
																			<asp:checkbox id="chkDireDefa" CssClass="titulo" Runat="server" Text="Default"></asp:checkbox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<% if mbooActi then %>
																	<TR>
																		<TD align="right"></TD>
																		<TD>
																			<asp:Label id="lblTituActiDire" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:datagrid id="grdActiDire" runat="server" width="70%" BorderStyle="None" BorderWidth="1px"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
																				Visible="False" ShowFooter="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderText="Asociar">
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:checkbox id="chkActi" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Prop.">
																						<HeaderStyle Width="70px"></HeaderStyle>
																						<ItemTemplate>
																							<cc1:combobox class="combo" enabled="false" id="cmbActiProp" runat="server" Width="50px" Visible="True">
																								<asp:ListItem Value="N">No</asp:ListItem>
																								<asp:ListItem Value="S">Si</asp:ListItem>
																							</cc1:combobox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<% end if %>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaDire" runat="server" cssclass="boton" Width="100px" Text="Agregar Direc."></asp:Button>&nbsp;
																			<asp:Button id="btnBajaDire" runat="server" cssclass="boton" Width="100px" Text="Eliminar Direc."></asp:Button>&nbsp;
																			<asp:Button id="btnModiDire" runat="server" cssclass="boton" Width="100px" Text="Modificar Direc."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDire" runat="server" cssclass="boton" Width="100px" Text="Limpiar Direc."
																				Visible="True"></asp:Button>&nbsp;&nbsp;<BUTTON class="boton" id="btnSelecDire" style="WIDTH: 100px" onclick="btnSelecDire_click();"
																				type="button" runat="server" value="Detalles">Selec. Direc.</BUTTON></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panMail" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdMail" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosMail"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="grdMail_PageChanged" AutoGenerateColumns="False" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="macl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="macl_mail" HeaderText="Mail">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="macl_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="35%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" HeaderImageUrl="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_prop" HeaderText="Prop.">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblMail" runat="server" cssclass="titulo">Mail:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtMail" runat="server" cssclass="cuadrotexto" Width="292px" Obligatorio="True"
																				AceptaNull="False" Panel="Mail" EsMail="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblMailRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtMailRefe" runat="server" cssclass="cuadrotexto" Width="440px" Obligatorio="True"
																				AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<% if mbooActi then %>
																	<TR>
																		<TD align="right"></TD>
																		<TD>
																			<asp:Label id="lblTituActiMail" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:datagrid id="grdActiMail" runat="server" width="70%" BorderStyle="None" BorderWidth="1px"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
																				Visible="False" ShowFooter="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderText="Asociar">
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:checkbox id="Checkbox1" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Prop.">
																						<HeaderStyle Width="70px"></HeaderStyle>
																						<ItemTemplate>
																							<cc1:combobox class="combo" enabled="false" id="Combobox1" runat="server" Width="50px" Visible="True">
																								<asp:ListItem Value="N">No</asp:ListItem>
																								<asp:ListItem Value="S">Si</asp:ListItem>
																							</cc1:combobox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<% end if %>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaMail" runat="server" cssclass="boton" Width="100px" Text="Agregar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaMail" runat="server" cssclass="boton" Width="100px" Text="Eliminar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnModiMail" runat="server" cssclass="boton" Width="100px" Text="Modificar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpMail" runat="server" cssclass="boton" Width="100px" Text="Limpiar Mail"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panTele" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdTele" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosTele"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="grdTele_PageChanged" AutoGenerateColumns="False" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="tecl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_tele_desc" ReadOnly="True" HeaderText="Tel&#233;fono">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="tecl_refe" HeaderText="Referencia">
																						<HeaderStyle Width="35%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_prop" HeaderText="Prop.">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTeleNume" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtTeleArea" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="True"></CC1:TEXTBOXTAB>&nbsp;
																			<CC1:TEXTBOXTAB id="txtTeleNume" runat="server" cssclass="cuadrotexto" Width="332px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTeleRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtTeleRefe" runat="server" cssclass="cuadrotexto" Width="450px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTeleTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:combobox class="combo" id="cmbTeleTipo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<% if mbooActi then %>
																	<TR>
																		<TD align="right"></TD>
																		<TD style="HEIGHT: 10px">
																			<asp:Label id="lblTituActiTele" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:datagrid id="grdActiTele" runat="server" width="70%" BorderStyle="None" BorderWidth="1px"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
																				Visible="False" ShowFooter="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderText="Asociar">
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:checkbox id="Checkbox2" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Prop.">
																						<HeaderStyle Width="70px"></HeaderStyle>
																						<ItemTemplate>
																							<cc1:combobox class="combo" enabled="false" id="Combobox2" runat="server" Width="50px" Visible="True">
																								<asp:ListItem Value="N">No</asp:ListItem>
																								<asp:ListItem Value="S">Si</asp:ListItem>
																							</cc1:combobox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<% end if %>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaTele" runat="server" cssclass="boton" Width="100px" Text="Agregar Tel."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaTele" runat="server" cssclass="boton" Width="100px" Text="Eliminar Tel."
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiTele" runat="server" cssclass="boton" Width="100px" Text="Modificar Tel."
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpTele" runat="server" cssclass="boton" Width="100px" Text="Limpiar Tel."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panRaza" runat="server" cssclass="titulo" Width="100%" Visible="false">
																<TABLE id="Table6" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdRaza" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosRaza"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="grdRaza_PageChanged" AutoGenerateColumns="False" Visible="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="crrz_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_raza_desc" ReadOnly="True" HeaderText="Raza">
																						<HeaderStyle Width="70%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="true" DataField="_soli" ReadOnly="True" HeaderText="Solicitud"></asp:BoundColumn>
																					<asp:BoundColumn Visible="true" DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="300px" filtra="true"
																				NomOper="razas_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaRaza" runat="server" cssclass="boton" Width="100px" Text="Agregar Raza"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaRaza" runat="server" cssclass="boton" Width="100px" Text="Eliminar Raza"
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiRaza" runat="server" cssclass="boton" Width="100px" Text="Modificar Raza"
																				Visible="False"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpRaza" runat="server" cssclass="boton" Width="100px" Text="Limpiar Raza"
																				Visible="False"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnTeleId" runat="server"></asp:textbox><asp:textbox id="hdnDireId" runat="server"></asp:textbox><asp:textbox id="hdnMailId" runat="server"></asp:textbox><asp:textbox id="hdnAgru" runat="server"></asp:textbox><asp:textbox id="hdnClagId" runat="server"></asp:textbox><asp:textbox id="hdnOrdenAnte" runat="server"></asp:textbox><asp:textbox id="hdnOrdenProx" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnClieIds" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><cc1:combobox class="combo" id="cmbLocaAux" runat="server" AceptaNull="false"></cc1:combobox><asp:textbox id="hdnRazaId" runat="server" AutoPostBack="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Criaderos');
		</SCRIPT>
	</BODY>
</HTML>
