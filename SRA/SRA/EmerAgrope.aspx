<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EmerAgrope" CodeFile="EmerAgrope.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Emergencia agropecuaria</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content="Visual Basic .NET 7.1" name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultgrupntScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="stylesheet/sra.css" type=text/css rel=stylesheet >
<script language=JavaScript src="includes/utiles.js"></script>

<script language=JavaScript src="includes/paneles.js"></script>
</HEAD>
<BODY class=pagina leftMargin=5 topMargin=5 
onload=javascript:gSetearTituloFrame(); rightMargin=0>
<form id=frmABM method=post runat="server">
			<!------------------ RECUADRO ------------------->
<table cellSpacing=0 cellPadding=0 width="97%" align=center border=0>
  <tr>
    <td width=9><IMG height=10 src="imagenes/recsupiz.jpg" width=9 border=0 ></TD>
    <td background=imagenes/recsup.jpg><IMG height=10 src="imagenes/recsup.jpg" width=9 border=0 ></TD>
    <td width=13><IMG height=10 src="imagenes/recsupde.jpg" width=13 border=0 ></TD></TR>
  <tr>
    <td width=9 background=imagenes/reciz.jpg><IMG height=10 src="imagenes/reciz.jpg" width=9 border=0 ></TD>
    <td vAlign=middle align=center>
						<!----- CONTENIDO ----->
      <TABLE id=Table1 style="WIDTH: 100%; HEIGHT: 130px" cellSpacing=0 
      cellPadding=0 border=0>
        <TR>
          <TD width="100%" colSpan=3></TD></TR>
        <TR>
          <TD style="HEIGHT: 25px" vAlign=bottom colSpan=3 height=25 
          ><asp:label id=lblTituAbm runat="server" cssclass="opcion"> Emergencia Agropecuaria</asp:label></TD></TR>
        <TR>
          <TD vAlign=top colSpan=3><asp:panel 
            id=panFiltro runat="server" cssclass="titulo" 
            BorderStyle="Solid" Width="100%" BorderWidth="0" Visible="True">
            <TABLE id=TableFil style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
            align=left border=0>
              <TR>
                <TD style="WIDTH: 100%">
                  <TABLE id=Table4 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24></TD>
                      <TD style="HEIGHT: 8px" width=42></TD>
                      <TD style="HEIGHT: 8px" width=26></TD>
                      <TD style="HEIGHT: 8px"></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnBusc runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnLimpiarFil runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24><IMG height=25 
                        src="imagenes/formfle.jpg" width=24 border=0></TD>
                      <TD style="HEIGHT: 8px" width=42><IMG height=25 
                        src="imagenes/formtxfiltro.jpg" width=113 border=0></TD>
                      <TD style="HEIGHT: 8px" width=26><IMG height=25 
                        src="imagenes/formcap.jpg" width=26 border=0></TD>
                      <TD style="HEIGHT: 8px" background=imagenes/formfdocap.jpg 
                      colSpan=3><IMG height=25 src="imagenes/formfdocap.jpg" 
                        width=7 border=0></TD></TR></TABLE></TD></TR>
              <TR>
                <TD>
                  <TABLE id=Table5 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD width=3 background=imagenes/formiz.jpg><IMG 
                        height=30 src="imagenes/formiz.jpg" width=3 border=0></TD>
                      <TD><!-- FOMULARIO -->
                        <TABLE id=Table6 cellSpacing=0 cellPadding=0 
                        width="100%" border=0>
                          <TR>
                            <TD style="HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg 
                          colSpan=2></TD></TR>
                          <TR>
                            <TD style="WIDTH: 20%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblApelFil runat="server" cssclass="titulo">Apellido:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 80%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<CC1:TEXTBOXTAB id=txtApelFil runat="server" cssclass="cuadrotexto" Width="250px"></CC1:TEXTBOXTAB></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 20%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblNumeFil runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 80%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:numberbox id=txtNumeFil runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False" AutoPostback="True"></cc1:numberbox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 20%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblCuitFil runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 80%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<CC1:CUITBOX id=txtCuitFil runat="server" cssclass="cuadrotexto" Width="200px" AceptaNull="False"></CC1:CUITBOX></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 20%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblDocuFil runat="server" cssclass="titulo">Nro. Documento:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 80%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbDocuTipoFil runat="server" Width="100px" AceptaNull="False"></cc1:combobox>
<cc1:numberbox id=txtDocuNumeFil runat="server" cssclass="cuadrotexto" Width="143px" MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 20%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblClieFil runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 80%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<UC1:CLIE id=usrClieFil runat="server" AceptaNull="false" FilDocuNume="True" MuestraDesc="False" FilSociNume="True" Saltos="1,2" Tabla="Clientes" FilTipo="S" Ancho="800"></UC1:CLIE></TD></TR>
                          <TR>
                            <TD style="HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg 
                          colSpan=2></TD></TR></TABLE></TD>
                      <TD width=2 background=imagenes/formde.jpg><IMG height=2 
                        src="imagenes/formde.jpg" width=2 
                  border=0></TD></TR></TABLE></TD></TR></TABLE></asp:panel></TD></TR>
        <TR>
          <TD colSpan=3 height=10></TD></TR>
        <TR>
          <TD vAlign=top align=center colSpan=3><asp:datagrid id=grdDato runat="server" BorderStyle="None" BorderWidth="1px" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" width="100%">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="emer_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="_soci_nume" HeaderText="Socio"></asp:BoundColumn>
											<asp:BoundColumn DataField="_clie_apel" HeaderText="Apellido / Raz�n Social"></asp:BoundColumn>
											<asp:BoundColumn DataField="emer_desde_fecha" HeaderText="Desde" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="69px"></asp:BoundColumn>
											<asp:BoundColumn DataField="emer_hasta_fecha" HeaderText="Hasta" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="69px"></asp:BoundColumn>
											<asp:BoundColumn DataField="_conf" HeaderText="Conf." HeaderStyle-Width="25px"></asp:BoundColumn>
											<asp:BoundColumn DataField="_comp" HeaderText="Comp."></asp:BoundColumn>
											<asp:BoundColumn DataField="emer_impo" HeaderText="Importe"></asp:BoundColumn>
											<asp:BoundColumn DataField="emer_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="69px"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD></TR>
        <TR>
          <TD colSpan=3 height=10></TD></TR>
        <TR>
          <TD vAlign=top align=center colSpan=3>
            <TABLE id=Table3 cellSpacing=0 cellPadding=0 width="100%" border=0 
            >
              <TR>
                <TD><CC1:BOTONIMAGEN id=btnAgre runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent" ToolTip="Agregar Emergencia Agropecuaria" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
                <TD align=right><CC1:BOTONIMAGEN id=btnList runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BOTONIMAGEN></TD></TR></TABLE></TD></TR>
        <TR>
          <TD colSpan=3 height=10></TD></TR>
        <TR>
          <TD align=center colSpan=3><a name=editar 
            ></A>
            <DIV><asp:panel id=panDato 
            runat="server" cssclass="titulo" BorderStyle="Solid" 
            BorderWidth="1px" Visible="False" width="100%" Height="116px">
            <P align=right>
            <TABLE class=FdoFld id=Table2 style="WIDTH: 100%; HEIGHT: 106px" 
            cellPadding=0 align=left border=0>
              <TR>
                <TD>
                  <P></P></TD>
                <TD height=5>
<asp:Label id=lblTitu runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
                <TD vAlign=top align=right>&nbsp; 
<asp:ImageButton id=imgClose runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panCabecera runat="server" cssclass="titulo" Width="100%">
                  <TABLE id=TableCabecera style="WIDTH: 100%" cellPadding=0 
                  align=left border=0>
                    <TR>
                      <TD style="HEIGHT: 24px" vAlign=top align=right>
<asp:Label id=lblSoci runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
                      <TD style="HEIGHT: 24px" align=left>
<UC1:CLIE id=UsrSoci runat="server" AceptaNull="false" FilDocuNume="True" FilSociNume="True" Saltos="1,2" Tabla="Socios" FilTipo="S" Ancho="800" Obligatorio="True" CampoVal="Socio"></UC1:CLIE></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 1px" align=right 
                      background=imagenes/formdivmed.jpg colSpan=4 
                        height=1><IMG height=2 src="imagenes/formdivmed.jpg" 
                        width=1></TD></TR>
                    <TR>
                      <TD noWrap align=right>
<asp:Label id=lblFecha runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp; 
                      </TD>
                      <TD noWrap align=left>
<cc1:DateBox id=txtFecha runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 1px" align=right 
                      background=imagenes/formdivmed.jpg colSpan=4 
                        height=1><IMG height=2 src="imagenes/formdivmed.jpg" 
                        width=1></TD></TR>
                    <TR>
                      <TD noWrap align=right>
<asp:Label id=lblDesdeFecha runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
                      <TD>
                        <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
                        border=0>
                          <TR>
                            <TD style="WIDTH: 97px" align=left width=97>
<cc1:DateBox id=txtDesdeFecha runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
                            <TD noWrap align=left colSpan=2>&nbsp;&nbsp;&nbsp; 
<asp:Label id=lblHastaFecha runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp; 
<cc1:DateBox id=txtHastaFecha runat="server" cssclass="cuadrotexto" Width="70px" CambiaValor="False" AutoPostBack="false"></cc1:DateBox>&nbsp; 
<asp:Button id=btnDeuda runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Deuda" Enabled="True"></asp:Button>&nbsp;&nbsp; 
                            </TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=3 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD vAlign=top align=right>
<asp:Label id=lbObse runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
                      <TD>
<CC1:TEXTBOXTAB id=txtObse runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" Obligatorio="true" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right></TD>
                      <TD>
<asp:CheckBox id=chkConf Text="Confirma" CssClass="titulo" Runat="server"></asp:CheckBox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD style="WIDTH: 100%" align=center 
                        colSpan=3>
<asp:panel id=panComp runat="server" cssclass="titulo" Width="95%" BorderWidth="1px">
                        <TABLE id=TabComp style="WIDTH: 95%" cellPadding=0 
                        align=left border=0>
                          <TR>
                            <TD align=right>
<asp:Label id=lblComp runat="server" cssclass="titulo" Visible="true">Comprobante:</asp:Label>&nbsp;</TD>
                            <TD>
<CC1:TEXTBOXTAB id=txtComp runat="server" cssclass="cuadrotextodeshab" Width="250px" Visible="true" enabled="false"></CC1:TEXTBOXTAB></TD></TR>
                          <TR>
                            <TD vAlign=top noWrap align=right nowrap>
<asp:Label id=lblImpoAjus runat="server" cssclass="titulo">&nbsp;&nbsp;Importe Confirmado:</asp:Label>&nbsp;</TD>
                            <TD width="80%">
<cc1:numberbox id=txtImpo runat="server" cssclass="cuadrotextodeshab" Width="100px" Enabled="false" EsDecimal="True"></cc1:numberbox>&nbsp;&nbsp; 
<asp:Label id=lblBimDescConf runat="server" cssclass="titulo"></asp:Label></TD></TR>
                          <TR>
                            <TD vAlign=top noWrap align=left colSpan=2 
                            height=10></TD></TR>
                          <TR>
                            <TD vAlign=top align=right>
<asp:Label id=lblTotalPagar runat="server" cssclass="titulo" Visible="True">Total a Ajustar:</asp:Label>&nbsp;</TD>
                            <TD width="80%">
<cc1:numberbox id=txtTotalPagar runat="server" cssclass="cuadrotextodeshab" Width="100px" Visible="True" Enabled="false" EsDecimal="True"></cc1:numberbox>&nbsp; 
<asp:Label id=lblBimDesc runat="server" cssclass="titulo"></asp:Label></TD></TR>
                          <TR>
                            <TD vAlign=top align=right>
<asp:Label id=lblDeudaAnterior runat="server" cssclass="titulo">Deuda Anterior:</asp:Label>&nbsp;</TD>
                            <TD width="80%">
<cc1:numberbox id=txtDeudaAnterior runat="server" cssclass="cuadrotextodeshabrojo" Width="100px" ForeColor="Red" EsDecimal="True" ReadOnly="True"></cc1:numberbox>&nbsp; 
<asp:Label id=lblBimAnteDesc runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD></TR></TABLE></asp:panel></TD></TR></TABLE></asp:panel></TD></TR>
              <TR height=30>
                <TD align=center colSpan=3><A id=editar name=editar></A>
<asp:Button id=btnAlta runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp; 
                  &nbsp; 
<asp:Button id=btnBaja runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Baja"></asp:Button>&nbsp;&nbsp; 
<asp:Button id=btnModi runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Modificar"></asp:Button>&nbsp; 
                  &nbsp; 
<asp:Button id=btnLimp runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Limpiar"></asp:Button></TD></TR></TABLE></P></asp:panel></DIV></TD></TR></TABLE>
						<!--- FIN CONTENIDO ---></TD>
    <td width=13 background=imagenes/recde.jpg><IMG height=10 src="imagenes/recde.jpg" width=13 border=0 ></TD></TR>
  <tr>
    <td width=9><IMG height=15 src="imagenes/recinfiz.jpg" width=9 border=0 ></TD>
    <td background=imagenes/recinf.jpg><IMG height=15 src="imagenes/recinf.jpg" width=13 border=0 ></TD>
    <td width=13><IMG height=15 src="imagenes/recinfde.jpg" width=13 border=0 ></TD></TR></TABLE>
			<!----------------- FIN RECUADRO ----------------->
<DIV style="DISPLAY: none"><asp:textbox id=hdnSociId runat="server"></asp:textbox><asp:textbox id=hdnDeuda AutoPostBack="true" Runat="server"></asp:textbox><asp:textbox id=hdnDatosPop runat="server" AutoPostBack="True"></asp:textbox><ASP:TEXTBOX id=lblMens runat="server"></ASP:TEXTBOX><asp:textbox id=hdnId runat="server"></asp:textbox><asp:textbox id=hdnFecha AutoPostBack="true" Runat="server"></asp:textbox></DIV></FORM>
<SCRIPT language=javascript>
			function UsrSoci_onchange()
			{
				document.all('hdnSociId').value = document.all('UsrSoci:txtId').value;
			}
			function btnDeuda_Click()
			{
				if (document.all('txtDesdeFecha').value != '' && document.all('txtHastaFecha').value != '' && document.all("hdnSociId").value != '')
				{
					//var lstrPagina = "DeudaAgro.aspx?sel=1&soci_id=" + document.all("usrSoci").value + "&fecha=" + document.all("txtFecha").value + "&Origen=Cobranzas";
					gAbrirVentanas("DeudaAgro.aspx?sel=1&soci_id=" + document.all("hdnSociId").value + "&fecha_desde=" + document.all("txtDesdeFecha").value + "&fecha_hasta=" + document.all("txtHastaFecha").value + "&Deuda=" + document.all('hdnDeuda').value,8,650,350);
					window.attachEvent('onfocus',window_onfocus);
					window.document.body.attachEvent('onfocus',window_onfocus);
				}
				else
				{
					alert('Debe indicar el socio y las fechas desde y hasta para poder consultar la deuda.');
				}
			}
		if (document.all["editar"]!= null && document.all["panDato"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>

	</BODY>
</HTML>
