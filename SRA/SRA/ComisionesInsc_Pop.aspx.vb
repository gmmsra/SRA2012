Imports SRA

Public Class ComisionesInsc_Pop
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'Protected WithEvents btnAceptar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnCerrar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents grdConsulta As System.Web.UI.WebControls.DataGrid

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comisiones
    Private mstrComiInsc As String = SRA_Neg.Constantes.gTab_ComisionesInscrip
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                mConsultar()

                mGuardarTodosIds()

                mChequearGrilla(grdConsulta.CurrentPageIndex)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Private Sub mGuardarTodosIds()
        Dim lstrId As New System.Text.StringBuilder
        Dim vrDatos() As DataRow
        Dim k As Integer

        While Not Session("mulPag" & k.ToString) Is Nothing
            Session("mulPag" & k.ToString) = Nothing
            k += 1
        End While

        vrDatos = DirectCast(grdConsulta.DataSource, DataSet).Tables(0).Select
        For i As Integer = 0 To (vrDatos.GetUpperBound(0) + 1) / grdConsulta.PageSize
            lstrId.Length = 0
            For j As Integer = i * grdConsulta.PageSize To (i + 1) * grdConsulta.PageSize - 1
                If j >= vrDatos.GetUpperBound(0) + 1 Then Exit For

                If vrDatos(j).IsNull("aprob") Then  'solo agrega los no-aprobados
                    If lstrId.Length > 0 Then lstrId.Append(",")
                    lstrId.Append(vrDatos(j).Item("id").ToString)
                End If
            Next

            Session("mulPag" & i.ToString) = lstrId.ToString
        Next
    End Sub

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            Dim lstrId As New StringBuilder

            For Each oDataItem As DataGridItem In grdConsulta.Items
                If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
                    If lstrId.Length > 0 Then lstrId.Append(",")
                    lstrId.Append(oDataItem.Cells(1).Text)
                End If
            Next

            Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) = lstrId.ToString

            grdConsulta.CurrentPageIndex = E.NewPageIndex

            mConsultar()

            If Not Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) Is Nothing Then
                Dim lstrIdSess As String = Session("mulPag" & grdConsulta.CurrentPageIndex.ToString)
                If lstrIdSess <> "" Then
                    lstrIdSess = "," & lstrIdSess & ","

                    For Each oDataItem As DataGridItem In grdConsulta.Items
                        If lstrIdSess.IndexOf("," & oDataItem.Cells(1).Text & ",") <> -1 Then
                            CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = True
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mChequearGrilla(ByVal pintIndice As Integer)
        If Not (Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) Is Nothing) Then
            Dim lstrIdSess As String = Session("mulPag" & grdConsulta.CurrentPageIndex.ToString)

            lstrIdSess = "," & lstrIdSess & ","

            For Each oDataItem As DataGridItem In grdConsulta.Items
                CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = lstrIdSess.IndexOf("," & oDataItem.Cells(1).Text & ",") <> -1
            Next
        End If
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lsbMsg As New StringBuilder
        lsbMsg.Append("<SCRIPT language='javascript'>")
        lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(1).Text))
        lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            Dim ds As DataSet
            Dim ldsDatos As DataSet

            lstrCmd.Append("exec inscripciones_cargar")
            lstrCmd.Append(" @insc_cicl_id=")
            lstrCmd.Append(Request("cicl_id"))
            lstrCmd.Append(", @mate_id=")
            lstrCmd.Append(Request("mate_id"))

            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)
            ldsDatos = Session(mstrTabla)

            For Each ldrTemp As DataRow In ds.Tables(0).Rows
                If ldsDatos.Tables(mstrComiInsc).Select("coin_insc_id=" & ldrTemp.Item("id").ToString).GetUpperBound(0) <> -1 Then
                    ldrTemp.Delete()
                End If
            Next

            grdConsulta.DataSource = ds
            grdConsulta.DataBind()
            ds.Dispose()

            Session("mulPaginas") = grdConsulta.PageCount

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim lstrId As New StringBuilder
        Dim lstrClieAnt, lstrBuqeAnt As String
        Try
            For Each oDataItem As DataGridItem In grdConsulta.Items
                'If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
                If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
                    If lstrId.Length > 0 Then lstrId.Append(",")
                    lstrId.Append(oDataItem.Cells(1).Text)
                End If
            Next

            For i As Integer = 0 To Session("mulPaginas")
                If Not Session("mulPag" & i.ToString) Is Nothing Then
                    Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
                    If i <> grdConsulta.CurrentPageIndex And lstrIdsSess <> "" Then
                        If lstrId.Length > 0 Then lstrId.Append(",")
                        lstrId.Append(lstrIdsSess)
                    End If
                    Session("mulPag" & i.ToString) = Nothing
                End If
            Next

            grdConsulta.DataSource = Nothing
            grdConsulta.DataBind()

            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", lstrId.ToString))
            lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Response.Write("<Script>window.close();</script>")
    End Sub
End Class
