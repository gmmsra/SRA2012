<%@ Reference Control="~/controles/usrprodnuevo.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PRON" Src="controles/usrProdNuevo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PedigreeCarga" CodeFile="PedigreeCarga.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Carga de Pedigree</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
				
		function cmbRazaFil_change()
		{
		    if(document.all("cmbRazaFil").value!="")
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = true;
				
				document.all("usrPadreFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrPadreFil:cmbProdRaza").onchange();
				document.all("usrPadreFil:cmbProdRaza").disabled = true;
				document.all("txtusrPadreFil:cmbProdRaza").disabled = true;
				
				document.all("usrMadreFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrMadreFil:cmbProdRaza").onchange();
				document.all("usrMadreFil:cmbProdRaza").disabled = true;
				document.all("txtusrMadreFil:cmbProdRaza").disabled = true;
			}
			else
			{
				document.all("usrCriaFil:cmbRazaCria").value = ""
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = false;
				
				document.all("usrPadreFil:cmbProdRaza").value = "";
				document.all("usrPadreFil:cmbProdRaza").onchange();
				document.all("usrPadreFil:cmbProdRaza").disabled = false;
				document.all("txtusrPadreFil:cmbProdRaza").disabled = false;
				
				document.all("usrMadreFil:cmbProdRaza").value = "";
				document.all("usrMadreFil:cmbProdRaza").onchange();
				document.all("usrMadreFil:cmbProdRaza").disabled = false;
				document.all("txtusrMadreFil:cmbProdRaza").disabled = false;
			}
		}
		
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Carga de Pedigree</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderStyle="none" BorderWidth="1px"
											width="100%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 50px">
														<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="17" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE class="FdoFld" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%; HEIGHT: 17px" align="right">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo"> N� Producto:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 85%; HEIGHT: 17px">
																				<CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="140px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="294px"
																					onchange="javascript:cmbRazaFil_change();" Height="20px" NomOper="razas_cargar" MostrarBotones="False"
																					filtra="true"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblSexoFil" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<cc1:combobox class="combo" id="cmbSexoFil" runat="server" AceptaNull="true" Width="113px">
																					<asp:ListItem selected="true" Value="">(Seleccione)</asp:ListItem>
																					<asp:ListItem Value="0">Hembra</asp:ListItem>
																					<asp:ListItem Value="1">Macho</asp:ListItem>
																				</cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblCriaFil" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="false" Tabla="Criadores" Saltos="1,2"
																					FilDocuNume="True" Ancho="800" AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="False"
																					Criador="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="400px"
																					Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right"></TD>
																			<TD style="HEIGHT: 17px">
																				<asp:checkbox id="chkBusc" Runat="server" Text="Buscar en..." CssClass="titulo"></asp:checkbox>&nbsp;&nbsp;&nbsp;
																				<asp:checkbox id="chkBaja" Runat="server" Text="Incluir Dados de Baja" CssClass="titulo"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblNaciFechaFil" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<cc1:DateBox id="txtNaciFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;&nbsp;
																				<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																				<cc1:DateBox id="txtNaciFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblRPNumeFil" runat="server" cssclass="titulo">N� RP:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<CC1:TEXTBOXTAB id="txtRPNumeFil" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="140px"
																					Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblPadreFil" runat="server" cssclass="titulo">Padre:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																					FilDocuNume="True" Ancho="800" AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="True"
																					FilSexo="false" Sexo="1"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblMadreFil" runat="server" cssclass="titulo">Madre:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<UC1:PROD id="usrMadreFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																					FilDocuNume="True" Ancho="800" AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="True"
																					FilSexo="false" Sexo="0"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 17px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel>
									</TD>
								</TR>
								<tr>
									<td colspan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="top" align="right" colSpan="3">
										<asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="grdDato_Page">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="prdt_id" ReadOnly="True" HeaderText="Id Producto"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_sra_nume" ReadOnly="True" HeaderStyle-Width="10%"
													HeaderText="Nro.Producto"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_raza" HeaderText="Raza" HeaderStyle-Width="30%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_sexo" ReadOnly="True" HeaderText="Sexo"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_nomb" ReadOnly="True" HeaderText="Nombre"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_rp" ReadOnly="True" HeaderText="RP"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid>
									</TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="3"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
											ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ImageUrl="imagenes/btnImpr.jpg" ToolTip="Cargar Nuevo Pedigree" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											width="100%" Height="116px" Visible="False">
											<TABLE class="FdoFld" id="tabDato" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
													<TD vAlign="top" align="right">
														<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" align="center" colSpan="2">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="tabCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<UC1:PRON id="usrProd" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2" FilDocuNume="True"
																	Ancho="800" AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="True"></UC1:PRON></TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel>
										<ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
			</DIV>
		</form>
	</BODY>
</HTML>
