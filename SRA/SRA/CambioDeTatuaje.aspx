<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CambioDeTatuaje" CodeFile="CambioDeTatuaje.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cambio de Tatuaje</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		
		
		
		function mSetearProducto()
		{
		 if (document.all('hdnCriaId').value!='')
		   {
		     document.all('usrProducto:txtCriaId').value = document.all('hdnCriaId').value;
		   }		
	   }
	    
		function HaceRPNumero(objOrigen, objDestino , evt) 
		{
		   if(document.all(objOrigen).value == '')
		   {
			 document.all(objDestino).value ='';
		   }
				
		   var charCode = (evt.which) ? evt.which : event.keyCode
		 
		   if (charCode > 47 && charCode < 58)
		   {
		  	 document.all(objDestino).value = document.all(objDestino).value + String.fromCharCode(charCode);
		   }
		  return true;
		}
		
		function usrCriadorId_onchange()
		{   
			document.all('hdnCriaId').value = document.all('usrCriador:txtId').value; 
			mSetearProducto();
		}
		function usrCriadorFilId_onchange()
		{
			document.all('hdnCriaFilId').value = document.all('usrCriadorFil:txtId').value; 
			mSetearProductoFil();
		}
		function mSetearProductoFil()
		{
		 if (document.all('hdnCriaFilId').value!='')
		   {
		     document.all('usrProductoFil:txtCriaId').value = document.all('hdnCriaFilId').value;
		   }		
	   }
	   function usrProducto_cmbProdRaza_onchange(pRaza)
		{
		    if(document.all(pRaza).value!="")
		    {
				document.all("usrCriador:cmbRazaCria").value = document.all(pRaza).value;
				document.all("usrCriador:cmbRazaCria").onchange();
				document.all("usrCriador:cmbRazaCria").disabled = true;
				document.all("txtusrCriador:cmbRazaCria").disabled = true;
			}
			else
			{
				document.all("usrCriador:cmbRazaCriador").value = ""
				document.all("usrCriador:cmbRazaCriador").onchange();
				document.all("usrCriador:cmbRazaCriador").disabled = false;
				document.all("txtusrCriador:cmbRazaCriador").disabled = false;
			}
		}
		function usrProducto_onchange()
		{
			document.all("txtRpAnt").value = document.all("usrProducto:txtRP").value;
			document.all("txtRpNumeAnt").value = document.all("usrProducto:txtRP").value;
		
		}
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" border="0">
							<TR>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="2"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Cambio de Tatuaje</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" BorderStyle="Solid"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																	IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 499px" background="imagenes/formfdofields.jpg" align="right">&nbsp;</TD>
															<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 499px" background="imagenes/formfdofields.jpg" align="right">
																<asp:label id="lblProductoFil" runat="server" cssclass="titulo">Producto:</asp:label>&nbsp;</TD>
															<TD background="imagenes/formfdofields.jpg">
																<UC1:PROH style="Z-INDEX: 0" id="usrProductoFil" runat="server" AceptaNull="false" Ancho="800"
																	MuestraDesc="True" FilTipo="S" AutoPostBack="False" Saltos="1,2" Tabla="productos" MuestraBotonAgregaImportado="False"
																	EsPropietario="True"></UC1:PROH></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 20px" height="20" colSpan="2"></TD>
							</TR> <!---fin filtro --->
							<TR>
								<TD vAlign="top" colSpan="2" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="rpcm_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="rpcm_soli_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="8%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_criador" HeaderText="Criador"></asp:BoundColumn>
											<asp:BoundColumn DataField="_prdt_nomb" HeaderText="Producto"></asp:BoundColumn>
											<asp:BoundColumn DataField="rpcm_ante_rp" HeaderText="Rp">
												<HeaderStyle Width="8%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="rpcm_ante_rp_nume" HeaderText="Rp(N�)">
												<HeaderStyle Width="8%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="rpcm_nuev_rp" HeaderText="Rp Nuevo">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="rpcm_nuev_rp_nume" HeaderText="Rp Nuevo(N�)">
												<HeaderStyle Width="12%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="2">
									<TABLE style="WIDTH: 100%" id="Table3" border="0" cellPadding="0" align="left">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
													ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Px"
													ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
													ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
													BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Imprimir Listado" ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2" align="center">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" Height="216">
											<P align="right">
												<TABLE style="WIDTH: 663px; HEIGHT: 215px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 205px" height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 205px" background="imagenes/formfdofields.jpg" align="right">
															<asp:label id="lblFechaSol" runat="server" cssclass="titulo">Fecha:</asp:label>&nbsp;</TD>
														<TD background="imagenes/formfdofields.jpg">
															<cc1:DateBox id="txtFechaSol" runat="server" cssclass="cuadrotexto" AceptaNull="False" Obligatorio="False"
																EnterPorTab="False"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 205px" background="imagenes/formfdofields.jpg" align="right">
															<asp:label id="Label2" runat="server" cssclass="titulo">Producto:</asp:label>&nbsp;</TD>
														<TD background="imagenes/formfdofields.jpg">
															<UC1:PROH style="Z-INDEX: 0" id="usrProducto" runat="server" AceptaNull="false" Ancho="500"
																MuestraDesc="True" FilTipo="S" AutoPostBack="False" Saltos="1,2" Tabla="productos" MuestraBotonAgregaImportado="False"
																EsPropietario="True"></UC1:PROH></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblRPAnt" runat="server" cssclass="titulo">Rp Anterior:</asp:Label></TD>
														<TD width="60%">
															<CC1:TEXTBOXTAB id="txtRpAnt" onkeypress="return HaceRPNumero('txtRpAnt','txtRpNumeAnt', event)"
																runat="server" cssclass="cuadrotexto" Width="88px" Obligatorio="True" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR style="VISIBILITY: hidden">
														<TD style="WIDTH: 205px" align="right">
															<asp:Label id="lblRpNumeAnt" runat="server" cssclass="titulo">Rp Anterior(N�):</asp:Label></TD>
														<TD>
															<CC1:numberbox id="txtRpNumeAnt" runat="server" cssclass="cuadrotexto" Width="88px" Obligatorio="True"
																Enabled="False"></CC1:numberbox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 205px" align="right">
															<asp:Label id="lblRpNew" runat="server" cssclass="titulo" width="78px">Rp Nuevo:</asp:Label></TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtRpNuevo" onkeypress="return HaceRPNumero('txtRpNuevo','txtRpNumeNuevo', event)"
																runat="server" cssclass="cuadrotexto" Width="88px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR style="VISIBILITY: hidden">
														<TD style="WIDTH: 205px" align="right">
															<asp:Label id="lblRpNumeNuevo" runat="server" cssclass="titulo">Rp Nuevo(N�):</asp:Label></TD>
														<TD>
															<CC1:numberbox id="txtRpNumeNuevo" runat="server" cssclass="cuadrotexto" Obligatorio="True" enabled="False"></CC1:numberbox></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaFilId" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="hdnCambioTatuajePop" runat="server" AutoPostBack="True"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		</SCRIPT>
	</BODY>
</HTML>
