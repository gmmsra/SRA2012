Namespace SRA

Partial Class EventosSociosCocktailRRII_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"

    Private mstrConn As String
    Private mstrTablaDeuSoc As String = "cobran_deusoc"
    Private mstrOrigen As String
    Private mstrSoloVencidas As String
    Private mstrSoloDevengadas As String

    Private mchrSepa As Char = ";"c

    Private Enum Columnas As Integer
        ApellidoNombre
    End Enum

    Private Enum Estados As Integer
        Encontrado
        NoEncontrado
        Cerrar
    End Enum

#End Region

#Region "Operaciones sobre la P�gina"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                txtApellidoNomb.Valor = Request.QueryString("soci_apel")
                mConsultar()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        Try
            mRetorno(Estados.Cerrar)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            If Not IsDBNull(txtApellidoNomb.Valor) Then
                If txtApellidoNomb.Valor <> "" Then
                    If txtApellidoNomb.Text.Length >= 3 Then
                        grdConsulta.CurrentPageIndex = 0
                        mConsultar()
                    Else
                        Throw New AccesoBD.clsErrNeg("Debe ingresar al menos 3 caracteres")
                    End If
                Else
                    Throw New AccesoBD.clsErrNeg("Debe ingresar al menos 3 caracteres")
                End If
            Else
                Throw New AccesoBD.clsErrNeg("Debe ingresar al menos 3 caracteres.")
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnEncontrado_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEncontrado.ServerClick
        Try
            mRetorno(Estados.Encontrado)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnNoEncontrado_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNoEncontrado.ServerClick
        Try
            mRetorno(Estados.NoEncontrado)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConsulta.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Funcionalidad"

    Private Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            Dim ds As DataSet

            lstrCmd.Append("exec rrii_cocktel_socios_consul")
            lstrCmd.Append(" @rrii_nomb=")
            lstrCmd.Append("'" & txtApellidoNomb.Valor & "'")

            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            grdConsulta.DataSource = ds
            grdConsulta.DataBind()
            ds.Dispose()

            Session("mulPaginas") = grdConsulta.PageCount

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mRetorno(ByVal estado As Estados)
        Try
            Dim lsbMsg As New StringBuilder
            Dim strLeyenda As String

            lsbMsg.Append("<SCRIPT language='javascript'>")

            Select Case estado
                Case Estados.Encontrado                   
                    strLeyenda = "RRII - entreg�"
                    lsbMsg.Append(String.Format("window.opener.document.all['cmbLugarDos'].value='{0}';", ""))
                    lsbMsg.Append(String.Format("window.opener.document.all['txtFechaRetiroDos'].value='{0}';", ""))
                Case Estados.NoEncontrado
                    strLeyenda = "RRII - No entreg�"

                Case Estados.Cerrar
                    strLeyenda = ""
            End Select

            lsbMsg.Append(String.Format("window.opener.document.all['hdnCocktailRRII'].value='{0}';", strLeyenda))

            lsbMsg.Append("window.opener.__doPostBack('hdnCocktailRRII','');")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region


End Class

End Namespace
