Namespace SRA

Partial Class Rendicion
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents hdnReafId As System.Web.UI.WebControls.TextBox


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Rendiciones
   Private mstrTablaDepo As String = SRA_Neg.Constantes.gTab_RendicionesDepo
   Private mstrTablaPagos As String = SRA_Neg.Constantes.gTab_RendicionesPagos
   Private mstrTablaFaltantes As String = SRA_Neg.Constantes.gTab_RendicionesFaltantes
   Private mstrTablaCheques As String = SRA_Neg.Constantes.gTab_Cheques
   Private mstrTablaChequesMovim As String = SRA_Neg.Constantes.gTab_ChequesMovim

   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet
   Private mbooRecep As Boolean

    Private mstrOrig As String
    Private mstrDest As String
    Private mstrFecha As String
    Private mstrNum As String
   Private Enum Columnas As Integer
      Id = 0
   End Enum

   Private Enum ColumnasDeta As Integer
      Id = 0
      Falta = 4
   End Enum

   Private Enum ColumnasTotales As Integer
      Edit = 0
      PatiId = 1
      MoneId = 2
      MoneDesc = 3
      PatiDesc = 4
      Impo = 5
      Faltante = 6
   End Enum
#End Region

#Region "Inicializaci�n de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      mbooRecep = CBool(CInt(Request("recep")))

      If Not mbooRecep Then
         lblTituAbm.Text = "Rendiciones"
         panTotalesDatos.Visible = False
         grdTotales.Columns(ColumnasTotales.Edit).Visible = False
         grdTotales.Columns(ColumnasTotales.Faltante).Visible = False

         trRepaFalta.Visible = False
         trRedoFalta.Visible = False
         grdRedo.Columns(ColumnasDeta.Falta).Visible = False
         grdRepa.Columns(ColumnasDeta.Falta).Visible = False

      Else
         lblTituAbm.Text = "Rendiciones-Recepci�n"

         txtFecha.Enabled = False
         btnAgre.Visible = False
         btnAlta.Visible = False
         btnBaja.Visible = False
         btnLimp.Visible = False

         btnAltaRedo.Visible = False
         btnBajaRedo.Visible = False
         btnLimpRedo.Visible = False
         btnAltaRepa.Visible = False
         btnBajaRepa.Visible = False
         btnLimpRepa.Visible = False
      End If
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mSetearEventos()
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mCargarCombos()

            If Not mbooRecep Then
               cmbEmctFil.Valor = hdnEmctId.Text
            End If

            mLimpiar()

            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)

				If Session("sCentroEmisorId") <> hdnDestEmctId.Text Then cmbEmctFil.Enabled = False

                If mbooRecep And Session("sCentroEmisorCentral") <> "S" Then
                    btnAgre.Visible = False
                    btnModi.Visible = False
                    btnRecep.Visible = False
                    btnAnul.Visible = False
                    grdDato.Enabled = False
                    Throw New AccesoBD.clsErrNeg("Solamente se puede recepcionar una rendici�n desde el centro emisor principal")
            End If

            Else
			mdsDatos = Session(mstrTabla)
         End If

		Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmctFil, IIf(mbooRecep, "T", ""))
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja de la rendici�n?')) return false;")
   End Sub
#End Region

#Region "Seteo de Controles"
	Private Sub mSetearBotones(ByVal pbooDesactivo As Boolean)
		If pbooDesactivo Then
			btnAltaRepa.Enabled = Not pbooDesactivo
			btnModiRepa.Enabled = Not pbooDesactivo
			btnBajaRepa.Enabled = Not pbooDesactivo
			btnLimpRepa.Enabled = Not pbooDesactivo
			btnAltaRedo.Enabled = Not pbooDesactivo
			btnModiRedo.Enabled = Not pbooDesactivo
			btnBajaRedo.Enabled = Not pbooDesactivo
			btnLimpRedo.Enabled = Not pbooDesactivo

			panTotalesDatos.Visible = Not pbooDesactivo

			btnAlta.Enabled = Not pbooDesactivo
			btnModi.Enabled = Not pbooDesactivo
            btnRecep.Enabled = Not pbooDesactivo
            btnBaja.Enabled = Not pbooDesactivo
			btnLimp.Enabled = Not pbooDesactivo
			btnModiTotal.Enabled = Not pbooDesactivo
			btnLimpTotal.Enabled = Not pbooDesactivo

		End If

	End Sub

	Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, ByVal pbooCerrada As Boolean)
		Select Case pstrTabla
			Case mstrTablaPagos
				btnAltaRepa.Enabled = pbooAlta And Not pbooCerrada
				btnModiRepa.Enabled = Not pbooAlta And Not pbooCerrada
				btnBajaRepa.Enabled = Not pbooAlta And Not pbooCerrada

			Case mstrTablaDepo
				btnAltaRedo.Enabled = pbooAlta And Not pbooCerrada
				btnModiRedo.Enabled = Not pbooAlta And Not pbooCerrada
				btnBajaRedo.Enabled = Not pbooAlta And Not pbooCerrada

			Case mstrTablaFaltantes
				panTotalesDatos.Visible = Not pbooAlta

			Case Else
				btnAlta.Enabled = pbooAlta And Not pbooCerrada
				btnModi.Enabled = Not pbooAlta And Not pbooCerrada
                btnRecep.Enabled = Not pbooAlta And Not pbooCerrada And mbooRecep
                btnBaja.Enabled = Not pbooAlta And Not pbooCerrada
				btnList.Visible = Not pbooAlta
                btnRecep.Visible = mbooRecep
                btnAnul.Visible = mbooRecep
      End Select

	End Sub


	Public Sub mCargarDatos(ByVal pstrId As String)
		Dim lbooCerrada As Boolean
		Dim lbooDesactivoBotones As Boolean

		hdnId.Text = pstrId
		mCrearDataSet(pstrId)
		lbooDesactivoBotones = False

		With mdsDatos.Tables(mstrTabla).Rows(0)
			hdnId.Text = .Item("rend_id").ToString()
			txtNume.Valor = .Item("rend_nume")
			hdnDestEmctId.Text = .Item("rend_dest_emct_id")
			txtOrigEmct.Text = .Item("_orig_emct_desc")
			txtDestEmct.Text = .Item("_dest_emct_desc")
			txtFecha.Fecha = .Item("rend_fecha")
			lbooCerrada = Not .IsNull("rend_cierre_fecha")
			'Seteo los botones segun Centro Emisor
			If Session("sCentroEmisorId") <> .Item("rend_orig_emct_id") Then lbooDesactivoBotones = True
		End With

		grdRedo.Columns(0).Visible = Not mbooRecep Or Not lbooCerrada
		grdRedo.DataSource = mdsDatos.Tables(mstrTablaDepo)
		grdRedo.DataBind()

		grdRepa.Columns(0).Visible = Not mbooRecep Or Not lbooCerrada
		grdRepa.DataSource = mdsDatos.Tables(mstrTablaPagos)
		grdRepa.DataBind()

		lblTitu.Text = "Registro Seleccionado: " + txtOrigEmct.Text + " - " + txtFecha.Text

		mSetearEditor("", False, lbooCerrada)

		mMostrarPanel(True)
		mSetearBotones(lbooDesactivoBotones)
		mLimpiarRepa()
		mLimpiarRedo()
		mLimpiarTotal()

		If Not lbooCerrada Then
            mSetearRecep()
            btnAnul.Enabled = False
            btnRecep.Enabled = True
        Else
            btnAnul.Enabled = True
            btnRecep.Enabled = False
        End If

	End Sub

	Public Sub mCrearDataSet(ByVal pstrId As String)

		mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

		mdsDatos.Tables(0).TableName = mstrTabla
		mdsDatos.Tables(1).TableName = mstrTablaPagos
		mdsDatos.Tables(2).TableName = mstrTablaDepo
		mdsDatos.Tables(3).TableName = mstrTablaFaltantes

		mConsultarRepa()
		mConsultarRedo()

		Session(mstrTabla) = mdsDatos
	End Sub

	Private Sub mAgregar()
		mLimpiar()
		mMostrarPanel(True)
	End Sub

	Private Sub mLimpiar()
		hdnId.Text = ""
		lblTitu.Text = ""

		txtNume.Text = ""
		With clsSQLServer.gObtenerEstruc(mstrConn, "emisores_ctros", hdnEmctId.Text).Tables(0).Rows(0)
			txtOrigEmct.Text = .Item("emct_codi").ToString + "-" + .Item("emct_desc")
		End With

		With clsSQLServer.gObtenerEstruc(mstrConn, "emisores_ctros", "@emct_central=1").Tables(0).Rows(0)
			hdnDestEmctId.Text = .Item("emct_id").ToString
			txtDestEmct.Text = .Item("emct_codi").ToString + "-" + .Item("emct_desc")
		End With

            txtFecha.Fecha = Today
            txtFecha.Enabled = Not mbooRecep

		grdRepa.CurrentPageIndex = 0
		grdRedo.CurrentPageIndex = 0

		mCrearDataSet("")
		mLimpiarRepa()
		mLimpiarRedo()
		mLimpiarTotal()

		mSetearEditor("", True, False)
		mShowTabs(1)
	End Sub

	Private Sub mCerrar()
		mLimpiar()
		mConsultar()
	End Sub

	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		panDato.Visible = pbooVisi
		panBotones.Visible = pbooVisi
		panFiltro.Visible = Not panDato.Visible
        'btnAgre.Visible = Not panDato.Visible And Not mbooRecep And hdnEmctId.Text <> hdnDestEmctId.Text
		grdDato.Visible = Not panDato.Visible
		tabLinks.Visible = pbooVisi

		If pbooVisi Then
			grdDato.DataSource = Nothing
			grdDato.DataBind()
			mShowTabs(1)
		Else
			Session(mstrTabla) = Nothing
			mdsDatos = Nothing
		End If
	End Sub

	Private Sub mShowTabs(ByVal origen As Byte)
		Dim lstrFiltro As String

		Try
			panDato.Visible = True
			panBotones.Visible = True
			panRepa.Visible = False
			panRedo.Visible = False
			panTotales.Visible = False
			panCabecera.Visible = False

			lnkCabecera.Font.Bold = False
			lnkRepa.Font.Bold = False
			lnkRedo.Font.Bold = False
			lnkTotales.Font.Bold = False

			lstrFiltro = "@fecha=" & clsFormatear.gFormatFecha2DB(txtFecha.Fecha)
			lstrFiltro += ",@emct_id=" + hdnEmctId.Text
			lstrFiltro += ",@rend_id=" + clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

			Select Case origen
				Case 1
					panCabecera.Visible = True
					lnkCabecera.Font.Bold = True
					lblTitu.Text = "Datos de la Rendici�n"
					txtFecha.Enabled = Not mbooRecep And mdsDatos.Tables(mstrTablaDepo).Select().GetUpperBound(0) = -1 And mdsDatos.Tables(mstrTablaPagos).Select().GetUpperBound(0) = -1

				Case 2
					panRepa.Visible = True
					lnkRepa.Font.Bold = True
					lblTitu.Text = "Pagos de la Rendici�n Nro: " & txtNume.Text

					lstrFiltro = "@fecha=" & clsFormatear.gFormatFecha2DB(txtFecha.Fecha)
					lstrFiltro += ",@emct_id=" + hdnEmctId.Text
					lstrFiltro += ",@rend_id=" + clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

					If hdnId.Text = "" Then
						mAgregarRepaTodos(lstrFiltro)
						mConsultarRepa()
					ElseIf Not mbooRecep Then					 'solo en una modificaci�n, para agregarlos manualemente
						clsWeb.gCargarRefeCmb(mstrConn, "comprob_pagos_rend", cmbPaco, "S", lstrFiltro)
					End If

				Case 3
					panRedo.Visible = True
					lnkRedo.Font.Bold = True
					lblTitu.Text = "Dep�sitos de la Rendici�n Nro: " & txtNume.Text

					If Not mbooRecep Then
						clsWeb.gCargarRefeCmb(mstrConn, "depositos_rend", cmbDepo, "S", lstrFiltro)
					End If

				Case 4
					panTotales.Visible = True
					lnkTotales.Font.Bold = True
					lblTitu.Text = "Totales de la Rendici�n Nro: " & txtNume.Text

					If hdnId.Text = "" Then
						mAgregarRepaTodos(lstrFiltro)
					End If
					mConsultarTotales()
			End Select

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
		mShowTabs(1)
	End Sub

	Private Sub lnkrepa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRepa.Click
		mShowTabs(2)
	End Sub

	Private Sub lnkredo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRedo.Click
		mShowTabs(3)
	End Sub

	Private Sub lnkTotales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTotales.Click
		mShowTabs(4)
	End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos(False)
         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
         hdnId.Text = lobjGenerico.Alta()
         mListar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mAnularRecepcion()
      Try

         mstrCmd = "rendiciones_anular "
         mstrCmd = mstrCmd & " @rend_id=" & hdnId.Text
         mstrCmd = mstrCmd & ",@rend_audi_user=" & Session("sUserId").ToString()

         clsSQLServer.gExecute(mstrConn, mstrCmd)

         mLimpiar()
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi(ByVal pbooRecep As Boolean)
      Try
         Dim ldsDatos As DataSet = mGuardarDatos(pbooRecep)
         Dim lobjRendicion As New SRA_Neg.Rendicion(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
         lobjRendicion.Modi()
         mListar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos(ByVal pbooRecep As Boolean) As DataSet
      Dim ldsDatos As DataSet = mdsDatos.Copy
      Dim lDrCheq, lDrMovim As DataRow

        'mValidarDatos() Dario 2016-12-21 se comenta por rendiciones sin filas

      With ldsDatos.Tables(mstrTabla).Rows(0)
         .Item("rend_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("rend_nume") = txtNume.Valor
         .Item("rend_fecha") = txtFecha.Fecha

         If pbooRecep Then
            .Item("rend_cierre_fecha") = Today

            If ldsDatos.Tables(mstrTablaFaltantes).Select().GetUpperBound(0) > -1 Then
               .Item("rend_esta_id") = SRA_Neg.Constantes.Estados.Rendiciones_Incompleta
            Else
               .Item("rend_esta_id") = SRA_Neg.Constantes.Estados.Rendiciones_Recibida
            End If

            SRA_Neg.Utiles.gAgregarTabla(mstrConn, ldsDatos, mstrTablaCheques)
            SRA_Neg.Utiles.gAgregarTabla(mstrConn, ldsDatos, mstrTablaChequesMovim)

            For Each lDrRepa As DataRow In ldsDatos.Tables(mstrTablaPagos).Select("_paco_pati_id=" & SRA_Neg.Constantes.PagosTipos.Cheque)
               lDrCheq = SRA_Neg.Utiles.gAgregarRegistro(mstrConn, ldsDatos.Tables(mstrTablaCheques), lDrRepa.Item("_paco_cheq_id"))

               'guardo los datos anteriores
               lDrMovim = ldsDatos.Tables(mstrTablaChequesMovim).NewRow
               With lDrMovim
                  .Item("mchq_id") = -1
                  .Item("mchq_cheq_id") = lDrCheq.Item("cheq_id")
                  .Item("mchq_rece_fecha") = lDrCheq.Item("cheq_rece_fecha")
                  .Item("mchq_teor_depo_fecha") = lDrCheq.Item("cheq_teor_depo_fecha")
                  .Item("mchq_clea") = lDrCheq.Item("cheq_clea_id")
                  .Item("mchq_emct_id") = lDrCheq.Item("cheq_emct_id")
                  .Table.Rows.Add(lDrMovim)
               End With

               lDrCheq.Item("cheq_emct_id") = ldsDatos.Tables(mstrTabla).Rows(0).Item("rend_dest_emct_id")
            Next
         Else
            If Not mbooRecep Then
               .Item("rend_orig_emct_id") = hdnEmctId.Text
               .Item("rend_dest_emct_id") = hdnDestEmctId.Text
            End If
            .Item("rend_cierre_fecha") = DBNull.Value
         End If
      End With

      Return (ldsDatos)
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
      With mdsDatos.Tables
         If .Item(mstrTablaPagos).Select().GetUpperBound(0) = -1 _
          And .Item(mstrTablaDepo).Select("redo_falta=TRUE").GetUpperBound(0) = -1 Then
            Throw New AccesoBD.clsErrNeg("La rendici�n no posee comprobantes")
         End If
      End With
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @rend_emct_id =" + cmbEmctFil.Valor.ToString)
         lstrCmd.Append(", @fecha=" & clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
         lstrCmd.Append(", @pendi_recep=" & Math.Abs(CInt(chkPendiFil.Checked)))

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Pagos"
   Public Sub mEditarDatosRepa(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrRepa As DataRow

         hdnRepaId.Text = E.Item.Cells(1).Text
         ldrRepa = mdsDatos.Tables(mstrTablaPagos).Select("repa_id=" & hdnRepaId.Text)(0)

         With ldrRepa
            mSelecItemCombo(.Item("repa_paco_id"), .Item("_paco_desc"), cmbPaco)
            chkRepaFalta.Checked = .Item("repa_falta")

            trRepaFalta.Visible = mbooRecep And .Item("_paco_pati_id") <> SRA_Neg.Constantes.PagosTipos.Efectivo
         End With

         mSetearEditor(mstrTablaPagos, False, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdRepa_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdRepa.EditItemIndex = -1
         If (grdRepa.CurrentPageIndex < 0 Or grdRepa.CurrentPageIndex >= grdRepa.PageCount) Then
            grdRepa.CurrentPageIndex = 0
         Else
            grdRepa.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarRepa()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarRepa()
      Try
         mGuardarDatosRepa()

         mLimpiarRepa()
         mConsultarRepa()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosRepa()
      Dim ldrDatos As DataRow

      If cmbPaco.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el pago")
      End If

      If mdsDatos.Tables(mstrTablaPagos).Select("repa_paco_id=" & cmbPaco.Valor & " AND repa_id <> " & clsSQLServer.gFormatArg(hdnRepaId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("El pago ya est� incluido en la rendici�n.")
      End If

      If hdnRepaId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrTablaPagos).NewRow
         ldrDatos.Item("repa_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaPagos), "repa_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrTablaPagos).Select("repa_id=" & hdnRepaId.Text)(0)
      End If

      With ldrDatos
         .Item("repa_paco_id") = cmbPaco.Valor
         .Item("repa_falta") = chkRepaFalta.Checked
         .Item("_paco_desc") = cmbPaco.SelectedItem.Text
         .Item("_falta") = IIf(chkRepaFalta.Checked, "SI", "NO")

         With clsSQLServer.gObtenerEstruc(mstrConn, "comprob_pagos", .Item("repa_paco_id")).Tables(0).Rows(0)
            ldrDatos.Item("repa_impo") = .Item("paco_orig_impo").ToString
            ldrDatos.Item("repa_mone_id") = .Item("paco_mone_id").ToString
            ldrDatos.Item("_paco_pati_id") = .Item("paco_pati_id").ToString
         End With
      End With

      cmbPaco.Items.Remove(cmbPaco.SelectedItem)

      If hdnRepaId.Text = "" Then
         mdsDatos.Tables(mstrTablaPagos).Rows.Add(ldrDatos)
      End If
   End Sub

   Private Sub mAgregarRepaTodos(ByVal pstrFiltro As String)
      Dim ldrDatos As DataRow
      If mdsDatos.Tables(mstrTablaPagos).Select().GetUpperBound(0) = -1 Then
         Dim ldsTemp As DataSet = clsSQLServer.gExecuteQuery(mstrConn, "comprob_pagos_rend_cargar " + pstrFiltro)
         For Each ldrTemp As DataRow In ldsTemp.Tables(0).Rows
            If mdsDatos.Tables(mstrTablaPagos).Select("repa_paco_id=" & ldrTemp.Item("id").ToString).GetUpperBound(0) = -1 Then
               ldrDatos = mdsDatos.Tables(mstrTablaPagos).NewRow

               With ldrDatos
                  .Item("repa_id") = clsSQLServer.gObtenerId(.Table, "repa_id")
                  .Item("repa_paco_id") = ldrTemp.Item("id")
                  .Item("repa_falta") = False
                  .Item("repa_impo") = ldrTemp.Item("paco_orig_impo").ToString
                  .Item("repa_mone_id") = ldrTemp.Item("paco_mone_id").ToString
                  .Item("_paco_desc") = ldrTemp.Item("descrip")
                  .Item("_paco_pati_id") = ldrTemp.Item("paco_pati_id").ToString
                  .Table.Rows.Add(ldrDatos)
               End With
            End If
         Next
      End If
   End Sub

   Private Sub mLimpiarRepa()
      hdnRepaId.Text = ""
      cmbPaco.Limpiar()
      chkRepaFalta.Checked = False
      mSetearEditor(mstrTablaPagos, True, False)
   End Sub

   Private Sub btnLimprepa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpRepa.Click
      mLimpiarRepa()
   End Sub

   Private Sub btnBajarepa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaRepa.Click
      Try
         mdsDatos.Tables(mstrTablaPagos).Select("repa_id=" & hdnRepaId.Text)(0).Delete()
         mConsultarRepa()
         mLimpiarRepa()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltarepa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaRepa.Click
      mActualizarRepa()
   End Sub

   Private Sub btnModirepa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiRepa.Click
      mActualizarRepa()
   End Sub

   Public Sub mConsultarRepa()
      grdRepa.DataSource = mdsDatos.Tables(mstrTablaPagos)
      grdRepa.DataBind()

      mSetearRecep()
   End Sub
#End Region

#Region "Dep�sitos"
   Public Sub mEditarDatosRedo(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrRedo As DataRow

         hdnRedoId.Text = E.Item.Cells(1).Text
         ldrRedo = mdsDatos.Tables(mstrTablaDepo).Select("redo_id=" & hdnRedoId.Text)(0)

         With ldrRedo
            mSelecItemCombo(.Item("redo_depo_id"), .Item("_depo_desc"), cmbDepo)
            chkRedoFalta.Checked = .Item("redo_falta")
         End With

         mSetearEditor(mstrTablaDepo, False, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdredo_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdRedo.EditItemIndex = -1
         If (grdRedo.CurrentPageIndex < 0 Or grdRedo.CurrentPageIndex >= grdRedo.PageCount) Then
            grdRedo.CurrentPageIndex = 0
         Else
            grdRedo.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarRedo()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarRedo()
      Try
         mGuardarDatosRedo()

         mLimpiarRedo()
         mConsultarRedo()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosRedo()
      Dim ldrDatos As DataRow

      If cmbDepo.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el dep�sito")
      End If

      If mdsDatos.Tables(mstrTablaDepo).Select("redo_depo_id=" & cmbDepo.Valor & " AND redo_id <> " & clsSQLServer.gFormatArg(hdnRedoId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("El dep�sito ya est� incluido en la rendici�n.")
      End If

      If hdnRedoId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrTablaDepo).NewRow
         ldrDatos.Item("redo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDepo), "redo_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrTablaDepo).Select("redo_id=" & hdnRedoId.Text)(0)
      End If

      With ldrDatos
         .Item("redo_depo_id") = cmbDepo.Valor
         .Item("_depo_desc") = cmbDepo.SelectedItem.Text
         .Item("redo_falta") = chkRedoFalta.Checked
         .Item("_falta") = IIf(chkRedoFalta.Checked, "SI", "NO")

         With clsSQLServer.gObtenerEstruc(mstrConn, "depositos", .Item("redo_depo_id")).Tables(0).Rows(0)
            ldrDatos.Item("redo_impo") = .Item("_depo_monto").ToString
            ldrDatos.Item("redo_mone_id") = .Item("depo_mone_id").ToString
         End With
      End With

      cmbDepo.Items.Remove(cmbDepo.SelectedItem)

      If hdnRedoId.Text = "" Then
         mdsDatos.Tables(mstrTablaDepo).Rows.Add(ldrDatos)
      End If
   End Sub

   Private Sub mLimpiarRedo()
      hdnRedoId.Text = ""
      cmbDepo.Limpiar()
      chkRedoFalta.Checked = False
      mSetearEditor(mstrTablaDepo, True, False)
   End Sub

   Private Sub btnLimpredo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpRedo.Click
      mLimpiarRedo()
   End Sub

   Private Sub btnBajaredo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaRedo.Click
      Try
         mdsDatos.Tables(mstrTablaDepo).Select("redo_id=" & hdnRedoId.Text)(0).Delete()
         mConsultarRedo()
         mLimpiarRedo()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaredo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaRedo.Click
      mActualizarRedo()
   End Sub

   Private Sub btnModiredo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiRedo.Click
      mActualizarRedo()
   End Sub

   Public Sub mConsultarRedo()
      grdRedo.DataSource = mdsDatos.Tables(mstrTablaDepo)
      grdRedo.DataBind()

      mSetearRecep()
   End Sub
#End Region

#Region "Totales"
   Public Sub mEditarDatosTotales(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrTotal As DataRow
         hdnMoneId.Text = E.Item.Cells(ColumnasTotales.MoneId).Text
         hdnPatiId.Text = E.Item.Cells(ColumnasTotales.PatiId).Text
         hdnImpo.Text = E.Item.Cells(ColumnasTotales.Impo).Text

         txtValor.Text = E.Item.Cells(ColumnasTotales.MoneDesc).Text + " - " _
                           + E.Item.Cells(ColumnasTotales.PatiDesc).Text + " (" _
                           + E.Item.Cells(ColumnasTotales.Impo).Text + ")"

         txtFaltante.Text = E.Item.Cells(ColumnasTotales.Faltante).Text

         mSetearEditor(mstrTablaFaltantes, False, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarTotal()
      Try
         mGuardarDatosTotal()

         mLimpiarTotal()
         mConsultarTotales()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosTotal()
      Dim ldrDatos As DataRow

      If CDec(txtFaltante.Valor) > CDec(hdnImpo.Text) Then
         Throw New AccesoBD.clsErrNeg("El importe faltante no puede ser mayor al importe total")
      End If

      With mdsDatos.Tables(mstrTablaFaltantes).Select("refa_pati_id=" & hdnPatiId.Text & " AND refa_mone_id = " & hdnMoneId.Text)
         If .GetUpperBound(0) = -1 Then
            ldrDatos = mdsDatos.Tables(mstrTablaFaltantes).NewRow
            With ldrDatos
               .Item("refa_id") = clsSQLServer.gObtenerId(.Table, "refa_id")
               .Item("refa_pati_id") = hdnPatiId.Text
               .Item("refa_mone_id") = hdnMoneId.Text
               .Table.Rows.Add(ldrDatos)
            End With
         Else
            ldrDatos = .GetValue(0)
         End If
      End With

      With ldrDatos
         .Item("refa_impo") = txtFaltante.Valor

         If .Item("refa_impo") = 0 Then
            .Delete()
         End If
      End With
   End Sub

   Private Sub mLimpiarTotal()
      hdnPatiId.Text = ""
      hdnMoneId.Text = ""
      hdnImpo.Text = ""
      txtValor.Text = ""
      txtFaltante.Text = ""
      mSetearEditor(mstrTablaFaltantes, True, False)
   End Sub

   Private Sub btnLimpTotal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpTotal.Click
      mLimpiarTotal()
   End Sub

   Private Sub btnModiTotal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiTotal.Click
      mActualizarTotal()
   End Sub

   Private Sub mConsultarTotales()
      Dim ldrDatos As DataRow
      Dim ldtDatos As New DataTable
      Dim lintMoneAnt, lintPatiAnt As Integer

      With ldtDatos
         .Columns.Add("pati_id", System.Type.GetType("System.Int32"))
         .Columns.Add("mone_id", System.Type.GetType("System.Int32"))
         .Columns.Add("mone_desc", System.Type.GetType("System.String"))
         .Columns.Add("pati_desc", System.Type.GetType("System.String"))
         .Columns.Add("total", System.Type.GetType("System.Double"))
         .Columns.Add("faltante", System.Type.GetType("System.Double"))
      End With

      For Each ldrTemp As DataRow In mdsDatos.Tables(mstrTablaPagos).Select("", "repa_mone_id, _paco_pati_id")
         If ldrTemp.Item("repa_mone_id") <> lintMoneAnt Or ldrTemp.Item("_paco_pati_id") <> lintPatiAnt Then
            ldrDatos = ldtDatos.NewRow
            ldtDatos.Rows.Add(ldrDatos)

            With clsSQLServer.gObtenerEstruc(mstrConn, "comprob_pagos", ldrTemp.Item("repa_paco_id")).Tables(0).Rows(0)
               ldrDatos.Item("mone_desc") = .Item("_mone_desc")
               ldrDatos.Item("pati_desc") = .Item("_pati_desc")
            End With

            ldrDatos.Item("mone_id") = ldrTemp.Item("repa_mone_id")
            ldrDatos.Item("pati_id") = ldrTemp.Item("_paco_pati_id")
            ldrDatos.Item("total") = 0
            ldrDatos.Item("faltante") = 0

            lintMoneAnt = ldrTemp.Item("repa_mone_id")
            lintPatiAnt = ldrTemp.Item("_paco_pati_id")

            'agrego los faltantes
            For Each ldrRefa As DataRow In mdsDatos.Tables(mstrTablaFaltantes).Select("refa_pati_id=" & ldrDatos.Item("pati_id") & " AND refa_mone_id=" & ldrDatos.Item("mone_id"))
               ldrDatos.Item("faltante") += ldrRefa.Item("refa_impo")
            Next
         End If

         ldrDatos.Item("total") += ldrTemp.Item("repa_impo")
      Next

      lintMoneAnt = 0

      For Each ldrTemp As DataRow In mdsDatos.Tables(mstrTablaDepo).Select("", "redo_mone_id")
         If ldrTemp.Item("redo_mone_id") <> lintMoneAnt Then
            ldrDatos = ldtDatos.NewRow
            ldtDatos.Rows.Add(ldrDatos)

            With clsSQLServer.gObtenerEstruc(mstrConn, "depositos", ldrTemp.Item("redo_depo_id")).Tables(0).Rows(0)
               ldrDatos.Item("mone_desc") = .Item("_mone_desc")
               ldrDatos.Item("pati_desc") = "DEPOSITO"
               ldrDatos.Item("total") = 0
            End With

            lintMoneAnt = ldrTemp.Item("redo_mone_id")
         End If

         ldrDatos.Item("total") += ldrTemp.Item("redo_impo")
      Next

      grdTotales.DataSource = ldtDatos
      grdTotales.DataBind()

      mSetearRecep()
   End Sub
#End Region

#Region "Eventos"
   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi(False)
   End Sub

   Private Sub btnRecep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecep.Click
      mModi(True)
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         mListar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Sub mTraerDatos()
        Dim ds As DataSet
        Dim strCmd As String = mstrTabla + "_rpt_consul "
        strCmd += hdnId.Text
        strCmd += "," + Math.Abs(CInt(mbooRecep)).ToString
        ds = clsSQLServer.gExecuteQuery(mstrConn, strCmd)

        With ds.Tables(0).Rows(0)
            mstrOrig = .Item("orig_emct_desc").ToString
            mstrDest = .Item("dest_emct_desc").ToString
            mstrNum = .Item("rend_nume").ToString
            mstrFecha = .Item("rend_fecha").ToString
        End With
    End Sub
    Private Sub mListar()
        Dim lstrRptName As String = "Rendicion"
        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
        mTraerDatos()

        lstrRpt += "&rend_id=" + hdnId.Text
        lstrRpt += "&recep=" + Math.Abs(CInt(mbooRecep)).ToString
        lstrRpt += "&orig=" + "'" + mstrOrig + "'"
        lstrRpt += "&dest=" + mstrDest
        lstrRpt += "&num=" + mstrNum
        lstrRpt += "&fecha=" + mstrFecha
        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)
    End Sub
#End Region

   Private Sub mSelecItemCombo(ByVal pstrValor As String, ByVal pstrDesc As String, ByVal pcmbCombo As NixorControls.ComboBox)
      Dim oItemSel As ListItem
      pcmbCombo.SelectedIndex = -1

      If mbooRecep Then
         pcmbCombo.Items.Clear()
      End If

      For Each oItem As ListItem In pcmbCombo.Items
         If oItem.Value = pstrValor Then
            oItem.Selected = True
            Return
         End If
      Next

      oItemSel = New ListItem
      oItemSel.Value = pstrValor
      oItemSel.Text = pstrDesc
      pcmbCombo.Items.Add(oItemSel)
      oItemSel.Selected = True
   End Sub

   Private Sub grdTotales_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdTotales.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 Then
            With CType(e.Item.DataItem, DataRowView).Row
               If .IsNull("pati_id") OrElse .Item("pati_id") <> SRA_Neg.Constantes.PagosTipos.Efectivo Then
                  e.Item.FindControl("lnkTotEdit").Visible = False
               End If
            End With
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearRecep()
      Dim lbooFaltantes, lbooRefaFalta, lbooRedoFalta As Boolean

      lbooFaltantes = mdsDatos.Tables(mstrTablaFaltantes).Select("").GetUpperBound(0) <> -1
      lbooRefaFalta = mdsDatos.Tables(mstrTablaPagos).Select("repa_falta=TRUE").GetUpperBound(0) <> -1
      lbooRedoFalta = mdsDatos.Tables(mstrTablaDepo).Select("redo_falta=TRUE").GetUpperBound(0) <> -1

      btnRecep.Enabled = (Not (lbooFaltantes Or lbooRedoFalta Or lbooRefaFalta) And Not btnAnul.Enabled)

   End Sub

   Private Sub btnAnul_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnul.Click
      mAnularRecepcion()
   End Sub

End Class
End Namespace
