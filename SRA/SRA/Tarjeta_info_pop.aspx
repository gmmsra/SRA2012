<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Tarjeta_info_pop" CodeFile="Tarjeta_info_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title> <%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mEstablecimiento(pEsta)
		{
			var win=window.open('establecimientos.aspx?id='+pEsta, '', 'location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=700px,height=400px,left=50px,top=50px');
		}		
		</script>
</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right">
									<DIV onclick="window.close();"><IMG style="CURSOR: hand" alt="Cerrar" src="imagenes/close3.bmp" border="0">
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD><asp:label id="lblTituAbm" runat="server" cssclass="titulo"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center" colSpan="2"><asp:panel id="panEspeciales" runat="server" Visible="true" cssclass="titulo" Width="100%"
										BorderWidth="1px" BorderStyle="Solid" height="100%">
            <TABLE class=FdoFld id=tblEspeciales style="WIDTH: 100%" 
            cellPadding=0 align=left border=0>
              <TR>
                <TD style="WIDTH: 15.7%" align=right>
<asp:label id=lbl runat="server" cssclass="titulo">Titular:</asp:label>&nbsp; 
                </TD>
                <TD>
<CC1:TEXTBOXTAB id=txtTitular runat="server" cssclass="cuadrotexto" Width="100%" Enabled="False"></CC1:TEXTBOXTAB></TD></TR>
              <TR>
                <TD style="WIDTH: 15.7%" align=right>
<asp:label id=lblNomb runat="server" cssclass="titulo">Direcci�n:</asp:label>&nbsp; 
                </TD>
                <TD>
<CC1:TEXTBOXTAB id=txtDire runat="server" cssclass="cuadrotexto" Width="100%" Enabled="False"></CC1:TEXTBOXTAB></TD></TR>
              <TR>
                <TD style="WIDTH: 15.7%" align=right>
<asp:label id=lblDoc runat="server" cssclass="titulo">C�d. Seguridad:</asp:label>&nbsp; 
                </TD>
                <TD>
<CC1:TEXTBOXTAB id=txtCodSegu runat="server" cssclass="cuadrotexto" Width="96px" Enabled="False"></CC1:TEXTBOXTAB></TD></TR>
              <TR>
                <TD align=right background=imagenes/formdivmed.jpg colSpan=2 
                height=2><IMG height=2 src="imagenes/formdivmed.jpg" 
                width=1></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top"><asp:datagrid id="grdConsulta" runat="server" width="100%" BorderStyle="None" AllowPaging="True"
										BorderWidth="1px" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
										PageSize="15" ItemStyle-Height="5px" OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns></Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
