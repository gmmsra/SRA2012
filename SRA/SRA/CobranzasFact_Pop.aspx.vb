' Dario 2013-08-26 para que tome todos los importes que se cancelaron
Namespace SRA

Partial Class CobranzasFact_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

#Region "Definici�n de Variables"
    Private mstrConn As String
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaCuentasCtes As String = SRA_Neg.Constantes.gTab_CuentasCtes
    Private mstrTipo As String

    Private mstrParaPageSize As Integer
    Private mdsDatos As DataSet
    Public mstrTitulo As String
    Private mstrSoloVencidas As String
    Private VarTipoForm As String


    Private Enum Columnas As Integer
        Chk = 0
        CompId = 1
        CovtId = 2
        ClieId = 3
        CcosId = 4
        Numero = 5
        Cliente = 6
        Actividad = 7
        FechaCuota = 8
        Neto = 9
        SobreTasasAlVto = 10
        Interes = 11
        Pagado = 12
        CompActiId = 13
        PermitePagoParciales = 14
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mdsDatos = Session(Request("sess"))

            mInicializar()

            If Not Page.IsPostBack Then
                Select Case VarTipoForm
                    Case Is = "FP" ' "Facturas Contado Propia"
                        lblClie.Visible = False
                        usrClie.Visible = False
                        usrClie.Valor = Request("clie_id")
                    Case Is = "DP" ' "Deuda Cta.Cte. Propia"
                        lblClie.Visible = False
                        usrClie.Visible = False
                        usrClie.Valor = Request("clie_id")
                    Case Is = "DT" '  "Deuda Cta.Cte. Terceros"
                        lblClie.Visible = True
                        usrClie.Visible = True
                        usrClie.Valor = ""
                        grdConsulta.Columns(Columnas.Cliente).Visible = False
                    Case Is = "F" ' "Facturas Contado"
                        lblClie.Visible = False
                        usrClie.Visible = False
                        'usrClie.Valor = Request("clie_id")
                        'grdConsulta.Columns(Columnas.Cliente).Visible = False
                    Case Else
                        usrClie.Valor = Request("clie_id")
                        grdConsulta.Columns(Columnas.Cliente).Visible = False
                End Select

                mConsultar()

                mCalcularTotales()
                mSetearDA()
                'Roxi: Modificaci�n 04/12/2009
                'Si el cliente es socio con estado suspendido moroso muestra un warning.
                mAvisarMorosidad()
            Else
                If Not (mdsDatos Is Nothing) Then
                    Select Case VarTipoForm
                        Case Is = "DT"
                            If usrClie.Valor = Request("clie_id") Then
                                clsError.gGenerarMensajes(Me, "No se puede seleccionar un cliente igual al origen")
                                Me.usrClie.Valor = ""
                            Else
                                ' Dario 2013-08-30 Se coloca este else sino cuando es DT y el cliente es 
                                ' distinto no pasaba la grilla que selecciono a la pagina de cobranzas
                                mGuardarDatosGrilla()
                            End If
                        Case Else
                            mGuardarDatosGrilla()
                    End Select
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mInicializar()
        mstrTipo = Request("tipo")
        hdnTipo.Text = mstrTipo

        If mstrTipo = "F" Then
            mstrTitulo = "Facturas Contado"
        Else
            mstrTitulo = "Deuda Cta.Cte."
        End If
        mstrSoloVencidas = Request("venc")
        If mdsDatos Is Nothing Then
            grdConsulta.Columns(Columnas.Chk).Visible = False
            grdConsulta.Columns(Columnas.Pagado).Visible = False
        End If
        '---------------------------------------------------------------
        Select Case mstrTipo
            Case "FP"
                mstrTitulo = "Facturas Contado Propia"
                VarTipoForm = mstrTipo
                mstrTipo = "F"
            Case "FT"
                mstrTitulo = "Facturas Contado Terceros"
                VarTipoForm = mstrTipo
                mstrTipo = "F"
            Case "DP"
                mstrTitulo = "Deuda Cta.Cte. Propia"
                VarTipoForm = mstrTipo
                mstrTipo = "D"
            Case "DT"
                mstrTitulo = "Deuda Cta.Cte. Terceros"
                VarTipoForm = mstrTipo
                mstrTipo = "D"
            Case "F"
                mstrTitulo = "Facturas Contado"
                VarTipoForm = mstrTipo
            Case Else

        End Select
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConsulta.CurrentPageIndex = E.NewPageIndex

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Dim lstrCmd As New StringBuilder
        Dim ds As DataSet
        Dim ldsDatos As DataSet
        'If VarTipoForm = "DT" Then Exit Sub

        If VarTipoForm = "FP" Or VarTipoForm = "FT" Or VarTipoForm = "DP" Then          '12/2010    Facturas Propias, Terceros y CC Propia
            lstrCmd.Append("exec cobranzas_deuda_consul_cliente")
        Else
            lstrCmd.Append("exec cobranzas_deuda_consul")
        End If
        lstrCmd.Append(" @clie_id=")
        lstrCmd.Append(usrClie.Valor)
        lstrCmd.Append(", @fecha=")
        lstrCmd.Append(clsSQLServer.gFormatArg(Request("fecha"), SqlDbType.SmallDateTime))
        lstrCmd.Append(", @tipo=")
        lstrCmd.Append(clsSQLServer.gFormatArg(mstrTipo, SqlDbType.VarChar))
        lstrCmd.Append(", @SoloVencidas=")
        lstrCmd.Append(clsSQLServer.gFormatArg(mstrSoloVencidas, SqlDbType.VarChar))
        lstrCmd.Append(", @emct_id=")
        lstrCmd.Append(SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request, True))
        Select Case VarTipoForm
            Case Is = "FP"
                lstrCmd.Append(",@origen='P'")
            Case Is = "FT"
                lstrCmd.Append(",@origen='T'")
            Case Is = "DP"
                lstrCmd.Append(",@origen='P'")
        End Select
        ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)


        grdConsulta.DataSource = ds
        grdConsulta.DataBind()
        ds.Dispose()
    End Sub
#End Region

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            mCerrar()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub mCerrar()
        If lblMens.Text = "" And (hdnConf.Text = "-1" Or hdnConf.Text = "") Then 'por si dio error el guardardatos o si falta confirmarle
            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            If Not (mdsDatos Is Nothing) Then
                lsbMsg.Append("window.opener.document.all['hdnDatosPop'].value='-1';")
                lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
            End If
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)
        End If
    End Sub
    Private Sub mCalcularTotales()
        Dim dTotal As Decimal
        Dim dTotalVenc As Decimal
        Dim dTotalSel As Decimal
        Dim lDr As DataRow

        For Each lDr In DirectCast(grdConsulta.DataSource, DataSet).Tables(0).Rows
            dTotal += lDr.Item("comp_neto")
        Next
        If Not (mdsDatos Is Nothing) Then
            For Each lDr In mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id <> 41")
                dTotalSel += lDr.Item("ctac_impor")
            Next
        End If
        txtTotal.Valor = dTotal
        txtTotalSel.Valor = dTotalSel
    End Sub
    Private Sub grdConsulta_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdConsulta.ItemDataBound
        Try
            If e.Item.ItemIndex <> -1 And Not (mdsDatos Is Nothing) Then
                Dim lstrFiltro As String
                Dim ldrRow As DataRow
                Dim lchkSel As CheckBox = e.Item.FindControl("chkSel")
                Dim ltxtPagado As NixorControls.NumberBox = e.Item.FindControl("txtPagado")

                ldrRow = CType(e.Item.DataItem, DataRowView).Row

                lstrFiltro = "ctac_comp_id=" & ldrRow.Item("comp_id")
                lstrFiltro += " AND ctac_covt_id = " & ldrRow.Item("covt_id")

                If ldrRow.Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.SolicitudIngreso Then
                    ltxtPagado.ReadOnly = True
                End If

                With mdsDatos.Tables(mstrTablaCuentasCtes).Select(lstrFiltro)
                    If .GetUpperBound(0) = -1 Then
                        lchkSel.Checked = False
                        ltxtPagado.Valor = 0
                        ltxtPagado.Enabled = False
                        ltxtPagado.CssClass = "cuadrotextodeshab"
                    Else
                        With DirectCast(.GetValue(0), DataRow)
                            'Dario 2013-08-30 cargo la variable para determina si permite o no pago parcial
                            Dim bitPermitePagoParciales As Boolean = e.Item.DataItem.row(Columnas.PermitePagoParciales)
                            lchkSel.Checked = True
                            'ltxtPagado.Valor = CDec(.Item("ctac_impor")).ToString("######0.00")
                            ' Dario 2013-08-30 Verifico que tiene o no que habilitar el control pagado
                            ltxtPagado.Enabled = IIf(bitPermitePagoParciales = True And mstrTipo <> "F", True, False)
                            ltxtPagado.CssClass = IIf(ltxtPagado.Enabled, "cuadrotexto", "cuadrotextodeshab")
                            ' Dario 2013-08-26 para que tome todos los importes que se cancelaron
                            Dim tmpimporte As Decimal
                            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select(lstrFiltro)
                                tmpimporte += lDr.Item("ctac_impor")
                            Next
                            ltxtPagado.Valor = tmpimporte.ToString("######0.00")
                        End With
                        'If .GetUpperBound(0) > 1 Then 'pregunto que tanga 2 registros adicionales (el inter�s y el pago del inter�s)
                        '    ltxtPagado.Valor += DirectCast(.GetValue(2), DataRow).Item("ctac_impor")
                        'End If
                    End If
                End With

            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosGrilla()
        Try
            Dim ldrDatos As DataRow
            Dim ldrDatosInte As DataRow
            Dim ldrDatosIntePago As DataRow
            ' Dario 2013-07-22
            Dim ldrSobreTasasAlVto As DataRow
            ' fin Dario 2013-07-22
            Dim lbooSelec As Boolean
            Dim ltxtPagado As NixorControls.NumberBox
            Dim lstrFiltro As String
            Dim ldecPagado, ldecInteres, ldecSobreTasasAlVto, ldecNeto As Decimal

            'ACA
            If VarTipoForm = "DT" Then

            End If
                If hdnConf.Text.Trim() <> "00" Then  'si no cancel� el cambio
                    For Each oItem As DataGridItem In grdConsulta.Items
                        lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked

                        lstrFiltro = "ctac_comp_id=" & oItem.Cells(Columnas.CompId).Text
                        lstrFiltro += " AND ctac_covt_id = " & oItem.Cells(Columnas.CovtId).Text

                        With mdsDatos.Tables(mstrTablaCuentasCtes).Select(lstrFiltro, "_comp_coti_id")
                            If .GetUpperBound(0) <> -1 Then
                                ldrDatos = .GetValue(0)
                                If .GetUpperBound(0) > 0 Then
                                    ldrDatosInte = .GetValue(1)
                                    If .GetUpperBound(0) > 1 Then
                                        ldrDatosInte = .GetValue(2)   'porque est� ordenado por _comp_coti_id
                                        ldrDatosIntePago = .GetValue(1)
                                    End If
                                End If
                            End If
                        End With

                        If (oItem.Cells(5).Text.Trim() = "Solicitud de Ingreso") Then
                            ldecPagado = oItem.Cells(Columnas.Neto).Text
                        Else
                            ldecPagado = DirectCast(oItem.FindControl("txtPagado"), NixorControls.NumberBox).Valor
                        End If

                        ldecNeto = oItem.Cells(Columnas.Neto).Text
                        ldecInteres = oItem.Cells(Columnas.Interes).Text
                        ldecSobreTasasAlVto = oItem.Cells(Columnas.SobreTasasAlVto).Text ' Dario 2013-07-22

                        ' cargo la variable para determina si permite o no pago parcial
                        Dim bitPermitePagoParciales As Boolean = Convert.ToBoolean(oItem.Cells(Columnas.PermitePagoParciales).Text)

                        If lbooSelec And mstrTipo <> "F" And bitPermitePagoParciales And ldecPagado = 0 Then  'si no es contado (porque siempre viene 0 por disabled) y puso 0, deschequearlo
                            lbooSelec = False
                        End If

                        If lbooSelec Then
                            If ldecPagado = 0 And bitPermitePagoParciales = True Then
                                ldecPagado = ldecNeto
                            ElseIf ldecPagado = 0 And bitPermitePagoParciales = False Then
                                ldecPagado = ldecNeto + ldecInteres + ldecSobreTasasAlVto
                            End If

                            'comentado porque puede pagar parte de los intereses
                            'If ldecPagado > ldecNeto And ldecPagado <> (ldecNeto + ldecInteres) Then
                            '   Throw New AccesoBD.clsErrNeg("Debe pagar una fracci�n del valor de la cuota o la cuota m�s intereses.")
                            'End If

                            If ldecPagado > (ldecNeto + ldecInteres + ldecSobreTasasAlVto) Then ' Dario 2013-07-22 ldecSobreTasasAlVto
                                Throw New AccesoBD.clsErrNeg("El importe ingresado supera el importe a pagar.")
                            End If

                            If ldrDatos Is Nothing Then
                                'valido que si eligi� un cliente en la cobranza, que coincida
                                If mstrTipo = "F" And Request("clie_id") <> "" And hdnConf.Text <> "-1" Then
                                    If oItem.Cells(Columnas.ClieId).Text <> Request("clie_id") Then
                                        hdnConf.Text = "0"
                                        Return
                                    End If
                                End If

                                ldrDatos = mdsDatos.Tables(mstrTablaCuentasCtes).NewRow
                                With ldrDatos
                                    .Item("ctac_id") = clsSQLServer.gObtenerId(.Table, "ctac_id")
                                    .Item("ctac_canc_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                                    .Item("ctac_comp_id") = oItem.Cells(Columnas.CompId).Text
                                    .Item("ctac_covt_id") = oItem.Cells(Columnas.CovtId).Text
                                    .Item("_acti_ccos_id") = oItem.Cells(Columnas.CcosId).Text
                                    .Item("_comp_Id_Cancela") = oItem.Cells(Columnas.CompId).Text ' Dario 2013-07-22 
                                    .Item("_acti_Id_Cancela") = oItem.Cells(Columnas.CompActiId).Text ' Dario 2013-07-23 

                                    If mstrTipo = "F" Then
                                        .Item("_comp_cpti_id") = SRA_Neg.Constantes.ComprobPagosTipos.Contado
                                    Else
                                        .Item("_comp_cpti_id") = SRA_Neg.Constantes.ComprobPagosTipos.CtaCte
                                    End If

                                    .Item("_desc") = oItem.Cells(Columnas.Numero).Text
                                    .Item("_comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Factura

                                    .Table.Rows.Add(ldrDatos)
                                End With
                            End If

                            With ldrDatos
                                If (ldecPagado - ldecNeto) > 0 Then 'si tiene intereses o sobre tasas al vto grabo solo el neto Dario 2013-07-22
                                    .Item("ctac_impor") = ldecNeto
                                Else
                                    .Item("ctac_impor") = ldecPagado 'si no grabo solo lo que pago, que puede ser menos
                                End If
                            End With

                            ' Dario 2013-07-22 genera la linea en cta cte con los datos 
                            ' de la la sobretasa
                            If ldecSobreTasasAlVto > 0 Then
                                If ldrSobreTasasAlVto Is Nothing Then
                                    ldrSobreTasasAlVto = mdsDatos.Tables(mstrTablaCuentasCtes).NewRow
                                    With ldrSobreTasasAlVto
                                        .Item("ctac_id") = clsSQLServer.gObtenerId(.Table, "ctac_id")
                                        .Item("ctac_canc_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                                        .Item("ctac_comp_id") = oItem.Cells(Columnas.CompId).Text
                                        .Item("ctac_covt_id") = oItem.Cells(Columnas.CovtId).Text
                                        .Item("_acti_ccos_id") = oItem.Cells(Columnas.CcosId).Text
                                        .Item("_comp_Id_Cancela") = oItem.Cells(Columnas.CompId).Text ' Dario 2013-07-22 
                                        .Item("_acti_Id_Cancela") = oItem.Cells(Columnas.CompActiId).Text ' Dario 2013-07-23 
                                        '.Item("ctac_impor") = ldecInteres   'genera el interese sin importar si lo paga o no
                                        If ldecPagado > ldecNeto Then
                                            .Item("ctac_impor") = (ldecPagado - ldecNeto)
                                        Else
                                            .Item("ctac_impor") = 0
                                        End If
                                        .Item("_comp_impo") = ldecSobreTasasAlVto

                                        If mstrTipo = "F" Then
                                            .Item("_comp_cpti_id") = 1
                                        Else
                                            .Item("_comp_cpti_id") = 2
                                        End If

                                        .Item("_desc") = " " + oItem.Cells(Columnas.Numero).Text
                                        .Item("_comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.SobreTasasAlVto_CtaCte

                                        .Table.Rows.Add(ldrSobreTasasAlVto)
                                    End With
                                End If
                            Else ' ldecSobreTasasAlVto
                                If Not ldrSobreTasasAlVto Is Nothing Then ldrSobreTasasAlVto.Delete()
                            End If ' ldecSobreTasasAlVto

                            ''If ldecPagado >= ldecNeto And ldecInteres > 0 Then    'si pag� todo el neto (por lo menos) genera el inter�s, aunque no lo pague
                            If ldecInteres > 0 Then    'aunque no haya pagado todo si tiene interes lo genera
                                If ldrDatosInte Is Nothing Then
                                    ldrDatosInte = mdsDatos.Tables(mstrTablaCuentasCtes).NewRow
                                    With ldrDatosInte
                                        .Item("ctac_id") = clsSQLServer.gObtenerId(.Table, "ctac_id")
                                        .Item("ctac_canc_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                                        .Item("ctac_comp_id") = oItem.Cells(Columnas.CompId).Text
                                        .Item("ctac_covt_id") = oItem.Cells(Columnas.CovtId).Text
                                        .Item("_acti_ccos_id") = oItem.Cells(Columnas.CcosId).Text
                                        '.Item("ctac_impor") = ldecInteres   'genera el interese sin importar si lo paga o no
                                        If ldecPagado > ldecNeto Then
                                            .Item("ctac_impor") = (ldecPagado - ldecNeto)
                                        Else
                                            .Item("ctac_impor") = 0
                                        End If
                                        .Item("_comp_impo") = ldecInteres

                                        If mstrTipo = "F" Then
                                            .Item("_comp_cpti_id") = 1
                                        Else
                                            .Item("_comp_cpti_id") = 2
                                        End If

                                        .Item("_desc") = " " + oItem.Cells(Columnas.Numero).Text
                                        .Item("_comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Interes_CtaCte 'el inter�s generado (aunque no lo pague)

                                        .Table.Rows.Add(ldrDatosInte)
                                    End With
                                End If

                                ''If (ldecPagado - ldecNeto) > 0 Then    'si pag� los intereses (lo pagado supera el neto)
                                If ldecInteres > 0 Then    'generar nd siempre que tenga interes
                                    If ldrDatosIntePago Is Nothing Then
                                        ldrDatosIntePago = mdsDatos.Tables(mstrTablaCuentasCtes).NewRow
                                        With ldrDatosIntePago
                                            .Item("ctac_id") = clsSQLServer.gObtenerId(.Table, "ctac_id")
                                            .Item("ctac_canc_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                                            .Item("ctac_comp_id") = oItem.Cells(Columnas.CompId).Text
                                            .Item("ctac_covt_id") = oItem.Cells(Columnas.CovtId).Text
                                            .Item("_acti_ccos_id") = oItem.Cells(Columnas.CcosId).Text
                                            ''.Item("ctac_impor") = (ldecPagado - ldecNeto)
                                            .Item("ctac_impor") = ldecInteres
                                            If ldecPagado > ldecNeto Then
                                                .Item("ctac_impor") = (ldecPagado - ldecNeto)
                                            Else
                                                .Item("ctac_impor") = 0
                                            End If
                                            .Item("_comp_impo") = ldecInteres

                                            If mstrTipo = "F" Then
                                                .Item("_comp_cpti_id") = 1
                                            Else
                                                .Item("_comp_cpti_id") = 2
                                            End If

                                            .Item("_desc") = " " + oItem.Cells(Columnas.Numero).Text
                                            .Item("_comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.ND    'la aplicaci�n de la ND

                                            .Table.Rows.Add(ldrDatosIntePago)
                                        End With
                                    End If
                                Else
                                    If Not ldrDatosIntePago Is Nothing Then ldrDatosIntePago.Delete()
                                End If
                            Else ' ldecInteres
                                If Not ldrDatosInte Is Nothing Then ldrDatosInte.Delete()
                                If Not ldrDatosIntePago Is Nothing Then ldrDatosIntePago.Delete()
                            End If ' ldecInteres

                        Else ' lbooSelec
                            If Not ldrDatos Is Nothing Then
                                ldrDatos.Delete()
                                If Not ldrDatosIntePago Is Nothing Then ldrDatosIntePago.Delete()
                                If Not ldrDatosInte Is Nothing Then ldrDatosInte.Delete()
                            End If
                        End If ' lbooSelec
                        ldrDatos = Nothing
                        ldrDatosInte = Nothing
                        ldrDatosIntePago = Nothing
                        ldrSobreTasasAlVto = Nothing
                    Next
                End If

            Catch ex As Exception
            mHabilitarTextosGrilla()
            Throw ex
        End Try
    End Sub

    Private Sub mHabilitarTextosGrilla()
        For Each oItem As DataGridItem In grdConsulta.Items
            With DirectCast(oItem.FindControl("txtPagado"), NixorControls.NumberBox)
                Dim bitPermitePagoParciales As Boolean = Convert.ToBoolean(oItem.Cells(Columnas.PermitePagoParciales).Text)
                .Enabled = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked() And mstrTipo <> "F" And bitPermitePagoParciales
                .CssClass = IIf(.Enabled, "cuadrotexto", "cuadrotextodeshab")
            End With
        Next
    End Sub

    Private Sub usrClie_Cambio(ByVal sender As Object) Handles usrClie.Cambio
        Try
            mConsultar()
            mCalcularTotales()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub hdnConf_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnConf.TextChanged
        Try
            If hdnConf.Text = "-1" Then   'ya pas� por el guardardatosgrilla
                mCerrar()
            Else
                hdnConf.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    'Roxi: Nueva funcion mAvisarMorosidad 04/12/2009
    'Si el cliente es socio con estado suspendido moroso muestra un warning.
    Private Sub mAvisarMorosidad()
        Dim lstrCmd As String
        Dim ldsDatos As DataSet

        If Request("clie_id") = "" Or Request("cobra") = "" Then
            Return
        End If

        lstrCmd = "exec socios_busq @soci_clie_id=" & Request("clie_id")
        ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

        Dim lstrText As String = ""
        For Each ldrDatos As DataRow In ldsDatos.Tables(0).Rows
            If ldrDatos.Item("soci_esta_id") = 19 Then
                lstrText = "El socio " & ldrDatos.Item("soci_nume").ToString
                lstrText = lstrText & " - " & ldrDatos.Item("_clie_apel").ToString
                lstrText = lstrText & " tiene estado: Suspendido Moroso."
            End If
        Next

        If lstrText <> "" Then
            clsError.gGenerarMensajes(Me, lstrText)
        End If
    End Sub

    Private Sub mSetearDA()
        Dim lstrCmd As String
        'Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Clientes, usrClie.Valor.ToString)

        lstrCmd = "exec alumnos_debito_consul"
        lstrCmd += " " + usrClie.Valor.ToString

        Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

        If ldsDatos.Tables(0).Rows.Count > 0 Then
            With ldsDatos.Tables(0).Rows(0)
                If .Item("_tacl_id").ToString <> "" Then
                    Dim boolCargaCBU = False
                    ' controlo que fue seleccionad una tarjeta, para realizar la consulta para
                    ' ver si carga CBU o nro de cuenta
                    If (Not .Item("_tacl_tarj_id") Is DBNull.Value) Then
                        boolCargaCBU = clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_consul @tarj_id=" & .Item("_tacl_tarj_id").ToString, "tarj_PermiteDebitoEnCuenta")
                    End If

                    Dim lstrText As New System.Text.StringBuilder
                    lstrText.Append("El Cliente tiene D�bito Autom�tico para ")
                    lstrText.Append(.Item("_inse_desc").ToString)
                    lstrText.Append(" con la Tarjeta: ")
                    lstrText.Append(.Item("_tacl_tarj").ToString)
                    ' Dario 2013-10-02
                    'lstrText.Append("  Nro.: ")
                    Dim texto = IIf(boolCargaCBU, "  CBU.: ", "  Nro.: ")
                    lstrText.Append(texto)
                    ' fin 2013-10-02
                    lstrText.Append(.Item("_tacl_nume").ToString)
                    If Not .IsNull("_tacl_vcto_fecha") Then
                        lstrText.Append(" Fecha Vto.: ")
                        lstrText.Append(CDate(.Item("_tacl_vcto_fecha")).ToString("dd/MM/yyyy"))
                    End If
                    lstrText.Append("   ")

                    clsError.gGenerarMensajes(Me, lstrText.ToString)
                End If
            End With
        End If
    End Sub
End Class

End Namespace
