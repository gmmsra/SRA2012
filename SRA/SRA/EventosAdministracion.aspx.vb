Imports System.Data.SqlClient


Namespace SRA


Partial Class EventosAdministracion

    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents grdMate As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAltaMail As System.Web.UI.WebControls.Button
    Protected WithEvents btnBajaMail As System.Web.UI.WebControls.Button
    Protected WithEvents btnModiMail As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimpMail As System.Web.UI.WebControls.Button
    Protected WithEvents grdMail As System.Web.UI.WebControls.DataGrid
    Protected WithEvents hdnMealId As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNume1 As NixorControls.TextBoxTab

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region


#Region "Definici�n de Variables"
    Private mstrTabla As String = "eventos"
    Private mstrTablaCate As String = "eventos_catego"
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mdsDatos As DataSet
    Private mstrConn As String

    Private Enum Columnas As Integer
        IdEven = 1
    End Enum

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mCargarComboPerfil()
                mCargarComboLugarUno()
                mCargarComboLugarDos()
                mCargarComboEntradaUno()
                mCargarComboEntradaDos()
                mCargarComboEntradaTres()
                mConsultar(grdDato, False)
                tabLinks.Visible = False
                panBotones.Visible = False

                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")

        btnBajaDeta.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mCargarComboPerfil()
        clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S")
    End Sub

    Private Sub mCargarComboLugarUno()
        clsWeb.gCargarRefeCmb(mstrConn, "eventos_lugares", cmbLugarUno, "S")
    End Sub

    Private Sub mCargarComboLugarDos()
        clsWeb.gCargarRefeCmb(mstrConn, "eventos_lugares", cmbLugarDos, "S")
    End Sub

    Private Sub mCargarComboEntradaUno()
        clsWeb.gCargarRefeCmb(mstrConn, "eventos_entradas", cmbEntradaUno, "S")
    End Sub

    Private Sub mCargarComboEntradaDos()
        clsWeb.gCargarRefeCmb(mstrConn, "eventos_entradas", cmbEntradaDos, "S")
    End Sub

    Private Sub mCargarComboEntradaTres()
        clsWeb.gCargarRefeCmb(mstrConn, "eventos_entradas", cmbEntradaTres, "S")
    End Sub

    Private Sub mEstablecerPerfil()
            'Dim lbooPermiAlta As Boolean
            'Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Usuarios, String), (mstrConn), (Session("sUserId").ToString()))) Then
        'Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "even_desc")
        txtObser.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "even_obse")

    End Sub


#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
    End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultar(grdDato, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


#End Region

#Region "Seteo de Controles"

    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla

            Case mstrTablaCate
                btnBajaDeta.Enabled = Not (pbooAlta)
                btnModiDeta.Enabled = Not (pbooAlta)
                btnAltaDeta.Enabled = pbooAlta
            Case Else
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnAlta.Enabled = pbooAlta
        End Select
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

        mLimpiar()
        mLimpiarCategoria()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.IdEven).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtDesc.Valor = .Item("even_desc")
                cmbLugarUno.Valor = .Item("even_evlu1_id")
                cmbLugarDos.Valor = .Item("even_evlu2_id")
                txtIniFecha.Fecha = .Item("even_inic_fecha")
                txtFinFecha.Fecha = .Item("even_fina_fecha")
                txtObser.Valor = .Item("even_obse")
                ChkNume1.Checked = clsWeb.gFormatCheck(.Item("even_cont1_nume"), "True")
                ChkNume2.Checked = clsWeb.gFormatCheck(.Item("even_cont2_nume"), "True")
                ChkNume3.Checked = clsWeb.gFormatCheck(.Item("even_cont3_nume"), "True")
                    cmbEntradaUno.Valor = .Item("even_evtr1_id").ToString
                    cmbEntradaDos.Valor = .Item("even_evtr2_id").ToString

                    cmbEntradaTres.Valor = .Item("even_evtr3_id").ToString

                If Not .IsNull("even_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("even_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                        lblBaja.Text = String.Empty
                End If
            End With
            mSetearEditor("", False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()

        hdnId.Text = ""
        lblTitu.Text = ""
        mCrearDataSet("")
        lblBaja.Text = ""
        txtDesc.Text = ""
        cmbLugarUno.Limpiar()
        cmbLugarDos.Limpiar()
        txtObser.Text = ""
        cmbEntradaUno.Limpiar()
        cmbEntradaDos.Limpiar()
        cmbEntradaTres.Limpiar()
        txtFinFecha.Text = ""
        txtIniFecha.Text = ""
        ChkNume1.Checked = False
        ChkNume2.Checked = False
        ChkNume3.Checked = False

        mSetearEditor("", True)
        mLimpiarCategoria()
        grdCate.CurrentPageIndex = 0

    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        grdDato.Visible = Not panDato.Visible
        btnAgre.Visible = Not (panDato.Visible)


        lnkCabecera.Font.Bold = True
        lnkDeta.Font.Bold = False

        panCabecera.Visible = True
        panDeta.Visible = False

        tabLinks.Visible = pbooVisi

        lnkCabecera.Font.Bold = True
        lnkDeta.Font.Bold = False
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panCabecera.Visible = False
        panDeta.Visible = False

        lnkCabecera.Font.Bold = False
        lnkDeta.Font.Bold = False

        Dim val As String

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
            Case 2
                panDeta.Visible = True
                lnkDeta.Font.Bold = True
                lblTitu.Text = "Evento: " & txtDesc.Text
                If (cmbEntradaUno.SelectedItem.Text <> "(Seleccione)") Then
                    lblEntra1.Text = "(" & cmbEntradaUno.SelectedItem.Text & ")"
                    grdCate.Columns(3).HeaderText = "Cantidad (" & cmbEntradaUno.SelectedItem.Text & ")"
                Else
                    lblEntra1.Text = ""
                    grdCate.Columns(3).HeaderText = "Cantidad"
                End If

                If (cmbEntradaDos.SelectedItem.Text <> "(Seleccione)") Then
                    lblEntra2.Text = "(" & cmbEntradaDos.SelectedItem.Text & ")"
                    grdCate.Columns(4).HeaderText = "Cantidad (" & cmbEntradaDos.SelectedItem.Text & ")"
                Else
                    lblEntra2.Text = ""
                    grdCate.Columns(4).HeaderText = "Cantidad"
                End If

                If (cmbEntradaTres.SelectedItem.Text <> "(Seleccione)") Then
                    lblEntra3.Text = "(" & cmbEntradaTres.SelectedItem.Text & ")"
                    grdCate.Columns(5).HeaderText = "Cantidad (" & cmbEntradaTres.SelectedItem.Text & ")"
                Else
                    lblEntra3.Text = ""
                    grdCate.Columns(5).HeaderText = "Cantidad"
                End If
                txtCant1.Enabled = (lblEntra1.Text <> "")
                txtCant2.Enabled = (lblEntra2.Text <> "")
                txtCant3.Enabled = (lblEntra3.Text <> "")
                grdCate.Visible = True
                btnBajaDeta.Enabled = False
                btnModiDeta.Enabled = False
                btnAltaDeta.Enabled = True
        End Select
    End Sub

    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub


#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjEvento As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjEvento.Alta()
            mConsultar(grdDato, True)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try

            mGuardarDatos()
            Dim lobjEvento As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjEvento.Modi()

            mConsultar(grdDato, True)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar(grdDato, True)

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage

            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            If (txtIniFecha.Text = "") Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de inicio del evento.")
            End If
            If (txtFinFecha.Text = "") Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de finalizaci�n del evento.")
            End If
            If txtDesc.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar una descripci�n.")
            End If
            If (cmbLugarUno.SelectedItem.Text = "(Seleccione)") Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el Lugar 1 del evento.")
            End If
            If (cmbEntradaUno.SelectedItem.Text = "(Seleccione)") Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la Entrada 1 del evento.")
            End If
        End With

    End Sub

    Private Function mGuardarDatos() As DataSet

        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("even_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("even_inic_fecha") = txtIniFecha.Fecha
            .Item("even_fina_fecha") = txtFinFecha.Fecha
            .Item("even_desc") = txtDesc.Valor
            .Item("even_evlu1_id") = cmbLugarUno.Valor
            'Si el valor es "Seleccione", pongo null.
            If (cmbLugarDos.SelectedItem.Text <> "(Seleccione)") Then .Item("even_evlu2_id") = cmbLugarDos.Valor Else .Item("even_evlu2_id") = DBNull.Value
            .Item("even_obse") = txtObser.Valor
            .Item("even_cont1_nume") = ChkNume1.Checked
            .Item("even_cont2_nume") = ChkNume2.Checked
            .Item("even_cont3_nume") = ChkNume3.Checked
            .Item("even_evtr1_id") = cmbEntradaUno.Valor
            'Si el valor es "Seleccione", pongo null.
            If (cmbEntradaDos.SelectedItem.Text <> "(Seleccione)") Then .Item("even_evtr2_id") = cmbEntradaDos.Valor Else .Item("even_evtr2_id") = DBNull.Value
            If (cmbEntradaTres.SelectedItem.Text <> "(Seleccione)") Then .Item("even_evtr3_id") = cmbEntradaTres.Valor Else .Item("even_evtr3_id") = DBNull.Value
        End With

        Return mdsDatos
    End Function


    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaCate

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdCate.DataSource = mdsDatos.Tables(mstrTablaCate)
        grdCate.DataBind()

        Session(mstrTabla) = mdsDatos

    End Sub


#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

#End Region

#Region "Detalle"
    Public Sub mEditarDatosCate(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrDeta As DataRow
            hdnCateId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaCate).Select("evca_id=" & hdnCateId.Text)(0)

            With ldrDeta
                cmbCate.Valor = .Item("evca_cate_id")
                txtCant1.Valor = .Item("evca_ent1_cant")
                    txtCant2.Valor = .Item("evca_ent2_cant")

                    txtCant3.Valor = IIf(.Item("evca_ent3_cant").ToString.Length > 0, .Item("evca_ent3_cant").ToString, 0)

            End With

            mSetearEditor(mstrTablaCate, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal pOk As Boolean)
        Try

            mstrCmd = "exec " + mstrTabla + "_consul "
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarCategoria()

        Dim ldrDeta As DataRow

        If hdnCateId.Text = "" Then
            ldrDeta = mdsDatos.Tables(mstrTablaCate).NewRow
            ldrDeta.Item("evca_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaCate), "evca_id")
        Else
            ldrDeta = mdsDatos.Tables(mstrTablaCate).Select("evca_id=" & hdnCateId.Text)(0)
        End If

        If (cmbCate.SelectedItem.Text = "(Seleccione)") Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la categor�a.")
        End If

        With ldrDeta
            .Item("evca_cate_id") = cmbCate.Valor
            .Item("_categoria") = cmbCate.SelectedItem.Text
            .Item("evca_ent1_cant") = txtCant1.Valor
                .Item("evca_ent2_cant") = txtCant2.Valor

                .Item("evca_ent3_cant") = IIf(txtCant3.Valor.ToString.Length() > 0, txtCant3.Valor.ToString, 0)

        End With

        If (mEstaEnElDataSet(mdsDatos.Tables(mstrTablaCate), ldrDeta, "evca_cate_id", "evca_id")) Then
            Throw New AccesoBD.clsErrNeg("La categor�a indicada ya se encuentra asociada al evento.")
        Else
            If (hdnCateId.Text = "") Then
                mdsDatos.Tables(mstrTablaCate).Rows.Add(ldrDeta)
            End If
        End If
    End Sub



    Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrDeta As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
        Dim filtro As String
        filtro = campo1 + " = " + ldrDeta.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrDeta.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrDeta.Item(campoId).ToString()
        Return (table.Select(filtro).GetLength(0) > 0)
    End Function

    Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCate As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
        Return mEstaEnElDataSet(table, ldrCate, campo1, campo1, campoId)
    End Function

    Private Sub mLimpiarCategoria()
        hdnCateId.Text = ""
        cmbCate.Limpiar()
        txtCant1.Valor = ""
        txtCant2.Valor = ""
        txtCant3.Valor = ""
        mSetearEditor(mstrTablaCate, True)
    End Sub

#End Region

    Public Sub grdCate_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdCate.EditItemIndex = -1
            If (grdCate.CurrentPageIndex < 0 Or grdCate.CurrentPageIndex >= grdCate.PageCount) Then
                grdCate.CurrentPageIndex = 0
            Else
                grdCate.CurrentPageIndex = E.NewPageIndex
            End If
            grdCate.DataSource = mdsDatos.Tables(mstrTablaCate)
            grdCate.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub ActualizarCategoria()
        Try
            mGuardarCategoria()

            mLimpiarCategoria()
            grdCate.DataSource = mdsDatos.Tables(mstrTablaCate)
            grdCate.DataBind()


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub lnkDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
        mShowTabs(2)
    End Sub

    Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        Try
            mdsDatos.Tables(mstrTablaCate).Select("evca_id=" & hdnCateId.Text)(0).Delete()
            grdCate.DataSource = mdsDatos.Tables(mstrTablaCate)
            grdCate.DataBind()
            mLimpiarCategoria()


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

    Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
        ActualizarCategoria()
    End Sub

    Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        ActualizarCategoria()
    End Sub

    Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        mLimpiarCategoria()
    End Sub

End Class
End Namespace
