Imports System.Data.SqlClient


Namespace SRA


Partial Class ClientesEntidades
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lblAlum As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ClientesEntidades

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String

   Private Enum Columnas As Integer
      ClieAtriId = 1
      Cliente = 2
      Socio = 3
      NomCliente = 4
      Entidad = 5
      EntiNume = 6
      Estado = 7
   End Enum

#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then

            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            'mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)

         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEntiFil, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEnti, "S")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnAlta.Attributes.Add("onclick", "return mConfirmar();")
      btnModi.Attributes.Add("onclick", "return mConfirmar();")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.ClieAtriId).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            cmbEnti.Valor = .Item("clen_enti_id")
            usrClie.Valor = .Item("clen_clie_id")
            txtNume.Valor = .Item("clen_nume")
            If Not .IsNull("clen_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("clen_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If
         End With
         mSetearEditor("", False)
         mMostrarPanel(True)
      End If
   End Sub

   Private Sub mLimpiarFiltros()
      cmbEntiFil.Valor = 0
      usrClieFil.Valor = 0
      grdDato.Visible = False
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      cmbEnti.Valor = cmbEntiFil.Valor
      usrClie.Valor = usrClieFil.Valor
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      mCrearDataSet("")
      lblBaja.Text = ""
      cmbEnti.Valor = cmbEntiFil.Valor
      usrClie.Valor = usrClieFil.Valor
      txtNume.Text = ""

      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mConsultarGrilla()
   End Sub

   Private Sub mConsultarGrilla()
      Try
         mConsultar()
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      btnAgre.Visible = Not (panDato.Visible)
      panCabecera.Visible = True
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos(True)
         Dim lobjClienEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjClienEnti.Alta()
         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mGuardarDatos(False)
         Dim lobjClienEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjClienEnti.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjClienEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjClienEnti.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidaDatos(ByVal pboolAlta As Boolean)

      With mdsDatos.Tables(0).Rows(0)
         If cmbEnti.SelectedItem.Text = "(Seleccione)" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una entidad.")
         End If

         If usrClie.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
         End If
      End With
   End Sub

   Private Function mGuardarDatos(ByVal pboolAlta As Boolean) As DataSet

      mValidaDatos(pboolAlta)

      With mdsDatos.Tables(0).Rows(0)
         .Item("clen_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("clen_enti_id") = cmbEnti.Valor
         .Item("clen_clie_id") = usrClie.Valor
         .Item("clen_nume") = txtNume.Valor

         If Not .IsNull("clen_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("clen_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If

         For i As Integer = 0 To .Table.Columns.Count - 1
            If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
               .Item(i) = DBNull.Value
            End If
         Next
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      Session(mstrTabla) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

#Region "Detalle"

   Public Sub mConsultar()
      Try

         mstrCmd = "exec " + mstrTabla + "_consul "
         mstrCmd = mstrCmd & " @clen_clie_id = " & usrClieFil.Valor
         mstrCmd = mstrCmd & ",@clen_enti_id = " & cmbEntiFil.Valor

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         If (usrClieFil.Valor = 0 Or cmbEntiFil.Valor = 0) Then
            grdDato.Columns(Columnas.Cliente).Visible = (usrClieFil.Valor = 0)
            grdDato.Columns(Columnas.NomCliente).Visible = (usrClieFil.Valor = 0)
            grdDato.Columns(Columnas.Socio).Visible = (usrClieFil.Valor = 0)
            grdDato.Columns(Columnas.Entidad).Visible = (cmbEntiFil.Valor = 0)
         End If

         If grdDato.Items.Count > 0 Then
            grdDato.Columns(Columnas.EntiNume).Visible = (cmbEntiFil.Valor <> 0 And grdDato.Items(0).Cells(Columnas.EntiNume).Text = "True")
         End If

         grdDato.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEditorMail(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

#End Region

Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         If usrClieFil.Valor = 0 And cmbEntiFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar un cliente o una entidad.")
         Else
            grdDato.CurrentPageIndex = 0
            mConsultar()
            mMostrarPanel(False)
            btnAgre.Visible = True
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
End Sub

Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
    Try
        Dim lstrRptName As String = "ClientesEntidades"
        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
        lstrRpt += "&enti_id=" + cmbEntiFil.Valor.ToString

        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)

    Catch ex As Exception
        clsError.gManejarError(Me, ex)
    End Try
End Sub

Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
    mAgregar()
End Sub

Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
    mLimpiarFiltros()
End Sub
End Class
End Namespace
