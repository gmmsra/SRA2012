' Dario 2013-10-03 se coloca filtro en la carga del combo "tarjetas", cmbTarj
Namespace SRA

Partial Class CobranzasPagos_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblCuot As System.Web.UI.WebControls.Label
    Protected WithEvents txtPlaza As NixorControls.NumberBox
    Protected WithEvents btnLimpBill As System.Web.UI.WebControls.Button




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaPagos As String = SRA_Neg.Constantes.gTab_ComprobPagos
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrTablaBilletes As String = SRA_Neg.Constantes.gTab_Billetes

    Private mstrConn As String
    Private mstrParaPageSize As Integer
    Private mdsDatos As DataSet
    Private mintPatiId As Integer
    Private mbooUnico As Boolean
    Private mbooTarjAutoriz As Boolean

    Private Enum Columnas As Integer
        id = 1
        Moneda = 2
        Importe = 3
        ImporteOri = 4
        Tarjeta = 5
        DineElec = 6
        TarjetaNro = 7
        Nume = 8
        TarjetaCuotas = 9
        ChequeFecha = 10
        ChequeTipo = 11
        ChequeClearing = 12
        ChequePlaza = 13
        ChequeBanco = 14
        ChequeNume = 15
        Cuenta = 16
        DepoFecha = 18
    End Enum
#End Region

#Region "Inicializaci�n de Variables"
    Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        mintPatiId = Request("PatiId")
        mbooUnico = (Request("nom") = "S")

        lblCabeTotalAplic.Text = Request("totalAPagar")
        lblCabeTotalPago.Text = Request("totalPagado")
        lblCabeTotalDifer.Text = Request("diferencia")
        lblCoti.Text = Request("cot")

    End Sub
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mdsDatos = Session(Request("sess"))

            mInicializar()
            mInicializarPati()

            If (Not Page.IsPostBack) Then
                mSetearEventos()
                mSetearMaxLength()
                mCargarCombos()
                mLimpiar()
                mConsultar()

                mInicializarDatosFact()
            End If


            mSetearMoneda()
            mSetearTarjeta()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnAlta.Attributes.Add("onclick", "return mChequeaFechaCheque();")

        btnAltaBill.Attributes.Add("onclick", "return mChequearBillete();")
        btnModiBill.Attributes.Add("onclick", "return mChequearBillete();")

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.Efectivo Then
            cmbMone.Attributes.Add("onchange", "cmbMone_change(this);")
        End If

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.DineroElec Then
            cmbMone.Attributes.Add("onchange", "mCargarDine(this);")
        End If
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaPagos)
        txtImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "paco_impo")
        txtTarjNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "paco_tarj_nume")
        txtTarjCuot.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "paco_tarj_cuot")
        txtTarjAutori.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "paco_tarj_autori")
        txtCheqNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cheq_nume")
        txtOrigBancSuc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "paco_orig_banc_suc")
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.Tarjeta Then
            ' Dario 2013-10-03 se coloca filtro en la carga del combo "tarjetas", cmbTarj
            'clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S", "@varTodos=N")
        End If

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.DineroElec Then
            mCargarComboDine()
        End If

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.AcredBancaria Then
            clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "S", "@con_cuentas=1")
            clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbOrigBanc, "S")
        End If

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.Cheque Then
            clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "clearings", cmbClea, "")
            clsWeb.gCargarRefeCmb(mstrConn, "cheques_tipos", cmbChti, "")

            cmbPlaza.Items.Add("Local")
            cmbPlaza.Items(cmbPlaza.Items.Count - 1).Value = ""
            cmbPlaza.Items.Add("Extranjera")
            cmbPlaza.Items(cmbPlaza.Items.Count - 1).Value = "0"
        End If

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.AcredBancaria Then
            mCargarCombosCuentas()
        End If

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.Retenciones Then
            clsWeb.gCargarRefeCmb(mstrConn, "retenciones_tipos", cmbRtip, "")
        End If
    End Sub

    Private Sub mCargarCombosCuentas()
        Dim lstrFiltro As String = "@banc_id=" & clsSQLServer.gFormatArg(cmbBanc.Valor.ToString, SqlDbType.Int)
        lstrFiltro += ", @mone_id=" + cmbMone.Valor.ToString

        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuba, "S", lstrFiltro)
    End Sub

    Private Sub mCargarComboDine()
        clsWeb.gCargarRefeCmb(mstrConn, "dinero_elec", cmbTarj, "S", "@mone_id=" & cmbMone.Valor.ToString)
    End Sub

    Private Sub mInicializarPati()
        trTarj.Visible = False
        TrNume.Visible = False

        trTarjNume.Visible = False
        TrTarjCuot.Visible = False
        TrChti.Visible = False
        TrTeorDepoFecha.Visible = False
        TrPlaza.Visible = False
        TrClea.Visible = False
        TrBanc.Visible = False
        TrOriBanc.Visible = False
        TrFechaAcreDepo.Visible = False
        TrCheqNume.Visible = False
        TrCuba.Visible = False
        TrRtip.Visible = False

        Select Case mintPatiId
            Case SRA_Neg.Constantes.PagosTipos.Efectivo

            Case SRA_Neg.Constantes.PagosTipos.Cheque
                divBilletes.Visible = False
                TrTeorDepoFecha.Visible = True
                TrChti.Visible = True
                TrPlaza.Visible = True
                TrClea.Visible = True
                TrBanc.Visible = True
                TrCheqNume.Visible = True

                grdDato.Columns(Columnas.ChequeFecha).Visible = True
                grdDato.Columns(Columnas.ChequeTipo).Visible = True
                grdDato.Columns(Columnas.ChequeClearing).Visible = True
                grdDato.Columns(Columnas.ChequePlaza).Visible = True
                grdDato.Columns(Columnas.ChequeBanco).Visible = True
                grdDato.Columns(Columnas.ChequeNume).Visible = True

            Case SRA_Neg.Constantes.PagosTipos.Tarjeta
                divBilletes.Visible = False
                trTarj.Visible = True
                TrNume.Visible = True

                trTarjNume.Visible = True
                TrTarjCuot.Visible = True

                grdDato.Columns(Columnas.Tarjeta).Visible = True
                grdDato.Columns(Columnas.TarjetaNro).Visible = True
                grdDato.Columns(Columnas.Nume).Visible = True
                grdDato.Columns(Columnas.TarjetaCuotas).Visible = True

                lblNume.Text = "Nro. Cup�n"     'agregado pantanetti Guillermo 20/06/2008

            Case SRA_Neg.Constantes.PagosTipos.DineroElec
                divBilletes.Visible = False
                trTarj.Visible = True
                TrNume.Visible = True

                grdDato.Columns(Columnas.DineElec).Visible = True
                grdDato.Columns(Columnas.Nume).Visible = True

                lblTarj.Text = "Din.Elec."
                grdDato.Columns(Columnas.Nume).HeaderText = "Nro.Cargo"
                lblNume.Text = "Nro.Cargo"

            Case SRA_Neg.Constantes.PagosTipos.AcredBancaria
                divBilletes.Visible = False
                TrNume.Visible = True
                TrBanc.Visible = True
                TrCuba.Visible = True
                TrOriBanc.Visible = True
                TrFechaAcreDepo.Visible = True

                grdDato.Columns(Columnas.Nume).Visible = True
                grdDato.Columns(Columnas.ChequeBanco).Visible = True
                grdDato.Columns(Columnas.Cuenta).Visible = True
                grdDato.Columns(Columnas.DepoFecha).Visible = True

                grdDato.Columns(Columnas.Nume).HeaderText = "Nro.Operaci�n"
                lblNume.Text = "Nro.Operaci�n"

            Case SRA_Neg.Constantes.PagosTipos.Retenciones
                divBilletes.Visible = False
                TrMone.Visible = False
                TrNume.Visible = True
                TrRtip.Visible = True
                grdDato.Columns(Columnas.ImporteOri).Visible = False
                grdDato.Columns(Columnas.Nume).Visible = True
                grdDato.Columns(Columnas.Nume).HeaderText = "Nro.Comp."
                lblNume.Text = "Nro.Comp."
        End Select
    End Sub
    Private Sub mInicializarDatosFact()
        Dim ldrDatos As DataRow
        Dim lintCompId As Integer
        Dim ldecImpo As Decimal

        hdnFechaDia.Text = Today.ToString("dd/MM/yyyy")

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.Tarjeta Then
            If mdsDatos.Tables(SRA_Neg.Constantes.gTab_CuentasCtes).Select.GetUpperBound(0) <> -1 Then
                With mdsDatos.Tables(SRA_Neg.Constantes.gTab_CuentasCtes).Select()(0)
                    lintCompId = .Item("ctac_comp_id")
                    ldecImpo = .Item("ctac_impor")
                End With
            End If
            If lintCompId <> 0 Then
                ' Dario 2013-08-30 se coloca control por si viene la tabla sin filas 
                ' error de cliente en ingreso 
                If (clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaDeta, "@code_comp_id=" & lintCompId.ToString).Tables(0).Rows.Count > 0) Then
                    ldrDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaDeta, "@code_comp_id=" & lintCompId.ToString).Tables(0).Rows(0)
                    With ldrDatos
                            txtImpo.Valor = ldecImpo
                            If (Not .Item("code_tarj_id") Is DBNull.Value) Then
                                cmbTarj.Valor = .Item("code_tarj_id")
                            End If
                            txtTarjCuot.Valor = IIf(.Item("code_tarj_cuot") Is DBNull.Value, "", .Item("code_tarj_cuot"))
                            txtTarjNume.Valor = IIf(.Item("code_tarj_nume") Is DBNull.Value, "", .Item("code_tarj_nume"))
                            hdnVtaTel.Text = Math.Abs(CInt(.Item("code_tarj_vtat")))
                    End With
                    mSetearTarjeta()
                End If
            End If
        End If
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTablaBilletes
                btnBajaBill.Enabled = Not (pbooAlta)
                btnModiBill.Enabled = Not (pbooAlta)
                btnAltaBill.Enabled = pbooAlta

            Case Else
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnAlta.Enabled = pbooAlta
        End Select
    End Sub

    Public Sub mCargarDatos(ByVal pstrId As String)
            With mdsDatos.Tables(mstrTablaPagos).Select("paco_id=" & pstrId)(0)
                hdnId.Text = .Item("paco_id").ToString()

                cmbMone.Valor = .Item("paco_mone_id")
                txtImpo.Valor = .Item("paco_orig_impo")

                Select Case mintPatiId
                    Case SRA_Neg.Constantes.PagosTipos.Efectivo
                        mSetearMoneda()

                    Case SRA_Neg.Constantes.PagosTipos.Tarjeta
                        txtNume.Valor = .Item("paco_nume")

                        cmbTarj.Valor = .Item("paco_tarj_id")
                        txtTarjCuot.Valor = .Item("paco_tarj_cuot")
                        txtTarjNume.Valor = .Item("paco_tarj_nume")
                        txtTarjAutori.Valor = .Item("paco_tarj_autori")

                        mSetearTarjeta()

                    Case SRA_Neg.Constantes.PagosTipos.Cheque
                        cmbBanc.Valor = .Item("cheq_banc_id")
                        txtCheqNume.Valor = .Item("cheq_nume")
                        If (Not .Item("cheq_teor_depo_fecha") Is DBNull.Value) Then
                            txtTeorDepoFecha.Fecha = .Item("cheq_teor_depo_fecha")
                        End If
                        If (Not .Item("cheq_plaza") Is DBNull.Value) Then
                            cmbPlaza.ValorBool = .Item("cheq_plaza")
                        End If
                        cmbClea.Valor = .Item("cheq_clea_id")
                        cmbChti.Valor = .Item("cheq_chti_id")

                    Case SRA_Neg.Constantes.PagosTipos.DineroElec
                        mCargarComboDine()
                        txtNume.Valor = .Item("paco_nume")
                        cmbTarj.Valor = .Item("paco_dine_id")

                    Case SRA_Neg.Constantes.PagosTipos.Retenciones
                        txtNume.Valor = .Item("paco_nume")
                        cmbRtip.Valor = .Item("paco_rtip_id")

                    Case SRA_Neg.Constantes.PagosTipos.AcredBancaria
                        txtNume.Valor = .Item("paco_nume")
                        cmbBanc.Valor = .Item("_cuba_banc_id")
                        mCargarCombosCuentas()
                        cmbCuba.Valor = .Item("paco_cuba_id")
                        cmbOrigBanc.Valor = .Item("paco_orig_banc_id")
                        txtOrigBancSuc.Valor = .Item("paco_orig_banc_suc")
                        txtFechaAcreDepo.Fecha = .Item("paco_acre_depo_fecha")
                End Select
                'mActualizarLabels("E", .Item("paco_impo"), pstrId)
            End With
            mSetearLabels()
        mSetearEditor("", False)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        lblTitu.Text = ""

        cmbMone.Limpiar()
        txtImpo.Text = ""
        cmbBanc.Limpiar()
        cmbOrigBanc.Limpiar()
        txtOrigBancSuc.Text = ""
        txtFechaAcreDepo.Text = ""
        txtNume.Text = ""

        cmbTarj.Limpiar()
        txtTarjCuot.Text = "1"
        txtTarjNume.Text = ""
        txtTarjAutori.Text = ""
            txtCheqNume.Text = ""
            ' dario 2022-10-25 1241 
            txtTeorDepoFecha.Fecha = IIf(mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_fecha") Is DBNull.Value, "", mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_fecha"))
            cmbPlaza.Limpiar()
        cmbClea.Limpiar()
        cmbChti.Limpiar()
        mCargarCombosCuentas()
        cmbCuba.Limpiar()
        cmbRtip.Limpiar()
        mSetearMoneda()
        mSetearTarjeta()
        hdnVtaTel.Text = "0"

        If mintPatiId = SRA_Neg.Constantes.PagosTipos.DineroElec Then
            mCargarComboDine()
        End If

        mSetearEditor("", True)
    End Sub

    Private Sub mCerrar()
        Dim lsbMsg As New StringBuilder
        lsbMsg.Append("<SCRIPT language='javascript'>")
        'lsbMsg.Append("window.opener.document.all['hdnDatosPop'].value='1';")
        'lsbMsg.Append("window.opener.document.all['hdnTipoPop'].value='P';")
        'lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mActualizar()
        Try
            mGuardarDatos()

            mConsultar()

            mLimpiar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            mdsDatos.Tables(mstrTablaPagos).Select("paco_id=" & hdnId.Text)(0).Delete()
            For Each ldrDatos As DataRow In mdsDatos.Tables(mstrTablaBilletes).Select("bill_paco_id=" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))
                ldrDatos.Delete()
            Next
            mConsultar()
            mSetearLabels()
            mLimpiar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatos()
        mValidarDatos()

        Dim ldrDatos As DataRow

        If mbooUnico Then
            If hdnId.Text = "" And mdsDatos.Tables(mstrTablaPagos).Select().Length = 1 Then
                Throw New AccesoBD.clsErrNeg("Solo puede ingresar un �nico pago.")
            End If
        End If

        If hdnId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaPagos).NewRow
            ldrDatos.Item("paco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaPagos), "paco_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaPagos).Select("paco_id=" & hdnId.Text)(0)
        End If

            With ldrDatos
                .Item("paco_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                .Item("paco_pati_id") = mintPatiId
                .Item("paco_mone_id") = cmbMone.Valor
                .Item("paco_orig_impo") = txtImpo.Valor

                If cmbMone.Valor = SRA_Neg.Comprobantes.gMonedaPesos(mstrConn) Then
                    .Item("paco_impo") = .Item("paco_orig_impo")
                ElseIf cmbMone.Valor = SRA_Neg.Comprobantes.gMonedaDolar(mstrConn) Then
                    .Item("paco_impo") = Math.Round(mdsDatos.Tables(mstrTablaDeta).Rows(0).Item("code_coti") * .Item("paco_orig_impo"), 2)
                Else
                    .Item("paco_impo") = Math.Round(SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_fecha"), .Item("paco_mone_id")) * .Item("paco_orig_impo"), 2)
                End If

                'mActualizarLabels("A", .Item("paco_impo"), hdnId.Text)

                .Item("_mone_desc") = cmbMone.SelectedItem.Text
                .Item("_pati_desc") = clsSQLServer.gObtenerEstruc(mstrConn, "pago_tipos", mintPatiId).Tables(0).Rows(0).Item("pati_desc")
                .Item("_desc") = .Item("_pati_desc")

                Select Case mintPatiId
                    Case SRA_Neg.Constantes.PagosTipos.Retenciones
                        .Item("paco_nume") = txtNume.Valor
                        .Item("paco_rtip_id") = cmbRtip.Valor

                    Case SRA_Neg.Constantes.PagosTipos.Tarjeta
                        .Item("paco_nume") = txtNume.Valor
                        .Item("paco_tarj_id") = IIf(cmbTarj.Text.Trim().Length = 0, "", cmbTarj.Valor)
                        .Item("paco_tarj_cuot") = IIf(txtTarjCuot.Text.Trim().Length = 0, "", txtTarjCuot.Valor)
                        .Item("paco_tarj_nume") = IIf(txtTarjNume.Text.Trim().Length = 0, "", txtTarjNume.Valor)
                        .Item("paco_tarj_autori") = IIf(txtTarjAutori.Text.Trim().Length = 0, "", txtTarjAutori.Valor)
                        .Item("paco_tacl_id") = SRA_Neg.Comprobantes.gObtenerTarjCliente(mstrConn, mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_clie_id"), .Item("paco_tarj_id"), .Item("paco_tarj_nume"))
                        .Item("_tarj_desc") = cmbTarj.SelectedItem.Text
                        .Item("_desc") += "-" + .Item("_tarj_desc") + "-" + .Item("paco_tarj_nume")

                    Case SRA_Neg.Constantes.PagosTipos.Cheque
                        .Item("cheq_banc_id") = cmbBanc.Valor
                        .Item("cheq_nume") = txtCheqNume.Valor
                        If (txtTeorDepoFecha.Text.ToString().Trim.Length > 0) Then
                            .Item("cheq_teor_depo_fecha") = txtTeorDepoFecha.Fecha
                        End If
                        .Item("cheq_rece_fecha") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_fecha")
                        If (cmbPlaza.Valor.ToString().Trim().Length > 0) Then
                            .Item("cheq_plaza") = cmbPlaza.ValorBool
                        End If
                        .Item("cheq_clea_id") = cmbClea.Valor
                        .Item("cheq_chti_id") = cmbChti.Valor
                        .Item("_banc_desc") = cmbBanc.SelectedItem.Text
                        .Item("_chti_desc") = cmbChti.SelectedItem.Text
                        .Item("_clea_desc") = cmbClea.SelectedItem.Text
                        .Item("_plaza_desc") = cmbPlaza.SelectedItem.Text.Substring(0, 1)
                        .Item("_desc") += "-" + .Item("_chti_desc") + "-" + .Item("cheq_nume")
                    Case SRA_Neg.Constantes.PagosTipos.DineroElec
                        .Item("paco_nume") = txtNume.Valor
                        .Item("paco_dine_id") = cmbTarj.Valor
                        .Item("_dine_desc") = cmbTarj.SelectedItem.Text
                        .Item("_desc") += "-" + .Item("_tarj_desc") + "-" + .Item("paco_nume")
                    Case SRA_Neg.Constantes.PagosTipos.AcredBancaria
                        .Item("paco_nume") = txtNume.Valor
                        .Item("paco_cuba_id") = IIf(cmbCuba.Text.Trim().Length = 0, "", cmbCuba.Valor)
                        .Item("paco_orig_banc_id") = cmbOrigBanc.Valor
                        .Item("paco_orig_banc_suc") = txtOrigBancSuc.Valor
                        If (txtFechaAcreDepo.Text.Trim().Length > 0) Then
                            .Item("paco_acre_depo_fecha") = Convert.ToDateTime(txtFechaAcreDepo.Fecha)
                        End If
                        .Item("_cuba_banc_id") = cmbBanc.Valor
                        .Item("_banc_desc") = cmbBanc.SelectedItem.Text
                        .Item("_cuba_desc") = cmbCuba.SelectedItem.Text
                        .Item("_desc") += "-" + .Item("_banc_desc") + "-" + .Item("_cuba_desc")
                End Select

                If hdnId.Text = "" Then
                    .Table.Rows.Add(ldrDatos)

                    For Each ldrBill As DataRow In mdsDatos.Tables(mstrTablaBilletes).Select("bill_paco_id=0")
                        ldrBill.Item("bill_paco_id") = ldrDatos.Item("paco_id")
                    Next
                End If
            End With
            mSetearLabels()
    End Sub

    Private Sub mValidarDatos()
        Dim lstrFiltro As String
        Dim ldecImpo As Decimal
        Dim lintDias, lintDiasParam As Integer

        clsWeb.gValidarControles(Me)

        Select Case mintPatiId
            Case SRA_Neg.Constantes.PagosTipos.Efectivo
                If cmbMone.Valor <> SRA_Neg.Comprobantes.gMonedaPesos(mstrConn) Then
                    lstrFiltro = "bill_paco_id=" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

                    For Each ldrDatos As DataRow In mdsDatos.Tables(mstrTablaBilletes).Select(lstrFiltro)
                        ldecImpo += ldrDatos.Item("bill_deno")
                    Next

                    If ldecImpo < txtImpo.Valor Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar la denominaci�n de todos los billetes.")
                    End If
                End If

                lstrFiltro = "paco_id<>" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                lstrFiltro += " AND paco_pati_id=" & SRA_Neg.Constantes.PagosTipos.Efectivo
                lstrFiltro += " AND paco_mone_id=" & cmbMone.Valor

                If mdsDatos.Tables(mstrTablaPagos).Select(lstrFiltro).GetUpperBound(0) <> -1 Then
                    Throw New AccesoBD.clsErrNeg("Ya existe un pago con la misma moneda.")
                End If

            Case SRA_Neg.Constantes.PagosTipos.Tarjeta
                If cmbTarj.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar la tarjeta.")
                End If
                If txtTarjCuot.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar las cuotas.")
                End If
                If txtTarjNume.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el n�mero de tarjeta.")
                End If
                If txtNume.Valor Is DBNull.Value And hdnVtaTel.Text = "0" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el numero de cup�n.")
                End If

                If mbooTarjAutoriz And txtTarjAutori.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar la autorizaci�n.")
                End If

                lstrFiltro = "paco_id<>" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    lstrFiltro += " AND paco_tarj_id=" & IIf(cmbTarj.Text.Trim().Length = 0, "-1", cmbTarj.Valor)
                    lstrFiltro += " AND paco_tarj_nume=" & clsSQLServer.gFormatArg(txtTarjNume.Valor.ToString, SqlDbType.VarChar)
                lstrFiltro += " AND paco_nume=" & clsSQLServer.gFormatArg(txtNume.Valor.ToString, SqlDbType.VarChar)

                If mdsDatos.Tables(mstrTablaPagos).Select(lstrFiltro).GetUpperBound(0) <> -1 Then
                    Throw New AccesoBD.clsErrNeg("Cup�n existente.")
                End If

            Case SRA_Neg.Constantes.PagosTipos.Cheque
                If cmbBanc.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar el banco.")
                End If
                If txtCheqNume.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el nro. de cheque.")
                End If
                If txtTeorDepoFecha.Fecha Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha.")
                End If
                If cmbClea.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el clearing.")
                End If
                If cmbChti.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el tipo de cheque.")
                End If
                If cmbChti.Valor = SRA_Neg.Constantes.ChequesTipos.Comun And txtTeorDepoFecha.Fecha > Today Then
                    Throw New AccesoBD.clsErrNeg("Si es un cheque com�n, la fecha debe ser menor o igual a la del d�a.")
                End If
                If cmbChti.Valor <> SRA_Neg.Constantes.ChequesTipos.Comun And cmbMone.Valor <> SRA_Neg.Comprobantes.gMonedaPesos(mstrConn) Then
                    Throw New AccesoBD.clsErrNeg("Si no es un cheque com�n, la moneda debe ser pesos.")
                End If
                If cmbChti.Valor = SRA_Neg.Constantes.ChequesTipos.Postdatado And txtTeorDepoFecha.Fecha <= Today Then
                    Throw New AccesoBD.clsErrNeg("Los cheques postdatados deben tener fecha posterior a la del d�a.")
                End If

                    If cmbPlaza.Valor.Length <> 0 Then ''   Not cmbPlaza.ValorBool Is DBNull.Value Then
                        If cmbMone.Valor = SRA_Neg.Comprobantes.gMonedaPesos(mstrConn) Then
                            Throw New AccesoBD.clsErrNeg("Si el cheque no es de plaza local, no puede ser en pesos.")
                        End If

                        If cmbChti.Valor <> SRA_Neg.Constantes.ChequesTipos.Comun Then
                            Throw New AccesoBD.clsErrNeg("Si el cheque no es de plaza local, debe ser com�n.")
                        End If
                    End If

                    lstrFiltro = "paco_id<>" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    lstrFiltro += " AND cheq_banc_id=" & cmbBanc.Valor
                    lstrFiltro += " AND cheq_nume=" & clsSQLServer.gFormatArg(txtCheqNume.Valor.ToString, SqlDbType.VarChar)

                    If mdsDatos.Tables(mstrTablaPagos).Select(lstrFiltro).GetUpperBound(0) <> -1 Then
                        Throw New AccesoBD.clsErrNeg("Cheque existente.")
                    End If

                    lintDias = Today.Subtract(CDate(txtTeorDepoFecha.Fecha)).Days
                    If lintDias > 30 Then
                        Throw New AccesoBD.clsErrNeg("El cheque venci� hace m�s de 30 d�as.")
                    Else
                        lintDiasParam = clsSQLServer.gParametroValorConsul(mstrConn, "para_dias_aviso_cheque")
                        If lintDias >= lintDiasParam Then
                            clsError.gGenerarMensajes(Me, "Advertencia: el cheque venci� hace " & lintDias & " d�as.")
                        End If
                    End If

                Case SRA_Neg.Constantes.PagosTipos.DineroElec
                    If cmbTarj.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe seleccionar el dinero electr�nico.")
                    End If
                    If txtNume.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar el numero de cargo.")
                    End If

                Case SRA_Neg.Constantes.PagosTipos.AcredBancaria
                    If cmbOrigBanc.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe seleccionar el banco origen de la acreditaci�n.")
                    End If
                    If cmbBanc.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe seleccionar el banco propio.")
                    End If
                    If txtNume.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar el numero de boleta.")
                    End If
                    If cmbCuba.Valor Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe seleccionar la cuenta.")
                    End If

                    lstrFiltro = "paco_id<>" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    lstrFiltro += " AND paco_orig_banc_id=" & cmbOrigBanc.Valor

                    If Not txtOrigBancSuc.Valor Is DBNull.Value Then
                        lstrFiltro += " AND paco_orig_banc_suc=" & clsSQLServer.gFormatArg(txtOrigBancSuc.Valor.ToString, SqlDbType.VarChar)
                    End If

                    If txtFechaAcreDepo.Fecha Is DBNull.Value Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de Deposito.")
                    End If
                    lstrFiltro += " AND paco_nume=" & clsSQLServer.gFormatArg(txtNume.Valor.ToString, SqlDbType.VarChar)

                    If mdsDatos.Tables(mstrTablaPagos).Select(lstrFiltro).GetUpperBound(0) <> -1 Then
                        Throw New AccesoBD.clsErrNeg("Acreditaci�n existente.")
                    End If

                Case SRA_Neg.Constantes.PagosTipos.Retenciones
                    lstrFiltro = "paco_id<>" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    lstrFiltro += " AND paco_rtip_id=" & cmbRtip.Valor
                    lstrFiltro += " AND paco_nume=" & clsSQLServer.gFormatArg(txtNume.Valor.ToString, SqlDbType.VarChar)

                    If mdsDatos.Tables(mstrTablaPagos).Select(lstrFiltro).GetUpperBound(0) <> -1 Then
                        Throw New AccesoBD.clsErrNeg("Retenci�n existente.")
                    End If
            End Select
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            mCargarDatos(e.Item.Cells(Columnas.id).Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mActualizar()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mActualizar()
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Public Sub mConsultar()
        Try
            mdsDatos.Tables(mstrTablaPagos).DefaultView.RowFilter = "paco_pati_id =" & mintPatiId
            mdsDatos.Tables(mstrTablaPagos).DefaultView.Sort = "paco_id DESC"
            grdDato.DataSource = mdsDatos.Tables(mstrTablaPagos).DefaultView
            grdDato.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearMoneda()
        Dim lbooVisible As Boolean
        'lbooVisible = cmbMone.Valor <> SRA_Neg.Comprobantes.gMonedaPesos(mstrConn)
        lbooVisible = True
        divBilletes.Style.Add("display", IIf(lbooVisible, "inline", "none"))
        If Not lbooVisible Then
            'If lbooVisible Then
            txtBillDeno.Text = ""
            txtBillNume.Text = ""

            For Each ldrDatos As DataRow In mdsDatos.Tables(mstrTablaBilletes).Select("bill_paco_id=" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))
                ldrDatos.Delete()
            Next
        End If

        mConsultarBill()
    End Sub

    Private Sub mSetearTarjeta()
        Dim lintMaxCuota As Integer
        Dim ldecMaxImpo As Decimal

            mbooTarjAutoriz = False

            lintMaxCuota = 0
            ldecMaxImpo = 0
            If mintPatiId <> SRA_Neg.Constantes.PagosTipos.DineroElec And (cmbTarj.Valor.Trim().ToString().Length > 0) Then
                With clsSQLServer.gObtenerEstruc(mstrConn, "tarjetas", cmbTarj.Valor).Tables(0).Rows(0)
                    If .Item("tarj_max_cuota").ToString.Length > 0 Then
                        lintMaxCuota = .Item("tarj_max_cuota")
                    End If
                    If .Item("tarj_impo_maxi").ToString.Length > 0 Then
                        ldecMaxImpo = .Item("tarj_impo_maxi")
                    End If
                End With

                If lintMaxCuota > 0 And lintMaxCuota < Convert.ToInt32("0" + txtTarjCuot.Valor) Then
                    mbooTarjAutoriz = True
                End If

                'Decimal impovalor = IIf(txtImpo.Valor.ToString().Trim().Length > 0, Convert.ToInt32(txtImpo.Valor )
                If ldecMaxImpo > 0 And ldecMaxImpo < Convert.ToDecimal("0" + txtImpo.Valor) Then
                    mbooTarjAutoriz = True
                End If
            End If

            divTarjAutoriz.Style.Add("display", IIf(mbooTarjAutoriz, "inline", "none"))

        If txtTarjAutori.Visible Then mbooTarjAutoriz = True
        If Not mbooTarjAutoriz Then
            txtTarjAutori.Text = ""
        End If
    End Sub

#Region "Detalle"
    Public Sub mEditarDatosBill(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrBill As DataRow

            hdnBillId.Text = E.Item.Cells(1).Text
            ldrBill = mdsDatos.Tables(mstrTablaBilletes).Select("bill_id=" & hdnBillId.Text)(0)

            With ldrBill
                txtBillDeno.Valor = .Item("bill_deno")
                txtBillNume.Valor = .Item("bill_nume")
            End With

            mConsultarBill()

            mSetearEditor(mstrTablaBilletes, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarBill()
        Try
            mGuardarDatosBill()

            mLimpiarBill()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosBill()
        Dim ldrDatos As DataRow
        Dim lstrFiltro As String

        If IsNumeric(Left(txtBillNume.Text, 1)) And cmbMone.Valor = SRA_Neg.Comprobantes.gMonedaDolar(mstrConn) Then
            Throw New AccesoBD.clsErrNeg("El n�mero del billete debe comenzar con una letra.")
        End If

        If IsNumeric(Right(txtBillNume.Text, 1)) And cmbMone.Valor = SRA_Neg.Comprobantes.gMonedaPesos(mstrConn) Then
            Throw New AccesoBD.clsErrNeg("El n�mero del billete debe finalizar con una letra.")
        End If

        If txtBillDeno.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la denominaci�n.")
        End If

        If txtBillNume.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el n�mero.")
        End If

        'If hdnBillId.Text = "" AndAlso mdsDatos.Tables(mstrTablaBilletes).Select("bill_nume=" & clsSQLServer.gFormatArg(txtBillNume.Valor.ToString, SqlDbType.VarChar)).GetUpperBound(0) <> -1 Then
        If hdnBillId.Text = "" Then
            'lstrFiltro = "paco_id<>" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            'lstrFiltro += " AND paco_pati_id=" & SRA_Neg.Constantes.PagosTipos.Efectivo
            'lstrFiltro += " AND paco_pati_id=" & SRA_Neg.Constantes.PagosTipos.Efectivo
            If mdsDatos.Tables(mstrTablaBilletes).Select("bill_nume=" & clsSQLServer.gFormatArg(txtBillNume.Valor.ToString, SqlDbType.VarChar)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Billete existente.")
            End If
        Else
            If mdsDatos.Tables(mstrTablaBilletes).Select("bill_id <> " & hdnBillId.Text & " and bill_nume=" & clsSQLServer.gFormatArg(txtBillNume.Valor.ToString, SqlDbType.VarChar)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Billete existente.")
            End If
        End If

        If hdnBillId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaBilletes).NewRow
            ldrDatos.Item("bill_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaBilletes), "bill_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaBilletes).Select("bill_id=" & hdnBillId.Text)(0)
        End If

        With ldrDatos
            .Item("bill_paco_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                If txtBillDeno.Valor.ToString.Length > 0 Then
                    .Item("bill_deno") = txtBillDeno.Valor.ToString
                Else
                    .Item("bill_deno") = 0
                End If
                .Item("bill_nume") = UCase(txtBillNume.Valor)
            End With

        If hdnBillId.Text = "" Then
            mdsDatos.Tables(mstrTablaBilletes).Rows.Add(ldrDatos)
        End If

        'lstrFiltro = "paco_id<>" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
        'lstrFiltro += " AND paco_pati_id=" & SRA_Neg.Constantes.PagosTipos.Efectivo
        'lstrFiltro += " AND paco_mone_id=" & cmbMone.Valor
        'lstrFiltro += " AND paco_mone_id=" & UCase(txtBillNume.Valor)

        'If mdsDatos.Tables(mstrTablaPagos).Select(lstrFiltro).GetUpperBound(0) <> -1 Then
        '    Throw New AccesoBD.clsErrNeg("Ya ya.")
        'End If

    End Sub

    Private Sub mLimpiarBill()
        hdnBillId.Text = ""
        txtBillDeno.Text = ""
        txtBillNume.Text = ""
        mSetearEditor(mstrTablaBilletes, True)

        mConsultarBill()
    End Sub

    Private Sub btnLimpBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpBill.Click
        mLimpiarBill()
    End Sub

    Private Sub btnBajaBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaBill.Click
        Try
            mdsDatos.Tables(mstrTablaBilletes).Select("bill_id=" & hdnBillId.Text)(0).Delete()
            mLimpiarBill()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAltaBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaBill.Click
        mActualizarBill()
    End Sub

    Private Sub btnModiBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiBill.Click
        mActualizarBill()
    End Sub

    Public Sub mConsultarBill()
        mdsDatos.Tables(mstrTablaBilletes).DefaultView.RowFilter = "bill_paco_id=" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
        grdBill.DataSource = mdsDatos.Tables(mstrTablaBilletes).DefaultView
        grdBill.DataBind()
    End Sub

#End Region
    Private Sub mActualizarLabels(ByVal origen As Char, ByVal importe As String, ByVal pstrId As String)
        Dim ldblImpoPesos As Double = 0
        ldblImpoPesos = mCalcularImportePesos(importe)
        If pstrId = "" Then
            lblCabeTotalPago.Text = Format(CType(lblCabeTotalPago.Text, Double) + ldblImpoPesos, "##0.00")
            lblCabeTotalDifer.Text = Format(CType(lblCabeTotalDifer.Text, Double) + ldblImpoPesos, "##0.00")
        Else
            lblCabeTotalPago.Text = Format(CType(lblCabeTotalPago.Text, Double) + ldblImpoPesos, "##0.00")
            lblCabeTotalDifer.Text = Format(CType(lblCabeTotalDifer.Text, Double) + ldblImpoPesos, "##0.00")
        End If
    End Sub

    Private Function mCalcularImportePesos(ByVal pstrImpo As String) As Double
        Dim ldblImpo As Double
        ldblImpo = CType(pstrImpo, Double)
        If lblCoti.Text <> "" And Convert.ToDecimal(lblCoti.Text) <> 0 Then
            ldblImpo = CType(pstrImpo, Double) / Convert.ToDecimal(lblCoti.Text)
        End If
        Return (ldblImpo)
    End Function

    Private Sub mSetearLabels()
        Dim dr As DataRow
        Dim ldblTotal, ldblTotalOtros As Double
        For Each dr In mdsDatos.Tables(mstrTablaPagos).Select()
            If dr.Item("paco_pati_id") = mintPatiId Then
                ldblTotal += dr.Item("paco_impo")
            Else
                ldblTotalOtros += dr.Item("paco_impo")
            End If
        Next
        lblCabeTotalPago.Text = Format(ldblTotal + ldblTotalOtros, "##0.00")
        lblCabeTotalDifer.Text = Format(ldblTotal + ldblTotalOtros - CType(lblCabeTotalAplic.Text, Double), "##.00")
    End Sub

    Private Sub cmbMone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMone.SelectedIndexChanged

    End Sub

        'Private Function lblCoti() As Object
        '    Throw New NotImplementedException
        'End Function

        'Private Function TrFechaAcreDepo() As Object
        '    Throw New NotImplementedException
        'End Function

        'Private Function hdnVtaTel() As Object
        '    Throw New NotImplementedException
        'End Function

        'Private Function txtFechaAcreDepo() As Object
        '    Throw New NotImplementedException
        'End Function

    End Class

End Namespace
