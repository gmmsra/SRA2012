<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.MovimCuotasCatego_Pop" CodeFile="MovimCuotasCatego_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Hist�rico de Movimientos de Cuotas de Categor�as</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR>
					<TD style="WIDTH: 15%" vAlign=top align="right"><asp:Label id=lbl runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
					<TD style="WIDTH: 85%" vAlign=top align="left"><asp:Label id="lblCate" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
				</TR>
				<TR height="100%">
						<TD vAlign="top" colspan=2><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px"
							PageSize="15" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
							BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<FooterStyle CssClass="footer"></FooterStyle>
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="False" DataField="mcuc_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
								<asp:BoundColumn DataField="mcuc_audi_fecha" HeaderText="Fecha modif." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
								<asp:BoundColumn DataField="mcuc_valor" HeaderText="Valor"></asp:BoundColumn>
								<asp:BoundColumn DataField="mcuc_deau_valor" HeaderText="Valor D.Auto."></asp:BoundColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="right" height="40" colspan=2>
						<asp:button id="btnCerrar" runat="server" Text="Cerrar" Width="80px" cssclass="boton"></asp:button>&nbsp;
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
