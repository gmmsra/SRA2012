<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CalculoMuestreo" CodeFile="CalculoMuestreo.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Calculo de Muestreo</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Calculo de Muestreo</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" BorderStyle="Solid"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																	ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																	ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Periodo a Controlar:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtPeriodoDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																			<asp:label id="Label3" runat="server" cssclass="titulo">Hasta:</asp:label>
																			<cc1:DateBox id="txtPeriodoHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label4" runat="server" cssclass="titulo">Tipo Fecha:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTipoFechaFil" runat="server" Width="180px">
																				<asp:ListItem Value="" Selected>(Todos)</asp:ListItem>
																				<asp:ListItem Value="N">Nacimiento</asp:ListItem>
																				<asp:ListItem Value="I">Inscripci�n</asp:ListItem>
																				<asp:ListItem Value="P">Presentaci�n</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="Label5" runat="server" cssclass="titulo">Especie:</asp:label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbEspecieFil" runat="server" Width="176px" onchange="mCargarRazaFil()"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label6" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="280px"
																				Height="19px" MostrarBotones="False" NomOper="razas_cargar" filtra="true" AutoPostBack="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2">
																			<asp:panel id="panCriaFil" runat="server" width="100%" cssclass="titulo" BorderWidth="0px"
																				Visible="False">
																				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="Label10" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 80%" align="left" background="imagenes/formfdofields.jpg" colSpan="2">
																							<cc1:combobox class="combo" id="cmbCriadorFil" runat="server" cssclass="cuadrotexto" Width="280px"
																								Height="19px" MostrarBotones="False" NomOper="criadores_cargar" filtra="true"></cc1:combobox></TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label7" runat="server" cssclass="titulo">Inicio de Control:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaIniCtrolFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label8" runat="server" cssclass="titulo">Incluir Crias de T/E:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbIncluyeTEFil" runat="server" Width="180px">
																				<asp:ListItem Value="" Selected>(Todos)</asp:ListItem>
																				<asp:ListItem Value="0">NO</asp:ListItem>
																				<asp:ListItem Value="1">SI</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="mufi_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_anio" HeaderText="A�o">
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_fecha_desde" ReadOnly="True" HeaderText="Fecha Desde" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="10%" HorizontalAlign="Left"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_fecha_hasta" ReadOnly="True" HeaderText="Fecha Hasta" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="10%" HorizontalAlign="Left"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_tipo_fecha" ReadOnly="True" HeaderText="mufi_tipo_fecha" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="_tipo_fecha" ReadOnly="True" HeaderText="Fecha de Filtro">
												<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_espe_id" ReadOnly="True" HeaderText="mufi_espe_id" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="_espesie" ReadOnly="True" HeaderText="Especie">
												<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_raza_id" ReadOnly="True" HeaderText="mufi_raza_id" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="_raza" ReadOnly="True" HeaderText="Raza">
												<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_criador" ReadOnly="True" HeaderText="Criador">
												<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_cria_id" ReadOnly="True" HeaderText="mufi_cria_id" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_porc" ReadOnly="True" HeaderText="Porcentaje">
												<HeaderStyle HorizontalAlign="Left" Width="5%"></HeaderStyle>
												<ItemStyle HorizontalAlign="right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_fecha_ctrol_ini" ReadOnly="True" HeaderText="inicio Ctrol." DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mufi_ctrola_te" ReadOnly="True" HeaderText="mufi_ctrola_te" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="_ctrola_te" ReadOnly="True" HeaderText="Nac. T/E">
												<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="3">
									<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
													ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Pelaje"
													ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
													ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
													BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Imprimir Listado" ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" Height="116px"><!--<P align="right">-->
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 25px">
														<P></P>
													</TD>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
													<TD vAlign="top" align="right">&nbsp;
														<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 25px" align="right">
														<asp:Label id="lblCodigo" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
													<TD style="HEIGHT: 16px" colSpan="2" height="16">
														<cc1:numberbox id="txtAnio" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 25px" align="right">
														<asp:Label id="lblPeriodoDesde" runat="server" cssclass="titulo">Periodo a Controlar:</asp:Label>&nbsp;</TD>
													<TD style="HEIGHT: 16px" colSpan="2" height="16">
														<cc1:DateBox id="txtPeriodoDesde" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
														<asp:label id="lblPeriodoHasta" runat="server" cssclass="titulo">Hasta:</asp:label>
														<cc1:DateBox id="txtPeriodoHasta" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 25px" align="right">
														<asp:Label id="lblUbicacion" runat="server" cssclass="titulo">Tipo Fecha:</asp:Label>&nbsp;</TD>
													<TD style="HEIGHT: 5px" colSpan="2">
														<cc1:combobox class="combo" id="cmbTipoFecha" runat="server" Width="180px">
															<asp:ListItem Value="N">Nacimiento</asp:ListItem>
															<asp:ListItem Value="I">Inscripci�n</asp:ListItem>
															<asp:ListItem Value="P">Presentaci�n</asp:ListItem>
														</cc1:combobox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
														<asp:label id="lblEspecie" runat="server" cssclass="titulo">Especie:</asp:label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 25px" background="imagenes/formfdofields.jpg" colSpan="2">
														<cc1:combobox class="combo" id="cmbEspecie" runat="server" Width="176px" onchange="mCargarRaza()"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%; HEIGHT: 25px" align="right">
														<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%" align="left" background="imagenes/formfdofields.jpg" colSpan="2">
														<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="280px" Height="19px"
															MostrarBotones="False" NomOper="razas_cargar" filtra="true" AutoPostBack="True"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD background="imagenes/formfdofields.jpg" colSpan="3">
														<asp:panel id="panCria" runat="server" width="100%" cssclass="titulo" BorderWidth="0px" Visible="False">
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 20%; HEIGHT: 25px" align="right">
																		<asp:Label id="Label9" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 80%" align="left" background="imagenes/formfdofields.jpg" colSpan="2">
																		<cc1:combobox class="combo" id="cmbCriador" runat="server" cssclass="cuadrotexto" Width="280px"
																			onchange="mCargarCriaID(this.value)" Height="19px" MostrarBotones="False" NomOper="criadores_cargar"
																			filtra="true"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 20%; HEIGHT: 25px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblPorc" runat="server" cssclass="titulo">Porcentaje:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 80%" align="left" background="imagenes/formfdofields.jpg" colSpan="2">
																		<cc1:numberbox id="txtPorc" runat="server" cssclass="cuadrotexto" Width="50px" height="19px" MaxLength="11"
																			EsDecimal="True" Columns="2"></cc1:numberbox>%</TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 224px; HEIGHT: 16px" align="right">
														<asp:Label id="lblFechaIniCtrol" runat="server" cssclass="titulo">Inicio de Control:</asp:Label>&nbsp;</TD>
													<TD style="HEIGHT: 16px" colSpan="2" height="16">
														<cc1:DateBox id="txtFechaIniCtrol" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 224px; HEIGHT: 5px" align="right">
														<asp:Label id="lblIncluyeTE" runat="server" cssclass="titulo">Incluir Cr�as de T/E:</asp:Label>&nbsp;</TD>
													<TD style="HEIGHT: 5px" colSpan="2">
														<cc1:combobox class="combo" id="cmbIncluyeTE" runat="server" Width="180px">
															<asp:ListItem Value="">(Seleccione)</asp:ListItem>
															<asp:ListItem Value="0">No</asp:ListItem>
															<asp:ListItem Value="1">SI</asp:ListItem>
														</cc1:combobox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 224px" align="right"></TD>
													<TD colSpan="2"></TD>
												</TR>
												<TR>
													<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnDetalle" runat="server" cssclass="boton" Width="103px" CausesValidation="False"
															Text="Ver Detalle"></asp:Button></TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
														height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
											</TABLE> <!--</P>--></asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnCriaID" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtAnio"]!= null)
			document.all["txtAnio"].focus();
		
		function mCargarCriaID(pCriaID)
		{
			document.all["hdnCriaID"].Value = pCriaID;
		}
		
		function mCargarRazaFil()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecieFil").value != '')
				sFiltro = document.all("cmbEspecieFil").value;
				LoadComboXML("razas_cargar", sFiltro, "cmbRazaFil", "T");
	    }
	    function setCmbEspecieFil()
	    {
			if (document.all("cmbRazaFil").value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all("cmbRazaFil").value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all("cmbEspecieFil").value = vstrRet[0];
					
				//setCmbCriadorFil(document.all("cmbRazaFil").value);
			}		
	    }
	    
	    	
		
		function mCargarRaza()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecie").value != '')
				sFiltro = document.all("cmbEspecie").value;
				LoadComboXML("razas_cargar", sFiltro, "cmbRaza", "S");
	    }
	    function setCmbEspecie()
	    {
			if (document.all("cmbRaza").value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all("cmbRaza").value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all("cmbEspecie").value = vstrRet[0];
					
				//setCmbCriadorFil(document.all("cmbRaza").value);
			}		
	    }
		</SCRIPT>
	</BODY>
</HTML>
