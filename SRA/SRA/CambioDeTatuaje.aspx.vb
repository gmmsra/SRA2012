Imports SRA_Neg.Utiles
Imports ReglasValida.Validaciones


Namespace SRA


Partial Class CambioDeTatuaje
    Inherits FormGenerico




#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnList As NixorControls.BotonImagen
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "rp_cambios"
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
    Private mstrCambioTatuajeId As String = ""
    Private mstrProductoId As String = ""
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearEventos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
                mstrProductoId = Request.QueryString("prdt_id")
                If mstrProductoId <> "" Then
                    ' Dario 2015-01-23 para que realmente genere el historico
                    ' sino esta mierda no camina solo deja al ultimo que modifica
                    'Dim oCambioTatuaje As New SRA_Neg.CambioTatuaje(mstrConn, Session("sUserId").ToString())
                    'mstrCambioTatuajeId = oCambioTatuaje.GetUltimoCambioTatuajeIdByProductoId(mstrProductoId)
                    'hdnId.Text = mstrCambioTatuajeId
                    hdnId.Text = "0"
                    If Val(hdnId.Text) > 0 Then
                        panFiltro.Visible = False
                        grdDato.Visible = False
                        panDato.Visible = True
                        btnAgre.Visible = False
                        btnImprimir.Visible = False
                        btnBaja.Enabled = False
                        mEditarDatos(mstrCambioTatuajeId)
                    Else
                        panFiltro.Visible = False
                        grdDato.Visible = False
                        panDato.Visible = True
                        btnAgre.Visible = False
                        btnImprimir.Visible = False
                        mSetearEditor(True)
                        btnBaja.Visible = False
                        btnModi.Visible = False

                        usrProducto.Valor = mstrProductoId
                        txtFechaSol.Fecha = System.DateTime.Now
                        txtRpAnt.Valor = usrProducto.txtRPExt.Text

                        ' mAlta()
                        btnAlta.Enabled = True
                    End If
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        usrProducto.EsPropietario = False

        'usrCriador.FilClaveUnica = False
        'usrCriador.ColClaveUnica = False
        'usrCriador.ColDocuNume = False
        'usrCriador.ColCUIT = False
        'usrCriador.ColClaveUnica = False
        'usrCriador.FilClaveUnica = True
        'usrCriador.FilAgru = False
        'usrCriador.FilCriaNume = True
        'usrCriador.FilCUIT = False
        'usrCriador.FilDocuNume = True
        'usrCriador.FilTarjNume = False
        'usrCriador.Criador = True
        'usrCriador.Inscrip = True
        'usrCriador.ColCriaNume = True


        usrProducto.Tabla = mstrProductos
        usrProducto.FilCriaNume = True
        usrProducto.ColCriaNume = True
        usrProducto.FilAsocNume = True
        usrProducto.ColAsocNume = True
        usrProducto.FilNaciFecha = True
        usrProducto.ColNaciFecha = True
        usrProducto.FilProp = True
        usrProducto.FilRpNume = True
        usrProducto.ColRpNume = True
        usrProducto.FilSraNume = True
        usrProducto.ColSraNume = True
        usrProducto.FilLaboNume = True
        usrProducto.FilMadre = False
        usrProducto.FilPadre = False
        usrProducto.FilRece = False
        usrProducto.FilSexo = True
        usrProducto.Inscrip = True
        usrProducto.ColRazaSoloCodi = True

        usrProductoFil.EsPropietario = False




    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd += " @cria_id = " + usrProductoFil.CriaOrPropId.ToString()
            mstrCmd += " ,@rpcm_prdt_id = " + usrProductoFil.Valor.ToString

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd += " " + hdnId.Text
            Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
            If ldsDatos.Tables(0).Rows.Count > 0 Then
                With ldsDatos.Tables(0).Rows(0)
                    usrProducto.Valor = .Item("rpcm_prdt_id")
                    usrProducto.CriaOrPropId = .Item("_cria_id")
                    txtRpAnt.Valor = IIf(.Item("rpcm_ante_rp").ToString.Length > 0, .Item("rpcm_ante_rp"), String.Empty)
                    txtRpNumeAnt.Valor = IIf(.Item("rpcm_ante_rp_nume").ToString.Length > 0, .Item("rpcm_ante_rp_nume"), String.Empty)
                    txtRpNuevo.Valor = IIf(.Item("rpcm_nuev_rp").ToString.Length > 0, .Item("rpcm_nuev_rp"), String.Empty)
                    txtRpNumeNuevo.Valor = IIf(.Item("rpcm_nuev_rp_nume").ToString.Length > 0, .Item("rpcm_nuev_rp_nume"), String.Empty)
                    txtFechaSol.Fecha = IIf(.Item("rpcm_soli_fecha").ToString.Length > 0, .Item("rpcm_soli_fecha"), String.Empty)
                End With
                mSetearEditor(False)
                mMostrarPanel(True)
            End If
        End Sub
        Private Sub mEditarDatos(ByVal pCambioTatuajeId As String)
            hdnId.Text = pCambioTatuajeId
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd += " " + hdnId.Text
            Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
            If ldsDatos.Tables(0).Rows.Count > 0 Then
                With ldsDatos.Tables(0).Rows(0)
                    usrProducto.Valor = .Item("rpcm_prdt_id")
                    usrProducto.CriaOrPropId = ValidarNulos(.Item("_cria_id"), False)
                    txtRpAnt.Valor = IIf(.Item("rpcm_ante_rp").ToString.Length > 0, .Item("rpcm_ante_rp"), String.Empty)
                    txtRpNumeAnt.Valor = IIf(.Item("rpcm_ante_rp_nume").ToString.Length > 0, .Item("rpcm_ante_rp_nume"), String.Empty)
                    txtRpNuevo.Valor = IIf(.Item("rpcm_nuev_rp").ToString.Length > 0, .Item("rpcm_nuev_rp"), String.Empty)
                    txtRpNumeNuevo.Valor = IIf(.Item("rpcm_nuev_rp_nume").ToString.Length > 0, .Item("rpcm_nuev_rp_nume"), String.Empty)
                    txtFechaSol.Fecha = IIf(.Item("rpcm_soli_fecha").ToString.Length > 0, .Item("rpcm_soli_fecha"), String.Empty)
                End With
                mSetearEditor(False)
                mMostrarPanel(True)
            End If
        End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        txtFechaSol.Fecha = Now
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiarFil()
        usrProductoFil.Limpiar()
        mConsultar()
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        txtFechaSol.Text = ""
        txtRpAnt.Text = ""
        txtRpNumeAnt.Text = ""
        txtRpNuevo.Text = ""
        txtRpNumeNuevo.Text = ""
        'usrCriador.Limpiar()
        usrProducto.Limpiar()
        usrProductoFil.Limpiar()
        mSetearEditor(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If txtFechaSol.Fecha > Now Then
            Throw New AccesoBD.clsErrNeg("El Campo Fecha debe ser menor o igual a la actual")
        End If
    End Sub
    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        If ldsEsta.Tables(0).Rows.Count = 0 Then
            ldsEsta.Tables(0).Rows.Add(ldsEsta.Tables(0).NewRow)
        End If

        With ldsEsta.Tables(0).Rows(0)
            .Item("rpcm_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("rpcm_prdt_id") = usrProducto.Valor
            .Item("rpcm_soli_fecha") = txtFechaSol.Fecha
            .Item("rpcm_ante_rp") = txtRpAnt.Valor
            .Item("rpcm_ante_rp_nume") = QuitarLetras(ValidarNulos(txtRpAnt.Valor, False))
            .Item("rpcm_nuev_rp") = txtRpNuevo.Valor
            .Item("rpcm_nuev_rp_nume") = QuitarLetras(ValidarNulos(txtRpNuevo.Valor, False))
        End With

        Return ldsEsta
    End Function
    Private Sub mAlta() 'Deberia hacer el alta en Productos
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos, Context)
            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()
            Dim lobjinstnEnti As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

            lobjinstnEnti.Modi()

            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjinstnEnti.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub 'tengo que modificar como hace la baja
#End Region

#Region "Eventos de Controles"
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
        mstrProductoId = Request.QueryString("prdt_id")
        If mstrProductoId <> "" Then

            Dim lsbMsg As New StringBuilder

            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['hdnCambioTatuajePop'].value='{0}';", txtRpNuevo.Text))


            lsbMsg.Append(String.Format("window.opener.document.all('btnPostback').click();"))
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")

            Response.Write(lsbMsg.ToString)

        End If
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
        mstrProductoId = Request.QueryString("prdt_id")
        If mstrProductoId <> "" Then

            Dim lsbMsg As New StringBuilder

            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['hdnCambioTatuajePop'].value='{0}';", txtRpNuevo.Text))


            lsbMsg.Append(String.Format("window.opener.document.all('btnPostback').click();"))
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
         
            Response.Write(lsbMsg.ToString)

        End If
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            Dim lstrRptName As String = "CambioTatuaje"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class
End Namespace
