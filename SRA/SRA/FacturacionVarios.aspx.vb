Imports SRA
Public Class FacturacionVarios
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    'Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnLimp As System.Web.UI.WebControls.Button
    'Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnPage As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
    'Protected WithEvents btnBusc As NixorControls.BotonImagen
    'Protected WithEvents btnLimpiarFil As NixorControls.BotonImagen
    'Protected WithEvents btnAgre As NixorControls.BotonImagen
    'Protected WithEvents btnList As NixorControls.BotonImagen
    'Protected WithEvents lblActiFil As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbActiFil As NixorControls.ComboBox
    'Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFecha As NixorControls.DateBox
    'Protected WithEvents lblClieFil As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTipoComp As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbTipoComp As NixorControls.ComboBox
    'Protected WithEvents lblNro As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCEmi As NixorControls.NumberBox
    'Protected WithEvents lblSepa As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNro As NixorControls.NumberBox
    'Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblActi As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbActi As NixorControls.ComboBox
    'Protected WithEvents lblCliente As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFormaPago As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbFormaPago As NixorControls.ComboBox
    'Protected WithEvents lblFechaIngr As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaIng As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaValor As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaValor As NixorControls.DateBox
    'Protected WithEvents lblFechaIva As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaIva As NixorControls.DateBox
    'Protected WithEvents chkTPers As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents usrClieFil As usrClieDeriv
    'Protected WithEvents usrClie As usrClieDeriv
    'Protected WithEvents btnAlta As System.Web.UI.WebControls.Button
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    'Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton
    ''NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet

    Private Enum Columnas As Integer
        Id = 1
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                ' mSetearMaxLength()
                ' mSetearEventos()
                ' mEstablecerPerfil()
                Session(mSess(mstrTabla)) = Nothing
                mdsDatos = Nothing

                '  mConsultar()

                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    '   Private Sub mSetearEventos()
    '      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    '   End Sub

    '   Private Sub mEstablecerPerfil()
    '      Dim lbooPermiAlta As Boolean
    '      Dim lbooPermiModi As Boolean

    '      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
    '      'Response.Redirect("noaccess.aspx")
    '      'End If

    '      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
    '      'btnAlta.Visible = lbooPermiAlta

    '      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

    '      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
    '      'btnModi.Visible = lbooPermiModi

    '      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
    '      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    '   End Sub

    '   Private Sub mSetearMaxLength()
    '      Dim lstrLongs As Object
    '      Dim lintCol As Integer

    '      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

    '      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "_desc")

    '   End Sub
    '#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()


        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        usrClieFil.Tabla = "clientes"
        usrClieFil.AutoPostback = True
        usrClieFil.Ancho = 790
        usrClieFil.Alto = 510

        'Clave unica
        usrClieFil.ColClaveUnica = True
        usrClieFil.FilClaveUnica = True
        'Numero de cliente
        usrClieFil.ColClieNume = True

        'Fantasia
        'usrClieFil.ColFanta = False
        'usrClieFil.FilFanta = False
        'Numero de CUIT
        usrClieFil.ColCUIT = True
        usrClieFil.FilCUIT = True
        'Numero de documento
        usrClieFil.ColDocuNume = True
        usrClieFil.FilDocuNume = True
        'Numero de socio
        usrClieFil.ColSociNume = True
        usrClieFil.FilSociNume = True


        usrClie.Tabla = "clientes"
        usrClie.AutoPostback = True
        usrClie.Ancho = 790
        usrClie.Alto = 510

        'Clave unica
        usrClie.ColClaveUnica = True
        usrClie.FilClaveUnica = True
        'Numero de cliente
        usrClie.ColClieNume = True

        'Fantasia
        'usrClieFil.ColFanta = False
        'usrClieFil.FilFanta = False
        'Numero de CUIT
        usrClie.ColCUIT = True
        usrClie.FilCUIT = True
        'Numero de documento
        usrClie.ColDocuNume = True
        usrClie.FilDocuNume = True
        'Numero de socio
        usrClie.ColSociNume = True
        usrClie.FilSociNume = True
    End Sub
#End Region

    '   '#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            'mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            Dim fil As String = ""
            'If (txtDescFil.Text <> "") Then
            'fil = "@beca_desc = " + txtDescFil.Text
            'End If

            mstrCmd = "exec " + mstrTabla + "_consul " + fil

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    '   '#End Region

    '   '#Region "Seteo de Controles"
    '   '   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
    '   '      btnBaja.Enabled = Not (pbooAlta)
    '   '      btnModi.Enabled = Not (pbooAlta)
    '   '      btnAlta.Enabled = pbooAlta
    '   '   End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
        mstrCmd = "exec " + mstrTabla + "_consul"
        mstrCmd += " " + hdnId.Text

        Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
        If ldsDatos.Tables(0).Rows.Count > 0 Then
            With ldsDatos.Tables(0).Rows(0)
                ' txtDesc.Valor = .Item("beca_desc")
                ' cmbMone.Valor = .Item("beca_mone_id")
                ' txtImpo.Valor = .Item("beca_impo")
                ' txtPorc.Valor = .Item("beca_porc")
                ' chkMatr.Checked = .Item("beca_matri")
                ' chkCuot.Checked = .Item("beca_cuota")
            End With

            ' mSetearEditor(False)
            ' mMostrarPanel(True)
        End If
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnAlta.Enabled = True ' solo da de alta
        mMostrarPanel(True)
    End Sub

    '   '   Private Sub mLimpiar()
    '   '      hdnId.Text = ""
    '   '      txtDesc.Text = ""
    '   '      cmbMone.Limpiar()
    '   '      txtImpo.Text = ""
    '   '      txtPorc.Text = ""
    '   '      chkMatr.Checked = True
    '   '      chkCuot.Checked = False

    '   '      mSetearEditor(True)
    '   '   End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        Dim lbooVisiOri As Boolean = panDato.Visible

        panDato.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
            grdDato.DataBind()
        Else
            Session(mSess(mstrTabla)) = Nothing
            mdsDatos = Nothing
        End If

        If lbooVisiOri And Not pbooVisi Then
            '  mLimpiarFiltros()
        End If

    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""

        mdsDatos = Nothing
        Session(mSess(mstrTabla)) = Nothing

        mSetearEditor("", True)
    End Sub
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)

        btnAlta.Enabled = Not (pbooAlta)

    End Sub
    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function

    '   '#End Region

    '   '#Region "Opciones de ABM"


    '   '   Private Sub mValidarDatos()
    '   '      clsWeb.gInicializarControles(Me, mstrConn)
    '   '      clsWeb.gValidarControles(Me)

    '   '      If (((txtPorc.Text = "0") Or (txtPorc.Text = "")) And ((txtImpo.Text = "0") Or (txtImpo.Text = ""))) Then
    '   '         Throw New AccesoBD.clsErrNeg("Debe ingresar el importe o el porcentaje.")
    '   '      End If

    '   '      If (chkMatr.Checked = False And chkCuot.Checked = False) Then
    '   '         Throw New AccesoBD.clsErrNeg("Debe seleccionar la cuota o la matricula.")
    '   '      End If


    '   '   End Sub

    '   '   Private Sub mAlta()
    '   '      Try

    '   '         Dim ldsDatos As DataSet = mGuardarDatos()
    '   '         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

    '   '         lobjGenerica.Alta()

    '   '         mConsultar()

    '   '         mMostrarPanel(False)

    '   '      Catch ex As Exception
    '   '         clsError.gManejarError(Me, ex)
    '   '      End Try
    '   '   End Sub

    '   '   Private Sub mModi()
    '   '      Try
    '   '         clsWeb.gInicializarControles(Me, mstrConn)
    '   '         clsWeb.gValidarControles(Me)

    '   '         Dim ldsDatos As DataSet = mGuardarDatos()
    '   '         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

    '   '         lobjGenerica.Modi()

    '   '         mConsultar()

    '   '         mMostrarPanel(False)

    '   '      Catch ex As Exception
    '   '         clsError.gManejarError(Me, ex)
    '   '      End Try
    '   '   End Sub

    '   '   Private Sub mBaja()
    '   '      Try
    '   '         Dim lintPage As Integer = grdDato.CurrentPageIndex

    '   '         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
    '   '         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

    '   '         grdDato.CurrentPageIndex = 0

    '   '         mConsultar()

    '   '         If (lintPage < grdDato.PageCount) Then
    '   '            grdDato.CurrentPageIndex = lintPage
    '   '            mConsultar()
    '   '         End If

    '   '         mMostrarPanel(False)

    '   '      Catch ex As Exception
    '   '         clsError.gManejarError(Me, ex)
    '   '      End Try
    '   '   End Sub

    '   '   Private Function mGuardarDatos() As DataSet
    '   '      Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
    '   '      Dim lintCpo As Integer

    '   '      mValidarDatos()

    '   '      With ldsDatos.Tables(0).Rows(0)
    '   '         .Item("beca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
    '   '         .Item("beca_inse_id") = mintinsti
    '   '         .Item("beca_desc") = txtDesc.Valor
    '   '         .Item("beca_mone_id") = cmbMone.Valor
    '   '         .Item("beca_impo") = txtImpo.Valor
    '   '         .Item("beca_porc") = txtPorc.Valor
    '   '         .Item("beca_matri") = chkMatr.Checked
    '   '         .Item("beca_cuota") = chkCuot.Checked
    '   '      End With

    '   '      Return ldsDatos
    '   '   End Function


    '   '#End Region

    '   '#Region "Eventos de Controles"


    '   '   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
    '   '      mAlta()
    '   '   End Sub

    '   '   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
    '   '      mBaja()
    '   '   End Sub

    '   '   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
    '   '      mModi()
    '   '   End Sub

    '   '   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
    '   '      mLimpiar()
    '   '   End Sub


#End Region


    '   Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '      grdDato.CurrentPageIndex = 0
    '      mConsultar()
    '   End Sub



    Private Overloads Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        grdDato.CurrentPageIndex = 0

        mConsultar()
    End Sub

    '   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
    '      mLimpiarFil()
    '   End Sub

    '   Private Sub mLimpiarFil()
    '      '  Me.txtDescFil.Text = ""
    '      mConsultar()
    '   End Sub

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub


    '   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '      Try
    '         Dim lstrRptName As String = "FacturacionVarios"
    '         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
    '         '      lstrRpt += "&descripcion=" + txtDescFil.Text

    '         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
    '         Response.Redirect(lstrRpt)

    '      Catch ex As Exception
    '         clsError.gManejarError(Me, ex)
    '      End Try
    '   End Sub



    Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()

    End Sub
End Class