<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DebitoAutomatico" CodeFile="DebitoAutomatico.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>D�bito Autom�tico</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultgrupntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" onunload="mCerrar()" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD>
									<P></P>
								</TD>
								<TD height="5"><asp:label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:label></TD>
								<TD vAlign="top" align="right">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" ImageUrl="imagenes\Close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2" align="center">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										CellPadding="1" GridLines="None" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="tacl_id" HeaderText="tacl_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="tacl_vcto_fecha" HeaderText="Fecha Vto." DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="tacl_vige_fecha" HeaderText="Fecha Vigen." DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_cliente" HeaderText="Cliente">
												<HeaderStyle></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="tacl_nume" HeaderText="N&#250;mero">
												<HeaderStyle></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_tarjeta" HeaderText="Tarjeta">
												<HeaderStyle></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="tacl_clie_id" HeaderText="tacl_clie_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="tacl_tarj_id" HeaderText="tacl_tarj_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado">
												<HeaderStyle></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2" align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Cliente"
										BorderStyle="None" ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False"
										OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False">
											<TABLE style="WIDTH: 100%" id="Table2" class="FdoFld" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="HEIGHT: 20px" vAlign="top" align="right">
														<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;
													</TD>
													<TD style="HEIGHT: 20px">
														<cc1:combobox id="cmbTarj" class="combo" runat="server" OnSelectedIndexChanged="cmbTarj_SelectedIndexChanged"
															AutoPostBack="True" Width="200px" AceptaNull="False" Obligatorio="True"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD vAlign="top" align="right">
														<asp:Label id="lblNrorunat" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;
													</TD>
													<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtNro" runat="server" cssclass="cuadrotexto" Width="160px" AceptaNull="False"
															Obligatorio="True" EsTarjeta="true"></cc1:numberbox></TD>
												</TR>
												<TR style="WIDTH: 100%; DISPLAY: none" id="trCUITCUIL" runat="server">
													<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
														<asp:label id="lblCuit" runat="server" cssclass="titulo">CUIT/CUIL:</asp:label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
														<CC1:CUITBOX id="txtCuit" runat="server" cssclass="cuadrotexto" Width="200px" AceptaNull="False"></CC1:CUITBOX></TD>
												</TR>
												<TR style="WIDTH: 100%; DISPLAY: none" id="trDocu" runat="server">
													<TD style="WIDTH: 20%; HEIGHT: 26px" background="imagenes/formfdofields.jpg" align="right">
														<asp:label id="lblDocu" runat="server" cssclass="titulo">Nro. Documento:</asp:label>&nbsp;</TD>
													<TD style="WIDTH: 80%; HEIGHT: 26px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbDocuTipo" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:combobox>
														<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="143px" MaxValor="9999999999999"
															esdecimal="False"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD vAlign="top" align="right">
														<asp:Label id="lblVctoFecha" runat="server" cssclass="titulo">Fecha Vto.:</asp:Label>&nbsp;
													</TD>
													<TD colSpan="2">
														<cc1:DateBox id="txtVctoFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD vAlign="top" align="right">
														<asp:Label id="lblVigeFecha" runat="server" cssclass="titulo">Fecha Vigencia:</asp:Label>&nbsp;</TD>
													<TD colSpan="2">
														<cc1:DateBox id="txtVigeFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtSoliFecha"]!= null)
			document.all["txtSoliFecha"].focus();
			
		function mCerrar()
		{
			var sFiltro = "tacl_<%=mstrOrigen%>_id=<%=mstrAlumSociId%>";
			var vsRet;
			var sAD = "";
		    vsRet = EjecutarMetodoXML("Utiles.DetallesTarjetaCliente", sFiltro).split(";");			
		    if(window.opener.document.all['lblDebAuto']!=null)
		      {
		       if(vsRet[0] == "")
		         {sAD = "NO";}
		       window.opener.document.all['lblDebAuto'].innerText = vsRet[0];			   
			   window.opener.document.all['lblDA'].innerText = sAD;
			  }
		}
		</SCRIPT>
	</BODY>
</HTML>
