Namespace SRA

Partial Class CuponesRechazo
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ComprobPagos
   Private mstrTablaAsiento As String
   Private mstrTablaAsientoAlta As String = "rechazo_pagos_asiento"
   Private mstrTablaAsientoBaja As String = "rechazo_pagos_asiento_baja"
   Private mstrTablaND As String = "nota_debito"

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicializaci�n de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      If hdnId.Text <> "" Then
         If btnBaja.Enabled = False Then
            mstrTablaAsiento = mstrTablaAsientoAlta
         Else
            mstrTablaAsiento = mstrTablaAsientoBaja
         End If
      End If
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
                    txtCompFecha.Fecha = Today
                    mCargarCombos()

            clsWeb.gInicializarControles(Me, mstrConn)
                If Session("sCentroEmisorCentral") <> "S" Then
                    btnBusc.Visible = False
                    btnModi.Enabled = False
                    grdDato.Enabled = False
                    cmbTarj.Enabled = False
                    cmbTarjFil.Enabled = False
                    Throw New AccesoBD.clsErrNeg("Opci�n No Disponible Solo Se Puede Ejecutar Desde Administraci�n Central")
                End If
            End If

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjFil, "")
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S")
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooRech As Boolean)
      btnModi.Enabled = Not (pbooRech)
      btnBaja.Enabled = pbooRech
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lbooRech As Boolean
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
         hdnId.Text = .Item("paco_id").ToString()
         txtImpo.Valor = .Item("paco_orig_impo")
         cmbMone.Valor = .Item("paco_mone_id")
         cmbTarj.Valor = .Item("paco_tarj_id")
         txtNume.Valor = .Item("paco_nume")
         txtCuot.Valor = .Item("paco_tarj_cuot")
         txtAuto.Valor = .Item("paco_tarj_autori")
         usrClie.Valor = .Item("_comp_clie_id")
            txtFecha.Fecha = .Item("_comp_fecha")
            txtNroTarjeta.Valor = .Item("paco_tarj_nume")
            If Not .IsNull("paco_tarj_rech_fecha") Then
                lblBaja.Text = "Cup�n rechazado en fecha: " & CDate(.Item("paco_tarj_rech_fecha")).ToString("dd/MM/yyyy HH:mm")
                lbooRech = True
                txtCompFecha.Enabled = False
                txtCompFecha.Fecha = .Item("paco_tarj_rech_fecha")
            Else
                lblBaja.Text = ""
                txtCompFecha.Enabled = True
            End If
        End With

      mSetearEditor("", lbooRech)
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      hdnImprimio.Text = ""
      hdnImprimir.Text = ""
      hdnImpriTipo.Text = ""

      txtImpo.Text = ""
      cmbTarj.Limpiar()

      txtNume.Text = ""
      txtFecha.Text = ""
            txtCompFecha.Fecha = Today
            cmbMone.Limpiar()

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible
      btnBaja.Visible = pbooVisi
      btnModi.Visible = pbooVisi
      btnLimp.Visible = pbooVisi
      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mModi()
      Try
         Dim lstrId As String
         Dim ldsEstruc As DataSet = mGuardarDatos(True)

         Dim lobjNeg As New SRA_Neg.Cheques(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lstrId = lobjNeg.RechazarCupones(True)
         hdnImprimir.Text = lstrId

         Dim lDsAler As DataSet
         lDsAler = clsSQLServer.gObtenerEstruc(mstrConn, "alertas", "@aler_alti_id=" & SRA_Neg.Constantes.AlertasTipos.RECHAZO_DE_CUPONES)
         If lDsAler.Tables(0).Rows.Count > 0 Then
             SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), lDsAler.Tables(0).Rows(0).Item("aler_id"), clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.VarChar))
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lstrId As String
         Dim ldsEstruc As DataSet = mGuardarDatos(False)

         Dim lobjNeg As New SRA_Neg.Cheques(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lstrId = lobjNeg.RechazarCupones(False)

         hdnImprimir.Text = lstrId

         'Dim lDsAler As DataSet
         'lDsAler = clsSQLServer.gObtenerEstruc(mstrConn, "alertas", "@aler_alti_id=" & SRA_Neg.Constantes.AlertasTipos.RECHAZO_DE_CUPONES)
         'If lDsAler.Tables(0).Rows.Count > 0 Then
         '    SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), lDsAler.Tables(0).Rows(0).Item("aler_id"), clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.VarChar))
         'End If

         If hdnImpriTipo.Text = "" Then
            mConsultar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos(ByVal pbooEsAlta As Boolean) As DataSet
      Dim ldsDatos As DataSet
      Dim ldrComp, ldrCode, ldrCoco, ldrCovt, ldrAsie, ldrND As DataRow
      Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
      Dim lstrHost, lstrCemiNume, lstrEmctId As String
      Dim lDrPaco As DataRow
      Dim lDrMovim As DataRow
      Dim lstrRechaFecha As String = ""

      mValidarDatos()

      'determinar la fecha de anulacion
      If Not pbooEsAlta Then
         lstrRechaFecha = txtCompFecha.Text
      End If

      'obtengo el dataset de COMPROBANTES vac�o
      ldsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, "")

      ldsDatos.Tables.Add(mstrTablaAsiento)
      With ldsDatos.Tables(mstrTablaAsiento)
         .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_paco_id", System.Type.GetType("System.Int32"))
         'If pbooEsAlta And Not (usrClie.Valor Is DBNull.Value) Then
            .Columns.Add("proc_comp_id", System.Type.GetType("System.Int32"))
         'End If
         .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
      End With

      ldsDatos.Tables.Add(mstrTablaND)
      With ldsDatos.Tables(mstrTablaND)
         .Columns.Add("comp_id", System.Type.GetType("System.Int32"))
         .Columns.Add("comp_baja_fecha", System.Type.GetType("System.DateTime"))
         .Columns.Add("comp_audi_user", System.Type.GetType("System.Int32"))
      End With

      ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Rows(0).Delete()

      If (pbooEsAlta Or (Not pbooEsAlta And lstrRechaFecha <> Date.Today.ToString("dd/MM/yyyy"))) _
           And Not (usrClie.Valor Is DBNull.Value) Then

         ldrComp = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).NewRow

         'COMPROBANTES
         With ldrComp
            oFact.CentroEmisorNro(mstrConn)
            lstrCemiNume = oFact.pCentroEmisorNro
            lstrEmctId = oFact.pCentroEmisorId
            lstrHost = oFact.pHost

            .Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
            .Item("comp_fecha") = txtCompFecha.Fecha
            .Item("comp_ingr_fecha") = .Item("comp_fecha")
            .Item("comp_impre") = 0
            .Item("comp_clie_id") = usrClie.Valor
            .Item("comp_cance") = 0 ' 0 = cancelado
            .Item("comp_cs") = 1 ' cta cte
            If pbooEsAlta Then
                .Item("comp_dh") = 0    ' 0 = debe
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.ND
                hdnImpriTipo.Text = "ND"
            Else
                .Item("comp_dh") = 1    ' 1 = haber
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.NC
                hdnImpriTipo.Text = "NC"
            End If
            .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.CuponesRechazo
            .Item("comp_mone_id") = 1
            If cmbMone.Valor = 1 Then
                'cupon en pesos
                .Item("comp_neto") = CDec(txtImpo.Valor)
            Else
                'cupon en dolares, convertir a pesos.
                    Dim ldecCoti As Decimal = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtCompFecha.Fecha, 2)
                If ldecCoti <> 0 Then
                    .Item("comp_neto") = (CDec(txtImpo.Valor) * ldecCoti)
                Else
                    .Item("comp_neto") = CDec(txtImpo.Valor)
                End If
            End If
            .Item("comp_letra") = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("_ivap_letra")
            .Item("comp_cpti_id") = 2 ' 2 = cta.cte
            .Item("comp_acti_id") = SRA_Neg.Constantes.Actividades.Administracion
            .Item("comp_cemi_nume") = lstrCemiNume
            .Item("comp_emct_id") = lstrEmctId
            .Item("comp_host") = lstrHost

            .Table.Rows.Add(ldrComp)
         End With

         'COMPROB_DETA
         ldrCode = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobDeta).NewRow
         With ldrCode
            .Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
            .Item("code_comp_id") = ldrComp.Item("comp_id")
            If cmbMone.Valor = 1 Then
                'cupon en pesos
                .Item("code_coti") = 1
            Else
                'cupon en dolares, convertir a pesos.
                    Dim ldecCoti As Decimal = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtCompFecha.Fecha, 2)
                .Item("code_coti") = ldecCoti
            End If
            .Item("code_pers") = True
            .Item("code_impo_ivat_tasa") = 0
            .Item("code_impo_ivat_tasa_redu") = 0
            .Item("code_impo_ivat_tasa_sobre") = 0
            .Item("code_impo_ivat_perc") = 0

            .Table.Rows.Add(ldrCode)
         End With

         'COMPROB_VTOS
         ldrCovt = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).NewRow
         With ldrCovt
            .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
            .Item("covt_comp_id") = ldrComp.Item("comp_id")
            .Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, ldrComp.Item("comp_acti_id"), ldrComp.Item("comp_fecha"))
            .Item("covt_porc") = 100
            .Item("covt_impo") = ldrComp.Item("comp_neto")
            .Item("covt_cance") = 0

            .Table.Rows.Add(ldrCovt)
         End With

         'COMPROB_CONCEPTOS
         ldrCoco = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).NewRow
         With ldrCoco
            .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
            .Item("coco_comp_id") = ldrComp.Item("comp_id")
            .Item("coco_conc_id") = clsSQLServer.gObtenerEstruc(mstrConn, "conceptos_formulas", "@conc_form_id=" & SRA_Neg.Constantes.Formulas.Rechazo_de_cupon_ajuste_cta_cte).Tables(0).Rows(0).Item("conc_id")
            .Item("coco_ccos_id") = clsSQLServer.gObtenerEstruc(mstrConn, "actividades", "@acti_id=" & SRA_Neg.Constantes.Actividades.Administracion).Tables(0).Rows(0).Item("acti_ccos_id")
                If cmbMone.Valor = 1 Then
                    .Item("coco_impo") = CDec(txtImpo.Valor)
                Else
                    Dim ldecCoti As Decimal = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtCompFecha.Fecha, 2)
                    If ldecCoti <> 0 Then
                        .Item("coco_impo") = (CDec(txtImpo.Valor) * ldecCoti)
                    Else
                        .Item("coco_impo") = CDec(txtImpo.Valor)
                    End If
                End If

                .Item("coco_impo_ivai") = .Item("coco_impo")

                .Item("coco_desc_ampl") = "Rechazo de cup�n " + cmbTarj.SelectedItem.Text + " Nro " + txtNume.Text

                .Table.Rows.Add(ldrCoco)
            End With
      End If

      'ASIENTOS
      ldrAsie = ldsDatos.Tables(mstrTablaAsiento).NewRow
      With ldrAsie
         .Item("proc_id") = clsSQLServer.gObtenerId(.Table, "proc_id")
         .Item("proc_paco_id") = hdnId.Text
         .Table.Rows.Add(ldrAsie)
      End With

      'obtengo el registro a modificar
      lDrPaco = SRA_Neg.Utiles.gAgregarRegistro(mstrConn, ldsDatos.Tables(mstrTabla), hdnId.Text)

      'nota de debito
      ldrND = ldsDatos.Tables(mstrTablaND).NewRow
      With ldrND
         .Item("comp_id") = lDrPaco.Item("paco_nd_comp_id")
         If lstrRechaFecha = Date.Today.ToString("dd/MM/yyyy") Then
             .Item("comp_baja_fecha") = Date.Today
         Else
             .Item("comp_baja_fecha") = DBNull.Value
         End If
         .Table.Rows.Add(ldrND)
      End With

      'actualizo con los datos nuevos
      With lDrPaco
         If pbooEsAlta Then
            .Item("paco_conc_id") = clsSQLServer.gObtenerEstruc(mstrConn, "conceptos_formulas", "@conc_form_id=" & SRA_Neg.Constantes.Formulas.Rechazo_de_cupon_ajuste_cta_cte).Tables(0).Rows(0).Item("conc_id")
            .Item("paco_ccos_id") = clsSQLServer.gObtenerEstruc(mstrConn, "actividades", "@acti_id=" & SRA_Neg.Constantes.Actividades.Administracion).Tables(0).Rows(0).Item("acti_ccos_id")
            .Item("paco_tarj_rech_fecha") = txtCompFecha.Fecha
         Else
            .Item("paco_conc_id") = DBNull.Value
            .Item("paco_ccos_id") = DBNull.Value
            .Item("paco_tarj_rech_fecha") = DBNull.Value
         End If
         ''.Item("paco_nd_comp_id") = DBNull.Value --> no blanquear para poder anular la ND
      End With

      Return ldsDatos
   End Function

   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      ldsEsta.Tables(0).TableName = mstrTabla

      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try

         hdnImprimio.Text = ""
         hdnImprimir.Text = ""
         hdnImpriTipo.Text = ""

         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec comprob_pagos_busq")
         lstrCmd.Append(" @fecha=" & clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
         lstrCmd.Append(",@paco_tarj_id =" + cmbTarjFil.Valor.ToString)
         lstrCmd.Append(",@paco_nume =" + clsSQLServer.gFormatArg(txtNumeFil.Valor.ToString, SqlDbType.VarChar))
         lstrCmd.Append(",@clie_id =" + usrClieFil.Valor.ToString)
         lstrCmd.Append(",@paco_pati_id =" + CType(SRA_Neg.Constantes.PagosTipos.Tarjeta, String))
         lstrCmd.Append(",@rechazado=" + Math.Abs(CInt(chkRech.Checked)).ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
      Try
         If e.Item.ItemIndex <> -1 Then
            If Not CType(e.Item.DataItem, DataRowView).Row.IsNull("paco_tarj_rech_fecha") Then
               e.Item.BackColor = System.Drawing.Color.LightSalmon
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos"
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mCerrar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
      Try
         ' si no cancela la impresion

         If CBool(CInt(hdnImprimio.Text)) Then
            Dim ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantes", hdnImprimir.Text)

            With ldsDatos.Tables(0)
               .TableName = "comprobantes"
               .Rows(0).Item("comp_impre") = CBool(CInt(hdnImprimio.Text))
            End With

            While ldsDatos.Tables.Count > 1
               ldsDatos.Tables.Remove(ldsDatos.Tables(1))
            End While

            Dim lobj As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), "comprobantes", ldsDatos)

            lobj.Modi()
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
