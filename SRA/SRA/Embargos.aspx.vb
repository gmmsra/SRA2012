
Imports ReglasValida.Validaciones
Imports SRA_Entidad
Imports SRA_Neg.Utiles


Namespace SRA


Partial Class Embargos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents usrCriadorFil As usrClieDeriv
    Protected WithEvents cmbEstadoFil As NixorControls.ComboBox
    Protected WithEvents lblClieFil As System.Web.UI.WebControls.Label


    Protected WithEvents lblProducto As System.Web.UI.WebControls.Label
    Protected WithEvents btnAltaDeta As System.Web.UI.WebControls.Button
    Protected WithEvents btnBajaDeta As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimpDeta As System.Web.UI.WebControls.Button

    Protected WithEvents lblClie As System.Web.UI.WebControls.Label
    Protected WithEvents btnDeta As System.Web.UI.WebControls.Button




    Protected WithEvents lblTipoMovimientoFil As System.Web.UI.WebControls.Label
    Protected WithEvents cmbTipoEmbargoFil As NixorControls.ComboBox

    Protected WithEvents lblTipoMovimiento As System.Web.UI.WebControls.Label
    Protected WithEvents lblRaza As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbRaza As NixorControls.ComboBox
    Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
    Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents lblNumeFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtNumeFil As NixorControls.TextBoxTab
    Protected WithEvents lblRegiTipoFil As System.Web.UI.WebControls.Label
    Protected WithEvents cmbRegiTipoFil As NixorControls.ComboBox
    Protected WithEvents lblSexoFil As System.Web.UI.WebControls.Label
    Protected WithEvents cmbSexoFil As NixorControls.ComboBox
    Protected WithEvents lblRPNumeFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtRPNumeFil As NixorControls.TextBoxTab
    Protected WithEvents lblCriador As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstadoEmbargoFil As System.Web.UI.WebControls.Label


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Embargos
    Private mstrTablaDetalle As String = SRA_Neg.Constantes.gTab_EmbargosDeta
    Private mstrCmd As String
    Private mstrConn As String
    Public mintProce As Integer
    Public strConn As String
    Public sUserId As String

    Private mdsDatos As DataSet
    Private mstrParaPageSize As Integer
    Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Const mProdTodos = 0
    Const mProdUno = 1
    Const mProdConjun = 2
    Private mstrTramiteId As String = ""
    Private mstrTipo As String = ""
    Private boolAgregar As Boolean = False

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            mstrConn = clsWeb.gVerificarConexion(Me)
            strConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            sUserId = Session("sUserId").ToString()

            If (Not Page.IsPostBack) Then

                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mSetearMaxLength()
                mSetearEventos()
                mConsultar(True)
                mCargarCombos()
                clsWeb.gInicializarControles(Me, mstrConn)
                mstrTramiteId = Request.QueryString("Tram_Id")
                mstrTipo = Request.QueryString("Tipo")

                If mstrTramiteId <> "" Then
                    hdnId.Text = mstrTramiteId
                    Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())

                    mEditarDatos(mstrTramiteId)
                    mMostrarPanel(True)
                End If


                If mstrTramiteId = "" Then
                    mSetearMaxLength()
                    mSetearEventos()
                    mCargarCombos()
                    mMostrarPanel(False)
                End If
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If

            If mstrTipo <> "" Then
                If mstrTipo = 0 Then

                    cmbTipoFil.Valor = 0
                ElseIf mstrTipo = 1 Then
                    cmbTipoFil.Valor = 1

                ElseIf mstrTipo = 2 Then
                    cmbTipoFil.Valor = 2
                End If


                cmbTipo.Valor = cmbTipoFil.Valor
                cmbTipo.Enabled = False
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")

    End Sub
    Private Sub mCargarCombos()
       
        'clsWeb.gCargarCombo(mstrConn, "razas_cargar ", cmbRaza, "id", "descrip_codi", "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar ", cmbRazaFil, "id", "descrip_codi", "T")



        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
    End Sub

    Public Sub mInicializar()

        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        mintProce = ReglasValida.Validaciones.Procesos.Embargos
        usrProdFilDeta.Tabla = mstrProductos
        usrProdFilDeta.AutoPostback = False
        usrProdFilDeta.Ancho = 790
        usrProdFilDeta.Alto = 510
        usrProdFilDeta.CriaId = hdnCriaId.Text

        usrProdFilDeta.BloqCriaFil = True

        usrProdFilDeta.PermiModi = False

        usrProdFil.MuestraAsocExtr = False
        usrProdFil.MuestraNroAsocExtr = False
        usrProdFil.MuestraAsocExtr = False
        usrProdFil.MuestraNroAsocExtr = False

        usrProdFilDeta.MuestraAsocExtr = False
        usrProdFilDeta.MuestraNroAsocExtr = False

        usrProdFilDeta.usrRazaCriadorExt.ColCriaNume = False
        usrProdFilDeta.usrRazaCriadorExt.Raza = False
        usrProdFilDeta.usrRazaCriadorExt.FilCriaNume = False
        usrProdFilDeta.usrRazaCriadorExt.cmbCriaRazaExt.Enabled = False

        usrProdFilDeta.usrRazaCriadorExt.txtCodiExt.Enabled = False

     

    End Sub


#End Region

#Region "Operaciones sobre el DataGrid"

    

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            If Page.IsPostBack Then
                mConsultar(True)
            Else
                mConsultar(False)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDetalle.EditItemIndex = -1
            If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
                grdDetalle.CurrentPageIndex = 0
            Else
                grdDetalle.CurrentPageIndex = E.NewPageIndex
            End If



            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            ' the currentpageindex must be >= 0 and < pagecount<BR>   
            Try

                grdDetalle.DataBind()
                grdDetalle.Visible = True

            Catch
                Try
                    grdDetalle.CurrentPageIndex = 0
                    grdDetalle.DataBind()
                    grdDetalle.Visible = True

                Catch
                    ' Me.lblError.Text = "No data for selection"
                    ' Me.lblError.Visible = True

                End Try

            End Try



        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Function boolTodosVacios() As Boolean

        boolTodosVacios = True

        If Not txtFechaDesdeFil.Fecha Is System.DBNull.Value Then
            boolTodosVacios = False
        End If
        If Not txtFechaHastaFil.Fecha Is System.DBNull.Value Then
            boolTodosVacios = False
        End If

        If txtNroEmbargoFil.Text <> "" Then
            boolTodosVacios = False
        End If

        If usrProdFil.txtSraNumeExt.Valor.ToString <> "" Then
            boolTodosVacios = False
        End If

        If usrProdFil.cmbSexoProdExt.Valor.ToString <> "" Then
            boolTodosVacios = False
        End If

        If cmbTipoMovimientoFil.SelectedIndex <> 0 Then
            boolTodosVacios = False
        End If

        If usrProdFil.RazaId <> "" Then
            boolTodosVacios = False

        Else
            If Not Session("sRazaId") Is Nothing Then
                boolTodosVacios = False
            End If

        End If

        'If txtNroPrestamo.Text <> "" Then
        '    boolTodosVacios = False
        'End If

        If usrProdFil.txtProdNombExt.Text <> "" Then
            boolTodosVacios = False
        End If
        If usrProdFil.txtRPExt.Text <> "" Then
            boolTodosVacios = False
        End If

    End Function
    Private Sub mConsultar(ByVal boolMostrarTodos As Boolean)
        Try

            Dim dsDatos As New DataSet



            If (usrProdFil.cmbProdRazaExt Is Nothing And usrProdFil.CriaOrPropId = "0" _
               And usrProdFil.CriaOrPropId = "0" And boolTodosVacios()) Then
                Throw New AccesoBD.clsErrNeg("Debe especificar al menos una Raza para Consultar.")

            End If


            mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + " @emba_cria_id=" + _
            IIf(usrProdFil.CriaOrPropId = "", "0", usrProdFil.CriaOrPropId)
            mstrCmd = mstrCmd + ", @fecha_desde = " + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
            mstrCmd = mstrCmd + ", @fecha_hasta = " + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)

            If usrProdFil.cmbSexoProdExt.Valor.ToString <> "" Then
                mstrCmd = mstrCmd + ", @sexo = " + usrProdFil.cmbSexoProdExt.Valor.ToString()
            End If

            If usrProdFil.txtProdNombExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @NombreProd = '" + usrProdFil.txtProdNombExt.Text + "'"
            End If
            If usrProdFil.txtRPExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @RP = '" + usrProdFil.txtRPExt.Text + "'"
            End If


            If usrProdFil.txtSraNumeExt.Valor.ToString <> "" Then
                mstrCmd = mstrCmd + ", @sra_nume = " + usrProdFil.txtSraNumeExt.Valor.ToString
            End If


            If usrProdFil.RazaId <> "" Then
                mstrCmd = mstrCmd + ", @raza_id = " + usrProdFil.RazaId

            Else
                If Session("sRazaId") Is Nothing Then
                    mstrCmd = mstrCmd + ", @raza_id = 0"
                Else
                    mstrCmd = mstrCmd + ", @raza_id = " + Session("sRazaId")
                End If
            End If

            If cmbTipoMovimientoFil.SelectedIndex <> 0 Then
                mstrCmd = mstrCmd + ",@tipo_movimiento=" + cmbTipoMovimientoFil.Valor.ToString()
            End If

            If cmbTipoFil.SelectedIndex <> 0 Then
                mstrCmd = mstrCmd + ",@emba_tipo=" + cmbTipoFil.Valor.ToString()
            End If
            If txtNroEmbargoFil.Text <> "" Then
                mstrCmd = mstrCmd + ",@emba_id=" + txtNroEmbargoFil.Text
            End If



            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)
            grdDato.Visible = True
            grdDato.DataSource = dsDatos

            grdDato.DataBind()


            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mConsultarDeta()

        Try



            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim dtProductos As DataTable
            Dim CriadorId As Integer = 0
            Dim RazaId As Integer = 0
            Dim intEmbargoId As Integer = 0
            Dim ds As New DataSet

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                CriadorId = usrCria.Valor
                If usrCria.cmbCriaRazaExt.Valor <> 0 Then
                    RazaId = usrCria.cmbCriaRazaExt.Valor
                End If

                If CriadorId.ToString() = "0" Or CriadorId.ToString() = "" Or _
                                   RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                    Throw New AccesoBD.clsErrNeg("La Raza y el propietario deben ser distinto de Vacio")

                    Return
                End If

                dtProductos = oProducto.GetProductosByCriadorId(CriadorId, RazaId, "ProductosPropios")
            End If



            Dim ldrDeta As DataRow

            If hdnDetaId.Text = "" Then
                ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                ldrDeta.Item("emde_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "emde_id")
            Else
                ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("emde_id=" & hdnDetaId.Text)(0)
            End If
            Dim PosMax As Integer = 0

            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                If lDr.Item("_pos") > PosMax Then
                    PosMax = lDr.Item("_pos")

                End If
            Next
            intEmbargoId = -1



            For Each rw As DataRow In dtProductos.Rows
                If Not ExistInDataset(mdsDatos, mdsDatos.Tables(mstrTablaDetalle), _
                "emde_prdt_id=" & rw.Item("prdt_id")) Then
                    PosMax = PosMax + 1
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                    ldrDeta.Item("emde_id") = PosMax
                    ldrDeta.Item("emde_emba_id") = intEmbargoId
                    ldrDeta.Item("emde_Fina_fecha") = rw.Item("_emde_Fina_fecha")
                    ldrDeta.Item("emde_prdt_id") = rw.Item("prdt_id")
                    ldrDeta.Item("_producto") = rw.Item("prdt_sra_nume").ToString() & _
                    " | " & "RP " & rw.Item("prdt_RP").ToString() & _
                       " | " & rw.Item("prdt_nomb").ToString()
                    If cmbTipo.Valor = mProdTodos Then
                        ldrDeta.Item("_estado") = "Embargo"
                        ' Si es nuevo y embarga todos  la primera vez siempre es embargado
                    Else
                        ldrDeta.Item("_estado") = "Activo"
                        ' Si es nuevo y embarga POR HBA o Por Lote son todos activos dado que hay que mostrarlos para seleccionar
                        ' ' Significa que hasta que no este seleccionado no esta ni embargado  , no levantado
                    End If

                    ldrDeta.Item("_pos") = PosMax
                    ldrDeta.Item("_hba") = ValidarNulos(rw.Item("prdt_sra_nume"), False).ToString()
                    ldrDeta.Item("_rp") = rw.Item("prdt_RP").ToString()
                    ldrDeta.Item("_nombre") = rw.Item("prdt_nomb").ToString()
                    ldrDeta.Item("_razaCodi") = rw.Item("_razaCodi").ToString()
                    ldrDeta.Item("_razaDesc") = rw.Item("_razaDesc").ToString()
                    ldrDeta.Item("_razaId") = rw.Item("_razaId").ToString()
                    ldrDeta.Item("_CHK") = rw.Item("_CHK").ToString()
                    ldrDeta.Item("_sexo") = rw.Item("_sexo").ToString()
                    ldrDeta.Item("_estadoProducto") = rw.Item("_estadoProducto").ToString()

                    If (hdnDetaId.Text = "") Then
                        mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
                    End If

                End If

            Next

            If btnAlta.Enabled Then
                If cmbTipo.Valor = mProdTodos Then
                    mActualizarCHK(True)
                Else
                    '   no hago nada dado que deben quedar en estado activo Hasta que se toque en la grilla mActualizarCHK(False)
                End If
            End If


            mdsDatos.AcceptChanges()

            With mdsDatos.Tables(mstrTablaDetalle)
                .DefaultView.RowFilter = ""
                .DefaultView.Sort = "_estado desc"
                grdDetalle.DataSource = .DefaultView

                ' the currentpageindex must be >= 0 and < pagecount<BR>   
                Try

                    grdDetalle.DataBind()
                    grdDetalle.Visible = True

                Catch
                    Try
                        grdDetalle.CurrentPageIndex = 0
                        grdDetalle.DataBind()
                        grdDetalle.Visible = True

                    Catch
                        ' Me.lblError.Text = "No data for selection"
                        ' Me.lblError.Visible = True

                    End Try

                End Try




            End With
            mActualizarEtiq()
            mdsDatos.AcceptChanges()

            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()


        Catch ex As Exception

            clsError.gManejarError(Me, ex)

        End Try

    End Sub

#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)

        If cmbTipo.Valor = mProdTodos Then
            btnSeleccionarTodos.Enabled = False
            btnDeSeleccionarTodos.Enabled = False

        Else
            btnSeleccionarTodos.Enabled = True
            btnDeSeleccionarTodos.Enabled = True

        End If

        If Not boolAgregar Then
            grdDetalle.Enabled = True
            grdDetalle.Columns(0).Visible = True
        Else
            If cmbTipo.Valor = mProdTodos Then
                grdDetalle.Enabled = False
                grdDetalle.Columns(0).Visible = False
            End If

        End If
    End Sub
    Public Function ExistInDataset(ByVal pDsExit As DataSet, ByVal pTabla As DataTable, ByVal Filtro As String) As Boolean



        For Each lDrExist As DataRow In pDsExit.Tables(pTabla.ToString).Select(Filtro)
            If lDrExist.Item("emde_prdt_id").ToString() = "" Then
                Return False
            End If
            If CInt(lDrExist.Item("emde_prdt_id")) <= 0 Then
                Return False
            Else
                Return True
            End If

        Next



    End Function

    Public Sub mEditarDatos(ByVal pNroEmbargo As String)
        Try


            mLimpiar()

            If Not boolAgregar Then
                grdDetalle.Enabled = True
                grdDetalle.Columns(0).Visible = True
            Else
                grdDetalle.Enabled = False
                grdDetalle.Columns(0).Visible = False
            End If

            hdnId.Text = pNroEmbargo

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    txtNroEmbargo.Text = pNroEmbargo
                    usrCria.Valor = .Item("emba_cria_id")
                    txtFechaDesde.Fecha = .Item("emba_inic_fecha")
                    txtFechaHasta.Fecha = .Item("emba_fina_fecha")
                    txtFechaPresentacion.Fecha = .Item("emba_fecha_mov")
                    cmbTipo.Valor = .Item("emba_tipo")
                    txtObse.Valor = .Item("emba_obse")
                    hdnCriaId.Text = .Item("emba_cria_id")


                    cmbTipoMovimiento.Valor = Left(.Item("_estado"), 1)

                    If Not .IsNull("emba_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & _
                        CDate(.Item("emba_baja_fecha")).ToString("dd/MM/yyyy HH:mm")

                    Else
                        lblBaja.Text = ""
                    End If


                End With
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim dtProductos As DataTable
                Dim CriadorId As Integer = 0
                Dim ClienteId As Integer = 0
                Dim RazaId As Integer = 0
                Dim intEmbargoId As Integer = 0
                Dim ds As New DataSet

                Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
                If mdsDatos.Tables(0).Rows.Count > 0 Then
                    intEmbargoId = mdsDatos.Tables(0).Rows(0).Item("emba_id")
                    CriadorId = mdsDatos.Tables(0).Rows(0).Item("emba_cria_id")
                    intEmbargoId = mdsDatos.Tables(0).Rows(0).Item("emba_id")

                    If usrCria.cmbCriaRazaExt.Valor <> 0 Then
                        RazaId = usrCria.cmbCriaRazaExt.Valor
                    End If

                    If CriadorId.ToString() = "0" Or CriadorId.ToString() = "" Or _
                       RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                        Throw New AccesoBD.clsErrNeg("La Raza y el propietario deben ser distinto de Vacio")

                        Return
                    End If


                    ClienteId = oCliente.GetClienteIdByRazaCriador(usrCria.cmbCriaRazaExt.Valor, usrCria.Valor)
                    ' si es modificacion solo se agregan solo 300 no  todo 
                    ' dtProductos = oProducto.GetProductosTop300ByClienteId(ClienteId, RazaId, "ProductosPropios")
                End If



                Dim ldrDeta As DataRow

                If hdnDetaId.Text = "" Then
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                    ldrDeta.Item("emde_id") = _
                    clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "emde_id")
                Else
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("emde_id=" & hdnDetaId.Text)(0)
                End If
                Dim PosMax As Integer = 0

                For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                    If lDr.Item("_pos") > PosMax Then
                        PosMax = lDr.Item("_pos")

                    End If
                Next

                If Not dtProductos Is Nothing Then

                    For Each rw As DataRow In dtProductos.Rows
                        If Not ExistInDataset(mdsDatos, mdsDatos.Tables(mstrTablaDetalle), "emde_prdt_id=" & rw.Item("prdt_id")) Then
                            PosMax = PosMax + 1
                            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                            ldrDeta.Item("emde_emba_id") = intEmbargoId
                            ldrDeta.Item("emde_prdt_id") = rw.Item("prdt_id")
                            ldrDeta.Item("_producto") = rw.Item("prdt_sra_nume").ToString() & _
                            " | " & "RP " & rw.Item("prdt_RP").ToString() & _
                               " | " & rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_estado") = "Activo"
                            ldrDeta.Item("_pos") = PosMax
                            ldrDeta.Item("_hba") = ValidarNulos(rw.Item("prdt_sra_nume"), False).ToString()
                            ldrDeta.Item("_rp") = ValidarNulos(rw.Item("prdt_RP").ToString(), False).ToString()
                            ldrDeta.Item("_nombre") = rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_razaCodi") = rw.Item("_razaCodi").ToString()
                            ldrDeta.Item("_razaDesc") = rw.Item("_razaDesc").ToString()
                            ldrDeta.Item("_razaId") = rw.Item("_razaId").ToString()
                            ldrDeta.Item("_CHK") = rw.Item("_CHK").ToString()
                            ldrDeta.Item("_sexo") = rw.Item("_sexo").ToString()
                            ldrDeta.Item("_estadoProducto") = rw.Item("_estadoProducto").ToString()


                            If (hdnDetaId.Text = "") Then
                                mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
                            End If

                        End If

                    Next
                End If
                mdsDatos.AcceptChanges()


                With mdsDatos.Tables(mstrTablaDetalle)
                    .DefaultView.RowFilter = ""
                    .DefaultView.Sort = "_estado desc"
                    grdDetalle.DataSource = .DefaultView
                    ' the currentpageindex must be >= 0 and < pagecount<BR>   
                    Try

                        grdDetalle.DataBind()
                        grdDetalle.Visible = True

                    Catch
                        Try
                            grdDetalle.CurrentPageIndex = 0
                            grdDetalle.DataBind()
                            grdDetalle.Visible = True

                        Catch
                            ' Me.lblError.Text = "No data for selection"
                            ' Me.lblError.Visible = True

                        End Try

                    End Try


                End With


                mActualizarEtiq()

                mSetearEditor(False)
                mSetearEditorDeta(False)
                mMostrarPanel(True)
                mShowTabs(1)
                If cmbTipo.Valor <> "0" Then
                    mShowTabs(2)
                End If


            End If

        Catch ex As Exception

            clsError.gManejarError(Me, ex)

        End Try
    End Sub


    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try


            mLimpiar()
            If Not boolAgregar Then
                grdDetalle.Enabled = True
                grdDetalle.Columns(0).Visible = True
            Else
                grdDetalle.Enabled = False
                grdDetalle.Columns(0).Visible = False
            End If
            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

            mCrearDataSet(hdnId.Text)

            txtNroEmbargo.Text = hdnId.Text


            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)

                    usrCria.Valor = .Item("emba_cria_id")
                        txtFechaDesde.Fecha = IIf(.Item("emba_inic_fecha").ToString.Length > 0, .Item("emba_inic_fecha"), String.Empty)
                        txtFechaHasta.Fecha = IIf(.Item("emba_fina_fecha").ToString.Length > 0, .Item("emba_fina_fecha"), String.Empty)
                        txtFechaPresentacion.Fecha = IIf(.Item("emba_fecha_mov").ToString.Length > 0, .Item("emba_fecha_mov"), String.Empty)
                        cmbTipo.Valor = IIf(.Item("emba_tipo").ToString.Length > 0, .Item("emba_tipo"), String.Empty)
                    usrCria.cmbCriaRazaExt.Valor = .Item("emba_raza_id")
                    txtObse.Valor = .Item("emba_obse")
                    hdnCriaId.Text = .Item("emba_cria_id")

                    If Left(.Item("_estado"), 1) = "E" Then
                        cmbTipoMovimiento.Valor = "E"
                    End If
                    If Left(.Item("_estado"), 1) = "L" Then
                        cmbTipoMovimiento.Valor = "X"
                    End If


                    If Not .IsNull("emba_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & _
                        CDate(.Item("emba_baja_fecha")).ToString("dd/MM/yyyy HH:mm")

                    Else
                        lblBaja.Text = ""
                    End If


                End With
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim dtProductos As DataTable
                Dim CriadorId As Integer = 0
                Dim RazaId As Integer = 0
                Dim intEmbargoId As Integer = 0
                Dim ds As New DataSet


                If mdsDatos.Tables(0).Rows.Count > 0 Then
                    intEmbargoId = mdsDatos.Tables(0).Rows(0).Item("emba_id")
                    CriadorId = mdsDatos.Tables(0).Rows(0).Item("emba_cria_id")
                    If usrCria.cmbCriaRazaExt.Valor <> 0 Then
                        RazaId = usrCria.cmbCriaRazaExt.Valor
                    End If

                    If CriadorId.ToString() = "0" Or CriadorId.ToString() = "" Or _
                       RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                        Throw New AccesoBD.clsErrNeg("La Raza y el propietario deben ser distinto de Vacio")

                        Return
                    End If
                    ' si es modificacion solo se agregan solo 300 no  todo 
                    '   dtProductos = oProducto.GetProductosTop300ByClienteId(ClienteId, RazaId, "ProductosPropios")
                End If



                Dim ldrDeta As DataRow

                If hdnDetaId.Text = "" Then
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                    ldrDeta.Item("emde_id") = _
                    clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "emde_id")
                Else
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("emde_id=" & hdnDetaId.Text)(0)
                End If
                Dim PosMax As Integer = 0

                For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                    If lDr.Item("_pos") > PosMax Then
                        PosMax = lDr.Item("_pos")

                    End If
                Next

                If Not dtProductos Is Nothing Then

                    For Each rw As DataRow In dtProductos.Rows
                        If Not ExistInDataset(mdsDatos, mdsDatos.Tables(mstrTablaDetalle), "emde_prdt_id=" & rw.Item("prdt_id")) Then
                            PosMax = PosMax + 1
                            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                            ldrDeta.Item("emde_id") = PosMax
                            ldrDeta.Item("emde_emba_id") = intEmbargoId
                            ldrDeta.Item("emde_prdt_id") = rw.Item("prdt_id")
                            ldrDeta.Item("_producto") = rw.Item("prdt_sra_nume").ToString() & _
                            " | " & "RP " & rw.Item("prdt_RP").ToString() & _
                               " | " & rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_estado") = "Activo"
                            ldrDeta.Item("_pos") = PosMax
                            ldrDeta.Item("_hba") = ValidarNulos(rw.Item("prdt_sra_nume"), False).ToString()
                            ldrDeta.Item("_rp") = ValidarNulos(rw.Item("prdt_RP").ToString(), False).ToString()
                            ldrDeta.Item("_nombre") = rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_razaCodi") = rw.Item("_razaCodi").ToString()
                            ldrDeta.Item("_razaDesc") = rw.Item("_razaDesc").ToString()
                            ldrDeta.Item("_razaId") = rw.Item("_razaId").ToString()
                            ldrDeta.Item("_CHK") = rw.Item("_CHK").ToString()
                            ldrDeta.Item("_sexo") = rw.Item("_sexo").ToString()
                            ldrDeta.Item("_estadoProducto") = rw.Item("_estadoProducto").ToString()


                            If (hdnDetaId.Text = "") Then
                                mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
                            End If

                        End If

                    Next
                End If
                mdsDatos.AcceptChanges()


                With mdsDatos.Tables(mstrTablaDetalle)
                    .DefaultView.RowFilter = ""
                    .DefaultView.Sort = "_estado desc"
                    grdDetalle.DataSource = .DefaultView
                    ' the currentpageindex must be >= 0 and < pagecount<BR>   
                    Try

                        grdDetalle.DataBind()
                        grdDetalle.Visible = True

                    Catch
                        Try
                            grdDetalle.CurrentPageIndex = 0
                            grdDetalle.DataBind()
                            grdDetalle.Visible = True

                        Catch
                            ' Me.lblError.Text = "No data for selection"
                            ' Me.lblError.Visible = True

                        End Try

                    End Try


                End With


                mActualizarEtiq()

                mSetearEditor(False)



                mMostrarPanel(True)
                mShowTabs(1)
                If cmbTipo.Valor <> "0" Then
                    mShowTabs(2)
                End If


            End If

        Catch ex As Exception

            clsError.gManejarError(Me, ex)

        End Try
    End Sub
    Private Sub mActualizarEtiq()
        Try


            Dim oEmbargos As New SRA_Neg.Embargos(mstrConn, Session("sUserId").ToString())
            Dim dtEmbargos As DataTable
            Dim intEmbargoId As Integer = 0
            If hdnId.Text = "" Then
                intEmbargoId = -1
            Else
                intEmbargoId = mdsDatos.Tables(0).Rows(0).Item("emba_id")
            End If


            dtEmbargos = oEmbargos.GetEmbargosDetallesById(intEmbargoId, "ProductosTotal")

            Dim intSeleccionados As Integer = 0
            Dim intTotalProductos As Integer = 0
            Dim intTotalEmbargados As Integer = 0
            Dim intTotalActivos As Integer = 0
            Dim intTotalLevanta As Integer = 0
            Dim intTotalRetenidos As Integer = 0


            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                intTotalProductos = intTotalProductos + 1

                Select Case UCase(lDr.Item("_estado"))
                    Case "EMBARGO"
                        intTotalEmbargados = intTotalEmbargados + 1
                    Case "ACTIVO"
                        intTotalActivos = intTotalActivos + 1
                    Case "LEVANTA"
                        intTotalLevanta = intTotalLevanta + 1
                    Case "INGRESADO RETENIDO"
                        If lDr.Item("_CHK") = "1" Then
                            intTotalRetenidos = intTotalRetenidos + 1
                        End If

                    Case Else
                        '  intTotalLevanta = intTotalLevanta + 1

                End Select


            Next


            lblTotalProductos.Text = "Total Productos:" & intTotalProductos
            lblTotalActivos.Text = "Total Activo:" & intTotalActivos
            lblTotalEmbargados.Text = "Total Embargados:" & intTotalEmbargados
            lblTotalLevanta.Text = "Total Levanta:" & intTotalLevanta
            lblTotalRetenidos.Text = "Total Retenidos:" & intTotalRetenidos
        Catch ex As Exception

        End Try
    End Sub

    Private Function EstaLevantado() As Boolean
        Try
            EstaLevantado = False

            Dim oEmbargos As New SRA_Neg.Embargos(mstrConn, Session("sUserId").ToString())
            Dim dtEmbargos As DataTable
            Dim intEmbargoId As Integer = 0
            If hdnId.Text = "" Then
                intEmbargoId = -1
            Else
                intEmbargoId = mdsDatos.Tables(0).Rows(0).Item("emba_id")
            End If


            dtEmbargos = oEmbargos.GetEmbargosDetallesById(intEmbargoId, "ProductosTotal")

            Dim intSeleccionados As Integer = 0
            Dim intTotalProductos As Integer = 0
            Dim intTotalEmbargados As Integer = 0
            Dim intTotalActivos As Integer = 0
            Dim intTotalLevanta As Integer = 0


            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                intTotalProductos = intTotalProductos + 1

                Select Case UCase(lDr.Item("_estado"))
                    Case "EMBARGO"
                        intTotalEmbargados = intTotalEmbargados + 1
                    Case "ACTIVO"
                        intTotalActivos = intTotalActivos + 1
                    Case "LEVANTA"
                        intTotalLevanta = intTotalLevanta + 1
                    Case Else
                        '  intTotalLevanta = intTotalLevanta + 1

                End Select


            Next


            If (intTotalProductos = intTotalLevanta) Then
                EstaLevantado = True
            End If

        Catch ex As Exception

        End Try
    End Function

    Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim intCriaId As Int32
            Dim intRazaId As Int32
            Dim ldrDeta As DataRow
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim dtRaza As DataTable


            hdnDetaId.Text = E.Item.Cells(1).Text


            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("emde_id=" & hdnDetaId.Text)(0)


            With ldrDeta

                intRazaId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("_prdt_raza_id"), False)
                intCriaId = ValidarNulos(mdsDatos.Tables(0).Rows(0).Item("emba_cria_id"), False)


                usrCria.Valor = intCriaId
                usrProdFilDeta.CriaOrPropId = intCriaId
                usrProdFilDeta.Valor = .Item("emde_prdt_id")

                intRazaId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("_prdt_raza_id"), False)
                intCriaId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_cria_id"), False)
                    txtFechaFinalProducto.Fecha = IIf(.Item("emde_fina_fecha").ToString.Length > 0, .Item("emde_fina_fecha"), String.Empty)

                If Not mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_baja_fecha") Is System.DBNull.Value Then
                    lblBajaDeta.Text = "Embargo dado de baja en fecha: " & _
                    CDate(mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaDeta.Text = ""
                End If

                If Not .Item("emde_baja_fecha") Is System.DBNull.Value Then
                    lblBajaDeta.Text = "Producto del Embargo dado de baja en fecha: " & _
                    CDate(.Item("emde_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaDeta.Text = ""
                End If





            End With

            mSetearEditorDeta(False)


            If Not boolAgregar Then
                grdDetalle.Enabled = True
                grdDetalle.Columns(0).Visible = True
            Else
                grdDetalle.Enabled = False
                grdDetalle.Columns(0).Visible = False
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)
        Try
            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrTablaDetalle

            If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
                mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
            End If
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()

            Session(mstrTabla) = mdsDatos
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mAgregar()
        boolAgregar = True
        lblAlta.text = "A"
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
        cmbTipoMovimiento.Valor = "E"
        cmbTipoMovimiento.Enabled = False

    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnCriaId.Text = ""
        cmbTipo.Limpiar()
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""
        txtNroEmbargo.Text = ""
        usrCria.Limpiar()

        txtObse.Text = ""
        lblBaja.Text = ""

        grdDato.CurrentPageIndex = 0
        grdDetalle.CurrentPageIndex = 0


        mstrTipo = Request.QueryString("Tipo")

        If mstrTipo <> "" Then
            If mstrTipo = 0 Then
                cmbTipo.Valor = 0
            ElseIf mstrTipo = 1 Then
                cmbTipo.Valor = 1

            ElseIf mstrTipo = 2 Then
                cmbTipo.Valor = 2
            End If
            cmbTipo.Enabled = False

            cmbTipo.Valor = cmbTipoFil.Valor
            cmbTipo.Enabled = False
        End If
        mLimpiarDeta()


        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarDeta()
        hdnDetaId.Text = ""
        usrProdFilDeta.Valor = ""
        lblBajaDeta.Text = ""
        usrProdFilDeta.Limpiar()
        txtFechaFinalProducto.Text = ""

        mSetearEditorDeta(True)
    End Sub
    Private Sub mLimpiarFil()
        grdDato.CurrentPageIndex = 0


        usrProdFil.Limpiar()
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        cmbTipoReporte.Limpiar()
        cmbTipoMovimientoFil.Limpiar()
        usrProdFil.cmbSexoProdExt.Valor = ""

        mConsultar(True)
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        btnAgre.Visible = panFiltro.Visible
        btnList.Visible = Not (panDato.Visible)

        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub


#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Dim lstrMsgError As String

        Dim booSexo As Boolean
        Dim pstrProductoId As String
        Dim lTransac As SqlClient.SqlTransaction

        Try
            mLimpiarActivos()
            lblAlta.Text = "A"
            mValidaCantProdUnico()
            mGuardarDatos()

            Dim lobjEmbargos As New SRA_Neg.Embargos(mstrConn, Session("sUserId").ToString(), "embargos", mdsDatos)
            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

            lblAltaId.Text = lobjEmbargos.mActualEmbargos("A")
            booSexo = oProducto.GetSexoByProductoId(hdnId.Text)
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)


            If (lblAltaId.Text <> "-1") Then
                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_Embargos, lblAltaId.Text)

                'Aplicar Reglas.
                lstrMsgError = gValidarReglas(lTransac, SRA_Neg.Constantes.gTab_Embargos, lblAltaId.Text, _
                                Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Embargos.ToString(), mdsDatos)


            End If

            If lobjEmbargos.GetEmbargoErrorByNroEmbargo(lblAltaId.Text) Then
                mdsDatos.Tables(0).Rows(0).Item("emba_esta_id") = SRA_Neg.Constantes.Estados.Prestamos_Retenido
                cmbTipoMovimiento.SelectedValue = "R"
                clsSQLServer.gModi(lTransac, Session("sUserId").ToString(), "embargos", mdsDatos.Tables(0).Rows(0))

            End If

            clsSQLServer.gCommitTransac(lTransac)

            If lstrMsgError <> "" Then
                AccesoBD.clsError.gGenerarMensajes(Me, lstrMsgError)
            End If


            mLimpiar()
            mConsultar(False)

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim intCriaId As Int32
            Dim intRazaId As Int32
            Dim intEmbaId As Int32
            Dim ldrDeta As DataRow
            Dim pstrProductoId As String
            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

            Dim lstrMsgError As String
            Dim lTransac As SqlClient.SqlTransaction
            Dim booSexo As Boolean



            booSexo = oProducto.GetSexoByProductoId(hdnId.Text)

            mValidarFechaFinalizacion()


            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            mLimpiarActivos()
            If hdnDetaId.Text <> "" Then
                ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("emde_id=" & hdnDetaId.Text)(0)
                With ldrDeta
                    .Item("_emde_fina_fecha") = txtFechaFinalProducto.Text
                End With
            End If
            mValidarDatosDeta()

            Dim lobjEmbargos As New SRA_Neg.Embargos(mstrConn, Session("sUserId").ToString(), "embargos", mdsDatos)


            intEmbaId = mdsDatos.Tables(0).Rows(0).Item("emba_id")
            ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_Embargos, intEmbaId)

            'lblAltaId.Text = lobjEmbargos.mActualEmbargos("M")

            lobjGenerica.Modi()
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)


            If (intEmbaId <> "-1") Then
                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_Embargos, intEmbaId)

                'Aplicar Reglas.
                lstrMsgError = gValidarReglas(lTransac, SRA_Neg.Constantes.gTab_Embargos, intEmbaId, _
                                 Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Embargos.ToString(), mdsDatos)


            End If

            If lobjEmbargos.GetEmbargoErrorByNroEmbargo(intEmbaId) Then
                mdsDatos.Tables(0).Rows(0).Item("emba_esta_id") = SRA_Neg.Constantes.Estados.Prestamos_Retenido
                cmbTipoMovimiento.SelectedValue = "R"
                clsSQLServer.gModi(lTransac, Session("sUserId").ToString(), "embargos", mdsDatos.Tables(0).Rows(0))

            End If

            clsSQLServer.gCommitTransac(lTransac)

                boolAgregar = False

                mConsultar(False)

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar(True)

            'If (lintPage < grdDato.PageCount) Then
            '   grdDato.CurrentPageIndex = lintPage
            'End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos(ByVal boolAlta)
        Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
            'If Not (txtFechaDesde.Fecha Is DBNull.Value) And Not (txtFechaHasta.Fecha Is DBNull.Value) Then
            If (txtFechaDesde.Fecha.Trim().Length > 0) And (txtFechaHasta.Fecha.Trim().Length > 0) Then
                If txtFechaDesde.Fecha > txtFechaHasta.Fecha Then
                    Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
                End If
            End If

            'embargo de un solo producto o varios
            If boolAlta Then

            mValidaGrupoHBA()
            mValidaCantProdUnico()


        End If


        If usrCria.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Propietario.")
        End If



    End Sub
    Private Sub mGuardarDatos()
        Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
        mValidaDatos(True)

        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim intProductoId As Integer
        Dim dtProducto As DataTable
        Dim intEstadoIdAnterior As Integer
        Dim intHBA As Integer
        Dim intRazaId As Integer
        Dim Sexo As String

        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

        With mdsDatos.Tables(0).Rows(0)
            .Item("emba_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            .Item("emba_clie_id") = oCliente.GetClienteIdByRazaCriador(usrCria.cmbCriaRazaExt.Valor, usrCria.Valor)
            .Item("emba_cria_id") = usrCria.Valor
            .Item("emba_inic_fecha") = txtFechaDesde.Fecha
            .Item("emba_raza_id") = usrCria.cmbCriaRazaExt.Valor


                'If GetEmbargo() = 0 And (GetLevanta() = GetTotal()) Then
                '    ' la fecha final se guardo automaticamente
                'Else
                .Item("emba_fina_fecha") = IIf(txtFechaHasta.Fecha.Trim().Length = 0, DBNull.Value, txtFechaHasta.Fecha)
                ' End If
                .Item("emba_tipo") = cmbTipo.Valor
            .Item("emba_obse") = txtObse.Valor
            .Item("emba_fecha_mov") = txtFechaPresentacion.Fecha
            If EstaLevantado() Then

                If GetEmbargo() = 0 And (GetLevanta() = GetTotal()) Then
                    .Item("emba_fina_fecha") = Date.Now.ToShortDateString()
                    cmbTipoMovimiento.Valor = "L"
                End If

            End If


        End With
        mdsDatos.AcceptChanges()

        lblAlta.Text = ""
    End Sub

    Private Sub mVerificarProducto()
        'verificar que el producto  seleccionado pueda ser embargado.
        Dim lstrfiltro As String
        Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())

        If usrProdFilDeta.Valor <> 0 Then 'And Not txtFechaDesde.Fecha Is DBNull.Value Then
            lstrfiltro = " @cria_id=" + clsSQLServer.gFormatArg(usrCria.Valor, SqlDbType.Int)
            lstrfiltro = lstrfiltro + ",@prdt_id=" + clsSQLServer.gFormatArg(usrProdFilDeta.Valor, SqlDbType.Int)
            lstrfiltro = lstrfiltro + ",@tipo='E'"



            If SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "productos_prop_prestamos_embargos", "resul", lstrfiltro) = "S" Then
                Throw New AccesoBD.clsErrNeg("El producto esta embargado.")
            End If
            ' Else
            '   Throw New AccesoBD.clsErrNeg("Debe seleccionar un producto y la fecha de inicio del embargo.")
        End If
    End Sub
    'Seccion de detalle
    Private Sub mActualizarDeta()
        Try
            mValidaDatos(False)
            mVerificarProducto()
            mGuardarDatosDeta()

            mLimpiarDeta()

            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Protected Sub mActualizarGrilla(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

        Dim oEmbargo As New SRA_Neg.Embargos(mstrConn, Session("sUserId").ToString())
        Dim cant As Integer = 0
        For Each oDataItem As DataGridItem In grdDetalle.Items


            If UCase(oDataItem.Cells(5).Text) = "INGRESADO FINALIZADO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If

            If UCase(oDataItem.Cells(5).Text) = "INGRESADO ACTIVO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = True
                If oEmbargo.GetEmbargoErrorByProdId(oDataItem.Cells(10).Text) Then

                    oDataItem.Cells(5).Text = "INGRESADO RETENIDO"
                End If
            End If
            If UCase(oDataItem.Cells(5).Text) = "LEVANTA" And IsDate(UCase(oDataItem.Cells(8).Text)) Then
                If Convert.ToDateTime((UCase(oDataItem.Cells(8).Text))) > System.DateTime.Now Then
                    CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
                    oDataItem.Cells(5).Text = "INGRESADO ACTIVO"
                Else
                    CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = True
                    oDataItem.Cells(5).Text = "INGRESADO FINALIZADO"
                End If

            End If
            If UCase(oDataItem.Cells(5).Text) = "ACTIVO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If

        Next

    End Sub
    Private Sub mBajaDeta()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("emde_id=" & hdnDetaId.Text)(0)
            row.Delete()
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()
            mLimpiarDeta()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mGuardarDatosDeta()
        Dim ldrDeta As DataRow
        Dim lstrFiltro As String
        Dim lstrResp As String
        Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim intEstadoIdAnterior As Integer
        Dim dtProducto As DataTable



        dtProducto = oProducto.GetProductoById(usrProdFilDeta.Valor, "Producto")

        If dtProducto.Rows.Count > 0 Then
            intEstadoIdAnterior = ValidarNulos(dtProducto.Rows(0).Item("prdt_esta_id"), False)
        End If



        For Each lDr As DataRow In _
                           mdsDatos.Tables(mstrTablaDetalle).Select("emde_prdt_id=" & _
                           usrProdFilDeta.Valor)
            If txtFechaFinalProducto.Text <> "" Then
                lDr.Item("_estado") = "Levanta"
                lDr.Item("emde_fina_fecha") = txtFechaFinalProducto.Text
            Else

                lDr.Item("_estado") = "Embargo"
                lDr.Item("emde_fina_fecha") = System.DBNull.Value
            End If


        Next




        lstrFiltro = " @cria_id=" + clsSQLServer.gFormatArg(usrCria.Valor, SqlDbType.Int)
        lstrFiltro = lstrFiltro + ",@prdt_id=" + clsSQLServer.gFormatArg(usrProdFilDeta.Valor, SqlDbType.Int)
        lstrFiltro = lstrFiltro + ",@fecha_ini=" + clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime)


    End Sub
    Private Sub mValidarDatosDeta()

        mValidaGrupoHBA()
        mValidaCantProdUnico()

    End Sub
    Private Sub mValidarFechaFinalizacion()
        If GetEmbargo() = 0 And (GetLevanta() = GetTotal()) Then
            If txtFechaHasta.Text Is System.DBNull.Value Then

                Throw New AccesoBD.clsErrNeg("La Fecha de Finalizacion  del Embargo ." + _
                       " no puede estar nula dado que el/los productos embargados ya Fueron Levantados.")

            End If
        End If

        If GetEmbargo() <> 0 Or (GetLevanta() <> GetTotal()) Then
            If txtFechaHasta.Text <> "" Then

                Throw New AccesoBD.clsErrNeg("La Fecha de Finalizacion  del Embargo ." + _
                       " no puede estar Cargada dado que aun hay Productos Embargados")

            End If
        End If
    End Sub

    Private Sub mValidaCantProdUnico()

        Dim intCant As Integer
        If cmbTipo.Valor = mProdUno Then

            If txtFechaDesde.Text = "" Then

                Throw New AccesoBD.clsErrNeg("La Fecha de Inicio  del Embargo ." + _
                                       " no puede estar vacio.")

            End If


            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Embargo'")
                intCant = intCant + 1
                If Not mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_fina_fecha") Is System.DBNull.Value And _
                    Not lDr.Item("emde_fina_fecha") Is System.DBNull.Value Then

                    If mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_fina_fecha") < lDr.Item("emde_fina_fecha") Then
                        Throw New AccesoBD.clsErrNeg("La Fecha de Finalizacion  del Embargo del Producto." + _
                                            " no puede ser Superior  a la Fecha  de Finalizacion del Embargo Total.")
                    End If
                End If

                If Not mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_inic_fecha") Is System.DBNull.Value And _
                                  Not lDr.Item("emde_fina_fecha") Is System.DBNull.Value Then
                    If mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_inic_fecha") > lDr.Item("emde_fina_fecha") Then
                        Throw New AccesoBD.clsErrNeg("La Fecha de Finalizacion  del Embargo del Producto." + _
                                           " no puede ser Inferior  a la Fecha  de Inicio del Embargo Total.")
                    End If
                End If


            Next

            If hdnId.Text <> "" Then
                If GetEmbargo() = 0 And (GetLevanta() = GetTotal()) Then
                    mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_fina_fecha") = Now.ToString("dd/MM/yyyy HH:mm")
                End If

            Else
                If intCant > 1 Then
                    Throw New AccesoBD.clsErrNeg("Solo se puede embargar 1 producto." + _
                    " De lo contrario cambie el tipo de embargo.")
                End If

                If intCant = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe al menos embargar 1 producto." + _
                    " De lo contrario cambie el tipo de embargo.")
                End If
            End If
        End If

        mdsDatos.AcceptChanges()

    End Sub

    Private Sub mValidaGrupoHBA()
        Dim intCant As Integer = 0

        If txtFechaDesde.Text = "" Then

            Throw New AccesoBD.clsErrNeg("La Fecha de Inicio  del Embargo ." + _
                                   " no puede estar vacio.")

        End If

        If cmbTipo.Valor = mProdConjun Then

            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Embargo'")

                intCant = intCant + 1

            Next

            If hdnId.Text <> "" Then
                If GetEmbargo() = 0 And (GetLevanta() = GetTotal()) Then

                    '  mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_baja_fecha") = _
                    '                 Now.ToString("dd/MM/yyyy HH:mm")


                    '  lblBaja.Text = "Registro dado de baja en fecha: " & _
                    'mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_baja_fecha") = _
                    'Now.ToString("dd/MM/yyyy HH:mm")

                    mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_fina_fecha") = _
                    Now.ToString("dd/MM/yyyy HH:mm")



                End If

            Else
                If intCant < 2 Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar mas de 1 producto. De lo contrario cambie el tipo de embargo.")
                End If
            End If
        End If
        mdsDatos.AcceptChanges()



    End Sub


#End Region

#Region "Eventos de Controles"
    Private Sub grdDetalle_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDetalle.ItemCommand

        Try
            Dim strPrestamoDetaId As String = String.Empty
            Dim lsbMsg As New StringBuilder
            Dim lsbPagina As New System.Text.StringBuilder

            If e.CommandName = "Select" Then
                strPrestamoDetaId = e.Item.Cells.Item(1).Text

                lsbPagina.Append("errores.aspx?tabla=embargos_errores&proce=" + mintProce.ToString() + "&ID=" + strPrestamoDetaId)
                clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 600, 250, 100, 100)

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub


    Private Sub cmbTipo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTipo.SelectedIndexChanged
        mDetalle()
        If cmbTipo.Valor = mProdTodos Then
            mCrearDataSet("")
        End If
    End Sub

    Public Sub messageBox(ByVal message As String, ByVal page As Object)
        Dim lbl As New Label
        lbl.Text = """"
        page.Controls.Add(lbl)
    End Sub
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panDato.Visible = True
            panBotones.Visible = True
            btnAgre.Visible = False

            panFiltro.Visible = False
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panDetalle.Visible = False
            lnkDetalle.Font.Bold = True
            panDetalle.Enabled = True
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    panDetalle.Visible = True
                    lnkDetalle.Font.Bold = True
                    If btnAlta.Enabled = True Then
                        mConsultarDeta()
                    End If


            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mstrTramiteId = Request.QueryString("Tram_Id")
        If mstrTramiteId <> "" Then
            Response.Write("<Script>window.close();</script>")
        End If
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click

        mAgregar()
        txtFechaPresentacion.Fecha = Now
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar(False)
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub

    'Botones Generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
        mShowTabs(2)
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintFechaD As Integer
            Dim lintFechaH As Integer

            If cmbTipoReporte.SelectedValue = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el Tipo de Reportes.")
            End If

            If cmbTipoReporte.SelectedValue = "R" Then
                lstrRptName = "EmbargosResumido"
            Else
                lstrRptName = "EmbargosCompleto"
            End If


            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            lstrRpt += "&emba_clie_id=0"
            If txtNroEmbargoFil.Text <> "" Then
                lstrRpt += "&emba_id=" + txtNroEmbargoFil.Text
            Else
                lstrRpt += "&emba_id=0"
            End If


            lstrRpt += "&emba_cria_id=" + IIf(usrProdFil.CriaOrPropId.ToString = "", "0", usrProdFil.CriaOrPropId.ToString)
            If cmbTipoFil.SelectedIndex <> 0 Then
                lstrRpt += "&emba_tipo=" + cmbTipoFil.Valor.ToString()

            End If

            If cmbTipoMovimientoFil.SelectedIndex <> 0 Then
                lstrRpt += "&tipo_movimiento=" + cmbTipoMovimientoFil.Valor.ToString()
            End If


            If txtFechaDesdeFil.Text = "" Then
                lintFechaD = 0
            Else
                lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaHastaFil.Text = "" Then
                lintFechaH = 0
            Else
                lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If

            lstrRpt += "&fecha_desde=" + lintFechaD.ToString()
            lstrRpt += "&fecha_hasta=" + lintFechaH.ToString()


            If usrProdFil.txtSraNumeExt.Valor.ToString <> "" Then
                lstrRpt += "&sra_nume=" + usrProdFil.txtSraNumeExt.Valor.ToString()
            Else
                lstrRpt += "&sra_nume=0"

            End If


            If usrProdFil.cmbSexoProdExt.Valor.ToString <> "" Then
                lstrRpt += "&sexo=" + usrProdFil.cmbSexoProdExt.Valor.ToString()
            End If

            If usrProdFil.RazaId <> "" Then
                lstrRpt += "&raza_id=" + usrProdFil.RazaId
            Else
                lstrRpt += "&raza_id=0"

            End If


            If usrProdFil.txtProdNombExt.Text <> "" Then
                lstrRpt += "&NombreProd=" + usrProdFil.txtProdNombExt.Text
            End If


            If usrProdFil.txtRPExt.Text <> "" Then
                lstrRpt += "&RP=" + usrProdFil.txtRPExt.Text
            Else

                lstrRpt += "&RP=0"
            End If


            If usrProdFil.RazaId <> "" Then
                Dim objRazas As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
                lstrRpt += "&Raza_codi=" + objRazas.GetRazaCodiById(usrProdFil.RazaId).ToString()

            Else
                lstrRpt += "&Raza_codi=0"

            End If

            If usrProdFil.CriaOrPropId <> "" Then
                Dim objCriadores As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
                lstrRpt += "&Cria_codi=" + objCriadores.GetCriaNumeByCriadorId(usrProdFil.CriaOrPropId).ToString()

            Else
                lstrRpt += "&Cria_codi=0"

            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)



            Response.Redirect(lstrRpt)



        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Funciones privadas"
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "emba_obse")
    End Sub


    Public Sub chkSelCheck_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Written into several lines to clarify, you could go with one line
        'Current checkbox
        Dim box As CheckBox = CType(sender, CheckBox)
        'The TableCell the control is in
        Dim cell As TableCell = CType(box.Parent, TableCell)
        'The DataGridItem the cell belongs to
        Dim dgItem As DataGridItem = CType(cell.Parent, DataGridItem)

        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim oEmbargo As New SRA_Neg.Embargos(mstrConn, Session("sUserId").ToString())

        Dim intProductoId As Integer
        Dim dtProducto As DataTable
        Dim intEstadoIdAnterior As Integer
        Dim intHBA As Integer
        Dim intRazaId As Integer
        Dim Sexo As String
        Dim lngProdId As Long = 0
        Dim lngEmbaId As Long = 0

        Dim dtProdSeleccionado As DataTable

        'With that DataGridItem you get for example ItemIndex of the DataGridItem you
        'are into. It is the same would be e.Item in ItemCommand,ItemDataBound or
        'ItemCreated. And you can then again get the PK of the current row and so on
        'via DataKeys collection.

        If lblAlta.Text = "A" And cmbTipo.Valor = mProdTodos Then
            CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True

            For Each lDr As DataRow In _
                        mdsDatos.Tables(mstrTablaDetalle).Select()

                lDr.Item("_estado") = "Embargo"
                lDr.Item("emde_fina_fecha") = System.DBNull.Value  ' se nodifico posicion de grilla
                grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO ACTIVO"
            Next
        End If


        If CType(sender, System.Web.UI.WebControls.CheckBox).Checked Then

            ' Si esta tildado esta Embargado 

            For Each lDr As DataRow In _
            mdsDatos.Tables(mstrTablaDetalle).Select("_pos=" & _
            grdDetalle.Items(dgItem.ItemIndex).Cells(7).Text)

                lngProdId = lDr.Item("emde_prdt_id")
                lngEmbaId = lDr.Item("emde_emba_id")


                dtProdSeleccionado = oEmbargo.GetEmbargoDetalleByProdId(lngEmbaId, lngProdId, "")
                If dtProdSeleccionado.Rows.Count > 0 Then
                    If ValidarNulos(dtProdSeleccionado.Rows(0).Item("emde_fina_fecha"), False).ToString() <> "0" And _
txtFechaHasta.Text <> "" Then
                        AccesoBD.clsError.gGenerarMensajes(Me, "El Producto ya se habia levantado Anteriormente")
                        Exit Sub

                    Else
                        lDr.Item("_estado") = "Embargo"
                        lDr.Item("emde_fina_fecha") = System.DBNull.Value
                        grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = "" '' se nodifico posicion de grilla
                        grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO ACTIVO"
                        CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True
                    End If
                Else
                    ' no lo encuantra porque no estaba embargado

                    grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = "" '' se nodifico posicion de grilla

                    lDr.Item("_estado") = "Embargo"
                    lDr.Item("emde_fina_fecha") = System.DBNull.Value
                    grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = "" '' se nodifico posicion de grilla
                    grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO ACTIVO"
                    CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True
                End If

            Next

        Else

            grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = DateTime.Now.ToShortDateString()  ' se nodifico posicion de grilla

            For Each lDr As DataRow In _
            mdsDatos.Tables(mstrTablaDetalle).Select("_pos=" & _
               grdDetalle.Items(dgItem.ItemIndex).Cells(7).Text)

                lDr.Item("_estado") = "Levanta"
                lngProdId = lDr.Item("emde_prdt_id")
                lngEmbaId = lDr.Item("emde_emba_id")
                lDr.Item("emde_fina_fecha") = grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text  ' se nodifico posicion de grilla
                grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO FINALIZADO"
                dtProducto = oEmbargo.GetEmbargoDetalleByProdId(lngEmbaId, lngProdId, "")
                If dtProducto.Rows.Count > 0 Then
                    intEstadoIdAnterior = ValidarNulos(dtProducto.Rows(0).Item("prdt_esta_id"), False)
                    ldr.Item("emde_prdt_esta_id_Ant") = intEstadoIdAnterior
                End If

            Next


        End If

        mdsDatos.AcceptChanges()
        mActualizarEtiq()
        Session(mstrTabla) = mdsDatos
    End Sub

    Private Sub mDetalle()

        If cmbTipo.Valor = mProdTodos Then
            btnSeleccionarTodos.Enabled = False
            btnDeSeleccionarTodos.Enabled = False

            ' lnkDetalle.Enabled = False
        Else
            btnSeleccionarTodos.Enabled = True
            btnDeSeleccionarTodos.Enabled = True

        End If

    End Sub
    Private Sub mLimpiarActivos()

        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Activo'")

            lDr.Delete()

        Next
        mdsDatos.AcceptChanges()
    End Sub





    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click

        Dim strFitro As String



        If usrProdFilDeta.txtSraNumeExt.Valor.ToString <> "" Then
            strFitro = strFitro + " _hba = " + usrProdFilDeta.txtSraNumeExt.Valor.ToString + " and "
        End If


        If usrProdFilDeta.txtRPExt.Text.ToString <> "" Then
            strFitro = strFitro + " _rp = '" + usrProdFilDeta.txtRPExt.Text.ToString + "' and "
        Else
            If txtRPBusq.Text.ToString <> "" Then
                strFitro = strFitro + " _rp = '" + txtRPBusq.Text.ToString + "' and "
            End If
        End If

        If usrProdFilDeta.RazaId.ToString <> "" Then
            strFitro = strFitro + " _RazaId = " + usrProdFilDeta.RazaId.ToString + " and "
        End If

        If txtFechaFinalProducto.Text <> "" Then
            strFitro = strFitro + " emde_fina_fecha = '" + txtFechaFinalProducto.Text + "' and "
        End If

        If usrProdFilDeta.cmbSexoProdExt.Valor.ToString <> "" Then
            strFitro = strFitro + " _sexo = " + _
            IIf(usrProdFilDeta.cmbSexoProdExt.Valor.ToString = "1", "'Macho'", "'Hembra'") + " and "
        End If

        If usrProdFilDeta.txtProdNombExt.Valor.ToString <> "" Then
            strFitro = strFitro + " _nombre like '%" & _
            usrProdFilDeta.txtProdNombExt.Valor.ToString & "%' and "
        End If

        strFitro = strFitro + "1=1"

        mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = strFitro

        If strFitro = "1=1" Then
            mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = ""
        End If

        With mdsDatos.Tables(mstrTablaDetalle)
            .DefaultView.Sort = "_estado desc"
            grdDetalle.DataSource = .DefaultView

            ' the currentpageindex must be >= 0 and < pagecount<BR>   
            Try

                grdDetalle.DataBind()
                grdDetalle.Visible = True

            Catch
                Try
                    grdDetalle.CurrentPageIndex = 0
                    grdDetalle.DataBind()
                    grdDetalle.Visible = True

                Catch
                    ' Me.lblError.Text = "No data for selection"
                    ' Me.lblError.Visible = True

                End Try

            End Try


        End With


    End Sub

    Private Sub btnSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionarTodos.Click
        mChequearTodos()

        mActualizarEtiq()

    End Sub
    Private Sub mChequearTodos()
        messageBox("El estado debe ser distinto de Levanta", Me)


        mActualizarCHK(True)

        For Each oDataItem As DataGridItem In grdDetalle.Items
            If UCase(oDataItem.Cells(7).Text) <> "EMBARGO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If
            If UCase(oDataItem.Cells(7).Text) <> "LEVANTA" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = True
            End If
        Next

        mdsDatos.AcceptChanges()
        mActualizarEtiq()
    End Sub
    Private Sub mActualizarCHK(ByVal boolCHKTodos As Boolean)
        If boolCHKTodos Then
            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                lDr.Item("_CHK") = "1"
                lDr.Item("_estado") = "Embargo"
            Next
        Else
            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                lDr.Item("_CHK") = "0"
                lDr.Item("_estado") = "Levanta"
            Next
        End If



        mdsDatos.AcceptChanges()
        mActualizarEtiq()
    End Sub

    Private Sub mDesChequearTodos()

        mActualizarCHK(False)

        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado<>'Levanta'")
            lDr.Item("_estado") = "Embargo"
        Next

        For Each oDataItem As DataGridItem In grdDetalle.Items

            If UCase(oDataItem.Cells(7).Text) <> "Levanta" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If
        Next
        mdsDatos.AcceptChanges()

        mActualizarEtiq()
    End Sub


    Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click

        mGuardarDatosDeta()

        mLimpiarDeta()

        grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
        grdDetalle.DataBind()
    End Sub

    Private Sub btnDeSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeSeleccionarTodos.Click
        mDesChequearTodos()
    End Sub

    Public Function MuestraEstado(ByVal objText1 As Object) As String

        Dim dtEstado As DataTable
        Dim sReturn As String = ""

        Dim oEstado As New SRA_Neg.Estado(strConn, 1)

        dtEstado = oEstado.GetEstadoById(objText1.ToString(), "")
        If dtEstado.Rows.Count > 0 Then
            sReturn = dtEstado.Rows(0).Item("ESTA_DESC").ToString()
        End If

        Return sReturn
    End Function
    Public Function MuestraRaza(ByVal objText1 As Object) As String


        Dim dtRaza As DataTable
        Dim sReturn As String = ""

        Dim intRetornoRaza As Integer

        Dim oRaza As New SRA_Neg.Razas(mstrConn, 1)

        intRetornoRaza = oRaza.GetRazaCodiById(objText1.ToString())
        oRaza.GetRazaDescripcionById(intRetornoRaza)
        Return sReturn
    End Function



    Private Function GetActivos() As Integer

        Dim iRetorno As Integer = 0


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Activo'")
            iRetorno = iRetorno + 1
        Next




        Return iRetorno
    End Function

    Private Function GetLevanta() As Integer

        Dim iRetorno As Integer = 0


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Levanta'")
            iRetorno = iRetorno + 1
        Next

        Return iRetorno
    End Function

    Private Function GetEmbargo() As Integer

        Dim iRetorno As Integer = 0


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Embargo'")
            iRetorno = iRetorno + 1
        Next

        Return iRetorno
    End Function

    Private Function GetTotal() As Integer

        Dim iRetorno As Integer = 0


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
            iRetorno = iRetorno + 1
        Next

        Return iRetorno
    End Function
#End Region
End Class

End Namespace
