Module Module1
   Private mstrConn As String
   Private mstrUserId As String

   Sub Main()
      mEnviarMailAlertas()
   End Sub

   Private Sub mEnviarMailAlertas()
      Try
         mEscribir("1")
         mVerificarConexion()
         mEscribir("2")

         SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, mstrUserId, "0")

         mEscribir("Fin del envio")

      Catch ex As Exception
         mEscribir(ex.Message)
         mEscribir(ex.StackTrace)
         clsError.gManejarError(ex)
      End Try
   End Sub

   Private Sub mVerificarConexion()
      Dim lstrServ As String = System.Configuration.ConfigurationSettings.AppSettings("conServ").ToString()
      Dim lstrBase As String = System.Configuration.ConfigurationSettings.AppSettings("conBase").ToString()
      Dim lstrUser As String = System.Configuration.ConfigurationSettings.AppSettings("conUser").ToString()
      Dim lstrPass As String = System.Configuration.ConfigurationSettings.AppSettings("conPass").ToString()
      Dim lstrConn As String = ""

      mstrUserId = System.Configuration.ConfigurationSettings.AppSettings("conAutoUsuario").ToString()

      lstrConn = "server=" + lstrServ + ";database=" + lstrBase
      lstrConn = lstrConn + ";uid=" + lstrUser + ";pwd=" + lstrPass + ""
      'lstrConn = lstrConn + "Integrated Security=SSPI"
      Dim myConnection As New SqlClient.SqlConnection(lstrConn)
      mstrConn = lstrConn
      myConnection.Open()
      myConnection.Close()
   End Sub

   Private Sub mEscribir(ByVal pDatos As String)
      If System.Configuration.ConfigurationSettings.AppSettings("conGrabarLog").ToString = "1" Then
         Dim lstrArchLog As String = System.Configuration.ConfigurationSettings.AppSettings("conArchLog").ToString.Replace("errores", "aplicacion")
         Dim fs As New IO.FileStream(lstrArchLog, IO.FileMode.OpenOrCreate, IO.FileAccess.ReadWrite)

         Dim w = New IO.StreamWriter(fs)
         w.BaseStream.Seek(0, IO.SeekOrigin.End)

         w.Write(clsFormatear.CrLf() & "Fecha: " & DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") & clsFormatear.CrLf())
         w.Write(pDatos & clsFormatear.CrLf())
         w.Write("------------------------------------------------------------------------" & clsFormatear.CrLf())

         w.Flush()
         w.Close()
      End If
   End Sub
End Module