Namespace SRA

Partial Class CobranzasComprobACta_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
   Private mstrTablaAcred As String = "cobran_acred"

   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Chk = 0
      SactId = 1
      CompId = 2
      Numero = 3
      ImpoOri = 4
      Saldo = 5
      chkAuto = 8
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mdsDatos = Session(Request("sess"))

         mInicializar()

         If Not Page.IsPostBack Then
            lblSess.Text = Request("sess")
            usrClie.Valor = Request("clie_id")
            usrClie.Activo = (Request("tipo") = "")

            mConsultar()

            mCalcularTotales()
         Else
            If hdnLoginPop.Text <> "" Then
               lblAutoUsuaTit.Text = clsSQLServer.gObtenerValorCampo(mstrConn, "usuarios", hdnLoginPop.Text, "usua_user")
            Else
               lblAutoUsuaTit.Text = ""
            End If

            mGuardarDatosGrilla()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCerrar()
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
#End Region
#Region "Inicialización de Variables"
   Private Sub mInicializar()
      lblCabeTotalAplic.Text = Request("totalAPagar")
      lblCabeTotalPago.Text = Request("totalPagado")
      lblCabeTotalDifer.Text = Request("diferencia")
   End Sub
#End Region
#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Dim lstrCmd As New StringBuilder
      Dim ds As DataSet
      Dim ldsDatos As DataSet

      lstrCmd.Append("exec saldos_pagar_consul")
      lstrCmd.Append(" @clie_id=")
      lstrCmd.Append(usrClie.Valor)

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      mHabilitarAutorizacion(ds)

      grdConsulta.DataSource = ds
      grdConsulta.DataBind()
      ds.Dispose()

   End Sub
#End Region

   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Try
         If lblMens.Text = "" Then
            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append("window.opener.document.all['hdnDatosPop'].value='-1';")
            lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub mCalcularTotales()
      Dim dTotal As Decimal
      Dim dTotalVenc As Decimal
      Dim dTotalSel As Decimal
      Dim lDr As DataRow

      For Each lDr In DirectCast(grdConsulta.DataSource, DataSet).Tables(0).Rows
         dTotal += lDr.Item("saldo")
      Next

            For Each lDr In mdsDatos.Tables(mstrTablaAcred).Select
                dTotalSel += lDr.Item("acre_impo")
            Next

            txtTotal.Valor = dTotal
      txtTotalSel.Valor = dTotalSel
   End Sub

   Private Sub grdConsulta_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdConsulta.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 Then
            Dim ldrRow As DataRow
            Dim lchkSel As CheckBox = e.Item.FindControl("chkSel")
            Dim lchkAuto As CheckBox = e.Item.FindControl("chkAuto")
            Dim ltxtPagado As NixorControls.NumberBox = e.Item.FindControl("txtPagado")
            Dim lstrFiltro As String

            ldrRow = CType(e.Item.DataItem, DataRowView).Row

            If Not ldrRow.IsNull("comp_id") Then
               lstrFiltro = "acre_apli_comp_id=" & ldrRow.Item("comp_id")
            Else
               lstrFiltro = "acre_sact_id=" & ldrRow.Item("sact_id")
            End If

            lchkAuto.Visible = ldrRow.Item("_auto")

            lchkSel.Enabled = (ldrRow.Item("sact_id") <> 0)

            With mdsDatos.Tables(mstrTablaAcred).Select(lstrFiltro)
               If .GetUpperBound(0) = -1 Then
                  lchkSel.Checked = False
                  lchkAuto.Checked = False
                  ltxtPagado.Valor = 0
                  ltxtPagado.Enabled = False
                  ltxtPagado.CssClass = "cuadrotextodeshab"
               Else
                  With DirectCast(.GetValue(0), DataRow)
                     lchkSel.Checked = True
                                ltxtPagado.Valor = IIf(.Item("acre_impo") = DBNull.Value, "0", CDec(.Item("acre_impo")).ToString("######0.00"))
                                ltxtPagado.Enabled = True
                                ltxtPagado.CssClass = "cuadrotexto"

                     lchkAuto.Checked = Not .IsNull("acre_auto_usua_id")
                  End With
               End If
            End With
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosGrilla()
      Dim ldrDatos As DataRow
      Dim lbooSelec As Boolean
      Dim ldecPagado, ldecSaldo As Decimal
      Dim lstrFiltro As String
      Dim lintAutoUsuaId, lintAutoAutiId As Integer

      For Each oItem As DataGridItem In grdConsulta.Items
         lintAutoUsuaId = 0
         lintAutoAutiId = 0
         lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked

         If IsNumeric(oItem.Cells(Columnas.CompId).Text) Then
            lstrFiltro = "acre_apli_comp_id=" & oItem.Cells(Columnas.CompId).Text
         Else
            lstrFiltro = "acre_sact_id=" & oItem.Cells(Columnas.SactId).Text
         End If

         With mdsDatos.Tables(mstrTablaAcred).Select(lstrFiltro)
            If .GetUpperBound(0) <> -1 Then
               ldrDatos = .GetValue(0)
               If ldrDatos.IsNull("acre_id") Then 'si solo se guardó la autorización, guardo el dato y borro el registro
                  If Not ldrDatos.IsNull("acre_auto_usua_id") Then
                     lintAutoUsuaId = ldrDatos.Item("acre_auto_usua_id")
                     lintAutoAutiId = ldrDatos.Item("acre_auto_auti_id")
                  End If

                  mdsDatos.Tables(mstrTablaAcred).Rows.Remove(ldrDatos)
                  ldrDatos = Nothing
               End If
            End If
         End With

                ldecPagado = IIf(DirectCast(oItem.FindControl("txtPagado"), NixorControls.NumberBox).Valor.Trim().Length = 0, 0, DirectCast(oItem.FindControl("txtPagado"), NixorControls.NumberBox).Valor)
                ldecSaldo = oItem.Cells(Columnas.Saldo).Text

         If lbooSelec And ldecPagado = 0 Then
            lbooSelec = False
         End If

         If lbooSelec Then
            If ldecPagado > ldecSaldo Then
               Throw New AccesoBD.clsErrNeg("El importe ingresado supera el saldo disponible.")
            End If

            If lintAutoUsuaId = 0 And oItem.FindControl("chkAuto").Visible Then
               Throw New AccesoBD.clsErrNeg("Debe autorizar el uso del saldo seleccionado.")
            End If

            If ldrDatos Is Nothing Then
               ldrDatos = mdsDatos.Tables(mstrTablaAcred).NewRow
               With ldrDatos
                  .Item("acre_id") = clsSQLServer.gObtenerId(.Table, "acre_id")
                  .Item("acre_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")

                  If IsNumeric(oItem.Cells(Columnas.SactId).Text) Then
                     .Item("acre_sact_id") = oItem.Cells(Columnas.SactId).Text
                  Else
                     .Item("acre_apli_comp_id") = oItem.Cells(Columnas.CompId).Text
                  End If

                  .Item("acre_desc") = oItem.Cells(Columnas.Numero).Text
                  If usrClie.Valor.ToString <> Request("clie_id").ToString Then
                     .Item("acre_desc") += " (" + usrClie.Apel.ToString + ")"
                  End If

                  .Item("acre_auto_usua_id") = IIf(lintAutoUsuaId = 0, DBNull.Value, lintAutoUsuaId)
                  .Item("acre_auto_auti_id") = IIf(lintAutoAutiId = 0, DBNull.Value, lintAutoAutiId)

                  .Table.Rows.Add(ldrDatos)
               End With
            End If

            With ldrDatos
               .Item("acre_impo") = DirectCast(oItem.FindControl("txtPagado"), NixorControls.NumberBox).Valor
            End With
         Else
            If Not ldrDatos Is Nothing Then
               ldrDatos.Delete()
            End If
         End If

         ldrDatos = Nothing
      Next
   End Sub
   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub usrClie_Cambio(ByVal sender As Object) Handles usrClie.Cambio
      Try
         mConsultar()
         mCalcularTotales()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mHabilitarAutorizacion(ByVal pDs As DataSet)
      Dim lbooPideAuto As Boolean
      Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "usuarios_actividades", "@usua_id=" + Session("sUserId").ToString())

      pDs.Tables(0).Columns.Add("_auto", System.Type.GetType("System.Int32"))

      For Each lDr As DataRow In pDs.Tables(0).Select
         If Not lDr.IsNull("sact_id") And Not lDr.IsNull("acti_id") Then
            If ldsDatos.Tables(0).Select("usac_acti_id=" + lDr.Item("acti_id").ToString).GetUpperBound(0) = -1 Then
               lDr.Item("_auto") = True
               lbooPideAuto = True
            End If
         End If
         If lDr.IsNull("_auto") Then
            lDr.Item("_auto") = False
         End If
      Next


      lblAutoUsuaTit.Visible = lbooPideAuto
      btnAutoUsua.Visible = lbooPideAuto
      grdConsulta.Columns(Columnas.chkAuto).Visible = lbooPideAuto

      If Not lbooPideAuto Then
         lblAutoUsuaTit.Text = ""
      End If
   End Sub
End Class
End Namespace
