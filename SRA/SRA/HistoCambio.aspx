<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.HistoCambio" CodeFile="HistoCambio.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Hist�rico de Cambios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mCargarProvincias(pPais)
		{
		    var sFiltro = pPais.value;
		    LoadComboXML("provincias_cargar", sFiltro, "cmbDirePcia", "S");
		}
		function mCargarLocalidades(pPcia)
		{
		    var sFiltro = pPcia.value 
		    LoadComboXML("localidades_cargar", sFiltro, "cmbDireLoca", "S");
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"></asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
											BorderStyle="Solid">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="6"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 72%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																				<asp:Label id="lblFHastaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 10%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblSociFil" runat="server" cssclass="titulo">Socio:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrSociFil" runat="server" width="100%" FilCUIT="True" Tabla="Socios" Saltos="1,1,1"
																					Autopostback="false" FilSociNume="True" PermiModi="True" MuestraDesc="False" FilDocuNume="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnUpdateCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="moso_id" ReadOnly="True" HeaderText="moso_id"></asp:BoundColumn>
												<asp:BoundColumn DataField="moso_movi_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
													<HeaderStyle Width="30%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_cate_desc" ReadOnly="True" HeaderText="Categor&#237;a"></asp:BoundColumn>
												<asp:BoundColumn DataField="_dist_desc" ReadOnly="True" HeaderText="Distrito"></asp:BoundColumn>
												<asp:BoundColumn DataField="_esta_desc" HeaderText="Estado"></asp:BoundColumn>
												<asp:BoundColumn DataField="moso_apel" ReadOnly="True" HeaderText="Apellido/Raz�n Social"></asp:BoundColumn>
												<asp:BoundColumn DataField="moso_nomb" ReadOnly="True" HeaderText="Nombre"></asp:BoundColumn>
												<asp:BoundColumn DataField="_acta" HeaderText="Nro.Acta"></asp:BoundColumn>
												<asp:BoundColumn DataField="_nombre_socio" HeaderText="Socio"></asp:BoundColumn>
												</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10" align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
											ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
											IncludesUrl="includes/" BackColor="Transparent"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD align="right"></TD>
								</tr>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
												BorderStyle="Solid" width="100%" Height="100%">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
																ToolTip="Cerrar"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 180px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblFech" runat="server" cssclass="titulo">Fecha Movim.:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<cc1:DateBox id="txtFech" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblCate" runat="server" cssclass="titulo">Categor�a:&nbsp;</asp:Label></TD>
																	<TD colSpan="2">
																		<cc1:combobox class="combo" id="cmbCate" runat="server" Width="281px"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblDist" runat="server" cssclass="titulo">Distrito:&nbsp;</asp:Label></TD>
																	<TD colSpan="2">
																		<cc1:combobox class="combo" id="cmbDist" runat="server" Width="281px"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:&nbsp;</asp:Label></TD>
																	<TD colSpan="2">
																		<cc1:combobox class="combo" id="cmbEsta" runat="server" Width="281px"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblApel" runat="server" cssclass="titulo">Apellido/Raz�n Social:</asp:Label></TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtApel" runat="server" cssclass="cuadrotexto" Width="100%"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:Label></TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="100%"></CC1:TEXTBOXTAB></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Eliminar"></asp:Button>&nbsp;
														</TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnSociId" runat="server"></asp:textbox><asp:textbox id="hdnTipo" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
		if (document.all("hdnTipo").value == 'C')
			gSetearTituloFrame('Hist�rico de Cambios de Categor�a');
		else
			gSetearTituloFrame('Hist�rico de Cambios de Estado');
		</SCRIPT>
	</BODY>
</HTML>
