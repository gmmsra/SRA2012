Namespace SRA

Partial Class SociMovimConsul
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_SociosMovim

   Private mstrConn As String
   Private mstrSociId As String
   Private mstrTipo As String
   Private mstrParaPageSize As Integer
	Public mstrTitulo As String
	Private mstrVisible As String
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      mstrSociId = Request.QueryString("soci_id")
		mstrTipo = Request.QueryString("tipo")
		'mstrVisible = Request.QueryString("Visible")

      Select Case mstrTipo
         Case "C"
            mstrTitulo = "CAMBIOS DE CATEGORIAS"
            DirectCast(grdDato.Columns(1), UI.WebControls.BoundColumn).DataField = "_cate_desc"
				grdDato.Columns(1).HeaderText = "Categoría"
				grdDato.Columns(3).Visible = True
         Case "E"
            mstrTitulo = "CAMBIOS DE ESTADOS"
            DirectCast(grdDato.Columns(1), UI.WebControls.BoundColumn).DataField = "_esta_desc"
				grdDato.Columns(1).HeaderText = "Estado"
				grdDato.Columns(3).Visible = True
         Case "N"
            mstrTitulo = "CAMBIOS DE NOMBRES"
            DirectCast(grdDato.Columns(1), UI.WebControls.BoundColumn).DataField = "_nombre"
				grdDato.Columns(1).HeaderText = "Apellido/Nombre"
				grdDato.Columns(3).Visible = False
      End Select
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mCerrar()
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @moso_soci_id=" + mstrSociId)
         lstrCmd.Append(", @tipo='" + mstrTipo + "'")

			clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

			'If mstrVisible <> "" Then
			'
			'End If

		Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class

End Namespace
