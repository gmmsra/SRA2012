Namespace SRA

Partial Class ProformasAnul
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents btnAgre As NixorControls.BotonImagen
    Protected WithEvents lblFDesdeEmi As System.Web.UI.WebControls.Label
    Protected WithEvents lblFDesdeValor As System.Web.UI.WebControls.Label

    Protected WithEvents panCriador As System.Web.UI.WebControls.Panel


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Proformas
    Private mstrConn As String
    Private mstrParaPageSize As Integer
    Public mbooRegis As Boolean

    Private Enum Columnas As Integer
        Id = 1
        Nro = 3
        Anular = 10

    End Enum
#End Region

#Region "Inicialización de Variables"


#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                mSetearEventos()
                mCargarCombos()

                clsWeb.gInicializarControles(Me, mstrConn)

            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnAnulReactiv.Attributes.Add("onclick", "if(!confirm('Confirma la baja de la proforma/s seleccionada/s?')) return false;")
    End Sub



#End Region


#Region "Opciones de ABM"
    Private Sub mModi()
        Try
            '  Dim ldsEstruc As DataSet = mGuardarDatos()
            ' ldsEstruc.Tables(mstrTablaSoli).DefaultView.Sort = "_soin_titu_clie_id"

            ' Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            ' lobjGenerico.Modi()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerico.Baja(hdnId.Text)

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub



    Private Function CrearDataSet(ByVal pstrId As String) As DataSet
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        ldsEsta.Tables(0).TableName = mstrTabla


        If mbooRegis Then
            With ldsEsta.Tables.Add(mstrTabla & "_proceso")
                .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
                .Columns.Add("proc_accd_id", System.Type.GetType("System.Int32"))
                .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
                .Rows.Add(.NewRow)
                .Rows(0).Item("proc_id") = 0
            End With
        End If

        Return ldsEsta
    End Function

    Private Sub mGuardarDatos()
        Dim lbooSelec As Boolean
        Dim lstrReactivIDS As String = ""
        Dim lstrAnulIDS As String = ""

        For Each oItem As DataGridItem In grdProfo.Items
            lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked
            If lbooSelec Then
                If oItem.Cells(Columnas.Anular).Text() = 1 Then
                    'proforma para anular 
                    lstrAnulIDS = lstrAnulIDS + IIf(lstrAnulIDS <> "", ",", "") + oItem.Cells(Columnas.Id).Text()
                Else
                    'proforma para reactivar
                    lstrReactivIDS = lstrReactivIDS + IIf(lstrReactivIDS <> "", ",", "") + oItem.Cells(Columnas.Id).Text()
                End If
            End If
        Next

        If lstrAnulIDS = "" And lstrReactivIDS = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar al menos una proforma.")
        End If

        clsSQLServer.gExecute(mstrConn, "exec dbo.proformas_anuladas_reactivadas_modi  @audi_user =" & Session("sUserId").ToString() & ", @prfr_anul_ids ='" & lstrAnulIDS & "', @prfr_reactiv_ids ='" & lstrReactivIDS & "'")
        mMostrarPanel(True)

    End Sub
    Public Sub mVerMasDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            'proformas

            Dim lsbPagina As New System.Text.StringBuilder
            lsbPagina.Append("Proforma_detalle_pop.aspx?")
            lsbPagina.Append("&id=" & E.Item.Cells(Columnas.Id).Text())
            lsbPagina.Append("&nro=" & E.Item.Cells(Columnas.Nro).Text())

            clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 700, 400, 100, 50)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub LimpiarTablaSoli(ByVal pdsEsta As DataSet)
        For Each oDataRow As DataRow In pdsEsta.Tables(mstrTabla).Select("sacd_id=0")
            pdsEsta.Tables(mstrTabla).Rows.Remove(oDataRow)
        Next
    End Sub

#End Region

#Region "Operacion Sobre la Grilla"



#End Region

    Private Sub btnBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mModi()
    End Sub
    Private Sub mValidar()
            'If Not (txtFDesdeEmi.Fecha Is DBNull.Value) And Not (txtFHastaEmi.Fecha Is DBNull.Value) Then
            If (txtFDesdeEmi.Text.Trim().Length > 0) And (txtFHastaEmi.Text.Trim().Length > 0) Then
                If txtFDesdeEmi.Fecha > txtFHastaEmi.Fecha Then
                    Throw New AccesoBD.clsErrNeg("La fecha de emisión Desde no puede ser mayor a la fecha Hasta.")
                End If
            End If

            'If Not (txtFDesdeValor.Fecha Is DBNull.Value) And Not (txtFHastaValor.Fecha Is DBNull.Value) Then
            If (txtFDesdeValor.Text.Trim().Length > 0) And (txtFHastaValor.Text.Trim().Length > 0) Then
                If txtFDesdeValor.Fecha > txtFHastaValor.Fecha Then
                    Throw New AccesoBD.clsErrNeg("La fecha valor Desde no puede ser mayor a la fecha Hasta.")
                End If
            End If
        End Sub

    Public Sub mConsultar()
        Try
            mValidar()

            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "X_consul")
            lstrCmd.Append("  @prfr_ingr_fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFDesdeEmi.Fecha))
            lstrCmd.Append(", @prfr_ingr_fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFHastaEmi.Fecha))
            lstrCmd.Append(", @prfr_fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFDesdeValor.Fecha))
            lstrCmd.Append(", @prfr_fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFHastaValor.Fecha))
            lstrCmd.Append(", @prfr_acti_id=" + cmbActi.Valor.ToString)
            lstrCmd.Append(", @prfr_clie_id=" + usrClieFil.Valor.ToString)
            lstrCmd.Append(", @prde_raza_id=" + cmbCriador.Valor.ToString)
            lstrCmd.Append(", @pran_aran_id=" + cmbArancel.Valor.ToString)
            lstrCmd.Append(", @prfr_nume=" + clsSQLServer.gFormatArg(txtNro.Valor, SqlDbType.Int))
            lstrCmd.Append(", @pran_tram=" + clsSQLServer.gFormatArg(txtTram.Valor, SqlDbType.Int))
            lstrCmd.Append(", @incluir_bajas = " + IIf(chkBaja.Checked, "1", "0"))

            If Session("sCentroEmisorId") Is Nothing Then
                lstrCmd.Append(", @emct_id=-1")
            Else
                lstrCmd.Append(", @emct_id=")
                lstrCmd.Append(Session("sCentroEmisorId"))
            End If

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdProfo)

            btnAnulReactiv.Enabled = (grdProfo.Items.Count > 0)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panFiltro.Visible = pbooVisi
        panDato.Visible = Not panDato.Visible



    End Sub



    Private Sub grdProfo_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            If e.Item.ItemIndex <> -1 Then
                Dim ochkSel As CheckBox = e.Item.FindControl("chkSel")

                With CType(e.Item.DataItem, DataRowView).Row
                    ochkSel.Checked = .Item("sacd_id") <> 0
                End With

            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        mConsultar()
    End Sub

    Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mMostrarPanel(True)
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ")
    End Sub

    Private Sub btnAnulReactiv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnulReactiv.Click
        Try

            mGuardarDatos()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        Try

            txtFDesdeEmi.Text = ""
            txtFHastaEmi.Text = ""
            txtFDesdeValor.Text = ""
            txtFHastaValor.Text = ""
            cmbActi.Limpiar()
            usrClieFil.Valor = ""

            txtNro.Valor = ""
            txtTram.Valor = ""
            chkBaja.Checked = True
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try


    End Sub
End Class
End Namespace
