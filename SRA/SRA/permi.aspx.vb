Namespace SRA

Partial Class permi
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents cmbOpci As NixorControls.ComboBox
    Public mstrTabla As String
    Public mstrTitu As String
    Public mstrCmd As String
   Public mstrWeb As Integer
   Private mstrConn As String


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

         mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            mEstablecerPerfil()
            If (Not Page.IsPostBack) Then

            clsWeb.gCargarRefeCmb(mstrConn, "perfiles", cmbProf, "")
            mCargarOpciones()
            mCargarPermisos()
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#Region "Operaciones sobre la Pagina"

   Private Sub mEstablecerPerfil()
      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Permisos, String), (mstrConn), (Session("sUserId").ToString()))) Then

         Response.Redirect("noaccess.aspx")
      End If
      dblOpciones.AltaVisible = clsSQLServer.gMenuPermi(CType(Opciones.Permisos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      dblOpciones.AltaMulVisible = dblOpciones.AltaVisible
      dblOpciones.BajaVisible = clsSQLServer.gMenuPermi(CType(Opciones.Permisos_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      dblOpciones.BajaMulVisible = dblOpciones.BajaVisible
   End Sub

   Public Sub mCargarPermisos()

      Try

         mstrCmd = "exec usua_permi_consul "
         mstrCmd += " " + clsSQLServer.gFormatArg(clsWeb.gValorCombo(cmbProf), SqlDbType.Int)

         clsWeb.gCargarCombo((mstrConn), mstrCmd, dblOpciones.lstDest, "codi", "deta")

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarOpciones()

      Try

         mstrCmd = "exec opciones_consul 'S'"

         clsWeb.gCargarCombo((mstrConn), mstrCmd, dblOpciones.lstOri, "codi", "deta", "")
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()

      mstrTabla = "usua_permi"
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta(ByVal lstrOpci As String)

      Try

         mstrCmd = "exec " + mstrTabla + "_alta "
         mstrCmd += " " + clsSQLServer.gFormatArg(cmbProf.SelectedItem.Value.ToString(), SqlDbType.Int)
         mstrCmd += "," + clsSQLServer.gFormatArg(lstrOpci, SqlDbType.VarChar)

         Dim lintExec As Integer = clsSQLServer.gExecute(mstrConn, mstrCmd)

         mCargarPermisos()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja(ByVal lstrOpci As String)

      Try

         mstrCmd = "exec " + mstrTabla + "_baja"
         mstrCmd += " " + clsSQLServer.gFormatArg(cmbProf.SelectedItem.Value.ToString(), SqlDbType.Int)
         mstrCmd += "," + clsSQLServer.gFormatArg(lstrOpci, SqlDbType.VarChar)

         Dim lintExec As Integer = clsSQLServer.gExecute(mstrConn, mstrCmd)

         mCargarPermisos()
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub dblOpciones_Alta_Click(ByVal sender As Object, ByVal pstrIds As String) Handles dblOpciones.Alta_Click, dblOpciones.AltaMul_Click
      mAlta(pstrIds)
   End Sub

   Private Sub dblOpciones_Baja_Click(ByVal sender As Object, ByVal pstrIds As String) Handles dblOpciones.Baja_Click, dblOpciones.BajaMul_Click
      mBaja(pstrIds)
   End Sub

   Private Sub cmbProf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProf.SelectedIndexChanged

      Try

         mCargarPermisos()
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class

End Namespace
