<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.NroTramite_pop" CodeFile="NroTramite_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion"></asp:label></TD>
								<TD vAlign="middle" align="right"><asp:imagebutton id="btnCerrar" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes/close3.bmp"></asp:imagebutton>&nbsp;</TD>
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panOtrosDatos" runat="server" Visible="true" cssclass="titulo" Width="100%"
										BorderWidth="1px" BorderStyle="Solid" height="100%">
										<TABLE class="FdoFld" id="tblOtrosDatos" style="WIDTH: 100%" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 100%" align="center" colSpan="4">
													<asp:datagrid id="grdConsulta" runat="server" BorderStyle="None" BorderWidth="1px" Visible="true"
														width="95%" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
														OnPageIndexChanged="DataGrid_Page" ItemStyle-Height="5px" AutoGenerateColumns="False">
														<FooterStyle CssClass="footer"></FooterStyle>
														<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
														<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
														<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
														<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
														<HeaderStyle Font-Size="X-Small" CssClass="header"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn>
																<HeaderStyle Width="5%"></HeaderStyle>
																<ItemTemplate>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
							<asp:textbox id="hdnClieId" runat="server"></asp:textbox>
							<asp:textbox id="hdnFecha" runat="server"></asp:textbox>
							<asp:textbox id="hdnSess" runat="server"></asp:textbox>
							<asp:textbox id="hdnTipoPop" runat="server"></asp:textbox></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
