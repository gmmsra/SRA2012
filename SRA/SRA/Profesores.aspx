<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Profesores" CodeFile="Profesores.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Profesores</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Profesores</asp:label></TD>
								</TR>
								<TR>
									<TD height="10"></TD>
									<TD vAlign="bottom" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<FooterStyle CssClass="footer"></FooterStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="9px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="prof_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="prof_desc" ReadOnly="True" HeaderText="Nombre y Apellido">
													<HeaderStyle Width="100%"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2">
										<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageDisable="btnNuev0.gif" ForeColor="Transparent"
														ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
														ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"
														ToolTip="Agregar un Nuevo Profesor"></CC1:BOTONIMAGEN></TD>
												<TD align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
														ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
														BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR height="10">
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle">
										<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD align="left"></TD>
												<TD></TD>
												<TD align="center" width="50"></TD>
											</TR>
										</TABLE>
									</TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Height="21px" CausesValidation="False" Width="80px"> Profesores</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td style="WIDTH: 38px" width="38" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkMail" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Height="21px" CausesValidation="False" Width="70px"> Mails</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td style="WIDTH: 38px" width="38" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkTele" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Height="21px" CausesValidation="False" Width="70px"> Telefonos</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td style="WIDTH: 38px" width="38" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDire" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Height="21px" CausesValidation="False" Width="70px"> Direcciones</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												Height="116px" Visible="False">
												<P align="right">
													<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
														border="0">
														<TR>
															<TD>
																<P></P>
															</TD>
															<TD height="5">
																<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
															<TD vAlign="top" align="right">&nbsp;
																<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="3">
																<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																	<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="HEIGHT: 16px" align="right" width="20%">
																				<asp:Label id="lblDesc" runat="server" cssclass="titulo">Nomb. y Apel.:</asp:Label>&nbsp;
																			</TD>
																			<TD style="HEIGHT: 16px" colSpan="2" height="16">
																				<CC1:TEXTBOXTAB id="txtNombApe" runat="server" cssclass="cuadrotexto" Width="299px" apeDesc Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="3">
																<asp:panel id="panMail" runat="server" cssclass="titulo" Width="100%" Visible="False">
																	<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdMail" runat="server" width="95%" AutoGenerateColumns="False" OnEditCommand="mEditarDatosMail"
																					OnPageIndexChanged="grdMail_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Left" AllowPaging="True" BorderWidth="1px" BorderStyle="None" Visible="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="mapr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="mapr_mail" HeaderText="Mail">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="mapr_refe" ReadOnly="True" HeaderText="Referencia">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="16"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblMail" runat="server" cssclass="titulo">Mail:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtMail" runat="server" cssclass="cuadrotexto" Width="292px" Obligatorio="True"
																					EsMail="True" Panel="Mail" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblMailRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtMailRefe" runat="server" cssclass="cuadrotexto" Width="411px" Obligatorio="True"
																					AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="center" colSpan="2" height="30">
																				<asp:panel id="PanBotones" runat="server" cssclass="titulo" Width="100%" Visible="False">
<asp:Button id="btnAltaMail" runat="server" cssclass="boton" Width="100px" Text="Agregar Mail"></asp:Button>&nbsp; 
<asp:Button id="btnBajaMail" runat="server" cssclass="boton" Width="100px" Text="Eliminar Mail"></asp:Button>&nbsp; 
<asp:Button id="btnModiMail" runat="server" cssclass="boton" Width="100px" Text="Modificar Mail"></asp:Button>&nbsp; 
<asp:Button id="btnLimpMail" runat="server" cssclass="boton" Width="100px" Text="Limpiar Mail"></asp:Button></asp:panel></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="3">
																<asp:panel id="PanTele" runat="server" cssclass="titulo" Width="100%" Visible="False">
																	<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdTele" runat="server" width="95%" AutoGenerateColumns="False" OnEditCommand="mEditarDatosTele"
																					OnPageIndexChanged="grdTele_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Left" AllowPaging="True" BorderWidth="1px" BorderStyle="None" Visible="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="tepr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_telefono" HeaderText="Telefono">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="tepr_refe" ReadOnly="True" HeaderText="Referencia">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="16"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 135px" align="right">
																				<asp:Label id="lblCodi" runat="server" cssclass="titulo">C�digo de �rea:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtCodi" runat="server" cssclass="cuadrotexto" Width="64px" Obligatorio="True"
																					EsMail="false" Panel="Mail" AceptaNull="False" MaxLength="5" MaxValor="10000"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 135px" align="right">
																				<asp:Label id="lblTele" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtTele" runat="server" cssclass="cuadrotexto" Width="112px" Obligatorio="True"
																					EsMail="false" Panel="Mail" AceptaNull="False" MaxLength="40"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 135px" align="right">
																				<asp:Label id="lblRefeTele" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtRefeTele" runat="server" cssclass="cuadrotexto" Width="364px" Obligatorio="True"
																					AceptaNull="False" MaxLength="50"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 135px" align="right">
																				<asp:Label id="Label1" runat="server" cssclass="titulo">Tipo de tel�fono:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbTeti" runat="server" Width="296px" AceptaNull="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="center" colSpan="2" height="30">
																				<asp:Button id="btnAltaTele" runat="server" cssclass="boton" Width="100px" Text="Agregar Tel"></asp:Button>&nbsp;
																				<asp:Button id="btnBajaTele" runat="server" cssclass="boton" Width="100px" Text="Eliminar Tel"></asp:Button>&nbsp;
																				<asp:Button id="btnModiTele" runat="server" cssclass="boton" Width="100px" Text="Modificar Tel"></asp:Button>&nbsp;
																				<asp:Button id="btnLimpTele" runat="server" cssclass="boton" Width="100px" Text="Limpiar Tel"></asp:Button></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="3">
																<asp:panel id="PanDire" runat="server" cssclass="titulo" Width="100%" Visible="False">
																	<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdDire" runat="server" width="95%" AutoGenerateColumns="False" OnEditCommand="mEditarDatosDire"
																					OnPageIndexChanged="grdDire_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Left" AllowPaging="True" BorderWidth="1px" BorderStyle="None" Visible="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="dipr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="dipr_dire" HeaderText="Direcci&#243;n">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="dipr_refe" ReadOnly="True" HeaderText="Referencia">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="16"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotexto" Width="292px" Obligatorio="True"
																					EsMail="false" AceptaNull="False" MaxLength="50"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 15px" align="right">
																				<asp:Label id="lblPais" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 15px">
																				<cc1:combobox class="combo" id="cmbPais" runat="server" Width="296px" AceptaNull="False" onchange="mCargarProvincias(this)"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 25px" align="right">
																				<asp:Label id="lblProv" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 25px">
																				<cc1:combobox class="combo" id="cmbProv" runat="server" Width="296px" AceptaNull="False" onchange="mCargarLocalidades()"
																																									NomOper="provincias_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 15px" align="right">
																				<asp:Label id="lblLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 15px">
																				<cc1:combobox class="combo" id="cmbLoca" runat="server" Width="296px" AceptaNull="False" onchange="mSelecCP();"
																					NomOper="localidades_cargar"></cc1:combobox></TD>
																		</TR>
																		
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblCodPostal" runat="server" cssclass="titulo">C�digo postal:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:CodPostalBox id="txtCodPostal" runat="server" cssclass="cuadrotexto" Width="72px" Obligatorio="True"
																					EsMail="True" Panel="Mail" AceptaNull="False" MaxValor="10000"></CC1:CodPostalBox></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblRefeDire" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtRefeDire" runat="server" cssclass="cuadrotexto" Width="411px" Obligatorio="True"
																					AceptaNull="False" MaxLength="50"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="center" colSpan="2" height="30">
																				<asp:Button id="btnAltaDire" runat="server" cssclass="boton" Width="100px" Text="Agregar Dir"></asp:Button>&nbsp;
																				<asp:Button id="btnBajaDire" runat="server" cssclass="boton" Width="100px" Text="Eliminar Dir"></asp:Button>&nbsp;
																				<asp:Button id="btnModiDire" runat="server" cssclass="boton" Width="100px" Text="Modificar Dir"></asp:Button>&nbsp;
																				<asp:Button id="btnLimpDire" runat="server" cssclass="boton" Width="100px" Text="Limpiar Dir"></asp:Button></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
													</TABLE>
												</P>
											</asp:panel></DIV>
									</TD>
								</TR>
								<TR>
									<TD align="center" height="15"></TD>
								</TR>
								<TR>
									<asp:panel id="PanBoto" runat="server" cssclass="titulo" Width="100%" Visible="False">
										<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
											<asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;
											<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Baja"></asp:button>&nbsp;
											<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Modificar"></asp:button>&nbsp;
											<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Limpiar"></asp:button>
									</asp:panel>
					</td>
				</tr>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
			</TR>
			<tr>
				<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
				<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
				<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnMailId" runat="server"></asp:textbox><asp:textbox id="hdnTeleId" runat="server"></asp:textbox><asp:textbox id="hdnDireId" runat="server"></asp:textbox><asp:textbox id="hdnPosic" runat="server" AutoPostBack="True"></asp:textbox>
				<cc1:combobox class="combo" id="cmbLocaAux" runat="server" AceptaNull="false"></cc1:combobox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
			
			

			function VigeChange()
			{
				ActivarFecha("txtInicFecha", document.all("cmbVige").value== "1");
				ActivarFecha("txtFinaFecha", document.all("cmbVige").value == "1");
				if(document.all("cmbVige").value!="1")
				{
					document.all("txtInicFecha").value="";
					document.all("txtFinaFecha").value="";
				}
			}
			
			function mSelecCP()
		  {
		    document.all["txtCodPostal"].value = '';
			document.all["cmbLocaAux"].value = document.all["cmbLoca"].value;
			document.all["txtCodPostal"].value = document.all["cmbLocaAux"].item(document.all["cmbLocaAux"].selectedIndex).text;
		  }
		function mCargarProvincias(pPais)
		{
		    var sFiltro = pPais.value;
		    LoadComboXML("provincias_cargar", sFiltro, "cmbProv", "S");
			if (pPais.value == '<%=Session("sPaisDefaId")%>')
				document.all('hdnValCodPostalPais').value = 'S';
			else
				document.all('hdnValCodPostalPais').value = 'N';
		}
		function mCargarLocalidades()
		{
		    var sFiltro = document.all("cmbProv").value 
		    if (sFiltro != '')
		       document.all["txtCodPostal"].value = '';
		       LoadComboXML("localidades_cargar", sFiltro, "cmbLoca", "S");
		       LoadComboXML("localidades_aux_cargar", sFiltro, "cmbLocaAux", "S");
		}		
		
		
			function cmbConc_Change()
			{
				var lstrRet = LeerCamposXML("conceptos", document.all("cmbConc").value, "conc_form_id");
				document.all("btnPosicDet").disabled = !(lstrRet=="10"||lstrRet=="11"||lstrRet=="12"||lstrRet=="85"||lstrRet=="300"||lstrRet=="400"||lstrRet=="500"||lstrRet=="600"||lstrRet=="700"||lstrRet=="1000");
				/*
				if(lstrRet=="10")
					document.all("btnPosicDet").value = "Posicionamiento";
				if(lstrRet=="11"||lstrRet=="12")
					document.all("btnPosicDet").value = "Recorridos";
				*/
			}
		</SCRIPT>
	</BODY>
</HTML>
