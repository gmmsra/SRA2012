Namespace SRA

Partial Class AlumDeuda_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents Label2 As System.Web.UI.WebControls.Label


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrTablaDeuSoc As String = "cobran_deusoc"
   Private mstrOrigen As String
   Private mstrSoloVencidas As String
   Private mstrSoloDevengadas As String

   Private Enum Columnas As Integer
      Chk = 0
      CompId = 1
      CheckIni = 2
      Anio = 3
      Perio = 4
      Conce = 5
      Impo = 6
      Inte = 7
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mConsultar()

            mChequearTodos()

            mCalcularTotales()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Private Sub mGuardarTodosIds()
      Dim lstrId As New System.Text.StringBuilder
      mLimpiarSession()

      For i As Integer = 0 To grdConsulta.PageCount - 1
         lstrId.Length = 0
         grdConsulta.CurrentPageIndex = i
         grdConsulta.DataBind()

         Session("mulPag" & i.ToString) = mGuardarDatos(True)
      Next

      grdConsulta.CurrentPageIndex = 0
      grdConsulta.DataBind()
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         Dim lstrId As String

         lstrId = mGuardarDatos()

         Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) = lstrId.ToString

         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar()

         mChequearGrilla()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Dim lstrCmd As New StringBuilder
      Dim ds As DataSet
      Dim ldsDatos As DataSet

      lstrCmd.Append("exec alumnos_deuda_consul")
      lstrCmd.Append(" @alum_id=")
      lstrCmd.Append(Request("alum_id"))

      If Not Request("anio") Is Nothing Then
         lstrCmd.Append(", @anio=")
         lstrCmd.Append(Request("anio"))
         lstrCmd.Append(", @perio=")
         lstrCmd.Append(Request("perio"))
      End If

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      grdConsulta.DataSource = ds
      grdConsulta.DataBind()
      ds.Dispose()

      Session("mulPaginas") = grdConsulta.PageCount
   End Sub
#End Region

   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Dim lstrId As New StringBuilder
      Dim lstrClieAnt, lstrBuqeAnt As String
      Try
         lstrId.Append(mGuardarDatos)

         For i As Integer = 0 To Session("mulPaginas")
            If Not Session("mulPag" & i.ToString) Is Nothing Then
               Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
               If i <> grdConsulta.CurrentPageIndex And lstrIdsSess <> "" Then
                  If lstrId.Length > 0 Then lstrId.Append(Chr(5))
                  lstrId.Append(lstrIdsSess)
               End If
               Session("mulPag" & i.ToString) = Nothing
            End If
         Next

         grdConsulta.DataSource = Nothing
         grdConsulta.DataBind()

         If lstrId.Length = 0 Then
            lstrId.Append("NADA")
         End If

         Dim lsbMsg As New StringBuilder
         lsbMsg.Append("<SCRIPT language='javascript'>")
         lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", lstrId.ToString))
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

   Private Function mGuardarDatos(Optional ByVal pbooTodos As Boolean = False) As String
      Dim lstrId As New StringBuilder

      For Each oDataItem As DataGridItem In grdConsulta.Items
         If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Or (pbooTodos And oDataItem.Cells(Columnas.CheckIni).Text = "1") Then
            If lstrId.Length > 0 Then lstrId.Append(Chr(5))
            lstrId.Append(oDataItem.Cells(Columnas.CompId).Text)
            lstrId.Append(Chr(6))
            'If oDataItem.Cells(Columnas.Anio).Text = "0" Then
                'lstrId.Append(Request("anio"))
            'Else
                lstrId.Append(oDataItem.Cells(Columnas.Anio).Text)
            'End If
            lstrId.Append(Chr(6))
            'If oDataItem.Cells(Columnas.Perio).Text = "0" Then
                'lstrId.Append(Request("perio"))
            'Else
                lstrId.Append(oDataItem.Cells(Columnas.Perio).Text)
            'End If
            lstrId.Append(Chr(6))
            lstrId.Append("")
            lstrId.Append(Chr(6))
            lstrId.Append("")
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.Impo).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.Inte).Text)
         End If
      Next

      Return (lstrId.ToString.Replace("&nbsp;", ""))
   End Function

   Private Sub mChequearGrilla()
      Dim lstrIdSess As String
      If Not (Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) Is Nothing) Then
         lstrIdSess = Session("mulPag" & grdConsulta.CurrentPageIndex.ToString)
      End If

      For Each oDataItem As DataGridItem In grdConsulta.Items
         CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = (Chr(5) & lstrIdSess & Chr(5)).IndexOf(Chr(5) & oDataItem.Cells(Columnas.CompId).Text.Replace("&nbsp;", "") & Chr(6) & oDataItem.Cells(Columnas.Anio).Text & Chr(6) & oDataItem.Cells(Columnas.Perio).Text) <> -1
      Next
   End Sub

   Private Sub mCalcularTotales()
      Dim dTotal As Decimal
      Dim dTotalSel As Decimal
      Dim lDr As DataRow

      For Each lDr In DirectCast(grdConsulta.DataSource, DataSet).Tables(0).Rows
         dTotal += lDr.Item("comp_neto") + lDr.Item("interes")
         If lDr.Item("selec_ini").ToString = "1" Then
            dTotalSel += lDr.Item("comp_neto") + lDr.Item("interes")
         End If
      Next

      txtTotal.Valor = dTotal
      txtTotalSel.Valor = dTotalSel
   End Sub

   Private Sub mLimpiarSession()
      Dim k As Integer

      While Not Session("mulPag" & k.ToString) Is Nothing
         Session("mulPag" & k.ToString) = Nothing
         k += 1
      End While
   End Sub

   Private Sub mChequearTodos()
      mGuardarTodosIds()

      mChequearGrilla()
   End Sub

   Private Function mEnMemoria(ByVal pstrSociId As String, ByVal pstrAnio As String, ByVal pstrPerio As String) As Boolean
      If Not Request("sess") Is Nothing Then
         Dim ldsDatos As DataSet = Session(Request("sess"))
         Dim ldtDatos As DataTable

         ldtDatos = ldsDatos.Tables(mstrTablaDeuSoc)
         If Not ldtDatos Is Nothing Then
            Return (ldtDatos.Select("cods_soci_id=" & pstrSociId & " AND cods_anio=" & pstrAnio.ToString & " AND cods_perio=" & pstrPerio).GetUpperBound(0) <> -1)
         End If
      End If
   End Function
End Class
End Namespace
