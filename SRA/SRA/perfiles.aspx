<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.perfiles" CodeFile="perfiles.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Perfiles</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY onload="javascript:gSetearTituloFrame();" class="pagina" leftMargin="5" topMargin="5"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign=middle align=center>
						<!----- CONTENIDO ----->
						<TABLE id="TableABM" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
							<tr>
								<td width="100%" colSpan="6"></td>
							</tr>
							<tr>
								<td width="100%" colSpan="6"></td>
							</tr>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="5" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Perfiles de Acceso</asp:label>
								</TD>
							</TR>
							<TR>
								<TD>
								</TD>
								<TD colSpan="2" vAlign="top" align="center">
									<asp:datagrid id="grdDato" runat="server" width="100%" AllowSorting="True" AllowPaging="True"
										BorderWidth="1px" BorderColor="WhiteSmoke" CellSpacing="1" GridLines="None" CellPadding="3"
										OnPageIndexChanged="DataGrid_Page" PageSize="10" OnEditCommand="mEditarDatos" AutoGenerateColumns="False"
										BorderStyle="None">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle cssclass="item"></AlternatingItemStyle>
										<ItemStyle cssclass="item2"></ItemStyle>
										<HeaderStyle cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="perf_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="perf_desc" ReadOnly="True" HeaderText="Descripci&#243;n" HeaderStyle-Width=100%></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ToolTip="Agregar Nuevo Perfil" ImageUrl="imagenes/btnImpr.jpg"
										ForeColor="Transparent" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif"
										OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
										ImageDisable="btnNuev0.gif"></CC1:BotonImagen>
								</TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD colSpan="5">
									<DIV align="left"><asp:panel cssclass="titulo" id="panDato" runat="server" BorderWidth="1px" BorderStyle="Solid"
											Width="100%" Visible="False">
											<P align=right>
												<TABLE class=FdoFld id=Table2 style="WIDTH: 100%" cellPadding=0 align=left>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 36px">
															<P>
																<asp:TextBox id=txtId runat="server" Visible="False" Width="54px"></asp:TextBox></P>
														</TD>
														<TD style="HEIGHT: 36px" colSpan=1 height=36>
															<asp:Label id=lblTitu runat="server" cssclass="titulo" width="219px">Registro Seleccionado</asp:Label></TD>
														<TD style="HEIGHT: 36px" vAlign=top align=right>&nbsp;
															<asp:ImageButton id=imgClose runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 27px">
															<P align=right>
																<asp:Label id=lblDesc runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;
															</P>
														</TD>
														<TD style="HEIGHT: 27px" colSpan=2 height=27>
															<cc1:TextBoxtab id=txtDesc runat="server" cssclass="cuadrotexto" Width="280px"></cc1:TextBoxtab></TD>
													</TR>
													<TR>
														<TD align=right background=imagenes/formdivmed.jpg colSpan=3 height=2><IMG height=2 src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 17px" vAlign=top align=right>
															<asp:CheckBox id=chkCopy runat="server" cssclass="check" Text="Copiar Permisos de: "></asp:CheckBox>&nbsp;</TD>
														<TD style="HEIGHT: 17px" vAlign=bottom colSpan=2 height=17>
															<cc1:combobox class=combo id=cmbPerf onkeydown=mEnterPorTab(); runat="server" Width="280px" AutoPostBack="True"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align=right background=imagenes/formdivmed.jpg colSpan=3 height=2><IMG height=2 src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
													<TR height=30>
														<TD align=center colSpan=3>
															<asp:button id=btnAlta runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;
															<asp:button id=btnBaja runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:button>&nbsp;&nbsp;
															<asp:button id=btnModi runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:button>&nbsp;&nbsp;
															<asp:button id=btnLimp runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnPage" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.frmABM.hdnPage.value != '') {
		   document.location='#editar';
		   document.frmABM.txtDesc.focus();
		}
		</SCRIPT>
	</BODY>
</HTML>
