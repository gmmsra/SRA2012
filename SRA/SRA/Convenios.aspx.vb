Namespace SRA

Partial Class Convenios
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Convenios
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String

   Private Enum Columnas As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()


            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      Dim lintCol As Integer

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "conv_obse")


   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()


      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      usrClieFil.Tabla = "clientes"
      usrClieFil.AutoPostback = False
      usrClieFil.Ancho = 790
      usrClieFil.Alto = 510

      'Clave unica
      usrClieFil.ColClaveUnica = True
      usrClieFil.FilClaveUnica = True
      'Numero de cliente
      usrClieFil.ColClieNume = True

      'Fantasia
      'usrClieFil.ColFanta = False
      'usrClieFil.FilFanta = False
      'Numero de CUIT
      usrClieFil.ColCUIT = True
      usrClieFil.FilCUIT = True
      'Numero de documento
      usrClieFil.ColDocuNume = True
      usrClieFil.FilDocuNume = True
      'Numero de socio
      usrClieFil.ColSociNume = True
      usrClieFil.FilSociNume = True

      usrClie.Tabla = "clientes"
      usrClie.AutoPostback = False
      usrClie.Ancho = 790
      usrClie.Alto = 510

      'Clave unica
      usrClie.ColClaveUnica = True
      usrClie.FilClaveUnica = True
      'Numero de cliente
      usrClie.ColClieNume = True

      'Fantasia
      'usrClieFil.ColFanta = False
      'usrClieFil.FilFanta = False
      'Numero de CUIT
      usrClie.ColCUIT = True
      usrClie.FilCUIT = True
      'Numero de documento
      usrClie.ColDocuNume = True
      usrClie.FilDocuNume = True
      'Numerode socio
      usrClie.ColSociNume = True
      usrClie.FilSociNume = True
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()

      Dim combos As New System.Collections.ArrayList
      With combos
         .Add(cmbActivFil)
         .Add(cmbActiv)
      End With
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", combos, "S")
     
   End Sub
   Public Sub mConsultar()
      Try
         Dim fil As String
         fil = "@conv_clie_id = " + usrClieFil.Valor.ToString
         fil += ", @conv_acti_id = " + cmbActivFil.Valor.ToString
         fil += ", @chkVto = " + IIf(chkVto.Checked, "1", "0")

         mstrCmd = "exec " + mstrTabla + "_consul " + fil

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
      mstrCmd = "exec " + mstrTabla + "_consul"
      mstrCmd += " " + hdnId.Text

      Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      If ldsDatos.Tables(0).Rows.Count > 0 Then
         With ldsDatos.Tables(0).Rows(0)
            usrClie.Valor = .Item("conv_clie_id")
            cmbActiv.Valor = .Item("conv_acti_id")
            chkAplicaSaldo.Checked = .Item("conv_cont_sldo")
            txtCoef.Valor = .Item("conv_coef")
            txtFechaDesde.Fecha = .Item("conv_inic_fecha")

            txtObse.Valor = .Item("conv_obse")

                    If Not .IsNull("conv_baja_fecha") Then
                        txtFechaHasta.Fecha = .Item("conv_fina_fecha")
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("conv_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If

         End With

         mSetearEditor(False)
         mMostrarPanel(True)
      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      usrClie.Limpiar()
      cmbActiv.Limpiar()
      chkAplicaSaldo.Checked = False
      txtCoef.Valor = ""
      txtFechaDesde.Text = ""
      txtFechaHasta.Text = ""
      txtObse.Text = ""
      mSetearEditor(True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         clsWeb.gInicializarControles(Me, mstrConn)
         clsWeb.gValidarControles(Me)

         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         clsWeb.gInicializarControles(Me, mstrConn)
         clsWeb.gValidarControles(Me)

         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidaDatos()

      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If Not (txtFechaHasta.Fecha Is DBNull.Value) Then
         If txtFechaDesde.Fecha > txtFechaHasta.Fecha Then
            Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
         End If
      End If
     


   End Sub
   Private Function mGuardarDatos() As DataSet
      Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
      Dim lintCpo As Integer
      mValidaDatos()

      With ldsDatos.Tables(0).Rows(0)
         .Item("conv_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("conv_clie_id") = usrClie.Valor
         .Item("conv_acti_id") = cmbActiv.Valor
         .Item("conv_cont_sldo") = chkAplicaSaldo.Checked
         .Item("conv_coef") = txtCoef.Valor()
         .Item("conv_inic_fecha") = txtFechaDesde.Fecha
         .Item("conv_fina_fecha") = txtFechaHasta.Fecha
         .Item("conv_obse") = txtObse.Valor
         .Item("conv_baja_fecha") = DBNull.Value

      End With

      Return ldsDatos
   End Function


#End Region

#Region "Eventos de Controles"


   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub btnClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
      mCerrar()
   End Sub
#End Region


   Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub



   Private Overloads Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub

   Private Sub mLimpiarFil()
      cmbActivFil.Limpiar()
      usrClieFil.Limpiar()
      mConsultar()
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         Dim params As String
         Dim lstrRptName As String = "Convenios"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&conv_clie_id=" + usrClieFil.Valor.ToString
         lstrRpt += "&conv_acti_id=" + IIf(cmbActivFil.Valor.ToString = "", "null", cmbActivFil.Valor.ToString)
         lstrRpt += "&chkVto=" + IIf(chkVto.Checked, "true", "false")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
