'Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports SRA_Entidad


Namespace SRA


Partial Class ProductosInscripcion
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

   'Protected WithEvents usrRece As usrProdDeriv


   Protected WithEvents lblPelaABov As System.Web.UI.WebControls.Label
   Protected WithEvents cmbPelaABov As NixorControls.ComboBox
   Protected WithEvents lblPelaBBov As System.Web.UI.WebControls.Label
   Protected WithEvents cmbPelaBBov As NixorControls.ComboBox
   Protected WithEvents lblPelaCBov As System.Web.UI.WebControls.Label
   Protected WithEvents cmbPelaCBov As NixorControls.ComboBox
   Protected WithEvents lblelaDBov As System.Web.UI.WebControls.Label
   Protected WithEvents cmbPelaDBov As NixorControls.ComboBox
   Protected WithEvents lblTransplante As System.Web.UI.WebControls.Label
   'Protected WithEvents lblRece As System.Web.UI.WebControls.Label
   'Protected WithEvents txtRecep As NixorControls.TextBoxTab
    ''Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
   Protected WithEvents btnTransplante As System.Web.UI.HtmlControls.HtmlButton
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    Protected WithEvents btnBaja As System.Web.UI.HtmlControls.HtmlButton


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_ProductosInscrip
    Private mstrTablaDocu As String = SRA_Neg.Constantes.gTab_ProductosInscripDocum
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mdsDatos As DataSet
    Private mstrConn As String

    Private Enum Tipo
        EspeBovinosNOHolando = 0
        EspeBovinos = 3
        EspePeliferos = 10
        EspeCaprinos = 9
        EspeOvinos = 12
        RazaHolando = 2
        RazaCriolla = 45
    End Enum

    Private Enum Columnas As Integer
        InscVisto = 0
        InscEdit = 1
        InscId = 2
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                Session(mstrTabla) = Nothing
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mMostrarPanel(False)
                clsWeb.gInicializarControles(Me, mstrConn)
                    txtFechaValor.Fecha = Today
                    txtInscFecha.Fecha = Today
                    Session("MilSecc") = Now.Millisecond.ToString
                btnPedigree.Disabled = True

            Else
                If panDato.Visible Then
                    mdsDatos = Session("sessProdInscrip")
                    If usrCria.Valor.ToString <> "" Then
                        mCargarEstablecimientos(usrCria.Valor.ToString)
                        If Not Request.Form("cmbEsbl") Is Nothing Then
                            cmbEsbl.Valor = Request.Form("cmbEsbl").ToString()
                        End If
                        clsWeb.gCargarCombo(mstrConn, "prefijos_cargar @cria_id=" & usrCria.Valor.ToString, cmbPref, "id", "descrip", "S")
                        If Not Request.Form("cmbPref") Is Nothing Then
                            cmbPref.Valor = Request.Form("cmbPref").ToString()
                        End If
                    Else
                        cmbPref.Items.Clear()
                        cmbEsbl.Items.Clear()
                    End If
                    'mCargarValores()
                    'mCargarComboReg(cmbRazaFil.Valor)
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mCargarComboReg(ByVal pintRazaID As Integer)
        clsWeb.gCargarRefeCmb(mstrConn, "rg_registros_tipos_cargar " & pintRazaID, cmbTipoRegiFil, "S")
    End Sub

    Private Sub mCargarValores()

        mdsDatos = Session("sessProdInscrip")

        If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then

            With mdsDatos.Tables(0).Rows(0)
                If usrCria.Valor Is DBNull.Value Then
                    usrCria.Valor = .Item("pdin_cria_id")
                End If
                If Not usrCria.Valor Is DBNull.Value Then
                    mCargarEstablecimientos(usrCria.Valor.ToString)
                    cmbEsbl.Valor = .Item("pdin_esbl_id")
                    If Not .Item("pdin_sdde_id") Is DBNull.Value Then hdnServId.Text = .Item("pdin_sdde_id")
                    If Not .Item("_serv_denun") Is DBNull.Value Then txtServ.Valor = .Item("_serv_denun")
                    If Not .Item("_madre") Is DBNull.Value Then txtMadre.Valor = .Item("_madre")
                    If Not .Item("_padre") Is DBNull.Value Then txtPadre.Valor = .Item("_padre")
                Else
                    cmbEsbl.Items.Clear()
                    mLimpiarServicio()
                    mLimpiarTE()
                End If

                If usrCria.cmbCriaRazaExt.Valor.ToString <> "" Then
                    mCargarPelajes(usrCria.cmbCriaRazaExt.Valor)
                    cmbPelaA.Valor = .Item("pdin_a_pela_id")
                    'If cmbPelaA.Valor Is DBNull.Value Then cmbPelaA.Valor = Request.Form("cmbPelaA").ToString
                    cmbPelaB.Valor = .Item("pdin_b_pela_id")
                    'If cmbPelaB.Valor Is DBNull.Value Then cmbPelaB.Valor = Request.Form("cmbPelaB").ToString
                    cmbPelaC.Valor = .Item("pdin_c_pela_id")
                    ' If cmbPelaC.Valor Is DBNull.Value Then cmbPelaC.Valor = Request.Form("cmbPelaC").ToString
                    cmbPelaD.Valor = .Item("pdin_d_pela_id")
                    ' If cmbPelaD.Valor Is DBNull.Value Then cmbPelaD.Valor = Request.Form("cmbPelaD").ToString
                Else
                    cmbPelaA.Items.Clear()
                    cmbPelaB.Items.Clear()
                    cmbPelaC.Items.Clear()
                    cmbPelaD.Items.Clear()
                End If
            End With

        Else
            If Not usrCria.Valor Is DBNull.Value Then
                mCargarEstablecimientos(usrCria.Valor.ToString)
                cmbEsbl.Valor = Request.Form("cmbEsbl")

            Else
                cmbEsbl.Items.Clear()
                mLimpiarServicio()
                mLimpiarTE()
            End If
            If Not usrCria.cmbCriaRazaExt.Valor.ToString = "" Then
                mCargarPelajes(usrCria.cmbCriaRazaExt.Valor)
                cmbPelaA.Valor = Request.Form("cmbPelaA")
                cmbPelaB.Valor = Request.Form("cmbPelaB")
                cmbPelaC.Valor = Request.Form("cmbPelaC")
                cmbPelaD.Valor = Request.Form("cmbPelaD")
            Else
                cmbPelaA.Items.Clear()
                cmbPelaB.Items.Clear()
                cmbPelaC.Items.Clear()
                cmbPelaD.Items.Clear()
            End If
        End If

    End Sub

    Private Sub mCargarObserNoHolando()
        clsWeb.gCargarCombo(mstrConn, "cs_productos_parto_cargar", cmbBovParto, "id", "descrip", "")
        clsWeb.gCargarCombo(mstrConn, "rg_crias_cargar", cmbBovCria, "id", "descrip", "S")

    End Sub


    Private Sub mCargarCombos()
        Dim combos As New System.Collections.ArrayList
        With combos
            .Add(cmbCriaTipo)
            .Add(cmbCriaTipoBov)
        End With
        clsWeb.gCargarRefeCmb(mstrConn, "rg_cria_tipos", combos, "S")
        'clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar " & cmbRazaFil.Valor, cmbTipoRegiFil, "id", "descrip", "S")

        'clsWeb.gCargarCombo(mstrConn, "razas_inscrip_cargar", cmbRazaFil, "id", "descrip", "T")
        'SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
    End Sub

    Private Sub mSetearEventos()
        ''btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
        btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
        ''btnPedigree.Attributes.Add("onclick", "btnPedigree_click();")
    End Sub


    Private Sub mSetearMaxLength()

        Dim lstrProdLong As Object
        Dim lstrDocuLong As Object
        Dim lintCol As Integer

        lstrProdLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNroControl.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "pdin_nro_control")
        txtSraNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "pdin_nomb")
        txtRP.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "pdin_rp")
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "pdin_obser")
        txtRPNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtNombFil.MaxLength = txtNomb.MaxLength
        txtNumeFil.MaxLength = txtSraNume.MaxLength
        txtRPNumeFil.MaxLength = txtRP.MaxLength
        txtPesoNacer.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtEpdNacer.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "pdin_epd_nacer")

        lstrDocuLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocu)
        txtDocu.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "pido_docu_path")
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "pido_obser")

    End Sub

#End Region

#Region "Inicializacion de Variables"

    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        hdnSRA.Text = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")

        usrCriaFil.Tabla = mstrCriadores
        usrCriaFil.AutoPostback = False
        usrCriaFil.FilClaveUnica = False
        usrCriaFil.ColClaveUnica = False
        usrCriaFil.Ancho = 790
        usrCriaFil.Alto = 510
        usrCriaFil.ColDocuNume = False
        usrCriaFil.ColCUIT = False
        usrCriaFil.ColClaveUnica = False
        usrCriaFil.FilClaveUnica = True
        usrCriaFil.FilAgru = False
        usrCriaFil.FilCriaNume = True
        usrCriaFil.FilCUIT = False
        usrCriaFil.FilDocuNume = True
        usrCriaFil.FilTarjNume = False
        usrCriaFil.Criador = True
        usrCriaFil.Inscrip = True
        usrCriaFil.ColCriaNume = True

        usrCria.Tabla = mstrCriadores
        usrCria.AutoPostback = False
        usrCria.FilClaveUnica = False
        usrCria.ColClaveUnica = False
        usrCria.Ancho = 790
        usrCria.Alto = 510
        usrCria.ColDocuNume = False
        usrCria.ColCUIT = False
        usrCria.ColClaveUnica = False
        usrCria.FilClaveUnica = False
        usrCria.FilCUIT = False
        usrCria.FilDocuNume = False
        usrCria.FilTarjNume = False
        usrCria.Criador = True
        usrCria.Inscrip = True
        usrCria.ColCriaNume = True


        usrMadreFil.Tabla = mstrProductos
        usrMadreFil.AutoPostback = False
        usrMadreFil.Ancho = 790
        usrMadreFil.Alto = 510
        usrMadreFil.FilCriaNume = True
        usrMadreFil.ColCriaNume = True
        usrMadreFil.FilAsocNume = True
        usrMadreFil.ColAsocNume = True
        usrMadreFil.FilNaciFecha = True
        usrMadreFil.ColNaciFecha = True
        usrMadreFil.FilProp = True
        usrMadreFil.FilRpNume = True
        usrMadreFil.ColRpNume = True
        usrMadreFil.FilSraNume = True
        usrMadreFil.ColSraNume = True
        usrMadreFil.FilLaboNume = True
        usrMadreFil.FilMadre = False
        usrMadreFil.FilPadre = False
        usrMadreFil.FilRece = False
        usrMadreFil.FilSexo = False
        usrMadreFil.Sexo = 0
        usrMadreFil.Inscrip = True
        usrMadreFil.ColRazaSoloCodi = True

        usrPadreFil.Tabla = mstrProductos
        usrPadreFil.AutoPostback = False
        usrPadreFil.Ancho = 790
        usrPadreFil.Alto = 510
        usrPadreFil.FilCriaNume = True
        usrPadreFil.ColCriaNume = True
        usrPadreFil.FilAsocNume = True
        usrPadreFil.ColAsocNume = True
        usrPadreFil.FilNaciFecha = True
        usrPadreFil.ColNaciFecha = True
        usrPadreFil.FilProp = True
        usrPadreFil.FilRpNume = True
        usrPadreFil.ColRpNume = True
        usrPadreFil.FilSraNume = True
        usrPadreFil.ColSraNume = True
        usrPadreFil.FilLaboNume = True
        usrPadreFil.FilMadre = False
        usrPadreFil.FilPadre = False
        usrPadreFil.FilRece = False
        usrPadreFil.FilSexo = False
        usrPadreFil.Sexo = 1
        usrPadreFil.Inscrip = True
        usrPadreFil.ColRazaSoloCodi = True

        usrMadre.Tabla = mstrProductos
        usrMadre.AutoPostback = False
        usrMadre.Ancho = 790
        usrMadre.Alto = 510
        usrMadre.FilCriaNume = True
        usrMadre.ColCriaNume = True
        usrMadre.FilAsocNume = True
        usrMadre.ColAsocNume = True
        usrMadre.FilNaciFecha = True
        usrMadre.ColNaciFecha = True
        usrMadre.FilProp = True
        usrMadre.FilRpNume = True
        usrMadre.ColRpNume = True
        usrMadre.FilSraNume = True
        usrMadre.ColSraNume = True
        usrMadre.FilLaboNume = True
        usrMadre.FilMadre = False
        usrMadre.FilPadre = False
        usrMadre.FilRece = False
        usrMadre.FilSexo = False
        usrMadre.Sexo = 0
        usrMadre.Inscrip = True
        usrMadre.ColRazaSoloCodi = True
        usrMadre.CriaId = hdnCriaId.Text
        usrMadre.Raza = usrCria.cmbCriaRazaExt.Valor
        'usrMadre.RazaPadre = True
        usrMadre.ValiExis = 0

        usrPadre.Tabla = mstrProductos
        usrPadre.AutoPostback = False
        usrPadre.Ancho = 790
        usrPadre.Alto = 510
        usrPadre.FilCriaNume = True
        usrPadre.ColCriaNume = True
        usrPadre.FilAsocNume = True
        usrPadre.ColAsocNume = True
        usrPadre.FilNaciFecha = True
        usrPadre.ColNaciFecha = True
        usrPadre.ColRazaSoloCodi = True
        usrPadre.FilProp = True
        usrPadre.FilRpNume = True
        usrPadre.ColRpNume = True
        usrPadre.FilSraNume = True
        usrPadre.ColSraNume = True
        usrPadre.FilLaboNume = True
        usrPadre.FilMadre = False
        usrPadre.FilPadre = False
        usrPadre.FilRece = False
        usrPadre.FilSexo = False
        usrPadre.Sexo = 1
        usrPadre.Inscrip = True
        usrPadre.ColRazaSoloCodi = True
        usrPadre.CriaId = hdnCriaId.Text
        usrPadre.Raza = usrCria.cmbCriaRazaExt.Valor
        'usrPadre.RazaPadre = True
        usrPadre.ValiExis = 0

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As String

            lstrCmd = "exec " & SRA_Neg.Constantes.gTab_ProductosInscrip & "_busq"
            lstrCmd = lstrCmd & "  @buscar_en=" + IIf(chkBusc.Checked, "1", "0")
            lstrCmd = lstrCmd & " ,@pdin_cria_id=" + clsSQLServer.gFormatArg(usrCriaFil.Valor, SqlDbType.Int)
            lstrCmd = lstrCmd & " ,@pdin_raza_id=" + clsSQLServer.gFormatArg(usrCriaFil.RazaId, SqlDbType.Int)
            lstrCmd = lstrCmd & " ,@pdin_sra_nume=" + clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int)
            lstrCmd = lstrCmd & " ,@incluir_bajas=" + IIf(chkBaja.Checked, "1", "0")
            lstrCmd = lstrCmd & " ,@madre_pdin_id=" + clsSQLServer.gFormatArg(usrMadreFil.Valor, SqlDbType.Int)
            lstrCmd = lstrCmd & " ,@padre_pdin_id=" + clsSQLServer.gFormatArg(usrPadreFil.Valor, SqlDbType.Int)
            lstrCmd = lstrCmd & " ,@pdin_rp=" + clsSQLServer.gFormatArg(txtRPNumeFil.Valor, SqlDbType.VarChar)
            lstrCmd = lstrCmd & " ,@pdin_nomb=" + clsSQLServer.gFormatArg(txtNombFil.Valor, SqlDbType.VarChar)
            lstrCmd = lstrCmd & " ,@pdin_naci_fecha_desde=" + clsSQLServer.gFormatArg(txtNaciFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & " ,@pdin_naci_fecha_hasta=" + clsSQLServer.gFormatArg(txtNaciFechaHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & " ,@pdin_valor_fecha_desde=" + clsSQLServer.gFormatArg(txtFechaPreseDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & " ,@pdin_valor_fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaPreseHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & " ,@pdin_alta_fecha_desde=" + clsSQLServer.gFormatArg(txtFechaIngreDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & " ,@pdin_alta_fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaIngreHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & " ,@mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")
            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

            grdDato.Visible = True

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTabla
                btnAlta.Enabled = pbooAlta
                If pbooAlta = False And lblAprob.Text <> "" Then
                    'si esta aprobada no puede ser modificado ni dado de baja el registro. 
                    pbooAlta = True
                End If
             
                btnModi.Enabled = Not (pbooAlta)
                btnErr.Visible = Not (pbooAlta)
            Case mstrTablaDocu
                btnAltaDocu.Enabled = pbooAlta
                btnBajaDocu.Enabled = Not (pbooAlta)
                btnModiDocu.Enabled = Not (pbooAlta)
        End Select
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            ' 'clsWeb.gCargarCombo(mstrConn, "prefijos_cargar", cmbPref, "id", "descrip", "S")

            mCargarDatos(E.Item.Cells(Columnas.InscId).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarEstablecimientos(ByVal pCriador As String)
        clsWeb.gCargarCombo(mstrConn, "establecimientos_cargar " & pCriador, cmbEsbl, "id", "descrip", "S")
    End Sub


    Private Sub mCargarPelajes(ByVal pRaza As String)
        Select Case SRA_Neg.Utiles.EspeRaza(pRaza, mstrConn)
            Case Tipo.EspePeliferos
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=1", cmbPelaAPeli, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=2", cmbPelaBPeli, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=3", cmbPelaCPeli, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=4", cmbPelaDPeli, "id", "descrip", "S")
            Case Tipo.RazaHolando
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=1", cmbHolanPelaA, "id", "descrip", "S")
            Case Tipo.EspeBovinosNOHolando
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=1", cmbBovPelaA, "id", "descrip", "S")
            Case Tipo.RazaCriolla
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=1", cmbCrioPelaA, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=2", cmbCrioPelaB, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=3", cmbCrioPelaC, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=4", cmbCrioPelaD, "id", "descrip", "S")
            Case Else
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=1", cmbPelaA, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=2", cmbPelaB, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=3", cmbPelaC, "id", "descrip", "S")
                clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=4", cmbPelaD, "id", "descrip", "S")
        End Select

    End Sub


    Public Sub mCargarDatos(ByVal pstrId As String)

        Try


            mLimpiar()

            grdDocu.CurrentPageIndex = 0

            hdnId.Text = clsFormatear.gFormatCadena(pstrId)

            mCrearDataSet(hdnId.Text)

            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())





            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    If Not mdsDatos.Tables(0).Rows(0).Item("pdin_raza_id") Is System.DBNull.Value Then
                        mCargarPelajes(.Item("pdin_raza_id"))
                    End If


                    Select Case SRA_Neg.Utiles.EspeRaza(.Item("pdin_raza_id"), mstrConn)
                        Case Tipo.EspePeliferos
                            usrCriader.Valor = .Item("pdin_crdr_id")
                            txtRPPeli.Valor = .Item("pdin_rp")
                            txtRPNumePeli.Valor = .Item("pdin_rp_nume")
                            'txtMarcaPeli.Valor = 'no hay campo
                                txtFechaNacPeli.Fecha = IIf(.Item("pdin_naci_fecha").ToString.Length > 0, .Item("pdin_naci_fecha"), String.Empty)
                                cmbPelaAPeli.Valor = IIf(.Item("pdin_a_pela_id").ToString.Length > 0, .Item("pdin_a_pela_id"), String.Empty)
                                cmbPelaBPeli.Valor = IIf(.Item("pdin_b_pela_id").ToString.Length > 0, .Item("pdin_b_pela_id"), String.Empty)
                                cmbPelaCPeli.Valor = IIf(.Item("pdin_c_pela_id").ToString.Length > 0, .Item("pdin_c_pela_id"), String.Empty)
                                cmbPelaDPeli.Valor = IIf(.Item("pdin_d_pela_id").ToString.Length > 0, .Item("pdin_d_pela_id"), String.Empty)
                            If .IsNull("pdin_sexo") Then
                                cmbSexo.Limpiar()
                            Else
                                cmbSexo.Valor = IIf(.Item("pdin_sexo"), "1", "0")
                            End If
                        Case Tipo.EspeCaprinos, Tipo.EspeOvinos
                                txtNomb.Valor = IIf(.Item("pdin_nomb").ToString.Length > 0, .Item("pdin_nomb"), String.Empty)
                            mCargarEstablecimientos(.Item("pdin_cria_id"))
                                cmbEsbl.Valor = IIf(.Item("pdin_esbl_id").ToString.Length > 0, .Item("pdin_esbl_id"), String.Empty)
                                txtRPOviCapri.Valor = IIf(.Item("pdin_rp").ToString.Length > 0, .Item("pdin_rp"), String.Empty)
                                txtRPNumeOviCapri.Valor = IIf(.Item("pdin_rp_nume").ToString.Length > 0, .Item("pdin_rp_nume"), String.Empty)
                                txtNaciFechaOviCapri.Fecha = IIf(.Item("pdin_naci_fecha").ToString.Length > 0, .Item("pdin_naci_fecha"), String.Empty)
                            If .IsNull("pdin_sexo") Then
                                cmbSexoOviCapri.Limpiar()
                            Else
                                cmbSexoOviCapri.Valor = IIf(.Item("pdin_sexo"), "1", "0")
                            End If
                        Case Tipo.EspeBovinosNOHolando
                            mCargarObserNoHolando()
                                txtNomb.Valor = IIf(.Item("pdin_nomb").ToString.Length > 0, .Item("pdin_nomb"), String.Empty)
                            If .IsNull("pdin_sexo") Then
                                cmbSexoBov.Limpiar()
                            Else
                                cmbSexoBov.Valor = IIf(.Item("pdin_sexo"), "1", "0")
                            End If
                                txtNaciFechaBov.Fecha = IIf(.Item("pdin_naci_fecha").ToString.Length > 0, .Item("pdin_naci_fecha"), String.Empty)
                                txtRpBov.Valor = IIf(.Item("pdin_rp").ToString.Length > 0, .Item("pdin_rp"), String.Empty)
                                txtRpnumeBov.Valor = IIf(.Item("pdin_rp_nume").ToString.Length > 0, .Item("pdin_rp_nume"), String.Empty)
                                cmbCriaTipoBov.Valor = IIf(.Item("pdin_ticr_id").ToString.Length > 0, .Item("pdin_ticr_id"), String.Empty)
                                cmbBovPelaA.Valor = IIf(.Item("pdin_a_pela_id").ToString.Length > 0, .Item("pdin_a_pela_id"), String.Empty)
                                cmbBovParto.Valor = IIf(.Item("pdin_parto").ToString.Length > 0, .Item("pdin_parto"), String.Empty)
                                cmbBovCria.Valor = IIf(.Item("pdin_cria").ToString.Length > 0, .Item("pdin_cria"), String.Empty)
                            mCargarEstablecimientos(.Item("pdin_cria_id"))
                                cmbEsbl.Valor = IIf(.Item("pdin_esbl_id").ToString.Length > 0, .Item("pdin_esbl_id"), String.Empty)
                        Case Tipo.RazaHolando
                            If .IsNull("pdin_sexo") Then
                                cmbSexo.Limpiar()
                            Else
                                cmbSexo.Valor = IIf(.Item("pdin_sexo"), "1", "0")
                            End If
                            mCargarEstablecimientos(.Item("pdin_cria_id"))
                                cmbEsbl.Valor = IIf(.Item("pdin_esbl_id").ToString.Length > 0, .Item("pdin_esbl_id"), String.Empty)
                                txtNomb.Valor = IIf(.Item("pdin_nomb").ToString.Length > 0, .Item("pdin_nomb"), String.Empty)
                                txtNaciFechaHolan.Fecha = IIf(.Item("pdin_naci_fecha").ToString.Length > 0, .Item("pdin_naci_fecha"), String.Empty)
                                txtRPHolan.Valor = IIf(.Item("pdin_rp").ToString.Length > 0, .Item("pdin_rp"), String.Empty)
                            txtRPNumeHolan.Valor = .Item("pdin_rp_nume")
                            cmbCriaTipo.Valor = .Item("pdin_ticr_id")
                            cmbHolanPelaA.Valor = .Item("pdin_a_pela_id")
                            'txtReceFecha.Fecha = .Item("pdin_trans_embr_fecha")
                            'usrRece.Valor = .Item("pdin_trans_embr_rece_id")
                        Case Tipo.RazaCriolla
                            If .IsNull("pdin_sexo") Then
                                cmbSexo.Limpiar()
                            Else
                                cmbSexo.Valor = IIf(.Item("pdin_sexo"), "1", "0")
                            End If
                            txtNomb.Valor = .Item("pdin_nomb")
                                txtNaciFecha.Fecha = IIf(.Item("pdin_naci_fecha").ToString.Length > 0, .Item("pdin_naci_fecha"), String.Empty)
                                txtRP.Valor = IIf(.Item("pdin_rp").ToString.Length > 0, .Item("pdin_rp"), String.Empty)
                                txtRPNume.Valor = IIf(.Item("pdin_rp_nume").ToString.Length > 0, .Item("pdin_rp_nume"), String.Empty)
                                cmbCrioPelaA.Valor = IIf(.Item("pdin_a_pela_id").ToString.Length > 0, .Item("pdin_a_pela_id"), String.Empty)
                                cmbCrioPelaB.Valor = IIf(.Item("pdin_b_pela_id").ToString.Length > 0, .Item("pdin_b_pela_id"), String.Empty)
                                cmbCrioPelaC.Valor = IIf(.Item("pdin_c_pela_id").ToString.Length > 0, .Item("pdin_c_pela_id"), String.Empty)
                                cmbCrioPelaD.Valor = IIf(.Item("pdin_d_pela_id").ToString.Length > 0, .Item("pdin_d_pela_id"), String.Empty)
                            mCargarEstablecimientos(.Item("pdin_cria_id"))
                            cmbEsbl.Valor = .Item("pdin_esbl_id")
                        Case Else
                                txtNomb.Valor = IIf(.Item("pdin_nomb").ToString.Length > 0, .Item("pdin_nomb"), String.Empty)
                            mCargarEstablecimientos(.Item("pdin_cria_id"))
                                cmbEsbl.Valor = IIf(.Item("pdin_esbl_id").ToString.Length > 0, .Item("pdin_esbl_id"), String.Empty)
                            If .IsNull("pdin_sexo") Then
                                cmbSexo.Limpiar()
                            Else
                                cmbSexo.Valor = IIf(.Item("pdin_sexo"), "1", "0")
                            End If
                                txtRP.Valor = IIf(.Item("pdin_rp").ToString.Length > 0, .Item("pdin_rp"), String.Empty)
                                txtRPNume.Valor = IIf(.Item("pdin_rp_nume").ToString.Length > 0, .Item("pdin_rp_nume"), String.Empty)
                                txtNaciFecha.Fecha = IIf(.Item("pdin_naci_fecha").ToString.Length > 0, .Item("pdin_naci_fecha"), String.Empty)
                                cmbPelaA.Valor = IIf(.Item("pdin_a_pela_id").ToString.Length > 0, .Item("pdin_a_pela_id"), String.Empty)
                                cmbPelaB.Valor = IIf(.Item("pdin_b_pela_id").ToString.Length > 0, .Item("pdin_b_pela_id"), String.Empty)
                                cmbPelaC.Valor = IIf(.Item("pdin_c_pela_id").ToString.Length > 0, .Item("pdin_c_pela_id"), String.Empty)
                            cmbPelaD.Valor = IIf(.Item("pdin_d_pela_id").ToString.Length>0,.Item("pdin_d_pela_id"),String.Empty)
                    End Select

                    'comun en todas

                    txtNroControl.Text = ValidarNulos(.Item("pdin_nro_control"), False)
                        txtObse.Valor = IIf(.Item("pdin_obser").ToString.Length > 0, .Item("pdin_obser"), String.Empty)
                        txtServiFecha.Fecha = IIf(.Item("pdin_servi_fecha").ToString.Length > 0, .Item("pdin_servi_fecha"), String.Empty)
                        txtSiete.Valor = IIf(.Item("pdin_siete").ToString.Length > 0, .Item("pdin_siete"), String.Empty)
                        txtTeNume.Valor = IIf(.Item("pdin_tede_nume").ToString.Length > 0, .Item("pdin_tede_nume"), String.Empty)
                        txtFechaServiTE.Fecha = IIf(.Item("pdin_servi_fecha").ToString.Length > 0, .Item("pdin_servi_fecha"), String.Empty)
                        txtFechaRecu.Fecha = IIf(.Item("pdin_tede_recu_fecha").ToString.Length > 0, .Item("pdin_tede_recu_fecha"), String.Empty)
                        txtImplFecha.Fecha = IIf(.Item("pdin_tedd_impl_fecha").ToString.Length > 0, .Item("pdin_tedd_impl_fecha"), String.Empty)
                        txtCara.Valor = IIf(.Item("pdin_tedd_carv").ToString.Length > 0, .Item("pdin_tedd_carv"), String.Empty)
                        txtTatu.Valor = IIf(.Item("pdin_tedd_tatu").ToString.Length > 0, .Item("pdin_tedd_tatu"), String.Empty)
                        txtCruza.Valor = IIf(.Item("pdin_tedd_raza_cruza").ToString.Length > 0, .Item("pdin_tedd_raza_cruza"), String.Empty)
                        txtPesoNacer.Valor = IIf(.Item("pdin_peso_nacer").ToString.Length > 0, .Item("pdin_peso_nacer"), String.Empty)
                        txtEpdNacer.Valor = IIf(.Item("pdin_epd_nacer").ToString.Length > 0, .Item("pdin_epd_nacer"), String.Empty)
                        usrCria.Valor = IIf(.Item("pdin_cria_id").ToString.Length > 0, .Item("pdin_cria_id"), String.Empty)

                    usrCria.cmbCriaRazaExt.Valor = .Item("pdin_raza_id") 'si o encuentra el criador (baja) toma la raza 
                    hdnServId.Text = IIf(.IsNull("pdin_sdde_id"), "", .Item("pdin_sdde_id"))
                    hdnTEId.Text = IIf(.IsNull("pdin_tedd_id"), "", .Item("pdin_tedd_id"))
                    btnServicios.Disabled = IIf(.IsNull("pdin_sdde_id"), True, False)
                    btnTE.Disabled = IIf(.IsNull("pdin_tedd_id"), True, False)
                    'chkServManual.Checked = (.IsNull("pdin_sdde_id") And .IsNull("pdin_tedd_id"))
                    usrPadre.Valor = .Item("pdin_padre_id")
                    usrMadre.Valor = .Item("pdin_madre_id")
                    usrPadre.hdnRazaCruzaExt.Text = Convert.ToString(.Item("pdin_raza_id"))
                    usrMadre.hdnRazaCruzaExt.Text = Convert.ToString(.Item("pdin_raza_id"))
                    'usrPadre.mCargarRazasPadres("1")
                    'usrMadre.mCargarRazasPadres("0")
                    usrPadre.RazaId = .Item("pdin_raza_id")
                    usrMadre.RazaId = .Item("pdin_raza_id")

                    If usrMadre.Valor Is DBNull.Value Then
                        usrMadre.txtRPExt.Text = IIf(.IsNull("pdin_madre_rp"), "", .Item("pdin_madre_rp"))
                        usrMadre.txtSraNumeExt.Valor = IIf(.IsNull("pdin_madre_sra_nume"), "", .Item("pdin_madre_sra_nume"))
                        usrMadre.txtProdNombExt.Valor = "---- Error: Producto inexistente ----"
                    End If
                    If usrPadre.Valor Is DBNull.Value Then
                        usrPadre.txtRPExt.Text = IIf(.IsNull("pdin_padre_rp"), "", .Item("pdin_padre_rp"))
                        usrPadre.txtSraNumeExt.Valor = IIf(.IsNull("pdin_padre_sra_nume"), "", .Item("pdin_padre_sra_nume"))
                        usrPadre.txtProdNombExt.Valor = "---- Error: Producto inexistente ----"
                    End If
                    Dim dtProductoPadre As DataTable
                    Dim dtProductoMadre As DataTable
                    If Not usrPadre.Valor Is DBNull.Value Then
                        dtProductoPadre = oProducto.GetProductoById(usrPadre.Valor, "")
                    End If


                    If Not usrMadre.Valor Is DBNull.Value Then
                        dtProductoMadre = oProducto.GetProductoById(usrMadre.Valor, "")
                    End If

                    If Not dtProductoMadre Is Nothing Then
                        If dtProductoMadre.Rows.Count > 0 Then
                            usrMadre.CriaOrPropId = ValidarNulos(dtProductoMadre.Rows(0).Item("prdt_prop_cria_id"), False)
                        End If
                    End If

                    If Not dtProductoPadre Is Nothing Then
                        If dtProductoPadre.Rows.Count > 0 Then
                            usrPadre.CriaOrPropId = ValidarNulos(dtProductoPadre.Rows(0).Item("prdt_prop_cria_id"), False)
                        End If
                    End If




                        txtMadre.Text = IIf(.Item("_madre").ToString.Length > 0, .Item("_madre"), String.Empty)
                        txtPadre.Text = IIf(.Item("_padre").ToString.Length > 0, .Item("_padre"), String.Empty)
                        txtSraNume.Valor = IIf(.Item("pdin_sra_nume").ToString.Length > 0, .Item("pdin_sra_nume"), String.Empty)
                    If Trim(txtSraNume.Text) = "" Then
                        btnPedigree.Disabled = True
                    Else
                        btnPedigree.Disabled = False
                    End If
                        txtInscFecha.Fecha = IIf(.Item("pdin_fecha").ToString.Length > 0, .Item("pdin_fecha"), String.Empty)
                        txtFechaValor.Fecha = IIf(.Item("pdin_fecha_valor").ToString.Length > 0, .Item("pdin_fecha_valor"), String.Empty)
                        If .Item("pdin_alta_fecha") Is DBNull.Value Or .Item("pdin_alta_fecha").ToString.Length = 0 Then
                            txtFechaAlta.Text = ""
                        Else
                            txtFechaAlta.Text = CDate(.Item("pdin_alta_fecha")).ToString("dd/MM/yyyy")
                        End If
                        txtSraNume.Valor = IIf(.Item("pdin_sra_nume").ToString.Length > 0, .Item("pdin_sra_nume"), String.Empty)
                        txtServ.Valor = IIf(.Item("_serv_denun").ToString.Length > 0, .Item("_serv_denun"), String.Empty)

                    If usrCria.Valor.ToString <> "" Then
                        clsWeb.gCargarCombo(mstrConn, "prefijos_cargar @cria_id=" & usrCria.Valor.ToString, cmbPref, "id", "descrip", "S")
                            cmbPref.Valor = IIf(.Item("pdin_pref_id").ToString.Length > 0, .Item("pdin_pref_id"), String.Empty)
                    Else
                        cmbPref.Items.Clear()
                    End If

                    If Not cmbPref.SelectedItem Is Nothing Then
                        If cmbPref.SelectedItem.Text <> "(Seleccione)" Then
                            hdnNombre.Text = cmbPref.SelectedItem.Text & txtNomb.Text      'Pantanettig 19/12/2007 Para llevar el nombre a POP - UP de Errores
                        Else
                            hdnNombre.Text = txtNomb.Text
                        End If
                    End If

                    If Not .IsNull("_esta_desc") Then
                        txtEsta.Text = .Item("_esta_desc")
                    Else
                        txtEsta.Text = ""
                    End If
                    If Not .IsNull("pdin_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("pdin_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If


                    If Not .IsNull("_pdin_apro_fecha") Then
                        lblAprob.Text = "Inscripci�n aprobada en fecha: " & CDate(.Item("_pdin_apro_fecha")).ToString("dd/MM/yyyy HH:mm") & ". El registro no puede ser modificado."
                    Else
                        lblAprob.Text = ""
                    End If

                    lblTitu.Text = "Datos del Producto: " & "  Raza: " & .Item("_raza_desc") & " - " & .Item("_sexo") & " - SRA Nro.: " & .Item("pdin_sra_nume")
                    lnkDocu.Enabled = .Item("_raza_diag")
                    hdnPrdt_id.Text = ValidarNulos(.Item("_prdt_id"), False)

                End With

                mSetearEditor(mstrTabla, False)
                mMostrarPanel(True)

            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub mAgregar()
        mLimpiar()

        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiarFiltros()

        hdnValorId.Text = "-1"  'para que ignore el id que vino del control
        txtNumeFil.Text = ""
        hdnSexoSele.Text = ""
        txtNombFil.Text = ""
        txtRPNumeFil.Text = ""
        txtNaciFechaDesdeFil.Text = ""
        txtNaciFechaHastaFil.Text = ""
        txtFechaPreseDesdeFil.Text = ""
        txtFechaPreseHastaFil.Text = ""
        txtFechaIngreDesdeFil.Text = ""
        txtFechaIngreHastaFil.Text = ""

        usrCriaFil.Valor = ""
        usrCriaFil.Limpiar()
        usrMadreFil.Limpiar()
        usrPadreFil.Limpiar()


        chkBusc.Checked = False
        chkBaja.Checked = False

        grdDato.Visible = False

    End Sub

    Public Sub grdDocu_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDocu.EditItemIndex = -1
            If (grdDocu.CurrentPageIndex < 0 Or grdDocu.CurrentPageIndex >= grdDocu.PageCount) Then
                grdDocu.CurrentPageIndex = 0
            Else
                grdDocu.CurrentPageIndex = E.NewPageIndex
            End If
            grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocu)
            grdDocu.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarDocum()
        hdnDocuId.Text = ""
        txtDocuObse.Valor = ""
        txtDocu.Valor = ""
        mSetearEditor(mstrTablaDocu, True)
    End Sub
    Private Sub mLimpiarServicio()
        hdnServId.Text = ""
        txtServ.Valor = ""
        txtMadre.Valor = ""
        txtPadre.Valor = ""
        txtServiFecha.Text = ""
    End Sub
    Private Sub mLimpiarTE()
        hdnTEId.Text = ""
        txtCruza.Text = ""
        txtCara.Text = ""
        txtTatu.Text = ""
        txtFechaServiTE.Text = ""
        txtFechaRecu.Text = ""
        txtImplFecha.Text = ""
        txtTeNume.Text = ""
    End Sub
    Private Sub mLimpiar()
        txtNroControl.Text = ""
        hdnCriaId.Text = ""
        hdnId.Text = ""
        hdnFiltroProdCriaId.Text = ""
        hdnPrdt_id.Text = ""
        btnPedigree.Disabled = True

        lblBaja.Text = ""
        lblAprob.Text = ""
        txtFechaAlta.Text = ""
        txtEsta.Text = ""
        txtSraNume.Text = ""
        txtRPNume.Text = ""
        txtRP.Text = ""

        txtNomb.Text = ""
        hdnServId.Text = ""
        txtServ.Valor = ""

        txtNaciFecha.Text = ""
            txtInscFecha.Fecha = Today
            txtFechaValor.Fecha = Today
            txtObse.Text = ""
        txtSiete.Text = ""
        txtPesoNacer.Text = ""
        txtEpdNacer.Text = ""
        txtFechaAlta.Text = ""
        txtCruza.Text = ""
        txtTatu.Text = ""
        txtTeNume.Text = ""
        txtFechaServiTE.Text = ""
        txtFechaRecu.Text = ""
        txtImplFecha.Text = ""
        txtCara.Text = ""

        cmbSexo.Limpiar()

        optServ.Checked = True
        btnServicios.Disabled = False
        btnTE.Disabled = True
        lblServ.Text = "Servicio: "

        cmbPref.Limpiar()
        cmbEsbl.Items.Clear()
        cmbCriaTipo.Limpiar()

        cmbPelaA.Limpiar()
        cmbPelaB.Limpiar()
        cmbPelaC.Limpiar()
        cmbPelaD.Limpiar()

        chkServManual.Checked = False

        usrCria.Limpiar()
        usrCriader.Limpiar()
        usrPadre.Limpiar()
        usrMadre.Limpiar()
        'usrRece.Limpiar()
        txtMadre.Text = ""
        txtPadre.Text = ""

        usrPadre.RazaId = ""
        usrMadre.RazaId = ""


        mLimpiarPeliferos()
        mLimpiarCriolla()
        mLimpiarOviCapri()
        mLimpiarHolando()
        mLimpiarBovNOHolando()

        mCrearDataSet("")

        lblTitu.Text = ""
        mLimpiarDocum()
        mLimpiarServicio()
        mLimpiarTE()

        If Not Session("sRazaId") Is Nothing Then
            usrCria.cmbCriaRazaExt.Valor = Session("sRazaId").ToString
        End If
        mSetearEditor(mstrTabla, True)

    End Sub

    Private Sub mLimpiarCriolla()
        'criolla
        cmbCrioPelaA.Limpiar()
        cmbCrioPelaB.Limpiar()
        cmbCrioPelaC.Limpiar()
        cmbCrioPelaD.Limpiar()
    End Sub
    Private Sub mLimpiarPeliferos()

        txtRPNumePeli.Valor = ""
        txtRPPeli.Valor = ""

        cmbPelaAPeli.Limpiar()
        cmbPelaBPeli.Limpiar()
        cmbPelaCPeli.Limpiar()
        cmbPelaDPeli.Limpiar()
    End Sub
    Private Sub mLimpiarHolando()
        txtRPNumeHolan.Valor = ""
        txtRPHolan.Valor = ""
        txtNaciFechaHolan.Text = ""
        cmbHolanPelaA.Limpiar()


    End Sub
    Private Sub mLimpiarBovNOHolando()
        cmbBovParto.Limpiar()
        cmbBovCria.Limpiar()
        cmbSexoBov.Limpiar()
        txtNaciFechaBov.Text = ""
        txtRpBov.Valor = ""
        txtRpnumeBov.Valor = ""
        cmbBovPelaA.Limpiar()
    End Sub
    Private Sub mLimpiarOviCapri()
        cmbSexoOviCapri.Limpiar()
        txtRPOviCapri.Valor = ""
        txtRPNumeOviCapri.Valor = ""
        txtNaciFechaOviCapri.Text = ""
        cmbSexoOviCapri.Limpiar()
    End Sub


    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)

        lnkCabecera.Font.Bold = True
        lnkDocu.Font.Bold = False

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panDocu.Visible = False

        tabLinks.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible

    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        Dim lstrTitu As String
        If lblTitu.Text <> "" Then
            lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
        End If

        panDato.Visible = True
        panBotones.Visible = True
        panDocu.Visible = False
        panCabecera.Visible = False

        lnkCabecera.Font.Bold = False
        lnkDocu.Font.Bold = False

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Visible = False
            Case 2
                ' mGuardarDatos(False)
                panDocu.Visible = True
                lnkDocu.Font.Bold = True
                lblTitu.Text = "Producto: " & lstrTitu
                lblTitu.Visible = True
                grdDocu.Visible = True
        End Select

    End Sub

#End Region

#Region "Opciones de ABM"
    Private Sub mLogErrores(ByVal pstrId As String)

        Dim lstrMsgError As String
        Dim lstrFecha As String = ""
        'Dim dsVali As DataSet = New DataSet

        ' Dario 2014-10-29
        'Dim oProductosInscrip As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString(), mdsDatos.Copy)

        'Select Case SRA_Neg.Utiles.EspeRaza(usrCria.cmbCriaRazaExt.Valor, mstrConn)
        '    Case Tipo.EspePeliferos
        '        lstrFecha = txtFechaNacPeli.Text
        '    Case Tipo.EspeOvinos, Tipo.EspeCaprinos
        '        lstrFecha = txtNaciFechaOviCapri.Text
        '    Case Tipo.EspeBovinosNOHolando
        '        lstrFecha = txtNaciFechaBov.Text
        '    Case Tipo.RazaHolando
        '        lstrFecha = txtNaciFechaHolan.Text
        '    Case Tipo.RazaCriolla
        '        lstrFecha = txtNaciFecha.Text
        '    Case Else
        '        lstrFecha = txtNaciFecha.Text
        'End Select


        'dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), "W", lstrFecha, lstrFecha)

        'Limpiar errores anteriores.
        'ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTabla, pstrId)

        'Aplicar Reglas.
        'lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, pstrId, usrCria.cmbCriaRazaExt.Valor, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), 0, pstrId)

        'If lstrMsgError <> "" Then
        'oProductosInscrip.GrabarEstadoRetenidaEnProductosInscrip(pstrId)
        'AccesoBD.clsError.gGenerarMensajes(Me, lstrMsgError)
        'End If
        If (Me.txtSraNume.Text = "") Then
            'Limpiar errores anteriores.
            ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, pstrId)

            'Aplicar Reglas.
            lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, pstrId, usrCria.cmbCriaRazaExt.Valor, Me.hdnSexo.Text, Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), 0, 0)
        End If

        If lstrMsgError <> "" Then
            AccesoBD.clsError.gGenerarMensajes(Me, "Se detectaron errores validatorios, por favor verifiquelo.")
        End If

    End Sub
    Private Sub mAlta()
        Try
            Dim lstrProdId As String

            mGuardarDatos(True)
            Dim lobjInscri As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)

            lstrProdId = lobjInscri.Alta()
            txtFechaAlta.Text = Today

            mLogErrores(lstrProdId)

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try


    End Sub

    Private Sub mModi()
        Try

            mGuardarDatos(True)
            Dim lobjInscri As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)

            lobjInscri.Modi()

            mLogErrores(hdnId.Text)

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjInscri As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            lobjInscri.Baja(hdnId.Text)

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatosDocum(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrDoc As DataRow
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lbooGene As Boolean

            hdnDocuId.Text = E.Item.Cells(1).Text
            ldrDoc = mdsDatos.Tables(mstrTablaDocu).Select("pido_id=" & hdnDocuId.Text)(0)

            With ldrDoc
                txtDocu.Text = .Item("pido_docu_path")
                txtDocuObse.Valor = .Item("pido_obser")
            End With

            mSetearEditor(mstrTablaDocu, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarDocum(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosDocum(pbooAlta)

            mLimpiarDocum()
            grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocu)
            grdDocu.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosDocum(ByVal pbooAlta As Boolean)
        Dim ldrDatos As DataRow
        Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_prdt_docu_path")

        If txtDocu.Valor.ToString = "" And filDocu.Value = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
        End If

        If hdnDocuId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDocu).NewRow
            ldrDatos.Item("pido_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocu), "pido_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaDocu).Select("pido_id=" & hdnDocuId.Text)(0)
        End If

        With ldrDatos
            .Item("pido_pdin_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("pido_baja_fecha") = DBNull.Value

            If filDocu.Value <> "" Then
                .Item("pido_docu_path") = filDocu.Value
            Else
                .Item("pido_docu_path") = txtDocu.Valor
            End If
            If Not .IsNull("pido_docu_path") Then
                .Item("pido_docu_path") = .Item("pido_docu_path").Substring(.Item("pido_docu_path").LastIndexOf("\") + 1)
            End If
            .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocu + "_" + Session("MilSecc") + "_" + _
            Replace(.Item("pido_id"), "-", "m") + "__" + .Item("pido_docu_path")

            .Item("pido_obser") = txtDocuObse.Valor

            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocu)
        End With

        If hdnDocuId.Text = "" Then
            mdsDatos.Tables(mstrTablaDocu).Rows.Add(ldrDatos)
        End If

    End Sub

    Private Function mGuardarDatos(ByVal pbooVali As Boolean) As DataSet

        If mdsDatos Is Nothing Then
            Exit Function
        End If

        If pbooVali Then

            mValidarDatos()

            'If usrPadre.Valor = 0 Or usrMadre.Valor = 0 Then
            '   Throw New AccesoBD.clsErrNeg("Debe ingresar el padre y la madre.")
            'End If

            If Not cmbPref.Valor Is DBNull.Value Then
                If (cmbPref.SelectedItem.Text.Trim.Length + txtNomb.Text.Length + 1) > 45 Then
                    Throw New AccesoBD.clsErrNeg("El nombre y el prefijo no pueden superar los 45 caracteres.")
                End If
            End If


            If Not cmbPref.Valor Is DBNull.Value Then
                If (cmbPref.SelectedItem.Text.Trim.Length + 1) > 35 Then
                    Throw New AccesoBD.clsErrNeg("El prefijo no puede superar los 35 caracteres.")
                End If
            End If

        End If

        If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then



            With mdsDatos.Tables(0).Rows(0)

                'solo peliferos
                Select Case SRA_Neg.Utiles.EspeRaza(usrCria.cmbCriaRazaExt.Valor, mstrConn)
                    Case Tipo.EspePeliferos
                        .Item("pdin_crdr_id") = usrCriader.Valor
                        .Item("pdin_rp") = txtRPPeli.Valor
                        .Item("pdin_rp_nume") = txtRPNumePeli.Valor
                        'txtMarcaPeli.Valor = 'no hay campo
                        .Item("pdin_naci_fecha") = txtFechaNacPeli.Fecha
                        .Item("pdin_a_pela_id") = cmbPelaAPeli.Valor
                        .Item("pdin_b_pela_id") = cmbPelaBPeli.Valor
                        .Item("pdin_c_pela_id") = cmbPelaCPeli.Valor
                        .Item("pdin_d_pela_id") = cmbPelaDPeli.Valor
                        If Not cmbSexo.Valor Is DBNull.Value Then
                            .Item("pdin_sexo") = IIf(cmbSexo.Valor = "1", True, False)
                        Else
                            .Item("pdin_sexo") = cmbSexo.Valor
                        End If
                        Me.hdnSexo.Text = cmbSexo.Valor
                    Case Tipo.EspeOvinos, Tipo.EspeCaprinos
                        'If Not cmbPref.Valor Is DBNull.Value Then
                        '   .Item("pdin_nomb") = cmbPref.SelectedItem.Text.Trim & " " & txtNomb.Text
                        'Else
                        .Item("pdin_nomb") = txtNomb.Text
                        'End If
                        .Item("pdin_rp") = txtRPOviCapri.Valor
                        .Item("pdin_rp_nume") = txtRPNumeOviCapri.Valor
                        .Item("pdin_naci_fecha") = txtNaciFechaOviCapri.Fecha
                        If Not cmbSexoOviCapri.Valor Is DBNull.Value Then
                            .Item("pdin_sexo") = IIf(cmbSexoOviCapri.Valor = "1", True, False)
                        Else
                            .Item("pdin_sexo") = cmbSexoOviCapri.Valor
                        End If
                        Me.hdnSexo.Text = cmbSexoOviCapri.Valor
                    Case Tipo.EspeBovinosNOHolando
                        'If Not cmbPref.Valor Is DBNull.Value Then
                        '   .Item("pdin_nomb") = cmbPref.SelectedItem.Text.Trim & " " & txtNomb.Text
                        'Else
                        .Item("pdin_nomb") = txtNomb.Text
                        'End If
                        If Not cmbSexoBov.Valor Is DBNull.Value Then
                            .Item("pdin_sexo") = IIf(cmbSexoBov.Valor = "1", True, False)
                        Else
                            .Item("pdin_sexo") = cmbSexoBov.Valor
                        End If
                        Me.hdnSexo.Text = cmbSexoBov.Valor
                        .Item("pdin_naci_fecha") = txtNaciFechaBov.Fecha
                        .Item("pdin_rp") = txtRpBov.Valor
                        .Item("pdin_rp_nume") = txtRpnumeBov.Valor
                            .Item("pdin_ticr_id") = IIf(cmbCriaTipoBov.Valor.ToString.Length > 0, cmbCriaTipoBov.Valor, DBNull.Value)
                            .Item("pdin_a_pela_id") = IIf(cmbBovPelaA.Valor.Length > 0, cmbBovPelaA.Valor, DBNull.Value)
                            .Item("pdin_parto") = IIf(cmbBovParto.Valor.Length > 0, cmbBovParto.Valor, DBNull.Value)
                            .Item("pdin_cria") = IIf(cmbBovCria.Valor.Length > 0, cmbBovCria.Valor, DBNull.Value)

                    Case Tipo.RazaHolando
                        'If Not cmbPref.Valor Is DBNull.Value Then
                        '   .Item("pdin_nomb") = cmbPref.SelectedItem.Text.Trim & " " & txtNomb.Text
                        'Else
                        .Item("pdin_nomb") = txtNomb.Text
                        'End If
                        If Not cmbSexo.Valor Is DBNull.Value Then
                            .Item("pdin_sexo") = IIf(cmbSexo.Valor = "1", True, False)
                        Else
                            .Item("pdin_sexo") = cmbSexo.Valor
                        End If
                        Me.hdnSexo.Text = cmbSexo.Valor
                        .Item("pdin_naci_fecha") = txtNaciFechaHolan.Fecha
                        .Item("pdin_rp") = txtRPHolan.Valor
                        .Item("pdin_rp_nume") = txtRPNumeHolan.Valor
                        .Item("pdin_ticr_id") = cmbCriaTipo.Valor
                        .Item("pdin_a_pela_id") = cmbHolanPelaA.Valor
                        '.Item("pdin_trans_embr_fecha") = txtReceFecha.Fecha
                        '.Item("pdin_trans_embr_rece_id") = usrRece.Valor
                    Case Tipo.RazaCriolla
                        'If Not cmbPref.Valor Is DBNull.Value Then
                        '   .Item("pdin_nomb") = cmbPref.SelectedItem.Text.Trim & " " & txtNomb.Text
                        'Else
                        .Item("pdin_nomb") = txtNomb.Text
                        'End If
                        If Not cmbSexo.Valor Is DBNull.Value Then
                            .Item("pdin_sexo") = IIf(cmbSexo.Valor = "1", True, False)
                        Else
                            .Item("pdin_sexo") = cmbSexo.Valor
                        End If
                        Me.hdnSexo.Text = cmbSexo.Valor
                            .Item("pdin_naci_fecha") = IIf(txtNaciFecha.Fecha.Length > 0, txtNaciFecha.Fecha, DBNull.Value)
                            .Item("pdin_rp") = IIf(txtRP.Valor.Length > 0, txtRP.Valor, DBNull.Value)
                            .Item("pdin_rp_nume") = IIf(txtRPNume.Valor.Length > 0, txtRPNume.Valor, DBNull.Value)
                            .Item("pdin_a_pela_id") = IIf(cmbCrioPelaA.Valor.Length > 0, cmbCrioPelaA.Valor, DBNull.Value)
                            .Item("pdin_b_pela_id") = IIf(cmbCrioPelaB.Valor.Length > 0, cmbCrioPelaB.Valor, DBNull.Value)
                            .Item("pdin_c_pela_id") = IIf(cmbCrioPelaC.Valor.Length > 0, cmbCrioPelaC.Valor, DBNull.Value)
                            .Item("pdin_d_pela_id") = IIf(cmbCrioPelaD.Valor.Length > 0, cmbCrioPelaD.Valor, DBNull.Value)
                    Case Else
                        'If Not cmbPref.Valor Is DBNull.Value Then
                        '   .Item("pdin_nomb") = cmbPref.SelectedItem.Text.Trim & " " & txtNomb.Text
                        'Else
                        .Item("pdin_nomb") = txtNomb.Text
                        'End If
                        If Not cmbSexo.Valor Is DBNull.Value Then
                            .Item("pdin_sexo") = IIf(cmbSexo.Valor = "1", True, False)
                        Else
                            .Item("pdin_sexo") = cmbSexo.Valor
                        End If
                        Me.hdnSexo.Text = cmbSexo.Valor
                        .Item("pdin_rp") = txtRP.Valor
                            .Item("pdin_rp_nume") = IIf(txtRPNume.Valor.Length > 0, txtRPNume.Valor, DBNull.Value)
                            .Item("pdin_naci_fecha") = IIf(txtNaciFecha.Fecha.Length > 0, txtNaciFecha.Fecha, DBNull.Value)
                            .Item("pdin_a_pela_id") = IIf(cmbPelaA.Valor.Length > 0, cmbPelaA.Valor, DBNull.Value)
                            .Item("pdin_b_pela_id") = IIf(cmbPelaB.Valor.Length > 0, cmbPelaB.Valor, DBNull.Value)
                            .Item("pdin_c_pela_id") = IIf(cmbPelaC.Valor.Length > 0, cmbPelaC.Valor, DBNull.Value)
                            .Item("pdin_d_pela_id") = IIf(cmbPelaD.Valor.Length > 0, cmbPelaD.Valor, DBNull.Value)
                End Select
                'comun en todas
                .Item("pdin_nro_control") = ValidarNulos(txtNroControl.Text, False)
                .Item("pdin_raza_id") = usrCria.cmbCriaRazaExt.Valor
                .Item("pdin_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("pdin_obser") = txtObse.Valor
                If txtServiFecha.Text <> "" Then
                        .Item("pdin_servi_fecha") = IIf(txtServiFecha.Fecha.Length > 0, txtServiFecha.Fecha, DBNull.Value)
                Else
                        .Item("pdin_servi_fecha") = IIf(txtFechaServiTE.Fecha.Length > 0, txtFechaServiTE.Fecha, DBNull.Value)
                End If
                If txtSiete.Text = "" Then
                    .Item("pdin_siete") = DBNull.Value
                Else
                    .Item("pdin_siete") = txtSiete.Valor
                End If
                .Item("pdin_tede_nume") = txtTeNume.Valor
                    .Item("pdin_tede_recu_fecha") = IIf(txtFechaRecu.Fecha.Length > 0, txtFechaRecu.Fecha, DBNull.Value)
                    .Item("pdin_tedd_impl_fecha") = IIf(txtImplFecha.Fecha.Length > 0, txtImplFecha.Fecha, DBNull.Value)
                    .Item("pdin_tedd_carv") = IIf(txtCara.Valor.Length > 0, txtCara.Valor, DBNull.Value)
                    .Item("pdin_tedd_tatu") = IIf(txtTatu.Valor.Length > 0, txtTatu.Valor, DBNull.Value)
                    .Item("pdin_tedd_raza_cruza") = IIf(txtCruza.Valor.Length > 0, txtCruza.Valor, DBNull.Value)
                    .Item("pdin_peso_nacer") = IIf(txtPesoNacer.Valor.Length > 0, txtPesoNacer.Valor, DBNull.Value)
                    .Item("pdin_epd_nacer") = IIf(txtEpdNacer.Valor.Length > 0, txtEpdNacer.Valor, DBNull.Value)
                .Item("pdin_cria_id") = usrCria.Valor
                    .Item("pdin_esbl_id") = IIf(cmbEsbl.Valor.Length > 0, cmbEsbl.Valor, DBNull.Value)
                If Not chkServManual.Checked Then
                    If optServ.Checked Then
                        .Item("pdin_tedd_id") = DBNull.Value
                        .Item("pdin_sdde_id") = clsSQLServer.gFormatArg(hdnServId.Text, SqlDbType.Int)
                    Else
                        .Item("pdin_sdde_id") = DBNull.Value
                        .Item("pdin_tedd_id") = clsSQLServer.gFormatArg(hdnTEId.Text, SqlDbType.Int)
                    End If
                Else
                    .Item("pdin_sdde_id") = DBNull.Value
                    .Item("pdin_tedd_id") = DBNull.Value
                End If
                .Item("pdin_padre_id") = usrPadre.Valor
                .Item("pdin_padre_rp") = usrPadre.txtRPExt.Text
                If usrPadre.txtSraNumeExt.Text = "" Then
                    .Item("pdin_padre_sra_nume") = DBNull.Value
                Else
                    .Item("pdin_padre_sra_nume") = usrPadre.txtSraNumeExt.Text
                End If

                If usrPadre.hdnRegtId.Text <> "" Then .Item("regis_padre_id") = usrPadre.hdnRegtId.Text
                .Item("pdin_madre_id") = usrMadre.Valor
                .Item("pdin_madre_rp") = usrMadre.txtRPExt.Text
                If usrMadre.txtSraNumeExt.Text = "" Then
                    .Item("pdin_madre_sra_nume") = DBNull.Value
                Else
                    .Item("pdin_madre_sra_nume") = usrMadre.txtSraNumeExt.Text
                End If
                If usrMadre.hdnRegtId.Text <> "" Then .Item("regis_madre_id") = usrMadre.hdnRegtId.Text
                    .Item("pdin_fecha") = IIf(txtInscFecha.Fecha.Length > 0, txtInscFecha.Fecha, DBNull.Value)
                    .Item("pdin_fecha_valor") = IIf(txtFechaValor.Fecha.Length > 0, txtFechaValor.Fecha, DBNull.Value)
                    .Item("pdin_sra_nume") = IIf(txtSraNume.Valor.Length > 0, txtSraNume.Valor, DBNull.Value)
                    .Item("pdin_pref_id") = IIf(cmbPref.Valor.Length > 0, cmbPref.Valor, DBNull.Value)
            End With
        End If
        Return mdsDatos
    End Function

    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDocu

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        Session("sessProdInscrip") = mdsDatos

        grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocu)
        grdDocu.DataBind()
        grdDocu.Visible = True

    End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    'Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
    '    mBaja()
    'End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub btnAltaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocu.Click
        mActualizarDocum(True)
    End Sub

    Private Sub btnLimpDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocu.Click
        mLimpiarDocum()
    End Sub

    Private Sub btnBajaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocu.Click
        Try
            With mdsDatos.Tables(mstrTablaDocu).Select("pido_id=" & hdnDocuId.Text)(0)
                '.Item("pido_baja_fecha") = System.DateTime.Now.ToString
                .Delete()
            End With
            grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocu)
            grdDocu.DataBind()
            mLimpiarDocum()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnModiDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocu.Click
        mActualizarDocum(False)
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub

    Private Sub cc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
        mShowTabs(2)
    End Sub

    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            hdnServId.Text = ""
            txtServ.Valor = ""
            txtMadre.Valor = ""
            txtMadre.Valor = ""

            If (hdnDatosPop.Text <> "") Then
                lblServ.Text = "Servicio: "
                hdnServId.Text = hdnDatosPop.Text
                mDescripServicio(hdnDatosPop.Text)
                hdnDatosPop.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub hdnDatosTEPop_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdnDatosTEPop.TextChanged
        Try
            hdnTEId.Text = ""
            txtServ.Valor = ""
            txtMadre.Valor = ""
            txtMadre.Valor = ""

            If (hdnDatosTEPop.Text <> "") Then
                lblServ.Text = "Transplante: "
                hdnTEId.Text = hdnDatosTEPop.Text
                mDescripTE(hdnDatosTEPop.Text)
                hdnDatosTEPop.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Validaciones"
    Private Sub mValidarPeliferos()
        If usrCriader.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el criadero.")
        End If
        If txtRPPeli.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (texto).")
        End If
        If txtRPNumePeli.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (n�mero).")
        End If
        'If txtFechaNacPeli.Fecha Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de nacimiento.")
        'End If
        If Not txtFechaNacPeli.Fecha Is DBNull.Value Then
            If txtFechaNacPeli.Fecha > txtInscFecha.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de inscripci�n.")
            End If
            If txtFechaNacPeli.Fecha > txtFechaValor.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de la denuncia.")
            End If
        End If
        'If cmbSexo.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el sexo.")
        'End If


        ' If txtRegPadresPeli.Valor Is DBNull.Value Then
        ' Throw New AccesoBD.clsErrNeg("Falta indicar en que registros se encuentran inscriptos los padres.")
        'End If
        'no esta el campo todavia
        ' If txtMarcaPeli.Valor Is DBNull.Value Then
        ' Throw New AccesoBD.clsErrNeg("Debe ingresar la marca.")
        'End
    End Sub
    Private Sub mValidarBovinosNOHolando()
        'If txtNaciFechaBov.Fecha Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de nacimiento.")
        'End If
        If Not txtNaciFechaBov.Fecha Is DBNull.Value Then
            If txtNaciFechaBov.Fecha > txtInscFecha.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de inscripci�n.")
            End If
            If txtNaciFechaBov.Fecha > txtFechaValor.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de la denuncia.")
            End If
        End If
        If txtRpBov.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (texto).")
        End If
        If txtRpnumeBov.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (n�mero).")
        End If
        'If cmbSexoBov.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el sexo.")
        'End If
        If txtNomb.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el nombre del producto.")
        End If
    End Sub
    Private Sub mValidarCaprinosOvinos()
        'If cmbSexoOviCapri.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el sexo.")
        'End If
        If txtRPOviCapri.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el tatuaje (texto).")
        End If
        If txtRPNumeOviCapri.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el tatuaje (n�mero).")
        End If
        'If txtNaciFechaOviCapri.Fecha Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de nacimiento.")
        'End If
        If Not txtNaciFechaOviCapri.Fecha Is DBNull.Value Then
            If txtNaciFechaOviCapri.Fecha > txtInscFecha.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de inscripci�n.")
            End If
            If txtNaciFechaOviCapri.Fecha > txtFechaValor.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de la denuncia.")
            End If
        End If
        If txtNomb.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el nombre del producto.")
        End If
        'If cmbEsbl.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el establecimiento.")
        'End If
    End Sub
    Private Sub mValidarCriolla()
        'If cmbSexo.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el sexo.")
        'End If
        'If txtNaciFecha.Fecha Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de nacimiento.")
        'End If
        If Not txtNaciFecha.Fecha Is DBNull.Value Then
            If txtNaciFecha.Fecha > txtInscFecha.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de inscripci�n.")
            End If
            If txtNaciFecha.Fecha > txtFechaValor.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de la denuncia.")
            End If
        End If
        If txtRP.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (texto).")
        End If
        If txtRPNume.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (n�mero).")
        End If
        If txtNomb.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el nombre del producto.")
        End If
        'If cmbEsbl.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el establecimiento.")
        'End If
    End Sub
    Private Sub mValidarHolando()
        'If cmbSexo.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el sexo.")
        'End If
        'If txtNaciFechaHolan.Fecha Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de nacimiento.")
        'End If
        If Not txtNaciFechaHolan.Fecha Is DBNull.Value Then
            If txtNaciFechaHolan.Fecha > txtInscFecha.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de inscripci�n.")
            End If
            If txtNaciFechaHolan.Fecha > txtFechaValor.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de la denuncia.")
            End If
        End If
        If txtRPHolan.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (texto).")
        End If
        If txtRPNumeHolan.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (n�mero).")
        End If
        If txtNomb.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el nombre del producto.")
        End If
        'If cmbEsbl.Valor Is DBNull.Value Then
        '   Throw New AccesoBD.clsErrNeg("Debe ingresar el establecimiento.")
        'End If
        'If cmbCriaTipo.Valor Is DBNull.Value Then
        'Throw New AccesoBD.clsErrNeg("Debe ingresar el tipo de cria.")
        'End If        
    End Sub
    Private Sub mValidacionGenerica()
        ' madre/padre
        'If usrMadre.Valor Is DBNull.Value Then
        'Throw New AccesoBD.clsErrNeg("Debe seleccionar la madre.")
        'End If
        'If usrPadre.Valor Is DBNull.Value Then
        'Throw New AccesoBD.clsErrNeg("Debe seleccionar el padre.")
        'End If
        'If Not chkServManual.Checked And (hdnServId.Text = "" Or hdnServId.Text = "0") And (hdnTEId.Text = "" Or hdnTEId.Text = "0") Then
        '   Throw New AccesoBD.clsErrNeg("Debe seleccionar un servicio o un transplante, de lo contrario cargar manualmente los padres.")
        'End If
    End Sub

    Private Sub mValidarDatos()

        Dim drTramites As DataRow
        Dim iCriador As Integer

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
        Select Case SRA_Neg.Utiles.EspeRaza(usrCria.cmbCriaRazaExt.Valor, mstrConn)
            Case Tipo.EspePeliferos
                mValidarPeliferos()
            Case Tipo.EspeBovinosNOHolando
                mValidarBovinosNOHolando()
            Case Tipo.EspeCaprinos, Tipo.EspeOvinos
                mValidarCaprinosOvinos()
            Case Tipo.RazaCriolla
                mValidarCriolla()
            Case Tipo.RazaHolando
                mValidarHolando()
            Case Else
                If txtNaciFecha.Fecha Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de nacimiento.")
                End If
                If txtNaciFecha.Fecha > txtInscFecha.Fecha Then
                    Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de inscripci�n.")
                End If
                If txtNaciFecha.Fecha > txtFechaValor.Fecha Then
                    Throw New AccesoBD.clsErrNeg("La fecha de nacimiento no puede ser mayor a la de la denuncia.")
                End If
                If txtRP.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (texto).")
                End If
                If txtRPNume.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de RP (n�mero).")
                End If

                If txtNomb.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el nombre del producto.")
                End If
                'If cmbEsbl.Valor Is DBNull.Value Then
                '   Throw New AccesoBD.clsErrNeg("Debe ingresar el establecimiento.")
                'End If
                If cmbSexo.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el sexo.")
                End If

        End Select

        If ValidarNulos(txtNroControl.Valor, False) = 0 Then
            Throw New AccesoBD.clsErrNeg("El Nro de Control debe ser distinto de 0.")
        End If
        Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())

        iCriador = ValidarNulos(usrCria.Valor, False)



        Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
        drTramites = oTramites.GetCantFacturadoByNroControlRazaCriador(txtNroControl.Valor, _
                      usrPadre.RazaId.ToString(), _
                     iCriador.ToString())

        If IsNothing(drTramites) Then
            Throw New AccesoBD.clsErrNeg("No se encontraron comprobantes ,ni proforma con el Nro.Control " & txtNroControl.Valor)
        Else



        End If
        mValidacionGenerica()
    End Sub
#End Region

#Region "Funciones_privadas"


    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub mDescripTE(ByVal pstrId As String)
        Dim lsrtFiltro As String = " @tedd_id =" + clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)

        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.te_denun_deta_naci_busq " & lsrtFiltro)
        For Each ldr As DataRow In ds.Tables(0).Select
            With ldr
                txtServ.Valor = "Nro: " + Convert.ToString(.Item("tede_nume")) + " - " + Convert.ToString(.Item("tedd_impl_fecha"))
                txtMadre.Valor = .Item("Madre")
                txtPadre.Valor = .Item("Padre")
                usrMadre.Valor = .Item("tede_madr_prdt_id")
                usrPadre.Valor = .Item("tede_pad1_prdt_id")
                txtCara.Valor = .Item("tedd_carv")
                txtTatu.Valor = .Item("tedd_tatu")
                txtCruza.Valor = .Item("tedd_raza_cruza")
                txtFechaServiTE.Fecha = .Item("tede_serv_fecha")
                txtImplFecha.Fecha = .Item("tedd_impl_fecha")
                txtFechaRecu.Fecha = .Item("tede_recu_fecha")
                txtTeNume.Valor = .Item("tede_nume")
            End With
        Next

        ' txtServ.Valor = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_servi_denuncias_deta", "_serv_denunc", lsrtFiltro)
        ' txtMadre.Valor = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_servi_denuncias_deta", "_madre", lsrtFiltro)
        '  usrMadre.Valor = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_servi_denuncias_deta", "sdde_madre_id", lsrtFiltro)
    End Sub

    Private Sub mDescripServicio(ByVal pstrId As String)
        Dim lsrtFiltro As String = " @sdde_id=" + clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)

        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.rg_servi_denuncias_deta_consul " & lsrtFiltro)
        For Each ldr As DataRow In ds.Tables(0).Select
            With ldr
                txtServ.Valor = .Item("_serv_denunc")
                txtServiFecha.Fecha = .Item("sdde_servi_desde")
                txtMadre.Valor = .Item("_madre")
                txtPadre.Valor = .Item("_padre")
                usrMadre.Valor = .Item("sdde_madre_id")
                usrPadre.Valor = .Item("sdde_padre_id")
            End With
        Next

        ' txtServ.Valor = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_servi_denuncias_deta", "_serv_denunc", lsrtFiltro)
        ' txtMadre.Valor = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_servi_denuncias_deta", "_madre", lsrtFiltro)
        '  usrMadre.Valor = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_servi_denuncias_deta", "sdde_madre_id", lsrtFiltro)
    End Sub

#End Region





End Class
End Namespace
