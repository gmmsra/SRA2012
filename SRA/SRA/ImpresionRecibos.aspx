<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%--<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImpresionRecibos.aspx.vb" Inherits="SRA.ImpresionRecibos" %>--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/ImpresionRecibos.aspx.vb" Inherits="ImpresionRecibos" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Impresi�n de Recibos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="391px">Impresi�n de Recibos</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" background="imagenes/formfdofields.jpg"
																	border="0">
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblTarjFil" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:combobox class="combo" id="cmbTarjFil" runat="server" Width="200px" AceptaNull="False"></cc1:combobox></TD>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Button id="btnBuscar" tabIndex="2" runat="server" cssclass="boton" Width="80px" text="Buscar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;
																		</TD>
																		<TD colSpan="3">
																			<cc1:numberbox id="txtPeriFil" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
																				MaxLength="2" MaxValor="12" CantMax="2"></cc1:numberbox>
																			<cc1:combobox class="combo" id="cmbPetiFil" runat="server" Width="80px" AceptaNull="False"></cc1:combobox>&nbsp;&nbsp;
																			<asp:Label id="Label1" runat="server" cssclass="titulo">/</asp:Label>&nbsp;
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="35px" AceptaNull="False"
																				MaxLength="4" MaxValor="2090" CantMax="4"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="4">
																			<asp:CheckBox id="chkTodas" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small"
																				Text="Todas"></asp:CheckBox></TD>
																	</TR>
																	<TR height="10">
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 1.81%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 12.69%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg" height="10"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD width="3" height="10"></TD>
															<TD align="right" height="10"></TD>
															<TD width="2" height="10"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE> <!---fin filtro --->
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3">
									<asp:datagrid id="grdDatoBusq" runat="server" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
										AutoGenerateColumns="False" width="100%" PageSize="15">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server" onclick="chkSelCheck(this);"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="deca_id" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_gene_fecha" HeaderText="Generado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_envi_fecha" HeaderText="Env�o" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_rece_fecha" HeaderText="Recepci�n" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_tarj_desc" HeaderText="Tarjeta"></asp:BoundColumn>
											<asp:BoundColumn DataField="_perio" HeaderText="Per�odo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cant" HeaderText="Cant."></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD background="imagenes/formfdofields.jpg" height="24"></TD>
								<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" height="24">
									<asp:Label id="lblSociDesde" runat="server" cssclass="titulo">Socio desde:&nbsp;</asp:Label></TD>
								<TD background="imagenes/formfdofields.jpg" height="24">
									<CC1:NumberBox id="txtSociDesde" runat="server" cssclass="cuadrotexto" Width="75px" AceptaNull="False"></CC1:NumberBox>&nbsp;
									<asp:Label id="lblSociHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:Label>
									<CC1:NumberBox id="txtSociHasta" runat="server" cssclass="cuadrotexto" Width="75px" AceptaNull="False"></CC1:NumberBox></TD>
							</TR>
							<TR>
								<TD background="imagenes/formfdofields.jpg" height="24"></TD>
								<TD align="right" background="imagenes/formfdofields.jpg" height="24">
									<asp:Label id="lblCPDesde" runat="server" cssclass="titulo">CP desde:&nbsp;</asp:Label></TD>
								<TD background="imagenes/formfdofields.jpg" height="24">
									<CC1:TEXTBOXTAB id="txtCPDesde" runat="server" cssclass="cuadrotexto" Width="75px" MaxLength="8"></CC1:TEXTBOXTAB>&nbsp;
									<asp:Label id="lblCPHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:Label>
									<CC1:TEXTBOXTAB id="txtCPHasta" runat="server" cssclass="cuadrotexto" Width="75px" MaxLength="8"></CC1:TEXTBOXTAB></TD>
							</TR>
							<TR>
								<TD background="imagenes/formfdofields.jpg" height="24"></TD>
								<TD align="right" background="imagenes/formfdofields.jpg" valign="top" height="24">
									<asp:Label id="Label2" runat="server" cssclass="titulo">Texto a elecci�n:&nbsp;</asp:Label></TD>
								<TD background="imagenes/formfdofields.jpg" height="24">
									<CC1:TEXTBOXTAB id="txtTexto1" runat="server" cssclass="cuadrotexto" Width="175px" Obligatorio="True"
										MaxLength="8"></CC1:TEXTBOXTAB><br>
									<CC1:TEXTBOXTAB id="txtTexto2" runat="server" cssclass="cuadrotexto" Width="175px" Obligatorio="True"
										MaxLength="8"></CC1:TEXTBOXTAB><br>
									<CC1:TEXTBOXTAB id="txtTexto3" runat="server" cssclass="cuadrotexto" Width="175px" Obligatorio="True"
										MaxLength="8"></CC1:TEXTBOXTAB><br>
									<CC1:TEXTBOXTAB id="txtTexto4" runat="server" cssclass="cuadrotexto" Width="175px" Obligatorio="True"
										MaxLength="8"></CC1:TEXTBOXTAB>
								</TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="right" colSpan="3">
									<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
										ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
										IncludesUrl="includes/" ImagesUrl="imagenes/" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		try{
			if (document.all["editar"]!= null)
				document.location='#editar';
			if (document.all["cmbEmprFil"]!= null)
				document.all["cmbEmprFil"].focus();
			if (document.all["cmbEmpr"]!= null)
				document.all["cmbEmpr"].focus();
			}
		catch(e){;}

		function chkSelCheck(pChk)
		{
			var lintFil= eval(pChk.id.replace("grdDatoBusq__ctl","").replace("_chkSel",""));

			if (document.all("grdDatoBusq").children(0).children.length > 1)
			{
				for (var fila=3; fila <= document.all("grdDatoBusq").children(0).children.length; fila++)
				{
					if (fila!=lintFil)
					{
						document.all("grdDatoBusq__ctl" + fila.toString() + "_chkSel").checked = false;
					}
				}
			}
		}
		</SCRIPT>
	</BODY>
</HTML>
