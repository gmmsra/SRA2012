Namespace SRA

Partial Class CobranzasSaldoACta_Pop
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
   Private mstrTablaSaldos As String = SRA_Neg.Constantes.gTab_SaldosACuenta

   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
   Private mbooAplicSaldo As Boolean

   Private Enum Columnas As Integer
      id = 1
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mdsDatos = Session(Request("sess"))

         mbooAplicSaldo = (Not mdsDatos.Tables(mstrTabla).Rows(0).IsNull("comp_modu_id")) AndAlso mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.AplicacionCreditos
         mInicializar()

         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
            mSetearEventos()
            mSetearMaxLength()
            mCargarCombos()

            If Not mbooAplicSaldo Then
               usrClie.Valor = Request("clie_id")
            End If

            mConsultar()

            mAvisarFacturasPendientes()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaSaldos)
      txtImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "sact_impo")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "T")
   End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

        Public Sub mCargarDatos(ByVal pstrId As String)
            With mdsDatos.Tables(mstrTablaSaldos).Select("sact_id=" & pstrId)(0)
                hdnId.Text = .Item("sact_id").ToString()
                usrClie.Valor = .Item("sact_clie_id")
                If (Not .Item("sact_acti_id") Is DBNull.Value) Then
                    cmbActi.Valor = .Item("sact_acti_id")
                End If
                txtImpo.Valor = .Item("sact_impo")
            End With
            mSetearEditor("", False)
        End Sub
        Private Sub mLimpiar()
            hdnId.Text = ""

            cmbActi.Limpiar()
            txtImpo.Text = ""

            mSetearEditor("", True)
        End Sub

        Private Sub mCerrar()
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mActualizar()
      Try
         mGuardarDatos()

         mConsultar()

         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         mdsDatos.Tables(mstrTablaSaldos).Select("sact_id=" & hdnId.Text)(0).Delete()

         mConsultar()

         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Private Sub mGuardarDatos()
            mValidarDatos()

            Dim ldrDatos As DataRow

            If hdnId.Text = "" Then
                If cmbActi.Valor.ToString <> "" Then
                    If mdsDatos.Tables(mstrTablaSaldos).Select("sact_acti_id = " & cmbActi.Valor.ToString & " and sact_clie_id = " & usrClie.Valor).Length > 0 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe otro saldo a cuenta para la actividad indicada.")
                        Return
                    End If
                Else
                    If mdsDatos.Tables(mstrTablaSaldos).Select("sact_acti_id is null and sact_clie_id = " & usrClie.Valor).Length > 0 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe otro saldo a cuenta para la actividad indicada.")
                        Return
                    End If
                End If
                ldrDatos = mdsDatos.Tables(mstrTablaSaldos).NewRow
                ldrDatos.Item("sact_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaSaldos), "sact_id")
            Else
                If cmbActi.Valor.ToString <> "" Then
                    If mdsDatos.Tables(mstrTablaSaldos).Select("sact_id <> " & hdnId.Text & " and sact_acti_id = " & cmbActi.Valor.ToString).Length > 0 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe otro saldo a cuenta para la actividad indicada.")
                        Return
                    End If
                Else
                    If mdsDatos.Tables(mstrTablaSaldos).Select("sact_id <> " & hdnId.Text & " and sact_acti_id is null ").Length > 0 Then
                        Throw New AccesoBD.clsErrNeg("Ya existe otro saldo a cuenta para la actividad indicada.")
                        Return
                    End If
                End If
                ldrDatos = mdsDatos.Tables(mstrTablaSaldos).Select("sact_id=" & hdnId.Text)(0)
            End If

            With ldrDatos
                .Item("sact_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                .Item("sact_clie_id") = usrClie.Valor
                If (cmbActi.Valor.Trim().Length > 0) Then
                    .Item("sact_acti_id") = cmbActi.Valor
                End If
                .Item("sact_impo") = txtImpo.Valor '.Replace(".", ",")
                .Item("sact_cance") = 0

                .Item("_acti_desc") = cmbActi.SelectedItem.Text

                If hdnId.Text = "" Then
                    .Table.Rows.Add(ldrDatos)
                End If
            End With
        End Sub

        Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(Columnas.id).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mActualizar()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mActualizar()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Public Sub mConsultar()
      Try
         mdsDatos.Tables(mstrTablaSaldos).DefaultView.Sort = "sact_id DESC"

         If Not mbooAplicSaldo Then
            mdsDatos.Tables(mstrTablaSaldos).DefaultView.RowFilter = "sact_clie_id =" + usrClie.Valor.ToString
         End If

         grdDato.DataSource = mdsDatos.Tables(mstrTablaSaldos).DefaultView
         grdDato.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub usrClie_Cambio(ByVal sender As Object) Handles usrClie.Cambio
      Try
         mLimpiar()
         mConsultar()
         mAvisarFacturasPendientes()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mAvisarFacturasPendientes()
      Dim lstrCmd As New StringBuilder
      Dim ldsDatos As DataSet

      lstrCmd.Append("exec cobranzas_deuda_consul")
      lstrCmd.Append(" @clie_id=")
      lstrCmd.Append(usrClie.Valor)
      lstrCmd.Append(", @fecha=")
      lstrCmd.Append(clsSQLServer.gFormatArg(Today.ToString("dd/MM/yyyy"), SqlDbType.SmallDateTime))
      lstrCmd.Append(", @tipo=")
      lstrCmd.Append(clsSQLServer.gFormatArg("", SqlDbType.VarChar))
      lstrCmd.Append(", @SoloVencidas=")
      lstrCmd.Append(clsSQLServer.gFormatArg("", SqlDbType.VarChar))
      lstrCmd.Append(", @emct_id=")
      lstrCmd.Append(SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request, True))

      ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      Dim lstrText As New System.Text.StringBuilder

      For Each ldrDatos As DataRow In ldsDatos.Tables(0).Rows
         If lstrText.Length = 0 Then
            lstrText.Append("El cliente tiene facturas pendientes:")
         Else
            lstrText.Append(" - ")
         End If
         lstrText.Append(ldrDatos.Item("numero").ToString)
      Next

      If lstrText.Length > 0 Then
         clsError.gGenerarMensajes(Me, lstrText.ToString)
      End If
   End Sub
End Class
End Namespace
