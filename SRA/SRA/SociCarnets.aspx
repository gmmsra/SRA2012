<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociCarnets" CodeFile="SociCarnets.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CARNETS</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onload="mChequearReporte();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD>
									<P></P>
								</TD>
								<TD height="5">
									<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
								<TD vAlign="top" align="right">&nbsp;
									<asp:ImageButton id="imgClose" runat="server" ImageUrl="imagenes\Close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										CellPadding="1" GridLines="None" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="carn_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="carn_soli_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="F.Pedido"></asp:BoundColumn>
											<asp:BoundColumn DataField="carn_disp_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="F.Recep."></asp:BoundColumn>
											<asp:BoundColumn DataField="carn_entr_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="F.Entrega"></asp:BoundColumn>
											<asp:BoundColumn DataField="carn_obse" HeaderText="Observaciones"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR HEIGHT="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" BorderStyle="Solid" BorderWidth="1px">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="right">
														<asp:Label id="lblSoliFecha" runat="server" cssclass="titulo">F.Pedido:</asp:Label>&nbsp;</TD>
													<TD width="100%" colSpan="2">
														<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
															<TR>
																<TD>
																	<cc1:DateBox id="txtSoliFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox></TD>
																<TD align="right">
																	<asp:Label id="lblDispFecha" runat="server" cssclass="titulo">F.Recepción:</asp:Label></TD>
																<TD>
																	<cc1:DateBox id="txtDispFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																<TD align="right">
																	<asp:Label id="lblEntrFecha" runat="server" cssclass="titulo">F. Entrega:</asp:Label></TD>
																<TD>
																	<cc1:DateBox id="txtEntrFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD vAlign="top" align="right">
														<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;
													</TD>
													<TD colSpan="2">
														<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" TextMode="MultiLine"
															EnterPorTab="False" Height="50px"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD align="center" colSpan="3">
														<A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnRpt" runat="server"></asp:TextBox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtSoliFecha"]!= null)
			document.all["txtSoliFecha"].focus();

		var vVentanas = new Array(null);
			
		function mChequearReporte()
		{
			if (document.all["hdnRpt"].value!= "")
			{
				gAbrirVentanas(document.all["hdnRpt"].value, 15, "600","350","150","200");
				document.all["hdnRpt"].value = "";
			}
		}
		</SCRIPT>
	</BODY>
</HTML>
