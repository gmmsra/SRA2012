' Dario 2013-10-01 Por cambio en procampo
Namespace SRA

Partial Class DebitoAutomatico
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_TarjetasClientes

    Private mstrConn As String
    Public mstrAlumSociId As String
    Private mstrParaPageSize As Integer = 5
    Public mstrOrigen As String
    Private mstrAlum As String
    Private mstrClieId As String
#End Region

#Region "Inicializaci�n de Variables"
    Private Sub mInicializar()
        ' Dario 2013-10-01 para que camine el paginado
        'Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        'grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        grdDato.PageSize = mstrParaPageSize
        mstrAlumSociId = Request.QueryString("id")
        mstrOrigen = Request.QueryString("origen")
        mstrAlum = Request.QueryString("alum")
        mstrClieId = Request.QueryString("clienteId")
        lblTitu.Text = "D�bito autom�tico para " & mstrOrigen

    End Sub

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then

                mSetearEventos()
                mSetearMaxLength()
                ' Dario 2013-10-02 se desabilita el control para que se habilite cuando el combo tiene algun valor
                txtNro.Enabled = False
                mCargarCombos()
                mLimpiar()
                mConsultar()

                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub


    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        ' Dario 2013-10-01 se comenta ya que ahora el maxlength se 
        ' hace segun el dato seleccionado del combo tarjeta
        ' si la tarjeta tiene en true o false el campo tarj_PermiteDebitoEnCuenta
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        'txtNro.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacl_nume")
        ' fin cambio
        Me.txtDocuNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacl_docu_nume")
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipo, "T")
    End Sub
    Public Sub mCargarDatos(ByVal pstrId As String)
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("tacl_id").ToString()
            If Not .IsNull("tacl_vcto_fecha") Then
                txtVctoFecha.Fecha = .Item("tacl_vcto_fecha")
            Else
                txtVctoFecha.Text = ""
            End If

            If Not .IsNull("tacl_vige_fecha") Then
                txtVigeFecha.Fecha = .Item("tacl_vige_fecha")
            Else
                txtVigeFecha.Text = ""
            End If
            txtNro.Valor = .Item("tacl_nume")
            cmbTarj.Valor = .Item("tacl_tarj_id")

            ' Dario 2013-10-01 nuevos campos
            If Not .IsNull("tacl_cuit") Then
                Me.txtCuit.Text = Me.FormatearCUIT(.Item("tacl_cuit"))
            Else
                Me.txtCuit.Text = ""
            End If

            If Not .IsNull("tacl_doti_id") Then
                Me.cmbDocuTipo.Valor = .Item("tacl_doti_id")
            Else
                Me.cmbDocuTipo.Valor = "0"
            End If

            If Not .IsNull("tacl_docu_nume") Then
                Me.txtDocuNume.Valor = .Item("tacl_docu_nume")
            Else
                Me.txtDocuNume.Text = ""
            End If
            ' Dario 2013-10-01 llamo a la funcion que determina si hay que mostrar o ocultar 
            ' los controles de documento
            Me.MostrarOcultarPanelesDocumentos(.Item("tacl_tarj_id"))

        End With

        panDato.Visible = True
        mSetearEditor("", False)
    End Sub
    ' Dario 2013-10-02 Funcion que recupera el CUIT / CUIl con y le pone los guiones
    Private Function FormatearCUIT(ByVal varCUITCUIL As String) As String
        Dim respuesta As String = ""

        If (varCUITCUIL.Length = 11) Then
            respuesta = Mid(varCUITCUIL, 1, 2) & "-" & Mid(varCUITCUIL, 3, 8) & "-" & Mid(varCUITCUIL, 11, 1)
        End If
        Return respuesta
    End Function


    Private Sub mLimpiar()
        hdnId.Text = ""
        txtCuit.Text = ""
        cmbDocuTipo.Limpiar()
        txtDocuNume.Text = ""
        txtVigeFecha.Text = ""
        txtVctoFecha.Text = ""
        txtNro.Text = ""
        cmbTarj.Limpiar()
        mSetearEditor("", True)
        'Dario 2013-10-01 acomodo los paneles de documentos 
        Me.MostrarOcultarPanelesDocumentos(Me.cmbTarj.Valor)
    End Sub

    Private Sub mCerrar()
        Dim lsbMsg As New StringBuilder
        lsbMsg.Append("<SCRIPT language='javascript'>")
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Alta()

            mConsultar()

            mLimpiar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Modi()

            mConsultar()

            mLimpiar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerico.Baja(hdnId.Text)

            mConsultar()

            mLimpiar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        With ldsEsta.Tables(0).Rows(0)
            .Item("tacl_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("tacl_clie_id") = clsSQLServer.gFormatArg(mstrClieId, SqlDbType.Int)
                .Item("tacl_tarj_id") = IIf(cmbTarj.Valor.Trim.Length > 0, cmbTarj.Valor, DBNull.Value)
                .Item("tacl_nume") = IIf(txtNro.Valor.Trim.Length > 0, txtNro.Valor, DBNull.Value)
                .Item("tacl_vcto_fecha") = IIf(txtVctoFecha.Fecha.Trim.Length > 0, txtVctoFecha.Fecha, DBNull.Value)
                .Item("tacl_vige_fecha") = IIf(txtVigeFecha.Fecha.Trim.Length > 0, txtVigeFecha.Fecha, DBNull.Value)
            If mstrAlum = "S" Then
                    .Item("tacl_alum_id") = IIf(mstrAlumSociId.Trim.Length > 0, mstrAlumSociId, DBNull.Value)
            Else
                    .Item("tacl_soci_id") = IIf(mstrAlumSociId.Trim.Length > 0, mstrAlumSociId, DBNull.Value)
            End If

            ' Dario 2013-10-01 nuevos campos
            If (Me.txtCuit.Text = "") Then
                .Item("tacl_cuit") = DBNull.Value
            Else
                .Item("tacl_cuit") = clsSQLServer.gFormatArg(Me.txtCuit.Text.Replace("-", ""), SqlDbType.Int)
            End If

            If (Me.cmbDocuTipo.Valor <> 0) Then
                    .Item("tacl_doti_id") = IIf(cmbDocuTipo.Valor.Trim.Length > 0, cmbDocuTipo.Valor, DBNull.Value)
            Else
                .Item("tacl_doti_id") = DBNull.Value
            End If

            If (Me.txtDocuNume.Text = "") Then
                .Item("tacl_docu_nume") = DBNull.Value
            Else
                .Item("tacl_docu_nume") = clsSQLServer.gFormatArg(Me.txtDocuNume.Text, SqlDbType.Int)
            End If

            For i As Integer = 0 To .Table.Columns.Count - 1
                If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
                    .Item(i) = DBNull.Value
                End If
            Next
        End With

        Return ldsEsta
    End Function

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            mCargarDatos(e.Item.Cells(1).Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "_consul")
            If mstrAlum = "S" Then
                lstrCmd.Append(" @tacl_alum_id=" + mstrAlumSociId)
            Else
                lstrCmd.Append(" @tacl_soci_id=" + mstrAlumSociId)
            End If
            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)
            panDato.Visible = False

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        panDato.Visible = True
        mLimpiar()
    End Sub
    ' Dario 2013-10-01 metodo que se ejecuta con el cambio de valor de combo de tarjeta
    Public Sub cmbTarj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTarj.SelectedIndexChanged
        ' obtengo el id de  la tarjeta
        Dim intIdTarjeta As Integer = Convert.ToInt32(Me.cmbTarj.Valor)
        ' llamo al la funcion que segun el id de la tarjeta aculta o muestra los controles de documento
        Me.MostrarOcultarPanelesDocumentos(intIdTarjeta)
    End Sub

    ' Dario 2013-10-01 metodo que oculta o muestra los controles de documentos segun el id de tarjeta
    Private Sub MostrarOcultarPanelesDocumentos(ByVal intIdTarjeta As Integer)
        ' Dario 2013-10-02 obtengo el valor de la tabla tarjetas
        ' que determina si habilita la carga de cbu
        Dim boolCargaCBU = False
        ' controlo que fue seleccionad una tarjeta, para realizar la consulta para
        ' ver si carga CBU o nro de cuenta
        If (intIdTarjeta > 0) Then
            boolCargaCBU = clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_consul @tarj_id=" & intIdTarjeta.ToString, "tarj_PermiteDebitoEnCuenta")
        End If

        'Dario 2013-10-01 para ocultar o mostrar los datos de documento
        If (boolCargaCBU) Then
            Me.trCUITCUIL.Style.Add("display", "inline")
            Me.trDocu.Style.Add("display", "inline")
            Me.lblNrorunat.Text = "CBU:"
            Me.txtNro.Enabled = True
            Me.txtNro.MaxLength = 22
        ElseIf (intIdTarjeta = 0) Then
            Me.trCUITCUIL.Style.Add("display", "none")
            Me.trDocu.Style.Add("display", "none")
            Me.lblNrorunat.Text = "N�mero:"
            Me.txtNro.Enabled = False
            Me.txtNro.MaxLength = 1
            Me.txtCuit.Text = ""
            Me.txtDocuNume.Text = ""
        Else
            Me.trCUITCUIL.Style.Add("display", "none")
            Me.trDocu.Style.Add("display", "none")
            Me.lblNrorunat.Text = "N�mero:"
            Dim intLength = Me.txtNro.Text.Length
            Me.txtNro.Text = Mid(Me.txtNro.Text, 1, IIf(intLength > 16, 16, intLength))
            Me.txtNro.Enabled = True
            Me.txtNro.MaxLength = 16
            Me.txtCuit.Text = ""
            Me.txtDocuNume.Text = ""
        End If
    End Sub

End Class

End Namespace
