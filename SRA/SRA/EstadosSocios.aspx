<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EstadosSocios" CodeFile="EstadosSocios.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Estados</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<body onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" rightMargin="0" topmargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="TableABM" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" colspan="2" vAlign="bottom" height="25"><asp:Label cssclass="opcion" id="lblTituAbm" runat="server">Estados</asp:Label></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 56px" colspan="2" vAlign="top" height="56">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AllowPaging="True" BorderWidth="1px"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
										ItemStyle-Height="5px" OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
														Height="5">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="esta_id" ReadOnly="True" HeaderText="ID">
												<HeaderStyle Width="2%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="esta_desc" HeaderText="Estado" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="38px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_deve" HeaderText="Devenga">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_requ" HeaderText="Req.Apro.">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_inte" HeaderText="Calc.Inte.">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD valign="top" colspan="2"><a name="editar"></a>
									<DIV align="left">
										<asp:panel cssclass="titulo" id="panDato" runat="server" BorderStyle="Solid" Width="100%" BorderWidth="1px"
											Visible="False">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 152px"></TD>
														<TD>
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="219px"></asp:Label></TD>
														<TD vAlign="top" align="right" width="15">
															<asp:ImageButton id="imgClose" runat="server" CausesValidation="False" ImageUrl="images\Close.bmp"
																ToolTip="Cerrar"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;
														</TD>
														<TD style="HEIGHT: 16px" colSpan="2" height="16">
															<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="352px" MaxLength="80"
																AceptaNull="False" Obligatorio="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
														<TD style="HEIGHT: 32px" colSpan="2" height="32">
															<asp:CheckBox id="chkDeve" CssClass="titulo" Runat="server" Text="Devenga Cuota"></asp:CheckBox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
														<TD style="HEIGHT: 32px" colSpan="2" height="32">
															<asp:CheckBox id="chkRequ" CssClass="titulo" Runat="server" Text="Requiere Aprobaci�n"></asp:CheckBox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
														<TD style="HEIGHT: 32px" colSpan="2" height="32">
															<asp:CheckBox id="chkInte" CssClass="titulo" Runat="server" Text="Calcula Inter�s"></asp:CheckBox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 32px" align="right">
															<asp:Label id="lblRelaEsta" runat="server" cssclass="titulo">Estado Relacionado:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 32px" colSpan="2" height="32">
															<cc1:combobox class="combo" id="cmbRelaEsta" runat="server" Width="352px"></cc1:combobox></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center" colSpan="3"><A id="editar" name="editar"></A>
															<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:button>&nbsp;&nbsp;
														</TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnId" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
		</A>
	</body>
</HTML>
