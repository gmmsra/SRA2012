Imports System.Data.SqlClient


Namespace SRA


Partial Class Firmas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtCriaNumeFil As NixorControls.NumberBox
    Protected WithEvents lblNombFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtNombFil As NixorControls.TextBoxTab
    Protected WithEvents lblCunicaFil As System.Web.UI.WebControls.Label
    Protected WithEvents chkBaja As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cmbEspeciesFil As NixorControls.ComboBox
Protected WithEvents lblProdFil As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_AutorizadosCriadores
   Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
   Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   'valores de los filtros 
   Private mstrClieNume As String
   Private mstrNume As String
   Private mstrNomb As String
   Private mstrCriaNume As String
   Private mstrDocuTipo As String
   Private mstrDocuNume As String
   
   Private Enum Columnas As Integer
      ClieEdit = 0
      CriaId = 1
      CriaClie = 2
      CriaCria = 3
      ClieClieNomb = 4
      ClieNomb = 5
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Dim lstrFil As String
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

            Session(mSess(mstrTabla)) = Nothing
            Session("MilSecc") = Now.Millisecond.ToString
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()

            mMostrarPanel(False)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mSess(mstrTabla))
               Dim x As String = Session.SessionID
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipo, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspeciesFil, "T")


        clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspeciesFil, "S")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()

      Dim lstrAutzLong As Object
      
      lstrAutzLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtApelFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrAutzLong, "autz_nyap")
      txtDocuNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrAutzLong, "autz_docu_nume")
      'txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrAutzLong, "autz_obse")
      'txtDocuNume.MaxLength = txtDocuNumeFil.MaxLength

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      


      usrCria.Tabla = mstrCriadores
      usrCria.Criador = True
      usrCria.AutoPostback = False
      usrCria.FilClaveUnica = True
      usrCria.ColClaveUnica = False
      usrCria.Ancho = 790
      usrCria.Alto = 510
      usrCria.ColDocuNume = False
      usrCria.ColCUIT = True
      usrCria.FilCUIT = True
      usrCria.FilDocuNume = True
      usrCria.FilAgru = False
      usrCria.FilTarjNume = False

      usrCriaFil.Tabla = mstrCriadores
      usrCriaFil.Criador = True
      usrCriaFil.AutoPostback = False
      usrCriaFil.FilClaveUnica = True
      usrCriaFil.ColClaveUnica = False
      usrCriaFil.Ancho = 790
      usrCriaFil.Alto = 510
      usrCriaFil.ColDocuNume = False
      usrCriaFil.ColCUIT = True
      usrCriaFil.FilCUIT = True
      usrCriaFil.FilDocuNume = True
      usrCriaFil.FilAgru = False
      usrCriaFil.FilTarjNume = False

   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mValidarConsulta()

      Try

         If (cmbDocuTipoFil.Valor = 0 And txtDocuNumeFil.Text <> "") _
         Or (cmbDocuTipoFil.Valor <> 0 And txtDocuNumeFil.Text = "") Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el tipo y n�mero de documento.")
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As String

            lstrCmd = "exec " & SRA_Neg.Constantes.gTab_AutorizadosCriadores & "_busq"
         lstrCmd = lstrCmd & " @buscar_en=" + IIf(chkBusc.Checked, "1", "0")
            'lstrCmd = lstrCmd & " ,@clie_id=" + clsSQLServer.gFormatArg(usrClieFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & " ,@cria_id=" + clsSQLServer.gFormatArg(usrCriaFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & " ,@nyap=" + clsSQLServer.gFormatArg(txtApelFil.Text, SqlDbType.VarChar)
         lstrCmd = lstrCmd & " ,@docu_tipo=" + clsSQLServer.gFormatArg(cmbDocuTipoFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & " ,@docu_nume=" + clsSQLServer.gFormatArg(txtDocuNumeFil.Text, SqlDbType.Int)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

         grdDato.Visible = True

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnAlta.Enabled = pbooAlta
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(Columnas.CriaId).Text)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)

    Try

      Dim lstrCarpeta As String

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(pstrId)

      lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString()
      lstrCarpeta += "/" + clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_cria_firma_path") + "/"

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
                    'usrClie.Valor = .Item("autz_clie_id")
            usrCria.Valor = .Item("autz_cria_id")
                        txtApel.Valor = .Item("autz_nyap").ToString
                        txtFechaVige.Fecha = .Item("autz_vige_fecha").ToString

            If Not .IsNull("autz_escria") Then
               chkescria.Checked = clsWeb.gFormatCheck(.Item("autz_escria"), "True")
            Else
               chkescria.Checked = False
            End If
                        cmbDocuTipo.Valor = .Item("autz_doti_id").ToString
                        txtDocuNume.Valor = .Item("autz_docu_nume").ToString
                        txtObse.Valor = .Item("autz_obse").ToString
               If .IsNull("autz_firma_path") Then
                  txtFirma.Valor = ""
                  btnFirmaVer.Visible = False
               Else
                  btnFirmaVer.Visible = True
                  txtFirma.Valor = .Item("autz_firma_path")
                        btnFirmaVer.Attributes.Add("onclick", String.Format("gAbrirVentanas('{0}', 14, '450','450');", lstrCarpeta + hdnId.Text + "_" + mstrTabla + "_" + txtFirma.Text.Substring(txtFirma.Text.LastIndexOf("\") + 1)))
               End If
               lblTitu.Text = "Cliente: " & .Item("_cliente")
            End With

            mSetearEditor("", False)
            mMostrarPanel(True)

         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()

      txtApelFil.Text = ""
      cmbDocuTipoFil.Limpiar()
      txtDocuNumeFil.Text = ""
      chkBusc.Checked = False
        'usrClieFil.Valor = ""
      usrCriaFil.Valor = ""
      grdDato.Visible = False

   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      txtApel.Text = ""
      txtObse.Text = ""
      txtDocuNume.Text = ""
      txtFirma.Text = ""
      txtFechaVige.Text = ""
      btnFirmaVer.Visible = False
      cmbDocuTipo.Limpiar()
        'usrClie.Limpiar()
      usrCria.Limpiar()
      chkEsCria.Checked = False

      mCrearDataSet("")

      lblTitu.Text = ""

      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = Not panDato.Visible

      panDato.Visible = pbooVisi
      panFiltros.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True

   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrAutoId As String

         mGuardarDatos()
  
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()

         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex
        
         Dim lobjGenericaRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
         lobjGenericaRel.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If Not chkEsCria.Checked Then
         If txtApel.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Apellido/Raz�n Social.")
         End If
         If cmbDocuTipo.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Tipo de Documento.")
         End If
         If txtDocuNume.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el N� de Documento.")
         End If
      End If
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer
      Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_clie_cria_firma_path")

      mValidarDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("autz_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            '.Item("autz_clie_id") = usrClie.Valor
         .Item("autz_cria_id") = usrCria.Valor
         If chkEsCria.Checked Then
            .Item("autz_nyap") = DBNull.Value
            .Item("autz_doti_id") = DBNull.Value
            .Item("autz_docu_nume") = DBNull.Value
            .Item("autz_vige_fecha") = DBNull.Value
         Else
            .Item("autz_nyap") = txtApel.Valor
            .Item("autz_doti_id") = cmbDocuTipo.Valor
            .Item("autz_docu_nume") = txtDocuNume.Valor
            .Item("autz_vige_fecha") = txtFechaVige.Fecha
         End If
         .Item("autz_escria") = IIf(chkEsCria.Checked, 1, 0)
         .Item("autz_obse") = txtObse.Valor
         
         If filFirma.Value <> "" Then
            .Item("autz_firma_path") = filFirma.Value
         Else
            .Item("autz_firma_path") = txtFirma.Valor
         End If
         If Not .IsNull("autz_firma_path") Then
            .Item("autz_firma_path") = .Item("autz_firma_path").Substring(.Item("autz_firma_path").LastIndexOf("\") + 1)
         End If
         .Item("_parampath") = lstrCarpeta + "_" + mstrTabla + "_" + Session("MilSecc") + "_" + Replace(.Item("autz_id"), "-", "m") + "__" + .Item("autz_firma_path")

         SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filFirma)
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      Dim lstrOpci As String

      lstrOpci = "@autz_audi_user=" & Session("sUserId").ToString
      mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, lstrOpci)

      mdsDatos.Tables(0).TableName = mstrTabla

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      Session(mSess(mstrTabla)) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mValidarConsulta()
      mConsultar()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

   Protected Overrides Sub Finalize()
      MyBase.Finalize()
   End Sub

End Class
End Namespace
