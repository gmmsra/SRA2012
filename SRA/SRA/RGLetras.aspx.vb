Namespace SRA

Partial Class RGLetras
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
      Protected WithEvents lblImpo As System.Web.UI.WebControls.Label
      

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
      Private mstrTabla As String = "rg_rpletras"
      Private mstrCmd As String
      Private mstrConn As String
      Private mintinsti As Int32

      Private Enum Columnas As Integer
            Id = 1
      End Enum
#End Region

#Region "Operaciones sobre la Pagina"
      Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                  mstrConn = clsWeb.gVerificarConexion(Me)
                  If (Not Page.IsPostBack) Then
                        mSetearEventos()

                        mCargarCombos()
                        mConsultar()

                        clsWeb.gInicializarControles(Me, mstrConn)
                  End If

            Catch ex As Exception
                  clsError.gManejarError(Me, ex)
            End Try
      End Sub
      Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "T")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "S")
            SRA_Neg.Utiles.gSetearRaza(cmbRaza)
            SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
            'clsWeb.gCargarCombo(mstrConn, "rg_rpletras_cargar", cmbPosicion, "id", "rple_luga", "S")
      End Sub
      Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      End Sub
      Private Sub mSetearMaxLength()
            Dim lstrLongs As Object
            Dim lintCol As Integer
            lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

            txtLetra.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "rple_letr")
            
      End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
      Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                  grdDato.EditItemIndex = -1
                  If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                        grdDato.CurrentPageIndex = 0
                  Else
                        grdDato.CurrentPageIndex = E.NewPageIndex
                  End If
                  mConsultar()

            Catch ex As Exception
                  clsError.gManejarError(Me, ex)
            End Try
      End Sub
      Public Sub mConsultar()
            Try
                  mstrCmd = "exec " + mstrTabla + "_consul"
                  mstrCmd += " @rple_raza_id = " + IIf(cmbRazaFil.Valor.ToString = "", "null", cmbRazaFil.Valor.ToString)
                  clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
                  mMostrarPanel(False)
            Catch ex As Exception
                  clsError.gManejarError(Me, ex)
            End Try
      End Sub
#End Region

#Region "Seteo de Controles"
      Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Sub
      Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd += " " + hdnId.Text

            Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
            If ldsDatos.Tables(0).Rows.Count > 0 Then
                  With ldsDatos.Tables(0).Rows(0)
                        cmbRaza.Valor = .Item("rple_raza_id")
                        txtLetra.Valor = .Item("rple_letr")
                        txtFechaDesde.Fecha = .Item("rple_desde_fecha")
                        txtFechaHasta.Fecha = .Item("rple_hasta_fecha")
                        'cmbPosicion.Valor = .Item("rple_luga")
                  End With

                  mSetearEditor(False)
                  mMostrarPanel(True)
            End If
      End Sub
      Private Sub mAgregar()
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
      End Sub
      Private Sub mLimpiar()
            hdnId.Text = ""
            cmbRaza.Limpiar()
            cmbRazafil.Limpiar()
            cmbPosicion.Limpiar()
            txtLetra.Text = ""
            txtFechaDesde.Text = ""
            txtFechaHasta.Text = ""
            mSetearEditor(True)
      End Sub

      Private Sub mCerrar()
            mMostrarPanel(False)
      End Sub
      Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            panDato.Visible = pbooVisi
            btnAgre.Enabled = Not (panDato.Visible)
      End Sub
#End Region

#Region "Opciones de ABM"
      Private Sub mValidarDatos()
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)
      End Sub
      Private Sub mAlta()
            Try
                  Dim ldsDatos As DataSet = mGuardarDatos()
                  Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

                  lobjGenerica.Alta()

                  mConsultar()

                  mMostrarPanel(False)

            Catch ex As Exception
                  clsError.gManejarError(Me, ex)
            End Try
      End Sub
      Private Sub mModi()
            Try
                  clsWeb.gInicializarControles(Me, mstrConn)
                  clsWeb.gValidarControles(Me)

                  Dim ldsDatos As DataSet = mGuardarDatos()
                  Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

                  lobjGenerica.Modi()

                  mConsultar()

                  mMostrarPanel(False)

            Catch ex As Exception
                  clsError.gManejarError(Me, ex)
            End Try
      End Sub
      Private Sub mBaja()
            Try
                  Dim lintPage As Integer = grdDato.CurrentPageIndex

                  Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
                  lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

                  grdDato.CurrentPageIndex = 0

                  mConsultar()

                  If (lintPage < grdDato.PageCount) Then
                        grdDato.CurrentPageIndex = lintPage
                        mConsultar()
                  End If

                  mMostrarPanel(False)

            Catch ex As Exception
                  clsError.gManejarError(Me, ex)
            End Try
      End Sub
      Private Function mGuardarDatos() As DataSet
            Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
            Dim lintCpo As Integer

            mValidarDatos()

            With ldsDatos.Tables(0).Rows(0)
                  .Item("rple_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("rple_letr") = UCase(txtLetra.Valor)
                  .Item("rple_desde_fecha") = txtFechaDesde.Fecha
                  .Item("rple_hasta_fecha") = txtFechaHasta.Fecha
                  .Item("rple_luga") = cmbPosicion.Valor
                  .Item("rple_raza_id") = cmbRaza.Valor
            End With
            Return ldsDatos
      End Function

#End Region

#Region "Eventos de Controles"


      Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
      End Sub

      Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
      End Sub

      Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
      End Sub

      Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
      End Sub

      Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
      End Sub
#End Region

      Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            grdDato.CurrentPageIndex = 0
            mConsultar()
      End Sub
      Private Overloads Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
            grdDato.CurrentPageIndex = 0
            mConsultar()
      End Sub
      Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
            mLimpiarFil()
      End Sub
      Private Sub mLimpiarFil()
            cmbRazaFil.Limpiar()
            mConsultar()
      End Sub
      Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            mAgregar()
      End Sub
      Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
            Try
                  Dim lstrRptName As String = "LetrasRP"
                  Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                  lstrRpt += "&rple_raza_id=" + IIf(cmbRazaFil.Valor.ToString = "", "0", cmbRazaFil.Valor.ToString)

                  lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                  Response.Redirect(lstrRpt)

            Catch ex As Exception
                  clsError.gManejarError(Me, ex)
            End Try
      End Sub

    Private Sub cmbRazaFil_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRazaFil.SelectedIndexChanged

    End Sub
End Class
End Namespace
