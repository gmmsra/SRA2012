<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InterfacePlantelBase" CodeFile="InterfacePlantelBase.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Interface Wingest</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script>
		function mImportar()
		{
			
			var bRet;
			bRet=confirm('Confirma la exportación de datos?');
			
			if(bRet)
			{	
				document.all("divgraba").style.display ="none";
				document.all("divproce").style.display ="inline";
			}
			return(bRet);
			 
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px; HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Interface Plantel Base</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px" width="2"></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="99%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderStyle="Solid" BorderWidth="0px"
															Height="116px">
															<P align="right">
																<TABLE id="Table2" style="WIDTH: 98%; HEIGHT: 106px" cellPadding="0" align="center" border="0">
																	<TR>
																		<TD vAlign="top" colSpan="4">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha Ingreso:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Enabled="False" obligatorio="false"
																				AceptaNull="True" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" colSpan="4">
																			<asp:Label id="lblFechaPresen" runat="server" cssclass="titulo">Fecha Presentación:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaPresen" runat="server" cssclass="cuadrotexto" AceptaNull="True" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" colSpan="4">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Height="19px" Width="280px"
																				filtra="true" NomOper="razas_cargar" MostrarBotones="False" AutoPostBack="False"></cc1:combobox></TD>
																	</TR>																	
																	<TR>
																		<TD vAlign="top" colSpan="4">
																			<asp:Label id="lblAsociacion" runat="server" cssclass="titulo">Asociación:</asp:Label>&nbsp;
																			<cc1:combobox class="combo" id="cmbAsoc" runat="server" cssclass="cuadrotexto" Height="19px" Width="280px"
																				filtra="true" NomOper="asociaciones_cargar" MostrarBotones="False" AutoPostBack="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 56px" colSpan="3">
																			<asp:Label id="lblArch" runat="server" cssclass="titulo">Archivo: </asp:Label><INPUT class="fileboton" id="filArch" style="WIDTH: 490px; HEIGHT: 22px" type="file" size="72"
																				name="filArch" runat="server"></TD>
																		<TD align="left">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo: </asp:Label><BR>
																			<cc1:combobox class="combo" id="cmbTipo" runat="server" AceptaNull="False" Width="80px">
																				<asp:ListItem Value="XLS" Selected="True">MS Excel</asp:ListItem>
																				<asp:ListItem Value="DBF">dBase IV</asp:ListItem>
																			</cc1:combobox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" colSpan="4">
																			<asp:Label id="lblErr" runat="server" cssclass="titulo">Resultados: </asp:Label><BR>
																			<asp:ListBox id="lisResu" runat="server" cssclass="listaerrores" Height="167px" Width="99%"></asp:ListBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="right" colSpan="4" height="28">
																			<DIV id="divgraba" style="DISPLAY: inline">
																				<CC1:BotonImagen id="btnImpo" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																					BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False"
																					OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnGrab.gif" ImageOver="btnGrab2.gif" ImageDisable="btnGrab0.gif"
																					ToolTip="Exportar Datos"></CC1:BotonImagen>
																			</DIV>
																			<DIV id="divproce" style="DISPLAY: none">
																				<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>
																				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																			</DIV>
																			</asp:Button>
																		</TD>
																	</TR>
																</TABLE>
															</P>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD colSpan="3"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO ----------------->
			<BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="txtIdCabe" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Interface Wingest');
		if (document.frmABM.lblMens.value != '') {
		   alert(document.frmABM.lblMens.value);
		   document.frmABM.lblMens.value = '';
		}
		</SCRIPT>
	</BODY>
</HTML>
