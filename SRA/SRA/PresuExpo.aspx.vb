Imports System.IO
Imports System.Text


Namespace SRA


Partial Class PresuExpo
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            mCargarCombos()
            mInicializar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "meses", cmbMesD, "")
   End Sub

   Private Sub mInicializar()
      btnGrab.Attributes.Add("onclick", "return(mProcesar());")
      cmbMesD.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "parametros_consul", "_para_inte_presu_expo_mes")
      txtAnioD.Text = clsSQLServer.gCampoValorConsul(mstrConn, "parametros_consul", "_para_inte_presu_expo_anio")
   End Sub

   Private Sub mLimpiarFiltros()
      cmbMesD.Limpiar()
      txtAnioD.Text = ""
   End Sub

#End Region

#Region "Consultas"

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
    End Sub

    Private Sub mExportarDBF(ByVal pstrArch As String, ByVal ds As DataSet)
        Try
            clsExporta.clsGrabarArchivo.FormatoDBF(ds.Tables(0), pstrArch)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnGrab_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGrab.Click
        Dim lstrArch As String
        Dim lstrNomb As String

        Try

            If cmbMesD.Valor = "" Or txtAnioD.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el mes y a�o.")
            End If

            mstrCmd = "exec expo_control_presupuestario_consul "
            mstrCmd += " @mes_desde=" + cmbMesD.Valor
            mstrCmd += ",@anio_desde=" + txtAnioD.Text
            mstrCmd += ",@mes_hasta=" + cmbMesD.Valor
            mstrCmd += ",@anio_hasta=" + txtAnioD.Text

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            lstrNomb = "ig" & txtAnioD.Text & IIf(cmbMesD.Valor < 10, "0", "") & cmbMesD.Valor.ToString & ".dbf"
            lstrArch = System.Configuration.ConfigurationSettings.AppSettings("conPathExpoPresu")
            lstrArch = HttpContext.Current.Server.MapPath(lstrArch)
            lstrArch = lstrArch.Replace("archivo.ext", lstrNomb)

            mExportarDBF(lstrArch, ds)

            hdnExpo.Text = lstrNomb

            Dim lstrMes As String = (CInt(cmbMesD.Valor) + 1).ToString
            Dim lstrAnio As String = txtAnioD.Text

            If lstrMes = "13" Then
                lstrMes = "1"
                lstrAnio = (CInt(lstrAnio) + 1).ToString
            End If

            mstrCmd = "parametros_inte_presu_modi "
            mstrCmd = mstrCmd & " @para_inte_presu_expo_mes=" & lstrMes
            mstrCmd = mstrCmd & ",@para_inte_presu_expo_anio=" & lstrAnio
            clsSQLServer.gExecute(mstrConn, mstrCmd)

            cmbMesD.Valor = lstrMes
            txtAnioD.Text = lstrAnio

            Throw New AccesoBD.clsErrNeg("La exportaci�n ha finalizado correctamente.")
            'lblMens.Text = "La exportaci�n ha finalizado correctamente."

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

#End Region

End Class
End Namespace
