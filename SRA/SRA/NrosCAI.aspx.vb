Imports SRA_Neg


Namespace SRA

Partial Class NrosCAI
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnList As NixorControls.BotonImagen
    Protected WithEvents lblFDesdeFil As System.Web.UI.WebControls.Label


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "nros_cai"
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
    Private mintinsti As Int32

    Private Enum Columnas As Integer
        Id = 1
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()

                mCargarCombos()
                mConsultar()

                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "EMISORES_CTROS", cmbCtroEmisor, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "EMISORES_CTROS", cmbCtroEmisorfil, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "COMPROBAN_TIPOS", cmbTipoComp, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "COMPROBAN_TIPOS", cmbTipoCompFil, "S")
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        Dim lintCol As Integer

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)


    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()

        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        mintinsti = Convert.ToInt32(Request.QueryString("fkvalor"))
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "_consul")
            lstrCmd.Append(" @emct_id=" + IIf(cmbCtroEmisor.Valor.ToString = "", "0", cmbCtroEmisor.Valor.ToString))
            lstrCmd.Append(", @nrca_coti_id=" + cmbTipoCompFil.Valor.ToString)
            If cmbTipoCompFil.Valor.ToString <> 30 Then
                lstrCmd.Append(", @nrca_letra=" + cmbLetraFil.SelectedItem.Value.ToString)
            End If
            lstrCmd.Append(", @nrca_fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFDesdeFil.Fecha))
            lstrCmd.Append(", @nrca_fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFHastaFil.Fecha))
            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
        mstrCmd = "exec " + mstrTabla + "_consul"
        mstrCmd += " " + hdnId.Text

        Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
        If ldsDatos.Tables(0).Rows.Count > 0 Then
            With ldsDatos.Tables(0).Rows(0)
                cmbCtroEmisorfil.Valor = .Item("emct_id")
                cmbTipoComp.Valor = .Item("nrca_coti_id")
                txtFechaVto.Fecha = .Item("nrca_vto_fecha")
                cmbLetra.Valor = .Item("nrca_letra")
                txtNumero.Valor = .Item("nrca_nume")
            End With
            mSetearEditor(False)
            mMostrarPanel(True)
        End If
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        cmbCtroEmisorfil.Limpiar()
        cmbTipoComp.Limpiar()
        txtFechaVto.Text = ""
        cmbLetra.Limpiar()
        txtNumero.Text = ""

        mSetearEditor(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
    Private Sub mAlta()
        Dim lTransac As SqlClient.SqlTransaction
        Try
            mValidarDatos()
            Dim mstrCmd As String
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            mstrCmd = "exec " + mstrTabla + "_alta"
            mstrCmd = mstrCmd + " " + cmbCtroEmisorfil.Valor.ToString
            mstrCmd = mstrCmd + ", " + cmbTipoComp.Valor.ToString
            mstrCmd = mstrCmd + ",'" + cmbLetra.Valor.ToString

            mstrCmd = mstrCmd + "'," + clsSQLServer.gFormatArg(txtFechaVto.Fecha, SqlDbType.SmallDateTime)
            mstrCmd = mstrCmd + ",'" + txtNumero.Valor.ToString
            mstrCmd = mstrCmd + "'," + Session("sUserId").ToString()

            clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)
            mMostrarPanel(False)
            mConsultar()
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Dim lTransac As SqlClient.SqlTransaction
        Try
            mValidarDatos()
            Dim mstrCmd As String
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            mstrCmd = "exec " + mstrTabla + "_modi"
            mstrCmd = mstrCmd + " " + hdnId.Text
            mstrCmd = mstrCmd + "," + cmbCtroEmisorfil.Valor.ToString
            mstrCmd = mstrCmd + "," + cmbTipoComp.Valor.ToString
            mstrCmd = mstrCmd + ",'" + cmbLetra.Valor.ToString
            mstrCmd = mstrCmd + "'," + clsFormatear.gFormatFecha2DB(txtFechaVto.Fecha)
            mstrCmd = mstrCmd + ",'" + txtNumero.Valor.ToString
            mstrCmd = mstrCmd + "'," + Session("sUserId").ToString()

            clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)
            mMostrarPanel(False)
            mConsultar()
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
#End Region
    Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Overloads Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    Private Sub mLimpiarFil()
        cmbCtroEmisor.Limpiar()
        cmbTipoCompFil.Limpiar()
        cmbLetraFil.Limpiar()
        txtFDesdeFil.Text = ""
        txtFHastaFil.Text = ""

        mConsultar()
    End Sub
    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            Dim lstrRptName As String = "NrosCAI"
            '*****************************************************************************
            ' Pantanettig  -- 16/10/2007
            Dim lintFechaD As Integer
            Dim lintFechaH As Integer

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtFDesdeFil.Text = "" Then
                lintFechaD = 0
            Else
                lintFechaD = CType(clsFormatear.gFormatFechaString(txtFDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFHastaFil.Text = "" Then
                lintFechaH = 0
            Else
                lintFechaH = CType(clsFormatear.gFormatFechaString(txtFHastaFil.Text, "Int32"), Integer)
            End If

            lstrRpt += "&emct_id=" + IIf(cmbCtroEmisor.Valor.ToString = "", "0", cmbCtroEmisor.Valor.ToString)
            lstrRpt += "&nrca_fecha_desde=" + lintFechaD.ToString
            lstrRpt += "&nrca_fecha_hasta=" + lintFechaH.ToString
            lstrRpt += "&nrca_coti_id=" + cmbTipoCompFil.Valor.ToString

            ' Dario 2013-11-21 Para corregir el problema que pasa al SP una letra cuando es recibo
            If (cmbTipoCompFil.Valor <> SRA_Neg.Constantes.ComprobTipos.Recibo) Then
                lstrRpt += "&nrca_letra=" + cmbLetraFil.SelectedItem.Value.ToString
            Else
                lstrRpt += "&nrca_letra=S"
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

            '****************************************************************************


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
