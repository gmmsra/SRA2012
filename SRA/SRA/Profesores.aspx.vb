Imports System.Data.SqlClient


Namespace SRA


Partial Class Profesores
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents Label2 As System.Web.UI.WebControls.Label
   Protected WithEvents Label3 As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "Profesores"
   Private mstrProfMail As String = "mails_profe"
   Private mstrProfTele As String = "tele_profe"
   Private mstrProfDire As String = "dire_profe"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String
   Private mintinsti As Int32

   Private Enum Columnas As Integer
      ProfeId = 1
      ProfeNombyApe = 2

   End Enum

   Private Enum ColumnasDeta As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()


         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar(grdDato, False)

            tabLinks.Visible = False
            clsWeb.gInicializarControles(Me, mstrConn)

            

         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If



      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()

      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "S")
      If Not Request.Form("cmbPais") Is Nothing Then
         cmbPais.Valor = Request.Form("cmbPais")
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbPais.Valor, cmbProv, "id", "descrip", "S", True)
      End If
   

   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaMail.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Profesores, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrProfMailLong As Object
      Dim lstrProfesoresLong As Object

      lstrProfesoresLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtNombApe.MaxLength = clsSQLServer.gObtenerLongitud(lstrProfesoresLong, "prof_desc")

      lstrProfMailLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrProfMail)
      txtMail.MaxLength = clsSQLServer.gObtenerLongitud(lstrProfMailLong, "mapr_mail")
      txtMailRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrProfMailLong, "mapr_refe")

   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintinsti = Convert.ToInt32(Request.QueryString("fkvalor"))
   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(grdDato, True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub



#End Region

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
        
         Case mstrProfMail
            btnBajaMail.Enabled = Not (pbooAlta)
            btnModiMail.Enabled = Not (pbooAlta)
            btnAltaMail.Enabled = pbooAlta

         Case mstrProfTele
            btnBajaTele.Enabled = Not (pbooAlta)
            btnModiTele.Enabled = Not (pbooAlta)
            btnAltaTele.Enabled = pbooAlta

         Case mstrProfDire
            btnBajaDire.Enabled = Not (pbooAlta)
            btnModiDire.Enabled = Not (pbooAlta)
            btnAltaDire.Enabled = pbooAlta

         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub




   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.ProfeId).Text)

      mCrearDataSet(hdnId.Text)

      grdMail.CurrentPageIndex = 0

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtNombApe.Valor = .Item("prof_desc")
         End With

         mSetearEditor("", False)
         mSetearEditor(mstrProfMail, True)
         mMostrarPanel(True)
         mShowTabs(1)

      End If
   End Sub

   Public Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Dim ldrTele As DataRow

      hdnTeleId.Text = E.Item.Cells(1).Text
      ldrTele = mdsDatos.Tables(mstrProfTele).Select("tepr_id=" & hdnTeleId.Text)(0)

      With ldrTele
         txtCodi.Valor = .Item("tepr_area_code")
         txtTele.Valor = .Item("tepr_tele")
         txtRefeTele.Valor = .Item("tepr_refe")
         cmbTeti.Valor = .Item("tepr_teti_id")
      End With
      mSetearEditor(mstrProfTele, False)
   End Sub

   Public Sub mEditarDatosDire(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Dim ldrDire As DataRow
      Me.mLimpiarDireccion()

      hdnDireId.Text = E.Item.Cells(1).Text
      ldrDire = mdsDatos.Tables(mstrProfDire).Select("dipr_id=" & hdnDireId.Text)(0)

      With ldrDire
         txtRefeDire.Valor = .Item("dipr_refe")
         cmbPais.Valor = .Item("_pais_id")
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbPais.Valor, cmbProv, "id", "descrip", "S", True)
         cmbProv.Valor = .Item("_prov_id")
         clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbProv.Valor, cmbLoca, "id", "descrip", "S", True)
         clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbProv.Valor, cmbLocaAux, "id", "descrip", "S", True)
         cmbLoca.Valor = .Item("dipr_loca_id")
         txtCodPostal.Valor = .Item("dipr_cpos")
         txtCodPostal.ValidarPaisDefault = IIf(cmbPais.Valor = Session("sPaisDefaId"), "S", "N")
       End With

      mSetearEditor(mstrProfDire, False)

   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      txtNombApe.Text = ""


      mLimpiarMail()

      mCrearDataSet("")

      lblTitu.Text = ""

      grdMail.CurrentPageIndex = 0
      mShowTabs(1)
      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      PanBotones.Visible = pbooVisi
      PanBoto.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)

      lnkCabecera.Font.Bold = True


      panDato.Visible = pbooVisi
      'grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True


      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True

   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panMail.Visible = False
      PanTele.Visible = False
      PanDire.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkMail.Font.Bold = False
      lnkTele.Font.Bold = False
      lnkDire.Font.Bold = False

      Select Case origen
         Case 1
            'Datos profesor
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
         Case 2
            'Mail
            panMail.Visible = True
            lnkMail.Font.Bold = True
         Case 3
            'Tel�fono
            clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTeti, "S")
            PanTele.Visible = True
            lnkTele.Font.Bold = True
         Case 4
            'Direcci�n
            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "S")
            PanDire.Visible = True
            lnkDire.Font.Bold = True
      End Select
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

   Private Sub lnkMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMail.Click
      mShowTabs(2)
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Alta()
         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Modi()

         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar(grdDato, True)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      If txtNombApe.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre y Apellido.")
      End If

      With mdsDatos.Tables(0).Rows(0)
         .Item("prof_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("prof_desc") = txtNombApe.Valor
         .Item("prof_inse_id") = mintinsti
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrProfMail
      mdsDatos.Tables(2).TableName = mstrProfTele
      mdsDatos.Tables(3).TableName = mstrProfDire

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      grdMail.DataSource = mdsDatos.Tables(mstrProfMail)
      grdMail.DataBind()

      grdTele.DataSource = mdsDatos.Tables(mstrProfTele)
      grdTele.DataBind()

      grdDire.DataSource = mdsDatos.Tables(mstrProfDire)
      grdDire.DataBind()

      Session(mstrTabla) = mdsDatos

   End Sub


#End Region

#Region "Eventos de Controles"
   
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnAltaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaMail.Click
      mActualizarMail()
   End Sub

   Private Sub btnBajaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaMail.Click
      Try
         mdsDatos.Tables(mstrProfMail).Select("mapr_id=" & hdnMailId.Text)(0).Delete()
         grdMail.DataSource = mdsDatos.Tables(mstrProfMail)
         grdMail.DataBind()
         mLimpiarMail()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpMail.Click
      mLimpiarMail()
   End Sub
   Private Sub btnModiMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiMail.Click
      mActualizarMail()
   End Sub
#End Region

#Region "Detalle"
   Public Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrMail As DataRow

         hdnMailId.Text = E.Item.Cells(1).Text
         ldrMail = mdsDatos.Tables(mstrProfMail).Select("mapr_id=" & hdnMailId.Text)(0)

         With ldrMail
            txtMail.Valor = .Item("mapr_mail")
            txtMailRefe.Valor = .Item("mapr_refe")
         End With

         mSetearEditor(mstrProfMail, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdMail_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdMail.EditItemIndex = -1
         If (grdMail.CurrentPageIndex < 0 Or grdMail.CurrentPageIndex >= grdMail.PageCount) Then
            grdMail.CurrentPageIndex = 0
         Else
            grdMail.CurrentPageIndex = E.NewPageIndex
         End If
         grdMail.DataSource = mdsDatos.Tables(mstrProfMail)
         grdMail.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdTele_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdTele.EditItemIndex = -1
         If (grdTele.CurrentPageIndex < 0 Or grdTele.CurrentPageIndex >= grdTele.PageCount) Then
            grdTele.CurrentPageIndex = 0
         Else
            grdTele.CurrentPageIndex = E.NewPageIndex
         End If
         grdTele.DataSource = mdsDatos.Tables(mstrProfTele)
         grdTele.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdDire_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDire.EditItemIndex = -1
         If (grdDire.CurrentPageIndex < 0 Or grdDire.CurrentPageIndex >= grdDire.PageCount) Then
            grdDire.CurrentPageIndex = 0
         Else
            grdDire.CurrentPageIndex = E.NewPageIndex
         End If
         grdDire.DataSource = mdsDatos.Tables(mstrProfDire)
         grdDire.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal pOk As Boolean)
      Try
         Dim strFin As String = ""
         If Not pOk Then
            strFin = ",@ejecuta = N "
         End If
         mstrCmd = "exec " + mstrTabla + "_consul " + "@prof_inse_id = " + mintinsti.ToString() + strFin
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub





   Private Sub mActualizarMail()
      Try
         mGuardarDatosMail()

         mLimpiarMail()
         grdMail.DataSource = mdsDatos.Tables(mstrProfMail)
         grdMail.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarTelefono()
      Try
         mGuardarDatosTelefono()

         mLimpiarTelefono()
         grdTele.DataSource = mdsDatos.Tables(mstrProfTele)
         grdTele.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarDireccion()
      Try
         mGuardarDatosDireccion()

         mLimpiarDireccion()
         grdDire.DataSource = mdsDatos.Tables(mstrProfDire)
         grdDire.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mGuardarDatosMail()

      Dim ldrDatos As DataRow

      If txtMail.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la direcci�n de mail.")
      End If

      If txtMailRefe.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar una referencia.")
      End If

      If hdnMailId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrProfMail).NewRow
         ldrDatos.Item("mapr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrProfMail), "mapr_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrProfMail).Select("mapr_id=" & hdnMailId.Text)(0)
      End If

      With ldrDatos
         If hdnId.Text <> "" Then
            '.Item("mapr_clie_id") = hdnId.Text
         End If
         .Item("mapr_mail") = txtMail.Valor
         .Item("mapr_refe") = txtMailRefe.Valor
      End With

      If hdnMailId.Text = "" Then
         mdsDatos.Tables(mstrProfMail).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosTelefono()

      Dim ldrDatos As DataRow

      If txtCodi.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el c�digo.")
      End If

      If txtTele.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el tel�fono.")
      End If

      If txtRefeTele.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar una referencia.")
      End If

      If cmbTeti.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe selecionar el tipo de tel�fono.")
      End If

      If hdnTeleId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrProfTele).NewRow
         ldrDatos.Item("tepr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrProfTele), "tepr_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrProfTele).Select("tepr_id=" & hdnTeleId.Text)(0)
      End If

      With ldrDatos
         If hdnId.Text <> "" Then
            '.Item("mapr_clie_id") = hdnId.Text
         End If
         .Item("tepr_area_code") = txtCodi.Valor
         .Item("tepr_tele") = txtTele.Valor
         .Item("tepr_refe") = txtRefeTele.Valor
         .Item("tepr_teti_id") = cmbTeti.Valor
         .Item("_telefono") = .Item("tepr_area_code") & " " & .Item("tepr_tele")
      End With

      If hdnTeleId.Text = "" Then
         mdsDatos.Tables(mstrProfTele).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosDireccion()

      Dim ldrDatos As DataRow

      If cmbLoca.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe selecionar la localidad.")
      End If

      If txtDire.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la Direcci�n.")
      End If

      If txtRefeDire.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar una referencia.")
      End If

      If hdnDireId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrProfDire).NewRow
         ldrDatos.Item("dipr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrProfDire), "dipr_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrProfDire).Select("dipr_id=" & hdnDireId.Text)(0)
      End If

      With ldrDatos
         If hdnId.Text <> "" Then
            '.Item("mapr_clie_id") = hdnId.Text
         End If
         .Item("dipr_refe") = txtRefeDire.Valor
         .Item("dipr_loca_id") = cmbLoca.Valor
         .Item("dipr_dire") = txtDire.Valor
         .Item("dipr_cpos") = txtCodPostal.Valor
         .Item("_prov_id") = cmbProv.Valor
         .Item("_pais_id") = cmbPais.Valor
      End With

      If hdnDireId.Text = "" Then
         mdsDatos.Tables(mstrProfDire).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mLimpiarMail()
      hdnMailId.Text = ""
      txtMail.Text = ""
      txtMailRefe.Text = ""
      mSetearEditor(mstrProfMail, True)
   End Sub

   Private Sub mLimpiarTelefono()
      txtCodi.Text = ""
      txtTele.Text = ""
      txtRefeTele.Text = ""
      cmbTeti.Limpiar()
      hdnTeleId.Text = ""
      mSetearEditor(mstrProfTele, True)
   End Sub

   Private Sub mLimpiarDireccion()
      txtRefeDire.Text = ""
      cmbPais.Limpiar()
      cmbProv.Items.Clear()
      cmbLoca.Items.Clear()
      cmbLocaAux.Items.Clear()
      txtDire.Text = ""
      txtCodPostal.Text = ""
      hdnDireId.Text = ""
      mSetearEditor(mstrProfDire, True)
   End Sub

   Private Sub mSetearEditorMail(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub


#End Region


   Private Sub hdnPosic_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnPosic.TextChanged
      Try
         mGuardarDatosMail()

         mLimpiarMail()


         hdnPosic.Text = ""

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "Profesores"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&fkvalor=" + Request.QueryString("fkvalor")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnAltaTele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaTele.Click
      mActualizarTelefono()
   End Sub

   Private Sub btnModiTele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiTele.Click
      mActualizarTelefono()
   End Sub

   Private Sub btnAltaDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDire.Click
      mActualizarDireccion()
   End Sub

   Private Sub btnModiDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDire.Click
      mActualizarDireccion()
   End Sub

   Private Sub btnBajaTele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaTele.Click
      Try
         mdsDatos.Tables(mstrProfTele).Select("tepr_id=" & hdnTeleId.Text)(0).Delete()
         grdTele.DataSource = mdsDatos.Tables(mstrProfTele)
         grdTele.DataBind()
         mLimpiarTelefono()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDire.Click
      Try
         mdsDatos.Tables(mstrProfDire).Select("dipr_id=" & hdnDireId.Text)(0).Delete()
         grdDire.DataSource = mdsDatos.Tables(mstrProfDire)
         grdDire.DataBind()
         mLimpiarDireccion()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpTele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpTele.Click
      mLimpiarTelefono()
   End Sub

   Private Sub btnLimpDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDire.Click
      mLimpiarDireccion()
   End Sub

   Private Sub lnkTele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkTele.Click
      mShowTabs(3)
   End Sub

   Private Sub lnkDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDire.Click
      mShowTabs(4)
   End Sub
End Class
End Namespace
