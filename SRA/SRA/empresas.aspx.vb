Imports SRA
Imports System.IO
Imports System.Data.SqlClient

Public Class empresas
   Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
    '  Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    '  Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
    '  Protected WithEvents btnAgre As System.Web.UI.WebControls.Button
    '  Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
    '  Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
    '  Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    '  Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    '  Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton
    '  Protected WithEvents lblDesc As System.Web.UI.WebControls.Label
    '  Protected WithEvents txtDesc As NixorControls.TextBoxTab
    '  Protected WithEvents lblTele As System.Web.UI.WebControls.Label
    '  Protected WithEvents txtTele As NixorControls.TextBoxTab
    '  Protected WithEvents lblCUIT As System.Web.UI.WebControls.Label
    '  Protected WithEvents txtCUIT As NixorControls.CUITBox
    '  Protected WithEvents panCabecera As System.Web.UI.WebControls.Panel
    '  Protected WithEvents lblFactu As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbFactu As NixorControls.ComboBox
    '  Protected WithEvents lblPilo As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbPilo As NixorControls.ComboBox
    '  Protected WithEvents lblDespa As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbDespa As NixorControls.ComboBox
    '  Protected WithEvents lblPresu As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbPresu As NixorControls.ComboBox
    '  Protected WithEvents panDetalle As System.Web.UI.WebControls.Panel
    '  Protected WithEvents btnAlta As System.Web.UI.WebControls.Button
    '  Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
    '  Protected WithEvents btnModi As System.Web.UI.WebControls.Button
    '  Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    '  Protected WithEvents btnLimp As System.Web.UI.WebControls.Button
    '  Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    '  Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    '  Protected WithEvents hdnId As System.Web.UI.WebControls.TextBox
    '  Protected WithEvents lblFile As System.Web.UI.WebControls.Label
    '  Protected WithEvents btnSubir As System.Web.UI.WebControls.Button
    '  Protected WithEvents lblImagen As System.Web.UI.WebControls.Label
    '  Protected WithEvents imgImagen As System.Web.UI.WebControls.Image
    '  Protected WithEvents lblLogo As System.Web.UI.WebControls.Label
    '  Protected WithEvents txtLogo As NixorControls.TextBoxTab
    '  Protected WithEvents txtFile As System.Web.UI.HtmlControls.HtmlInputFile
    '  Protected WithEvents lblImgPrin As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbImgPrin As NixorControls.ComboBox
    'Protected WithEvents lblSucu As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDivi As System.Web.UI.WebControls.Label
    'Protected WithEvents txtSucu As NixorControls.NumberBox
    'Protected WithEvents txtDivi As NixorControls.NumberBox

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.
   Private designerPlaceholderDeclaration As System.Object

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "empresas"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String

   Private Enum Columnas As Integer
      Id = 1
      desc = 2
      factu = 3
      pilo = 4
      despa = 5
      presu = 6
   End Enum

   Private Enum ColumnasDeta As Integer
      Id = 2
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()

            clsWeb.gCargarComboBool(cmbDespa, "")
            clsWeb.gCargarComboBool(cmbFactu, "")
            clsWeb.gCargarComboBool(cmbPilo, "")
            clsWeb.gCargarComboBool(cmbPresu, "")
            clsWeb.gCargarComboBool(cmbImgPrin, "")
            cmbImgPrin.ValorBool = False

            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnReset.Attributes.Add("onclick", "if(!confirm('Confirma la renumeraci�n?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Empresas, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If

      lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Empresas_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      btnAlta.Visible = lbooPermiAlta

      btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Empresas_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Empresas_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = lbooPermiModi
      btnReset.Visible = lbooPermiModi

      lnkDeta.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Empresas_Alcance, String), (mstrConn), (Session("sUserId").ToString()))

      btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "empr_desc")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnReset.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim ldsDatos As DataSet

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
      ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      If ldsDatos.Tables(0).Rows.Count > 0 Then
         With ldsDatos.Tables(0).Rows(0)
            txtDesc.Valor = .Item("empr_desc")
            txtCUIT.TextoPlano = .Item("empr_cuit")
            txtTele.Valor = .Item("empr_tele")
			txtSucu.Valor = .Item("empr_sucursal")
			txtDivi.Valor = .Item("empr_division")
            txtLogo.Text = .Item("empr_logo").ToString
            cmbFactu.ValorBool = .Item("empr_factu")
            cmbPilo.ValorBool = .Item("empr_pilo")
            cmbDespa.ValorBool = .Item("empr_despa")
            cmbPresu.ValorBool = .Item("empr_presu")
            cmbImgPrin.ValorBool = .Item("empr_img_prin")

            mCargarImagen(.Item("empr_logo").ToString)
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnReset.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtDesc.Text = ""
      txtDesc.Text = ""
      txtCUIT.Text = ""
  	  txtSucu.Valor = ""
	  txtDivi.Valor = ""
      txtTele.Text = ""
      txtLogo.Text = ""
      cmbFactu.Limpiar()
      cmbPilo.Limpiar()
      cmbDespa.Limpiar()
      cmbPresu.Limpiar()
      cmbImgPrin.ValorBool = False

      mCargarImagen("")

      mSetearEditor(True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)

      lnkCabecera.Font.Bold = True
      lnkDeta.Font.Bold = False

      panDato.Visible = pbooVisi
      'grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True
      panDetalle.Visible = False

      lnkCabecera.Visible = pbooVisi
      lnkDeta.Visible = pbooVisi And clsSQLServer.gMenuPermi(CType(Opciones.Empresas_Alcance, String), (mstrConn), (Session("sUserId").ToString()))
      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkDeta.Font.Bold = False
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panDetalle.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkDeta.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
         Case 2
            panDetalle.Visible = True
            lnkDeta.Font.Bold = True
      End Select
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

   Private Sub lnkDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
      mShowTabs(2)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         clsWeb.gInicializarControles(Me, mstrConn)
         clsWeb.gValidarControles(Me)

         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Alta()

         mGuardarImagen(ldsDatos)

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         clsWeb.gInicializarControles(Me, mstrConn)
         clsWeb.gValidarControles(Me)

         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Modi()

         mGuardarImagen(ldsDatos)

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mReset()
      Try
         Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
         ldsDatos.Tables(0).Rows(0).Item("empr_ult_num") = 0

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsDatos.Tables(0).Rows(0)
         .Item("empr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("empr_desc") = txtDesc.Valor
         .Item("empr_cuit") = txtCUIT.TextoPlano
         .Item("empr_sucursal") = txtSucu.Valor
         .Item("empr_division") = txtDivi.Valor
         .Item("empr_tele") = txtTele.Valor
         .Item("empr_factu") = cmbFactu.ValorBool
         .Item("empr_pilo") = cmbPilo.ValorBool
         .Item("empr_despa") = cmbDespa.ValorBool
         .Item("empr_presu") = cmbPresu.ValorBool
         .Item("empr_img_prin") = cmbImgPrin.ValorBool
         .Item("empr_logo") = txtLogo.Text
      End With

      Return (ldsDatos)
   End Function

   Private Sub mGuardarImagen(ByVal pdsDatos As DataSet)
      Dim imagen() As Byte = mObtenerImagen(pdsDatos.Tables(0).Rows(0).Item("empr_logo"))

      Dim myConnection As New SqlConnection(mstrConn)
      Dim cmdExecCommand As New SqlCommand
      cmdExecCommand.Connection = myConnection
      cmdExecCommand.CommandText = "EXEC empresas_img_modi @empr_id, @empr_imagen"

      cmdExecCommand.Parameters.Add("@empr_id", SqlDbType.Int).Value = pdsDatos.Tables(0).Rows(0).Item("empr_id")
      If imagen Is Nothing Then
         cmdExecCommand.Parameters.Add("@empr_imagen", SqlDbType.Image, 0).Value = DBNull.Value
      Else
         cmdExecCommand.Parameters.Add("@empr_imagen", SqlDbType.Image, imagen.Length).Value = imagen
      End If


      myConnection.Open()

      cmdExecCommand.ExecuteNonQuery()

      myConnection.Close()
   End Sub

   Private Function mObtenerImagen(ByVal lstrNombre As String) As Byte()
      Dim lstrCarpeta As String
      If lstrNombre <> "" Then
         lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("logoImagenes").ToString()

         lstrNombre = Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre

         Dim fs As FileStream = New FileStream(lstrNombre, FileMode.Open, FileAccess.Read)
         Dim br As BinaryReader = New BinaryReader(fs)

         Dim imagen() As Byte = br.ReadBytes(fs.Length)

         br.Close()
         fs.Close()

         Return imagen
      End If
   End Function
#End Region

#Region "Eventos de Controles"
   Private Sub btnAgre_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnReset_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
      mReset()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnSubir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubir.Click
      Try
         mGuardarArchivo()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarArchivo()
      Dim lstrNombre As String
      Dim lstrCarpeta As String

      If Not (txtFile.PostedFile Is Nothing) AndAlso txtFile.PostedFile.FileName <> "" Then
         lstrNombre = txtFile.PostedFile.FileName
         lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)

         lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("logoImagenes").ToString()

         txtFile.PostedFile.SaveAs(Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
      End If

      txtLogo.Text = lstrNombre
      mCargarImagen(lstrNombre)
   End Sub

   Private Sub mCargarImagen(ByVal pstrNombre As String)
      Dim lstrCarpeta As String
      If pstrNombre <> "" Then
         lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("logoImagenes").ToString()
         imgImagen.ImageUrl = lstrCarpeta + "\" + pstrNombre
         imgImagen.Visible = True
      Else
         imgImagen.ImageUrl = ""
         imgImagen.Visible = False
      End If
   End Sub
#End Region

   Private Sub grdDato_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDato.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 Then
            With CType(e.Item.DataItem, DataRowView).Row
               If CBool(.Item("empr_despa")) Then
                  e.Item.Cells(Columnas.despa).Text = "SI"
               Else
                  e.Item.Cells(Columnas.despa).Text = "NO"
               End If

               If CBool(.Item("empr_factu")) Then
                  e.Item.Cells(Columnas.factu).Text = "SI"
               Else
                  e.Item.Cells(Columnas.factu).Text = "NO"
               End If

               If CBool(.Item("empr_pilo")) Then
                  e.Item.Cells(Columnas.pilo).Text = "SI"
               Else
                  e.Item.Cells(Columnas.pilo).Text = "NO"
               End If

               If CBool(.Item("empr_presu")) Then
                  e.Item.Cells(Columnas.presu).Text = "SI"
               Else
                  e.Item.Cells(Columnas.presu).Text = "NO"
               End If
            End With
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class