Imports Business.Facturacion.CobranzasBusiness
Imports System.Collections
Imports SRA

Public Class CobranzasRecepcionPagosMisCuentas
    Inherits FormGenerico
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalApl As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalAplic As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCabeTotalPag As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalPago As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalDif As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalDifer As System.Web.UI.WebControls.Label
    'Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblCabeTotalRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCantTotalRegistrosDato As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCantTotalPago As System.Web.UI.WebControls.Label
    'Protected WithEvents btnConfirmar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    'Private mstrConn As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mstrConn As String = clsWeb.gVerificarConexion(Me)

        If (Not Page.IsPostBack) Then
            '' obtengo la coneccion
            'mstrConn = clsWeb.gVerificarConexion(Me)
            Me.lblTitu.Text = "Recepción de Cobranzas Pagos mis Cuentas"
            ' Blanqueo la variable de session
            Me.Session.Remove("GetCobranzasARecepcionar")
            ' variable de session que controla la doble operacion sobre el 
            ' boton confirmar
            Me.Session.Add("CobranzasRecepcionPagosMisCuentas_btnConfirmar", False)
            ' se consulta para cargar la grilla
            mConsultar()
        End If
    End Sub
    ' Dario 2013-05-31 Metodo que se ejecuta en el cambio de pagina(metodo de paginacion de la grilla)
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    ' Dario 2013-05-31 Metodo que carga la grilla 
    Private Sub mConsultar()
        Try
            Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
            ' ejecuto el metodo que recupera los datos a mostar en la grilla
            Dim idEncabezado As Integer = 0
            Dim ds As DataSet = Business.Facturacion.CobranzasBusiness.GetCobranzasARecepcionar(Me.User.Identity.Name, idEncabezado)
            ' ejecuto metodo que arma el resumen
            Me.getDatosResumen(ds)
            '
            Me.Session.Add("GetCobranzasARecepcionar", ds)
            '
            ' cargo el resultado de la consulta en la grilla 
            grdDato.DataSource = ds
            grdDato.DataBind()
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    'Dario 2013-05-31 metodo que hace el calculo del resumen de la pagina
    Private Sub getDatosResumen(ByVal oDs As DataSet)
        If (oDs Is Nothing And oDs.Tables.Count > 0) Then
            Me.lblCantTotalRegistrosDato.Text = "0"
            Me.lblCantTotalPago.Text = "$  0,00"
        Else
            Me.lblCantTotalRegistrosDato.Text = oDs.Tables(0).Rows.Count.ToString()
            Me.lblCantTotalPago.Text = ("$ " & Me.getMontoNovedadesRecibidas(oDs).ToString("#,###,##0.00")).Replace(",", "*").Replace(".", ",").Replace("*", ".")
        End If
    End Sub

    ' Dario 2013-05-31 Metodo que hace el calculo del importe total 
    '                  de las novedades a ingresar
    Private Function getMontoNovedadesRecibidas(ByVal oDs As DataSet) As Decimal
        ' Seteo variable de respuesta
        Dim respuesta As Decimal = 0
        ' recorro la grilla y acumulo el importe de cada item
        For Each dr As DataRow In oDs.Tables(0).Rows
            respuesta = respuesta + System.Convert.ToDecimal(dr("Importe"))
        Next
        ' retornamos la respuesta del importe totalizado
        Return respuesta
    End Function

    ' Dario 2013-05-31 Evento del boton Confirmar
    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        ' Verifico que la variable de sesion este <> true para proseguir con la operacion 
        ' sino se oprimio mas de una vez el boton confirmar
        If (Me.Session("CobranzasRecepcionPagosMisCuentas_btnConfirmar") <> True) Then
            ' Ingreso ponemos la variable de session en true
            Me.Session.Add("CobranzasRecepcionPagosMisCuentas_btnConfirmar", True)
            ' recupero la coleccion de la session de los datos cargados en la grilla
            Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
            Dim listaCoVtIds As New ArrayList
            ' 
            Dim ds As DataSet = Me.Session("GetCobranzasARecepcionar")
            ' obtengo en nombre de la pc para dejar registro
            Dim lstrHost As String = System.Net.Dns.GetHostByAddress(Request.ServerVariables("REMOTE_ADDR")).HostName
            ' ejecuta el metodo que registra las cobranzas 
            ' ingresadas de pagos mis cuentas
            Dim respuesta As String = "" ' _CobranzasBusiness.RecepcionarCobranzasPagosMisCuentas(ds, Me.Session("sUserId"), lstrHost)

            'si hubo errores la cadena de respuesta tiene length mayor a 0
            If (respuesta.Length > 0) Then
                Response.Write("<script language='javascript'>window.alert('" & respuesta & "');</script>")
            Else
                ' menasje de exito, glorioso.
                Response.Write("<script language='javascript'>window.alert('La operación de recepción termino correctamente!!');</script>")
                'Dim lDsAler As DataSet
                'lDsAler = clsSQLServer.gObtenerEstruc(mstrConn, "alertas", "@aler_alti_id=" & SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_INGRESO)
                'If lDsAler.Tables(0).Rows.Count > 0 Then
                '    SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), lDsAler.Tables(0).Rows(0).Item("aler_id"), clsSQLServer.gFormatArg(lobj.AcreId, SqlDbType.VarChar))
                'End If
            End If
            ' Cuando termina se vuelve a dejar la variable de session en false
            Me.Session.Add("CobranzasRecepcionPagosMisCuentas_btnConfirmar", False)
            ' elimino la coleccion de pagos luego de completar la operacion
            ' de recepciond e cobranzas
            Me.Session.Remove("GetCobranzasARecepcionar")
            ' recargo la grilla por si hay errores en la operacion asi los muestra
            Me.mConsultar()
        End If
    End Sub
End Class
