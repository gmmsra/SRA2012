<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ConsultaDatos" CodeFile="ConsultaDatos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right">
									<DIV onclick="window.close();"><IMG style="CURSOR: hand" alt="Cerrar" src="imagenes/close3.bmp" border="0">
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD><asp:label id="lblTituAbm" runat="server" cssclass="titulo"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" height="10"></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top" align="center">
									<div class="cuadrotexto2"><br>
										<table border="0" width="97%">
											<tr>
												<td>
													<asp:label id="txtDato" runat="server" cssclass="titulo"></asp:label>
												</td>
											</tr>
										</table>
										<br>
									</div>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" height="10"></TD>
							</TR>
							<TR>
								<TD><asp:label id="lblComen" runat="server" cssclass="titulo">Comentario:</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" height="10"></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top" align="center">
									<div class="cuadrotexto2"><br>
										<table border="0" width="97%">
											<tr>
												<td>
													<asp:label id="lblComentarioNixor" runat="server" cssclass="titulo"></asp:label>
												</td>
											</tr>
										</table>
										<br>
									</div>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" height="10"></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top" align="center">
									<div class="cuadrotexto2">
										<table border="0" width="97%">
											<TR>
												<TD><asp:label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:label></TD>
											</TR>
											<tr>
												<td>
													<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="75px" TextMode="MultiLine"></CC1:TEXTBOXTAB>
												</td>
											</tr>
											<TR>
												<td align="right"><asp:Button id="btnModi" runat="server" cssclass="boton" Width="110px" Text="Guardar Observ."></asp:Button></td>
											</TR>
										</table>
										<br>
									</div>
								</TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
