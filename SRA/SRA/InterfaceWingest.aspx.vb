Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Diagnostics.Debug


Namespace SRA



Partial Class InterfaceWingest
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents btnVerArch As System.Web.UI.WebControls.Button

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"

    Private mstrTablaNaci As String = SRA_Neg.Constantes.gTab_ProductosInscrip
    Private mstrTablaServ As String = SRA_Neg.Constantes.gTab_ServiDenuncias
    Private mstrTablaServDeta As String = SRA_Neg.Constantes.gTab_ServiDenunciasDeta
    Private mstrTablaTeDenu As String = SRA_Neg.Constantes.gTab_TeDenun
    Private mstrTablaTeDenuDeta As String = SRA_Neg.Constantes.gTab_TeDenunDeta
    Private mstrCmd As String
    Private mstrConn As String
    Private dsValiTeDenu As DataSet
    Private dsValiNaci As DataSet
    Private dsValiServ As DataSet
    Private mstrArchivoOri As String
    Private lstrServDetaId As String
    Private lstrServId As String

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lstrFil As String
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                btnImpo.Attributes.Add("onclick", "if(confirm('Desea iniciar el proceso de importaci�n de datos?')){}else {return false} ")
            End If
            btnListar.Enabled = False

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub btnImpo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImpo.Click
        mImportar()

    End Sub

    Private Sub mImportar()

        If (filArch.Value = "") Then
            lblMens.Text = "Error: " & "Debe indicar el archivo a importar."
            Return
        End If
        If (txtFechaRecep.Text = "") Then
            lblMens.Text = "Error: " & "Debe indicar la Fecha de Recepci�n."
            Return
        End If

        Try

            lisResu.Items.Clear()

            Dim lstrFilename As String = filArch.Value
            Dim lstrTempPath As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_temp_path")
            Dim lstrArchNomb As String = lstrFilename.Substring(lstrFilename.LastIndexOf("\") + 1)
            mstrArchivoOri = lstrArchNomb.ToString()

            Dim lstrExte As String = IIf(cmbTipo.Valor.ToString = "DBF", "dbf", "xls")
            lstrArchNomb = lstrArchNomb.Substring(0, lstrArchNomb.LastIndexOf(".") + 1) & lstrExte
            Dim lstrArchTemp As String = "C:\" & lstrTempPath & "\" & lstrArchNomb

            If Not (filArch.PostedFile Is Nothing) Then
                If Not System.IO.Directory.Exists("C:\" & lstrTempPath) Then
                    System.IO.Directory.CreateDirectory("C:\" & lstrTempPath)
                End If
                filArch.PostedFile.SaveAs(lstrArchTemp)
            End If

            mProcesarArchivo(lstrArchTemp)

            File.Delete(lstrArchTemp)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Protected Sub mMostrarMensaje(ByVal pstrError As String, ByVal pintRegi As Integer)
        Dim lstrMsg As String = ""
        If (pintRegi <> -1) Then
            lstrMsg = lstrMsg & "Registro #" & Convert.ToString(pintRegi + 1) & ": "
        End If
        lstrMsg = lstrMsg & pstrError
        lisResu.Items.Insert(0, lstrMsg)
    End Sub

    Protected Sub mProcesarArchivo(ByVal pstrFile As String)
        Dim lintRecAff As Integer

        Try
            Dim lstrId As String
            Dim lstrIdCabe As String
            Dim lstrIdDeta As String
            Dim lstrFecha As String
            Dim lstrMsgError As String
            Dim lstrRaza As String
            Dim lstrProp As String
            Dim lstrNoveTipo As String
            Dim lintCantRegi As Integer = 0
            Dim lintCantIgno As Integer = 0
            Dim ldsDatos As DataSet
            Dim lstrFechaVali As String
            Dim blnErrores As Boolean = False
            Dim lstrbuilErrores As StringBuilder = New StringBuilder("")
            Dim lstrPath As String = pstrFile.Substring(0, pstrFile.LastIndexOf("\") + 1)
            Dim lstrArch As String = pstrFile.Substring(pstrFile.LastIndexOf("\") + 1)

            'Obtengo el nombre del archivo.
            'dim lstrNombreArchivo as string = txtArch.Text.Substring(txtArch.Text.LastIndexOf("\\"), (txtArch.Text.Length - txtArch.Text.LastIndexOf("\\")));
            'Obtengo el tipo de Archivo.
            Dim lstrTipoArchivo = mstrArchivoOri.Substring(mstrArchivoOri.IndexOf(".") + 1, 1)

            If cmbTipo.Valor.ToString = "DBF" Then
                ldsDatos = clsImporta.clsAbrirArchivo.mImportarDBF(lstrArch.Substring(0, lstrArch.LastIndexOf(".")), "*", "", lstrPath)
            Else
                ldsDatos = clsImporta.clsAbrirArchivo.mImportarExcel(pstrFile, "*", "", lstrArch.Substring(0, lstrArch.LastIndexOf(".")))
            End If

            Interfaces.Importacion.Inicializar(mstrConn, Session("sUserId").ToString(), dsValiNaci, dsValiServ, dsValiTeDenu)

            'Raza: primeros 3 caracteres en el nombre del archivo.
            lstrRaza = lstrArch.Substring(0, 3)
            'Prop: el resto del nombre del archivo correponde al propietario.
            lstrProp = Convert.ToInt32(lstrArch.Substring(3, lstrArch.LastIndexOf(".") - 3)).ToString()

            lisResu.Items.Clear()

            'Doy de alta la cabecera del registro Con tipo de Archivo "W" = WinGest
            lstrIdCabe = Interfaces.Importacion.ProcesarCabecera(mstrArchivoOri, txtFechaRecep.Fecha, "W")
            txtIdCabe.Text = lstrIdCabe

            'mMostrarMensaje("Iniciando proceso de importaci�n...", -1)

            For Each odrDato As DataRow In ldsDatos.Tables(0).Select()

                If odrDato.Item("DENU") = 1 Then
                    'Las denuncias de tipo 1 son las unicas que se informan.
                    'Ignorar el resto de los registros.

                    If mstrArchivoOri.IndexOf("B") = -1 Then
                        'Valida Raza. Siempre y cuando el archivo no sea de Baja.
                        lstrMsgError = Interfaces.Importacion.ValidarRaza(odrDato, lstrRaza)
                    End If
                    If lstrMsgError <> "" Then
                        mMostrarMensaje(lstrMsgError, lintCantRegi)
                        If lintCantRegi = 0 Then blnErrores = True
                    Else
                        lstrMsgError = Interfaces.Importacion.ValidarPropietario(odrDato, lstrProp)
                        If lstrMsgError <> "" Then
                            mMostrarMensaje(lstrMsgError, lintCantRegi)
                            If lintCantRegi = 0 Then blnErrores = True
                        Else
                            lstrNoveTipo = odrDato.Item("TNOV")

                            Select Case lstrNoveTipo

                                Case SRA_Neg.Constantes.TipoNovedad.DenunciaNacimientoNatural, SRA_Neg.Constantes.TipoNovedad.DenunciaNacimientoTE

                                    ' DA DE ALTA LA DENUNCIA DE NACIMIENTO
                                    lstrMsgError = Interfaces.Importacion.ValidarDatosImportados(odrDato)
                                    If (lstrMsgError <> "") Then
                                        mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                        If lintCantRegi = 0 Then blnErrores = True
                                    Else
                                        lstrId = Interfaces.Importacion.ProcesarImportacion(odrDato, lstrIdCabe, lstrTipoArchivo)

                                        lstrMsgError = Interfaces.Importacion.ValidarFechas(odrDato, mstrTablaNaci)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                        lstrMsgError = Interfaces.Importacion.ValidarNombre(odrDato)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                        lstrId = Interfaces.Importacion.ProcesarNacimiento(odrDato, "", txtFechaRecep.Fecha)

                                        'lstrFechaVali = Interfaces.Importacion.FechaProcesoNacimiento(odrDato)
                                        'dsValiNaci = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), ReglasValida.Validaciones.TiposValidacion.CapturaTxt, lstrFechaVali, lstrFechaVali)

                                        lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, mstrTablaNaci, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), 0, 0)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If
                                    End If
                                Case SRA_Neg.Constantes.TipoNovedad.DenunciaImplante

                                    ' DA DE ALTA LA DENUNCIA DE IMPLANTE
                                    lstrMsgError = Interfaces.Importacion.ValidarDatosImportados(odrDato)
                                    If (lstrMsgError <> "") Then
                                        mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                        If lintCantRegi = 0 Then blnErrores = True
                                    Else

                                        lstrId = Interfaces.Importacion.ProcesarImportacion(odrDato, lstrIdCabe, lstrTipoArchivo)

                                        lstrMsgError = Interfaces.Importacion.ValidarFechas(odrDato, mstrTablaTeDenuDeta)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                        lstrId = Interfaces.Importacion.ProcesarImplante(odrDato)

                                        lstrFechaVali = Interfaces.Importacion.FechaProcesoImplante(odrDato)
                                        dsValiTeDenu = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), ReglasValida.Validaciones.TiposValidacion.CapturaTxt, lstrFechaVali, lstrFechaVali)

                                        lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsValiTeDenu, mstrTablaTeDenuDeta, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), 0, lstrId)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If
                                    End If
                                Case SRA_Neg.Constantes.TipoNovedad.DenunciaServicio

                                    ' DA DE ALTA LA DENUNCIA DE SERVICIO
                                    lstrMsgError = Interfaces.Importacion.ValidarDatosImportados(odrDato)
                                    If (lstrMsgError <> "") Then
                                        mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                        If lintCantRegi = 0 Then blnErrores = True
                                    Else

                                        lstrId = Interfaces.Importacion.ProcesarImportacion(odrDato, lstrIdCabe, lstrTipoArchivo)

                                        ' DA DE ALTA LA DENUNCIA DE SERVICIO (CABECERA)
                                        lstrMsgError = Interfaces.Importacion.ValidarFechas(odrDato, mstrTablaServ)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                        lstrServId = Interfaces.Importacion.ProcesarServicio(odrDato, txtFechaRecep.Fecha)

                                        lstrFechaVali = Interfaces.Importacion.FechaProcesoServicio(odrDato)
                                        dsValiServ = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Servicios.ToString(), ReglasValida.Validaciones.TiposValidacion.CapturaTxt, lstrFechaVali, lstrFechaVali)

                                        lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsValiServ, mstrTablaServ, lstrServId, lstrRaza, "", Session("sUserId").ToString(), True, _
                                        ReglasValida.Validaciones.Procesos.Servicios.ToString(), 0, 0)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If
                                        ' DA DE ALTA LA DENUNCIA DE SERVICIO (DETALLE)
                                        lstrMsgError = Interfaces.Importacion.ValidarFechas(odrDato, mstrTablaServDeta)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                        lstrServDetaId = Interfaces.Importacion.ProcesarServicioDetalle(odrDato, lstrServId)

                                        lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsValiServ, mstrTablaServDeta, lstrServDetaId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Servicios.ToString(), 0, 0)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                    End If


                                Case SRA_Neg.Constantes.TipoNovedad.DenunciaBajaProducto
                                    'TODO:  GSZ - Revisar validaciones TipoNovedad.DenunciaBajaProducto
                                    lstrMsgError = Interfaces.Importacion.ValidarDatosImportados(odrDato)
                                    If (lstrMsgError <> "") Then
                                        mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                        If lintCantRegi = 0 Then blnErrores = True
                                    Else

                                        lstrId = Interfaces.Importacion.ProcesarImportacion(odrDato, lstrIdCabe, lstrTipoArchivo)

                                        'Procesar Baja.
                                        Dim lintRegAff As Integer = Interfaces.Importacion.ProcesarBaja(odrDato)

                                        'NO encontr� el Producto.
                                        If lintRegAff = 0 Then
                                            lstrbuilErrores.Append("Error: ")
                                            lstrbuilErrores.AppendFormat("T.Nov.: {0} - ", odrDato("TNOV").ToString())
                                            lstrbuilErrores.AppendFormat("Raza: {0} - ", odrDato("RAZA").ToString())
                                            lstrbuilErrores.AppendFormat("Prop.: {0} - ", odrDato("PROP").ToString())
                                            lstrbuilErrores.AppendFormat("L�nea: {0} - ", odrDato("LNEA").ToString())
                                            lstrbuilErrores.AppendFormat("Folio: {0} - ", odrDato("FOLI").ToString())
                                            lstrbuilErrores.AppendFormat("Regis.: {0} - ", odrDato("RGST").ToString())
                                            lstrbuilErrores.AppendFormat("Varie.: {0} - ", odrDato("VARI").ToString())
                                            lstrbuilErrores.AppendFormat("HBA: {0} - ", odrDato("HBAE").ToString())
                                            lstrbuilErrores.AppendFormat("RP: {0} - ", odrDato("RPEX").ToString().Trim())
                                            lstrbuilErrores.AppendFormat("Sexo: {0} - ", odrDato("SEXO").ToString())
                                            lstrbuilErrores.AppendFormat("C�d.Baja: {0} - ", odrDato("CBAJ").ToString())
                                            lstrbuilErrores.AppendFormat("Fec.Baja: {0}", odrDato("FBAJ").ToString().Substring(0, 10))

                                            mMostrarMensaje(lstrbuilErrores.ToString(), lintCantRegi)
                                        End If

                                    End If

                                Case SRA_Neg.Constantes.TipoNovedad.DenunciaMochos
                                    'TODO: GSZ - Revisar validaciones TipoNovedad.DenunciaMochos
                                    lstrMsgError = Interfaces.Importacion.ValidarDatosImportados(odrDato)
                                    If (lstrMsgError <> "") Then
                                        mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                        If lintCantRegi = 0 Then blnErrores = True
                                    Else
                                        lstrId = Interfaces.Importacion.ProcesarImportacion(odrDato, lstrIdCabe, lstrTipoArchivo)

                                        lstrMsgError = Interfaces.Importacion.ValidarFechas(odrDato, mstrTablaNaci)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                        lstrMsgError = Interfaces.Importacion.ValidarNombre(odrDato)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If
                                    End If

                                Case SRA_Neg.Constantes.TipoNovedad.DenunciaRecuperacionEmbriones
                                    'TODO: GSZ - Revisar validaciones TipoNovedad.DenunciaRecuperacionEmbriones
                                    lstrMsgError = Interfaces.Importacion.ValidarDatosImportados(odrDato)
                                    If (lstrMsgError <> "") Then
                                        mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                        If lintCantRegi = 0 Then blnErrores = True
                                    Else
                                        lstrId = Interfaces.Importacion.ProcesarImportacion(odrDato, lstrIdCabe, lstrTipoArchivo)

                                        lstrMsgError = Interfaces.Importacion.ValidarFechas(odrDato, mstrTablaNaci)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                        lstrMsgError = Interfaces.Importacion.ValidarNombre(odrDato)
                                        If (lstrMsgError <> "") Then
                                            mMostrarMensaje("Error: " + lstrMsgError, lintCantRegi)
                                            If lintCantRegi = 0 Then blnErrores = True
                                        End If

                                    End If
                                Case Else
                                    mMostrarMensaje("C�digo de novedad inv�lido: " + lstrNoveTipo, lintCantRegi)
                                    If lintCantRegi = 0 Then blnErrores = True
                            End Select

                        End If
                    End If
                    If lstrMsgError <> "" Then
                        lintCantIgno = lintCantIgno + 1
                    End If
                Else
                    lintCantIgno = lintCantIgno + 1
                End If

                lintCantRegi = lintCantRegi + 1
            Next

            If lintCantIgno > 0 Then
                btnListar.Enabled = False
            Else
                lblArchivoOrigen.Text = mstrArchivoOri
                btnListar.Enabled = True
            End If

            If blnErrores Then
                lintRecAff = Interfaces.Importacion.EliminarCabecera(txtIdCabe.Text.Trim)
            Else
                mMostrarMensaje("Registros ignorados: " + lintCantIgno.ToString(), -1)
                mMostrarMensaje("Registros procesados: " + lintCantRegi.ToString(), -1)
                mMostrarMensaje("Se complet� el proceso de importaci�n del archivo: " + mstrArchivoOri, -1)
            End If


        Catch ex As Exception
            lintRecAff = Interfaces.Importacion.EliminarCabecera(txtIdCabe.Text.Trim)
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Try
            Dim lstrRptName As String
            Dim lintRF As Integer
            Dim lstrRepoPara As String = ""
            Dim lstrRepo As String = ""
            Dim strNombreArchivo As String
            Dim lstrRpt As String

            mstrArchivoOri = lblArchivoOrigen.Text

            'Obtengo el nombre del archivo.
            strNombreArchivo = mstrArchivoOri.Substring(mstrArchivoOri.LastIndexOf("."), (mstrArchivoOri.Length - mstrArchivoOri.LastIndexOf(".")))

            'Verifico que letra contiene el nombre del archivo.
            If (strNombreArchivo.IndexOf("N") <> -1) Then
                'Nacimiento.
                lstrRepo = "ImportacionWingestNacimiento"
            ElseIf (strNombreArchivo.IndexOf("S") <> -1) Then
                'Servicios.
                lstrRepo = "ImportacionWingestServicios"
            ElseIf (strNombreArchivo.IndexOf("R") <> -1) Then
                'Recuperaci�n.
                lstrRepo = "ImportacionWingestRecuperacion"
            ElseIf (strNombreArchivo.IndexOf("T") <> -1) Then
                'Trasplante.
                lstrRepo = "ImportacionWingestTrasplante"
            ElseIf (strNombreArchivo.IndexOf("I") <> -1) Then
                'Implante.
                lstrRepo = "ImportacionWingestImplantacion"
            ElseIf (strNombreArchivo.IndexOf("B") <> -1) Then
                'Baja.
                lstrRepo = "ImportacionWingestBaja"
            Else
                lstrRepo = "ImportacionWingest"
            End If

            lstrRepo += "&impo_id=" & IIf(txtIdCabe.Text = "", "0", txtIdCabe.Text)

            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRepo)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class

End Namespace
