<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="controles/usrSociosFiltro.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.RevaluosConfirma" CodeFile="RevaluosConfirma.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Confirmación de Revaluo de Cuotas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Confirmación de Revaluo de Cuotas</asp:label></TD>
									</TR>
									<TR>
										<TD align="right" width="100%" colSpan="3"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" IncludesUrl="includes/"
												ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" BackColor="Transparent"
												ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD colspan="3" vAlign="top" align="center"><!--DIV id="panGrid" style="BORDER-RIGHT: #003784 0px solid; BORDER-TOP: #003784 0px solid; OVERFLOW: auto; BORDER-LEFT: #003784 0px solid; WIDTH: 560px; BORDER-BOTTOM: #003784 0px solid; HEIGHT: 100%"-->
															<asp:datagrid id="grdCons" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" CellPadding="1"
																CellSpacing="1" AutoGenerateColumns="False" ItemStyle-Height="5px" OnPageIndexChanged="DataGrid_Page"
																GridLines="None" HorizontalAlign="Center" AllowPaging="True">
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<FooterStyle CssClass="footer"></FooterStyle>
																<Columns>
																	<asp:TemplateColumn Visible="False">
																		<HeaderStyle Width="5%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																				Height="5">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn DataField="cate_desc" HeaderText="Categor&#237;a"></asp:BoundColumn>
																	<asp:BoundColumn DataField="comp_canti" HeaderText="Casos"></asp:BoundColumn>
																	<asp:BoundColumn DataField="reva_valor" HeaderText="Valor Cuota"></asp:BoundColumn>
																	<asp:BoundColumn DataField="reva_ante_valor" HeaderText="Valor Anter."></asp:BoundColumn>
																	<asp:BoundColumn DataField="periodo" HeaderText="Per&#237;odo"></asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid><!--/DIV--></TD>
														<TD width="2"></TD>
													</TR>
													<TR>
														<TD width="3" height="10"></TD>
														<TD vAlign="middle" align="right" height="10"></TD>
														<TD width="2" height="10"></TD>
													</TR>
													<TR>
														<TD colspan="3" vAlign="middle" align="right"><A id="editar" name="editar"></A>
															<asp:Button id="btnBorr" runat="server" cssclass="boton" Width="140px" Text="Borrar"></asp:Button>&nbsp;
															<asp:Button id="btnConf" runat="server" cssclass="boton" Width="140px" Text="Confirmar"></asp:Button></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
							</TBODY>
						</table>
					<!--- FIN CONTENIDO ---> 
					</TD>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</TR>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</TBODY>
			</TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE>
		<script language="javascript">
		if (document.all["editar"]!= null && document.all("panGrab")!=null )
			document.location='#editar';
		</script>
		</FORM>
	</BODY>
</HTML>
