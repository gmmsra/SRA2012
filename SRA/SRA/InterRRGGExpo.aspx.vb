Imports System.IO
Imports System.Text
Imports System.Data.SqlClient


Namespace SRA


Partial Class InterRRGGExpo
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            mInicializar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdCons.PageSize = Convert.ToInt32(mstrParaPageSize)
        btnGrab.Attributes.Add("onclick", "return(mProcesar());")
   End Sub

   Private Sub mLimpiarFiltros()
      txtFechaDesde.Text = ""
      txtFechaHasta.Text = ""
      panGrab.Visible = False
      grdCons.Visible = False
      lblPrev.Visible = False
      cmbPend.Valor = 1
   End Sub

#End Region

#Region "Consultas"

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mExecuteQuery(ByVal pstrSql As String) As DataSet
      Dim cmdExecCommand As SqlDataAdapter = New SqlDataAdapter(pstrSql, mstrConn)
      Dim ds = New DataSet

      cmdExecCommand.Fill(ds)

      Return (ds)
   End Function

   Private Function mFormatearValor(ByVal pstrParam As Object, ByVal pstrTipo As SqlDbType, Optional ByVal pbooAdmiteNull As Boolean = True) As String
        Dim lstrValor As String = ""
        If pstrParam Is DBNull.Value Then
            If pbooAdmiteNull Then
                lstrValor = "null"
            Else
                Select Case pstrTipo
                    Case SqlDbType.VarChar
                        lstrValor = "''"
                    Case SqlDbType.Int
                        lstrValor = "0"
                End Select
            End If
        Else
            lstrValor = pstrParam.ToString.Trim
            Select Case pstrTipo
                Case SqlDbType.VarChar
                    lstrValor = "'" & lstrValor & "'"
                Case SqlDbType.Int
                    If lstrValor = "" Then
                        lstrValor = "0"
                    End If
            End Select
        End If
        mFormatearValor = lstrValor
    End Function

    Private Sub mProcesarDbf(ByVal pstrArchCabe As String, ByVal pstrArchDeta As String, ByVal pDs As DataSet)

        Dim lstrCon As String
        Dim lTrans As OleDb.OleDbTransaction
        Dim lstrCmd As String = ""
        Dim lobjCon As New System.Data.OleDb.OleDbConnection
        Dim lintCantReg As Integer = 0
        Dim lintCantErr As Integer = 0
        Dim dr As System.Data.OleDb.OleDbDataReader
        Dim mstrTabla As String = txtArch.Text.ToUpper.Replace(".DBF", "")
        Dim mstrTablaDeta As String = txtArchDeta.Text.ToUpper.Replace(".DBF", "")
        Dim lstrPath As String = System.Configuration.ConfigurationSettings.AppSettings("conPathExpoRRGG")
        Dim dsExiste As DataSet
        Dim lstrCondicion As String

        lstrCon = "Provider=VFPOLEDB.1;Data Source=" & lstrPath & ";"
        'lstrCon = "Provider=Microsoft.Jet.OLEDB.4.0;" & "Data Source=" & lstrPath & ";Extended Properties=dBase IV;"

        lobjCon = New System.Data.OleDb.OleDbConnection(lstrCon)
        lobjCon.Open()
        lTrans = lobjCon.BeginTransaction()

        '---- CABECERA ----
        For Each ldrOri As DataRow In pDs.Tables(0).Rows
            lintCantReg += 1

            lstrCondicion = " WHERE CTRLFAC = " & mFormatearValor(ldrOri.Item("CTRLFAC"), SqlDbType.VarChar)
            dsExiste = clsImporta.clsAbrirArchivo.mImportarDBF(mstrTabla, "*", lstrCondicion, lstrPath & "\")

            If dsExiste.Tables(0).Rows.Count = 0 Then
                lstrCmd = "INSERT INTO " & mstrTabla
                lstrCmd = lstrCmd & "(CTRLFAC, NROCTE, TIPOSERV, RAZA, CANTSFAC, CANTNOFACT, NROFACT)"
                lstrCmd = lstrCmd & " VALUES (" & mFormatearValor(ldrOri.Item("CTRLFAC"), SqlDbType.VarChar)
                lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("NROCTE"), SqlDbType.VarChar)
                lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("TIPOSERV"), SqlDbType.Int)
                lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("RAZA"), SqlDbType.Int, False)
                lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("CANTSFAC"), SqlDbType.Int)
                lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("CANTNOFACT"), SqlDbType.Int)
                lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("NROFACT"), SqlDbType.VarChar) & ")"
            Else
                lstrCmd = "UPDATE " & mstrTabla & " SET "
                lstrCmd = lstrCmd & "  NROCTE = " & mFormatearValor(ldrOri.Item("NROCTE"), SqlDbType.VarChar)
                lstrCmd = lstrCmd & ", TIPOSERV = " & mFormatearValor(ldrOri.Item("TIPOSERV"), SqlDbType.Int)
                lstrCmd = lstrCmd & ", RAZA = " & mFormatearValor(ldrOri.Item("RAZA"), SqlDbType.Int, False)
                lstrCmd = lstrCmd & ", CANTSFAC = " & mFormatearValor(ldrOri.Item("CANTSFAC"), SqlDbType.Int)
                lstrCmd = lstrCmd & ", CANTNOFACT = " & mFormatearValor(ldrOri.Item("CANTNOFACT"), SqlDbType.Int)
                lstrCmd = lstrCmd & ", NROFACT = " & mFormatearValor(ldrOri.Item("NROFACT"), SqlDbType.VarChar)
                lstrCmd = lstrCmd & " WHERE CTRLFAC = " & mFormatearValor(ldrOri.Item("CTRLFAC"), SqlDbType.VarChar)
            End If

            Try
                Dim myCommand As New System.Data.OleDb.OleDbCommand(lstrCmd, lobjCon)
                myCommand.Transaction = lTrans
                myCommand.ExecuteNonQuery()
            Catch ex As Exception
                lintCantErr += 1
                clsError.gManejarError(Me, ex)
            End Try
        Next

        '---- DETALLE ----
        'lintCantReg = 0
        'lintCantErr = 0
        'For Each ldrOri As DataRow In pDs.Tables(1).Rows
        '    lintCantReg += 1

        '    lstrCondicion = " WHERE CTRLFAC = " & mFormatearValor(ldrOri.Item("CTRLFAC"), SqlDbType.VarChar)
        '    lstrCondicion = lstrCondicion & " AND NROREF = " & mFormatearValor(ldrOri.Item("NROREF"), SqlDbType.Int)
        '    dsExiste = clsImporta.clsAbrirArchivo.mImportarDBF(mstrTablaDeta, "*", lstrCondicion, lstrPath & "\")

        '    If dsExiste.Tables(0).Rows.Count = 0 Then
        '        lstrCmd = "INSERT INTO " & mstrTablaDeta
        '        lstrCmd = lstrCmd & "(CTRLFAC, RAZA, NROREF, ESTADO, NUECTRLFAC)"
        '        lstrCmd = lstrCmd & " VALUES (" & mFormatearValor(ldrOri.Item("CTRLFAC"), SqlDbType.VarChar)
        '        lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("RAZA"), SqlDbType.Int, False)
        '        lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("NROREF"), SqlDbType.Int)
        '        lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("ESTADO"), SqlDbType.Int)
        '        lstrCmd = lstrCmd & "," & mFormatearValor(ldrOri.Item("NUECTRLFAC"), SqlDbType.VarChar) & ")"
        '    Else
        '        lstrCmd = "UPDATE " & mstrTablaDeta & " SET "
        '        lstrCmd = lstrCmd & "  RAZA = " & mFormatearValor(ldrOri.Item("RAZA"), SqlDbType.Int, False)
        '        lstrCmd = lstrCmd & ", ESTADO = " & mFormatearValor(ldrOri.Item("ESTADO"), SqlDbType.Int)
        '        lstrCmd = lstrCmd & ", NUECTRLFAC = " & mFormatearValor(ldrOri.Item("NUECTRLFAC"), SqlDbType.VarChar)
        '        lstrCmd = lstrCmd & " WHERE CTRLFAC = " & mFormatearValor(ldrOri.Item("CTRLFAC"), SqlDbType.VarChar)
        '        lstrCmd = lstrCmd & " AND NROREF = " & mFormatearValor(ldrOri.Item("NROREF"), SqlDbType.Int)
        '    End If

        '    Try
        '        Dim myCommand As New System.Data.OleDb.OleDbCommand(lstrCmd, lobjCon)
        '        myCommand.Transaction = lTrans
        '        myCommand.ExecuteNonQuery()
        '    Catch ex As Exception
        '        lintCantErr += 1
        '        clsError.gManejarError(Me, ex)
        '    End Try
        'Next

        lTrans.Commit()
        lTrans = Nothing

        lobjCon.Close()
        lobjCon = Nothing

        lstrPath = lstrPath.Replace("c:\Inetpub\wwwroot\", "http://localhost/")
        lstrPath = lstrPath.Replace("\", "/") & "/"
        hdnExpo.Text = lstrPath & txtArch.Text.ToLower
        hdnExpoDeta.Text = lstrPath & txtArchDeta.Text.ToLower

   End Sub

   Private Function mFormatearFecha(ByVal pstrFecha As String) As String
      Dim lstrFecha As String = ""
      If pstrFecha = "" Then
         lstrFecha = ""
      Else
         lstrFecha = CDate(pstrFecha).ToString("MM/dd/yyyy")
      End If
      Return (lstrFecha)
   End Function

   Private Function mExecute(ByVal pstrProc As String) As Integer
        Dim myConnection = New SqlConnection(mstrConn)
        Dim cmdExecCommand = New SqlCommand
        cmdExecCommand.Connection = myConnection
        cmdExecCommand.CommandText = pstrProc
        cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout
        myConnection.Open()
        Dim lintExec As Integer = cmdExecCommand.ExecuteNonQuery()
        myConnection.Close()
        Return (lintExec)
    End Function

    Private Sub mConsultar()
      Try

         If txtFechaDesde.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha desde.")
         End If

         If txtFechaHasta.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha hasta.")
         End If

         mstrCmd = "exec expo_interfaz_rrgg_consul "
         mstrCmd += " @fecha_desde=" + clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime)
         mstrCmd += ",@fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaHasta.Text, SqlDbType.SmallDateTime)
         mstrCmd += ",@top=1"
         mstrCmd += ",@pendientes=" + cmbPend.Valor

         Dim ds As New DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

         For Each dc As DataColumn In ds.Tables(0).Columns
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            dgCol.DataField = dc.ColumnName
            dgCol.HeaderText = dc.ColumnName
            'If dc.Ordinal = 0 Then
            '   dgCol.Visible = False
            'End If
            grdCons.Columns.Add(dgCol)
         Next
         grdCons.DataSource = ds
         grdCons.DataBind()
         grdCons.Visible = True
         If ds.Tables(0).Rows.Count = 30 Then
            lblPrev.Visible = True
         End If
         panGrab.Visible = True
         btnGrab.Enabled = (grdCons.Items.Count > 0)
         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub

    Private Sub btnCons_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCons.Click
        grdCons.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub btnGrab_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGrab.Click
        mGrabar()
    End Sub

    Private Sub mGrabar()

      Try

        Dim lstrPath As String = System.Configuration.ConfigurationSettings.AppSettings("conPathExpoRRGG")

        If txtArch.Text = "" Or txtArchDeta.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el nombre de los archivos.")
        End If

        mstrCmd = "exec expo_interfaz_rrgg_consul "
        mstrCmd += " @fecha_desde=" + clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime)
        mstrCmd += ",@fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaHasta.Text, SqlDbType.SmallDateTime)
        mstrCmd += ",@pendientes=" + cmbPend.Valor

        Dim ds As New DataSet
        ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        hdnExpo.Text = ""
        hdnExpoDeta.Text = ""
        lblMens.Text = ""

        mProcesarDbf(txtArch.Text, txtArchDeta.Text, ds)

        mLimpiarFiltros()

        'Throw New AccesoBD.clsErrNeg("La exportación ha finalizado correctamente.")
        lblMens.Text = "El proceso de exportación ha finalizado."

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      Finally
         ''mConsultar()
      End Try

    End Sub

#End Region

End Class
End Namespace
