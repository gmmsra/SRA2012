<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.IngresoPlantelBase" CodeFile="IngresoPlantelBase.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Ingreso Plantel Base</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
			function expandir()
			{
				try{ parent.frames("menu").CambiarExp();}catch(e){;}
			}
			
			function mDeterminarRP(pobjRP,pobjNume)
			{
				if (document.all(pobjNume).value == '' && pobjRP.value != '')
				{
					var lstrRp = '';
					var lstrChar = '';
					for (i=0;i<pobjRP.value.length;i++)
					{
						lstrChar = pobjRP.value.substring(i,i+1);
						if (booIsNumber(lstrChar))
							lstrRp = lstrRp + lstrChar;
					}
					if (document.all(pobjNume).value == '')
						document.all(pobjNume).value = lstrRp;
				}
			}
					
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Ingreso Plantel Base</asp:label></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderStyle="Solid"
												BorderWidth="0">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 21px" vAlign="middle" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblFechaPresenDesdeFil" runat="server" cssclass="titulo">Fecha Presentación Desde:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaPresenDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"></cc1:DateBox>&nbsp;
																					<asp:Label id="lblFechaPresenHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaPresenHastaFil" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 21px" vAlign="middle" background="imagenes/formfdofields.jpg"
																					width="20" align="right">
																					<asp:Label id="lblCriaFil" runat="server" cssclass="titulo">Raza/Criador:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriaFil" runat="server" Alto="560" FilClie="True" MuestraDesc="False" FilCUIT="True"
																						Ancho="780" FilDocuNume="True" Saltos="1,1,1" Tabla="Criadores"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formfdofields.jpg" colSpan="3" align="right">
																					<asp:panel id="panAsocFil" runat="server" cssclass="titulo" Width="100%" visible="false">
																						<TABLE style="WIDTH: 100%" id="TablaAsocFil" border="0" cellPadding="0" align="left">
																							<TR>
																								<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																							</TR>
																							<TR>
																								<TD style="WIDTH: 20%; HEIGHT: 21px" vAlign="middle" align="right">
																									<asp:Label id="lblAsocFil" runat="server" cssclass="titulo" Visible="true">Asociación:</asp:Label>&nbsp;</TD>
																								<TD vAlign="middle">
																									<cc1:combobox id="cmbAsocFil" class="combo" runat="server" cssclass="cuadrotexto" Width="280px"
																										filtra="true" NomOper="asociaciones_cargar" MostrarBotones="False" Height="19px" AutoPostBack="False"></cc1:combobox></TD>
																							</TR>
																						</TABLE>
																					</asp:panel></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="10"></TD>
													</TR>
													<TR>
														<TD height="10"></TD>
													</TR>
													<TR>
														<TD vAlign="top" align="center">
															<asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
																CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="plba_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="plba_presen_fecha" ReadOnly="True" HeaderText="Fec. Present." DataFormatString="{0:dd/MM/yyyy}">
																		<HeaderStyle Width="15%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="plba_alta_fecha" ReadOnly="True" HeaderText="Fec. Ingreso" DataFormatString="{0:dd/MM/yyyy}">
																		<HeaderStyle Width="15%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn Visible="False" DataField="plba_raza_id" ReadOnly="True" HeaderText="plba_raza_id"
																		HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_raza" ReadOnly="True" HeaderText="Raza">
																		<HeaderStyle Width="30%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn Visible="False" DataField="plba_cria_id" ReadOnly="True" HeaderText="plba_cria_id"
																		HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_criador" ReadOnly="True" HeaderText="Criador" ItemStyle-HorizontalAlign="Left">
																		<HeaderStyle Width="40%"></HeaderStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD height="10"></TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="TablaAgre" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
																<TR>
																	<TD colSpan="2" align="left">
																		<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																			IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																			ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ForeColor="Transparent" ImageDisable="btnNuev0.gif"
																			ToolTip="Argerar un Nuevo Registro"></CC1:BOTONIMAGEN></TD>
																	<TD align="right">
																		<CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"
																			ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="10"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" Height="124%" width="100%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False" Font-Bold="True"> General</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDetalle" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Detalle</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
													</TR>
												</TABLE>
											</asp:panel>
										</TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" Height="116px" width="100%">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha Ingreso:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				obligatorio="false"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblFechaPresen" runat="server" cssclass="titulo">Fecha Presentación:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtFechaPresen" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 25px" align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg" colSpan="2" align="left">
																			<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="280px" filtra="true"
																				NomOper="razas_cargar" MostrarBotones="False" Height="19px" AutoPostBack="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<UC1:CLIE id="usrCria" runat="server" Alto="560" FilClie="True" MuestraDesc="False" FilCUIT="True"
																				Ancho="780" FilDocuNume="True" Saltos="1,1,1" Tabla="Criadores"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formfdofields.jpg" colSpan="3" align="right">
																			<asp:panel id="panAsoc" runat="server" cssclass="titulo" Width="100%" visible="true">
																				<TABLE style="WIDTH: 100%" id="TablaAsociacion" border="0" cellPadding="0" align="left">
																					<TR>
																						<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 20%; HEIGHT: 21px" vAlign="middle" align="right">
																							<asp:Label id="lblAsociacion" runat="server" cssclass="titulo" Visible="true">Asociación:</asp:Label>&nbsp;</TD>
																						<TD vAlign="middle">
																							<cc1:combobox id="cmbAsoc" class="combo" runat="server" cssclass="cuadrotexto" Width="280px" Visible="true"
																								filtra="true" NomOper="asociaciones_cargar" MostrarBotones="False" Height="19px" AutoPostBack="False"></cc1:combobox></TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TablaDeta" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdDeta" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosDeta" OnPageIndexChanged="DataGridDeta_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%" PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="pbde_id" ReadOnly="True" HeaderText="pbde_id"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pbde_sexo" ReadOnly="True" HeaderText="pbde_sexo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_sexo" ReadOnly="True" HeaderText="Sexo">
																						<HeaderStyle Width="5%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pbde_regt_id" ReadOnly="True" HeaderText="pbde_regi_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_regt_desc" ReadOnly="True" HeaderText="Reg.">
																						<HeaderStyle Width="5%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pbde_px_id" ReadOnly="True" HeaderText="pbde_px_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_px_desc" ReadOnly="True" HeaderText="PX">
																						<HeaderStyle Width="5%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="pbde_rp" ReadOnly="True" HeaderText="RP"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pbde_pref_id" ReadOnly="True" HeaderText="pbde_pref_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_pref" ReadOnly="True" HeaderText="Prefijo">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="pbde_nomb" ReadOnly="True" HeaderText="Nombre">
																						<HeaderStyle Width="30%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="pbde_naci_fecha" ReadOnly="True" HeaderText="Fec. Nacim." DataFormatString="{0:dd/MM/yyyy}">
																						<HeaderStyle Width="10%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pbde_pela_id" ReadOnly="True" HeaderText="pbde_pela_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_pelaje" ReadOnly="True" HeaderText="Pelaje">
																						<HeaderStyle Width="10%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pbde_vari_id" ReadOnly="True" HeaderText="pbde_vari_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_variedad" ReadOnly="True" HeaderText="Variedad">
																						<HeaderStyle Width="10%"></HeaderStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbSexo" class="combo" runat="server" Width="100px">
																				<asp:ListItem Value="" Selected="True">(Seleccione)</asp:ListItem>
																				<asp:ListItem Value="1">Macho</asp:ListItem>
																				<asp:ListItem Value="0">Hembra</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblRegi" runat="server" cssclass="titulo">Registro:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbRegi" class="combo" runat="server" Width="100px" NomOper="rg_registros_tipos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblPX" runat="server" cssclass="titulo">Px:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbPX" class="combo" runat="server" Width="100px" NomOper="rg_px_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">RP:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<CC1:TEXTBOXTAB onblur="javascript:mDeterminarRP(this,'txtRPnume');" id="txtRP" runat="server" cssclass="cuadrotexto"
																				Width="150px"></CC1:TEXTBOXTAB>&nbsp;
																			<cc1:numberbox id="txtRPnume" runat="server" cssclass="cuadrotexto" Width="100px" Enabled="False"
																				MaxLength="12" MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblPrefijo" runat="server" cssclass="titulo">Prefijo:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbPref" class="combo" runat="server" Width="100px" NomOper="prefijos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblNombre" runat="server" cssclass="titulo">Nombre:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:textboxtab id="txtNombre" runat="server" cssclass="textolibre" Width="75%"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblFechaNaci" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:DateBox id="txtFechaNaci" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblAsoc" runat="server" cssclass="titulo" Visible="False">Asociación:</asp:Label></TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbAsocId" class="combo" runat="server" Width="340px" Visible="False" AceptaNull="True"
																				NomOper="rg_pelajes_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Nro. Asociación:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<CC1:NUMBERBOX id="txtAsocNume" runat="server" cssclass="cuadrotexto" Width="100px" MaxLength="12"
																				MaxValor="9999999999999" esdecimal="False"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblPelaje" runat="server" cssclass="titulo">Pelaje:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbPelaje" class="combo" runat="server" Width="208px" AceptaNull="True" NomOper="rg_pelajes_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblVari" runat="server" cssclass="titulo">Variedad:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbVari" class="combo" runat="server" Width="100px" NomOper="rg_variedades_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblProductoFil" runat="server" cssclass="titulo">Padre:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<UC1:PROD id="usrPadre" runat="server" AceptaNull="false" MuestraDesc="True" Ancho="800" FilDocuNume="True"
																				Saltos="1,2" Tabla="productos" AutoPostBack="False" FilTipo="S" FilSociNume="True"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Madre:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<UC1:PROD id="usrMadre" runat="server" AceptaNull="false" MuestraDesc="True" Ancho="800" FilDocuNume="True"
																				Saltos="1,2" Tabla="productos" AutoPostBack="False" FilTipo="S" FilSociNume="True"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBajaDeta" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Visible="False" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
			if (document.all["editar"]!= null)
				document.location='#editar';
				
			function usrCria_onchange()
			{
				try 
				{
					if (document.all('usrCria:cmbRazaCria').value!='')
					{
		   				mCargarPrefijos();
					}
				
				} catch(e){ }
			}			
			
			function mCargarPrefijos()
			{
				if (document.all('usrCria:txtId').value!='')
				{
					LoadComboXML("prefijos_cargar", document.all('usrCria:txtId').value, "cmbPref", "S");
				}
				else
				{
					for(i=document.all('cmbPref').length;i>-1;i--)
						document.all('cmbPref').remove(i);
				}
			}
					
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
