<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EstaCambio" CodeFile="EstaCambio.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cambio de Estado</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mConfirmarSociosRela()
		{
			var lstrFiltros = document.all('usrSoci:txtId').value + ',' + document.all('cmbEsta').value;
			var strRet = LeerCamposXML("socios_dependientes", lstrFiltros, "socios");
			if (strRet == 'S')
			{
				if (confirm('Existen socios familiares dependientes del socio a dar de baja que se encuentran en estado vigente, los mismos pasarán a Categoria en Revision.\nConfirma el cambio de estado?'))
					return(true);
				else
					return(false);
			}
			else
				return(true);
		}
		function mEstado(pEsta)
		{
			if (pEsta.value == '')			
			{
				document.all('lblReqApro').innerHTML = '';
			}
			else
			{
				var lstrApro = LeerCamposXML("estados", pEsta.value, "esta_reque_apro").split("|");					
				if (lstrApro == '1')
					document.all('lblReqApro').innerHTML = '(Requiere Aprobación)';
				else
					document.all('lblReqApro').innerHTML = '(Cambio Automático)';
			}
		}
		function usrSoci_onchange()
		{
			if(document.all("usrSoci:txtId").value=='')
			{
				document.all('lblFechaAnterior').innerHTML = '';
				document.all('lblEstadoAnterior').innerHTML = '';
			}
			else
			{
				var sFiltro = document.all("usrSoci:txtId").value;
				var vsRet;
				vsRet = EjecutarMetodoXML("Utiles.DatosEstaSocio", sFiltro).split(";");
				document.all('lblFechaAnterior').innerHTML = vsRet[0];
				document.all('lblEstadoAnterior').innerHTML = vsRet[1];
			}
		}
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function btnHisto_click()
		{
			var lstrPagina = "SociMovimConsul.aspx?tipo=" + document.all("hdnTipo").value + "&soci_id=" + document.all("hdnId").value;
			Ventana = window.open (lstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=300px,height=300px");
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="javascript:gSetearTituloFrame();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Solicitud de Cambio de Estado</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																	ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																	ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	BackColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																	OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblSociFil" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrSociFil" runat="server" FilCUIT="True" PermiModi="True" width="100%" FilDocuNume="True"
																				Ancho="790" MuestraDesc="False" FilSociNume="True" Saltos="1,1,1" Tabla="Socios" FilClieNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblEstaFil" runat="server" cssclass="titulo">Estado Solicitud:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbEstaFil" runat="server" Width="147px" Obligatorio="True">
																				<asp:ListItem Value="T" Selected="True">(Todas)</asp:ListItem>
																				<asp:ListItem Value="A">Aprobadas</asp:ListItem>
																				<asp:ListItem Value="P">Pendientes</asp:ListItem>
																				<asp:ListItem Value="R">Rechazadas</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
										OnPageIndexChanged="DataGrid_Page">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" CausesValidation="false" Text="Editar" CommandName="Update">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="socm_id" HeaderText="socm_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_automatico" HeaderText="tipo"></asp:BoundColumn>
											<asp:BoundColumn DataField="socm_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
											<asp:BoundColumn DataField="_socio" HeaderText="Apellido"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado_categoria" HeaderText="Estado"></asp:BoundColumn>
											<asp:BoundColumn DataField="_solicitud" HeaderText="Solicitud"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="left" colSpan="3"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" IncludesUrl="includes/"
										ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
										BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Solicitud" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3"><a name="editar"></a>
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" width="100%" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 129px" vAlign="top" align="right">
																			<asp:Label id="lblNume" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
																		<TD width="100%" colSpan="3">
																			<UC1:CLIE id="usrSoci" runat="server" FilCUIT="True" PermiModi="True" width="100%" FilDocuNume="True"
																				MuestraDesc="False" FilSociNume="True" Saltos="1,1,1" Tabla="Socios" FilClieNume="True"
																				SoloBusq="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="4"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD noWrap align="right">
																			<asp:Label id="lblEstaActu" runat="server" cssclass="titulo">Estado Actual:</asp:Label>&nbsp;
																		</TD>
																		<TD style="WIDTH: 191px" noWrap>
																			<asp:Label id="lblEstadoAnterior" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																		<TD align="right">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD align="left">
																			<asp:Label id="lblFechaAnterior" runat="server" cssclass="titulo" Width="100px"></asp:Label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 36px" vAlign="top" noWrap align="right">
																			<asp:Label id="lblNuevEsta" runat="server" cssclass="titulo">Nuevo Estado:</asp:Label>&nbsp;
																		</TD>
																		<TD style="WIDTH: 191px; HEIGHT: 36px" vAlign="top">
																			<cc1:combobox class="combo" id="cmbEsta" runat="server" Width="130px" Obligatorio="True" onchange="mEstado(this);"></cc1:combobox><BR>
																			<asp:Label id="lblReqApro" runat="server" cssclass="titulo"></asp:Label></TD>
																		<TD style="HEIGHT: 36px" vAlign="top" align="right">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 36px" vAlign="top" align="left">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																				AceptaNull="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="4"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 129px" noWrap align="right">
																			<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado Solicitud:</asp:Label>&nbsp;</TD>
																		<TD colSpan="3">
																			<asp:Label id="txtEsta" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="4"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 129px" vAlign="top" align="right">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo" Visible="True">Motivo:</asp:Label>&nbsp;
																		</TD>
																		<TD colSpan="3">
																			<CC1:TEXTBOXTAB id="txtMoti" runat="server" cssclass="cuadrotexto" Visible="True" Width="390px"
																				Height="50px" EnterPorTab="False" TextMode="MultiLine" MaxLength="200"></CC1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD vAlign="middle" align="center" colSpan="3" height="40"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="89px" CausesValidation="False"
																Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="89px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="89px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="89px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDiclId" runat="server"></asp:textbox><asp:textbox id="hdnTeclId" runat="server"></asp:textbox><asp:textbox id="hdnMaclId" runat="server"></asp:textbox><asp:textbox id="hdnTipo" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null && document.all["panDato"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		if (document.all('lblReqApro')!=null)
			mEstado(document.all('cmbEsta'));
		</SCRIPT>
	</BODY>
</HTML>
