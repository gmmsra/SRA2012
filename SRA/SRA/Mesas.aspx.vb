Namespace SRA

Partial Class Mesas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Mesas
   Private mstrMesaProf As String = SRA_Neg.Constantes.gTab_MesasProfe
   Private mstrMesaAlum As String = SRA_Neg.Constantes.gTab_MesasAlum

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet
   Private mintInse As Integer
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
      hdnInse.Text = mintInse
      usrAlum.FilInseId = mintInse
      hdnImprimir.Text = ""
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mSetearMaxLength()
            mSetearEventos()
            txtAnioFil.Valor = Today.Year

            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
            mRecuperarParametros()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMateFil, "T", "@mate_inse_id=" & mintInse.ToString)

      clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMate, "S", "@mate_inse_id=" & mintInse.ToString)
      clsWeb.gCargarRefeCmb(mstrConn, "profesores", cmbProf, "S", mintInse.ToString)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrMesaProf
            btnBajaProf.Enabled = Not (pbooAlta)
            btnModiProf.Enabled = Not (pbooAlta)
            btnAltaProf.Enabled = pbooAlta
         Case mstrMesaAlum
            btnBajaAlum.Enabled = Not (pbooAlta)
            btnModiAlum.Enabled = Not (pbooAlta)
            btnAltaAlum.Enabled = pbooAlta
            chkRecu.Enabled = pbooAlta
            chkGeneFact.Enabled = pbooAlta
            chkExceCC.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
            btnGeneLista.Disabled = not pbooAlta
      End Select
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      mCrearDataSet(pstrId)

      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("meex_id").ToString()
         txtFecha.Fecha = .Item("meex_fecha")
         txtDesc.Valor = .Item("meex_desc")
                If .IsNull("meex_comi_id") Then
                    rbtnCarr.Checked = False
                    rbtnMate.Checked = True
                    cmbMate.Valor = .Item("meex_mate_id")
                Else
                    rbtnMate.Checked = False
            rbtnCarr.Checked = True
            hdnSeleComi.Text = .Item("meex_comi_id") 
            hdnSeleCicl.Text = clsSQLServer.gCampoValorConsul(mstrConn, "comisiones_consul " & hdnSeleComi.Text, "comi_cicl_id")
            txtAnio.Text = clsSQLServer.gCampoValorConsul(mstrConn, "ciclos_consul " & hdnSeleCicl.Text, "cicl_anio")
            mRecuperarParametros()
            cmbComi.Valor = .Item("meex_comi_id")             
         End If
         If Not .IsNull("meex_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("meex_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      lblTitu.Text = "Registro Seleccionado: " + txtDesc.Text + " - " + cmbMate.SelectedItem.Text

      mSetearEditor(mstrMesaProf, True)
      mSetearEditor(mstrMesaAlum, True)
      mSetearEditor("", False)
      mConsultarAlum()
      mHabilitarMateria(grdAlum.Items.Count = 0)
      mLimpiarAlum()
      mLimpiarProf()
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""
      txtFecha.Text = ""
      txtDesc.Text = ""
      cmbMate.Limpiar()
      txtAnio.Text = ""
      cmbCicl.Items.Clear()
      cmbComi.Items.Clear()
      rbtnCarr.Checked = True
      hdnSeleCicl.Text = ""
      hdnSeleComi.Text = ""
      hdnSeleMate.Text = ""
      lblBaja.Text = ""

      grdProf.CurrentPageIndex = 0
      grdAlum.CurrentPageIndex = 0

      mCrearDataSet("")

      mLimpiarProf()
      mLimpiarAlum()
      mHabilitarMateria(True)

      mSetearEditor("", True)
      mShowTabs(1)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      Dim lbooVisiOri As Boolean = panDato.Visible

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()

         mShowTabs(1)
      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      tabLinks.Visible = pbooVisi

      If lbooVisiOri And Not pbooVisi Then
         cmbMateFil.Limpiar()
         'txtAnioFil.Valor = Today.Year
      End If
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panProf.Visible = False
      panAlum.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkProf.Font.Bold = False
      lnkAlum.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos de la Mesa"
         Case 2
            panProf.Visible = True
            lnkProf.Font.Bold = True
            lblTitu.Text = "Profesores de la Mesa: " & txtDesc.Text
            grdProf.Visible = True
         Case 3
            panAlum.Visible = True
            lnkAlum.Font.Bold = True
            lblTitu.Text = "Alumnos de la Mesa: " & txtDesc.Text
            grdAlum.Visible = True
            mHabilitarMateria(grdAlum.Items.Count = 0)
      End Select
   End Sub

   Private Sub mHabilitarMateria(ByVal pbooHabi As Boolean)
        rbtnCarr.Enabled = pbooHabi
        rbtnMate.Enabled = pbooHabi
        cmbMate.Enabled = pbooHabi
        cmbCicl.Enabled = pbooHabi
        cmbComi.Enabled = pbooHabi
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

   Private Sub lnkProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkProf.Click
      mShowTabs(2)
   End Sub

   Private Sub lnkalum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAlum.Click
      mShowTabs(3)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         'lobjGenericoRel.Alta()
         lobjGenericoRel.ActualizaMulRelDetaIds(mstrMesaAlum)
         hdnImprimir.Text = lobjGenericoRel.gstrMultiplesId

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         'lobjGenericoRel.Modi()
         lobjGenericoRel.ActualizaMulRelDetaIds(mstrMesaAlum)
         hdnImprimir.Text = lobjGenericoRel.gstrMultiplesId

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenericoRel.Baja(hdnId.Text)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatos()
      mValidarDatos()

      With mdsDatos.Tables(mstrTabla).Rows(0)
         .Item("meex_fecha") = txtFecha.Fecha
         .Item("meex_desc") = txtDesc.Valor
                .Item("meex_comi_id") = IIf(cmbComi.Valor = "", DBNull.Value, cmbComi.Valor)
                If cmbMate.Valor <> "0" Then
                    .Item("meex_mate_id") = IIf(cmbMate.Valor = "", DBNull.Value, cmbMate.Valor)
                Else
            .Item("meex_mate_id") = clsSQLServer.gCampoValorConsul(mstrConn, "comisiones_consul " + cmbComi.Valor, "comi_mate_id")
         End If
      End With

      'mGuardarDatosComprobantes()

   End Sub

   Private Sub mGuardarDatosComprobantes()
     Dim lstrActiId As String = clsSQLServer.gCampoValorConsul(mstrConn, "institutos_consul @inse_id=" + mintInse.ToString(), "inse_acti_id")
     Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
     oFact.CentroEmisorNro(mstrConn)
     Dim lstrCtroEmiNro As String = oFact.pCentroEmisorNro
     Dim lstrCtroEmiId As String = oFact.pCentroEmisorId
     Dim lstrHost As String = oFact.pHost
     Dim lstrFechaVto As String = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, lstrActiId, Today)
     For Each odrDetaInte As DataRow In mdsDatos.Tables(mstrMesaAlum).Select()
        odrDetaInte.Item("gene_fact") = IIf(chkGeneFact.Checked, "S", "N")
        odrDetaInte.Item("proc_cemi_nume") = lstrCtroEmiNro
        odrDetaInte.Item("proc_emct_id") = lstrCtroEmiId
        odrDetaInte.Item("proc_host") = lstrHost
        odrDetaInte.Item("proc_vcto_fecha") = lstrFechaVto
        If chkExceCC.Checked Then
            odrDetaInte.Item("proc_cpti_id") = 2
        Else
            odrDetaInte.Item("proc_cpti_id") = 1
        End If
     Next
   End Sub

   Private Sub mRecuperarParametros()
        Dim lstrFiltros As String = ""
        Dim lstrCicl As String = hdnSeleCicl.Text
        Dim lstrComi As String = hdnSeleComi.Text
        txtMate.Text = ""
        If rbtnCarr.Checked And txtAnio.Text <> "" Then

            'ciclos
            lstrFiltros = mintInse & ","
            If txtAnio.Text = "" Then
                lstrFiltros = lstrFiltros & "-1"
            Else
                lstrFiltros = lstrFiltros & txtAnio.Text
            End If
            clsWeb.gCargarCombo(mstrConn, "ciclos_cargar " & lstrFiltros, cmbCicl, "id", "descrip", "T")

            'comisiones y materias
            lstrFiltros = "@mate_inse_id=" & mintInse
            If lstrCicl <> "" Then
                lstrFiltros = lstrFiltros + ",@cicl_id=" & lstrCicl
                clsWeb.gCargarCombo(mstrConn, "comisiones_cargar @cicl_id=" & lstrCicl, cmbComi, "id", "descrip", "T")
            End If
            cmbCicl.Valor = lstrCicl
            cmbComi.Valor = lstrComi

            If lstrComi <> "" Then
                txtMate.Text = clsSQLServer.gCampoValorConsul(mstrConn, "comisiones_consul " + lstrComi, "_mate_desc")
            End If
        End If
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
      If txtMate.Text = "" And cmbMate.Valor = "0" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la materia.")
      End If
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrMesaProf
      mdsDatos.Tables(2).TableName = mstrMesaAlum

      With mdsDatos.Tables(mstrTabla).Rows(0)
         If .IsNull("meex_id") Then
            .Item("meex_id") = -1
         End If
      End With

      mConsultarProf()

      mConsultarAlum()

      Session(mstrTabla) = mdsDatos
   End Sub

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub



   Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
      Try
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @mate_inse_id=" + mintInse.ToString)
         lstrCmd.Append(", @anio=" + clsSQLServer.gFormatArg(txtAnioFil.Text, SqlDbType.Int))
         lstrCmd.Append(", @meex_mate_id=" + cmbMateFil.Valor.ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mLimpiar()
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Detalle"
   Public Sub mEditarDatosProf(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrMepr As DataRow

         hdnMeprId.Text = E.Item.Cells(1).Text
         ldrMepr = mdsDatos.Tables(mstrMesaProf).Select("mepr_id=" & hdnMeprId.Text)(0)

         With ldrMepr
            cmbProf.Valor = .Item("mepr_prof_id")
         End With

         mSetearEditor(mstrMesaProf, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosAlum(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrAlum As DataRow

         hdnMealId.Text = E.Item.Cells(1).Text
         ldrAlum = mdsDatos.Tables(mstrMesaAlum).Select("meal_id=" & hdnMealId.Text)(0)

         With ldrAlum
            usrAlum.Valor = .Item("meal_alum_id")
         End With

         mSetearEditor(mstrMesaAlum, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Public Sub grdProf_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdProf.EditItemIndex = -1
         If (grdProf.CurrentPageIndex < 0 Or grdProf.CurrentPageIndex >= grdProf.PageCount) Then
            grdProf.CurrentPageIndex = 0
         Else
            grdProf.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarProf()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdalum_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdAlum.EditItemIndex = -1
         If (grdAlum.CurrentPageIndex < 0 Or grdAlum.CurrentPageIndex >= grdAlum.PageCount) Then
            grdAlum.CurrentPageIndex = 0
         Else
            grdAlum.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarAlum()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarProf()
      Try
         mGuardarDatosMepr()

         mLimpiarProf()
         mConsultarProf()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mActualizarAlum()
      Try
         mGuardarDatosAlum()

         mLimpiarAlum()
         mConsultarAlum()
         mHabilitarMateria(grdAlum.Items.Count = 0)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


        Private Sub mGuardarDatosMepr()

            Dim ldrDatos As DataRow

            If cmbProf.Valor Is DBNull.Value Or cmbProf.Valor.Trim.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Profesor.")
            End If


            If mdsDatos.Tables(mstrMesaProf).Select("mepr_prof_id=" & cmbProf.Valor & " AND mepr_id <> " & clsSQLServer.gFormatArg(hdnMeprId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Profesor-Tipo existente.")
            End If

            If hdnMeprId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrMesaProf).NewRow
                ldrDatos.Item("mepr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrMesaProf), "mepr_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrMesaProf).Select("mepr_id=" & hdnMeprId.Text)(0)
            End If

            With ldrDatos
                .Item("mepr_prof_id") = cmbProf.Valor
                .Item("_prof_nyap") = cmbProf.SelectedItem.Text
            End With

            If hdnMeprId.Text = "" Then
                mdsDatos.Tables(mstrMesaProf).Rows.Add(ldrDatos)
            End If

        End Sub

   Private Sub mGuardarDatosAlum()
      Dim ldrDatos As DataRow

      Dim lstrActiId As String = clsSQLServer.gCampoValorConsul(mstrConn, "institutos_consul @inse_id=" + mintInse.ToString(), "inse_acti_id")

      If txtMate.Text = "" And cmbMate.Valor = "0" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la materia.")
      End If

      If usrAlum.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el alumno.")
      End If

      If mdsDatos.Tables(mstrMesaAlum).Select("meal_alum_id=" & usrAlum.Valor & " AND meal_id <> " & clsSQLServer.gFormatArg(hdnMealId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("Alumno existente.")
      End If

      Dim lstrResu As String = ""
      Dim lstrMate As String = ""
      If cmbMate.Valor <> "0" Then
         lstrMate = cmbMate.Valor
      Else
         lstrMate = clsSQLServer.gCampoValorConsul(mstrConn, "comisiones_consul " + cmbComi.Valor, "comi_mate_id")
      End If
      If lstrMate <> "" Then
         lstrResu = clsSQLServer.gCampoValorConsul(mstrConn, "alumnos_correl_verificar " & usrAlum.Valor & "," & lstrMate, "correl")
         If lstrResu = "S" Then
            Throw New AccesoBD.clsErrNeg("El alumno posee materias correlativas sin aprobar.")
         End If
      End If

      If hdnMealId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrMesaAlum).NewRow
         ldrDatos.Item("meal_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrMesaAlum), "meal_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrMesaAlum).Select("meal_id=" & hdnMealId.Text)(0)
      End If

      With ldrDatos
         .Item("meal_alum_id") = usrAlum.Valor

         .Item("_alum_lega") = usrAlum.Codi
         .Item("_alum_desc") = usrAlum.Apel

         .Item("gene_fact") = IIf(chkGeneFact.Checked, "S", "N")
         Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
         oFact.CentroEmisorNro(mstrConn)
         .Item("proc_cemi_nume") = oFact.pCentroEmisorNro
         .Item("proc_emct_id") = oFact.pCentroEmisorId
         .Item("proc_host") = oFact.pHost
         .Item("proc_vcto_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, lstrActiId, Today)
         If chkExceCC.Checked Then
            .Item("proc_cpti_id") = 2
         Else
            .Item("proc_cpti_id") = 1
         End If
      End With

      If hdnMealId.Text = "" Then
         mdsDatos.Tables(mstrMesaAlum).Rows.Add(ldrDatos)
      End If
   End Sub

   Private Sub mLimpiarProf()
      hdnMeprId.Text = ""
      cmbProf.Limpiar()

      mSetearEditor(mstrMesaProf, True)
   End Sub

   Private Sub mLimpiarAlum()
      hdnMealId.Text = ""
      usrAlum.Limpiar()
      chkRecu.Checked = False
      chkGeneFact.Checked = False
      chkExceCC.Checked = False 
      mSetearEditor(mstrMesaAlum, True)
   End Sub


   Private Sub btnLimpalum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpAlum.Click
      mLimpiarAlum()
   End Sub

   Private Sub btnLimpProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpProf.Click
      mLimpiarProf()
   End Sub

   Private Sub btnBajaProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaProf.Click
      Try
         mdsDatos.Tables(mstrMesaProf).Select("mepr_id=" & hdnMeprId.Text)(0).Delete()
         mConsultarProf()
         mLimpiarProf()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaalum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaAlum.Click
      Try
         Dim lstrResu as String = ""

         If hdnId.Text <> "" Then
            lstrResu = clssqlserver.gCampoValorConsul(mstrconn, "mesas_alumnos_verificar " & usrAlum.Valor & "," & hdnId.Text, "cali")
            If lstrResu = "S" Then
                Throw New AccesoBD.clsErrNeg("El alumno ya posee una calificación en la mesa de examen, no puede ser eliminado.")
            End If
         End If

         If mdsDatos.Tables(mstrMesaAlum).Select("meal_alum_id=" & usrAlum.Valor & " and _comp_id is not null").Length > 0 Then
            Throw New AccesoBD.clsErrNeg("No se pueden eliminar alumnos con facturas vigentes para la mesa.")
         End If

         mdsDatos.Tables(mstrMesaAlum).Select("meal_id=" & hdnMealId.Text)(0).Delete()
         mConsultarAlum()

         mHabilitarMateria(grdAlum.Items.Count = 0)

         mLimpiarAlum()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaProf.Click
      mActualizarProf()
   End Sub

   Private Sub btnAltaAlum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAlum.Click
      mActualizarAlum()
   End Sub

   Private Sub btnModiProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiProf.Click
      mActualizarProf()
   End Sub

   Private Sub btnModialum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiAlum.Click
      mActualizarAlum()
   End Sub

   Public Sub mConsultarProf()
      grdProf.DataSource = mdsDatos.Tables(mstrMesaProf)
      grdProf.DataBind()
   End Sub

   Public Sub mConsultarAlum()
      grdAlum.DataSource = mdsDatos.Tables(mstrMesaAlum)
      grdAlum.DataBind()
   End Sub
#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "MesasExamen"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&meex_id=" + hdnId.Text

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
      Try
         If (hdnDatosPop.Text <> "") Then
            Dim lvrDatos() As String = hdnDatosPop.Text.Split(Chr(5))
            Dim lvrDato() As String
            Dim ldrDatos As DataRow
            Dim ldrDeta As DataRow

            'Dim lstrActiId As String = clsSQLServer.gCampoValorConsul(mstrConn, "institutos_consul @inse_id=" + mintInse.ToString(), "inse_acti_id")
            'Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
            'oFact.CentroEmisorNro(mstrConn)
            'Dim lstrCtroEmiNro As String = oFact.pCentroEmisorNro
            'Dim lstrCtroEmiId As String = oFact.pCentroEmisorId
            'Dim lstrHost As String = oFact.pHost
            'Dim lstrFechaVto As String = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, lstrActiId, Today)

            For i As Integer = 0 To lvrDatos.GetUpperBound(0)
               lvrDato = lvrDatos(i).Split(Chr(6))
               If mdsDatos.Tables(mstrMesaAlum).Select("meal_alum_id=" + lvrDato(0)).GetUpperBound(0) = -1 Then
                  ldrDatos = mdsDatos.Tables(mstrMesaAlum).NewRow
                  ldrDatos.Item("meal_id") = clsSQLServer.gObtenerId(ldrDatos.Table, "meal_id")
                  With ldrDatos
                     .Item("meal_alum_id") = lvrDato(0)
                     .Item("_alum_lega") = lvrDato(1)
                     .Item("_alum_desc") = lvrDato(2)
                     .Item("gene_fact") = "N"
                     '.Item("proc_cemi_nume") = lstrCtroEmiNro
                     '.Item("proc_emct_id") = lstrCtroEmiId
                     '.Item("proc_host") = lstrHost
                     '.Item("proc_vcto_fecha") = lstrFechaVto
                     'If chkExceCC.Checked Then
                     '   .Item("proc_cpti_id") = 2
                     'Else
                     '   .Item("proc_cpti_id") = 1
                     'End If
                  End With
                  ldrDatos.Table.Rows.Add(ldrDatos)
               End If
            Next
            hdnDatosPop.Text = ""
            mConsultarAlum()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub
End Class
End Namespace
