<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ChequesDisponibles_Pop" CodeFile="ChequesDisponibles_Pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cheques a Depositar</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<tr>
					<TD style="WIDTH: 15%; HEIGHT: 17px" align="right">
						<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Valores al:</asp:Label>&nbsp;</TD>
					<TD style="WIDTH: 85%; HEIGHT: 17px">
						<cc1:DateBox id="txtFechaFil" runat="server" AutoPostBack="True" cssclass="cuadrotexto" Width="70px"
							AceptaNull="True"></cc1:DateBox></TD>
				</tr>
				<tr>
					<TD style="HEIGHT: 15px" colspan="2"></TD>
				</tr>
				<TR height="100%">
					<TD vAlign="top" colSpan="2">
						<asp:datagrid id="grdConsulta" runat="server" width="100%" BorderStyle="None" AllowPaging="true"
							BorderWidth="1px" HorizontalAlign="Center" OnPageIndexChanged="DataGrid_Page" CellSpacing="1"
							GridLines="None" CellPadding="1" PageSize="10" ItemStyle-Height="5px" AutoGenerateColumns="False">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
									<ItemTemplate>
										<asp:CheckBox ID="chkSel" Checked=<%#DataBinder.Eval(Container, "DataItem._chk")%> Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="False" DataField="_cheq_id"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="_banc_id"></asp:BoundColumn>
								<asp:BoundColumn DataField="_banc_desc" HeaderText="Banco">
									<HeaderStyle Width="50%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="_cheq_nume" HeaderText="N� Cheque">
									<HeaderStyle Width="20%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="_cheq_teor_depo_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
								<asp:BoundColumn DataField="_cheq_impo" ItemStyle-HorizontalAlign="Right" HeaderText="Importe"></asp:BoundColumn>
								<asp:BoundColumn DataField="_clea_desc" HeaderText="Clearing"></asp:BoundColumn>
								<asp:BoundColumn DataField="_diferido" HeaderText="Diferido"></asp:BoundColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 100%" align="right" colspan="2">
						<asp:button id="btnAceptar" runat="server" cssclass="boton" Width="80px" Text="Aceptar"></asp:button>&nbsp;
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="lblCentId" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
