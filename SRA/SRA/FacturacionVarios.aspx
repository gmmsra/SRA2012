<%@ Register TagPrefix="uc1" TagName="CLIE" Src="~/controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%--<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FacturacionVarios.aspx.vb" Inherits="SRA.FacturacionVarios" %>--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/FacturacionVarios.aspx.vb" Inherits="FacturacionVarios" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Facturaci�n Varios </title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<SCRIPT language="javascript">
		function expandir()
		{
		 parent.frames("menu").CambiarExp();
		}
		var vVentanas = new Array(null,null,null,null,null,null,null);
		
		function btnDA_click()
		{
			mAbrirVentanas("Alertas_pop.aspx?, 3, 700,300,100,100");
		}
		function mAbrirVentanas(pstrPagina, pintOrden, pstrAncho, pstrAlto, pstrLeft, pstrTop)
		{
			vVentanas[pintOrden] = window.open (pstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=" + pstrAncho + "px,height=" + pstrAlto + "px,left=" + pstrLeft + "px,top=" + pstrTop + "px");
			
			for(i=0;i<7;i++)
			{
				if(vVentanas[i]!=null)
				{
					try{vVentanas[i].focus();}
					catch(e){vVentanas[i];}
					finally{;}
				}
			}
			
			vVentanas[pintOrden].focus();
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Facturaci�n Varios</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="97%" BorderStyle="Solid"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																	ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																	ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 30%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblActiFil" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 68%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbActiFil" runat="server" Width="180px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 30%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 68%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 30%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 68%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" width="68px" Obligatorio="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 30%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 68%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 30%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 68%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" Saltos="1,1,1,1" Ancho="800" FilFanta="true"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 30%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 68%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 30%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblTipoComp" runat="server" cssclass="titulo">Tipo de comprobante:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 68%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTipoComp" runat="server" Width="180px" AceptaNull="False"></cc1:combobox>&nbsp;
																			<asp:label id="lblNro" runat="server" cssclass="titulo">N�mero:</asp:label>&nbsp;
																			<cc1:numberbox id="txtCEmi" runat="server" cssclass="cuadrotexto" Width="40px" Enabled="False"></cc1:numberbox>
																			<asp:label id="lblSepa" runat="server" cssclass="titulo">-</asp:label>
																			<cc1:numberbox id="txtNro" runat="server" cssclass="cuadrotexto" Width="80px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 30%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 68%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 2px" align="right" background="imagenes/formdivfin.jpg"
																			height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 30%; HEIGHT: 2px" align="right" background="imagenes/formdivfin.jpg"
																			height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 38%; HEIGHT: 2px" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="False"
										AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
										OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="comp_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_ingr_fecha" ReadOnly="True" HeaderText="Fecha Emisi&#243;n">
												<HeaderStyle Width="20%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="comp_fecha" ReadOnly="True" HeaderText="Fecha Valor" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="20%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_clie_apel" ReadOnly="True" HeaderText="Cliente">
												<HeaderStyle Width="30%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_actividad" ReadOnly="True" HeaderText="Actividad">
												<HeaderStyle Width="30%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
													ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Alumno"
													ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="right"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD colSpan="2">
															<DIV id="divAlerta">
																<asp:panel id="panAlerta" runat="server" cssclass="titulo" width="20%" BorderStyle="Solid"
																	BorderWidth="1px">
																	<TABLE class="FdoFld" id="Table20" style="WIDTH: 100%; HEIGHT: 30px" cellPadding="0" align="left"
																		border="0">
																		<TR>
																			<TD>
																				<asp:ImageButton id="imgTarj" runat="server" ImageUrl="imagenes/Tarj.gif" ToolTip="Puede operar con tarjetas"
																					CausesValidation="False"></asp:ImageButton>
																				<asp:ImageButton id="imgAlertaSaldos" runat="server" ImageUrl="imagenes/CircRojo.bmp" ToolTip="Estado de saldos"
																					CausesValidation="False"></asp:ImageButton>
																				<asp:ImageButton id="ImageEspec" runat="server" ImageUrl="imagenes/CircTrianRojo.bmp" ToolTip="Especiales"
																					CausesValidation="False"></asp:ImageButton>
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel>
															</DIV>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
															<asp:label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
															<cc1:combobox class="combo" id="cmbActi" runat="server" Width="180px" AceptaNull="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
															<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
															<UC1:CLIE id="usrClie" runat="server" AceptaNull="false" Saltos="1,1,1,1" Ancho="800" FilFanta="true"></UC1:CLIE></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
															<asp:label id="lblFormaPago" runat="server" cssclass="titulo">Forma de pago:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
															<cc1:combobox class="combo" id="cmbFormaPago" runat="server" Width="180px" AceptaNull="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD></TD>
														<TD align="center">
															<DIV>
																<asp:panel id="panTarjeta" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid"
																	BorderWidth="1px" Visible="true" Height="90px">
																	<P align="right">
																		<TABLE class="FdoFld" id="tbltarj" style="WIDTH: 70%; HEIGHT: 90px" cellPadding="0" align="left"
																			border="0">
																			<TR>
																				<TD vAlign="top" align="right">
																					<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 60%">
																					<cc1:combobox class="combo" id="cmbTarj" runat="server" Width="200px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD vAlign="top" align="right">
																					<asp:Label id="lblTarjNro" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;
																				</TD>
																				<TD style="WIDTH: 60%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtTarjNro" runat="server" cssclass="cuadrotexto" Width="160px" EsTarjeta="true"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD vAlign="top" align="right">
																					<asp:Label id="lblTarjCuo" runat="server" cssclass="titulo">Cant. Cuotas:</asp:Label>&nbsp;
																				</TD>
																				<TD style="WIDTH: 60%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtTarjCuo" runat="server" cssclass="cuadrotexto" Width="160px"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD align="right"></TD>
																				<TD colSpan="2">
																					<asp:CheckBox id="chkVtaTele" Text="Venta Telef�nica" Runat="server" CssClass="titulo" Checked="false"></asp:CheckBox></TD>
																			</TR>
																		</TABLE>
																	</P>
																</asp:panel>
															</DIV>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
															<asp:label id="lblFechaIngr" runat="server" cssclass="titulo">Fecha Ingreso:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
															<asp:label id="lblFechaIng" runat="server" cssclass="titulo"></asp:label>&nbsp;
															<asp:label id="lblFechaValor" runat="server" cssclass="titulo">Fecha Valor:</asp:label>&nbsp;
															<cc1:DateBox id="txtFechaValor" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox>
															<asp:label id="lblFechaIva" runat="server" cssclass="titulo">Fecha Ref. I.V.A.:</asp:label>&nbsp;
															<cc1:DateBox id="txtFechaIva" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right"></TD>
														<TD colSpan="2">
															<asp:CheckBox id="chkTPers" Text="Tr�mite Personal" Runat="server" CssClass="titulo" Checked="True"></asp:CheckBox>&nbsp;&nbsp;
															<asp:label id="Label1" runat="server" cssclass="titulo">Convenio:</asp:label>&nbsp;
															<cc1:combobox class="combo" id="cmbConv" runat="server" Width="180px" AceptaNull="False"></cc1:combobox>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="center" colSpan="3">
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Grabar"></asp:Button>
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
			
		function ValorFijoChange()
		{
			if(window.event.srcElement.value!="")
				document.all["cmbTari"].selectedIndex=2;
			else
				document.all["cmbTari"].selectedIndex=1;
		}
		</SCRIPT>
	</BODY>
</HTML>
