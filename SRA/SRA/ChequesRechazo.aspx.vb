Namespace SRA

Partial Class ChequesRechazo
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

    '15/09/2010     Protected WithEvents txtTeorDepoFecha As NixorControls.DateBox






   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Cheques
   Private mstrTablaChequesMovim As String = SRA_Neg.Constantes.gTab_ChequesMovim
   Private mstrTablaAsiento As String = "rechazo_cheques_asiento"
   Private mstrTablaAsientoAlta As String = "rechazo_cheques_asiento"
   Private mstrTablaAsientoBaja As String = "rechazo_cheques_asiento_baja"
   Private mstrTablaND As String = "nota_debito"

   Private mstrConn As String
    Private mstrParaPageSize As Integer


   Private Enum Columnas As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      If hdnId.Text <> "" Then
         If btnBaja.Enabled = False Then
            mstrTablaAsiento = mstrTablaAsientoAlta
         Else
            mstrTablaAsiento = mstrTablaAsientoBaja
         End If
      End If
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mCargarCombos()
            'mConsultar()

                clsWeb.gInicializarControles(Me, mstrConn)

                If Session("sCentroEmisorCentral") <> "S" Then
                    btnBusc.Visible = False
                    btnModi.Enabled = False
                    grdDato.Enabled = False
                    cmbBanc.Enabled = False
                    cmbBancF.Enabled = False
                    Throw New AccesoBD.clsErrNeg("Opción No Disponible Solo Se Puede Ejecutar Desde Administración Central")
                End If

            End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmct, "@ori_emct_id=" + hdnEmctId.Text)
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancF, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbDepoBanc, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "cheques_tipos", cmbChti, "S")
    
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooRech As Boolean)
      btnModi.Enabled = Not (pbooRech)
      btnBaja.Enabled = pbooRech
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lbooRech As Boolean
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
         hdnId.Text = .Item("cheq_id").ToString()
         txtImpo.Valor = .Item("cheq_impo")
         cmbMone.Valor = .Item("cheq_mone_id")
         cmbEmct.Valor = .Item("cheq_emct_id")
         cmbBanc.Valor = .Item("cheq_banc_id")
         cmbDepoBanc.Valor = .Item("_cuba_banc_id")
         clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuba, "S", cmbDepoBanc.Valor)
         cmbCuba.Valor = .Item("_cuba_id")

         usrClie.Valor = .Item("_comp_clie_id")
         txtNume.Valor = .Item("cheq_nume")
         txtReceFecha.Fecha = .Item("cheq_rece_fecha")
            '15/09/2010     txtTeorDepoFecha.Fecha = .Item("cheq_teor_depo_fecha")
            txtCheqFecha.Fecha = .Item("cheq_fecha")
            cmbChti.Valor = .Item("cheq_chti_id")

                txtGstoImpo.Valor = .Item("cheq_gsto_impo").ToString

         If Not .IsNull("cheq_rech_fecha") Then
            lblBaja.Text = "Cheque rechazado en fecha: " & CDate(.Item("cheq_rech_fecha")).ToString("dd/MM/yyyy HH:mm")
            lbooRech = True
                txtGstoImpo.Enabled = False
                txtFechaComp.Fecha = .Item("cheq_rech_fecha")
            Else
                lblBaja.Text = ""
                txtGstoImpo.Enabled = True
                    txtFechaComp.Fecha = Today
                End If
        End With

      mSetearEditor("", lbooRech)
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()

      hdnImprimio.Text = ""
      hdnImprimir.Text = ""
      hdnImpriTipo.Text = ""

      hdnId.Text = ""
      lblTitu.Text = ""

      txtImpo.Text = ""
      cmbBanc.Limpiar()

      txtNume.Text = ""
      txtReceFecha.Text = ""
        '15/09/2010         txtTeorDepoFecha.Text = ""
        txtCheqFecha.Text = ""
      cmbDepoBanc.Limpiar()
      cmbCuba.Items.Clear()
      cmbChti.Limpiar()
      cmbMone.Limpiar()
      cmbEmct.Limpiar()
            txtFechaComp.Fecha = Today
            mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mModi()
      Try
         Dim lstrId As String
         Dim ldsEstruc As DataSet = mGuardarDatos(True)

         Dim lobjNeg As New SRA_Neg.Cheques(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lstrId = lobjNeg.RechazarCheques(True)
         hdnImprimir.Text = lstrId

         Dim lDsAler As DataSet
         lDsAler = clsSQLServer.gObtenerEstruc(mstrConn, "alertas", "@aler_alti_id=" & SRA_Neg.Constantes.AlertasTipos.RECHAZO_DE_CHEQUES)
         If lDsAler.Tables(0).Rows.Count > 0 Then
             SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), lDsAler.Tables(0).Rows(0).Item("aler_id"), clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.VarChar))
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lstrId As String
         Dim ldsEstruc As DataSet = mGuardarDatos(False)

         Dim lobjNeg As New SRA_Neg.Cheques(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lstrId = lobjNeg.RechazarCheques(False)
         hdnImprimir.Text = lstrId

         'Dim lDsAler As DataSet
         'lDsAler = clsSQLServer.gObtenerEstruc(mstrConn, "alertas", "@aler_alti_id=" & SRA_Neg.Constantes.AlertasTipos.RECHAZO_DE_CHEQUES)
         'If lDsAler.Tables(0).Rows.Count > 0 Then
         '    SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), lDsAler.Tables(0).Rows(0).Item("aler_id"), clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.VarChar))
         'End If

         If hdnImpriTipo.Text = "" Then
            mConsultar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos(ByVal pbooEsAlta As Boolean) As DataSet
      Dim ldsDatos As DataSet
      Dim ldrComp, ldrCode, ldrCoco, ldrCovt, ldrAsie, ldrND As DataRow
      Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
      Dim lstrHost, lstrCemiNume, lstrEmctId As String
      Dim lDrCheq As DataRow
      Dim lDrPaco As DataRow
      Dim lDrMovim As DataRow
      Dim lstrRechaFecha As String = ""

      mValidarDatos()


      ldsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, "")

      SRA_Neg.Utiles.gAgregarTabla(mstrConn, ldsDatos, mstrTabla, hdnId.Text)
      SRA_Neg.Utiles.gAgregarTabla(mstrConn, ldsDatos, mstrTablaChequesMovim)

      'determinar la fecha de anulacion
      lDrPaco = SRA_Neg.Utiles.gAgregarRegistro(mstrConn, ldsDatos.Tables(mstrTabla), hdnId.Text)
      If Not pbooEsAlta Then
         lstrRechaFecha = lDrPaco.Item("cheq_rech_fecha")
      End If

      ldsDatos.Tables.Add(mstrTablaAsiento)
      With ldsDatos.Tables(mstrTablaAsiento)
         .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_cheq_id", System.Type.GetType("System.Int32"))

         'If pbooEsAlta And Not (usrClie.Valor Is DBNull.Value) Then
            .Columns.Add("proc_comp_id", System.Type.GetType("System.Int32"))
         'End If

         .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
      End With

      ldsDatos.Tables.Add(mstrTablaND)
      With ldsDatos.Tables(mstrTablaND)
         .Columns.Add("comp_id", System.Type.GetType("System.Int32"))
         .Columns.Add("comp_baja_fecha", System.Type.GetType("System.DateTime"))
         .Columns.Add("comp_audi_user", System.Type.GetType("System.Int32"))
      End With

      ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Rows(0).Delete()

      If (pbooEsAlta Or (Not pbooEsAlta And lstrRechaFecha <> Date.Today.ToString("dd/MM/yyyy"))) _
           And Not (usrClie.Valor = "0") Then

         ldrComp = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).NewRow

         'COMPROBANTES
         With ldrComp
            oFact.CentroEmisorNro(mstrConn)
            lstrCemiNume = oFact.pCentroEmisorNro
            lstrEmctId = oFact.pCentroEmisorId
            lstrHost = oFact.pHost

            .Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
                .Item("comp_fecha") = txtFechaComp.Fecha
            .Item("comp_ingr_fecha") = .Item("comp_fecha")
            .Item("comp_impre") = 0
            .Item("comp_clie_id") = usrClie.Valor
            If pbooEsAlta Then
                .Item("comp_dh") = 0    ' 0 = debe
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.ND
                hdnImpriTipo.Text = "ND"
            Else
                .Item("comp_dh") = 1    ' 1 = haber
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.NC
                hdnImpriTipo.Text = "NC"
            End If
            .Item("comp_cance") = 0 ' 0 = cancelado
            .Item("comp_cs") = 1 ' cta cte
            .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.ChequesRechazo
            .Item("comp_mone_id") = 1
            If lDrPaco.Item("cheq_mone_id") = 1 Then
                'cheque en pesos
                .Item("comp_neto") = CDec(txtImpo.Valor) + CDec(txtGstoImpo.Valor)
            Else
                'cheque en dolares, convertir a pesos.
                    Dim ldecCoti As Decimal = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtFechaComp.Fecha, 2)
                If ldecCoti <> 0 Then
                    .Item("comp_neto") = (CDec(txtImpo.Valor) * ldecCoti) + CDec(txtGstoImpo.Valor)
                Else
                    .Item("comp_neto") = CDec(txtImpo.Valor) + CDec(txtGstoImpo.Valor)
                End If
            End If
            .Item("comp_letra") = clsSQLServer.gObtenerValorCampo(mstrConn, "clientes", .Item("comp_clie_id").ToString, "_ivap_letra")
            .Item("comp_cpti_id") = 2 ' 2 = cta.cte
            .Item("comp_acti_id") = SRA_Neg.Constantes.Actividades.Administracion
            .Item("comp_cemi_nume") = lstrCemiNume
            .Item("comp_emct_id") = lstrEmctId
            .Item("comp_host") = lstrHost

            .Table.Rows.Add(ldrComp)
         End With

         'COMPROB_DETA
         ldrCode = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobDeta).NewRow
         With ldrCode
            .Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
            .Item("code_comp_id") = ldrComp.Item("comp_id")
            If lDrPaco.Item("cheq_mone_id") = 1 Then
                'cheque en pesos
                .Item("code_coti") = 1
            Else
                'cheque en dolares, convertir a pesos.
                    Dim ldecCoti As Decimal = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtFechaComp.Fecha, 2)
                .Item("code_coti") = ldecCoti
            End If
            .Item("code_pers") = True
            .Item("code_impo_ivat_tasa") = 0
            .Item("code_impo_ivat_tasa_redu") = 0
            .Item("code_impo_ivat_tasa_sobre") = 0
            .Item("code_impo_ivat_perc") = 0

            .Table.Rows.Add(ldrCode)
         End With

         'COMPROB_VTOS
         ldrCovt = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).NewRow
         With ldrCovt
            .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
            .Item("covt_comp_id") = ldrComp.Item("comp_id")
            .Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, ldrComp.Item("comp_acti_id"), ldrComp.Item("comp_fecha"))
            .Item("covt_porc") = 100
            .Item("covt_impo") = ldrComp.Item("comp_neto")
            .Item("covt_cance") = 0

            .Table.Rows.Add(ldrCovt)
         End With

         'COMPROB_CONCEPTOS
         ldrCoco = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).NewRow
         With ldrCoco
            .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
            .Item("coco_comp_id") = ldrComp.Item("comp_id")
            .Item("coco_conc_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "conceptos_formulas", "@conc_form_id=" & SRA_Neg.Constantes.Formulas.Rechazo_de_cheque_ajuste_cta_cte, "conc_id")
            .Item("coco_ccos_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "actividades", "@acti_id=" & SRA_Neg.Constantes.Actividades.Administracion, "acti_ccos_id")
                If lDrPaco.Item("cheq_mone_id") = 1 Then
                    .Item("coco_impo") = CDec(txtImpo.Valor)
                Else
                    Dim ldecCoti As Decimal = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtFechaComp.Fecha, 2)
                    If ldecCoti <> 0 Then
                        .Item("coco_impo") = (CDec(txtImpo.Valor) * ldecCoti)
                    Else
                        .Item("coco_impo") = CDec(txtImpo.Valor)
                    End If
                End If
                .Item("coco_impo_ivai") = .Item("coco_impo")
                .Item("coco_desc_ampl") = "Rechazo de cheque " + cmbBanc.SelectedItem.Text + " Nro " + txtNume.Text

                .Table.Rows.Add(ldrCoco)
            End With

         If txtGstoImpo.Valor > 0 Then
            ldrCoco = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).NewRow
            With ldrCoco
               .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
               .Item("coco_comp_id") = ldrComp.Item("comp_id")
               .Item("coco_conc_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "conceptos_formulas", "@conc_form_id=" & SRA_Neg.Constantes.Formulas.Gasto_administrativo_por_rechazo_de_Chq, "conc_id")
               .Item("coco_ccos_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "actividades", "@acti_id=" & SRA_Neg.Constantes.Actividades.Administracion, "acti_ccos_id")
                    If lDrPaco.Item("cheq_mone_id") = 1 Then
                        .Item("coco_impo") = CDec(txtGstoImpo.Valor)
                    Else
                        'Dim ldecCoti As Decimal = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtFechaComp.Fecha, 2)
                        'If ldecCoti <> 0 Then
                        '    .Item("coco_impo") = (CDec(txtGstoImpo.Valor) * ldecCoti)
                        'Else
                            .Item("coco_impo") = CDec(txtGstoImpo.Valor)
                        'End If
                    End If


                    .Item("coco_impo_ivai") = .Item("coco_impo")
                    .Table.Rows.Add(ldrCoco)
                End With
         End If
      End If

      'ASIENTOS
      ldrAsie = ldsDatos.Tables(mstrTablaAsiento).NewRow
      With ldrAsie
         .Item("proc_id") = clsSQLServer.gObtenerId(.Table, "proc_id")
         .Item("proc_cheq_id") = hdnId.Text
         .Item("proc_comp_id") = DBNull.Value
         .Table.Rows.Add(ldrAsie)
      End With

      lDrCheq = ldsDatos.Tables(mstrTabla).Rows(0)

      'guardo los datos anteriores
      lDrMovim = ldsDatos.Tables(mstrTablaChequesMovim).NewRow
      With lDrMovim
         .Item("mchq_id") = -1
         .Item("mchq_cheq_id") = lDrCheq.Item("cheq_id")
         .Item("mchq_rece_fecha") = lDrCheq.Item("cheq_rece_fecha")
         .Item("mchq_teor_depo_fecha") = lDrCheq.Item("cheq_teor_depo_fecha")
         .Item("mchq_rech_fecha") = lDrCheq.Item("cheq_rech_fecha")
         .Item("mchq_clea") = lDrCheq.Item("cheq_clea_id")
         .Item("mchq_emct_id") = lDrCheq.Item("cheq_emct_id")
         .Table.Rows.Add(lDrMovim)
      End With

      'actualizo con los datos nuevos
      With lDrCheq
         If pbooEsAlta Then
            If Not ldrCoco Is Nothing Then
               .Item("cheq_conc_id") = ldrCoco.Item("coco_conc_id")
               .Item("cheq_ccos_id") = ldrCoco.Item("coco_ccos_id")
            End If
            .Item("cheq_gsto_impo") = txtGstoImpo.Valor
            .Item("cheq_rech_fecha") = Today
         Else
            .Item("cheq_conc_id") = DBNull.Value
            .Item("cheq_ccos_id") = DBNull.Value
            .Item("cheq_gsto_impo") = DBNull.Value
            .Item("cheq_rech_fecha") = DBNull.Value
         End If
         ''.Item("cheq_nd_comp_id") = DBNull.Value --> no blanquear para poder anular la ND
      End With

      'nota de debito
      ldrND = ldsDatos.Tables(mstrTablaND).NewRow
      With ldrND
         .Item("comp_id") = lDrCheq.Item("cheq_nd_comp_id")
         If lstrRechaFecha = Date.Today.ToString("dd/MM/yyyy") Then
             .Item("comp_baja_fecha") = Date.Today
         Else
             .Item("comp_baja_fecha") = DBNull.Value
         End If
         .Table.Rows.Add(ldrND)
      End With

      Return ldsDatos
   End Function

   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      ldsEsta.Tables(0).TableName = mstrTabla
      ldsEsta.Tables(1).TableName = mstrTablaChequesMovim

      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If txtGstoImpo.Valor > 0 Then
         

      End If
   End Sub


#End Region

#Region "Operacion Sobre la Grilla"
   Private Sub mValidar()

      If cmbBancF.Valor = 0 Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el banco.")
      End If

      'If txtNumeFil.Valor Is DBNull.Value Then
      '   Throw New AccesoBD.clsErrNeg("Debe ingresar el número del cheque.")
      'End If

   End Sub
   Public Sub mConsultar()
      Try

         hdnImprimio.Text = ""
         hdnImprimir.Text = ""
         hdnImpriTipo.Text = ""

         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec cheques_busq")
         lstrCmd.Append(" @cheq_banc_id =" + cmbBancF.Valor.ToString)
         lstrCmd.Append(",@cheq_nume =" + clsSQLServer.gFormatArg(txtNumeFil.Valor.ToString, SqlDbType.VarChar))
         lstrCmd.Append(",@depositado=1")
         lstrCmd.Append(",@cheq_rece_fecha=" & clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
         lstrCmd.Append(",@rechazado=" + Math.Abs(CInt(chkRech.Checked)).ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
      Try
         If e.Item.ItemIndex <> -1 Then
            If Not CType(e.Item.DataItem, DataRowView).Row.IsNull("cheq_rech_fecha") Then
               e.Item.BackColor = System.Drawing.Color.LightSalmon
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos"
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mCerrar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mValidar()
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
      Try
         ' si no cancela la impresion

         If CBool(CInt(hdnImprimio.Text)) Then
            Dim ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantes", hdnImprimir.Text)

            With ldsDatos.Tables(0)
               .TableName = "comprobantes"
               .Rows(0).Item("comp_impre") = CBool(CInt(hdnImprimio.Text))
            End With

            While ldsDatos.Tables.Count > 1
               ldsDatos.Tables.Remove(ldsDatos.Tables(1))
            End While

            Dim lobj As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), "comprobantes", ldsDatos)

            lobj.Modi()
         End If

         txtNumeFil.Text = ""
         cmbBancF.Limpiar()
         chkRech.Checked = False

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
