<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="~/controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="~/controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PRON" Src="~/controles/usrProdNuevo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Tramites.aspx.vb" Inherits="Tramites" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		
			<title><%=mstrTitulo%></title>
		
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/prototype.js"></script>
		<script language="JavaScript">
		
		function mVerErrores()
		{
			var lstrProc = '<%=mintProce%>';
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Errores&tabla=rg_denuncias_errores&filtros=" + document.all("hdnId").value + ","+lstrProc, 1, "600","300","70","100");
			var lstrId = document.all("hdnDetaId").value;
			if (lstrId == "0")
				lstrId = document.all("hdnId").value;
			gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce="+lstrProc+"&id=" + lstrId, 1, "700","400","20","40");
		}
		
		function mVerStockSemen()
		{
			var lstrClieId = document.all("hdnCompOrig").value;
			var lstrClieNomb = document.all('usrClieComp_txtApel').value;
			gAbrirVentanas("Reportes/SaldoSemenStock.aspx?clieID=" + lstrClieId + "&clieNomb=" + lstrClieNomb, 1, "750","550","20","40");
		}
		
		function mVerStockEmbriones()
		{
			var lstrClieId = document.all("hdnCompOrig").value;
			var lstrClieNomb = document.all('usrClieComp_txtApel').value;
			gAbrirVentanas("Reportes/SaldoEmbrionesStock.aspx?clieID=" + lstrClieId + "&clieNomb=" + lstrClieNomb, 1, "750","550","20","40");
		}
		
		function mCargarTE()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Denuncias de TE&tabla=te_denun_tramite&hdn=hdnDatosTEPop&filtros=" + document.all('usrClieVend:txtId').value, 2, "700","400","100","50");
		}
		
		function btnAgre_click()
		{
			document.all('hdnDatosPop').value = "-1";
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=SELECCIONE UNA PLANTILLA&tabla=rg_tramites_planXrg_tramites_tipos&filtros=<%=mintTtraId%>", 8,650,350);
			return(false);
		}
		
		function btnModi_click(pstrEstaBaja)
		{
			if(document.all("cmbEsta").value == pstrEstaBaja)
			{
				if(!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?'))
					return(false);
			}
			return(true);
		}
		
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
					{
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 					document.all('lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					document.all('lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
		}
		
		function cmbRazaFil_change()
		{
		    if(document.all("cmbRazaFil").value!="")
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = true;
				
				document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = true;
				document.all("txtusrProdFil:cmbProdRaza").disabled = true;
				
				
			}
			else
			{
				document.all("usrCriaFil:cmbRazaCria").value = ""
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = false;
				
				document.all("usrProdFil:cmbProdRaza").value = "";
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = false;
				document.all("txtusrProdFil:cmbProdRaza").disabled = false;
			
			}
		}
		
		function usrProd_usrProducto_cmbProdSexo_onchange()
		{
			if ('<%=mintTtraId%>'=='11' || '<%=mintTtraId%>'=='30' || '<%=mintTtraId%>'=='31' || '<%=mintTtraId%>'=='20' || '<%=mintTtraId%>'=='1')
			{
				if (document.all("usrProd_usrProducto_cmbProdSexo")!=null &&  document.all('usrClieProp:txtCodi')!=null)
				{
					//if(document.all("usrProd_usrProducto_cmbProdSexo").value=="1")
					//	document.all('rowDosisCria').style.display = 'inline';
					//else
					//	document.all('rowDosisCria').style.display = 'none';
				}
			}
		}
		
		function usrClieVend_onchange()
		{
			if (document.all('usrClieVend:txtCodi')!=null && document.all('usrClieProp:txtCodi')==null && document.all("cmbPais")!=null)
				mBuscarPaisDefa('usrClieVend');
			var lstrRaza = '';
			if (document.all('usrProd:usrProducto:cmbProdRaza')!=null)
				lstrRaza = document.all('usrProd:usrProducto:cmbProdRaza').value;
			else
				lstrRaza = document.all('hdnRazaId').value;
			if (lstrRaza != '' && document.all('usrClieVend:txtId').value != '')
			{
				var sFiltro = '@clie_id='+document.all('usrClieVend:txtId').value+',@raza_id='+lstrRaza;
				LoadComboXML("criadores_cliente_cargar", sFiltro, "cmbCriaVend", "S");
		    }
		    else
				LoadComboXML("criadores_cliente_cargar", "@raza_id=0", "cmbCriaVend", "S");
		}
		
		function usrClieComp_onchange()
		{
			if (document.all('usrClieComp:txtCodi')!=null  && document.all('usrClieProp:txtCodi')!=null && document.all("cmbPais")!=null)
				mBuscarPaisDefa('usrClieComp');
			var lstrRaza = '';
			if (document.all('usrProd:usrProducto:cmbProdRaza')!=null)
				lstrRaza = document.all('usrProd:usrProducto:cmbProdRaza').value;
			else
				lstrRaza = document.all('hdnRazaId').value;
		    if (lstrRaza != '' && document.all('usrClieComp:txtId').value != '')
			{
				var sFiltro = '@clie_id='+document.all('usrClieComp:txtId').value+',@raza_id='+lstrRaza;
				LoadComboXML("criadores_cliente_cargar", sFiltro, "cmbCriaComp", "S");
			}
			else
				LoadComboXML("criadores_cliente_cargar", "@raza_id=0", "cmbCriaComp", "S");
		}	
		
		function mBuscarPaisDefa(pstrCon)
		{
			if (document.all(pstrCon + ':txtCodi').value != "")
				{
					var strFiltro = "@clie_id = " + document.all(pstrCon + ':txtCodi').value;
					var strRet = LeerCamposXML("dire_clientes", strFiltro, "_dicl_pais_id");
					
					strFiltro = "@pais_defa = 1";
					var strRet2 = LeerCamposXML("paises", strFiltro, "pais_id");
					if (strRet == strRet2)
						document.all("cmbPais").value = "";
					else
						document.all("cmbPais").value = strRet;
				}
			else
				document.all("cmbPais").value = "";
		}
				
		function expandir()
		{
			if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Tr�mites</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" width="100%" BorderWidth="1px"
											BorderStyle="none">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 50px">
														<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="17" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE class="FdoFld" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																																			
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%" align="right" height="15">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo"> Nro.Tr�mite:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 85%">
																				<CC1:NUMBERBOX id="txtNumeFil" runat="server" cssclass="cuadrotexto" MaxValor="999999999" Width="140px"
																					AceptaNull="False"></CC1:NUMBERBOX></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right" height="15">
																				<asp:Label id="lblEstadoFil" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbEstadoFil" runat="server" Width="200px" AceptaNull="False"
																					NomOper="estados_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR id="rowPaisFil" style="DISPLAY: inline" runat="server">
																			<TD align="right">
																				<asp:Label id="lblPaisFil" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbPaisFil" runat="server" Width="280px" nomoper="paises_cargar"
																					aceptanull="false"></cc1:combobox></TD>
																		</TR>
																		<TR id="rowDivPaisFil" style="DISPLAY: inline" runat="server">
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" height="15">
																				<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="294px"
																					AceptaNull="False" NomOper="razas_cargar" filtra="true" MostrarBotones="False" onchange="javascript:cmbRazaFil_change();"
																					Height="20px"></cc1:combobox></TD>
																		</TR>
																		
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" height="15">
																				<asp:label id="lblSraNumeFil" runat="server" cssclass="titulo">Nro.Producto:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:NUMBERBOX id="txtSraNumeFil" runat="server" cssclass="cuadrotexto" Width="140px" AceptaNull="False"
																					maxvalor="999999999"></CC1:NUMBERBOX></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" align="right" height="15">
																				<asp:Label id="lblCriaFil" runat="server" cssclass="titulo">Raza/Criador:</asp:Label>&nbsp;</TD>
																			<TD>
																				<UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
																					Saltos="1,2" criador="true" Tabla="Criadores"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" height="15">
																				<asp:label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																		
																	
																			<TD align="right" height="15">
																				<asp:label id="lblProdFil" runat="server" cssclass="titulo">Producto:</asp:label>&nbsp;</TD>
																			<TD>
																				<UC1:PROD id="usrProdFil" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True"
																					Saltos="1,2"  Tabla="productos" MuestraDesc="True" FilTipo="S" FilSociNume="True" AutoPostBack="False"></UC1:PROD>
																																					
																			</TD>
																					
																					
																		</TR>
																		
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>																			
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD width="72%" background="imagenes/formfdofields.jpg"><asp:checkbox id="chkVisto" Text="Incluir Registros Vistos y/o Aprobados" CssClass="titulo" Runat="server"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 17px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="top" align="right" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdDato_Page"
											OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton5" runat="server" CausesValidation="false" CommandName="Update">
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="tram_id"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="tram_inic_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
													ItemStyle-HorizontalAlign="Center" HeaderText="Fecha Inic."></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="tram_nume" HeaderText="N� Tr�mite" HeaderStyle-HorizontalAlign="Center"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="raza" HeaderText="Raza" HeaderStyle-Width="20%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="sexo" HeaderText="Sexo"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_nomb" HeaderText="Nombre" HeaderStyle-Width="30%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_numero" HeaderText="N� Producto" HeaderStyle-HorizontalAlign="Center"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_cria_nume" HeaderText="Nro. Criador" HeaderStyle-HorizontalAlign="Center"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="esta_desc" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="top" align="left" colSpan="3"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
											ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Agregar un nuevo tr�mite"></CC1:BOTONIMAGEN></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panLinks" runat="server" cssclass="titulo" width="100%" Height="124%" Visible="False">
											<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Tr�mite </asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkRequ" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Requisitos</asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkDocu" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Documentaci�n </asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkObse" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Comentarios</asp:linkbutton></TD>
													<TD width="0"><!--IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"--></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkVend" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
															Width="70px" Visible=False Height="21px" CausesValidation="False"> Propietarios </asp:linkbutton></TD>
													<TD width="0" background="imagenes/tab_fondo.bmp"><!--IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"--></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkComp" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
															Width="70px" Height="21px" Visible=False CausesValidation="False"> Compradores </asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderWidth="1px" BorderStyle="Solid"
											Height="116px" Visible="False">
											<TABLE class="FdoFld" id="tabDato" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
													<TD vAlign="top" align="right">
														<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" align="center" colSpan="2">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="tabCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD noWrap align="right" width="18%">
																		<asp:Label id="lblInicFecha" runat="server" cssclass="titulo">Fecha Inicio:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtInicFecha" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																		<asp:Label id="lblFinaFecha" runat="server" cssclass="titulo">Fecha Cierre:</asp:Label>
																		<cc1:DateBox id="txtFinaFecha" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD noWrap align="right" width="18%">
																		<asp:Label id="lblFechaTransferencia" runat="server" cssclass="titulo">Fecha Transferencia:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtFechaTransf" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="True"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																		
																</TR>
																<TR>
																	<TD noWrap align="right">
																		<asp:Label id="lblFechaTram" runat="server" cssclass="titulo">Fecha Tramite:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtFechaTram" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:combobox class="combo" id="cmbEsta" runat="server" cssclass="cuadrotexto" Width="180px"></cc1:combobox></TD>
																</TR>
																<tr>
																		<TD align="right" height="15">
																		<asp:Label id="lblVariedad" runat="server" cssclass="titulo">Variedad:</asp:Label></td>
																		<td><cc1:combobox class="combo" id="cmbVari" runat="server" Width="67px" NomOper="rg_registros_tipos_cargar"
																				DESIGNTIMEDRAGDROP="2076"></cc1:combobox></td>
																																												
																		
																		
																</tr>
																<TR>
																			<TD align="right" height="15">
																				<asp:label id="lblTipoRegistro" runat="server" cssclass="titulo">Tipo de Registro:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbRegiTipo" runat="server" Width="77px" NomOper="rg_registros_tipos_cargar"
																				DESIGNTIMEDRAGDROP="2076"></cc1:combobox></TD>
																</TR>
																
																
																
																<TR>
																	<TD noWrap align="center" colSpan="2">
																		<asp:Label id="lblTranPlan" runat="server" cssclass="titulo"><br>Se ha detectado m�s de un producto para este tr�mite.<br>Puede modificar los mismos desde la opci�n de Transferencia por Planilla.<br></asp:Label></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<UC1:PRON id="usrProd" runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True" Saltos="1,2"
																	Tabla="productos"  MuestraDesc="True" FilTipo="S" FilSociNume="True" AutoPostBack="False"></UC1:PRON>
																<TR id="rowDosisCria" style="DISPLAY: none" runat="server">
																	<TD colSpan="2">
																		<TABLE style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																			<TR>
																				<TD align="right" width="18%">
																					<asp:Label id="lblDosiCant" runat="server" cssclass="titulo">Cantidad Dosis:</asp:Label></TD>
																				<TD>
																					<cc1:numberbox id="txtDosiCant" runat="server" cssclass="cuadrotexto" MaxValor="999999999" Width="81px"
																						Visible="true" MaxLength="3" EsPorcen="False" esdecimal="False"></cc1:numberbox>
																					<asp:Label id="lblCriaCant" runat="server" cssclass="titulo">&nbsp;&nbsp;Cantidad Crias:&nbsp;</asp:Label>
																					<cc1:numberbox id="txtCriaCant" runat="server" cssclass="cuadrotexto" MaxValor="999999999" Width="81px"
																						Visible="true" EsPorcen="False" esdecimal="False"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR id="rowCantEmbr" style="DISPLAY: none" runat="server">
																	<TD colSpan="2">
																		<TABLE style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																			<TR>
																				<TD align="right" width="18%">
																					<asp:Label id="lblCantEmbr" runat="server" cssclass="titulo">Cantidad:</asp:Label></TD>
																				<TD noWrap>
																					<cc1:numberbox id="txtCantEmbr" runat="server" cssclass="cuadrotexto" MaxValor="999999999" Width="81px"
																						AceptaNull="false" Visible="true" EsPorcen="False" esdecimal="False"></cc1:numberbox>&nbsp;
																					<asp:Label id="lblRecuFecha" runat="server" cssclass="titulo">Fecha Recup.:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtRecuFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																					<asp:Button id="btnTeDenu" runat="server" cssclass="boton" Width="90px" Visible="False" Text="Denuncia TE"></asp:Button>&nbsp;
																					<CC1:TEXTBOXTAB id="txtTeDesc" runat="server" cssclass="cuadrotexto" Width="176px" Visible="False"
																						Enabled="False" ReadOnly="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR id="rowProp" style="DISPLAY: inline" runat="server">
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblClieProp" runat="server" cssclass="titulo">Propietario Actual:&nbsp;</asp:Label></TD>
																	<TD>
																		<UC1:CLIE id="usrClieProp" runat="server" AceptaNull="true" FilDocuNume="True" Saltos="1,2"
																			Tabla="Clientes" Activo="False"></UC1:CLIE></TD>
																</TR>
																<TR id="rowDivProp" style="DISPLAY: inline" runat="server">
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR id="rowImpo" style="DISPLAY: inline" runat="server">
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblImpo" runat="server" cssclass="titulo">Despachante:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:combobox class="combo" id="cmbImpo" runat="server" Width="280px"></cc1:combobox></TD>
																</TR>
																<TR id="rowDivImpo" style="DISPLAY: inline" runat="server">
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR id="rowPais" style="DISPLAY: inline" runat="server">
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblPais" runat="server" cssclass="titulo">Pa�s:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:combobox class="combo" id="cmbPais" runat="server" Width="280px" nomoper="paises_cargar"></cc1:combobox></TD>
																</TR>
																<TR id="rowDivPais" style="DISPLAY: inline" runat="server">
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblClieComp" runat="server" cssclass="titulo">Comprador Cliente:&nbsp;</asp:Label></TD>
																	<TD>
																		<UC1:CLIE id="usrClieComp" runat="server" AceptaNull="true" FilDocuNume="True" Saltos="1,2"
																			Tabla="Clientes"></UC1:CLIE></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblCriaComp" runat="server" cssclass="titulo">Comprador Criador:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:combobox class="combo" id="cmbCriaComp" runat="server" Width="340px" nomoper="paises_cargar"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblClieVend" runat="server" cssclass="titulo">Vendedor Cliente:&nbsp;</asp:Label></TD>
																	<TD>
																		<UC1:CLIE id="usrClieVend" runat="server" AceptaNull="True" FilDocuNume="True" Saltos="1,2"
																			Tabla="Clientes"></UC1:CLIE></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblCriaVend" runat="server" cssclass="titulo">Vendedor Criador:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:combobox class="combo" id="cmbCriaVend" runat="server" Width="340px" nomoper="paises_cargar"></cc1:combobox></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:panel id="panRequ" runat="server" cssclass="titulo" Width="100%" Visible="False">
															<TABLE id="tabRequ" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 100%" colSpan="2">
																		<asp:datagrid id="grdRequ" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdRequ_Page"
																			AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																			AllowPaging="True" OnEditCommand="mEditarDatosRequ">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="trad_id"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
																					<HeaderStyle Width="40%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_pend" HeaderText="Pendiente"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_manu" HeaderText="Manual"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblRequRequ" runat="server" cssclass="titulo">Requisito:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<cc1:combobox class="combo" id="cmbRequRequ" runat="server" cssclass="cuadrotexto" Width="80%"
																			NomOper="rg_requisitos_cargar" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<asp:CheckBox id="chkRequPend" Text="Pendiente" Runat="server" CssClass="titulo" Checked="False"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblRequManuTit" runat="server" cssclass="titulo">Manual:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblRequManu" runat="server" cssclass="Desc">Si</asp:Label></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblRequFinaFechaTit" runat="server" cssclass="titulo">Fecha Finalizaci�n:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblRequFinaFecha" runat="server" cssclass="Desc"></asp:Label></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="middle" align="center" colSpan="2" height="30">
																		<asp:Button id="btnAltaRequ" runat="server" cssclass="boton" Width="100px" Text="Agregar Req."></asp:Button>&nbsp;
																		<asp:Button id="btnBajaRequ" runat="server" cssclass="boton" Width="100px" Text="Eliminar Req."></asp:Button>&nbsp;
																		<asp:Button id="btnModiRequ" runat="server" cssclass="boton" Width="100px" Text="Modificar Req."></asp:Button>&nbsp;
																		<asp:Button id="btnLimpRequ" runat="server" cssclass="boton" Width="100px" Text="Limpiar Req."></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" vAlign="top" align="center" colSpan="2">
														<asp:panel id="panDocu" runat="server" cssclass="titulo" Width="100%" Visible="False">
															<TABLE id="tabDocu" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 100%" align="center" colSpan="2">
																		<asp:datagrid id="grdDocu" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdDocu_Page"
																			OnUpdateCommand="mEditarDatosDocu" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																			CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="15px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="trdo_id" HeaderText="trdo_id"></asp:BoundColumn>
																				<asp:BoundColumn DataField="trdo_path" HeaderText="Documento">
																					<HeaderStyle Width="50%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="trdo_refe" HeaderText="Referencia">
																					<HeaderStyle Width="50%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblDocuDocu" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<CC1:TEXTBOXTAB id="txtDocuDocu" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG id="imgDelDocuDoc" style="CURSOR: hand" onclick="javascript:mLimpiarPath('txtDocuDocu','imgDelDocuDoc');"
																			alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<BR>
																		<INPUT id="filDocuDocu" style="WIDTH: 340px; HEIGHT: 22px" type="file" size="49" name="File1"
																			runat="server">
																	</TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblDocuObse" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<CC1:TEXTBOXTAB id="txtDocuObse" runat="server" cssclass="cuadrotexto" Width="80%" AceptaNull="False"
																			Height="41px" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="middle" align="center" colSpan="2" height="34">
																		<asp:Button id="btnAltaDocu" runat="server" cssclass="boton" Width="110px" Text="Agregar Docum."></asp:Button>&nbsp;
																		<asp:Button id="btnBajaDocu" runat="server" cssclass="boton" Width="110px" Visible="true" Text="Eliminar Docum."></asp:Button>&nbsp;
																		<asp:Button id="btnModiDocu" runat="server" cssclass="boton" Width="110px" Visible="true" Text="Modificar Docum."></asp:Button>&nbsp;
																		<asp:Button id="btnLimpDocu" runat="server" cssclass="boton" Width="110px" Visible="true" Text="Limpiar Docum."></asp:Button>&nbsp;&nbsp;<BUTTON class="boton" id="btnDescDocu" style="FONT-WEIGHT: bold; WIDTH: 110px" onclick="javascript:mDescargarDocumento('hdnDocuId','tramites_docum','txtDocuDocu');"
																			type="button" runat="server" value="Descargar">Descargar</BUTTON></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:panel id="panObse" runat="server" cssclass="titulo" Width="100%" Visible="False">
															<TABLE id="tabObse" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 100%" colSpan="2">
																		<asp:datagrid id="grdObse" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdObse_Page"
																			AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																			AllowPaging="True" OnEditCommand="mEditarDatosObse">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="trao_id"></asp:BoundColumn>
																				<asp:BoundColumn DataField="trao_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
																					ItemStyle-HorizontalAlign="Center" HeaderText="Fecha"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
																					<HeaderStyle Width="50%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="trao_obse" HeaderText="Comentarios">
																					<HeaderStyle Width="50%"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblObseFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<cc1:DateBox id="txtObseFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblObseRequ" runat="server" cssclass="titulo">Requisito:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<cc1:combobox class="combo" id="cmbObseRequ" runat="server" cssclass="cuadrotexto" Width="80%"
																			NomOper="rg_requisitos_cargar" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																		<asp:Label id="lblObseObse" runat="server" cssclass="titulo">Comentario:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																		<CC1:TEXTBOXTAB id="txtObseObse" runat="server" cssclass="cuadrotexto" Width="80%" Height="41px"
																			TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="middle" align="center" colSpan="2" height="30">
																		<asp:Button id="btnAltaObse" runat="server" cssclass="boton" Width="100px" Text="Agregar Com."></asp:Button>&nbsp;
																		<asp:Button id="btnBajaObse" runat="server" cssclass="boton" Width="100px" Text="Eliminar Com."></asp:Button>&nbsp;
																		<asp:Button id="btnModiObse" runat="server" cssclass="boton" Width="100px" Text="Modificar Com."></asp:Button>&nbsp;
																		<asp:Button id="btnLimpObse" runat="server" cssclass="boton" Width="100px" Text="Limpiar Com."></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:panel id="panVend" runat="server" cssclass="titulo" Width="100%" Visible="False">
															<TABLE id="tabPers" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 100%" colSpan="3">
																		<asp:datagrid id="grdVend" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdVend_Page"
																			AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																			AllowPaging="True">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:BoundColumn Visible="False" DataField="trpe_id"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_nombre" HeaderText="Nombre y Apellido/R.Social"></asp:BoundColumn>
																				<asp:BoundColumn DataField="trpe_porc" HeaderText="Porcentaje"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:panel id="panComp" runat="server" cssclass="titulo" Width="100%" Visible="False">
															<TABLE id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 100%" colSpan="3">
																		<asp:datagrid id="grdComp" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdComp_Page"
																			AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																			AllowPaging="True" OnEditCommand="mEditarDatosComp">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="trpe_id"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_nombre" HeaderText="Nombre y Apellido/R.Social"></asp:BoundColumn>
																				<asp:BoundColumn DataField="trpe_porc" HeaderText="Porcentaje"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD colSpan="2" height="1">
																		<asp:Label id="lblNombComp" runat="server" cssclass="titulo"></asp:Label></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 15%" align="right" height="1">
																		<asp:Label id="lblPorcComp" runat="server" cssclass="titulo">Porcentaje:</asp:Label></TD>
																	<TD style="WIDTH: 85%">
																		<cc1:numberbox id="txtPorcComp" runat="server" cssclass="cuadrotexto" MaxValor="100" Width="60px"
																			MaxLength="3" esdecimal="False"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 15%" align="right">
																		<asp:Label id="lblObseComp" runat="server" cssclass="titulo">Observaciones:</asp:Label></TD>
																	<TD style="WIDTH: 85%">
																		<CC1:TEXTBOXTAB id="txtObseComp" runat="server" cssclass="cuadrotexto" Width="80%" Height="41px"
																			TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="middle" align="center" colSpan="3" height="30">
																		<asp:Button id="btnModiComp" runat="server" cssclass="boton" Width="100px" Text="Modificar"></asp:Button>&nbsp;
																		<asp:Button id="btnLimpComp" runat="server" cssclass="boton" Width="100px" Text="Limpiar"></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnErr" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Ver Errores"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnStock" runat="server" cssclass="boton" Width="110px" CausesValidation="False"
															Text="Stock Semen" Visible="false"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></TD>
								</TR>
							</TBODY>
						</TABLE> <!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnRazaId" runat="server"></asp:textbox>				
				<asp:textbox id="hdnRequId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDocuId" runat="server"></asp:textbox>
				<asp:textbox id="hdnObseId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnCompId" runat="server"></asp:textbox>
				<asp:textbox id="lblAltaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnMultiProd" runat="server"></asp:textbox>
				<asp:textbox id="hdnTramNro" runat="server"></asp:textbox>
				<asp:textbox id="hdnVendOrig" runat="server"></asp:textbox>
				<asp:textbox id="hdnCompOrig" runat="server"></asp:textbox>
				<asp:textbox id="hdnMsgError" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosTEPop" runat="server"></asp:textbox>
				<asp:textbox id="hdnTEId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaComp" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaVend" runat="server"></asp:textbox>.
			</DIV>
			<script language="JavaScript">
				if (document.all('lblAltaId')!=null && document.all('lblAltaId').value!='')
				{
					alert('Se ha asignado el nro de tramite '+document.all('lblAltaId').value);
					document.all('lblAltaId').value = '';
				}
				if ('<%=mintTtraId%>'=='11' || '<%=mintTtraId%>'=='30' || '<%=mintTtraId%>'=='31')
				{
					if (document.all('usrProd:usrProducto:txtSexoId')!=null)
						document.all('usrProd:usrProducto:txtSexoId').value='1';
				}
			</script>
		</form>
	</BODY>
</HTML>
