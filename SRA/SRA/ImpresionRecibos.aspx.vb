Imports SRA

Public Class ImpresionRecibos
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'Protected WithEvents btnList As NixorControls.BotonImagen
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTarjFil As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbTarjFil As NixorControls.ComboBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblFechaFil As System.Web.UI.WebControls.Label
    'Protected WithEvents txtPeriFil As NixorControls.NumberBox
    'Protected WithEvents cmbPetiFil As NixorControls.ComboBox
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtAnioFil As NixorControls.NumberBox
    'Protected WithEvents chkTodas As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdDatoBusq As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblSociDesde As System.Web.UI.WebControls.Label
    'Protected WithEvents txtSociDesde As NixorControls.NumberBox
    'Protected WithEvents lblSociHasta As System.Web.UI.WebControls.Label
    'Protected WithEvents txtSociHasta As NixorControls.NumberBox
    'Protected WithEvents lblCPDesde As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCPDesde As NixorControls.TextBoxTab
    'Protected WithEvents lblCPHasta As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCPHasta As NixorControls.TextBoxTab
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtTexto1 As NixorControls.TextBoxTab
    'Protected WithEvents txtTexto2 As NixorControls.TextBoxTab
    'Protected WithEvents txtTexto3 As NixorControls.TextBoxTab
    'Protected WithEvents txtTexto4 As NixorControls.TextBoxTab

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrConn As String
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_DebitosCabe

    Private Enum ColumnasFil As Integer
        lnkEdit = 0
        comp_id = 1
        fecha = 2
        tico_desc = 3
        comp_nro = 4
        cliente = 5
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                mEstablecerPerfil()
                mInicializar()
                mCargarCombos()
                mConsultarBusc()

                clsWeb.gInicializarControles(sender, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEstablecerPerfil()
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPetiFil, "T", SRA_Neg.Constantes.PeriodoTipos.Bimestre)
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjFil, "S")
    End Sub

    Public Sub mInicializar()
        Dim lstrParaPageSize As String
        clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
        grdDatoBusq.PageSize = Convert.ToInt32(lstrParaPageSize)
    End Sub
#End Region

    Private Sub btnBuscar_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            grdDatoBusq.CurrentPageIndex = 0
            mConsultarBusc()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDatoBusq.CurrentPageIndex = E.NewPageIndex
            mConsultarBusc()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Public Sub mConsultarBusc()
        Dim lstrCmd As New StringBuilder

        lstrCmd.Append("exec " + mstrTabla + "_consul")
        lstrCmd.Append(" @deca_anio=" + txtAnioFil.Valor.ToString)
        lstrCmd.Append(",@deca_peti_id=" + cmbPetiFil.Valor.ToString)
        lstrCmd.Append(",@deca_perio=" + txtPeriFil.Valor.ToString)
        lstrCmd.Append(",@deca_tarj_id=" + cmbTarjFil.Valor.ToString)
        lstrCmd.Append(",@deca_acti_id=" + CType(SRA_Neg.Constantes.Actividades.Socios, String))
        lstrCmd.Append(",@noImp=" + Math.Abs(CInt(chkTodas.Checked)).ToString)

        clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDatoBusq)
    End Sub
#End Region

    Private Sub mListar()
        Dim lstrRptName As String = "DebitosCupones"

        Dim lstrId As String

        For Each lItem As WebControls.DataGridItem In grdDatoBusq.Items
            If DirectCast(lItem.FindControl("chkSel"), CheckBox).Checked Then
                lstrId = lItem.Cells(ColumnasFil.comp_id).Text
                Exit For
            End If
        Next

        If lstrId = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un d�bito!")
        End If

        Dim lDs As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, lstrId)
        lDs.Tables(0).TableName = mstrTabla
        lDs.Tables(mstrTabla).Rows(0).Item("deca_impr_fecha") = Today
        Dim lObj As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, lDs)
        lObj.Modi()

        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        lstrRpt += "&deca_id=" + lstrId
        lstrRpt += "&soci_desde=" + txtSociDesde.Valor.ToString
        lstrRpt += "&soci_hasta=" + txtSociHasta.Valor.ToString
        lstrRpt += "&cp_desde=" + txtCPDesde.Text.ToString
        lstrRpt += "&cp_hasta=" + txtCPHasta.Text.ToString
        lstrRpt += "&texto1=" + txtTexto1.Text.ToString
        lstrRpt += "&texto2=" + txtTexto2.Text.ToString
        lstrRpt += "&texto3=" + txtTexto3.Text.ToString
        lstrRpt += "&texto4=" + txtTexto4.Text.ToString

        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub
End Class
