<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.consulta_pop" CodeFile="consulta_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript"> 
		function mImprimir()
		{
			window.print();
		}		
		function mEstablecimiento(pEsta)
		{
			var win=window.open('establecimientos.aspx?id='+pEsta, '', 'location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=700px,height=400px,left=50px,top=50px');
		}
		
				
		function mRelaciones(pProd)
		{
			var win=window.open('productos.aspx?ProdId='+pProd +"&SoloConsulta="+document.all("hdnSoloConsulta").value, '', 'location=no,menubar=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,width=1200px,height=500px,left=50px,top=50px');
		}	
	
		/*
		function usrClieFil_cmbRazaCria_onchange()
		{
			document.all("usrClieFil:cmbRazaCria").value = "3"
			document.all("usrClieFil:cmbRazaCria").onchange();
		}
		*/	
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG class="noprint" border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td class="noprint" background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG class="noprint" border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG class="noprint" border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%" id="Table1" border="0">
							<TR>
								<TD vAlign="middle" align="right">
									<DIV onclick="window.close();"><IMG style="CURSOR: hand" class="noprint" border="0" alt="Cerrar" src="imagenes/close3.bmp">
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD><asp:label id="lblTituProd" runat="server" cssclass="titulo" Font-Size="X-Small"></asp:label></TD>
							</TR>
							<TR>
								<TD><asp:label id="lblTituAbm" runat="server" cssclass="titulo" Font-Size="X-Small"></asp:label></TD>
							</TR>
							<TR style="DISPLAY: none" id="trFiltroHijos" runat="server">
								<TD vAlign="middle" align="right">
									<div class="noFiltrar"><asp:button id="btnFiltrar" runat="server" cssclass="boton" CausesValidation="False" Text="Filtrar"
											Width="100px"></asp:button><asp:button id="btnLimpiar" runat="server" cssclass="boton" CausesValidation="False" Text="Limpiar Filtro"
											Width="100px"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
								</TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="right">
									<div class="noprint"><asp:button id="btnList" runat="server" cssclass="boton" CausesValidation="False" Text="Listar"
											Width="100px" Visible="false"></asp:button><asp:button id="btnImpri" runat="server" cssclass="boton" CausesValidation="False" Text="Imprimir"
											Width="100px" Visible="True"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
								</TD>
							</TR>
							<TR style="DISPLAY: none" id="trFiltro" runat="server">
								<TD style="HEIGHT: 15px" height="15" vAlign="bottom" colSpan="6"><asp:panel id="panFiltro" class="noprint" runat="server" cssclass="titulo" Width="100%" Visible="True"
										BorderStyle="Solid" BorderWidth="0">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TBODY>
												<TR>
													<TD>
														<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
														</TABLE>
													</TD>
												</TR>
							</TR>
							<TR>
								<TD>
									<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
										<TR>
											<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="10"></TD>
											<TD>
												<TABLE id="Table5" class="noprint" border="0" cellSpacing="0" cellPadding="0" width="100%">
													<TR>
														<TD height="6" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
													</TR>
													<TR>
														<TD background="imagenes/formfdofields.jpg"><asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:&nbsp;</asp:label></TD>
														<TD background="imagenes/formfdofields.jpg"><UC1:CLIE id="usrClieFil" runat="server" criador="true" AceptaNull="False" Tabla="Criadores"
																Saltos="1,2" Ancho="800" MuestraDesc="false" FilDocuNume="True"></UC1:CLIE></TD>
													</TR>
												</TABLE>
											</TD>
											<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						</asp:panel></td>
				</tr>
				<TR height="100%">
					<TD vAlign="top"><asp:datagrid id="grdConsulta" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
							OnEditCommand="mEditarDatos" ItemStyle-Height="5px" PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1"
							GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="5%"></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
											Height="5">
											<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR style="DISPLAY: inline" id="trHijos" height="100%" runat="server">
					<TD vAlign="top">
						<div><asp:datagrid id="grdHijos" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
								ItemStyle-Height="5px" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
								AllowPaging="False" width="40%">
								<FooterStyle CssClass="footer"></FooterStyle>
								<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
								<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
								<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
								<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
								<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
								<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></div>
					</TD>
				</TR>
			</table>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="hdnSoloConsulta" runat="server"></ASP:TEXTBOX>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</DIV>
			<!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG class="noprint" border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG class="noprint" border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td class="noprint" background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG class="noprint" border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE></form>
	</BODY>
</HTML>
