Imports System
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Printing
Imports System.IO
Imports System.Web.Services.Protocols
Imports SRA.ReportService
Imports System.Runtime.InteropServices ' For Marshal.Copy Dario

Friend Class clsrptPrint
   Private rs As ReportingService
   Private m_renderedReport As Byte()()
   Private m_delegate As Graphics.EnumerateMetafileProc = Nothing
   Private m_currentPageStream As MemoryStream
   Private m_metafile As Metafile = Nothing
   Private m_numberOfPages As Integer
   Private m_currentPrintingPage As Integer
   Private m_lastPrintingPage As Integer
   Private WithEvents m_PrintDocument As New PrintDocument

   Private Function RenderEMF(ByVal reportPath As String, ByVal parameters() As ParameterValue, ByVal myCred As Net.NetworkCredential) As Boolean
      Dim rs As New ReportingService
      'rs.Credentials = System.Net.CredentialCache.DefaultCredentials
      rs.Credentials = myCred

      ' Render arguments
      Dim format As String = "IMAGE"
      Dim historyID As String = Nothing
      Dim deviceInfo As String = Nothing
      Dim credentials As DataSourceCredentials() = Nothing
      Dim showHideToggle As String = Nothing
      Dim encoding As String
      Dim mimeType As String
      Dim reportHistoryParameters As ParameterValue() = Nothing
      Dim warnings As Warning() = Nothing
      Dim streamIDs As String() = Nothing
      Dim sh As New SessionHeader
      rs.SessionHeaderValue = sh

      Dim firstpage As Byte() = Nothing

      ' Build device info based on the start page 
      deviceInfo = String.Format("<DeviceInfo><OutputFormat>{0}</OutputFormat></DeviceInfo>", "emf")

      ' Exectute the report and get page count.
      Try
         ' Renders the first page of the report and returns streamIDs for 
         ' subsequent pages
         firstpage = rs.Render(reportPath, format, historyID, deviceInfo, _
         parameters, credentials, showHideToggle, _
         encoding, mimeType, reportHistoryParameters, _
         warnings, streamIDs)
         ' The total number of pages of the report is 1 + the streamIDs 

         m_numberOfPages = streamIDs.Length + 1
         Dim pages(m_numberOfPages)() As Byte

         ' The first page was already rendered
         pages(0) = firstpage

         Dim pageindex As Integer
         For pageindex = 1 To m_numberOfPages - 1
            ' Build device info based on start page
            deviceInfo = String.Format("<DeviceInfo><OutputFormat>{0}</OutputFormat><StartPage>{1}</StartPage></DeviceInfo>", "emf", Trim(Str(pageindex + 1)) & "")

            pages(pageindex) = rs.Render(reportPath, format, historyID, _
            deviceInfo, parameters, credentials, showHideToggle, encoding, mimeType, reportHistoryParameters, warnings, streamIDs)
         Next

         Me.RenderedReport = pages
      Catch ex As SoapException
         clsError.gManejarError(ex)
         Return False
      Catch ex As Exception
         clsError.gManejarError(ex)
         Return False
      End Try
      Return True
   End Function

	Public Function PrintReport(ByVal printerName As String, ByVal reportPath As String, ByVal parameters() As ParameterValue, ByVal myCred As Net.NetworkCredential) As Boolean
		If Me.RenderEMF(reportPath, parameters, myCred) = False Then Return False
		Try
			' Wait for the report to completely render.
			If (m_numberOfPages < 1) Then Return False
			Dim printerSettings As New PrinterSettings

			'printerSettings.MaximumPage = m_numberOfPages
			'printerSettings.MinimumPage = 1
			'printerSettings.PrintRange = PrintRange.SomePages
			'printerSettings.FromPage = 1
			'printerSettings.ToPage = m_numberOfPages
			'printerSettings.PrinterName = printerName

			m_currentPrintingPage = 1
            m_lastPrintingPage = m_numberOfPages

            m_PrintDocument.PrinterSettings.PrinterName = printerName

			'm_PrintDocument.DefaultPageSettings = printerSettings.DefaultPageSettings

            'Dim pkCustomSize1 As New PaperSize("Custom Paper Size", 795, 1123)
            'm_PrintDocument.PrinterSettings.DefaultPageSettings.PaperSize = pkCustomSize1
            'm_PrintDocument.DefaultPageSettings.PaperSize = pkCustomSize1

			' Set page orientation
            m_PrintDocument.DefaultPageSettings.Landscape = IsLandscape()
            'm_PrintDocument.DefaultPageSettings.Margins.Top = 0
            'm_PrintDocument.DefaultPageSettings.Margins.Bottom = 0
            'm_PrintDocument.DefaultPageSettings.Bounds.Size.Height = 1100
            'm_PrintDocument.DefaultPageSettings.PaperSize
            'm_PrintDocument.DefaultPageSettings.Margins.Top = 0
            'm_PrintDocument.DefaultPageSettings.Margins.Left = 3
            'm_PrintDocument.DefaultPageSettings.Margins.Right = (1150 / 100)
            'm_PrintDocument.DefaultPageSettings.Margins.Bottom = 0
            'm_PrintDocument.PrinterSettings.Copies = 1
            'm_PrintDocument.PrinterSettings.DefaultPageSettings.PaperSize.Kind = PaperKind.A4
            'm_PrintDocument.DefaultPageSettings.PaperSize.Kind = PaperKind.Custom
            'm_PrintDocument.DefaultPageSettings.PaperSize.Height = 297
            'm_PrintDocument.DefaultPageSettings.PaperSize.Width = 210

			' Print report 
			m_PrintDocument.Printooo()
		Catch ex As Exception
			clsError.gManejarError(ex)
			Return False
		Finally
			' Clean up goes here.
		End Try
		Return True
	End Function

	Private Function IsLandscape() As Boolean
		' Set current page stream equal to the 1st rendered page
		Dim PageStream As New MemoryStream(Me.RenderedReport(0))
		' Set its position to start.
		PageStream.Position = 0
		' Load the metafile image for 1st page
		Dim Metafile As New Metafile(CType(PageStream, Stream))
		Return Metafile.Width > Metafile.Height
	End Function

	Private Sub PrintDocument_PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs) Handles m_PrintDocument.PrintPage
		ev.HasMorePages = False
		If (m_currentPrintingPage <= m_lastPrintingPage And MoveToPage(m_currentPrintingPage)) Then
			' Draw the page
			ReportDrawPage(ev.Graphics)
			' If the next page is less than or equal to the last page, 
			' print another page.
			m_currentPrintingPage += 1
			If (m_currentPrintingPage <= m_lastPrintingPage) Then
				ev.HasMorePages = True
			End If
		End If
	End Sub

	' Method to draw the current emf memory stream 
	Private Sub ReportDrawPage(ByVal g As Graphics)
		If (m_currentPageStream Is Nothing Or _
		0 = m_currentPageStream.Length Or _
		m_metafile Is Nothing) Then Return
		SyncLock (Me)
			' Set the metafile delegate.
			Dim width As Integer = m_metafile.Width
            Dim height As Integer = m_metafile.Height
			m_delegate = _
			New Graphics.EnumerateMetafileProc(AddressOf MetafileCallback)
			' Draw in the rectangle
			Dim points(2) As Point
			points(0) = New Point(0, 0)
			points(1) = New Point(width, 0)
			points(2) = New Point(0, height)

			g.EnumerateMetafile(m_metafile, points, m_delegate)
			' Clean up
			m_delegate = Nothing
		End SyncLock
	End Sub

	Private Function MoveToPage(ByVal page As Int32) As Boolean
		' Check to make sure that the current page exists in
		' the array list
		If (Me.RenderedReport(m_currentPrintingPage - 1) Is Nothing) Then Return False
		' Set current page stream equal to the rendered page
		m_currentPageStream = _
		New MemoryStream(Me.RenderedReport(m_currentPrintingPage - 1))
		' Set its postion to start.
		m_currentPageStream.Position = 0
		' Initialize the metafile
		If Not (m_metafile Is Nothing) Then
			m_metafile.Dispose()
			m_metafile = Nothing
		End If
		' Load the metafile image for this page
		m_metafile = New Metafile(CType(m_currentPageStream, Stream))
		Return True
	End Function

	Private Function MetafileCallback(ByVal recordType As EmfPlusRecordType, _
	ByVal flags As Integer, ByVal dataSize As Integer, _
	ByVal data As IntPtr, _
	ByVal callbackData As PlayRecordCallback) As Boolean

		Dim dataArray As Byte() = Nothing
		' Dance around unmanaged code.
		If Not IntPtr.Zero.Equals(data) Then
			' Copy the unmanaged record to a managed byte buffer 
			' that can be used by PlayRecord.
			dataArray = New Byte(dataSize) {}
			Marshal.Copy(data, dataArray, 0, dataSize)
		End If
		' play the record. 
		m_metafile.PlayRecord(recordType, flags, dataSize, dataArray)

		Return True
	End Function

	Public Property RenderedReport() As Byte()()
		Get
			Return m_renderedReport
		End Get
		Set(ByVal Value As Byte()())
			m_renderedReport = Value
		End Set
	End Property


End Class