
Public Class clsXMLHTTP
   Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState

   Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
      Get
         Return True
      End Get
   End Property

   Public Sub ProcessRequest(ByVal pContext As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
      Try
         Dim lstrOper As String = pContext.Request.QueryString("oper")
         Dim lstrArgs As String = pContext.Request.QueryString("Args")
         Dim lstrOpc As String = pContext.Request.QueryString("opc")
         Dim lstrCmb As String = pContext.Request.QueryString("cmb")
         Dim vstrOper() As String = Split(lstrOper, ";")
         Dim vstrArgs() As String = Split(lstrArgs, ";")

         Dim sB As StringBuilder
         Dim Ds As DataSet

         If Not System.Configuration.ConfigurationSettings.AppSettings("conCulture") Is Nothing Then
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo(System.Configuration.ConfigurationSettings.AppSettings("conCulture"))
         End If

         If lstrCmb <> "" Then
            Dim lstrCmd As String
            For i As Integer = 0 To vstrOper.GetUpperBound(0)
               lstrCmd += "exec " + vstrOper(i) + " " + vstrArgs(i) + ";"
            Next

            Ds = clsSQLServer.gExecuteQuery(pContext.Session("sConn").ToString(), lstrCmd)
            sB = CargarDSXML(Ds, lstrOpc, pContext, lstrCmb, lstrOper)
         Else
            sB = EjecutarMetodo(lstrOper, lstrArgs, lstrOpc, pContext)
         End If

         pContext.Response.ContentType = "text/xml"
         pContext.Response.Write(sB.ToString())

      Catch ex As Exception
         clsError.gManejarError(ex)
      End Try
   End Sub

   Private Function CargarDSXML(ByVal DsOri As DataSet, ByVal pstrOpc As String, ByVal pContext As HttpContext, ByVal pstrCmb As String, ByVal pstrOper As String) As StringBuilder
      Try
         Dim Ds As DataSet
         Dim dt As DataTable
         Dim j As Integer
         Dim lstrOpc() As String = Split(pstrOpc, ";")
         Dim lstrCmb() As String = Split(pstrCmb, ";")
         Dim lstrOper() As String = Split(pstrOper, ";")
         Dim lDoc As New System.Xml.XmlDocument
         Dim Elem As System.Xml.XmlElement = lDoc.CreateElement("todo")
         lDoc.AppendChild(Elem)

         While DsOri.Tables.Count > 0
            Ds = New DataSet
            dt = DsOri.Tables(0)
            DsOri.Tables.Remove(dt)
            Ds.Tables.Add(dt)

            If (Ds.Tables(0).Rows.Count <> 1 Or lstrOpc(j) <> "S") Then
               Dim lDr As DataRow = Ds.Tables(0).NewRow()
               'lDr("id") = "0"
               Select Case lstrOpc(j)
                  Case "S"
                     lDr("descrip") = "(Seleccione)"
                     Ds.Tables(0).Rows.InsertAt(lDr, 0)

                  Case "N"
                     lDr("descrip") = "(Ninguno)"
                     Ds.Tables(0).Rows.InsertAt(lDr, 0)

                  Case "T"
                     lDr("descrip") = "(Todos)"
                     Ds.Tables(0).Rows.InsertAt(lDr, 0)
               End Select
            End If

            'If sB.Length > 0 Then
            '   sB.Append(";.;")
            'End If

            'sB.Append(Ds.GetXml())

            Dim elemSec As System.Xml.XmlElement = lDoc.CreateElement("tabla" & j.ToString)
            elemSec.InnerXml = Ds.GetXml()
            Elem.AppendChild(elemSec)

            If pstrOpc <> "C" Then
               pContext.Session.Add("-" + lstrCmb(j) + "_" + lstrOper(j), Ds)
            End If

            j += 1
         End While

         Dim sB As New StringBuilder
         sB.Append(lDoc.OuterXml)

         'lDoc.xp()
         'lDoc.AppendChild(elem)
         'lDoc.LoadXml(Ds.GetXml())

         'lDoc.InnerXml = Ds.GetXml()
         Return sB
      Catch ex As Exception
         clsError.gManejarError(ex)
      End Try
   End Function

   Private Function EjecutarMetodo(ByVal pstrMetodo As String, ByVal pstrArgs As String, ByVal pstrOpc As String, ByVal pContext As HttpContext) As StringBuilder
      Try
         Dim lDs As New DataSet
         Dim lDt As New DataTable
         Dim lstrResul As String

         lDs.Tables.Add(lDt)
         lDt.Columns.Add("resul", System.Type.GetType("System.String"))

         Dim lDr As DataRow = lDt.NewRow()
         Select Case pstrMetodo
            Case "Utiles.DatosEstaSocio"
               lDr("resul") = SRA_Neg.Utiles.DatosEstaSocio(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.BuscarClieDeriv"
               lDr("resul") = SRA_Neg.Utiles.BuscarClieDeriv(pContext.Session("sConn").ToString(), pstrArgs)

            Case "BuscarClieDerivCons"
               lDr("resul") = BuscarClieDerivCons(pContext.Session("sConn").ToString(), pstrArgs, pContext)

            Case "Utiles.ObtenerDescCiclo"
               lDr("resul") = SRA_Neg.Utiles.ObtenerDescCiclo(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.ObtenerFechaVigencia"
               lDr("resul") = SRA_Neg.Utiles.ObtenerFechaVigencia(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.ObtenerIdAlumXCliente"
               lDr("resul") = SRA_Neg.Utiles.ObtenerIdAlumXCliente(pContext.Session("sConn").ToString(), pstrArgs)
               'facturacion----------------------------------------
            Case "Utiles.ClienteGenerico"
               lDr("resul") = SRA_Neg.Utiles.ClienteGenerico(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.AplicarFormula"
               lDr("resul") = SRA_Neg.Utiles.AplicarFormula(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)

            Case "Utiles.CalculoConcepto"
               lDr("resul") = SRA_Neg.Utiles.CalculoConcepto(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.Tarjetas"
               lDr("resul") = SRA_Neg.Utiles.Tarjetas(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.EstadoSaldos"
               lDr("resul") = SRA_Neg.Utiles.EstadoSaldos(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.Especiales"
               lDr("resul") = SRA_Neg.Utiles.Especiales(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.AranRRGGValidacionFecha"
               lDr("resul") = SRA_Neg.Utiles.AranRRGGValidacionFecha(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.ValidarFechaMayor"
               lDr("resul") = SRA_Neg.Utiles.ValidarFechaMayor(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.CalculoArancel"
               lDr("resul") = SRA_Neg.Utiles.CalculoArancel(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
               'RRGG
            Case "Utiles.AranRRGG"
               lDr("resul") = SRA_Neg.Utiles.AranRRGG(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.AranRRGGSobretasa"
               lDr("resul") = SRA_Neg.Utiles.AranRRGGSobretasa(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)
            Case "Utiles.AranRRGGDescFecha"
               lDr("resul") = SRA_Neg.Utiles.AranRRGGDescFecha(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)

            Case "Utiles.CalcularImporteCuota"
               lDr("resul") = SRA_Neg.Utiles.CalcularImporteCuota(pstrArgs)

            Case "Utiles.LetraComprob"
               lDr("resul") = SRA_Neg.Utiles.LetraComprob(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)

            Case "Utiles.GuardarAutorizaciones"
               lDr("resul") = SRA_Neg.Utiles.GuardarAutorizaciones(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)

            Case "Utiles.SeleccionaComprobante"
               lDr("resul") = SRA_Neg.Utiles.SeleccionaComprobante(pContext.Session("sConn").ToString(), pstrArgs, pContext.Session)

               '---fin facturacion ----------------------------------
            Case "Utiles.Recargos" ' lista de precios
               lDr("resul") = SRA_Neg.Utiles.Recargos(pstrArgs)

            Case "Utiles.DatosCateSocio"
               lDr("resul") = SRA_Neg.Utiles.DatosCateSocio(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.FechasCicloMate"
               lDr("resul") = SRA_Neg.Utiles.FechasCicloMate(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.DatosEvento"
               lDr("resul") = SRA_Neg.Utiles.DatosEvento(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.DatosEventoCate"
               lDr("resul") = SRA_Neg.Utiles.DatosEventoCate(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.DatosIngreSocio"
               lDr("resul") = SRA_Neg.Utiles.DatosIngreSocio(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.RptFiltrosLimpiar"
               lDr("resul") = SRA_Neg.Utiles.RptFiltrosLimpiar(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.ObtenerInstituEntidades"
               lDr("resul") = SRA_Neg.Utiles.ObtenerInstituEntidades(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.DetallesTarjetaCliente"
               lDr("resul") = SRA_Neg.Utiles.DetallesTarjetaCliente(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.ObtenerDatosIncobCliente"
               lDr("resul") = SRA_Neg.Clientes.ObtenerDatosIncobCliente(pContext.Session("sConn").ToString(), pstrArgs)

            Case "Utiles.PlanDeudaConsulTotales"
               lDr("resul") = SRA_Neg.Utiles.PlanDeudaConsulTotales(pContext.Session("sConn").ToString(), pstrArgs)

            Case "DebiAutoEnvio.gObtenerExiste"
               lDr("resul") = DebiAutoEnvio.gObtenerExiste(pContext.Session("sConn").ToString(), pstrArgs)

            Case "ImprimirReporte"
               lDr("resul") = ImprimirReporte(pContext, pstrArgs)

            Case "ObtenerImpresora"
               lDr("resul") = ObtenerImpresora(pContext, pstrArgs)

            Case Else   'TABLAS
               Dim lstrRet As String
               If pstrArgs <> "" Then
                  Dim ldsEstr As DataSet = clsSQLServer.gObtenerEstruc(pContext.Session("sConn").ToString(), pstrMetodo, pstrArgs)
                  If ldsEstr.Tables(0).Rows.Count > 0 Then
                     Dim vstrOpc As String() = pstrOpc.Split(",")
                     For i As Integer = 0 To vstrOpc.GetUpperBound(0)
                        If i > 0 Then lstrRet += "|"
                        If Not ldsEstr.Tables(0).Rows(0).IsNull(vstrOpc(i)) Then
                           Select Case ldsEstr.Tables(0).Columns(vstrOpc(i)).DataType.ToString.ToLower
                              Case "system.datetime"
                                 lstrRet += CDate(ldsEstr.Tables(0).Rows(0).Item(vstrOpc(i))).ToString("dd/MM/yyyy")
                              Case "system.boolean"
                                 lstrRet += Math.Abs(CInt(ldsEstr.Tables(0).Rows(0).Item(vstrOpc(i)))).ToString
                              Case Else
                                 lstrRet += ldsEstr.Tables(0).Rows(0).Item(vstrOpc(i)).ToString
                           End Select
                        End If
                     Next
                  End If
                  lDr("resul") = lstrRet
               End If
         End Select

         lDt.Rows.Add(lDr)
         Dim sB As New StringBuilder
         sB.Append(lDs.GetXml())

         Return sB

      Catch ex As Exception
         clsError.gManejarError(ex)
      End Try
   End Function

   Private Function BuscarClieDerivCons(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pContext As HttpContext) As String
      Dim ldsDatos As DataSet
      Dim lstrCmd As String
      Dim lvstrArgs() As String = pstrArgs.Split(";") 'valor;tabla;campo_busc;InseId;CateTitu;CatePode;EstaId
      Dim lstrRet, lstrApel, lstrNomb As String

      If lvstrArgs(0) <> "" Then
         lstrApel = pContext.Request("apel")
         If Not pContext.Request("nomb") Is Nothing Then
            lstrNomb = pContext.Request("nomb")
         End If

         Select Case lvstrArgs(0).ToLower.Replace(".aspx", "").Replace(".", "").Replace("/", "")
            Case "socios"
               lstrCmd = Socios.ObtenerSql(0, pContext.Request("ValorId"), pContext.Request("InclBloq"), _
                           lstrApel, pContext.Request("ClieNume"), pContext.Request("SociNume"), _
                           pContext.Request("Cuit"), pContext.Request("DocuTipo"), pContext.Request("DocuNume"), _
                           "", "", pContext.Request("CateTitu"), pContext.Request("CatePode"), pContext.Request("EstaId"))

               ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd)

               If ldsDatos.Tables(0).Rows.Count = 1 Then
                  lstrRet = ldsDatos.Tables(0).Rows(0).Item("soci_nume").ToString
               End If

            Case "alumnos"
               lstrCmd = alumnos.ObtenerSql("", lstrApel, pContext.Request("ClieNume"), _
                           pContext.Request("LegaNume"), pContext.Request("Cuit"), pContext.Request("DocuTipo"), _
                           pContext.Request("DocuNume"), pContext.Request("FilInseId"), lvstrArgs(1))

               ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd)

               If ldsDatos.Tables(0).Rows.Count = 1 Then
                  lstrRet = ldsDatos.Tables(0).Rows(0).Item("alum_lega").ToString
               End If

            Case "clientes"
               lstrCmd = Clientes.ObtenerSql(pContext.Session("sUserId").ToString(), 0, pContext.Request("ClieNume"), _
                           0, lvstrArgs(1), pContext.Request("FilAgru"), pContext.Request("Agru"), 0, pContext.Request("ValorId"), _
                           lstrApel, lstrNomb, pContext.Request("SociNume"), pContext.Request("FilCriaNume"), _
                           pContext.Request("raza"), pContext.Request("CriaNume"), pContext.Request("FilNoCriaNume"), _
                           pContext.Request("NoCriaNume"), pContext.Request("FilCuit"), pContext.Request("Cuit"), _
                           pContext.Request("FilDocu"), pContext.Request("DocuTipo"), pContext.Request("DocuNume"), _
                           pContext.Request("FilTarj"), pContext.Request("TarjTipo"), pContext.Request("TarjNume"), _
                           pContext.Request("FilEnti"), pContext.Request("Enti"), pContext.Request("FilMedioPago"), _
                           pContext.Request("MedioPago"), pContext.Request("FilEmpr"), pContext.Request("Empr"), _
                           pContext.Request("FilClaveUnica"), pContext.Request("ClaveUnica"), pContext.Request("FilLegaNume"), _
                           pContext.Request("FilInseId"), pContext.Request("LegaNume"), pContext.Request("FilTipo"))

               ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd)

               If ldsDatos.Tables(0).Rows.Count = 1 Then
                  lstrRet = ldsDatos.Tables(0).Rows(0).Item("clie_id").ToString
               End If

            Case "criadores"
               lstrCmd = Criadores.ObtenerSql(pContext.Session("sUserId").ToString(), 0, pContext.Request("ClieNume"), _
                           0, lvstrArgs(1), lstrApel, lstrNomb, pContext.Request("SociNume"), pContext.Request("FilCriaNume"), _
                           pContext.Request("raza"), pContext.Request("CriaNume"), pContext.Request("FilRepreCriaNume"), _
                           pContext.Request("repre_raza"), pContext.Request("RepreCriaNume"), pContext.Request("FilCuit"), _
                           pContext.Request("Cuit"), pContext.Request("FilDocu"), pContext.Request("DocuTipo"), _
                           pContext.Request("DocuNume"), pContext.Request("FilClaveUnica"), pContext.Request("ClaveUnica"))

               ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd)

               If ldsDatos.Tables(0).Rows.Count = 1 Then
                  lstrRet = ldsDatos.Tables(0).Rows(0).Item("cria_id").ToString
               End If

         End Select
      End If

      Return (lstrRet)
   End Function

   Private Function ValorParam(ByVal pstrParam As String, ByVal pstrArgs As String) As String
      Dim lintInd As Integer = pstrArgs.IndexOf("&" & pstrParam & "=")
      If lintInd <> -1 Then
         Return (pstrArgs.Substring(lintInd, pstrArgs.IndexOf("&", lintInd) - lintInd))
      Else
         Return ""
      End If
   End Function

   Private Function ImprimirReporte(ByVal pContext As HttpContext, ByVal pstrArgs As String) As String
      Dim lvstrArgs() As String = pstrArgs.Split("|") 'reporte(|)impr_id(|)parametrosNom(|)parametrosVal
      Dim lintRet As Integer
      Dim lstrImpId As String
      Dim lDs As DataSet

      If lvstrArgs(1) <> "" Then
         lstrImpId = lvstrArgs(1)
      Else
         lstrImpId = ObtenerImpresora(pContext, lvstrArgs(0))
      End If

      If Not IsNumeric(lstrImpId) OrElse CInt(lstrImpId) = 0 Then
         'no encontr� ninguna o encontr� muchas
         Return (lstrImpId)
      Else
         lintRet = clsWeb.gImprimirReporte(pContext.Session("sConn").ToString(), pContext.Session("sUserId").ToString(), lvstrArgs(0), lvstrArgs(1), lvstrArgs(2), lvstrArgs(3), pContext.Request)
         If lintRet = 0 Then
            Return ("ERROR")
         Else
            Return (lintRet)
         End If
      End If
   End Function

   Private Function ObtenerImpresora(ByVal pContext As HttpContext, ByVal pstrReporte As String) As String
      Dim lstrImpId As String
      Dim lstrCmd As String
      Dim lDs As DataSet

      Try
         lstrCmd = "exec impresoras_reportes_busq"
         lstrCmd += " @usua_id=" + pContext.Session("sUserId").ToString()
         lstrCmd += ",@reporte=" + clsSQLServer.gFormatArg(pstrReporte, SqlDbType.VarChar)
         lstrCmd += ",@emct_id=" + SRA_Neg.Comprobantes.gCentroEmisorId(pContext.Session("sConn").ToString(), pContext.Request, True).ToString

         lDs = clsSQLServer.gExecuteQuery(pContext.Session("sConn").ToString(), lstrCmd)

         If lDs.Tables(0).Rows.Count = 0 Then
            lstrImpId = "0" 'falta impresora
         ElseIf lDs.Tables(0).Rows.Count = 1 Then
            lstrImpId = lDs.Tables(0).Rows(0).Item("impr_id")
         Else
            For Each lDr As DataRow In lDs.Tables(0).Rows
               If lstrImpId <> "" Then lstrImpId += "|"
               lstrImpId += lDr.Item("impr_id").ToString + "!" + lDr.Item("impr_desc").ToString
            Next
         End If

         Return (lstrImpId)

      Catch ex As Exception
         clsError.gManejarError(ex)
         Throw (ex)
      End Try
   End Function

   Private Sub mEscribir(ByVal pDatos As String)
      Dim lstrArchLog As String = System.Configuration.ConfigurationSettings.AppSettings("conArchLog").ToString
      Dim fs As New System.IO.FileStream(lstrArchLog, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite)

      Dim w = New System.IO.StreamWriter(fs)
      w.BaseStream.Seek(0, System.IO.SeekOrigin.End)

      w.Write(clsFormatear.CrLf() & "Fecha: " & DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") & clsFormatear.CrLf())
      w.Write(pDatos & clsFormatear.CrLf())
      w.Write("------------------------------------------------------------------------" & clsFormatear.CrLf())

      w.Flush()
      w.Close()
   End Sub
End Class