<%@ Reference Page="~/DebiAutoEnvio.aspx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AlumDebiAutoEnvio" CodeFile="AlumDebiAutoEnvio.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>D�bito Autom�tico - Env�o</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" onunload="mCerrarVentanas();" class="pagina" leftMargin="5"
		topMargin="5" rightMargin="0">
		<form onsubmit="mCerrarVentanas();" id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25" width="100%">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server">D�bito Autom�tico - Env�o</asp:label>
								</TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2" width="100%">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="97%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																	ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblTarjFil" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTarjFil" runat="server" Width="200px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblMesFil" runat="server" cssclass="titulo">Mes:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbMesFil" runat="server" Width="160px"></cc1:combobox>&nbsp;
																			<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">/</asp:Label>&nbsp;
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="35px" AceptaNull="False"
																				CantMax="4" MaxValor="2090" MaxLength="4"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center" width="100%">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="deca_id" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_gene_fecha" HeaderText="Generado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_envi_fecha" HeaderText="Env�o" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_rece_fecha" HeaderText="Recepci�n" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_tarj_desc" HeaderText="Tarjeta"></asp:BoundColumn>
											<asp:BoundColumn DataField="_perio" HeaderText="Per�odo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cant" HeaderText="Cant."></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="1">&nbsp;</TD>
								<TD vAlign="middle" width="100%">
									<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar Nuevo D�bito"></CC1:BotonImagen>
								</TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> D�bito</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" CausesValidation="False" Height="21px"> Alumnos</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
											BorderWidth="1px" Visible="False">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" width="200">
																			<asp:Label id="lblGeneFecha" runat="server" cssclass="titulo">Fecha Generaci�n:</asp:Label></TD>
																		<TD>
																			<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
																				<TR>
																					<TD>
																						<cc1:DateBox id="txtGeneFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																					<TD align="right">
																						<asp:Label id="lblEnviFecha" runat="server" cssclass="titulo">Fecha Env�o:</asp:Label>&nbsp;</TD>
																					<TD>
																						<cc1:DateBox id="txtEnviFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnio" runat="server" cssclass="titulo">Mes:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbMes" runat="server" Width="160px"></cc1:combobox>&nbsp;
																			<asp:Label id="Label2" runat="server" cssclass="titulo">/</asp:Label>&nbsp;
																			<cc1:numberbox id="txtAnio" runat="server" cssclass="cuadrotexto" Width="35px" AceptaNull="False"
																				CantMax="4" MaxValor="2090" MaxLength="4"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label></TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbTarj" runat="server" Width="150px" enabled="false" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnBajaListaTodos" runat="server" cssclass="boton" Width="165px" Text="Borrar Todos y Regenerar"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="4">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="Table4" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdDeta" runat="server" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosDeta"
																				OnPageIndexChanged="grdDeta_PageChanged" AutoGenerateColumns="False" width="95%">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="dede_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_alum_lega" HeaderText="Legajo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_clie_apel" HeaderText="Cliente"></asp:BoundColumn>
																					<asp:BoundColumn DataField="dede_envio_impo" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																						HeaderText="Env�o"></asp:BoundColumn>
																					<asp:BoundColumn DataField="dede_total_impo" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																						HeaderText="Total"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_coti_desc" HeaderText="Cuota"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_comp" HeaderText="Comprobantes"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_auto" HeaderText="Auto"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_se_factura" HeaderText="Se Factura"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="dede_anio" HeaderText="dede_anio"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="dede_perio" HeaderText="dede_perio"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="dede_deca_id" HeaderText="dede_deca_id"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="dede_comp_id" HeaderText="dede_comp_id"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="center" colSpan="3">
																			<asp:panel id="panClie" runat="server" cssclass="titulo" Width="95%" BorderWidth="1px">
																				<TABLE id="TabClie" style="WIDTH: 95%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD vAlign="top" align="right" width="50">
																							<asp:Label id="lblAlum" runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;
																						</TD>
																						<TD>
																							<UC1:CLIE id="usrAlum" runat="server" FilDocuNume="True" FilClie="True" FilSociNume="True"
																								autopostback="true" Alto="560" CampoVal="Alumno" Ancho="780" Saltos="1,1,1" Tabla="Alumnos"
																								FilCUIT="True"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"></TD>
																						<TD>
																							<TABLE style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																								<TR>
																									<TD align="right">
																										<asp:Label id="lblImpo" runat="server" cssclass="titulo">Imp.a Enviar:</asp:Label></TD>
																									<TD>
																										<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="50px" EsDecimal="True"></cc1:numberbox></TD>
																									<TD align="right">
																										<asp:Label id="lblImpoOrig" runat="server" cssclass="titulo">Imp.Total:</asp:Label></TD>
																									<TD>
																										<cc1:numberbox id="txtImpoOrig" runat="server" cssclass="cuadrotextodeshab" Width="50px" EsDecimal="True"
																											Enabled="false"></cc1:numberbox></TD>
																									<TD align="right">
																										<asp:Label id="lblTaclNume" runat="server" cssclass="titulo">Tarjeta:</asp:Label></TD>
																									<TD>
																										<asp:TextBox id="txtTaclNume" runat="server" cssclass="cuadrotextodeshab" Width="100px" Enabled="false"></asp:TextBox></TD>
																									<TD align="right">
																										<asp:Label id="lblAutoTit" runat="server" cssclass="titulo">Auto.:</asp:Label></TD>
																									<TD>
																										<asp:Label id="lblAuto" runat="server" cssclass="desc">NO</asp:Label></TD>
																									<TD align="right">
																										<asp:Label id="lblCotiDesc" runat="server" cssclass="titulo"></asp:Label>&nbsp;
																									</TD>
																								</TR>
																								<TR>
																									<TD vAlign="top" align="right">
																										<asp:Label id="lblObse" runat="server" cssclass="titulo">Obs.:</asp:Label>&nbsp;
																									</TD>
																									<TD colSpan="8">
																										<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" EnterPorTab="False"
																											TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																								</TR>
																							</TABLE>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="3"><BUTTON class="boton" id="btnDeuda" style="WIDTH: 135px" onclick="btnDeuda_click();" type="button"
																								runat="server" value="Incluir otras cuotas">Incluir otras deudas</BUTTON></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="center" colSpan="2" height="30">
																							<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Agregar"></asp:Button>&nbsp;
																							<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="80px" Text="Eliminar"></asp:Button>&nbsp;
																							<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="80px" Text="Modificar"></asp:Button>&nbsp;
																							<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" Text="Limpiar"></asp:Button></TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel>
										<ASP:PANEL Runat="server" ID="panBotones">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Generar"></asp:Button>&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button>&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnEnviar" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Enviar" Font-Bold="True"></asp:Button>
														<CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
															ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
															ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BOTONIMAGEN></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<asp:textbox id="hdnDatosPop" runat="server" Width="400"></asp:textbox>
				<asp:textbox id="txtAnteImpo" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
				<asp:textbox id="hdnDedeId" runat="server"></asp:textbox>
				<asp:textbox id="hdnTaclId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCompId" runat="server"></asp:textbox>
				<asp:textbox id="hdnExpo" runat="server"></asp:textbox>
				<asp:textbox id="hdnExpo2" runat="server"></asp:textbox>
				<asp:textbox id="hdnEtapa" runat="server"></asp:textbox>
				<asp:textbox id="hdnAlumId" runat="server"></asp:textbox>
				<asp:textbox id="hdnAnio" runat="server"></asp:textbox>
				<asp:textbox id="hdnPerio" runat="server"></asp:textbox>
				<asp:textbox id="hdnFecha" runat="server"></asp:textbox>
				<cc1:numberbox id="txtDedeTotalImpo" runat="server" cssclass="cuadrotexto" Width="50px" EsDecimal="True"></cc1:numberbox>
				<cc1:numberbox id="txtDedeEnvioImpo" runat="server" cssclass="cuadrotexto" Width="50px" EsDecimal="True"></cc1:numberbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		var winArch;
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();

		if (document.all("hdnExpo").value!='')
		{
		
		    //var winArch = window.open(document.all("hdnExpo").value);
			window.open("Descarga.aspx?archivo=" + document.all("hdnExpo").value, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=20px,height=10px,left=0px,top=0px");
			document.all("hdnExpo").value='';
			
			//winArch = window.open("descarga.aspx?archivo=" + document.all("hdnExpo").value, "", "location=no,menubar=no,scrollbars=no,status=no,titlebar=no,toolbar=no,width=50px,height=50px,left=50px,top=50px");
			//document.all("hdnExpo").value='';
		}
		
		if (document.all("hdnExpo2").value!='')
		{
		
		    var winArch = window.open(document.all("hdnExpo2").value);
			document.all("hdnExpo2").value='';
		
			//winArch = window.open("descarga.aspx?archivo=" + document.all("hdnExpo2").value, "", "location=no,menubar=no,scrollbars=no,status=no,titlebar=no,toolbar=no,width=50px,height=50px,left=75px,top=50px");
			//document.all("hdnExpo2").value='';
		}
		
		function mCerrarVentanas()
		{
			if(winArch!=null)
			{
				winArch.close();
				winArch=null;
			}
		}
		
		function btnDeuda_click()
		{
		
			if(document.all("hdnAlumId").value=="")
			{
				alert("Debe seleccionar un alumno");
				return;
			}

			var lstrPagina = "AlumDeuda_pop.aspx?alum_id=" + document.all("hdnAlumId").value + "&anio=" + document.all("hdnAnio").value + "&perio=" + document.all("hdnPerio").value + "&fecha=" + document.all("hdnFecha").value;
			//var lstrPagina = "CobranzasFact_Pop.aspx?tipo=D&alum_id=" + document.all("hdnAlumId").value + "&fecha=" + document.all("hdnFecha").value + "&venc=0&isea=1";
			Ventana = window.open (lstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=700px,height=500px,left=20,top=20");
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		</SCRIPT>
	</BODY>
</HTML>
