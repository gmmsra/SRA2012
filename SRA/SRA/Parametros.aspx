<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Parametros" CodeFile="Parametros.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Par�metros</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultgrupntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" onunload="gCerrarVentanas();" onload="gSetearTituloFrame('');" leftMargin="5"
		rightMargin="0" topMargin="5">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Par�metros</asp:label></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD height="10" colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3" align="right">
									<table id="tabLinks" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
										<TR>
											<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
											<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
											<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
											<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> General</asp:linkbutton></TD>
											<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
											<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkFacturacion" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Facturaci�n</asp:linkbutton></TD>
											<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
											<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkSocios" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Socios</asp:linkbutton></TD>
											<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
											<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkPagosMisCuentas" runat="server"
													cssclass="solapa" Width="120px" CausesValidation="False" Height="21px"> Pagos Mis Cuentas</asp:linkbutton></TD>
											<TD background="imagenes/tab_fondo.bmp" width="1"></TD>
											<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
										</TR>
									</table>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Height="50%" BorderStyle="Solid"
										BorderWidth="1px" Visible="False">
										<TABLE style="WIDTH: 100%; HEIGHT: 100%" id="Table2" class="FdoFld" border="0" cellPadding="0"
											align="left">
											<TR>
												<TD height="5">
													<asp:label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
														<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblMonedaLoc" runat="server" cssclass="titulo">Moneda Local:</asp:label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbMonedaLoc" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblMonedaExt" runat="server" cssclass="titulo">Moneda Extranjera:</asp:label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbMonedaExt" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblDocuPath" runat="server" cssclass="titulo">Path Documento Socio:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<CC1:TEXTBOXTAB id="txtDocuPath" runat="server" cssclass="cuadrotexto" Width="90%" obligatorio="True"
																		AceptaNull="False"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblFotoPath" runat="server" cssclass="titulo">Path Foto Socio:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<CC1:TEXTBOXTAB id="txtFotoPath" runat="server" cssclass="cuadrotexto" Width="90%" obligatorio="True"
																		AceptaNull="False"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblRazaPath" runat="server" cssclass="titulo">Path Documentos Razas:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<CC1:TEXTBOXTAB id="txtRazaPath" runat="server" cssclass="cuadrotexto" Width="90%" obligatorio="True"
																		AceptaNull="False"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblTempPath" runat="server" cssclass="titulo">Path Temporal:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<CC1:TEXTBOXTAB id="txtTempPath" runat="server" cssclass="cuadrotexto" Width="90%" obligatorio="True"
																		AceptaNull="False"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblCantGrilla" runat="server" cssclass="titulo">Cantidad de Registros por pantalla por grilla:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtCantGrilla" runat="server" cssclass="cuadrotexto" Width="60px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblCantBoviServ" runat="server" cssclass="titulo">Cantidad maxima servicios (Bovinos/Equinos/Camelidos/Porcinos)</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtCantBoviServ" runat="server" cssclass="cuadrotexto" Width="60px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblCantCaprServ" runat="server" cssclass="titulo">Cantidad maxima servicios (Caprinos/Ovinos)</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtCantCaprServ" runat="server" cssclass="cuadrotexto" Width="60px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblEnvioEmails" runat="server" cssclass="titulo">Cuenta de env�o de emails:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:TEXTBOXTAB id="txtEnvioEmails" runat="server" cssclass="cuadrotexto" Width="90%" AceptaNull="False"
																		EsMail="True"></cc1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblAdmEmails" runat="server" cssclass="titulo">Cuenta de administraci�n de emails:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:TEXTBOXTAB id="txtAdmEmails" runat="server" cssclass="cuadrotexto" Width="90%" AceptaNull="False"
																		EsMail="True"></cc1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:Label id="lblSraAsoc" runat="server" cssclass="titulo">Asociaci�n SRA:</asp:Label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:combobox id="cmbSraAsoc" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:Label id="lblExpo" runat="server" cssclass="titulo">Exposici�n Aves:</asp:Label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:combobox id="cmbExpo" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panEstu" runat="server" cssclass="titulo" Width="100%" Visible="False">
														<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
															<TR>
																<TD width="40%" align="right">
																	<asp:Label id="lblIVAPosic" runat="server" cssclass="titulo">IVA Consumidor Final:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbIVAPosic" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblClientes" runat="server" cssclass="titulo">Clientes:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbClientes" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblProvincias" runat="server" cssclass="titulo">Provincias:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbProvincias" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 69px" align="right">
																	<asp:Label id="lblEmisoresCtro" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 69px">
																	<cc1:combobox id="cmbEmisoresCtro" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblClaveUnica" runat="server" cssclass="titulo">Numerador de Clave �nica:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtClaveUnica" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblMesCierre" runat="server" cssclass="titulo">Mes Cierre Contable:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtFechaInicio" runat="server" cssclass="cuadrotexto" Width="70px" Visible="False"></cc1:DateBox>
																	<cc1:combobox id="cmbMesCierre" class="combo" runat="server" Width="98px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblFechaCierre" runat="server" cssclass="titulo" Visible="False">Fecha cierre del ejercicio contable:</asp:Label></TD>
																<TD>
																	<cc1:DateBox id="txtFechaCierre" runat="server" cssclass="cuadrotexto" Width="70px" Visible="False"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblNroRRGG" runat="server" cssclass="titulo">�ltimo N�mero de Tr�mite de RRGG:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtNroRRGG" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblMontoInt" runat="server" cssclass="titulo">Monto m�nimo de Inter�s para ND:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtMontoInt" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblFactLab" runat="server" cssclass="titulo">Laboratorio para Facturaci�n:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtFactLab" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblDiasVtoCAI" runat="server" cssclass="titulo">Cantidad de d�as para aviso de Vto. de CAI:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDiasVtoCAI" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblAceptaCheque" runat="server" cssclass="titulo">Aceptar Cheques hasta(d�as):</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtAceptaCheque" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblLeyendaDol" runat="server" cssclass="titulo">Leyenda fija por Facturas en D�lares:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:TEXTBOXTAB id="txtLeyendaDol" runat="server" cssclass="cuadrotexto" Width="80%" obligatorio="True"
																		AceptaNull="False"></cc1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblDiasCierreArqueo" runat="server" cssclass="titulo">Permitir arqueo sin cerrar(d�as):</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDiasCierreArqueo" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblRecaProfDias" runat="server" cssclass="titulo">D�as para Recalculo de Proformas:</asp:Label></TD>
																<TD>
																	<cc1:numberbox id="txtRecaProfDias" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblFacProformaSinRecargoDesdeFecha" runat="server" cssclass="titulo" Visible="true">Facturar Proforma sin recargo desde Fecha:</asp:Label></TD>
																<TD>
																	<cc1:DateBox id="txtFacProformaSinRecargoDesdeFecha" runat="server" cssclass="cuadrotexto" Width="70px"
																		Visible="true"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" vAlign="middle" align="right">
																	<asp:Label id="lblconceptoDescuentoBonifLabAuto" runat="server" cssclass="titulo">Concepto x Bonificaci�n de Laboratotio:</asp:Label>&nbsp;
																</TD>
																<TD vAlign="middle" align="left">
																	<TABLE border="0" cellSpacing="0" cellPadding="0">
																		<TR>
																			<TD>
																				<cc1:combobox id="cmbConceptoDescuentoBonifLabAuto" class="combo" runat="server" cssclass="cuadrotexto"
																					Height="20px" Width="350px" Obligatorio="True" MostrarBotones="False" NomOper="conceptos_cargar"
																					filtra="true" TextMaxLength="7"></cc1:combobox></TD>
																			<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																					id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbConceptoDescuentoBonifLabAuto','Concepto x Bonificaci�n de Laboratotio','');"
																					border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblIIBBPerc" runat="server" cssclass="titulo">SRA Agente de Percepci�n IIBB:</asp:Label></TD>
																<TD>
																	<asp:CheckBox id="chkIIBBPerc" CssClass="titulo" Checked="false" Text="" Runat="server"></asp:CheckBox></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblIVAPerc" runat="server" cssclass="titulo">SRA Agente de Percepci�n IVA:</asp:Label></TD>
																<TD>
																	<asp:CheckBox id="chkIVAPerc" CssClass="titulo" Checked="false" Text="" Runat="server"></asp:CheckBox></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panOcup" runat="server" cssclass="titulo" Width="100%" Visible="False">
														<TABLE style="WIDTH: 100%" id="Table4" border="0" cellPadding="0" align="left">
															<TR>
																<TD width="40%" align="right">
																	<asp:Label id="lblCategorias" runat="server" cssclass="titulo">Categor�as:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbCategorias" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblInstituciones" runat="server" cssclass="titulo">Instituciones:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbInstituciones" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblDiasIng" runat="server" cssclass="titulo">D�as de vigencia de la Solicitud de Ingreso:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDiasIng" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblCantAnios" runat="server" cssclass="titulo">Cantidad de a�os para ser socio Vitalicio:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCantAnios" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblBimestre" runat="server" cssclass="titulo">Bimestre en Curso:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtBimestre" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblMayorEdad" runat="server" cssclass="titulo">A�os para mayor�a de edad:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtMayorEdad" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblMenorEdad" runat="server" cssclass="titulo">A�os para categor�a Menor:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtMenorEdad" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblFechaIntFlot" runat="server" cssclass="titulo">Fecha Tasa de Inter�s Flotantes Ctas. Soc.:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtFechaIntFlot" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblFechaRevaluo" runat="server" cssclass="titulo">Fecha de �ltimo revaluo:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtFechaRevaluo" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblCantBim" runat="server" cssclass="titulo">M�xima cantidad de Bimestres Adeudados:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCantBim" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblSociUltNume" runat="server" cssclass="titulo">�ltimo Nro. de Socio:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtSociUltNume" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 17px" align="right">
																	<asp:Label id="lblTopePoder" runat="server" cssclass="titulo">Cantidad Tope de Poderdantes por apoderado:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 17px">
																	<cc1:numberbox id="txtTopePoder" runat="server" cssclass="cuadrotexto" Width="62px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblSociTope" runat="server" cssclass="titulo">Contar socios que son firmas de socios (sociedades) para control de este tope:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbSociTope" class="combo" runat="server" Width="95px" obligatorio="True">
																		<ASP:LISTITEM Value="null">(Seleccione)</ASP:LISTITEM>
																		<ASP:LISTITEM Value="S">Si</ASP:LISTITEM>
																		<ASP:LISTITEM Value="N">No</ASP:LISTITEM>
																	</cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:label id="lblSocioFallecido" runat="server" cssclass="titulo">Prorroga precio socio fallecido (meses):</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtProSocioFallecido" runat="server" cssclass="cuadrotexto" Width="60px" obligatorio="True"
																		AceptaNull="False" MaxValor="100"></cc1:numberbox></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panPagosMisCuentas" runat="server" cssclass="titulo" Width="100%" Visible="False">
														<TABLE style="WIDTH: 100%" id="Table5" border="0" cellPadding="0" align="left">
															<TR>
																<TD style="WIDTH: 40%" align="right">
																	<asp:Label id="lblNroEstablePmC" runat="server" cssclass="titulo">N�mero del Establecimiento:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtNroEstablePmC" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" align="right">
																	<asp:Label id="lblEmpresaAsingnadoPmC" runat="server" cssclass="titulo">N�mero de Empresa Asignado:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtEmpresaAsingnadoPmC" runat="server" cssclass="cuadrotexto" Width="100px"
																		obligatorio="True" AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" align="right">
																	<asp:Label id="lblNroBanelcoPmC" runat="server" cssclass="titulo">N�mero de Banelco:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtNroBanelcoPmC" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" vAlign="middle" align="right">
																	<asp:Label id="lblDescuentoPmC" runat="server" cssclass="titulo">Descuento por Comisi�n:</asp:Label>&nbsp;</TD>
																<TD vAlign="middle" align="left">
																	<TABLE border="0" cellSpacing="0" cellPadding="0">
																		<TR>
																			<TD>
																				<cc1:combobox id="cmbDescuentoPmC" class="combo" runat="server" cssclass="cuadrotexto" Height="20px"
																					Width="350px" Obligatorio="True" MostrarBotones="False" NomOper="conceptos_cargar" filtra="true"
																					TextMaxLength="7"></cc1:combobox></TD> <!--onchange="javascript:mSetearCuenta('txtcmbDescuento', 'cmbSociDctoCcos', '1');"-->
																			<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																					id="btnAvanBusq2" onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbDescuentoPmC','Descuento por Comisi�n','');"
																					border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:Label id="lblTipoComprobantePmC" runat="server" cssclass="titulo">Tipo Comprobante Porvisorio:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbTipoComprPmC" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD width="40%" align="right">
																	<asp:Label id="lblCuentaBcoDepositoPmC" runat="server" cssclass="titulo">Cuenta Bancaria de Dep�sito:</asp:Label>&nbsp;
																</TD>
																<TD>
																	<cc1:combobox id="cmbCuentaBcoDepositoPmC" class="combo" runat="server" Width="85%" obligatorio="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" vAlign="middle" align="right">
																	<asp:Label id="lblBcoOrigDepositoPmC" runat="server" cssclass="titulo">Banco de Origen del Dep�sito:</asp:Label>&nbsp;</TD>
																<TD vAlign="middle" align="left">
																	<TABLE border="0" cellSpacing="0" cellPadding="0">
																		<TR>
																			<TD>
																				<cc1:combobox id="cmbBcoOrigDepositoPmC" class="combo" runat="server" cssclass="cuadrotexto" Height="20px"
																					Width="350px" Obligatorio="True" MostrarBotones="False" NomOper="bancos_cargar" filtra="true"
																					TextMaxLength="7"></cc1:combobox></TD>
																			<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																					id="btnAvanBusq3" onclick="mBotonBusquedaAvanzada('bancos','banc_desc','cmbBcoOrigDepositoPmC','Banco de Origen del Dep�sito','');"
																					border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" align="right">
																	<asp:Label id="lblProcComisionPmC" runat="server" cssclass="titulo">% de Comision:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtPorcComisionPmC" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False" EsDecimal="True" CantMax="2"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" align="right">
																	<asp:Label id="lblMontoMinimoComisionPmC" runat="server" cssclass="titulo">Monto Minimo de Comision:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtMontoMinimoComisionPmC" runat="server" cssclass="cuadrotexto" Width="100px"
																		obligatorio="True" AceptaNull="False" EsDecimal="True" CantMax="2"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 40%" align="right">
																	<asp:Label id="lblIvaComisionPmC" runat="server" cssclass="titulo">% Iva Sobre Comision:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtIvaComisionPmC" runat="server" cssclass="cuadrotexto" Width="100px" obligatorio="True"
																		AceptaNull="False" EsDecimal="True" CantMax="2"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3"></TD>
											</TR>
										</TABLE>
									</asp:panel><ASP:PANEL id="panBotones" Height="50%" Runat="server">
										<TABLE width="100%">
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Modificar"></asp:Button>&nbsp;&nbsp;
												</TD>
											</TR>
										</TABLE>
									</ASP:PANEL></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
