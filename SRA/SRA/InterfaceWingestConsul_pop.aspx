<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InterfaceWingestConsul_pop" CodeFile="InterfaceWingestConsul_pop.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Consulta de Archivos Interface Wingest </title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!-----------------CONTENIDO---------------------->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
							<TR>
								<TD vAlign="top" align="right" width="100%" colSpan="6">
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"></asp:imagebutton>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 30px" vAlign="middle" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Consulta de Archivos Interface Wingest</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="left" colSpan="4"><asp:label id="lblArch" runat="server" cssclass="titulo">Archivo:</asp:label>&nbsp;
									<CC1:TEXTBOXTAB id="txtArch" runat="server" cssclass="cuadrotexto" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="4">
									<asp:label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:label>&nbsp;
									<cc1:datebox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:datebox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:label>&nbsp;
									<cc1:datebox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:datebox></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="left" colSpan="4"></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top" colSpan="6"><asp:datagrid id="grdConsulta" runat="server" BorderStyle="None" AllowPaging="True" BorderWidth="1px"
										HorizontalAlign="Center" CellPadding="1" CellSpacing="1" GridLines="None" OnPageIndexChanged="DataGrid_Page" PageSize="10"
										ItemStyle-Height="5px" AutoGenerateColumns="False" PagerStyle-CssClass="titulo" width="100%">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" style="font-color:black;font-size:10pt;" onclick="chkSelCheck('chkSel',this);"
														Runat="server"></asp:CheckBox>
													<DIV runat="server" id="impo_id" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.impo_id")%></DIV>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="impo_id" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="impo_nomb_arch" HeaderText="Nombre Archivo"></asp:BoundColumn>
											<asp:BoundColumn DataField="impo_recep_fecha" HeaderText="Fecha Recepción" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="bottom" align="right" colSpan="4" height="28"><asp:button id="btnConsul" runat="server" CausesValidation="False" cssclass="boton" Width="80px"
										Text="Consultar"></asp:button>&nbsp;&nbsp;&nbsp;
									<asp:button id="btnListar" runat="server" CausesValidation="False" cssclass="boton" Width="80px"
										Text="Listar"></asp:button></TD>
							</TR>
						</TABLE>
					<!----------------CONTENIDO-------------->
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnConf" runat="server" AutoPostBack="True"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnTipo" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnImpoID" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnNombArch" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function chkSelCheck(oControl,pChk)
		{		
			var re = new RegExp(':' + oControl + '$');
			var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
			var impoid = eval(document.all("grdConsulta__ctl" + lintFil.toString() + "_impo_id").innerText);
			document.all("hdnImpoID").value = impoid;
			var nombArch = document.getElementById('grdConsulta').rows[lintFil-2].cells[1].innerHTML;
			document.all("hdnNombArch").value = nombArch;	
			
			if (pChk.checked)
			{
				for (var i=1;i<=document.getElementById('grdConsulta').rows.length;i++)
				{
					elm = document.forms[0].elements[i+3];
					if (elm.type == 'checkbox')
					{
						if (eval(document.all("grdConsulta__ctl" + i.toString() + "_impo_id").innerText) == impoid )
						{
							if (re.test(elm.name))
								elm.checked = true;
						}
						else
						{
							if (re.test(elm.name))
								elm.checked = false;
						}
					}
				}
			}
			else
			{
				document.all("hdnImpoID").value = "";			
			}
		}
		</SCRIPT>
	</BODY>
</HTML>
