<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Alertas_pop" CodeFile="Alertas_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function btnOtrasCtas_click(pstrTipo)
		{
          gAbrirVentanas("reportes/DeudaClienteAgru_pop.aspx?clie_id=" + document.all("hdnClieId").value 
					 	+ "&fechaValor=" + document.all("hdnFechaValor").value
					 	+ "&sociId=" + document.all("hdnSociId").value
					 	, 2, "450","300");
	 	}		
		function btnDeudaCte_click(pstrTipo,pstrTipoB)
		{
			document.all("hdnTipoPop").value = pstrTipo;

			var lstrPagina = "CobranzasFact_Pop.aspx?tipo=" + pstrTipo + "&clie_id=" + document.all("hdnClieId").value + "&fecha=" + document.all("hdnFecha").value + "&venc=" + pstrTipoB;
			Ventana = window.open (lstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=500px,height=500px");
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		function btnDeudaSocial_click(pstrTipo)
		{
			
       		var lstrPagina = "SociDeuda_Pop.aspx?dev=S" + "&soci_id=" + document.all("hdnSociId").value + "&venc=" + pstrTipo;
			Ventana = window.open (lstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=600px,height=500px");
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		
		function mProformasAbrir()
	   	{
		 gAbrirVentanas("proforma_pop.aspx?EsConsul=1"+"&cliente="+document.all("hdnClieId").value+"&amp;"+"fecha_valor="+document.all("hdnFecha").value, 7);
	    }		
	    function btnAcuses_click()
	    {
		 gAbrirVentanas("reportes/DeudaClienteAcuses.aspx?clie_id="+document.all("hdnClieId").value, 5);
		}
		function btnACtaCtaSocial_click()
		{
			document.all("hdnTipoPop").value="A";

			var lstrPagina = "CtaCteACta_Pop.aspx?social=1&clie_id=" + document.all("hdnClieId").value;
			Ventana = window.open (lstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=550px,height=500px");
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		function btnACtaCtaCte_click()
		{
			document.all("hdnTipoPop").value="A";

			var lstrPagina = "CtaCteACta_Pop.aspx?social=0&clie_id=" + document.all("hdnClieId").value;
			Ventana = window.open (lstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=550px,height=500px");
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%" id="Table1" border="0">
							<TR>
								<TD vAlign="middle" align="right"><asp:imagebutton id="btnCerrar" runat="server" ImageUrl="imagenes/close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton>&nbsp;
								</TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="2" align="center"><asp:panel id="panEspeciales" runat="server" height="100%" BorderStyle="Solid" BorderWidth="1px"
										Width="100%" cssclass="titulo" Visible="False">
										<TABLE style="WIDTH: 100%" id="tblEspeciales" class="FdoFld" border="0" cellPadding="0"
											align="left">
											<TR>
												<TD style="WIDTH: 50%" align="right">
													<asp:label id="lblExentaT" runat="server" cssclass="titulo">Facturaci�n exenta:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 50%">
													<CC1:TEXTBOXTAB id="txtExenta" runat="server" cssclass="cuadrotexto" Width="25px" Enabled="False"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 50%" align="right">
													<asp:label id="lblPercepT" runat="server" cssclass="titulo">Sujeto a Percepci�n I.V.A:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 50%">
													<CC1:TEXTBOXTAB id="txtPercep" runat="server" cssclass="cuadrotexto" Width="25px" Enabled="False"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 50%" align="right">
													<asp:Label id="lblIIBB" runat="server" cssclass="titulo">Categor�a IIBB:</asp:Label>&nbsp;
												</TD>
												<TD style="WIDTH: 50%">
													<cc1:combobox id="cmbIIBB" class="combo" tabIndex="3" runat="server" Width="130px" Enabled="False"></cc1:combobox>&nbsp;&nbsp;
												</TD>
											</TR>
											<TR>
												<TD style="WIDTH: 50%" align="right">
													<asp:Label id="lblNroInsc" runat="server" cssclass="titulo">Nro. Insc. IIBB:</asp:Label>&nbsp;
												</TD>
												<TD style="WIDTH: 50%">
													<CC1:TEXTBOXTAB id="txtNroInsc" tabIndex="4" runat="server" cssclass="cuadrotexto" Width="110px"
														Enabled="False"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 50%" align="right">
													<asp:label id="lblPrecSociT" runat="server" cssclass="titulo">Fact. a precio de socio:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 50%">
													<CC1:TEXTBOXTAB id="txtPrecSoci" runat="server" cssclass="cuadrotexto" Width="25px" Enabled="False"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 50%" align="right">
													<asp:label id="lblMiemCDT" runat="server" cssclass="titulo">Miembro de Comisi�n Directiva:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 50%">
													<CC1:TEXTBOXTAB id="txtMiemCD" runat="server" cssclass="cuadrotexto" Width="25px" Enabled="False"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 50%" colSpan="2" align="left">
													<asp:Label id="lblObser" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;
												</TD>
											</TR>
											<TR>
												<TD background="imagenes/formfdofields.jpg" colSpan="2">
													<CC1:TEXTBOXTAB id="txtObser" runat="server" cssclass="textolibredeshab" Width="100%" Height="54px"
														EnterPorTab="False" TextMode="MultiLine" enabled="false" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="2" align="center"><asp:panel id="panEstadoSaldos" runat="server" height="100%" BorderStyle="Solid" BorderWidth="1px"
										Width="100%" cssclass="titulo" Visible="false">
										<TABLE style="WIDTH: 100%" id="tblEstadoSaldos" class="FdoFld" border="0" cellPadding="0"
											align="left">
											<TR>
												<TD style="WIDTH: 45.05%" align="right">
													<asp:label id="lblSaldoCaCte" runat="server" cssclass="titulo">Saldo de Cta. Cte. (Total):</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 60%">
													<CC1:NUMBERBOX id="txtSaldoCaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="False"></CC1:NUMBERBOX>&nbsp;
													<A id="btnDeudaCte" href="javascript:btnDeudaCte_click('D','');" runat="server"><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Total Saldo" src="imagenes/Buscar16.gif"></A> <A id="btnDeudaCte2" href="javascript:alert('No hay datos para mostrar');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Total Saldo" src="imagenes/Buscar16.gif"></A>&nbsp;&nbsp;
													<asp:label id="Label2" runat="server" cssclass="titulo">(Vencida):</asp:label>&nbsp;
													<CC1:NUMBERBOX id="txtSaldoCaCteVenc" runat="server" cssclass="cuadrotextodeshab" Width="65px"
														Enabled="False"></CC1:NUMBERBOX>&nbsp; <A id="btnDeudaCteVen" href="javascript:btnDeudaCte_click('D','V');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Saldo Venc." src="imagenes/Buscar16.gif"></A> <A id="btnDeudaCteVen2" href="javascript:alert('No hay datos para mostrar');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Saldo Venc." src="imagenes/Buscar16.gif"></A>
												</TD>
											</TR>
											<TR>
												<TD style="WIDTH: 45.05%; HEIGHT: 24px" align="right">
													<asp:label id="lblSaldoCtaSoci" runat="server" cssclass="titulo">Saldo de Cta. Social (Total):</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 60%; HEIGHT: 24px">
													<CC1:NUMBERBOX id="txtSaldoCtaSoci" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="False"></CC1:NUMBERBOX>&nbsp;
													<A id="btnDeudaSocial" href="javascript:btnDeudaSocial_click('');" runat="server"><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Total Saldo" src="imagenes/Buscar16.gif"></A> <A id="btnDeudaSocial2" href="javascript:alert('No hay datos para mostrar');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Total Saldo" src="imagenes/Buscar16.gif"></A>&nbsp;&nbsp;
													<asp:label id="Label3" runat="server" cssclass="titulo">(Vencida):</asp:label>&nbsp;
													<CC1:NUMBERBOX id="txtSaldoCtaSociVenc" runat="server" cssclass="cuadrotextodeshab" Width="65px"
														Enabled="False"></CC1:NUMBERBOX>&nbsp; <A id="btnDeudaSocialVda" href="javascript:btnDeudaSocial_click('V');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Saldo Venc." src="imagenes/Buscar16.gif"></A> <A id="btnDeudaSocialVda2" href="javascript:alert('No hay datos para mostrar');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Saldo Venc." src="imagenes/Buscar16.gif"></A>
												</TD>
											</TR>
											<TR>
												<TD style="WIDTH: 45.05%; HEIGHT: 23px" align="right">
													<asp:label id="Label5" runat="server" cssclass="titulo">Saldo de Otras Ctas.  (Total):</asp:label>&nbsp;</TD>
												<TD style="WIDTH: 60%; HEIGHT: 23px">
													<CC1:NUMBERBOX id="txtSaldoOtrasCtas" runat="server" cssclass="cuadrotextodeshab" Width="65px"
														Enabled="False"></CC1:NUMBERBOX>&nbsp; <A id="btnOtrasCtas" href="javascript:btnOtrasCtas_click('');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Total Saldo" src="imagenes/Buscar16.gif"></A> <A id="btnOtrasCtas2" href="javascript:alert('No hay datos para mostrar');" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Total Saldo" src="imagenes/Buscar16.gif"></A> &nbsp;&nbsp;
													<asp:label id="Label6" runat="server" cssclass="titulo">(Vencida):</asp:label>&nbsp;
													<CC1:NUMBERBOX id="txtSaldoOtrasCtasVenc" runat="server" cssclass="cuadrotextodeshab" Width="65px"
														Enabled="False"></CC1:NUMBERBOX>&nbsp;
												</TD>
											</TR>
											<TR>
												<TD style="WIDTH: 45.05%; HEIGHT: 23px" align="right">
													<asp:label id="lblTotalSaldo" runat="server" cssclass="titulo">Total:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 60%; HEIGHT: 23px">
													<CC1:NUMBERBOX id="txtTotalSaldo" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="False"></CC1:NUMBERBOX></TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 45.05%" align="right">
													<asp:label id="lblImpCuentaCtaCte" runat="server" cssclass="titulo">Importe a cuenta en Cta. Cte.:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 60%">
													<CC1:NUMBERBOX id="txtImpCuentaCtaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
														Enabled="False"></CC1:NUMBERBOX>&nbsp; <A id="btnACtaCtaCte" href="javascript:btnACtaCtaCte_click();" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Saldo a Cuenta Cta Cte" src="imagenes/Buscar16.gif"></A>
												</TD>
											</TR>
											<TR>
												<TD style="WIDTH: 45.05%" align="right">
													<asp:label id="lblImpCuentaCtaSocial" runat="server" cssclass="titulo">Importe a cuenta en Cta. Social:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 60%">
													<CC1:NUMBERBOX id="txtImpCuentaCtaSocial" runat="server" cssclass="cuadrotextodeshab" Width="65px"
														Enabled="False"></CC1:NUMBERBOX>&nbsp; <A id="btnImpCuentaCtaSocial" href="javascript:btnACtaCtaSocial_click();" runat="server">
														<IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Saldo a Cuenta Cta Social" src="imagenes/Buscar16.gif"></A></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 45.05%" align="right">
													<asp:label id="lblImpAcuses" runat="server" cssclass="titulo">Acuses de Recibo Pend.de Proc.:  </asp:label></TD>
												<TD style="WIDTH: 60%">
													<CC1:NUMBERBOX id="txtImpAcuses" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="False"></CC1:NUMBERBOX>&nbsp;
													<A id="btnImpAcuses" href="javascript:btnAcuses_click();" runat="server"><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
															border="0" alt="Acuses de Recibo Pendientes de Procesamiento" src="imagenes/Buscar16.gif"></A></TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 45.05%" align="right">
													<asp:label id="lblLimiteSaldo" runat="server" cssclass="titulo">L�mite saldo deudor:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 60%">
													<CC1:TEXTBOXTAB id="txtLimiteSaldo" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="False"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 40%" colSpan="2" align="center">&nbsp;&nbsp;
													<asp:label id="lblProf" runat="server" cssclass="titulo">Proformas</asp:label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 40%" colSpan="2" align="center">
													<asp:datagrid id="grdproforma" runat="server" Visible="true" BorderWidth="1px" BorderStyle="None"
														AutoGenerateColumns="False" ItemStyle-Height="5px" PageSize="15" OnPageIndexChanged="grdproforma_Page"
														CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True"
														width="90%" OnEditCommand="mProformasDeta">
														<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
														<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
														<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
														<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
														<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
														<FooterStyle CssClass="footer"></FooterStyle>
														<Columns>
															<asp:TemplateColumn>
																<HeaderStyle Width="3%"></HeaderStyle>
																<ItemTemplate>
																	<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																		Height="5">
																		<img src='imagenes/Buscar16.gif' border="0" alt="Ver Comprobantes" style="cursor:hand;"
																			vspace="0" />
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn Visible="False" DataField="prfr_id" HeaderText="ID"></asp:BoundColumn>
															<asp:BoundColumn DataField="actividad" HeaderText="Actividad"></asp:BoundColumn>
															<asp:BoundColumn DataField="N�mero" HeaderText="N�mero"></asp:BoundColumn>
															<asp:BoundColumn DataField="F.Ingreso" HeaderText="Fecha Valor" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
															<asp:BoundColumn DataField="total" HeaderText="Total">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
														<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 90%" colSpan="2" align="center">
													<asp:label id="lblProformas" runat="server" cssclass="titulo">Total Proformas:</asp:label>&nbsp;
													<CC1:NUMBERBOX id="txtProformas" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="False"></CC1:NUMBERBOX></TD>
											</TR>
											<TR>
												<TD colSpan="2"></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2" align="center"><asp:panel id="panOtrosDatos" runat="server" height="100%" BorderStyle="Solid" BorderWidth="1px"
										Width="100%" cssclass="titulo" Visible="false">
										<TABLE style="WIDTH: 100%" id="tblOtrosDatos" class="FdoFld" border="0" cellPadding="0"
											align="left">
											<TBODY>
												<TR>
													<TD style="WIDTH: 20%" align="right"><asp:label id="lblNroSocio" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;
													</TD>
													<TD style="WIDTH: 20%"><CC1:NUMBERBOX id="txtNroSocio" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"></CC1:NUMBERBOX></TD>
													<TD align="right"><asp:label id="lblCunica" runat="server" cssclass="titulo">Clave Unica:</asp:label>&nbsp;</TD>
													<TD><asp:label id="txtCunica" runat="server" cssclass="titulo"></asp:label></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD align="right"><asp:label id="lblestado" runat="server" cssclass="titulo">Estado:</asp:label>&nbsp;
													</TD>
													<TD><CC1:TEXTBOXTAB id="txtestado" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"></CC1:TEXTBOXTAB></TD>
													<TD align="right"><asp:label id="lblCatego" runat="server" cssclass="titulo">Categor�a:</asp:label>&nbsp;
													</TD>
													<TD><CC1:TEXTBOXTAB id="txtCatego" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"
															AceptaNull="False"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD align="right"><asp:label id="lblCondIVAT" runat="server" cssclass="titulo">Cond.I.V.A.:</asp:label>&nbsp;
													</TD>
													<TD><CC1:TEXTBOXTAB id="txtCondIVA" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"></CC1:TEXTBOXTAB></TD>
													<TD align="right"><asp:label id="lblCuitT" runat="server" cssclass="titulo">C.U.I.T.:</asp:label>&nbsp;
													</TD>
													<TD><CC1:TEXTBOXTAB id="txtcuit" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
												</TR>
												<TR>
													<TD align="right"><asp:label id="lblISEAT" runat="server" cssclass="titulo">ISEA(leg):</asp:label>&nbsp;
													</TD>
													<TD><CC1:NUMBERBOX id="txtISEA" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"></CC1:NUMBERBOX></TD>
													<TD align="right"><asp:label id="lblCEIDAT" runat="server" cssclass="titulo">CEIDA(leg):</asp:label>&nbsp;
													</TD>
													<TD><CC1:NUMBERBOX id="txtCEIDA" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"></CC1:NUMBERBOX></TD>
												</TR>
												<TR>
													<TD align="right"><asp:label id="lblEGEA" runat="server" cssclass="titulo">EGEA(leg):</asp:label>&nbsp;
													</TD>
													<TD colSpan="3"><CC1:NUMBERBOX id="txtEGEA" runat="server" Width="90px" cssclass="cuadrotexto" Enabled="False"></CC1:NUMBERBOX></TD></TD>
							</TR>
							<TR>
								<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 100%" colSpan="4" align="center"><asp:label id="Label1" runat="server" cssclass="titulo">CRIADORES</asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 100%" colSpan="4" align="center"><asp:datagrid id="grdConsulta" runat="server" BorderStyle="None" BorderWidth="1px" width="100%"
										AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" PageSize="15"
										ItemStyle-Height="5px" AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemTemplate>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 100%" colSpan="4" align="center"><asp:label id="Label4" runat="server" cssclass="titulo">EXPOSITORES</asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 100%" colSpan="4" align="center"><asp:datagrid id="grdExpositores" runat="server" BorderStyle="None" BorderWidth="1px" width="70%"
										AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdExpositores_Page" PageSize="15"
										ItemStyle-Height="5px" AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="exps_id" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="exps_nume" HeaderText="N�mero"></asp:BoundColumn>
											<asp:BoundColumn DataField="exps_baja_fecha" HeaderText="B.Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
						</asp:panel></td>
				</tr>
			</table>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnSociId" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnClieId" runat="server"></asp:textbox><asp:textbox id="hdnFecha" runat="server"></asp:textbox><asp:textbox id="hdnFechaValor" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnTipoPop" runat="server"></asp:textbox></DIV>
			<!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE></form>
	</BODY>
</HTML>
