Imports System.Data.SqlClient


Namespace SRA


Partial Class PlanMesas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents txtDescFil As NixorControls.TextBoxTab
   Protected WithEvents Panedfdf As System.Web.UI.WebControls.Panel
   Protected WithEvents Peri As NixorControls.ComboBox
   Protected WithEvents periodo As NixorControls.TextBoxTab
   Protected WithEvents panDire As System.Web.UI.WebControls.Panel
   Protected WithEvents Chkintensivo As System.Web.UI.WebControls.CheckBox
   Protected WithEvents lnkCorre As System.Web.UI.WebControls.LinkButton
   Protected WithEvents btnAltaEsta As System.Web.UI.WebControls.Button
   Protected WithEvents btnBajaEsta As System.Web.UI.WebControls.Button
   Protected WithEvents btnModiEsta As System.Web.UI.WebControls.Button
   Protected WithEvents btnLimpEsta As System.Web.UI.WebControls.Button

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "plan_mesas_vota"
   Private mstrCategorias As String = "plan_mesas_cate"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String


   Private Enum Columnas As Integer
      MateId = 1
      MateDesc = 2
      MateTeoHs = 3
      MatePracHs = 4

   End Enum

   Private Enum ColumnasDeta As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()


         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mConsultar(grdDato, False)
            tabLinks.Visible = False
            panBotones.Visible = False

            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If



      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      'btnBajaEqui.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaCate.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      'btnBajaCarre.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")

   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Profesores, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrCarrEquiLong As Object
      Dim lstrCarrerasLong As Object

      lstrCarrerasLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)


   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultar(grdDato, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


#End Region

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla

         Case mstrCategorias
            btnBajaCate.Enabled = Not (pbooAlta)
            btnModiCate.Enabled = Not (pbooAlta)
            btnAltaCate.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()
      mLimpiarCate()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.MateId).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            hdnId.Text = .Item("pmvo_id")
            txtNum.Text = .Item("pmvo_nume")
            txtDesde.Text = .Item("pmvo_desde")
            txtHasta.Text = .Item("pmvo_hasta")
         End With
         mSetearEditor("", False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      txtNum.Text = ""
      txtDesde.Text = ""
      txtHasta.Text = ""


      mLimpiarCate()

      mCrearDataSet("")

      lblTitu.Text = ""

      grdCate.CurrentPageIndex = 0

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      grdDato.Visible = Not panDato.Visible
      btnAgre.Visible = Not (panDato.Visible)


      lnkCabecera.Font.Bold = True
      lnkCate.Font.Bold = False

      panCabecera.Visible = True
      panCate.Visible = False

      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkCate.Font.Bold = False
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panCabecera.Visible = False
      panCate.Visible = False



      lnkCabecera.Font.Bold = False
      lnkCate.Font.Bold = False

      Dim val As String

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos del plan de mesa: " & txtNum.Text
         Case 2
            clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCateC, "S")
            panCate.Visible = True
            lnkCate.Font.Bold = True
            lblTitu.Text = "Categorias del plan de mesa: " & txtNum.Text
            grdCate.Visible = True
            btnBajaCate.Enabled = False
            btnModiCate.Enabled = False
            btnAltaCate.Enabled = True

      End Select
   End Sub

   Private Sub mCargarComboConDataSet(ByVal cmb As NixorControls.ComboBox, ByVal dataTable As Data.DataTable, ByVal strId As String, ByVal strDesc As String)

      cmb.DataSource = dataTable
      cmb.DataValueField = strId
      cmb.DataTextField = strDesc
      cmb.DataBind()
      cmb.Items.Insert(0, "(Seleccione)")
      cmb.Items(0).Value = ""
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub


#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjAsamblea As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjAsamblea.Alta()
         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjAsamblea As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjAsamblea.Modi()

         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar(grdDato, True)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet

      If txtNum.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Numero.")
      End If

      If txtDesde.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar las letras de inicio.")
      End If

      If txtHasta.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar las letras de fin.")
      End If


      With mdsDatos.Tables(0).Rows(0)
         .Item("pmvo_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("pmvo_nume") = txtNum.Text
         .Item("pmvo_desde") = txtDesde.Text
         .Item("pmvo_hasta") = txtHasta.Text
         .Item("pmvo_pmvc_id") = 1
      End With
      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrCategorias

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      grdCate.DataSource = mdsDatos.Tables(mstrCategorias)
      grdCate.DataBind()


      Session(mstrTabla) = mdsDatos

   End Sub


#End Region

#Region "Eventos de Controles"
   
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub




#End Region

#Region "Detalle"
   Public Sub mEditarDatosCate(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrCate As DataRow
         hdnCateId.Text = E.Item.Cells(1).Text
         ldrCate = mdsDatos.Tables(mstrCategorias).Select("pmca_id=" & hdnCateId.Text)(0)

         With ldrCate
            cmbCateC.Valor = .Item("pmca_cate_id")
         End With

         mSetearEditor(mstrCategorias, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal pOk As Boolean)
      Try
         Dim strFin As String = ""
         If (Not pOk) Then
            strFin = "@ejecuta = N "
         End If
         mstrCmd = "exec " + mstrTabla + "_consul " + strFin
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarCategoria()

      Dim ldrCate As DataRow

      If hdnCateId.Text = "" Then
         ldrCate = mdsDatos.Tables(mstrCategorias).NewRow
         ldrCate.Item("pmca_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrCategorias), "pmca_id")
      Else
         ldrCate = mdsDatos.Tables(mstrCategorias).Select("pmca_id=" & hdnCateId.Text)(0)
      End If

      If (cmbCateC.SelectedItem.Text = "(Seleccione)") Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la categoria.")
      End If

      With ldrCate
         .Item("pmca_cate_id") = cmbCateC.Valor
         .Item("_categoria") = cmbCateC.SelectedItem.Text
      End With

      If (mEstaEnElDataSet(mdsDatos.Tables(mstrCategorias), ldrCate, "pmca_cate_id", "pmca_id")) Then
         Throw New AccesoBD.clsErrNeg("El plan de mesa ya tiene la categoria seleccionada.")
      Else
         If (hdnCateId.Text = "") Then
            mdsDatos.Tables(mstrCategorias).Rows.Add(ldrCate)
         End If
      End If
   End Sub



   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCate As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
      Dim filtro As String
      filtro = campo1 + " = " + ldrCate.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrCate.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrCate.Item(campoId).ToString()
      Return (table.Select(filtro).GetLength(0) > 0)
   End Function

   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCate As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
      Return mEstaEnElDataSet(table, ldrCate, campo1, campo1, campoId)
   End Function

   Private Sub mLimpiarCate()
      hdnCateId.Text = ""
      cmbCateC.Limpiar()
      mSetearEditor(mstrCategorias, True)
   End Sub



#End Region



   Public Sub grdCate_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCate.EditItemIndex = -1
         If (grdCate.CurrentPageIndex < 0 Or grdCate.CurrentPageIndex >= grdCate.PageCount) Then
            grdCate.CurrentPageIndex = 0
         Else
            grdCate.CurrentPageIndex = E.NewPageIndex
         End If
         grdCate.DataSource = mdsDatos.Tables(mstrCategorias)
         grdCate.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub btnModiCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub ActualizarCategorias()
      Try
         mGuardarCategoria()

         mLimpiarCate()
         grdCate.DataSource = mdsDatos.Tables(mstrCategorias)
         grdCate.DataBind()
         

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub lnkCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCate.Click
      mShowTabs(2)
   End Sub

   Private Sub btnBajaCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCate.Click
      Try
         mdsDatos.Tables(mstrCategorias).Select("pmca_id=" & hdnCateId.Text)(0).Delete()
         grdCate.DataSource = mdsDatos.Tables(mstrCategorias)
         grdCate.DataBind()
         mLimpiarCate()


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpCate.Click
      mLimpiarCate()
   End Sub

Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
    mAgregar()
End Sub
End Class
End Namespace
