<%@ Reference Control="~/controles/usrcriadero.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CRIA" Src="controles/usrCriadero.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AnillosSoli" CodeFile="AnillosSoli.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>  
		<title>Solicitud de Anillos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
   
    	function btnRazas_click() 
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Razas&tabla=criaderos_razas&filtros=" + document.all("hdnCriaId").value, 5, "500","300","250","50");
		}
		
		function usrCria_onchange()
		{
			document.all('hdnCriaId').value = document.all('usrCria:txtId').value;
			var strFiltro = document.all('hdnCriaId').value;
		    var strRet = LeerCamposXML("criaderos_dire", strFiltro, "dire");
	 	    document.all('txtDire').value = strRet;
		}

	    function expandir()
		{
			if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Solicitud de Anillos</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="100%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="4">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="4">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblCriaFil" runat="server" cssclass="titulo">Criadero:&nbsp;</asp:label></TD>
																			<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<UC1:CRIA id="usrCriaFil" runat="server" Tabla="Criaderos" Saltos="1,2" CampoCodi="crdr_nume" FilSociNume="True"
																					FilTipo="T" MuestraDesc="False" Ancho="800" AceptaNull="false" CampoVal="Criadero" AutoPostBack="True"></UC1:CRIA></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<asp:checkbox id="chkTodo" CssClass="titulo" Runat="server" Text="Mostrar Todas"></asp:checkbox></TD>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD height="10" colSpan="3"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="amca_id" ReadOnly="True" HeaderText="Id.Solicitud"></asp:BoundColumn>
												<asp:BoundColumn DataField="amca_soli_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="F. Solicitud"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="amca_nume" HeaderText="Nro."></asp:BoundColumn>
												<asp:BoundColumn DataField="_criadero" ReadOnly="True" HeaderText="Criadero"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_canti" HeaderText="Cant.Anillos"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="amca_envi_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="F. Envio"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10" colSpan="3"></td>
								</tr>
								<TR>
									<TD colSpan="2" vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Solicitud" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="200" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" CausesValidation="False" Height="21px" Width="80px"> Solicitud</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkAni" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														CausesValidation="False" Height="21px" Width="80px"> Anillos</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkRaza" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														CausesValidation="False" Height="21px" Width="70px"> Razas</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												width="100%" Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 143px" vAlign="top" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criadero:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<UC1:CRIA id="usrCria" CampoCodi="crdr_nume" runat="server" Tabla="Criaderos" Saltos="1,2" FilSociNume="True" FilTipo="T"
																				MuestraDesc="False" Ancho="800" AceptaNull="false" CampoVal="Criadero" AutoPostBack="True"></UC1:CRIA></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 143px" vAlign="top" align="right">
																			<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro. Solicitud:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<asp:Label id="txtNume" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 143px" vAlign="middle" noWrap align="right">
																			<asp:Label id="lblSoliFecha" runat="server" cssclass="titulo">Fecha Solicitud:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:DateBox id="txtSoliFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 143px" noWrap align="right">
																			<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 143px" vAlign="top" align="right">
																			<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotextodeshab" Height="50px" Width="380px"
																				Obligatorio="True" ReadOnly="True" MaxLength="20" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 143px" vAlign="top" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Height="50px" Width="380px" Obligatorio="True"
																				MaxLength="20" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="center" colSpan="3"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panAni" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="center" colSpan="2">
																			<asp:datagrid id="grdAni" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdAni_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																				AllowPaging="True" Visible="False" OnEditCommand="mEditarDatosAni">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="amde_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_anillo" HeaderText="Anillo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="amde_cant" HeaderText="Cantidad"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAni" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:combobox class="combo" id="cmbAni" runat="server" Width="100px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAniCant" runat="server" cssclass="titulo">Cantidad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:numberbox id="txtAniCant" runat="server" cssclass="cuadrotexto" Width="100px" esdecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaAni" runat="server" cssclass="boton" Text="Agregar Anillo" Width="100px"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaAni" runat="server" cssclass="boton" Text="Eliminar Anillo" Width="100px"
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiAni" runat="server" cssclass="boton" Text="Modificar Anillo" Width="100px"
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpAni" runat="server" cssclass="boton" Text="Limpiar Anillo" Width="100px"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panRaza" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table6" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdRaza" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdRaza_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																				HorizontalAlign="Center" AllowPaging="True" Visible="True" OnEditCommand="mEditarDatosRaza">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="crrz_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_raza_desc" ReadOnly="True" HeaderText="Raza">
																						<HeaderStyle Width="70%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="true" DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="280px" Height="20px"
																				MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" noWrap align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaRaza" runat="server" cssclass="boton" Text="Agregar Raza" Width="100px"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaRaza" runat="server" cssclass="boton" Text="Eliminar Raza" Width="100px"
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiRaza" runat="server" cssclass="boton" Text="Modificar Raza" Width="100px"
																				Visible="False"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpRaza" runat="server" cssclass="boton" Text="Limpiar Raza" Width="100px"
																				Visible="False"></asp:Button>&nbsp;<BUTTON class="boton" id="btnRaza" style="WIDTH: 100px" onclick="btnRazas_click();" type="button"
																				value="Detalles">Razas Criadero</BUTTON></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Text="Alta" Width="80px"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Text="Baja" CausesValidation="False"
																Width="80px"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Text="Modificar" CausesValidation="False"
																Width="80px"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Text="Limpiar" CausesValidation="False"
																Width="80px"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnImpr" runat="server" cssclass="boton" Text="Imprimir" CausesValidation="False"
																Width="80px"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnAniId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnRazaId" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnCriaId" runat="server" AutoPostBack="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Solicitud de Anillos');
		</SCRIPT>
	</BODY>
</HTML>
