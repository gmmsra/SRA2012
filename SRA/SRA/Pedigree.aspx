<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Pedigree" CodeFile="Pedigree.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc2" TagName="PROH" Src="controles/usrProducto.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Pedigree</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
		
		   if (document.all('usrProd:usrRazaCriadorExt:cmbCriaRazaExt') != null)
			{	
				document.all('usrProd:usrRazaCriadorExt:cmbCriaRazaExt').value = document.all('hdnRaza').value;
				document.all('txtusrProd:usrRazaCriadorExt:cmbCriaRazaExt').value = document.all('hdnRazaCodi').value;
				document.all('usrProd:usrRazaCriadorExt:cmbCriaRazaExt').onchange();
			}
			
			if (document.all('usrProdPadre:usrRazaCriadorExt:cmbCriaRazaExt') != null)
			{	
				document.all('usrProdPadre:usrRazaCriadorExt:cmbCriaRazaExt').value = document.all('hdnRaza').value;
				document.all('txtusrProdPadre:usrRazaCriadorExt:cmbCriaRazaExt').value = document.all('hdnRazaCodi').value;
				document.all('usrProdPadre:usrRazaCriadorExt:cmbCriaRazaExt').onchange();
			}
			if (document.all('usrProdMadre:usrRazaCriadorExt:cmbCriaRazaExt') != null)
			{	
				document.all('usrProdMadre:usrRazaCriadorExt:cmbCriaRazaExt').value = document.all('hdnRaza').value;
				document.all('txtusrProdMadre:usrRazaCriadorExt:cmbCriaRazaExt').value = document.all('hdnRazaCodi').value;
				document.all('usrProdMadre:usrRazaCriadorExt:cmbCriaRazaExt').onchange();
			}
		
		function mValidar() 
		{
			if (document.all('optExis')!=null)
			{
				if (document.all('optExis').checked && document.all('usrProd1:txtId') == "0")
				{
					alert('Debe indicar el producto.');
					return(false);
				}
				if (document.all('optNuev').checked)
				{
					if (document.all('cmbAsoc').value == "")
					{
						alert('Debe indicar la asociaci�n.');
						document.all('cmbAsoc').focus();
						return(false);
					}
					/* Dario 2013-07-17 elimina controles obligatorios 
					if (document.all('txtNume').value == "")
					{
						alert('Debe indicar el numero.');
						document.all('txtNume').focus();
						return(false);
					}
					if (document.all('txtRP').value == "")
					{
						alert('Debe indicar el RP.');
						document.all('txtRP').focus();
						return(false);
					}
					if (document.all('txtRPNume').value == "")
					{
						alert('Debe indicar el RP.');
						document.all('txtRPNume').focus();
						return(false);
            		}
            		*/
					if (document.all('txtNomb').value == "")
					{
						alert('Debe indicar el nombre.');
						document.all('txtNomb').focus();
						return(false);
            		}
				}
            }
		}
		
		function mImprimir()
		{
			if (document.all('btnImpri').value != document.all(pRaza).value)
			{
			document.all("usrProd1:usrProducto:cmbProdRaza").disabled=true;
			}
		}

		function mCancelar()
		{
			mRestaurarColores();
			document.all('hdnSexo').value = '';
			document.all('hdnProd').value = '';
		}
		
		function mValiImpre()
		{
		
		return true;
		}
		function mRestaurarColores()
		{
			var arrElements = document.getElementsByTagName('input'); 
			for (var i = 0; i < arrElements.length; i++) 
			{
				if (arrElements[i].id != '')
				{
					if (arrElements[i].id.substring(0,3) == 'txt' && arrElements[i].id != 'txtProd')
					{
						arrElements[i].style.backgroundColor = 'white';
					}
				}
			}
		}
		
		function mPedigree(pobjProd, pstrSexo, pstrAnte)
		{
			if (document.all(pstrAnte).value == '')
				alert('No puede asociar un producto en el arbol de pedigree sin haber cargado el producto que le precede.');
			else
			{	
				if (pobjProd.value != '')
					document.all('hdnAnte').value = pobjProd.id;
				else
					document.all('hdnAnte').value = pstrAnte;
				
				document.all('hdnSexo').value = pstrSexo;
				
				document.all("hdnProd").value = pobjProd.id;
				
				if (document.all('hdnEstado').value.toLowerCase() == "de baja" )
			          alert('No se permite modificar pedigree en estado de  Baja');
			   else
			         __doPostBack('hdnProd','');
			   
			   /* 2015-06-15  Dario  se comenta y se da nueva logica   
			   if (document.all('hdnInscribeSRA').value == '0' )
			
			         alert('Esta Raza no inscribe en SRA, cargar pedigree en MASIVO - Opciones/Denuncia de Nacimiento/Inscripci�n Extranjero');
			   else
			         __doPostBack('hdnProd','');     
			   */ 
			   /* 2015-06-15  Dario  se comenta y se da nueva logica   */    
			    if (document.all('hdnSoloConsulta').value.toLowerCase() == 'true' && document.all('hdnInscribeSRA').value == '1')
				{
					if (document.all('hdnmensajeConsulta').value != '')
						alert(document.all('hdnmensajeConsulta').value);
					else
						alert('Ingreso por opcion de Solo Consulta de productos');
			
				}
			      
				else
			         __doPostBack('hdnProd','');  
			      
			             
			
			}
		}
				
		function mSetearRazas()
		{
			if (document.all('usrProd:usrRazaCriadorExt') != null)
			{	
				document.all('usrProd:usrRazaCriadorExt').value = document.all('hdnRaza').value;
				document.all('txtusrProd:usrRazaCriadorExt').value = document.all('hdnRazaCodi').value;
			}
			
			if (document.all('usrProdPadre:usrRazaCriadorExt') != null)
			{	
				document.all('usrProdPadre:usrRazaCriadorExt').value = document.all('hdnRaza').value;
				document.all('txtusrProdPadre:usrRazaCriadorExt').value = document.all('hdnRazaCodi').value;
			}
			if (document.all('usrProdMadre:usrRazaCriadorExt') != null)
			{	
				document.all('usrProdMadre:usrRazaCriadorExt').value = document.all('hdnRaza').value;
				document.all('txtusrProdMadre:usrRazaCriadorExt').value = document.all('hdnRazaCodi').value;
			}
		}
		
		function mTipo(pstrPanel)
		{
			mSetearTipo(pstrPanel);
			mSetearRazas();
		}
		
		function mSetearTipo(pstrPanel)
		{
			if (document.all('panProd' + pstrPanel) !=null)
			{
				if (document.all('optExis' + pstrPanel).checked)
				{
					document.all('panProd' + pstrPanel).style.display='';
					document.all('panNuev' + pstrPanel).style.display='none';
				}
				else
				{
					document.all('panProd' + pstrPanel).style.display='none';
					document.all('panNuev' + pstrPanel).style.display='';		
				}
			}
		}
		
		/* Funcion que extrae el RP numerico desde el Rp
		   hay que pasarle el txtcontrolado = origen
							  txtdel rp numerico = destino 
							  evt evento de la tecla que controlamos
		  si detectamos el ques un numero la agrega al valor del txtdestino.
		  sino no hacemos nada solo lo agrega en el cotrol origen,
		  si se borra el txtorigen en el primer digito que se tipee en origen blanquea y vueve a 
		  autocompletar el destino si corresponde
		  ejemplo de llamada
		  onkeypress = "return HaceRPNumero('txtRP','txtRPNume', event)"								   		
		*/
		function HaceRPNumero(objOrigen, objDestino , evt) 
		{
		   if(document.all(objOrigen).value == '')
		   {
			 document.all(objDestino).value ='';
		   }
				
		   var charCode = (evt.which) ? evt.which : event.keyCode
		 
		   if (charCode > 47 && charCode < 58)
		   {
		  	 document.all(objDestino).value = document.all(objDestino).value + String.fromCharCode(charCode);
		   }
		  return true;
		}
		
		</script>
	</HEAD>
	<BODY onload="" class="pagina" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" border="0" width="900">
							<TR height="100%">
								<TD vAlign="top">
									<TABLE id="Table2" border="0" cellSpacing="0" cellPadding="0" width="100%">
										<TR>
											<TD style="WIDTH: 475px; HEIGHT: 20px"></TD>
											<TD style="WIDTH: 253px; HEIGHT: 20px"></TD>
											<TD style="WIDTH: 192px; HEIGHT: 20px"></TD>
											<TD style="WIDTH: 159px; HEIGHT: 20px" align="right"></TD>
											<TD vAlign="bottom" width="350" align="right">
												<DIV onclick="window.close();"><IMG style="CURSOR: hand" class="noprint" border="0" alt="Cerrar" src="imagenes/close3.bmp">
												</DIV>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px; HEIGHT: 20px"></TD>
											<TD style="WIDTH: 253px; HEIGHT: 20px"></TD>
											<TD style="WIDTH: 192px; HEIGHT: 20px"></TD>
											<TD style="WIDTH: 159px; HEIGHT: 20px" align="right"></TD>
											<TD vAlign="bottom" width="350"><asp:imagebutton id="Imagebutton3" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreA211" ondblclick="javascript:mPedigree(this,'1','txtPadreA21');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 199px" colSpan="2"><CC1:TEXTBOXTAB id="txtPadreA21" ondblclick="javascript:mPedigree(this,'1','txtPadreA1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 326px" colSpan="2" align="center"><asp:imagebutton id="Imagebutton18" runat="server" ImageUrl="images\pedigree1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton2" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreA211" ondblclick="javascript:mPedigree(this,'0','txtPadreA21');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px" height="20"></TD>
											<TD style="WIDTH: 253px" height="20"></TD>
											<TD style="WIDTH: 326px" height="20" colSpan="2" noWrap><CC1:TEXTBOXTAB id="txtPadreA1" ondblclick="javascript:mPedigree(this,'1','txtPadreA');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
											<TD height="20" width="350" noWrap></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 253px" vAlign="bottom" rowSpan="3" align="right"><asp:imagebutton id="Imagebutton26" runat="server" ImageUrl="images\pedigreeA.gif" ToolTip="" CausesValidation="False"
													Height="74px"></asp:imagebutton></TD>
											<TD style="WIDTH: 326px" width="80" colSpan="2" align="center"><asp:imagebutton id="Imagebutton19" runat="server" ImageUrl="images\pedigree2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD vAlign="bottom" width="350" noWrap><asp:imagebutton id="Imagebutton7" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreA212" ondblclick="javascript:mPedigree(this,'1','txtMadreA21');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 79px" colSpan="3"><CC1:TEXTBOXTAB id="txtMadreA21" ondblclick="javascript:mPedigree(this,'0','txtPadreA1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px" width="475"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 159px"></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton4" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreA212" ondblclick="javascript:mPedigree(this,'0','txtMadreA21');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px" height="20"></TD>
											<TD style="WIDTH: 317px" height="20" colSpan="2"><CC1:TEXTBOXTAB id="txtPadreA" ondblclick="javascript:mPedigree(this,'1','txtProd');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
											<TD style="WIDTH: 159px" height="20"></TD>
											<TD height="20" width="350" noWrap></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px" vAlign="bottom" rowSpan="7" align="right"><asp:imagebutton id="Imagebutton30" runat="server" ImageUrl="images\pedigreeP.gif" ToolTip="" CausesValidation="False"
													Height="171px"></asp:imagebutton></TD>
											<TD style="WIDTH: 253px" vAlign="top" rowSpan="3" align="right"><asp:imagebutton id="Imagebutton29" runat="server" ImageUrl="images\pedigreeB.gif" ToolTip="" CausesValidation="False"
													Height="74px"></asp:imagebutton></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 159px"></TD>
											<TD vAlign="bottom" width="350" noWrap><asp:imagebutton id="Imagebutton8" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreA221" ondblclick="javascript:mPedigree(this,'1','txtPadreA22');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 79px" colSpan="3"><CC1:TEXTBOXTAB id="txtPadreA22" ondblclick="javascript:mPedigree(this,'1','txtMadreA1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 326px" colSpan="2" align="center"><asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="images\pedigree1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton5" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreA221" ondblclick="javascript:mPedigree(this,'0','txtPadreA22');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 253px" height="20"></TD>
											<TD style="WIDTH: 326px" height="20" colSpan="2"><CC1:TEXTBOXTAB id="txtMadreA1" ondblclick="javascript:mPedigree(this,'0','txtPadreA');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
											<TD height="20" width="350" noWrap></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 326px" colSpan="2" align="center"><asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="images\pedigree2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD width="350" noWrap><asp:imagebutton id="Imagebutton9" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreA222" ondblclick="javascript:mPedigree(this,'1','txtMadreA22');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 199px" colSpan="2"><CC1:TEXTBOXTAB id="txtMadreA22" ondblclick="javascript:mPedigree(this,'0','txtMadreA1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 159px"></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton6" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreA222" ondblclick="javascript:mPedigree(this,'0','txtMadreA22');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 408px" height="20" colSpan="3"><CC1:TEXTBOXTAB id="txtProd" ondblclick="javascript:mPedigree(this,'0','txtProd');" runat="server"
													ReadOnly="True" Height="20px" Width="380px" BackColor="AliceBlue" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
											<TD style="WIDTH: 159px" height="20"></TD>
											<TD height="20" width="350" noWrap></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px" vAlign="top" rowSpan="7" align="right"><asp:imagebutton id="Imagebutton31" runat="server" ImageUrl="images\pedigreeM.gif" ToolTip="" CausesValidation="False"
													Height="171px"></asp:imagebutton></TD>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 159px"></TD>
											<TD vAlign="bottom" width="350" noWrap><asp:imagebutton id="Imagebutton10" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreB111" ondblclick="javascript:mPedigree(this,'1','txtPadreB11');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 199px" colSpan="2"><CC1:TEXTBOXTAB id="txtPadreB11" ondblclick="javascript:mPedigree(this,'1','txtPadreB1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 326px" colSpan="2" align="center"><asp:imagebutton id="Imagebutton22" runat="server" ImageUrl="images\pedigree1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton14" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreB111" ondblclick="javascript:mPedigree(this,'0','txtPadreB11');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 253px" height="20"></TD>
											<TD style="WIDTH: 326px" height="20" colSpan="2"><CC1:TEXTBOXTAB id="txtPadreB1" ondblclick="javascript:mPedigree(this,'1','txtMadreB');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
											<TD height="20" width="350" noWrap></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 253px" vAlign="bottom" rowSpan="3" align="right"><asp:imagebutton id="Imagebutton27" runat="server" ImageUrl="images\pedigreeA.gif" ToolTip="" CausesValidation="False"
													Height="74px"></asp:imagebutton></TD>
											<TD style="WIDTH: 326px" colSpan="2" align="center"><asp:imagebutton id="Imagebutton25" runat="server" ImageUrl="images\pedigree2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD vAlign="bottom" width="350" noWrap><asp:imagebutton id="Imagebutton11" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreB112" ondblclick="javascript:mPedigree(this,'1','txtMadreB11');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 199px" colSpan="2"><CC1:TEXTBOXTAB id="txtMadreB11" ondblclick="javascript:mPedigree(this,'0','txtPadreB1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 159px"></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton15" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreB112" ondblclick="javascript:mPedigree(this,'0','txtMadreB11');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px" height="20"></TD>
											<TD style="WIDTH: 317px" height="20" colSpan="2"><CC1:TEXTBOXTAB id="txtMadreB" ondblclick="javascript:mPedigree(this,'0','txtProd');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
											<TD style="WIDTH: 159px" height="20"></TD>
											<TD height="20" width="350" noWrap></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px; HEIGHT: 15px"></TD>
											<TD style="WIDTH: 253px; HEIGHT: 15px" vAlign="top" rowSpan="3" align="right"><asp:imagebutton id="Imagebutton28" runat="server" ImageUrl="images\pedigreeB.gif" ToolTip="" CausesValidation="False"
													Height="74px"></asp:imagebutton></TD>
											<TD style="WIDTH: 192px; HEIGHT: 15px"></TD>
											<TD style="WIDTH: 159px; HEIGHT: 15px"></TD>
											<TD style="HEIGHT: 15px" vAlign="bottom" width="350" noWrap><asp:imagebutton id="Imagebutton12" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreB121" ondblclick="javascript:mPedigree(this,'1','txtPadreB12');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 199px" colSpan="2"><CC1:TEXTBOXTAB id="txtPadreB12" ondblclick="javascript:mPedigree(this,'1','txtMadreB1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 326px" colSpan="2" align="center"><asp:imagebutton id="Imagebutton23" runat="server" ImageUrl="images\pedigree1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton16" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreB121" ondblclick="javascript:mPedigree(this,'0','txtPadreB12');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px" height="20"></TD>
											<TD style="WIDTH: 253px" height="20"></TD>
											<TD style="WIDTH: 326px" height="20" colSpan="2"><CC1:TEXTBOXTAB id="txtMadreB1" ondblclick="javascript:mPedigree(this,'0','txtMadreB');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
											<TD height="20" width="350" noWrap></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 326px" colSpan="2" align="center"><asp:imagebutton id="Imagebutton24" runat="server" ImageUrl="images\pedigree2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton></TD>
											<TD vAlign="bottom" width="350" noWrap><asp:imagebutton id="Imagebutton13" runat="server" ImageUrl="images\pedigreeP1.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtPadreB122" ondblclick="javascript:mPedigree(this,'1','txtMadreB12');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 199px" colSpan="2"><CC1:TEXTBOXTAB id="txtMadreB12" ondblclick="javascript:mPedigree(this,'0','txtMadreB1');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 475px"></TD>
											<TD style="WIDTH: 253px"></TD>
											<TD style="WIDTH: 192px"></TD>
											<TD style="WIDTH: 159px"></TD>
											<TD vAlign="top" width="350" noWrap><asp:imagebutton id="Imagebutton17" runat="server" ImageUrl="images\pedigreeP2.gif" ToolTip="" CausesValidation="False"></asp:imagebutton><CC1:TEXTBOXTAB id="txtMadreB122" ondblclick="javascript:mPedigree(this,'0','txtMadreB12');" runat="server"
													ReadOnly="True" Height="18px" Width="290px" BackColor="WhiteSmoke" cssclass="textopedigree"></CC1:TEXTBOXTAB></TD>
										</TR>
										<tr>
											<TD style="WIDTH: 475px; HEIGHT: 33px"></TD>
											<TD style="WIDTH: 253px; HEIGHT: 33px"></TD>
											<TD style="WIDTH: 192px; HEIGHT: 33px"></TD>
											<TD style="WIDTH: 159px; HEIGHT: 33px"></TD>
											<TD style="WIDTH: 159px; HEIGHT: 33px"></TD>
										</tr>
										<tr>
											<td colSpan="5" align="right"><asp:button id="btnImpri" runat="server" CausesValidation="False" Width="80px" cssclass="boton"
													Text="Imprimir"></asp:button></td>
										</tr>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<TR>
					<TD background="imagenes/reciz.jpg" width="9"></TD>
					<TD vAlign="middle" align="left"><asp:panel style="POSITION: absolute; VISIBILITY: hidden; TOP: 110px; LEFT: 30px" id="panCarga"
							runat="server" Width="90%" cssclass="titulo" BorderStyle="Solid" BorderWidth="2px">
							<TABLE style="WIDTH: 100%" id="tablaCarga" class="FdoFld" border="0" cellSpacing="0" cellPadding="0"
								align="left">
								<TR>
									<TD vAlign="top">
										<TABLE id="tabla11" border="0" cellSpacing="0" cellPadding="0" width="100%">
											<TR>
												<TD vAlign="top"><!-- FOMULARIO -->
													<TABLE id="tabla111" border="0" cellSpacing="0" cellPadding="0" width="100%" height="180">
														<TR>
															<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
														</TR>
														<TR>
															<TD height="6" colSpan="2" align="center">
																<asp:Label id="txtProdAnte" runat="server" cssclass="titulo"></asp:Label></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD vAlign="top" colSpan="2" align="center">
																<asp:Panel id="panTipo" runat="server">
<asp:RadioButton id="optExis" onclick="javascript:mTipo('');" runat="server" Text="Producto Existente"
																		Checked="True" CssClass="titulo"
																		GroupName="grpTipo"></asp:RadioButton>&nbsp;&nbsp; 
<asp:RadioButton id="optNuev" onclick="javascript:mTipo('');" runat="server" Text="Nuevo Producto"
																		CssClass="titulo" GroupName="grpTipo"></asp:RadioButton></asp:Panel></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR id="panProd" runat="server">
															<TD style="WIDTH: 81px" align="right">
																<asp:Label id="lblProd" runat="server" cssclass="titulo">Producto:&nbsp;</asp:Label></TD>
															<TD>
																<UC2:PROH id="usrProd" runat="server" FilSexo="False" Tabla="productos" Saltos="1,2" AutoPostBack="False"
																	FilSociNume="True" FilTipo="S" MuestraDesc="false" FilDocuNume="True" Ancho="800" AceptaNull="false"
																	EsPropietario="false" CriaOrPropDescrip="Raza:" MuestraSoloRazaClieDerivr="True"></UC2:PROH></TD>
														</TR>
														<TR id="panNuev" runat="server">
															<TD height="18" colSpan="2" align="left">
																<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="98%">
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza - :&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="150" AceptaNull="true"
																				onchange="Combo_change(this)" mostrarbotones="False" filtra="True" NomOper="razas_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAsoc" runat="server" cssclass="titulo">Asociaci�n - :&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox id="cmbAsoc" class="combo" runat="server" cssclass="cuadrotexto" Width="300" AceptaNull="true"
																				onchange="Combo_change(this)" mostrarbotones="False" filtra="True" NomOper="asociaciones_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblNume" runat="server" cssclass="titulo">N�mero - :&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:numberbox id="txtNume" runat="server" cssclass="cuadrotexto" Width="80px" esdecimal="False"
																				MaxValor="99999999999999999" MaxLength="17"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">RP - :&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtRP" onkeypress="return HaceRPNumero('txtRP','txtRPNume', event)" runat="server"
																				cssclass="cuadrotexto" Width="120px"></CC1:TEXTBOXTAB>
																			<asp:Label id="lblRpNume" runat="server" cssclass="titulo">RP Num�rico - :&nbsp;</asp:Label>
																			<cc1:numberbox id="txtRPNume" runat="server" cssclass="cuadrotexto" Width="80px" esdecimal="False"
																				MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre - :&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="365px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD noWrap align="right">
																			<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">&nbsp;Fecha Nacim. - :&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD height="30" vAlign="bottom" colSpan="2" align="center">
																<asp:Button id="btnGrabar" runat="server" cssclass="boton" Width="80px" Text="Grabar"></asp:Button>&nbsp;&nbsp;
																<asp:Button id="btnCance" runat="server" cssclass="boton" Width="80px" Text="Cancelar"></asp:Button></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
														</TR>
													</TABLE>
												</TD>
												<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel style="POSITION: absolute; VISIBILITY: hidden; TOP: 110px; LEFT: 30px" id="panCargaDoble"
							runat="server" Width="90%" cssclass="titulo" BorderStyle="Solid" BorderWidth="2px">
							<TABLE style="WIDTH: 100%" id="Table3" class="FdoFld" border="0" cellSpacing="0" cellPadding="0"
								align="left">
								<TR>
									<TD vAlign="top">
										<TABLE id="tablaPadre" border="0" cellSpacing="0" cellPadding="0" width="100%" height="180">
											<TR>
												<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
											</TR>
											<TR>
												<TD height="6" colSpan="2" align="center">
													<asp:Label id="txtProdAntePadre" runat="server" cssclass="titulo"></asp:Label>
													</TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD vAlign="top" colSpan="2" align="center">
													<asp:Panel id="panTipoPadre" runat="server">
<asp:RadioButton id="optExisPadre" onclick="javascript:mTipo('Padre');" runat="server" Text="Producto Existente"
															Checked="True" CssClass="titulo" GroupName="grpTipoPadre"></asp:RadioButton>&nbsp;&nbsp; 
<asp:RadioButton id="optNuevPadre" onclick="javascript:mTipo('Padre');" runat="server" Text="Nuevo Producto"
															CssClass="titulo" GroupName="grpTipoPadre"></asp:RadioButton>
												</asp:Panel></TD>
											</TR>
											<TR>
												<TD vAlign="top" colSpan="2" align="center">
													<asp:Label id="lblMensajePadre" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
												</td>
											</tr>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR id="panProdPadre" runat="server">
												<TD style="WIDTH: 15%" align="right">
													<asp:Label id="lblProdPadre" runat="server" cssclass="titulo">Producto ACTUAL:&nbsp;</asp:Label></TD>
												<TD>
													<UC2:PROH id="usrProdPadre" runat="server" FilSexo="False" Tabla="productos" Saltos="1,2"
														AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="true" FilDocuNume="True" IncluirDeshabilitados="True"
														Ancho="800" AceptaNull="false" EsPropietario="false" CriaOrPropDescrip="Raza:" MuestraSoloRazaClieDerivr="True"
														Sexo="1"></UC2:PROH></TD>
											</TR>
											<TR id="panNuevPadre" runat="server">
												<TD colSpan="2" align="left">
													<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD align="right">
																<asp:Label id="lblRazaPadre" runat="server" cssclass="titulo">Raza PADRE:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:combobox id="cmbRazaPadre" class="combo" runat="server" cssclass="cuadrotexto" Width="250"
																	AceptaNull="true" onchange="Combo_change(this)" mostrarbotones="False" filtra="True" NomOper="razas_cargar"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD width="15%" align="right">
																<asp:Label id="lblAsocPadre" runat="server" cssclass="titulo">Asociaci�n:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:combobox id="cmbAsocPadre" class="combo" runat="server" cssclass="cuadrotexto" Width="300"
																	AceptaNull="true" onchange="Combo_change(this)" mostrarbotones="False" filtra="True" NomOper="asociaciones_cargar"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblNumePadre" runat="server" cssclass="titulo">N�mero:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:numberbox id="txtNumePadre" runat="server" cssclass="cuadrotexto" Width="80px" esdecimal="False"
																	MaxValor="99999999999999999" MaxLength="17"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblRPPadre" runat="server" cssclass="titulo">RP:&nbsp;</asp:Label></TD>
															<TD>
																<CC1:TEXTBOXTAB id="txtRPPadre" onkeypress="return HaceRPNumero('txtRPPadre','txtRPNumePadre', event)"
																	runat="server" cssclass="cuadrotexto" Width="120px"></CC1:TEXTBOXTAB>
																<asp:Label id="lblRpNumePadre" runat="server" cssclass="titulo">RP Num�rico:&nbsp;</asp:Label>
																<cc1:numberbox id="txtRPNumePadre" runat="server" cssclass="cuadrotexto" Width="80px" esdecimal="False"
																	MaxValor="99999999999999999" MaxLength="17"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblNombPadre" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:Label></TD>
															<TD>
																<CC1:TEXTBOXTAB id="txtNombPadre" runat="server" cssclass="cuadrotexto" Width="365px"></CC1:TEXTBOXTAB></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblNaciFechaPadre" runat="server" cssclass="titulo">Fecha Nacim.:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:DateBox id="txtNaciFechaPadre" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
											</TR>
										</TABLE>
										<TABLE id="tablaMadre" border="0" cellSpacing="0" cellPadding="0" width="100%" height="180">
											<TR>
												<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
											</TR>
											<TR>
												<TD height="6" colSpan="2" align="center">
													<asp:Label id="txtProdAnteMadre" runat="server" cssclass="titulo"></asp:Label>
													</TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD vAlign="top" colSpan="2" align="center">
													<asp:Panel id="panTipoMadre" runat="server">
<asp:RadioButton id="optExisMadre" onclick="javascript:mTipo('Madre');" runat="server" Text="Producto Existente"
															Checked="True" CssClass="titulo" GroupName="grpTipoMadre"></asp:RadioButton>&nbsp;&nbsp; 
<asp:RadioButton id="optNuevMadre" onclick="javascript:mTipo('Madre');" runat="server" Text="Nuevo Producto"
															CssClass="titulo" GroupName="grpTipoMadre"></asp:RadioButton>
															</asp:Panel></TD>
											</TR>
											<TR>
												<TD vAlign="top" colSpan="2" align="center">
													<asp:Label id="lblMensajeMadre" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
												</td>
											</tr>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR id="panProdMadre" runat="server">
												<TD style="WIDTH: 15%" align="right">
													<asp:Label id="lblProdMadre" runat="server" cssclass="titulo">Producto:&nbsp;</asp:Label></TD>
												<TD>
													<UC2:PROH id="usrProdMadre" runat="server" FilSexo="False" Tabla="productos" Saltos="1,2"
														AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="true" FilDocuNume="True" IncluirDeshabilitados="True"
														Ancho="800" AceptaNull="false" EsPropietario="false" CriaOrPropDescrip="Raza:" MuestraSoloRazaClieDerivr="True"
														Sexo="0"></UC2:PROH></TD>
											</TR>
											<TR id="panNuevMadre" runat="server">
												<TD colSpan="2" align="left">
													<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD align="right">
																<asp:Label id="lblRazaMadre" runat="server" cssclass="titulo">Raza MADRE:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:combobox id="cmbRazaMadre" class="combo" runat="server" cssclass="cuadrotexto" Width="250"
																	AceptaNull="true" onchange="Combo_change(this)" mostrarbotones="False" filtra="True" NomOper="razas_cargar"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD width="15%" align="right">
																<asp:Label id="lblAsocMadre" runat="server" cssclass="titulo">Asociaci�n:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:combobox id="cmbAsocMadre" class="combo" runat="server" cssclass="cuadrotexto" Width="300"
																	AceptaNull="true" onchange="Combo_change(this)" mostrarbotones="False" filtra="True" NomOper="asociaciones_cargar"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblNumeMadre" runat="server" cssclass="titulo">N�mero:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:numberbox id="txtNumeMadre" runat="server" cssclass="cuadrotexto" Width="80px" esdecimal="False"
																	MaxValor="99999999999999999" MaxLength="17"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblRPMadre" runat="server" cssclass="titulo">RP:&nbsp;</asp:Label></TD>
															<TD>
																<CC1:TEXTBOXTAB id="txtRPMadre" onkeypress="return HaceRPNumero('txtRPMadre','txtRPNumeMadre', event)"
																	runat="server" cssclass="cuadrotexto" Width="120px"></CC1:TEXTBOXTAB>
																<asp:Label id="lblRpNumeMadre" runat="server" cssclass="titulo">RP Num�rico:&nbsp;</asp:Label>
																<cc1:numberbox id="txtRPNumeMadre" runat="server" cssclass="cuadrotexto" Width="80px" esdecimal="False"
																	MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblNombMadre" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:Label></TD>
															<TD>
																<CC1:TEXTBOXTAB id="txtNombMadre" runat="server" cssclass="cuadrotexto" Width="365px"></CC1:TEXTBOXTAB></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblNaciFechaMadre" runat="server" cssclass="titulo">Fecha Nacim.:&nbsp;</asp:Label></TD>
															<TD>
																<cc1:DateBox id="txtNaciFechaMadre" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD height="30" vAlign="bottom" colSpan="2" align="center">
													<asp:Button id="btnGrabarPadres" runat="server" cssclass="boton" Width="80px" Text="Grabar"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnCancePadres" runat="server" cssclass="boton" Width="80px" Text="Cancelar"></asp:Button></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD background="imagenes/recde.jpg" width="13"></TD>
				</TR>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<script language="javascript">
				mSetearTipo('');
				mSetearTipo('Padre');
				mSetearTipo('Madre');
				
				if(document.all('panCarga')!=null)
					document.all('panCarga').style.top = document.body.scrollTop + 100;			
				if(document.all('panCargaDoble')!=null)
				 document.all('panCargaDoble').style.top = document.body.scrollTop + 100;			
			</script>
			<asp:textbox style="VISIBILITY: hidden" id="hdnSexo" runat="server"></asp:textbox><asp:textbox style="VISIBILITY: hidden" id="hdnSexoPadre" runat="server">1</asp:textbox><asp:textbox style="VISIBILITY: hidden" id="hdnSexoMadre" runat="server">0</asp:textbox><asp:textbox style="VISIBILITY: hidden" id="hdnRaza" runat="server"></asp:textbox><asp:textbox style="VISIBILITY: hidden" id="hdnRazaCodi" runat="server"></asp:textbox><asp:textbox style="VISIBILITY: hidden" id="hdnEstado" runat="server"></asp:textbox><asp:textbox style="VISIBILITY: hidden" id="hdnInscribeSRA" runat="server"></asp:textbox><asp:textbox style="VISIBILITY: hidden" id="hdnAnte" runat="server"></asp:textbox><ASP:TEXTBOX style="VISIBILITY: hidden" id="hdnSoloConsulta" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX style="VISIBILITY: hidden" id="hdnmensajeConsulta" runat="server"></ASP:TEXTBOX><asp:textbox style="VISIBILITY: hidden" id="hdnProd" runat="server" AutoPostBack="True"></asp:textbox></form>
	</BODY>
</HTML>
