Namespace SRA

Partial Class Password
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Usuarios
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"

   Private Sub mLimpiar()
      txtPass.Text = ""
      txtPass2.Text = ""
      txtPassAnt.Text = ""
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mModi()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGene As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGene.Modi()
         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, Session("sUserId").ToString())

      With ldsEsta.Tables(0).Rows(0)
         .Item("usua_pass") = SRA_Neg.Encript.gEncriptar(txtPass.Valor)
      End With

      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If SRA_Neg.Encript.gEncriptar(txtPassAnt.Text) <> clsSQLServer.gObtenerValorCampo(mstrConn, mstrTabla, Session("sUserId").ToString(), "usua_pass").ToString Then
         Throw New AccesoBD.clsErrNeg("Password actual incorrecto.")
      End If

      If txtPass.Valor.ToString <> txtPass2.Valor.ToString Then
         Throw New AccesoBD.clsErrNeg("No coinciden los passwords nuevos.")
      End If
   End Sub
#End Region

#Region "Eventos"
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
#End Region
End Class
End Namespace
