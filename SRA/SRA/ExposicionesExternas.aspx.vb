Namespace SRA

Partial Class ExposicionesExternas

    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "
   'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
   Protected WithEvents lblObser As System.Web.UI.WebControls.Label
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
    Public mstrTabla As String
    Public mstrParaPageSize As String
   Public mstrCmd As String
   Private mstrConn As String
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = "RG_Expo_Externas"
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mConsultar()
            mSetearMaxLength()
            mSetearEventos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Perfiles, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If
      btnAlta.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      btnLimp.Visible = (btnAlta.Visible Or btnBaja.Visible Or btnModi.Visible)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtCodi.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "exex_codi")
      txtAbre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "exex_abre")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "exex_obse")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal e As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = e.NewPageIndex
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()

      Try
         mstrCmd = "exec " + mstrTabla + "_consul null, "

         If txtFechaDesdeFil.Fecha Is DBNull.Value Then
            mstrCmd = mstrCmd + "null"
         Else
            mstrCmd = mstrCmd + "'" + Format(txtFechaDesdeFil.Fecha, "yyyyMMdd") + "'"
         End If

         If txtFechaHastaFil.Fecha Is DBNull.Value Then
            mstrCmd = mstrCmd + ",null"
         Else
            mstrCmd = mstrCmd + ",'" + Format(txtFechaHastaFil.Fecha, "yyyyMMdd") + "'"
         End If
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal e As DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
      ldsEsta.Tables(0).TableName = mstrTabla
      Return ldsEsta
   End Function

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)
      With ldsEsta.Tables(0).Rows(0)
         txtId.Text = .Item("exex_id").ToString()
         txtFechaD.Fecha = .Item("exex_desde_fecha")
         txtFechaH.Fecha = .Item("exex_hasta_fecha")
         txtCodi.Valor = .Item("exex_codi").ToString()
         txtAbre.Valor = .Item("exex_abre").ToString()
         txtObse.Valor = .Item("exex_obse").ToString()
      End With
      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      txtId.Text = ""
      txtCodi.Text = ""
      txtAbre.Text = ""
      txtObse.Text = ""
      txtFechaDesdeFil.Text = ""
      txtFechaHastaFil.Text = ""
      mSetearEditor(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If pbooVisi Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      mEstablecerPerfil()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Alta()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()

      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(txtId.Text)

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, txtId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("exex_id") = clsSQLServer.gFormatArg(txtId.Text, SqlDbType.Int)
         .Item("exex_desde_fecha") = txtFechaD.Fecha
         .Item("exex_hasta_fecha") = txtFechaH.Fecha
         .Item("exex_codi") = txtCodi.Valor
         .Item("exex_abre") = txtAbre.Valor
         .Item("exex_obse") = txtObse.Valor
      End With

      Return ldsEsta
   End Function
#End Region

   Private Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "ExposicionesExternas"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&exex_desde_fecha=" + lintFechaDesde.ToString
         lstrRpt += "&exex_hasta_fecha=" + lintFechaHasta.ToString
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      mConsultar()
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiar()
   End Sub
End Class
End Namespace
