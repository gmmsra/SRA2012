<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AcreditacionesConsul" CodeFile="AcreditacionesConsul.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD> 
		<TITLE>Acreditaciones Pendientes</TITLE>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function CargaCuentas(ComboOrigen,ComboDestino,Opc)
		{
			var sFiltro = document.all(ComboOrigen).value;
			if (sFiltro != '')
			{
				LoadComboXML("cuentas_bancos_cargar", sFiltro, ComboDestino, Opc);
			}
		}
		function expandir()
		{
			if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function ImprimirSeleccion()
		{
			document.all('hdnImprSele').value = "S";
			__doPostBack('hdnImprSele',''); 
		}
		function mSeleccionar(pSele)
		{
			if (document.all("grdDato").children(0).children.length > 1)
			{
				for (var fila=1; fila <= document.all("grdDato").children(0).children.length-1; fila++)
				{
					document.all("grdDato").children(0).children(fila).children(0).children(0).checked = pSele;
				}
			}
		}

		 function mImprimirCopias(pCanti,pEsta)
		 {
		    var lstrEstaPara = "";
		    var lstrEstaValor = "";
		    if (pEsta != 'O')
		    {
				lstrEstaPara = ";estado";
				lstrEstaValor = ";Duplicado";
			}
			for (var lintI = 1; lintI < pCanti; lintI++)	
			{
				if (document.all("hdnComprobante").value == "Proforma")
				{
					var sRet = ImprimirReporte("Proforma", "comp_id" + lstrEstaPara + ";random", document.all("hdnImprimir").value + lstrEstaValor + ";0", "<%=Session("sImpreTipo")%>",document.all("hdnImprimir").value,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
				}
				else
				{
					var sRet = ImprimirReporte("Factura", "comp_id" + lstrEstaPara + ";random", document.all("hdnImprimir").value + lstrEstaValor + ";0", "<%=Session("sImpreTipo")%>",document.all("hdnImprimir").value,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
				}
			}
		 }
		 		
		function Imprimir()
		{
			var comprob="";
			var compId="";
			var compIds="";
			var boolOriginal="";
			var compImpreso="";
			var compAnulado="";
			var lstrRpt = "";
			var valParam="";
			
			lstrRpt = "Recibo";		    
			var strComp = document.all('hdnIds').value.split(";");

			if (document.all('hdnIds').value != '')
			{
				try
				{
					for (var fila=0; fila <= strComp.length-1; fila++)
					{
							comprob = strComp[fila].split(",");
							compId = comprob[0]
							boolOriginal = comprob[1]
							compAnulado = comprob[2]

							var Estado="Original";
						    
							if (compAnulado==1) Estado="Anulado";
							else if (boolOriginal==0) Estado="Duplicado";
							else if (boolOriginal==1) Estado="Original";

							valParam=compId + ";0;" + Estado;
							var sRet = ImprimirReporte(lstrRpt,"comp_id;random;estado", valParam, "<%=Session("sImpreTipo")%>",compId,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							if(sRet=="0") 
							{
								document.all("hdnImprId").value="";
								return;
							}

							if (lstrRpt=="Recibo")
							{
								ImprimirTalones(compId, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							}
					}
				}
				catch(e)
				{
					document.all("hdnImprId").value="";
					alert("Error al intentar efectuar la impresi�n");
				}	
			}
			else
			{
				alert('Debe seleccionar los comprobantes a imprimir.');
			}
							
			document.all("hdnImprId").value="";
			document.all('hdnImprSele').value = "";

			compIds = document.all('hdnIdsImpr').value;
			if (compIds!="")
			{
			    if (window.confirm("�Se imprimieron los comprobantes correctamente?"))
				{
					document.all("hdnImprimio").value=compIds;
					__doPostBack('hdnImprimio','');				
				}
			}
		}
		</SCRIPT>
</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Estado de Acreditaciones</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
							<TABLE id=TableFil style="WIDTH: 100%" cellSpacing=0 cellPadding=0  align=left border=0>
							<TR>
								<TD>
									<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
										<TR>
											<TD style="HEIGHT: 8px" width=24></TD>
											<TD style="HEIGHT: 8px" width=42></TD>
											<TD style="HEIGHT: 8px" width=26></TD>
											<TD style="HEIGHT: 8px"></TD>
											<TD style="HEIGHT: 8px" width=26><CC1:BotonImagen id=btnBusc runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
											<TD style="HEIGHT: 8px" width=26><CC1:BotonImagen id=btnLimpiarFil runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg" ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD></TR>
										<TR>
											<TD style="HEIGHT: 8px" width=24><IMG height=25 src="imagenes/formfle.jpg" width=24 border=0></TD>
											<TD style="HEIGHT: 8px" width=42><IMG height=25 src="imagenes/formtxfiltro.jpg" width=113 border=0></TD>
											<TD style="HEIGHT: 8px" width=26><IMG height=25 src="imagenes/formcap.jpg" width=26 border=0></TD>
											<TD style="HEIGHT: 8px" background=imagenes/formfdocap.jpg colSpan=3><IMG height=25 src="imagenes/formfdocap.jpg" border=0></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD>
									<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
										<TR> 
											<TD width=3 background=imagenes/formiz.jpg><IMG height=30 src="imagenes/formiz.jpg" width=3 border=0></TD>
											<TD> <!-- FOMULARIO -->
												<TABLE class=FdoFld cellSpacing=0 cellPadding=0 width="100%" border=0>
													<TR>
														<TD style="HEIGHT: 10px" align=right background=imagenes/formfdofields.jpg colSpan=2></TD>
													</TR>
													<TR>
													    <TD style="WIDTH: 25%; HEIGHT: 17px" align=right><asp:Label id=Label1 runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
														<TD style="WIDTH: 75%"> <cc1:combobox class=combo id=cmbCentroEmisor runat="server" Width="50%" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
													</TR>
													<TR>
													    <TD align=right background=imagenes/formdivmed.jpg  colSpan=2 height=2><IMG height=2 src="imagenes/formdivmed.jpg" width=1></TD>
												    </TR>
													<TR>
														<TD style="HEIGHT: 17px" align=right><asp:Label id=lblBancFil runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
														<TD>
															<table border=0 cellpadding=0 cellspacing=0>
																<tr>
																	<td><cc1:combobox class=combo id=cmbBancFil runat="server" cssclass="cuadrotexto" Width="200" AceptaNull="false" NomOper="bancos_cargar" filtra="true" MostrarBotones="False" onchange="CargaCuentas('cmbBancFil', 'cmbCubaFil','T');"></cc1:combobox></td>
																	<td><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																	onclick="mBotonBusquedaAvanzada('bancos','banc_desc','cmbBancFil','Bancos','@con_cuentas=1');"
																	alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0"></td>
																</tr>
															</table>
														</TD>
													</TR>
													<TR>
														<TD align=right background=imagenes/formdivmed.jpg colSpan=2 height=2><IMG height=2 src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
													<TR>
													    <TD style="HEIGHT: 17px" align=right><asp:Label id=lblCubaFil runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
														<TD><cc1:combobox class=combo id=cmbCubaFil runat="server" Width="50%" nomoper="cuentas_bancos_cargar" AceptaNull="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align=right background=imagenes/formdivmed.jpg colSpan=2 height=2><IMG height=2 src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
							                        <TR>
														<TD style="HEIGHT: 17px" align=right><asp:label id=lblClieFil runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
														<TD><UC1:CLIE id=usrClieFil runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True" MuestraDesc="true" FilSociNume="True" Saltos="1,1" Tabla="Clientes"></UC1:CLIE></TD>
													</TR>
													<TR>
													    <TD align=right background=imagenes/formdivmed.jpg colSpan=2 height=2><IMG height=2 src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 17px" align=right><asp:Label id=lblFechaDesdeFil runat="server" cssclass="titulo" Width="150px">Fecha Deposito Desde:</asp:Label>&nbsp;</TD>
													    <TD><cc1:DateBox id=txtFechaDesdeFil runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp; 
															<asp:Label id=lblFechaHastaFil runat="server" cssclass="titulo">Hasta:</asp:Label>
															<cc1:DateBox id=txtFechaHastaFil runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
														</TD>
													</TR>
													<TR>
														<TD align=right background=imagenes/formdivmed.jpg colSpan=2 height=2><IMG height=2  src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
													<TR>
													    <TD style="HEIGHT: 17px" align=right><asp:Label id=lblNumeFil runat="server" cssclass="titulo">Nro. Acreditaci�n Bancaria:</asp:Label>&nbsp;</TD>
														<TD><CC1:TEXTBOXTAB id=txtNumeFil runat="server" cssclass="cuadrotexto" Width="270"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD align=right background=imagenes/formdivmed.jpg colSpan=2 height=2><IMG height=2 src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 17px" align=right><asp:Label id=lblEstaFil runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
														<TD><cc1:combobox class=combo id=cmbEstaFil runat="server" Width="200px" AceptaNull="False"><asp:ListItem Value="T">(Todas)</asp:ListItem>
																																					<asp:ListItem Value="I">Ingresadas</asp:ListItem>
																																					<asp:ListItem Value="P">Pendientes</asp:ListItem>
																																					<asp:ListItem Value="A">Anuladas</asp:ListItem>
																																					<asp:ListItem Value="C">Confirmadas</asp:ListItem>
															</cc1:combobox>
														</TD>
													</TR>
													<TR>
														<TD align=right background=imagenes/formdivmed.jpg colSpan=2 height=2><IMG height=2  src="imagenes/formdivmed.jpg" width=1></TD>
													</TR>
  													<tr>
						 								<TD style="WIDTH: 25%; HEIGHT: 17px" align="right"><asp:Label id="lblEmpCobElectFil" runat="server" cssclass="titulo">Empresa Cobro Electr�nica:</asp:Label>&nbsp;</TD>
														<td style="WIDTH: 75%"><cc1:combobox id="cmbEmpCobElecFil" class="combo" runat="server" Width="260px" obligatorio="True" /></td>
													</tr>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<tr>
														<TD style="WIDTH: 25%; HEIGHT: 17px" align="right"><asp:Label id="lblNroCompCobElectFil" runat="server" cssclass="titulo">Nro. Comprobante Cobro Elect.:</asp:Label>&nbsp;</TD>
														<td style="WIDTH: 75%"><CC1:TEXTBOXTAB id="txtNroCompCobElectFil" runat="server" cssclass="cuadrotexto" Width="270"></CC1:TEXTBOXTAB></TD>
													</tr>
													<TR>
														<TD style="HEIGHT: 17px" align=right></TD>
												<TD>
											</TD>
									</TR>
                          <TR>
                            <TD style="HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg 
                          colSpan=2></TD></TR></TABLE></TD>
                      <TD width=2 background=imagenes/formde.jpg><IMG height=2 
                        src="imagenes/formde.jpg" width=2 
                  border=0></TD></TR></TABLE></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Seleccionar Acreditaci�n" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="paco_id"></asp:BoundColumn>
											<asp:BoundColumn visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_ingr_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="Fecha"></asp:BoundColumn>
											<asp:BoundColumn DataField="banc_desc" HeaderText="Banco"></asp:BoundColumn>
											<asp:BoundColumn DataField="cuba_desc" HeaderText="Cuenta"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_nume" HeaderText="N�mero"></asp:BoundColumn>
											<asp:BoundColumn DataField="clie_apel" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
											<asp:BoundColumn DataField="recibo" HeaderText="Recibo"></asp:BoundColumn>
											<asp:BoundColumn DataField="impresa" HeaderText="Impresa"></asp:BoundColumn>
											<asp:TemplateColumn HeaderStyle-Width="10" HeaderText="Emitir">
													<ItemTemplate>
														<asp:CheckBox ID="chkSel" visible=<%#DataBinder.Eval(Container, "DataItem.impre")%> Checked=<%#DataBinder.Eval(Container, "DataItem.chk")%> Runat="server"></asp:CheckBox>														
													</ItemTemplate>
											</asp:TemplateColumn>
												<asp:TemplateColumn HeaderStyle-Width="10" HeaderText="Original">
													<ItemTemplate>
															<asp:CheckBox ID="chkOriginal" Runat="server" visible=<%#DataBinder.Eval(Container, "DataItem.comp_impre")%>
															Checked=<%#DataBinder.Eval(Container, "DataItem.original")%>></asp:CheckBox>
													</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible=False DataField="comp_id" HeaderText="comp_id"></asp:BoundColumn>											
											<asp:BoundColumn Visible=False DataField="original" HeaderText="original"></asp:BoundColumn>
											<asp:BoundColumn Visible=False DataField="anulado" HeaderText="anulado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="right" colSpan="3"><asp:button id="btnList" runat="server" cssclass="boton" Width="90px" Text="Listado" CausesValidation="False"></asp:button>&nbsp;&nbsp; 
									&nbsp;
									<asp:button id="btnReci" runat="server" cssclass="boton" Width="100px" Text="Emitir Recibos"
										CausesValidation="False"></asp:button>&nbsp;&nbsp;&nbsp;</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
            <TABLE class=FdoFld id=Table2 style="WIDTH: 100%" cellPadding=0 
            align=left border=0>
              <TR>
                <TD height=5>
<asp:Label id=lblTitu runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
                <TD vAlign=top align=right>&nbsp; 
<asp:ImageButton id=imgClose runat="server" ImageUrl="images\Close.bmp" CausesValidation="False" ToolTip="Cerrar"></asp:ImageButton></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=2>
                  <TABLE id=TableCabecera style="WIDTH: 100%" cellPadding=0 
                  align=left border=0>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=4 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblAcreNume runat="server" cssclass="titulo">Nro.Acreditaci�n:</asp:Label>&nbsp;</TD>
                      <TD colSpan=3>
<cc1:textboxtab id=txtNume runat="server" cssclass="cuadrotextodeshab" Width="100px" enabled="false"></cc1:textboxtab></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=4 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblMone runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbMone runat="server" cssclass="cuadrotextodeshab" Width="90px" enabled="false" Obligatorio="True"></cc1:combobox></TD>
                      <TD align=right>
<asp:Label id=lblImpo runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:numberbox id=txtImpo runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false" Obligatorio="True" EsDecimal="true"></cc1:numberbox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=4 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblFecha runat="server" cssclass="titulo">Fecha Valor:</asp:Label>&nbsp;</TD>
                      <TD noWrap>
<cc1:DateBox id=txtFecha runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false" Obligatorio="true"></cc1:DateBox>&nbsp;&nbsp; 
<asp:Label id=lblDepoFecha runat="server" cssclass="titulo">Fecha Dep�sito:</asp:Label>
<cc1:DateBox id=txtDepoFecha runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false" Obligatorio="true"></cc1:DateBox></TD>
                      <TD align=right>
<asp:Label id=lblIngrFecha runat="server" cssclass="titulo">Fecha Ingreso:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:DateBox id=txtIngrFecha runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false" Obligatorio="true"></cc1:DateBox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=4 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 9px" vAlign=top align=right>
<asp:Label id=lblClie runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
                      <TD style="HEIGHT: 9px" colSpan=3>
<UC1:CLIE id=usrClie runat="server" AceptaNull="false" Ancho="800" FilDocuNume="True" MuestraDesc="true" FilSociNume="True" Saltos="1,1" Tabla="Clientes" Activo="False"></UC1:CLIE>
<CC1:TEXTBOXTAB id="txtClie" enabled=False runat="server" cssclass="cuadrotexto" Width="420"></CC1:TEXTBOXTAB>
</TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=4 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblBanc runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbBanc runat="server" Width="240px" AceptaNull="false" enabled="false"></cc1:combobox></TD>
                      <TD align=right>
<asp:Label id=lblCuba runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbCuba runat="server" Width="180px" enabled="false"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblBancOrig runat="server" cssclass="titulo">Banco Origen:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbBancOrig runat="server" Width="240px" AceptaNull="false" enabled="false"></cc1:combobox></TD>
                      <TD noWrap align=right>
<asp:Label id=lblSucu runat="server" cssclass="titulo">Sucursal:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:textboxtab id=txtSucu runat="server" cssclass="cuadrotextodeshab" Width="140px" enabled="false"></cc1:textboxtab></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=4 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD vAlign=top align=center colSpan=4>
<asp:Label id=lblBaja runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
                      <TD colSpan=3></TD></TR>
                    <TR>
                      <TD vAlign=top align=right>
<asp:Label id=lblMoti runat="server" cssclass="titulo">Motivo Anulaci�n:</asp:Label>&nbsp;</TD>
                      <TD colSpan=3>
<cc1:textboxtab id=txtMoti runat="server" cssclass="cuadrotexto" Width="80%" enabled="false" TextMode="MultiLine" Height="60px"></cc1:textboxtab></TD></TR>
                    <TR>
                      <TD align=center colSpan=4>
<asp:Label id=lblDatos runat="server" cssclass="titulo">Datos del Comprobante</asp:Label>&nbsp;</TD></TR>
                    <TR><!--- detalle - Aplic-->
                      <TD vAlign=top align=right>
<asp:Label id=lblAplic runat="server" cssclass="titulo">Aplicaciones:</asp:Label>&nbsp;</TD>
                      <TD colSpan=3>
<asp:datagrid id=grdAplic runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="True" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Left" AllowPaging="False" PageSize="100">
																	<FooterStyle CssClass="footer"></FooterStyle>
																	<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																	<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																	<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																	<Columns>
																		<asp:BoundColumn DataField="descrip" HeaderText="Descripci�n"></asp:BoundColumn>
																		<asp:BoundColumn DataField="importe" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																	</Columns>
																	<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																</asp:datagrid></TD></TR>
                    <TR>
                      <TD vAlign=bottom background=imagenes/formfdofields.jpg 
                      colSpan=2 height=5></TD></TR>
                    <TR>
                      <TD vAlign=top align=right>
<asp:Label id=lblPagos runat="server" cssclass="titulo">Valores:</asp:Label>&nbsp;</TD>
                      <TD colSpan=3>
<asp:datagrid id=grdPagos runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="True" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Left" AllowPaging="False" PageSize="100">
																	<FooterStyle CssClass="footer"></FooterStyle>
																	<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																	<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																	<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																	<Columns>
																		<asp:BoundColumn DataField="descrip" HeaderText="Descripci�n"></asp:BoundColumn>
																		<asp:BoundColumn DataField="importe" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																	</Columns>
																	<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																</asp:datagrid></TD></TR>
                    <TR>
                      <TD vAlign=middle align=center colSpan=4 
                    height=5></TD></TR></TABLE></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
			<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			<asp:textbox id="hdnId" runat="server"></asp:textbox>
			<asp:textbox id="hdnSess" runat="server"></asp:textbox>
			<asp:textbox id="hdnIds" runat="server"></asp:textbox>
			<asp:textbox id="hdnIdsImpr" runat="server"></asp:textbox>
			<asp:textbox id="hdnImprSele" runat="server"></asp:textbox>
			<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
			<asp:textbox id="hdnEmctId" runat="server"></asp:textbox>
			<ASP:TEXTBOX id="hdnImprimio" AutoPostBack="True" runat="server"></ASP:TEXTBOX>
			<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["cmbConc"]!= null&&!document.all["cmbConc"].disabled)
			document.all["cmbConc"].focus();
		if (document.all('hdnImprSele').value == "S")
		{
			document.all('hdnImprSele').value = "";
			Imprimir();
		}			
		</SCRIPT>
	</BODY>
</HTML>
