<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DenunciaServicio" CodeFile="DenunciaServicio.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Denuncias de Servicios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mCargarRegistros(pRaza,pFiltro) 
		{
			sFiltro = pRaza.value; 
		    if (sFiltro!='')
			{
				var It =document.createElement("option");
				It.text="(Seleccione)";
				if (pFiltro)
				{
					LoadComboXML("rg_registros_tipos_cargar", sFiltro, "cmbRegiFil", "S");
					//if (document.all("cmbRegiFil").length==1)
					//	document.all("cmbRegiFil").add(It,0);
				}
				else
				{
					LoadComboXML("rg_registros_tipos_cargar", sFiltro, "cmbRegi", "S");
					//if (document.all("cmbRegi").length==1)
					//	document.all("cmbRegi").add(It,0);
				}
			}
			else
			{
				if (pFiltro)
					document.all('cmbRegiFil').selectedIndex = 0;
				else
					document.all('cmbRegi').selectedIndex = 0;
			}	
		}
		function mSiembraMultiple(pTipo)
		{
			document.all('btnAltaPadre').disabled = (document.all('hdnDetaId').value=='');
			document.all('btnBajaPadre').disabled = (document.all('hdnDetaId').value=='');
			if (pTipo.value == "8")
				document.all('panPadresDeta').style.display = '';
			else
				document.all('panPadresDeta').style.display = 'none';
		}
		function expandir() 
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function mVerErrores()
		{
			gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce=2&id=" + document.all("hdnId").value, 1, "700","400","20","40");
		}
		function mSetearRaza(pRaza)
		{
			document.all('hdnRaza').value = document.all('cmbRaza').value;
			if (document.all('txtcmbRaza').value != document.all('txtusrCria:cmbRazaCria').value || document.all('txtusrCria:cmbRazaCria').value != '')
			{
				document.all('usrCria_imgLimp').onclick();
				document.all('txtusrCria:cmbRazaCria').value = '';
				if (document.all('txtusrPadre:cmbProdRaza')!=null)
				{
					document.all('usrPadre_imgLimp').onclick();
					document.all('usrPadre:cmbProdRaza').selectedIndex = 0;
					document.all('txtusrPadre:cmbProdRaza').value = '';
				}
			}
			if (document.all('txtcmbRaza').value != document.all('txtusrCria:cmbRazaCria').value || document.all('txtusrCria:cmbRazaCria').value == '')
			{
				document.all('txtusrCria:cmbRazaCria').value = document.all('txtcmbRaza').value;
				document.all('txtusrCria:cmbRazaCria').onchange();
				if (document.all('txtusrPadre:cmbProdRaza')!=null)
				{
					document.all('txtusrPadre:cmbProdRaza').value = document.all('txtcmbRaza').value;
					document.all('txtusrPadre:cmbProdRaza').onchange();
				}
				else
				{
					if (document.all('txtusrPadre:cmbProdRaza').value=='')
					{
						document.all('txtusrPadre:cmbProdRaza').value = '';
						document.all('txtusrPadre:cmbProdRaza').onchange();
					}
				}
			}
			document.all('txtusrCria:cmbRazaCria').disabled = (document.all('cmbRaza').value!='');
			document.all('usrCria:cmbRazaCria').disabled = (document.all('cmbRaza').value!='');
			if (document.all('txtusrPadre:cmbProdRaza')!=null)
			{
				document.all('txtusrPadre:cmbProdRaza').disabled = (document.all('cmbRaza').value!='');
				document.all('usrPadre:cmbProdRaza').disabled = (document.all('cmbRaza').value!='');
			}
		}
		function mSetearCamposDetalle(pEspe)
		{			
			var lstrEspe = pEspe;
			switch(lstrEspe)
		    {
		      case '2':	  // 2 - EQUINOS
		      case '3':   // 3 - BOVINOS
		      case '11':  //11 - CAMELIDOS
		      {
		         document.all('panServDeta').style.display = '';
		         document.all('panServDeta').style.display = '';		 
		         break;
		      }
		      case '9':   // 9 - Caprinos
		      case '12':  // 12 - Ovinos
		      {
				 document.all('panServDeta').style.display = 'none';
				 document.all('panServDeta').style.display = 'none';			 
		         break;
		      }
		    }
		}
		function mSetearCampos(pEspe)
		{			
			var lstrEspe = pEspe;						
		    switch(lstrEspe)
		    {
		      case '2':	  // 2 - EQUINOS
		      case '3':   // 3 - BOVINOS
		      case '11':  //11 - CAMELIDOS
		      {
		         document.all('panServ').style.display = 'none';
		         document.all('panServ').style.display = 'none';		 
		         break;
		      }
		      case '9':   // 9 - Caprinos
		      case '12':  // 12 - Ovinos
		      {
				 document.all('panServ').style.display = '';
				 document.all('panServ').style.display = '';			 
		         break;
		      }
		    }
		}
		function mCambioEspecie(pRaza)
		{
			var lstrEspe = LeerCamposXML("razas", pRaza.value, "raza_espe_id");
			document.all('hdnEspe').value = lstrEspe;
			//document.all('cmbRaza').selectedIndex = 0;
			//document.all('txtcmbRaza').value = '';
			//mCargarRazas(pEspe);
			mSetearRaza(document.all('cmbRaza'));		    
		    mSetearCampos(lstrEspe);
		}		
		
		function mCargarRazas(pEspe)
		{
		//aaaaaaaaaaaaaaaaaaaaaa
		    var sFiltro = pEspe.value;
		    if (sFiltro!='')
				LoadComboXML("razas_cargar", sFiltro, "cmbRaza", "S");
			else
				document.all('cmbRaza').selectedIndex = 0;
		}	
		
			
		function usrClie_onchange()
		{
			var lstrClie = document.all('usrClie:txtId').value;
			document.all('usrCria:txtId').value = '';
			document.all('usrCria:txtCodi').value = '';
			document.all('usrCria:txtApel').value = '';
			document.all('txtusrCria:cmbRazaCria').value = '';
			document.all('usrCria:cmbRazaCria').selectedIndex = 0;
		}	
		function usrCriaFil_onchange()
		{
			var lstrCria = document.all('usrCriaFil:txtId').value;
			document.all('usrPadreFil:txtCriaId').value = lstrCria;
			document.all('usrMadreFil:txtCriaId').value = lstrCria;
		}
		function usrCria_onchange()
		{
			var lstrCria = document.all('usrCria:txtId').value;
			document.all('usrClie:txtId').value = '';
			document.all('usrClie:txtCodi').value = '';
			document.all('usrClie:txtApel').value = '';
			document.all('usrClie:txtDesc').value = '';
		}		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Denuncias de Servicios</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" width="100%" BorderStyle="none"
											BorderWidth="1px">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
													<TD>
														<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
															</TR>
															<!--	<TR>
																<TD align="right">
																	<asp:label id="lblRazaFil" runat="server" cssclass="titulo">Raza:&nbsp;</asp:label></TD>
																<TD>
																	<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="300px"
																		onchange="javascript:mCargarRegistros(this,true);" Height="20px" NomOper="razas_cargar" MostrarBotones="False"
																		filtra="true" AceptaNull="False"></cc1:combobox></TD>
															</TR>-->
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:label id="lblCriaFil" runat="server" cssclass="titulo">Raza/Criador:&nbsp;</asp:label></TD>
																<TD>
																	<UC1:CLIE id="usrCriaFil" runat="server" MostrarBotones="False" AceptaNull="false" FilDocuNume="True"
																		MuestraDesc="False" FilTipo="S" FilCUIT="True" ColCUIT="True" FilSociNume="True" Saltos="1,2"
																		Tabla="Criadores" FilTarjNume="False" FilAgru="False" ColCriaNume="True" FilClaveUnica="True"
																		CampoVal="Criador" Criador="True"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:&nbsp;</asp:label></TD>
																<TD>
																	<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" FilDocuNume="True" MuestraDesc="False"
																		FilTipo="S" FilCUIT="True" ColCUIT="True" ColDocuNume="True" FilSociNume="True" Saltos="1,2"
																		Tabla="Clientes" Ancho="800"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:label id="lblFolioFil" runat="server" cssclass="titulo">&nbsp;Nro.Folio:&nbsp;</asp:label></TD>
																<TD>
																	<cc1:numberbox id="txtFolioFil" runat="server" cssclass="cuadrotexto" Width="121px" MaxValor="9999999999999"
																		esdecimal="False"></cc1:numberbox>&nbsp;
																	<asp:label id="lblLineFil" runat="server" cssclass="titulo">&nbsp;Nro.L�nea:&nbsp;</asp:label>
																	<cc1:numberbox id="txtLineFil" runat="server" cssclass="cuadrotexto" Width="121px" MaxValor="9999999999999"
																		esdecimal="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Denuncia Desde:&nbsp;</asp:Label></TD>
																<TD>
																	<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																		Width="68px"></cc1:DateBox>&nbsp; &nbsp;
																	<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>
																	<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																		Width="68px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblRegiFil" runat="server" cssclass="titulo">Tipo Registro:&nbsp;</asp:Label></TD>
																<TD>
																	<cc1:combobox class="combo" id="cmbRegiFil" runat="server" Width="100px" AceptaNull="True" Obligatorio="False"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:label id="lblPadreFil" runat="server" cssclass="titulo">Padre:&nbsp;</asp:label></TD>
																<TD>
																	<UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" FilDocuNume="True" MuestraDesc="True"
																		FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" Ancho="800" sexo="1" FilSexo="false"
																		AutoPostBack="False"></UC1:PROD></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:label id="lblMadreFil" runat="server" cssclass="titulo">Madre:&nbsp;</asp:label></TD>
																<TD>
																	<UC1:PROD id="usrMadreFil" runat="server" AceptaNull="false" FilDocuNume="True" MuestraDesc="True"
																		FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" Ancho="800" sexo="0" FilSexo="false"
																		AutoPostBack="False"></UC1:PROD></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD noWrap align="right" background="imagenes/formfdofields.jpg"></TD>
																<TD width="72%" background="imagenes/formfdofields.jpg"><asp:checkbox id="chkVisto" Text="Incluir Registros Vistos y/o Aprobados" CssClass="titulo" Runat="server"></asp:checkbox></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
															</TR>
														</TABLE>
													</TD>
													<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD height="10"><A id="Ancla" name="Ancla"></A></TD>
									<TD vAlign="top" align="right" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnUpdateCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton2" runat="server" CausesValidation="false" CommandName="Update">
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="sede_id" ReadOnly="True" HeaderText="Id.EstablCliente"></asp:BoundColumn>
												<asp:BoundColumn DataField="_fecha" HeaderText="Fecha Denuncia" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Center"
													DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cliente" HeaderText="Propietario"></asp:BoundColumn>
												<asp:BoundColumn DataField="_folio_linea" HeaderText="Folio/L�nea"></asp:BoundColumn>
												<asp:BoundColumn DataField="_tipo_servicio" HeaderText="Tipo Servicio"></asp:BoundColumn>
												<asp:BoundColumn DataField="_raza" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn DataField="_registro" HeaderText="Registro"></asp:BoundColumn>
												<asp:BoundColumn DataField="_padre" HeaderText="HBA/RP Padre"></asp:BoundColumn>
												<asp:BoundColumn DataField="_madre" HeaderText="HBA/RP Madre"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" disabled="true" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
											ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
											BtnImage="edit.gif" ImageBoton="btnNuev0.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Denuncia"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Denuncia </asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" Height="21px" CausesValidation="False"> Servicios </asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderStyle="Solid" BorderWidth="1px"
												Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																<!--	<TR>
																		<TD width="18%" align="right">
																			<asp:Label id="lblNroControl" runat="server" cssclass="titulo">Nro. de Control:</asp:Label></TD>
																		<TD>
																			<cc1:numberbox id="txtNroControl" runat="server" cssclass="cuadrotexto" Width="81px" esdecimal="False"
																				MaxValor="999999999" Visible="true" MaxLength="3" EsPorcen="False"></cc1:numberbox></TD>
																	</TR>
																	-->
																	<TR>
																		<TD vAlign="middle" noWrap align="right" height="20">
																			<asp:Label id="lblCriador1" runat="server" cssclass="titulo">Raza/Criador:</asp:Label>&nbsp;</TD>
																		<TD>
																			<UC1:CLIE id="usrCriador1" runat="server" AceptaNull="true" FilDocuNume="True" MuestraDesc="False"
																				FilCUIT="True" ColCUIT="True" Saltos="1,1,1" Tabla="Criadores" FilTarjNume="False" FilAgru="False"
																				ColCriaNume="True" Criador="True" ColClaveUnica="True" FilClie="True"></UC1:CLIE></TD>
																	</TR>
																	
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="right" height="20">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD>
																			<UC1:CLIE id="usrClie" runat="server" AceptaNull="true" FilDocuNume="True" FilTipo="S" FilCUIT="True"
																				ColCUIT="True" ColDocuNume="True" FilSociNume="True" Saltos="1,2" Tabla="Clientes" FilClaveUnica="True"
																				ColClaveUnica="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" noWrap align="right" height="20">
																			<asp:Label id="lblRegi" runat="server" cssclass="titulo">Tipo Registro:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbRegi" runat="server" Width="100px" AceptaNull="True" Obligatorio="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="right" height="20">
																			<asp:Label id="lblFechaDenu" runat="server" cssclass="titulo">Fecha Denuncia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"
																				Obligatorio="True"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblPresFecha" runat="server" cssclass="titulo">Fecha Presentaci�n:</asp:Label>
																			<cc1:DateBox id="txtPresFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"
																				Obligatorio="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="right" height="20">
																			<asp:Label id="lblFolio" runat="server" cssclass="titulo">N� Folio:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtFolio" runat="server" cssclass="cuadrotexto" Width="121px" MaxValor="9999999999999"
																				esdecimal="False" Obligatorio="false"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" colSpan="2">
																			<DIV id="panServ">
																				<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" noWrap align="right" width="15%" height="20">
																							<asp:Label id="lblServ" runat="server" cssclass="titulo">Servicio Desde:</asp:Label>&nbsp;</TD>
																						<TD>
																							<cc1:DateBox id="txtServDesd" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox>
																							<asp:Label id="lblServHast" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																							<cc1:DateBox id="txtServHast" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" noWrap align="right" height="20">
																							<asp:Label id="lblSeti" runat="server" cssclass="titulo">Tipo de Servicio:</asp:Label>&nbsp;</TD>
																						<TD>
																							<cc1:combobox class="combo" id="cmbSeti" runat="server" Width="140px" AceptaNull="True" Obligatorio="False"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right" height="20">
																							<asp:Label id="lblPadre" runat="server" cssclass="titulo">Padre:</asp:Label>&nbsp;</TD>
																						<TD>
																							<UC1:PROD id="usrPadre" runat="server" AceptaNull="true" FilDocuNume="True" MuestraDesc="True"
																								FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" FilSexo="False" AutoPostBack="False"
																								Sexo="1"></UC1:PROD></TD>
																					</TR>
																				</TABLE>
																			</DIV>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:datagrid id="grdDeta" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdDeta_PageChanged"
																				AutoGenerateColumns="False" Visible="False" OnEditCommand="mEditarDatosDeta">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="sdde_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_item_nume" HeaderText="N� L�nea"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_madre_rp" HeaderText="RP Madre"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_madre_sra_nume" HeaderText="RIL Madre"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_servi_desde" HeaderText="Entrada" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_servi_hasta" HeaderText="Salida" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_tipo" HeaderText="Tipo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_padre_sra_nume" HeaderText="RILL RIA Ja�acho"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_padre_rp" HeaderText="RP Ja�acho"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_padre_sra_nume" HeaderText="HBA Toro"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_padre_rp" HeaderText="RP Toro"></asp:BoundColumn>
																					<asp:BoundColumn DataField="sdde_madre_rp" HeaderText="Tatuaje Vaca"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 193px" align="right" width="193">
																			<asp:Label id="lblLinea" runat="server" cssclass="titulo">N� L�nea:&nbsp;</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtLinea" runat="server" cssclass="cuadrotexto" Width="121px" MaxValor="9999999999999"
																				esdecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 193px" align="right" width="193">
																			<asp:Label id="lblMadreDeta" runat="server" cssclass="titulo">Madre:</asp:Label></TD>
																		<TD>
																			<UC1:PROD id="usrMadreDeta" runat="server" AceptaNull="true" FilDocuNume="True" MuestraDesc="True"
																				FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" FilSexo="False" AutoPostBack="False"
																				Sexo="0"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD align="left" colSpan="2">
																			<DIV id="panServDeta">
																				<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 123px" align="right">
																							<asp:Label id="lblServFecha" runat="server" cssclass="titulo">Servicio Desde:&nbsp;</asp:Label></TD>
																						<TD>
																							<cc1:DateBox id="txtServDesdeDeta" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																								Width="68px"></cc1:DateBox>&nbsp;
																							<asp:Label id="lblServHastaDeta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																							<cc1:DateBox id="txtServHastaDeta" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																								Width="68px"></cc1:DateBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 123px" noWrap align="right">
																							<asp:Label id="lblSetiDeta" runat="server" cssclass="titulo">Tipo de Servicio:&nbsp;</asp:Label></TD>
																						<TD>
																							<cc1:combobox class="combo" id="cmbSetiDeta" runat="server" Width="140px" onchange="javascript:mSiembraMultiple(this);"
																								AceptaNull="True" Obligatorio="False"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD style="HEIGHT: 18px" align="center" width="80%" colSpan="2">
																							<DIV id="panPadresDeta" style="DISPLAY: none">
																								<TABLE cellSpacing="0" cellPadding="0" width="96%" border="0">
																									<TR>
																										<TD height="22">
																											<asp:label id="Label1" runat="server" cssclass="titulo">Detalle de Padres de la Siembra M�ltiple:</asp:label></TD>
																									</TR>
																									<TR>
																										<TD>
																											<asp:datagrid id="grdPadresDeta" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																												AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																												OnPageIndexChanged="grdPadresDeta_PageChanged" OnUpdateCommand="mEditarPadre" AutoGenerateColumns="False"
																												Visible="true">
																												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																												<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																												<FooterStyle CssClass="footer"></FooterStyle>
																												<Columns>
																													<asp:TemplateColumn>
																														<HeaderStyle Width="15px"></HeaderStyle>
																														<ItemTemplate>
																															<asp:LinkButton id="LinkButton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																															</asp:LinkButton>
																														</ItemTemplate>
																													</asp:TemplateColumn>
																													<asp:BoundColumn Visible="False" DataField="sdpa_id" ReadOnly="True" HeaderText="sdpa_id"></asp:BoundColumn>
																													<asp:BoundColumn DataField="sdpa_padre_id" Visible="False" HeaderText="ID"></asp:BoundColumn>
																													<asp:BoundColumn DataField="sdpa_padre_rp" HeaderText="RP"></asp:BoundColumn>
																													<asp:BoundColumn DataField="sdpa_padre_sra_nume" HeaderText="HBA"></asp:BoundColumn>
																													<asp:BoundColumn DataField="_padre" HeaderText="Padre"></asp:BoundColumn>
																												</Columns>
																												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																											</asp:datagrid></TD>
																									</TR>
																									<TR>
																										<TD align="center" height="30">
																											<asp:Button id="btnAltaPadre" runat="server" cssclass="boton" Width="100px" Text="Agregar Padre"></asp:Button>&nbsp;&nbsp;
																											<asp:Button id="btnBajaPadre" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Eliminar Padre"></asp:Button>&nbsp;&nbsp;&nbsp;
																										</TD>
																									</TR>
																								</TABLE>
																							</DIV>
																						</TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 123px" align="right">
																							<asp:Label id="lblPadreDeta" runat="server" cssclass="titulo">Padre:&nbsp;</asp:Label></TD>
																						<TD>
																							<UC1:PROD id="usrPadreDeta" runat="server" AceptaNull="true" FilDocuNume="True" MuestraDesc="True"
																								FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" FilSexo="False" AutoPostBack="False"
																								Sexo="1"></UC1:PROD></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 123px" align="right">
																							<asp:Label id="lblMadreDetaBov" runat="server" cssclass="titulo">Madre:&nbsp;</asp:Label></TD>
																						<TD>
																							<UC1:PROD id="usrMadreDetaBov" runat="server" AceptaNull="true" FilDocuNume="True" MuestraDesc="True"
																								FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" FilSexo="False" AutoPostBack="False"
																								Sexo="0"></UC1:PROD></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																				</TABLE>
																			</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right" width="13%">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="450px" Height="50px" MaxLength="20"
																				TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="100px" Text="Agregar Serv."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Eliminar Serv."></asp:Button>&nbsp;
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Modificar Serv."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="100px" Text="Limpiar Serv."></asp:Button>&nbsp;
																			<asp:Button id="btnErr" runat="server" cssclass="boton" Width="100px" Text="Ver Errores"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnValorId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnModi" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnEspe" runat="server"></asp:textbox>
				<asp:textbox id="hdnItem" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaCarga" runat="server"></asp:textbox>
				<asp:textbox id="hdnMsgError" runat="server"></asp:textbox>
				<asp:textbox id="hdnPadreId" runat="server"></asp:textbox></DIV>
			<DIV style="DISPLAY: none"><asp:textbox id="hdnRaza" runat="server"></asp:textbox>
				<asp:textbox id="hdnFiltroProdCriaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["Ancla"]!= null)
			document.location='#Ancla';
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Denuncias de Servicios');
		if (document.all('cmbRaza')!=null)
			mSetearCampos(document.all('hdnEspe').value);
		if (document.all('cmbSetiDeta')!=null)
			mSetearCamposDetalle(document.all('hdnEspe').value);
		if (document.all('cmbRaza')!=null)
		{
			//document.all('cmbEspe').disabled = (document.all('hdnDetaCarga').value!='');
			document.all('cmbRaza').disabled = (document.all('hdnDetaCarga').value!='');
			document.all('txtcmbRaza').disabled = (document.all('hdnDetaCarga').value!='');
			document.all('txtusrCria:cmbRazaCria').disabled = (document.all('cmbRaza').value!='');
			document.all('usrCria:cmbRazaCria').disabled = (document.all('cmbRaza').value!='');
			if (document.all('txtusrPadre:cmbProdRaza')!=null)
			{
				document.all('txtusrPadre:cmbProdRaza').disabled = (document.all('cmbRaza').value!='');
				document.all('usrPadre:cmbProdRaza').disabled = (document.all('cmbRaza').value!='');
			}			
		}	
		if (document.all('cmbSetiDeta')!=null)
			mSiembraMultiple(document.all('cmbSetiDeta'));	
		</SCRIPT>
	</BODY>
</HTML>
