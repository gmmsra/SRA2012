Imports System.Data.SqlClient


Namespace SRA


Partial Class ValidacionGrupos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    Protected WithEvents hdnValorId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
   Protected WithEvents hdnModi As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_GruposValidacion
   Private mstrTablaGruposRazas As String = SRA_Neg.Constantes.gTab_GruposValidacionRazas
   Private mstrTablaGruposReglas As String = SRA_Neg.Constantes.gTab_GruposValidacionReglas
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
      clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecie, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "rg_reglas_vali", cmbRegRegla, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "rg_mensajes", cmbRegMensaje, "S")
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnVer.Attributes.Add("onclick", "mCargarReglas();return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

      txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "grva_codi")
      txtCodigoFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "grva_codi")
      txtAbre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "grva_abre")
      txtAbreFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "grva_abre")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "grva_obse")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      grdDeta.PageSize = Convert.ToInt32(mstrParaPageSize)
      grdRegla.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.EditItemIndex = -1
         If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
            grdDeta.CurrentPageIndex = 0
         Else
            grdDeta.CurrentPageIndex = E.NewPageIndex
         End If
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaGruposRazas)
         grdDeta.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridRegla_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdRegla.EditItemIndex = -1
         If (grdRegla.CurrentPageIndex < 0 Or grdRegla.CurrentPageIndex >= grdRegla.PageCount) Then
            grdRegla.CurrentPageIndex = 0
         Else
            grdRegla.CurrentPageIndex = E.NewPageIndex
         End If
         grdRegla.DataSource = mdsDatos.Tables(mstrTablaGruposReglas)
         grdRegla.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd = mstrCmd + " @grva_codi=" + IIf(txtCodigoFil.Valor.ToString = "", "null", txtCodigoFil.Text)
            mstrCmd = mstrCmd + ",@grva_abre='" + txtAbreFil.Valor.ToString + "'"

            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)

            grdDato.DataSource = dsDatos
            grdDato.DataBind()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
      btnBajaDeta.Enabled = Not (pbooAlta)
      btnModiDeta.Enabled = Not (pbooAlta)
      btnAltaDeta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtCodigo.Valor = .Item("grva_codi")
            txtAbre.Valor = .Item("grva_abre")
            txtObse.Valor = .Item("grva_obse")

            If Not .IsNull("grva_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("grva_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub
   Public Sub mEditarDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDire As DataRow

         hdnDetaId.Text = E.Item.Cells(1).Text
         ldrDire = mdsDatos.Tables(mstrTablaGruposRazas).Select("grra_id=" & hdnDetaId.Text)(0)

         With ldrDire
            cmbRaza.Valor = .Item("grra_raza_id")
            cmbEspecie.Valor = .Item("grra_espe_id")
         End With
         mSetearEditorDeta(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarRegla(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrRegla As DataRow

         hdnReglId.Text = E.Item.Cells(1).Text
         ldrRegla = mdsDatos.Tables(mstrTablaGruposReglas).Select("grre_id=" & hdnReglId.Text)(0)

         With ldrRegla
            cmbRegRegla.Valor = .Item("grre_regv_id")
            cmbRegMensaje.Valor = .Item("grre_rmen_id")

            If Not .IsNull("grre_baja_fecha") Then
               lblBajaReg.Text = "Regla dada de baja en fecha: " & CDate(.Item("grre_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaReg.Text = ""
            End If
         End With
         mSetearEditorDeta(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFil()
      txtCodigoFil.Text = ""
      txtAbreFil.Text = ""
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""

      txtObse.Text = ""
      txtAbre.Text = ""
      txtCodigo.Text = ""

      lblBaja.Text = ""

      mLimpiarDeta()
      mLimpiarReglas()

      grdDato.CurrentPageIndex = 0
      grdDeta.CurrentPageIndex = 0
      grdRegla.CurrentPageIndex = 0

      mCrearDataSet("")
      mShowTabs(1)
      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarDeta()
      hdnDetaId.Text = ""
      cmbEspecie.Limpiar()
      cmbRaza.Limpiar()
      mSetearEditorDeta(True)
   End Sub
   Private Sub mLimpiarReglas()
      hdnReglId.Text = ""
      txtRegla.Text = ""
      lblBajaReg.Text = ""
      cmbRegRegla.Limpiar()
      cmbRegMensaje.Limpiar()
      mSetearEditorDeta(True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaGruposRazas
      mdsDatos.Tables(2).TableName = mstrTablaGruposReglas

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      grdDeta.DataSource = mdsDatos.Tables(mstrTablaGruposRazas)
      grdDeta.DataBind()

      grdRegla.DataSource = mdsDatos.Tables(mstrTablaGruposReglas)
      grdRegla.DataBind()

      Session(mstrTabla) = mdsDatos
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If grdDeta.Items.Count = 0 Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar al menos una Especie/Raza para el grupo.")
      End If
   End Sub
   Private Sub mGuardarDatos()
      mValidaDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("grva_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("grva_codi") = txtCodigo.Valor
         .Item("grva_abre") = txtAbre.Valor
         .Item("grva_obse") = txtObse.Valor
         .Item("grva_baja_fecha") = DBNull.Value
         .Item("grva_audi_user") = Session("sUserId").ToString()
      End With
   End Sub
   'Razas
   Private Sub mActualizarDeta()
      Try
         mGuardarDatosDeta()

         mLimpiarDeta()
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaGruposRazas)
         grdDeta.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatosDeta()
      If cmbEspecie.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Especie.")
      End If

      'If cmbRaza.Valor.ToString = "" Then
      '   Throw New AccesoBD.clsErrNeg("Debe ingresar la Raza.")
      'End If

      If cmbRaza.Valor.ToString <> "" Then
         If mdsDatos.Tables(mstrTablaGruposRazas).Select("grra_espe_id=" + cmbEspecie.Valor.ToString + " and grra_raza_id=" + cmbRaza.Valor.ToString).GetUpperBound(0) > -1 Then
            Throw New AccesoBD.clsErrNeg("La raza indicada ya se encuentra asociada.")
         End If
      Else
         If mdsDatos.Tables(mstrTablaGruposRazas).Select("grra_espe_id=" + cmbEspecie.Valor.ToString + " and grra_raza_id is null").GetUpperBound(0) > -1 Then
            Throw New AccesoBD.clsErrNeg("La especie indicada ya se encuentra asociada.")
         End If
      End If

   End Sub
   Private Sub mBajaDeta()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaGruposRazas).Select("grra_id=" & hdnDetaId.Text)(0)
         row.Delete()
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaGruposRazas)
         grdDeta.DataBind()
         mLimpiarDeta()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosDeta()
      Dim ldrProc As DataRow

      mValidaDatosDeta()

      If hdnDetaId.Text = "" Then
         ldrProc = mdsDatos.Tables(mstrTablaGruposRazas).NewRow
         ldrProc.Item("grra_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaGruposRazas), "grra_id")
      Else
         ldrProc = mdsDatos.Tables(mstrTablaGruposRazas).Select("grra_id=" & hdnDetaId.Text)(0)
      End If

      With ldrProc
         .Item("grra_id") = clsSQLServer.gFormatArg(hdnDetaId.Text, SqlDbType.Int)
         .Item("grra_grva_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("grra_raza_id") = cmbRaza.Valor
         If cmbRaza.Valor.ToString() <> "" Then
            .Item("_raza_desc") = cmbRaza.SelectedItem.Text
         Else
            .Item("_raza_desc") = ""
         End If
         .Item("grra_espe_id") = cmbEspecie.Valor
         .Item("_espe_desc") = cmbEspecie.SelectedItem.Text
         .Item("grra_audi_user") = Session("sUserId").ToString()
         .Item("grra_baja_fecha") = DBNull.Value
      End With
      If (hdnDetaId.Text = "") Then
         mdsDatos.Tables(mstrTablaGruposRazas).Rows.Add(ldrProc)
      End If
   End Sub
   'Reglas
   Private Sub mActualizarReg()
      Try
         mGuardarDatosReg()

         mLimpiarReglas()
         grdRegla.DataSource = mdsDatos.Tables(mstrTablaGruposReglas)
         grdRegla.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatosReg()
      If cmbRegRegla.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Regla.")
      End If

      If cmbRegMensaje.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Mensaje.")
      End If

      If mdsDatos.Tables(mstrTablaGruposReglas).Select("grre_regv_id=" + cmbRegRegla.Valor.ToString + " and grre_rmen_id=" + cmbRegMensaje.Valor.ToString).GetUpperBound(0) > -1 Then
         Throw New AccesoBD.clsErrNeg("Ya existe Regla/Mensaje seleccionados en la tabla.")
      End If
   End Sub
   Private Sub mBajaReg()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaGruposReglas).Select("grre_id=" & hdnReglId.Text)(0)
         row.Delete()
         grdRegla.DataSource = mdsDatos.Tables(mstrTablaGruposReglas)
         grdRegla.DataBind()
         mLimpiarReglas()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosReg()
      Dim ldrRegl As DataRow

      mValidaDatosReg()

      If hdnReglId.Text = "" Then
         ldrRegl = mdsDatos.Tables(mstrTablaGruposReglas).NewRow
         ldrRegl.Item("grre_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaGruposReglas), "grre_id")
      Else
         ldrRegl = mdsDatos.Tables(mstrTablaGruposReglas).Select("grre_id=" & hdnReglId.Text)(0)
      End If

      With ldrRegl
         .Item("grre_id") = clsSQLServer.gFormatArg(hdnReglId.Text, SqlDbType.Int)
         .Item("grre_grva_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("grre_regv_id") = cmbRegRegla.Valor
         .Item("_regla") = cmbRegRegla.SelectedItem.Text
         .Item("grre_rmen_id") = cmbRegMensaje.Valor
         .Item("_mensaje") = cmbRegMensaje.SelectedItem.Text
         .Item("grre_audi_user") = Session("sUserId").ToString()
         .Item("grre_baja_fecha") = DBNull.Value
         .Item("_estado") = "Activo"
      End With
      If (hdnReglId.Text = "") Then
         mdsDatos.Tables(mstrTablaGruposReglas).Rows.Add(ldrRegl)
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panCabecera.Visible = False
         lnkCabecera.Font.Bold = False
         panDeta.Visible = False
         lnkDeta.Font.Bold = False
         panReglas.Visible = False
         lnkReglas.Font.Bold = False
         Select Case Tab
            Case 1
               panCabecera.Visible = True
               lnkCabecera.Font.Bold = True
            Case 2
               panDeta.Visible = True
               lnkDeta.Font.Bold = True
            Case 3
               panReglas.Visible = True
               lnkReglas.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkDirecciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkReglas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReglas.Click
      mShowTabs(3)
   End Sub
   'Botones generales
   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   'Botones de Razas
   Private Sub btnAltaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDeta()
   End Sub
   Private Sub btnBajaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
      mBajaDeta()
   End Sub
   Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDeta()
   End Sub
   Private Sub btnLimpDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub
   'Botones de Reglas
   Private Sub btnAltaReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaReg.Click
      mActualizarReg()
   End Sub
   Private Sub btnBajaReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaReg.Click
      mBajaReg()
   End Sub
   Private Sub btnModiReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiReg.Click
      mActualizarReg()
   End Sub
   Private Sub btnLimpReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpReg.Click
      mLimpiarReglas()
   End Sub
#End Region

Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click

End Sub

    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "ValidacionesGrupo"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            If (txtCodigoFil.Valor.ToString <> "") Then
                lstrRpt += "&grva_codi=" + txtCodigoFil.Valor.ToString
            End If
            'lstrRpt += "&grva_codi=" + txtCodigoFil.Valor.ToString
            lstrRpt += "&grva_abre=" + txtAbreFil.Valor.ToString
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

   
End Class

End Namespace
