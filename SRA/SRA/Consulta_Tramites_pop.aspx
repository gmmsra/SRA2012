<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Consulta_Tramites_pop" CodeFile="Consulta_Tramites_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Consulta Tr�mites</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function mAbrirActa(pActaId, pProceso) {
			
			switch (pProceso) {
			case 1:
				//TransferenciaProductos = 1
				gAbrirVentanas("TransferenciaAnimalPie.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 10:
				//ImportacionProductos = 10
				gAbrirVentanas("ImportacionAnimalPie.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 20:
				//ExportacionProductos = 20
				gAbrirVentanas("ExportacionAnimalPie.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 32:
				gAbrirVentanas("TransferenciaEmbrion.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 33:
				gAbrirVentanas("ExportacionEmbriones.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;
				
			case 31:
				gAbrirVentanas("ExportacionSemen.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 11:
				gAbrirVentanas("ImportacionSemen.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 12:
				gAbrirVentanas("ImportacionEmbriones.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 104:
				gAbrirVentanas("prestamos.aspx?tram_id=" + pActaId, 1, 750, 500, 50, 50);
				break;

			case 103:
				gAbrirVentanas("embargos.aspx?tram_id=" + pActaId, 1, 750, 500, 100, 100);
				break;
			}

		}
		function ValidaControl(pHab,pId)
		{
			if (pHab == "1") 
				document.all["divImagen"+pId].style.display="inline";
			else 
				document.all["divImagen"+pId].style.display="none";
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR>
					<TD style="HEIGHT: 15px" vAlign="bottom" colSpan="6" height="15"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Consulta Tr�mites</asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 15px" vAlign="bottom" colSpan="6" height="15">
						<asp:panel class="noprint" id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid"
							BorderWidth="0" Visible="True" Width="100%">
							<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
								<TR>
									<TD>
										<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD style="HEIGHT: 8px" width="24"></TD>
												<TD style="HEIGHT: 8px" width="42"></TD>
												<TD style="HEIGHT: 8px" width="26"></TD>
												<TD style="HEIGHT: 8px" align="right">
													<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
														BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
														BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
												<TD style="HEIGHT: 8px" width="26">
													<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
														BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
														BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
												<TD style="HEIGHT: 8px" width="10"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
												<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
												<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
												<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD width="3" background="imagenes/formiz.jpg"><IMG height="10" src="imagenes/formiz.jpg" width="3" border="0"></TD>
												<TD><!-- FOMULARIO -->
													<TABLE class="noprint" id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD background="imagenes/formfdofields.jpg" colSpan="3" height="6"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 160px" align="right" background="imagenes/formfdofields.jpg">
																<asp:label id="lblFechaFil" runat="server" cssclass="titulo">Fecha Desde:</asp:label>&nbsp;</TD>
															<TD background="imagenes/formfdofields.jpg" colSpan="2">
																<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>
																<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Fecha Hasta:</asp:label>
																<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 160px" align="right" background="imagenes/formfdofields.jpg">
																<asp:Label id="lblTipoFil" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
															<TD background="imagenes/formfdofields.jpg" colSpan="2">
																<cc1:combobox class="combo" id="cmbTipoFil" runat="server" Width="260px" AceptaNull="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 160px; HEIGHT: 7px" align="right" background="imagenes/formfdofields.jpg">
																<asp:Label id="lblEstaFil" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
															<TD style="HEIGHT: 7px" background="imagenes/formfdofields.jpg" colSpan="2">
																<cc1:combobox class="combo" id="cmbEstaFil" runat="server" Width="145px" AceptaNull="True">
																	<asp:ListItem Value="T">(Todos)</asp:ListItem>
																	<asp:ListItem Value="A">Activo</asp:ListItem>
																	<asp:ListItem Value="I">Inactivo</asp:ListItem>
																</cc1:combobox></TD>
														</TR>
														<TR>
															<TD background="imagenes/formfdofields.jpg" colSpan="3" height="6"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 19.26%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
															<TD style="WIDTH: 13.43%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
															<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
														</TR>
													</TABLE>
												</TD>
												<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD vAlign="top" colspan="6"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px"
							PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
							BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
																
								<asp:BoundColumn DataField="tram_inic_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"
									HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
								<asp:BoundColumn DataField="ttra_desc" HeaderText="Tipo"></asp:BoundColumn>
								<asp:BoundColumn DataField="tram_nume" HeaderText="N�mero"></asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<div style="DISPLAY: none" id="divImagen<%#DataBinder.Eval(Container, "DataItem.trpr_id")%>">
											<a href="javascript:mAbrirActa(<%#DataBinder.Eval(Container, "DataItem.trpr_tram_id")%>,<%#DataBinder.Eval(Container, "DataItem.tram_ttra_id")%>);">
												<img onload="javascript:ValidaControl(1,<%#DataBinder.Eval(Container, "DataItem.trpr_id")%>);" src='imagenes/Buscar16.gif' style="cursor:hand; VISIBILITY: " border="0" alt="Ver Detalle Tr�mite" />
											</a>
										</div>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Comprador" HeaderText="Comprador"></asp:BoundColumn>
								<asp:BoundColumn DataField="Vendedor" HeaderText="Vendedor"></asp:BoundColumn>
								<asp:BoundColumn DataField="esta_desc" HeaderText="Estado"></asp:BoundColumn>
								<asp:BoundColumn DataField="estado_actual" HeaderText="Estado Actual"></asp:BoundColumn>
								
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</TD>
				</TR>
				<TR height="100%">
					<TD align="right" colspan="3" valign="top">
						<asp:Button id="btnList" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
							Text="Listar" Visible="true"></asp:Button>
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
