<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Tarjetas" CodeFile="Tarjetas.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Tarjetas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<SCRIPT language="javascript">
		
			function mSetearCuenta(pCmb,pCuen)
			{
				document.all(pCuen).value=pCmb.value;
			}

			function HabilitaControles(ContOrig,ContHab1,ContHab2,Conthab3)
			{
				document.all(ContHab1).disabled=!(document.all(ContOrig).checked==true);
				document.all(ContHab2).disabled=!(document.all(ContOrig).checked==true);
				document.all(Conthab3).disabled=!(document.all(ContOrig).checked==true);
				document.all("txt" + Conthab3).disabled=!(document.all(ContOrig).checked==true);
			}

			function CargaCuentas(ComboOrigen,ComboDestino,HiddenDestino)
			{
				var sFiltro = document.all(ComboOrigen).value;
				if (sFiltro != '')
				{
					LoadComboXML("cuentas_bancos_cargar", sFiltro, ComboDestino, 'S');
				}
				else
				{
					document.all(ComboDestino).innerHTML = "";
					document.all(HiddenDestino).value = "";
				}
			}
					
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD></TD>
									<TD width="100%" colSpan="2"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 27px" height="27"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Tarjetas</asp:label></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<FooterStyle CssClass="footer"></FooterStyle>
											<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
											<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="tarj_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="tarj_codi" ReadOnly="True" HeaderText="C&#243;digo">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="tarj_desc" ReadOnly="True" HeaderText="Descripci&#243;n"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cuct_pes_desc" ReadOnly="True" HeaderText="Cuenta Contable Pesos"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cuct_dol_desc" ReadOnly="True" HeaderText="Cuenta Contable D�lares"></asp:BoundColumn>
												<asp:BoundColumn DataField="_Estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<TD colSpan="2"></TD>
								</tr>
								<TR>
									<TD style="HEIGHT: 9px" height="9"></TD>
									<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
											ImageOver="btnNuev2.gif" ForeColor="Transparent" ToolTip="Agregar una Nueva Tarjeta" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
											BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
											BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent" ToolTip="Listar"
											ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD colSpan="2"></TD>
								</tr>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Visible="False" Height="124%">
											<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
															cssclass="solapa" Height="21px" Font-Bold="True" Width="80px" CausesValidation="False"> General</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDebito" runat="server"
															cssclass="solapa" Height="21px" Width="80px" CausesValidation="False"> Debito</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkMonedas" runat="server"
															cssclass="solapa" Height="21px" Width="80px" CausesValidation="False"> Moneda</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkEstab" runat="server" cssclass="solapa"
															Height="21px" Width="70px" CausesValidation="False"> Establecimientos</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" Height="116px">
											<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
												align="left">
												<TR>
													<TD style="WIDTH: 100%" vAlign="top" align="right">
														<asp:imagebutton id="btnClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:imagebutton></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
															<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																<TR>
																	<TD style="WIDTH: 300px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 300px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblDescripcion" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtDescripcion" runat="server" cssclass="cuadrotexto" Width="180px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 300px" align="right">
																		<asp:Label id="lblDebitoEnCuenta" runat="server" cssclass="titulo"><DIV>Tarjeta Cr�dito 
																				- D�bito en Cuenta</DIV>:</asp:Label>&nbsp;
																	</TD>
																	<TD colSpan="4">
																		<cc1:combobox id="cmbDebitoEnCuenta" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox></TD>
																<TR>
																<TR>
																	<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="5"
																		align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 300px; HEIGHT: 22px" align="right">
																		<asp:Label id="lblPorcComCupo" runat="server" cssclass="titulo">Porcentaje Comisi�n Cupon:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 22px" colSpan="4">
																		<CC1:NUMBERBOX id="txtPorcComCupo" runat="server" cssclass="cuadrotexto" Width="80px" EsDecimal="True"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 300px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblMontComCupo" runat="server" cssclass="titulo">Monto Comisi�n Cupon:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:NUMBERBOX id="txtMontComCupo" runat="server" cssclass="cuadrotexto" Width="80px" EsDecimal="True"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 300px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblMaxCuotas" runat="server" cssclass="titulo">M�ximo Cuotas:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:NUMBERBOX id="txtMaxCuotas" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="True"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 300px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblMaxCuotasTel" runat="server" cssclass="titulo">M�ximo Cuotas Vta.Telef�noca:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:NUMBERBOX id="txtMaxCuotasTel" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="True"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panDebito" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE style="WIDTH: 100%" id="TablaDebito" border="0" cellPadding="0" align="left">
																<TR>
																	<TD style="WIDTH: 333px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblNroComSoci" runat="server" cssclass="titulo">N� Comercio Socios:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtNroComSoci" runat="server" cssclass="cuadrotexto" Width="256px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 333px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblNomArchSoci" runat="server" cssclass="titulo">Nombre Archivo Socios:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtNomArchSoci" runat="server" cssclass="cuadrotexto" Width="256px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 333px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblBancSocios" runat="server" cssclass="titulo">Banco de Dep�sito Socios:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<TABLE cellSpacing="0" cellPadding="0">
																			<TR>
																				<TD>
																					<cc1:combobox id="cmbBancSocios" class="combo" runat="server" cssclass="cuadrotexto" Width="328px"
																						NomOper="bancos_cargar" filtra="true" MostrarBotones="False" onchange="CargaCuentas('cmbBancSocios','cmbCuenSocios','hdnCtaSoci');"></cc1:combobox></TD>
																				<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																						id="btnBancoBusq" language="javascript" onclick="BusqCombo('bancos','@con_cuentas=1','banc_desc','cmbBancSocios','Bancos');"
																						border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 42.95%; HEIGHT: 17px" align="right">
																		<asp:Label id="lblCuenSocios" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 85%">
																		<cc1:combobox id="cmbCuenSocios" class="combo" runat="server" cssclass="cuadrotexto" Height="20px"
																			Width="280px" NomOper="centas_ctables_cargar" filtra="False" MostrarBotones="False" onchange="javascript:mSetearCuenta(this,'hdnCtaSoci');"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="5"
																		align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 333px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblNroComISEA" runat="server" cssclass="titulo">N� Comercio ISEA:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtNroComISEA" runat="server" cssclass="cuadrotexto" Width="256px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 333px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblNomArchISEA" runat="server" cssclass="titulo">Nombre Archivo ISEA:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtNomArchISEA" runat="server" cssclass="cuadrotexto" Width="256px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 333px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblBancISEA" runat="server" cssclass="titulo">Banco de Dep�sito ISEA:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<TABLE id="Table3" cellSpacing="0" cellPadding="0">
																			<TR>
																				<TD>
																					<cc1:combobox id="cmbBancISEA" class="combo" runat="server" Width="328px" MostrarBotones="False"
																						onchange="CargaCuentas('cmbBancISEA','cmbCuenISEA','hdnCtaIsea');" nomoper="bancos_cargar"
																						CssClass="cuadrotexto" Filtra="true"></cc1:combobox></TD>
																				<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																						id="btnBancoIseaBusq" language="javascript" onclick="BusqCombo('bancos','@con_cuentas=1','banc_desc','cmbBancISEA','Bancos');"
																						border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 42.95%; HEIGHT: 17px" noWrap align="right">
																		<asp:Label id="lblCuenISEA" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 85%">
																		<cc1:combobox id="cmbCuenISEA" class="combo" runat="server" cssclass="cuadrotexto" Height="20px"
																			Width="280px" NomOper="centas_ctables_cargar" filtra="False" MostrarBotones="False" onchange="javascript:mSetearCuenta(this,'hdnCtaIsea');"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panMoneda" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE style="WIDTH: 100%" id="TablaMoneda" border="0" cellPadding="0" align="left">
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right"></TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<asp:CheckBox id="chkConPesos" onclick="HabilitaControles('chkConPesos','txtMontoMaxSAPes','txtMontoMinPes','cmbCuentaContablePes')"
																			CssClass="titulo" Text="Opera en Pesos" Runat="server" Checked="true"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblMontoMaxSAPes" runat="server" cssclass="titulo">Monto M�ximo Sin Autorizaci�n:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:NUMBERBOX id="txtMontoMaxSAPes" runat="server" cssclass="cuadrotexto" Width="100px"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblMontoMinPes" runat="server" cssclass="titulo">Monto M�nimo:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:NUMBERBOX id="txtMontoMinPes" runat="server" cssclass="cuadrotexto" Width="100px"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblCuentaContablePes" runat="server" cssclass="titulo">Cuenta Contable:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<TABLE border="0" cellSpacing="0" cellPadding="0">
																			<TR>
																				<TD>
																					<cc1:combobox id="cmbCuentaContablePes" class="combo" runat="server" cssclass="cuadrotexto" Height="20px"
																						Width="280px" NomOper="centas_ctables_cargar" filtra="true" MostrarBotones="False" TextMaxLength="7"></cc1:combobox></TD>
																				<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																						id="btnAvanBusq2" onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuentaContablePes','Cuentas Contables','');"
																						border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="5"
																		align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right"></TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<asp:CheckBox id="chkConDolares" onclick="HabilitaControles('chkConDolares','txtMontoMaxSADol','txtMontoMinDol','cmbCuentaContableDol')"
																			CssClass="titulo" Text="Opera en D�lares" Runat="server" Checked="false"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblMontoMaxSADol" runat="server" cssclass="titulo">Monto M�ximo Sin Autorizaci�n:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:NUMBERBOX id="txtMontoMaxSADol" runat="server" cssclass="cuadrotexto" Width="100px"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblMontoMinDol" runat="server" cssclass="titulo">Monto M�nimo:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:NUMBERBOX id="txtMontoMinDol" runat="server" cssclass="cuadrotexto" Width="100px"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblCuentaContableDol" runat="server" cssclass="titulo">Cuenta Contable:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<TABLE border="0" cellSpacing="0" cellPadding="0">
																			<TR>
																				<TD>
																					<cc1:combobox id="cmbCuentaContableDol" class="combo" runat="server" cssclass="cuadrotexto" Height="20px"
																						Width="280px" NomOper="centas_ctables_cargar" filtra="true" MostrarBotones="False" TextMaxLength="7"></cc1:combobox></TD>
																				<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																						id="btnAvanBusq2" onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuentaContableDol','Cuentas Contables','');"
																						border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="5"
																		align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panEstab" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE style="WIDTH: 100%" id="TablaEstab" border="0" cellPadding="0" align="left">
																<TR>
																	<TD vAlign="top" colSpan="2" align="center">
																		<asp:datagrid id="grdEstab" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatosEstab"
																			OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																			AllowPaging="True" BorderWidth="1px" BorderStyle="None" width="100%" PageSize="100">
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																			<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="2%"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="taes_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn DataField="taes_esta_nume" ReadOnly="True" HeaderText="N�mero">
																					<HeaderStyle Width="45%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="taes_esta_refe" ReadOnly="True" HeaderText="Referencia"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																		align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 213px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblNumeroEstab" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																	<TD>
																		<CC1:TEXTBOXTAB id="txtNumeroEstab" runat="server" cssclass="cuadrotexto" Width="272px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 213px; HEIGHT: 22px" align="right">
																		<asp:Label id="lblReferEstab" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 22px">
																		<CC1:TEXTBOXTAB id="txtReferEstab" runat="server" cssclass="cuadrotexto" Width="376px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																		align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR height="30">
																	<TD colSpan="2" align="center">
																		<asp:Button id="btnAltaEstab" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																		<asp:Button id="btnBajaEstab" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Baja"></asp:Button>
																		<asp:Button id="btnModiEstab" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Modificar"></asp:Button>
																		<asp:Button id="btnLimpEstab" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Limpiar"></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										<DIV></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCtaSoci" runat="server"></asp:textbox>
				<asp:textbox id="hdnCtaIsea" runat="server"></asp:textbox>
				<asp:textbox id="hdnEstabId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodigo"]!= null)
			document.all["txtCodigo"].focus();
								   	
		</SCRIPT>
	</BODY>
</HTML>
