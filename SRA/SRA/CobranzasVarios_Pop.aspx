<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CobranzasVarios_Pop" CodeFile="CobranzasVarios_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>VARIOS</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
		function btnBuscar_click(pstrTabla,pTitu)
		{
		  var filClieGene=0;
		  if(mClienteGenerico()=="True")
          {
             filClieGene="1";
          }  
		  var campo="conc_desc"
		  var combo="cmbConc"; 
		  //var filtro='@conc_acti_id='+ document.all("hdnActiId").value + ',@conc_clie_gene=' + filClieGene; 
		  var filtro='@conc_reci=1,@conc_clie_gene=' + filClieGene; 
		  BusqCombo(pstrTabla,filtro,campo,combo,pTitu)
	    }
	    
		function mClienteGenerico()
		{
			var sRet; 
			sRet = ""
			if(document.all("hdnClieId").value!="")//usrClie:txtId
				{
				sRet=EjecutarMetodoXML("Utiles.ClienteGenerico", document.all("hdnClieId").value);
				}
			return sRet
		}
  	    
		function mSetearConcepto(pConc)
		{
			var strRet = LeerCamposXML("conceptos", "@conc_id="+pConc.value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet!='')
			{
				var lstrCta = strRet.substring(0,1);
				document.all('hdnCta').value = lstrCta;
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCcos').disabled = true;
					document.all('txtcmbCcos').disabled = true;
					document.all('cmbCcos').innerHTML = '';
					document.all('txtcmbCcos').value = '';
				}
				else
				{
					document.all('cmbCcos').disabled = false;
					document.all('txtcmbCcos').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCcos", "N");					
				}
			}			
		}
		function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="Salida();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD>
									<P></P>
								</TD>
								<TD height="5">
									<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
								<TD vAlign="top" align="right">&nbsp;
									<asp:ImageButton id="imgClose" runat="server" ImageUrl="imagenes\Close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center" onclick="Variable=1;">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										CellPadding="1" GridLines="None" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="coco_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="_desc" HeaderText="Concepto"></asp:BoundColumn>
											<asp:BoundColumn DataField="coco_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR HEIGHT="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" BorderStyle="Solid" BorderWidth="1px">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblConc" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
														<TABLE cellSpacing="0" cellPadding="0" border="0">
															<TR>
																<TD>
																	<cc1:combobox class="combo" id="cmbConc" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"
																		NomOper="cuentas_cargar" filtra="true" MostrarBotones="False" onchange="javascript:mSetearConcepto(this);"
																		TextMaxLength="5"></cc1:combobox></TD>
																<TD>&nbsp;<IMG language="javascript" id="btnConcepBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																		onclick="btnBuscar_click('conceptos','Conceptos');" alt="Busqueda avanzada" src="imagenes/Buscar16.gif"
																		border="0">
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
														<table border=0 cellpadding=0 cellspacing=0>
														<tr>
														<td>															
														<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="cuentas_cargar"
															filtra="true" MostrarBotones="False" TextMaxLength="6"></cc1:combobox>
														</td>
														<td>&nbsp;<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																							onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos','Centros de Costo','@cuenta='+document.all('hdnCta').value);"
																							alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
														</td>
														</tr>
														</table>	
													</TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
													<TD background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"
															EsDecimal="true"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblDescAmpliConcep" runat="server" cssclass="titulo">Descrip. Ampliada:</asp:Label>&nbsp;</TD>
													<TD background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtDescAmpliConcep" runat="server" cssclass="textolibredeshab" Width="400px"
															EnterPorTab="False" TextMode="MultiLine" AceptaNull="False" Height="30px"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD onclick="Variable=1;" align="center" colSpan="3"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
				<ASP:TEXTBOX id="hdnCta" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnClieId" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		var Variable;
		Variable="";
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtSoliFecha"]!= null)
			document.all["txtSoliFecha"].focus();
			
		function Salida()
		{
			if(window.event.srcElement==null && Variable=="")
			{
				try
				{
					window.opener.document.all['hdnDatosPop'].value='1';
					window.opener.__doPostBack('hdnDatosPop','');
				}
				catch(e){;}
			}
		}
		
		function GrabarUnaVez(pCtrl)
		{  
		if(pCtrl.Grabo==null)
	        pCtrl.Grabo="1";
	    else
			return false;
		   } 
		</SCRIPT>
	</BODY>
</HTML>
