<%--<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CobranzasRecepcionPagosMisCuentas.aspx.vb" Inherits="SRA.CobranzasRecepcionPagosMisCuentas"%>--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/CobranzasRecepcionPagosMisCuentas.aspx.vb" Inherits="CobranzasRecepcionPagosMisCuentas" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CobranzasRecepcionPagosMisCuentas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<body class="pagina" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="Form1" method="post" runat="server">
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 20px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:label></TD>
								<TD vAlign="top" align="right">&nbsp;</TD>
							</TR>
							<tr>
								<td colSpan="3">
									<table>
										<TR>
											<td></td>
											<TD width="30%" align="right"><asp:label id="lblCabeTotalRegistros" runat="server" cssclass="titulo">Cantidad Novedades:</asp:label>&nbsp;</TD>
											<TD width="20%"><asp:label id="lblCantTotalRegistrosDato" runat="server" cssclass="titulo"></asp:label></TD>
											<TD width="30%" align="right"><asp:label id="lblCabeTotalPag" runat="server" cssclass="titulo">Monto Total Novedades:</asp:label>&nbsp;</TD>
											<TD width="20%"><asp:label id="lblCantTotalPago" runat="server" cssclass="titulo"></asp:label></TD>
											<td></td>
										</TR>
										<tr>
											<td colSpan="3" align="right"><asp:button id="btnConfirmar" runat="server" cssclass="boton" Text="Confirmar" Width="80px"></asp:button></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colSpan="3" align="center"></td>
							</tr>
							<TR>
								<TD vAlign="top" colSpan="3" align="center"><asp:datagrid id="grdDato" runat="server" width="95%" PageSize="15" AutoGenerateColumns="False"
										OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" BorderWidth="1px"
										BorderStyle="None">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Id" ReadOnly="True" Visible="False" HeaderText="ID" ItemStyle-HorizontalAlign="Center">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NroReferencia" ReadOnly="True" HeaderText="Nro. Cliente" ItemStyle-HorizontalAlign="Center">
												<HeaderStyle Width="5%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="varDescipcion" ReadOnly="True" HeaderText="Descripci&#243;n" ItemStyle-HorizontalAlign="Left">
												<HeaderStyle Width="25%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="IdFactura" ReadOnly="True" Visible="False" HeaderText="IdFactura"></asp:BoundColumn>
											<asp:BoundColumn DataField="descComprobante" ReadOnly="True" HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
												<HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Importe" ReadOnly="True" HeaderText="Saldo" ItemStyle-HorizontalAlign="Center"
												DataFormatString="{0:c}">
												<HeaderStyle Width="5%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FechaAcreditacion" ReadOnly="True" HeaderText="Fh. Acreditaci&#243;n"
												ItemStyle-HorizontalAlign="Center">
												<HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FechaCobro" ReadOnly="True" Visible="False" HeaderText="Fh. FechaCobro"
												ItemStyle-HorizontalAlign="Center">
												<HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NroControl" ReadOnly="True" HeaderText="Nro. Control" ItemStyle-HorizontalAlign="Center">
												<HeaderStyle Width="5%" HorizontalAlign="Center"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="intNroComprobante" ReadOnly="True" Visible="False" HeaderText="Nro Comp Gemerado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</td>
					<!--- FIN CONTENIDO --->
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
