<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InstitutosParametros" CodeFile="InstitutosParametros.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function mSetearConcepto()
		{
			var strRet1 = '';
			var strRet2 = '';
			var strRet3 = '';
			var strRet4 = '';
			var lstrCta1 = '';
			var lstrCta2 = '';
			var lstrCta3 = '';
			var lstrCta4 = '';
			if (document.all('cmbCertConc').value!='')
				strRet1 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbCertConc').value, "_cuenta");
			if (document.all('cmbCuotConc').value!='')
				strRet2 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbCuotConc').value, "_cuenta");
			if (document.all('cmbExamConc').value!='')
				strRet3 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbExamConc').value, "_cuenta");
			if (document.all('cmbMatrConc').value!='')
				strRet4 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbMatrConc').value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet1!='')
				lstrCta1 = strRet1.substring(0,1);
			if (strRet2!='')
				lstrCta2 = strRet2.substring(0,1);
			if (strRet3!='')
				lstrCta3 = strRet3.substring(0,1);
			if (strRet4!='')
				lstrCta4 = strRet4.substring(0,1);
			document.all('hdnCta').value = lstrCta1;
			if (lstrCta1=='' || lstrCta1=='1' || lstrCta1=='2' || lstrCta1=='3'
				|| lstrCta2=='' || lstrCta2=='1' || lstrCta2=='2' || lstrCta2=='3'
				|| lstrCta1 != lstrCta2 || lstrCta1 != lstrCta3 || lstrCta1 != lstrCta4)
			{
				document.all('cmbCentroCost').disabled = true;
				document.all('txtcmbCentroCost').disabled = true;
				document.all('cmbCentroCost').innerHTML = '';
				document.all('txtcmbCentroCost').value = '';
			}
			else
			{
				document.all('cmbCentroCost').disabled = false;
				document.all('txtcmbCentroCost').disabled = false;
				LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta1, "cmbCentroCost", "N");
			}			
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="javascript:gSetearTituloFrame();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="TableABM" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
							<TR>
								<TD style="HEIGHT: 25px"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="4" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px"></asp:label></TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD colSpan="5">
									<DIV align="left"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="true" Width="100%" BorderStyle="Solid"
											BorderWidth="1px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left">
													<TR>
														<TD colSpan="3"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblMatrConc" runat="server" cssclass="titulo">Concepto de Matrícula:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbMatrConc" runat="server" cssclass="cuadrotexto" Width="300px"
																			Obligatorio="True" filtra="true" NomOper="conceptos" MostrarBotones="False" TextMaxLength="5"
																			onchange="javascript:mSetearConcepto();"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbMatrConc','Conceptos','@conc_grava=0');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblCuotConc" runat="server" cssclass="titulo">Concepto de Cuotas:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbCuotConc" runat="server" cssclass="cuadrotexto" Width="300px"
																			Obligatorio="True" filtra="true" NomOper="conceptos" MostrarBotones="False" TextMaxLength="5"
																			onchange="javascript:mSetearConcepto();"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbCuotConc','Conceptos','@conc_grava=0');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblCertConc" runat="server" cssclass="titulo">Concepto de Certificados:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbCertConc" runat="server" cssclass="cuadrotexto" Width="300px"
																			Obligatorio="True" filtra="true" NomOper="conceptos" MostrarBotones="False" TextMaxLength="5"
																			onchange="javascript:mSetearConcepto();"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbCertConc','Conceptos','@conc_grava=0');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblExamConc" runat="server" cssclass="titulo">Concepto de Examen:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbExamConc" runat="server" cssclass="cuadrotexto" Width="300px"
																			Obligatorio="True" filtra="true" NomOper="conceptos" MostrarBotones="False" TextMaxLength="5"
																			onchange="javascript:mSetearConcepto();"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbExamConc','Conceptos','@conc_grava=0');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblCentroCost" runat="server" cssclass="titulo">Centro de Costo:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbCentroCost" runat="server" cssclass="cuadrotexto" Width="300px"
																			Obligatorio="True" filtra="true" NomOper="centrosc_cuenta_cargar" MostrarBotones="False" TextMaxLength="6"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearConcepto(); var lstrFil='-1'; if (document.all('hdnCta').value != '') lstrFil = document.all('hdnCta').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCentroCost','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblFormaPago" runat="server" cssclass="titulo">Tipo de Pago de Matrícula:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:combobox class="combo" id="cmbFormaPago" runat="server" Width="180px" Obligatorio="True"></cc1:combobox></TD>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblDescDeb" runat="server" cssclass="titulo">Descuento Debito Automático(%):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txtDescDeb" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																Obligatorio="True" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblCuotaDescSocio" runat="server" cssclass="titulo">Descuento Activo Cuotas(%):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txtCuotaDescSocio" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																Width="55px" Obligatorio="True" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lbl1CuotaDescSocio" runat="server" cssclass="titulo">Descuento Adherente Cuotas(%):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txt1CuotaDescSocio" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																Width="55px" Obligatorio="True" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lbl2CuotaDescSocio" runat="server" cssclass="titulo">Descuento Menor Cuotas(%):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txt2CuotaDescSocio" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																Width="55px" Obligatorio="True" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblMatrDescSocio" runat="server" cssclass="titulo">Descuento Activo Matrícula(%):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txtMatrDescSocio" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																Width="55px" Obligatorio="True" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lbl1MatrDescSocio" runat="server" cssclass="titulo">Descuento Adherente Matrícula(%):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txt1MatrDescSocio" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																Width="55px" Obligatorio="True" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lbl2MatrDescSocio" runat="server" cssclass="titulo">Descuento Menor Matrícula(%):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txt2MatrDescSocio" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																Width="55px" Obligatorio="True" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px; HEIGHT: 19px" vAlign="middle" align="right">
															<asp:Label id="lblNotaAprob" runat="server" cssclass="titulo">Nota aprobación:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 19px" vAlign="middle" align="left">
															<cc1:numberbox id="txtNotaAprob" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																Obligatorio="False" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px" vAlign="middle" align="right">
															<asp:Label id="lblPorcAsis" runat="server" cssclass="titulo">Porcentaje asistencia:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<cc1:numberbox id="txtPorcAsis" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																Obligatorio="False" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 206px; HEIGHT: 18px" vAlign="middle" align="right">
															<asp:Label id="Label1" runat="server" cssclass="titulo">Importe de Examen:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 18px" vAlign="middle" align="left">
															<cc1:numberbox id="txtExamImpo" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="104px"
																Obligatorio="False" MaxValor="100" EsDecimal="True" height="18px"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
															height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR height="30">
														<TD align="center" colSpan="2">
															<asp:button id="btnModificar" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:button>&nbsp;&nbsp;</TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnInstId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnPage" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnCta" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.frmABM.hdnPage.value != '') {
		   document.location='#editar';
		   document.frmABM.txtDesc.focus();
		}
		</SCRIPT>
	</BODY>
</HTML>
