Namespace SRA

Partial Class ArancelesRangos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_RangosAranCabe
   Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_RangosAranDeta
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      'Dim lbooPermiAlta As Boolean
      'Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      Dim lintCol As Integer

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "raac_desc")
      txtDescFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "raac_desc")

   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
  
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
         mstrCmd = mstrCmd + "@raac_desc =" + clsSQLServer.gFormatArg(txtDescFil.Text, SqlDbType.VarChar)


         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorRango(ByVal pbooAlta As Boolean)
      btnBajaRango.Enabled = Not (pbooAlta)
      btnModiRango.Enabled = Not (pbooAlta)
      btnAltaRango.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         txtDesc.Valor = mdsDatos.Tables(0).Rows(0).Item("raac_desc")
         mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If

   End Sub
   Public Sub mEditarDatosRangos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldr As DataRow

         hdnRangoId.Text = E.Item.Cells(1).Text
         ldr = mdsDatos.Tables(mstrTablaDeta).Select("raad_id=" & hdnRangoId.Text)(0)

         With ldr
            txtMesDesde.Valor = .Item("raad_desde")
            txtMesHasta.Valor = .Item("raad_hasta")
            txtFechaTope.Fecha = .Item("raad_tope_fecha")
            If Not .IsNull("raad_sobre_fecha") Then
                txtFechaSobre.Fecha = .Item("raad_sobre_fecha")
            Else
                txtFechaSobre.Text = ""
            End If
         End With
         mSetearEditorRango(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaDeta

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      grdRangos.DataSource = mdsDatos.Tables(mstrTablaDeta)
      grdRangos.DataBind()

      Session(mstrTabla) = mdsDatos
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      txtDesc.Text = ""
      mLimpiarRangos()

      grdRangos.CurrentPageIndex = 0

      mCrearDataSet("")
      mShowTabs(1)
      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarRangos()
      hdnRangoId.Text = ""
      txtMesDesde.Valor = ""
      txtMesHasta.Valor = ""
      txtFechaSobre.Text = ""
      txtFechaTope.Text = ""
      txtMesHasta.Valor = ""
      mSetearEditorRango(True)
   End Sub
   Private Sub mLimpiarFil()
      txtDescFil.Text = ""

      mConsultar()
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      panBotones.Visible = pbooVisi
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
     
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If txtMesDesde.Valor > txtMesHasta.Valor Then
         Throw New AccesoBD.clsErrNeg("Mes desde no puede ser mayor a mes hasta.")
      End If
      If txtFechaSobre.Text <> "" Then
         If Me.txtFechaTope.Fecha > txtFechaSobre.Fecha Then
            Throw New AccesoBD.clsErrNeg("La fecha tope no puede ser mayor a la de sobretasa.")
         End If
      End If


   End Sub
   Private Sub mGuardarDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("raac_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("raac_desc") = txtDesc.Valor

      End With
    End Sub
   Private Sub mActualizarRangos()
      Try
         mValidaDatos()
         mGuardarDatosRangos()
         mLimpiarRangos()
         grdRangos.DataSource = mdsDatos.Tables(mstrTablaDeta)
         grdRangos.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaRango()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDeta).Select("raad_id=" & hdnRangoId.Text)(0)
         row.Delete()
         grdRangos.DataSource = mdsDatos.Tables(mstrTablaDeta)
         grdRangos.DataBind()
         mLimpiarRangos()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosRangos()
      Dim ldr As DataRow

      If hdnRangoId.Text = "" Then
         ldr = mdsDatos.Tables(mstrTablaDeta).NewRow
         ldr.Item("raad_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "raad_id")
      Else
         ldr = mdsDatos.Tables(mstrTablaDeta).Select("raad_id=" & hdnRangoId.Text)(0)
      End If

      With ldr
         .Item("raad_raac_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("raad_desde") = txtMesDesde.Valor
         .Item("raad_hasta") = txtMesHasta.Valor
         .Item("raad_tope_fecha") = txtFechaTope.Fecha
         If txtFechaSobre.Text <> "" Then
            .Item("raad_sobre_fecha") = txtFechaSobre.Fecha
         Else
            .Item("raad_sobre_fecha") = DBNull.Value
         End If
      End With
      If (hdnRangoId.Text = "") Then
         mdsDatos.Tables(mstrTablaDeta).Rows.Add(ldr)
      End If
   End Sub

#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panGeneral.Visible = False
         lnkGeneral.Font.Bold = False
         panRangos.Visible = False
         lnkRangos.Font.Bold = False
         Select Case Tab
            Case 1
               panGeneral.Visible = True
               lnkGeneral.Font.Bold = True
            Case 2
               panRangos.Visible = True
               lnkRangos.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub lnkRangos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRangos.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
      mShowTabs(1)
   End Sub
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub btnClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
      mCerrar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   
   Private Overloads Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub btnAltaActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaRango.Click
      mActualizarRangos()
   End Sub
   Private Sub btnBajaActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaRango.Click
      mBajaRango()
   End Sub
   Private Sub btnModiActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiRango.Click
      mActualizarrangos()
   End Sub
   Private Sub btnLimpActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpRango.Click
      mLimpiarRangos()
   End Sub
#End Region

   
End Class
End Namespace
