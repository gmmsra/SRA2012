<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SeleRaza" CodeFile="SeleRaza.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Seleccionar Raza</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mCambioEspecie(pEspe)
		{
			mCargarRazas(pEspe);
		}		
		function mCargarRazas(pEspe)
		{
		    var sFiltro = pEspe.value;
		    if (sFiltro!='')
				LoadComboXML("razas_cargar", sFiltro, "cmbRaza", "S");
			else
				document.all('cmbRaza').selectedIndex = 0;
		}		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();" onload="gSetearTituloFrame('');" >
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Seleccionar Raza</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="99%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3"></TD>
												</TR>
												<TR>
													<TD vAlign="middle" align="center" colSpan="3" height="30">
														<asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderStyle="Solid" BorderWidth="1px"
															Height="43px">
															<P align="right">
																<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
																	border="0">
																	<TR>
																		<TD style="WIDTH: 97%" colSpan="3">
																			<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																				<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblEspe" runat="server" cssclass="titulo">Especie:&nbsp;</asp:Label></TD>
																						<TD>
																							<cc1:combobox class="combo" id="cmbEspe" runat="server" Width="234px" AceptaNull="True" onchange="javascript:mCambioEspecie(this);"
																								Obligatorio="True"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																						<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																							<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="300px" Height="20px"
																								NomOper="razas_cargar" MostrarBotones="False" filtra="true" AceptaNull="True" Obligatorio="True"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</P>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD vAlign="middle" align="center" colSpan="3" height="30">
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Grabar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		gSetearTituloFrame('Denuncias de Servicios');
		</SCRIPT>
	</BODY>
</HTML>
