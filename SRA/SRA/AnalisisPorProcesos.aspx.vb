Imports System.Data.SqlClient


Namespace SRA


Partial Class AnalisisPorProcesos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents lblDesc As System.Web.UI.WebControls.Label
    Protected WithEvents lblCodigo As System.Web.UI.WebControls.Label
    Protected WithEvents txtDesc As NixorControls.TextBoxTab
    Protected WithEvents txtCodigo As NixorControls.NumberBox
    Protected WithEvents lblFormula As System.Web.UI.WebControls.Label
    Protected WithEvents cmbFormula As NixorControls.ComboBox
    Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Protected WithEvents cmbMensaje As NixorControls.ComboBox
    Protected WithEvents chkManu As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblAgru As System.Web.UI.WebControls.Label
    Protected WithEvents lblOrde As System.Web.UI.WebControls.Label
    Protected WithEvents txtOrde As NixorControls.NumberBox
    Protected WithEvents chkObli As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblSexo As System.Web.UI.WebControls.Label
    Protected WithEvents cmbSexo As NixorControls.ComboBox
    Protected WithEvents lblCamp As System.Web.UI.WebControls.Label
    Protected WithEvents lblMini As System.Web.UI.WebControls.Label
    Protected WithEvents lblMaxi As System.Web.UI.WebControls.Label
    Protected WithEvents txtMini As NixorControls.TextBoxTab
    Protected WithEvents txtMaxi As NixorControls.TextBoxTab
    Protected WithEvents cmbCamp As NixorControls.ComboBox
    Protected WithEvents cmbMini As NixorControls.ComboBox
    Protected WithEvents cmbMaxi As NixorControls.ComboBox
    Protected WithEvents cmbAgru As NixorControls.ComboBox
    Protected WithEvents btnMens As System.Web.UI.HtmlControls.HtmlButton
    Protected WithEvents btnCond As System.Web.UI.HtmlControls.HtmlButton
    Protected WithEvents lblVige As System.Web.UI.WebControls.Label
    Protected WithEvents txtVigeDesde As NixorControls.DateBox
    Protected WithEvents txtVigeHasta As NixorControls.DateBox
    Protected WithEvents lblVigeDesde As System.Web.UI.WebControls.Label
    Protected WithEvents lblVigeHasta As System.Web.UI.WebControls.Label
    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "rg_resul_procesos"
    Private mstrTablaProc As String = "rg_resul_proce_razas"
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()

        clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspeFil, "T", "@SeAnalizan=1", True)
        clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecie, "S", "@SeAnalizan=1", True)
        clsWeb.gCargarRefeCmb(mstrConn, "rg_procesos", cmbProceFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_procesos", cmbProceso, "S")
        If Not Session("sEspeId") Is Nothing Then
            cmbEspecie.Valor = Session("sEspeId").ToString
        End If
        mCargarResultados()
        If Not Session("sResulId") Is Nothing Then
            cmbResultados.Valor = Session("sResulId").ToString
        End If

        
    End Sub
    Private Sub mCargarResultados()
        clsWeb.gCargarRefeCmb(mstrConn, "rg_analisis_resul", cmbResultados, "S")

    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        'btnMens.Attributes.Add("onclick", "mMensajes();return false;")
        'btnCond.Attributes.Add("onclick", "mCondicion();return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrDenuLong As Object
        Dim lstrDetaLong As Object
        'lstrDenuLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        'txtFolio.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero

        'lstrDetaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)
        'txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "sdde_obser")
    End Sub
#End Region

#Region "Inicializacion de Variables"
    'Public Sub mInicializar()
    '   clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
    '   grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
    'End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDeta.EditItemIndex = -1
            If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
                grdDeta.CurrentPageIndex = 0
            Else
                grdDeta.CurrentPageIndex = E.NewPageIndex
            End If
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
            grdDeta.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
           
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd = mstrCmd + " @espe_id=" + IIf(cmbEspeFil.Valor.ToString = "0", "null", cmbEspeFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@rpro_id=" + IIf(cmbProceFil.Valor.ToString = "0", "null", cmbProceFil.Valor.ToString)
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
        btnBajaDeta.Enabled = Not (pbooAlta)
        btnModiDeta.Enabled = Not (pbooAlta)
        btnAltaDeta.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                cmbEspecie.Valor = .Item("_espe_id")
                cmbResultados.Valor = .Item("_ares_id")
                If .Item("arrp_tipo") Is System.DBNull.Value Then
                    cmbTipo.Valor = "null"
                Else
                    cmbTipo.Valor = IIf(Convert.ToBoolean(.Item("arrp_tipo")), "1", "0")
                End If

                txtDesdeFecha.Text = .Item("arrp_desde_fecha")
                txtHastaFecha.Text = .Item("arrp_hasta_fecha")
                cmbProceso.Valor = .Item("_rpro_id")
                clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazas, "S", "@raza_espe_id =" & cmbEspecie.SelectedValue)

                If Not .IsNull("arrp_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("arrp_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
            End With

            mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        'If grdDeta.Items.Count = 0 Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un Proceso")
        'End If
    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("arrp_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("arrp_ares_id") = cmbResultados.Valor
            .Item("arrp_rpro_id") = cmbProceso.Valor
            Select Case cmbTipo.Valor
                Case "0"
                    .Item("arrp_tipo") = False
                Case "1"
                    .Item("arrp_tipo") = True
                Case Else
                    .Item("arrp_tipo") = DBNull.Value
            End Select
            .Item("arrp_desde_fecha") = txtDesdeFecha.Text
            .Item("arrp_hasta_fecha") = txtHastaFecha.Text
            .Item("arrp_audi_user") = Session("sUserId").ToString()
            .Item("arrp_baja_fecha") = DBNull.Value

           
        End With
    End Sub
    Public Sub mEditarDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDire As DataRow

            hdnDetaId.Text = E.Item.Cells(1).Text
            ldrDire = mdsDatos.Tables(mstrTablaProc).Select("arrd_id=" & hdnDetaId.Text)(0)

            With ldrDire
                cmbRazas.Valor = .Item("_raza_id")

                'If Not .IsNull("repr_baja_fecha") Then
                '   lblBajaDeta.Text = "Direcci�n dada de baja en fecha: " & CDate(.Item("repr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                'Else
                '   lblBajaDeta.Text = ""
                'End If
            End With
            mSetearEditorDeta(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mActualizarDeta()
        Try
            mGuardarDatosDeta()

            mLimpiarDeta()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
            grdDeta.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaDeta()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaProc).Select("arrd_id=" & hdnDetaId.Text)(0)
            row.Delete()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
            grdDeta.DataBind()
            mLimpiarDeta()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosDeta()
        Dim ldrProc As DataRow
        Dim vbooDefault As Boolean

        If cmbRazas.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una Raza.")
        End If

        If hdnDetaId.Text = "" Then
            ldrProc = mdsDatos.Tables(mstrTablaProc).NewRow
            ldrProc.Item("arrd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaProc), "arrd_id")
        Else
            ldrProc = mdsDatos.Tables(mstrTablaProc).Select("arrd_id=" & hdnDetaId.Text)(0)
        End If

        With ldrProc
            .Item("arrd_id") = clsSQLServer.gFormatArg(hdnDetaId.Text, SqlDbType.Int)
            .Item("arrd_arrp_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("arrd_raza_id") = cmbRazas.Valor
            .Item("_raza_desc") = cmbRazas.SelectedItem.ToString()
            .Item("arrd_audi_user") = Session("sUserId").ToString()
        End With
        If (hdnDetaId.Text = "") Then
            mdsDatos.Tables(mstrTablaProc).Rows.Add(ldrProc)
        End If
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panCabecera.Visible = False
            lnkCabecera.Font.Bold = False
            panDeta.Visible = False
            lnkDeta.Font.Bold = False
            Select Case Tab
                Case 1
                    panCabecera.Visible = True
                    lnkCabecera.Font.Bold = True
                Case 2
                    panDeta.Visible = True
                    lnkDeta.Font.Bold = True

                    clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazas, "S", "@raza_espe_id =" & IIf(cmbEspecie.SelectedValue = "", "null", cmbEspecie.SelectedValue))

            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        mConsultar()
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiarFiltros()

        cmbEspeFil.Limpiar()
        cmbProceFil.Limpiar()
        mMostrarPanel(False)
    End Sub
    Private Sub mLimpiar()

        hdnId.Text = ""
        cmbEspecie.Limpiar()
        cmbResultados.Limpiar()
        cmbTipo.Limpiar()
        cmbProceso.Limpiar()

        txtDesdeFecha.Text = ""
        txtHastaFecha.Text = ""


        mLimpiarDeta()

        grdDato.CurrentPageIndex = 0
        grdDeta.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarDeta()
        hdnDetaId.Text = ""
        cmbRazas.Limpiar()
        mSetearEditorDeta(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaProc

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
        grdDeta.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    'Botones generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones de Procesos
    Private Sub btnAltaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
        mActualizarDeta()
    End Sub
    Private Sub btnBajaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        mBajaDeta()
    End Sub
    Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        mActualizarDeta()
    End Sub
    Private Sub btnLimpDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        mLimpiarDeta()
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDirecciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
        mShowTabs(2)
    End Sub
#End Region

    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "AnalisisPorProcesos"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)


            lstrRpt += "&espe_id=" + IIf(cmbEspeFil.Valor.ToString = "", "0", cmbEspeFil.Valor.ToString)
            lstrRpt += "&rpro_id=" + IIf(cmbProceFil.Valor.ToString = "", "0", cmbProceFil.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
