<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ProformasAnul" CodeFile="ProformasAnul.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Anulaci�n de Proformas</title>
		<meta content="False" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function mCargarCriadores()
		 {
		   var sFiltro = document.all("cmbActi").value;
	       var sFiltroClie = document.all("usrClieFil:txtId").value;
		   if (sFiltro != '')
            {
              LoadComboXML("precios_aran_cargar", sFiltro, "cmbArancel", "N"); 
			  if(document.all("cmbActi").value == "3"  & sFiltroClie != '')
  				LoadComboXML("criadores_razas_cargar", document.all("usrClieFil:txtId").value, "cmbCriador", "N"); 
			   else
				document.all("cmbCriador").innerText = ""
            }
           else
			{
				document.all("cmbCriador").innerText='';
				document.all("cmbArancel").innerText='';
			}	
		}
		
		function usrClieFil_onchange()
		{	
			mCargarCriadores();
		}
		
		</SCRIPT>
</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 10px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Anulaci�n de Proformas</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
										BorderStyle="Solid">
            <TABLE id=TableFil style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
            align=left border=0>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24></TD>
                      <TD style="HEIGHT: 8px" width=42></TD>
                      <TD style="HEIGHT: 8px" width=26></TD>
                      <TD style="HEIGHT: 8px"></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnBusc runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
                      <TD style="HEIGHT: 8px" vAlign=middle width=26>
<CC1:BotonImagen id=btnLimpiarFil runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24><IMG height=25 
                        src="imagenes/formfle.jpg" width=24 border=0></TD>
                      <TD style="HEIGHT: 8px" width=42><IMG height=25 
                        src="imagenes/formtxfiltro.jpg" width=113 border=0></TD>
                      <TD style="HEIGHT: 8px" width=26><IMG height=25 
                        src="imagenes/formcap.jpg" width=26 border=0></TD>
                      <TD style="HEIGHT: 8px" background=imagenes/formfdocap.jpg 
                      colSpan=3><IMG height=25 src="imagenes/formfdocap.jpg" 
                        border=0></TD></TR></TABLE></TD></TR>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD width=3 background=imagenes/formiz.jpg><IMG 
                        height=10 src="imagenes/formiz.jpg" width=3 border=0></TD>
                      <TD><!-- FOMULARIO -->
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" 
border=0>
                          <TR>
                            <TD style="HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg 
                          colSpan=2></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblActi runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbActi runat="server" Width="250px" onchange="mCargarCriadores()" AceptaNull="False"></cc1:combobox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblPeriEmision runat="server" cssclass="titulo">Per�odo de Emisi�n Desde:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:DateBox id=txtFDesdeEmi runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp; 
<asp:Label id=lblFHastaEmi runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp; 
<cc1:DateBox id=txtFHastaEmi runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblPerFechValor runat="server" cssclass="titulo">Per�odo de Fecha Valor Desde:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:DateBox id=txtFDesdeValor runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp; 
<asp:Label id=lblFHastaValor runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp; 
<cc1:DateBox id=txtFHastaValor runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblCliente runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<UC1:CLIE id=usrClieFil runat="server" width="100%" AceptaNull="False" FilCUIT="True" Tabla="Clientes" Saltos="1,1,1" FilSociNume="True" MuestraDesc="False" FilDocuNume="True" FilLegaNume="True"></UC1:CLIE></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblCriador runat="server" cssclass="titulo">Raza/Criador:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbCriador runat="server" Width="250px" AceptaNull="false"></cc1:combobox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblArancel runat="server" cssclass="titulo">Arancel:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
								<table border=0 cellpadding=0 cellspacing=0>
								<tr>
								<td>															
								<cc1:combobox id=cmbArancel runat="server" cssclass="cuadrotexto" Width="300px" AceptaNull="False" NomOper="aranceles_cargar" MostrarBotones="False" filtra="true"></cc1:combobox>
								</td>
								<td>
									<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																	onclick="var lstrFiltro='-1'; if (document.all('cmbActi').value != '') lstrFiltro=document.all('cmbActi').value; mBotonBusquedaAvanzada('precios_aran','aran_desc','cmbArancel','Aranceles',lstrFiltro);"
																	alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
								</td>
								</tr>
								</table>
							</TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblNro runat="server" cssclass="titulo">Nro.Proforma:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:numberbox id=txtNro runat="server" cssclass="cuadrotexto" Width="100px" AceptaNull="false"></cc1:numberbox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblTram runat="server" cssclass="titulo">Nro.control facturaci�n:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<cc1:numberbox id=txtTram runat="server" cssclass="cuadrotexto" Width="100px" AceptaNull="false"></cc1:numberbox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 75%; HEIGHT: 17px" 
                            background=imagenes/formfdofields.jpg>
<asp:CheckBox id=chkBaja Visible="False" Text="Incluir dadas de baja" Runat="server" CssClass="titulo"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD style="HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg 
                          colSpan=2></TD></TR></TABLE></TD>
                      <TD width=2 background=imagenes/formde.jpg><IMG height=2 
                        src="imagenes/formde.jpg" width=2 
                  border=0></TD></TR></TABLE><A id=editar 
                  name=editar></A></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
											BorderStyle="Solid">
            <TABLE width="100%">
              <TR>
                <TD>
                  <P align=right>
                  <TABLE class=FdoFld id=Table2 style="WIDTH: 100%" 
                  cellPadding=0 align=left border=0>
                    <TR>
                      <TD>
                        <P></P></TD>
                      <TD height=5>
<asp:Label id=lblTitu runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
                      <TD vAlign=top align=right>&nbsp; 
<asp:ImageButton id=imgClose runat="server" ImageUrl="images\Close.bmp" CausesValidation="False" ToolTip="Cerrar"></asp:ImageButton></TD></TR>
                    <TR>
                      <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panCabecera runat="server" cssclass="titulo" Width="100%">
                        <TABLE id=TableCabecera style="WIDTH: 100%" 
                        cellPadding=0 align=left border=0>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=3 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD vAlign=top align=center colSpan=2>
<asp:datagrid id=grdProfo runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="true" HorizontalAlign="Center" OnEditCommand="mVerMasDatos" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False">
																							<FooterStyle CssClass="footer"></FooterStyle>
																							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																							<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																							<Columns>
																								<asp:TemplateColumn HeaderText="Incl.">
																									<HeaderStyle Width="3%"></HeaderStyle>
																									<ItemTemplate>
																										<asp:CheckBox ID="chkSel" Runat="server"></asp:CheckBox>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:BoundColumn Visible="False" DataField="prfr_id"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_nume" HeaderText="N&#250;mero"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_ingr_fecha" HeaderText="Fecha Ing." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_fecha" HeaderText="Fecha Valor" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_clie_apel" HeaderText="Cliente">
																									<HeaderStyle Width="20%"></HeaderStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="acti_desc" HeaderText="Actividad"></asp:BoundColumn>
																								<asp:BoundColumn DataField="raza_desc" HeaderText="Raza"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_neto" HeaderText="Importe" HeaderStyle-HorizontalAlign="Right">
																									<ItemStyle HorizontalAlign="Right"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="anular" HeaderText="Anular"></asp:BoundColumn>
																								<asp:TemplateColumn HeaderStyle-Width="9px">
																									<ItemTemplate>
																										<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																											<img src='images/icodate.gif' border="0" alt="Ver detalle" style="cursor:hand;" />
																										</asp:LinkButton>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																							</Columns>
																							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD></TR>
                          <TR height=30>
                            <TD align=center>
<asp:button id=btnAnulReactiv runat="server" cssclass="boton" Width="157px" Text="Anular"></asp:button></TD></TR></TABLE></asp:panel></TD></TR></TABLE></P></TD></TR></TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnUltSociNume" runat="server"></asp:textbox><asp:textbox id="hdnTotalSolis" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
