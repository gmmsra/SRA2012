Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports Entities


Namespace SRA


Partial Class DenuTE
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents usrDonaFil As usrClieDeriv 
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents txtImplFechaRecu As NixorControls.DateBox
    '  Protected WithEvents txtCruza As NixorControls.NumberBox

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_TeDenun
   Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_TeDenunDeta
   Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
   Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrReceptorasTipos As String = "rg_receptoras_tipos"

   Private mstrParaPageSize As Integer
   Private dsVali As DataSet
   Private mstrCmd As String
   Private mdsDatos As DataSet
    Private mstrConn As String
    Public strConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                Session(mstrTabla) = Nothing
            mCargarCombos()
            mSetearMaxLength()
            mSetearEventos()
            mMostrarPanel(False)
         Else
            If Not Request.Form("cmbCentro") Is Nothing Then
                mCargarVeterinarios(Request.Form("cmbCentro").ToString)
                If Not Request.Form("cmbVete") Is Nothing Then
                    cmbVete.Valor = Request.Form("cmbVete").ToString
                End If
            End If
            If panDato.Visible Then
                If usrCria.cmbCriaRazaExt.Valor.ToString <> "0" Then
                    usrDona.cmbProdRazaExt.Enabled = False
                    usrPadre1.cmbProdRazaExt.Enabled = False
                    usrPadre2.cmbProdRazaExt.Enabled = False
                Else
                    usrDona.cmbProdRazaExt.Enabled = True
                    usrPadre1.cmbProdRazaExt.Enabled = True
                    usrPadre2.cmbProdRazaExt.Enabled = True
                End If
                    mdsDatos = Session(mstrTabla)
               usrDona.Raza = usrCria.cmbCriaRazaExt.Valor.ToString
           End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbReceRazaFil, "id", "descrip_codi", "S")
      SRA_Neg.Utiles.gSetearRaza(cmbRaza)
      clsWeb.gCargarRefeCmb(mstrConn, "implante_ctros", cmbCentro, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "implante_ctros", cmbCentroFil, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_receptoras_tipos", cmbReti, "S")
        clsWeb.gCargarCombo(mstrConn, "autorizados_implantes_cargar " & cmbCentro.Valor.ToString, cmbVete, "id", "descrip", "S")

        clsWeb.gCargarCombo(mstrConn, "autorizados_implantes_cargar " & cmbCentroFil.Valor.ToString, cmbVete, "id", "descrip", "S")
   End Sub

   Private Sub mCargarVeterinarios(ByVal pstrCentro As String)
      If pstrCentro <> "" Then
        clsWeb.gCargarCombo(mstrConn, "autorizados_implantes_cargar " & pstrCentro, cmbVete, "id", "descrip", "S")
      Else
        cmbVete.Items.Clear()
        End If
       
    End Sub
    Private Sub mCargarVeterinariosFil(ByVal pstrCentro As String)
        If pstrCentro <> "" Then
            clsWeb.gCargarCombo(mstrConn, "autorizados_implantes_cargar " & pstrCentro, cmbCentroFil, "id", "descrip", "S")
        Else
            cmbCentroFil.Items.Clear()
        End If

    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnBajaDeta.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
    End Sub

    Private Sub mSetearMaxLength()

        Dim lstrDenuLong As Object
        Dim lstrDetaLong As Object

        lstrDenuLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtDiasGest.MaxLength = clsSQLServer.gObtenerLongitud(lstrDenuLong, "tede_gest_dias")
        txtFresEmbr.MaxLength = clsSQLServer.gObtenerLongitud(lstrDenuLong, "tede_embr_fres")
        txtCongEmbr.MaxLength = clsSQLServer.gObtenerLongitud(lstrDenuLong, "tede_embr_cong")
        txtImplFres.MaxLength = clsSQLServer.gObtenerLongitud(lstrDenuLong, "tede_impl_embr_fres")
        txtImplCong.MaxLength = clsSQLServer.gObtenerLongitud(lstrDenuLong, "tede_impl_embr_cong")

        txtNroFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrDenuLong, "tede_nume")

        lstrDetaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)
        txtCara.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "tedd_carv")
        txtTatu.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "tedd_tatu")
        '   txtCruza.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "tedd_raza_cruza")
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "tedd_obse")

    End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      usrCria.Tabla = mstrCriadores
      usrCria.Criador = True
      usrCria.AutoPostback = False
      usrCria.FilClaveUnica = False
      usrCria.ColClaveUnica = True
      usrCria.Ancho = 790
      usrCria.Alto = 510
      usrCria.ColDocuNume = False
      usrCria.ColCUIT = True
      usrCria.ColCriaNume = True
      usrCria.FilCUIT = True
      usrCria.FilDocuNume = True
      usrCria.FilAgru = False
      usrCria.FilTarjNume = False

      usrCriaFil.Tabla = mstrCriadores
      usrCriaFil.Criador = True
      usrCriaFil.AutoPostback = False
      usrCriaFil.FilClaveUnica = True
      usrCriaFil.ColClaveUnica = False
      usrCriaFil.Ancho = 790
      usrCriaFil.Alto = 510
      usrCriaFil.ColDocuNume = False
      usrCriaFil.ColCriaNume = True
      usrCriaFil.ColCUIT = True
      usrCriaFil.FilCUIT = True
      usrCriaFil.FilDocuNume = True
      usrCriaFil.FilAgru = False
      usrCriaFil.FilTarjNume = False

      usrDona.Sexo = "0"
      usrDona.FilSexo = False
      usrPadre1.Sexo = "1"
      usrPadre1.FilSexo = False
      usrPadre2.Sexo = "1"
      usrPadre2.FilSexo = False

      usrMadreFil.Tabla = SRA_Neg.Constantes.gTab_Productos
      usrMadreFil.AutoPostback = False
      usrMadreFil.Ancho = 790
      usrMadreFil.Alto = 510
      usrMadreFil.FilSexo = False
      usrMadreFil.Sexo = 0

      usrPadreFil.Tabla = SRA_Neg.Constantes.gTab_Productos
      usrPadreFil.AutoPostback = False
      usrPadreFil.Ancho = 790
      usrPadreFil.Alto = 510
      usrPadreFil.FilSexo = False
      usrPadreFil.Sexo = 1

   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub
    Public Function MuestraCentroImplante(ByVal objText1 As Object) As String

        Dim dtCentroImplante As DataTable
        Dim sReturn As String = ""
        Dim oCentroImplante As New SRA_Neg.CentroImplante(mstrConn, 1)

        dtCentroImplante = oCentroImplante.GetCentroImplanteById(objText1.ToString())
        If dtCentroImplante.Rows.Count > 0 Then
            sReturn = dtCentroImplante.Rows(0).Item("imcr_codi").ToString()
        End If

        Return sReturn
    End Function

    Public Sub mConsultar()
        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet


            lstrCmd = "exec " & mstrTabla & "_busq"
            lstrCmd = lstrCmd & " @cria_id=" & clsSQLServer.gFormatArg(usrCriaFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd + ",@clie_id=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            lstrCmd = lstrCmd & ",@fecha_desde=" & clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@fecha_hasta=" & clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@nro=" & clsSQLServer.gFormatArg(txtNroFil.Text, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@recu_fecha_desde=" & clsSQLServer.gFormatArg(txtRecuFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@recu_fecha_hasta=" & clsSQLServer.gFormatArg(txtRecuFechaHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@imple_fecha_desde=" & clsSQLServer.gFormatArg(txtImplFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@imple_fecha_hasta=" & clsSQLServer.gFormatArg(txtImplFechaHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@madre_prdt_id=" & clsSQLServer.gFormatArg(usrMadreFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@padre_prdt_id=" & clsSQLServer.gFormatArg(usrPadreFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@rece_raza_id=" & clsSQLServer.gFormatArg(cmbReceRazaFil.Valor.ToString, SqlDbType.Int)

            If txtReceNumeFil.Text <> "" Then
                lstrCmd = lstrCmd & ",@rece_nume=" & clsSQLServer.gFormatArg(txtReceNumeFil.Valor.ToString, SqlDbType.VarChar)
            End If


            'If usrMadreFil.txtSraNumeExt.Text <> "" Then
            '    lstrCmd = lstrCmd & ",@SRA_nume=" & clsSQLServer.gFormatArg(usrMadreFil.txtSraNumeExt.Text, SqlDbType.VarChar)
            'End If


            If cmbCentroFil.Valor.ToString() <> "" Then
                'tede_imcr_id centro de implante
                lstrCmd = lstrCmd & ",@tede_imcr_id=" & clsSQLServer.gFormatArg(cmbCentroFil.Valor.ToString, SqlDbType.VarChar)
            End If


            lstrCmd = lstrCmd & " ,@mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")


            dsDatos = clsWeb.gCargarDataSet(mstrConn, lstrCmd)
            grdDato.DataSource = dsDatos
            grdDato.DataBind()

            grdDato.Visible = True


            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrTablaDeta
            btnBajaDeta.Enabled = Not (pbooAlta)
            btnModiDeta.Enabled = Not (pbooAlta)
            btnAltaDeta.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
            lblNro.Visible = Not (pbooAlta)
            lblFecha.Visible = Not (pbooAlta)
      End Select
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
			mCargarDatos(E.Item.Cells(2).Text)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(pstrId)

      mCrearDataSet(hdnId.Text)

        grdDeta.CurrentPageIndex = 0


      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            hdnId.Text = .Item("tede_id")
            usrCria.Valor = .Item("tede_cria_id")
                    If .Item("_raza").ToString.Length > 0 Then
                        usrCria.cmbCriaRazaExt.Valor = .Item("_raza")
                    End If
                    If .Item("tede_recu_fecha").ToString.Length > 0 Then
                        txtFechaRecu.Fecha = .Item("tede_recu_fecha")
                    End If
                    If .Item("tede_gest_dias").ToString.Length > 0 Then
                        txtDiasGest.Valor = .Item("tede_gest_dias")
                    End If
                    txtCongEmbr.Valor = .Item("tede_embr_cong").ToString
                    txtFresEmbr.Valor = .Item("tede_embr_fres").ToString
                    txtImplCong.Valor = .Item("tede_impl_embr_cong").ToString
                    txtImplFres.Valor = .Item("tede_impl_embr_fres").ToString
                    txtServFecha.Fecha = .Item("tede_serv_fecha").ToString
                    usrPadre1.Valor = .Item("tede_pad1_prdt_id").ToString
                    usrPadre2.Valor = .Item("tede_pad2_prdt_id").ToString
                    usrDona.Valor = .Item("tede_madr_prdt_id").ToString
                    usrClie.Valor = .Item("tede_clie_id").ToString
                    cmbCentro.Valor = .Item("tede_imcr_id").ToString
                    mCargarVeterinarios(cmbCentro.Valor.ToString)
                    cmbVete.Valor = .Item("tede_auim_id").ToString
                    txtNro.Text = .Item("tede_nume").ToString
                    txtFecha.Text = .Item("_fecha").ToString
                    txtEsta.Text = .Item("_estado").ToString

                    If Not .IsNull("tede_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("tede_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If

                    lblTitu.Text = "Denuncia: Raza: " & .Item("_raza") & " - Cliente: " & .Item("tede_clie_id") & "-" & .Item("_cliente") & " - Criador: " & .Item("_criador")


                End With

         hdnDetaCarga.Text = "S"

         btnErr.Visible = (txtEsta.Text.ToUpper = "RECHAZADO")

         mSetearEditor("", False)
         mSetearEditor(mstrTablaDeta, True)
         mMostrarPanel(True)
         mShowTabs(1)

      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()

      hdnValorId.Text = "-1"  'para que ignore el id que vino del control
      txtFechaDesdeFil.Text = ""
      txtFechaHastaFil.Text = ""
      txtRecuFechaDesdeFil.Text = ""
      txtRecuFechaHastaFil.Text = ""
      txtImplFechaDesdeFil.Text = ""
      txtImplFechaHastaFil.Text = ""
      txtNroFil.Text = ""
      txtReceNumeFil.Text = ""
      usrCriaFil.Limpiar()
      usrClieFil.Limpiar()
      usrMadreFil.Limpiar()
      usrPadreFil.Limpiar()
        cmbReceRazaFil.Limpiar()
        cmbCentroFil.Limpiar()
        cmbVete.Limpiar()
      grdDato.Visible = False

   End Sub

   Private Sub mLimpiar()

        hdnId.Text = ""
        txtImplFres.Valor = ""
        txtImplCong.Valor = ""
        txtFechaRecu.Text = ""
        txtDiasGest.Valor = ""
        txtCongEmbr.Valor = ""
        txtFresEmbr.Valor = ""
        txtServFecha.Text = ""
        txtTotal.Text = ""
        txtNro.Text = ""
        txtFecha.Text = ""
        cmbCentro.Valor = ""
        cmbVete.Valor = ""
        usrClie.Limpiar()
        usrCria.Limpiar()
        usrDona.Limpiar()
        usrPadre1.Limpiar()
        usrPadre2.Limpiar()

      mLimpiarDetalle()

      btnErr.Visible = False

      mCrearDataSet("")

      lblTitu.Text = ""

      grdDeta.CurrentPageIndex = 0

      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = Not panDato.Visible

      lnkCabecera.Font.Bold = True
      lnkDeta.Font.Bold = False

      panDato.Visible = pbooVisi
      panFiltros.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True
      panDeta.Visible = False

      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkDeta.Font.Bold = False

   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)

    Try
        Dim lstrTitu As String

        panDato.Visible = True
        panBotones.Visible = True
        btnAgre.Visible = False
        panDeta.Visible = False
        panCabecera.Visible = False

        lnkCabecera.Font.Bold = False
        lnkDeta.Font.Bold = False

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Visible = False
            Case 2
                panDeta.Visible = True
                lnkDeta.Font.Bold = True
                lblTitu.Text = "Vientres Receptores"
                lblTitu.Visible = True
                grdDeta.Visible = True
        End Select

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrDenuId As String
         
            mGuardarDatos()

            Dim lTransac3 As SqlClient.SqlTransaction


            lTransac3 = clsSQLServer.gObtenerTransac(mstrConn)

         Dim lobjDenu As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            Dim lstrReg As String
            For Each odrReg As DataRow In mdsDatos.Tables("RG_RECEPTORAS_TIPOS").Select()
                lstrReg = clsSQLServer.gAlta(lTransac3, Session("sUserId").ToString(), "RG_RECEPTORAS_TIPOS", odrReg)
            Next


            clsSQLServer.gCommitTransac(lTransac3)


            lstrDenuId = lobjDenu.Alta()

            mAplicarReglas(lstrDenuId, 0)

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    'Private Sub mAplicarReglas(ByVal pstrDenuId As String)

    '   Try

    '      Dim lintImplCanti As Integer = 0

    '      ''dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString())
    '      lintImplCanti = (mdsDatos.Tables(mstrTablaDeta).Select("tedd_baja_fecha is null").GetUpperBound(0) + 1)
    '      If lintImplCanti <> 0 Then
    '          'implante
    '          dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), "W", txtImplFecha.Text, txtImplFecha.Text)
    '      Else
    '          'recuperacion
    '          dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), "W", txtFechaRecu.Text, txtFechaRecu.Text)
    '      End If

    '      'Limpiar errores anteriores.
    '      ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTabla, pstrDenuId)
    '         hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, pstrDenuId, usrCria.cmbCriaRazaExt.Valor.ToString, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), 0, pstrDenuId)

    '      mstrCmd = "exec " & mstrTablaDeta & "_consul @tedd_tede_id = " & pstrDenuId

    '      Dim dr As SqlDataReader = clsSQLServer.gExecuteQueryDR(mstrConn, mstrCmd)

    '      Do While dr.Read
    '         'Aplicar reglas (si el registro no esta dado de baja)
    '         'If dr.GetValue(dr.GetOrdinal("tedd_baja_fecha")) Then
    '             'Limpiar errores anteriores.
    '             ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTablaDeta, dr.GetValue(dr.GetOrdinal("tedd_id")))
    '             'Aplicar Reglas.
    '             hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaDeta, dr.GetValue(dr.GetOrdinal("tedd_id")), usrCria.cmbCriaRazaExt.Valor.ToString, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), 0, 0)
    '         'End If
    '      Loop
    '      dr.Close()

    '      If hdnMsgError.Text <> "" Then
    '         AccesoBD.clsError.gGenerarMensajes(Me, hdnMsgError.Text)
    '      End If

    '   Catch ex As Exception
    '      clsError.gManejarError(Me, ex)
    '   End Try

    ' End Sub

    Private Sub mAplicarReglas(ByVal pstrDenuId As String, ByVal pstrTramId As String)

        Try

            Dim lintImplCanti As Integer = 0
            Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())

            Dim dtTramite As DataTable
            Dim intTramiteId As Integer



            lintImplCanti = (mdsDatos.Tables(mstrTablaDeta).Select("tedd_baja_fecha is null").GetUpperBound(0) + 1)
            If lintImplCanti <> 0 Then
                'implante
                dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), "W", txtImplFecha.Text, txtImplFecha.Text)
            Else
                'recuperacion
                dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), "W", txtFechaRecu.Text, txtFechaRecu.Text)
            End If

            dtTramite = oTramites.GetTramiteByDenunTEId(pstrDenuId, "TRAMITE")
            If dtTramite.Rows.Count() > 0 Then
                intTramiteId = dtTramite.Rows(0).Item("tram_id")
            End If

            'Limpiar errores anteriores.
            ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTabla, pstrDenuId)
            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, pstrDenuId, usrCria.cmbCriaRazaExt.Valor.ToString, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), intTramiteId, pstrDenuId)

            mstrCmd = "exec " & mstrTablaDeta & "_consul @tedd_tede_id = " & pstrDenuId

            Dim dr As SqlDataReader = clsSQLServer.gExecuteQueryDR(mstrConn, mstrCmd)

            Do While dr.Read
                'Aplicar reglas (si el registro no esta dado de baja)
                'If dr.GetValue(dr.GetOrdinal("tedd_baja_fecha")) Then
                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTablaDeta, dr.GetValue(dr.GetOrdinal("tedd_id")))
                'Aplicar Reglas.
                hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaDeta, dr.GetValue(dr.GetOrdinal("tedd_id")), usrCria.cmbCriaRazaExt.Valor.ToString, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImplantesRecuperaEmbriones.ToString(), intTramiteId, pstrDenuId)
                'End If
            Loop
            dr.Close()

            If hdnMsgError.Text <> "" Then
                AccesoBD.clsError.gGenerarMensajes(Me, hdnMsgError.Text)
                oTramites.GrabarEstadoRetenidaEnTramites(pstrTramId)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub


    Private Sub mModi()
        Try
            Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())

            Dim dtTramite As DataTable
            Dim intTramiteId As Integer = 0

            mGuardarDatos()
            Dim lobjDenu As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)

            lobjDenu.Modi()

            dtTramite = oTramites.GetTramiteByDenunTEId(hdnId.Text, "TRAMITE")
            If dtTramite.Rows.Count() > 0 Then
                intTramiteId = dtTramite.Rows(0).Item("tram_id")
            End If


            mAplicarReglas(hdnId.Text, intTramiteId)

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjDenu As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjDenu.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet

        Dim lintImplCanti As Integer

        mValidarDatos()

        If usrDona.Valor Is DBNull.Value And usrCria.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el propietario.")
        End If

        'If usrCria.Valor Is DBNull.Value And usrCria.Valor Is DBNull.Value Then
        '    Throw New AccesoBD.clsErrNeg("Debe indicar el criador.")
        'End If

        lintImplCanti = mdsDatos.Tables(mstrTablaDeta).Select("tedd_baja_fecha is null").GetUpperBound(0) + 1

        'If txtImplFecha.Text <> "" And lintImplCanti = 0 Then
        '    Throw New AccesoBD.clsErrNeg("Debe indicar el detalle de la implantaci�n.")
        'End If

        'If txtImplFecha.Text = "" And lintImplCanti > 0 Then
        '    Throw New AccesoBD.clsErrNeg("Debe indicar los datos de la implantaci�n.")
        'End If

        With mdsDatos.Tables(0).Rows(0)
            .Item("tede_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("tede_recu_fecha") = txtFechaRecu.Fecha.ToString
                .Item("tede_gest_dias") = txtDiasGest.Valor.ToString
                If txtCongEmbr.Valor.ToString.Length > 0 Then
                    .Item("tede_embr_cong") = txtCongEmbr.Valor.ToString
                End If
                If txtFresEmbr.Valor.ToString.Length > 0 Then
                    .Item("tede_embr_fres") = txtFresEmbr.Valor.ToString
                End If
                If txtImplCong.Valor.ToString.Length > 0 Then
                    .Item("tede_impl_embr_cong") = txtImplCong.Valor.ToString
                End If
                If txtImplFres.Valor.ToString.Length > 0 Then
                    .Item("tede_impl_embr_fres") = txtImplFres.Valor.ToString
                End If
                If txtServFecha.Fecha.ToString.Length > 0 Then
                    .Item("tede_serv_fecha") = txtServFecha.Fecha.ToString
                End If
                .Item("tede_pad1_prdt_id") = usrPadre1.Valor
                .Item("tede_pad2_prdt_id") = usrPadre2.Valor
                .Item("tede_madr_prdt_id") = usrDona.Valor
                .Item("tede_cria_id") = usrCria.Valor
                .Item("tede_clie_id") = usrClie.Valor
                If cmbCentro.Valor.ToString.Length > 0 Then
                    .Item("tede_imcr_id") = cmbCentro.Valor
                End If
                If cmbVete.Valor.ToString.Length > 0 Then
                    .Item("tede_auim_id") = cmbVete.Valor
                End If
            End With


        Return mdsDatos

    End Function

    Public Sub mCrearDataSet(ByVal pstrId As String)

        Dim lstrOpci As String


        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDeta


        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If




        grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
        grdDeta.DataBind()

        Session(mstrTabla) = mdsDatos

    End Sub

    

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

#Region "Detalle"
   Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrDeta As DataRow

         hdnDetaId.Text = E.Item.Cells(1).Text
         ldrDeta = mdsDatos.Tables(mstrTablaDeta).Select("tedd_id=" & hdnDetaId.Text)(0)

         With ldrDeta
                    txtTatu.Valor = .Item("tedd_tatu").ToString
                    txtObse.Valor = .Item("tedd_obse").ToString
                    cmbReti.Valor = .Item("tedd_rcti_id").ToString
                    txtCara.Valor = .Item("tedd_carv").ToString
                    cmbRaza.Valor = .Item("tedd_raza_id").ToString
                '  txtCruza.Valor = .Item("tedd_raza_cruza")
                    txtNaciFecha.Fecha = .Item("tedd_naci_fecha").ToString
                    txtImplFecha.Fecha = .Item("tedd_Impl_fecha").ToString

                    txtEsta.Text = .Item("_estado").ToString
         End With

         mSetearEditor(mstrTablaDeta, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.EditItemIndex = -1
         If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
            grdDeta.CurrentPageIndex = 0
         Else
            grdDeta.CurrentPageIndex = E.NewPageIndex
         End If
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
         grdDeta.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarDetalle(ByVal pbooAlta As Boolean)
      Try

            '  Dim lstrCruza As String = txtCruza.Text

         mGuardarDatosDeta(pbooAlta)

         If Not pbooAlta Then
            mLimpiarDetalle()
         End If

         grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
         grdDeta.DataBind()

            'If lstrCruza <> "" And Not mCruzaValida(lstrCruza) Then
            '   Throw New AccesoBD.clsErrNeg("El campo de cruzas de razas contiene c�digos de raza inv�lidos.")
            'End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosDeta(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
        Dim ldrDatos2 As DataRow
        Dim lstrProdRP As String
        Dim lintProdNume As Integer
        Dim mdsDatos2 As New DataSet
        mdsDatos2 = clsSQLServer.gObtenerEstruc(mstrConn, "rg_receptoras_tipos", "")


      mValidarDetalle()

      If hdnDetaId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDeta).NewRow
            ldrDatos2 = mdsDatos2.Tables("rg_receptoras_tipos").NewRow
            ldrDatos.Item("tedd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "tedd_id")
      Else
            ldrDatos = mdsDatos.Tables(mstrTablaDeta).Select("tedd_id=" & hdnDetaId.Text)(0)
            ldrDatos2 = mdsDatos2.Tables("rg_receptoras_tipos").Select("rcti_id=" & ldrDatos.Item("tedd_rcti_id"))(0)
      End If

      With ldrDatos
            .Item("tedd_tatu") = txtTatu.Valor.ToString
            .Item("tedd_obse") = txtObse.Valor.ToString
            '  .Item("tedd_raza_cruza") = txtCruza.Valor
            .Item("tedd_naci_fecha") = txtNaciFecha.Fecha
            .Item("tedd_impl_fecha") = txtImplFecha.Fecha
            .Item("_tipo") = cmbReti.SelectedItem.Text.ToString
            .Item("tedd_carv") = txtCara.Valor.ToString
            .Item("tedd_raza_id") = cmbRaza.Valor
            .Item("_raza") = IIf(cmbRaza.Valor.ToString = "0", "", cmbRaza.SelectedItem.Text)
            .Item("tedd_baja_fecha") = System.DBNull.Value
            .Item("_estado") = ""
      End With

        Dim oREGTiposRecep As New rgReceptorasTiposEntity
        oREGTiposRecep.rcti_audi_fecha = Date.Now
        oREGTiposRecep.rcti_audi_user = 1
        oREGTiposRecep.rcti_codi = 0
        oREGTiposRecep.rcti_desc = ""
        oREGTiposRecep.rcti_edad = DateDiff(DateInterval.Year, Convert.ToDateTime(txtNaciFecha.Fecha), Date.Now)
        '  oREGTiposRecep.rcti_id = ldrDatos.Item("tedd_rcti_id")


        If hdnDetaId.Text = "" Then

            With ldrDatos2
                mdsDatos.AcceptChanges()
                oREGTiposRecep.rcti_id = clsSQLServer.gProximoId(mstrConn, mdsDatos2.Tables("rg_receptoras_tipos").ToString(), "rcti_id") + 1

                .Item("rcti_audi_fecha") = Date.Now
                .Item("rcti_audi_user") = 1
                .Item("rcti_codi") = oREGTiposRecep.rcti_codi
                .Item("rcti_desc") = oREGTiposRecep.rcti_desc
                .Item("rcti_edad") = oREGTiposRecep.rcti_edad
                .Item("rcti_id") = oREGTiposRecep.rcti_id
            End With
        End If


        If hdnDetaId.Text = "" Then
            ldrDatos.Item("tedd_rcti_id") = oREGTiposRecep.rcti_id

            mdsDatos.Tables(mstrTablaDeta).Rows.Add(ldrDatos)

            ldrDatos2.Item("rcti_id") = oREGTiposRecep.rcti_id
            mdsDatos2.Tables("rg_receptoras_tipos").Rows.Add(ldrDatos2)
            mdsDatos.AcceptChanges()
            mdsDatos2.AcceptChanges()
        End If


        If Not mdsDatos.Tables.Contains("rg_receptoras_tipos") Then
            Dim dt As DataTable = mdsDatos2.Tables(0)
            mdsDatos2.Tables.Remove(dt)
            dt.TableName = "rg_receptoras_tipos"
            mdsDatos.Tables.Add(dt)

        End If
        mdsDatos.AcceptChanges()
        mdsDatos2.AcceptChanges()

        For Each lDr As DataRow In mdsDatos.Tables("rg_receptoras_tipos").Select()
            If lDr.Item("rcti_id") Is System.DBNull.Value Then
                lDr.Delete()
                'Else
                '    mdsDatos.Tables("rg_receptoras_tipos").Rows.Remove(lDr)
            End If
        Next
        Session(mstrTabla) = mdsDatos

   End Sub

   Function mCruzaValida(ByVal pstrCruza As String) As Boolean
      Try
         mstrCmd = "exec razas_cruzas_verificar "
         mstrCmd = mstrCmd & clsSQLServer.gFormatArg(pstrCruza, SqlDbType.VarChar)
         Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), mstrCmd)
         While (dr.Read())
            If dr.GetValue(0) = "S" Then
                Return True
            Else
                Return False
            End If
         End While
         dr.Close()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
         Return False
      End Try
   End Function

   Sub mValidarDetalle()
        If txtTatu.Text = "" And txtCara.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el nro de tatuaje o caravana.")
        End If


            If txtImplFecha.Text.Length = 0 Then
                If txtImplCong.Text <> "" Or txtImplFres.Text <> "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de implantaci�n.")
                End If
            End If
            If txtImplFecha.Text.Length <> 0 And txtImplCong.Text = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el detalle de la implantaci�n.")
            End If



        'If cmbReti.Valor.ToString = "" Then
        '    Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de receptora VC/VQ.")
        'End If
   End Sub

   Private Sub mValidarDatos()

      If usrClie.Valor.ToString = "" And usrCria.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente o el criador.")
      End If
            If usrDona.Valor.ToString.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la hembra donante.")
            End If
            If usrPadre1.Valor.ToString.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el padre sirviente.")
            End If

      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

            If txtServFecha.Text.ToString.Length > 0 And txtFechaRecu.Text.ToString.Length > 0 Then
                If txtServFecha.Fecha >= txtFechaRecu.Fecha Then
                    Throw New AccesoBD.clsErrNeg("La fecha de recupero debe ser mayor a la fecha de servicio.")
                End If
            End If

      If txtFechaRecu.Text = "" Then
         If txtCongEmbr.Text <> "" Or txtFresEmbr.Text <> "" Or txtDiasGest.Text <> "" Then
             Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de recupero.")
         End If
      End If

            If txtFechaRecu.Text.ToString.Length > 0 Then
                If (txtCongEmbr.Text.Length = 0 And txtFresEmbr.Text.Length = 0) Or txtDiasGest.Text.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar los datos de la recuperaci�n.")
                End If
            End If

        ''If txtImplFecha.Text = "" Then
        'If txtImplCong.Text <> "" Or txtImplFres.Text <> "" Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de implantaci�n.")
        'End If
        ''End If

        '' If txtImplFecha.Text <> "" Then
        'If txtImplCong.Text = "" And txtImplFres.Text = "" Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar los datos de la implantaci�n.")
        'End If
        ''End If

        If txtFechaRecu.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar los datos de la recuperaci�n .")
        End If

   End Sub

   Private Sub mLimpiarDetalle()
      hdnDetaId.Text = ""
      txtCara.Text = ""
      txtTatu.Text = ""
      txtObse.Text = ""
      txtEsta.Text = ""
      txtNaciFecha.Text = ""
        txtImplFecha.Text = ""
        '   txtCruza.Text = ""
      cmbReti.Limpiar()
      cmbRaza.Limpiar()
      mSetearEditor(mstrTablaDeta, True)
   End Sub

#End Region

   Private Sub lnkDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
      mShowTabs(2)
   End Sub

   Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDetalle()
   End Sub

   Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
      Try
         With mdsDatos.Tables(mstrTablaDeta).Select("tedd_id=" & hdnDetaId.Text)(0)
            .Item("tedd_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
         grdDeta.DataBind()
         mLimpiarDetalle()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDetalle(True)
   End Sub

   Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDetalle(False)
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Protected Overrides Sub Finalize()
      MyBase.Finalize()
   End Sub

End Class
End Namespace
