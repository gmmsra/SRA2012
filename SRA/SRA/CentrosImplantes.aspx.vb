Namespace SRA

Partial Class CentrosImplantes
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_ImplantesCentros
    Private mstrTablaAutor As String = SRA_Neg.Constantes.gTab_ImplantesCentrosAutor
    Private mstrTablaDire As String = SRA_Neg.Constantes.gTab_ImplantesCentrosDire
    Private mstrTablaTele As String = SRA_Neg.Constantes.gTab_ImplantesCentrosTele
    Private mstrTablaMail As String = SRA_Neg.Constantes.gTab_ImplantesCentrosMails
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

        txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "imcr_codi")
        txtDenominacion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "imcr_deno")
        txtVeterinarioCargo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "imcr_vete")

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDire)
        txtDireRef.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "diim_refe")
        txtDireCP.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "diim_cpos")
        txtDireDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "diim_dire")

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaTele)
        txtTeleAreaCode.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "teim_area_code")
        txtTeleRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "teim_refe")
        txtTeleTele.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "teim_tele")

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaMail)
        txtMailRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "maim_refe")
        txtMailMail.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "maim_mail")

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaAutor)
        txtAutorNomAp.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "auim_nyap")
        txtAutorObs.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "auim_obse")


    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstado, "", SRA_Neg.Constantes.EstaTipos.EstaTipo_CtroImplante)
        clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTeleTipo, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDirePais, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDirePaisFil, "S")
        cmbDirePaisFil.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id")
        cmbDirePais.Valor = cmbDirePaisFil.Valor
        clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePaisFil.Valor, cmbDirePciaFil, "id", "descrip", "S", True)
        clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridDire_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDire.EditItemIndex = -1
            If (grdDire.CurrentPageIndex < 0 Or grdDire.CurrentPageIndex >= grdDire.PageCount) Then
                grdDire.CurrentPageIndex = 0
            Else
                grdDire.CurrentPageIndex = E.NewPageIndex
            End If
            grdDire.DataSource = mdsDatos.Tables(mstrTablaDire)
            grdDire.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridTele_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdTele.EditItemIndex = -1
            If (grdTele.CurrentPageIndex < 0 Or grdTele.CurrentPageIndex >= grdTele.PageCount) Then
                grdTele.CurrentPageIndex = 0
            Else
                grdTele.CurrentPageIndex = E.NewPageIndex
            End If
            grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
            grdTele.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridMail_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdMail.CurrentPageIndex < 0 Or grdMail.CurrentPageIndex >= grdMail.PageCount) Then
                grdMail.CurrentPageIndex = 0
            Else
                grdMail.CurrentPageIndex = E.NewPageIndex
            End If
            grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
            grdMail.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridAutor_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAutor.EditItemIndex = -1
            If (grdAutor.CurrentPageIndex < 0 Or grdAutor.CurrentPageIndex >= grdAutor.PageCount) Then
                grdAutor.CurrentPageIndex = 0
            Else
                grdAutor.CurrentPageIndex = E.NewPageIndex
            End If
            grdAutor.DataSource = mdsDatos.Tables(mstrTablaAutor)
            grdAutor.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul @formato='1'"
            mstrCmd += ", @imcr_codi= " + IIf(txtCodigoFil.Text = "", "null", txtCodigoFil.Text)
            mstrCmd += ", @imcr_deno= '" + txtDenominacionFil.Text + "'"
            mstrCmd += ", @imcr_vete= '" + txtVeterinarioCargoFil.Text + "'"
            mstrCmd += ", @pais_id  = " + cmbDirePaisFil.Valor.ToString
            mstrCmd += ", @prov_id  = " + cmbDirePciaFil.Valor.ToString
            mstrCmd += ", @loca_id  = " + cmbDireLocaFil.Valor.ToString
            mstrCmd += ", @imcr_clie_id  = " + IIf(usrClieFilt.Valor Is DBNull.Value, "null", usrClieFilt.Valor)


            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorDire(ByVal pbooAlta As Boolean)
        btnBajaDire.Enabled = Not (pbooAlta)
        btnModiDire.Enabled = Not (pbooAlta)
        btnAltaDire.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorTele(ByVal pbooAlta As Boolean)
        btnBajaTele.Enabled = Not (pbooAlta)
        btnModiTele.Enabled = Not (pbooAlta)
        btnAltaTele.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorMail(ByVal pbooAlta As Boolean)
        btnBajaMail.Enabled = Not (pbooAlta)
        btnModiMail.Enabled = Not (pbooAlta)
        btnAltaMail.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorAutor(ByVal pbooAlta As Boolean)
        btnBajaAutor.Enabled = Not (pbooAlta)
        btnModiAutor.Enabled = Not (pbooAlta)
        btnAltaAutor.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtCodigo.Text = .Item("imcr_codi")
                txtDenominacion.Text = .Item("imcr_deno")
                txtVeterinarioCargo.Text = .Item("imcr_vete")
                cmbEstado.Valor = .Item("imcr_esta_id")
                cmbEstado.Enabled = True
                If Not .IsNull("imcr_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("imcr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
                txtNroHabSenasa.Text = .Item("imcr_senasa_nume_habi")
                    txtFechaAlta.Fecha = IIf(.Item("imcr_fecha_alta").ToString.Length > 0, .Item("imcr_fecha_alta"), String.Empty)
                    txtFechaBaja.Fecha = IIf(.Item("imcr_fecha_baja").ToString.Length > 0, .Item("imcr_fecha_baja"), String.Empty)
                    txtFechaHabiDesde.Fecha = IIf(.Item("imcr_fecha_habi_desde").ToString.Length > 0, .Item("imcr_fecha_habi_desde"), String.Empty)
                    txtFechaHabihasta.Fecha = IIf(.Item("imcr_fecha_habi_hasta").ToString.Length > 0, .Item("imcr_fecha_habi_hasta"), String.Empty)
                    usrClie.Valor = .Item("imcr_clie_id")
            End With

            mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
    Public Sub mEditarDatosDire(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDire As DataRow

            hdnDireId.Text = E.Item.Cells(1).Text
            ldrDire = mdsDatos.Tables(mstrTablaDire).Select("diim_id=" & hdnDireId.Text)(0)

            With ldrDire
                cmbDirePais.Valor = .Item("_pais_id")
                txtDireRef.Valor = .Item("diim_refe")
                txtDireCP.Valor = .Item("diim_cpos")
                txtDireCP.ValidarPaisDefault = IIf(cmbDirePais.Valor = Session("sPaisDefaId"), "S", "N")
                txtDireDire.Valor = .Item("diim_dire")

                clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
                cmbDirePcia.Valor = .Item("_pcia_id")

                clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
                cmbDireLoca.Valor = .Item("diim_loca_id")


                chkDireDefalt.Checked = IIf(.IsNull("diim_defa"), 0, .Item("diim_defa"))
                If Not .IsNull("diim_baja_fecha") Then
                    lblBajaDire.Text = "Direcci�n dada de baja en fecha: " & CDate(.Item("diim_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaDire.Text = ""
                End If
            End With
            mSetearEditorDire(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrTele As DataRow

            hdnTeleId.Text = E.Item.Cells(1).Text
            ldrTele = mdsDatos.Tables(mstrTablaTele).Select("teim_id=" & hdnTeleId.Text)(0)

            With ldrTele
                txtTeleRefe.Valor = .Item("teim_refe")
                txtTeleAreaCode.Valor = .Item("teim_area_code")
                txtTeleTele.Valor = .Item("teim_tele")
                cmbTeleTipo.Valor = .Item("teim_teti_id")
                If Not .IsNull("teim_baja_fecha") Then
                    lblBajaTele.Text = "Tel�fono dado de baja en fecha: " & CDate(.Item("teim_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaTele.Text = ""
                End If
            End With
            mSetearEditorTele(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrMail As DataRow

            hdnMailId.Text = E.Item.Cells(1).Text
            ldrMail = mdsDatos.Tables(mstrTablaMail).Select("maim_id=" & hdnMailId.Text)(0)

            With ldrMail
                txtMailRefe.Valor = .Item("maim_refe")
                txtMailMail.Valor = .Item("maim_mail")
                If Not .IsNull("maim_baja_fecha") Then
                    lblBajaMail.Text = "Mail dado de baja en fecha: " & CDate(.Item("maim_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaMail.Text = ""
                End If
            End With
            mSetearEditorMail(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosAutor(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrAutor As DataRow

            hdnAutorId.Text = E.Item.Cells(1).Text
            ldrAutor = mdsDatos.Tables(mstrTablaAutor).Select("auim_id=" & hdnAutorId.Text)(0)

            With ldrAutor
                txtAutorNomAp.Valor = .Item("auim_nyap")
                txtAutorObs.Valor = .Item("auim_obse")
                If Not .IsNull("auim_baja_fecha") Then
                    lblBajaAutor.Text = "Autorizado dado de baja en fecha: " & CDate(.Item("auim_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaAutor.Text = ""
                End If
            End With
            mSetearEditorAutor(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDire
        mdsDatos.Tables(2).TableName = mstrTablaTele
        mdsDatos.Tables(3).TableName = mstrTablaMail
        mdsDatos.Tables(4).TableName = mstrTablaAutor

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdDire.DataSource = mdsDatos.Tables(mstrTablaDire)
        grdDire.DataBind()

        grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
        grdTele.DataBind()

        grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
        grdMail.DataBind()

        grdAutor.DataSource = mdsDatos.Tables(mstrTablaAutor)
        grdAutor.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        txtCodigo.Text = darProximoCodigo()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    ''' Informa el proximo numero disponible y lo suguiere al usuario
    ''' MF 22/03/2013
    Private Function darProximoCodigo() As Integer
        darProximoCodigo = clsSQLServer.gExecuteScalar(mstrConn, "IMPLANTE_CTROS_ProxCodigo")

    End Function
    Private Sub mLimpiar()
        hdnId.Text = ""
        txtCodigo.Text = ""
        txtDenominacion.Text = ""
        txtVeterinarioCargo.Text = ""
        cmbEstado.Valor = CType(SRA_Neg.Constantes.Estados.CentrosImplanes_Vigente, String)
        cmbEstado.Enabled = False
        txtNroHabSenasa.Text = ""
        txtFechaAlta.Text = ""
        txtFechaBaja.Text = ""
        txtFechaHabiDesde.Text = ""
        txtFechaHabihasta.Text = ""
        lblBaja.Text = ""
        hdnLocaPop.Text = ""
        mLimpiarDire()
        mLimpiarTele()
        mLimpiarMail()
        mLimpiarAutor()
        usrClie.Limpiar()
        usrClieFilt.Limpiar()
        grdDato.CurrentPageIndex = 0
        grdDire.CurrentPageIndex = 0
        grdTele.CurrentPageIndex = 0
        grdMail.CurrentPageIndex = 0
        grdAutor.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)

        cmbDirePaisFil.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id")
        cmbDirePais.Valor = cmbDirePaisFil.Valor
    End Sub
    Private Sub mLimpiarFil()
        txtCodigoFil.Text = ""
        txtDenominacionFil.Text = ""
        txtVeterinarioCargoFil.Text = ""

        cmbDirePaisFil.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id")
        cmbDirePais.Valor = cmbDirePaisFil.Valor
        clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePaisFil.Valor, cmbDirePaisFil, "id", "descrip", "S", True)
        clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)

        mConsultar()
    End Sub
    Private Sub mLimpiarDire()
        hdnDireId.Text = ""
        txtDireRef.Text = ""
        txtDireCP.Text = ""
        txtDireDire.Text = ""

        cmbDirePais.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id")
        If cmbDirePais.Valor.ToString <> "" Then
            clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
        Else
            cmbDirePcia.Items.Clear()
        End If

        cmbDireLoca.Items.Clear()

        lblBajaDire.Text = ""
        chkDireDefalt.Checked = False
        mSetearEditorDire(True)
    End Sub
    Private Sub mLimpiarTele()
        hdnTeleId.Text = ""
        txtTeleRefe.Text = ""
        txtTeleAreaCode.Text = ""
        txtTeleTele.Text = ""
        cmbTeleTipo.Limpiar()
        lblBajaTele.Text = ""
        mSetearEditorTele(True)
    End Sub
    Private Sub mLimpiarMail()
        hdnMailId.Text = ""
        txtMailRefe.Text = ""
        txtMailMail.Text = ""
        lblBajaMail.Text = ""
        mSetearEditorMail(True)
    End Sub
    Private Sub mLimpiarAutor()
        hdnAutorId.Text = ""
        txtAutorNomAp.Text = ""
        txtAutorObs.Text = ""
        lblBajaAutor.Text = ""
        mSetearEditorAutor(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            If usrClie.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente.")
            End If

            mGuardarDatos()

            If grdMail.Items.Count = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un correo electronico.")
            End If

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            Dim intId As Integer = lobjGenerica.Alta()
            Dim mdsDs As New DataSet

            mdsDs = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, intId)

            clsError.gGenerarMensajes(Me, "Se gener� el Centro de Implante con el c�digo " + mdsDs.Tables(0).Rows(0).Item("imcr_codi").ToString)

            ' mail aca
            Dim email As New Common.MailManager
            If (mdsDatos.Tables(3).Rows(0).Item("maim_mail") Is DBNull.Value Or mdsDatos.Tables(3).Rows(0).Item("maim_mail").ToString.Length > 0) Then
                email.VarEmail = mdsDatos.Tables(3).Rows(0).Item("maim_mail").ToString
            Else
                email.VarEmail = "procesos@sra.org.ar;dariovasqueznigro@gmail.com"
            End If

            Try
                email.EnviaMensaje("Mensaje de Sociedad Rural Argentna" _
                                 , "Codigo de centro SRA: " & mdsDs.Tables(0).Rows(0).Item("imcr_codi").ToString & " " & _
                                 ",Nro habilitaci�n SENASA: " & mdsDs.Tables(0).Rows(0).Item("imcr_senasa_nume_habi").ToString & " " & _
                                 ",Nro Cliente: " & mdsDs.Tables(0).Rows(0).Item("imcr_clie_id").ToString)
            Catch ex As System.Exception
                Do Until (ex.InnerException Is Nothing)
                    ex = ex.InnerException
                Loop
            End Try
            '

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            If usrClie.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente.")
            End If

            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Modi()

            Dim mdsDs As New DataSet
            mdsDs = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

            clsError.gGenerarMensajes(Me, "Se actualiz� el Centro de Implante con el c�digo " + mdsDs.Tables(0).Rows(0).Item("imcr_codi").ToString)

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If grdDire.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar al menos una Direcci�n.")
        End If

        Dim cantVal As Integer
        Dim cantValAct As Integer
        For Each oDataItem As DataGridItem In grdDire.Items
            If oDataItem.Cells(5).Text = "Si" Then
                cantVal += 1
            End If
            If oDataItem.Cells(6).Text = "Activo" Then
                cantValAct += 1
            End If
        Next
        If cantVal = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe haber una Direcci�n default vigente.")
        End If

        If cantValAct = 0 Then
            Throw New AccesoBD.clsErrNeg("No hay Direcciones vigentes ingresadas.")
        End If
    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("imcr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("imcr_codi") = txtCodigo.Valor
            .Item("imcr_deno") = txtDenominacion.Valor
            .Item("imcr_vete") = txtVeterinarioCargo.Valor
            .Item("imcr_esta_id") = cmbEstado.Valor
            If cmbEstado.Valor = 71 Then
                .Item("imcr_baja_fecha") = Date.Now
            Else
                .Item("imcr_baja_fecha") = DBNull.Value
            End If
            .Item("imcr_audi_user") = Session("sUserId").ToString()
            .Item("imcr_senasa_nume_habi") = txtNroHabSenasa.Valor
                .Item("imcr_fecha_alta") = txtFechaAlta.Fecha
                If txtFechaBaja.Fecha.ToString.Length > 0 Then
                    .Item("imcr_fecha_baja") = txtFechaBaja.Fecha
                End If
                If txtFechaHabiDesde.Fecha.ToString.Length > 0 Then
                    .Item("imcr_fecha_habi_desde") = txtFechaHabiDesde.Fecha
                End If
                If txtFechaHabihasta.Fecha.ToString.Length > 0 Then
                    .Item("imcr_fecha_habi_hasta") = txtFechaHabihasta.Fecha
                End If
                .Item("imcr_clie_id") = usrClie.Valor
            End With
    End Sub
    'Seccion de los Direcciones
    Private Sub mActualizarDire()
        Try
            mGuardarDatosDire()

            mLimpiarDire()
            grdDire.DataSource = mdsDatos.Tables(mstrTablaDire)
            grdDire.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaDire()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDire).Select("diim_id=" & hdnDireId.Text)(0)
            row.Delete()
            grdDire.DataSource = mdsDatos.Tables(mstrTablaDire)
            grdDire.DataBind()
            mLimpiarDire()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosDire()
        Dim ldrDire As DataRow
        Dim vbooDefault As Boolean

        If cmbDireLoca.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Localidad.")
        End If

        If txtDireDire.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Direcci�n.")
        End If

        If txtDireCP.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el C�digo Postal.")
        End If

        If chkDireDefalt.Checked Then
            For Each dr As DataRow In mdsDatos.Tables(mstrTablaDire).Select("diim_defa=1")
                With dr
                    .Item("diim_defa") = False
                    .Item("_default") = ""
                End With
            Next
        End If

        vbooDefault = (mdsDatos.Tables(mstrTablaDire).Select("diim_defa=1").GetLength(0) = 0)

        If hdnDireId.Text = "" Then
            ldrDire = mdsDatos.Tables(mstrTablaDire).NewRow
            ldrDire.Item("diim_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDire), "diim_id")
        Else
            ldrDire = mdsDatos.Tables(mstrTablaDire).Select("diim_id=" & hdnDireId.Text)(0)
        End If

        With ldrDire
            .Item("diim_refe") = txtDireRef.Valor
            .Item("diim_cpos") = txtDireCP.Valor
            .Item("diim_dire") = txtDireDire.Valor
            .Item("diim_loca_id") = cmbDireLoca.Valor
            .Item("_pcia_id") = cmbDirePcia.Valor
            .Item("_pais_id") = cmbDirePais.Valor
            .Item("_localidad") = cmbDireLoca.SelectedItem.Text
            .Item("diim_defa") = vbooDefault
            .Item("_default") = IIf(.Item("diim_defa"), "Si", "No")
            .Item("_estado") = "Activo"
            .Item("diim_imcr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("diim_audi_user") = Session("sUserId").ToString()
            .Item("diim_baja_fecha") = DBNull.Value
        End With
        If (hdnDireId.Text = "") Then
            mdsDatos.Tables(mstrTablaDire).Rows.Add(ldrDire)
        End If
    End Sub
    'Seccion de los Telefonos
    Private Sub mActualizarTele()
        Try
            mGuardarDatosTele()

            mLimpiarTele()
            grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
            grdTele.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaTele()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaTele).Select("teim_id=" & hdnTeleId.Text)(0)
            row.Delete()
            grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
            grdTele.DataBind()
            mLimpiarTele()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosTele()
        Dim ldrTele As DataRow

        If cmbTeleTipo.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Tipo de Tel�fono.")
        End If

        If txtTeleRefe.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
        End If

        If txtTeleAreaCode.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el C�digo de �rea.")
        End If

        If txtTeleTele.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Tel�fono.")
        End If

        If hdnTeleId.Text = "" Then
            ldrTele = mdsDatos.Tables(mstrTablaTele).NewRow
            ldrTele.Item("teim_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaTele), "teim_id")
        Else
            ldrTele = mdsDatos.Tables(mstrTablaTele).Select("teim_id=" & hdnTeleId.Text)(0)
        End If

        With ldrTele
            .Item("teim_refe") = txtTeleRefe.Valor
            .Item("teim_area_code") = txtTeleAreaCode.Valor
            .Item("teim_tele") = txtTeleTele.Valor
            .Item("teim_teti_id") = cmbTeleTipo.Valor
            .Item("_TipoTele") = cmbTeleTipo.SelectedItem.Text
            .Item("teim_imcr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("_estado") = "Activo"
            .Item("teim_audi_user") = Session("sUserId").ToString()
            .Item("teim_baja_fecha") = DBNull.Value
        End With
        If (hdnTeleId.Text = "") Then
            mdsDatos.Tables(mstrTablaTele).Rows.Add(ldrTele)
        End If
    End Sub
    'Seccion de los Mails
    Private Sub mActualizarMail()
        Try
            mGuardarDatosMail()

            mLimpiarMail()
            grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
            grdMail.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaMail()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaMail).Select("maim_id=" & hdnMailId.Text)(0)
            row.Delete()
            grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
            grdMail.DataBind()
            mLimpiarMail()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosMail()
        Dim ldrMail As DataRow

        If txtMailRefe.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
        End If

        If txtMailMail.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Direcci�n de Mail.")
        End If

        If hdnMailId.Text = "" Then
            ldrMail = mdsDatos.Tables(mstrTablaMail).NewRow
            ldrMail.Item("maim_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaMail), "maim_id")
        Else
            ldrMail = mdsDatos.Tables(mstrTablaMail).Select("maim_id=" & hdnMailId.Text)(0)
        End If

        With ldrMail
            .Item("maim_refe") = txtMailRefe.Valor
            .Item("maim_mail") = txtMailMail.Valor
            .Item("maim_imcr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("_estado") = "Activo"
            .Item("maim_audi_user") = Session("sUserId").ToString()
            .Item("maim_baja_fecha") = DBNull.Value
        End With
        If (hdnMailId.Text = "") Then
            mdsDatos.Tables(mstrTablaMail).Rows.Add(ldrMail)
        End If
    End Sub
    'Seccion de los Autorizados
    Private Sub mActualizarAutor()
        Try
            mGuardarDatosAutor()

            mLimpiarAutor()
            grdAutor.DataSource = mdsDatos.Tables(mstrTablaAutor)
            grdAutor.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaAutor()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaAutor).Select("auim_id=" & hdnAutorId.Text)(0)
            row.Delete()
            grdAutor.DataSource = mdsDatos.Tables(mstrTablaAutor)
            grdAutor.DataBind()
            mLimpiarAutor()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosAutor()
        Dim ldrAutor As DataRow

        If txtAutorNomAp.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre y Apellido.")
        End If

        If hdnAutorId.Text = "" Then
            ldrAutor = mdsDatos.Tables(mstrTablaAutor).NewRow
            ldrAutor.Item("auim_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaAutor), "auim_id")
        Else
            ldrAutor = mdsDatos.Tables(mstrTablaAutor).Select("auim_id=" & hdnAutorId.Text)(0)
        End If

        With ldrAutor
            .Item("auim_nyap") = txtAutorNomAp.Valor
            .Item("auim_obse") = txtAutorObs.Valor
            .Item("auim_imcr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("_estado") = "Activo"
            .Item("auim_audi_user") = Session("sUserId").ToString()
            .Item("auim_baja_fecha") = DBNull.Value
        End With
        If (hdnAutorId.Text = "") Then
            mdsDatos.Tables(mstrTablaAutor).Rows.Add(ldrAutor)
        End If
        'mdsDatos.Tables(mstrTablaAutor).DefaultView.Sort = "clal_id DESC"
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panDirecciones.Visible = False
            lnkDirecciones.Font.Bold = False
            panTelefonos.Visible = False
            lnkTelefonos.Font.Bold = False
            panMails.Visible = False
            lnkMails.Font.Bold = False
            panAutorizados.Visible = False
            lnkAutorizados.Font.Bold = False
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    panDirecciones.Visible = True
                    lnkDirecciones.Font.Bold = True
                Case 3
                    panTelefonos.Visible = True
                    lnkTelefonos.Font.Bold = True
                Case 4
                    panMails.Visible = True
                    lnkMails.Font.Bold = True
                Case 5
                    panAutorizados.Visible = True
                    lnkAutorizados.Font.Bold = True
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        mConsultar()
    End Sub
    'Botones generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones de direcciones
    Private Sub btnAltaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDire.Click
        mActualizarDire()
    End Sub
    Private Sub btnBajaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDire.Click
        mBajaDire()
    End Sub
    Private Sub btnModiDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDire.Click
        mActualizarDire()
    End Sub
    Private Sub btnLimpDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDire.Click
        mLimpiarDire()
    End Sub
    'Botones de Telefonos
    Private Sub btnAltaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaTele.Click
        mActualizarTele()
    End Sub
    Private Sub btnBajaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaTele.Click
        mBajaTele()
    End Sub
    Private Sub btnModiTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiTele.Click
        mActualizarTele()
    End Sub
    Private Sub btnLimpTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpTele.Click
        mLimpiarTele()
    End Sub
    'Botones de mails
    Private Sub btnAltaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaMail.Click
        mActualizarMail()
    End Sub
    Private Sub btnBajaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaMail.Click
        mBajaMail()
    End Sub
    Private Sub btnModiMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiMail.Click
        mActualizarMail()
    End Sub
    Private Sub btnLimpMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpMail.Click
        mLimpiarMail()
    End Sub
    'Botones de Autorizados
    Private Sub btnAltaActor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAutor.Click
        mActualizarAutor()
    End Sub
    Private Sub btnBajaAutor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAutor.Click
        mBajaAutor()
    End Sub
    Private Sub btnModiAutor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAutor.Click
        mActualizarAutor()
    End Sub
    Private Sub btnLimpAutor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAutor.Click
        mLimpiarAutor()
    End Sub
    'Solapas
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDirecciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDirecciones.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkTelefonos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTelefonos.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkMails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMails.Click
        mShowTabs(4)
    End Sub
    Private Sub lnkAutorizados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAutorizados.Click
        mShowTabs(5)
    End Sub
    Private Sub hdnLocaPop_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdnLocaPop.TextChanged
        Try
            If (hdnLocaPop.Text <> "") Then
                cmbDireLoca.Valor = hdnLocaPop.Text
                txtDireCP.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "localidades", cmbDireLoca.Valor.ToString, "loca_cpos").ToString
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            If (hdnDatosPop.Text <> "") Then

                mstrCmd = "exec dbo.dire_clientes_consul "
                mstrCmd += hdnDatosPop.Text


                Dim ds As New DataSet
                ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

                If ds.Tables(0).Rows.Count > 0 Then
                    With ds.Tables(0).Rows(0)
                        cmbDirePais.Valor = .Item("_pais_id")
                        clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
                        cmbDirePcia.Valor = .Item("_pcia_id")
                        clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
                        cmbDireLoca.Valor = .Item("diim_loca_id")

                        txtDireCP.Valor = .Item("diim_cpos")
                        txtDireDire.Valor = .Item("diim_dire")
                        txtDireRef.Valor = .Item("diim_refe")
                    End With
                End If
                hdnDatosPop.Text = ""

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
#End Region

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
End Class
End Namespace
