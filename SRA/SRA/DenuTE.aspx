<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DenuTE" CodeFile="DenuTE.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Denuncias de Transferencia y Recuperacion Embrionaria</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		} 
		function mVerErrores()
		{
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Errores&tabla=te_denun_errores&filtros=" + document.all("hdnId").value, 1, "600","300","70","100");
			gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce=6&id=" + document.all("hdnId").value, 6, "700","400","20","40");
		}
		function mCargarVeterinarios(pCentro)
		{
		    var sFiltro = '0' + pCentro.value;
			LoadComboXML("autorizados_implantes_cargar", sFiltro, "cmbVete", "S");
 
		}		
		
		
		function mCargarVeterinariosFil(pCentro)
		{
		    var sFiltro = '0' + pCentro.value;
			LoadComboXML("autorizados_implantes_cargar", sFiltro, "cmbCentroFil", "S");

		}		

		
		function mSumarTotales()
		{
			document.all('txtTotal').value = gConvertNumber(document.all('txtFresEmbr').value) + gConvertNumber(document.all('txtCongEmbr').value);
		}	
		function usrCria_onchange()
		{		
			//alert('entro usrCria_onchange');
			var lstrCriaId = document.all('usrCria:txtId').value;
			var lstrClieId = LeerCamposXML("criadores", lstrCriaId, "cria_clie_id");
			if (lstrClieId != "")
			{
				document.all('usrClie:txtCodi').value = lstrClieId;
				document.all('usrClie:txtCodi').onchange();
			}
		}	
		function usrDona_cmbProdRaza_onchange()
		{
		 
		}
		
		
		
		
		function usrCria_cmbRazaCria_onchange()
		{
		
				 
			if (document.all('usrDona_cmbProdRaza')!=null)
			{
				if (document.all('usrDona_cmbProdRaza').selectedIndex != document.all('usrCria_cmbRazaCria').selectedIndex)
				{
					imgLimpProdDeriv_click('usrDona');
					document.all('usrDona_cmbProdRaza').selectedIndex = document.all('usrCria_cmbRazaCria').selectedIndex;
					document.all('usrDona_cmbProdRaza').onchange();
				}
				document.all('usrDona_cmbProdRaza').disabled = true;
				document.all('txtusrDona:cmbProdRaza').disabled = true;					
				if (document.all('usrPadre1_cmbProdRaza').selectedIndex != document.all('usrCria_cmbRazaCria').selectedIndex)
				{
					imgLimpProdDeriv_click('usrPadre1');
					document.all('usrPadre1_cmbProdRaza').selectedIndex = document.all('usrCria_cmbRazaCria').selectedIndex;
					document.all('usrPadre1_cmbProdRaza').onchange();
				}
				document.all('usrPadre1_cmbProdRaza').disabled = true;
				document.all('txtusrPadre1:cmbProdRaza').disabled = true;
				if (document.all('usrPadre2_cmbProdRaza').selectedIndex != document.all('usrCria_cmbRazaCria').selectedIndex)
				{
					imgLimpProdDeriv_click('usrPadre2');
					document.all('usrPadre2_cmbProdRaza').selectedIndex = document.all('usrCria_cmbRazaCria').selectedIndex;
					document.all('usrPadre2_cmbProdRaza').onchange();
				}
				document.all('usrPadre2_cmbProdRaza').disabled = true;
				document.all('txtusrPadre2:cmbProdRaza').disabled = true;
			}
			
		 
		}
		
		
		function usrCriaFil_cmbRazaCria_onchange()
		{
		
			if (document.all('usrCriaFil:cmbRazaCria')!=null)
			{
				document.all("usrPadreFil:cmbProdRaza").value = document.all("usrCriaFil:cmbRazaCria").value;
				
				document.all("usrPadreFil:cmbProdRaza").selectedIndex = document.all('usrCriaFil_cmbRazaCria').selectedIndex;
					//document.all('usrPadre2_cmbProdRaza').onchange();
				
				document.all("usrPadreFil:cmbProdRaza").onchange();
				document.all("usrPadreFil:cmbProdRaza").disabled = true;
				document.all("txtusrPadreFil:cmbProdRaza").disabled = true;	 
				
			 
				document.all("usrMadreFil:cmbProdRaza").value = document.all("usrCriaFil:cmbRazaCria").value;
				document.all("usrMadreFil:cmbProdRaza").onchange();
				document.all("usrMadreFil:cmbProdRaza").disabled = true;
				document.all("txtusrMadreFil:cmbProdRaza").disabled = true;	 
					
			}
		}
		
		
		
		function cmbRazaCriaFil_change()
		{
		alert('cmbRazaCriaFil_change');
		    if(document.all("cmbRazaFil").value!="")
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = true;
				
				document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = true;
				document.all("txtusrProdFil:cmbProdRaza").disabled = true;
								
			}
			else
			{
				document.all("usrCriaFil:cmbRazaCria").value = ""
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = false;
				
				document.all("usrProdFil:cmbProdRaza").value = "";
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = false;
				document.all("txtusrProdFil:cmbProdRaza").disabled = false;
			}
			 
		}
		
		
		
		function usrCriaFil()
		{
		 
		
		    if(document.all("cmbRazaFil").value!="")
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = true;
				
				document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = true;
				document.all("txtusrProdFil:cmbProdRaza").disabled = true;
			}
			else
			{
				document.all("usrCriaFil:cmbRazaCria").value = ""
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = false;
				
				document.all("usrProdFil:cmbProdRaza").value = "";
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = false;
				document.all("txtusrProdFil:cmbProdRaza").disabled = false;
			}
		}			
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Transferencia y recuperacion embrionaria</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" width="99%" BorderStyle="none"
											BorderWidth="1px">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD colSpan="3">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="2" colSpan="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																<TD>
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TR>
																			<TD height="6" background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD height="6" background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																			<TD style="WIDTH: 88%" height="6" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:label id="lblNroFil" runat="server" cssclass="titulo">Nro.Denuncia:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtNroFil" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="9999999999999"
																					esdecimal="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:label id="lblClieFil" runat="server" cssclass="titulo">Propietario (Cliente):&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClieFil" runat="server" MostrarBotones="False" AceptaNull="false" CampoVal="Propietario"
																					AutoPostBack="False" Ancho="800" Tabla="Clientes" Saltos="1,2" FilSociNume="True" MuestraDesc="False"
																					FilDocuNume="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:label id="lblCriaFil" runat="server" cssclass="titulo">Propietario (Raza/Criador):&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrCriaFil" runat="server" MostrarBotones="False" AceptaNull="false" CampoVal="Propietario"
																					AutoPostBack="False" Ancho="800" Tabla="Criadores" Saltos="1,2" FilSociNume="True" MuestraDesc="False"
																					FilDocuNume="True" FilTipo="S"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Denuncia Desde:&nbsp;</asp:Label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox>&nbsp; &nbsp;
																				<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:Label id="lblRecuFechaDesde" runat="server" cssclass="titulo">Fecha Recup. Desde:&nbsp;</asp:Label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtRecuFechaDesdeFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																				<asp:Label id="lblRecuFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>
																				<cc1:DateBox id="txtRecuFechaHastaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:Label id="lblImplFechaDesde" runat="server" cssclass="titulo">Fecha Implante Desde:&nbsp;</asp:Label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtImplFechaDesdeFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																				<asp:Label id="lblImplFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>
																				<cc1:DateBox id="txtImplFechaHastaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																														
																			<asp:Label id="lblCentroFil" runat="server" cssclass="titulo">Centro de Implante:&nbsp;</asp:Label></TD>
																		<TD  background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbCentroFil" autopostback=true class="combo" runat="server" cssclass="cuadrotexto" Width="460px"
																				MostrarBotones="False" Height="20px" NomOper="Centro" filtra="true" Obligatorio="false" onchange="javascript:mCargarVeterinarios(this);"></cc1:combobox></TD>
																	</tr>	
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="20" background="imagenes/formfdofields.jpg"></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:label id="lblPadreFil" runat="server" cssclass="titulo">Padre Sirviente:&nbsp;</asp:label></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg">
																				<UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
																					Tabla="productos" Saltos="1,2" FilSociNume="True" MuestraDesc="True" FilDocuNume="True"
																					FilTipo="S"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="20" background="imagenes/formfdofields.jpg"></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:label id="lblMadreFil" runat="server" cssclass="titulo">Hembra Donante:&nbsp;</asp:label></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg">
																				<UC1:PROD id="usrMadreFil" runat="server" AceptaNull="false" AutoPostBack="True" Ancho="800"
																					Tabla="productos" Saltos="1,2" FilSociNume="True" MuestraDesc="True" FilDocuNume="True"
																					FilTipo="S"></UC1:PROD></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="20" background="imagenes/formfdofields.jpg"></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg" noWrap align="right">
																				<asp:label id="lblReceFil" runat="server" cssclass="titulo">Hembra Receptora:&nbsp;</asp:label></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg"><TABLE border="0" cellSpacing="0" cellPadding="0">
																					<TR>
																						<TD>
																							<cc1:combobox id="cmbReceRazaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="260px"
																								MostrarBotones="False" AceptaNull="False" Height="20px" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																						<TD width="4"></TD>
																						<TD>
																							<CC1:TEXTBOXTAB id="txtReceNumeFil" runat="server" cssclass="cuadrotexto" Width="100px" EnterPorTab="True"
																								MaxLength="10"></CC1:TEXTBOXTAB></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<asp:checkbox id="chkVisto" Runat="server" CssClass="titulo" Text="Incluir Registros Vistos y/o Aprobados"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD height="20" background="imagenes/formfdofields.jpg"></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																			<TD height="20" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 18.15%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 88%" height="2" background="imagenes/formdivfin.jpg" width="8"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD height="10"></TD>
									<TD vAlign="top" align="right" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="left" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="99%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnUpdateCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton1" runat="server" CausesValidation="false" CommandName="Update">
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="tede_id" ReadOnly="True" HeaderText="Id."></asp:BoundColumn>
												<asp:BoundColumn DataField="tede_fecha" HeaderText="Fecha Denuncia" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="tede_recu_fecha" HeaderText="Fecha Recup." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												
												<asp:TemplateColumn HeaderStyle-Width="0%">
													<ItemTemplate>
														<div><%#MuestraCentroImplante(DataBinder.Eval(Container, "DataItem.tede_imcr_id"))%></div>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="tede_nume" HeaderText="Nro."></asp:BoundColumn>
												<asp:BoundColumn DataField="_criador" HeaderText="Raza Hembra Receptora/Propietario"></asp:BoundColumn>
												<asp:BoundColumn DataField="_madre" HeaderText="Hembra Donante"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
											BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
											ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Denuncia"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Denuncia </asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" CausesValidation="False" Height="21px"> Implantes </asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderStyle="Solid" BorderWidth="1px"
												Height="100px" Visible="False">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 148px; HEIGHT: 12px" vAlign="middle" align="right">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha Denuncia:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 12px" colSpan="2">
																			<asp:Label id="txtFecha" runat="server" cssclass="titulo"></asp:Label>&nbsp;&nbsp; 
																			&nbsp;
																			<asp:Label id="lblNro" runat="server" cssclass="titulo">Nro.Denuncia:&nbsp;</asp:Label>
																			<asp:Label id="txtNro" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" noWrap align="right">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Propietario (Cliente):&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<UC1:CLIE id="usrClie" runat="server" AceptaNull="false" AutoPostBack="False" Tabla="Clientes"
																				Saltos="1,2" FilSociNume="True" MuestraDesc="False" FilDocuNume="True" FilTipo="S"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" noWrap align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Propietario (Raza/Criador):&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<UC1:CLIE id="usrCria" runat="server" AceptaNull="true" Ancho="780" Tabla="Criadores" Saltos="1,1,1"
																				MuestraDesc="False" FilDocuNume="True" Obligatorio="false" Alto="560" FilClie="True" FilCUIT="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">
																			<asp:Label id="lblCentro" runat="server" cssclass="titulo">Centro de Implante:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<cc1:combobox id="cmbCentro" autopostback=true class="combo" runat="server" cssclass="cuadrotexto" Width="460px"
																				MostrarBotones="False" Height="20px" NomOper="Centro" filtra="true" Obligatorio="false" onchange="javascript:mCargarVeterinarios(this);"></cc1:combobox></TD>
																	</tr>		
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px; HEIGHT: 1px" vAlign="middle" align="right">
																			<asp:Label id="lblVete" runat="server" cssclass="titulo">Veterinario:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 1px" colSpan="2">
																			<cc1:combobox id="cmbVete" class="combo" runat="server" Width="300px" AceptaNull="true" Obligatorio="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">
																			<asp:Label id="lblDona" runat="server" cssclass="titulo">Hembra Donante:&nbsp;</asp:Label></TD>
																		<TD vAlign="middle" colSpan="2">
																			<UC1:PROD id="usrDona" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
																				Tabla="productos" Saltos="1,2" FilSociNume="True" MuestraDesc="True" FilDocuNume="True"
																				FilTipo="S"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">
																			<asp:Label id="lblPadre1" runat="server" cssclass="titulo">Padre Sirviente 1:&nbsp;</asp:Label></TD>
																		<TD vAlign="middle" colSpan="2">
																			<UC1:PROD id="usrPadre1" runat="server" AceptaNull="false" AutoPostBack="True" Ancho="800"
																				Tabla="productos" Saltos="1,2" FilSociNume="True" MuestraDesc="True" FilDocuNume="True"
																				FilTipo="S"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">
																			<asp:Label id="lblPadre2" runat="server" cssclass="titulo">Padre Sirviente 2:&nbsp;</asp:Label></TD>
																		<TD vAlign="middle" colSpan="2">
																			<UC1:PROD id="usrPadre2" runat="server" AceptaNull="false" AutoPostBack="True" Ancho="800"
																				Tabla="productos" Saltos="1,2" FilSociNume="True" MuestraDesc="True" FilDocuNume="True"
																				FilTipo="S"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">
																			<asp:Label id="lblServ" runat="server" cssclass="titulo">Servicio:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<cc1:DateBox id="txtServFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" height="26" vAlign="middle" align="right"></TD>
																		<TD height="26" colSpan="2">
																			<asp:Label id="lblTituRecu" runat="server" cssclass="titulo">Recuperaci�n Embrionaria</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">
																			<asp:Label id="lblFechaRecu" runat="server" cssclass="titulo">Fecha:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<cc1:DateBox id="txtFechaRecu" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																			<asp:Label id="lblDiasGest" runat="server" cssclass="titulo">D�as de Gestaci�n:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtDiasGest" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="9999999999999"
																				esdecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" noWrap align="right">
																			<asp:Label id="lblCongEmbr" runat="server" cssclass="titulo">Embriones Congelados:</asp:Label></TD>
																		<TD colSpan="2" noWrap>
																			<cc1:numberbox id="txtCongEmbr" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="9999999999999"
																				esdecimal="False" onchange="javascript:mSumarTotales();"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblFresEmbr" runat="server" cssclass="titulo">Embriones Frescos:</asp:Label>
																			<cc1:numberbox id="txtFresEmbr" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="9999999999999"
																				esdecimal="False" onchange="javascript:mSumarTotales();"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblTotal" runat="server" cssclass="titulo">Total:</asp:Label>
																			<cc1:numberbox id="txtTotal" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="9999999999999"
																				esdecimal="False" ReadOnly="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" height="26" vAlign="middle" align="right"></TD>
																		<TD height="26" colSpan="2">
																			<asp:Label id="lblImplTitu1" runat="server" cssclass="titulo">Implante de Embriones</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">																			</TD>
																		<TD colSpan="2" noWrap>
																			<asp:Label id="lblImplFres" runat="server" cssclass="titulo">Embriones Frescos:</asp:Label>
																			<CC1:TEXTBOXTAB id="txtImplFres" runat="server" cssclass="cuadrotexto" Width="60px" EnterPorTab="True"
																				MaxLength="20"></CC1:TEXTBOXTAB>&nbsp;
																			<asp:Label id="lblImplCong" runat="server" cssclass="titulo">Embriones Congelados:</asp:Label>
																			<cc1:numberbox id="txtImplCong" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="9999999999999"
																				esdecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 148px" vAlign="middle" align="right">
																			<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:&nbsp;</asp:Label></TD>
																		<TD colSpan="2" noWrap>
																			<asp:Label id="txtEsta" runat="server" cssclass="titulo"></asp:Label>&nbsp;
																			<asp:Button id="btnErr" runat="server" cssclass="boton" Width="60px" Text="Errores"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:datagrid id="grdDeta" runat="server" BorderWidth="1px" BorderStyle="None" width="98%" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdDeta_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																				HorizontalAlign="Center" AllowPaging="True" Visible="False" OnEditCommand="mEditarDatosDeta">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="tedd_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="tedd_carv" HeaderText="Caravana"></asp:BoundColumn>
																					<asp:BoundColumn DataField="tedd_tatu" HeaderText="Tatuaje"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_raza" HeaderText="Raza"></asp:BoundColumn>
																					<asp:BoundColumn DataField="tedd_raza_cruza" HeaderText="Cruzas"></asp:BoundColumn>
																					<asp:BoundColumn DataField="tedd_naci_fecha" HeaderText="F.Nacim." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_tipo" HeaderText="Tipos de Receptoras"></asp:BoundColumn>
																					<asp:BoundColumn DataField="tedd_obse" HeaderText="Observaciones"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 226px" width="226" align="right">
																			<asp:Label id="lblCara" runat="server" cssclass="titulo">Caravana:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB onblur="javascript:if(document.all('btnAltaDeta')!=null &amp;&amp; !document.all('btnAltaDeta').disabled) document.all('btnAltaDeta').focus();"
																				id="txtCara" runat="server" cssclass="cuadrotexto" Width="214px" EnterPorTab="True" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 226px" width="226" align="right">
																			<asp:Label id="lblTatu" runat="server" cssclass="titulo">Tatuaje:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtTatu" runat="server" cssclass="cuadrotexto" Width="214px" EnterPorTab="True"
																				MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 226px" width="226" align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="180px" MostrarBotones="False"
																				AceptaNull="False" Height="20px" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																	</TR>
																<!--	<TR>
																		<TD style="WIDTH: 226px" width="226" align="right">
																			<asp:Label id="lblCruza" runat="server" cssclass="titulo">Cruzas:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:NUMBERBOX id="txtCruza" runat="server" cssclass="cuadrotexto" Width="138px" EnterPorTab="True"
																			esdecimal="False"	MaxValor="99999"></CC1:NUMBERBOX></TD>
																	</TR>-->
																	<TR>
																		<TD style="WIDTH: 226px" width="226" align="right">
																			<asp:Label id="lblReti" runat="server" cssclass="titulo">Tipos de Receptoras:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox id="cmbReti" class="combo" AutoPostBack="False" runat="server" Width="265px" AceptaNull="true"></cc1:combobox></TD>
																	</TR>
																	<tr>
																	
																	<TD style="WIDTH: 226px" width="226" align="right">
																			<asp:Label id="lblImplFecha" runat="server" cssclass="titulo">Fecha Implantaci�n:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtImplFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox></td>
																	
																	</tr>
																	<TR>
																		<TD style="WIDTH: 226px" width="226" align="right">
																			<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Nacimiento:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 226px" vAlign="top" width="226" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="340px" Height="40px" EnterPorTab="True"
																				MaxLength="20" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 226px" height="16" width="226" align="right"></TD>
																		<TD height="16"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="100px" Text="Agregar Impl."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="100px" Text="Eliminar Impl."
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="100px" Text="Modificar Impl."
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="100px" Text="Limpiar Impl."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnEspe" runat="server"></asp:textbox><asp:textbox id="hdnItem" runat="server"></asp:textbox><asp:textbox id="hdnDetaCarga" runat="server"></asp:textbox></DIV>
			<DIV style="DISPLAY: none"><asp:textbox id="hdnRaza" runat="server"></asp:textbox><asp:textbox id="hdnMsgError" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Denuncias de Transferencia y Recuperacion Embrionaria');
		usrCria_cmbRazaCria_onchange();
		if (document.all('txtTotal')!=null)
			mSumarTotales();
		if (document.all('hdnMsgError').value != '')
		{
			//alert(document.all('hdnMsgError').value);
			document.all('hdnMsgError').value = '';
		}
		</SCRIPT>
	</BODY>
</HTML>
