Namespace SRA

Partial Class carreras
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents Panedfdf As System.Web.UI.WebControls.Panel
   Protected WithEvents Peri As NixorControls.ComboBox
   Protected WithEvents periodo As NixorControls.TextBoxTab
   Protected WithEvents panDire As System.Web.UI.WebControls.Panel
   Protected WithEvents Chkintensivo As System.Web.UI.WebControls.CheckBox

    ' Protected WithEvents btnAgre As NixorControls.BotonImagen

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "carreras"
   Private mstrCarrEqui As String = "mate_equiva"
   Private mstrCarrCorre As String = "mate_correl"
   Private mstrCarrMate As String = "carreras_materias"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String
   Private mintinsti As Int32
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar(grdDato, False)
            tabLinks.Visible = False
            panBotones.Visible = False
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "carrera_tipos", cmbTipo, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPerTipo, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeriCarre, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "certificados_tipos", cmbAsisCer, "N")
      clsWeb.gCargarRefeCmb(mstrConn, "certificados_tipos", cmbAprCer, "N")
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "S", "@esti_id = 10")
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaEqui.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaCorre.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaCarre.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Profesores, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrCarrerasLong As Object

      lstrCarrerasLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrCarrerasLong, "carr_desc")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintinsti = Convert.ToInt32(Request.QueryString("fkvalor"))
      lblInstituto.Text = Request.QueryString("fkvalor")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(grdDato, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla

         Case mstrCarrEqui
            btnBajaEqui.Enabled = Not (pbooAlta)
            btnModiEqui.Enabled = Not (pbooAlta)
            btnAltaEqui.Enabled = pbooAlta

         Case mstrCarrCorre
            btnBajaCorre.Enabled = Not (pbooAlta)
            btnModiCorre.Enabled = Not (pbooAlta)
            btnAltaCorre.Enabled = pbooAlta

         Case mstrCarrMate
            btnBajaCarre.Enabled = Not (pbooAlta)
            btnModiCarre.Enabled = Not (pbooAlta)
            btnAltaCarre.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      grdEqui.CurrentPageIndex = 0

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtDesc.Valor = .Item("carr_desc")
            txtAbre.Valor = .Item("carr_abre")
            txtObse.Valor = .Item("carr_obse")
            txtDura.Valor = .Item("carr_dura")
            cmbEsta.Valor = .Item("carr_esta_id")
            cmbTipo.Valor = .Item("carr_cati_id")
            cmbPerTipo.Valor = .Item("carr_peti_id")
                    cmbAsisCer.Valor = IIf(.Item("carr_asis_ceti_id").ToString.Length > 0, .Item("carr_asis_ceti_id"), String.Empty)
                    cmbAprCer.Valor = IIf(.Item("carr_apro_ceti_id").ToString.Length > 0, .Item("carr_apro_ceti_id"), String.Empty)
            If Not .IsNull("carr_baja_fecha") Then
               btnList.Visible = False
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("carr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               btnList.Visible = True
               lblBaja.Text = ""
                End If
                ChkPresencial.Checked = .Item("carr_pres")
            End With
         mSetearEditor("", False)
         mSetearEditor(mstrCarrEqui, True)
         mSetearEditor(mstrCarrCorre, True)
         mSetearEditor(mstrCarrMate, True)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub
   Public Sub mEditarDatosEqui(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrEqui As DataRow

         hdnEquiId.Text = E.Item.Cells(1).Text
         ldrEqui = mdsDatos.Tables(mstrCarrEqui).Select("maeq_id=" & hdnEquiId.Text)(0)

         With ldrEqui
            cmbmateE.Valor = .Item("maeq_mate_id")
            cmbMateEqui.Valor = .Item("maeq_equi_mate_id") '393
            txtFechaDesdeEqui.Fecha = .Item("maeq_desde_fecha")
            txtFechaHastaEqui.Fecha = .Item("maeq_hasta_fecha")
         End With

         mSetearEditor(mstrCarrEqui, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosCorre(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrCorre As DataRow

         hdnCorreId.Text = E.Item.Cells(1).Text
         ldrCorre = mdsDatos.Tables(mstrCarrCorre).Select("maco_id=" & hdnCorreId.Text)(0)

         With ldrCorre
            cmbmate.Valor = .Item("maco_mate_id")
            cmbMateCorr.Valor = .Item("maco_corr_mate_id")
            txtFechaDesde.Fecha = .Item("maco_desde_fecha")
            txtFechaHasta.Fecha = .Item("maco_hasta_fecha")

         End With

         mSetearEditor(mstrCarrCorre, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosCarre(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrCarre As DataRow

         hdnCarreId.Text = E.Item.Cells(1).Text
         ldrCarre = mdsDatos.Tables(mstrCarrMate).Select("cama_id=" & hdnCarreId.Text)(0)

         With ldrCarre
            cmbmateC.Valor = .Item("cama_mate_id")
            cmbPeriCarre.Valor = .Item("cama_peti_id")
            txtPerioCarre.Valor = .Item("cama_perio")
            chkObligatorio.Checked = clsWeb.gFormatCheck(.Item("cama_obli"), "True")
         End With

         mSetearEditor(mstrCarrMate, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mLimpiar()
      btnList.Visible = False
      hdnId.Text = ""
      lblBaja.Text = ""
      txtDesc.Text = ""
      txtAbre.Text = ""
      txtObse.Text = ""
      txtDura.Text = ""
      cmbEsta.Limpiar()
      cmbTipo.Limpiar()
      cmbPerTipo.Limpiar()
      cmbAsisCer.Limpiar()
      cmbAprCer.Limpiar()

        'mLimpiarCorre()
      mLimpiarEqui()
      mLimpiarCarre()
      grdDato.CurrentPageIndex = 0
      grdCarre.CurrentPageIndex = 0
      grdCorre.CurrentPageIndex = 0
      grdEqui.CurrentPageIndex = 0

      mCrearDataSet("")

      lblTitu.Text = ""

        mSetearEditor("", True)

        ChkPresencial.Checked = True
   End Sub
   Private Sub mLimpiarEqui()
      hdnEquiId.Text = ""
      txtFechaDesdeEqui.Text = ""
      txtFechaHastaEqui.Text = ""
      cmbmateE.Limpiar()
      cmbMateEqui.Limpiar()

      mSetearEditor(mstrCarrEqui, True)
   End Sub
   Private Sub mLimpiarCorre()
      hdnCorreId.Text = ""

      cmbmate.Limpiar()
      cmbMateCorr.Limpiar()
      txtFechaHasta.Text = ""
      txtFechaDesde.Text = ""


      mSetearEditor(mstrCarrCorre, True)
   End Sub
   Private Sub mLimpiarCarre()
      hdnCarreId.Text = ""
      chkObligatorio.Checked = True
      txtPerioCarre.Text = ""
      cmbmateC.Limpiar()
      cmbPeriCarre.Limpiar()

      mSetearEditor(mstrCarrMate, True)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      grdDato.Visible = Not panDato.Visible
      btnAgre.Visible = Not (panDato.Visible)

      lnkCabecera.Font.Bold = True
      lnkCorre.Font.Bold = False
      lnkCarre.Font.Bold = False
      lnkEqui.Font.Bold = False

      panDato.Visible = pbooVisi

      panCabecera.Visible = True
      panCorre.Visible = False
      panEqui.Visible = False
      panCarre.Visible = False

      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkCorre.Font.Bold = False
   End Sub
   Private Sub mCargarComboConDataSet(ByVal cmb As NixorControls.ComboBox, ByVal dataTable As Data.DataTable, ByVal strId As String, ByVal strDesc As String)

      cmb.DataSource = dataTable
      cmb.DataValueField = strId
      cmb.DataTextField = strDesc
      cmb.DataBind()
      cmb.Items.Insert(0, "(Seleccione)")
      cmb.Items(0).Value = ""
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjCarrera As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCarrera.Alta()
         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjCarrera As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCarrera.Modi()
         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar(grdDato, True)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mActualizarEqui()
      Try
         mGuardarDatosEqui()

         mLimpiarEqui()
         grdEqui.DataSource = mdsDatos.Tables(mstrCarrEqui)
         grdEqui.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mActualizarCarre()
      Try
         mGuardarDatosCarre()

         mLimpiarCarre()
         grdCarre.DataSource = mdsDatos.Tables(mstrCarrMate)
         grdCarre.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mActualizarCorre()
      Try
         mGuardarDatosCorre()

            mLimpiarCorre()
            grdCorre.DataSource = mdsDatos.Tables(mstrCarrCorre)
            grdCorre.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mActualizarRelaciones(ByVal idMate As String)
      mActualizarRelacion(grdCorre, mstrCarrCorre, "maco_mate_id = " + idMate + " or maco_corr_mate_id = " + idMate)
      mActualizarRelacion(grdEqui, mstrCarrEqui, "maeq_mate_id = " + idMate + " or maeq_equi_mate_id = " + idMate)
   End Sub
   Private Sub mActualizarRelacion(ByVal pgrgGrilla As System.Web.UI.WebControls.DataGrid, ByVal pstrTable As String, ByVal pfiltro As String)
      Dim row As Data.DataRow
      Dim rows As Collections.IEnumerable = mdsDatos.Tables(pstrTable).Select(pfiltro)
      For Each row In rows
         row.Delete()
      Next
      pgrgGrilla.DataSource = mdsDatos.Tables(pstrTable)
      pgrgGrilla.DataBind()
   End Sub
        Private Function mGuardarDatos() As DataSet
            Dim lintCpo As Integer

            If txtDesc.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la descripci�n.")
            End If
            If txtAbre.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la abreviatura.")
            End If
            If cmbPerTipo.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el tipo de periodo.")
            End If
            If txtDura.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la duracion.")
            End If
            If cmbTipo.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el tipo de carrera.")
            End If
            If (cmbAsisCer.Valor.ToString = "" And cmbAprCer.Valor.ToString = "") Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un certificado.")
            End If
            If cmbEsta.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Estado.")
            End If

            With mdsDatos.Tables(0).Rows(0)
                .Item("carr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("carr_desc") = txtDesc.Valor
                .Item("carr_abre") = txtAbre.Valor
                .Item("carr_Obse") = txtObse.Valor
                .Item("carr_dura") = txtDura.Valor
                .Item("carr_esta_id") = IIf(cmbEsta.Valor.Length > 0, cmbEsta.Valor, DBNull.Value)
                .Item("carr_cati_id") = IIf(cmbTipo.Valor.Length > 0, cmbTipo.Valor, DBNull.Value)
                .Item("carr_peti_id") = IIf(cmbPerTipo.Valor.Length > 0, cmbPerTipo.Valor, DBNull.Value)
                .Item("carr_asis_ceti_id") = IIf(cmbAsisCer.Valor.Length > 0, cmbAsisCer.Valor, DBNull.Value)
                .Item("carr_apro_ceti_id") = IIf(cmbAprCer.Valor.Length > 0, cmbAprCer.Valor, DBNull.Value)
                .Item("carr_inse_id") = mintinsti
                .Item("carr_baja_fecha") = DBNull.Value
                .Item("carr_pres") = ChkPresencial.Checked
            End With
            Return mdsDatos
        End Function
   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrCarrEqui
      mdsDatos.Tables(2).TableName = mstrCarrCorre
      mdsDatos.Tables(3).TableName = mstrCarrMate


      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      grdEqui.DataSource = mdsDatos.Tables(mstrCarrEqui)
      grdEqui.DataBind()


      grdCorre.DataSource = mdsDatos.Tables(mstrCarrCorre)
      grdCorre.DataBind()

      grdCarre.DataSource = mdsDatos.Tables(mstrCarrMate)
      grdCarre.DataBind()

      Session(mstrTabla) = mdsDatos

   End Sub
#End Region

#Region "Detalle"
   Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal ejecuta As Boolean)
      Try
         mstrCmd = "exec " + mstrTabla + "_consul " + "@carr_inse_id = " + mintinsti.ToString()
         mstrCmd = mstrCmd + ",@formato = '1'"
         mstrCmd = mstrCmd + ",@descFilt = '" + txtDescFil.Text + "'"
         mstrCmd = mstrCmd + ",@abreFilt = '" + txtAbreFil.Text + "'"
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mLimpiaMate()
      hdnCarreId.Text = ""
      txtPerioCarre.Text = ""
      chkObligatorio.Checked = True
      cmbmateC.Limpiar()
      cmbPeriCarre.Limpiar()
   End Sub
   Private Sub mGuardarDatosEqui()

      Dim ldrDatos As DataRow
      If cmbmateE.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia.")
      End If
      If cmbMateEqui.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia equivalente.")
      End If
      If cmbmateE.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia.")
      End If
      If cmbMateEqui.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia equivalente.")
      End If

      If (cmbMateEqui.Valor = cmbmateE.Valor) Then
         Throw New AccesoBD.clsErrNeg("Una materia no puede ser equivalente de si misma.")
      End If

      If Not (txtFechaDesdeEqui.Fecha Is DBNull.Value) And Not (txtFechaHastaEqui.Fecha Is DBNull.Value) Then
         If txtFechaDesdeEqui.Fecha > txtFechaHastaEqui.Fecha Then
            Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
         End If
      End If

      If hdnEquiId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrCarrEqui).NewRow
         ldrDatos.Item("maeq_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrCarrEqui), "maeq_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrCarrEqui).Select("maeq_id=" & hdnEquiId.Text)(0)
      End If

      With ldrDatos
         .Item("maeq_equi_mate_id") = cmbMateEqui.Valor
         .Item("maeq_mate_id") = cmbmateE.Valor
                .Item("maeq_desde_fecha") = IIf(txtFechaDesdeEqui.Fecha.Length > 0, txtFechaDesdeEqui.Fecha, DBNull.Value)
                .Item("maeq_hasta_fecha") = IIf(txtFechaHastaEqui.Fecha.Length > 0, txtFechaHastaEqui.Fecha, DBNull.Value)
                .Item("_mate_descComp") = IIf(cmbmateE.SelectedItem.Text.Length > 0, cmbmateE.SelectedItem.Text, DBNull.Value)
                .Item("_equi_descComp") = IIf(cmbMateEqui.SelectedItem.Text.Length > 0, cmbMateEqui.SelectedItem.Text, DBNull.Value)
         .Item("_estado") = "Activo"
      End With

      If (mEstaEnElDataSet(mdsDatos.Tables(mstrCarrEqui), ldrDatos, "maeq_equi_mate_id", "maeq_mate_id", "maeq_id")) Then
         Throw New AccesoBD.clsErrNeg("La Carrera ya contiene la equivalencia seleccionada.")
      Else
         If (hdnEquiId.Text = "") Then
            mdsDatos.Tables(mstrCarrEqui).Rows.Add(ldrDatos)
         End If
      End If
   End Sub
   Private Sub mGuardarDatosCorre()

      Dim ldrCorre As DataRow

      If cmbmate.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia.")
      End If
      If cmbMateCorr.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia correlativa.")
      End If

      If cmbmate.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia.")
      End If
      If cmbMateCorr.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia correlativa.")
      End If
      If (cmbMateCorr.Valor = cmbmate.Valor) Then
         Throw New AccesoBD.clsErrNeg("Una matrie no puede ser correlativa de si misma.")
      End If

      If Not (txtFechaDesde.Fecha Is DBNull.Value) And Not (txtFechaHasta.Fecha Is DBNull.Value) Then
         If txtFechaDesde.Fecha > txtFechaHasta.Fecha Then
            Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
         End If
      End If

      If hdnCorreId.Text = "" Then
         ldrCorre = mdsDatos.Tables(mstrCarrCorre).NewRow
         ldrCorre.Item("maco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrCarrCorre), "maco_id")
      Else
         ldrCorre = mdsDatos.Tables(mstrCarrCorre).Select("maco_id=" & hdnCorreId.Text)(0)
      End If

      With ldrCorre
         .Item("maco_mate_id") = cmbmate.Valor
         .Item("maco_corr_mate_id") = cmbMateCorr.Valor
                .Item("maco_desde_fecha") = IIf(txtFechaDesde.Fecha.Length > 0, txtFechaDesde.Fecha, DBNull.Value)
                .Item("maco_hasta_fecha") = IIf(txtFechaHasta.Fecha.Length > 0, txtFechaHasta.Fecha, DBNull.Value)
         .Item("_mate_descComp") = cmbmate.SelectedItem.Text
         .Item("_corr_descComp") = cmbMateCorr.SelectedItem.Text
            .Item("_estado") = "Activo"
            .Item("maco_baja_fecha") = DBNull.Value
      End With

      If (mEstaEnElDataSet(mdsDatos.Tables(mstrCarrCorre), ldrCorre, "maco_corr_mate_id", "maco_mate_id", "maco_id")) Then
         Throw New AccesoBD.clsErrNeg("La Carrera ya tiene la correlativa seleccionada.")
      Else
         If (hdnCorreId.Text = "") Then
            mdsDatos.Tables(mstrCarrCorre).Rows.Add(ldrCorre)
         End If
      End If
   End Sub
   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCorre As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
      Dim filtro As String
      filtro = campo1 + " = " + ldrCorre.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrCorre.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrCorre.Item(campoId).ToString()
      Return (table.Select(filtro).GetLength(0) > 0)
   End Function
   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCorre As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
      Return mEstaEnElDataSet(table, ldrCorre, campo1, campo1, campoId)
   End Function
   Private Sub mGuardarDatosCarre()

      Dim ldrCarre As DataRow

      If cmbmateC.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la Materia.")
      End If
      If cmbPeriCarre.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Tipo de Per�odo.")
      End If
      If txtPerioCarre.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Per�odo.")
      End If

      If hdnCarreId.Text = "" Then
         ldrCarre = mdsDatos.Tables(mstrCarrMate).NewRow
         ldrCarre.Item("cama_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrCarrMate), "cama_id")
      Else
         ldrCarre = mdsDatos.Tables(mstrCarrMate).Select("cama_id=" & hdnCarreId.Text)(0)
      End If

      With ldrCarre
         .Item("cama_mate_id") = cmbmateC.Valor
         .Item("cama_peti_id") = cmbPeriCarre.Valor
         .Item("cama_perio") = txtPerioCarre.Valor
         .Item("cama_obli") = chkObligatorio.Checked
         .Item("_mate_descComp") = cmbmateC.SelectedItem.Text
         .Item("_peti_desc") = cmbPeriCarre.SelectedItem.Text
         .Item("_obli") = IIf(chkObligatorio.Checked, "Si", "No")
         .Item("_estado") = "Activo"
      End With

      If (mEstaEnElDataSet(mdsDatos.Tables(mstrCarrMate), ldrCarre, "cama_mate_id", "cama_id")) Then
         Throw New AccesoBD.clsErrNeg("La materia ya pertenece a la carrera seleccionada.")
      Else
         If (hdnCarreId.Text = "") Then
            mdsDatos.Tables(mstrCarrMate).Rows.Add(ldrCarre)
         End If
      End If
   End Sub
   Private Sub mSetearEditorEqui(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panEqui.Visible = False
      panCabecera.Visible = False
      panCorre.Visible = False
      panCarre.Visible = False


      lnkCabecera.Font.Bold = False
      lnkCorre.Font.Bold = False
      lnkEqui.Font.Bold = False
      lnkCarre.Font.Bold = False
      Dim val As String
      Dim filtroTotal As String
      Dim filtro As String = "@mate_inse_id = " + mintinsti.ToString()
      If (hdnId.Text <> "") Then
         val = hdnId.Text
      Else
         val = "null"
      End If
      filtroTotal = filtro + ",@carre_id = " + val

      Select Case origen
         Case 1
            mLimpiarCorre()
            mLimpiarEqui()
            mLimpiarCarre()
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos de la Carrera "
         Case 2
            mCargarComboConDataSet(cmbmate, mdsDatos.Tables(mstrCarrMate), "cama_mate_id", "_mate_descComp")
            mCargarComboConDataSet(cmbMateCorr, mdsDatos.Tables(mstrCarrMate), "cama_mate_id", "_mate_descComp")
            mLimpiarEqui()
            mLimpiarCarre()
            panCorre.Visible = True
            lnkCarre.Font.Bold = True
            lblTitu.Text = "Correlativas de la Carrera: " & txtDesc.Text '& IIf(txtDesc.Text <> "", ", " & txtDesc.Text, "")
            grdCorre.Visible = True
         Case 3
            mCargarComboConDataSet(cmbmateE, mdsDatos.Tables(mstrCarrMate), "cama_mate_id", "_mate_descComp")
            clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMateEqui, "S", filtro)
            mLimpiarCorre()
            mLimpiarCarre()
            panEqui.Visible = True
            lnkEqui.Font.Bold = True
            lblTitu.Text = "Equivalentes de la Carrera: " & txtDesc.Text '& IIf(txtDesc.Text <> "", ", " & txtDesc.Text, "")
            grdEqui.Visible = True
         Case 4
            clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbmateC, "S", filtro)
            mLimpiarEqui()
            mLimpiarCorre()
            panCarre.Visible = True
            lnkCorre.Font.Bold = True
            lblTitu.Text = "Materias de la Carrera: " & txtDesc.Text '& IIf(txtDesc.Text <> "", ", " & txtDesc.Text, "")
            grdCarre.Visible = True

      End Select
   End Sub

   Public Sub grdCorre_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCorre.EditItemIndex = -1
         If (grdCorre.CurrentPageIndex < 0 Or grdCorre.CurrentPageIndex >= grdCorre.PageCount) Then
            grdCorre.CurrentPageIndex = 0
         Else
            grdCorre.CurrentPageIndex = E.NewPageIndex
         End If
         grdCorre.DataSource = mdsDatos.Tables(mstrCarrCorre)
         grdCorre.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdEqui_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdEqui.EditItemIndex = -1
         If (grdEqui.CurrentPageIndex < 0 Or grdEqui.CurrentPageIndex >= grdEqui.PageCount) Then
            grdEqui.CurrentPageIndex = 0
         Else
            grdEqui.CurrentPageIndex = E.NewPageIndex
         End If
         grdEqui.DataSource = mdsDatos.Tables(mstrCarrEqui)
         grdEqui.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdCarre_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCarre.EditItemIndex = -1
         If (grdCarre.CurrentPageIndex < 0 Or grdCarre.CurrentPageIndex >= grdCarre.PageCount) Then
            grdCarre.CurrentPageIndex = 0
         Else
            grdCarre.CurrentPageIndex = E.NewPageIndex
         End If
         grdCarre.DataSource = mdsDatos.Tables(mstrCarrMate)
         grdCarre.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCorre.Click
      mShowTabs(4)
   End Sub
   Private Sub lnkEqui_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEqui.Click
      mShowTabs(3)
   End Sub
   Private Sub lnkCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCarre.Click
      mShowTabs(2)
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
            Dim lstrRptName As String = "Carreras"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&fkvalor=" + Request.QueryString("fkvalor")
            lstrRpt += "&carr_id=" + hdnId.Text

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar(grdDato, False)
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnAltaEqui_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaEqui.Click
      mActualizarEqui()
   End Sub
   Private Sub btnBajaEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaEqui.Click
      Try
         mdsDatos.Tables(mstrCarrEqui).Select("maeq_id=" & hdnEquiId.Text)
         With mdsDatos.Tables(mstrCarrEqui).Rows(0)
            .Item("maeq_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdEqui.DataSource = mdsDatos.Tables(mstrCarrEqui)
         grdEqui.DataBind()
         mLimpiarEqui()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpEqui.Click
      mLimpiarEqui()
   End Sub
   Private Sub btnModiEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiEqui.Click
      mActualizarEqui()
   End Sub

   Private Sub btnAltaCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaCarre.Click
      mActualizarCarre()
   End Sub
   Private Sub btnBajaCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaCarre.Click
      Try
         Dim idMate As String
         Dim row As Data.DataRow = mdsDatos.Tables(mstrCarrMate).Select("cama_id=" & hdnCarreId.Text)(0)
         If (row.RowState = DataRowState.Added) Then
            idMate = row.Item("cama_mate_id")
            mActualizarRelaciones(idMate)
         End If
         row.Delete()
         grdCarre.DataSource = mdsDatos.Tables(mstrCarrMate)
         grdCarre.DataBind()
         mLimpiarCarre()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnModiCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiCarre.Click
      mActualizarCarre()
   End Sub
   Private Sub btnLimpCarre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpCarre.Click
      mLimpiaMate()
   End Sub

   Private Sub btnAltaCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaCorre.Click
      mActualizarCorre()
   End Sub
   Private Sub btnBajaCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaCorre.Click
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrCarrCorre).Select("maco_id=" & hdnCorreId.Text)(0)
         row.Delete()
         grdCorre.DataSource = mdsDatos.Tables(mstrCarrCorre)
         grdCorre.DataBind()
         mLimpiarCorre()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnModiCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiCorre.Click
      mActualizarCorre()
   End Sub
   Private Sub btnLimpCorre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpCorre.Click
      mLimpiarCorre()
   End Sub
#End Region

End Class
End Namespace
