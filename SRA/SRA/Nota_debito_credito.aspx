<%@ Reference Page="~/Actividades.aspx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Nota_debito_credito" CodeFile="Nota_debito_credito.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Nota de D�bito/ Nota de Cr�dito</title>
		<meta content="False" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript"> 
		function mSetearConcepto(pConc)
		{
			var strRet = LeerCamposXML("conceptos", "@conc_id="+pConc.value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet!='')
			{ 
				var lstrCta = strRet.substring(0,1);
				document.all('hdnCta').value = lstrCta;
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCCosConcep').disabled = true;
					document.all('txtcmbCCosConcep').disabled = true;
					document.all('cmbCCosConcep').innerHTML = '';
					document.all('txtcmbCCosConcep').value = '';
				}
				else
				{
					document.all('cmbCCosConcep').disabled = false;
					document.all('txtcmbCCosConcep').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCCosConcep", "N");					
				}
			}
			mCalcularConceptos();			
		}

		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null);
				
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function mCargarClienteSocio()
		{
			var sFiltro = document.all("txtSocio").value;

 			if (sFiltro != '')
 			{
 				sFiltro = "@soci_nume=" + sFiltro + ",@baja=1";
				var vstrRet = LeerCamposXML("socios_datos", sFiltro, "clie_id").split("|");
				
				if(vstrRet[0]=="")
					alert("Socio inexistente");
				else
				{
					document.all["usrClie:txtCodi"].value = vstrRet[0]; //selecciona el cliente
					document.all["usrClie:txtCodi"].onchange();
				}
			}
		} 
		
		function mTipoComprobante()
		{
			if (document.all("cmbComprobante").value!="")
				{
				if (document.all("cmbComprobante").value==31)
					{
						document.all("lblTitulo").innerHTML= "Nota de Cr�dito";
					} 
					else
						{
						document.all("lblTitulo").innerHTML= "Nota de D�bito";
						}
					
				}
			else
				document.all("lblTitulo").innerHTML= "Nota de D�bito/Cr�dito";
		}

		function mCalcularImporteCuota()
		{
			if(document.all("txtCuotPorc").value!="")
			{   
				try
				{
				sFiltro = document.all("lblImpTotal").innerText + ";" + document.all("txtCuotPorc").value
				vsRet =EjecutarMetodoXML("Utiles.CalcularImporteCuota", sFiltro).split(";");
				document.all("txtCuotImporte").value =  vsRet[0]     
				}
				catch(e)
     				{
     					alert("Error al intentar calcular el importe de la cuota");
     				}
			}
			else
				document.all("txtCuotImporte").value = ""
		}
		
		function mCargarCliente()
		{
			var sFiltroC;
			var sFiltroR;
			var sFiltro;
			var cria;
			sFiltro = ""
			cria = "";
			document.all("hdnRazaId").value ="0";  
			document.all("hdnCriaNume").value ="0"; 
			try
			{  //para RRGG y LAB
			   //ahora lo ven todas las actividades (250407)
  				//if(document.all("cmbActi").value == "3" || document.all("cmbActi").value == "4")
				//{	  
					var sFiltroC = document.all("txtCriaNume").value;//nro criador
					var sFiltroR = document.all("cmbRaza").value;//raza
 					if (sFiltroC != '' & sFiltroR != '')
 					{
 						cria = "1";
 						sFiltro = "@cria_nume=" + sFiltroC + ", @raza_id=" + sFiltroR ;
						var vstrRet = LeerCamposXML("criadores_razas", sFiltro, "cria_clie_id,cria_id").split("|");
						sFiltro = vstrRet[0]; //selecciona el cliente
						document.all("hdnCria").value = vstrRet[1];
					}
					else
					{
						document.all("txtCriaNume").value = ""
						document.all("hdnCria").value ="";
						sFiltro="" 
						if (sFiltroC != '')
						alert ("Debe seleccionar la raza.")
					}
					//alert(document.all("usrClie:txtId").value)
					//si el cliente va fue seleccionado, verifica que coincida 
					if(document.all("usrClie:txtId").value!="")
					{
						if (sFiltro != '' || cria != '')
						{    
							if (document.all("usrClie:txtId").value != sFiltro)
							{	
							alert("El nro. de criador no coincide con el cliente")
							document.all("txtCriaNume").value = ""
							document.all("hdnCria").value ="";
							}
						}    
						else
						{	
							document.all("txtCriaNume").value = ""
							document.all("hdnCria").value ="";
						}    
					}
					else //si no fue ingresado lo ingresa
					{
						if (sFiltro !="")
							mSelClie(sFiltro)
						else
						{
							document.all("txtCriaNume").value = ""
							document.all("hdnCria").value ="";
					       
							if (sFiltroC != '' & sFiltroR != '')
 									alert("Raza/Criador inexistente");
						} 
					} 
				//}
			}
			catch(e)
    			{
    				// alert("Error al intentar seleccionar el cliente por Nro. de criador.");
				}    
		} 	
    
		function mSelClie(pClieId)
        {
			document.all("usrClie:txtCodi").value = pClieId;
			document.all("usrClie:txtCodi").onchange();
		}
	 		
		
		function mCargarComprobAsociados()
		{
			var sFiltro = document.all("cmbActi").value;
			var sFiltroClie = document.all("usrClie:txtId").value;
			if (sFiltro != '' & sFiltroClie != '')
			{  
				sFiltro = sFiltro + "," + sFiltroClie
				LoadComboXML("comprobantes_cargar", sFiltro, "cmbComprobanteAsoc", "N"); 
			}
			else
				document.all("cmbComprobanteAsoc").innerText = "";
		}
     
   		function mSetearCheck()
		{
			if (document.all["chkTPers"].checked == true)
			{
				today = new Date();
				var fechaActual;
				fechaActual =today.getDate();
				if ((fechaActual+"").length==1)
				fechaActual ="0" + fechaActual;

				if (((today.getMonth()+1)+"").length==1)
					fechaActual = fechaActual+"/0"+(today.getMonth()+1);
				else
					fechaActual = fechaActual+"/"+(today.getMonth()+1);
				
				fechaActual = fechaActual+"/"+today.getYear()
				document.all("txtFechaValor").value = fechaActual //fecha de ingreso
			}
			
			var activ = document.all["chkTPers"].checked == false

				ActivarFecha("txtFechaValor",activ);
				document.all["chkVtaTele"].checked = !document.all["chkTPers"].checked == true;
		}
	 		
		function mCargarExposiciones(pcargar)
		{
			if (document.all("cmbActi").value != '')
			{
				if(document.all("cmbActi").value == "7" )
				{
				if (document.all("cmbExpo").value == "" || pcargar !='')
					{
					document.all("cmbExpo").disabled = false;
					document.all("divExpo").style.display ="inline";
					var lstrExpoSele = document.all("cmbExpo").selectedIndex;
					LoadComboXML("exposiciones_cargar","","cmbExpo", "S"); 
					document.all("cmbExpo").selectedIndex = lstrExpoSele;
					if (document.all("cmbExpo").length > 1 && document.all("cmbExpo").selectedIndex <= 0)
						document.all("cmbExpo").selectedIndex = 1;
					} 
				}
				else
				{
					document.all("cmbExpo").innerText = ""
					document.all("divExpo").style.display ="none";
				}			
			}
		}
		
	 		
		function mCargarCriadores()
		{
			var sFiltro = document.all("cmbActi").value;
			if (sFiltro != '')
			{
				//RRGG o laboratorio debe mostrar raza/criador
				//lo ven todas las actividades (250407)
				//if(document.all("cmbActi").value == "3" || document.all("cmbActi").value == "4")
				//{
					//document.all("divCriador").style.display ="inline";
					if (document.all("cmbRaza").value == "")//para que no lo cargue el combo siempre que dibuja la pag. 
					{ 
					document.all("txtCriaNume").value = ""; 
					document.all("hdnCria").value ="";
					LoadComboXML("razas_cargar", "", "cmbRaza", "N"); 
					}
				//}
				//else
				//	document.all("divCriador").style.display ="none";
			}
		}
     
		function mSetearCombos(pstrOri)
		{
			try
			{
				if (pstrOri=='C') //criadores
				{
					if (document.all("cmbCriador").value!="")
						document.all("cmbRaza").selectedIndex=0;
				}
				else//raza
				{
					if (document.all("cmbRaza").value!="")
						document.all("cmbcriador").selectedIndex=0;
				}
			}
			catch(e)
				{
					alert("Error al intentar setear los combos de raza/criador");
				}
		}				

		function mValidarConceptos()
		{   
			var sRet; 
			sRet = ""
			if(document.all("cmbConcep").value=="")
			{ 
				sRet = "error"
				alert("Debe seleccionar un concepto.")
			}
			if(document.all("txtImporteConcep").value=="")
			{ 
				//document.all("txtImpoIvaConcep").value ="";
				//document.all("txtImpoNConcep").value ="";
				document.all("txtTasaIVAConcep").value ="";
				document.all("hdnTasaIVAConcep").value = "";
				document.all("hdnImpoIvaConcep").value = "";
				
				sRet = "error"
				alert("Debe ingresar el valor de concepto.")
			} 
			return sRet
		}     
      
		function mCalcularConceptos()
		{
			var valor= ""; //mValidarConceptos()
			if(document.all("cmbConcep").value=="" || document.all("txtImporteConcep").value=="")
				return;

			if(valor=="")
			{   
				var ldouTasaIVA;
				var ldouImporteiva;
				var ldouTasaST;
				var ldouImporteST;
					try
					{ 
						//'obtiene el valor de la tasa
						sFiltro = document.all("cmbConcep").value + ";" + document.all("hdnFechaIva").value + ";" + document.all("txtImporteConcep").value
						vsRet =EjecutarMetodoXML("Utiles.CalculoConcepto", sFiltro).split(";");
						// vsRet[0];//TASA IVA   vsRet[1];//IMPORTE DEL IVA  'vsRet[2];//IMPORTE DEL CONCEPTO SIN IVA
						document.all("hdnTasaIVAConcep").value = vsRet[0]
						document.all("hdnImpoIvaConcep").value = vsRet[1]
									      
						if(document.all("hdnClieDiscIVA").value == "True")
						{
							document.all("txtTasaIVAConcep").value = cent(vsRet[0]);
							document.all("txtImpoIvaConcep").value = cent(vsRet[1]);
						}
						
						//tasa sobretasa   //importe sobreatasa
						var lstrClieSobreTasa = LeerCamposXML("clientes_posic_iva", document.all("hdnClieId").value, "ivap_sobre");
						if (lstrClieSobreTasa == "1")
						{
							document.all("hdnTasaSobreTasaC").value = cent(vsRet[5]);
							document.all("hdnTotalSobreTasaC").value = cent(vsRet[4]);
  							if (document.all("txtTotalSobreTasaC")!=null)
			 					document.all("txtTotalSobreTasaC").value = cent(vsRet[4]);
							if (document.all("txtTasaSobreTasaC")!=null)
								document.all("txtTasaSobreTasaC").value =  cent(vsRet[5]);
						}
						else
						{
							document.all("hdnTasaSobreTasaC").value = "";
							document.all("hdnTotalSobreTasaC").value = "";
  							if (document.all("txtTotalSobreTasaC")!=null)
			 					document.all("txtTotalSobreTasaC").value = "";
							if (document.all("txtTasaSobreTasaC")!=null)
								document.all("txtTasaSobreTasaC").value =  "";
						}
					   
					}
					catch(e)
					{
						alert("Error al intentar calcular el concepto");
					}       
			}  
		}
			
		function mFiltro(pPasarPUnit)
		{
			//arma el string del filtro para calcular el valor del arancel
			var lstrArancelManual ="";
			if (pPasarPUnit=='S')
			lstrArancelManual = document.all("txtPUnitAran").value;
			sFiltro = document.all("cmbAran").value + ";" + document.all("txtCantAran").value + ";" + document.all("hdnActiId").value + ";" + document.all("hdnFechaIva").value + ";" + document.all("hdnCotDolar").value + ";" + document.all("hdnFormaPago").value + ";" + document.all("hdnListaPrecios").value  + ";" + lstrArancelManual;

			return sFiltro
		} 
  										
		function mCalcularImporteAranceles()
		{ 
			if(document.all("cmbaran").value=="")
				alert("Debe seleccionar el arancel.");
			else
			{  
				if(document.all("txtCantAran").value!="")
					{
						var sFiltro; 
						var ldouImporte = 0;
						var ldouTasaIVA= 0;
						var ldouImpoIVA= 0;
						var vsRet = 0;
						var lstrClieSobreTasa = "";
						
						try
						{ 
      						vsRet = EjecutarMetodoXML("Utiles.CalculoArancel", mFiltro('N')).split(";"); // Dario puse filtro en N para que quede igual que fact
							// vsRet[0]; //PRECIO UNITARIO
							// vsRet[1]; //IMPORTE APLICADA LA FORMULA 
							// vsRet[2];//TASA IVA
							// vsRet[3];//IMPORTE IVA
							// vsRet[4];//IMPORTE NETO 
							// vsRet[5];//IMPORTE UNITARIO CON IVA 
							// vsRet[6];//IMPORTE UNITARIO SOBRETASA 
							// vsRet[7];//TASA SOBRETASA		
							document.all("hdlImpoIvaAran").value= vsRet[3]
							document.all("hdnTasaIVAAran").value =vsRet[2]
							if (document.all("txtTotalSobreTasa1")!=null)
								document.all("txtTotalSobreTasa1").value = vsRet[6];
							if (document.all("txtTasaSobreTasa")!=null)
								document.all("txtTasaSobreTasa").value = vsRet[7];
							
							//guarda el importe sin iva  (indistintamente de la condicion IVA del cliente)
							//es necesario para que quede el cambio del valor del arancel, si el usuario lo modifico manualmente
							document.all("txtPUnitAranSIVA").value = cent(vsRet[0])
					     				
							if(document.all("hdnClieDiscIVA").value == "True")
							{
								//tasa iva   //importe iva
								document.all("txtImpoIvaAran").value = cent(vsRet[3]);
								document.all("txtTasaIVAAran").value =  cent(vsRet[2]);
								//tasa sobretasa   //importe sobreatasa
								lstrClieSobreTasa = LeerCamposXML("clientes_posic_iva", document.all("hdnClieId").value, "ivap_sobre");
								if (lstrClieSobreTasa == "1")
								{
									if (document.all("txtTotalSobreTasa1")!=null)
										document.all("txtTotalSobreTasa1").value = cent(vsRet[6]);
									if (document.all("txtTasaSobreTasa")!=null)
										document.all("txtTasaSobreTasa").value =  cent(vsRet[7]);
								}
								else
								{
  									if (document.all("txtTotalSobreTasa1")!=null)
			 							document.all("txtTotalSobreTasa1").value = "";
									if (document.all("txtTasaSobreTasa")!=null)
										document.all("txtTasaSobreTasa").value =  "";
								}
								// valor bruto		
								document.all("txtImpoAran").value = cent(vsRet[1]);
							}
							else// valor neto
		 							{
		 							document.all("txtImpoAran").value = cent(vsRet[4]);
		 							}
		 					if (document.all("txtTotalSobreTasa1")!=null)
								document.all("hdnTotalSobreTasa1").value = document.all("txtTotalSobreTasa1").value;
							if (document.all("txtTasaSobreTasa")!=null)
								document.all("hdnTasaSobreTasa").value =  document.all("txtTasaSobreTasa").value;																
						}	
						catch(e)
						{
							alert("Error al intentar calcular el arancel");
						}
					}
			}
		    
		}

		function cent(amount) {
			// returns el valor con 2 decimales
			return (amount == Math.floor(amount)) ? amount + '.00' : ( (amount*10 == Math.floor(amount*10)) ? amount + '0' : amount);
		}

		function mClienteGenerico()
		{
			var sRet; 
			sRet = ""
			if(document.all("hdnClieId").value!="")//usrClie:txtId
				sRet=EjecutarMetodoXML("Utiles.ClienteGenerico", document.all("hdnClieId").value);
			return sRet
		}
  
		function mRaza()
		{
			var sRet; 
			sRet = ""
			if(document.all("hdnRazaId").value !="")
				sRet=document.all("hdnRazaId").value;
			else
			{
				sRet ="0"
				if(document.all("hdnCriaNume").value !="")
					sRet = LeerCamposXML("criadores_razas", document.all("hdnCriaNume").value, "raza_id").split("|");
			}
			return sRet
		}
   
		function mDivClienteGenerico()
		{   
			if(mClienteGenerico()=="True")
	  			document.all("divClieGene").style.display ="inline"; 
			else
				document.all("divClieGene").style.display ="none";
		}  
		
		
		function usrClie_onchange()
		{
			if(document.all("usrClie:txtId").value!="")
			{
				mDivClienteGenerico()		
				
				var vstrRet = LeerCamposXML("clientes_soci", document.all("usrClie:txtId").value + ",1", "soci_id,soci_nume").split("|");
				document.all("hdnSocioId").value = "";
				document.all("txtSocio").value = "";
					
				if(vstrRet!="")
				{
					document.all("hdnSocioId").value = vstrRet[0];
					document.all("txtSocio").value = vstrRet[1];
				}
									
				vstrRet = LeerCamposXML("clientes_posic_iva", document.all("usrClie:txtId").value, "ivap_discri").split("|");
						 			
				document.all("hdnClieDiscIVA").value  = vstrRet[0]
					
				mSetearCuadros();
                					
				mTarjetas()				
				mEstadoSaldos()	            
				mEspeciales()
	                
				mCargar()
				mCargarCliente();
				document.all("cmbComprobanteAsoc").focus();
	                
			}
			else
			{
				document.all("hdnSocioId").value = "";
				document.all("txtSocio").value = "";
				document.all("cmbRaza").value= "";
				document.all("txtcmbRaza").value= "";
				document.all("txtCriaNume").value = ""
				document.all("hdnCria").value ="";
				
				if(document.all("tblEstadoSaldos")!=null)
				{
					document.all("divEstadoSaldos").style.display="none";
					document.all("divProformas").style.display="none";
				}
			}
		}
		function mSetearVentaTele()
		{      
			if (document.all("imgTarjTele")!=null && document.all("usrClie:txtId").value!='')
			{
				// icono venta telefonica	
				var lstrImg = document.all("imgTarjTele").src;
				var strRet = LeerCamposXML("tarjetas_clientes", "@tacl_clie_id="+document.all("usrClie:txtId").value, "tacl_vta_tele");
				if (strRet == "0")
					lstrImg = lstrImg.replace('CreditCardTele.gif','CreditCard.gif');
				else
					lstrImg = lstrImg.replace('CreditCard.gif','CreditCardTele.gif');
				document.all("imgTarjTele").src = lstrImg;
			}
		}             
        function mTarjetas()
        {
			var sRet=EjecutarMetodoXML("Utiles.Tarjetas", document.all("usrClie:txtId").value);
			document.all("DivimgTarj").style.display = "none";
			if (sRet == "-1") 
			{
				document.all("DivimgTarj").style.display ="inline";   // mostrar la imagen de la tarjeta  
				mSetearVentaTele();
			}
        } 
        
        function mEspeciales()
        {
			document.all("DivimgEspecVerde").style.display = "none";
			document.all("DivimgEspecRojo").style.display = "none";
			var sRet=EjecutarMetodoXML("Utiles.Especiales", document.all("usrClie:txtId").value);
			if(""!=sRet)
			{      
				if (sRet == "V")
					document.all("DivimgEspecVerde").style.display = "inline";
				else
					document.all("DivimgEspecRojo").style.display = "inline";
			}
         }
                 
		
		function mEstadoSaldos()
		{
			document.all("DivimgAlertaSaldosAmarillo").style.display = "none";
			document.all("DivimgAlertaSaldosVerde").style.display = "none";
			document.all("DivimgAlertaSaldosRojo").style.display = "none";
			    
			var sFiltro = document.all("usrClie:txtId").value + ";" + document.all("txtFechaValor").value;
			var sRet=EjecutarMetodoXML("Utiles.EstadoSaldos", sFiltro);
			if(""!=sRet)
		   		{      
					if (sRet == "A")
					document.all("DivimgAlertaSaldosAmarillo").style.display = "inline";
					else
					{
						if (sRet == "V") 
							document.all("DivimgAlertaSaldosVerde").style.display = "inline";
						else
							document.all("DivimgAlertaSaldosRojo").style.display = "inline";
					} 
				}   
		}  
		

		
       function mCargarCombos()
		{
  	      mCargar();
  	      mLlenaComboListaPrecios()
		}  
	
	function mCargar()	  
	   {
	     mCargarCriadores();
		 mCargarComprobAsociados();
		 mCargarExposiciones();	  
	   }
	 function mLlenaComboListaPrecios()
      {
       var sFiltroActiv = document.all("cmbActi").value;
	   var sFiltroFecValor = document.all("txtFechaValor").value;
	   if (sFiltroActiv != '' & sFiltroFecValor != '')
	   {
	     sFiltro = " @acti_id=" + sFiltroActiv + ",@peli_vige_fecha='" + FechaSQL(sFiltroFecValor) + "'"
	     LoadComboXML("precios_lista_fact_cargar", sFiltro, "cmbListaPrecios", "");
	     var vstrRet = LeerCamposXML("precios_lista_fact", sFiltro, "lista_id");
         //document.all("cmbListaPrecios").value = vstrRet;
         document.all("hdnListaPrecios").value = vstrRet;
	   }
        else
       {
        document.all("cmbListaPrecios").innerText = "";
        document.all("hdnListaPrecios").value = "";
       }       
       if (document.all('cmbListaPrecios').length > 1)
	  	   document.all('cmbListaPrecios').style.color = 'red';
	   else
		   document.all('cmbListaPrecios').style.color = 'black';
   }   

		function mSetearFechas()
		{
		 
		  var sFiltro= document.all("cmbComprobanteAsoc").value;
		  var vstrRet;
		  document.all("hdnComprobanteAsoc").value = ""
		  mCargarExposiciones(1)
		  if (sFiltro!="")
		    {
		      document.all("hdnComprobanteAsoc").value = sFiltro
		      sFiltro = sFiltro + ", @ejecuta=N"
		     
		      vstrRet = LeerCamposXML("comprobantes", sFiltro, "comp_fecha,comp_ivar_fecha").split("|");
			  if(vstrRet!=undefined)
			   {
			    //ActivarControl(document.all("txtFechaValor").value,false);
				//ActivarControl(document.all("txtFechaIva").value,false);
				document.all("txtFechaValor").value=vstrRet[0];
				document.all("txtFechaValor").onchange();				
				document.all("hdnFValorAnterior").value=vstrRet[0];
				//26/10/06 no debe tomar la fecha de ref de IVA del comp asociado
				//document.all("txtFechaIva").value=vstrRet[1];
			   } 
			    mCargarConvenio();	
			    mExposicion();
			    mLlenaComboListaPrecios();
			 }
			 mHabilitarListaSetearPrecios();
			 ActivarFecha("txtFechaValor",(sFiltro==""));
	    }
	 function mHabilitarListaSetearPrecios()
	  {
		if (document.all("cmbComprobanteAsoc").value == '' || document.all("cmbActi").value == 7)
		    document.all("cmbListaPrecios").disabled = false;
		else
		    document.all("cmbListaPrecios").disabled = true;
	  }
	  
	 function mExposicion()
	  {
	 
	    if (document.all("cmbActi").value != '')
          if(document.all("cmbActi").value == "7" )
	        {
	        
	         if (document.all("cmbComprobanteAsoc").value != '')
	          {
	          //esta cargado el combo 
	          var vstrRet = LeerCamposXML("comprob_deta", document.all("cmbComprobanteAsoc").value, "code_expo_id").split("|");
			  
			  if (vstrRet!="")
		        {
		         document.all("cmbExpo").value = vstrRet[0];
		         document.all("cmbExpo").disabled = true;
			     } 
			    else
			     {
			      document.all("cmbExpo").disabled = false;
			      mCargarExposiciones(1)
			    
			     }		   
			  }
			  
			 }
	  
	  }	        
		        
		        


       function mCargarConvenio()
		{
		    if (document.all("cmbComprobanteAsoc").value!= '')
		     {	     
		     	 var vstrRet = LeerCamposXML("convenios_fact", document.all("cmbComprobanteAsoc").value, "conv_id,descrip").split("|");
				 document.all("hdnConvId").value = "";
				 document.all("txtConv").value = "";
		
		      if (vstrRet!="")
		        {
		         document.all("hdnConvId").value = vstrRet[0];
				 document.all("txtConv").value = vstrRet[1];
		        }
		  	     
	   	   }
	
		 }		
		


















	
		
		function mTarjetaPago()
		
		{			
			if(document.all("cmbFormaPago").value == "3")
			{
		
				document.all("divTarje").style.display ="inline";
			}
			else
			{
				document.all("divTarje").style.display ="none";
			}	
	   }		
	   
	          
       	function mProformasAbrir()
	   	{
		 mAbrirVentanas("proforma_pop.aspx?EsConsul=1&actividad="+document.all("cmbActi").value+"&amp;"+"cliente="+document.all("usrClie:txtId").value+"&amp;"+"fecha_valor="+document.all("txtFechaValor").value, 7, "700","400","100");
	    }
	   	function mTarjetasAbrir()
	   	{
		 gAbrirVentanas("TarjetasHabilitadas_pop.aspx?EsConsul=1&titulo=Tarjetas Habilitadas&tabla=tarjetas_clientes&filtros=" + document.all("usrClie:txtId").value , 7, "700","300","100");
		}
		function mLeyendasAbrir()
	   	{
		 mAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Leyendas Facturaci�n&tabla=leyendas_factu", 7, "450","300","100");
		}
			 
	   function mEstadosAbrir()
	   	{
	   	 mAbrirPop("Estado de Saldos","S");
	 	}
	  
	   function mEspecialesAbrir()
	   	{
	   	 mAbrirPop("Especificaciones Especiales","N");
	    }
	function mAutoriza(pChk)
	 {
	  try{
	        var pstrId=document.all(pChk.id.replace("chkApro","divAutoId")).innerText;
	      	var pstrComen = document.all(pChk.id.replace("chkApro","txtComen")).value.replace(";","");
	      	      
	        var lbooChecked = 0;
	       	if (pChk.checked)
	     	    lbooChecked = 1;
		    var sFiltro = pstrId + ";" + lbooChecked + ";0" + document.all("hdnLoginPop").value + ";" + pstrComen;
		    var sRet=EjecutarMetodoXML("Utiles.GuardarAutorizaciones", sFiltro);
			if(sRet=="0" && lbooChecked==1)
		    {
				pChk.checked=false
				alert("El usuario no tiene permiso para autorizar");
			}		  
		   
	     }	
	 	catch(e)
		{
		 alert("Error al confirmar/desconfirmar la autorizaci�n");
		}
	 }
		
	function mAutorizaComen(pTxt)
	 {
	  try{
	        var pstrId = document.all(pTxt.id.replace("txtComen","divAutoId")).innerText;
	        var pChk = document.all(pTxt.id.replace("txtComen","chkApro"));
	      	var pstrComen = pTxt.value;
	      	
	        var lbooChecked = 0;
	       	if (pChk.checked)
	     	    lbooChecked = 1;
		    var sFiltro = pstrId + ";" + lbooChecked + ";0" + document.all("hdnLoginPop").value + ";" + pstrComen;
		    
		    var sRet=EjecutarMetodoXML("Utiles.GuardarAutorizaciones", sFiltro);
	     }	
	 	catch(e){}
	 }
	 
      function mAbrirPop(ptitulo,popcion)
       {
         var sFiltroClie = document.all("usrClie:txtId").value;
	    if (sFiltroClie != '')
         {
         
          mAbrirVentanas("Alertas_pop.aspx?titulo="+ptitulo+"&amp;"+"origen="+popcion+"&amp;"+"clieId=" + document.all("usrClie:txtId").value+"&amp;"+"FechaValor=" + document.all("txtFechaValor").value+"&amp;"+"sociId="+document.all("hdnSocioId").value, 7, "700", "400", "100");
	     }else
	     {
	      alert("Debe ingresar el cliente.")
	     }		
	   }  
	   
       function mTurnosEquinosAbrir()
       {
         mAbrirTurnos("Turnos de Equinos (pendientes de facturaci�n)","E");  
       }	
       function mTurnosBovinosAbrir()
       {
         mAbrirTurnos("Turnos de Bovinos (pendientes de facturaci�n)","B");  
       }
	    function mAbrirTurnos(ptitulo,popcion)
       {
         mAbrirVentanas("turnos_labo_pop.aspx?titulo="+ptitulo+"&amp;"+"origen="+popcion+"&amp;"+"cliente=" + document.all("hdnClieId").value, 7, "700","400","100");
	   }

  
	 function mOtrosDatosAbrir()
	   	{
	   	 mAbrirPop("Otros Datos","O");
	 	}
				
	   	function mAbrirVentanas(pstrPagina, pintOrden, pstrAncho, pstrAlto, pstrLeft, pstrTop)
		{
			vVentanas[pintOrden] = window.open (pstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=" + pstrAncho + "px,height=" + pstrAlto + "px,left=" + pstrLeft + "px,top=" + pstrTop + "px");
			
			for(i=0;i<11;i++)
			{
				if(vVentanas[i]!=null)
				{
					try{vVentanas[i].focus();}
					catch(e){vVentanas[i];}
					finally{;}
				}
			}
			
			vVentanas[pintOrden].focus();
		}
		
		function mCerrarVentanas()
		{
			for(i=0;i<11;i++)
			{
				if(vVentanas[i]!=null)
				{
					vVentanas[i].close();
					vVentanas[i]=null;
				}
			}
		}			
		
	function mValidarFechaRRGG()
	 {
	   if (document.all("txtFechaAranRRGG").value != "")
	    {
	      var sFiltro = document.all("txtFechaAranRRGG").value + ";" + document.all("hdnFechaValor").value;
	      var sRet=EjecutarMetodoXML("Utiles.ValidarFechaMayor", sFiltro);
           if(""!=sRet)
            {
	         alert("La Fecha no puede ser mayor a la fecha valor.");
	         document.all("txtFechaAranRRGG").value = ""
	         return "error" 
	        } 
	    }     
	 }
	 	
	function mFechaRRGG()
	 {
	 
	  if (mValidarFechaRRGG()!="error")
        {
	   
	    var sFiltro = document.all("cmbAran").value + ";" + document.all("txtFechaAranRRGG").value + ";" + document.all("hdnFechaValor").value;
	   var sRet=EjecutarMetodoXML("Utiles.AranRRGGValidacionFecha", sFiltro);
	    if(""!=sRet)
	  	  { 
	  	  
	  	    ActivarControl(document.all("txtActaAranRRGG"),sRet);
	  	 
	 	  }	        
         
       }
	 }	
	

	function mArancel()
      {
       if (document.all("cmbAran").value !="")
        {
         
          mCalcularPrecioUnitArancel();
          mCalcularImporteAranceles();
        }      
	 }
	 function mLimpiarRRGG()
	 {
	   ActivarControl(document.all("txtFechaAranRRGG"),false);
       ActivarControl(document.all("txtActaAranRRGG"),false);
       // 23/08/2010
       //ActivarControl(document.all("txtRPAranDdeRRGG"),false);
       //ActivarControl(document.all("txtRPAranHtaRRGG"),false); 
       document.all("lblFechaAranRRGG").innerText="Fecha:";
       document.all("txtRPAranDdeRRGG").value= ""; 
       document.all("txtRPAranHtaRRGG").value= "";  
   
       
       
	 }
	 
	 function mLimpiarTextRelaAran()
	  {
	    if(document.all("hdnClieDiscIVA").value == "True")
		 {
           document.all("txtTasaIVAAran").value = "";
           document.all("txtImpoIvaAran").value = "";
           document.all("hdlImpoIvaAran").value = "";
           document.all("hdnTasaIVAAran").value = "";
            

         }		
         document.all("txtPUnitAran").value = "";
         document.all("txtPUnitAranSIVA").value = "";
         document.all("txtImpoAran").value = "";
         document.all("chkExentoAran").checked = false;
      }
      
         

 function  mCalcularPrecioUnitArancel()
      {
      
       var ldouImporte;
       var ldouTasaIVA; 
       ldouImporte = 0;
       ldouTasaIVA = 0;
       //valor del arancel
      try{ 
      
		vsRet = EjecutarMetodoXML("Utiles.CalculoArancel", mFiltro('N')).split(";");
		// vsRet[0]; //PRECIO UNITARIO
		// vsRet[1]; //IMPORTE APLICADA LA FORMULA 
		// vsRet[2];//TASA IVA
		// vsRet[3];//IMPORTE IVA
		// vsRet[5];//IMPORTE NETO (PRECIO UNITARIO CON IVA)
	   
	    //guarda el importe sin iva  (indistintamente de la condicion IVA del cliente)
        document.all("txtPUnitAranSIVA").value = cent(vsRet[0])
     
        //tasa iva
	    //muestra es precio unitario con o sin IVA segun la cond. del cliente 
        if(document.all("hdnClieDiscIVA").value == "True")
		 {
		   document.all("txtPUnitAran").value =  cent(vsRet[0])
		 }else
		 {
		   document.all("txtPUnitAran").value = cent(vsRet[5]);
   		 }
   		} 
   		catch(e)
		{
		 alert("Error al intentar calcular el precio unitario del arancel");
		}
   		
      } 
      
       function btnBuscar_click(pstrTabla,pTitu)
		{
		  var filtro;
		  var filClieGene;
		  var filRazaId;
		  var campo;
		  var combo;   
		  filClieGene =0;
		  if(mClienteGenerico()=="True")
                 {
                   filClieGene="1";
                 }  
		  if (pstrTabla=='precios_aran')
		     {		      
              filRazaId = mRaza();
              campo="aran_desc";
              combo="cmbAran"; 
              filtro='@peli_acti_id='+ document.all("hdnActiId").value + ',@aran_clie_gene=' + filClieGene + ',@aran_raza_id=' + filRazaId
		     }
		    else
		     {
		       campo="conc_desc"
		       combo="cmbConcep"; 
		       filtro='@conc_acti_id='+ document.all("hdnActiId").value + ',@conc_clie_gene=' + filClieGene 
		     } 
		     BusqCombo(pstrTabla,filtro,campo,combo,pTitu)
	    }  
	    
	   

	  function btnAutoUsua_click()
      {
		gAbrirVentanas("Login_pop.aspx", 3, "200","110");
      }
      
      function hdnLoginPop_change()
      {
		document.all("lblAutoUsuaTit").innerText = LeerCamposXML("usuarios", "0" + document.all("hdnLoginPop").value, "usua_user");
      }
      
      function mFechaValor()
		{  
		 
		  mLlenaComboListaPrecios();
		  document.all("txtCotDolar").value = mTraerCotizacion(document.all("txtFechaValor").value);
		  document.all("hdnCotDolar").value = document.all("txtCotDolar").value;
		  document.all("hdnFocoSig").value = "S"
		} 
		
      function mTraerCotizacion(pFecha)
      {
		  var lstrCoti = EjecutarMetodoXML("ObtenerCotizacion", pFecha);
     	  return(lstrCoti);
      }            
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');Imprimir();"
		rightMargin="0" onunload="mCerrarVentanas();">
		<form id="frmABM" onsubmit="mCerrarVentanas();" method="post" runat="server">
			<P align="justify"></P>
			<!------------------ RECUADRO ----------------------->
			<TABLE cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label id="lbltitulo" runat="server" width="391px" cssclass="opcion">Nota de d�bito/cr�dito</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center">
										<!--- encabezado de la factura --->
										<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												Visible="False" Height="5px">
												<P align="right">
													<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
														<TR>
															<TD style="WIDTH: 100%">
																<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																	<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD vAlign="top" align="right" width="100%" colSpan="3" height="5">
																				<TABLE id="TableAlertas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgTarj" style="DISPLAY: none" runat="server"><A id="imgTarj" href="javascript:mTarjetasAbrir();" runat="server"><IMG alt="Puede operar con tarjetas" src="imagenes/CreditCard.gif" border="0" runat="server" id="imgTarjTele">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgAlertaSaldosAzul" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldos" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosAmarillo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosV" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircAmarillo.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosVerde" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosA" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosRojo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosR" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgEspecAzul" style="DISPLAY: none" runat="server"><A id="imgEspecAzul" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecVerde" style="DISPLAY: none" runat="server"><A id="imgEspecVerde" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecRojo" style="DISPLAY: none" runat="server"><A id="imgEspecRojo" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblFechaIng" runat="server" cssclass="titulo">Fecha Ingreso:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																				<asp:label id="lblFechaIngreso" runat="server" cssclass="titulo"></asp:label>&nbsp;
																				<asp:label id="lblFechaIva" runat="server" cssclass="titulo">Fecha Ref. I.V.A.:</asp:label>&nbsp;
																				<cc1:DateBox id="txtFechaIva" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" colSpan="4" height="5"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="Label5" runat="server" cssclass="titulo">Comprobante:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																				<cc1:combobox class="combo" id="cmbComprobante" runat="server" Width="250px" onchange="mTipoComprobante();"
																					AceptaNull="False"></cc1:combobox></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" colSpan="4" height="5"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																				<cc1:combobox class="combo" id="cmbActi" runat="server" Width="250px" onchange="mCargarCombos();mHabilitarListaSetearPrecios();"
																					AceptaNull="False"></cc1:combobox></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" colSpan="4" height="5"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClie" runat="server" AceptaNull="false" Ancho="800" FilFanta="true" FilLegaNume="True"></UC1:CLIE></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 5px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">&nbsp;
																				<A id="btnOtrosDatos" href="javascript:mOtrosDatosAbrir();" runat="server"><IMG alt="Otros Datos" src="imagenes/btnOtrosDatos.gif" border="0">
																				</A>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblSocio" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 30%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																				<cc1:numberbox id="txtSocio" runat="server" onchange="mCargarClienteSocio()" cssclass="cuadrotexto"
																					Width="80px"></cc1:numberbox></TD>
																			<TD style="WIDTH: 50%; HEIGHT: 5px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																		</TR>
																		<TR id="divCriador" runat="server">
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblraza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																			<TD align="right" colSpan="2">
																				<TABLE class="FdoFld" id="tblCria" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																							<table border=0 cellpadding=0 cellspacing=0>
																							<tr>
																							<td>
																							<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="250px" onchange="mCargarCliente()"
																													NomOper="razas_cargar" filtra="true" MostrarBotones="False"></cc1:combobox>
																							</td>
																							<td>
																								<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																											onclick="mBotonBusquedaAvanzada('razas','raza_desc','cmbRaza','Razas','');"
																											alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</td>
																							</tr>
																							</table>
																						</TD>
																						<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																							<asp:label id="lblCriaNume" runat="server" cssclass="titulo"> Nro. Criador:</asp:label>
																							<cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Visible="true" Width="80px"
																								Obligatorio="True" onchange="mCargarCliente()" MaxLength="12" MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<TD style="WIDTH: 50%; HEIGHT: 5px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD align="right" colSpan="3">
																				<DIV id="divClieGene" style="DISPLAY: none" runat="server">
																					<TABLE class="FdoFld" id="tblClieGene" style="WIDTH: 100%" cellPadding="0" align="left"
																						border="0">
																						<TR>
																							<TD style="WIDTH: 18%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblNombApel" runat="server" cssclass="titulo">Nomb.y apel.:</asp:Label>&nbsp;</TD>
																							<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																								<CC1:TEXTBOXTAB id="txtNombApel" runat="server" cssclass="textolibredeshab" Height="28px" Width="400px"
																									AceptaNull="False" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="Label6" runat="server" cssclass="titulo">Comp. asociado:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																				<cc1:combobox class="combo" id="cmbComprobanteAsoc" runat="server" Width="250px" onchange="mSetearFechas();"
																					AceptaNull="False" NomOper="comprobantes_cargar"></cc1:combobox></TD> <!---121206 si se habilita modificar el comp asociado desp de pasar la 1er solapa, poner al control AutoPostBack="True" -->
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR id="divExpo" runat="server">
																			<TD style="WIDTH: 18%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblExpo" runat="server" cssclass="titulo">Exposiciones:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbExpo" runat="server" Width="250px" NomOper="exposiciones_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblFechaValor" runat="server" cssclass="titulo">Fecha Valor:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																				<cc1:DateBox id="txtFechaValor" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="true"
																					onchange="mFechaValor();"></cc1:DateBox>&nbsp;&nbsp;
																				<asp:label id="lblListaPrecios" runat="server" cssclass="titulo">L. de Precios:</asp:label>&nbsp;
																				<cc1:combobox class="combo" id="cmbListaPrecios" runat="server" Width="100px" AceptaNull="False"
																					NomOper="precios_lista_fact_cargar"></cc1:combobox>
																				<asp:label id="lblCotDolar" runat="server" cssclass="titulo">Cot.Dolar:</asp:label>&nbsp;
																				<cc1:numberbox id="txtCotDolar" runat="server" cssclass="textomonto" Width="40px" Enabled="False"
																					EsDecimal="True" CambiaValor="True" CantMax="4"></cc1:numberbox></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblconv" runat="server" cssclass="titulo">Convenio:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																				<CC1:TEXTBOXTAB id="txtconv" runat="server" cssclass="cuadrotexto" Width="199px" enabled="false"></CC1:TEXTBOXTAB></TD>
																			<TD style="WIDTH: 5%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD align="center" colSpan="3">
																				<TABLE style="WIDTH: 100%">
																					<TR>
																						<TD align="center">
																	<TABLE style="WIDTH: 100%" border=0>
																		<TR>
																			<TD style="HEIGHT: 14px; width:300px;">
																				<DIV id="divEstadoSaldos" style="DISPLAY: none" runat=server>
																					<TABLE class="FdoFld" runat=server id="tblEstadoSaldos" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 300px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																						cellPadding="0" align="left" border="1">
																						<TR>
																							<TD align="right">
																								<asp:label id="lblSaldoCaCte" runat="server" cssclass="titulo">Saldo de Cta.Cte.:</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtSaldoCaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblSaldoCtaSoci" runat="server" cssclass="titulo">Saldo de Cta. Social :</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtSaldoCtaSoci" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpCuentaCtaCte" runat="server" cssclass="titulo">Importe a cuenta en Cta. Cte.:</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtImpCuentaCtaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpCuentaCtaSocial" runat="server" cssclass="titulo">Importe a cuenta en Cta. Social:</asp:label>&nbsp;
																							</TD>
																							<TD style="WIDTH: 70px">
																								<CC1:NUMBERBOX id="txtImpCuentaCtaSocial" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>																						
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpAcuses" runat="server" cssclass="titulo">Acuses de Recibo Pend.de Proc.:</asp:label>&nbsp;
																							</TD>
																							<TD style="WIDTH: 70px">
																								<CC1:NUMBERBOX id="txtImpAcuses" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																				</td>
																							<TD style="HEIGHT: 14px; width: 10px;">
																							<DIV id="divProformas" style="DISPLAY: none" runat="server">
																								<TABLE class="FdoFld" runat="server" id="Table3" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 260px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																									cellPadding="0" align="left" border="1">
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfRRGG" runat="server" cssclass="titulo">Total Proformas RRGG:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfRRGG" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfLabo" runat="server" cssclass="titulo">Total Prof.Laboratorio:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfLabo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfExpo" runat="server" cssclass="titulo">Total Prof.Exposiciones:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfExpo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfOtras" runat="server" cssclass="titulo">Total Otras Proformas:</asp:label>&nbsp;
																										</TD>
																										<TD style="WIDTH: 70px">
																											<CC1:NUMBERBOX id="txtTotalProfOtras" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																								</TABLE>
																							</DIV>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD valign=bottom align="right" colSpan="2">
																							<asp:ImageButton id="btnSigCabe" runat="server" CausesValidation="False" ToolTip="Posterior" ImageUrl="imagenes/fle.jpg"></asp:ImageButton></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- panel compartido con datos importantes del encabezado --->
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panEnca" runat="server" cssclass="titulo" Visible="False" BorderWidth="2px"
																	Width="100%">
																	<TABLE id="tblEnca" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD width="100%" colSpan="2">
																				<TABLE style="WIDTH: 100%">
																					<TR>
																						<TD align="right" background="imagenes/formfdofields.jpg" colSpan="4"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 10%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeClien" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;
																						</TD>
																						<TD style="WIDTH: 90%" align="left" background="imagenes/formfdofields.jpg" colSpan="3">
																							<asp:Label id="lblCabeCliente" runat="server" cssclass="titulo" Width="440px"></asp:Label></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 10%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;
																						</TD>
																						<TD style="WIDTH: 40%" align="left" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeActividad" runat="server" cssclass="titulo"></asp:Label></TD>
																						<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeTPag" runat="server" cssclass="titulo">Tipo de Pago:</asp:Label>&nbsp;
																						</TD>
																						<TD style="WIDTH: 40%" align="left" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeTPago" runat="server" cssclass="titulo"></asp:Label></TD>
																					</TR>
																					<TR>
																						<TD align="right" colSpan="5">
																							<HR width="100%" SIZE="1">
																						</TD>
																					</TR>
																					<TR> <!-- el importe neto pasa a ser el bruto, se cambio de lugar los text y se modifico la leyenda de los label (09/06)-->
																						<TD background="imagenes/formfdofields.jpg" colSpan="5"><TABLE style="WIDTH: 100%" border="0">
																								<TR>
																									<TD>
																										<asp:Label id="lblCabeTotalNetoT" runat="server" cssclass="titulo">Total neto: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalBruto" runat="server" cssclass="titulo"></asp:Label></TD>
																									<TD>
																										<asp:Label id="lblCabeTotalIVAT" runat="server" cssclass="titulo">Total I.V.A.: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalIVA" runat="server" cssclass="titulo"></asp:Label></TD>
																									<TD>
																										<asp:Label id="lblCabeTotalSobreT" runat="server" cssclass="titulo">Total sobretasa: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalSobre" runat="server" cssclass="titulo"></asp:Label></TD>
																									<TD>
																										<asp:Label id="lblCabeTotalBrutoT" runat="server" cssclass="titulo">Total bruto: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalNeto" runat="server" cssclass="titulo"></asp:Label></TD>
																								</TR>
																							</TABLE>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- detalle / solapas aranceles - conceptos-->
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panDeta" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE class="FdoFld" id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2"><!--- grilla compartida -->
																				<asp:datagrid id="grdDetalle" runat="server" width="100%" Visible="true" BorderWidth="1px" BorderStyle="None"
																					AutoGenerateColumns="False" OnEditCommand="mEditarDatosDetalle" OnPageIndexChanged="grdDetalle_PageChanged"
																					CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Left" AllowPaging="True">
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_id" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_tipo" ReadOnly="True" HeaderText="Tipo"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_codi_aran_concep" HeaderText="C&#243;digo"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_desc_aran_concep" ReadOnly="True" HeaderText="Descripci&#243;n">
																							<ItemStyle HorizontalAlign="Left"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_cant" HeaderText="Cant.">
																							<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
																							<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_impo" HeaderText="Importe($)" DataFormatString="{0:F2}">
																							<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
																							<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_ccos_codi" HeaderText="C.Cos."></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_exen" HeaderText="Ex"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_aran_concep_id" HeaderText="temp_aran_concep_id"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_ccos_id" HeaderText="ccos_id"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_impo" HeaderText="Importe"></asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="16"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 10%" vAlign="top"><!--- solapas --->
																				<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
																					<TR vAlign="top">
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg"></TD>
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:ImageButton id="btnAran" runat="server" CausesValidation="False" ToolTip="Aranceles" ImageUrl="imagenes/tabArancel1.gif"></asp:ImageButton></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg"></TD>
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:ImageButton id="btnConcep" runat="server" CausesValidation="False" ToolTip="Conceptos" ImageUrl="imagenes/tabConcep1.gif"></asp:ImageButton></TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<TD style="WIDTH: 90%"><!--- aranceles -->
																				<asp:panel id="panAran" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
																					Width="100%" BorderColor="#A4AFC3">
																					<TABLE class="FdoFld" id="tblaran" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																						<TR>
																							<TD colSpan="2">
																								<DIV id="DivAranNormal" style="DISPLAY: inline" runat="server">
																									<asp:panel id="panAranNormal" runat="server" cssclass="titulo" Visible="true">
																										<TABLE class="FdoFld" id="AranNormal" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblAran" runat="server" cssclass="titulo">Arancel:</asp:Label>&nbsp;</TD>
																												<TD background="imagenes/formfdofields.jpg" Width="300px">
																													<cc1:combobox class="combo" id="cmbAran" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"
																														onchange="mArancel();" NomOper="precios_aran" filtra="true" MostrarBotones="False" TextMaxLength="5"></cc1:combobox></TD>
																												<TD>
																													<IMG language="javascript" id="btnAranBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																														onclick="btnBuscar_click('precios_aran','Aranceles');" alt="Busqueda avanzada" src="imagenes/Buscar16.gif"
																														border="0">
																												</TD>
																											</TR>
																										</TABLE>
																									</asp:panel></DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblCantAran" runat="server" cssclass="titulo">Cantidad:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																								<cc1:numberbox id="txtCantAran" runat="server" cssclass="cuadrotexto" Width="48px" onchange="mCalcularImporteAranceles();"
																									AceptaNull="false"></cc1:numberbox>&nbsp;
																							</TD>
																						</TR> <!-- onchange="mCalcularImporteAranceles();" usarlo en txtPUnitAran si se permite cambiar el valor del arancel-->
																						<TR>
																							<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblPUnitAran" runat="server" cssclass="titulo">P.Unitario:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 80%" align="left" background="imagenes/formfdofields.jpg">
																								<cc1:numberbox id="txtPUnitAran" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																									Enabled="false" onchange="mCalcularImporteAranceles();" EsDecimal="True"></cc1:numberbox></TD>
																						</TR>
																						<TR height="5">
																							<TD colSpan="2" height="5">
																								<DIV>
																									<asp:panel id="panIVAAran" runat="server" cssclass="titulo" Visible="False">
																										<TABLE class="FdoFld" id="tblIVAAranRRGG" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblImpoIvaAran" runat="server" cssclass="titulo">Importe I.V.A.:</asp:Label>&nbsp;
																												</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtImpoIvaAran" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox>&nbsp;
																													<asp:Label id="lblTasaIVAAran" runat="server" cssclass="titulo">Tasa I.V.A.:</asp:Label>&nbsp;
																													<cc1:numberbox id="txtTasaIVAAran" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																											</TR>
																												<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTotalSobreTasa1" runat="server" cssclass="titulo">Importe Sobretasa:</asp:Label>&nbsp;
																												</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTotalSobreTasa1" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox>&nbsp;
																													<asp:Label id="lblTasaSobreTasa" runat="server" cssclass="titulo">Tasa Sobretasa:</asp:Label>&nbsp;
																													<cc1:numberbox id="txtTasaSobreTasa" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																											</TR>
																										</TABLE>
																									</asp:panel></DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblImpoAran" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																							<TD background="imagenes/formfdofields.jpg">
																								<cc1:numberbox id="txtImpoAran" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																									Enabled="False" EsDecimal="True"></cc1:numberbox>&nbsp;&nbsp;
																								<asp:Label id="lblExentoAran" runat="server" cssclass="titulo">Exento:</asp:Label>&nbsp;
																								<asp:CheckBox id="chkExentoAran" Enabled="False" Runat="server" CssClass="titulo" Checked="false"></asp:CheckBox>&nbsp;
																							</TD>
																						</TR>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblDescAmpliAran" runat="server" cssclass="titulo">Descrip. Ampliada:</asp:Label>&nbsp;</TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtDescAmpliAran" runat="server" cssclass="textolibredeshab" Height="30px" Width="400px"
																					AceptaNull="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR height="5"> <!--- datos que le pertenecen solo a arancel RRGG -->
																			<TD colSpan="2" height="5">
																				<DIV>
																					<asp:panel id="panDatosRRGG" runat="server" cssclass="titulo" Visible="False">
																						<TABLE class="FdoFld" id="tblDatosRRGG" style="WIDTH: 100%" cellPadding="0" align="left"
																							border="0">
																							<TR>
																								<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblFechaAranRRGG" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																									<cc1:DateBox id="txtFechaAranRRGG" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="true"></cc1:DateBox></TD>
																							</TR>
																							<TR>
																								<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblRPAranDdeRRGG" runat="server" cssclass="titulo">R.P.(desde):</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																									<cc1:numberbox id="txtRPAranDdeRRGG" runat="server" cssclass="textomonto" Width="100px"></cc1:numberbox>&nbsp;
																									<asp:Label id="lblRPAranHtaRRGG" runat="server" cssclass="titulo">R.P.(hasta):</asp:Label>&nbsp;
																									<cc1:numberbox id="txtRPAranHtaRRGG" runat="server" cssclass="textomonto" Width="100px"></cc1:numberbox></TD>
																							</TR>
																							<TR>
																								<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblActaAranRRGG" runat="server" cssclass="titulo">Acta:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																									<CC1:TEXTBOXTAB id="txtActaAranRRGG" runat="server" cssclass="cuadrotexto" Width="88px" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																							</TR>
																						</TABLE>
																					</asp:panel></DIV>
																			</TD>
																		<TR>
																			<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2"
																				height="30">
																				<asp:panel id="PanBotonesDetaAran" runat="server" cssclass="titulo" Width="100%">
																					<asp:Button id="btnAltaAran" runat="server" cssclass="boton" Width="120px" Text="Agregar Arancel"></asp:Button>&nbsp; 
																					<asp:Button id="btnBajaAran" runat="server" cssclass="boton" Width="120px" Text="Eliminar Arancel"></asp:Button>&nbsp; 
																					<asp:Button id="btnModiAran" runat="server" cssclass="boton" Width="120px" Text="Modificar Arancel"></asp:Button>&nbsp; 
																					<asp:Button id="btnLimpAran" runat="server" cssclass="boton" Width="120px" Text="Limpiar Arancel"></asp:Button>
																				</asp:panel>
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel><!--- detalle  conceptos-->
																<asp:panel id="panConcep" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
																	Width="100%" BorderColor="#A4AFC3">
																	<TABLE class="FdoFld" id="tblConcep" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<td colspan="3">
																				<table class="FdoFld" id="tblConcepCombo" style="WIDTH: 100%" cellPadding="0" align="left"
																					border="0">
																					<tr>
																						<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblConcep" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
																						<TD background="imagenes/formfdofields.jpg" Width="300px">
																							<cc1:combobox class="combo" id="cmbConcep" runat="server" cssclass="cuadrotexto" Width="300px"  onchange="javascript:mSetearConcepto(this);" 
																								Obligatorio="True" NomOper="cuentas_cargar" filtra="true" MostrarBotones="False" TextMaxLength="5"></cc1:combobox></TD>
																						<TD>
																							<IMG language="javascript" id="btnConcepBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																								onclick="btnBuscar_click('conceptos','Conceptos');" alt="Busqueda avanzada" src="imagenes/Buscar16.gif"
																								border="0">
																						</TD>
																					</tr>
																				</table>
																			</td>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCCosConcep" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																				<table border=0 cellpadding=1 cellspacing=0>
																				<tr>
																				<td>															
																				<cc1:combobox class="combo" id="cmbCCosConcep" runat="server" cssclass="cuadrotexto" Width="300px"
																					Obligatorio="True" NomOper="centrosc_cuenta_cargar" filtra="true" MostrarBotones="False" TextMaxLength="6"></cc1:combobox>
																				</td>
																				<td>
																					<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																						onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCCosConcep','Centros de Costo','@cuenta='+document.all('hdnCta').value);"
																						alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																				</td>
																				</tr>
																				</table>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="2">
																				<DIV>
																					<asp:panel id="panIVAConcep" runat="server" cssclass="titulo" Visible="False">
																						<TABLE class="FdoFld" id="tblIVAConcep" style="WIDTH: 100%" cellPadding="0" align="left"
																							border="0">
																							<TR>
																								<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblImpoIvaConcep" runat="server" cssclass="titulo">Importe I.V.A.:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																									<cc1:numberbox id="txtImpoIvaConcep" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																										EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																								<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblTasaIVAConcep" runat="server" cssclass="titulo">Tasa I.V.A.:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 40%" background="imagenes/formfdofields.jpg">
																									<cc1:numberbox id="txtTasaIVAConcep" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																										EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																							</TR>
																							<TR>
																								<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblTotalSobreTasaC" runat="server" cssclass="titulo">Sobretasa:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																									<cc1:numberbox id="txtTotalSobreTasaC" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																										EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																								<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblTasaSobreTasaC" runat="server" cssclass="titulo">Tasa Sobretasa:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 40%" background="imagenes/formfdofields.jpg">
																									<cc1:numberbox id="txtTasaSobreTasaC" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																										EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																							</TR> 
																							<!--- 30/08/06 --ocultado
																							<TR>
																								<TD align="right" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblImpoNConcep" runat="server" cssclass="titulo">Importe S/IVA:</asp:Label>&nbsp;</TD>
																								<TD background="imagenes/formfdofields.jpg">
																									<cc1:numberbox id="txtImpoNConcep" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																										ReadOnly="True" EsDecimal="True"></cc1:numberbox></TD>
																							</TR>	!---></TABLE>
																					</asp:panel></DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblImporteConcep" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtImporteConcep" runat="server" cssclass="cuadrotexto" Width="100px" onchange="mCalcularConceptos();"
																					AceptaNull="false" EsDecimal="True"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblDescAmpliConcep" runat="server" cssclass="titulo">Descrip. Ampliada:</asp:Label>&nbsp;</TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtDescAmpliConcep" runat="server" cssclass="textolibredeshab" Height="30px"
																					Width="400px" AceptaNull="False" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2">
																				<DIV style="DISPLAY: none">&nbsp;</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2"
																				height="30">
																				<asp:panel id="PanBotonesDetaConcep" runat="server" cssclass="titulo" Width="100%">
																					<asp:Button id="btnAltaConcep" runat="server" cssclass="boton" Width="120px" Text="Agregar Concep."></asp:Button>&nbsp; 
																					<asp:Button id="btnBajaConcep" runat="server" cssclass="boton" Width="120px" Text="Eliminar Concep."></asp:Button>&nbsp; 
																					<asp:Button id="btnModiConcep" runat="server" cssclass="boton" Width="120px" Text="Modificar Concep."></asp:Button>&nbsp; 
																					<asp:Button id="btnLimpiarConcep" runat="server" cssclass="boton" Width="120px" Text="Limpiar Concep."></asp:Button>
																				</asp:panel>
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- detalle - conceptos --->
															<TD style="WIDTH: 100%" colSpan="2">
														<TR>
															<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2"
																height="30">
																<asp:panel id="PanBotonesNavegDeta" runat="server" cssclass="titulo" Width="100%" visible="False">
																	<TABLE style="WIDTH: 100%">
																		<TR>
																			<TD align="right" colSpan="2">
																				<asp:ImageButton id="btnAnteriorDeta" runat="server" CausesValidation="False" ToolTip="Anterior"
																					ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																				<asp:ImageButton id="btnPosteriorDeta" runat="server" CausesValidation="False" ToolTip="Posterior"
																					ImageUrl="imagenes/fle.jpg"></asp:ImageButton>&nbsp;
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
													</TABLE>
											</asp:panel>
									</TD>
								</TR>
								<TR> <!--- cuotas --->
									<TD style="WIDTH: 100%" colSpan="2">
										<asp:panel id="panCuotas" runat="server" cssclass="titulo" Visible="False" Width="100%">
											<TABLE class="FdoFld" id="tblCuotas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD style="WIDTH: 100%" background="imagenes/formfdofields.jpg" colSpan="2">
														<asp:datagrid id="grdCuotas" runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
															OnEditCommand="mEditarDatosCuotas" OnPageIndexChanged="grdCuotas_PageChanged" CellPadding="1"
															GridLines="None" CellSpacing="1" HorizontalAlign="Left" AllowPaging="True">
															<FooterStyle CssClass="footer"></FooterStyle>
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="20px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																			<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="covt_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																<asp:BoundColumn DataField="covt_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
																	<HeaderStyle Width="20%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="covt_porc" ReadOnly="True" HeaderText="Porcen." HeaderStyle-HorizontalAlign="Right"
																	ItemStyle-HorizontalAlign="Right">
																	<HeaderStyle Width="40%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="covt_impo" HeaderText="Importe" DataFormatString="{0:###0.00}" HeaderStyle-HorizontalAlign="Right"
																	ItemStyle-HorizontalAlign="Right">
																	<HeaderStyle Width="40%"></HeaderStyle>
																</asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="16"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblImpTotalt" runat="server" cssclass="titulo">Importe total $:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblImpTotal" runat="server" cssclass="titulo" Width="128px" Font-Bold="True"
															ForeColor="#C00000"></asp:Label>&nbsp;</TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblCuotFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:DateBox id="txtCuotFecha" runat="server" cssclass="cuadrotexto" width="68px" Obligatorio="True"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblCuotPorc" runat="server" cssclass="titulo">Porcentaje:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtCuotPorc" runat="server" cssclass="textomonto" Width="40px" onchange="mCalcularImporteCuota();"
															MaxValor="100"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblCuotImporte" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtCuotImporte" runat="server" cssclass="textomonto" Width="84px" EsDecimal="True"></cc1:numberbox>&nbsp;
														<asp:Label id="Label4" runat="server" cssclass="titulo">Saldo $:</asp:Label>&nbsp;
														<asp:Label id="lblImpoSaldo" runat="server" cssclass="titulo" Width="128px" Font-Bold="True"
															ForeColor="IndianRed"></asp:Label></TD>
												</TR>
												<TR>
													<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="5"></TD>
												</TR>
												<TR>
													<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2">
														<asp:panel id="panBotonesCuotas" runat="server" cssclass="titulo" Width="100%">
															<asp:Button id="btnAltaCuot" runat="server" cssclass="boton" Width="120px" Text="Agregar Cuota"></asp:Button>&nbsp; 
															<asp:Button id="btnBajaCuot" runat="server" cssclass="boton" Width="120px" Text="Eliminar Cuota"></asp:Button>&nbsp; 
															<asp:Button id="btnModiCuot" runat="server" cssclass="boton" Width="120px" Text="Modificar Cuota"></asp:Button>&nbsp; 
															<asp:Button id="btnLimpiarCuot" runat="server" cssclass="boton" Width="120px" Text="Limpiar Cuota"></asp:Button>
														</asp:panel>
													</TD>
												</TR>
												<TR>
													<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2">
														<asp:panel id="PanBotonesNavegCuot" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE style="WIDTH: 100%">
																<TR>
																	<TD align="right" colSpan="2">
																		<asp:ImageButton id="btnAnteriorCuot" runat="server" CausesValidation="False" ToolTip="Anterior"
																			ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																		<asp:ImageButton id="btnPosteriorCuot" runat="server" CausesValidation="False" ToolTip="Posterior"
																			ImageUrl="imagenes/fle.jpg"></asp:ImageButton>&nbsp;
																	</TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR> <!--- autorizaciones --->
									<TD style="WIDTH: 100%" background="imagenes/formfdofields.jpg" colSpan="2">
										<asp:panel id="panAutoriza" runat="server" cssclass="titulo" Visible="False" Width="100%">
											<TABLE class="FdoFld" id="tblAutiza" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" align="left" background="imagenes/formfdofields.jpg"
														colSpan="2">
														<asp:Label id="Label1" runat="server" cssclass="titulo">Autorizaciones</asp:Label></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" background="imagenes/formfdofields.jpg" colSpan="2">
														<asp:datagrid id="grdAutoriza" runat="server" width="100%" BorderWidth="1px" BorderStyle="None"
															AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
															PageSize="200">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<Columns>
																<asp:BoundColumn Visible="False" DataField="coau_id" HeaderText="ID"></asp:BoundColumn>
																<asp:BoundColumn Visible="False" DataField="coau_auti_id" HeaderText="coau_auti_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="_auti_desc" ReadOnly="True" HeaderText="Tipo de autorizaci&#243;n">
																	<HeaderStyle Width="35%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:TemplateColumn HeaderText="Aprob.">
																	<HeaderStyle Width="5%"></HeaderStyle>
																	<ItemTemplate>
																		<asp:CheckBox ID="chkApro" Runat="server" onclick="mAutoriza(this);"></asp:CheckBox>
																		<DIV runat="server" id="divAutoId" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.coau_auti_id")%></DIV>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Comentario">
																	<HeaderStyle Width="60%"></HeaderStyle>
																	<ItemTemplate>
																		<CC1:TEXTBOXTAB id="txtComen" runat="server" onchange="mAutorizaComen(this);" cssclass="cuadrotexto"
																			Width="411px" AceptaNull="False"></CC1:TEXTBOXTAB>
																	</ItemTemplate>
																</asp:TemplateColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="16"></TD>
												</TR>
												<TR>
													<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2"
														height="30">
														<asp:panel id="panBotonesNavegAutiza" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE style="WIDTH: 100%">
																<TR>
																	<TD align="center">
																		<BUTTON runat="server" class="boton" id="btnAutoUsua" style="WIDTH: 115px" onclick="btnAutoUsua_click();"
																			type="button" runat="server" value="Detalles">Autorizante</BUTTON>
																		<asp:Label id="lblAutoUsuaTit" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
																	</TD>
																	<TD align="right">
																		<asp:ImageButton id="btnAnteriorAutiza" runat="server" CausesValidation="False" ToolTip="Anterior"
																			ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																		<asp:ImageButton id="btnPosteriorAutoriza" runat="server" CausesValidation="False" ToolTip="Posterior"
																			ImageUrl="imagenes/fle.jpg"></asp:ImageButton>&nbsp;
																	</TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR> <!--- pie ---> <!-- el importe neto pasa a ser el bruto, se cambio de lugar los text y se modifico la leyenda de los label (09/06)-->
									<TD style="WIDTH: 100%" colSpan="2">
										<asp:panel id="panPie" runat="server" cssclass="titulo" Height="230px" Visible="False" Width="100%">
											<TABLE class="FdoFld" id="tblPie" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblTotNeto" runat="server" cssclass="titulo">TOTALES ----> Neto:</asp:Label>&nbsp;
													</TD>
													<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtTotBruto" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
															EsDecimal="True" Enabled="False"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblTotalIVA" runat="server" cssclass="titulo">I.V.A.:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtTotalIVA" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
															EsDecimal="True" Enabled="False"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblTotalSobretasa" runat="server" cssclass="titulo">Sobretasa:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtTotalSobretasa" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
															Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD background="imagenes/formfdofields.jpg"></TD>
													<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblTotBruto" runat="server" cssclass="titulo">Bruto:</asp:Label>&nbsp;
													</TD>
													<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtTotNeto" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
															EsDecimal="True" Enabled="False"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 30%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="Label2" runat="server" cssclass="titulo">Leyenda:</asp:Label>&nbsp;
													</TD>
													<TD background="imagenes/formfdofields.jpg" colSpan="3">
														<CC1:TEXTBOXTAB id="txtObser" runat="server" cssclass="textolibredeshab" Height="54px" Width="400px"
															AceptaNull="False" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB><A id="btnBusLeyenda" href="javascript:mLeyendasAbrir();" runat="server"><IMG style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																alt="Buscar Leyenda" src="imagenes/Buscar16.gif" border="0"> </A>
													</TD>
												</TR>
												<TR>
													<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="4"
														height="100%">
														<asp:panel id="panBotonesPie" runat="server" cssclass="titulo" Height="33" Visible="False"
															Width="100%">
															<TABLE style="WIDTH: 100%">
																<TR>
																	<TD align="right" colSpan="2">
																		<asp:ImageButton id="btnAnteriorPie" runat="server" CausesValidation="False" ToolTip="Anterior" ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																		<asp:ImageButton id="btnEncabezadoPie" runat="server" CausesValidation="False" ToolTip="Cabecera"
																			ImageUrl="imagenes/fleup.jpg"></asp:ImageButton>&nbsp;
																	</TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
						</TABLE>
						</P> </asp:panel></DIV>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><A id="editar" name="editar"></A><CC1:BOTONIMAGEN id="ImgbtnGenerar" runat="server" BorderStyle="None" CambiaValor="False" ToolTip="Generar Comprobante"
							ImageUrl="imagenes/btngrab2.gif" ForeColor="Transparent" ImageDisable="btnGrab0.gif" ImageEnable="btnGrab2.gif" ImageOver="btnGrab.gif" BackColor="Transparent"
							ImageBoton="btnGrab2.gif" BtnImage="edit.gif" OutImage="del.gif" ImagesUrl="imagenes/" IncludesUrl="includes/"></CC1:BOTONIMAGEN><CC1:BOTONIMAGEN id="ImgbtnLimpiar" runat="server" BorderStyle="None" CambiaValor="False" ToolTip="Limpiar"
							ImageUrl="imagenes/btnlimp2.gif" ForeColor="Transparent" ImageOver="btnlimp2.gif" BackColor="Transparent" ImageBoton="btnlimp2.gif" BtnImage="edit.gif" OutImage="del.gif" ImagesUrl="imagenes/" IncludesUrl="includes/"></CC1:BOTONIMAGEN></td>
				</tr>
				</TBODY>
			</TABLE>
			<!--- FIN CONTENIDO ---> </td>
			<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0">
			</td>
			</tr>
			<tr>
				<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0">
				</td>
				<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0">
				</td>
				<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0">
				</td>
			</tr>
			</TABLE> 
			<!----------------- FIN RECUADRO ----------------->						
			<DIV style="DISPLAY: none"><asp:textbox id="hdnFechaValor" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnDatosPopTurnoEqu" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnDatosPopTurnoBov" runat="server" AutoPostBack="True"></asp:textbox>				
				<asp:textbox id="hdnFValorAnterior" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta" runat="server"></asp:textbox>
				<asp:textbox id="hdnCotDolar" runat="server"></asp:textbox>
				<asp:textbox id="hdnCotiAnterior" runat="server"></asp:textbox>			
				<asp:textbox id="hdnTasaSobreTasaC" runat="server"></asp:textbox>
				<asp:textbox id="hdnTotalSobreTasaC" runat="server"></asp:textbox>
				<asp:textbox id="hdnComprobante" runat="server"></asp:textbox>
				<asp:textbox id="hdnComprobanteAsoc" runat="server"></asp:textbox>
				<asp:textbox id="hdnCria" runat="server"></asp:textbox>
				<asp:textbox id="hdnCompGenera" runat="server"></asp:textbox><asp:textbox id="hdnImprimio" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="lblMens" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnImprimir" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnDatosPopProforma" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnProformaId" runat="server"></asp:textbox><asp:textbox id="hdnDatosCPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnDetaTempId" runat="server"></asp:textbox><asp:textbox id="hdnDetaTempTipo" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox><asp:textbox id="hdnDetaAranId" runat="server"></asp:textbox><asp:textbox id="hdnDetaConcepId" runat="server"></asp:textbox><asp:textbox id="hdnCuotasId" runat="server"></asp:textbox><asp:textbox id="hdnAutorizaId" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<cc1:numberbox id="txtPUnitAranSIVA" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
					EsDecimal="True"></cc1:numberbox><asp:textbox id="hdnFormaPago" runat="server"></asp:textbox><asp:textbox id="hdnConv" runat="server"></asp:textbox><asp:textbox id="hdnClieDiscIVA" runat="server"></asp:textbox><asp:textbox id="hdnUbic" runat="server"></asp:textbox><asp:textbox id="hdnClieId" runat="server"></asp:textbox><asp:textbox id="hdnActiId" runat="server"></asp:textbox><asp:textbox id="hdnFechaIva" runat="server"></asp:textbox><asp:textbox id="hdlImpoIvaAran" runat="server"></asp:textbox><asp:textbox id="hdnTasaIVAAran" runat="server"></asp:textbox><asp:textbox id="hdnTasaIVAConcep" runat="server"></asp:textbox><asp:textbox id="hdnImpoIvaConcep" runat="server"></asp:textbox><asp:textbox id="hdnConvId" runat="server"></asp:textbox><asp:textbox id="hdnSocioId" runat="server"></asp:textbox><asp:textbox id="hdnConf" runat="server"></asp:textbox><asp:textbox id="hdnFechaVigencia" runat="server"></asp:textbox>
				<cc1:numberbox id="txtTotIVA" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
					EsDecimal="True"></cc1:numberbox>
				<cc1:numberbox id="txtTotIVARedu" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
					EsDecimal="True"></cc1:numberbox>
				<cc1:numberbox id="txtTotIVASTasa" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
					EsDecimal="True"></cc1:numberbox>
				<asp:textbox id="hdnListaPrecios" runat="server"></asp:textbox>
				<asp:textbox id="hdnActivAnterior" runat="server"></asp:textbox>				
				<asp:textbox id="hdnLPrecioAnterior" runat="server"></asp:textbox>				
				<asp:textbox id="hdnRazaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnLoginPop" runat="server" onchange="hdnLoginPop_change();"></asp:textbox>
				<asp:textbox id="hdnFocoSig" runat="server" onchange="hdnLoginPop_change();"></asp:textbox>
				<asp:textbox id="hdnTasaSobreTasa" runat="server"></asp:textbox>
				<asp:textbox id="hdnTotalSobreTasa1" runat="server"></asp:textbox>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT>
				</OBJECT>
			</DIV>
		</form>
		<script language="javascript">
		function Imprimir()
		{
			if(document.all("hdnImprimir").value!="")
			{
				var pRepo = "Factura";
				if (document.all("hdnComprobante").value == "Proforma")	
					pRepo = "Proforma";

				var sRet = '';
			    sRet=EjecutarMetodoXML("Utiles.LetraComprob", document.all("hdnClieId").value);

				if (window.confirm("�Desea imprimir la " + document.all("hdnComprobante").value + " " + sRet + " "+ document.all("hdnCompGenera").value + " ?"))
				{
				 try{					
				        sRet = mImprimirCopias(2,'O',document.all("hdnImprimir").value,pRepo, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original
			          
					 	if (window.confirm("�Se imprimi� la " + document.all("hdnComprobante").value + " correctamente?"))
						{
							var lstrCopias = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimir").value, "copias");

						    if (lstrCopias != "0" && lstrCopias != "1")
								sRet = mImprimirCopias(lstrCopias,'D',document.all("hdnImprimir").value,pRepo, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias

							document.all("hdnImprId").value = '';
							document.all("hdnImprimio").value="1";
							__doPostBack('hdnImprimio','');	
							document.all("hdnImprimir").value="";			
						}
						else
						{
							document.all("hdnImprId").value = '';
							Imprimir();
						}
					}
				catch(e)
						{
							alert("Error al intentar efectuar la impresi�n");
						}
				}
				else
				{
					document.all("hdnImprimio").value="0";
					document.all("hdnImprimir").value=""
					__doPostBack('hdnImprimio','');		
				}
			}
		}

		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		try{	
		     if (document.all["cmbAran"]!= null && document.all["cmbAran"].value=="")
			   document.all["txtcmbAran"].focus();		
		    } catch(e){}
		try{     
		     if (document.all["cmbConcep"]!= null && document.all["cmbConcep"].value=="")
			   document.all["txtcmbConcep"].focus();
	       } catch(e){}
	
	    try{     
		    mCargarCriadores();
		   } catch(e){}  
		   
		try{
			if (document.all("hdnFocoSig").value!="")  
		    { 
		     //onactivate="this.ImageUrl='imagenes/fle_foco.jpg'"
		     document.all("hdnFocoSig").value = "";
		     document.all["btnSigCabe"].setActive();
		    }
		   } catch(e){}

		function GrabarUnaVez(pCtrl)
		{  
			if(pCtrl.Grabo==null)
				pCtrl.Grabo="1";
			else
				return false;
		} 
		if (document.all("usrClie:txtId")!=null)
		{
			if (document.all("cmbActi").value == '' || document.all("usrClie:txtId").value == '')
				document.all("cmbComprobanteAsoc").innerText = "";
		}
		
		mSetearCuadros();
  
		function mSetearCuadros(pCtrl)
		{  
			if(document.all("usrClie:txtId")!=null)
			{
				if(document.all("usrClie:txtId").value!='')
                {
					if(document.all("tblEstadoSaldos")!=null)
					{
						vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClie:txtId").value, "saldo_cta_cte,saldo_social,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras").split("|");
						
						document.all("txtSaldoCaCte").value  = vstrRet[0];
						document.all("txtSaldoCtaSoci").value  = vstrRet[1];
						document.all("txtImpCuentaCtaCte").value  = vstrRet[2];
						document.all("txtImpCuentaCtaSocial").value  = vstrRet[3];
						document.all("txtTotalProfRRGG").value  = vstrRet[4];
						document.all("txtTotalProfLabo").value  = vstrRet[5];
						document.all("txtTotalProfExpo").value  = vstrRet[6];
						document.all("txtTotalProfOtras").value  = vstrRet[7];				
						
						if(vstrRet[0]!=0||vstrRet[1]!=0||vstrRet[2]!=0||vstrRet[3]!=0)
							document.all("divEstadoSaldos").style.display="inline";
						else
							document.all("divEstadoSaldos").style.display="none";
						if(vstrRet[4]!=0||vstrRet[5]!=0||vstrRet[6]!=0||vstrRet[7]!=0)
							document.all("divProformas").style.display="inline";
						else
							document.all("divProformas").style.display="none";
							
						var vstrRet2 = LeerCamposXML("acuses_total", document.all("usrClie:txtId").value, "total_acuses").split("|");
						document.all("txtImpAcuses").value  = vstrRet2[0];
							
					} 
					else
					{
						document.all("divEstadoSaldos").style.display="none";
						document.all("divProformas").style.display="none";
					}
				}
			}
		}
			
		if (document.all["cmbCCosConcep"]!= null)
		{
			document.all('cmbCCosConcep').disabled = (document.all('cmbCCosConcep').options.length <= 1);
			document.all('txtcmbCCosConcep').disabled = (document.all('cmbCCosConcep').options.length <= 1);
		}
		mSetearVentaTele();
		if (document.all('cmbActi') != null)
			mCargarExposiciones();
		if (document.all('cmbListaPrecios')!=null)
		{
			if (document.all('cmbListaPrecios').length > 1)
				document.all('cmbListaPrecios').style.color = 'red';
			else
				document.all('cmbListaPrecios').style.color = 'black';
		}			
		</script>
		</FORM>
	</BODY>
</HTML>
