<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Password" CodeFile="Password.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cambio de Password</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js">	</script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Cambio de password</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderWidth="1px"
										BorderStyle="Solid">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%" colSpan="2">
													<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
														<TR>
															<TD align="right" width=50%>
																<asp:Label id="lblPassAnt" runat="server" cssclass="titulo">Ingrese password actual:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:textboxtab class="cuadrotexto" id="txtPassAnt" runat="server" TextMode="Password" Width="100px"></cc1:textboxtab></TD>
														</TR>
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblPass" runat="server" cssclass="titulo">Ingrese nuevo password:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:textboxtab class="cuadrotexto" id="txtPass" runat="server" TextMode="Password" Width="100px"></cc1:textboxtab></TD>
														</TR>
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblPass2" runat="server" cssclass="titulo">Repita nuevo password:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:textboxtab class="cuadrotexto" id="txtPass2" runat="server" TextMode="Password" Width="100px"></cc1:textboxtab></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
									<ASP:PANEL id="panBotones" Runat="server">
										<TABLE width="100%">
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Cambiar"></asp:Button>&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Limpiar"></asp:Button></TD>
											</TR>
										</TABLE>
									</ASP:PANEL>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtPassAnt"]!= null)
			document.all["txtPassAnt"].focus();
		</SCRIPT>
	</BODY>
</HTML>
