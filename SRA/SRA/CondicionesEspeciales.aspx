<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CondicionesEspeciales" CodeFile="CondicionesEspeciales.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Clientes con Condiciones Especiales</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var vVentanas = new Array(null);
		function mCerrarVentanas()
		{
			for(i=0;i<1;i++)
			{
				if(vVentanas[i]!=null)
				{
					vVentanas[i].close();
					vVentanas[i]=null;
				}
			}
		}
		
		function mHabilitarMonto()
		{
		document.all["txtLimite"].value="";
			if (document.all["rbtnConLimite"].checked==true)
				document.all["txtLimite"].disabled=false;
			else
				document.all["txtLimite"].disabled=true;
		}
		
		function cmbIIBB_Change()
		{
			var sFiltro = document.all("cmbIIBB").value;
			var vsRet;
			vsRet = EjecutarMetodoXML("Utiles.DatosIIBB", sFiltro).split(";");
			
			if (vsRet[2]=="True")
				{
					document.all["txtNroInsc"].disabled=false;
					document.all["txtNroInsc"].value = "";
				}
			else
				{
					document.all["txtNroInsc"].disabled=true;
					document.all["txtNroInsc"].value = "";
				}
		}
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="mCerrarVentanas();">
		<form id="frmABM" onsubmit="mCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 27px" height="27"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Clientes con Condiciones Especiales</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
											BorderStyle="Solid">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" tabIndex="11" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		ForeColor="Transparent" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif"
																		OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClieFil" runat="server" FilFanta="true" Ancho="800" AceptaNull="false" Saltos="1,1,1,1"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
												BorderStyle="Solid" Height="116px" width="100%">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD style="WIDTH: 100%" vAlign="top" align="left" height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" tabIndex="11" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar"
																CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFacEx" runat="server" cssclass="titulo">Facturaci�n Exenta:</asp:Label>&nbsp;
																		</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbFactExen" tabIndex="1" runat="server" Width="60px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblAgPreIVA" runat="server" cssclass="titulo">Sujeto a Percepci�n IVA:</asp:Label>&nbsp;
																		</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbAgPerIVA" tabIndex="2" runat="server" Width="60px"></cc1:combobox>&nbsp;&nbsp;
																			<asp:Label id="lblIIBB" runat="server" cssclass="titulo">Categor�a IIBB:</asp:Label>
																			<cc1:combobox class="combo" id="cmbIIBB" tabIndex="3" runat="server" Width="130px" onchange="cmbIIBB_Change();"></cc1:combobox>&nbsp;&nbsp;
																			<asp:Label id="lblNroInsc" runat="server" cssclass="titulo">Nro. Insc. IIBB:</asp:Label>
																			<CC1:TEXTBOXTAB id="txtNroInsc" tabIndex="4" runat="server" cssclass="cuadrotexto" Width="110px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFacPreSocio" runat="server" cssclass="titulo">Facturaci�n a Precio de Socio:</asp:Label>&nbsp;
																		</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbFacPreSocio" tabIndex="5" runat="server" Width="60px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblMieComDirectiva" runat="server" cssclass="titulo">Miembro Comisi�n Directiva:</asp:Label>&nbsp;
																		</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbMieComDirectiva" tabIndex="6" runat="server" Width="60px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Exento ND por Intereses</asp:Label></TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbNDInte" tabIndex="7" runat="server" Width="60px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="Label3" runat="server" cssclass="titulo">Cuenta Corriente</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<asp:RadioButton id="rbtnNoOpera" onclick="mHabilitarMonto();" tabIndex="8" runat="server" cssclass="titulo"
																				Width="100" checked="True" GroupName="RadioGroup1" Text="No opera"></asp:RadioButton></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<asp:RadioButton id="rbtnSinLimite" onclick="mHabilitarMonto();" tabIndex="9" runat="server" cssclass="titulo"
																				Width="100" checked="false" GroupName="RadioGroup1" Text="Sin L�mite"></asp:RadioButton></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<asp:RadioButton id="rbtnConLimite" onclick="mHabilitarMonto();" tabIndex="10" runat="server" cssclass="titulo"
																				Width="100" checked="false" GroupName="RadioGroup1" Text="Con L�mite"></asp:RadioButton>
																			<asp:Label id="lblMonto" runat="server" cssclass="titulo">Monto:</asp:Label>&nbsp;
																			<CC1:NUMBERBOX id="txtLimite" tabIndex="8" runat="server" cssclass="cuadrotexto" Width="100px"
																				Enabled="False" esdecimal="true"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 37.08%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblObser" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;
																		</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtObser" tabIndex="11" runat="server" cssclass="textolibredeshab" Width="536px"
																				AceptaNull="False" Height="54px" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnGrabar" tabIndex="9" runat="server" cssclass="boton" Width="80px" Text="Grabar"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<asp:Button id="btnLimp" tabIndex="10" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button>&nbsp;&nbsp;&nbsp;</TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnClieId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
		//document.all["txtNroInsc"].disabled=true;
		</SCRIPT>
	</BODY>
</HTML>
