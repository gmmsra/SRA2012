' Dario 2014-05-28 Cambios de nombres asignacion de campos y volar planilla 1
' Dario 2014-06-12 filtar la consulta por comprador, agregar combo pais olbigatorio
' Dario 2014-06-12 se agrega check para indicar con reserva de semen o embiones segun corresonda
Imports SRA_Entidad
Imports ReglasValida.Validaciones


Namespace SRA



Partial Class ExportacionAnimalPie
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents usrClieCompLocal As usrClieDeriv
    Protected WithEvents lblClieCompLocal As System.Web.UI.WebControls.Label
    Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    'Protected WithEvents usrClieVend As usrClieDeriv

    'Protected WithEvents usrCriaVend As usrClieDeriv
    Protected WithEvents usrCriaProp As usrClieDeriv
    Protected WithEvents rowDivProp As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents lblRegiTipo As System.Web.UI.WebControls.Label
    Protected WithEvents cmbRegiTipo As NixorControls.ComboBox

    Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents cmbRazaCria As NixorControls.ComboBox

    Protected WithEvents lblSraNumeFil As System.Web.UI.WebControls.Label

    Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblCantEmbr As System.Web.UI.WebControls.Label
    Protected WithEvents txtCantEmbr As NixorControls.NumberBox
    Protected WithEvents cmbCriaComp As NixorControls.ComboBox
    Protected WithEvents hdnCriaComp As System.Web.UI.WebControls.TextBox

    Protected WithEvents lblPorcComp As System.Web.UI.WebControls.Label
    Protected WithEvents txtPorcComp As NixorControls.NumberBox
    Protected WithEvents lblObseComp As System.Web.UI.WebControls.Label
    Protected WithEvents txtObseComp As NixorControls.TextBoxTab
    Protected WithEvents btnModiComp As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimpComp As System.Web.UI.WebControls.Button



    Protected WithEvents lblNombComp As System.Web.UI.WebControls.Label

    Protected WithEvents lblRecuFecha As System.Web.UI.WebControls.Label
    Protected WithEvents txtRecuFecha As NixorControls.DateBox
    Protected WithEvents btnTeDenu As System.Web.UI.WebControls.Button
    Protected WithEvents txtTeDesc As NixorControls.TextBoxTab

    Protected WithEvents cmbVari As NixorControls.ComboBox
    Protected WithEvents lblPelaAPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaAPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaBPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaBPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaCPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaCPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaDPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaDPeli As NixorControls.ComboBox
    Protected WithEvents lblTipoRegistro As System.Web.UI.WebControls.Label

    Protected WithEvents lblExp As System.Web.UI.WebControls.Label
    Protected WithEvents lblClieVend As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Combobox1 As NixorControls.ComboBox
    Protected WithEvents lblCriaVend As System.Web.UI.WebControls.Label
    Protected WithEvents txtCodi As NixorControls.NumberBox
    Protected WithEvents PnlReserva As Anthem.Panel
    'Protected WithEvents pnlusrProd As Anthem.Panel


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Tramites
    Private mstrTablaRequisitos As String = SRA_Neg.Constantes.gTab_Tramites_Deta
    Private mstrTablaDocumentos As String = SRA_Neg.Constantes.gTab_Tramites_Docum
    Private mstrTablaObservaciones As String = SRA_Neg.Constantes.gTab_Tramites_Obse
    Private mstrTablaProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrTablaEspecies As String = SRA_Neg.Constantes.gTab_Especies
    Private mstrTablaTramitesProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
    Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis
    Private mstrTablaProductosNumeros As String = SRA_Neg.Constantes.gTab_ProductosNumeros
    Private mstrTablaProductosDocum As String = SRA_Neg.Constantes.gTab_ProductosDocum


    Private mstrParaPageSize As Integer
    Private mdsDatosPadre As DataSet
    Private mdsDatosMadre As DataSet
    Private mdsDatosProd As DataSet
    Private dsVali As DataSet
    Private mstrCmd As String
    Public mintProce As Integer
    Private mdsDatos As DataSet
    Private mstrConn As String
    Private mstrTrapId As String
    Public mstrTitulo As String
    Public mintTtraId As Integer

    Private mstrTramiteId As String = ""

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            mSetearEventos()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                clsWeb.gInicializarControles(Me, mstrConn)

                Session("mitramite") = Nothing
                Session("productosExpAnimalPie") = Nothing

                mSetearMaxLength()
                'mSetearEventos()
                mCargarCombos()
                mMostrarPanel(False)
                mstrTramiteId = Request.QueryString("Tram_Id")

                If mstrTramiteId <> "" Then
                    hdnId.Text = mstrTramiteId
                    Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
                    cmbEsta.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                    cmbEstadoFil.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                    mEditarTramite(mstrTramiteId)
                    mMostrarPanel(True)
                    lnkRequ.Font.Bold = False
                    lnkDocu.Font.Bold = False
                    lnkObse.Font.Bold = False

                    lnkRequ.Enabled = False
                    lnkDocu.Enabled = False
                    lnkObse.Enabled = False

                    panRequ.Visible = False
                    panDocu.Visible = False
                    panObse.Visible = False
                End If
                If mstrTramiteId = "" Then
                    mSetearMaxLength()
                    mSetearEventos()
                    mCargarCombos()
                    mMostrarPanel(False)
                End If
            Else
                mstrTramiteId = Request.QueryString("Tram_Id")
                If mstrTramiteId <> "" Then
                    Response.Write("<Script>window.close();</script>")
                End If

            End If

            mdsDatos = Session("mitramite")
            mdsDatosProd = Session("productosExpAnimalPie")

            mstrTrapId = Session("mstrTrapId")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        Dim lstrSRA As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")
        clsWeb.gCargarCombo(mstrConn, "importadores_cargar", cmbExpo, "id", "descrip_codi", "S")
        clsWeb.gCargarCombo(mstrConn, "rg_requisitos_cargar", cmbRequRequ, "id", "descrip_codi", "S")
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "S", "@inc_defa = null", True)
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPaisFil, "S", "", True)
        'cmbExpo.Items.Insert(0, "(Seleccione)")
        'cmbExpo.Items(0).Value = ""
        'cmbExpo.SelectedIndex = -1

    End Sub
    Private Sub mSetearEventos()
        btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
        btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
        btnBaja.Attributes.Add("onclick", "if(!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?')){return false;} else {mPorcPeti();}")
        ' btnTeDenu.Attributes.Add("onclick", "mCargarTE();return(false);")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
        ' Dario 2014-06-12 atrapa el evento del combo de raza del producto para cambiar el del comprador de acuerdo a este  
        Me.usrProdFil.usrRazaCriadorExt.cmbCriaRazaExt.onchange = "return usrProdFil_usrRazaCriadorExt_cmbCriaRazaExt_change();"
        ' Dario 2014-06-13 atrapa el evento del combo de raza del producto para cambiar cambiar las descriciones de la 
        ' seccion de reserva de semen o embriones
        Me.usrProd.cmbSexoProdExt.onchange = "return usrProd_cmbProdSexo_change();"
        btnCriaCopropiedadComp.Attributes.Add("onclick", "mCriaCopropiedadComp();return false;")
        btnCriaCopropiedadVend.Attributes.Add("onclick", "mCriaCopropiedadVend();return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nume")

        txtNroControl.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nro_control")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocumentos)
        txtDocuObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trdo_refe")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaObservaciones)
        txtObseObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trao_obse")

    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mHabilitarDatosTE(ByVal pbooHabi As Boolean)


        'usrProd.Habilitar = False
        'usrProd.usrMadreExt.Activo = False
        'usrProd.usrMadreExt.cmbProdRazaExt.Enabled = False
        'usrProd.usrPadreExt.Activo = False
        'usrProd.MostrarResultado = False
    End Sub
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdRequ.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdDocu.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdObse.PageSize = Convert.ToInt32(mstrParaPageSize)

        mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionProductos
        mintProce = ReglasValida.Validaciones.Procesos.ExportacionProductos

        mstrTitulo = "Exportacion de Productos"


        rowProp.Style.Add("display", "none")



        mHabilitarDatosTE(False)
        btnAgre.AlternateText = "Nueva " & mstrTitulo

        lblTituAbm.Text = mstrTitulo

        usrProd.MuestraAsocExtr = False
        usrProd.MuestraNroAsocExtr = False

        usrProdFil.MuestraAsocExtr = False
        usrProdFil.MuestraNroAsocExtr = False

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub grdDato_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            If Page.IsPostBack Then

                mConsultar(True)
            Else
                mConsultar(False)

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdRequ_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdRequ.EditItemIndex = -1
            If (grdRequ.CurrentPageIndex < 0 Or grdRequ.CurrentPageIndex >= grdRequ.PageCount) Then
                grdRequ.CurrentPageIndex = 0
            Else
                grdRequ.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdDocu_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDocu.EditItemIndex = -1
            If (grdDocu.CurrentPageIndex < 0 Or grdDocu.CurrentPageIndex >= grdDocu.PageCount) Then
                grdDocu.CurrentPageIndex = 0
            Else
                grdDocu.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarDocu()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdObse.EditItemIndex = -1
            If (grdObse.CurrentPageIndex < 0 Or grdObse.CurrentPageIndex >= grdObse.PageCount) Then
                grdObse.CurrentPageIndex = 0
            Else
                grdObse.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mConsultar(ByVal boolMostrarTodos As Boolean)

        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet

            lstrCmd = "exec  tramites_animalEnPie_busq "

            If Not boolMostrarTodos Then
                If txtNumeFil.Valor <> 0 Then
                    lstrCmd += " @tram_nume=" + clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int) + ","
                End If

            End If
            If Not usrProdFil Is Nothing Then
                If usrProdFil.RazaId.ToString() <> "" Then
                    lstrCmd += " @tram_raza_id=" + clsSQLServer.gFormatArg(usrProdFil.RazaId, SqlDbType.Int) + ","
                End If


                If usrProdFil.Valor <> 0 Then
                    lstrCmd += "@prod_id=" + clsSQLServer.gFormatArg(usrProdFil.Valor, SqlDbType.Int) + ","
                End If

                lstrCmd += " @sexo=" + IIf(usrProdFil.cmbSexoProdExt.SelectedValue.ToString = "", _
                           "null,", usrProdFil.cmbSexoProdExt.SelectedValue.ToString + ",")
            End If

            lstrCmd += " @sra_nume=" + IIf(usrProdFil.txtSraNumeExt.Text = "", "null", usrProdFil.txtSraNumeExt.Text) + ","

                lstrCmd += " @tram_fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha) + ","
            lstrCmd += " @tram_fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha) + ","
            lstrCmd += " @tram_ttra_id=" + mintTtraId.ToString + ","
            lstrCmd += " @tram_esta_id=" + cmbEstadoFil.Valor.ToString + ","
            lstrCmd += " @tram_pais_id=" + cmbPaisFil.Valor.ToString + ","
            lstrCmd += " @mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")

            If Not usrCompradorFil Is Nothing Then
                If usrCompradorFil.Valor.ToString() <> "0" And usrCompradorFil.Valor.ToString() <> "" Then
                    lstrCmd += ",@cria_id=" + clsSQLServer.gFormatArg(usrCompradorFil.Valor.ToString(), SqlDbType.Int)
                End If
            End If

            If Not usrProdFil.usrRazaCriadorExt Is Nothing Then
                If usrProdFil.usrRazaCriadorExt.Valor.ToString() <> "0" And usrProdFil.usrRazaCriadorExt.Valor.ToString() <> "" Then
                    lstrCmd += ",@cria_vend_id=" + clsSQLServer.gFormatArg(usrProdFil.usrRazaCriadorExt.Valor.ToString(), SqlDbType.Int)
                End If
            End If

            dsDatos = clsWeb.gCargarDataSet(mstrConn, lstrCmd)
            grdDato.Visible = True
            grdDato.DataSource = dsDatos
            grdDato.DataBind()

            grdDato.Visible = True

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub mConsultarRequ()
        grdRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
        grdRequ.DataBind()
    End Sub
    Private Sub mConsultarDocu()
        grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocumentos)
        grdDocu.DataBind()
    End Sub
    Private Sub mConsultarObse()
        mdsDatos.Tables(mstrTablaObservaciones).DefaultView.Sort = "trao_fecha desc,_requ_desc"
        grdObse.DataSource = mdsDatos.Tables(mstrTablaObservaciones)
        grdObse.DataBind()
    End Sub

    Private Function mCrearDataSet(ByVal pstrId As String, ByVal pSpEstructura As String, _
                    ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                    ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean

        Dim ldsDatosTransf As New DataSet
        Dim tblDatos As New DataTable(pTableName)

        ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
        tblDatos = ldsDatosTransf.Tables(0)
        tblDatos.TableName = pTableName

        dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
        Session("miTramite") = dsDatosNewDataSet

        Return True
    End Function

    Public Sub mCrearDataSetProd(ByVal pstrId As String)
        mdsDatosProd = Nothing

        mdsDatosProd = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaProductos, pstrId)

        mdsDatosProd.Tables(0).TableName = mstrTablaProductos
        mdsDatosProd.Tables(1).TableName = mstrTablaAnalisis
        mdsDatosProd.Tables(2).TableName = mstrTablaProductosNumeros
        mdsDatosProd.Tables(3).TableName = mstrTablaProductosDocum

        Session("productosExpAnimalPie") = mdsDatosProd
    End Sub

    Private Function mCrearDataSetProd(ByVal pstrId As String, ByVal pSpEstructura As String, _
                      ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                      ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


        Dim ldsDatosTransf As New DataSet
        Dim tblDatos As New DataTable(pTableName)

        ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
        tblDatos = ldsDatosTransf.Tables(0)
        tblDatos.TableName = pTableName

        dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
        Session("productosExpAnimalPie") = dsDatosNewDataSet

        Return True
    End Function

    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = Nothing
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaRequisitos
        mdsDatos.Tables(2).TableName = mstrTablaDocumentos
        mdsDatos.Tables(3).TableName = mstrTablaObservaciones
        mdsDatos.Tables(4).TableName = mstrTablaTramitesProductos

        grdRequ.CurrentPageIndex = 0
        grdDato.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0

        mConsultarRequ()
        mConsultarDocu()
        mConsultarObse()

        Session("mitramite") = mdsDatos
    End Sub

#End Region

#Region "Seteo de Controles"
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mEditarTramite(E.Item.Cells(2).Text)
    End Sub

    Public Sub mEditarTramite(ByVal pstrTram As String)
        Try
            mLimpiar()
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
            Dim intClienteId As Integer
            Dim intVendClienteId As Integer
            Dim strProdId As String
            Dim dtProducto As DataTable
            Dim dtProductos As DataTable
            Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
            Dim cont As Integer

            intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
            intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrProd.RazaId, usrProd.CriaOrPropId)

            hdnId.Text = clsFormatear.gFormatCadena(pstrTram)
            mCrearDataSet(hdnId.Text)

            dtProducto = oTramite.GetProductoByTramiteId(hdnId.Text, "")
            If dtProducto.Rows.Count > 0 Then
                strProdId = dtProducto.Rows(0).Item("prdt_id")
            End If
            mCrearDataSetProd(strProdId)

            Session("mitramite") = mdsDatos
            Session("productosExpAnimalPie") = mdsDatosProd

            With mdsDatos.Tables("tramites").Rows(0)
                hdnTramNro.Text = "Tr�mite Nro: " & .Item("tram_nume")
                lblTitu.Text = hdnTramNro.Text
                hdnRazaId.Text = .Item("tram_raza_id")
                    txtInicFecha.Fecha = .Item("tram_inic_fecha").ToString

                    txtFinaFecha.Fecha = .Item("tram_fina_fecha").ToString
                    txtFechaTram.Fecha = .Item("tram_pres_fecha").ToString
                    txtFechaExportacion.Fecha = .Item("tram_oper_fecha").ToString
                    ' Dario 2014-06-18
                    If (ValidarNulos(.Item("tram_dosi_cant"), True) = "" And ValidarNulos(.Item("tram_embr_cant"), True) = "" And ValidarNulos(.Item("tram_cria_cant"), True) = "") Then
                        chkReservaEstock.Checked = False
                        Me.txtCantidadReser.Text = ""
                        Me.txtCantidadReser.Enabled = False
                    Else
                        chkReservaEstock.Checked = True
                        Me.txtCantidadReser.Text = IIf(ValidarNulos(.Item("tram_dosi_cant"), True) = "", IIf(ValidarNulos(.Item("tram_cria_cant"), True) = "", .Item("tram_embr_cant"), .Item("tram_cria_cant")), .Item("tram_dosi_cant"))
                        Me.txtCantidadReser.Enabled = True
                    End If

                cmbEsta.Valor = .Item("tram_esta_id")
                txtNroControl.Text = ValidarNulos(.Item("tram_nro_control"), False)
                usrClieProp.Valor = .Item("tram_prop_clie_id")
                usrClieProp.Activo = .IsNull("tram_prop_clie_id")
                ' usrProd.Tramite = hdnId.Text
                If dtProducto.Rows.Count > 0 Then
                    usrProd.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_id"), False)
                    usrProd.RazaId = ValidarNulos(dtProducto.Rows(0).Item("prdt_raza_id"), False)
                    'AAAAAAAAAA   usrProd.CriaOrPropId = .Item("tram_vend_cria_id")
                End If
                cmbPais.Valor = .Item("tram_pais_id")
                'AAAAAAAAAAAusrProd.Valor = hdnId.Text
                usrCriaComp.Valor = ValidarNulos(.Item("tram_comp_cria_id"), False)
                usrProd.CriaOrPropId = mdsDatos.Tables("tramites").Rows(0).Item("tram_vend_cria_id")
                hdnCompOrig.Text = ValidarNulos(.Item("tram_comp_clie_id"), False)
                hdnVendOrig.Text = ValidarNulos(.Item("tram_vend_clie_id"), False)

                If Not .IsNull("tram_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("tram_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

                strProdId = ""

                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 1 Then
                    With mdsDatos.Tables(mstrTablaTramitesProductos).Rows(0)
                        hdnDetaId.Text = .Item("trpr_id")
                        strProdId = ValidarNulos(.Item("trpr_prdt_id"), False)
                    End With
                Else
                    hdnDetaId.Text = "0"
                End If

                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count > 1 Then
                    usrProd.Visible = False

                    lblTranPlan.Visible = True
                    hdnMultiProd.Text = "S"
                Else
                    usrProd.Visible = True
                    lblTranPlan.Visible = False
                    hdnMultiProd.Text = ""
                End If
                ' Dario 2014-12-15 se comenta esta al pedo y altera el dato de oper fecha
                'If mdsDatosProd.Tables(mstrTablaProductos).Rows.Count > 0 Then
                '    If oTramite.GetFechaTransByTramiteId(pstrTram).ToString <> "" Then
                '        txtFechaExportacion.Fecha = oTramite.GetFechaTransByTramiteId(pstrTram)
                '    End If
                'End If

                'datos del �ltimo analisis
                If mdsDatosProd.Tables(mstrTablaAnalisis).Rows.Count > 0 Then
                    mdsDatosProd.Tables(mstrTablaAnalisis).DefaultView.Sort() = "prta_fecha DESC"
                    With mdsDatosProd.Tables(mstrTablaAnalisis).Select()(0)
                        txtNroAnal.Valor = .Item("prta_nume")
                        ' txtTipoAnal.Valor = .Item("_tipo")
                        txtResulAnal.Valor = .Item("_resul_codi") & "-" & .Item("_resul")
                    End With
                    mdsDatosProd.Tables(mstrTablaAnalisis).DefaultView.RowFilter = ""
                End If

                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count > 0 Then
                    'With mdsDatos.Tables(mstrTablaTramitesProductos).Rows(0)
                    '    usrProductoMadre.cmbProdAsocExt.Valor = .Item("trpr_madre_asoc_id")
                    '    usrProductoPadre.cmbProdAsocExt.Valor = .Item("trpr_padre_asoc_id")

                    'End With
                End If

                mSetearEditor("tramites", False)
                mMostrarPanel(True)
            End With
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosRequ(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDeta As DataRow

            hdnRequId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)

            With ldrDeta
                cmbRequRequ.Valor = .Item("trad_requ_id")
                chkRequPend.Checked = .Item("trad_pend")
                lblRequManu.Text = .Item("_manu")
            End With

            mSetearEditor(mstrTablaRequisitos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosDocu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDoc As DataRow

            hdnDocuId.Text = E.Item.Cells(1).Text
            ldrDoc = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)

            With ldrDoc
                If .IsNull("trdo_path") Then
                    txtDocuDocu.Valor = ""
                    imgDelDocuDoc.Visible = False
                Else
                    imgDelDocuDoc.Visible = True
                    txtDocuDocu.Valor = .Item("trdo_path")
                End If
                txtDocuObse.Valor = .Item("trdo_refe")
            End With

            mSetearEditor(mstrTablaDocumentos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrObse As DataRow

            hdnObseId.Text = E.Item.Cells(1).Text
            ldrObse = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)

            With ldrObse
                cmbObseRequ.Valor = .Item("_trad_requ_id")
                txtObseObse.Valor = .Item("trao_obse")
                lblObseFecha.Text = CDate(.Item("trao_fecha")).ToString("dd/MM/yyyy")
            End With

            mSetearEditor(mstrTablaObservaciones, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarFiltros()
        Try

            txtFechaDesdeFil.Text = ""
            txtFechaHastaFil.Text = ""
            cmbPaisFil.Limpiar()

            cmbEstadoFil.Limpiar()


            usrProdFil.Limpiar()

            grdDato.Visible = False
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnRazaId.Text = ""
        lblBaja.Text = ""
        hdnMultiProd.Text = ""
        hdnTEId.Text = ""
        txtNroControl.Text = ""
        txtInicFecha.Fecha = Now
        txtFechaTram.Fecha = Now
            txtFechaExportacion.Fecha = "" 'System.DBNull.Value
        txtFinaFecha.Text = ""
        chkReservaEstock.Checked = False
        Me.txtCantidadReser.Text = ""
        Me.txtCantidadReser.Enabled = False
        cmbEsta.Limpiar()
        cmbExpo.Limpiar()
        cmbPais.Limpiar()
        usrProd.Limpiar()
        lblTranPlan.Visible = False
        usrProd.Visible = True
        usrCriaComp.Limpiar()

        hdnCompOrig.Text = ""
        hdnVendOrig.Text = ""
        usrClieProp.Limpiar()
        usrClieProp.Activo = mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionProductos

        lblTitu.Text = ""
        hdnTramNro.Text = ""

        grdRequ.CurrentPageIndex = 0
        grdDocu.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0

        mLimpiarRequ()
        mLimpiarDocu()
        mLimpiarObse()

        mCrearDataSet("")
        mCrearDataSetProd("")

        mSetearEditor("tramites", True)
        mShowTabs(1)
        mCargarPlantilla()
    End Sub
    Private Sub mLimpiarRequ()
        hdnRequId.Text = ""
        cmbRequRequ.Limpiar()
        chkRequPend.Checked = False
        lblRequManu.Text = "S�"

        mSetearEditor(mstrTablaRequisitos, True)
    End Sub
    Private Sub mLimpiarDocu()
        hdnDocuId.Text = ""
        txtDocuObse.Valor = ""
        txtDocuDocu.Valor = ""
        imgDelDocuDoc.Visible = False

        mSetearEditor(mstrTablaDocumentos, True)
    End Sub


    Private Sub mLimpiarObse()
        hdnObseId.Text = ""
        cmbObseRequ.Limpiar()
        txtObseObse.Text = ""
        txtObseFecha.Fecha = Now

        mSetearEditor(mstrTablaObservaciones, True)
    End Sub


    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTabla
                btnAlta.Enabled = pbooAlta
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnErr.Visible = Not (pbooAlta)
            Case mstrTablaRequisitos
                btnAltaRequ.Enabled = pbooAlta
                btnBajaRequ.Enabled = Not (pbooAlta)
                btnModiRequ.Enabled = Not (pbooAlta)
            Case mstrTablaDocumentos
                btnAltaDocu.Enabled = pbooAlta
                btnBajaDocu.Enabled = Not (pbooAlta)
                btnModiDocu.Enabled = Not (pbooAlta)
            Case mstrTablaObservaciones
                btnAltaObse.Enabled = pbooAlta
                btnBajaObse.Enabled = Not (pbooAlta)
                btnModiObse.Enabled = Not (pbooAlta)

        End Select
    End Sub
    Private Sub mAgregar()
        Try
            mstrTrapId = 9 ' hdnDatosPop.Text Dario 2014-05-28
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
            hdnDatosPop.Text = ""
            Session("mstrTrapId") = mstrTrapId
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        lnkCabecera.Font.Bold = True
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False
        lnkVend.Font.Bold = False
        lnkComp.Font.Bold = False

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False

        panLinks.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Dim ldsDatos As DataSet
        Dim ldsDatosProd As DataSet
        Dim lstrTramiteId As String
        Dim eError As New ErrorEntity
        Dim strSexo As String
        Dim strRaza As String
        Dim strTramiteProdId As String
        Dim strProdId As String
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
        Dim strProductoId As String
        Dim intTramProdId As Integer
        Dim lTransac As SqlClient.SqlTransaction
        Try
            hdnOperacion.Text = "A"
            ldsDatos = Nothing
            ldsDatosProd = Nothing

            'mdsDatos = Session("mitramite")
            'mdsDatosProd = Session("productosExpAnimalPie")
            Dim ValorActual As Integer
            ValorActual = SRA_Neg.Constantes.Estados.Tramites_Vigente
            ' gsz 10/03/2015 se quito porque si es una alta no debe tomar nada de la sesion


            cmbEsta.Valor = ValorActual.ToString()

            ldsDatos = mGuardarDatos()

            If Not mdsDatosProd Is Nothing Then
                ldsDatosProd = mdsDatosProd
            End If

            If Not ldsDatosProd Is Nothing Then
                strProdId = Convert.ToString(usrProd.Valor.ToString())
                hdnProdId.Text = usrProd.Valor.ToString
            End If

            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_id") = usrProd.Valor
            strSexo = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_sexo"), True)

            ldsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_id") = usrProd.Valor
                ldsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_tran_fecha") = _
                IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)

            strSexo = ValidarNulos(ldsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_sexo"), True)

            ' Dario 2014-06-12 creo la transaccion para pasar al metodo de alta
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)
            Dim lTramites As New SRA_Neg.Tramites(lTransac, Session("sUserId").ToString(), _
                                                  mstrTabla, ldsDatos, ldsDatosProd, context, _
                                                  False, mintTtraId, mdsDatosMadre, mdsDatosPadre)

            lstrTramiteId = lTramites.Alta()
            mdsDatos.Tables("tramites").Rows(0).Item("tram_id") = lstrTramiteId
            ldsDatos.Tables("tramites").Rows(0).Item("tram_id") = lstrTramiteId
            Dim intClienteId As Integer

            intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)

            lblAltaId.Text = lTramites.mObtenerTramite(lstrTramiteId)
            mdsDatos.Tables("tramites").Rows(0).Item("tram_nume") = lblAltaId.Text
            ldsDatos.Tables("tramites").Rows(0).Item("tram_nume") = lblAltaId.Text

            'ldsDatosProd.Tables("productos").Rows(0).Item("prdt_tram_nume") = lblAltaId.Text
            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_prop_cria_id") = usrCriaComp.Valor
                ldsDatosProd.Tables("productos").Rows(0).Item("prdt_tran_fecha") = IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)
            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_prop_clie_id") = intClienteId

            strRaza = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_raza_id"), True)
            strTramiteProdId = ValidarNulos(ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_id"), True)

            lTramites.Tramites_updateByTramiteId(lstrTramiteId, lblAltaId.Text)

            strRaza = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_raza_id"), True)

            strProdId = ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_prdt_id").ToString()

            mdsDatos.AcceptChanges()
            ldsDatos.AcceptChanges()
            ldsDatosProd.AcceptChanges()

            ldsDatos.Tables("tramites").Rows(0).Item("tram_id") = lstrTramiteId

            clsSQLServer.gCommitTransac(lTransac)

            Dim lTramites2 As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), _
                                                  mstrTabla, ldsDatos, ldsDatosProd, context, _
                                                  False, mintTtraId, mdsDatosMadre, mdsDatosPadre)
            lTramites2.AplicarReglasTramite(lstrTramiteId, mintProce, mstrTablaTramitesProductos, "", strRaza, eError)
            If eError.errorDescripcion <> "" Then
                lTramites2.GrabarEstadoRetenidaEnTramites(lstrTramiteId)
                Throw New AccesoBD.clsErrNeg("Se detectaron errores validatorios, por favor verifiquelo.")
            End If

            mConsultar(False)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(Me, ex)
            If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                mConsultar(False)
            End If
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub mModi()
        Dim ldsDatos As DataSet
        Dim ldsDatosProd As DataSet
        Dim eError As New ErrorEntity
        Dim strSexo As String
        Dim strRaza As String
        Dim strTramiteProdId As String
        Dim strTramiteId As String
        Dim strProductoId As String
        Dim lTransac As SqlClient.SqlTransaction
        Try
            ldsDatosProd = Nothing
            ldsDatos = Nothing

            hdnOperacion.Text = "M"
            ldsDatos = mGuardarDatos()

            If ldsDatosProd Is Nothing Then
                ldsDatosProd = mGuardarDatosProd()
            End If

            mdsDatos = Session("mitramite")
            mdsDatosProd = Session("productosExpAnimalPie")

            ' Dario 2014-06-12 creo la transaccion para pasar al metodo de alta
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)
            Dim lTramites As New SRA_Neg.Tramites(lTransac, Session("sUserId").ToString(), mstrTabla, _
                                                  ldsDatos, ldsDatosProd, context, False, _
                                                  mintTtraId, Nothing, Nothing)

            strSexo = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_sexo"), True)
            strRaza = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_raza_id"), True)
            strTramiteProdId = ValidarNulos(ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_id"), True)
            strTramiteId = ValidarNulos(ldsDatos.Tables("tramites").Rows(0).Item("tram_id"), True)

            lTramites.Modi()

            strProductoId = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_id"), False)
            'Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

            mdsDatos.AcceptChanges()
            ldsDatos.AcceptChanges()
            ldsDatosProd.AcceptChanges()

            clsSQLServer.gCommitTransac(lTransac)

            Dim lTramites2 As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), _
                                                   mstrTabla, ldsDatos, ldsDatosProd, context, _
                                                   False, mintTtraId, mdsDatosMadre, mdsDatosPadre)

            lTramites2.AplicarReglasTramite(hdnId.Text, mintProce, mstrTablaTramitesProductos, "", strRaza, eError)
            If eError.errorDescripcion <> "" Then
                lTramites2.GrabarEstadoRetenidaEnTramites(hdnId.Text)
                Throw New AccesoBD.clsErrNeg("Se detectaron errores validatorios, por favor verifiquelo.")
            End If

            mLimpiar()
            mConsultar(False)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            clsSQLServer.gRollbackTransac(lTransac)
            If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                mConsultar(False)
            End If
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            cmbEsta.SelectedValue = SRA_Neg.Constantes.Estados.Tramites_Baja

            mModi()

            grdDato.CurrentPageIndex = 0

            mConsultar(True)

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub mValidarDatos(ByVal pbooCierre As Boolean, ByVal pbooCierreBaja As Boolean)
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If ValidarNulos(txtNroControl.Valor, False) = 0 Then
            Throw New AccesoBD.clsErrNeg("El numero de Control debe ser distinto de cero.")
        End If

            If IsDate(txtFechaExportacion.Fecha) Then
                If txtFechaExportacion.Fecha > DateTime.Now Then
                    Throw New AccesoBD.clsErrNeg("La Fecha de Exportaci�n no puede ser superior a la fecha actual.")
                End If
                If txtFechaTram.Fecha > DateTime.Now Then
                    Throw New AccesoBD.clsErrNeg("La Fecha del Tr�mite no puede ser superior a la fecha actual.")
                End If
            End If
            'GSZ 15/10/2014 Se quito validacion por pedido de Sergio	12/09/2014	(Debe ser opcional la carga del comprador, hoy te lo obliga a cargar)

            'If usrCriaComp.Valor.ToString = "" Then
            '    Throw New AccesoBD.clsErrNeg("Debe indicar el comprador.")
            'Else
            '    If usrCriaComp.Valor = "0" Then
            '        Throw New AccesoBD.clsErrNeg("El Comprador no tiene datos de Criador.")
            '    End If
            'End If

            If usrProd.CriaOrPropId.ToString().Trim() = "" Then
            Throw New AccesoBD.clsErrNeg("El Vendedor no tiene datos de Criador.")
        End If

        If usrCriaComp.Valor = usrProd.CriaOrPropId.ToString().Trim() Then
            Throw New AccesoBD.clsErrNeg("El Vendedor y Comprador deben ser distintos")
        End If

        If usrProd.RazaId = 0 Or usrProd.CriaOrPropId.ToString().Trim() = "" Then
            Throw New AccesoBD.clsErrNeg("El Vendedor no puede estar vacio.")
        End If

        ' Dario se comenta comprador opcional
        'If usrCriaComp.RazaId = 0 Or usrCriaComp.cmbCriaRazaExt.Valor.ToString().Trim() = "" _
        '                          Or usrCriaComp.cmbCriaRazaExt.Valor.ToString().Trim() = "0" Then
        '    Throw New AccesoBD.clsErrNeg("El Comprador no puede estar vacio.")
        'End If

            If cmbPais.Valor Is DBNull.Value Or cmbPais.Valor.Trim() = String.Empty Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Pais de Destino de la Exportaci�n.")
            End If

        Dim drTramites As DataRow
        If ValidarNulos(txtNroControl.Valor, False) = 0 Then
            Throw New AccesoBD.clsErrNeg("El Nro de Control debe ser distinto de 0.")
        End If

        'If ValidarNulos(txtNroControl.Valor, False) = 0 Then
        '    Throw New AccesoBD.clsErrNeg("El Nro de Control debe ser distinto de 0.")
        'End If
        ' Dario 2014-06-18 
        If (chkReservaEstock.Checked = True And txtCantidadReser.Text.Trim.Length = 0) Then
            Throw New AccesoBD.clsErrNeg("Se indico que se realiza reserva y no se informa candidad de la misma")
        End If

        'Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
        'drTramites = oTramites.GetCantFacturadoByNroControlRazaCriador(txtNroControl.Valor, _
        '              usrProd.RazaId.ToString, _
        '             usrProd.CriaOrPropId)

        'If IsNothing(drTramites) Then
        '    Throw New AccesoBD.clsErrNeg("No se encontraron comprobantes ,ni proforma con el Nro.Control " & txtNroControl.Valor)
        'Else
        'End If

        '' Dario 2014-10-31 comentado ahora no lo quierenal control de nro de control
        '' creo el objeto de productos 
        'Dim objProductosBusiness As New Business.Productos.ProductosBusiness
        '' ejecuto la validacion que me retorna un string con el mensaje de error si existe
        'Dim msg As String = objProductosBusiness.ValidaNumeroControlTramitesPropiedad(txtNroControl.Valor, usrProd.CriaOrPropId, usrCriaComp.Valor, usrProd.RazaId, 1, 0, Convert.ToInt16(Common.CodigosServiciosTiposRRGG.TransferenciaAnimalesPie))
        'If (Len(msg.Trim) > 0) Then
        '    Throw New AccesoBD.clsErrNeg(msg)
        'End If

        'Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
        'If Not oTramite.ValidarPaisesExportacionByRazaCriador(usrCriaVend.cmbCriaRazaExt.Valor, _
        '                                usrCriaVend.Valor, _
        '                                usrCriaComp.cmbCriaRazaExt.Valor, _
        '                                usrCriaComp.Valor) Then
        '    Throw New AccesoBD.clsErrNeg("El vendedor debe ser Argentino y el Comprador debe ser extranjero.")
        'End If

        If Not pbooCierreBaja Then
                If cmbPais.Valor Is DBNull.Value Or cmbPais.Valor.Trim() = String.Empty Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Pa�s para cerrar el Tr�mite.")
                End If
            If hdnSexo.Text <> "" Then
                usrProd.cmbSexoProdExt.Valor = hdnSexo.Text
            End If
            If usrProd.cmbSexoProdExt.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Sexo del Producto para el Tr�mite.")
            End If

            If clsSQLServer.gCampoValorConsul(mstrConn, "razas_consul @raza_id=" + usrProd.RazaId.ToString, "raza_espe_id") <> SRA_Neg.Constantes.Especies.Peliferos Then
                If usrProd.txtProdNombExt.Valor.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre del Producto para el Tr�mite.")
                End If
            End If
        End If
    End Sub
    Public Function mGuardarDatosPadre(ByVal pusrProd As SRA.usrProducto, _
        ByVal pstrId As String, ByVal pbooCierre As Boolean) As DataSet

        Dim ldsDatos As DataSet
        Dim lstrId As String
        Dim ldrProd As DataRow
        Dim ldrNume As DataRow

        ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, SRA_Neg.Constantes.gTab_Productos, "", "")

        With ldsDatos
            .Tables(0).TableName = SRA_Neg.Constantes.gTab_Productos
            .Tables(1).TableName = SRA_Neg.Constantes.gTab_ProductosNumeros

        End With

        If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select.GetUpperBound(0) = -1 Then
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).NewRow
            ldrProd.Table.Rows.Add(ldrProd)
        Else
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select()(0)
        End If

        'productos
        With ldrProd
            lstrId = clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
            .Item("prdt_id") = lstrId
            .Item("prdt_raza_id") = pusrProd.cmbProdRazaExt.Valor
            .Item("prdt_sexo") = pusrProd.cmbSexoProdExt.ValorBool
            .Item("prdt_nomb") = pusrProd.txtProdNombExt.Valor
                .Item("prdt_tran_fecha") = IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)

            .Item("prdt_rp") = IIf(Trim(pusrProd.txtRPExt.Text) = "", DBNull.Value, Trim(pusrProd.txtRPExt.Text))
            .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", "E", .Item("prdt_ndad"))
            .Item("prdt_ori_asoc_id") = pusrProd.cmbProdAsocExt.Valor
            .Item("prdt_ori_asoc_nume") = pusrProd.txtCodiExt.Valor
            '.Item("prdt_ori_asoc_nume_lab") = pusrProd.txtCodiExtLab.Valor
            .Item("generar_numero") = pbooCierre

        End With
        Return ldsDatos
    End Function

    Private Function mGuardarDatos() As DataSet

        Dim ldsDatos As DataSet
        Dim lbooCierre As Boolean = False
        Dim lbooCierreBaja As Boolean = False
        Dim ldrDatos As DataRow
        Dim dtProducto As DataTable

        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

        ldsDatos = Session("mitramite")

        If ldsDatos.Tables(mstrTablaRequisitos).Select("trad_pend = 1").GetUpperBound(0) < 0 And _
                grdRequ.Items.Count > 0 Then
            lbooCierre = True
        End If
        If cmbEsta.Valor.ToString = SRA_Neg.Constantes.Estados.Tramites_Baja Then
            lbooCierreBaja = True
        End If

        mValidarDatos(lbooCierre, lbooCierreBaja)

        Dim intClienteId As Integer
        Dim intVendClienteId As Integer
        intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
        intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrProd.RazaId, usrProd.CriaOrPropId)

        If ldsDatos.Tables("tramites").Rows.Count > 0 Then
            With mdsDatos.Tables("tramites").Rows(0)
                .Item("tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("tram_ttra_id") = mintTtraId
                .Item("tram_nro_control") = ValidarNulos(txtNroControl.Text, False)
                .Item("tram_raza_id") = usrProd.RazaId
                    .Item("tram_pres_fecha") = IIf(txtFechaTram.Fecha.Length > 0, txtFechaTram.Fecha, DBNull.Value)
                    .Item("tram_oper_fecha") = IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)
                    .Item("tram_esta_id") = IIf(cmbEsta.Valor.Length > 0, cmbEsta.Valor, DBNull.Value)
                    .Item("tram_pais_id") = IIf(cmbPais.Valor.Length > 0, cmbPais.Valor, DBNull.Value)
                .Item("tram_prop_clie_id") = intClienteId
                .Item("tram_vend_clie_id") = intVendClienteId
                .Item("tram_comp_clie_id") = intClienteId
                .Item("tram_vend_cria_id") = usrProd.CriaOrPropId
                .Item("tram_comp_cria_id") = usrCriaComp.Valor
                If .IsNull("tram_inic_fecha") Then
                    .Item("tram_inic_fecha") = Today
                End If
                .Item("tram_nro_control") = ValidarNulos(txtNroControl.Text, False)
                .Item("tram_baja_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                .Item("tram_fina_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                ' Dario 2014-06-18 se agrega datos de crias semen y embriones
                .Item("tram_cria_cant") = DBNull.Value
                If (usrProd.cmbSexoProdExt.ValorBool = True) Then
                    .Item("tram_dosi_cant") = IIf(chkReservaEstock.Checked, txtCantidadReser.Text, DBNull.Value)
                    .Item("tram_embr_cant") = DBNull.Value
                Else
                    .Item("tram_dosi_cant") = DBNull.Value
                    .Item("tram_embr_cant") = IIf(chkReservaEstock.Checked, txtCantidadReser.Text, DBNull.Value)
                End If

                dtProducto = oProducto.GetProductoById(usrProd.Valor, "")
                hdnProdId.Text = usrProd.Valor

                If dtProducto.Rows.Count = 0 Then
                    Throw New AccesoBD.clsErrNeg("El producto no existe en tabla de productos.")
                Else
                    mdsDatosProd = mGuardarDatosProd()

                    mdsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_id") = _
                    usrProd.Valor

                        mdsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_tran_fecha") = _
                        IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)

                    mdsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_sra_nume") = ValidarNulos(usrProd.txtSraNumeExt.Text, False)
                End If
            End With
        End If

        'GUARDA LOS DATOS DE TRAMITES_PRODUCTOS
        If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 0 Then
            ldrDatos = mdsDatos.Tables(mstrTablaTramitesProductos).NewRow
            ldrDatos.Item("trpr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaTramitesProductos), "trpr_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaTramitesProductos).Select()(0)
        End If

        With ldrDatos
            .Item("trpr_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("trpr_prdt_id") = usrProd.Valor
                If usrProd.cmbProdAsocExt.Valor.Length > 0 Then
                    .Item("trpr_asoc_id") = usrProd.cmbProdAsocExt.Valor
                End If
                .Item("trpr_audi_user") = Session("sUserId").ToString()
                .Item("trpr_baja_fecha") = DBNull.Value
                .Item("trpr_rp") = Trim(usrProd.txtRPExt.Text)
                .Item("trpr_sra_nume") = ValidarNulos(usrProd.txtSraNumeExt.Text, False)
                .Item("trpr_sexo") = usrProd.cmbSexoProdExt.ValorBool
                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 0 Then
                    .Table.Rows.Add(ldrDatos)
                End If
            End With

        mdsDatos.AcceptChanges()

        Return mdsDatos
    End Function

    Private Sub mActualizarRequ(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosRequ(pbooAlta)
            mLimpiarRequ()
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarRequ(ByVal pbooAlta As Boolean)
        If cmbRequRequ.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
        End If
        If pbooAlta Then
            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Requisito existente.")
            End If
        End If
    End Sub
    Private Sub mValidarModiRequ(ByVal pbooAlta As Boolean)
        If grdRequ.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
        End If
        If pbooAlta Then
            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Requisito existente.")
            End If
        End If
    End Sub
    Private Sub mGuardarDatosRequ(ByVal pbooAlta As Boolean)
        Dim ldrDatos As DataRow
        If hdnOperacion.Text = "M" Then
            mValidarModiRequ(pbooAlta)
        Else
            mValidarRequ(pbooAlta)
        End If

        If hdnRequId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
            ldrDatos.Item("trad_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRequisitos), "trad_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)
        End If

        With ldrDatos
            .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("trad_requ_id") = cmbRequRequ.Valor
            .Item("trad_pend") = chkRequPend.Checked
            .Item("trad_obli") = True
            .Item("trad_manu") = IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
            .Item("trad_baja_fecha") = DBNull.Value
            .Item("trad_audi_user") = Session("sUserId").ToString()
            .Item("_requ_desc") = cmbRequRequ.SelectedItem.Text
            .Item("_pend") = IIf(chkRequPend.Checked, "S�", "No")
            .Item("_manu") = "S�"
            .Item("_estado") = "Activo"

            If hdnRequId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With

    End Sub
    'DOCUMENTOS
    Private Sub mActualizarDocu()
        Try
            mGuardarDatosDocu()
            mLimpiarDocu()
            mConsultarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDocu()
        If txtDocuDocu.Valor.ToString = "" And filDocuDocu.Value = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
        End If

        If txtDocuObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
        End If
    End Sub
    Private Sub mGuardarDatosDocu()
        Dim ldrDatos As DataRow
        Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_tram_docu_path")

        mValidarDocu()

        If hdnDocuId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).NewRow
            ldrDatos.Item("trdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocumentos), "trdo_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)
        End If

        With ldrDatos
            .Item("trdo_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            If filDocuDocu.Value <> "" Then
                .Item("trdo_path") = filDocuDocu.Value
            Else
                .Item("trdo_path") = txtDocuDocu.Valor
            End If
            If Not .IsNull("trdo_path") Then
                .Item("trdo_path") = .Item("trdo_path").Substring(.Item("trdo_path").LastIndexOf("\") + 1)
            End If
            .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(.Item("trdo_id"), "-", "m") + "__" + .Item("trdo_path")
            .Item("trdo_refe") = txtDocuObse.Valor
            .Item("trdo_baja_fecha") = DBNull.Value
            .Item("trdo_audi_user") = Session("sUserId").ToString()
            .Item("_estado") = "Activo"

            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocuDocu)

            If hdnDocuId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub
    Private Sub mActualizarObse()
        Try
            mGuardarDatosObse()
            mLimpiarObse()
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarObse()
        If txtObseObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Observaci�n.")
        End If
        If txtObseFecha.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de la Observaci�n.")
        End If
    End Sub
    Private Sub mGuardarDatosObse()
        Dim ldrDatos As DataRow
        mValidarObse()

        If hdnObseId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).NewRow
            ldrDatos.Item("trao_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaObservaciones), "trao_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)
        End If

        With ldrDatos
            .Item("trao_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            If cmbObseRequ.Valor.ToString = "" Then
                .Item("trao_trad_id") = DBNull.Value
            Else
                .Item("trao_trad_id") = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_requ_id=" + cmbObseRequ.Valor.ToString)(0).Item("trad_id")
            End If
            .Item("_requ_desc") = IIf(cmbObseRequ.Valor.ToString = "", "COMENTARIO GENERAL", cmbObseRequ.SelectedItem.Text)
            .Item("_trad_requ_id") = IIf(cmbObseRequ.Valor.ToString = "", "0", cmbObseRequ.Valor)
            .Item("trao_obse") = txtObseObse.Valor
            .Item("trao_fecha") = txtObseFecha.Fecha
            .Item("trao_audi_user") = Session("sUserId").ToString()

            If hdnObseId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub

    Private Function mGuardarDatosProd() As DataSet
        Dim ldsDatosProd As DataSet
        Dim ldrDatosProd As DataRow
        Dim lbooCierre As Boolean = False
        Dim lbooCierreBaja As Boolean = False
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())


        mValidarDatos(lbooCierre, lbooCierreBaja)

        hdnProdId.Text = usrProd.Valor

        If mdsDatosProd.Tables(mstrTablaProductos).Select.GetUpperBound(0) = -1 Then
            ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).NewRow
            ldrDatosProd.Table.Rows.Add(ldrDatosProd)
        Else
            ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).Select()(0)
        End If

        With ldrDatosProd
            If hdnProdId.Text <> "" Then
                .Item("prdt_id") = clsSQLServer.gFormatArg(hdnProdId.Text, SqlDbType.Int)
            Else
                .Item("prdt_id") = clsSQLServer.gFormatArg(ldrDatosProd.ItemArray(0).ToString(), SqlDbType.Int)
            End If
            .Item("prdt_raza_id") = usrProd.RazaId
            .Item("prdt_sexo") = usrProd.cmbSexoProdExt.ValorBool
            .Item("prdt_nomb") = usrProd.txtProdNombExt.Valor
            .Item("prdt_rp") = Trim(usrProd.txtRPExt.Text)
            ' .Item("prdt_rp_extr") = txtRPExtr.Valor
                .Item("prdt_ndad") = "N"
                If usrProd.cmbProdAsocExt.Valor.Length > 0 Then
                    .Item("prdt_ori_asoc_id") = usrProd.cmbProdAsocExt.Valor
                End If
                .Item("prdt_ori_asoc_nume") = usrProd.txtCodiExt.Valor
                '.Item("prdt_prop_clie_id") = _
                'oCliente.GetClienteIdByRazaCriador(usrProd.RazaId, usrProd.CriaOrPropId)
                .Item("generar_numero") = False
                .Item("prdt_prop_cria_id") = usrProd.CriaOrPropId ' Dario 2014-12-09 se cambia para que no pise el cria_id
            End With

        mdsDatosProd.AcceptChanges()
        ldsDatosProd = mdsDatosProd
        ldsDatosProd.AcceptChanges()

        Return (ldsDatosProd)
    End Function

#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panCabecera.Visible = False
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False

        lnkCabecera.Font.Bold = False
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False

        lblTitu.Text = ""

        Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())

        If origen <> 1 And usrProd.Valor.ToString <> "" Then
            lblTitu.Text = "Producto: " & usrProd.RazaId & " - " & _
           oRaza.GetRazaDescripcionById(usrProd.RazaId) & _
            " - " & usrProd.txtProdNombExt.Valor.ToString
        End If

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Text = hdnTramNro.Text
            Case 2
                panRequ.Visible = True
                lnkRequ.Font.Bold = True
            Case 3
                panDocu.Visible = True
                lnkDocu.Font.Bold = True
            Case 4
                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = "trad_baja_fecha is null"

                cmbObseRequ.DataTextField = "_requ_desc"
                cmbObseRequ.DataValueField = "trad_requ_id"
                cmbObseRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
                cmbObseRequ.DataBind()
                cmbObseRequ.Items.Insert(0, "(Seleccione)")
                cmbObseRequ.Items(0).Value = ""
                cmbObseRequ.Valor = ""

                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = ""

                panObse.Visible = True
                lnkObse.Font.Bold = True

        End Select
    End Sub
    Private Sub mLimpiarPersonas(ByVal pstrTabla As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
        For Each lDr As DataRow In mdsDatos.Tables(pstrTabla).Select()
            If lDr.Item("trpe_id") > 0 Then
                lDr.Delete()
            Else
                mdsDatos.Tables(pstrTabla).Rows.Remove(lDr)
            End If
        Next

        If mdsDatos.Tables(pstrTabla).Select.GetUpperBound(0) = -1 Then pctrHdnOrig.Text = ""

        pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
        pgrdGrilla.DataBind()
    End Sub

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnAltaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRequ.Click
        mActualizarRequ(True)
    End Sub
    Private Sub btnLimpRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRequ.Click
        mLimpiarRequ()
    End Sub
    Private Sub btnBajaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRequ.Click
        Try
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaObservaciones).Select("trao_trad_id=" + hdnRequId.Text)
                odrDeta.Delete()
            Next

            mConsultarObse()
            mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0).Delete()
            grdRequ.CurrentPageIndex = 0
            mConsultarRequ()
            mLimpiarRequ()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRequ.Click
        mActualizarRequ(False)
    End Sub
    Private Sub btnAltaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocu.Click
        mActualizarDocu()
    End Sub
    Private Sub btnLimpDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocu.Click
        mLimpiarDocu()
    End Sub
    Private Sub btnBajaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocu.Click
        Try
            mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0).Delete()
            grdDocu.CurrentPageIndex = 0
            mConsultarDocu()
            mLimpiarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocu.Click
        mActualizarDocu()
    End Sub
    Private Sub btnAltaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaObse.Click
        mActualizarObse()
    End Sub
    Private Sub btnLimpObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpObse.Click
        mLimpiarObse()
    End Sub
    Private Sub btnBajaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaObse.Click
        Try
            mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0).Delete()
            grdObse.CurrentPageIndex = 0
            mConsultarObse()
            mLimpiarObse()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiObse.Click
        mActualizarObse()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar(False)
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRequ.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkObse.Click
        mShowTabs(4)
    End Sub

#End Region

#Region "Opciones de POP"
    Private Sub mCargarPlantilla()
        If mstrTrapId <> "" Then

            Dim lDs As New DataSet
            lDs = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Tramites_Plantilla, "@SinBaja=1,@trap_id=" + mstrTrapId)
            Dim ldrDatos As DataRow

            mdsDatos.Tables(mstrTablaRequisitos).Clear()

            For Each ldrOri As DataRow In lDs.Tables(1).Select
                ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                With ldrDatos
                    .Item("trad_id") = clsSQLServer.gObtenerId(.Table, "trad_id")
                    .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    .Item("trad_baja_fecha") = DBNull.Value
                    .Item("trad_requ_id") = ldrOri.Item("trpd_requ_id")
                    .Item("trad_obli") = ldrOri.Item("trpd_obli")
                    .Item("trad_pend") = True
                    .Item("trad_manu") = False
                    .Item("_requ_desc") = ldrOri.Item("_requisito")
                    .Item("_obli") = ldrOri.Item("_obligatorio")
                    .Item("_pend") = "S�"
                    .Item("_manu") = "No"
                    .Item("_estado") = ldrOri.Item("_estado")

                    .Table.Rows.Add(ldrDatos)
                End With
            Next

            Dim lstrRazas As String = ""
            Dim lstrEspecies As String = ""

            For Each ldrOri As DataRow In lDs.Tables(2).Select
                With ldrOri
                    If .IsNull("trdr_raza_id") Then
                        If lstrEspecies.Length > 0 Then lstrEspecies += ","
                        lstrEspecies += .Item("trdr_espe_id").ToString
                    Else
                        If lstrRazas.Length > 0 Then lstrRazas += ","
                        lstrRazas += .Item("trdr_raza_id").ToString
                    End If
                End With
            Next

            'usrProd.usrProductoExt.FiltroRazas = "@raza_ids = " & IIf(lstrRazas = "", "null", "'" & lstrRazas & "'") & ",@raza_espe_ids = " & IIf(lstrEspecies = "", "null", "'" & lstrEspecies & "'")
            'usrProd.usrProductoExt.mCargarRazas()

            mConsultarRequ()
        End If
    End Sub
    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            mstrTrapId = hdnDatosPop.Text
            mAgregar()
            hdnDatosPop.Text = ""
            Session("mstrTrapId") = mstrTrapId

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub hdnDatosTEPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosTEPop.TextChanged
        Try
            hdnTEId.Text = ""
            txtRecuFecha.Text = ""
            txtTeDesc.Text = ""
            txtCantEmbr.Text = ""


            If (hdnDatosTEPop.Text <> "") Then
                hdnTEId.Text = hdnDatosTEPop.Text
                hdnDatosTEPop.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Function MessageBox(ByRef oPage As Page, ByVal sAviso As String)
        If sAviso.Trim.Length > 0 Then
            Dim s As String = "alert('" & sAviso & "') "
            oPage.RegisterStartupScript("OnLoad", s)
        End If
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub grdDato_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdDato.SelectedIndexChanged

    End Sub

    'Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

    'End Sub
    ' Dario 2014-06-13 Metodo que recupera el evento del cambio de producto del control usrPord
    Private Sub usrProd_Change(ByVal sender As Object) Handles usrProd.Cambio
        If Not (sender Is Nothing) Then
            Dim obj As usrProducto = CType(sender, usrProducto)

            If (obj.cmbSexoProdExt.ValorBool = True) Then
                Me.lblReserva.Text = "Con Reserva de Semen: "
                Me.txtCantidadReser.Enabled = True
                Me.chkReservaEstock.Checked = True
            ElseIf (obj.cmbSexoProdExt.ValorBool = False) Then
                Me.lblReserva.Text = "Con Reserva de Embriones: "
                Me.txtCantidadReser.Enabled = True
                Me.chkReservaEstock.Checked = True
            Else
                Me.lblReserva.Text = "Reserva: "
                Me.txtCantidadReser.Enabled = False
                Me.txtCantidadReser.Text = ""
                Me.chkReservaEstock.Checked = False
            End If
            'Me.PnlReserva.UpdateAfterCallBack = True
        End If
    End Sub
End Class
End Namespace
