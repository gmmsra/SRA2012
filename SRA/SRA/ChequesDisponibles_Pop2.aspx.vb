Imports SRA
Public Class ChequesDisponibles_Pop2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents grdConsulta As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnAceptar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblCentId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblFechaFil As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaFil As NixorControls.DateBox


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Cheques
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                txtFechaFil.Fecha = Today.AddDays(-1).ToString("dd/MM/yyyy")
                mConsultar()
            Else
                mdsDatos = Session(mstrTabla)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub mConsultar()
        Dim lstrCmd As New StringBuilder

        lstrCmd.Append("exec depo_cheques_2_consul")
        lstrCmd.Append(" @Fecha = ")
        lstrCmd.Append(clsSQLServer.gFormatArg(txtFechaFil.Text, SqlDbType.SmallDateTime))
        lstrCmd.Append(",@CenEmId = ")
        lstrCmd.Append(clsSQLServer.gFormatArg(Request("cemisor"), SqlDbType.Int))
        lstrCmd.Append(",@CuentaId = ")
        lstrCmd.Append(clsSQLServer.gFormatArg(Request("cuenta"), SqlDbType.Int))
        lstrCmd.Append(",@CleaId = ")
        lstrCmd.Append(clsSQLServer.gFormatArg(Request("clearing"), SqlDbType.Int))
        lstrCmd.Append(",@Cheques = ''")
        lstrCmd.Append(",@GigoBanc = ")
        lstrCmd.Append(clsSQLServer.gFormatArg(Request("GiroBanc"), SqlDbType.TinyInt))

        mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)
        Dim lvrCheques() As String = Request("ChequesGrilla").Split(",")
        If lvrCheques(0) <> "" Then
            For i As Integer = 0 To UBound(lvrCheques)
                If mdsDatos.Tables(0).Select("_cheq_id=" & lvrCheques(i).ToString).GetUpperBound(0) <> -1 Then
                    mdsDatos.Tables(0).Select("_cheq_id=" & lvrCheques(i).ToString)(0).Delete()
                End If
            Next
        End If

        grdConsulta.DataSource = mdsDatos.Tables(0)
        grdConsulta.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            mGuardarChechk()

            grdConsulta.EditItemIndex = -1
            If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
                grdConsulta.CurrentPageIndex = 0
            Else
                grdConsulta.CurrentPageIndex = E.NewPageIndex
            End If

            grdConsulta.DataSource = mdsDatos.Tables(0)
            grdConsulta.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarChechk()
        Dim dr As DataRow
        'Mantengo los check de las paguinas de la grilla
        For Each oDataItem As DataGridItem In grdConsulta.Items
            dr = mdsDatos.Tables(0).Select("_cheq_id = " & oDataItem.Cells(1).Text)(0)
            dr.Item("_chk") = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
        Next
        Session(mstrTabla) = mdsDatos
    End Sub
#End Region

#Region "Operaciones sobre Controles"
    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim lstrId As String = mGuardarDatos()
        Try

            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append("window.opener.document.all['hdnDatosPop'].value='")
            lsbMsg.Append(lstrId & "';")

            lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Private Function mGuardarDatos() As String
        Dim lstrId As New StringBuilder
        mGuardarChechk()

        For Each oDataItem As DataRow In mdsDatos.Tables(0).Select("_chk = true")
            If lstrId.Length > 0 Then lstrId.Append(",")
            lstrId.Append(oDataItem.Item("_cheq_id"))
        Next

        Return (lstrId.ToString)
    End Function
    Private Sub txtFechaFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFechaFil.TextChanged
        If txtFechaFil.Fecha Is DBNull.Value Then
            txtFechaFil.Fecha = Today.AddDays(-1).ToString("dd/MM/yyyy")
        End If
        mConsultar()
    End Sub
#End Region

End Class
