<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.login" EnableViewState="false" CodeFile="login.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Sistema Administrativo - Login</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		if (top.location.href != document.location.href)
			{
				top.location.href = document.location.href;
			}
		</script>
	</HEAD>
	<body bgcolor="#ffffff" topmargin="20" leftmargin="0" marginheight="20" marginwidth="0">
		<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="100%">
			<tr>
				<td valign="middle" align="center">
					<!------------------ RECUADRO ------------------->
					<table border="0" cellpadding="0" cellspacing="0" align="center">
						<tr>
							<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
							<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
							<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
						</tr>
						<tr>
							<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
							<td valign="middle">
								<!----- CONTENIDO ----->
								<table border="0">
									<tr>
										<td><img src="imagenes/txlogin.jpg" border="0" WIDTH="255" HEIGHT="40"></td>
									</tr>
									<tr>
										<td height="10"></td>
									</tr>
									<tr>
										<td>
											<table border="0">
												<tr>
													<td align="center" width="150"><img src="imagenes/llaves1.jpg" border="0" WIDTH="118" HEIGHT="115"></td>
													<td>
														<FORM id="frmLogin" runat="server">
															<!-------- FORMULARIO LOGIN -------->
															<table border="0" cellpadding="0" cellspacing="0" width="200">
																<tr>
																	<td>
																		<table border="0" width="100%" cellpadding="0" cellspacing="0">
																			<tr>
																				<td width="24"><img src="imagenes/formfle.jpg" border="0" WIDTH="24" HEIGHT="25"></td>
																				<td width="42"><img src="imagenes/formtxlogin.jpg" border="0" WIDTH="42" HEIGHT="25"></td>
																				<td width="26"><img src="imagenes/formcap.jpg" border="0" WIDTH="26" HEIGHT="25"></td>
																				<td background="imagenes/formfdocap.jpg"><img src="imagenes/formfdocap.jpg" border="0" WIDTH="7" HEIGHT="25"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table border="0" width="100%" cellpadding="0" cellspacing="0">
																			<tr>
																				<td background="imagenes/formiz.jpg" width="3"><img src="imagenes/formiz.jpg" border="0" WIDTH="3" HEIGHT="30"></td>
																				<td>
																					<!-- FOMULARIO -->
																					<table border="0" width="100%" cellpadding="0" cellspacing="0">
																						<tr>
																							<td background="imagenes/formfdofields.jpg" width="20"></td>
																							<td background="imagenes/formfdofields.jpg" align="right"><font class="titulo">Usuario:</font></td>
																							<td background="imagenes/formfdofields.jpg" width="8"></td>
																							<td background="imagenes/formfdofields.jpg" align="left">
																								<cc1:textboxtab id="UserName" class="cuadrotexto" runat="server" Width="100px"></cc1:textboxtab>
																							</td>
																						</tr>
																						<tr>
																							<td background="imagenes/formdivmed.jpg" height="2" align="right"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivmed.jpg" height="2" align="right"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivmed.jpg" height="2" width="8"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivmed.jpg" height="2" align="left"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																						</tr>
																						<tr>
																							<td background="imagenes/formfdofields.jpg" width="20"></td>
																							<td background="imagenes/formfdofields.jpg" align="right"><font class="titulo">Password:</font></td>
																							<td background="imagenes/formfdofields.jpg" width="8"></td>
																							<td background="imagenes/formfdofields.jpg" align="left">
																								<cc1:textboxtab id="UserPass" class="cuadrotexto" runat="server" Width="100px" TextMode="Password"
																									EnterPorTab="False"></cc1:textboxtab></td>
																						</tr>
																						<tr>
																							<td background="imagenes/formdivmed.jpg" height="2" align="right"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivmed.jpg" height="2" align="right"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivmed.jpg" height="2" width="8"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivmed.jpg" height="2" align="left"><img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
																						</tr>
																						<tr>
																							<td align="center" colspan="4" background="imagenes/formfdofields.jpg">
																								<table border="0">
																									<tr>
																										<td>
																											<asp:button cssclass="boton" id="cmdLogin" runat="server" Width="90px" text="Ingresar"></asp:button>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td background="imagenes/formdivfin.jpg" height="2" align="right"><img src="imagenes/formdivfin.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivfin.jpg" height="2" align="right"><img src="imagenes/formdivfin.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivfin.jpg" height="2" width="8"><img src="imagenes/formdivfin.jpg" width="1" height="2"></td>
																							<td background="imagenes/formdivfin.jpg" height="2" align="left"><img src="imagenes/formdivfin.jpg" width="1" height="2"></td>
																						</tr>
																					</table>
																					<!-- FOMULARIO -->
																				</td>
																				<td background="imagenes/formde.jpg" width="2"><img src="imagenes/formde.jpg" border="0" WIDTH="2" HEIGHT="30"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!------ FIN FORMULARIO LOGIN ------>
														</FORM>
													</td>
												</tr>
												<TR>
													<TD vAlign="middle" align="center" height="30" colspan="2">
														<asp:Label id="Msg" Font-Bold="False" Font-Size="x-small" Font-Name="Arial" ForeColor="red"
															runat="server" width="100%"></asp:Label>
													</TD>
												</TR>
											</table>
										</td>
									</tr>
								</table>
<%--								<asp:Label id="Label1" runat="server">Version 2.0.00 (25/11/2014)</asp:Label>--%>
								<asp:Label id="Label2" runat="server">Version 4.0</asp:Label>
								<!--- FIN CONTENIDO --->
							</td>
							<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
						</tr>
						<tr>
							<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
							<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
							<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
						</tr>
					</table>
					<!----------------- FIN RECUADRO ----------------->
				</td>
			</tr>
		</table>
		<script language="javascript">
<!--
	document.frmLogin.UserName.focus();
//-->
		</script>
	</body>
</HTML>
