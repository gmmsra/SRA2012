<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Cotizacion" CodeFile="Cotizacion.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cotización</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Cotización</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" BorderStyle="Solid"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																	IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="left">
																			<cc1:DateBox id="txtFechaFil" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblMonedaFil" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="left">
																			<cc1:combobox id="cmbMonedaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%" height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 27.6%" height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 72%" height="2" background="imagenes/formdivmed.jpg"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 13.76%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 82%" height="2" background="imagenes/formdivfin.jpg" width="8"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="como_id" ReadOnly="True" HeaderText="ID">
												<HeaderStyle Width="2px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="como_fecha_cotiza" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_mone_desc" HeaderText="Moneda">
												<HeaderStyle Width="20%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="como_impo_cotiza" HeaderText="Cotiza"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
													IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
													ImageOver="btnNuev2.gif" ForeColor="Transparent" ToolTip="Agregar una Nueva Cotización" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent" ToolTip="Imprimir Listado"
													ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 126px">
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 10.39%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
															<asp:Label id="lblMoneda" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
														<TD style="WIDTH: 27.6%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="3"
															align="left">
															<cc1:combobox id="cmbMoneda" class="combo" runat="server" cssclass="cuadrotexto" Width="90px"
																Obligatorio="True"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 10.39%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
															<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label></TD>
														<TD style="WIDTH: 27.6%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2"
															align="left">
															<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																Obligatorio="true"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 10.39%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
															<asp:Label id="lblCotiza" runat="server" cssclass="titulo">Cotiza:</asp:Label></TD>
														<TD colSpan="3">
															<CC1:TEXTBOXTAB id="txtCotiza" runat="server" cssclass="cuadrotexto" Width="60px" Obligatorio="True"
																MaxLength="6"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 173px" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
													</TR>
													<TR>
														<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
			
		function ValorFijoChange()
		{
			if(window.event.srcElement.value!="")
				document.all["cmbTari"].selectedIndex=2;
			else
				document.all["cmbTari"].selectedIndex=1;
		}
		</SCRIPT>
	</BODY>
</HTML>
