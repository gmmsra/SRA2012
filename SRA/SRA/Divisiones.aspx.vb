Namespace SRA

Partial Class Divisiones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Divisiones
   Private mstrDiviInsc As String = SRA_Neg.Constantes.gTab_DivisionesInscrip

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet
   Private mintInse As Integer

   Private Enum ColumnasInsc As Integer
      Id = 0
      InscId = 1
      DiviId = 2
      Recur = 3
      Oyen = 4
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

            mSetearMaxLength()
            mSetearEventos()
            txtAnio.Valor = Today.Year
            txtAnioFil.Valor = Today.Year

            mCargarCombos()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      txtAnio.Attributes.Add("onchange", "txtAnio_Change(this,'" & mintInse & "');")
      btnBajaComiTodos.Attributes.Add("onclick", "if(!confirm('Confirma la baja de TODAS las comisiones?')) return false;")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCiclFil, "T", mintInse.ToString)
      clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCicl, "S", mintInse.ToString & "," & txtAnio.Valor.ToString)
      mSetearCiclDesc()
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "divi_nomb")
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, ByVal pbooTieneComi As Boolean, ByVal pbooTieneAsist As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta

      btnGeneComi.Enabled = Not (pbooAlta) And Not pbooTieneAsist
      btnBajaComiTodos.Enabled = Not (pbooAlta) And pbooTieneComi And Not pbooTieneAsist

      btnGeneInsc.Enabled = Not pbooTieneComi
      btnBajaInscTodos.Enabled = Not pbooTieneComi
      grdInsc.Columns(0).Visible = Not pbooTieneComi

      btnGeneComi.Text = IIf(pbooTieneComi, "Re-Generar Comisiones", "Generar Comisiones")
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lintComiCant As Integer
      Dim lbooTieneAsist As Boolean

      mCrearDataSet(pstrId)

      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("divi_id").ToString()
         cmbCicl.Valor = .Item("divi_cicl_id")
         txtAnio.Valor = .Item("_cicl_anio")

         txtNomb.Valor = .Item("divi_nomb")
         cmbCicl.Valor = .Item("divi_cicl_id")
         lblPeti.Text = .Item("_peti_desc")
         txtPerio.Valor = .Item("divi_perio")

         lintComiCant = .Item("_comi_cant")

         mSetearCiclDesc()

         If Not .IsNull("divi_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("divi_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      lblTitu.Text = "Registro Seleccionado: " + cmbCicl.SelectedItem.Text + " - " + txtNomb.Text

      With clsSQLServer.gExecuteQuery(mstrConn, "exec division_datos_consul " + hdnId.Text).Tables(0).Rows(0)
         lbooTieneAsist = .Item("cant_coca") > 0 Or .Item("cant_asist") > 0
      End With

      mSetearEditor("", False, lintComiCant <> 0, lbooTieneAsist)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""
      txtAnio.Text = Today.Year
      cmbCicl.Limpiar()

      txtNomb.Text = ""
      txtPerio.Text = ""

      mSetearCiclDesc()
      lblBaja.Text = ""

      grdInsc.CurrentPageIndex = 0

      txtAnio.Enabled = True
      cmbCicl.Enabled = True
      txtPerio.Enabled = True

      mCrearDataSet("")

      mSetearEditor("", True, 0, False)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      Dim lbooVisiOri As Boolean = panDato.Visible

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible
      btnList.Visible = Not pbooVisi

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()

      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      If lbooVisiOri And Not pbooVisi Then
         cmbCiclFil.Limpiar()
         txtAnioFil.Valor = Today.Year
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjGenericoRel.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi(ByVal pbooGenerar As Boolean)
      Try
         mGuardarDatos()

         Dim ldsTmp As DataSet = mdsDatos.Copy

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
         lobjGenericoRel.Modi()

         If pbooGenerar Then
            mGenerarComisiones
         End If

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenericoRel.Baja(hdnId.Text)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Private Sub mGuardarDatos()
            mValidarDatos()

            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("divi_cicl_id") = IIf(cmbCicl.Valor.Length > 0, cmbCicl.Valor, DBNull.Value)
                .Item("divi_nomb") = IIf(txtNomb.Valor.Length > 0, txtNomb.Valor, DBNull.Value)
                .Item("divi_peti_id") = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Ciclos, .Item("divi_cicl_id").ToString).Tables(0).Rows(0).Item("_carr_peti_id")
                .Item("divi_perio") = IIf(txtPerio.Valor.Length > 0, txtPerio.Valor, DBNull.Value)
            End With
        End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrDiviInsc

      With mdsDatos.Tables(mstrTabla).Rows(0)
         If .IsNull("divi_id") Then
            .Item("divi_id") = -1
         End If
      End With

      mConsultarInsc()
      Session(mstrTabla) = mdsDatos
   End Sub

#Region "Comisiones"
   Private Sub mGenerarComisiones()
      Dim ldsEstruc As DataSet = mGuardarDatosGene()

      Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), SRA_Neg.Constantes.gTab_Comisiones + "_proceso", ldsEstruc)
      lobjGenerico.Alta()
   End Sub

   Private Function mGuardarDatosGene() As DataSet
      Dim ldsEsta As DataSet = CrearDataSetGene()

      With ldsEsta.Tables(0).Rows(0)
         .Item("comi_divi_id") = hdnId.Text
      End With

      Return ldsEsta
   End Function

   Private Function CrearDataSetGene() As DataSet
      Dim ldsEsta As New DataSet
      ldsEsta.Tables.Add(SRA_Neg.Constantes.gTab_Comisiones & "_proceso")
      With ldsEsta.Tables(0)
         .Columns.Add("comi_id", System.Type.GetType("System.Int32"))
         .Columns.Add("comi_divi_id", System.Type.GetType("System.Int32"))
         .Columns.Add("comi_audi_user", System.Type.GetType("System.Int32"))
         .Rows.Add(.NewRow)
      End With
      Return ldsEsta
   End Function

   Private Sub mBorrarComisiones()
      Dim lstrCmd As New StringBuilder

      With lstrCmd
         .Append("exec ")
         .Append(SRA_Neg.Constantes.gTab_Comisiones)
         .Append("_proceso_baja @comi_divi_id=")
         .Append(hdnId.Text)
      End With

      clsSQLServer.gExecute(mstrConn, lstrCmd.ToString)

      mConsultar()
   End Sub
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi(False)
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub btnBajaComiTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaComiTodos.Click
      mBorrarComisiones()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub


   Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
      Try
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @cicl_inse_id=" + mintInse.ToString)
         lstrCmd.Append(", @cicl_anio=" + clsSQLServer.gFormatArg(txtAnioFil.Text, SqlDbType.Int))
         lstrCmd.Append(", @divi_cicl_id=" + cmbCiclFil.Valor.ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         txtAnio.Enabled = grdDato.Items.Count = 0
         cmbCicl.Enabled = grdDato.Items.Count = 0
         txtPerio.Enabled = grdDato.Items.Count = 0

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub cmbCicl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCicl.SelectedIndexChanged
      Try
         mSetearCiclDesc()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearCiclDesc()
      txtCiclDesc.Text = SRA_Neg.Utiles.ObtenerDescCiclo(mstrConn, cmbCicl.Valor.ToString)
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "Divisiones"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&fkvalor=" + Request.QueryString("fkvalor")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Detalle"
   Public Sub mEditarDatosInsc(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrInsc As DataRow

         hdnDiinId.Text = E.Item.Cells(1).Text

         mdsDatos.Tables(mstrDiviInsc).Select("diin_id=" & hdnDiinId.Text)(0).Delete()
         mConsultarInsc()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Public Sub grdInsc_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdInsc.EditItemIndex = -1
         If (grdInsc.CurrentPageIndex < 0 Or grdInsc.CurrentPageIndex >= grdInsc.PageCount) Then
            grdInsc.CurrentPageIndex = 0
         Else
            grdInsc.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarInsc()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub btnBajaInscTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaInscTodos.Click
      Try

         For Each ldrDiin As DataRow In mdsDatos.Tables(mstrDiviInsc).Select
            ldrDiin.Delete()
         Next

         grdInsc.CurrentPageIndex = 0
         mConsultarInsc()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub btnGeneInsc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGeneInsc.Click
      Dim ldrDatos As DataRow

      Try
                If cmbCicl.Valor Is DBNull.Value Or cmbCicl.Valor.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el ciclo.")
                End If

                If txtPerio.Valor Is DBNull.Value Or txtPerio.Valor.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el período.")
                End If

         Dim lstrPagina As String = "DivisionesInsc_Pop.aspx?cicl_id=" + cmbCicl.Valor.ToString + "&perio=" + txtPerio.Valor.ToString + "&divi_id=" + clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

			clsWeb.gGenerarPopUp(Me, lstrPagina, 650, 400, 10, 75, True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
      Try
         If (hdnDatosPop.Text <> "") Then
            Dim lvrDatos() As String = hdnDatosPop.Text.Split(",")
            Dim ldrDatos As DataRow
            Dim ldrInsc As DataRow

            For i As Integer = 0 To lvrDatos.GetUpperBound(0)
               ldrDatos = mdsDatos.Tables(mstrDiviInsc).NewRow
               ldrDatos.Item("diin_id") = clsSQLServer.gObtenerId(ldrDatos.Table, "diin_id")

               With ldrDatos
                  .Item("diin_insc_id") = lvrDatos(i)

                  ldrInsc = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Inscripciones, .Item("diin_insc_id").ToString).Tables(0).Rows(0)
                  .Item("_insc_desc") = ldrInsc.Item("_alumno")
                  .Item("_edad") = ldrInsc.Item("_edad")
                  .Item("_mate_desc") = ldrInsc.Item("_mate_descrip")
               End With

               ldrDatos.Table.Rows.Add(ldrDatos)
            Next

            hdnDatosPop.Text = ""
            mConsultarInsc()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultarInsc()
      grdInsc.DataSource = mdsDatos.Tables(mstrDiviInsc)
      grdInsc.DataBind()
   End Sub
#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnGeneComi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGeneComi.Click
      mModi(True)
   End Sub
End Class
End Namespace
