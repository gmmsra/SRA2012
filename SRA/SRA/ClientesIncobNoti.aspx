<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ClientesIncobNoti" CodeFile="ClientesIncobNoti.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Notificaci�n a Clientes Incobrables</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<SCRIPT language="javascript">		
		function mConfirmar()
		{
			if (document.all('cmbRepo').value == 'M')
			{
				if (document.all('cmbMail').value == '')
				{				
					alert('Debe indicar un modelo de mail.');
					document.all('cmbMail').focus();
					return(false);
				}
				else
				{
					return(confirm('Confirma el env�o de los correos?'));
				}
			}
			else
				return(true);
		}
		function mSetearTipo()
		{
			if (document.all('cmbRepo')!=null)
			{
				if (document.all('cmbRepo').value=='M')
				{
					document.all('lblMail').style.display='';
					document.all('cmbMail').style.display='';					
				}
				else
				{
					document.all('lblMail').style.display='none';
					document.all('cmbMail').style.display='none';
				}
				if (document.all('cmbRepo').value=='E')
				{
					document.all('panDire').style.display='';
				}
				else
				{
					document.all('panDire').style.display='none';		
				}
				if (document.all('cmbRepo').value!='M')
				{
					document.all('lblRepoMail').style.display='';
					document.all('cmbRepoMail').style.display='';
					document.all('lblRepoCorreo').style.display='';
					document.all('cmbRepoCorreo').style.display='';
				}
				else
				{
					document.all('lblRepoMail').style.display='none';
					document.all('cmbRepoMail').style.display='none';
					document.all('lblRepoCorreo').style.display='none';
					document.all('cmbRepoCorreo').style.display='none';
				}				
			}
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" width="100%" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Notificaci�n a Clientes Incobrables</asp:label></TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD vAlign="top" width="100%" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD width="100%"><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																				MaxValor="2099"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" align="center" width="100%" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" AutoGenerateColumns="False"
										width="100%">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="lsin_id" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="lsin_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="lsin_obse" HeaderText="Observaciones"></asp:BoundColumn>
											<asp:BoundColumn DataField="_esta_desc" HeaderText="Estado"></asp:BoundColumn>
											<asp:BoundColumn DataField="lsin_cierre_fecha" HeaderText="Fecha Cierre" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" width="100%" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="2">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" width="15%">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																		<TD align="right">
																			<asp:Label id="lblCierreFecha" runat="server" cssclass="titulo">Fecha de cierre:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtCierreFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
																		<TD style="HEIGHT: 32px" height="32">
																			<asp:CheckBox id="chkIncl" enabled="false" Text="Clientes en otras listas?" Runat="server" CssClass="titulo"></asp:CheckBox>&nbsp;</TD>
																		<TD style="HEIGHT: 32px" align="right">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 32px" height="32">
																			<cc1:combobox class="combo" id="cmbTipo" runat="server" Width="180px" enabled="false" Obligatorio="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD colSpan="3">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotextolibredeshab" Width="100%" Height="50px"
																				enabled="false" EnterPorTab="False" TextMode="MultiLine" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="left" colSpan="4">
																			<TABLE id="Table3" height="34" border="0">
																				<TR>
																					<TD noWrap>
																						<asp:label id="lblRepo" runat="server" cssclass="titulo">Tipo de Reporte:</asp:label></TD>
																					<TD>
																						<cc1:combobox class="combo" id="cmbRepo" runat="server" Width="120px" onchange="javascript:mSetearTipo();">
																							<asp:ListItem Value="R" Selected="True">Reporte</asp:ListItem>
																							<asp:ListItem Value="E">Etiquetas</asp:ListItem>
																							<asp:ListItem Value="M">Mails</asp:ListItem>
																						</cc1:combobox>&nbsp;
																					</TD>
																					<TD>
																						<DIV id="panDire" noWrap>
																							<asp:checkbox id="chkDire" Text="Con Direcci�n" Runat="server" CssClass="titulo"></asp:checkbox></DIV>
																					</TD>
																					<TD width="10"></TD>
																					<TD>
																						<asp:label id="lblMail" runat="server" cssclass="titulo">Modelo:</asp:label></TD>
																					<TD>
																						<cc1:combobox class="combo" id="cmbMail" runat="server" Width="240px"></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD noWrap align="right">
																						<asp:label id="lblRepoMail" runat="server" cssclass="titulo">e-Mail:</asp:label></TD>
																					<TD>
																						<cc1:combobox class="combo" id="cmbRepoMail" runat="server" Width="120px">
																							<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
																							<asp:ListItem Value="1">Con</asp:ListItem>
																							<asp:ListItem Value="0">Sin</asp:ListItem>
																						</cc1:combobox></TD>
																					<TD colSpan="3">
																						<asp:label id="lblRepoCorreo" runat="server" cssclass="titulo">Correo:</asp:label>
																						<cc1:combobox class="combo" id="cmbRepoCorreo" runat="server" Width="120px">
																							<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
																							<asp:ListItem Value="0">Con</asp:ListItem>
																							<asp:ListItem Value="1">Sin</asp:ListItem>
																						</cc1:combobox></TD>
																					<TD></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="4">
																			<asp:datagrid id="grdDeta" runat="server" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdDeta_PageChanged"
																				AutoGenerateColumns="False" width="95%">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="3%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:CheckBox ID="chkSel" Runat="server"></asp:CheckBox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="inco_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_clie_apel" HeaderText="Cliente"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
																					<asp:BoundColumn DataField="inco_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="inco_impo" HeaderText="Importe"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_esta_desc" HeaderText="Estado"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_mail" HeaderText="Mail"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%" border="0">
												<TR height="40">
													<TD vAlign="bottom" align="right"><A id="editar" name="editar"></A>
														<CC1:BotonImagen id="btnEnvio" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
															ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnEnvi.gif"
															ImageOver="btnEnvi2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnEnvi.gif" visible="true"
															ImageDisable="btnEnvi0.gif"></CC1:BotonImagen>&nbsp;&nbsp;
													</TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		mSetearTipo();
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
