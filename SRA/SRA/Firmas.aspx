<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Firmas" CodeFile="Firmas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Registro de Firmas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var gstrCria = '';
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
	    function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function mEsCriador(pCriador)
		{
		  if (pCriador!=null)
		  {
			if (pCriador.checked)
			{
				document.all('txtApel').disabled = true;
				document.all('txtDocuNume').disabled = true;
				document.all('cmbDocuTipo').disabled = true;
				document.all('txtFechaVige').value = '';
			    ActivarFecha("txtFechaVige",false);
				document.all('txtApel').value = '';
				document.all('txtDocuNume').value = '';
				document.all('cmbDocuTipo').selectedIndex = 0;
			}
			else
			{
				document.all('txtApel').disabled = false;
				document.all('txtDocuNume').disabled = false;
				document.all('cmbDocuTipo').disabled = false;
				ActivarFecha("txtFechaVige",true);				
			}
		  }
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Registro de Firmas</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3">
										<asp:panel id="panFiltros" runat="server" cssclass="titulo" width="100%" BorderWidth="1px"
											BorderStyle="none">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																	align="right">
																	<asp:Label id="lblCriaFil" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																	<UC1:CLIE id="usrCriaFil" runat="server" Ancho="800" FilDocuNume="True" MuestraDesc="False"
																		FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="Criadores" AceptaNull="false"
																		CampoVal="Criador"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																	align="right">
																	<asp:label id="lblApelFil" runat="server" cssclass="titulo">Apellido/Raz�n Social:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																	<CC1:TEXTBOXTAB id="txtApelFil" runat="server" cssclass="cuadrotexto" Width="300px"></CC1:TEXTBOXTAB>&nbsp;</TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																	align="right">
																<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																	<asp:checkbox id="chkBusc" Text="Buscar en..." CssClass="titulo" Runat="server"></asp:checkbox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																	align="right">
																	<asp:label id="lblDocuFil" runat="server" cssclass="titulo">Nro. Documento:&nbsp;</asp:label></TD>
																<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																	<cc1:combobox id="cmbDocuTipoFil" class="combo" runat="server" AceptaNull="False" Width="100px"></cc1:combobox>
																	<cc1:numberbox id="txtDocuNumeFil" runat="server" cssclass="cuadrotexto" Width="143px" esdecimal="False"
																		MaxValor="9999999999999"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel>
									</TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right" colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="DataGrid_Page"
											OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="autz_id" ReadOnly="True" HeaderText="Id"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="autz_clie_id" ReadOnly="True" HeaderText="Nro.Cliente"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_criador" ReadOnly="True" HeaderText="Raza/Criador"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_cliente" ReadOnly="True" HeaderText="Autorizante"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="autz_nyap" ReadOnly="True" HeaderText="Nombre y Apellido"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colspan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="middle" width="100" colspan="3">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
											ForeColor="Transparent" ToolTip="Agregar un Nuevo Expediente" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right"></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderWidth="1px" BorderStyle="Solid"
												Visible="False" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="middle" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<UC1:CLIE id="usrCria" runat="server" Ancho="800" FilDocuNume="True" MuestraDesc="False" FilTipo="S"
																				FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="Criadores" AceptaNull="true"
																				CampoVal="Criador" obligatorio="false"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right"></TD>
																		<TD colSpan="2">
																			<asp:CheckBox id="chkEsCria" onclick="javascript:mEsCriador(this)" Text="Es la firma del Criador"
																				CssClass="titulo" Runat="server"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblApel" runat="server" cssclass="titulo">Nombre y Apellido:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtApel" runat="server" cssclass="cuadrotexto" Width="300px"></CC1:TEXTBOXTAB>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblDocuTipo" runat="server" cssclass="titulo">Nro.Documento:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:combobox id="cmbDocuTipo" class="combo" runat="server" AceptaNull="True" Width="90px"></cc1:combobox>&nbsp;
																			<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="143px" esdecimal="False"
																				MaxValor="9999999999999"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblFechaVige" runat="server" cssclass="titulo">Fecha de Vigencia:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:DateBox id="txtFechaVige" runat="server" cssclass="cuadrotexto" AceptaNull="True" Width="70px"
																				obligatorio="false"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblFirm" runat="server" cssclass="titulo">Firma:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtFirma" runat="server" cssclass="cuadrotexto" Width="350px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG style="CURSOR: hand" id="imgDelFirm" onclick="javascript:mLimpiarPath('txtFirma','imgDelFirm');"
																				alt="Limpiar Firma" src="imagenes\del.gif" runat="server">&nbsp;<BUTTON style="WIDTH: 90px" id="btnFirmaVer" class="boton" runat="server" value="Ver Firma">Ver 
																				Firma</BUTTON><BR>
																			<INPUT style="WIDTH: 350px" id="filFirma" type="file" name="filFirma" runat="server">
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="300px" Height="41px" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel>
											<ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
										</DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["usrClieFil_txtCodi"]!= null && !document.all["usrClieFil_txtCodi"].disabled)
			document.all["usrClieFil_txtCodi"].focus();		
			
		gSetearTituloFrame('Registro de Firmas');
		mEsCriador(document.all('chkEsCria'));
		
		</SCRIPT>
	</BODY>
</HTML>
