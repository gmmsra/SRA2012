Namespace SRA

Partial Class ProcesosBajaMoti
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents btnList As NixorControls.BotonImagen
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label
   Protected WithEvents Combobox1 As NixorControls.ComboBox
   Protected WithEvents Combobox2 As NixorControls.ComboBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "rg_procesos_estados"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mCargarCombos()
            mSetearEventos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "rg_procesos_cargar", cmbProce, "id", "descrip", "S")
      clsWeb.gCargarCombo(mstrConn, "rg_procesos_cargar", cmbProceFil, "id", "descrip", "S")
      clsWeb.gCargarCombo(mstrConn, "baja_motivos_cargar", cmbBaja, "id", "descrip", "S")
      clsWeb.gCargarCombo(mstrConn, "baja_motivos_cargar", cmbBajaFil, "id", "descrip", "S")
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
        Try
            grdDato.DataSource = ""
            grdDato.CurrentPageIndex = 0
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd += " @pres_rpro_id=" + clsSQLServer.gFormatArg(cmbProceFil.Valor, SqlDbType.Int)
            mstrCmd += ",@pres_bamo_id=" + clsSQLServer.gFormatArg(cmbBajaFil.Valor, SqlDbType.Int)
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
      mstrCmd = "exec " + mstrTabla + "_consul"
      mstrCmd += " " + hdnId.Text

      Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      If ldsDatos.Tables(0).Rows.Count > 0 Then
         With ldsDatos.Tables(0).Rows(0)
            hdnId.Text = .Item("pres_id")
            cmbBaja.Valor = .Item("pres_bamo_id")
            cmbProce.Valor = .Item("pres_rpro_id")
            cmbTipo.Valor = .Item("pres_vali_fecha")
         End With
         mSetearEditor(False)
         mMostrarPanel(True)
      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      Dim lstrOpci As String

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      
      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      cmbBaja.Limpiar()
      cmbProce.Limpiar()
      cmbTipo.Limpiar()
      mSetearEditor(True)
      mCrearDataSet("")
   End Sub

   Private Sub mLimpiarFil()
      cmbBajaFil.Limpiar()
      cmbProceFil.Limpiar()
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Function mGuardarDatos() As DataSet
      mValidaDatos()
      Try
         Dim ldsDatos As DataSet

         ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla)

         With ldsDatos.Tables(0).Rows(0)
            .Item("pres_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("pres_bamo_id") = cmbBaja.Valor
            .Item("pres_rpro_id") = cmbProce.Valor
            .Item("pres_vali_fecha") = cmbTipo.Valor
         End With
         Return ldsDatos
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Function
   Private Sub mAlta()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Alta()

         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Modi()

         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      mConsultar()
   End Sub
#End Region

End Class
End Namespace
