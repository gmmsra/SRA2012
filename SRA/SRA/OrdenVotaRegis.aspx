<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.OrdenVotaRegis" CodeFile="OrdenVotaRegis.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Acreditaciones por Asamblea</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3" style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Acreditaciones por Asamblea</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD vAlign="top"><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblAsam" runat="server" cssclass="titulo">Asamblea:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbAsam" runat="server" Width="350px" AutoPostback="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblSociFil" runat="server" cssclass="titulo">Socio:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrSociFil" runat="server" width="100%" AutoPostback="True" FilDocuNume="True"
																				MuestraDesc="False" PermiModi="True" FilSociNume="True" Saltos="1,1,1" CampoVal="Socio"
																				Tabla="Socios" FilCUIT="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" width="99%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Height="116px" Visible="False">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD></TD>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
													<TD vAlign="top" align="right">&nbsp;
														<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" align="right" colSpan="3">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblDatos" runat="server" cssclass="titulo">Datos del Titular:</asp:Label>&nbsp;
																	</TD>
																	<TD width="100%">
																		<CC1:TEXTBOXTAB id="txtDatos" style="FONT-WEIGHT: bold" runat="server" cssclass="cuadrotexto" Width="100%"
																			Height="80px" Enabled="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD></TD>
																	<TD>
																		<asp:Label id="lblEsta" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																</TR>
																<TR>
																	<TD></TD>
																	<TD>
																		<asp:Label id="lblCate" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblApodFirm" runat="server" cssclass="titulo">Apoderados y Firmantes:</asp:Label>&nbsp;</TD>
																	<TD>
																		<cc1:combobox class="combo" id="cmbApodFirm" runat="server" Width="100%" autopostback="true"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblVoto" runat="server" cssclass="titulo">Vino:</asp:Label>&nbsp;</TD>
																	<TD>
																		<cc1:combobox class="combo" id="cmbVoto" runat="server" Width="80px" Obligatorio="True"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblDatosVotante" runat="server" cssclass="titulo">Datos del Votante:</asp:Label>&nbsp;
																	</TD>
																	<TD width="100%">
																		<CC1:TEXTBOXTAB id="txtDatosVotante" style="FONT-WEIGHT: bold" runat="server" cssclass="cuadrotexto"
																			Width="100%" Height="80px" Enabled="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblFirma" runat="server" cssclass="titulo">Firma:</asp:Label>&nbsp;</TD>
																	<TD>
																		<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
																			<TR>
																				<TD align="left" width="45%">
																					<asp:Image id="imgFirma" Height="100px" Runat="server"></asp:Image>
																					<BUTTON class="boton" id="btnFirmaVer" style="WIDTH: 80px" type="button" runat="server"
																					 onclick="javascript:mVerImagen('Firma');" value="Ver Firma">Ver Firma</BUTTON>
																				</TD>
																				<TD vAlign="top" align="right" width="180">
																					<asp:Label id="lblFoto" runat="server" cssclass="titulo">Foto:</asp:Label>&nbsp;</TD>
																				<TD width="45%">
																					<asp:Image id="imgFoto" Height="100px" Runat="server"></asp:Image>
																					<BUTTON class="boton" onclick="javascript:mVerImagen('Foto');" id="btnFotoVer" style="WIDTH: 80px" type="button" runat="server" value="Ver Foto">Ver 
																					Foto</BUTTON>
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
															</TABLE>
														</asp:panel>
														<ASP:PANEL id="panBotones" Runat="server">
															<TABLE width="100%">
																<TR>
																	<TD align="center">
																		<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																</TR>
																<TR height="30">
																	<TD align="center"><A id="editar" name="editar"></A>
																		<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Modificar"></asp:Button></TD>
																</TR>
															</TABLE>
														</ASP:PANEL></TD>
												</TR>
											</TABLE>
										</asp:panel>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSociId" runat="server"></asp:textbox>
				<asp:textbox id="hdnFoto" runat="server"></asp:textbox>
				<asp:textbox id="hdnFirma" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
			if (document.all["editar"]!= null)
				document.location = '#editar';
            
            function mVerImagen(pTipo)
            {
				if (pTipo == 'Foto')
					gAbrirVentanas('fotos/' + document.all('hdnFoto').value);
					else
					gAbrirVentanas('fotos/' + document.all('hdnFirma').value);
			}
        
        </SCRIPT>
	</BODY>
</HTML>
