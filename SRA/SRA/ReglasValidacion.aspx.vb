Imports System.Data.SqlClient


Namespace SRA


Partial Class ReglasValidacion
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dblOpciones As NixorControls.DobleLista
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents txtcmbRaza As NixorControls.TextBoxTab


    Protected WithEvents lblRazas As System.Web.UI.WebControls.Label
    Protected WithEvents ListaRazas As NixorControls.Lista
    Protected WithEvents ComboBox1 As NixorControls.ComboBox
    Protected WithEvents cmbRaza As NixorControls.ComboBox
    Protected WithEvents btnEliminarRaza As System.Web.UI.WebControls.Button
    Protected WithEvents btnAgregarRaza As System.Web.UI.WebControls.Button

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "rg_reglas_vali"
   Private mstrTablaProc As String = "reglas_procesos"
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "rg_mensajes", cmbMensaje, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_formulas", cmbFormula, "")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_mensajes", cmbMsgFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_procesos", cmbProcesos, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_procesos", cmbProcFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_reglas_vali_agrupa", cmbAgru, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_reglas_vali_campos", cmbCamp, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_reglas_vali_tablas", cmbMini, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_reglas_vali_tablas", cmbMaxi, "S")

        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "S")
        SRA_Neg.Utiles.gSetearRaza(cmbRaza)
   End Sub
   Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnMens.Attributes.Add("onclick", "mMensajes();return false;")
        btnCond.Attributes.Add("onclick", "mCondicion();return false;")
       
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrDenuLong As Object
      Dim lstrDetaLong As Object
      'lstrDenuLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      'txtFolio.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero

      'lstrDetaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)
      'txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "sdde_obser")
    End Sub
 
    Protected Sub btnAgregarRaza_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim objRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())

        ListaRazas.Items.Add(objRaza.GetRazaCodiById(cmbRaza.Valor).ToString() + "-" + cmbRaza.SelectedItem.Text)

    End Sub

    Protected Sub btnEliminarRaza_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim objRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())

        ListaRazas.Items.Remove(ListaRazas.SelectedItem)

    End Sub




#End Region

#Region "Inicializacion de Variables"
   'Public Sub mInicializar()
   '   clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
   '   grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   'End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.EditItemIndex = -1
         If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
            grdDeta.CurrentPageIndex = 0
         Else
            grdDeta.CurrentPageIndex = E.NewPageIndex
         End If
         grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
         grdDeta.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
        Try
            
            Dim lstrCmd As String
            Dim dsDatos As New DataSet

            mstrCmd = "exec " + mstrTabla + "_consul @formato='1'"
            mstrCmd = mstrCmd + ",@regv_codi=" + IIf(txtCodigoFil.Valor.ToString = "", "null", txtCodigoFil.Text)
            mstrCmd = mstrCmd + ",@regv_desc='" + txtDescFil.Valor.ToString + "'"
            mstrCmd = mstrCmd + ",@regv_rmen_id=" + cmbMsgFil.Valor.ToString
            mstrCmd = mstrCmd + ",@rpro_id=" + cmbProcFil.Valor.ToString

            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)

            grdDato.DataSource = dsDatos
            grdDato.DataBind()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
      btnBajaDeta.Enabled = Not (pbooAlta)
      btnModiDeta.Enabled = Not (pbooAlta)
      btnAltaDeta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtCodigo.Valor = .Item("regv_codi")
            txtDesc.Valor = .Item("regv_desc")
            cmbFormula.Valor = .Item("regv_rfor_id")
            cmbMensaje.Valor = .Item("regv_rmen_id")

            If .Item("regv_sexo") Is DBNull.Value Then
                cmbSexo.Limpiar()
            Else
                cmbSexo.Valor = IIf(.Item("regv_sexo"), "1", "0")
            End If
            If .Item("regv_obli") Is DBNull.Value Then
                chkObli.Checked = False
            Else
                chkObli.Checked = .Item("regv_obli")
            End If
            If .Item("regv_manual") Is DBNull.Value Then
                chkManu.Checked = False
            Else
                chkManu.Checked = .Item("regv_manual")
            End If
                    txtVigeDesde.Fecha = .Item("regv_vigen_desde").ToString
                    txtVigeHasta.Fecha = .Item("regv_vigen_hasta").ToString
                    cmbCamp.Valor = .Item("regv_campo").ToString
                    cmbAgru.Valor = .Item("regv_agru").ToString
                    txtOrde.Valor = .Item("regv_orden").ToString

                    cmbMini.Valor = .Item("regv_mini_rvta_id").ToString
                    cmbMaxi.Valor = .Item("regv_maxi_rvta_id").ToString
                    If .Item("regv_mini_rvta_id").ToString.Length > 0 Then
                        txtMini.Valor = .Item("regv_mini_valor")
                        txtMini.Enabled = True
                    Else
                        txtMini.Valor = ""
                        txtMini.Enabled = False
                    End If
                    If .Item("regv_maxi_rvta_id").ToString.Length > 0 Then
                        txtMaxi.Valor = .Item("regv_maxi_valor")
                        txtMaxi.Enabled = True
                    Else
                        txtMaxi.Valor = ""
                        txtMaxi.Enabled = False
                    End If

                    If .Item("regv_baja_fecha").ToString.Length > 0 Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("regv_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub
   
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If grdDeta.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un Proceso")
        End If
    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("regv_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("regv_codi") = txtCodigo.Valor
            .Item("regv_desc") = txtDesc.Valor
            .Item("regv_rfor_id") = cmbFormula.Valor
            .Item("regv_rmen_id") = cmbMensaje.Valor
            .Item("regv_baja_fecha") = DBNull.Value
            .Item("regv_audi_user") = Session("sUserId").ToString()
            Select Case cmbSexo.Valor
                Case "0"
                    .Item("regv_sexo") = False
                Case "1"
                    .Item("regv_sexo") = True
                Case Else
                    .Item("regv_sexo") = DBNull.Value
            End Select
            .Item("regv_obli") = chkObli.Checked
                .Item("regv_manual") = chkManu.Checked
                If txtVigeDesde.Fecha.ToString.Length > 0 Then
                    .Item("regv_vigen_desde") = txtVigeDesde.Fecha
                End If
                If txtVigeHasta.Fecha.ToString.Length > 0 Then
                    .Item("regv_vigen_hasta") = txtVigeHasta.Fecha
                End If
                If cmbCamp.Valor.ToString.Length > 0 Then
                    .Item("regv_campo") = cmbCamp.Valor
                End If
                If cmbAgru.Valor.ToString.Length > 0 Then
                    .Item("regv_agru") = cmbAgru.Valor
                End If
                If txtOrde.Valor.ToString.Length > 0 Then
                    .Item("regv_orden") = txtOrde.Valor
                End If
                If txtMini.Valor.ToString.Length > 0 Then
                    .Item("regv_mini_valor") = txtMini.Valor
                End If
                If txtMaxi.Valor.ToString.Length > 0 Then
                    .Item("regv_maxi_valor") = txtMaxi.Valor
                End If
                If cmbMini.Valor.ToString.Length > 0 Then
                    .Item("regv_mini_rvta_id") = cmbMini.Valor
                End If
                If cmbMaxi.Valor.ToString.Length > 0 Then
                    .Item("regv_maxi_rvta_id") = cmbMaxi.Valor
                End If
            End With
    End Sub
    Public Sub mEditarDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDire As DataRow

            hdnDetaId.Text = E.Item.Cells(1).Text
            ldrDire = mdsDatos.Tables(mstrTablaProc).Select("repr_id=" & hdnDetaId.Text)(0)

            With ldrDire
                cmbProcesos.Valor = .Item("repr_rpro_id")

                'If Not .IsNull("repr_baja_fecha") Then
                '   lblBajaDeta.Text = "Direcci�n dada de baja en fecha: " & CDate(.Item("repr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                'Else
                '   lblBajaDeta.Text = ""
                'End If
            End With
            mSetearEditorDeta(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mActualizarDeta()
        Try
            mGuardarDatosDeta()

            mLimpiarDeta()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
            grdDeta.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaDeta()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaProc).Select("repr_id=" & hdnDetaId.Text)(0)
            row.Delete()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
            grdDeta.DataBind()
            mLimpiarDeta()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosDeta()
        Dim ldrProc As DataRow
        Dim vbooDefault As Boolean

        If cmbProcesos.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar un Proceso.")
        End If

        If hdnDetaId.Text = "" Then
            ldrProc = mdsDatos.Tables(mstrTablaProc).NewRow
            ldrProc.Item("repr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaProc), "repr_id")
        Else
            ldrProc = mdsDatos.Tables(mstrTablaProc).Select("repr_id=" & hdnDetaId.Text)(0)
        End If

        With ldrProc
            .Item("repr_id") = clsSQLServer.gFormatArg(hdnDetaId.Text, SqlDbType.Int)
            .Item("repr_regv_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("repr_rpro_id") = cmbProcesos.Valor
            .Item("_rpro_desc") = cmbProcesos.SelectedItem.Text
            .Item("repr_audi_user") = Session("sUserId").ToString()
            .Item("repr_baja_fecha") = DBNull.Value
        End With
        If (hdnDetaId.Text = "") Then
            mdsDatos.Tables(mstrTablaProc).Rows.Add(ldrProc)
        End If
    End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panCabecera.Visible = False
         lnkCabecera.Font.Bold = False
         panDeta.Visible = False
         lnkDeta.Font.Bold = False
         Select Case Tab
            Case 1
               panCabecera.Visible = True
               lnkCabecera.Font.Bold = True
            Case 2
               panDeta.Visible = True
               lnkDeta.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiarFiltros()
        txtCodigoFil.Text = ""
        txtDescFil.Text = ""
        cmbMsgFil.Limpiar()
        cmbProcFil.Limpiar()
        mMostrarPanel(False)
    End Sub
    Private Sub mLimpiar()

        hdnId.Text = ""
        txtCodigo.Text = clsSQLServer.gObtenerValorCampo(mstrConn, "rg_reglas_vali_prox_codi", "", "prox_regv_codi")
        txtDesc.Text = ""
        txtVigeDesde.Text = ""
        txtVigeHasta.Text = ""
        cmbFormula.Limpiar()
        cmbMensaje.Limpiar()
        lblBaja.Text = ""

        cmbSexo.Limpiar()
        cmbCamp.Limpiar()
        cmbAgru.Limpiar()

        chkObli.Checked = False
        chkManu.Checked = False

        txtOrde.Valor = ""
        txtMini.Valor = ""
        txtMaxi.Valor = ""

        mLimpiarDeta()

        grdDato.CurrentPageIndex = 0
        grdDeta.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarDeta()
        hdnDetaId.Text = ""
        cmbProcesos.Limpiar()
        mSetearEditorDeta(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaProc

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdDeta.DataSource = mdsDatos.Tables(mstrTablaProc)
        grdDeta.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    'Botones generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones de Procesos
    Private Sub btnAltaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
        mActualizarDeta()
    End Sub
    Private Sub btnBajaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        mBajaDeta()
    End Sub
    Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        mActualizarDeta()
    End Sub
    Private Sub btnLimpDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        mLimpiarDeta()
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDirecciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
        mShowTabs(2)
    End Sub
#End Region

   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "ReglasValidacion"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If (txtCodigoFil.Valor.ToString <> "") Then
                lstrRpt += "&regv_codi=" + txtCodigoFil.Valor.ToString
            End If
            'If (txtDescFil.Valor.ToString <> "") Then
            lstrRpt += "&regv_desc=" + txtDescFil.Valor.ToString
            'End If

            lstrRpt += "&regv_rmen_id=" + IIf(cmbMsgFil.Valor.ToString = "", "0", cmbMsgFil.Valor.ToString)
            lstrRpt += "&rpro_id=" + IIf(cmbProcFil.Valor.ToString = "", "0", cmbProcFil.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub




    Private Sub ListaRazas_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListaRazas.PreRender
        Session("event_controle") = (CType(sender, NixorControls.Lista))
    End Sub




    Private Sub ListaRazas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListaRazas.SelectedIndexChanged

        Try
            'If Not Session("event_controle") Is Nothing Then


            '        Dim controle As NixorControls.Lista =(Lista) Session("event_controle")

            '    controle.Focus()

            'End If

        Catch inEx As InvalidCastException

        End Try



    End Sub
End Class
End Namespace
