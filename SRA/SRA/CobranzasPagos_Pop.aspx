<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CobranzasPagos_Pop" CodeFile="CobranzasPagos_Pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PAGOS</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="Salida();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:label></TD>
								<TD vAlign="top" align="right">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"></asp:imagebutton></TD>
							</TR>
							<tr>
								<td colSpan="2">
									<table>
										<TR>
											<TD align="right" width="15%"><asp:label id="lblCabeTotalApl" runat="server" cssclass="titulo">Total a Pagar:</asp:label>&nbsp;
											</TD>
											<TD width="15%"><asp:label id="lblCabeTotalAplic" runat="server" cssclass="titulo"></asp:label></TD>
											<TD align="right" width="20%"><asp:label id="lblCabeTotalPag" runat="server" cssclass="titulo">Total Pagado:</asp:label>&nbsp;
											</TD>
											<TD width="15%"><asp:label id="lblCabeTotalPago" runat="server" cssclass="titulo"></asp:label></TD>
											<TD align="right" width="10%"><asp:label id="lblCabeTotalDif" runat="server" cssclass="titulo">Diferencia:</asp:label>&nbsp;
											</TD>
											<TD width="10%"><asp:label id="lblCabeTotalDifer" runat="server" cssclass="titulo"></asp:label></TD>
										</TR>
									</table>
								</td>
							</tr>
							<TR>
								<TD onclick="Variable=1;" vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" AllowPaging="True" BorderWidth="1px" HorizontalAlign="Center"
										CellSpacing="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" GridLines="None" CellPadding="1" AutoGenerateColumns="False" BorderStyle="None">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="paco_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="_mone_desc" HeaderText="Mon"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_orig_impo" HeaderText="Impo.Ori." DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_tarj_desc" HeaderText="Tarjeta"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_dine_desc" HeaderText="Din.Elec."></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="paco_tarj_nume" HeaderText="Nro.Tarj."></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="paco_nume" HeaderText="Nro.Cup�n:"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="paco_tarj_autori" HeaderText="C�d.Aut.:"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="paco_tarj_cuot" HeaderText="Cuotas"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="cheq_teor_depo_fecha" DataFormatString="{0:dd/MM/yyyy}"
												HeaderText="Fecha"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_chti_desc" HeaderText="Tipo"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_clea_desc" HeaderText="Cle."></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_plaza_desc" HeaderText="Plaza"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_banc_desc" HeaderText="Banco"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="cheq_nume" HeaderText="Nro.Cheque"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_cuba_desc" HeaderText="Cuenta"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="paco_acre_depo_fecha" DataFormatString="{0:dd/MM/yyyy}"
												HeaderText="F. Deposito"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD colSpan="2"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderWidth="1px" BorderStyle="Solid">
											<TABLE style="WIDTH: 100%" id="Table2" class="FdoFld" border="0" cellPadding="0" align="left"
												runat="server">
												<TR id="trTarj" runat="server">
													<TD width="25%" align="right">
														<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:combobox id="cmbTarj" class="combo" runat="server" Width="300px"></cc1:combobox></TD>
												</TR>
												<TR id="TrNume" runat="server">
													<TD align="right">
														<asp:Label id="lblNume" runat="server" cssclass="titulo">C�digo de Autorizaci�n:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:textboxtab id="txtNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab></TD>
												</TR>
												<TR id="trTarjNume" runat="server">
													<TD align="right">
														<asp:Label id="lblTarjNume" runat="server" cssclass="titulo">Nro.Tarjeta:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:textboxtab id="txtTarjNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab></TD>
												</TR>
												<TR id="TrTarjCuot" runat="server">
													<TD align="right">
														<asp:Label id="lblTarjCuot" runat="server" cssclass="titulo">Cuotas:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:numberbox id="txtTarjCuot" runat="server" cssclass="cuadrotexto" Width="40px" onchange="mChequearAutoriz();"></cc1:numberbox></TD>
												</TR>
												<TR id="TrRtip" runat="server">
													<TD align="right">
														<asp:Label id="lblRtip" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:combobox id="cmbRtip" class="combo" runat="server" Width="200px"></cc1:combobox></TD>
												</TR>
												<TR id="TrMone" runat="server">
													<TD align="right">
														<asp:Label id="lblMone" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:combobox id="cmbMone" class="combo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></cc1:combobox>
														<asp:label id="lblCoti" runat="server" cssclass="titulo" Visible="False"></asp:label></TD>
												</TR>
												<TR id="TrBanc" runat="server">
													<TD align="right">
														<asp:Label id="lblBanc" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0">
															<TR>
																<TD>
																	<cc1:combobox id="cmbBanc" class="combo" runat="server" cssclass="cuadrotexto" Width="300px" onchange="mCargarCuentas();"
																		NomOper="bancos_cargar" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																		id="btnAvanBusq2" onclick="mBotonBusquedaAvanzada('bancos','banc_desc','cmbBanc','Bancos','@con_cuentas=1');"
																		border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR id="TrCuba" runat="server">
													<TD align="right">
														<asp:Label id="lblCuba" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:combobox id="cmbCuba" class="combo" runat="server" Width="300px"></cc1:combobox></TD>
												</TR>
												<TR id="TrCheqNume" runat="server">
													<TD align="right">
														<asp:Label id="lblCheqNume" runat="server" cssclass="titulo">Nro.Cheque:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:textboxtab id="txtCheqNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab></TD>
												</TR>
												<TR id="TrTeorDepoFecha" runat="server">
													<TD align="right">
														<asp:Label id="lblTeorDepoFecha" runat="server" cssclass="titulo">Fecha:</asp:Label></TD>
													<TD>
														<cc1:DateBox id="txtTeorDepoFecha" runat="server" cssclass="cuadrotexto" Width="80px" onchange="javascript:mVerificarDiaHabil(this);"></cc1:DateBox></TD>
												</TR>
												<TR id="TrChti" runat="server">
													<TD align="right">
														<asp:Label id="lblChti" runat="server" cssclass="titulo">Tipo Cheque:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:combobox id="cmbChti" class="combo" runat="server" Width="100px"></cc1:combobox></TD>
												</TR>
												<TR id="TrClea" runat="server">
													<TD align="right">
														<asp:Label id="lblClea" runat="server" cssclass="titulo">Clearing:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:combobox id="cmbClea" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:combobox></TD>
												</TR>
												<TR id="TrPlaza" runat="server">
													<TD align="right">
														<asp:Label id="lblPlaza" runat="server" cssclass="titulo">Plaza:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:combobox id="cmbPlaza" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:combobox></TD>
												</TR>
												<TR id="TrImpo" runat="server">
													<TD align="right">
														<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"
															AceptaNull="False" EsDecimal="true" CampoVal="Importe"></cc1:numberbox></TD>
												</TR>
												<TR id="TrOriBanc" runat="server">
													<TD align="right">
														<asp:Label id="lblOrigBanc" runat="server" cssclass="titulo">Banco Origen:</asp:Label>&nbsp;</TD>
													<TD>
														<TABLE border="0" cellPadding="0" width="100%" align="center">
															<TR>
																<TD width="100%" align="left">
																	<cc1:combobox id="cmbOrigBanc" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																		NomOper="bancos_cargar" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																		id="btnBancoBusq" language="javascript" onclick="BusqCombo('bancos','','banc_desc','cmbOrigBanc');"
																		border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif"></TD>
																<TD align="right">
																	<asp:Label id="lblOrigBancSuc" runat="server" cssclass="titulo">Suc:</asp:Label></TD>
																<TD>
																	<cc1:textboxtab id="txtOrigBancSuc" runat="server" cssclass="cuadrotexto" Width="80px"></cc1:textboxtab></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR id="TrFechaAcreDepo" runat="server">
													<TD align="right">
														<asp:Label id="lblFechaAcreDepo" runat="server" cssclass="titulo">Fecha Deposito:</asp:Label>&nbsp;
													</TD>
													<TD>
														<cc1:DateBox id="txtFechaAcreDepo" runat="server" cssclass="cuadrotexto" Width="80px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD align="right"></TD>
													<TD colSpan="3">
														<DIV style="DISPLAY: none" id="divBilletes" runat="server">
															<TABLE style="BORDER-BOTTOM: 1px solid; BORDER-LEFT: 1px solid; WIDTH: 100%; BORDER-TOP: 1px solid; BORDER-RIGHT: 1px solid"
																id="TableBill" border="0" cellPadding="0" align="center">
																<TR>
																	<TD width="100%" align="center">
																		<asp:Label id="lblBillDeno" runat="server" cssclass="titulo">Valor billete:</asp:Label>&nbsp;
																		<CC1:NUMBERBOX id="txtBillDeno" runat="server" cssclass="cuadrotexto" Width="50px"></CC1:NUMBERBOX>&nbsp;
																		<asp:Label id="lblBillNume" runat="server" cssclass="titulo">Nro:</asp:Label>&nbsp;
																		<cc1:textboxtab id="txtBillNume" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:textboxtab><BR>
																	</TD>
																</TR>
																<TR>
																	<TD onclick="Variable=1;" align="center">
																		<asp:Button id="btnAltaBill" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																		<asp:Button id="btnBajaBill" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																			Text="Baja"></asp:Button>&nbsp;&nbsp;
																		<asp:Button id="btnModiBill" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																			Text="Modificar"></asp:Button></TD>
																</TR>
																<TR>
																	<TD onclick="Variable=1;">
																		<asp:datagrid id="grdBill" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
																			CellPadding="1" GridLines="None" OnEditCommand="mEditarDatosBill" CellSpacing="1" HorizontalAlign="Center"
																			BorderWidth="1px">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn HeaderStyle-Width="2%">
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="bill_id"></asp:BoundColumn>
																				<asp:BoundColumn DataField="bill_deno" HeaderText="Valor billete"></asp:BoundColumn>
																				<asp:BoundColumn DataField="bill_nume" HeaderText="Nume."></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
															</TABLE>
														</DIV>
													</TD>
												</TR>
												<TR>
													<TD colSpan="4" align="left">
														<DIV style="DISPLAY: none" id="divTarjAutoriz" runat="server">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD width="25%" align="right">
																		<asp:Label id="lblTarjAutori" runat="server" cssclass="titulo">Autorizaci�n:</asp:Label>&nbsp;
																	</TD>
																	<TD>
																		<cc1:textboxtab id="txtTarjAutori" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:textboxtab><BR>
																	</TD>
																</TR>
															</TABLE>
														</DIV>
													</TD>
												</TR>
												<TR>
													<TD onclick="Variable=1;" colSpan="4" align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnBillId" runat="server"></asp:textbox><asp:textbox id="hdnFechaDia" runat="server"></asp:textbox><asp:textbox id="hdnVtaTel" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		var Variable;
		Variable="";
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtBillDeno"]!= null && document.all["divBilletes"].style.display=="inline")
			document.all["txtBillDeno"].focus();
		if (document.all["txtImpo"]!= null)
			document.all["txtImpo"].focus();
		if (document.all["txtcmbBanc"]!= null)
			document.all["txtcmbBanc"].focus();
		if (document.all["txtNume"]!= null && document.all["txtNume"].value=="")
			document.all["txtNume"].focus();
		if (document.all["cmbTarj"]!= null)
		{
			if (document.all("txtTarjCuot").style.display="inline")
			{
				document.all["divTarjAutoriz"].style.display="inline";
				document.all["cmbTarj"].focus();
			}
		}	
						
		function Salida()
		{
			if(window.event.srcElement==null && Variable=="")
			{
				try
				{
					window.opener.document.all['hdnDatosPop'].value='1';
					window.opener.document.all['hdnTipoPop'].value='P';
					window.opener.__doPostBack('hdnDatosPop','');
				}
				catch(e){;}
			}
		}
		
		function mCargarCuentas()
		{
			var sFiltro;
			if(document.all["cmbCuba"]!=null)
			{
				sFiltro = "@banc_id=";
				if(document.all["cmbBanc"].value!="")
					sFiltro += document.all["cmbBanc"].value;
				else
					sFiltro += "-1"
				
				sFiltro += ",@mone_id=";
				sFiltro += document.all["cmbMone"].value;
				LoadComboXML("cuentas_bancos_cargar", sFiltro, "cmbCuba", "S");
			}
		}
		
		function mCargarDine(pObj)
		{
			LoadComboXML("dinero_elec_cargar", pObj.value, "cmbTarj", "S");
		}
		
		function cmbMone_change(pObj)
		{
			if(document.all["divBilletes"]!=null)
			{
				//if(pObj.value != "1")
				//{
					document.all["divBilletes"].style.display="inline";
				//}
				//else
				//{
				//	document.all["divBilletes"].style.display="none";
				//	document.all["txtBillNume"].value = "";
				//	document.all["txtBillDeno"].value = "";
				//}
			}
		}

		function mChequearAutoriz(pObj)
		{
			/*var lbooReqAuto=false;
			
			if(document.all("cmbTarj").value!=""&&document.all("txtTarjCuot").value!="")
			{
				var lstrRet = LeerCamposXML("tarjetas", document.all("cmbTarj").value, "tarj_max_cuota");
				//Modificado pantanetti Guillermo 20/06/2008
				lbooReqAuto = true //lstrRet<document.all("txtTarjCuot").value
			}
		
			if(lbooReqAuto)
			{
				document.all["divTarjAutoriz"].style.display="inline";
			}
			else
			{
				document.all["divTarjAutoriz"].style.display="none";
				document.all["txtTarjAutori"].value = "";
			}*/
		}

		function mVerificarDiaHabil(pFecha)
		{
			if (pFecha.value != '')
			{
				var lstrRet = LeerCamposXML("ProximoDiaHabil", gArg(pFecha.value), "fecha");
				if (pFecha.value != lstrRet)
				{
					alert('La fecha ingresada no corresponde a un d�a h�bil!');

					//23/08/2010 alert('La fecha ingresada no corresponde a un d�a h�bil, se tomar� el primer d�a h�bil que le siga.');
					//23/08/2010 deja la fecha de ingreso			pFecha.value = lstrRet;					
				}
			}
		}
		function mChequearBillete()
		{
			var vstrRet = '';
			if (document.all('txtBillNume').value != '' && document.all('txtBillDeno').value != '')
			{
				sFiltro = " @bill_nume='"+document.all('txtBillNume').value+"'";
				sFiltro = sFiltro+",@bill_deno="+document.all('txtBillDeno').value;		
				vstrRet = LeerCamposXML("billetes", sFiltro, "bill_id").split("|");
			}
			if (vstrRet != '')
				return confirm("El billete ya ha sido registrado en otra cobranza, confirma el ingreso?");
			else
				return true;
		}
		function mChequeaFechaCheque()
		{
			var oFecha,oFechaDia;
			if(document.all["txtTeorDepoFecha"]!=null)
			{
				oFecha=gFechaDate(document.all["txtTeorDepoFecha"].value);
				oFechaDia=gFechaDate(document.all["hdnFechaDia"].value);

				if(oFecha!=null && oFecha>oFechaDia)
					return confirm("El cheque tiene fecha posterior a la del d�a, confirma el ingreso?");
				else
					return true;
			}
		}
		
		</SCRIPT>
	</BODY>
</HTML>
