Namespace SRA

Partial Class PlantillasTramites
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblBajaAutor As System.Web.UI.WebControls.Label
   Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Tramites_Plantilla
   Private mstrTablaDetalle As String = SRA_Neg.Constantes.gTab_Tramites_Plantilla_Deta
   Private mstrTablaRazas As String = SRA_Neg.Constantes.gTab_Tramites_Plantilla_Razas
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaDeta.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "trap_desc")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "trap_obse")
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "rg_requisitos_cargar", cmbDetaReq, "id", "descrip_codi", "S")
      clsWeb.gCargarRefeCmb(mstrConn, "rg_tramites_tipos", cmbTipoTramiteFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "rg_tramites_tipos", cmbTipoTramite, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecie, "S")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDetalle.EditItemIndex = -1
         If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
            grdDetalle.CurrentPageIndex = 0
         Else
            grdDetalle.CurrentPageIndex = E.NewPageIndex
         End If
         grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
         grdDetalle.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridRaza_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdRazas.EditItemIndex = -1
         If (grdRazas.CurrentPageIndex < 0 Or grdRazas.CurrentPageIndex >= grdRazas.PageCount) Then
            grdRazas.CurrentPageIndex = 0
         Else
            grdRazas.CurrentPageIndex = E.NewPageIndex
         End If
         grdRazas.DataSource = mdsDatos.Tables(mstrTablaRazas)
         grdRazas.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
         mstrCmd = mstrCmd + " @trap_ttra_id=" + cmbTipoTramiteFil.Valor.ToString

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
      btnBajaDeta.Enabled = Not (pbooAlta)
      btnModiDeta.Enabled = Not (pbooAlta)
      btnAltaDeta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorRaza(ByVal pbooAlta As Boolean)
      btnBajaRaza.Enabled = Not (pbooAlta)
      btnModiRaza.Enabled = Not (pbooAlta)
      btnAltaRaza.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
                    cmbTipoTramite.Valor = .Item("trap_ttra_id").ToString
                    txtDesc.Valor = .Item("trap_desc").ToString
                    txtObse.Valor = .Item("trap_obse").ToString


            If Not .IsNull("trap_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("trap_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub
   Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDeta As DataRow

         hdnDetaId.Text = E.Item.Cells(1).Text
         ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("trpd_id=" & hdnDetaId.Text)(0)

         With ldrDeta
            cmbDetaReq.Valor = .Item("trpd_requ_id")
            chkDetaOblig.Checked = .Item("trpd_obli")
            If Not .IsNull("trpd_baja_fecha") Then
               lblBajaDeta.Text = "Registro dado de baja en fecha: " & CDate(.Item("trpd_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaDeta.Text = ""
            End If
         End With
         mSetearEditorDeta(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosRaza(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDeta As DataRow

         hdnRazaId.Text = E.Item.Cells(1).Text
         ldrDeta = mdsDatos.Tables(mstrTablaRazas).Select("trdr_id=" & hdnRazaId.Text)(0)

         With ldrDeta
            cmbEspecie.Valor = .Item("trdr_espe_id")
            clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")
            cmbRaza.Valor = .Item("trdr_raza_id")

            If Not .IsNull("trdr_baja_fecha") Then
               lblBajaRaza.Text = "Registro dado de baja en fecha: " & CDate(.Item("trdr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaRaza.Text = ""
            End If
         End With
         mSetearEditorRaza(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaDetalle
      mdsDatos.Tables(2).TableName = mstrTablaRazas

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
      grdDetalle.DataBind()

      grdRazas.DataSource = mdsDatos.Tables(mstrTablaRazas)
      grdRazas.DataBind()

      Session(mstrTabla) = mdsDatos
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      cmbTipoTramite.Limpiar()
      txtDesc.Text = ""
      txtObse.Text = ""
      lblBaja.Text = ""

      mLimpiarDeta()
      mLimpiarRaza()

      grdDato.CurrentPageIndex = 0
      grdDetalle.CurrentPageIndex = 0
      grdRazas.CurrentPageIndex = 0

      mCrearDataSet("")
      mShowTabs(1)
      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarDeta()
      hdnDetaId.Text = ""
      cmbDetaReq.Limpiar()
      chkDetaOblig.Checked = False
      lblBajaDeta.Text = ""
      mSetearEditorDeta(True)
   End Sub
   Private Sub mLimpiarRaza()
      hdnRazaId.Text = ""
      cmbEspecie.Limpiar()
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")
      lblBajaRaza.Text = ""
      mSetearEditorRaza(True)
   End Sub
   Private Sub mLimpiarFil()
      cmbTipoTramiteFil.Limpiar()
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      'If mdsDatos.Tables(mstrTablaDetalle).Select("trpd_baja_fecha is null").GetUpperBound(0) = -1 Then
      '   Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un Requisito para la Plantilla.")
      'End If
      If mdsDatos.Tables(mstrTablaRazas).Select("trdr_baja_fecha is null").GetUpperBound(0) = -1 Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar al menos una Especie o Raza.")
      End If
   End Sub
   Private Sub mGuardarDatos()
      mValidaDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("trap_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("trap_ttra_id") = cmbTipoTramite.Valor
         .Item("trap_desc") = txtDesc.Valor
         .Item("trap_obse") = txtObse.Valor
         .Item("trap_baja_fecha") = DBNull.Value
         .Item("trap_audi_user") = Session("sUserId").ToString()
      End With
   End Sub
   'Seccion de detalle
   Private Sub mActualizarDeta()
      Try
         mGuardarDatosDeta()

         mLimpiarDeta()
         grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
         grdDetalle.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaDeta()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("trpd_id=" & hdnDetaId.Text)(0)
         row.Delete()
         grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
         grdDetalle.DataBind()
         mLimpiarDeta()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosDeta()
      Dim ldrDeta As DataRow

      mValidarDatosDeta()

      If hdnDetaId.Text = "" Then
         ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
         ldrDeta.Item("trpd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "trpd_id")
      Else
         ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("trpd_id=" & hdnDetaId.Text)(0)
      End If

      With ldrDeta
         .Item("trpd_requ_id") = cmbDetaReq.Valor
         .Item("trpd_obli") = chkDetaOblig.Checked
         .Item("trpd_audi_user") = Session("sUserId").ToString()
         .Item("trpd_baja_fecha") = DBNull.Value
         .Item("_requisito") = cmbDetaReq.SelectedItem.Text
         .Item("_estado") = "Activo"
         .Item("_obligatorio") = IIf(chkDetaOblig.Checked, "S�", "No")
      End With
      If (hdnDetaId.Text = "") Then
         mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
      End If
   End Sub
   Private Sub mValidarDatosDeta()
      If cmbDetaReq.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
      End If

      If mdsDatos.Tables(mstrTablaDetalle).Select("trpd_baja_fecha is null and trpd_requ_id=" & cmbDetaReq.Valor.ToString & IIf(hdnDetaId.Text = "", "", " and trpd_id<>" & hdnDetaId.Text)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("Requisito existente.")
      End If
   End Sub
   'Seccion de Razas
   Private Sub mActualizarRaza()
      Try
         mGuardarDatosRaza()

         mLimpiarRaza()
         grdRazas.DataSource = mdsDatos.Tables(mstrTablaRazas)
         grdRazas.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaRaza()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaRazas).Select("trdr_id=" & hdnRazaId.Text)(0)
         row.Delete()
         grdRazas.DataSource = mdsDatos.Tables(mstrTablaRazas)
         grdRazas.DataBind()
         mLimpiarRaza()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosRaza()
      Dim ldrDeta As DataRow

      mValidarDatosRaza()

      If hdnRazaId.Text = "" Then
         ldrDeta = mdsDatos.Tables(mstrTablaRazas).NewRow
         ldrDeta.Item("trdr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRazas), "trdr_id")
      Else
         ldrDeta = mdsDatos.Tables(mstrTablaRazas).Select("trdr_id=" & hdnRazaId.Text)(0)
      End If

      With ldrDeta
                .Item("trdr_raza_id") = IIf(cmbRaza.Valor.ToString.Length > 0, cmbRaza.Valor, DBNull.Value)
                .Item("trdr_espe_id") = IIf(cmbEspecie.Valor.ToString.Length > 0, cmbEspecie.Valor, DBNull.Value)
         .Item("_espe_desc") = IIf(cmbEspecie.Valor.ToString = "", "", cmbEspecie.SelectedItem.Text)
         .Item("_raza_desc") = IIf(cmbRaza.Valor.ToString = "", "", cmbRaza.SelectedItem.Text)
         .Item("trdr_trap_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("trdr_audi_user") = Session("sUserId").ToString()
         .Item("trdr_baja_fecha") = DBNull.Value
         .Item("_estado") = "Activo"
      End With
      If (hdnRazaId.Text = "") Then
         mdsDatos.Tables(mstrTablaRazas).Rows.Add(ldrDeta)
      End If
   End Sub
   Private Sub mValidarDatosRaza()
      If cmbRaza.Valor.ToString = "" And cmbEspecie.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la especie o raza.")
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panGeneral.Visible = False
         lnkGeneral.Font.Bold = False
         panDetalle.Visible = False
         lnkDetalle.Font.Bold = False
         panRazas.Visible = False
         lnkRazas.Font.Bold = False
         Select Case Tab
            Case 1
               panGeneral.Visible = True
               lnkGeneral.Font.Bold = True
            Case 2
               panDetalle.Visible = True
               lnkDetalle.Font.Bold = True
            Case 3
               panRazas.Visible = True
               lnkRazas.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
      Try
         Dim lstrRptName As String

         lstrRptName = "PlantillasTramites"

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&trap_ttra_id=" + IIf(cmbTipoTramiteFil.Valor.ToString = "", "0", cmbTipoTramiteFil.Valor.ToString)
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   'Botones Generales
   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   'Botones Detalle
   Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDeta()
   End Sub
   Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
      mBajaDeta()
   End Sub
   Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDeta()
   End Sub
   Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub
   'Botones Detalle
   Private Sub btnAltaRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRaza.Click
      mActualizarRaza()
   End Sub
   Private Sub btnBajaRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRaza.Click
      mBajaRaza()
   End Sub
   Private Sub btnModiRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRaza.Click
      mActualizarRaza()
   End Sub
   Private Sub btnLimpRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRaza.Click
      mLimpiarRaza()
   End Sub
   'Solapas
   Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkRazas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRazas.Click
      mShowTabs(3)
   End Sub
#End Region

End Class

End Namespace
