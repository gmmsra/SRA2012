<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Migrated_Socios" CodeFile="Socios.aspx.vb" %>

<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Socios</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultgrupntScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="stylesheet/sra.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="includes/utiles.js"></script>
    <script language="JavaScript" src="includes/paneles.js"></script>
    <script language="JavaScript">
        function mHabilitarControles(pTipo) {
            try {
                var lbooPersFisi = false;
                if (pTipo != null) {
                    lbooPersFisi = (pTipo.options[pTipo.selectedIndex].value == 1)
                }
                gHabilitarControl(document.all("txtNomb"), lbooPersFisi)
                //gHabilitarControl(document.all("txtNaciFecha"),lbooPersFisi)
                //gHabilitarControl(document.all("txtFalleFecha"),lbooPersFisi)
                ActivarFecha("txtNaciFecha", lbooPersFisi);
                ActivarFecha("txtFalleFecha", lbooPersFisi);
                if (!lbooPersFisi) {
                    document.all("txtNaciFecha").value = '';
                    document.all("txtFalleFecha").value = '';
                }
                //gHabilitarControl(document.all("cmbTipoDocu"),lbooPersFisi)
                document.all("cmbTipoDocu").disabled = !lbooPersFisi
                gHabilitarControl(document.all("txtDocuNume"), lbooPersFisi)
                var lbool;

                if (pTipo != null) {

                    var lstrRet = LeerCamposXML("personas_tipos", pTipo.options[pTipo.selectedIndex].value, "peti_gene");
                    if (lstrRet != '') {
                        document.all("txtNomb").value = "";
                        document.all("txtCUIT").value = "";
                        var lstrCons = LeerCamposXML("parametros", null, "para_iva_cons_final");
                        lbool = false
                    }
                    else {
                        lbool = true
                    }
                    //document.all("hdnTipoId").value = pTipo.value;

                    gHabilitarControl(document.all("txtCUIT"), lbool);
                    //if (pTipo.value == "2" || pTipo.value == "3") 
                    if (pTipo.value != "1") {
                        gHabilitarControl(document.all("txtNomb"), false);
                    }
                    else {
                        gHabilitarControl(document.all("txtNomb"), true);
                    }
                }
            }
            catch (e) {
                alert("Error al setear controles");
            }

        }
        function mSelecCP(pstrTipo) {
            document.all["txtDireCP"].value = LeerCamposXML("localidades", document.all["cmb" + pstrTipo + "DireLoca"].value, "loca_cpos");
        }
        function btnLoca_click(pstrTipo) {
            if (document.all("cmb" + pstrTipo + "DirePais").value == "" || document.all("cmb" + pstrTipo + "DirePcia").value == "") {
                alert('Debe seleccionar Pa�s y Provincia.')
            }
            else {
                gAbrirVentanas("localidades.aspx?EsConsul=1&t=popup&tipo=" + pstrTipo + "&pa=" + document.all("cmb" + pstrTipo + "DirePais").value + "&pr=" + document.all("cmb" + pstrTipo + "DirePcia").value, 1);
            }

        }
        function mCargarProvincias(pstrTipo) {
            var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePais").value;
            LoadComboXML("provincias_cargar", sFiltro, "cmb" + pstrTipo + "DirePcia", "S");
            if (document.all("cmb" + pstrTipo + "DirePais").value == '<%=Session("sPaisDefaId")%>')
                document.all('hdnValCodPostalPais').value = 'S';
            else
                document.all('hdnValCodPostalPais').value = 'N';
            document.all("cmb" + pstrTipo + "DirePcia").onchange();
        }

        function mCargarLocalidades(pstrTipo) {
            var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePcia").value
            LoadComboXML("localidades_cargar", sFiltro, "cmb" + pstrTipo + "DireLoca", "S");
        }
        function btnDA_click() {
            gAbrirVentanas("DebitoAutomatico.aspx?alum=N&origen=Socios&id=" + document.all("hdnId").value + "&amp;" + "clienteId=" + document.all("usrClie:txtId").value, 1, "700", "400");
        }
        function btnCategs_click() {
            gAbrirVentanas("SociMovimConsul.aspx?tipo=C&soci_id=" + document.all("hdnId").value, 2, "450", "300", "50");
        }
        function btnEstados_click() {
            gAbrirVentanas("SociMovimConsul.aspx?tipo=E&soci_id=" + document.all("hdnId").value, 3, "450", "300", "150");
        }
        function btnNombres_click() {
            gAbrirVentanas("SociMovimConsul.aspx?tipo=N&soci_id=" + document.all("hdnId").value, 4, "600", "300", "150");
        }
        function btnPlanPagos_click() {
            gAbrirVentanas("SociPlanPagosConsul.aspx?soci_id=" + document.all("hdnId").value, 5, "700", "400", "100");
        }
        function btnDocumentacion_click() {
            gAbrirVentanas("SociDocum.aspx?clie_id=" + document.all("usrClie:txtId").value, 5, "650", "350", "100");
        }
        function btnFicha_click() {
            gAbrirVentanas(document.all("hdnRptFicha").value, 6, "750", "600", "10");
        }

        function btnEmer_click() {
            gAbrirVentanas("EmerAgrope.aspx?soci_id=" + document.all("hdnId").value, 7, "600", "300", "150");
        }

        function btnEntidades_click() {
            gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=ENTIDADES&tabla=entidades&filtros=" + document.all("hdnId").value, 8, "500", "300", "250");
        }
        function btnCarnets_click() {
            gAbrirVentanas("SociCarnets.aspx?soci_id=" + document.all("hdnId").value, 9, "700", "300", "100", "100");
        }
        function btnFirmantes_click() {
            gAbrirVentanas("SociFirmantes.aspx?soci_id=" + document.all("hdnId").value, 10, "870", "500", "30", "30");
        }
        function btnApoderados_click() {
            gAbrirVentanas("Apoderados.aspx?soci_id=" + document.all("hdnId").value, 11, "700", "300", "200", "300");
        }
        function btnAcargo_click() {
            gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=SOCIOS A CARGO&tabla=socios_titular&filtros=" + document.all("hdnId").value, 12, "500", "300", "350");
        }
        function btnRepresentaA_click() {
            gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=SOCIOS QUE REPRESENTA&tabla=firmantesXsoci_id&filtros=" + document.all("hdnId").value + ",S", 13, "650", "400", "50", "50");
        }
        function btnPoderdantes_click() {
            gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PODERDANTES&tabla=firmantesXsoci_id&filtros=" + document.all("hdnId").value, 13, "650", "400", "50", "50");
        }
        //Aca estoy TRABAJANDO---------------------------------------------------------------------------------------------------------------------------------------
        function btnAsambleaSoc_click() {
            gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=ASAMBLEAS&tabla=socios_asambleas_cons&filtros=" + document.all("hdnId").value, 16, "750", "300", "350");
        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------
        function btnDeuda_click() {
            gAbrirVentanas("SociDeudaAplica_Pop.aspx?soci_id=" + document.all("hdnId").value + "&Origen=Socios", 14, "700", "500", "50");
        }

        function btnDeudaTit_click() {
            gAbrirVentanas("SociDeudaAplica_Pop.aspx?soci_id=" + document.all('usrTituSoci:txtId').value + "&Origen=Socios", 15, "700", "500", "50");
        }

        function btnPromos_click() {
            gAbrirVentanas("ClientesPromociones.aspx?clie_id=" + document.all("hdnClieId").value, 1, "500", "500", "250");
        }
        function expandir() {
            try { parent.frames("menu").CambiarExp(); } catch (e) { ; }
        }
    </script>
</head>
<body class="pagina" leftmargin="5" topmargin="5" onload="gSetearTituloFrame('');" rightmargin="0"
    onunload="gCerrarVentanas();">
    <form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
        <!------------------ RECUADRO ------------------->
        <table id="Table3" cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
            <tr>
                <td width="9">
                    <img height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recsup.jpg">
                    <img height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
                <td width="13">
                    <img height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9" background="imagenes/reciz.jpg">
                    <img height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table id="Table1" style="width: 100%; height: 130px" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td width="100%" colspan="3"></td>
                        </tr>
                        <tr>
                            <td style="height: 25px" valign="bottom" colspan="3" height="25">
                                <asp:Label ID="lblTituAbm" runat="server" Width="391px" CssClass="opcion">Socios</asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">
                                <asp:Panel ID="panFiltro" runat="server" CssClass="titulo" BorderStyle="Solid" Width="100%"
                                    BorderWidth="0" Visible="True">
                                    <table style="width: 100%" id="TableFil" border="0" cellspacing="0"
                                        cellpadding="0" align="left">
                                        <tr>
                                            <td style="width: 100%">
                                                <table id="Table4" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%">
                                                    <tr>
                                                        <td style="height: 8px" width="24"></td>
                                                        <td style="height: 8px" width="42"></td>
                                                        <td style="height: 8px" width="26"></td>
                                                        <td style="height: 8px"></td>
                                                        <td style="height: 8px" width="26">
                                                            <cc1:BotonImagen ID="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></cc1:BotonImagen></td>
                                                        <td style="height: 8px" width="26">
                                                            <cc1:BotonImagen ID="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></cc1:BotonImagen></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px" width="24">
                                                            <img border="0"
                                                                src="imagenes/formfle.jpg" width="24" height="25"></td>
                                                        <td style="height: 8px" width="42">
                                                            <img border="0"
                                                                src="imagenes/formtxfiltro.jpg" width="113" height="25"></td>
                                                        <td style="height: 8px" width="26">
                                                            <img border="0"
                                                                src="imagenes/formcap.jpg" width="26" height="25"></td>
                                                        <td style="height: 8px" background="imagenes/formfdocap.jpg"
                                                            colspan="3">
                                                            <img border="0" src="imagenes/formfdocap.jpg"
                                                                width="7" height="25"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table id="Table5" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%">
                                                    <tr>
                                                        <td background="imagenes/formiz.jpg" width="3">
                                                            <img border="0"
                                                                src="imagenes/formiz.jpg" width="3" height="30"></td>
                                                        <td>
                                                            <!-- FOMULARIO -->
                                                            <table id="Table6" border="0" cellspacing="0" cellpadding="0"
                                                                width="100%">
                                                                <tr>
                                                                    <td style="height: 10px"
                                                                        background="imagenes/formfdofields.jpg" colspan="2"
                                                                        align="right"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%; height: 10px"
                                                                        background="imagenes/formfdofields.jpg" align="right">
                                                                        <asp:Label ID="lblApelFil" runat="server" CssClass="titulo">Apellido:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 80%; height: 17px"
                                                                        background="imagenes/formfdofields.jpg">
                                                                        <cc1:TextBoxTab ID="txtApelFil" runat="server" CssClass="cuadrotexto" Width="250px"></cc1:TextBoxTab></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg"
                                                                        colspan="2" align="right">
                                                                        <img
                                                                            src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%; height: 10px"
                                                                        background="imagenes/formfdofields.jpg" align="right">
                                                                        <asp:Label ID="lblNumeFil" runat="server" CssClass="titulo">Nro.Socio:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 80%; height: 17px"
                                                                        background="imagenes/formfdofields.jpg">
                                                                        <cc1:NumberBox ID="txtNumeFil" runat="server" CssClass="cuadrotexto" Width="80px" AutoPostBack="true" AceptaNull="False"></cc1:NumberBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg"
                                                                        colspan="2" align="right">
                                                                        <img
                                                                            src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%; height: 10px"
                                                                        background="imagenes/formfdofields.jpg" align="right">
                                                                        <asp:Label ID="lblCuitFil" runat="server" CssClass="titulo">CUIT/CUIL:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 80%; height: 17px"
                                                                        background="imagenes/formfdofields.jpg">
                                                                        <cc1:CUITBox ID="txtCuitFil" runat="server" CssClass="cuadrotexto" Width="200px" AceptaNull="False"></cc1:CUITBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg"
                                                                        colspan="2" align="right">
                                                                        <img
                                                                            src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%; height: 26px"
                                                                        background="imagenes/formfdofields.jpg" align="right">
                                                                        <asp:Label ID="lblDocuFil" runat="server" CssClass="titulo">Nro. Documento:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 80%; height: 26px"
                                                                        background="imagenes/formfdofields.jpg">
                                                                        <cc1:ComboBox ID="cmbDocuTipoFil" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:ComboBox>
                                                                        <cc1:NumberBox ID="txtDocuNumeFil" runat="server" CssClass="cuadrotexto" Width="143px" MaxValor="9999999999999" EsDecimal="False"></cc1:NumberBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg"
                                                                        colspan="2" align="right">
                                                                        <img
                                                                            src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%; height: 10px"
                                                                        background="imagenes/formfdofields.jpg" align="right">
                                                                        <asp:Label ID="lblClieFil" runat="server" CssClass="titulo">Cliente:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 80%; height: 17px"
                                                                        background="imagenes/formfdofields.jpg">
                                                                        <uc1:CLIE ID="usrClieFil" runat="server" AceptaNull="false" Tabla="Clientes" Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" Ancho="800"></uc1:CLIE>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg"
                                                                        colspan="2" align="right">
                                                                        <img
                                                                            src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%; height: 10px"
                                                                        background="imagenes/formfdofields.jpg" align="right">
                                                                        <asp:Label ID="lblFirmanteFil" runat="server" CssClass="titulo">Nro.Firmante:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 80%; height: 17px"
                                                                        background="imagenes/formfdofields.jpg">
                                                                        <cc1:NumberBox ID="txtFirmanteNumeFil" runat="server" CssClass="cuadrotexto" Width="100px" EsDecimal="False"></cc1:NumberBox>
                                                                        <cc1:TextBoxTab ID="txtFirmanteFil" runat="server" CssClass="cuadrotexto" Width="250px"></cc1:TextBoxTab></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg"
                                                                        colspan="2" align="right">
                                                                        <img
                                                                            src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%; height: 10px"
                                                                        background="imagenes/formfdofields.jpg"
                                                                        align="right"></td>
                                                                    <td style="width: 80%; height: 17px"
                                                                        background="imagenes/formfdofields.jpg">
                                                                        <asp:CheckBox ID="chkBaja" Visible="False" runat="server" CssClass="titulo" Text="Incluir Dados de Baja"></asp:CheckBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 10px"
                                                                        background="imagenes/formfdofields.jpg" colspan="2"
                                                                        align="right"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td background="imagenes/formde.jpg" width="2">
                                                            <img border="0"
                                                                src="imagenes/formde.jpg" width="2"
                                                                height="2"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" height="10"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" colspan="3">
                                <asp:DataGrid ID="grdDato" runat="server" Width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
                                    HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
                                    OnPageIndexChanged="DataGrid_Page" OnEditCommand="mSeleccionar">
                                    <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                    <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                    <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                    <FooterStyle CssClass="footer"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderStyle Width="15px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Linkbutton3" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
														<img src='images/sele.gif' border="0" alt="Seleccionar Socio" style="cursor:hand;" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="soci_id"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="soci_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_clie_apel" HeaderText="Apellido"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_clie_docu_nume" HeaderText="Nro.Doc"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_cate_desc" HeaderText="Categor�a"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="_tacl_id" HeaderText="_tacl_id"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="_tacl_tarj"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="_tacl_nume"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="_tacl_vcto_fecha"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td colspan="3" height="10"></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <div>
                                    <asp:Panel ID="panDato" runat="server" Width="100%" CssClass="titulo" BorderStyle="Solid" BorderWidth="1px"
                                        Visible="False" Height="116px">
                                        <p align="right">
                                            <table style="width: 100%; height: 106px" id="Table2" class="FdoFld"
                                                border="0" cellpadding="0" align="left">
                                                <tr>
                                                    <td>
                                                        <p></p>
                                                    </td>
                                                    <td height="5">
                                                        <asp:Label ID="lblTitu" runat="server" CssClass="titulo" Width="100%"></asp:Label></td>
                                                    <td valign="top" align="right">&nbsp; 
                                                        <asp:ImageButton ID="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%" colspan="3">
                                                        <asp:Panel ID="panCabecera" runat="server" CssClass="titulo" Width="100%">
                                                            <table style="width: 100%" id="TableCabecera" border="0"
                                                                cellpadding="0" align="left">
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblNume" runat="server" CssClass="titulo">Nro.Socio:</asp:Label>&nbsp;</td>
                                                                    <td width="100%" colspan="2">
                                                                        <table style="width: 100%" id="Table7" border="0"
                                                                            cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td width="70">
                                                                                    <cc1:NumberBox ID="txtNume" runat="server" CssClass="cuadrotextodeshab" Width="65px" Enabled="false"></cc1:NumberBox>&nbsp;</td>
                                                                                <td width="70" align="right">
                                                                                    <asp:Label ID="lblClie" runat="server" CssClass="titulo">Cliente:</asp:Label>&nbsp; 
                                                                                </td>
                                                                                <td>
                                                                                    <uc1:CLIE ID="usrClie" runat="server" width="100%" Tabla="Clientes" Saltos="1,1,1" FilSociNume="True" MuestraDesc="False" FilDocuNume="True" FilCUIT="True" Activo="False" PermiModi="True"></uc1:CLIE>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" align="center">
                                                                        <table style="margin-top: 10px" id="Table8" class="marco"
                                                                            cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td width="120" align="right">
                                                                                    <asp:Label ID="lblTipo" runat="server" CssClass="titulo">Tipo:</asp:Label></td>
                                                                                <td colspan="2">
                                                                                    <cc1:ComboBox ID="cmbTipo" class="combo" runat="server" Width="25%" AceptaNull="True" Obligatorio="false" onchange="mHabilitarControles(this);"></cc1:ComboBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" background="imagenes/formdivmed.jpg"
                                                                                    colspan="3" align="right">
                                                                                    <img
                                                                                        src="imagenes/formdivmed.jpg" width="1"
                                                                                        height="2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="120" align="right">
                                                                                    <asp:Label ID="lblApel" runat="server" CssClass="titulo">Apellido/Raz�n Social:</asp:Label></td>
                                                                                <td colspan="2">
                                                                                    <cc1:TextBoxTab ID="txtApel" runat="server" CssClass="cuadrotexto" Width="100%" Obligatorio="True"></cc1:TextBoxTab></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" background="imagenes/formdivmed.jpg"
                                                                                    colspan="3" align="right">
                                                                                    <img
                                                                                        src="imagenes/formdivmed.jpg" width="1"
                                                                                        height="2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="middle" align="right">
                                                                                    <asp:Label ID="lblNomb" runat="server" CssClass="titulo">Nombre:</asp:Label></td>
                                                                                <td valign="middle">
                                                                                    <cc1:TextBoxTab ID="txtNomb" runat="server" CssClass="cuadrotexto" Width="100%" AceptaNull="False"></cc1:TextBoxTab></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 49px" valign="middle" align="right">
                                                                                    <asp:Label ID="lblFantSocios" runat="server" CssClass="titulo" Height="23px">Interface Clientes:</asp:Label></td>
                                                                                <td style="height: 49px" valign="baseline">
                                                                                    <p>
                                                                                        <cc1:TextBoxTab ID="txtFant" runat="server" CssClass="cuadrotexto" Visible="True" Width="100%" AceptaNull="False"></cc1:TextBoxTab>
                                                                                        <asp:Label ID="LblAdvertencia" runat="server" CssClass="titulo" ForeColor="Red">Debe obligatoriamente completar o actualizar este campo al cambiar el Apellido o Nombre</asp:Label>
                                                                                    </p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" background="imagenes/formdivmed.jpg"
                                                                                    colspan="3" align="right">
                                                                                    <img
                                                                                        src="imagenes/formdivmed.jpg" width="1"
                                                                                        height="2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" align="right">
                                                                                    <asp:Label ID="lblDire" runat="server" CssClass="titulo">Direcci�n:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <table style="width: 100%" id="Table9" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <cc1:TextBoxTab ID="txtDire" runat="server" CssClass="cuadrotexto" Width="100%"></cc1:TextBoxTab></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="height: 36px" colspan="3">
                                                                                                <asp:Label ID="lblDefaDirePais" runat="server" CssClass="titulo">Pa�s:</asp:Label>&nbsp; 
                                                                                                <cc1:ComboBox ID="cmbDefaDirePais" class="combo" runat="server" Width="168px" onchange="mCargarProvincias('Defa')"></cc1:ComboBox>&nbsp; 
                                                                                                <asp:Label ID="lblDefaDirePcia" runat="server" CssClass="titulo">Provincia:</asp:Label>
                                                                                                <cc1:ComboBox ID="cmbDefaDirePcia" class="combo" runat="server" Width="195px" onchange="mCargarLocalidades('Defa')" NomOper="provincias_cargar"></cc1:ComboBox></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="lblDefaDireLoca" runat="server" CssClass="titulo">Localidad:</asp:Label>&nbsp; 
                                                                                                <cc1:ComboBox ID="cmbDefaDireLoca" class="combo" runat="server" Width="207px" onchange="mSelecCP('Defa');" NomOper="localidades_cargar"></cc1:ComboBox>
                                                                                                <button
                                                                                                    style="width: 25.47%; height: 20px"
                                                                                                    id="btnDefaLoca" class="boton"
                                                                                                    onclick="btnLoca_click('Defa');" runat="server"
                                                                                                    value="Detalles">
                                                                                                    Localidades</button></td>
                                                                                            <td width="50" align="right">
                                                                                                <asp:Label ID="lblDireCP" runat="server" CssClass="titulo">C.P.:</asp:Label>&nbsp;</td>
                                                                                            <td width="80">
                                                                                                <cc1:CodPostalBox ID="txtDireCP" runat="server" CssClass="cuadrotexto" Width="77px"></cc1:CodPostalBox></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="lblDefaDireRefe" runat="server" CssClass="titulo">Referencia:</asp:Label>&nbsp; 
                                                                                                <cc1:TextBoxTab ID="txtDefaDireRefe" runat="server" CssClass="cuadrotexto" Width="288px"></cc1:TextBoxTab></td>
                                                                                            <td width="50" align="right"></td>
                                                                                            <td width="80"></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" background="imagenes/formdivmed.jpg"
                                                                                    colspan="3" align="right">
                                                                                    <img
                                                                                        src="imagenes/formdivmed.jpg" width="1"
                                                                                        height="2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" align="right">
                                                                                    <asp:Label ID="lblTeleNume" runat="server" CssClass="titulo">Tel�fono:</asp:Label>&nbsp;</td>
                                                                                <td nowrap>
                                                                                    <table id="Table10" border="0" cellspacing="0"
                                                                                        cellpadding="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 291px">
                                                                                                <cc1:TextBoxTab ID="txtTeleArea" runat="server" CssClass="cuadrotexto" Width="40px"></cc1:TextBoxTab>&nbsp; 
                                                                                                <cc1:TextBoxTab ID="txtTeleNume" runat="server" CssClass="cuadrotexto" Width="232px"></cc1:TextBoxTab></td>
                                                                                            <td colspan="2">
                                                                                                <asp:Label ID="lblTeleTipo" runat="server" CssClass="titulo">Tipo:</asp:Label>&nbsp; 
                                                                                                <cc1:ComboBox ID="cmbTeleTipo" class="combo" runat="server" Width="150px"></cc1:ComboBox></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <asp:Label ID="lblTeleRefe" runat="server" CssClass="titulo">Referencia:</asp:Label>&nbsp; 
                                                                                                <cc1:TextBoxTab ID="txtTeleRefe" runat="server" CssClass="cuadrotexto" Width="288px"></cc1:TextBoxTab></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" background="imagenes/formdivmed.jpg"
                                                                                    colspan="3" align="right">
                                                                                    <img
                                                                                        src="imagenes/formdivmed.jpg" width="1"
                                                                                        height="2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" align="right">
                                                                                    <asp:Label ID="lblMail" runat="server" CssClass="titulo">Mail:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <cc1:TextBoxTab ID="txtMail" runat="server" CssClass="cuadrotexto" Width="208px" Panel="Mail" EsMail="True"></cc1:TextBoxTab>&nbsp; 
                                                                                    <asp:Label ID="lblMailRefe" runat="server" CssClass="titulo">Referencia:</asp:Label>
                                                                                    <cc1:TextBoxTab ID="txtMailRefe" runat="server" CssClass="cuadrotexto" Width="216px"></cc1:TextBoxTab></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" background="imagenes/formdivmed.jpg"
                                                                                    colspan="3" align="right">
                                                                                    <img
                                                                                        src="imagenes/formdivmed.jpg" width="1"
                                                                                        height="2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label ID="lblCUIT" runat="server" CssClass="titulo">CUIT:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <table style="width: 100%" border="0" cellspacing="0"
                                                                                        cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <cc1:CUITBox ID="txtCUIT" runat="server" CssClass="cuadrotexto" Width="168px" AceptaNull="False"></cc1:CUITBox></td>
                                                                                            <td align="right">
                                                                                                <asp:Label ID="lblIVA" runat="server" CssClass="titulo">Posic. IVA:</asp:Label></td>
                                                                                            <td>
                                                                                                <cc1:ComboBox ID="cmbIVA" class="combo" runat="server" Width="150px"></cc1:ComboBox></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="2" background="imagenes/formdivmed.jpg"
                                                                                    colspan="3" align="right">
                                                                                    <img
                                                                                        src="imagenes/formdivmed.jpg" width="1"
                                                                                        height="2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label ID="lblDocu" runat="server" CssClass="titulo">Tipo y Nro.Doc.:</asp:Label>&nbsp;</td>
                                                                                <td colspan="2">
                                                                                    <cc1:ComboBox ID="cmbTipoDocu" class="combo" runat="server" Width="84px" Obligatorio="True"></cc1:ComboBox>
                                                                                    <cc1:NumberBox ID="txtDocuNume" runat="server" CssClass="cuadrotexto" Width="112px" MaxValor="9999999999999" EsDecimal="False"></cc1:NumberBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="3"
                                                                        align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblCate" runat="server" CssClass="titulo">Categor�a:</asp:Label></td>
                                                                    <td colspan="2">
                                                                        <table style="width: 100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <cc1:TextBoxTab ID="txtCate" runat="server" CssClass="cuadrotextodeshab" Width="160px" Enabled="false"></cc1:TextBoxTab></td>
                                                                                <td style="padding-left: 10px">
                                                                                    <asp:Label ID="lblCateFecha" runat="server" CssClass="titulo">F.Cate.:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <cc1:DateBox ID="txtCateFecha" runat="server" CssClass="cuadrotextodeshab" Width="68px" Enabled="False"></cc1:DateBox></td>
                                                                                <td style="padding-left: 10px">
                                                                                    <asp:Label ID="lblDist" runat="server" CssClass="titulo">Distrito:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <cc1:ComboBox class="combo" ID="cmbDist" runat="server" Width="160px" Obligatorio="true"></cc1:ComboBox></td>
                                                                                            <td style="padding-left: 10px">
                                                                                                <asp:Label ID="llbDirePcia" runat="server" CssClass="titulo">Provincia:</asp:Label></td>
                                                                                            <td>
                                                                                                <cc1:ComboBox ID="cmbDirePcia" class="combo" runat="server" Width="200px" NomOper="provincias_cargar" onchange="mCargarLocalidades('')"></cc1:ComboBox></td>
                                                                                            <td style="padding-left: 10px">
                                                                                                <asp:Label ID="lblDireLoca" runat="server" CssClass="titulo">Localidad:</asp:Label></td>
                                                                                            <td>
                                                                                                <cc1:ComboBox ID="cmbDireLoca" class="combo" runat="server" Width="200px" NomOper="localidades_cargar"></cc1:ComboBox></td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="3"
                                                                        align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblNaciFecha" runat="server" CssClass="titulo">Fecha Nac.:</asp:Label></td>
                                                                    <td colspan="2">
                                                                        <table style="width: 100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td width="105">
                                                                                    <cc1:DateBox ID="txtNaciFecha" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox></td>
                                                                                <td width="100" align="right">
                                                                                    <asp:Label ID="lblIngrFecha" runat="server" CssClass="titulo">F. Ingreso:</asp:Label>&nbsp;</td>
                                                                                <td width="120">
                                                                                    <cc1:DateBox ID="txtIngrFecha" runat="server" CssClass="cuadrotextodeshab" Width="68px" Enabled="False"></cc1:DateBox></td>
                                                                                <td width="120" align="right">
                                                                                    <asp:Label ID="lblEmanFecha" runat="server" CssClass="titulo">Emancipaci�n:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <cc1:DateBox ID="txtEmanFecha" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="3"
                                                                        align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblFalleFecha" runat="server" CssClass="titulo">Fecha Fallecimiento:</asp:Label></td>
                                                                    <td colspan="2">
                                                                        <cc1:DateBox ID="txtFalleFecha" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp; 
                                                                        <asp:CheckBox ID="chkSinCorreo" runat="server" CssClass="titulo" Text="Sin Correo"></asp:CheckBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="3"
                                                                        align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblEsta" runat="server" CssClass="titulo">Estado:</asp:Label></td>
                                                                    <td colspan="2">
                                                                        <table style="width: 100%" border="0" cellspacing="0"
                                                                            cellpadding="0">
                                                                            <tr>
                                                                                <td width="105">
                                                                                    <cc1:TextBoxTab ID="txtEsta" runat="server" CssClass="cuadrotextodeshab" Width="140px" Enabled="false"></cc1:TextBoxTab></td>
                                                                                <td width="100" align="right">
                                                                                    <asp:Label ID="lblEstaFecha" runat="server" CssClass="titulo">F.Estado:</asp:Label>&nbsp;</td>
                                                                                <td nowrap>
                                                                                    <cc1:DateBox ID="txtEstaFecha" runat="server" CssClass="cuadrotextodeshab" Width="68px" Enabled="False"></cc1:DateBox></td>
                                                                                <td style="height: 20px" align="right">
                                                                                    <asp:Label ID="lblDevengaTit" runat="server" CssClass="titulo">Bloquea Devengamiento:</asp:Label></td>
                                                                                <td style="height: 20px" colspan="2">
                                                                                    <asp:Label ID="lblDevenga" runat="server" CssClass="titulo"></asp:Label></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="3"
                                                                        align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="middle" align="right">
                                                                        <asp:Label ID="lblTituSoci" runat="server" CssClass="titulo">Socio Titular:</asp:Label>&nbsp; 
                                                                    </td>
                                                                    <td>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <uc1:CLIE ID="usrTituSoci" runat="server" Tabla="Socios" MuestraDesc="False" Activo="False"></uc1:CLIE>
                                                                                </td>
                                                                                <td style="width: 95px" align="right">
                                                                                    <button
                                                                                        style="width: 90px" id="btnDeudaTit" class="boton"
                                                                                        onclick="btnDeudaTit_click();" runat="server"
                                                                                        value="Detalles">
                                                                                        Deuda</button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="3"
                                                                        align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" colspan="4" align="center">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td style="width: 126px" align="center">
                                                                                    <asp:Label ID="lblDAut" runat="server" CssClass="titulo" Width="128px" Height="6px">D�bito Autom�tico:</asp:Label></td>
                                                                                <td style="width: 5%" align="left">
                                                                                    <asp:Label ID="lblDA" runat="server" CssClass="titulo" Visible="True">NO</asp:Label></td>
                                                                                <td style="width: 85%" align="left">
                                                                                    <asp:Label ID="lblDebAuto" runat="server" CssClass="titulo" Visible="True"></asp:Label></td>
                                                                                <td style="width: 10%" align="right">
                                                                                    <button
                                                                                        style="width: 90px" id="btnDA" class="boton"
                                                                                        onclick="btnDA_click();" runat="server"
                                                                                        value="Detalles">
                                                                                        D�bito Auto.</button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="3"
                                                                        align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1"
                                                                            height="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" align="right">
                                                                        <asp:Label ID="lbObse" runat="server" CssClass="titulo">Observaciones:</asp:Label>&nbsp; 
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <cc1:TextBoxTab ID="txtObse" runat="server" CssClass="cuadrotexto" Width="100%" Height="50px" TextMode="MultiLine" EnterPorTab="False"></cc1:TextBoxTab></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="center">
                                                        <button style="width: 70px"
                                                            id="btnFirmantes" class="boton" onclick="btnFirmantes_click();"
                                                            runat="server"
                                                            value="Detalles">
                                                            Firmantes</button>&nbsp;&nbsp;
                                                        <button
                                                            style="width: 80px" id="btnCategs" class="boton"
                                                            onclick="btnCategs_click();" runat="server"
                                                            value="Detalles">
                                                            Hist.Categ.</button>&nbsp;&nbsp;
                                                        <button
                                                            style="width: 80px" id="btnEstados" class="boton"
                                                            onclick="btnEstados_click();" runat="server"
                                                            value="Detalles">
                                                            Hist.Estados</button>&nbsp;&nbsp;
                                                        <button
                                                            style="width: 90px" id="btnNombres" class="boton"
                                                            onclick="btnNombres_click();" runat="server"
                                                            value="Detalles">
                                                            Hist.Nombres</button>&nbsp;&nbsp;
                                                        <button
                                                            style="width: 85px" id="btnCarnets" class="boton"
                                                            onclick="btnCarnets_click();" runat="server"
                                                            value="Detalles">
                                                            Dup.Carnets</button>&nbsp;&nbsp;
                                                        <button
                                                            style="width: 80px" id="btnApoderados" class="boton"
                                                            onclick="btnApoderados_click();" runat="server"
                                                            value="Detalles">
                                                            Apoderados</button>&nbsp;&nbsp;
                                                        <button
                                                            style="width: 95px" id="btnPlanPagos" class="boton"
                                                            onclick="btnPlanPagos_click();" runat="server"
                                                            value="Detalles">
                                                            Planes de Pago</button>&nbsp;
                                                        <button
                                                            style="width: 95px" id="btnDocu" class="boton"
                                                            onclick="btnDocumentacion_click();" runat="server"
                                                            value="Detalles">
                                                            Documentaci�n</button>
                                                    </td>
                                                </tr>
                                                <tr height="30">
                                                    <td colspan="3" align="center">
                                                        <button style="width: 65px"
                                                            id="btnEntidades" class="boton" onclick="btnEntidades_click();"
                                                            runat="server"
                                                            value="Detalles">
                                                            Entidades</button>&nbsp;
                                                        <button
                                                            style="width: 85px" id="btnRepresentaA" class="boton"
                                                            onclick="btnRepresentaA_click();" runat="server"
                                                            value="Detalles">
                                                            Representa A</button>&nbsp;
                                                        <button
                                                            style="width: 85px" id="btnPoder" class="boton"
                                                            onclick="btnPoderdantes_click();" runat="server"
                                                            value="Detalles">
                                                            Poderdantes</button>&nbsp;
                                                        <button
                                                            style="width: 60px" id="btnAcargo" class="boton"
                                                            onclick="btnAcargo_click();" runat="server" value="Detalles">
                                                            A 
                  Cargo</button>&nbsp;
                                                        <button style="width: 60px" id="btnDeuda"
                                                            class="boton" onclick="btnDeuda_click();" runat="server"
                                                            value="Deuda">
                                                            Deuda</button>&nbsp;
                                                        <button style="width: 80px"
                                                            id="btnPromos" class="boton" onclick="btnPromos_click();"
                                                            runat="server" value="Deuda">
                                                            Promociones</button>&nbsp;
                                                        <button
                                                            style="width: 65px" id="btnFicha" class="boton"
                                                            onclick="btnFicha_click();" runat="server" value="Deuda">
                                                            Ver 
                  Ficha</button>&nbsp;
                                                        <button style="width: 80px" id="btnEmer"
                                                            class="boton" onclick="btnEmer_click();" runat="server"
                                                            value="Deuda">
                                                            Emer.Agrop.</button>&nbsp;
                                                        <button
                                                            style="width: 80px" id="btnAsambleaSoc" class="boton"
                                                            onclick="btnAsambleaSoc_click();" runat="server"
                                                            value="Deuda">
                                                            Asambleas</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </p>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                        <tr height="30">
                            <td align="center" colspan="3"><a id="editar" name="editar"></a>
                                <asp:Button ID="btnModi" runat="server" CssClass="boton" Width="80px" Text="Modificar" CausesValidation="False"
                                    Visible="False"></asp:Button></td>
                        </tr>
                    </table>
                    <!--- FIN CONTENIDO --->
                </td>
                <td width="13" background="imagenes/recde.jpg">
                    <img height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9">
                    <img height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recinf.jpg">
                    <img height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
                <td width="13">
                    <img height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
            </tr>
        </table>
        <!----------------- FIN RECUADRO ----------------->
        <div style="display: none">
            <asp:TextBox ID="lblMens" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnValorId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDiclId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnTeclId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnMaclId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnSess" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnRefresh" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnClieId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnRptFicha" runat="server"></asp:TextBox>
        </div>
    </form>

    <script type="text/jscript" language="javascript">

        if (document.all["editar"] != null)
            document.location = '#editar';

        if (document.all["txtDesc"] != null)
            document.all["txtDesc"].focus();

        function mRefresh() {
            if (document.all("txtApel") != null) {
                this.xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                this.xmlDoc.async = false;
                var CallString = url + "?Oper=socios_clien_datos&Args=" + document.all("hdnId").value + "&Opc=" + "clie_apel,clie_nomb,clie_cuit,clie_docu_nume,clie_doti_id,clie_ivap_id,dicl_dire,dicl_cpos,macl_mail,tecl_area_code,tecl_tele";
                this.xmlDoc.load(CallString);
                var rs = this.xmlDoc.lastChild.childNodes;
                var unr = rs[0];
                var vsRet = this.xmlDoc.lastChild.lastChild.text.split("|");
                document.all("txtApel").value = vsRet[0];
                document.all("txtNomb").value = vsRet[1];
                document.all("txtCUIT").value = vsRet[2];
                document.all("txtDocuNume").value = vsRet[3];
                gBuscarCombo(vsRet[4], document.all("cmbTipoDocu"))
                gBuscarCombo(vsRet[5], document.all("cmbIVA"))
                document.all("txtDire").value = vsRet[6];
                document.all("txtDireCP").value = vsRet[7];
                document.all("txtMail").value = vsRet[8];
                document.all("txtTeleArea").value = vsRet[9];
                document.all("txtTeleNume").value = vsRet[10];
            }
        }

        if (document.all('cmbTipo') != null)
            mHabilitarControles(document.all('cmbTipo'));

        function mCargarLocalidades(pstrTipo) {
            var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePcia").value
            LoadComboXML("localidades_cargar", sFiltro, "cmb" + pstrTipo + "DireLoca", "S");
        }

    </script>

</body>
</html>
