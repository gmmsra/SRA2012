<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ReglasValidacion" CodeFile="ReglasValidacion.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reglas de Validaci�n</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function mMensajes()
	   	{
			gAbrirVentanas("mensajes.aspx?t=popup",1);
		}
		function mCondicion()
	   	{
	   		var lstrSp = 'rg_formulas_cargar';
	   		var lstrFil = document.all('cmbFormula').value;
	   		var lstrCamp = 'condicion';
	   		var lstrTitu = 'Formula: '+document.all["cmbFormula"].item(document.all["cmbFormula"].selectedIndex).text;
			if (lstrFil == '')
		   	{
				alert('Debe seleccionar una f�rmula.');	
				document.all('cmbFormula').focus();
				return(false);
			}				
			gAbrirVentanas("ConsultaDatos.aspx?titu="+lstrTitu+"&fil="+lstrFil+"&sp="+lstrSp+"&camp="+lstrCamp,1);
		}
		function mCargarMensajes()
	   	{
			LoadComboXML("rg_mensajes_cargar", "", "cmbMensaje", "S");
		}		
		function mSetearValores(ptxtObje,pcmbObje)
		{
			if (pcmbObje.value == '')
			{
				document.all[ptxtObje].disabled = false;
			}
			else
			{
				document.all[ptxtObje].disabled = true;
				document.all[ptxtObje].value = '';
			}		
		}
		</SCRIPT>
		<script id="clientEventHandlersJS" language="javascript">
<!--

function frmABM_onfocus() {

}

//-->
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server" language="javascript"
			onfocus="return frmABM_onfocus()">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Reglas de Validaci�n</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="100%">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD colSpan="3">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD>
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 37.04%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																				noWrap align="right">
																				<asp:label id="lblCodigoFil" runat="server" cssclass="titulo" Width="100px">C�digo de Regla:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtCodigoFil" runat="server" cssclass="cuadrotexto" Width="88px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 37.04%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																				align="right">
																				<asp:label id="lblDescFil" runat="server" cssclass="titulo">Descripci�n para el Usuario:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtDescFil" runat="server" cssclass="cuadrotexto" Width="312px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 37.04%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																				align="right">
																				<asp:label id="lblMsgFil" runat="server" cssclass="titulo">Mensaje para el Usuario:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbMsgFil" class="combo" runat="server" cssclass="cuadrotexto" Width="312px"
																					Height="20px" AceptaNull="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 37.04%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																				align="right">
																				<asp:label id="lblProcFil" runat="server" cssclass="titulo" ToolTip=" ">Proceso:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbProcFil" class="combo" runat="server" cssclass="cuadrotexto" Width="312px"
																					Height="20px" AceptaNull="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<FooterStyle CssClass="footer"></FooterStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="regv_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="regv_codi" HeaderText="C&#243;d.Regla"></asp:BoundColumn>
												<asp:BoundColumn DataField="regv_desc" HeaderText="Descripci&#243;n para el Usuario"></asp:BoundColumn>
												<asp:BoundColumn DataField="_rfor_desc" HeaderText="F&#243;rmula"></asp:BoundColumn>
												<asp:BoundColumn DataField="_rmen_desc" HeaderText="Mensaje para el Usuario"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Denuncia" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Imprimir Listado" ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD colSpan="3"></TD>
								</tr>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Height="124%" Visible="False">
											<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
												<TR>
													<TD width="1"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="28"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="28"></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="28"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> General </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDeta" runat="server" cssclass="solapa"
															Width="80px" Height="21px" CausesValidation="False"> Procesos </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="28"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												width="99%" Height="116px" Visible="False">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo de Regla:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<CC1:numberbox id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="88px" Obligatorio="True"></CC1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n para el Usuario:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="360px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblFormula" runat="server" cssclass="titulo">F�rmula:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<cc1:combobox id="cmbFormula" class="combo" runat="server" cssclass="cuadrotexto" Width="312px"
																				Height="20px" AceptaNull="true" Obligatorio="true"></cc1:combobox>&nbsp;<BUTTON style="WIDTH: 70px" id="btnCond" class="boton" onclick="btnLoca_click('Defa');"
																				runat="server" value="Detalles">Condici�n</BUTTON></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 15px" vAlign="middle" align="right">
																			<asp:Label id="lblMensaje" runat="server" cssclass="titulo">Mensaje  para el Usuario:</asp:Label></TD>
																		<TD style="HEIGHT: 15px" colSpan="2" noWrap>
																			<cc1:combobox id="cmbMensaje" class="combo" runat="server" cssclass="cuadrotexto" Width="312px"
																				Height="20px" AceptaNull="true" Obligatorio="true"></cc1:combobox>&nbsp;<BUTTON style="WIDTH: 70px" id="btnMens" class="boton" onclick="btnLoca_click('Defa');"
																				runat="server" value="Detalles">Mensajes</BUTTON></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 19px" vAlign="middle" align="right">
																			<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:</asp:Label></TD>
																		<TD style="HEIGHT: 19px" colSpan="2">
																			<cc1:combobox id="cmbSexo" class="combo" runat="server" Width="113px" AceptaNull="False" onchange="mSelecSexo(this,'lblDonaFil');">
																				<asp:ListItem Value="null" Selected="True">(Todos)</asp:ListItem>
																				<asp:ListItem Value="0">Hembra</asp:ListItem>
																				<asp:ListItem Value="1">Macho</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right"></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<asp:checkbox id="chkManu" CssClass="titulo" Runat="server" Text="Validaci�n Manual"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblVige" runat="server" cssclass="titulo">Vigencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<asp:Label id="lblVigeDesde" runat="server" cssclass="titulo">Desde:</asp:Label>
																			<cc1:DateBox id="txtVigeDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblVigeHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="txtVigeHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblCamp" runat="server" cssclass="titulo">Campo:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<cc1:combobox id="cmbCamp" class="combo" runat="server" Width="186px" AceptaNull="False">
																				<asp:ListItem Value="null" Selected="True">(Todos)</asp:ListItem>
																				<asp:ListItem Value="0">Hembra</asp:ListItem>
																				<asp:ListItem Value="1">Macho</asp:ListItem>
																			</cc1:combobox>&nbsp;
																			<asp:checkbox id="chkObli" CssClass="titulo" Runat="server" Text="Campo Obligatorio"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblAgru" runat="server" cssclass="titulo">Agrup.:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<cc1:combobox id="cmbAgru" class="combo" runat="server" Width="260px" AceptaNull="False" onchange="mSelecSexo(this,'lblDonaFil');">
																				<asp:ListItem Value="null" Selected="True">(Todos)</asp:ListItem>
																				<asp:ListItem Value="0">Hembra</asp:ListItem>
																				<asp:ListItem Value="1">Macho</asp:ListItem>
																			</cc1:combobox>&nbsp; &nbsp;
																			<asp:Label id="lblOrde" runat="server" cssclass="titulo">Orden:</asp:Label>&nbsp;
																			<CC1:numberbox id="txtOrde" runat="server" cssclass="cuadrotexto" Width="55px" Obligatorio="false"></CC1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblMini" runat="server" cssclass="titulo">Valor M�nimo:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtMini" runat="server" cssclass="cuadrotexto" Width="129px" Obligatorio="false"></CC1:TEXTBOXTAB>
																			<cc1:combobox id="cmbMini" class="combo" runat="server" Width="211px" AceptaNull="False" onchange="javascript:mSetearValores('txtMini',this);">
																				<asp:ListItem Value="null" Selected="True">(Todos)</asp:ListItem>
																				<asp:ListItem Value="0">Hembra</asp:ListItem>
																				<asp:ListItem Value="1">Macho</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="lblMaxi" runat="server" cssclass="titulo">Valor M�ximo:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtMaxi" runat="server" cssclass="cuadrotexto" Width="129px" Obligatorio="false"></CC1:TEXTBOXTAB>
																			<cc1:combobox id="cmbMaxi" class="combo" runat="server" Width="213px" AceptaNull="False" onchange="javascript:mSetearValores('txtMaxi',this);">
																				<asp:ListItem Value="null" Selected="True">(Todos)</asp:ListItem>
																				<asp:ListItem Value="0">Hembra</asp:ListItem>
																				<asp:ListItem Value="1">Macho</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:datagrid style="Z-INDEX: 0" id="grdDeta" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="DataGridDeta_Page" AutoGenerateColumns="False" OnEditCommand="mEditarDeta">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="repr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_rpro_desc" HeaderText="Proceso"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblProcesos" runat="server" cssclass="titulo">Procesos:</asp:Label></TD>
																		<TD>
																			<cc1:combobox id="cmbProcesos" class="combo" runat="server" Width="248px" AceptaNull="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="100px" Text="Alta"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Baja"></asp:Button>&nbsp;
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Modificar"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="100px" Text="Limpiar "></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnValorId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnModi" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all[document.all('txtMini')]!= null)
		{
			mSetearValores(document.all('txtMini'),document.all('cmbMini'));
			mSetearValores(document.all('txtMaxi'),document.all('cmbMaxi'));
		}
		</SCRIPT>
	</BODY>
</HTML>
