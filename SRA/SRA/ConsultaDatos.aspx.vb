Namespace SRA

Partial Class ConsultaDatos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
Protected WithEvents caudotexto As System.Web.UI.WebControls.Label
	'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
	'No se debe eliminar o mover.

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
		'No la modifique con el editor de c�digo.
		InitializeComponent()
	End Sub

#End Region



#Region "Definici�n de Variables"
	Public mstrCmd As String
	Public mstrSQL As String
	Public mstrTitulo As String
	Public mstrTabla As String
	Public mstrFiltros As String
	Public mstrCampo As String
	Private mstrConn As String
	Private mbooAutopostback As Boolean
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()
			If Not Page.IsPostBack Then
				mConsultar(False)
			End If
		Catch ex As Exception

			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

#Region "Inicializacion"

	Public Sub mInicializar()
		mstrSQL = Request.QueryString("sp")
		mstrTitulo = Request.QueryString("titu")
		mstrTabla = "rg_formulas"
		mstrFiltros = Request.QueryString("fil")
		mstrCampo = Request.QueryString("camp")
		lblTituAbm.Text = mstrTitulo
		If Not Request.QueryString("Autopostback") Is Nothing Then
			mbooAutopostback = CBool(CInt(Request.QueryString("Autopostback")))
		Else
			mbooAutopostback = True
		End If
	End Sub

	Public Sub mConsultar(ByVal pbooPage As Boolean)
		Try

			mstrCmd = "exec " + mstrSQL + " "
			mstrCmd += mstrFiltros

			Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), mstrCmd)
			While (dr.Read())
				txtDato.Text = dr.GetValue(dr.GetOrdinal(mstrCampo)).ToString().Trim()
				lblComentarioNixor.Text = dr.GetValue(dr.GetOrdinal("comentario")).ToString().Trim()
				txtObse.Text = dr.GetValue(dr.GetOrdinal("obse")).ToString().Trim()
			End While
			dr.Close()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Function mGuardarDatos() As DataSet
		Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, mstrFiltros)
		Dim lintCpo As Integer

		With ldsEsta.Tables(0).Rows(0)
			.Item(1) = clsSQLServer.gFormatArg(mstrFiltros, SqlDbType.Int)
			.Item("rfor_obse") = txtObse.Text
		End With

		Return ldsEsta
	End Function

	Private Sub mModi()
		Try
			Dim ldsEstruc As DataSet = mGuardarDatos()
			Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

			lobjGenerica.Modi()


		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

	Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
		mModi()
	End Sub


End Class

End Namespace
