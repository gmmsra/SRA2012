<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ConsultaFiltros" CodeFile="ConsultaFiltros.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript"> 
		function mImprimir()
		{
			window.print();
		}		
		function mEstablecimiento(pEsta)
		{
			var win=window.open('establecimientos.aspx?id='+pEsta, '', 'location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=700px,height=400px,left=50px,top=50px');
		}		
		function mRelaciones(pProd)
		{
			var win=window.open('productos.aspx?ProdId='+pProd, '', 'location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=700px,height=400px,left=50px,top=50px');
		}		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG class="noprint" height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td class="noprint" background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG class="noprint" height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG class="noprint" height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right">
									<DIV onclick="window.close();"><IMG class="noprint" style="CURSOR: hand" alt="Cerrar" src="imagenes/close3.bmp" border="0">
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD><asp:label id="lblTituAbm" runat="server" cssclass="titulo" Font-Size="X-Small"></asp:label></TD>
							</TR>
							<TR>
								<TD class="noprint">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True" class="noprint">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" align="right">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="10"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="10" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE class="noprint" id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" colSpan="3" height="6"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 160px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblFechaFil" runat="server" cssclass="titulo">Fecha:</asp:label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>
																			<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:label>
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 160px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblTipoFil" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2">
																			<cc1:combobox class="combo" id="cmbTipoFil" runat="server" Width="260px" AceptaNull="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 160px; HEIGHT: 7px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblEstaFil" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 7px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<cc1:combobox class="combo" id="cmbEstaFil" runat="server" Width="145px" AceptaNull="True">
																				<asp:ListItem Value="T">(Todos)</asp:ListItem>
																				<asp:ListItem Value="A">Activo</asp:ListItem>
																				<asp:ListItem Value="I">Inactivo</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" colSpan="3" height="6"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 19.26%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 13.43%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
										ItemStyle-Height="5px" PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
														Height="5">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG class="noprint" height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<TR>
					<TD width="9" background="imagenes/reciz.jpg"></TD>
					<TD vAlign="middle" align="right"><div class="noprint">
							<asp:Button id="btnList" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
								Text="Listar" Visible="false"></asp:Button><asp:Button id="btnImpri" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
								Text="Imprimir" Visible="true"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
					</TD>
					<TD width="13" background="imagenes/recde.jpg"></TD>
				</TR>
				<tr>
					<td width="9"><IMG class="noprint" height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td class="noprint" background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG class="noprint" height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
