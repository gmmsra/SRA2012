<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Comisiones" CodeFile="Comisiones.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Comisiones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
		function expandir()
		{
		try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function txtAnio_Change()
		{
			if (document.all('txtAnio').value !='')
			{				
				var lstrAnio = document.all('txtAnio').value;
				var lstrInse = document.all('hdnInse').value;
				LoadComboXML("ciclos_cargar", lstrInse+','+lstrAnio, "cmbCicl", "S");
				LoadComboXML("materiasXciclo_cargar", '@mate_inse_id='+lstrInse+',@cicl_id=0', "cmbMate", "S");
				LoadComboXML("divisiones_cargar", "@divi_cicl_id=0", "cmbDivi", "S");
		    }	
		}
		function txtAnioFil_Change()
		{
			if (document.all('txtAnioFil').value !='')
			{				
				var lstrAnio = document.all('txtAnioFil').value;
				var lstrInse = document.all('hdnInse').value;
				LoadComboXML("ciclos_cargar", lstrInse+','+lstrAnio, "cmbCiclFil", "T");
				LoadComboXML("materiasXciclo_cargar", '@mate_inse_id='+lstrInse+',@cicl_id=0', "cmbMateFil", "T");
		    }	
		}		
		function cmbMateFil_Change()
		{
			document.all('hdnSeleMateFil').value = document.all('cmbMateFil').value;
		}
		function cmbCiclFil_Change()
		{
			document.all('hdnSeleCiclFil').value = document.all('cmbCiclFil').value;
		    var sFiltro = "@mate_inse_id=" + document.all('hdnInse').value + ",@cicl_id=" + document.all('cmbCiclFil').value;
		    LoadComboXML("materiasXciclo_cargar", sFiltro, "cmbMateFil", "S");
		}
		function cmbCicl_Change(pCiclo)
		{
		    var sFiltro = pCiclo.value;
		    if (sFiltro=="") sFiltro="0"
		    LoadComboXML("materiasXciclo_cargar", sFiltro, "cmbMate", "S");
			document.all("txtCiclDesc").value = EjecutarMetodoXML("Utiles.ObtenerDescCiclo", sFiltro);
			cmbMate_Change(document.all("cmbMate"));
			
			LoadComboXML("divisiones_cargar", "@divi_cicl_id=" + sFiltro, "cmbDivi", "S");
		}
	
		function cmbMate_Change(pMate)
		{
		    var sFiltro = document.all("cmbCicl").value + ";" + pMate.value;
		    var vsRet;
		    vsRet = EjecutarMetodoXML("Utiles.FechasCicloMate", sFiltro).split(";");
			document.all("txtInicFecha").value = vsRet[0];
			document.all("txtFinaFecha").value = vsRet[1];
		}
		</script>
</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Comisiones</asp:label>
								</TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="97%" Visible="True">
            <TABLE id=TableFil style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
            align=left border=0>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24></TD>
                      <TD style="HEIGHT: 8px" width=42></TD>
                      <TD style="HEIGHT: 8px" width=26></TD>
                      <TD style="HEIGHT: 8px"></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnBusc runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
                      <TD style="HEIGHT: 8px" width=26></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24><IMG height=25 
                        src="imagenes/formfle.jpg" width=24 border=0></TD>
                      <TD style="HEIGHT: 8px" width=42><IMG height=25 
                        src="imagenes/formtxfiltro.jpg" width=113 border=0></TD>
                      <TD style="HEIGHT: 8px" width=26><IMG height=25 
                        src="imagenes/formcap.jpg" width=26 border=0></TD>
                      <TD style="HEIGHT: 8px" background=imagenes/formfdocap.jpg 
                      colSpan=3><IMG height=25 src="imagenes/formfdocap.jpg" 
                        border=0></TD></TR></TABLE></TD></TR>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD width=3 background=imagenes/formiz.jpg><IMG 
                        height=30 src="imagenes/formiz.jpg" width=3 border=0></TD>
                      <TD><!-- FOMULARIO -->
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" 
border=0>
                          <TR>
                            <TD style="WIDTH: 2%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblAnioFil runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 88%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:numberbox id=txtAnioFil onchange="javascript:txtAnioFil_Change();" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False" MaxValor="2099"></cc1:numberbox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 2%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 88%" 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" 
                          width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 2%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:Label id=lblCiclFil runat="server" cssclass="titulo">Ciclo:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 88%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo onchange="javascript:cmbCiclFil_Change();" id=cmbCiclFil runat="server" Width="100%" AceptaNull="false"></cc1:combobox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 2%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 88%" 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" 
                          width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 2%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" vAlign=top 
                            align=right background=imagenes/formfdofields.jpg>
<asp:label id=lblMateFil runat="server" cssclass="titulo">Materia:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 88%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox onchange="javascript:cmbMateFil_Change();" class=combo id=cmbMateFil runat="server" Width="100%" AceptaNull="false"></cc1:combobox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 2%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 88%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD></TR>
                          <TR>
                            <TD style="WIDTH: 2%" align=right 
                            background=imagenes/formdivfin.jpg height=2><IMG 
                              height=2 src="imagenes/formdivfin.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivfin.jpg height=2><IMG 
                              height=2 src="imagenes/formdivfin.jpg" width=1></TD>
                            <TD style="WIDTH: 88%" width=8 
                            background=imagenes/formdivfin.jpg height=2><IMG 
                              height=2 src="imagenes/formdivfin.jpg" 
                          width=1></TD></TR></TABLE></TD>
                      <TD width=2 background=imagenes/formde.jpg><IMG height=2 
                        src="imagenes/formde.jpg" width=2 
                  border=0></TD></TR></TABLE></TD></TR></TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="comi_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cicl_desc" HeaderText="Ciclo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_divi_nomb" HeaderText="Divisi�n"></asp:BoundColumn>
											<asp:BoundColumn DataField="_mate_desc" HeaderText="Materia"></asp:BoundColumn>
											<asp:BoundColumn DataField="_titular" HeaderText="Titular"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cant" HeaderText="Ins"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar una Nueva Comisi�n"></CC1:BotonImagen>
								</TD>
								<TD align="right">
									<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen>
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Comision</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkProf" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" CausesValidation="False" Height="21px"> Profesores</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkInsc" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" CausesValidation="False" Height="21px"> Inscripciones</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkSema" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" CausesValidation="False" Height="21px"> Horarios</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDias" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" CausesValidation="False" Height="21px"> Fechas</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
											BorderWidth="1px" Visible="False">
            <P align=right>
            <TABLE class=FdoFld id=Table2 style="WIDTH: 100%; HEIGHT: 106px" 
            cellPadding=0 align=left border=0>
              <TR>
                <TD>
                  <P></P></TD>
                <TD height=5>
<asp:Label id=lblTitu runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
                <TD vAlign=top align=right>&nbsp; 
<asp:ImageButton id=imgClose runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panCabecera runat="server" cssclass="titulo" Width="100%">
                  <TABLE id=TableCabecera style="WIDTH: 100%" cellPadding=0 
                  align=left border=0>
                    <TR>
                      <TD style="HEIGHT: 22px" align=right width=140>
<asp:Label id=lblAnio runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
                      <TD style="HEIGHT: 22px">
<cc1:numberbox id=txtAnio onchange="javascript:txtAnio_Change();" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="2100" MaxLength="4"></cc1:numberbox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblCicl runat="server" cssclass="titulo">Ciclo:</asp:Label>&nbsp; 
                      </TD>
                      <TD>
<cc1:combobox class=combo id=cmbCicl runat="server" Width="100%" Obligatorio="true" NomOper="ciclos_cargar" onchange="cmbCicl_Change(this)"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right></TD>
                      <TD align=left>
<asp:textbox id=txtCiclDesc runat="server" cssclass="textolibredeshab" Width="100%" Rows="4" TextMode="MultiLine" Font-Size="10pt" Font-Bold="True" Enabled="False"></asp:textbox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblDivi runat="server" cssclass="titulo">Divisi�n:</asp:Label></TD>
                      <TD>
<cc1:combobox class=combo id=cmbDivi runat="server" Width="100%" Obligatorio="true" NomOper="divisiones_cargar"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblMate runat="server" cssclass="titulo">Materia:</asp:Label></TD>
                      <TD>
<cc1:combobox class=combo id=cmbMate runat="server" Width="100%" Obligatorio="true" NomOper="materiasXciclo_cargar" onchange="cmbMate_Change(this);"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblInicFecha runat="server" cssclass="titulo">Inicio de Cursada:</asp:Label></TD>
                      <TD>
                        <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
                        border=0>
                          <TR>
                            <TD align=left width=100>
<cc1:DateBox id=txtInicFecha runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
                            <TD align=right width=90>
<asp:Label id=lblFinaFecha runat="server" cssclass="titulo">Finaliza:</asp:Label></TD>
                            <TD>
<cc1:DateBox id=txtFinaFecha runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblVaca runat="server" cssclass="titulo">Vacantes:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:numberbox id=txtVaca runat="server" cssclass="cuadrotexto" Width="50px"></cc1:numberbox></TD></TR></TABLE></asp:panel></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" align=right colSpan=3>
<asp:panel id=panProf runat="server" cssclass="titulo" Visible="False" Width="100%">
                  <TABLE id=TableProf style="WIDTH: 100%" cellPadding=0 
                  align=left border=0>
                    <TR>
                      <TD style="WIDTH: 100%" colSpan=2>
<asp:datagrid id=grdProf runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosProf" OnPageIndexChanged="grdProf_PageChanged" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="copr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prof_nyap" HeaderText="Profesor"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prti_desc" HeaderText="Tipo"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=16></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblProf runat="server" cssclass="titulo">Profesor:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbProf runat="server" Width="260px"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblPrti runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbPrti runat="server" Width="200px"></cc1:combobox></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=5></TD></TR>
                    <TR>
                      <TD vAlign=middle align=center colSpan=2 height=30>
<asp:Button id=btnAltaProf runat="server" cssclass="boton" Width="100px" Text="Agregar Prof."></asp:Button>&nbsp;&nbsp; 
<asp:Button id=btnBajaProf runat="server" cssclass="boton" Width="100px" Text="Eliminar Prof."></asp:Button>&nbsp; 
<asp:Button id=btnModiProf runat="server" cssclass="boton" Width="100px" Text="Modificar Prof."></asp:Button>&nbsp; 
<asp:Button id=btnLimpProf runat="server" cssclass="boton" Width="100px" Text="Limpiar Prof."></asp:Button></TD></TR></TABLE></asp:panel></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panInsc runat="server" cssclass="titulo" Visible="False" Width="100%">
                  <TABLE id=Table4 style="WIDTH: 100%" cellPadding=0 align=left 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 100%" align=left colSpan=2>
<asp:datagrid id=grdInsc runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosInsc" OnPageIndexChanged="grdInsc_PageChanged" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="coin_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_insc_desc" HeaderText="Alumno"></asp:BoundColumn>
																					<asp:BoundColumn DataField="coin_recur" HeaderText="Recu."></asp:BoundColumn>
																					<asp:BoundColumn DataField="coin_oyen" HeaderText="Oyen."></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=16></TD></TR>
                    <TR>
                      <TD align=right width="19%">
<asp:Label id=lblInsc runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbInsc runat="server" Width="100%"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblRecur runat="server" cssclass="titulo">Recursa:</asp:Label>&nbsp; 
                      </TD>
                      <TD>
<cc1:combobox class=combo id=cmbRecur runat="server" Width="60px"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblOyen runat="server" cssclass="titulo">Oyente:</asp:Label>&nbsp; 
                      </TD>
                      <TD>
<cc1:combobox class=combo id=cmbOyen runat="server" Width="60px"></cc1:combobox></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=5></TD></TR>
                    <TR>
                      <TD vAlign=middle align=center colSpan=2 height=30>
<asp:Button id=btnAltaInsc runat="server" cssclass="boton" Width="90px" Text="Agregar Insc."></asp:Button>&nbsp; 
<asp:Button id=btnBajaInsc runat="server" cssclass="boton" Width="90px" Text="Eliminar Insc."></asp:Button>&nbsp; 
<asp:Button id=btnModiInsc runat="server" cssclass="boton" Width="90px" Text="Modificar Insc."></asp:Button>&nbsp; 
<asp:Button id=btnLimpInsc runat="server" cssclass="boton" Width="90px" Text="Limpiar Insc."></asp:Button></TD></TR></TABLE></asp:panel></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panSema runat="server" cssclass="titulo" Visible="False" Width="100%">
                  <TABLE id=Table5 style="WIDTH: 100%" cellPadding=0 align=left 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 100%" colSpan=2>
<asp:datagrid id=grdSema runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosSema" OnPageIndexChanged="grdSema_PageChanged" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="cose_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_dia" HeaderText="D�a"></asp:BoundColumn>
																					<asp:BoundColumn DataField="cose_hora" HeaderText="Hora" DataFormatString="{0:HH:mm}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="cose_hs" HeaderText="Horas"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prac" HeaderText="Tipo"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=16></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblDia runat="server" cssclass="titulo">D�a:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbDia runat="server" Width="260px"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblHora runat="server" cssclass="titulo">Horario:</asp:Label>&nbsp; 
                      </TD>
                      <TD>
<cc1:HourBox id=txtHora runat="server" cssclass="cuadrotexto" Width="40px"></cc1:HourBox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblSemaHs runat="server" cssclass="titulo">Horas:</asp:Label>&nbsp; 
                      </TD>
                      <TD>
<cc1:numberbox id=txtSemaHs runat="server" cssclass="cuadrotexto" Width="80px"></cc1:numberbox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblSemaPrac runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbSemaPrac runat="server" Width="130px"></cc1:combobox></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=5></TD></TR>
                    <TR>
                      <TD vAlign=middle align=center colSpan=2 height=30>
<asp:Button id=btnAltaSema runat="server" cssclass="boton" Width="100px" Text="Agregar Hor."></asp:Button>&nbsp; 
<asp:Button id=btnBajaSema runat="server" cssclass="boton" Width="100px" Text="Eliminar Hor."></asp:Button>&nbsp; 
<asp:Button id=btnModiSema runat="server" cssclass="boton" Width="100px" Text="Modificar Hor."></asp:Button>&nbsp; 
<asp:Button id=btnLimpSema runat="server" cssclass="boton" Width="100px" Text="Limpiar Hor."></asp:Button></TD></TR></TABLE></asp:panel></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panDias runat="server" cssclass="titulo" Visible="False" Width="100%">
                  <TABLE id=tabDias style="WIDTH: 100%" cellPadding=0 align=left 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 100%" colSpan=2>
<asp:datagrid id=grdDias runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosDias" OnPageIndexChanged="grdDias_PageChanged" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="codc_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="codc_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy HH:mm}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prac" HeaderText="Tipo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="codc_hs" HeaderText="Horas"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=16></TD></TR>
                    <TR>
                      <TD align=right width="20%">
<asp:Label id=lblFecha runat="server" cssclass="titulo">Fecha:</asp:Label></TD>
                      <TD>
<cc1:DateBox id=txtFecha runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp; 
<cc1:HourBox id=txtDiaHora runat="server" cssclass="cuadrotexto" Width="40px"></cc1:HourBox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:label id=lblDiaHs runat="server" cssclass="titulo">Horas:</asp:label>&nbsp;</TD>
                      <TD>
<cc1:numberbox id=txtDiaHs runat="server" cssclass="cuadrotexto" Width="80px"></cc1:numberbox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblDiasPrac runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbDiasPrac runat="server" Width="130px"></cc1:combobox></TD></TR>
                    <TR>
                      <TD vAlign=bottom colSpan=2 height=5></TD></TR>
                    <TR>
                      <TD vAlign=middle align=center colSpan=2 height=30>
<asp:Button id=btnAltaDias runat="server" cssclass="boton" Width="80px" Text="Agregar D�a"></asp:Button>&nbsp; 
<asp:Button id=btnBajaDias runat="server" cssclass="boton" Width="85px" Text="Eliminar D�a"></asp:Button>&nbsp; 
<asp:Button id=btnModiDias runat="server" cssclass="boton" Width="90px" Text="Modificar D�a"></asp:Button>&nbsp; 
<asp:Button id=btnLimpDias runat="server" cssclass="boton" Width="85px" Text="Limpiar D�a"></asp:Button>&nbsp;&nbsp;&nbsp; 
<asp:Button id=btnGeneDias runat="server" cssclass="boton" Width="100px" Font-Bold="True" Text="Generar D�a"></asp:Button>&nbsp; 
<asp:Button id=btnBajaDiasTodos runat="server" cssclass="boton" Width="100px" Text="Borrar Todos"></asp:Button></TD></TR></TABLE></asp:panel></TD></TR></TABLE></P>
										</asp:panel>
										<ASP:PANEL Runat="server" ID="panBotones">
            <TABLE width="100%">
              <TR>
                <TD align=center>
<asp:Label id=lblBaja runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD></TR>
              <TR height=30>
                <TD align=center><A id=editar name=editar></A>
<asp:Button id=btnAlta runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp; 
<asp:Button id=btnBaja runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Baja"></asp:Button>&nbsp;&nbsp; 
<asp:Button id=btnModi runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Modificar"></asp:Button>&nbsp;&nbsp; 
<asp:Button id=btnLimp runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Limpiar"></asp:Button></TD></TR></TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<asp:TextBox id="hdnSeleCiclFil" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnSeleCicl" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnSeleMateFil" runat="server"></asp:TextBox>
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
				<asp:textbox id="hdnInse" runat="server"></asp:textbox>
				<asp:textbox id="hdnCoprId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCoinId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCoseId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCodcId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function mRecargarCombos()
		{
			if (document.all('txtAnio').value !='')
			{				
				var lstrAnio = document.all('txtAnio').value;
				var lstrInse = document.all('hdnInse').value;
				var lstrCiclo = document.all('hdnSeleCicl').value;
				LoadComboXML("ciclos_cargar", lstrInse+','+lstrAnio, "cmbCicl", "T");
				if (lstrCiclo != '')
				{
					document.all('cmbCicl').value = lstrCiclo;
					var lstrMate = document.all('hdnSeleMateFil').value;
					LoadComboXML("materiasXciclo_cargar", '@mate_inse_id='+lstrInse+',@cicl_id='+lstrCiclo, "cmbMatel", "T");
				}
		    }			
		}
		function mRecargarCombosFil()
		{
			if (document.all('txtAnioFil').value !='')
			{				
				var lstrAnio = document.all('txtAnioFil').value;
				var lstrInse = document.all('hdnInse').value;
				var lstrCiclo = document.all('hdnSeleCiclFil').value;
				LoadComboXML("ciclos_cargar", lstrInse+','+lstrAnio, "cmbCiclFil", "T");
				if (lstrCiclo != '')
				{
					document.all('cmbCiclFil').value = lstrCiclo;
					var lstrMate = document.all('hdnSeleMateFil').value;
					LoadComboXML("materiasXciclo_cargar", '@mate_inse_id='+lstrInse+',@cicl_id='+lstrCiclo, "cmbMateFil", "T");
					document.all('cmbMateFil').value = lstrMate;
				}
		    }			
		}
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		if (document.all('cmbCiclFil')!=null)
			mRecargarCombosFil();
		//if (document.all('cmbCicl')!=null)
			//mRecargarCombos();
		</SCRIPT>
	</BODY>
</HTML>
