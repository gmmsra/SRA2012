Imports System.Data.SqlClient


Namespace SRA


Partial Class Asambleas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents txtDescFil As NixorControls.TextBoxTab
   Protected WithEvents Peri As NixorControls.ComboBox
   Protected WithEvents periodo As NixorControls.TextBoxTab
   Protected WithEvents panDire As System.Web.UI.WebControls.Panel
   Protected WithEvents Chkintensivo As System.Web.UI.WebControls.CheckBox
   Protected WithEvents lnkCorre As System.Web.UI.WebControls.LinkButton

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "Asambleas"
   Private mstrCategorias As String = "Asambleas_categorias"
   Private mstrEstados As String = "Asambleas_estados"
   Private mstrTemas As String = "Asambleas_temas"
   Private mstrDistritos As String = "Asambleas_distritos"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String


   Private Enum Columnas As Integer
      Edit = 0
      AsamId = 1
   End Enum

   Private Enum ColumnasDeta As Integer
      Id = 1
   End Enum

   Private Enum ColumnasTema As Integer
      Edit = 0
      Flechas = 1
      AsteId = 2
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()

            mEstablecerPerfil()

            mConsultar(grdDato, True)
            tabLinks.Visible = False
            panBotones.Visible = False

            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If



      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      'btnBajaEqui.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaCate.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      'btnBajaCarre.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")

   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "asamblea_tipos", cmbTipo, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeti, "")
      clsWeb.gCargarComboBool(cmbCierre, "")
      cmbCierre.ValorBool = False
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Profesores, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrCarrEquiLong As Object
      Dim lstrCarrerasLong As Object

      lstrCarrerasLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrCarrerasLong, "carr_desc")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(grdDato, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


#End Region

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla

         Case mstrCategorias
            btnBajaCate.Enabled = Not (pbooAlta)
            btnModiCate.Enabled = Not (pbooAlta)
            btnAltaCate.Enabled = pbooAlta
            cmbCateC.Enabled = pbooAlta

         Case mstrEstados
            btnBajaEsta.Enabled = Not (pbooAlta)
            btnModiEsta.Enabled = Not (pbooAlta)
            btnAltaEsta.Enabled = pbooAlta
         Case mstrDistritos
            btnBajaDist.Enabled = Not (pbooAlta)
            btnModiDist.Enabled = Not (pbooAlta)
            btnAltaDist.Enabled = pbooAlta
         Case mstrTemas
            btnBajaTema.Enabled = Not (pbooAlta)
            btnModiTema.Enabled = Not (pbooAlta)
            btnAltaTema.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()
      mLimpiarCate()
      mLimpiarEsta()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.AsamId).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            hdnId.Text = .Item("asam_id")
            txtDesc.Valor = .Item("asam_desc")
                    txtAbre.Valor = .Item("asam_abre").ToString
                    txtNum.Valor = .Item("asam_nume").ToString
                    cmbTipo.Valor = .Item("asam_asti_id")
            txtReal.Fecha = .Item("asam_real_fecha")
            txtAnti.Fecha = .Item("asam_anti_fecha")
            txtCate.Fecha = .Item("asam_cate_fecha")
            txtPerio.Valor = .Item("asam_deuda_perio")
            txtAnio.Valor = .Item("asam_deuda_anio")
            cmbPeti.Valor = .Item("asam_deuda_peti_id")
            cmbCierre.ValorBool = .Item("asam_cerrada")
                    txtObse.Valor = .Item("asam_Obse").ToString
         End With
         mSetearEditor("", False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtNum.Text = ""
      txtDesc.Text = ""
      txtAbre.Text = ""
      txtObse.Text = ""
      txtReal.Text = ""
      txtAnti.Text = ""
      txtCate.Text = ""
      txtAnio.Text = ""
      txtPerio.Text = ""
      cmbPeti.Limpiar()
      cmbTipo.Limpiar()
      cmbCierre.ValorBool = False

      mLimpiarCate()
      mLimpiarEsta()
      mLimpiarTema()
      mLimpiardist()

      lblTitu.Text = ""

      grdCate.CurrentPageIndex = 0
      grdEsta.CurrentPageIndex = 0
      grdTema.CurrentPageIndex = 0
      grdDist.CurrentPageIndex = 0

      mCrearDataSet("")

      grdCate.DataSource = Nothing
      grdCate.DataBind()
      grdEsta.DataSource = Nothing
      grdEsta.DataBind()

      mConsultarTema()
      mShowTabs(1)

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      grdDato.Visible = Not panDato.Visible
      btnAgre.Visible = Not (panDato.Visible)

      lnkCabecera.Font.Bold = True
      lnkCate.Font.Bold = False
      lnkestados.Font.Bold = False
      lnkTema.Font.Bold = False
      lnkDist.Font.Bold = False

      panCabecera.Visible = True

      panCate.Visible = False
      PanEstados.Visible = False
      PanTemas.Visible = False
      PanDistritos.Visible = False

      tabLinks.Visible = pbooVisi
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panCabecera.Visible = False
      panCate.Visible = False
      PanEstados.Visible = False
      PanTemas.Visible = False
      PanDistritos.Visible = False

      lnkCabecera.Font.Bold = False
      lnkCate.Font.Bold = False
      lnkestados.Font.Bold = False
      lnkTema.Font.Bold = False
      lnkDist.Font.Bold = False

      Dim val As String

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos de la Asamblea: " & txtDesc.Text
         Case 2
            clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCateC, "S", "@cate_vota=1")
            panCate.Visible = True
            lnkCate.Font.Bold = True
            lblTitu.Text = "Categorias de la asamblea: " & txtDesc.Text
            grdCate.Visible = True
            btnBajaCate.Enabled = False
            btnModiCate.Enabled = False
            btnAltaCate.Enabled = True

         Case 3
            clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "S", "@esti_id = 3")
            PanEstados.Visible = True
            lnkestados.Font.Bold = True
            lblTitu.Text = "Estados de la asamblea: " & txtDesc.Text
            grdEsta.Visible = True
            btnBajaEsta.Enabled = False
            btnModiEsta.Enabled = False
            btnAltaEsta.Enabled = True

         Case 4
            PanTemas.Visible = True
            lnkTema.Font.Bold = True
            lblTitu.Text = "Temas de la asamblea: " & txtDesc.Text
            grdTema.Visible = True
            btnBajaTema.Enabled = False
            btnModiTema.Enabled = False
            btnAltaTema.Enabled = True

         Case 5
            clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDist, "S")
            PanDistritos.Visible = True
            lnkDist.Font.Bold = True
            lblTitu.Text = "Distritos de la asamblea: " & txtDesc.Text
            grdDist.Visible = True
            btnBajaDist.Enabled = False
            btnModiDist.Enabled = False
            btnAltaDist.Enabled = True
      End Select
   End Sub

   Private Sub mCargarComboConDataSet(ByVal cmb As NixorControls.ComboBox, ByVal dataTable As Data.DataTable, ByVal strId As String, ByVal strDesc As String)
      cmb.DataSource = dataTable
      cmb.DataValueField = strId
      cmb.DataTextField = strDesc
      cmb.DataBind()
      cmb.Items.Insert(0, "(Seleccione)")
      cmb.Items(0).Value = ""
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub


#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjAsamblea As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjAsamblea.Alta()
         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjAsamblea As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjAsamblea.Modi()

         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar(grdDato, True)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("asam_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("asam_desc") = txtDesc.Valor
                .Item("asam_abre") = txtAbre.Valor
                If txtNum.Valor.Length > 0 Then
                    .Item("asam_nume") = txtNum.Valor
                End If
                .Item("asam_asti_id") = cmbTipo.Valor
                .Item("asam_real_fecha") = txtReal.Fecha
                .Item("asam_anti_fecha") = txtAnti.Fecha
                .Item("asam_cate_fecha") = txtCate.Fecha
                .Item("asam_deuda_perio") = txtPerio.Valor
                .Item("asam_deuda_anio") = txtAnio.Valor
                .Item("asam_deuda_peti_id") = cmbPeti.Valor
                .Item("asam_cerrada") = cmbCierre.ValorBool
                .Item("asam_Obse") = txtObse.Valor
            End With
      Return mdsDatos
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrCategorias
      mdsDatos.Tables(2).TableName = mstrEstados
      mdsDatos.Tables(3).TableName = mstrTemas
      mdsDatos.Tables(4).TableName = mstrDistritos

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      mConsultarCate()

      grdEsta.DataSource = mdsDatos.Tables(mstrEstados)
      grdEsta.DataBind()

      mConsultarTema()

      mConsultarDist()

      Session(mstrTabla) = mdsDatos

   End Sub


#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub




#End Region

#Region "Detalle"
   Public Sub mEditarDatosCate(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrCate As DataRow
         Dim ldrTemp As DataRow

         hdnCateId.Text = E.Item.Cells(1).Text
         ldrTemp = mdsDatos.Tables(mstrCategorias).Select("asca_id=" & hdnCateId.Text)(0)

         cmbCateC.Valor = ldrTemp.Item("asca_cate_id")
         txtGrupo.Valor = ldrTemp.Item("asca_grupo")

         lstInst.SelectedIndex = -1

         For Each ldrCate In mdsDatos.Tables(mstrCategorias).Select("asca_cate_id=" & ldrTemp.Item("asca_cate_id"))
            If ldrCate.IsNull("asca_inst_id") Then
               lstInst.Items.FindByValue(0).Selected = True
            Else
               lstInst.Items.FindByValue(ldrCate.Item("asca_inst_id")).Selected = True
            End If
         Next

         mSetearEditor(mstrCategorias, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosEsta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrEsta As DataRow
         hdnEstaId.Text = E.Item.Cells(1).Text
         ldrEsta = mdsDatos.Tables(mstrEstados).Select("ases_id=" & hdnEstaId.Text)(0)

         With ldrEsta
            cmbEsta.Valor = .Item("ases_esta_id")
         End With

         mSetearEditor(mstrEstados, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosTema(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrTema As DataRow
         hdnTemaId.Text = E.Item.Cells(ColumnasTema.AsteId).Text
         ldrTema = mdsDatos.Tables(mstrTemas).Select("aste_id=" & hdnTemaId.Text)(0)

         With ldrTema
            txtTema.Valor = .Item("aste_tema")
         End With

         mSetearEditor(mstrTemas, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosdist(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrdist As DataRow
         hdnDistId.Text = E.Item.Cells(1).Text
         ldrdist = mdsDatos.Tables(mstrDistritos).Select("asdi_id=" & hdnDistId.Text)(0)

         With ldrdist
            cmbDist.Valor = .Item("asdi_dist_id")
         End With

         mSetearEditor(mstrDistritos, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal pOk As Boolean)
      Try
         Dim strFin As String = ""
         If (Not pOk) Then
            strFin = ",@ejecuta = N "
         End If
         mstrCmd = "exec " + mstrTabla + "_consul " + strFin
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarCategoria()
      Dim lstrFiltro As String
      Dim ldrCate As DataRow

      If (cmbCateC.SelectedItem.Text = "(Seleccione)") Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la categoria.")
      End If

      If lstInst.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la opci�n de instituciones.")
      End If

      If txtGrupo.Valor.ToString = "0" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el grupo.")
      End If

      For Each lItem As ListItem In lstInst.Items
         ldrCate = Nothing
         lstrFiltro = "asca_cate_id=" & cmbCateC.Valor
         If lItem.Value = "0" Then
            lstrFiltro += " AND asca_inst_id IS NULL"
         Else
            lstrFiltro += " AND asca_inst_id = " + lItem.Value
         End If

         With mdsDatos.Tables(mstrCategorias).Select(lstrFiltro)
            If .GetUpperBound(0) <> -1 Then
               ldrCate = .GetValue(0)
            End If
            If lItem.Selected Then
               If ldrCate Is Nothing Then
                  ldrCate = mdsDatos.Tables(mstrCategorias).NewRow
                  With ldrCate
                     .Item("asca_id") = clsSQLServer.gObtenerId(.Table, "asca_id")
                     .Item("asca_cate_id") = cmbCateC.Valor
                     .Item("asca_inst_id") = lItem.Value
                     If .Item("asca_inst_id") = 0 Then
                        .Item("asca_inst_id") = DBNull.Value
                     End If
                     .Item("_categoria") = cmbCateC.SelectedItem.Text
                     .Table.Rows.Add(ldrCate)
                  End With
               End If

               ldrCate.Item("asca_grupo") = txtGrupo.Valor

            Else
               If Not ldrCate Is Nothing Then
                  ldrCate.Delete()
               End If
            End If
         End With
      Next
   End Sub

   Private Sub mGuardarEstado()

      Dim ldrEsta As DataRow

      If hdnEstaId.Text = "" Then
         ldrEsta = mdsDatos.Tables(mstrEstados).NewRow
         ldrEsta.Item("ases_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrEstados), "ases_id")
      Else
         ldrEsta = mdsDatos.Tables(mstrEstados).Select("ases_id=" & hdnEstaId.Text)(0)
      End If

      If (cmbEsta.SelectedItem.Text = "(Seleccione)") Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el estado.")
      End If

      With ldrEsta
         .Item("ases_esta_id") = cmbEsta.Valor
         .Item("_Estado") = cmbEsta.SelectedItem.Text
      End With

      If (mEstaEnElDataSet(mdsDatos.Tables(mstrEstados), ldrEsta, "ases_esta_id", "ases_id")) Then
         Throw New AccesoBD.clsErrNeg("La asamblea ya tiene el estado seleccionado.")
      Else
         If (hdnEstaId.Text = "") Then
            mdsDatos.Tables(mstrEstados).Rows.Add(ldrEsta)
         End If
      End If
   End Sub

   Private Sub mGuardarTema()
      Dim ldrTema As DataRow

      If (txtTema.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Tema.")
      End If

      If mdsDatos.Tables(mstrTemas).Select.GetUpperBound(0) = 8 Then
         Throw New AccesoBD.clsErrNeg("Ya hay 9 temas ingresados")
      End If

      If hdnTemaId.Text = "" Then
         ldrTema = mdsDatos.Tables(mstrTemas).NewRow
         ldrTema.Item("aste_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTemas), "aste_id")
      Else
         ldrTema = mdsDatos.Tables(mstrTemas).Select("aste_id=" & hdnTemaId.Text)(0)
      End If

      With ldrTema
         .Item("aste_tema") = txtTema.Valor
      End With

      If (hdnTemaId.Text = "") Then
         mdsDatos.Tables(mstrTemas).Rows.Add(ldrTema)
      End If
   End Sub

   Private Sub mGuardarDistrito()
      Dim ldrdist As DataRow

      If hdnDistId.Text = "" Then
         ldrdist = mdsDatos.Tables(mstrDistritos).NewRow
         ldrdist.Item("asdi_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrDistritos), "asdi_id")
      Else
         ldrdist = mdsDatos.Tables(mstrDistritos).Select("asdi_id=" & hdnDistId.Text)(0)
      End If

      If (cmbDist.SelectedItem.Text = "(Seleccione)") Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el distdo.")
      End If

      With ldrdist
         .Item("asdi_dist_id") = cmbDist.Valor
         .Item("_dist_codi") = cmbDist.SelectedItem.Text
         .Item("_dist_desc") = cmbDist.SelectedItem.Text
      End With

      If (mEstaEnElDataSet(mdsDatos.Tables(mstrDistritos), ldrdist, "asdi_dist_id", "asdi_id")) Then
         Throw New AccesoBD.clsErrNeg("La asamblea ya tiene el distdo seleccionado.")
      Else
         If (hdnDistId.Text = "") Then
            mdsDatos.Tables(mstrDistritos).Rows.Add(ldrdist)
         End If
      End If
   End Sub

   Private Sub mGuardarDistritoTodos()
      Dim ldrdist As DataRow

      For Each lItem As ListItem In cmbDist.Items
         If lItem.Value <> "" Then
            If mdsDatos.Tables(mstrDistritos).Select("asdi_dist_id=" & lItem.Value).GetUpperBound(0) = -1 Then
               ldrdist = mdsDatos.Tables(mstrDistritos).NewRow

               With ldrdist
                  .Item("asdi_id") = clsSQLServer.gObtenerId(.Table, "asdi_id")
                  .Item("asdi_dist_id") = lItem.Value
                  .Item("_dist_codi") = lItem.Text
                  .Item("_dist_desc") = lItem.Text
                  .Table.Rows.Add(ldrdist)
               End With
            End If
         End If
      Next
   End Sub

   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCate As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
      Dim filtro As String
      filtro = campo1 + " = " + ldrCate.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrCate.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrCate.Item(campoId).ToString()
      Return (table.Select(filtro).GetLength(0) > 0)
   End Function

   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCate As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
      Return mEstaEnElDataSet(table, ldrCate, campo1, campo1, campoId)
   End Function

   Private Sub mLimpiarCate()
      hdnCateId.Text = ""
      cmbCateC.Limpiar()
      lstInst.SelectedIndex = -1
      txtGrupo.Text = ""
      mSetearEditor(mstrCategorias, True)
   End Sub

   Private Sub mLimpiarEsta()
      hdnEstaId.Text = ""
      cmbEsta.Limpiar()
      mSetearEditor(mstrEstados, True)
   End Sub

   Private Sub mLimpiarTema()
      hdnTemaId.Text = ""
      txtTema.Text = ""
      mSetearEditor(mstrTemas, True)
   End Sub

   Private Sub mLimpiardist()
      hdnDistId.Text = ""
      cmbDist.Limpiar()
      mSetearEditor(mstrDistritos, True)
   End Sub
#End Region



   Public Sub grdCate_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCate.EditItemIndex = -1
         If (grdCate.CurrentPageIndex < 0 Or grdCate.CurrentPageIndex >= grdCate.PageCount) Then
            grdCate.CurrentPageIndex = 0
         Else
            grdCate.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarCate()
      Try
         Dim ldsTemp As New DataSet
         Dim ldtTemp As DataTable
         Dim ldrTemp As DataRow
         Dim lstrDesc As String
         Dim lintCateId As Integer

         ldtTemp = ldsTemp.Tables.Add("temp")
         With ldtTemp
            .Columns.Add("asca_id", System.Type.GetType("System.Int32"))
            .Columns.Add("asca_cate_id", System.Type.GetType("System.Int32"))
            .Columns.Add("asca_grupo", System.Type.GetType("System.Int32"))
            .Columns.Add("_cate_desc", System.Type.GetType("System.String"))
            .Columns.Add("_ci", System.Type.GetType("System.String"))
            .Columns.Add("_cf", System.Type.GetType("System.String"))
            .Columns.Add("_sin", System.Type.GetType("System.String"))
         End With

         For Each lDr As DataRow In mdsDatos.Tables(mstrCategorias).Select("", "asca_cate_id")
            If lintCateId <> lDr.Item("asca_cate_id") Then
               ldrTemp = ldtTemp.NewRow
               ldrTemp.Item("asca_id") = lDr.Item("asca_id")
               ldrTemp.Item("asca_cate_id") = lDr.Item("asca_cate_id")
               ldrTemp.Item("asca_grupo") = lDr.Item("asca_grupo")
               ldrTemp.Item("_cate_desc") = lDr.Item("_categoria")
               ldtTemp.Rows.Add(ldrTemp)
            End If

            If lDr.IsNull("asca_inst_id") Then
               ldrTemp.Item("_sin") = "X"
            Else
               Select Case lDr.Item("asca_inst_id")
                  Case 1
                     ldrTemp.Item("_ci") = "X"
                  Case 3
                     ldrTemp.Item("_cf") = "X"
               End Select
            End If

            lintCateId = lDr.Item("asca_cate_id")
         Next

         grdCate.DataSource = ldsTemp
         grdCate.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub btnModiCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub ActualizarCategorias()
      Try
         mGuardarCategoria()

         mLimpiarCate()
         mConsultarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub ActualizarEstados()
      Try
         mGuardarEstado()

         mLimpiarEsta()
         grdEsta.DataSource = mdsDatos.Tables(mstrEstados)
         grdEsta.DataBind()


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub ActualizarDistritos()
      Try
         mGuardarDistrito()

         mLimpiardist()
         mConsultarDist()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCate.Click
      Try
         For Each lDr As DataRow In mdsDatos.Tables(mstrCategorias).Select("asca_cate_id=" & mdsDatos.Tables(mstrCategorias).Select("asca_id=" & hdnCateId.Text)(0).Item("asca_cate_id").ToString)
            lDr.Delete()
         Next

         mConsultarCate()
         mLimpiarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpCate.Click
      mLimpiarCate()
   End Sub

   Private Sub lnkCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCate.Click
      mShowTabs(2)
   End Sub

   Private Sub btnBajaEsta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaEsta.Click
      Try
         mdsDatos.Tables(mstrEstados).Select("ases_id=" & hdnEstaId.Text)(0).Delete()

         grdEsta.DataSource = mdsDatos.Tables(mstrEstados)
         grdEsta.DataBind()
         mLimpiarEsta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnModiEsta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiEsta.Click
      ActualizarEstados()
   End Sub

   Private Sub btnAltaEsta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaEsta.Click
      ActualizarEstados()
   End Sub

   Private Sub lnkestados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkestados.Click
      mShowTabs(3)
   End Sub

   Private Sub btnLimpEsta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpEsta.Click
      mLimpiarEsta()
   End Sub

   Public Sub grdEsta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdEsta.EditItemIndex = -1
         If (grdEsta.CurrentPageIndex < 0 Or grdEsta.CurrentPageIndex >= grdEsta.PageCount) Then
            grdEsta.CurrentPageIndex = 0
         Else
            grdEsta.CurrentPageIndex = E.NewPageIndex
         End If
         grdEsta.DataSource = mdsDatos.Tables(mstrEstados)
         grdEsta.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajadist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDist.Click
      Try
         mdsDatos.Tables(mstrDistritos).Select("asdi_id=" & hdnDistId.Text)(0).Delete()

         mConsultarDist()
         mLimpiardist()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaaDistTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaaDistTodos.Click
      Try
         For Each ldrDatos As DataRow In mdsDatos.Tables(mstrDistritos).Select
            ldrDatos.Delete()
         Next

         mConsultarDist()
         mLimpiardist()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnModidist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDist.Click
      ActualizarDistritos()
   End Sub

   Private Sub btnAltaDist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDist.Click
      ActualizarDistritos()
   End Sub

   Private Sub btnAltaDistTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDistTodos.Click
      Try
         mGuardarDistritoTodos()

         mLimpiardist()
         mConsultarDist()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub lnkDistritos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDist.Click
      mShowTabs(5)
   End Sub

   Private Sub btnLimpdist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDist.Click
      mLimpiardist()
   End Sub

   Public Sub grddist_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDist.EditItemIndex = -1
         If (grdDist.CurrentPageIndex < 0 Or grdDist.CurrentPageIndex >= grdDist.PageCount) Then
            grdDist.CurrentPageIndex = 0
         Else
            grdDist.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarDist()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub ActualizarTemas()
      Try
         mGuardarTema()

         mLimpiarTema()
         mConsultarTema()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaTema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaTema.Click
      Try
         mdsDatos.Tables(mstrTemas).Select("aste_id=" & hdnTemaId.Text)(0).Delete()

         mConsultarTema()
         mLimpiarTema()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnModiTema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiTema.Click
      ActualizarTemas()
   End Sub

   Private Sub btnAltaTema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaTema.Click
      ActualizarTemas()
   End Sub

   Private Sub lnkTema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkTema.Click
      mShowTabs(4)
   End Sub

   Private Sub btnLimpTema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpTema.Click
      mLimpiarTema()
   End Sub

   Public Sub grdTema_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdTema.EditItemIndex = -1
         If (grdTema.CurrentPageIndex < 0 Or grdTema.CurrentPageIndex >= grdTema.PageCount) Then
            grdTema.CurrentPageIndex = 0
         Else
            grdTema.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultarTema()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarTema()
      Dim lintInd As Integer = 1

      With mdsDatos.Tables(mstrTemas)
         For Each lDr As DataRow In .Rows
            If lDr.RowState <> DataRowState.Deleted Then
               lDr.Item("aste_nume") = lintInd
               lintInd += 1
            End If
         Next

         .DefaultView.Sort = "aste_nume"
         grdTema.DataSource = .DefaultView
         grdTema.DataBind()
      End With
   End Sub
   Private Overloads Sub mConsultarDist()
      mdsDatos.Tables(mstrDistritos).DefaultView.Sort = "_dist_codi"
      grdDist.DataSource = mdsDatos.Tables(mstrDistritos).DefaultView
      grdDist.DataBind()
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub grdTema_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdTema.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 Then
            e.Item.Cells(ColumnasTema.Flechas).Text = String.Format("<img style='cursor:hand;' ondragstart='javascript:mTurnosDrag({0});' ondragenter='javascript:mTurnosDropCancel(this);' ondragover='javascript:mTurnosDropCancel(this);' ondrop='javascript:mTurnosDrop({0});' style='cursor:hand;' border='0' alt='Modificar turno' src='imagenes/flechas.gif'>", e.Item.ItemIndex.ToString)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub hdnAsigDragDesti_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnAsigDragDesti.TextChanged
      Try
         Dim oDrOri, oDrDest As DataRow
         Dim lintIndOri, lintIndDest As Integer
         Dim lintBajas As Integer

         lintIndOri = hdnAsigDragOri.Text
         lintIndDest = hdnAsigDragDesti.Text

         With mdsDatos.Tables(mstrTemas)
            For i As Integer = 0 To lintIndOri
               If .Rows(i).RowState = DataRowState.Deleted Then
                  lintBajas += 1
               End If
            Next
            lintIndOri += lintBajas    'descuento los registros borrados

            lintBajas = 0
            For i As Integer = 0 To lintIndDest
               If .Rows(i).RowState = DataRowState.Deleted Then
                  lintBajas += 1
               End If
            Next
            lintIndDest += lintBajas   'descuento los registros borrados

            oDrOri = .Rows(lintIndOri)
            oDrDest = .NewRow
            oDrDest.ItemArray = oDrOri.ItemArray

            .Rows.Remove(oDrOri)

            .Rows.InsertAt(oDrDest, lintIndDest)
         End With

         mConsultarTema()
         mLimpiarTema()

         hdnAsigDragOri.Text = ""
         hdnAsigDragDesti.Text = ""

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
