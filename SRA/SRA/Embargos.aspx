<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Embargos" CodeFile="Embargos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Embargos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		
	
	
		 function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function mSetearRazas()
		{
						
			// Manejo del combo Registro Tipo
			if (document.all("txtcmbRazaFil")!=null)
			{
				if (document.all("cmbRazaFil").value != "")
				{
					if (!isPostBack())
					{
						LoadComboXML("rg_registros_tipos_productos_cargar", document.all("cmbRazaFil").value, "cmbRegiTipoFil", "N");
					}

					if (document.all("cmbRazaFil").value != document.all("hdnRazaAnteriorId").value)
					{
						document.all("hdnRazaAnteriorId").value = document.all("cmbRazaFil").value
						LoadComboXML("rg_registros_tipos_productos_cargar", document.all("cmbRazaFil").value, "cmbRegiTipoFil", "N");
					}
					if (document.all("cmbRazaFil").value != "")
						document.all("cmbRegiTipoFil").disabled = false;
					else
						document.all("cmbRegiTipoFil").disabled = true;
				}
			}
		}
		
		
		
		function usrCriaFil_cmbRazaCria_onchange()
		{
			if (document.all('usrProdFil_cmbProdRaza')!=null)
			{
				if (document.all('usrProdFil_cmbProdRaza').selectedIndex != document.all('usrCriaFil_cmbRazaCria').selectedIndex)
				{					
						document.all("usrProdFil_cmbProdRaza").value= document.all('usrCriaFil_cmbRazaCria').value;
						document.all("txtusrProdFil:cmbProdRaza").value= document.all('txtusrCriaFil:cmbRazaCria').value;
						document.all('usrProdFil_cmbProdRaza').selectedIndex = document.all('usrCriaFil_cmbRazaCria').selectedIndex;	
						document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
						
						document.all("usrProdFil:cmbProdRaza").onchange();
						document.all("usrProdFil:cmbProdRaza").disabled = true;
						document.all("txtusrProdFil:cmbProdRaza").disabled = true;
							
				}
				
				
				if (document.all('usrCriaFil_cmbRazaCria').value != '')
				{
					document.all('usrProdFil_cmbProdRaza').disabled = true;
					document.all('txtusrProdFil:cmbProdRaza').disabled = true;	
				}
				else
				{
					document.all('usrProd_cmbProdRaza').disabled = false;
					document.all('txtusrProd:cmbProdRaza').disabled = false;	
				}
			}
		}
				
		
		
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
					{
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 					 
	 				}
	 				else
	 				{
	 					document.all('lblNumeFil').innerHTML = 'Nro.:';
	 				 
	 				}
	 			}
 			}
		}	
		
			
		
		function mDetalle()	
		{
		try 
		  {		 
		   var str = document.all("cmbTipo").value;
  		   if (str==0)
		    {
		     document.all('lnkDetalle').disabled=false;
		    }
		   else
		     document.all('lnkDetalle').disabled=false;
	       }
	        catch(e){} 
		}
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('')" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO -------------------> &nbsp;
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="panCabecera" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Embargos</asp:label></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderStyle="Solid"
												BorderWidth="0">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblTipoMovimientoEmbargoFil" runat="server" cssclass="titulo">Estado del Parte:</asp:Label>&nbsp;</TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbTipoMovimientoFil" class="combo" runat="server" Width="200px" AceptaNull="False">
																						<asp:ListItem Selected="True">(Seleccione)</asp:ListItem>
																						<asp:ListItem Value="E">Ingresado Activo</asp:ListItem>
																						<asp:ListItem Value="X">Ingresado Finalizado</asp:ListItem>
																						<asp:ListItem Value="N">Anulado</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR id="tr1productoid">
																				<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																					align="right">
																					<asp:Label id="lblProdFil" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:PROH id="usrProdFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																						FilTipo="S" MuestraDesc="True" Ancho="800" AutoPostBack="False" EsPropietario="True"></UC1:PROH></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" background="imagenes/formfdofields.jpg"
																					align="right">
																					<asp:Label id="lblTipoFil" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbTipoFil" class="combo" runat="server" Width="208px" AceptaNull="False" AutoPostBack="True"
																						Obligatorio="true">
																						<asp:ListItem Value="-1" Selected="True">(Cualquier Tipo de Prestamo)</asp:ListItem>
																						<asp:ListItem Value="0">Todos los productos</asp:ListItem>
																						<asp:ListItem Value="1">Un producto</asp:ListItem>
																						<asp:ListItem Value="2">Conjunto de productos</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD height="15" background="imagenes/formfdofields.jpg" align="right">
																					<asp:label id="lblNroEmbargoFil" runat="server" cssclass="titulo">Nro.Embargo:</asp:label>&nbsp;</TD>
																				<TD vAlign="middle" background="imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtNroEmbargoFil" runat="server" cssclass="cuadrotexto" Width="140px" AceptaNull="False"
																						maxvalor="999999999"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR id="tr2productoid">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblTipoReporte" runat="server" cssclass="titulo">Tipo Reporte:</asp:Label>&nbsp;</TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbTipoReporte" class="combo" runat="server" Width="200px" AceptaNull="False">
																						<asp:ListItem Value="(Seleccione)">(Seleccione)</asp:ListItem>
																						<asp:ListItem Value="C" Selected="True">Completo</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivfin.jpg" colSpan="2"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="10" colSpan="3"></TD>
													</TR>
													<TR>
														<TD vAlign="top" colSpan="3" align="center">
															<asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
																HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
																OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="TRUE" DataField="emba_id" ReadOnly="True" HeaderText="Nro. Embargo"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_cliente" ReadOnly="True" HeaderText="Propietario">
																		<HeaderStyle Width="30%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_raza" ReadOnly="True" HeaderText="Raza">
																		<HeaderStyle Width="30%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_sexo" ReadOnly="True" HeaderText="Sexo">
																		<HeaderStyle Width="30%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_sra_nume" ReadOnly="True" HeaderText="HBA">
																		<HeaderStyle Width="30%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_rp" ReadOnly="True" HeaderText="RP">
																		<HeaderStyle Width="30%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_pref_nombre" ReadOnly="True" HeaderText="Pref-Producto">
																		<HeaderStyle Width="20%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="emba_inic_fecha" ReadOnly="True" HeaderText="F.desde" DataFormatString="{0:dd/MM/yyyy}">
																		<HeaderStyle Width="7%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="emba_fina_fecha" ReadOnly="True" HeaderText="F.hasta" DataFormatString="{0:dd/MM/yyyy}">
																		<HeaderStyle Width="7%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_emde_fina_fecha" ReadOnly="True" HeaderText="F.hasta Detallada" DataFormatString="{0:dd/MM/yyyy}">
																		<HeaderStyle Width="7%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado Detallado">
																		<HeaderStyle Width="10%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn Visible="False" DataField="_estadoGlobal" ReadOnly="True" HeaderText="Estado ">
																		<HeaderStyle Width="10%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_emba_tipo" ReadOnly="True" HeaderText="Tipo">
																		<HeaderStyle Width="10%"></HeaderStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<tr>
										<TD height="10" colSpan="3"></TD>
									</tr>
									<TR>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
												ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
												IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif"
												ToolTip="Argerar un Nuevo Registro"></CC1:BOTONIMAGEN></TD>
										<TD align="right"></TD>
										<TD width="100%" align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
												ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
									</TR>
									<tr>
										<TD height="10" colSpan="3"></TD>
									</tr>
									<TR>
										<TD colSpan="3" align="center"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> Embargo</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDetalle" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False" enabled="True"> Detalle</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderStyle="Solid"
												BorderWidth="1px" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left"> <!--<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>-->
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaPresentacion" runat="server" cssclass="titulo">Fecha Presentación:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaPresentacion" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="top" align="right">
																			<asp:Label id="lblNroEmbargo" runat="server" cssclass="titulo">Nro. Embargo:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<CC1:NUMBERBOX id="txtNroEmbargo" runat="server" cssclass="cuadrotexto" Width="65px" AceptaNull="False"
																				maxvalor="999999" Enabled="False"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblTipoMovimientoEmbargo" runat="server" cssclass="titulo">Estado del Parte:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 30px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbTipoMovimiento" class="combo" runat="server" Width="200px" AceptaNull="False">
																				<asp:ListItem Value="E" Selected="True">Ingresado Activo</asp:ListItem>
																				<asp:ListItem Value="X">Ingresado Finalizado</asp:ListItem>
																				<asp:ListItem Value="N">Anulado</asp:ListItem>
																				<asp:ListItem Value="R">Ingresado Retenido</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha desde:&nbsp;</asp:label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha hasta:&nbsp;</asp:label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Raza/Criador Embargado :</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<UC1:CLIE id="usrCria" runat="server" AceptaNull="false" Tabla="Criadores" Saltos="1,2" FilTipo="S"
																				MuestraDesc="False" Ancho="800" AutoPostBack="False" Criador="True" FilSociNume="True"
																				FilDocuNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:combobox id="cmbTipo" class="combo" runat="server" Width="208px" AceptaNull="False" AutoPostBack="True"
																				Obligatorio="true">
																				<asp:ListItem Value="0" Selected="True">Por Lote</asp:ListItem>
																				<asp:ListItem Value="1">Por HBA</asp:ListItem>
																				<asp:ListItem Value="2">Conjunto de HBA</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="top" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="450px" Height="50px" TextMode="MultiLine"
																				EnterPorTab="False" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panDetalle" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TablaDetalle" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdDetalle" runat="server" BorderWidth="1px" BorderStyle="None" width="100%"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="DataGridDeta_Page" OnEditCommand="mEditarDatosDeta" AutoGenerateColumns="False"
																				OnItemDataBound="mActualizarGrilla" PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="emde_id" ReadOnly="True" HeaderText="Nro. Embargo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_producto" ReadOnly="True" HeaderText="Producto">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_RazaId" ReadOnly="True" HeaderText="RazaId"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_sexo" ReadOnly="True" HeaderText="Sexo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Embargado">
																						<HeaderStyle Width="3%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:CheckBox ID="chkSelect" AutoPostBack="True" Checked="True" Runat="server" OnCheckedChanged="chkSelCheck_OnCheckedChanged"></asp:CheckBox>
																							<DIV runat="server" id="divCuot" style="DISPLAY: none"><%#(DataBinder.Eval(Container, "DataItem._POS"))%></DIV>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="_pos" ReadOnly="True" HeaderText="pos"></asp:BoundColumn>
																					<asp:BoundColumn DataField="emde_fina_fecha" ReadOnly="True" HeaderText="Fecha Finalizaci&#243;n"
																						DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Ver Errores">
																						<ItemTemplate>
																							<asp:Button runat="server" Text="Ver Errores" CommandName="Select" CausesValidation="false"
																								ID="btnVerErrores"></asp:Button>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="false" DataField="emde_prdt_id" ReadOnly="True" HeaderText="emde_prdt_id"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																			align="right">
																			<asp:Label id="lblProdFilDeta" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<UC1:PROH id="usrProdFilDeta" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																				FilTipo="S" MuestraDesc="True" Ancho="800" AutoPostBack="False" EsPropietario="True"></UC1:PROH></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																			align="right">&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">Filtrar por RP:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtRPBusq" runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaFinal" runat="server" cssclass="titulo">Fecha Final:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaFinalProducto" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBajaDeta" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnFiltrar" runat="server" cssclass="boton" Width="167px" CausesValidation="False"
																				Text="Filtrar"></asp:Button>
																			<asp:Button id="btnSeleccionarTodos" runat="server" cssclass="boton" Width="167px" CausesValidation="False"
																				Text="Seleccionar Todos"></asp:Button>
																			<asp:Button id="btnDeSeleccionarTodos" runat="server" cssclass="boton" Width="167px" CausesValidation="False"
																				Text="Desmarcar Todos"></asp:Button>
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="167px" Height="21px" CausesValidation="False"
																				Text="Modificar Fecha Finalización"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblSeleccionados" runat="server" cssclass="titulo" ForeColor="Brown"></asp:Label>
																			<asp:Label id="lblTotalProductos" runat="server" cssclass="titulo" ForeColor="Black"></asp:Label>
																			<asp:Label id="lblTotalEmbargados" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
																			<asp:Label id="lblTotalActivos" runat="server" cssclass="titulo" ForeColor="Blue"></asp:Label>
																			<asp:Label id="lblTotalLevanta" runat="server" cssclass="titulo" ForeColor="Green"></asp:Label>
																			<asp:Label id="lblTotalRetenidos" runat="server" cssclass="titulo" ForeColor="Orange"></asp:Label>
																			</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center">
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnCriaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="lblAltaId" runat="server"></asp:textbox>
				<asp:textbox id="lblAlta" runat="server"></asp:textbox>
			</DIV>
			<script language="JavaScript">
				if (document.all('lblAltaId')!=null && document.all('lblAltaId').value!='')
				{
					alert('Se ha asignado el nro de Embargo '+document.all('lblAltaId').value);
					document.all('lblAltaId').value = '';
				}
				
			</script>
		</form>
		<SCRIPT language="javascript">
			
		
			function mChequearTodos()
	{
		if (document.all("grdDetalle").children(0).children.length > 2)
		{
			for (var fila=3; fila <= document.all("grdDetalle").children(0).children.length; fila++)
			{
				document.all("grdDetalle__ctl" + fila.toString() + "_chkSelect").checked = true;
			}
			fila = fila-1;
			chkSelCheck(document.all('grdDetalle__ctl'+fila+'_chkSelect'));
		}
	}
		
		if (document.all["editar"]!= null)
			document.location='#editar';
			
		mDetalle();
			
		if (document.all["usrClieFil_txtCodi"]!= null && !document.all["usrClieFil_txtCodi"].disabled)
			document.all["usrClieFil_txtCodi"].focus();	
				
		if (document.all('cmbRazaFil')!=null)
			mNumeDenoConsul(document.all('cmbRazaFil'),true);
			
		if (document.all('cmbRaza')!=null)
			mNumeDenoConsul(document.all('cmbRaza'),false);
		
		
		
			
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
