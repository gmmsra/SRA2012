<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Becas" CodeFile="Becas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Becas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function mSetearConcepto()
		{
			var strRet1 = '';
			var strRet2 = '';
			var lstrCta1 = '';
			var lstrCta2 = '';
			if (document.all('cmbCuotConc').value!='')
				strRet1 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbMatrConc').value, "_cuenta");
			if (document.all('cmbCuotConc').value!='')
				strRet2 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbCuotConc').value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet1!='')
				lstrCta1 = strRet1.substring(0,1);
			if (strRet2!='')
				lstrCta2 = strRet2.substring(0,1);
			document.all('hdnCta').value = lstrCta1;
			if (lstrCta1=='' || lstrCta1=='1' || lstrCta1=='2' || lstrCta1=='3'
				|| lstrCta2=='' || lstrCta2=='1' || lstrCta2=='2' || lstrCta2=='3'
				|| lstrCta1 != lstrCta2)
			{
				document.all('cmbCcos').disabled = true;
				document.all('txtcmbCcos').disabled = true;
				document.all('cmbCcos').innerHTML = '';
				document.all('txtcmbCcos').value = '';
			}
			else
			{
				document.all('cmbCcos').disabled = false;
				document.all('txtcmbCcos').disabled = false;
				LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta1, "cmbCcos", "N");
			}			
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Becas</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" BorderStyle="Solid"
										Width="100%">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																	IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblDescFil" runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtDescFil" runat="server" cssclass="cuadrotexto" Width="65%"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 13.76%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="2" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" colspan="2" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="beca_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="beca_desc" ReadOnly="True" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="80%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_moneda" ReadOnly="True" HeaderText="Moneda">
												<HeaderStyle Width="8%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="beca_impo" ReadOnly="True" HeaderText="Importe">
												<HeaderStyle Width="3%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="beca_porc" ReadOnly="True" HeaderText="Porcentaje">
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_matricula" HeaderText="Matricula">
												<HeaderStyle Width="3%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_cuota" HeaderText="Cuota">
												<HeaderStyle Width="3%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD colSpan="2" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colspan="2">
									<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
													ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif"
													ToolTip="Agregar una Nueva Beca"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
													ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
													BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnImpr0.gif" ToolTip="Imprimir listado"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2" height="10"></TD>
							</TR>
							<TR>
								<TD align="center" colspan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripción:</asp:Label></TD>
														<TD colSpan="2" rowSpan="1">
															<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="65%" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="lblMone" runat="server" cssclass="titulo">Moneda:</asp:Label></TD>
														<TD style="HEIGHT: 16px" colSpan="2" height="16">
															<cc1:combobox class="combo" id="cmbMone" runat="server" Width="20%" Obligatorio="True"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label></TD>
														<TD style="HEIGHT: 16px" colSpan="2" height="16">
															<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																AutoPostback="True" esdecimal="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 8px" align="right">
															<asp:Label id="lblPorc" runat="server" cssclass="titulo">Porcentaje:</asp:Label></TD>
														<TD style="HEIGHT: 8px" colSpan="2">
															<cc1:numberbox id="txtPorc" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																AutoPostback="True" maxvalor="100"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right"></TD>
														<TD colSpan="2">
															<asp:CheckBox id="chkMatr" Checked="True" CssClass="titulo" Runat="server" Text="Aplica a matricula"></asp:CheckBox></TD>
													</TR>
													<TR>
														<TD align="right"></TD>
														<TD colSpan="2">
															<asp:CheckBox id="chkCuot" CssClass="titulo" Runat="server" Text="Aplica a cuota"></asp:CheckBox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblMatrConc" runat="server" cssclass="titulo">Concepto Beca para Matrícula:</asp:Label>&nbsp;</TD>
														<TD colSpan="2">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbMatrConc" runat="server" cssclass="cuadrotexto" Width="300px"
																			TextMaxLength="5" MostrarBotones="False" onchange="javascript:mSetearConcepto();" filtra="true"
																			NomOper="conceptos_cargar"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbMatrConc','Conceptos','@conc_grava=0');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblCuotConc" runat="server" cssclass="titulo">Concepto Beca para Cuotas:</asp:Label>&nbsp;</TD>
														<TD colSpan="2">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbCuotConc" runat="server" cssclass="cuadrotexto" Width="300px"
																			TextMaxLength="5" MostrarBotones="False" onchange="javascript:mSetearConcepto();" filtra="true"
																			NomOper="conceptos_cargar"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbCuotConc','Conceptos','@conc_grava=0');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
														<TD colSpan="3">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"
																			TextMaxLength="6" MostrarBotones="False" onchange="javascript:;" filtra="true" NomOper="centrosc_cargar"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearConcepto(); var lstrFil='-1'; if (document.all('hdnCta').value != '') lstrFil = document.all('hdnCta').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
															height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
			
		function ValorFijoChange()
		{
			if(window.event.srcElement.value!="")
				document.all["cmbTari"].selectedIndex=2;
			else
				document.all["cmbTari"].selectedIndex=1;
		}
		</SCRIPT>
	</BODY>
</HTML>
