<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.FacturacionExterna" CodeFile="FacturacionExterna.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Facturación externa</title>
		<meta content="False" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null);
	
		function expandir()
		 {
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		 }
		
		function Impresion()
		 {
		 if(document.all("hdnCompGenera").value!="")
			{
			 alert(document.all("hdnCompGenera").value);
			
			}
		 } 
		function mAbrirDetalle(pintID1,PintID2)
	     {
	
	     var vstrRet = LeerCamposXML("FACA801", pintID1 + ',' + PintID2, "f801_fecha,acti_desc,clie_apel,importe").split("|");
				 
		 	gAbrirVentanas("FacExterna_detalle_pop.aspx?id01="+pintID1+"&amp;"+"id02="+PintID2+"&amp;"+"fecha="+vstrRet[0]+"&amp;"+"actividad="+vstrRet[1]+"&amp;"+"cliente="+vstrRet[2]+"&amp;"+"importe="+vstrRet[3], 7, "700","500","100");
	    }
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');VerObservaciones();Impresion();" rightMargin="0"  onunload="gCerrarVentanas();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 10px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Facturación externa</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="97%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="10" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 70%; HEIGHT: 5px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbActi" runat="server" Width="250px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 20%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 70%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" width="100%" AceptaNull="False" FilCUIT="True" Tabla="Clientes"
																				 FilSociNume="True" MuestraDesc="False" FilDocuNume="True" FilLegaNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 20%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 70%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
													<A id="editar" name="editar"></A>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
											Visible="false">
											<TABLE width="100%">
												<TR>
													<TD>
														<P align="right">
															<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD>
																		<P></P>
																	</TD>
																	<TD height="5">
																		<asp:label id="lblFechaValor" runat="server" cssclass="titulo">Fecha valor de facturación:</asp:label>&nbsp;
																		<cc1:DateBox id="txtFechaValor" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="true"></cc1:DateBox>
																		<asp:Label id="lblExpo" runat="server" cssclass="titulo">Exposiciones:</asp:Label>&nbsp;
																		<cc1:combobox class="combo" id="cmbExpo" runat="server" Width="250px" NomOper="exposiciones_cargar"></cc1:combobox></TD>
																	<TD vAlign="top" align="right">&nbsp;
																		<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
																			ToolTip="Cerrar"></asp:ImageButton></TD>
																</TR>
																	<tr>
																
																	<TD style="WIDTH: 100%" colSpan="3">
																		<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																			<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																				<TR>
																					<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" align="center" colSpan="2">
																						<asp:datagrid id="grdFac" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="true"
																							HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																							AutoGenerateColumns="False">
																							<FooterStyle CssClass="footer"></FooterStyle>
																							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																							<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																							<Columns>
																								<asp:TemplateColumn HeaderText="Incl.">
																									<HeaderStyle Width="3%"></HeaderStyle>
																									<ItemTemplate>
																										<asp:CheckBox ID="chkSel" Runat="server"></asp:CheckBox>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:BoundColumn Visible="False" DataField="f801_sistgen" HeaderText="id01"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="f801_opergen" HeaderText="id02"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="resul" HeaderText="Resul"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_tipocomp" HeaderText="Tipo">																									
																								</asp:BoundColumn>																								
																									<asp:BoundColumn Visible="False" DataField="acti_id" HeaderText="id_activ"></asp:BoundColumn>
																								<asp:BoundColumn DataField="cliente" HeaderText="Cliente">
																									<HeaderStyle Width="30%"></HeaderStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="actividad" HeaderText="Actividad">
																									<HeaderStyle Width="35%"></HeaderStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="f801_fecha" HeaderText="Fecha " DataFormatString="{0:dd/MM/yyyy}">
																									<HeaderStyle Width="10%"></HeaderStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="importe" HeaderText="Importe($)" DataFormatString="{0:F2}">
																									<HeaderStyle HorizontalAlign="Right" Width="15%"></HeaderStyle>
																									<ItemStyle HorizontalAlign="Right"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="fecha_vto" HeaderText="F. Vto." DataFormatString="{0:dd/MM/yyyy}">
																									<HeaderStyle Width="10%"></HeaderStyle>
																								</asp:BoundColumn>
																								<asp:TemplateColumn>
																									<HeaderStyle Width="3%"></HeaderStyle>
																									<ItemTemplate>																					
																										<A href="javascript:mAbrirDetalle(<%#DataBinder.Eval(Container, "DataItem.f801_sistgen")%>,<%#DataBinder.Eval(Container, "DataItem.f801_opergen")%>);">
																											<img src='images/icodate.gif' border="0" alt="Ver detalle" style="cursor:hand;" />
																										</A>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn>
																									<HeaderStyle Width="3%"></HeaderStyle>
																									<ItemTemplate>
																										<img src='images/excla.gif' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.obser")%>" style="cursor:hand;" />
																									</ItemTemplate>
																								</asp:TemplateColumn>																								
																							</Columns>
																							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD>
																				</TR>
																				<TR height="30">
																					<TD align="left">
																						<asp:button id="btnTodos" runat="server" cssclass="boton" Width="80px" Text="Todos"></asp:button>
																						<asp:button id="btnNinguno" runat="server" cssclass="boton" Width="80px" Text="Ninguno"></asp:button></TD>
																				</TR>
																				<TR height="30">
																					<TD align="center">
																						<asp:button id="btngene" runat="server" cssclass="boton" Width="157px" Text="Generar"></asp:button></TD>
																				</TR>
																			</TABLE>
																		</asp:panel></TD>
																</TR>
															</TABLE>
														</P>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnCompGenera" runat="server"></asp:textbox><asp:textbox id="hdnImprimio" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnObser" runat="server" AutoPostBack="True"></asp:textbox><ASP:TEXTBOX id="hdnImprId" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		
		
		function VerObservaciones()
		{

			if(document.all("hdnObser").value!="")
			{
				alert(document.all("hdnObser").value);
				document.all("hdnObser").value="";
			}
		}
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtNume"]!= null&&!document.all["txtNume"].disabled)
			document.all["txtNume"].focus();
		
		</SCRIPT>
	</BODY>
</HTML>
