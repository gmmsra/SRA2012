Namespace SRA

Partial Class ABM_Opciones
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents btnList As NixorControls.BotonImagen
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label
   Protected WithEvents Combobox1 As NixorControls.ComboBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "rg_px"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()

            mCargarCombos()
            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=3 ", cmbRaza, "id", "descrip_codi", "S")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=3", cmbRazaFil, "id", "descrip_codi", "T")
      SRA_Neg.Utiles.gSetearRaza(cmbRaza)
      SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
         mstrCmd += " @pxrz_raza_id = " + IIf(cmbRazaFil.Valor.ToString = "", "null", cmbRazaFil.Valor.ToString)

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
      mstrCmd = "exec " + mstrTabla + "_consul"
      mstrCmd += " " + hdnId.Text

      Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      If ldsDatos.Tables(0).Rows.Count > 0 Then
         With ldsDatos.Tables(0).Rows(0)
            cmbRaza.Valor = .Item("pxrz_raza_id")
            txtPxPadre.Text = .Item("pxrz_padre")
            txtPxMadre.Text = .Item("pxrz_madre")
            txtPxCria.Text = .Item("pxrz_cria")
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
      End If
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiarFil()
      cmbRazaFil.Limpiar()
      mConsultar()
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      cmbRaza.Limpiar()
      txtPxPadre.Text = ""
      txtPxMadre.Text = ""
      txtPxCria.Text = ""
      mSetearEditor(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Private Sub mAlta()
      Dim lTransac As SqlClient.SqlTransaction
      Try
         mValidarDatos()
         Dim mstrCmd As String
         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         mstrCmd = "exec " + mstrTabla + "_alta"
         mstrCmd = mstrCmd + " " + cmbRaza.Valor
         mstrCmd = mstrCmd + ",'" + txtPxPadre.Text
         mstrCmd = mstrCmd + "','" + txtPxMadre.Text
         mstrCmd = mstrCmd + "','" + txtPxCria.Text + "'"
         mstrCmd = mstrCmd + "," + Session("sUserId").ToString()

         clsSQLServer.gExecute(lTransac, mstrCmd)

         clsSQLServer.gCommitTransac(lTransac)
         mMostrarPanel(False)
         mConsultar()
      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Dim lTransac As SqlClient.SqlTransaction
      Try
         mValidarDatos()

         Dim mstrCmd As String

         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         mstrCmd = "exec " + mstrTabla + "_modi"
         mstrCmd = mstrCmd + " " + hdnId.Text
         mstrCmd = mstrCmd + "," + cmbRaza.Valor
         mstrCmd = mstrCmd + ",'" + txtPxPadre.Text
         mstrCmd = mstrCmd + "','" + txtPxMadre.Text
         mstrCmd = mstrCmd + "','" + txtPxCria.Text + "'"
         mstrCmd = mstrCmd + "," + Session("sUserId").ToString()
         mstrCmd = mstrCmd + ",null"
         clsSQLServer.gExecute(lTransac, mstrCmd)

         clsSQLServer.gCommitTransac(lTransac)
         mMostrarPanel(False)
         mConsultar()
      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         Dim lstrRptName As String = "Px"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub cmbRaza_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbRaza.SelectedIndexChanged
      mConsultar()
   End Sub
#End Region

End Class
End Namespace
