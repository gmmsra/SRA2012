<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ComisionesRazas" CodeFile="ComisionesRazas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Comisiones de Criadores</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
		
		function mAbrirActa(pActaId)
		{
			gAbrirVentanas("ComisionesActas.aspx?sess=" + document.all("hdnSess").value + "&crac_cdoc_id=" + pActaId + "&Raza=" + document.all("hdnRazaId").value, 1)
		}
		
		function ValidaControl(pHab,pId)
		{
			if (pHab == "1") 
				document.all["divImagen"+pId].style.display="inline";
			else 
				document.all["divImagen"+pId].style.display="none";
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" onunload="gCerrarVentanas();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Comisiones de Criadores</asp:label></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
												BorderWidth="0" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																			CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" background="imagenes/formfdofields.jpg"
																			border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="280px"
																						filtra="true" NomOper="razas_cargar" MostrarBotones="False" Height="20px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="imagenes/formde.jpg" height="10"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel>
										</TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
												OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" BorderWidth="1px"
												BorderStyle="None" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="craz_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="_raza" ReadOnly="True" HeaderText="Raza">
														<HeaderStyle Width="85%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado">
														<HeaderStyle Width="15%"></HeaderStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD colspan="3" align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Raza"
												ForeColor="Transparent" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
												ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Height="124%" Visible="False">
												<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> General</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkAgenda" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Agenda</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkCriadores" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Integrantes</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkDocumentos" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Documentos</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 30px" vAlign="middle">
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="280px" filtra="true"
																				NomOper="razas_cargar" MostrarBotones="False" Height="20px" AutoPostBack="True" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panAgenda" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaAgenda" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD vAlign="top" align="center" colSpan="2">
																			<asp:datagrid id="grdAgenda" runat="server" BorderWidth="1px" BorderStyle="None" width="100%"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="DataGridAgen_Page" OnEditCommand="mEditarDatosAgen" AutoGenerateColumns="False"
																				PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="cage_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="cage_fecha" ReadOnly="True" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="cage_tema" ReadOnly="True" HeaderText="Temario">
																						<HeaderStyle Width="80%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblAgenFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 16px" height="16">
																			<cc1:DateBox id="txtAgenFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblAgenTema" runat="server" cssclass="titulo">Temario:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 16px" vAlign="top">
																			<cc1:textboxtab id="txtAgenTema" runat="server" cssclass="textolibre" Width="100%" TextMode="MultiLine"
																				Rows="10" height="100px"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblAgenObser" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 16px">
																			<cc1:textboxtab id="txtAgenObser" runat="server" cssclass="textolibre" Width="100%" TextMode="MultiLine"
																				Rows="10" height="100px"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblAgenDocAd" runat="server" cssclass="titulo">Documento Adjunto:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtAgenDocAd" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG id="imgDelAgenDoc" style="CURSOR: hand" onclick="javascript:mLimpiarPath('txtAgenDocAd','imgDelAgenDoc');"
																				alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<BR>
																			<INPUT id="filAgenDocAd" style="WIDTH: 340px; HEIGHT: 22px" type="file" size="49" name="File1"
																				runat="server"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:Label id="lblBajaAgen" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="2">
																			<asp:Button id="btnAltaAgen" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaAgen" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiAgen" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpAgen" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button>&nbsp;<BUTTON class="boton" id="btnDescAgen" style="FONT-WEIGHT: bold; WIDTH: 80px" onclick="javascript:mDescargarDocumento('hdnAgenId','comisiones_agenda','txtAgenDocAd');"
																				type="button" runat="server" value="Descargar">Descargar</BUTTON>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCriadores" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaCriadores" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD vAlign="top" align="center" colSpan="2">
																			<asp:datagrid id="grdCriad" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGridCriad_Page"
																				OnEditCommand="mEditarDatosCriad" AutoGenerateColumns="False" PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="ccri_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_criador" ReadOnly="True" HeaderText="Criador">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="ccri_desde_fecha" ReadOnly="True" DataFormatString="{0:dd/MM/yyyy}" HeaderText="F.Desde"></asp:BoundColumn>
																					<asp:BoundColumn DataField="ccri_hasta_fecha" ReadOnly="True" HeaderText="F.Hasta" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="ccri_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																		<TD>
																			<UC1:CLIE id="usrCria" runat="server" Alto="560" FilClie="True" FilCUIT="True" Ancho="780"
																				FilDocuNume="True" Saltos="1,1,1" criador="True" Tabla="Criadores"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCriadFechaDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:DateBox id="txtCriadFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCriadFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:DateBox id="txtCriadFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCriadRefer" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtCriadRefer" runat="server" cssclass="cuadrotexto" Width="376px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:Label id="lblBajaCriad" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="2">
																			<asp:Button id="btnAltaCriad" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaCriad" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiCriad" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpCriad" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDocumentos" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaDocumentos" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD vAlign="top" align="center" colSpan="3">
																			<asp:datagrid id="grdDocum" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGridDocum_Page"
																				OnEditCommand="mEditarDatosDocum" AutoGenerateColumns="False" PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="cdoc_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="cdoc_fecha" ReadOnly="True" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"
																						HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					<asp:BoundColumn DataField="cdoc_path" ReadOnly="True" HeaderText="Documento">
																						<HeaderStyle Width="45%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cdoc_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cdoc_acta" ReadOnly="True" HeaderText="N�Acta"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="3%"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Center"></ItemStyle>
																						<ItemTemplate>
																							<div style="DISPLAY: none" id="divImagen<%#DataBinder.Eval(Container, "DataItem.cdoc_id")%>">
																								<A href="javascript:mAbrirActa(<%#DataBinder.Eval(Container, "DataItem.cdoc_id")%>);">
																									<img onload="javascript:ValidaControl(<%#DataBinder.Eval(Container, "DataItem._acta")%>,<%#DataBinder.Eval(Container, "DataItem.cdoc_id")%>);" src='imagenes/Buscar16.gif' style="cursor:hand; VISIBILITY: " border="0" alt="Ver Detalle Acta" />
																								</A>
																							</div>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDocumFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<cc1:DateBox id="txtDocumFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDocumActa" runat="server" cssclass="titulo">Acta:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:numberbox id="txtDocumActa" runat="server" cssclass="cuadrotexto" Width="50px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 14px" vAlign="top" align="right">
																			<asp:Label id="lblDocumDoc" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px" colSpan="2" height="14">
																			<CC1:TEXTBOXTAB id="txtDocumDoc" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG id="imgDelDocDoc" style="CURSOR: hand" onclick="javascript:mLimpiarPath('txtDocumDoc','imgDelDocDoc');"
																				alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<BR>
																			<INPUT id="filDocumDoc" style="WIDTH: 340px; HEIGHT: 22px" type="file" size="49" name="File1"
																				runat="server"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDocumRefer" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtDocumRefer" runat="server" cssclass="cuadrotexto" Width="376px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:Label id="lblBajaDocum" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="3">
																			<asp:Button id="btnAltaDocum" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button>&nbsp;<BUTTON class="boton" id="btnDescDocum" style="FONT-WEIGHT: bold; WIDTH: 80px" onclick="javascript:mDescargarDocumento('hdnDocumId','comisiones_docum','txtDocumDoc');"
																				type="button" runat="server" value="Descargar">Descargar</BUTTON>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnAgenId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriadId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDocumId" runat="server"></asp:textbox>
				<asp:textbox id="hdnRazaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
