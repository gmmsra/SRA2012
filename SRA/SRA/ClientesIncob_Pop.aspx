<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ClientesIncob_Pop" CodeFile="ClientesIncob_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Busqueda de Incobrables</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/valDecimal.js"></script>
	</HEAD>
	<script language="JavaScript">
		function mRecalcular(pSele)  
		{
			var lCtrl
			if (document.all('hdnTipo').value == 'C')
				lCtrl  = pSele.id.replace('chkSel','hdnDeudaC');
			else
				lCtrl = pSele.id.replace('chkSel','hdnDeudaS');
			var lstrDeuda = document.all(lCtrl).value;
			var lstrTotal = document.all('txtTotal').value;
			if (pSele.checked)
				lstrTotal = gConvertDecimal(lstrTotal) + gConvertDecimal(lstrDeuda);
			else
				lstrTotal = gConvertDecimal(lstrTotal) - gConvertDecimal(lstrDeuda);
			document.all('txtTotal').value = gRedondear(lstrTotal,2);
			document.all('hdnTotal').value = document.all('txtTotal').value;
		}		
	</script>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR height="100%">
					<TD vAlign="top" colspan="2"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
							ItemStyle-Height="5px" PageSize="15" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1"
							HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
									<ItemTemplate>
										<asp:CheckBox ID="chkSel" onclick="javascript:mRecalcular(this);" style="font-color:black;font-size:10pt;" Runat="server"></asp:CheckBox>
										<DIV style="DISPLAY: none">
										<ASP:TEXTBOX id="hdnDeudaS" runat="server" Text=<%#DataBinder.Eval(Container, "DataItem.deuda_social")%>></ASP:TEXTBOX>
										<ASP:TEXTBOX id="hdnDeudaC" runat="server" Text=<%#DataBinder.Eval(Container, "DataItem.deuda_no_social")%>></ASP:TEXTBOX>
										</DIV>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="False" DataField="comp_clie_id"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="cocu_soci_id"></asp:BoundColumn>
								<asp:BoundColumn DataField="clie_apel" HeaderText="Cliente"></asp:BoundColumn>
								<asp:BoundColumn DataField="soci_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
								<asp:BoundColumn DataField="cuotas" HeaderText="Cuotas"></asp:BoundColumn>
								<asp:BoundColumn DataField="vigencia_social" HeaderText="Vigencia Soc."></asp:BoundColumn>
								<asp:BoundColumn DataField="deuda_social" HeaderText="Deuda Soc."></asp:BoundColumn>
								<asp:BoundColumn DataField="vigencia_no_social" HeaderText="Vigencia No Soc."></asp:BoundColumn>
								<asp:BoundColumn DataField="deuda_no_social" HeaderText="Deuda No Soc."></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="soci_lsin_id" HeaderText="Lista Socios"></asp:BoundColumn>								
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="right">
						<asp:Label id="lblTotal" runat="server" cssclass="titulo">Total Deuda:</asp:Label>&nbsp;
					</TD>
					<TD>
						<CC1:NUMBERBOX id="txtTotal" runat="server" cssclass="cuadrotextodeshab" Enabled="false" Width="65px"></CC1:NUMBERBOX></TD>
				</TR>
				<TR>
					<TD>
						<asp:button id="btnTodos" Width="80px" cssclass="boton" runat="server" Text="Todos"></asp:button>
						<asp:button id="btnNinguno" Width="80px" cssclass="boton" runat="server" Text="Ninguno"></asp:button>
					</TD>
					<TD vAlign="middle" align="right" height="40" colspan="2">
						<asp:button id="btnAceptar" Width="80px" cssclass="boton" runat="server" Text="Aceptar"></asp:button>&nbsp;
						<asp:button id="btnCerrar" runat="server" Text="Cerrar" Width="80px" cssclass="boton"></asp:button>
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none">
			<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="hdnTotal" runat="server"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="hdnSess" runat="server"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="hdnTipo" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
	</BODY>
</HTML>
