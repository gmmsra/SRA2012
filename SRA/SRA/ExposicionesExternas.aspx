<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ExposicionesExternas" CodeFile="ExposicionesExternas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Exposiciones Externas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="javascript:gSetearTituloFrame();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="TableABM" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
							<tr>
								<td width="100%" colSpan="3"></td>
							</tr>
							<tr>
								<td width="100%" colSpan="3"></td>
							</tr>
							<TR>
								<TD style="HEIGHT: 27px" vAlign="bottom" colSpan="3" height="27">
									<P><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Exposiciones Externas</asp:label></P>
									<P><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" Visible="True" BorderStyle="Solid"
											BorderWidth="0">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																		ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																		ImageOver="btnLimp2.gif" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 70px">
														<TABLE id="Table3" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE id="Table4" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 95px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></P>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="exex_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="exex_codi" ReadOnly="True" HeaderText="C&#243;digo">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="exex_abre" ReadOnly="True" HeaderText="Abreviatura"></asp:BoundColumn>
											<asp:BoundColumn DataField="exex_obse" ReadOnly="True" HeaderText="Observaciones">
												<HeaderStyle Width="50%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="exex_desde_fecha" HeaderText="Desde" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Center></asp:BoundColumn>
											<asp:BoundColumn DataField="exex_hasta_fecha" HeaderText="Hasta" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Center></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colspan="3">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
													ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Agregar un Nuevo Host"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="center" width="50"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD  colspan="3"><a name="editar"></a>
									<DIV align="left"><asp:panel id="panDato" runat="server" cssclass="titulo" Width="100%" Visible="False" BorderStyle="Solid"
											BorderWidth="1px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 18px">
															<P>
																<asp:TextBox id="txtId" runat="server" Visible="False" Width="54px"></asp:TextBox></P>
														</TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
															height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													<TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 18px">
															<P align="right">
																<asp:Label id="lblFechaD" runat="server" cssclass="titulo" Width="100px">Fecha Desde:</asp:Label>&nbsp;
															</P>
														</TD>
														<TD style="HEIGHT: 18px" colSpan="2">
															<cc1:DateBox id="txtFechaD" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="True"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
															height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													<TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 10px">
															<P align="right">
																<asp:Label id="lblFechaH" runat="server" cssclass="titulo" Width="100px">Fecha Hasta:</asp:Label>&nbsp;
															</P>
														</TD>
														<TD style="HEIGHT: 10px" colSpan="2">
															<cc1:DateBox id="txtFechaH" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="True"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
															height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													<TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 14px">
															<P align="right">
																<asp:Label id="lblCodi" runat="server" cssclass="titulo" Width="80px">C�digo:</asp:Label>&nbsp;
															</P>
														</TD>
														<TD style="HEIGHT: 14px" colSpan="2">
															<cc1:TextBoxtab id="txtCodi" runat="server" cssclass="cuadrotexto" Width="280px" obligatorio="True"></cc1:TextBoxtab></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
															height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													<TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 18px">
															<P align="right">
																<asp:Label id="lblAbre" runat="server" cssclass="titulo" Width="80px">Abreviaci�n:</asp:Label>&nbsp;
															</P>
														</TD>
														<TD style="HEIGHT: 18px" colSpan="2">
															<cc1:TextBoxtab id="txtAbre" runat="server" cssclass="cuadrotexto" Width="280px" obligatorio="True"></cc1:TextBoxtab></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 18px">
															<P align="right">
																<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;
															</P>
														</TD>
														<TD background="imagenes/formfdofields.jpg" colSpan="2">
															<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" obligatorio="True"
																Height="54px" EnterPorTab="False" TextMode="MultiLine" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 17px" align="center" colSpan="3" height="17">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR height="30">
														<TD align="center" colSpan="3">
															<asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;
															<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnPage" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
