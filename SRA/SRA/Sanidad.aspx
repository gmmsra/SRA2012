<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Sanidad" CodeFile="Sanidad.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Sanidad</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
		function usrProducto_usrCriadorFil_cmbRazaCria_onchange()
		{
			mSetearEnfermedades();
		}
		function mCargarProductos()
		{
			document.all("usrProductoFil:cmbProdRaza").value=document.all("cmbRazaFil").value;
			document.all("usrProductoFil:cmbProdRaza").onchange();
			document.all("usrProductoFil:cmbProdRaza").disabled=(document.all("cmbRazaFil").value!="");
			document.all("txtusrProductoFil:cmbProdRaza").disabled=(document.all("cmbRazaFil").value!="");
		
			
				
			
		}
		function mSetearEnfermedades()
		{
			var strRaza = document.all("usrProducto:usrCriadorFil:cmbRazaCria").value;
			if (strRaza!='')
			{  
				document.all('cmbEnfe').disabled = false;
				LoadComboXML("enfermedades_razas_cargar", "@raza_id="+strRaza, "cmbEnfe", "S");	
			}
			else
			{
				document.all('cmbEnfe').disabled = true;
			}
						
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Sanidad</asp:label></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
												Visible="True" Width="100%">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblProductoFil" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:PROH id="usrProductoFil" runat="server" AutoPostBack="False" FilSociNume="True" FilTipo="S"
																						Sanidad="True" FilDocuNume="True" AceptaNull="false" MuestraDesc="False" Tabla="productos"
																						Saltos="1,2" Ancho="800"></UC1:PROH></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblEnfeFil" runat="server" cssclass="titulo">Vacuna:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbEnfeFil" class="combo" runat="server" cssclass="cuadrotexto" Width="150px"
																						Obligatorio="False" MostrarBotones="False" filtra="False"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 16px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Vencimiento:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 16px" height="16" background="imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaVenceDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																						obligatorio="False"></cc1:DateBox>&nbsp;
																					<asp:Label id="Label3" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaVenceHastaFil" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																						obligatorio="False"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="sani_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_cria_id" ReadOnly="True" HeaderText="CriaID"></asp:BoundColumn>
													<asp:BoundColumn DataField="sani_fecha" ReadOnly="True" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"
														HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
													<asp:BoundColumn DataField="_Criador" ReadOnly="True" HeaderText="Criador">
														<HeaderStyle Width="40%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_Raza" ReadOnly="True" HeaderText="Raza">
														<HeaderStyle Width="30%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_Producto" ReadOnly="True" HeaderText="Producto">
														<HeaderStyle Width="30%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_enfermedad" ReadOnly="True" HeaderText="Enfermedad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
												ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
												ImageOver="btnNuev2.gif" ForeColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Nuevo Registro"></CC1:BOTONIMAGEN></TD>
										<TD align="right"><CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
												BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
												BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent" ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" Height="116px" width="100%">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD vAlign="top" colSpan="2" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 16px" align="right">
															<asp:Label id="lblProducto" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<UC1:PROH id="usrProducto" runat="server" AutoPostBack="False" FilSociNume="True" FilTipo="S"
																Sanidad="True" FilDocuNume="True" AceptaNull="false" MuestraDesc="True" Tabla="productos"
																Saltos="1,2" Ancho="800"></UC1:PROH></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 16px" align="right">
															<asp:Label id="lblFechaCertificado" runat="server" cssclass="titulo">Fecha Certificado:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																obligatorio="true"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
															<asp:Label id="lblLaborat" runat="server" cssclass="titulo">Laboratorio:</asp:Label>&nbsp;</TD>
														<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
															<cc1:combobox id="cmbLaboratorio" class="combo" runat="server" cssclass="cuadrotexto" Width="150px"
																Obligatorio="True" MostrarBotones="False" filtra="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
															<asp:Label id="lblEnfe" runat="server" cssclass="titulo">Vacuna:</asp:Label>&nbsp;</TD>
														<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
															<cc1:combobox id="cmbEnfe" class="combo" runat="server" cssclass="cuadrotexto" Width="150px" Obligatorio="True"
																MostrarBotones="False" filtra="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 16px" align="right">
															<asp:Label id="lblCalificacion" runat="server" cssclass="titulo">Resultado:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<cc1:combobox id="cmbResu" class="combo" runat="server" cssclass="cuadrotexto" Width="150px" Obligatorio="True"
																MostrarBotones="False" filtra="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 16px" vAlign="top" align="right">
															<asp:Label id="lblObservaciones" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<cc1:textboxtab id="txtObservaciones" runat="server" cssclass="textolibre" Width="90%" height="75px"
																Rows="10" TextMode="MultiLine"></cc1:textboxtab></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 16px" align="right">
															<asp:Label id="Label2" runat="server" cssclass="titulo">Vencimiento:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<cc1:DateBox id="txtFechaVence" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																obligatorio="false"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
