<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Migrated_Clientes" CodeFile="Clientes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Clientes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var gstrClie = '';

		function mSetearImagenes()
		{
			if (document.all('txtFoto').value != '')
			{
				document.all('imgFoto').src = 'fotos/'+document.all('txtFoto').value;
			}
			else
			{
				document.all('imgFoto').src = 'images/none.jpg';
				document.all('btnFotoVer').disabled = true;
			}	
			if (document.all('txtFirma').value != '')
			{
				document.all('imgFirma').src = 'fotos/'+document.all('txtFirma').value;
			}
			else
			{
				document.all('imgFirma').src = 'images/none.jpg';
				document.all('btnFirmaVer').disabled = true;
			}
			var lbooSocio = (document.all('hdnEsSocio').value == 'S');
			document.all('imgDelFoto').disabled = lbooSocio;
			document.all('FilFoto').disabled = lbooSocio;
			document.all('imgDelFirm').disabled = lbooSocio;
			document.all('FilFirma').disabled = lbooSocio;
		}

		function mSelecCP(pstrTipo)
		{	
			document.all["txt" + pstrTipo + "DireCP"].value = LeerCamposXML("localidades", document.all["cmb" + pstrTipo + "DireLoca"].value, "loca_cpos");
		}

		function mHabilitarControles(pTipo) 
		{
		 try
		 { 
		  //debugger;
			var lbooPersFisi = false;
			var lbooEsSocio = (document.all('hdnEsSocio').value=="S");
			<% If Not mbooActi Then %>
				if (pTipo != null)
				{
					lbooPersFisi = (pTipo.options[pTipo.selectedIndex].value==1)
				}			
				if (!lbooEsSocio)
					gHabilitarControl(document.all("txtNomb"),lbooPersFisi)
				else
				{
					//document.all('txtNomb').value='';
					document.all('txtNomb').disabled = true;
					document.all["txtFant"].disabled = true;
					document.all["txtCUIT"].disabled = true;
					document.all["cmbIVA"].disabled = true;
				}
					
				gHabilitarControl(document.all("txtFoto"),lbooPersFisi)
				gHabilitarControl(document.all("filFoto"),lbooPersFisi)
				gHabilitarControl(document.all("txtFirma"),lbooPersFisi)
				gHabilitarControl(document.all("filFirma"),lbooPersFisi)
				//gHabilitarControl(document.all("txtNaciFecha"),lbooPersFisi)
				//gHabilitarControl(document.all("txtFalleFecha"),lbooPersFisi)
				ActivarFecha("txtNaciFecha", lbooPersFisi && !lbooEsSocio);
				ActivarFecha("txtFalleFecha", lbooPersFisi && !lbooEsSocio);
				if (!lbooPersFisi)
				{
					document.all("txtNaciFecha").value = '';
					document.all("txtFalleFecha").value = '';
				}
				//gHabilitarControl(document.all("cmbTipoDocu"),lbooPersFisi)
				document.all("cmbTipoDocu").disabled= (!lbooPersFisi || lbooEsSocio)
				document.all("cmbSexo").disabled= (!lbooPersFisi || lbooEsSocio)

				if (!lbooEsSocio)
					gHabilitarControl(document.all("txtDocuNume"),lbooPersFisi)
				else
				{
					//document.all('txtDocuNume').value = '';
					document.all('txtDocuNume').disabled = true;
					//document.all("cmbTipoDocu").value = '';
					}
			<% End If %>
			var lbool;
			
			if (pTipo != null)
			 {
			  
			  var lstrRet = LeerCamposXML("personas_tipos", pTipo.options[pTipo.selectedIndex].value, "peti_gene");
			  if (lstrRet!= '')
			  {
			     //document.all("txtNomb").value ="";
			     //document.all("txtCUIT").value = "";
			     var lstrCons = LeerCamposXML("parametros", null, "para_iva_cons_final");
			     document.all("cmbIVA").value=lstrCons;
        	     lbool = false
			  }
			  else 
			  {			        
			     lbool = true
			  }
			  document.all("hdnTipoId").value = pTipo.value;
			  document.all("lnkIntegr").disabled = (pTipo.value != "2");
			  	
			 
			  //if (pTipo.value == "2" || pTipo.value == "3") 
			  if (pTipo.value != "1") 
			  {			        
				gHabilitarControl(document.all("txtNomb"),false);
				document.all("cmbTipoDocu").value = '';
				document.all("cmbSexo").value = '';
			  }
			  else
			  {			        
			    if (!lbooEsSocio)
					gHabilitarControl(document.all("txtNomb"),true);
				else
				{
					//document.all('txtNomb').value = '';
					document.all('txtNomb').disabled = true;
					}
			  }
			 	  	 
			 }
			}
			 catch(e)
				{
				alert("Error al setear controles");
				}       
			 
		}
		
		function btnLoca_click(pstrTipo)
	   	{
	   	  if (document.all("cmb" + pstrTipo + "DirePais").value == "" || document.all("cmb" + pstrTipo + "DirePcia").value == "" )
		   {
		    alert('Debe seleccionar Pa�s y Provincia.')
	       }	  
		  else	
	      {
	   	   gAbrirVentanas("localidades.aspx?EsConsul=1&t=popup&tipo=" + pstrTipo + "&pa=" + document.all("cmb" + pstrTipo + "DirePais").value + "&pr=" + document.all("cmb" + pstrTipo + "DirePcia").value,1);
		  }
		 
		}
		
		function btnSelecDire_click()
	   	{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Direciones de los integrantes&tabla=dire_clientesX&filtros='"+ document.all("hdnClieIds").value +"'", 7, "700","300","100");
		}
		
		function mValidaCuit()
		{ var strRet; 
		  var strfiltro;
		  var strCuit;
		 try
		  {
		  if (document.all("txtCUIT").value!='')
		    {
		      	strCuit =  document.all("txtCUIT").value;
		     	strCuit = strCuit.replace("-","");
		     	strCuit = strCuit.replace("-","");
		     	       
		        if (document.all("hdnId").value!='')
		          { 
		            strFiltro = document.all("hdnId").value + ", " + strCuit;
		          }
				 else
				 { 
				  strFiltro = "@clie_cuit= " + strCuit;
				  
				 }
				
			   
			    strRet = LeerCamposXML("clientes_dublic_cuit", strFiltro, "mens");
			   if(strRet!='')
			     alert(strRet)
	         }
	       }  
			 catch(e)
				{
				alert("Error al controlar C.U.I.T. duplicado");
				}   
	    }
	    
	    function mValidaDocu()
		{
			var strRet; 
		    var strfiltro;
		    var strCuit;
		    try
		    {
		       if (document.all("cmbTipoDocu").value!='' & document.all("txtDocuNume").value!='')
		       {
		      	     	       
		        if (document.all("hdnId").value!='')
		          { 
		            strFiltro = document.all("hdnId").value + ", " + document.all("cmbTipoDocu").value + ", " + document.all("txtDocuNume").value;
		          }
				 else
				 { 
				  strFiltro = "@clie_doti_id= " + document.all("cmbTipoDocu").value + " ,@clie_docu_nume= " + document.all("txtDocuNume").value;
				  
				 }
				
			   
			    strRet = LeerCamposXML("clientes_dublic_docu", strFiltro, "mens");
			   if(strRet!='')
			    {
			     alert(strRet);
			     document.all("txtDocuNume").value = "";
			    } 
	         }
	       }  
			 catch(e)
				{
				alert("Error al controlar C.U.I.T. duplicado");
				}   
	    }
	    
	    function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function mLimpiarFoto()
		{
			document.all("txtFoto").value = '';
		}
		
		function mLimpiarFirma()
		{
			document.all("txtFirma").value = '';
		}
		
		function mSugerirTipo(pTipo)
		{
			if (document.all("txtFant").value == '')
			{
				document.all("txtFant").value = pTipo+' ';
			}		
		}		
		
		function mCargarProvincias(pstrTipo)
		{
		    var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePais").value;
		    LoadComboXML("provincias_cargar", sFiltro, "cmb" + pstrTipo + "DirePcia", "S");
			if (document.all("cmb" + pstrTipo + "DirePais").value == '<%=Session("sPaisDefaId")%>')
				document.all('hdnValCodPostalPais').value = 'S';
			else
				document.all('hdnValCodPostalPais').value = 'N';		    
			document.all("cmb" + pstrTipo + "DirePcia").onchange();
		}
		
		function mCargarLocalidades(pstrTipo)
		{
		    var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePcia").value 
		    LoadComboXML("localidades_cargar", sFiltro, "cmb" + pstrTipo + "DireLoca", "S");
		}		

		function mDrag(pValor) 
		{
			//if (document.all("hdnHabi").value != 'N')
			//{
				gstrClie = pValor;
				event.dataTransfer.setData("Text", ""); 
				event.dataTransfer.effectAllowed = "copy";
			//}
		}
		function mDrop(pValor) 
		{
			document.all("hdnOrdenAnte").value = gstrClie;
			document.all("hdnOrdenProx").value = pValor;
			event.returnValue = false;                           
			event.dataTransfer.dropEffect = "copy"; 
			if (gstrClie != '')
			{
				document.frmABM.submit();
			}
		}
		function mDropCancel(pObje) 
		{
			event.returnValue = false;                  
			event.dataTransfer.dropEffect = "copy";  
		}		
		
	 function mOtrosDatosAbrir()
	   	{
          gAbrirVentanas("Alertas_pop.aspx?titulo=Otros Datos&amp;origen=O&amp;clieId=" + document.all("hdnId").value+"&amp;"+"FechaValor=&amp;sociId=", 7, "450","300","100");
	 	}

		function mClaveUnica()
		{
		  var strClave;
		  if (document.all("txtClaveUnica").value!='')
		    {
		      	strClave =  document.all("txtClaveUnica").value;
		     	strClave = strClave.replace("-","");
		     	strClave = strClave.replace("-","");		        
		        if (booIsNumber(strClave)==false)
		          {
		           document.all("txtClaveUnica").value='';
		           alert('La clave �nica no puede contener letras ni espacios intermedios.')
		          }
		          
		    }
        }
       
        function booIsNumber(astrValue)
			{
				for ( var i = 0 ; i < astrValue.length ; i++ )
				{
					var ch = astrValue.substring( i, i + 1 )
					if ( ( ch < "0" || "9" < ch ))
					{
						return false
					}
				}	
				return true
			}
			         
  
       
        </script>
	</HEAD>
	<BODY class="pagina" onunload="gCerrarVentanas();" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Clientes</asp:label></TD>
									</TR>
									<TR>
										<TD vAlign="middle" colSpan="3" noWrap><asp:panel id="panFiltros" runat="server" cssclass="titulo" width="100%" BorderStyle="none"
												BorderWidth="1px">
												<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
													<TR>
														<TD colSpan="4">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																			CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																			CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD colSpan="4">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="2" colSpan="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD>
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD height="6" background="imagenes/formfdofields.jpg" align="right"></TD>
																				<TD height="6" background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																				<TD style="WIDTH: 88%" height="6" background="imagenes/formfdofields.jpg" align="right">
																					<asp:ImageButton id="imgCloseFil" runat="server" ImageUrl="images\Close.bmp" Visible="False" ToolTip="Cerrar"
																						CausesValidation="False"></asp:ImageButton></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="3" align="right"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblClieNumeFil" runat="server" cssclass="titulo"> Nro. Cliente:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtClieNumeFil" runat="server" cssclass="cuadrotexto" Width="200px" MaxValor="9999999999999"
																						esdecimal="False"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg" align="right"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblApelFil" runat="server" cssclass="titulo">&nbsp;&nbsp;Apellido/Raz�n Social:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtApelFil" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB>&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<asp:checkbox id="chkBusc" Text="Buscar en..." Runat="server" CssClass="titulo"></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 22px" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="HEIGHT: 22px" background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																				<TD style="HEIGHT: 22px" background="imagenes/formfdofields.jpg">
																					<asp:checkbox id="chkBaja" Text="Incluir Dados de Baja" Runat="server" CssClass="titulo"></asp:checkbox>&nbsp;&nbsp;&nbsp;
																					<asp:checkbox id="chkAgrupa" Visible="False" Text="Incluir Agrupaciones" Runat="server" CssClass="titulo"
																						Checked="True"></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblSociNumeFil" runat="server" cssclass="titulo">Nro. Socio:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtSociNumeFil" runat="server" cssclass="cuadrotexto" Width="200px" MaxValor="9999999999999"
																						esdecimal="False" MaxLength="12"></cc1:numberbox></TD>
																			</TR>
																			<TR id="trLegaNumeFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblLegaNumeFil" runat="server" cssclass="titulo">Nro. Legajo:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbInseFil" class="combo" runat="server" Width="80px" AceptaNull="False"></cc1:combobox>
																					<cc1:numberbox id="txtLegaNumeFil" runat="server" cssclass="cuadrotexto" Width="200px" MaxValor="9999999999999"
																						esdecimal="False" MaxLength="12"></cc1:numberbox></TD>
																			</TR>
																			<TR id="trTarjFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblTarjFil" runat="server" cssclass="titulo">Nro. Tarjeta:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbTarjTipoFil" class="combo" runat="server" Width="180px" AceptaNull="False"></cc1:combobox>
																					<cc1:numberbox id="txtTarjNumeFil" runat="server" cssclass="cuadrotexto" Width="200px" MaxValor="9999999999999"
																						esdecimal="False" MaxLength="20" EsTarjeta="True"></cc1:numberbox></TD>
																			</TR>
																			<TR id="trCriaNumeFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblCriaNumeFil" runat="server" cssclass="titulo" Visible="False">Raza/Criador:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap>
																					<TABLE border="0" cellSpacing="0" cellPadding="0">
																						<TR>
																							<TD>
																								<cc1:combobox id="cmbRazaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="200px"
																									AceptaNull="False" Height="20px" NomOper="razas_cargar" MostrarBotones="False" filtra="true"></cc1:combobox></TD>
																							<TD>
																								<cc1:numberbox id="txtCriaNumeFil" runat="server" cssclass="cuadrotexto" Visible="False" Width="140px"
																									MaxValor="9999999999999" esdecimal="False" MaxLength="12"></cc1:numberbox></TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR id="trNoCriaNumeFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblNoCriaNumeFil" runat="server" cssclass="titulo" Visible="False">Nro. No Criador:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtNoCriaNumeFil" runat="server" cssclass="cuadrotexto" Visible="False" Width="200px"
																						MaxValor="9999999999999" esdecimal="False" MaxLength="12"></cc1:numberbox></TD>
																			</TR>
																			<TR id="trCuitFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblCuitFil" runat="server" cssclass="titulo">CUIT:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<CC1:CUITBOX id="txtCuitFil" runat="server" cssclass="cuadrotexto" Width="200px"></CC1:CUITBOX></TD>
																			</TR>
																			<TR id="trDocuFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblDocuFil" runat="server" cssclass="titulo">Nro. Documento:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbDocuTipoFil" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:combobox>
																					<cc1:numberbox id="txtDocuNumeFil" runat="server" cssclass="cuadrotexto" Width="143px" MaxValor="9999999999999"
																						esdecimal="False"></cc1:numberbox></TD>
																			</TR>
																			<TR id="trEntiFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblEntiFil" runat="server" cssclass="titulo">Entidad:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbEntiFil" class="combo" runat="server" Width="260px"></cc1:combobox></TD>
																			</TR>
																			<TR id="trPagoFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblPagoFil" runat="server" cssclass="titulo" Visible="False">Medio de Pago:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbPagoFil" class="combo" runat="server" Visible="False" Width="260px"></cc1:combobox></TD>
																			</TR>
																			<TR id="trEmprFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblEmprFil" runat="server" cssclass="titulo" Visible="False">Empresa:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbEmprFil" class="combo" runat="server" Visible="False" Width="260px"></cc1:combobox></TD>
																			</TR>
																			<TR id="trCunicaFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblCunicaFil" runat="server" cssclass="titulo">Clave �nica:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtCunicaFil" onkeypress="gMascaraCunica(this)" runat="server" cssclass="cuadrotexto"
																						Width="161px" maxlength="13"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR id="trExpoFil" runat="server">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="imagenes/formfdofields.jpg"></TD>
																				<TD background="imagenes/formfdofields.jpg" noWrap align="right">
																					<asp:label id="lblExpoFil" runat="server" cssclass="titulo">Nro.Expositor:&nbsp;</asp:label></TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtExpoFil" runat="server" cssclass="cuadrotexto" Width="161px" MaxValor="9999999999999"
																						esdecimal="False" MaxLength="12"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="3" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD height="10" vAlign="top" colSpan="3" align="right"></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3" align="right"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mSeleccionarCliente"
												OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
												<FooterStyle CssClass="footer"></FooterStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="15px"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton3" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
																<img src='images/sele.gif' border="0" alt="Seleccionar Cliente" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn>
														<HeaderStyle Width="15px"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="clie_id" ReadOnly="True" HeaderText="Nro.Cliente"></asp:BoundColumn>
													<asp:BoundColumn DataField="_descrip" HeaderText="Descripci&#243;n"></asp:BoundColumn>
													<asp:BoundColumn DataField="clie_apel" ReadOnly="True" HeaderText="Apellido/Raz&#243;n Social"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="clie_nomb" HeaderText="Nombre"></asp:BoundColumn>
													<asp:BoundColumn DataField="_clie_socio_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_raza" HeaderText="Raza"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_cria_nume" HeaderText="Nro.Criador"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_nocria_nume" HeaderText="Nro.No Criador"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_cuit" ReadOnly="True" HeaderText="CUIT"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_docu" HeaderText="Documento"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_enti" HeaderText="Entidad"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_pago" HeaderText="Medio de Pago"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_empr" HeaderText="Empresa"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_cunica" HeaderText="Clave Unica"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_instituto" HeaderText="Instituto"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_clie_lega_nume" HeaderText="Nro.Legajo"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<tr>
										<td height="10" colSpan="3"></td>
									</tr>
									<TR>
										<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
												BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
												ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ToolTip="Agregar un Nuevo Cliente" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
										<TD colSpan="2" align="right">
											<table id="tabLinks" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
												<tr>
													<td width="1"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="28"></td>
													<td background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="28"></td>
													<td width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="28"></td>
													<td background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
															cssclass="solapa" CausesValidation="False" Width="80px" Height="21px"> Cliente</asp:linkbutton></td>
													<td width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></td>
													<td background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkTele" runat="server" cssclass="solapa"
															CausesValidation="False" Width="85px" Height="21px"> Otros Tel.</asp:linkbutton></td>
													<td width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></td>
													<td background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDire" runat="server" cssclass="solapa"
															CausesValidation="False" Width="85px" Height="21px"> Otras Dir.</asp:linkbutton></td>
													<td width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></td>
													<td background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkMail" runat="server" cssclass="solapa"
															CausesValidation="False" Width="75px" Height="21px"> Otros Mails</asp:linkbutton></td>
													<td width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></td>
													<td background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDocu" runat="server" cssclass="solapa"
															CausesValidation="False" Width="70px" Height="21px"> Documentaci�n</asp:linkbutton></td>
													<td id="tdIntegrSep" width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></td>
													<td id="tdIntegr" background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkIntegr" runat="server"
															cssclass="solapa" CausesValidation="False" Width="75px" Height="21px"> Codeudores</asp:linkbutton></td>
													<td width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="28"></td>
												</tr>
											</table>
										</TD>
									</TR>
									<TR>
										<TD colSpan="3" align="center">
											<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
													Visible="False" Height="116px">
													<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
														align="left">
														<TR>
															<TD height="5">
																<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
															<TD vAlign="top" align="right">
																<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																	<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																		<TR>
																			<TD width="15%" align="right">
																				<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:</asp:Label></TD>
																			<TD width="85%">
																				<cc1:combobox id="cmbTipo" class="combo" runat="server" Width="25%" AceptaNull="True" onchange="mHabilitarControles(this);"
																					obligatorio="False"></cc1:combobox>&nbsp;&nbsp;
																				<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>
																				<asp:Label id="txtEsta" runat="server" cssclass="titulo"></asp:Label>&nbsp;&nbsp;<BUTTON style="WIDTH: 90px" id="btnOtrosDatos" class="boton" onclick="mOtrosDatosAbrir();"
																					runat="server" value="Otros Datos">Otros Datos</BUTTON>
																			</TD>
																		</TR>
																		<% if not mbooAgru then %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<% end if %>
																		<TR>
																			<TD vAlign="middle" align="right">
																				<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label></TD>
																			<TD vAlign="middle">
																				<asp:Label id="txtDesc" runat="server" cssclass="titulo"></asp:Label></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="right">
																				<asp:Label id="lblApel" runat="server" cssclass="titulo">Apellido/R.Social:</asp:Label></TD>
																			<TD vAlign="middle">
																				<CC1:TEXTBOXTAB id="txtApel" runat="server" cssclass="cuadrotexto" Width="100%" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="right">
																				<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:Label></TD>
																			<TD vAlign="middle">
																				<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="100%" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<% if false then 'not mbooAgru then %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right"></TD>
																			<TD vAlign="middle">
																				<cc1:combobox id="cmbTipoSuge" class="combo" runat="server" Visible="True" Width="50%"></cc1:combobox></TD>
																		</TR>
																		<% end if %>
																		<TR>
																			<TD style="HEIGHT: 49px" vAlign="middle" align="right">
																				<asp:Label id="lblFant" runat="server" cssclass="titulo" Height="23px">Interface Clientes:</asp:Label></TD>
																			<TD style="HEIGHT: 49px" vAlign="baseline">
																				<P>
																					<CC1:TEXTBOXTAB id="txtFant" runat="server" cssclass="cuadrotexto" Visible="True" Width="100%" AceptaNull="False"></CC1:TEXTBOXTAB>
																					<asp:Label id="LblAdvertencia" runat="server" cssclass="titulo" ForeColor="Red">Debe obligatoriamente completar o actualizar este campo al cambiar el Apellido o Nombre</asp:Label></P>
																			</TD>
																		</TR>
																		<% if not mbooAgru then %>
																		<TR>
																			<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="2"
																				align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="right">
																				<asp:Label id="lblCUIT" runat="server" cssclass="titulo">CUIT:</asp:Label></TD>
																			<TD vAlign="middle">
																				<CC1:CUITBOX id="txtCUIT" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:CUITBOX>
																				<asp:Label id="lblIVA" runat="server" cssclass="titulo">Posic.IVA:</asp:Label>
																				<cc1:combobox id="cmbIVA" class="combo" runat="server" Width="135px" AceptaNull="False" Obligatorio="True"></cc1:combobox>
																				<asp:Label id="lblTipoDocu" runat="server" cssclass="titulo">Tipo Doc.:</asp:Label>
																				<cc1:combobox id="cmbTipoDocu" class="combo" runat="server" Width="90px" onchange="mValidaDocu();"></cc1:combobox>
																				<asp:Label id="lblDocuNume" runat="server" cssclass="titulo">N�:</asp:Label>
																				<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="70px" MaxValor="9999999999999"
																					esdecimal="False" onchange="mValidaDocu();"></cc1:numberbox>&nbsp
																				<asp:Label ID="lblSexo" runat="server" Text="Sexo:" CssClass="titulo"></asp:Label>
																				<cc1:combobox id="cmbSexo" class="combo" runat="server" AceptaNull="True"
																									Width="100px"><asp:ListItem Selected="True" value="">Seleccione</asp:ListItem><asp:ListItem Value="0">Femenino</asp:ListItem><asp:ListItem Value="1">Masculino</asp:ListItem></cc1:combobox>
																			</TD>
																			                                                                               
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="right">
																				<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label></TD>
																			<TD vAlign="middle">
																				<TABLE border="0" cellSpacing="0" cellPadding="0">
																					<TR>
																						<TD>
																							<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																							<asp:Label id="lblFalleFecha" runat="server" cssclass="titulo">Fecha Fallecimiento:</asp:Label></TD>
																						<TD>
																							<cc1:DateBox id="txtFalleFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																							<asp:CheckBox id="chkSinCorreo" Text="Sin Correo" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<TABLE style="WIDTH: 100%" id="tabDefaDire" class="marco" border="0" cellPadding="0" align="left"
																					runat="server">
																					<TR>
																						<TD width="15%" align="right">
																							<asp:Label id="lblDefaDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																						<TD colSpan="3">
																							<CC1:TEXTBOXTAB id="txtDefaDire" runat="server" cssclass="cuadrotexto" Width="100%"></CC1:TEXTBOXTAB></TD>
																					</TR>
																					<TR>
																						<TD style="HEIGHT: 11px" align="right">
																							<asp:Label id="lblDefaDirePais" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																						<TD width="35%">
																							<cc1:combobox id="cmbDefaDirePais" class="combo" runat="server" Width="80%" onchange="mCargarProvincias('Defa')"></cc1:combobox></TD>
																						<TD align="right">
																							<asp:Label id="lblDefaDirePcia" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																						<TD width="35%">
																							<cc1:combobox id="cmbDefaDirePcia" class="combo" runat="server" Width="80%" NomOper="provincias_cargar"
																								onchange="mCargarLocalidades('Defa')"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD style="HEIGHT: 11px" align="right">
																							<asp:Label id="lblDefaDireLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																						<TD noWrap>
																							<cc1:combobox id="cmbDefaDireLoca" class="combo" runat="server" Width="207px" NomOper="localidades_cargar"
																								onchange="mSelecCP('Defa');"></cc1:combobox><BUTTON style="WIDTH: 25.47%; HEIGHT: 20px" id="btnDefaLoca" class="boton" onclick="btnLoca_click('Defa');"
																								runat="server" value="Detalles">Localidades</BUTTON></TD>
																						<TD align="right">
																							<asp:Label id="lblDefaDireCP" runat="server" cssclass="titulo">C�digo Postal:</asp:Label>&nbsp;</TD>
																						<TD>
																							<CC1:CODPOSTALBOX id="txtDefaDireCP" runat="server" cssclass="cuadrotexto" Width="20%"></CC1:CODPOSTALBOX></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblDefaDireRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																						<TD colSpan="3">
																							<CC1:TEXTBOXTAB id="txtDefaDireRefe" runat="server" cssclass="cuadrotexto" Width="60%"></CC1:TEXTBOXTAB></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<TABLE style="WIDTH: 100%" id="tabDefaTele" class="marco" border="0" cellPadding="0" align="left"
																					runat="server">
																					<TR>
																						<TD style="HEIGHT: 11px" width="15%" align="right">
																							<asp:Label id="lblDefaTeleNume" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																						<TD width="50%">
																							<CC1:TEXTBOXTAB id="txtDefaTeleArea" runat="server" cssclass="cuadrotexto" Width="15%"></CC1:TEXTBOXTAB>&nbsp;
																							<CC1:TEXTBOXTAB id="txtDefaTeleNume" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
																						<TD align="right">
																							<asp:Label id="lblDefaTeleTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																						<TD width="30%">
																							<cc1:combobox id="cmbDefaTeleTipo" class="combo" runat="server" Width="50%"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD width="15%" align="right">
																							<asp:Label id="lblDefaTeleRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																						<TD colSpan="3">
																							<CC1:TEXTBOXTAB id="txtDefaTeleRefe" runat="server" cssclass="cuadrotexto" Width="50%"></CC1:TEXTBOXTAB></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<TABLE style="WIDTH: 100%" id="tabDefaMail" class="marco" border="0" cellPadding="0" align="left"
																					runat="server">
																					<TR>
																						<TD width="15%" align="right">
																							<asp:Label id="lblDefaMail" runat="server" cssclass="titulo">Mail:</asp:Label>&nbsp;</TD>
																						<TD>
																							<CC1:TEXTBOXTAB id="txtDefaMail" runat="server" cssclass="cuadrotexto" Width="60%" AceptaNull="False"
																								EsMail="True" Panel="Mail"></CC1:TEXTBOXTAB></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblDefaMailRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																						<TD>
																							<CC1:TEXTBOXTAB id="txtDefaMailRefe" runat="server" cssclass="cuadrotexto" Width="60%" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<% else%>
																		<TR>
																			<TD vAlign="middle" align="right">
																				<asp:Label id="lblAgruEstaTit" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																			<TD vAlign="middle">
																				<asp:Label id="lblAgruEsta" runat="server" cssclass="titulo"></asp:Label>&nbsp;<BUTTON style="WIDTH: 90px" id="btnOtrosDatosAgrup" class="boton" onclick="mOtrosDatosAbrir();"
																					runat="server" value="Otros Datos">Otros Datos</BUTTON></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<% end if %>
																		<TR> <!-- este TR va abja del else , cuando vuelva a ser automatica la clave unica-->
																			<TD vAlign="middle" width="15%" align="right">
																				<asp:Label id="lblAgruCunicaTit" runat="server" cssclass="titulo">Clave �nica:</asp:Label>&nbsp;</TD>
																			<TD vAlign="middle">
																				<CC1:TEXTBOXTAB id="txtClaveUnica" onkeypress="gMascaraCunica(this)" runat="server" cssclass="cuadrotexto"
																					Width="161px" maxlength="13" onchange="mClaveUnica();"></CC1:TEXTBOXTAB><!---26/02/07 descomentar esta linea cuando se vuelva a generar automaticamente la clave unica
																			   y borra la anterior   
																				<asp:Label id="lblAgruCunica" runat="server" cssclass="titulo"></asp:Label>&nbsp;&nbsp;</TD>
																		    -----></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" colSpan="2" align="center">
																				<asp:Panel id="panInte" runat="server">
																					<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD height="30" colSpan="2" align="center">
																								<asp:Label id="lblInte" runat="server" cssclass="titulo">Integrantes de la Agrupaci�n</asp:Label></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD colSpan="2">
																								<asp:datagrid id="grdInte" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" Visible="True"
																									AutoGenerateColumns="False" OnEditCommand="mEditarDatosInte" OnPageIndexChanged="grdTele_PageChanged"
																									CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" OnDeleteCommand="mEliminarIntegrante"
																									ShowFooter="True">
																									<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																									<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																									<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																									<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																									<Columns>
																										<asp:TemplateColumn>
																											<HeaderStyle Width="20px"></HeaderStyle>
																											<ItemTemplate>
																												<asp:LinkButton id="Linkbutton7" runat="server" Text="Editar" CausesValidation="false" CommandName="Delete">
																													<img onclick="javascript:return(confirm('Desea dar de baja el integrante seleccionado?'))"
																														src='imagenes/del.gif' border="0" alt="Eliminar Integrante" style="cursor:hand;" />
																												</asp:LinkButton>
																											</ItemTemplate>
																										</asp:TemplateColumn>
																										<asp:TemplateColumn>
																											<HeaderStyle Width="20px"></HeaderStyle>
																											<ItemTemplate>
																											</ItemTemplate>
																										</asp:TemplateColumn>
																										<asp:TemplateColumn>
																											<HeaderStyle Width="15px"></HeaderStyle>
																											<ItemTemplate>
																												<asp:LinkButton id="Linkbutton8" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
																													<img src='images/edit.gif' border="0" alt="Seleccionar Integrante" style="cursor:hand;" />
																												</asp:LinkButton>
																											</ItemTemplate>
																										</asp:TemplateColumn>
																										<asp:BoundColumn Visible="False" DataField="clag_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																										<asp:BoundColumn DataField="_cliente" ReadOnly="True" HeaderText="Apellido / Raz&#243;n Social"></asp:BoundColumn>
																										<asp:BoundColumn DataField="clag_orden" HeaderText="Orden">
																											<HeaderStyle Width="50px"></HeaderStyle>
																										</asp:BoundColumn>
																										<asp:BoundColumn DataField="_estado" HeaderText="Estado">
																											<HeaderStyle Width="60px"></HeaderStyle>
																										</asp:BoundColumn>
																										<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro.Socio">
																											<HeaderStyle Width="60px"></HeaderStyle>
																										</asp:BoundColumn>
																										<asp:BoundColumn DataField="_soci_esta" HeaderText="Estado Socio">
																											<HeaderStyle Width="60px"></HeaderStyle>
																										</asp:BoundColumn>
																										<asp:BoundColumn Visible="False" DataField="clag_clie_id" HeaderText="clag_clie_id"></asp:BoundColumn>
																										<asp:BoundColumn Visible="true" DataField="_resp" HeaderText="Resp."></asp:BoundColumn>
																									</Columns>
																									<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																								</asp:datagrid></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD vAlign="top" width="15%" align="right">
																								<asp:Label id="lblClieInte" runat="server" cssclass="titulo">Cliente:</asp:Label></TD>
																							<TD>
																								<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" FilFanta="true" Ancho="800"></UC1:CLIE>&nbsp;</TD>
																						</TR>
																						<TR>
																							<TD vAlign="top" width="15%" align="right"></TD>
																							<TD>
																								<asp:checkbox id="chkResp" Text="Responsable" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD height="30" colSpan="2" align="center">
																								<asp:Button id="btnAltaInte" runat="server" cssclass="boton" Width="150px" Text="Actualizar Integrante"></asp:Button>&nbsp;
																								<asp:Button id="btnBajaInte" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Eliminar Int."></asp:Button>
																								<asp:Button id="btnModiInte" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Modificar Int."></asp:Button>
																								<asp:Button id="btnLimpInte" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Limpiar Int."></asp:Button></TD>
																						</TR>
																					</TABLE>
																				</asp:Panel></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panDire" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE style="WIDTH: 100%" id="Table4" border="0" cellPadding="0" align="left">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2" align="left">
																				<asp:datagrid id="grdDire" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" Visible="False"
																					AutoGenerateColumns="False" OnEditCommand="mEditarDatosDire" OnPageIndexChanged="grdDire_PageChanged"
																					CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="dicl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_dire_desc" HeaderText="Direcci&#243;n">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="dicl_refe" ReadOnly="True" HeaderText="Referencia">
																							<HeaderStyle Width="35%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_prop" HeaderText="Prop.">
																							<HeaderStyle Width="25%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="_defa" HeaderText="Default" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotexto" Width="440px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblDirePais" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 6px">
																				<cc1:combobox id="cmbDirePais" class="combo" runat="server" Width="260px" onchange="mCargarProvincias('')"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 11px" align="right">
																				<asp:Label id="llbDirePcia" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 11px">
																				<cc1:combobox id="cmbDirePcia" class="combo" runat="server" Width="260px" NomOper="provincias_cargar"
																					onchange="mCargarLocalidades('')"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 19px" align="right">
																				<asp:Label id="lblDireLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 19px">
																				<cc1:combobox id="cmbDireLoca" class="combo" runat="server" Width="260px" NomOper="localidades_cargar"
																					onchange="mSelecCP('');"></cc1:combobox>&nbsp;<BUTTON style="WIDTH: 87px" id="btnLoca" class="boton" onclick="btnLoca_click('');" runat="server"
																					value="Detalles">Localidades</BUTTON></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblDireCP" runat="server" cssclass="titulo">C�digo Postal:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:CODPOSTALBOX id="txtDireCP" runat="server" cssclass="cuadrotexto" Width="77px"></CC1:CODPOSTALBOX></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblDireRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtDireRefe" runat="server" cssclass="cuadrotexto" Width="440px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right"></TD>
																			<TD>
																				<asp:checkbox id="chkDireDefa" Text="Default" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																		</TR>
																		<% if mbooActi then %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2">
																				<asp:Label id="lblTituActiDire" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD height="5" vAlign="bottom" colSpan="2" align="center">
																				<asp:datagrid id="grdActiDire" runat="server" BorderWidth="1px" BorderStyle="None" width="70%"
																					Visible="False" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Center" ShowFooter="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn HeaderText="Asociar">
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:checkbox id="chkActi" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																						<asp:TemplateColumn HeaderText="Prop.">
																							<HeaderStyle Width="70px"></HeaderStyle>
																							<ItemTemplate>
																								<cc1:combobox class="combo" enabled="false" id="cmbActiProp" runat="server" Width="50px" Visible="True">
																									<asp:ListItem Value="N">No</asp:ListItem>
																									<asp:ListItem Value="S">Si</asp:ListItem>
																								</cc1:combobox>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<% end if %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="30" vAlign="middle" colSpan="2" align="center">
																				<asp:Button id="btnAltaDire" runat="server" cssclass="boton" Width="100px" Text="Agregar Direc."></asp:Button>&nbsp;
																				<asp:Button id="btnBajaDire" runat="server" cssclass="boton" Width="100px" Text="Eliminar Direc."></asp:Button>&nbsp;
																				<asp:Button id="btnModiDire" runat="server" cssclass="boton" Width="100px" Text="Modificar Direc."></asp:Button>&nbsp;
																				<asp:Button id="btnLimpDire" runat="server" cssclass="boton" Visible="True" Width="100px" Text="Limpiar Direc."></asp:Button>&nbsp;&nbsp;<BUTTON style="WIDTH: 100px" id="btnSelecDire" class="boton" onclick="btnSelecDire_click();"
																					runat="server" value="Detalles">Selec. Direc.</BUTTON></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panMail" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE style="WIDTH: 100%" id="Table5" border="0" cellPadding="0" align="left">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdMail" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" Visible="False"
																					AutoGenerateColumns="False" OnEditCommand="mEditarDatosMail" OnPageIndexChanged="grdMail_PageChanged"
																					CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="macl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="macl_mail" HeaderText="Mail">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="macl_refe" ReadOnly="True" HeaderText="Referencia">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" HeaderImageUrl="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_prop" HeaderText="Prop.">
																							<HeaderStyle Width="15%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="_defa" HeaderText="Default" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblMail" runat="server" cssclass="titulo">Mail:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtMail" runat="server" cssclass="cuadrotexto" Width="292px" AceptaNull="False"
																					EsMail="True" Panel="Mail"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblMailRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtMailRefe" runat="server" cssclass="cuadrotexto" Width="440px" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right"></TD>
																			<TD>
																				<asp:checkbox id="chkMailDefa" Text="Default" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																		</TR>
																		<% if mbooActi then %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2">
																				<asp:Label id="lblTituActiMail" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD height="5" vAlign="bottom" colSpan="2" align="center">
																				<asp:datagrid id="grdActiMail" runat="server" BorderWidth="1px" BorderStyle="None" width="70%"
																					Visible="False" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Center" ShowFooter="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn HeaderText="Asociar">
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:checkbox id="chkActi" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																						<asp:TemplateColumn HeaderText="Prop.">
																							<HeaderStyle Width="70px"></HeaderStyle>
																							<ItemTemplate>
																								<cc1:combobox class="combo" enabled="false" id="cmbActiProp" runat="server" Width="50px" Visible="True">
																									<asp:ListItem Value="N">No</asp:ListItem>
																									<asp:ListItem Value="S">Si</asp:ListItem>
																								</cc1:combobox>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<% end if %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="30" vAlign="middle" colSpan="2" align="center">
																				<asp:Button id="btnAltaMail" runat="server" cssclass="boton" Width="100px" Text="Agregar Mail"></asp:Button>&nbsp;
																				<asp:Button id="btnBajaMail" runat="server" cssclass="boton" Width="100px" Text="Eliminar Mail"></asp:Button>&nbsp;
																				<asp:Button id="btnModiMail" runat="server" cssclass="boton" Width="100px" Text="Modificar Mail"></asp:Button>&nbsp;
																				<asp:Button id="btnLimpMail" runat="server" cssclass="boton" Width="100px" Text="Limpiar Mail"></asp:Button></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panTele" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdTele" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" Visible="False"
																					AutoGenerateColumns="False" OnEditCommand="mEditarDatosTele" OnPageIndexChanged="grdTele_PageChanged"
																					CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="tecl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_tele_desc" ReadOnly="True" HeaderText="Tel&#233;fono">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="tecl_refe" HeaderText="Referencia">
																							<HeaderStyle Width="50%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_prop" HeaderText="Prop.">
																							<HeaderStyle Width="15%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="_defa" HeaderText="Default" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblTeleNume" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtTeleArea" runat="server" cssclass="cuadrotexto" Width="80px"></CC1:TEXTBOXTAB>&nbsp;
																				<CC1:TEXTBOXTAB id="txtTeleNume" runat="server" cssclass="cuadrotexto" Width="332px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblTeleRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtTeleRefe" runat="server" cssclass="cuadrotexto" Width="450px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblTeleTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 10px">
																				<cc1:combobox id="cmbTeleTipo" class="combo" runat="server" Width="100px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right"></TD>
																			<TD>
																				<asp:checkbox id="chkTeleDefa" Text="Default" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																		</TR>
																		<% if mbooActi then %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2">
																				<asp:Label id="lblTituActiTele" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD height="5" vAlign="bottom" colSpan="2" align="center">
																				<asp:datagrid id="grdActiTele" runat="server" BorderWidth="1px" BorderStyle="None" width="70%"
																					Visible="False" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Center" ShowFooter="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn HeaderText="Asociar">
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:checkbox id="chkActi" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																						<asp:TemplateColumn HeaderText="Prop.">
																							<HeaderStyle Width="70px"></HeaderStyle>
																							<ItemTemplate>
																								<cc1:combobox class="combo" enabled="false" id="cmbActiProp" runat="server" Width="50px" Visible="True">
																									<asp:ListItem Value="N">No</asp:ListItem>
																									<asp:ListItem Value="S">Si</asp:ListItem>
																								</cc1:combobox>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<% end if %>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="30" vAlign="middle" colSpan="2" align="center">
																				<asp:Button id="btnAltaTele" runat="server" cssclass="boton" Width="100px" Text="Agregar Tel."></asp:Button>&nbsp;&nbsp;
																				<asp:Button id="btnBajaTele" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Eliminar Tel."></asp:Button>&nbsp;
																				<asp:Button id="btnModiTele" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Modificar Tel."></asp:Button>&nbsp;
																				<asp:Button id="btnLimpTele" runat="server" cssclass="boton" Width="100px" Text="Limpiar Tel."></asp:Button></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panDocu" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE style="WIDTH: 100%" id="tabDocu" border="0" cellPadding="0" align="left">
																		<% if not mbooAgru then %>
																		<TR>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblFoto" runat="server" cssclass="titulo">Fotograf�a:</asp:Label></TD>
																			<TD>
																				<TABLE id="Table8" border="0" cellSpacing="0" cellPadding="0">
																					<TR>
																						<TD></TD>
																						<TD vAlign="top">
																							<CC1:TEXTBOXTAB id="txtFoto" runat="server" cssclass="cuadrotexto" Width="250px" ReadOnly="True"></CC1:TEXTBOXTAB><BR>
																							<INPUT style="WIDTH: 250px" id="filFoto" type="file" name="filFoto" runat="server"></TD>
																						<TD vAlign="top"><IMG style="CURSOR: hand" id="imgDelFoto" onclick="javascript:mLimpiarFoto();" alt="Limpiar Foto"
																								src="imagenes\del.gif" runat="server">&nbsp;</TD>
																						<TD width="90" align="center">
																							<asp:Image id="imgFoto" Width="80px" Runat="server" Height="80px" Border="1"></asp:Image></TD>
																						<TD vAlign="top"><BUTTON style="WIDTH: 80px" id="btnFotoVer" class="boton" runat="server" value="Ver Foto">Ver 
																								Foto</BUTTON></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="10" vAlign="top" align="right"></TD>
																			<TD height="10"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblFirma" runat="server" cssclass="titulo">Firma:</asp:Label></TD>
																			<TD>
																				<TABLE id="Table9" border="0" cellSpacing="0" cellPadding="0">
																					<TR>
																						<TD vAlign="top">
																							<CC1:TEXTBOXTAB id="txtFirma" runat="server" cssclass="cuadrotexto" Width="252px" ReadOnly="True"></CC1:TEXTBOXTAB><BR>
																							<INPUT style="WIDTH: 250px" id="filFirma" type="file" name="filFirma" runat="server"></TD>
																						<TD vAlign="top"><IMG style="CURSOR: hand" id="imgDelFirm" onclick="javascript:mLimpiarFirma();" alt="Limpiar Firma"
																								src="imagenes\del.gif" runat="server">&nbsp;
																						</TD>
																						<TD width="90" align="center">
																							<asp:Image id="imgFirma" Width="80px" Runat="server" Height="80px" Border="1"></asp:Image></TD>
																						<TD vAlign="top"><BUTTON style="WIDTH: 80px" id="btnFirmaVer" class="boton" runat="server" value="Ver Firma">Ver 
																								Firma</BUTTON></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<% end if%>
																		<TR>
																			<TD height="10" vAlign="top" align="right"></TD>
																			<TD height="10"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label></TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="460px" MaxLength="20"
																					Height="50px" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panIntegr" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE style="WIDTH: 100%" id="tblIntegr" border="0" cellPadding="0" align="left">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdIntegr" runat="server" BorderWidth="1px" BorderStyle="None" width="100%"
																					Visible="True" AutoGenerateColumns="False" OnEditCommand="mEditarDatosIntegr" OnPageIndexChanged="grdIntegr_PageChanged"
																					CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="clag_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_cliente" ReadOnly="True" HeaderText="Codeudor">
																							<HeaderStyle Width="100%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro.Socio">
																							<HeaderStyle Width="60px"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="_soci_esta" HeaderText="Estado Socio">
																							<HeaderStyle Width="90px"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="_resp" HeaderText="Resp.">
																							<HeaderStyle Width="90px"></HeaderStyle>
																						</asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" width="15%" align="right">
																				<asp:Label id="lblIntegr" runat="server" cssclass="titulo">Codeudor:</asp:Label>&nbsp;</TD>
																			<TD>
																				<UC1:CLIE id="usrIntegr" runat="server" AceptaNull="false" Ancho="800" Tabla="Clientes" Saltos="1,2"
																					FilLegaNume="True" FilTipo="T" MuestraDesc="True" FilDocuNume="True" SoloBusq="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" width="15%" align="right"></TD>
																			<TD>
																				<asp:checkbox id="chkInteResp" Text="Responsable" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="center">
																				<asp:Label id="lblBajaIntegr" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																		</TR>
																		<TR>
																			<TD height="30" vAlign="middle" colSpan="2" align="center">
																				<asp:Button id="btnAltaIntegr" runat="server" cssclass="boton" Width="100px" Text="Agregar cod."></asp:Button>&nbsp;&nbsp;
																				<asp:Button id="btnBajaIntegr" runat="server" cssclass="boton" Width="100px" Text="Eliminar Cod."></asp:Button>&nbsp;
																				<asp:Button id="btnModiIntegr" runat="server" cssclass="boton" Width="100px" Text="Modificar cod."></asp:Button>&nbsp;
																				<asp:Button id="btnLimpIntegr" runat="server" cssclass="boton" Width="100px" Text="Limpiar Cod."></asp:Button></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
													</TABLE>
												</asp:panel><ASP:PANEL id="panBotones" Runat="server">
													<TABLE width="100%">
														<TR>
															<TD align="center">
																<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
														</TR>
														<TR height="30">
															<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
																<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																	Text="Baja"></asp:Button>&nbsp;&nbsp;
																<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																	Text="Modificar"></asp:Button>&nbsp;&nbsp;
																<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																	Text="Limpiar"></asp:Button></TD>
														</TR>
													</TABLE>
												</ASP:PANEL></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnAltaId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnEsSocio" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnValorId" runat="server"></asp:textbox>
				<asp:textbox id="hdnTeleId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDireId" runat="server"></asp:textbox>
				<asp:textbox id="hdnMailId" runat="server"></asp:textbox>
				<asp:textbox id="hdnAgru" runat="server"></asp:textbox>
				<asp:textbox id="hdnIntegrId" runat="server"></asp:textbox>
				<asp:textbox id="hdnClagId" runat="server"></asp:textbox>
				<asp:textbox id="hdnOrdenAnte" runat="server"></asp:textbox>
				<asp:textbox id="hdnOrdenProx" runat="server"></asp:textbox>
				<asp:textbox id="hdnModi" runat="server"></asp:textbox>
				<asp:textbox id="hdnModiMain" runat="server"></asp:textbox>
				<asp:textbox id="hdnModiTeleAdic" runat="server"></asp:textbox>
				<asp:textbox id="hdnModiMailAdic" runat="server"></asp:textbox>
				<asp:textbox id="hdnModiDireAdic" runat="server"></asp:textbox>
				<asp:textbox id="hdnAltaDireAdic" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnClieIds" runat="server"></asp:textbox>
				<asp:textbox id="hdnTipoId" runat="server"></asp:textbox>
				<asp:textbox id="hdnLocaPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
				<cc1:combobox id="cmbLocaAux" class="combo" runat="server" AceptaNull="false" NomOper="localidades_aux_cargar"></cc1:combobox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		if (document.all["usrClieFil_txtCodi"]!= null && !document.all["usrClieFil_txtCodi"].disabled)
			document.all["usrClieFil_txtCodi"].focus();		
		
		if (document.all["txtClieNumeFil"]!= null)
			document.all["txtClieNumeFil"].focus();
			
		if (document.all("cmbTipo")!=null && (document.all("cmbTipo").options[document.all("cmbTipo").selectedIndex].value==2 || document.all("cmbTipo").options[document.all("cmbTipo").selectedIndex].value==3))
			 mHabilitarControles(document.all("cmbTipo"))
		
		if (document.all("lnkIntegr")!=null)	 
			document.all("lnkIntegr").disabled = (document.all("hdnTipoId").value != "2");
			
		<% If mbooAgru Then %>
			gSetearTituloFrame('Agrupaciones');
		<% Else %>
			<% If mbooActi Then %>
				gSetearTituloFrame('Clientes - Datos por Actividad');
			<% Else %>
				gSetearTituloFrame('Clientes');
			<% End If %>
		<% End If %>
		function mDescargar()
	    {
			if ('<%=mstrClieCtrl%>'!='')
			{
				if(window.opener.mRefresh!=undefined)
					window.opener.mRefresh();
			}				
		}
		if (document.all["hdnAltaId"].value!='')
		{
			alert(document.all["hdnAltaId"].value);
			document.all["hdnAltaId"].value = '';
		}
		if (document.all["txtNomb"]!=null)
		{
			if (document.all["hdnEsSocio"].value == 'S')
			{
				//document.all('txtNomb').value='';
				//document.all('txtApel').value='';
				document.all["txtNomb"].disabled = true;
				document.all["txtApel"].disabled = true;
				document.all["txtFant"].disabled = true;
				document.all["txtCUIT"].disabled = true;
				document.all["cmbIVA"].disabled = true;
			}
		}
		if (document.all('cmbTipo')!=null)
			mHabilitarControles(document.all('cmbTipo'));
		if (document.all('txtFoto')!=null)
			mSetearImagenes();
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
