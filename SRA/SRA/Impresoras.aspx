<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Impresoras" CodeFile="Impresoras.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Impresoras</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('')" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Impresoras</asp:label></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="95%" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
										OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" BorderWidth="1px"
										BorderStyle="None">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="impr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="impr_desc" HeaderText="Nombre">
												<HeaderStyle Width="40%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="impr_dire" HeaderText="Direcci�n">
												<HeaderStyle Width="50%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left">
												<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ToolTip="Agregar un Nuevo Distrito"
													ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/"
													ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
													ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif"></CC1:BotonImagen></TD>
											<TD></TD>
											<TD align="center" width="50">
												<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" Visible="False"></CC1:BotonImagen></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderWidth="1px" BorderStyle="Solid"
											Height="116px" Visible="False">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" align="center" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 99px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblNomb" runat="server" cssclass="titulo" Width="62px">Nombre:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="textolibre" Width="360px" Obligatorio="True"
																				AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 99px; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<P>
																				<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="textolibre" Width="360px" Obligatorio="True"
																					AceptaNull="False"></CC1:TEXTBOXTAB></P>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel>
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR height="30">
														<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();

			function VigeChange()
			{
				ActivarFecha("txtInicFecha", document.all("cmbVige").value== "1");
				ActivarFecha("txtFinaFecha", document.all("cmbVige").value == "1");
				if(document.all("cmbVige").value!="1")
				{
					document.all("txtInicFecha").value="";
					document.all("txtFinaFecha").value="";
				}
			}
			
			function cmbConc_Change()
			{
				var lstrRet = LeerCamposXML("conceptos", document.all("cmbConc").value, "conc_form_id");
				document.all("btnPosicDet").disabled = !(lstrRet=="10"||lstrRet=="11"||lstrRet=="12"||lstrRet=="85"||lstrRet=="300"||lstrRet=="400"||lstrRet=="500"||lstrRet=="600"||lstrRet=="700"||lstrRet=="1000");
				/*
				if(lstrRet=="10")
					document.all("btnPosicDet").value = "Posicionamiento";
				if(lstrRet=="11"||lstrRet=="12")
					document.all("btnPosicDet").value = "Recorridos";
				*/
			}
		</SCRIPT>
	</BODY>
</HTML>
