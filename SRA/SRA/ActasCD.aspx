<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ActasCD" CodeFile="ActasCD.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Actas CD</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<SCRIPT language="javascript">
		function cmbEsta_change(pObj)
		{
			var iRowActual = pObj.parentElement.parentElement.rowIndex + 2;
			var lstrEsta=pObj.value;
			var lstrSoliIngre = document.all("grdSolicitudes__ctl" + iRowActual.toString() + "_divSoliTipo").innerText;
			
			if(lstrEsta == "27" && lstrSoliIngre != "")
			{
				TraerDiv(iRowActual).style.display="inline";
				
				if (TraerNroSoli(iRowActual).innerText == "")				
				 {
				  TraerTxt(iRowActual).value = ObtenerUltNum();
				 
				 } else
				 {
				  
				  TraerTxt(iRowActual).value = TraerNroSoli(iRowActual).innerText;
				 }
						
			}
			else
			{
				TraerDiv(iRowActual).style.display="none";
				TraerTxt(iRowActual).value = "";
				TraerNroSoli(iRowActual).value = "";
			}
			
		}
		
		function TraerDiv(piRow)
		{
			return(document.all("grdSolicitudes__ctl" + piRow.toString() + "_divSociNume"));
		}
		
		function TraerTxt(piRow)
		{
			return(document.all("grdSolicitudes__ctl" + piRow.toString() + "_txtSociNume"));
		}
		
		function TraerNroSoli(piRow)
		{
			return(document.all("grdSolicitudes__ctl" + piRow.toString() + "_divAnteSoci"));
		}
		
		
		function ObtenerUltNum()
		{
			var i=0;
			var total=0;
			var MaxNum=0;
			var Num = 0;
			
			if(MaxNum==0)
				MaxNum = parseInt(document.all("hdnUltSociNume").value);
			
			total = parseInt(document.all("hdnTotalSolis").value) + 3;
			for(i=3;i<total;i++)
			{
				if (TraerTxt(i).value=="")
				{
						document.all("hdnUltSociNume").value = MaxNum + 1;
				}
			}
			
			return(MaxNum+1);
		}
		
		/*function Imprimir(sReporte, sMessage)
		{
			if((document.all("hdnId").value != "") && (document.all("txtNume").value != ""))
			{
				if(window.confirm("�Desea imprimir la Planilla de " + sMessage + "?"))
				{
						var nomParam = "accd_id;accd_nume";
						var valParam = document.all("hdnId").value + ";" + document.all("txtNume").value;
									
						var sRet = ImprimirReporte(sReporte, nomParam, valParam, "<%=Session("sImpreTipo")%>", 0,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
				}
			}
		}*/
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Generaci�n Acta CD</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="1"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="10"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" width="68px" cssclass="cuadrotexto"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblNumeFil" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<CC1:NUMBERBOX id="txtNumeFil" runat="server" cssclass="cuadrotexto" Width="270" AceptaNull="False"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="accd_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="accd_nume" HeaderText="Nro"></asp:BoundColumn>
											<asp:BoundColumn DataField="accd_fecha" HeaderText="Fecha Pres." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cant_soli" HeaderText="Cant.Sol."></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="middle" width="100%" colSpan="3"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
										BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ForeColor="Transparent"
										ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Acta" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
											Visible="False">
											<TABLE width="100%">
												<TR>
													<TD>
														<P align="right">
															<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD>
																		<P></P>
																	</TD>
																	<TD height="5">
																		<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
																	<TD vAlign="top" align="right">&nbsp;
																		<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 100%" colSpan="3">
																		<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																			<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																				<TR>
																					<TD align="right" width="20%">
																						<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro:</asp:Label>&nbsp;</TD>
																					<TD>
																						<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
																							<TR>
																								<TD align="left">
																									<cc1:numberbox id="txtNume" runat="server" cssclass="cuadrotexto" Width="60px" Obligatorio="true"></cc1:numberbox></TD>
																								<TD align="right" width="100">
																									<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																								<TD>
																									<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																								<TD>
																									<cc1:DateBox id="txtFechaCierre" visible="false" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																								<TD align="right">
																									<asp:Button id="btnTraer" runat="server" cssclass="boton" Width="140px" Text="Traer Solicitudes"></asp:Button></TD>
																							</TR>
																						</TABLE>
																					</TD>
																				</TR>
																				<TR>
																					<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" align="center" colSpan="2">
																						<asp:datagrid id="grdSolicitudes" runat="server" width="100%" BorderWidth="1px" BorderStyle="None"
																							AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																							OnPageIndexChanged="DataGridSoli_Page" AutoGenerateColumns="False" PageSize="1000">
																							<FooterStyle CssClass="footer"></FooterStyle>
																							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																							<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																							<Columns>
																								<asp:BoundColumn Visible="False" DataField="sacd_id"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="sacd_soin_id"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="sacd_socm_id"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_tipo" HeaderText="Tipo"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_clie_desc" HeaderText="Socio/Cliente"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_datos" HeaderText="Otros Datos"></asp:BoundColumn>
																								<asp:TemplateColumn HeaderStyle-Width="2%" HeaderText="Incl.">
																									<ItemTemplate>
																										<asp:CheckBox ID="chkSel" Checked=<%#DataBinder.Eval(Container, "DataItem._chk")%> Runat="server">
																										</asp:CheckBox>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn HeaderText="Estado">
																									<HeaderStyle Width="3%"></HeaderStyle>
																									<ItemTemplate>
																										<cc1:combobox class="combo" id="cmbEsta" onchange="cmbEsta_change(this);" runat="server" Width="100px"></cc1:combobox>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn HeaderText="Nro">
																									<HeaderStyle Width="3%"></HeaderStyle>
																									<ItemTemplate>
																										<DIV runat="server" id="divSociNume" style="DISPLAY: none">
																											<CC1:NUMBERBOX id="txtSociNume" runat="server" cssclass="cuadrotexto" Width="50"></CC1:NUMBERBOX></DIV>
																										<DIV runat="server" id="divAnteSoci" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem._soin_ante_soci")%></DIV>
																										<DIV runat="server" id="divSoliTipo" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.sacd_soin_id")%></DIV>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:BoundColumn Visible="False" DataField="_importe"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="_paga"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="_chk"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="_deuda"></asp:BoundColumn>
																								<asp:BoundColumn Visible="False" DataField="_tipo_soli"></asp:BoundColumn>
																							</Columns>
																							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD>
																				</TR>
																			</TABLE>
																		</asp:panel></TD>
																</TR>
															</TABLE>
														</P>
													</TD>
												</TR>
											</TABLE>
										</asp:panel>
										<ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnCerrar" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Cerrar"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnList" runat="server" cssclass="boton" Width="150px" CausesValidation="False"
															Text="Imprimir Planilla" Visible="False"></asp:Button>
													</TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										<ASP:PANEL id="panBotonesImp" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblImprimir" runat="server" cssclass="titulo" ForeColor="Black">Impresi�n de Planilla</asp:Label>
													</TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editarImp" name="editar"></A>
														<asp:Button id="btnSolIngPren" runat="server" cssclass="boton" Width="150px" CausesValidation="False"
															Text="Socios a Presentarse"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnSolIngRein" runat="server" cssclass="boton" Width="150px" CausesValidation="False"
															Text="Socios Reincorporados"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnSolIngGral" runat="server" cssclass="boton" Width="150px" CausesValidation="False"
															Text="Socios a Votar"></asp:Button>
													</TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editarImp2" name="editar"></A>
														<asp:Button id="btnCambEstado" runat="server" cssclass="boton" Width="150px" CausesValidation="False"
															Text="Socios Renunciantes"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnCambCatego" runat="server" cssclass="boton" Width="150px" CausesValidation="False"
															Text="Cambios de Categor�a"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnSolIngApro" runat="server" cssclass="boton" Width="150px" CausesValidation="False"
															Text="Socios Aprobados"></asp:Button>
													</TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnUltSociNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnTotalSolis" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="hdnEdit" runat="server"></ASP:TEXTBOX>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT>
				</OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtNume"]!= null&&!document.all["txtNume"].disabled)
			document.all["txtNume"].focus();
		<% If mbooRegis Then %>
			gSetearTituloFrame('Registraci�n de Decisiones'); 
		<% Else %>
			gSetearTituloFrame('Generaci�n Acta CD'); 
		<% End If %>
		</SCRIPT>
	</BODY>
</HTML>
