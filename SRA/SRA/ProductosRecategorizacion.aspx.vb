Imports System.Data.SqlClient


Namespace SRA


Partial Class ProductosRecategorizacion
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	'Controles filtros

	'Controles ABM
	Protected WithEvents txtlblRegtAnte As System.Web.UI.WebControls.Label

	'Controles Varios
	Protected WithEvents btnClose As System.Web.UI.WebControls.ImageButton
	'Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
	'Protected WithEvents btnModi As System.Web.UI.WebControls.Button
	'Protected WithEvents lblBajaTit As System.Web.UI.WebControls.Label
	'Protected WithEvents lblbajaAnterior As System.Web.UI.WebControls.Label
	'Protected WithEvents lblBaja As System.Web.UI.WebControls.Label
	


	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

#Region "Definición de Variables"
	Private mstrTabla As String = "rg_recategorizados"
	Private mstrCmd As String
	Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()
			If (Not Page.IsPostBack) Then
				Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
				mSetearMaxLength()
				mSetearEventos()
				mCargarCombos()
				'mConsultar()
				mMostrarPanel(False)
				clsWeb.gInicializarControles(Me, mstrConn)
			Else
				'usrProductoFil.cmbProdRazaExt.Valor = usrProductoFil.cmbProdRazaExt.Valor
				If usrProducto.Valor.ToString <> "0" Then
					clsWeb.gCargarCombo(mstrConn, "rg_registro_producto_consul @prdt_id=" & usrProducto.Valor.ToString, cmbRegtAnte, "id", "descrip", "")
					If cmbRegtActu.SelectedValue.ToString = "" Then
						clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id=" & usrProducto.cmbProdRazaExt.Valor.ToString, cmbRegtActu, "id", "descrip", "S")
					End If
				End If
			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mSetearEventos()
		btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
	End Sub

	Private Sub mSetearMaxLength()
		'Dim lstrLongs As Object

		'lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
		'txtObservaciones.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "mopr_obse")

	End Sub
	Public Sub mInicializar()
		usrProductoFil.AutoPostback = False
		usrProductoFil.Ancho = 790
		usrProductoFil.Alto = 510

		usrProducto.AutoPostback = False
		usrProducto.IncluirDeshabilitados = True
		usrProducto.Ancho = 790
		usrProducto.Alto = 510

		cmbRegtAnte.Enabled = False

		
	End Sub
	Private Sub mCargarCombos()
		clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar", cmbRegtActuFil, "id", "descrip", "T")
		clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar", cmbRegtActu, "id", "descrip", "S")
		clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar", cmbRegtAnteFil, "id", "descrip", "T")
		'clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar", cmbRegtAnte, "id", "descrip", "")
	End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
	Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdDato.EditItemIndex = -1
			If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
				grdDato.CurrentPageIndex = 0
			Else
				grdDato.CurrentPageIndex = E.NewPageIndex
			End If
			mConsultar()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Public Sub mConsultar()
		Try
			mstrCmd = "exec " + mstrTabla + "_consul "

			mstrCmd = mstrCmd + " @reca_fecha_desde=" + clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
			mstrCmd = mstrCmd + ",@reca_fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
			mstrCmd = mstrCmd + ",@reca_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
			mstrCmd = mstrCmd + ",@reca_ante_regt_id=" + IIf(cmbRegtAnteFil.Valor.ToString = "", "0", cmbRegtAnteFil.Valor.ToString)
			mstrCmd = mstrCmd + ",@reca_actu_regt_id=" + IIf(cmbRegtActuFil.Valor.ToString = "", "0", cmbRegtActuFil.Valor.ToString)

			clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
			mMostrarPanel(False)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

#Region "Seteo de Controles"
	Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
		btnBaja.Enabled = Not (pbooAlta)
		btnModi.Enabled = Not (pbooAlta)
		btnAlta.Enabled = pbooAlta
	End Sub
	Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
		Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, E.Item.Cells(1).Text)
		ldsEsta.Tables(0).TableName = mstrTabla
		mSetearEditor(False)
		With ldsEsta.Tables(0).Rows(0)

			hdnId.Text = .Item("reca_id")
			txtFecha.Fecha = CDate(.Item("reca_fecha")).ToString("dd/MM/yyyy")
			usrProducto.Valor = .Item("reca_prdt_id")
			cmbRegtActu.Valor = .Item("reca_actu_regt_id")
			cmbRegtAnte.Valor = .Item("reca_ante_regt_id")
		End With

		mMostrarPanel(True)
	End Sub

	Private Sub mAgregar()
		mLimpiar()
		'usrProducto.Valor = usrProductoFil.Valor
		
		txtFecha.Fecha = CDate(Now())
		btnAlta.Enabled = True
		mMostrarPanel(True)
	End Sub
	Private Sub mLimpiar()
		hdnId.Text = ""
		txtFecha.Fecha = CDate(Now())
		usrProducto.Limpiar()
		cmbRegtAnte.Limpiar()
		cmbRegtActu.Limpiar()
		
		grdDato.CurrentPageIndex = 0
		mSetearEditor(True)
	End Sub
	Private Sub mLimpiarFiltro()
		usrProductoFil.Limpiar()
		cmbRegtAnteFil.Limpiar()
		cmbRegtActuFil.Limpiar()
		txtFechaHastaFil.Text = ""
		grdDato.CurrentPageIndex = 0
		mConsultar()
	End Sub
	Private Sub mCerrar()
		mMostrarPanel(False)
	End Sub
	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		If (pbooVisi) Then
			hdnPage.Text = " "
		Else
			hdnPage.Text = ""
		End If
		panDato.Visible = pbooVisi
		panBotones.Visible = pbooVisi
		btnAgre.Enabled = Not (panDato.Visible)

	End Sub
#End Region

#Region "Opciones de ABM"
	Private Sub mAlta()
		Dim lTransac As SqlClient.SqlTransaction
		Dim myConnection As New SqlConnection(mstrConn)

		Try
			Dim lstrCmd As String
			Dim lstrID As String

			myConnection.Open()
			lTransac = myConnection.BeginTransaction()

			Dim ldsDatos As DataSet = mGuardarDatos()

			lstrCmd = mObtenerParamABM(mstrTabla & "_alta", ldsDatos.Tables(0).Rows(0))
			lstrID = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, lstrCmd)

			lTransac.Commit()
			myConnection.Close()

			mConsultar()
			mMostrarPanel(False)

		Catch ex As Exception
			lTransac.Rollback()
			myConnection.Close()
			clsError.gManejarError(Me, ex)
		End Try
	End Sub


	
	'Private Sub mModi()
	'	Dim lTransac As SqlClient.SqlTransaction
	'	Dim myConnection As New SqlConnection(mstrConn)

	'	Try
	'		Dim lstrCmd As String
	'		Dim lstrID As String

	'		myConnection.Open()
	'		lTransac = myConnection.BeginTransaction()

	'		Dim ldsDatos As DataSet = mGuardarDatos()

	'		lstrCmd = mObtenerParamABM(mstrTabla & "_modi", ldsDatos.Tables(0).Rows(0))
	'		lstrID = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, lstrCmd)

	'		lTransac.Commit()
	'		myConnection.Close()

	'		mConsultar()
	'		mMostrarPanel(False)

	'	Catch ex As Exception
	'		lTransac.Rollback()
	'		myConnection.Close()
	'		clsError.gManejarError(Me, ex)
	'	End Try
	'End Sub
	'Private Sub mBaja()
	'	Dim lTransac As SqlClient.SqlTransaction
	'	Dim myConnection As New SqlConnection(mstrConn)

	'	Try
	'		Dim lstrCmd As String
	'		Dim lstrID As String

	'		myConnection.Open()
	'		lTransac = myConnection.BeginTransaction()

	'		Dim lintPage As Integer = grdDato.CurrentPageIndex

	'		'Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
	'		'lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

	'		grdDato.CurrentPageIndex = 0

	'		mConsultar()

	'		If (lintPage < grdDato.PageCount) Then
	'			grdDato.CurrentPageIndex = lintPage
	'		End If

	'		lTransac.Commit()
	'		myConnection.Close()

	'		mConsultar()
	'		mMostrarPanel(False)

	'	Catch ex As Exception
	'		lTransac.Rollback()
	'		myConnection.Close()
	'		clsError.gManejarError(Me, ex)
	'	End Try
	'End Sub
	
	Private Sub mValidarDatos()

		clsWeb.gInicializarControles(Me, mstrConn)
		clsWeb.gValidarControles(Me)

		If usrProducto.ID = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar el Producto a recategorizar.")
		End If
		If cmbRegtActu.SelectedValue.ToString() = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar el registro al cual se va a recategorizar el Producto.")
		End If
		If cmbHijosReca.SelectedValue.ToString() = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar si recategoriza los Hijos del Producto.")
		End If
		If txtFecha.Text = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar Fecha de recategorización.")
		End If

	End Sub

	Private Function mObtenerParamABM(ByVal pstrProcedure As String, ByVal pOdr As DataRow) As String
		Dim lstrCmd As String

		lstrCmd = "exec " + pstrProcedure

		With pOdr
			lstrCmd += " @reca_id=" & clsSQLServer.gFormatArg(.Item("reca_id").ToString, SqlDbType.Int)
			lstrCmd += ",@reca_fecha=" & clsSQLServer.gFormatArg(.Item("reca_fecha").ToString, SqlDbType.SmallDateTime)
			lstrCmd += ",@reca_prdt_id=" & clsSQLServer.gFormatArg(.Item("reca_prdt_id").ToString, SqlDbType.Int)
			lstrCmd += ",@reca_actu_regt_id=" & clsSQLServer.gFormatArg(.Item("reca_actu_regt_id").ToString, SqlDbType.Int)
			lstrCmd += ",@reca_hijos=" & IIf(.Item("reca_hijos").ToString = "False", "0", "1")
			lstrCmd += ",@reca_audi_user=" & Session("sUserId").ToString()
		End With

		Return (lstrCmd)
	End Function

	Private Function mGuardarDatos() As DataSet
		Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
		Dim lintCpo As Integer
		Dim ldsEsta As DataSet

		mValidarDatos()

		With ldsDatos.Tables(0).Rows(0)
			.Item("reca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
			.Item("reca_prdt_id") = usrProducto.ProdId
			.Item("reca_actu_regt_id") = cmbRegtActu.Valor
			.Item("reca_fecha") = txtFecha.Fecha
			.Item("reca_hijos") = IIf(cmbHijosReca.Valor.ToString = "0", False, True)
		End With
		Return ldsDatos
	End Function
#End Region

#Region "Eventos de Controles"
	
	Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
		mAgregar()
	End Sub
	Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
		grdDato.CurrentPageIndex = 0
		mConsultar()
	End Sub
	Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltro()
	End Sub
	Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
		mAlta()
	End Sub
	
	
	Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
		mLimpiar()
	End Sub
#End Region

	Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
		Try
			Dim lstrRptName As String
			Dim lintFechaD As Integer
			Dim lintFechaH As Integer

			If txtFechaDesdeFil.Fecha.ToString = "" Then
				lintFechaD = 0
			Else
				lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
			End If
			If txtFechaHastaFil.Fecha.ToString = "" Then
				lintFechaH = 0
			Else
				lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
			End If

			lstrRptName = "Recategorizacion"

			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			lstrRpt = lstrRpt + "&reca_fecha_desde=" + lintFechaD.ToString
			lstrRpt = lstrRpt + "&reca_fecha_hasta=" + lintFechaH.ToString
			lstrRpt = lstrRpt + "&reca_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
			lstrRpt = lstrRpt + "&reca_ante_regt_id=" + IIf(cmbRegtAnteFil.Valor.ToString = "", "0", cmbRegtAnteFil.Valor.ToString)
			lstrRpt = lstrRpt + "&reca_actu_regt_id=" + IIf(cmbRegtActuFil.Valor.ToString = "", "0", cmbRegtActuFil.Valor.ToString)

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub imgCerrar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCerrar.Click
		mCerrar()
	End Sub

End Class
End Namespace
