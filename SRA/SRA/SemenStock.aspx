<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SemenStock" CodeFile="SemenStock.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Recuperación de Semen</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
		function usrCria_onchange()
		{		
			var lstrCriaId = document.all('usrCria:txtId').value;
			var lstrClieId = LeerCamposXML("criadores", lstrCriaId, "cria_clie_id");
			if (lstrClieId != "")
			{
				document.all('usrClie:txtCodi').value = lstrClieId;
				document.all('usrClie:txtCodi').onchange();
			}
		}	
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function usrCria_cmbRazaCria_onchange()
		{
			if (document.all('usrProd_cmbProdRaza')!=null)
			{
				if (document.all('usrProd_cmbProdRaza').selectedIndex != document.all('usrCria_cmbRazaCria').selectedIndex)
				{
					imgLimpProdDeriv_click('usrProd');
					document.all('usrProd_cmbProdRaza').selectedIndex = document.all('usrCria_cmbRazaCria').selectedIndex;
					document.all('usrProd_cmbProdRaza').onchange();
				}
				if (document.all('usrCria_cmbRazaCria').value != '')
				{
					document.all('usrProd_cmbProdRaza').disabled = true;
					document.all('txtusrProd:cmbProdRaza').disabled = true;	
				}
				else
				{
					document.all('usrProd_cmbProdRaza').disabled = false;
					document.all('txtusrProd:cmbProdRaza').disabled = false;	
				}
			}
		}
		function usrCriaId_onchange()
		{
			//document.all('usrProd:txtCriaId').value = document.all('usrCria:txtId').value;
		}
		
		function mPorcPeti()
		{
			document.all("divgraba").style.display ="none";
			document.all("divproce").style.display ="inline";
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('')" class="pagina" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Recuperación de Semen</asp:label></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
												Visible="True" Width="100%">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 21.41%; HEIGHT: 17px" vAlign="top" background="imagenes/formfdofields.jpg"
																					align="right">
																					<asp:Label id="lblProductoFil" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" vAlign="top" background="imagenes/formfdofields.jpg">
																					<UC1:PROH style="Z-INDEX: 0" id="usrProdFil" runat="server" EsPropietario="True" MuestraBotonAgregaImportado="False"
																						FilTipo="S" Ancho="800" Saltos="1,2" Tabla="productos" MuestraDesc="True" AceptaNull="false"
																						AutoPostBack="False"></UC1:PROH></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="2" align="center"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="sest_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="sest_pres_fecha" ReadOnly="True" HeaderText="F.Present." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
													<asp:BoundColumn DataField="sest_recu_fecha" ReadOnly="True" HeaderText="F.Recup." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
													<asp:BoundColumn DataField="_Criador" ReadOnly="True" HeaderText="Propietario">
														<HeaderStyle></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_Producto" ReadOnly="True" HeaderText="Producto">
														<HeaderStyle></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="sest_cant" ReadOnly="True" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"><A id="editar" name="editar"></A></TD>
									</TR>
									<TR id="trbtnAgre" runat="server">
										<TD></TD>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
												ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
												IncludesUrl="includes/" BackColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Nuevo Registro"></CC1:BOTONIMAGEN></TD>
										<TD align="right"></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="2" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												Visible="False" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD vAlign="top" colSpan="2" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 14.63%; HEIGHT: 16px" noWrap align="right">
															<asp:Label id="lblPresFecha" runat="server" cssclass="titulo">Fecha Presentación:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<cc1:DateBox id="txtPresFecha" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																obligatorio="true"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 14.63%; HEIGHT: 16px" noWrap align="right">&nbsp;&nbsp;&nbsp;&nbsp;
															<asp:Label id="lblRecuFecha" runat="server" cssclass="titulo">Fecha Recuperación:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<cc1:DateBox id="txtRecuFecha" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																obligatorio="true"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 14.63%; HEIGHT: 21px" align="right">
															<asp:Label id="lblProducto" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 21px" height="21">
															<UC1:PROH style="Z-INDEX: 0" id="usrProdu" runat="server" EsPropietario="True" MuestraBotonAgregaImportado="False"
																FilTipo="S" Ancho="800" Saltos="1,2" Tabla="productos" MuestraDesc="True" AceptaNull="false"
																AutoPostBack="False"></UC1:PROH></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 14.63%; HEIGHT: 16px" vAlign="top" align="right">
															<asp:Label id="lblCant" runat="server" cssclass="titulo">Cantidad Dosis:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" height="16">
															<cc1:numberbox id="txtCant" runat="server" cssclass="cuadrotexto" Width="75px" obligatorio="true"
																maxvalor="999999999"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 14.63%; HEIGHT: 16px" vAlign="top" align="right"></TD>
														<TD style="HEIGHT: 16px" height="16"></TD>
													</TR>
												</TABLE>
											</asp:panel>
											<DIV style="DISPLAY: inline" id="divgraba" runat="server"><ASP:PANEL id="panBotones" Runat="server">
													<TABLE width="100%">
														<TR>
															<TD align="center">
																<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
														</TR>
														<TR height="30">
															<TD align="center">
																<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																	Text="Baja"></asp:Button>&nbsp;
																<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																	Text="Modificar"></asp:Button>&nbsp;
																<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																	Text="Limpiar"></asp:Button></TD>
														</TR>
													</TABLE>
												</ASP:PANEL></DIV>
											<DIV style="DISPLAY: none" id="divproce" runat="server"><asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            </asp:panel>
											</DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDenunciaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		gSetearTituloFrame('Recuperación de Semen');
		usrCria_cmbRazaCria_onchange();
	    if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
