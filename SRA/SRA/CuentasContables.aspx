<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CuentasContables" CodeFile="CuentasContables.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cuentas Contables</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function mSetearCuenta(pCuenta, pCost, pDest, pEdit)
		{
			var lstrCta = '';
			if (document.all(pCuenta).value!='')
				lstrCta = document.all(pCuenta).value.substring(0,1);
			document.all('hdnCta'+pDest).value = lstrCta;
			if (lstrCta=='' || lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
			{
				document.all(pCost).disabled = true;
				document.all('txt'+pCost).disabled = true;
				document.all(pCost).innerHTML = '';
				document.all('txt'+pCost).value = '';
				if (pCuenta == 'txtcmbDeudIncob' && pEdit != '')
				{
					document.all('cmbSociIncoCcos').disabled = true;
					document.all('txtcmbSociIncoCcos').disabled = true;
					document.all('cmbSociIncoCcos').innerHTML = '';
					document.all('txtcmbSociIncoCcos').value = '';
				}
			}
			else
			{
				document.all(pCost).disabled = false;
				document.all('txt'+pCost).disabled = false;
				LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, pCost, "N");
				if (pCuenta == 'txtcmbDeudIncob' && pEdit != '')
				{
					document.all('cmbSociIncoCcos').disabled = false;
					document.all('txtcmbSociIncoCcos').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, 'cmbSociIncoCcos', "N");				
				}
			}			
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="javascript:gSetearTituloFrame();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="TableABM" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
							<TR>
								<TD style="HEIGHT: 25px"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="4" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Cuentas Contables</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="5"></TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD colSpan="5">
									<DIV align="left">
										<asp:panel id="panDato" runat="server" cssclass="titulo" Visible="true" Width="100%" BorderStyle="Solid"
											BorderWidth="1px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left">
													<TR>
														<TD colSpan="3"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblDescuento" runat="server" cssclass="titulo">Descuento Cuota Social:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbDescuento" runat="server" cssclass="cuadrotexto" Width="350px"
																			onchange="javascript:mSetearCuenta('txtcmbDescuento', 'cmbSociDctoCcos', '1');" TextMaxLength="7"
																			Height="20px" Obligatorio="True" filtra="true" NomOper="cuentas_ctables_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbDescuento','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblSociDctoCcos" runat="server" cssclass="titulo">C.Costo:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbSociDctoCcos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="centrosc_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbDescuento','cmbSociDctoCcos','1'); var lstrFil='-1'; if (document.all('hdnCta1').value != '') lstrFil = document.all('hdnCta1').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbSociDctoCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblDeudIncob" runat="server" cssclass="titulo">Deudores Incobrables:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbDeudIncob" runat="server" cssclass="cuadrotexto" Width="350px"
																			onchange="javascript:mSetearCuenta('txtcmbDeudIncob', 'cmbDeudIncobCcos', '2');" TextMaxLength="7"
																			Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbDeudIncob','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblDeudIncobCCos" runat="server" cssclass="titulo">C.Costo Cta.Cte.:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbDeudIncobCcos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="centrosc_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbDeudIncob','cmbDeudIncobCcos','2',''); var lstrFil='-1'; if (document.all('hdnCta2').value != '') lstrFil = document.all('hdnCta2').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbDeudIncobCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblSociIncoCcos" runat="server" cssclass="titulo">C.Costo Social:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbSociIncoCcos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="centrosc_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbDeudIncob','cmbSociIncoCcos','2'); var lstrFil='-1'; if (document.all('hdnCta2').value != '') lstrFil = document.all('hdnCta2').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbSociIncoCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblEmerAgroS" runat="server" cssclass="titulo">Emergencia Agropecuaria (Socios):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbEmerAgroS" runat="server" cssclass="cuadrotexto" Width="350px"
																			onchange="javascript:mSetearCuenta('txtcmbEmerAgroS', 'cmbEmerAgroSCCos', '3');" TextMaxLength="7"
																			Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbEmerAgroS','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblEmerAgroSCCos" runat="server" cssclass="titulo">C.Costo:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbEmerAgroSCCos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbEmerAgroS','cmbEmerAgroSCCos','3'); var lstrFil='-1'; if (document.all('hdnCta3').value != '') lstrFil = document.all('hdnCta3').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbEmerAgroSCCos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblPrevDeudIncob" runat="server" cssclass="titulo">Previsi�n Deudores Incobrables:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbPrevDeudIncob" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbPrevDeudIncob','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="Label1" runat="server" cssclass="titulo">Previsi�n Deudores Incobrables (Servicios):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbPrevDeudIncobServ" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbPrevDeudIncobServ','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblRecargoMoraS" runat="server" cssclass="titulo">Recargo por Mora (Socios):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbRecargoMoraS" runat="server" cssclass="cuadrotexto" Width="350px"
																			onchange="javascript:mSetearCuenta('txtcmbRecargoMoraS', 'cmbRecargoMoraSCcos', '4');" TextMaxLength="7"
																			Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbRecargoMoraS','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblRecargoMoraSCcos" runat="server" cssclass="titulo">C.Costo:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbRecargoMoraSCcos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbRecargoMoraS','cmbRecargoMoraSCcos','4'); var lstrFil='-1'; if (document.all('hdnCta4').value != '') lstrFil = document.all('hdnCta4').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbRecargoMoraSCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblSolicitudIngS" runat="server" cssclass="titulo">Solicitudes de Ingreso (Socios):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbSolicitudIngS" runat="server" cssclass="cuadrotexto" Width="350px"
																			onchange="javascript:mSetearCuenta('txtcmbSolicitudIngS', 'cmbSociSoliCcos', '5');" TextMaxLength="7"
																			Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbSolicitudIngS','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblSociSoliCcos" runat="server" cssclass="titulo">C.Costo:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbSociSoliCcos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="ccosto_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbSolicitudIngS','cmbSociSoliCcos','5'); var lstrFil='-1'; if (document.all('hdnCta5').value != '') lstrFil = document.all('hdnCta5').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbSociSoliCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblSociBajaSoli" runat="server" cssclass="titulo">Baja de Solicitudes de Ingreso (Socios):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbSociBajaSoli" runat="server" cssclass="cuadrotexto" Width="350px"
																			onchange="javascript:mSetearCuenta('txtcmbSociBajaSoli', 'cmbSociBajaSoliCcos', '6');" TextMaxLength="7"
																			Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbSociBajaSoli','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblSociBajaSoliCcos" runat="server" cssclass="titulo">C.Costo:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbSociBajaSoliCcos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="ccosto_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbSociBajaSoli','cmbSociBajaSoliCcos','6'); var lstrFil='-1'; if (document.all('hdnCta6').value != '') lstrFil = document.all('hdnCta6').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbSociBajaSoliCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblIVATasaNormal" runat="server" cssclass="titulo">IVA tasa normal:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbIVATasaNormal" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbIVATasaNormal','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblIVATasaReducida" runat="server" cssclass="titulo">IVA tasa Reducida:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbIVATasaReducida" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbIVATasaReducida','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblIVATasaSobre" runat="server" cssclass="titulo">IVA tasa Sobretasa:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbIVATasaSobre" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbIVATasaSobre','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblIVAPercepcion" runat="server" cssclass="titulo">IVA Percepci�n:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbIVAPercepcion" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbIVAPercepcion','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblSaldosCuenta" runat="server" cssclass="titulo">Saldos a Cuenta:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbSaldosCuenta" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbSaldosCuenta','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblChequesDiaPesos" runat="server" cssclass="titulo">Cheques al D�a (pesos):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbChequesDiaPesos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbChequesDiaPesos','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblChequesDiaDolares" runat="server" cssclass="titulo">Cheques al D�a (d�lares):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbChequesDiaDolares" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbChequesDiaDolares','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblChequesDiferPesos" runat="server" cssclass="titulo">Cheques Diferidos (pesos):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbChequesDiferPesos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbChequesDiferPesos','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblChequesDiferDolares" runat="server" cssclass="titulo">Cheques Diferidos (d�lares):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbChequesDiferDolares" runat="server" cssclass="cuadrotexto"
																			Width="350px" TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbChequesDiferDolares','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblValoresCartera" runat="server" cssclass="titulo">Valores en Cartera (deudor):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbValoresCartera" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbValoresCartera','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="Label2" runat="server" cssclass="titulo">Valores en Cartera (acreedor):</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbValoresCarteraAcreedor" runat="server" cssclass="cuadrotexto"
																			Width="350px" TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbValoresCarteraAcreedor','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblDifererCambio" runat="server" cssclass="titulo">Diferencia de Cambio:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbDifererCambio" runat="server" cssclass="cuadrotexto" Width="350px"
																			onchange="javascript:mSetearCuenta('txtcmbDifererCambio', 'cmbDifererCambioCcos', '7');" TextMaxLength="7"
																			Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbDifererCambio','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px; HEIGHT: 19px" vAlign="middle" align="right">
															<asp:Label id="lblDifererCambioCcos" runat="server" cssclass="titulo">C.Costo:</asp:Label></TD>
														<TD style="HEIGHT: 19px" vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbDifererCambioCcos" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="6" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mSetearCuenta('txtcmbDifererCambio','cmbDifererCambioCcos','7'); var lstrFil='-1'; if (document.all('hdnCta7').value != '') lstrFil = document.all('hdnCta7').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbDifererCambioCcos','Centros de Costos','@cuenta='+lstrFil);"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblRecaudacionesDepositar" runat="server" cssclass="titulo">Recaudaciones a Depositar:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbRecaudacionesDepositar" runat="server" cssclass="cuadrotexto"
																			Width="350px" TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbRecaudacionesDepositar','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblGastosBancarios" runat="server" cssclass="titulo">Gastos Bancarios:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbGastosBancarios" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbGastosBancarios','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 339px" vAlign="middle" align="right">
															<asp:Label id="lblIIBBPerc" runat="server" cssclass="titulo">IIBB Percepci�n:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle" align="left">
															<TABLE cellSpacing="0" cellPadding="0" border="0">
																<TR>
																	<TD>
																		<cc1:combobox class="combo" id="cmbIIBBPerc" runat="server" cssclass="cuadrotexto" Width="350px"
																			TextMaxLength="7" Height="20px" Obligatorio="True" filtra="true" NomOper="centas_ctables_cargar"
																			MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																			onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbIIBBPerc','Cuentas Contables','');"
																			alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR height="30">
														<TD align="center" colSpan="2">
															<asp:button id="btnGrabar" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Grabar"></asp:button>&nbsp;&nbsp;</TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="5"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="5"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnPage" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnCta1" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta2" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta3" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta4" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta5" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta6" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta7" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.frmABM.hdnPage.value != '') {
		   document.location='#editar';
		   document.frmABM.txtDesc.focus();
		}
		</SCRIPT>
	</BODY>
</HTML>
