<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ProcesamientoLote" CodeFile="ProcesamientoLote.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Procesar Lote</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var gstrCria = '';   
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
				
		function mAbrirProducto(pstrPrdtId)
		{
			gAbrirVentanas("productos.aspx?ProdId=" + pstrPrdtId + "&ctrlId='proclote'", 1);  	
		}
				
		function mSetearTipoPropietario()
		{
			if (document.all('panPropCria')!=null && document.all('optCria')!=null)
			{
				if (document.all('optCria').checked)
				{
					document.all('panPropCria').style.display = '';
					document.all('panPropClie').style.display = 'none';
				}
				else
				{
					document.all('panPropCria').style.display = 'none';
					document.all('panPropClie').style.display = '';		
				}
			}
		}

		function mSetearRazas()
		{
			if (document.all("txtcmbRazaFil")!=null && document.all("txtusrMadreFil:cmbProdRaza")!= null)
			{
				document.all("txtusrMadreFil:cmbProdRaza").value = document.all("txtcmbRazaFil").value;
				mBuscarIdXCodi("usrMadreFil:cmbProdRaza", "razas_cargar", "Id");
				document.all("txtusrPadreFil:cmbProdRaza").value = document.all("txtcmbRazaFil").value;
				mBuscarIdXCodi("usrPadreFil:cmbProdRaza", "razas_cargar", "Id");
				document.all("txtusrReceFil:cmbProdRaza").value = document.all("txtcmbRazaFil").value;
				mBuscarIdXCodi("usrReceFil:cmbProdRaza", "razas_cargar", "Id");
			}
		}
		
		function mSelecSexo(pcmb, pTitu)
		{
		 if (document.all(pTitu)!=null && document.all(pcmb)!=null)
		 {
			if (document.all(pcmb).value="")//todos
				document.all(pTitu).innertext = "Nro.Donante/Dador:"
			else
				if (document.all(pcmb).value="0")//hembra
				document.all(pTitu).innertext = "Nro.Donante:"
				else //macho
				document.all(pTitu).innertext = "Nro.Dador:"	   	   
		 }
		}
																
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
					{
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 					document.all('lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					document.all('lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
		}
		
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function CambiarExp()
		{
			if(document.all("spExp").innerText=="+")
			{
				document.all("panBuscAvan").style.display = "inline";
				document.all("spExp").innerText="-";
			}
			
			else
			{
				document.all("panBuscAvan").style.display = "none";
				document.all("spExp").innerText="+";
			}

		}
		
		function cmbRazaFil_onchange(pstrNomb)
		{
			if(document.all(pstrNomb)!=null && document.all("txtusrCriaFil:cmbRazaCria")!=null)
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all(pstrNomb).value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = !(document.all(pstrNomb).value=="");
				document.all("txtusrCriaFil:cmbRazaCria").disabled = !(document.all(pstrNomb).value=="");
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Procesar Lote</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3">
										<asp:panel id="panFiltros" runat="server" cssclass="titulo" width="100%" BorderWidth="1px"
											BorderStyle="none">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD style="WIDTH: 100%">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%">
														<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD style="HEIGHT: 100%">
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 10px" align="right">
																				<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																			<TD>
																				<TABLE cellSpacing="0" cellPadding="0" border="0">
																					<TR>
																						<TD>
																							<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="180px"
																								AceptaNull="False" onchange="javascript:mNumeDenoConsul(this,true);mSetearRazas();Combo_change(this);"
																								Height="20px" NomOper="razas_cargar" MostrarBotones="False" filtra="true"></cc1:combobox></TD>
																						<TD align="right" width="50">
																							<asp:label id="lblNumeFil" runat="server" cssclass="titulo"> Nro.:&nbsp;</asp:label></TD>
																						<TD>
																							<CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:Label id="lblSexoFil" runat="server" cssclass="titulo">&nbsp;&nbsp;Sexo:&nbsp;</asp:Label></TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbSexoFil" runat="server" Width="113px" AceptaNull="True" onchange="mSelecSexo(this,'lblDonaFil');">
																					<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																					<asp:ListItem Value="0">Hembra</asp:ListItem>
																					<asp:ListItem Value="1">Macho</asp:ListItem>
																				</cc1:combobox>
																				<asp:label id="lblRPNumeFil" runat="server" cssclass="titulo">RP:&nbsp;</asp:label>
																				<CC1:TEXTBOXTAB id="txtRPNumeFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblNumeDesdeFil" runat="server" cssclass="titulo">Nro. Desde:&nbsp;</asp:label></TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtNumeDesdeFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB>
																				<asp:label id="lblNumeHastaFil" runat="server" cssclass="titulo">Nro. Hasta:&nbsp;</asp:label>
																				<CC1:TEXTBOXTAB id="txtNumeHastaFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblRPDesdeFil" runat="server" cssclass="titulo">RP Desde:&nbsp;</asp:label></TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtRPDesdeFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB>
																				<asp:label id="lblRPHastaFil" runat="server" cssclass="titulo">RP Hasta:&nbsp;</asp:label>
																				<CC1:TEXTBOXTAB id="txtRPHastaFil" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblNaciFechaFil" runat="server" cssclass="titulo">Fecha Nacimiento:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:DateBox id="txtNaciFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>
																				<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtNaciFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblInscFechaFil" runat="server" cssclass="titulo">Fecha Inscripci�n:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:DateBox id="txtInscFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>
																				<asp:label id="lblFechaInscHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtInscFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:Label id="lblCriaFil" runat="server" cssclass="titulo">Criador:&nbsp;</asp:Label></TD>
																			<TD>
																				<UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="false" criador="true" Tabla="Criadores"
																					Saltos="1,2" AutoPostBack="False" FilSociNume="True" FilTipo="T" MuestraDesc="False" FilDocuNume="True"
																					Ancho="800"></UC1:CLIE></TD>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblPropFil" runat="server" cssclass="titulo">Propietario:&nbsp;</asp:label></TD>
																			<TD>
																				<asp:RadioButton id="optClie" onclick="javascript:mSetearTipoPropietario();" runat="server" CssClass="titulo"
																					Text="Cliente" GroupName="grpTipo" Checked="True"></asp:RadioButton>
																				<asp:RadioButton id="optCria" onclick="javascript:mSetearTipoPropietario();" runat="server" CssClass="titulo"
																					Text="Expediente" GroupName="grpTipo"></asp:RadioButton>
																				<DIV id="panPropClie">
																					<UC1:CLIE id="usrPropClieFil" runat="server" AceptaNull="false" Tabla="Clientes" Saltos="1,2"
																						AutoPostBack="False" FilSociNume="True" FilTipo="T" MuestraDesc="False" FilDocuNume="True"
																						Ancho="800"></UC1:CLIE></DIV>
																				<DIV id="panPropCria">
																					<UC1:CLIE id="usrPropCriaFil" runat="server" AceptaNull="false" Tabla="Criadores" Saltos="1,2"
																						AutoPostBack="False" FilSociNume="True" FilTipo="T" MuestraDesc="False" FilDocuNume="True"
																						Ancho="800" Criador="True"></UC1:CLIE></DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:label></TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblApodoFil" runat="server" cssclass="titulo">Apodo:&nbsp;</asp:label></TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtApodoFil" runat="server" cssclass="cuadrotexto" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right"></TD>
																			<TD>
																				<asp:checkbox id="chkBusc" CssClass="titulo" Text="Buscar en..." Runat="server"></asp:checkbox>&nbsp;&nbsp;&nbsp;
																				<asp:checkbox id="chkBaja" CssClass="titulo" Text="Incluir Dados de Baja" Runat="server"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:label id="lblAsoNumeFil" runat="server" cssclass="titulo">Asociaci�n:&nbsp;</asp:label></TD>
																			<TD>
																				<TABLE cellSpacing="0" cellPadding="0" width="95%" border="0">
																					<TR>
																						<TD>
																							<cc1:combobox class="combo" id="cmbAsocFil" runat="server" cssclass="cuadrotexto" Width="100%"
																								AceptaNull="False" onchange="Combo_change(this)" NomOper="asociaciones_cargar" filtra="True"
																								mostrarbotones="False"></cc1:combobox></TD>
																						<TD width="30">
																							<asp:Label id="lblAsocNumeFil" runat="server" cssclass="titulo">&nbsp;&nbsp;Nro:&nbsp;</asp:Label></TD>
																						<TD width="80">
																							<CC1:TEXTBOXTAB id="txtAsoNumeFil" runat="server" cssclass="cuadrotexto" Width="100%" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblLaboNumeFil" runat="server" cssclass="titulo">N� An�lisis:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:numberbox id="txtLaboNumeFil" runat="server" cssclass="cuadrotexto" Width="100px" MaxLength="12"
																					MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblTramNumeFil" runat="server" cssclass="titulo">N� Tr�mite:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:numberbox id="txtTramNumeFil" runat="server" cssclass="cuadrotexto" Width="160px" MaxLength="12"
																					MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblCuigFil" runat="server" cssclass="titulo">Nro.Oficial:&nbsp;</asp:label></TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtCuigFil" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblDonaFil" runat="server" cssclass="titulo" Visible="False">Nro.Donante/Dador:&nbsp;</asp:Label></TD>
																			<TD>
																				<cc1:numberbox id="txtDonaFil" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="false"
																					MaxLength="12" MaxValor="9999999999999" esdecimal="False" Visible="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblCtroFil" runat="server" cssclass="titulo" Visible="False">Centro de Transplante:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbCtroFil" runat="server" Width="200px" AceptaNull="False" Visible="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblCtrolFactuNumeFil" runat="server" cssclass="titulo" Visible="False">N� Ctrol Fact.:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:numberbox id="txtCtrolFactuNumeFil" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"
																					MaxLength="12" MaxValor="9999999999999" esdecimal="False" Visible="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblEstaFil" runat="server" cssclass="titulo">Estado:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbEstaFil" runat="server" Width="140px" AceptaNull="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblNaciFil" runat="server" cssclass="titulo">Nacionalidad:&nbsp;</asp:label></TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbNaciFil" runat="server" Width="140px" AceptaNull="False">
																					<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
																					<asp:ListItem Value="I">Importado</asp:ListItem>
																					<asp:ListItem Value="E">Extranjero</asp:ListItem>
																					<asp:ListItem Value="N">Nacional</asp:ListItem>
																				</cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2">
																				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR id="divBusqAvanzada" runat="server">
																						<TD align="right"><SPAN id="spExp" style="FONT-WEIGHT: bold; FONT-SIZE: 14px; CURSOR: hand; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 4px"
																								onclick="CambiarExp()">+</SPAN></TD>
																						<TD width="100%">
																							<asp:label id="lblExp" runat="server" cssclass="titulo">B�squeda avanzada</asp:label></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																							<DIV class="panelediciontransp" id="panBuscAvan" style="DISPLAY: none; Z-INDEX: 101; WIDTH: 100%">
																								<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
																									<TR>
																										<TD style="HEIGHT: 10px" align="right" colSpan="2"></TD>
																									</TR>
																									<TR>
																										<TD style="HEIGHT: 10px" align="right">
																											<asp:label id="lblObservacionFil" runat="server" cssclass="titulo">Observaci�n:</asp:label>&nbsp;</TD>
																										<TD>
																											<CC1:TEXTBOXTAB id="txtObservacionFil" runat="server" cssclass="cuadrotexto" Width="80%" Height="46px"
																												EnterPorTAb="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																									</TR>
																									<TR>
																										<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																									</TR>
																									<TR>
																										<TD style="WIDTH: 15%" align="right">
																											<asp:label id="lblPadreFil" runat="server" cssclass="titulo">Padre:</asp:label>&nbsp;</TD>
																										<TD>
																											<UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																												AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True"
																												Ancho="800" FilSexo="False" Inscrip="False" Sexo="1"></UC1:PROD></TD>
																									</TR>
																									<TR>
																										<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblMadreFil" runat="server" cssclass="titulo">Madre:</asp:label>&nbsp;</TD>
																										<TD>
																											<UC1:PROD id="usrMadreFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																												AutoPostBack="False" FilSociNume="True" FilTipo="T" MuestraDesc="True" FilDocuNume="True"
																												Ancho="800" FilSexo="False" Inscrip="False" Sexo="0"></UC1:PROD></TD>
																									</TR>
																									<TR>
																										<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblReceFil" runat="server" cssclass="titulo">Receptora:</asp:label>&nbsp;</TD>
																										<TD>
																											<UC1:PROD id="usrReceFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																												AutoPostBack="False" FilSociNume="True" FilTipo="T" MuestraDesc="True" FilDocuNume="True"
																												Ancho="800" FilSexo="False" Inscrip="False" Sexo="0"></UC1:PROD></TD>
																									</TR>
																									<TR>
																										<TD style="HEIGHT: 10px" align="right" colSpan="2"></TD>
																									</TR>
																								</TABLE>
																							</DIV>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right" colSpan="3"><BR>
										<asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
											OnPageIndexChanged="DataGrid_Page">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderStyle-Width="2%">
													<ItemTemplate>
														<asp:CheckBox ID="chkSel" Checked=<%#DataBinder.Eval(Container, "DataItem.chk")%> Runat="server"></asp:CheckBox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="10px"></HeaderStyle>
													<ItemTemplate>
														<A href="javascript:mAbrirProducto(<%#DataBinder.Eval(Container, "DataItem.prdt_id")%>);">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</A>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="prdt_id" ReadOnly="True" HeaderText="Id Producto"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_raza" HeaderText="Raza">
													<HeaderStyle Width="50px"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_sexo" ReadOnly="True" HeaderText="Sexo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_nomb" ReadOnly="True" HeaderText="Nombre">
													<HeaderStyle Width="30%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_asoc" ReadOnly="True" HeaderText="Asoc."></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_regi" ReadOnly="True" HeaderText="Registro"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_rp" ReadOnly="True" HeaderText="RP"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_propietario" ReadOnly="True" HeaderText="Propietario"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_naci_fecha" ReadOnly="True" HeaderText="F.Nacim."
													DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV style="DISPLAY: inline" id="divgraba" runat="server">
										<ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR height="30">
													<TD vAlign="middle" width="50%">
														<asp:Button id="btnTodos" runat="server" cssclass="boton" Width="80px" Text="Todos"></asp:Button>&nbsp;
														<asp:Button id="btnNinguno" runat="server" cssclass="boton" Width="80px" Text="Ninguno" CausesValidation="False"></asp:Button></TD>
													<TD vAlign="middle" align=right><A id="editar" name="editar"></A>
														<asp:Button id="btnProcesar" runat="server" cssclass="boton" Width="80px" Text="Procesar" CausesValidation="False"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										</DIV>
										<DIV style="DISPLAY: none" id="divproce" runat="server">
											<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
											     <asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										     </asp:panel>
										</DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE> <!--- FIN CONTENIDO ---></td>
					<TD width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></TD>
				</tr>
				<TR>
					<TD width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></TD>
					<TD background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></TD>
					<TD width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></TD>
				</TR>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnAnaId" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnSRA" runat="server"></asp:textbox><asp:textbox id="hdnEspe" runat="server"></asp:textbox><asp:textbox id="hdnDocumId" runat="server"></asp:textbox><asp:textbox id="hdnAsocId" runat="server"></asp:textbox><asp:textbox id="hdnCriaId" runat="server"></asp:textbox><asp:textbox id="hdnCriaFilId" runat="server"></asp:textbox><asp:textbox id="hdnObseId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
			function mPorcPeti()
			{
				document.all("divgraba").style.display ="none";
				document.all("divproce").style.display ="inline";
			}
			if (document.all["editar"]!= null)
				document.location='#editar';
			if (document.all["usrClieFil_txtCodi"]!= null && !document.all["usrClieFil_txtCodi"].disabled)
				document.all["usrClieFil_txtCodi"].focus();		
			if (document.all('cmbRazaFil')!=null)
				mNumeDenoConsul(document.all('cmbRazaFil'),true);

			mSetearRazas();
			mSetearTipoPropietario();
		</SCRIPT>
	
		
		</TD></TR></TBODY></TABLE>
	</BODY>
</HTML>
