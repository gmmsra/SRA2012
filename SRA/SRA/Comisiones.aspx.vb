Namespace SRA

    Partial Class Comisiones
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comisiones
        Private mstrComiProf As String = SRA_Neg.Constantes.gTab_ComisionesProfe
        Private mstrComiInsc As String = SRA_Neg.Constantes.gTab_ComisionesInscrip
        Private mstrComiSema As String = SRA_Neg.Constantes.gTab_ComisionesSemana
        Private mstrComiDias As String = SRA_Neg.Constantes.gTab_ComisionesDias

        Private mstrConn As String
        Private mstrParaPageSize As Integer

        Private mdsDatos As DataSet
        Private mintInse As Integer

        Private Enum ColumnasInsc As Integer
            Id = 0
            InscId = 1
            ComiId = 2
            Recur = 3
            Oyen = 4
        End Enum
#End Region

#Region "Inicializaci�n de Variables"
        Private Sub mInicializar()
            Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
            hdnInse.Text = mintInse
        End Sub

#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()
                If (Not Page.IsPostBack) Then
                    Session(mstrTabla) = Nothing
                    mdsDatos = Nothing

                    Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                    mSetearMaxLength()
                    mSetearEventos()
                    txtAnio.Valor = Today.Year
                    txtAnioFil.Valor = Today.Year

                    mCargarCombos()

                    mConsultar()
                    clsWeb.gInicializarControles(Me, mstrConn)
                Else
                    mdsDatos = Session(mstrTabla)
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mRecuperarParametros()
            Dim lstrFiltros As String = ""
            Dim lstrCicl As String = hdnSeleCiclFil.Text
            Dim lstrMate As String = hdnSeleMateFil.Text

            'ciclos
            lstrFiltros = mintInse & ","
            If txtAnio.Text = "" Then
                lstrFiltros = lstrFiltros & "-1"
            Else
                lstrFiltros = lstrFiltros & txtAnio.Text
            End If
            clsWeb.gCargarCombo(mstrConn, "ciclos_cargar " & lstrFiltros, cmbCiclFil, "id", "descrip", "T")
            cmbCiclFil.Valor = lstrCicl

            'materias
            lstrFiltros = "@mate_inse_id=" & mintInse
            If lstrCicl <> "" Then
                lstrFiltros = lstrFiltros + ",@cicl_id=" & lstrCicl
                clsWeb.gCargarCombo(mstrConn, "materiasXciclo_cargar " & lstrFiltros, cmbMateFil, "id", "descrip", "T")
            End If
            cmbMateFil.Valor = lstrMate

        End Sub

        Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
            btnGeneDias.Attributes.Add("onclick", "if(!confirm('Confirma la generaci�n autom�tica de fechas?')) return false;")
            cmbCiclFil.Attributes.Add("onchange", "cmbCiclFil_Change(this,'" & mintInse & "');")
        End Sub

        Private Sub mCargarCombos()
            clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCiclFil, "T", mintInse.ToString)
            clsWeb.gCargarRefeCmb(mstrConn, "materiasXciclo", cmbMateFil, "T", "@mate_inse_id=" & mintInse.ToString)
            clsWeb.gCargarRefeCmb(mstrConn, "profesores", cmbProf, "S", mintInse.ToString)
            clsWeb.gCargarRefeCmb(mstrConn, "profe_tipos", cmbPrti, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCicl, "S", mintInse.ToString & "," & txtAnio.Valor.ToString)
            mSetearCiclDesc()
            mCargarMaterias()
            mCargarDivisiones()
            mSetearFechas()

            clsWeb.gCargarRefeCmb(mstrConn, "dias", cmbDia, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "prac_teo", cmbDiasPrac, "")
            clsWeb.gCargarRefeCmb(mstrConn, "prac_teo", cmbSemaPrac, "")

            clsWeb.gCargarComboBool(cmbOyen, "")
            clsWeb.gCargarComboBool(cmbRecur, "")

            ' cmbOyen.ValorBool = False
            '       cmbRecur.ValorBool = False
            cmbOyen.Valor = False
            cmbRecur.Valor = False

        End Sub

        Private Sub mCargarMaterias()
            clsWeb.gCargarRefeCmb(mstrConn, "materiasXciclo", cmbMate, "S", clsSQLServer.gFormatArg(cmbCicl.Valor.ToString, SqlDbType.Int))
        End Sub

        Private Sub mCargarDivisiones()
            clsWeb.gCargarRefeCmb(mstrConn, "divisiones", cmbDivi, "S", "@divi_cicl_id=" + clsSQLServer.gFormatArg(cmbCicl.Valor.ToString, SqlDbType.Int))
        End Sub

        Private Sub mCargarInscripciones()
            clsWeb.gCargarRefeCmb(mstrConn, "inscripciones", cmbInsc, "S", "@insc_cicl_id=" + clsSQLServer.gFormatArg(cmbCicl.Valor.ToString, SqlDbType.Int) + ", @mate_id=" + clsSQLServer.gFormatArg(cmbMate.Valor.ToString, SqlDbType.Int))
        End Sub

        Private Sub mSetearMaxLength()
            Dim lstrLong As Object

            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        End Sub
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            Select Case pstrTabla
                Case mstrComiProf
                    btnBajaProf.Enabled = Not (pbooAlta)
                    btnModiProf.Enabled = Not (pbooAlta)
                    btnAltaProf.Enabled = pbooAlta
                Case mstrComiInsc
                    btnBajaInsc.Enabled = Not (pbooAlta)
                    btnModiInsc.Enabled = Not (pbooAlta)
                    btnAltaInsc.Enabled = pbooAlta
                Case mstrComiSema
                    btnBajaSema.Enabled = Not (pbooAlta)
                    btnModiSema.Enabled = Not (pbooAlta)
                    btnAltaSema.Enabled = pbooAlta
                Case mstrComiDias
                    btnBajaDias.Enabled = Not (pbooAlta)
                    btnModiDias.Enabled = Not (pbooAlta)
                    btnAltaDias.Enabled = pbooAlta
                Case Else
                    btnBaja.Enabled = Not (pbooAlta)
                    btnModi.Enabled = Not (pbooAlta)
                    btnAlta.Enabled = pbooAlta
                    cmbDivi.Enabled = pbooAlta
            End Select
        End Sub

        Public Sub mCargarDatos(ByVal pstrId As String)
            mCrearDataSet(pstrId)

            With mdsDatos.Tables(mstrTabla).Rows(0)
                hdnId.Text = .Item("comi_id").ToString()
                txtAnio.Valor = .Item("_cicl_anio")
                clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCicl, "S", mintInse.ToString & "," & txtAnio.Valor.ToString)
                cmbCicl.Valor = IIf(.Item("comi_cicl_id").ToString.Trim.Length > 0, .Item("comi_cicl_id"), DBNull.Value)
                txtInicFecha.Fecha = IIf(.Item("comi_inic_fecha").ToString.Trim.Length > 0, .Item("comi_inic_fecha"), DBNull.Value)
                txtFinaFecha.Fecha = IIf(.Item("comi_fina_fecha").ToString.Trim.Length > 0, .Item("comi_fina_fecha"), DBNull.Value)

                txtVaca.Valor = IIf(.Item("comi_vaca").ToString.Trim.Length > 0, .Item("comi_vaca"), String.Empty)

                mSetearCiclDesc()
                mCargarMaterias()
                mCargarDivisiones()

                cmbDivi.Valor = IIf(.Item("comi_divi_id").ToString.Trim.Length > 0, .Item("comi_divi_id"), DBNull.Value)
                cmbMate.Valor = IIf(.Item("comi_mate_id").ToString.Trim.Length > 0, .Item("comi_mate_id"), DBNull.Value)

                If Not .IsNull("comi_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("comi_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
            End With

            mSetearCiclos()
            mSetearGenerarDias()

            lblTitu.Text = "Registro Seleccionado: " + cmbCicl.SelectedItem.Text + " - " + mdsDatos.Tables(mstrTabla).Rows(0).Item("_divi_nomb") + " - " + cmbMate.SelectedItem.Text

            mSetearEditor("", False)
            mMostrarPanel(True)
        End Sub

        Public Sub mSetearCiclos()
            cmbCicl.Enabled = grdInsc.Items.Count = 0
            cmbMate.Enabled = grdInsc.Items.Count = 0
        End Sub

        Public Sub mSetearGenerarDias()
            btnGeneDias.Enabled = grdDias.Items.Count = 0
        End Sub

        Private Sub mAgregar()
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            mMostrarPanel(True)
        End Sub

        Private Sub mLimpiar()
            hdnId.Text = ""
            lblTitu.Text = ""
            txtAnio.Text = Today.Year
            cmbCicl.Limpiar()
            txtInicFecha.Text = ""
            txtFinaFecha.Text = ""

            txtVaca.Text = ""

            mSetearCiclDesc()
            mCargarMaterias()
            mCargarDivisiones()
            mSetearFechas()

            cmbMate.Limpiar()
            lblBaja.Text = ""

            grdProf.CurrentPageIndex = 0
            grdDias.CurrentPageIndex = 0
            grdSema.CurrentPageIndex = 0
            grdInsc.CurrentPageIndex = 0

            mCrearDataSet("")

            mLimpiarProf()
            mLimpiarDias()
            mLimpiarSema()
            mLimpiarInsc()

            mSetearCiclos()

            mSetearGenerarDias()

            mSetearEditor("", True)
            mShowTabs(1)
        End Sub

        Private Sub mCerrar()
            mLimpiar()
            mConsultar()
        End Sub

        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            Dim lbooVisiOri As Boolean = panDato.Visible

            panDato.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            panFiltro.Visible = Not panDato.Visible
            btnAgre.Visible = Not panDato.Visible
            grdDato.Visible = Not panDato.Visible
            tabLinks.Visible = pbooVisi
            btnList.Visible = Not pbooVisi

            If pbooVisi Then
                grdDato.DataSource = Nothing
                grdDato.DataBind()

                mShowTabs(1)
            Else
                Session(mstrTabla) = Nothing
                mdsDatos = Nothing
            End If

            'If lbooVisiOri And Not pbooVisi Then
            'cmbCiclFil.Limpiar()
            'clsWeb.gCargarRefeCmb(mstrConn, "materiasXciclo", cmbMateFil, "T", "@mate_inse_id=" & mintInse.ToString)
            'txtAnioFil.Valor = Today.Year
            'End If
        End Sub

        Private Sub mShowTabs(ByVal origen As Byte)
            Try
                panDato.Visible = True
                panBotones.Visible = True
                panProf.Visible = False
                panDias.Visible = False
                panSema.Visible = False
                panInsc.Visible = False
                panCabecera.Visible = False

                lnkCabecera.Font.Bold = False
                lnkProf.Font.Bold = False
                lnkDias.Font.Bold = False
                lnkSema.Font.Bold = False
                lnkInsc.Font.Bold = False

                Select Case origen
                    Case 1
                        panCabecera.Visible = True
                        lnkCabecera.Font.Bold = True
                        lblTitu.Text = "Datos del Comision "
                    Case 2
                        panProf.Visible = True
                        lnkProf.Font.Bold = True
                        lblTitu.Text = "Profesores de la Comisi�n: " & cmbCicl.SelectedItem.Text & "-" & cmbMate.SelectedItem.Text
                        grdProf.Visible = True
                    Case 3
                        panInsc.Visible = True
                        lnkInsc.Font.Bold = True
                        lblTitu.Text = "Inscripciones de la Comisi�n: " & cmbCicl.SelectedItem.Text & "-" & cmbMate.SelectedItem.Text
                        grdInsc.Visible = True

                        mCargarInscripciones()

                    Case 4
                        panSema.Visible = True
                        lnkSema.Font.Bold = True
                        lblTitu.Text = "Horarios de la Comisi�n: " & cmbCicl.SelectedItem.Text & "-" & cmbMate.SelectedItem.Text
                        grdSema.Visible = True
                    Case 5
                        panDias.Visible = True
                        lnkDias.Font.Bold = True
                        lblTitu.Text = "D�as de la Comisi�n: " & cmbCicl.SelectedItem.Text & "-" & cmbMate.SelectedItem.Text
                        grdSema.Visible = True
                End Select


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
            mShowTabs(1)
        End Sub

        Private Sub lnkProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkProf.Click
            mShowTabs(2)
        End Sub

        Private Sub lnkInsc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInsc.Click
            mShowTabs(3)
        End Sub

        Private Sub lnkSema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSema.Click
            mShowTabs(4)
        End Sub

        Private Sub lnkDias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDias.Click
            mShowTabs(5)
        End Sub
#End Region

#Region "Opciones de ABM"
        Private Sub mAlta()
            Try
                mGuardarDatos()

                Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
                lobjGenericoRel.Alta()

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mModi()
            Try
                mGuardarDatos()

                Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
                lobjGenericoRel.Modi()

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mBaja()
            Try
                Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
                lobjGenericoRel.Baja(hdnId.Text)

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mGuardarDatos()
            mValidarDatos()

            If cmbCicl.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Seleccione un cliclo!")
            End If

            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("comi_cicl_id") = cmbCicl.Valor
                .Item("comi_mate_id") = cmbMate.Valor

                If .IsNull("comi_divi_id") Then
                    .Item("comi_divi_id") = cmbDivi.Valor
                End If

                .Item("comi_baja_fecha") = DBNull.Value

                .Item("comi_inic_fecha") = txtInicFecha.Fecha
                .Item("comi_fina_fecha") = txtFinaFecha.Fecha

                .Item("comi_vaca") = IIf(txtVaca.Valor.Trim.Length > 0, txtVaca.Valor, DBNull.Value)
            End With
        End Sub

        Private Sub mValidarDatos()
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)
        End Sub

        Public Sub mCrearDataSet(ByVal pstrId As String)

            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrComiProf
            mdsDatos.Tables(2).TableName = mstrComiInsc
            mdsDatos.Tables(3).TableName = mstrComiSema
            mdsDatos.Tables(4).TableName = mstrComiDias

            With mdsDatos.Tables(mstrTabla).Rows(0)
                If .IsNull("comi_id") Then
                    .Item("comi_id") = -1
                End If
            End With

            mConsultarProf()

            mConsultarInsc()

            mConsultarSema()

            mConsultarDias()

            Session(mstrTabla) = mdsDatos
        End Sub

        Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub

        Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub

        Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub

        Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
        End Sub

        Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub



        Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
            Try
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mConsultar()
            Try
                Dim lstrCmd As New StringBuilder
                lstrCmd.Append("exec " + mstrTabla + "_consul")
                lstrCmd.Append(" @cicl_inse_id=" + mintInse.ToString)
                lstrCmd.Append(", @cicl_anio=" + clsSQLServer.gFormatArg(txtAnioFil.Text, SqlDbType.Int))
                lstrCmd.Append(", @comi_cicl_id=" + cmbCiclFil.Valor.ToString)
                lstrCmd.Append(", @comi_mate_id=" + cmbMateFil.Valor.ToString)

                clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub cmbCicl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCicl.SelectedIndexChanged
            Try
                mSetearCiclDesc()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mSetearCiclDesc()
            txtCiclDesc.Text = SRA_Neg.Utiles.ObtenerDescCiclo(mstrConn, cmbCicl.Valor.ToString)
        End Sub

        Private Sub mSetearFechas()
            Dim vsRet As String()
            vsRet = SRA_Neg.Utiles.FechasCicloMate(mstrConn, cmbCicl.Valor.ToString & ";" & cmbMate.Valor.ToString).Split(";")

            txtInicFecha.Text = vsRet(0)
            txtFinaFecha.Text = vsRet(1)
        End Sub

        Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
            Try
                Dim lstrRptName As String = "Comisiones"
                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

                lstrRpt += "&fkvalor=" + Request.QueryString("fkvalor")

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                Response.Redirect(lstrRpt)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

#Region "Operacion Sobre la Grilla"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.CurrentPageIndex = E.NewPageIndex
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Try
                mCargarDatos(e.Item.Cells(1).Text)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

#Region "Detalle"
        Public Sub mEditarDatosProf(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try

                Dim ldrCopr As DataRow

                hdnCoprId.Text = E.Item.Cells(1).Text
                ldrCopr = mdsDatos.Tables(mstrComiProf).Select("copr_id=" & hdnCoprId.Text)(0)

                With ldrCopr
                    cmbProf.Valor = .Item("copr_prof_id")
                    cmbPrti.Valor = .Item("copr_prti_id")
                End With

                mSetearEditor(mstrComiProf, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosInsc(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                Dim ldrInsc As DataRow

                hdnCoinId.Text = E.Item.Cells(1).Text
                ldrInsc = mdsDatos.Tables(mstrComiInsc).Select("coin_id=" & hdnCoinId.Text)(0)

                With ldrInsc
                    cmbInsc.Valor = .Item("coin_Insc_id")
                End With

                mSetearEditor(mstrComiInsc, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosDias(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                Dim ldrCodc As DataRow

                hdnCodcId.Text = E.Item.Cells(1).Text
                ldrCodc = mdsDatos.Tables(mstrComiDias).Select("codc_id=" & hdnCodcId.Text)(0)

                With ldrCodc
                    txtFecha.Fecha = .Item("codc_fecha")
                    txtDiaHora.Valor = .Item("codc_fecha")
                    txtDiaHs.Valor = .Item("codc_hs")

                    cmbDiasPrac.Valor = Math.Abs(CInt(.Item("codc_prac")))
                End With

                mSetearEditor(mstrComiDias, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosSema(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try

                Dim ldrSema As DataRow

                hdnCoseId.Text = E.Item.Cells(1).Text
                ldrSema = mdsDatos.Tables(mstrComiSema).Select("cose_id=" & hdnCoseId.Text)(0)

                With ldrSema
                    cmbDia.Valor = .Item("cose_dia")
                    txtHora.Valor = .Item("cose_hora")
                    txtSemaHs.Valor = .Item("cose_hs")
                    cmbSemaPrac.Valor = Math.Abs(CInt(.Item("cose_prac")))
                End With

                mSetearEditor(mstrComiSema, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdProf_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdProf.EditItemIndex = -1
                If (grdProf.CurrentPageIndex < 0 Or grdProf.CurrentPageIndex >= grdProf.PageCount) Then
                    grdProf.CurrentPageIndex = 0
                Else
                    grdProf.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarProf()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdInsc_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdInsc.EditItemIndex = -1
                If (grdInsc.CurrentPageIndex < 0 Or grdInsc.CurrentPageIndex >= grdInsc.PageCount) Then
                    grdInsc.CurrentPageIndex = 0
                Else
                    grdInsc.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarInsc()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdSema_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdSema.EditItemIndex = -1
                If (grdSema.CurrentPageIndex < 0 Or grdSema.CurrentPageIndex >= grdSema.PageCount) Then
                    grdSema.CurrentPageIndex = 0
                Else
                    grdSema.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarSema()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdDias_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDias.EditItemIndex = -1
                If (grdDias.CurrentPageIndex < 0 Or grdDias.CurrentPageIndex >= grdDias.PageCount) Then
                    grdDias.CurrentPageIndex = 0
                Else
                    grdDias.CurrentPageIndex = E.NewPageIndex
                End If

                mConsultarDias()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mActualizarProf()
            Try
                mGuardarDatosCopr()

                mLimpiarProf()
                mConsultarProf()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mActualizarSema()
            Try
                mGuardarDatosSema()

                mLimpiarSema()
                mConsultarSema()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mActualizarInsc()
            Try
                mGuardarDatosInsc()

                mLimpiarInsc()
                mConsultarInsc()



            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mActualizarDias()
            Try
                mGuardarDatosDias()

                mLimpiarDias()

                mConsultarDias()

                mSetearCiclos()
                mSetearGenerarDias()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mGuardarDatosCopr()
            Dim ldrDatos As DataRow

            If cmbProf.Valor.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Profesor.")
            End If

            If cmbPrti.Valor.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Tipo de Profesor.")
            End If

            If mdsDatos.Tables(mstrComiProf).Select("copr_prof_id=" & cmbProf.Valor & " AND copr_prti_id=" & cmbPrti.Valor & " AND copr_id <> " & clsSQLServer.gFormatArg(hdnCoprId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Profesor-Tipo existente.")
            End If

            If hdnCoprId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrComiProf).NewRow
                ldrDatos.Item("copr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrComiProf), "copr_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrComiProf).Select("copr_id=" & hdnCoprId.Text)(0)
            End If

            With ldrDatos
                .Item("copr_prof_id") = cmbProf.Valor
                .Item("copr_prti_id") = cmbPrti.Valor
                .Item("_prof_nyap") = cmbProf.SelectedItem.Text
                .Item("_prti_desc") = cmbPrti.SelectedItem.Text
            End With

            If hdnCoprId.Text = "" Then
                mdsDatos.Tables(mstrComiProf).Rows.Add(ldrDatos)
            End If

        End Sub

        Private Sub mGuardarDatosSema()
            Dim ldrDatos As DataRow

            If cmbDia.Valor.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el d�a")
            End If

            If txtHora.Valor.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la hora")
            End If

            If txtSemaHs.Valor.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar las horas")
            End If

            If hdnCoseId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrComiSema).NewRow
                ldrDatos.Item("cose_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrComiSema), "cose_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrComiSema).Select("cose_id=" & hdnCoseId.Text)(0)
            End If

            With ldrDatos
                .Item("cose_dia") = IIf(cmbDia.Valor.Length > 0, cmbDia.Valor, DBNull.Value)
                .Item("cose_hora") = txtHora.Valor
                .Item("cose_hs") = txtSemaHs.Valor
                .Item("cose_prac") = CBool(cmbSemaPrac.Valor)

                .Item("_dia") = cmbDia.SelectedItem.Text
                .Item("_prac") = cmbSemaPrac.SelectedItem.Text
            End With

            If hdnCoseId.Text = "" Then
                mdsDatos.Tables(mstrComiSema).Rows.Add(ldrDatos)
            End If
        End Sub

        Private Sub mGuardarDatosInsc()
            Dim ldrDatos As DataRow

            If cmbInsc.Valor.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el alumno.")
            End If

            If mdsDatos.Tables(mstrComiInsc).Select("coin_insc_id=" & cmbInsc.Valor & " AND coin_id <> " & clsSQLServer.gFormatArg(hdnCoinId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Alumno existente.")
            End If

            If hdnCoinId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrComiInsc).NewRow
                ldrDatos.Item("coin_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrComiInsc), "coin_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrComiInsc).Select("coin_id=" & hdnCoinId.Text)(0)
            End If

            With ldrDatos
                .Item("coin_insc_id") = cmbInsc.Valor
                .Item("coin_recur") = IIf(cmbRecur.Valor = "1", True, False) ' cmbRecur.ValorBool
                .Item("coin_oyen") = IIf(cmbOyen.Valor = "1", True, False)  'cmbOyen.ValorBool
                .Item("coin_baja_fecha") = DBNull.Value
                .Item("_estado") = "ACTIVO"

                .Item("_insc_desc") = cmbInsc.SelectedItem.Text
            End With

            If hdnCoinId.Text = "" Then
                mdsDatos.Tables(mstrComiInsc).Rows.Add(ldrDatos)
            End If
        End Sub

        Private Sub mGuardarDatosDias()
            Dim ldrDatos As DataRow

            If txtFecha.Fecha.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar una fecha.")
            End If

            If txtDiaHs.Valor.Length = 0 Then ' Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar las horas.")
            End If

            If hdnCodcId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrComiDias).NewRow
                ldrDatos.Item("codc_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrComiDias), "codc_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrComiDias).Select("codc_id=" & hdnCodcId.Text)(0)
            End If

            With ldrDatos
                .Item("codc_fecha") = clsWeb.gSumarFechas(txtFecha.Fecha, txtDiaHora.Valor)
                .Item("codc_prac") = CBool(cmbDiasPrac.Valor)
                .Item("codc_hs") = txtDiaHs.Valor

                .Item("_prac") = cmbDiasPrac.SelectedItem.Text
            End With

            If hdnCodcId.Text = "" Then
                mdsDatos.Tables(mstrComiDias).Rows.Add(ldrDatos)
            End If
        End Sub

        Private Sub mLimpiarProf()
            hdnCoprId.Text = ""
            cmbProf.Limpiar()
            cmbPrti.Limpiar()

            mSetearEditor(mstrComiProf, True)
        End Sub

        Private Sub mLimpiarInsc()
            hdnCoinId.Text = ""
            cmbInsc.Limpiar()
            'cmbOyen.ValorBool = False
            'cmbRecur.ValorBool = False
            cmbOyen.Valor = False
            cmbRecur.Valor = False
            mSetearEditor(mstrComiInsc, True)
        End Sub

        Private Sub mLimpiarDias()
            hdnCodcId.Text = ""

            cmbDiasPrac.Limpiar()
            txtFecha.Text = ""
            txtDiaHora.Text = ""
            txtDiaHs.Text = ""

            mSetearEditor(mstrComiDias, True)
        End Sub

        Private Sub mLimpiarSema()
            hdnCoseId.Text = ""

            cmbDia.Limpiar()
            txtHora.Text = ""
            txtSemaHs.Text = ""
            cmbSemaPrac.Limpiar()

            mSetearEditor(mstrComiSema, True)
        End Sub

        Private Sub btnLimpInsc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpInsc.Click
            mLimpiarInsc()
        End Sub

        Private Sub btnLimpSema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpSema.Click
            mLimpiarSema()
        End Sub

        Private Sub btnLimpProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpProf.Click
            mLimpiarProf()
        End Sub

        Private Sub btnLimpDias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDias.Click
            mLimpiarDias()
        End Sub

        Private Sub btnBajaProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaProf.Click
            Try
                mdsDatos.Tables(mstrComiProf).Select("copr_id=" & hdnCoprId.Text)(0).Delete()
                mConsultarProf()
                mLimpiarProf()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnBajaInsc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaInsc.Click
            Try
                mdsDatos.Tables(mstrComiInsc).Select("coin_id=" & hdnCoinId.Text)(0).Delete()
                mConsultarInsc()

                mSetearCiclos()

                mLimpiarInsc()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnBajaSema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaSema.Click
            Try
                mdsDatos.Tables(mstrComiSema).Select("cose_id=" & hdnCoseId.Text)(0).Delete()
                mConsultarSema()
                mLimpiarSema()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnBajaDias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDias.Click
            Try
                mdsDatos.Tables(mstrComiDias).Select("codc_id=" & hdnCodcId.Text)(0).Delete()
                mConsultarDias()

                mSetearGenerarDias()

                mLimpiarDias()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnBajaDiasTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDiasTodos.Click
            Try
                mdsDatos.Tables(mstrComiDias).Rows.Clear()

                grdDias.CurrentPageIndex = 0
                mConsultarDias()

                mSetearGenerarDias()

                mLimpiarDias()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnAltaProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaProf.Click
            mActualizarProf()
        End Sub

        Private Sub btnAltaSema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaSema.Click
            mActualizarSema()
        End Sub

        Private Sub btnAltaInsc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaInsc.Click
            mActualizarInsc()
        End Sub

        Private Sub btnAltaDias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDias.Click
            mActualizarDias()
        End Sub

        Private Sub btnModiProf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiProf.Click
            mActualizarProf()
        End Sub

        Private Sub btnModiInsc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiInsc.Click
            mActualizarInsc()
        End Sub

        Private Sub btnModiSema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiSema.Click
            mActualizarSema()
        End Sub

        Private Sub btnModiDias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDias.Click
            mActualizarDias()
        End Sub

        Private Sub grdInsc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdInsc.ItemDataBound
            Try
                If e.Item.ItemIndex <> -1 Then
                    With CType(e.Item.DataItem, DataRowView).Row
                        e.Item.Cells(ColumnasInsc.Oyen).Text = IIf(Not .IsNull("coin_oyen") AndAlso .Item("coin_oyen"), "SI", "NO")
                        e.Item.Cells(ColumnasInsc.Recur).Text = IIf(Not .IsNull("coin_recur") AndAlso .Item("coin_recur"), "SI", "NO")
                    End With
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnGeneDias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGeneDias.Click
            Dim lstrCmd As New StringBuilder
            Dim lstrSema As New StringBuilder
            Dim ds As DataSet
            Dim ldrDatos As DataRow

            Try
                If cmbCicl.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el ciclo.")
                End If

                If mdsDatos.Tables(mstrComiSema).Select().GetUpperBound(0) = -1 Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar al menos 1 horario.")
                End If

                If txtInicFecha.Fecha Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de inicio.")
                End If

                If txtFinaFecha.Fecha Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de finalizaci�n.")
                End If

                For Each ldrSema As DataRow In mdsDatos.Tables(mstrComiSema).Select()
                    With ldrSema
                        lstrSema.Append("|")
                        lstrSema.Append(.Item("cose_dia"))
                        lstrSema.Append("!")
                        lstrSema.Append(CDate(.Item("cose_hora")).ToString("HH!mm"))
                        lstrSema.Append("!")
                        lstrSema.Append(.Item("cose_hs"))
                        lstrSema.Append("!")
                        lstrSema.Append(Math.Abs(CInt(.Item("cose_prac"))))
                        lstrSema.Append("!")
                        lstrSema.Append(CDate(.Item("cose_hora")).ToString("HH!mm"))
                    End With
                Next
                lstrSema.Append("|")

                lstrCmd.Append("exec comisiones_dias_generar ")
                lstrCmd.Append(clsSQLServer.gFormatArg(txtInicFecha.Text, SqlDbType.SmallDateTime))
                lstrCmd.Append(",")
                lstrCmd.Append(clsSQLServer.gFormatArg(txtFinaFecha.Text, SqlDbType.SmallDateTime))
                lstrCmd.Append(", @fechas=")
                lstrCmd.Append(clsSQLServer.gFormatArg(lstrSema.ToString, SqlDbType.VarChar))

                ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

                For Each ldrTemp As DataRow In ds.Tables(0).Rows
                    ldrDatos = mdsDatos.Tables(mstrComiDias).NewRow
                    ldrDatos.Item("codc_id") = clsSQLServer.gObtenerId(ldrDatos.Table, "codc_id")

                    With ldrDatos
                        .Item("codc_fecha") = ldrTemp.Item("fecha")
                        .Item("codc_prac") = ldrTemp.Item("prac")
                        .Item("codc_hs") = ldrTemp.Item("hs")

                        .Item("_prac") = cmbDiasPrac.Items.FindByValue(Math.Abs(CInt(.Item("codc_prac")))).Text
                    End With

                    If hdnCodcId.Text = "" Then
                        ldrDatos.Table.Rows.Add(ldrDatos)
                    End If
                Next

                mConsultarDias()

                mSetearGenerarDias()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
            Try
                If (hdnDatosPop.Text <> "") Then
                    Dim lvrDatos() As String = hdnDatosPop.Text.Split(",")
                    Dim ldrDatos As DataRow

                    For i As Integer = 0 To lvrDatos.GetUpperBound(0)
                        ldrDatos = mdsDatos.Tables(mstrComiInsc).NewRow
                        ldrDatos.Item("coin_id") = clsSQLServer.gObtenerId(ldrDatos.Table, "coin_id")

                        With ldrDatos
                            .Item("coin_insc_id") = lvrDatos(i)
                            .Item("coin_recur") = False
                            .Item("coin_oyen") = False
                            .Item("coin_baja_fecha") = DBNull.Value
                            .Item("_insc_desc") = cmbInsc.Items.FindByValue(.Item("coin_insc_id")).Text
                            .Item("_estado") = "ACTIVO"
                        End With

                        ldrDatos.Table.Rows.Add(ldrDatos)
                    Next

                    hdnDatosPop.Text = ""
                    mConsultarInsc()
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mConsultarDias()
            mdsDatos.Tables(mstrComiDias).DefaultView.Sort = "codc_fecha"
            grdDias.DataSource = mdsDatos.Tables(mstrComiDias).DefaultView
            grdDias.DataBind()
        End Sub

        Public Sub mConsultarSema()
            grdSema.DataSource = mdsDatos.Tables(mstrComiSema)
            grdSema.DataBind()
        End Sub

        Public Sub mConsultarProf()
            grdProf.DataSource = mdsDatos.Tables(mstrComiProf)
            grdProf.DataBind()
        End Sub

        Public Sub mConsultarInsc()
            grdInsc.DataSource = mdsDatos.Tables(mstrComiInsc)
            grdInsc.DataBind()
        End Sub
#End Region

        Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
            Try
                grdDato.CurrentPageIndex = 0
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            Try
                mAgregar()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
    End Class
End Namespace
