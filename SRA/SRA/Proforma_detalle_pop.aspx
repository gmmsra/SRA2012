<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Proforma_detalle_pop" CodeFile="Proforma_detalle_pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Detalle de anulación de proforma </title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		
		
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right"><asp:imagebutton id="btnCerrar" runat="server" ImageUrl="imagenes/close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton>&nbsp;
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panOtrosDatos" runat="server" height="100%" BorderStyle="Solid" BorderWidth="1px"
										Width="100%" cssclass="titulo" Visible="true">
										<TABLE class="FdoFld" id="tblOtrosDatos" style="WIDTH: 100%" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 100%" align="center" colSpan="4">
													<asp:label id="lbltitulo" runat="server" cssclass="titulo"></asp:label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" align="center" colSpan="4">
													<asp:datagrid id="grdConsulta" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
														ItemStyle-Height="5px" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
														CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
														<FooterStyle CssClass="footer"></FooterStyle>
														<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
														<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
														<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
														<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
														<HeaderStyle Font-Size="X-Small" CssClass="header"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="item" HeaderText="Arancel/Concepto">
																<HeaderStyle Width="40%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn Visible="false" DataField="pran_inic_rp" HeaderText="RP i"></asp:BoundColumn>
															<asp:BoundColumn Visible="false" DataField="pran_fina_rp" HeaderText="RP f"></asp:BoundColumn>
															<asp:BoundColumn Visible="true" DataField="_rp" HeaderText="Identificación"></asp:BoundColumn>
															<asp:BoundColumn DataField="ccos_codi" HeaderText="C.Costo">
																<HeaderStyle Width="7%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="pran_tram" HeaderText="Tramite">
																<HeaderStyle HorizontalAlign="Right" Width="5%"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="pran_cant" HeaderText="Cant.">
																<HeaderStyle HorizontalAlign="Right" Width="5%"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="pran_anim_no_fact" HeaderText="Anim No fact. "></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="pran_unit_impo" HeaderText="Precio Unit"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="pran_fecha" HeaderText="Fecha"></asp:BoundColumn>
															<asp:BoundColumn DataField="pran_sin_carg" HeaderText="S/cargo">
																<HeaderStyle Width="10%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="pran_exen" HeaderText="Exento">
																<HeaderStyle Width="5%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="pran_impo_ivai" HeaderText="Imp.Neto($)">
																<HeaderStyle HorizontalAlign="Right" Width="13%"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="pran_acta" HeaderText="Acta"></asp:BoundColumn>
															<asp:BoundColumn DataField="_estado" HeaderText="Estado">
																<HeaderStyle Width="10%"></HeaderStyle>
															</asp:BoundColumn>
														</Columns>
														<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD align="right" colSpan="2">
													<CC1:BotonImagen id="btnList" runat="server" ImageUrl="imagenes/btnImpr.jpg" BorderStyle="None" IncludesUrl="../includes/"
														ImagesUrl="imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
														ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
