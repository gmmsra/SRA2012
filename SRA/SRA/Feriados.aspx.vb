

Namespace SRA

Partial Class feriados
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region


#Region "Definici�n de Variables"

   Public mstrCmd As String
   Public mstrTabla As String = "feriados"
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mConsultar()
            mCargarCombos()
            'mSetearMaxLength()
            mSetearEventos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "T")
      clsWeb.gCargarComboBool(cmbRecu, "S")

   End Sub

   Private Sub mEstablecerPerfil()
      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Usuarios, String), (mstrConn), (Session("sUserId").ToString()))) Then

         Response.Redirect("noaccess.aspx")
      End If
      btnAlta.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      btnLimp.Visible = (btnAlta.Visible Or btnBaja.Visible Or btnModi.Visible)
   End Sub

   
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

      Try

         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then

            grdDato.CurrentPageIndex = 0
         Else

            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()

      Try

         mstrCmd = "exec " + mstrTabla + "_consul "
         clsWeb.gCargarDataGrid((mstrConn), mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = E.Item.Cells(1).Text
      mCargarDatos()
      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtFecha.Text = ""
      cmbActi.Limpiar()
      cmbRecu.Limpiar()
      mSetearEditor(True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      mEstablecerPerfil()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Alta()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)

         If (txtFecha.Text = "") Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha.")
         End If
         If (cmbActi.SelectedItem.Text = "(Seleccione)") Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la actividad.")
         End If
         If (cmbRecu.SelectedItem.Text = "(Sel)") Then
            Throw New AccesoBD.clsErrNeg("Debe especificar si es recurrente o no.")
         End If

         .Item("feri_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("feri_fecha") = txtFecha.Fecha
                If cmbActi.Valor.Length > 0 Then
                    .Item("feri_acti_id") = cmbActi.Valor
                End If
                Dim rec As Boolean
                If cmbRecu.SelectedItem.Text = "Si" Then
                    rec = True
                Else
                    rec = False
                End If
                .Item("feri_recu") = rec
            End With

      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Function mCargarDatos() As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
                txtFecha.Fecha = .Item("feri_fecha")
                If .Item("feri_acti_id").ToString.Length > 0 Then
                    cmbActi.Valor = .Item("feri_acti_id")
                End If

                cmbRecu.ValorBool = .Item("feri_recu")
            End With

      Return ldsEsta
   End Function
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
    mAgregar()
End Sub
End Class
End Namespace
