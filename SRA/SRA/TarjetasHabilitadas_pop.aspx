<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TarjetasHabilitadas_pop" CodeFile="TarjetasHabilitadas_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
		function mVerTarjetaInfo(pTaclId)
		{
			gAbrirVentanas("Tarjeta_info_pop.aspx?tabla=tarjetas_clientes_exten&filtros=" + pTaclId, 1,750, 300)
		}
		function ValidaControl(pHab,pId)
		{
			if (pHab == "1") 
				document.all["divImagen"+pId].style.display="inline";
			else 
				document.all["divImagen"+pId].style.display="none";
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" border="0">
								<TBODY>
									<TR>
										<TD vAlign="middle" align="right"><asp:imagebutton id="Imagebutton1" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes/close3.bmp"></asp:imagebutton>&nbsp;
										</TD>
									</TR>
									<TR>
										<TD vAlign="middle" align="center" colSpan="2"><asp:panel id="panEspeciales" runat="server" Visible="true" cssclass="titulo" Width="100%"
												BorderWidth="1px" BorderStyle="Solid" height="100%">
												<TABLE class="FdoFld" id="tblEspeciales" style="WIDTH: 100%" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD style="WIDTH: 25%" align="right">
															<asp:label id="lblNomb" runat="server" cssclass="titulo">Nombre y apellido:</asp:label>&nbsp;
														</TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtNomb" runat="server" Width="100%" cssclass="cuadrotexto" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 25%" align="right">
															<asp:label id="lblDoc" runat="server" cssclass="titulo">Tipo y nro. de doc.:</asp:label>&nbsp;
														</TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtDoc" runat="server" Width="250px" cssclass="cuadrotexto" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR height="100%">
										<TD vAlign="top"><asp:datagrid id="grdConsulta" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" ItemStyle-Height="5px" PageSize="15" OnPageIndexChanged="DataGrid_Page" CellPadding="1"
												GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="id" HeaderText="id">
														<HeaderStyle Width="5%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="tarj_desc" HeaderText="Tarjeta">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="tacl_nume" HeaderText="N&#250;mero">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="tacl_vcto_fecha" HeaderText="Fech. Venc." DataFormatString="{0:dd/MM/yyyy}">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="DA_Soci" HeaderText="DA Soci">
														<HeaderStyle Width="5%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="DA_Alum" HeaderText="DA Alum">
														<HeaderStyle Width="5%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Vta_Tel" HeaderText="Vta Tel">
														<HeaderStyle Width="5%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:TemplateColumn>
														<HeaderStyle Width="1%"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<div style="DISPLAY: none" id="divImagen<%#DataBinder.Eval(Container, "DataItem.id")%>">
																<A href="javascript:mVerTarjetaInfo(<%#DataBinder.Eval(Container, "DataItem.id")%>);">
																	<img onload="javascript:ValidaControl(1,<%#DataBinder.Eval(Container, "DataItem.id")%>);" src='imagenes/Buscar16.gif' style="cursor:hand; VISIBILITY: " border="0" alt="Tarjeta Info" />
																</A>
															</div>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
								</TBODY>
							</TABLE>
							<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnOK" runat="server"></ASP:TEXTBOX></DIV>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
