Namespace SRA

Partial Class SociDeudaDA_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
   Protected WithEvents txtTotal As NixorControls.NumberBox
   Protected WithEvents btnCerrar As System.Web.UI.WebControls.Button 
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label
   Protected WithEvents Label2 As System.Web.UI.WebControls.Label
   Protected WithEvents lblTotalCuota As System.Web.UI.WebControls.Label
   Protected WithEvents lblTotalDtos As System.Web.UI.WebControls.Label
   Protected WithEvents txtTotalDtos As NixorControls.NumberBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrTablaDeuSoc As String = "cobran_deusoc"
   Private mstrOrigen As String
   Private mstrSoloVencidas As String
   Private mstrSoloDevengadas As String

   Private mchrSepa As Char = ";"c

   Private Enum Columnas As Integer
      Chk = 0
      CompId = 1
      CheckIni = 2
      Anio = 3
      Perio = 4
      PetiId = 5
      InstId = 6
      Desc = 7
      ImpoCuota = 9
      ImpoCuotaDA = 10
      ImpoCuotaProm = 11
      ImpoInteres = 12
      ImpoDescuen = 13
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mInicializar()

            mConsultar()

            mChequearTodos()

            mCalcularTotales()

            mSetearDA()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mInicializar()
      If Request("anio") Is Nothing And Request("sel") Is Nothing Then
         btnAceptar.Visible = False
      End If

      mstrOrigen = Request("origen")
      Session("pSele") = Request("psele")

      Select Case mstrOrigen
         Case "Cobranzas"
            txtFecha.Enabled = False
      End Select

      If Request("sel") Is Nothing Then
         usrSoci.Visible = False
         lblSoci.Visible = False
      End If

      usrSoci.Valor = Request("soci_id")
      mstrSoloVencidas = Request("venc")
      mstrSoloDevengadas = Request("dev")
      If mstrSoloVencidas = "" Then mstrSoloVencidas = "0"
      If Request("fecha") Is Nothing Then
                txtFecha.Fecha = Today
            Else
         txtFecha.Text = Request("fecha")
      End If
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Private Sub mGuardarTodosIds()
      Dim lstrId As New System.Text.StringBuilder
      mLimpiarSession()

      For i As Integer = 0 To grdConsulta.PageCount - 1
         lstrId.Length = 0
         grdConsulta.CurrentPageIndex = i
         grdConsulta.DataBind()

         Session("mulPag" & i.ToString) = mGuardarDatos(True)
      Next

      grdConsulta.CurrentPageIndex = 0
      grdConsulta.DataBind()
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         Dim lstrId As String

         lstrId = mGuardarDatos()

         Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) = lstrId.ToString

         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar()

         mChequearGrilla()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Dim lstrCmd As New StringBuilder
      Dim ds As DataSet
      Dim ldsDatos As DataSet
      Dim lDsDescu As DataSet
      Dim lstrFecha As String
      Dim lDr As DataRow

      If usrSoci.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el socio.")
      End If
      If txtFecha Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la fecha.")
      End If

      lstrCmd.Append("exec rpt_calculo_deuda_consul")
      lstrCmd.Append(" @soci_id=")
      lstrCmd.Append(usrSoci.Valor)
      lstrCmd.Append(", @fecha=")
      lstrCmd.Append(clsFormatear.gFormatFecha2DB(txtFecha.Fecha))

      If Not Request("anio") Is Nothing Then
         lstrCmd.Append(", @anio=")
         lstrCmd.Append(Request("anio"))
         lstrCmd.Append(", @perio=")
         lstrCmd.Append(Request("perio"))
      End If

      lstrCmd.Append(", @consulta=1")
      lstrCmd.Append(", @SoloVencidas=")
      lstrCmd.Append(clsSQLServer.gFormatArg(mstrSoloVencidas, SqlDbType.VarChar))
      lstrCmd.Append(", @SoloDevengadas =")
      lstrCmd.Append(clsSQLServer.gFormatArg(mstrSoloDevengadas, SqlDbType.VarChar))

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      If ds.Tables(0).Rows.Count > 0 Then
         With ds.Tables(0).Select("", "anio desc,perio desc")(0)
            lDsDescu = SRA_Neg.Comprobantes.gObtenerDescuentoSocioPagoAdel(mstrConn, usrSoci.Valor, .Item("anio"), .Item("perio"), .Item("peti_id"), "0" & .Item("inst_id").ToString, txtFecha.Fecha)

            If lDsDescu.Tables(0).Rows.Count > 0 Then
               .Item("descuento") = lDsDescu.Tables(0).Rows(0).Item("cucd_valor")
            End If
         End With
      End If

      For Each lDr In ds.Tables(0).Rows
          lstrFecha = lDr.Item("inte_fecha")
      Next

      If Not lstrFecha Is Nothing Then
          grdConsulta.Columns(Columnas.ImpoInteres).HeaderText = "Interes al " & lstrFecha
      End If

      grdConsulta.DataSource = ds
      grdConsulta.DataBind()
      ds.Dispose()

      Session("mulPaginas") = grdConsulta.PageCount
   End Sub
#End Region

   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Dim lstrId As New StringBuilder
      Dim lintSem As Integer = 0
      Dim ldecDescuen As Decimal = 0
      Dim lstrClieAnt, lstrBuqeAnt As String
      Try
         lstrId.Append(mGuardarDatos)

            For Each oDataItem As DataGridItem In grdConsulta.Items
                If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
                    ldecDescuen = ldecDescuen + oDataItem.Cells(Columnas.ImpoDescuen).Text
                    If oDataItem.Cells(Columnas.Desc).Text.ToUpper().IndexOf("SEM") <> -1 Then
                        lintSem = lintSem + 1
                    End If
                End If
            Next

            If lintSem Mod 2 <> 0 And Page.IsPostBack Then
                Throw New AccesoBD.clsErrNeg("Debe indicar a�os completos para socios menores.")
            End If

            For i As Integer = 0 To Session("mulPaginas")
                If Not Session("mulPag" & i.ToString) Is Nothing Then
                    Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
                    If i <> grdConsulta.CurrentPageIndex And lstrIdsSess <> "" Then
                        If lstrId.Length > 0 Then lstrId.Append(Chr(5))
                        lstrId.Append(lstrIdsSess)
                    End If
                    Session("mulPag" & i.ToString) = Nothing
                End If
            Next

            grdConsulta.DataSource = Nothing
            grdConsulta.DataBind()

            If lstrId.Length = 0 Then
                lstrId.Append("NADA")
            End If

            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("if(window.opener.document.all['hdnSociDatosPop']!=null) window.opener.document.all['hdnSociDatosPop'].value='{0}';", usrSoci.Valor.ToString))
            lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", lstrId.ToString))
            lsbMsg.Append(String.Format("window.opener.document.all('hdnDescuen').value='{0}';", ldecDescuen.ToString()))
            lsbMsg.Append(String.Format("window.opener.document.all('txtImpoOrig').value='{0}';", txtTotalTotal.Text))
            'lsbMsg.Append(String.Format("window.opener.document.all('txtImpo').value='{0}';", txtTotalDA.Text))
            lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)

        Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   'Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click

   'End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      Response.Write("<Script>window.close();</script>")
   End Sub
   Private Function mGuardarDatos(Optional ByVal pbooTodos As Boolean = False) As String
      Dim lstrId As New StringBuilder

      For Each oDataItem As DataGridItem In grdConsulta.Items
         If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Or pbooTodos Then
            If lstrId.Length > 0 Then lstrId.Append(Chr(5))
            lstrId.Append(oDataItem.Cells(Columnas.CompId).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.Anio).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.Perio).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.PetiId).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.InstId).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.ImpoCuota).Text.Replace(",", "."))  'estaba grabando mal!
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.ImpoInteres).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(usrSoci.Valor)
            lstrId.Append(Chr(6))
            lstrId.Append(clsFormatear.gFormatFechaString(txtFecha.Text, "Int32"))
         End If
      Next

      Return (lstrId.ToString.Replace("&nbsp;", ""))
   End Function

   Private Sub mChequearGrilla()
      Dim lstrIdSess As String
      If Not (Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) Is Nothing) Then
         lstrIdSess = Session("mulPag" & grdConsulta.CurrentPageIndex.ToString)
      End If

      For Each oDataItem As DataGridItem In grdConsulta.Items
         CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = (Chr(5) & lstrIdSess & Chr(5)).IndexOf(Chr(5) & oDataItem.Cells(Columnas.CompId).Text.Replace("&nbsp;", "") & Chr(6) & oDataItem.Cells(Columnas.Anio).Text & Chr(6) & oDataItem.Cells(Columnas.Perio).Text) <> -1
      Next
   End Sub

   Private Sub mCalcularTotales()
      Dim dTotalCuot As Decimal = 0
      Dim dTotalCuotDA As Decimal = 0
      Dim dTotalInte As Decimal = 0
      Dim dTotalDesc As Decimal = 0
      Dim lstrFecha As String
      Dim lDr As DataRow

      For Each lDr In DirectCast(grdConsulta.DataSource, DataSet).Tables(0).Rows
         dTotalCuot += lDr.Item("cuota")
         dTotalCuotDA += lDr.Item("cuota_da")
         If Not lDr.IsNull("interes") Then dTotalInte += lDr.Item("interes")
         dTotalDesc += lDr.Item("descuento")
         lstrFecha = lDr.Item("inte_fecha")
      Next

        txtTotalTotal.Valor = (dTotalCuot + dTotalInte + dTotalDesc).ToString("######0.00")
        txtTotalCuot.Valor = dTotalCuot.ToString("######0.00")
        txtTotalDA.Valor = (dTotalCuotDA + dTotalInte).ToString("######0.00")
        If Not lstrFecha Is Nothing Then
            lblTotalInte.Text = "Interes al:" + CDate(lstrFecha).ToString("dd/MM/yyyy")
            grdConsulta.Columns(Columnas.ImpoInteres).HeaderText = lblTotalInte.Text
        End If

        txtTotalInte.Valor = dTotalInte.ToString("######0.00")
        txtTotalDesc.Valor = dTotalDesc.ToString("######0.00")

        Dim lstrCmd As String
        Dim ds As DataSet

        lstrCmd = "exec clientes_estado_de_saldos_consul"
        lstrCmd += " @clie_id = " + clsSQLServer.gFormatArg(usrSoci.ClieId(mstrConn), SqlDbType.Int)
        lstrCmd += ",@fecha = " + clsSQLServer.gFormatArg(txtFecha.Text, SqlDbType.SmallDateTime)
        lstrCmd += ",@solo_ctacte = 1"

        ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

        With ds.Tables(0).Rows(0)
            txtTotalCC.Valor = .Item("saldo_cta_cte")
            txtTotalVencCC.Valor = .Item("saldo_cta_cte_venc")
            txtSaldoCtaCC.Valor = .Item("imp_a_cta_cta_cte")
            txtSaldoCtaCS.Valor = .Item("imp_a_cta_cta_social")
        End With
   End Sub

   Private Sub mEstadoSaldos(ByVal pintClie As Integer, ByVal pFecha As Date)

   End Sub

   Private Sub mLimpiarSession()
      Dim k As Integer

      While Not Session("mulPag" & k.ToString) Is Nothing
         Session("mulPag" & k.ToString) = Nothing
         k += 1
      End While
   End Sub

   Private Sub mChequearTodos()
      mGuardarTodosIds()

      mChequearGrilla()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "CalculoDeudaSocio"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         Dim lstrFecha As String
         Dim lintFecha As Integer
         Dim lintPerio, lintAnio As Integer

         Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) = mGuardarDatos()

         mObtenerUltPerio(lintAnio, lintPerio)

         lstrRpt += "&soci_id=" + usrSoci.Valor.ToString

         lstrFecha = txtFecha.Text
         lintFecha = CType(clsFormatear.gFormatFechaString(lstrFecha, "Int32"), Integer)

         lstrRpt += "&fecha=" & lintFecha.ToString
         lstrRpt += "&anio=" & lintAnio.ToString
         lstrRpt += "&perio=" & lintPerio.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mObtenerUltPerio(ByRef pintAnio As Integer, ByRef pintPerio As Integer)
      Dim lintPerio, lintAnio As Integer
      Dim lvrDatos, lvrDato As String()
      Dim lstrFiltro As String

      For j As Integer = 0 To Session("mulPaginas")
         If Not Session("mulPag" & j.ToString) Is Nothing Then
            Dim lstrIdsSess As String = Session("mulPag" & j.ToString)
            lvrDatos = lstrIdsSess.Split(Chr(5))

            Dim ldrDeta As DataRow
            Dim lstrClieId As String
            Dim ldsDeuda As DataSet
            Dim ldecTotDev As Decimal

            If Not lvrDatos Is Nothing Then
                For i As Integer = 0 To lvrDatos.GetUpperBound(0)
                    lvrDato = lvrDatos(i).Split(Chr(6))   'CompId(6)Anio(6)Perio(6)PetiId(6)InstId(6)ImpoCuota(6)ImpoInteres(6)SociId(6)Fecha(5)...
                    If lvrDato(0) <> "" Then
                        lintAnio = lvrDato(1)
                        lintPerio = lvrDato(2)
                    End If
                Next
            End If
         End If
      Next

      pintPerio = lintPerio
      pintAnio = lintAnio
   End Sub

   Private Function mEnMemoria(ByVal pstrSociId As String, ByVal pstrAnio As String, ByVal pstrPerio As String) As Boolean
      If Not Request("sess") Is Nothing Then
         Dim ldsDatos As DataSet = Session(Request("sess"))
         Dim ldtDatos As DataTable

         ldtDatos = ldsDatos.Tables(mstrTablaDeuSoc)
         If Not ldtDatos Is Nothing Then
            Return (ldtDatos.Select("cods_soci_id=" & pstrSociId & " AND cods_anio=" & pstrAnio.ToString & " AND cods_perio=" & pstrPerio).GetUpperBound(0) <> -1)
         End If
      End If
   End Function

   Private Sub usrSoci_Cambio(ByVal sender As Object) Handles usrSoci.Cambio
      Try
         mConsultar()
         mChequearTodos()
         mCalcularTotales()
         mSetearDA()
         If Not usrSoci.Valor Is DBNull.Value And Not Session("pSele") Is Nothing Then
            If Session("pSele") <> "" Then
                For Each oDataItem As DataGridItem In grdConsulta.Items
                    CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = (Chr(6) & Session("pSele") & Chr(6)).IndexOf(Chr(6) & "CS" & usrSoci.Valor.ToString() & Chr(6) & oDataItem.Cells(Columnas.Anio).Text & Chr(6) & oDataItem.Cells(Columnas.Perio).Text & Chr(6)) <> -1
                Next
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub txtFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFecha.TextChanged
      Try
         mConsultar()
         mChequearTodos()
         mCalcularTotales()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearDA()
      If mstrOrigen = "Cobranzas" Then
         Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Socios, usrSoci.Valor.ToString)

         With ldsDatos.Tables(0).Rows(0)
                If .Item("_tacl_id").ToString <> "" Then
                    Dim boolCargaCBU = False
                    ' controlo que fue seleccionad una tarjeta, para realizar la consulta para
                    ' ver si carga CBU o nro de cuenta
                    If (Not .Item("_tacl_tarj_id") Is DBNull.Value) Then
                        boolCargaCBU = clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_consul @tarj_id=" & .Item("_tacl_tarj_id").ToString, "tarj_PermiteDebitoEnCuenta")
                    End If

                    Dim lstrText As New System.Text.StringBuilder
                    lstrText.Append("El socio tiene D�bito Autom�tico. ")
                    lstrText.Append("Tarjeta: ")
                    lstrText.Append(.Item("_tacl_tarj").ToString)
                    ' Dario 2013-10-02
                    'lstrText.Append("  Nro.: ")
                    Dim texto = IIf(boolCargaCBU, "  CBU.: ", "  Nro.: ")
                    lstrText.Append(texto)
                    ' fin 2013-10-02
                    lstrText.Append(.Item("_tacl_nume").ToString)
                    If Not .IsNull("_tacl_vcto_fecha") Then
                        lstrText.Append(" Fecha Vto.: ")
                        lstrText.Append(CDate(.Item("_tacl_vcto_fecha")).ToString("dd/MM/yyyy"))
                    End If

                    clsError.gGenerarMensajes(Me, lstrText.ToString)
                End If
            End With
      End If
   End Sub
End Class
End Namespace
