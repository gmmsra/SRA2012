Imports SRA

Public Class ProformaCerrar_pop
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblExentaT As System.Web.UI.WebControls.Label
    'Protected WithEvents lblExenta As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPercepT As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPercep As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPrecSociT As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPrecSoci As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMiemCDT As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMiemCD As System.Web.UI.WebControls.Label
    Protected WithEvents panEspeciales As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblSaldoCaCte As System.Web.UI.WebControls.Label

    'Protected WithEvents lblSaldoCtaSoci As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTotalSaldo As System.Web.UI.WebControls.Label
    'Protected WithEvents txtTotalSaldo As NixorControls.NumberBox
    'Protected WithEvents lblImpCuentaCtaCte As System.Web.UI.WebControls.Label
    'Protected WithEvents txtImpCuentaCtaCte As NixorControls.NumberBox
    'Protected WithEvents lblLimiteSaldo As System.Web.UI.WebControls.Label
    'Protected WithEvents txtLimiteSaldo As NixorControls.TextBoxTab
    'Protected WithEvents lblImpCuentaCtaSocial As System.Web.UI.WebControls.Label
    'Protected WithEvents txtImpCuentaCtaSocial As NixorControls.NumberBox
    Protected WithEvents panEstadoSaldos As System.Web.UI.WebControls.Panel
    'Protected WithEvents btnCtaCte As System.Web.UI.WebControls.Button
    'Protected WithEvents lblCuitT As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroSocio As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCondIVAT As System.Web.UI.WebControls.Label
    'Protected WithEvents lblestado As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCatego As System.Web.UI.WebControls.Label
    'Protected WithEvents panOtrosDatos As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblISEAT As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCEIDAT As System.Web.UI.WebControls.Label
    'Protected WithEvents grdConsulta As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnGenerarCuot As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents btnCerrar As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    'Protected WithEvents txtNroSocio As NixorControls.NumberBox
    'Protected WithEvents txtestado As NixorControls.TextBoxTab
    'Protected WithEvents txtCatego As NixorControls.TextBoxTab
    'Protected WithEvents txtcuit As NixorControls.TextBoxTab
    'Protected WithEvents txtISEA As NixorControls.NumberBox
    'Protected WithEvents txtCEIDA As NixorControls.NumberBox
    'Protected WithEvents txtCondIVA As NixorControls.TextBoxTab
    'Protected WithEvents txtExenta As NixorControls.TextBoxTab
    'Protected WithEvents txtPercep As NixorControls.TextBoxTab
    'Protected WithEvents txtPrecSoci As NixorControls.TextBoxTab
    'Protected WithEvents txtMiemCD As NixorControls.TextBoxTab
    'Protected WithEvents hdnClieId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnFecha As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnTipoPop As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblObser As System.Web.UI.WebControls.Label
    'Protected WithEvents txtObser As NixorControls.TextBoxTab
    'Protected WithEvents btnDeudaSocial As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents btnDeudaSocialVda As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents btnACtaCtaCte As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents txtSaldoCaCteVenc As NixorControls.NumberBox
    'Protected WithEvents btnDeudaCteVen As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents txtSaldoCaCte As NixorControls.NumberBox
    'Protected WithEvents btnDeudaCte As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtSaldoCtaSoci As NixorControls.NumberBox
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtSaldoCtaSociVenc As NixorControls.NumberBox
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    ' Protected WithEvents btnAlta As System.Web.UI.WebControls.Button
    ' Protected WithEvents btnBaja As System.Web.UI.WebControls.Button

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Public mstrTitulo As String
    Private mstrCompId As String
    Private mstrProf As String
    Private mstrTitu As String
    Private mbooEsConsul As Boolean
    Private mstrConn As String
    Dim mdsDatos As DataSet
    Dim mstrCmd As String


#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If Not Page.IsPostBack Then
                mConsultar(True)
            End If
        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try

    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()

        mstrTitulo = Request.QueryString("titulo")
        mstrCompId = Request.QueryString("compId")
        mstrTitu = Request.QueryString("titu")
        mstrProf = Request.QueryString("prof")
        lblTitu.Text = mstrTitu

    End Sub
#End Region

    Private Sub mMostrarPanel(ByVal pboolmostrar As Integer)
        Select Case pboolmostrar
            Case 1
                panEstadoSaldos.Visible = True
            Case 2
                panOtrosDatos.Visible = True
            Case 3
                panEspeciales.Visible = True
        End Select

    End Sub



    'Private Sub btnCtaCte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCtaCte.Click
    '    Try
    '        Dim lsbPagina As New System.Text.StringBuilder
    '        Dim pstrTitulo As String = "Cuenta Corriente"
    '        ' grilla con la actividad y el importe a cuenta --falta hacerlo  
    '        'lsbPagina.Append("consulta_pop.aspx?EsConsul=0&titulo=" & pstrTitulo & "&tabla=leyendas_factu")

    '        clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 500, 400, 9, 250)

    '    Catch ex As Exception

    '        clsError.gManejarError(Me, ex)
    '    End Try
    'End Sub
    Public Sub mConsultar(ByVal pbooPage As Boolean)
        Try
            Dim lstrfiltro As String = mstrCompId
            mstrCmd = "exec  proformas_cerradas_consul "
            If mstrProf <> "" Then lstrfiltro = lstrfiltro + "," + clsSQLServer.gFormatArg(mstrProf, SqlDbType.VarChar)
            mstrCmd += lstrfiltro


            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            If pbooPage Then
                Dim i As Integer = grdConsulta.Columns.Count - 1
                While i > 0
                    grdConsulta.Columns.Remove(grdConsulta.Columns(i))
                    i -= 1
                End While
            End If

            For Each dc As DataColumn In ds.Tables(0).Columns
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                dgCol.DataField = dc.ColumnName
                dgCol.HeaderText = dc.ColumnName
                If dc.Ordinal = 0 Then
                    dgCol.Visible = False
                End If

                grdConsulta.Columns.Add(dgCol)
            Next
            grdConsulta.DataSource = ds
            grdConsulta.DataBind()
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConsulta.EditItemIndex = -1
            If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
                grdConsulta.CurrentPageIndex = 0
            Else
                grdConsulta.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub




    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrar.Click
        Response.Write("<Script>window.close();</script>")
    End Sub


    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        Response.Write("<Script>window.close();</script>")
    End Sub

    Protected Sub grdConsulta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdConsulta.SelectedIndexChanged

    End Sub
End Class
