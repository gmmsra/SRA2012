<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TransferenciaAnimalPie" CodeFile="TransferenciaAnimalPie.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="CRIA" Src="controles/usrCriador.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>
        <%=mstrTitulo%>
    </title>
    <meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultpilontScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="includes/utiles.js"></script>
    <script language="JavaScript" src="includes/paneles.js"></script>
    <script src="INCLUDES/jquery-1.10.2.min.js"></script>
    <script src="INCLUDES/transferenciaProductos.js"></script>

    <script language="JavaScript">

        function chkReservaEstock_checked(chk) {
            document.all("txtCantidadReser").value = '';
            document.all("txtCantCriasReser").value = '';

            document.all('txtCantCriasReser').style.visibility = 'hidden';
            document.all('lblReserCrias').style.visibility = 'hidden';

            if (chk.checked) {
                document.all("txtCantidadReser").disabled = false;
                document.all("txtCantCriasReser").disabled = false;
            }
            else {
                document.all("txtCantidadReser").disabled = true;
                document.all("txtCantCriasReser").disabled = true;
            }

            if (document.all("usrProd:cmbProdSexo").selectedIndex != "0") {
                if (document.all("usrProd:cmbProdSexo").selectedIndex == "2") {
                    document.all("lblReserva").innerHTML = 'Con Reserva de Semen:&nbsp';
                    document.all('txtCantCriasReser').style.visibility = '';
                    document.all('lblReserCrias').style.visibility = '';
                }
                else {
                    document.all("lblReserva").innerHTML = 'Con Reserva de Embriones:&nbsp';
                    document.all("txtCantCriasReser").disabled = true;
                }
            }
            else {
                document.all("lblReserva").innerHTML = 'Reserva:&nbsp';
                document.all("txtCantidadReser").disabled = true;
                document.all("txtCantCriasReser").disabled = true;
                document.all('txtCantCriasReser').style.visibility = '';
                document.all('lblReserCrias').style.visibility = '';
            }
        }

        /* Dario 2015-02-25 funcion que cambia el mensaje de la seccion de reserva de
		 semen o embriones*/
        function usrProd_cmbProdSexo_change() {
            if (document.all("usrProd:cmbProdSexo").selectedIndex != "0") {
                if (document.all("usrProd:cmbProdSexo").selectedIndex == "2")
                    document.all("lblReserva").innerHTML = 'Con Reserva de Semen:&nbsp';
                else
                    document.all("lblReserva").innerHTML = 'Con Reserva de Embriones:&nbsp';
            }
            else {
                document.all("lblReserva").innerHTML = 'Reserva:&nbsp';
            }
            return false
        }

        function usrProd_usrProducto_cmbProdSexo_onchange() {
            if ('<%=mintTtraId%>' == '11' || '<%=mintTtraId%>' == '30' || '<%=mintTtraId%>' == '31' || '<%=mintTtraId%>' == '20' || '<%=mintTtraId%>' == '1') {
                if (document.all("usrProd_usrProducto_cmbProdSexo") != null && document.all('usrClieProp:txtCodi') != null) {
                    if (document.all("usrProd_usrProducto_cmbProdSexo").value == "1")
                        document.all('rowDosisCria').style.display = 'inline';
                    else
                        document.all('rowDosisCria').style.display = 'none';
                }
            }
        }

        function usrCriaComp_cmbRazaCria_onchange(pRaza) {
            if (document.all('txtusrCriaComp:cmbRazaCria').value != null) {
                document.all('usrCriaVend:cmbRazaCria').value = document.all(pRaza).value;
                document.all('usrCriaVend:cmbRazaCria').onchange();

                document.all("usrCriaVend:cmbRazaCria").disabled = true;
                document.all("txtusrCriaVend:cmbRazaCria").disabled = true;

                document.all('usrProd:usrCriadorFil:cmbRazaCria').value = document.all(pRaza).value;
                document.all('usrProd:usrCriadorFil:cmbRazaCria').onchange();

                document.all("usrProd:usrCriadorFil:cmbRazaCria").disabled = true;
                document.all("txtusrProd:usrCriadorFil:cmbRazaCria").disabled = true;
            }
        }

        function mSetearRaza(pRaza, pControl) {
            if (document.all('usrProd:usr' + pControl + ':cmbProdRaza').value != document.all(pRaza).value) {
                document.all('usrProd:usr' + pControl + ':cmbProdRaza').value = document.all(pRaza).value;
                document.all('usrProd:usr' + pControl + ':cmbProdRaza').onchange();
                document.all('usrCriaVend:cmbRazaCria').value = document.all(pRaza).value;
                document.all('usrCriaVend:cmbRazaCria').onchange();
                document.all("usrCriaVend:cmbProdRaza").disabled = true;
                document.all("usrProd:usrProducto:cmbProdRaza").disabled = true;
            }
        }

        function mCriaCopropiedadComp() {
            if (document.all('usrCriaComp:txtId').value != null && document.all('usrCriaComp:txtId').value != '')
                gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Copropiedad&tabla=criadores_por_cliente_raza&Rpt=L&filtros=" + document.all('usrCriaComp:txtId').value, 7, "900", "300", "50", "100");
            else
                alert('El comprador no tiene copropiedad');
        }

        function mCriaCopropiedadVend() {
            if (document.all('usrCriaVend:txtId').value != null && document.all('usrCriaVend:txtId').value != '')
                gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Copropiedad&tabla=criadores_por_cliente_raza&Rpt=L&filtros=" + document.all('usrCriaVend:txtId').value, 7, "900", "300", "50", "100");
            else
                alert('El vendedor no tiene copropiedad');
        }

        function usrCriaComp_onchange() {
            if (document.all('usrCriaComp:txtCodi') != null && document.all('usrClieProp:txtCodi') != null && document.all("cmbPais") != null)
                mBuscarPaisDefa('usrCriaComp');
            var lstrRaza = '';
            if (document.all('usrProd:usrProducto:cmbProdRaza') != null)
                lstrRaza = document.all('usrProd:usrProducto:cmbProdRaza').value;
            else
                lstrRaza = document.all('hdnRazaId').value;

            if (lstrRaza != '' && document.all('usrCriaComp:txtId').value != '') {
                var sFiltro = '@clie_id=' + document.all('usrCriaComp:txtId').value + ',@raza_id=' + lstrRaza;
            }
        }


        /****** CONTROL CLIENTES *******/

        function mVerErrores() {
            var lstrProc = '<%=mintProce%>';
            var lstrId = document.all("hdnDetaId").value;
            /*cuando son animales en  pie hay que pasar el trpr_id tramprodid*/
            gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce=" + lstrProc + "&id=" + lstrId, 1, "750", "550", "20", "40");
        }

        function mCargarTE() {
            gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Denuncias de TE&tabla=te_denun_tramite&hdn=hdnDatosTEPop&filtros=" + document.all('usrClieVend:txtId').value, 2, "700", "400", "100", "50");
        }

        function btnAgre_click() {
            document.all('hdnDatosPop').value = "1";
            //gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=SELECCIONE UNA PLANTILLA&tabla=rg_tramites_planXrg_tramites_tipos&filtros=<%=mintTtraId%>", 8,650,350);
		    return (false);
		}

		function btnModi_click(pstrEstaBaja) {
		    if (document.all("cmbEsta").value == pstrEstaBaja) {
		        if (!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?'))
		            return (false);
		    }
		    return (true);
		}

		function mNumeDenoConsul(pRaza, pFiltro) {
		    var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro) {
		        if (document.all('lblSraNume') != null) {
		            if (strRet != '')
		                document.all('lblSraNume').innerHTML = strRet + ':';
		            else
		                document.all('lblSraNume').innerHTML = 'Nro.:';
		        }
		    }
		    else {
		        if (document.all('lblNumeFil') != null) {
		            if (strRet != '') {
		                document.all('lblNumeFil').innerHTML = strRet + ':';
		                document.all('lblNumeDesdeFil').innerHTML = strRet + ' Desde:';
		                document.all('lblNumeHastaFil').innerHTML = strRet + ' Hasta:';
		            }
		            else {
		                document.all('lblNumeFil').innerHTML = 'Nro.:';
		                document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
		                document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
		            }
		        }
		    }
		}

		function cmbRazaFil_change() {
		    if (document.all("cmbRazaFil").value != "") {
		        document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
		        document.all("usrCriaFil:cmbRazaCria").onchange();
		        document.all("usrCriaFil:cmbRazaCria").disabled = true;
		        document.all("txtusrCriaFil:cmbRazaCria").disabled = true;

		        document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
		        document.all("usrProdFil:cmbProdRaza").onchange();
		        document.all("usrProdFil:cmbProdRaza").disabled = true;
		        document.all("txtusrProdFil:cmbProdRaza").disabled = true;

		        document.all("usrCompradorFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
		        document.all("usrCompradorFil:cmbRazaCria").onchange();
		    }
		    else {
		        document.all("usrCriaFil:cmbRazaCria").value = ""
		        document.all("usrCriaFil:cmbRazaCria").onchange();
		        document.all("usrCriaFil:cmbRazaCria").disabled = false;
		        document.all("txtusrCriaFil:cmbRazaCria").disabled = false;

		        document.all("usrProdFil:cmbProdRaza").value = "";
		        document.all("usrProdFil:cmbProdRaza").onchange();
		        document.all("usrProdFil:cmbProdRaza").disabled = false;
		        document.all("txtusrProdFil:cmbProdRaza").disabled = false;

		        document.all("usrCompradorFil:cmbRazaCria").value = ""
		        document.all("usrCompradorFil:cmbRazaCria").onchange();
		    }

		}

		function usrProd_usrProducto_txtSraNume_onchange() {

		}

		/*
        
        function mBuscarPaisDefa(pstrCon)
        {
            if (document.all(pstrCon + ':txtCodi').value != "")
                {
                    var strFiltro = "@clie_id = " + document.all(pstrCon + ':txtCodi').value;
                    var strRet = LeerCamposXML("dire_clientes", strFiltro, "_dicl_pais_id");
                    
                    strFiltro = "@pais_defa = 1";
                    var strRet2 = LeerCamposXML("paises", strFiltro, "pais_id");
                    if (strRet == strRet2)
                        document.all("cmbPais").value = "";
                    else
                        document.all("cmbPais").value = strRet;
                }
            else
                document.all("cmbPais").value = "";
        }
        */

		function expandir() {
		    if (parent.frames.length > 0) try { parent.frames("menu").CambiarExp(); } catch (e) {; }
		}
    </script>
</head>
<body class="pagina" leftmargin="5" topmargin="5" onload="gSetearTituloFrame('');" rightmargin="0"
    onunload="gCerrarVentanas();">
    <form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
        <!------------------ RECUADRO ------------------->
        <table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
            <tr>
                <td width="9">
                    <img height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recsup.jpg">
                    <img height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
                <td width="13">
                    <img height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9" background="imagenes/reciz.jpg">
                    <img height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table id="Table1" style="WIDTH: 100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="HEIGHT: 25px" valign="bottom" colspan="3" height="25">
                                    <asp:Label ID="lblTituAbm" runat="server" CssClass="opcion">Transferencia de Animal en Pie</asp:Label></td>
                            </tr>
                            <tr>
                                <td valign="middle" nowrap colspan="3">
                                    <asp:Panel ID="panFiltros" runat="server" CssClass="titulo" Width="100%" BorderWidth="1px"
                                        BorderStyle="none">
                                        <table id="Table3" border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td style="HEIGHT: 8px" width="24"></td>
                                                            <td style="HEIGHT: 8px" width="42"></td>
                                                            <td style="HEIGHT: 8px" width="26"></td>
                                                            <td style="HEIGHT: 8px"></td>
                                                            <td style="HEIGHT: 8px" width="26">
                                                                <cc1:BotonImagen ID="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
                                                                    ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></cc1:BotonImagen></td>
                                                            <td style="HEIGHT: 8px" valign="middle" width="26">
                                                                <cc1:BotonImagen ID="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
                                                                    ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></cc1:BotonImagen></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="HEIGHT: 8px" width="24">
                                                                <img border="0" src="imagenes/formfle.jpg" width="24" height="25"></td>
                                                            <td style="HEIGHT: 8px" width="42">
                                                                <img border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></td>
                                                            <td style="HEIGHT: 8px" width="26">
                                                                <img border="0" src="imagenes/formcap.jpg" width="26" height="25"></td>
                                                            <td style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colspan="3">
                                                                <img border="0" src="imagenes/formfdocap.jpg" height="25"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="HEIGHT: 50px">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
                                                        <tr>
                                                            <td background="imagenes/formiz.jpg" width="3">
                                                                <img border="0" src="imagenes/formiz.jpg" width="3" height="17"></td>
                                                            <td style="HEIGHT: 100%">
                                                                <!-- FOMULARIO -->
                                                                <table class="FdoFld" border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
                                                                    <tr>
                                                                        <td style="WIDTH: 15%" height="15" align="right">
                                                                            <asp:Label ID="lblNumeFil" runat="server" CssClass="titulo"> Nro.Tr�mite:</asp:Label>&nbsp;</td>
                                                                        <td style="WIDTH: 85%">
                                                                            <cc1:NumberBox ID="txtNumeFil" runat="server" CssClass="cuadrotexto" MaxValor="999999999" Width="140px"
                                                                                AceptaNull="False"></cc1:NumberBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" align="right">
                                                                            <asp:Label ID="lblEstadoFil" runat="server" CssClass="titulo">Estado:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:ComboBox ID="cmbEstadoFil" class="combo" runat="server" Width="200px" AceptaNull="False"
                                                                                NomOper="estados_cargar">
                                                                            </cc1:ComboBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="../imagenes/formdivmed.jpg" colspan="3" align="right">
                                                                            <img src="../imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" align="right">
                                                                            <asp:Label ID="lblFechaDesdeFil" runat="server" CssClass="titulo">Fecha Desde:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtFechaDesdeFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:Label ID="lblFechaHastaFil" runat="server" CssClass="titulo">Hasta:&nbsp;</asp:Label>
                                                                            <cc1:DateBox ID="txtFechaHastaFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" align="right">
                                                                            <asp:Label ID="lblFechaTransferenciaFil" runat="server" CssClass="titulo">Fecha Transferencia:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtFechaTransferenciaFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="WIDTH: 153px" height="15" align="right">
                                                                            <asp:Label ID="lblProdFil" runat="server" CssClass="titulo">Producto:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <uc1:PROH ID="usrProdFil" runat="server" AceptaNull="false" MuestraDesc="false" AutoPostback="False"
                                                                                IncluirDeshabilitados="true" FilTipo="S" EsPropietario="True" Ancho="800" Saltos="1,2"
                                                                                Tabla="productos"></uc1:PROH>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="HEIGHT: 10px" align="right">
                                                                            <asp:Label ID="lblCompradorFil" runat="server" CssClass="titulo">Comprador:&nbsp;</asp:Label></td>
                                                                        <td>
                                                                            <uc1:CLIE ID="usrCompradorFil" runat="server" AceptaNull="false" MuestraDesc="False" AutoPostback="False"
                                                                                FilTipo="S" Saltos="1,2" Tabla="Criadores" FilDocuNume="True" MostrarBotones="False" FilSociNume="True"
                                                                                Criador="true" CampoVal="Criador"></uc1:CLIE>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td background="imagenes/formfdofields.jpg" nowrap align="right"></td>
                                                                        <td background="imagenes/formfdofields.jpg" width="72%">
                                                                            <asp:CheckBox ID="chkVisto" Text="Incluir Registros Vistos y/o Aprobados" CssClass="titulo" runat="server"></asp:CheckBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colspan="2" align="right"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2">
                                                                <img border="0" src="imagenes/formde.jpg" width="2" height="2"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" colspan="3">
                                    <asp:DataGrid ID="grdDato" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdDato_Page"
                                        OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                        AllowPaging="True">
                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                        <FooterStyle CssClass="footer"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton5" runat="server" CausesValidation="false" CommandName="Update">
															<!--<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />--></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="tram_id"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_inic_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha Inic."></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_oper_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha Transf."></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_nume" HeaderText="N� Tr�mite" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="raza" HeaderText="Raza" HeaderStyle-Width="20%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="sexo" HeaderText="Sexo"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="prdt_nomb" HeaderText="Nombre" HeaderStyle-Width="30%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="_numero" HeaderText="N� Producto" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="tram_comp_clie_id" HeaderText="Comprador" HeaderStyle-Width="10%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="tram_vend_clie_id" HeaderText="Vendedor" HeaderStyle-Width="10%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="true" DataField="clie_apel" ReadOnly="True" HeaderText="Cmprador Apellido/Raz&#243;n Social"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="esta_desc" HeaderText="Estado"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td colspan="3" height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" colspan="3">
                                    <cc1:BotonImagen ID="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
                                        ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                        IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif"
                                        ToolTip="Agregar una nueva Transferencia"></cc1:BotonImagen></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3">
                                    <asp:Panel ID="panLinks" runat="server" CssClass="titulo" Width="100%" Height="124%" Visible="False">
                                        <table id="TablaSolapas" border="0" cellspacing="0" cellpadding="0" width="100%" runat="server">
                                            <tr>
                                                <td style="WIDTH: 0.24%">
                                                    <img border="0" src="imagenes/tab_a.bmp" width="9" height="27"></td>
                                                <td background="imagenes/tab_b.bmp">
                                                    <img border="0" src="imagenes/tab_b.bmp" width="8" height="27"></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_c.bmp" width="31" height="27"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkCabecera" runat="server"
                                                        CssClass="solapa" Width="80px" Height="21px" CausesValidation="False"> Tr�mite </asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkRequ" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Requisitos</asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkDocu" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Documentaci�n </asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkObse" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Comentarios</asp:LinkButton></td>
                                                <td></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkVend" runat="server" CssClass="solapa"
                                                        Width="70px" Visible="False" Height="21px" CausesValidation="False"> Propietarios </asp:LinkButton></td>
                                                <td background="imagenes/tab_fondo.bmp"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkComp" runat="server" CssClass="solapa"
                                                        Width="70px" Visible="False" Height="21px" CausesValidation="False"> Compradores </asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>

                            <%-- ... --%>

                            <tr>
                                <td align="center" colspan="3">
                                    <asp:Panel ID="panDato" runat="server" CssClass="titulo" Width="100%" BorderWidth="1px" BorderStyle="Solid"
                                        Height="116px" Visible="False">
                                        <table style="WIDTH: 100%; HEIGHT: 106px" id="tabDato" class="FdoFld" border="0" cellpadding="0"
                                            align="left">
                                            <tbody>
                                                <tr>
                                                    <td height="5">
                                                        <asp:Label ID="lblTitu" runat="server" CssClass="titulo" Width="100%"></asp:Label></td>
                                                    <td valign="top" align="right">
                                                        <asp:ImageButton ID="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></td>
                                                </tr>
                                                <tr>
                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                        <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                </tr>
                                                <tr>
                                                    <td style="WIDTH: 100%" colspan="2" align="center">
                                                        <asp:Panel ID="panCabecera" runat="server" CssClass="titulo" Width="100%">
                                                            <table style="WIDTH: 100%" id="tabCabecera" border="0" cellpadding="0" align="left">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="18%" nowrap align="right">
                                                                            <asp:Label ID="lblInicFecha1" runat="server" CssClass="titulo">Fecha Carga:&nbsp;</asp:Label></td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtInicFecha" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																				<asp:Label ID="lblFinaFecha" runat="server" CssClass="titulo">Fecha Cierre:</asp:Label>
                                                                            <cc1:DateBox ID="txtFinaFecha" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="18%" nowrap align="right">
                                                                            <asp:Label ID="lblFechaTransferencia1" runat="server" CssClass="titulo">Fecha de Transferencia:&nbsp;</asp:Label></td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtFechaTransferencia" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="True"></cc1:DateBox>&nbsp;&nbsp;&nbsp;</td>
                                                                        <tr>
                                                                            <td nowrap align="right">
                                                                                <asp:Label ID="lblFechaTram1" runat="server" CssClass="titulo">Fecha Recepi�n Tramite:&nbsp;</asp:Label></td>
                                                                            <td>
                                                                                <cc1:DateBox ID="txtFechaTram" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox></td>
                                                                        </tr>
                                                                    <tr>
                                                                        <td width="18%" align="right">
                                                                            <asp:Label ID="lblNroControl" runat="server" CssClass="titulo">Nro. de Control:</asp:Label></td>
                                                                        <td>
                                                                            <cc1:NumberBox ID="txtNroControl" runat="server" CssClass="cuadrotexto" Width="81px" MaxValor="999999999"
                                                                                Visible="true" MaxLength="3" EsPorcen="False" EsDecimal="False"></cc1:NumberBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblEsta" runat="server" CssClass="titulo">Estado:&nbsp;</asp:Label></td>
                                                                        <td>
                                                                            <cc1:ComboBox ID="cmbEsta" class="combo" runat="server" CssClass="cuadrotexto" Width="180px" Enabled="False"
                                                                                AutoPostBack="True">
                                                                            </cc1:ComboBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" valign="top" align="right">
                                                                            <asp:Label ID="lblCriaComp" runat="server" CssClass="titulo">Comprador Raza/Criador:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <uc1:CLIE ID="usrCriaComp" runat="server" Copropiedad="True" AceptaNull="False" Tabla="Criadores"
                                                                                Saltos="1,2" Ancho="800" Criador="true" FilDocuNume="True" MuestraDesc="False"></uc1:CLIE>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:Button ID="btnCriaCopropiedadComp" runat="server" CssClass="boton" Width="90px" Text="Copropiedad"
                                                                                CausesValidation="False"></asp:Button></td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                        <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                </tr>
                                                <tr>
                                                    <td height="15" valign="top" align="right">
                                                        <asp:Label ID="lblCriaVend" runat="server" CssClass="titulo">Vendedor Raza/Criador:</asp:Label>&nbsp;</td>
                                                    <td>
                                                        <uc1:CLIE ID="usrCriaVend" runat="server" Copropiedad="True" AceptaNull="False" Tabla="Criadores"
                                                            Saltos="1,2" Ancho="800" Criador="true" FilDocuNume="True" MuestraDesc="False"></uc1:CLIE>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Button ID="btnCriaCopropiedadVend" runat="server" CssClass="boton" Width="90px" Text="Copropiedad"
                                                            CausesValidation="False" Style="Z-INDEX: 0"></asp:Button></td>
                                                </tr>
                                                <tr>
                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                        <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                </tr>
                                                <tr>
                                                    <td background="imagenes/formfdofields.jpg" align="right">
                                                        <asp:Label ID="lblProducto" runat="server" CssClass="titulo">Vendedor Raza/Criador:</asp:Label>&nbsp;</td>
                                                    <td style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
                                                        <uc1:PROH ID="usrProd" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2" Ancho="800"
                                                            EsPropietario="True" Copropiedad="True" FilTipo="S" AutoPostback="False" MuestraDesc="false"
                                                            MuestraSoloRazaClieDerivr="true" IncluirDeshabilitados="true"></uc1:PROH>
                                                    </td>
                                </td>
                            </tr>

                            <%-- ... --%>


                            <tr style="DISPLAY: inline" id="Tr2" runat="server">
                                <td valign="top" align="right">
                                    <asp:Label ID="lblReserva" runat="server" CssClass="titulo">Reserva:&nbsp;</asp:Label></td>
                                <td align="left">
                                    <input id="chkReservaEstock" onclick="chkReservaEstock_checked(this)" type="checkbox" runat="server"
                                        name="chkReservaEstock" value="" checked>&nbsp;
										<cc1:NumberBox ID="txtCantidadReser" runat="server" CssClass="cuadrotexto" Width="50px" MaxValor="999999999"
                                            AutoPostBack="false" Enabled="false" MaxLength="9" name="txtCantidadReser" autocomplete="off"></cc1:NumberBox>&nbsp;&nbsp;
										<asp:Label ID="lblReserCrias" runat="server" CssClass="titulo">Crias:&nbsp;</asp:Label>
                                    <cc1:NumberBox ID="txtCantCriasReser" runat="server" CssClass="cuadrotexto" Width="50px" MaxValor="999999999"
                                        AutoPostBack="false" Enabled="false" MaxLength="9" name="txtCantidadReser" autocomplete="off"></cc1:NumberBox></td>
                            </tr>
                            <tr style="DISPLAY: none" id="rowDosisCria" runat="server">
                                <td colspan="2">
                                    <table style="WIDTH: 100%" border="0" cellpadding="0" align="left">
                                        <tr>
                                            <td width="18%" align="right"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!--	<TR>
																	<TD colSpan="3" align="center">
																		<TABLE style="MARGIN-TOP: 10px" id="Table8" class="marco" cellSpacing="0" cellPadding="0"
																			width="100%">
																			<TR>
																				<TD colSpan="3" align="left"><U>
																						<asp:Label id="lblTituAnal" runat="server" cssclass="titulo">Datos del �ltimo an�lisis</asp:Label></U></TD>
																			</TR>
																			<TR>
																				<TD noWrap align="left">
																					<asp:Label id="lblNroAnal" runat="server" cssclass="titulo">N�mero:&nbsp;</asp:Label>
																					<cc1:numberbox id="txtNroAnal" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></cc1:numberbox>
																					<asp:Label id="lblTipoAnal" runat="server" cssclass="titulo">Tipo de analisis:&nbsp;</asp:Label>
																					<CC1:TEXTBOXTAB id="txtTipoAnal" runat="server" cssclass="cuadrotexto" Width="100px" Enabled="False"
																						MaxLength="4"></CC1:TEXTBOXTAB>
																					<asp:Label id="lblResulAnal" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label>
																					<CC1:TEXTBOXTAB id="txtResulAnal" runat="server" cssclass="cuadrotexto" Width="50%" Enabled="False"
																						MaxLength="4"></CC1:TEXTBOXTAB></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>-->
                            <tr style="DISPLAY: inline" id="rowProp" runat="server">
                                <td valign="top" align="right">
                                    <asp:Label ID="lblClieProp" runat="server" CssClass="titulo">Propietario Actual:&nbsp;</asp:Label></td>
                                <td>
                                    <uc1:CLIE ID="usrClieProp" runat="server" AceptaNull="true" Tabla="Clientes" Saltos="1,2"
                                        MuestraDesc="False" FilDocuNume="True" Activo="False"></uc1:CLIE>
                                </td>
                            </tr>
                            <tr style="DISPLAY: inline" id="rowExpo" runat="server">
                                <td valign="top" align="right"></td>
                                <td></td>
                            </tr>
                            <tr style="DISPLAY: inline" id="rowDivExpo" runat="server">
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2" nowrap align="center">
                                    <asp:Label ID="lblTranPlan" runat="server" CssClass="titulo"><br>Se ha detectado m�s de un producto para este tr�mite.<br>Puede modificar los mismos desde la opci�n de Transferencia por Planilla.<br></asp:Label></td>
                            </tr>
                        </tbody>
                    </table>
                    </asp:panel></td>
            </tr>
            <tr>
                <td style="WIDTH: 100%" colspan="2">
                    <asp:Panel ID="panRequ" runat="server" CssClass="titulo" Width="100%" Visible="False">
                        <table style="WIDTH: 100%" id="tabRequ" border="0" cellpadding="0" align="left">
                            <tr>
                                <td style="WIDTH: 100%" colspan="2">
                                    <asp:DataGrid ID="grdRequ" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                        HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
                                        OnPageIndexChanged="grdRequ_Page" OnEditCommand="mEditarDatosRequ">
                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                        <FooterStyle CssClass="footer"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="trad_id"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
                                                <HeaderStyle Width="40%"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="_pend" HeaderText="Pendiente"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="_manu" HeaderText="Manual"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblRequRequ" runat="server" CssClass="titulo">Requisito:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <cc1:ComboBox ID="cmbRequRequ" class="combo" runat="server" CssClass="cuadrotexto" Width="80%"
                                        NomOper="rg_requisitos_cargar" MostrarBotones="False" Filtra="true">
                                    </cc1:ComboBox></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <asp:CheckBox ID="chkRequPend" Text="Pendiente" CssClass="titulo" runat="server" Checked="False"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblRequManuTit" runat="server" CssClass="titulo">Manual:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <asp:Label ID="lblRequManu" runat="server" CssClass="Desc">Si</asp:Label></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblRequFinaFechaTit" runat="server" CssClass="titulo">Fecha Finalizaci�n:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <asp:Label ID="lblRequFinaFecha" runat="server" CssClass="Desc"></asp:Label></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td height="30" valign="middle" colspan="2" align="center">
                                    <asp:Button ID="btnAltaRequ" runat="server" CssClass="boton" Width="100px" Text="Agregar Req."></asp:Button>&nbsp;
										<asp:Button ID="btnBajaRequ" runat="server" CssClass="boton" Width="100px" Text="Eliminar Req."></asp:Button>&nbsp;
										<asp:Button ID="btnModiRequ" runat="server" CssClass="boton" Width="100px" Text="Modificar Req."></asp:Button>&nbsp;
										<asp:Button ID="btnLimpRequ" runat="server" CssClass="boton" Width="100px" Text="Limpiar Req."></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="WIDTH: 100%" valign="top" colspan="2" align="center">
                    <asp:Panel ID="panDocu" runat="server" CssClass="titulo" Width="100%" Visible="False">
                        <table style="WIDTH: 100%" id="tabDocu" border="0" cellpadding="0" align="left">
                            <tr>
                                <td style="WIDTH: 100%" colspan="2" align="center">
                                    <asp:DataGrid ID="grdDocu" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                        HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
                                        OnUpdateCommand="mEditarDatosDocu" OnPageIndexChanged="grdDocu_Page">
                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                        <FooterStyle CssClass="footer"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="trdo_id" HeaderText="trdo_id"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="trdo_path" HeaderText="Documento">
                                                <HeaderStyle Width="50%"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="trdo_refe" HeaderText="Referencia">
                                                <HeaderStyle Width="50%"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblDocuDocu" runat="server" CssClass="titulo">Documento:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <cc1:TextBoxTab ID="txtDocuDocu" runat="server" CssClass="cuadrotexto" Width="340px" ReadOnly="True"></cc1:TextBoxTab><img style="CURSOR: hand" id="imgDelDocuDoc" onclick="javascript:mLimpiarPath('txtDocuDocu','imgDelDocuDoc');"
                                        alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<br>
                                    <input style="WIDTH: 340px; HEIGHT: 22px" id="filDocuDocu" name="File1" size="49" type="file"
                                        runat="server">
                                </td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblDocuObse" runat="server" CssClass="titulo">Referencia:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <cc1:TextBoxTab ID="txtDocuObse" runat="server" CssClass="cuadrotexto" Width="80%" AceptaNull="False"
                                        Height="41px" TextMode="MultiLine"></cc1:TextBoxTab></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td height="34" valign="middle" colspan="2" align="center">
                                    <asp:Button ID="btnAltaDocu" runat="server" CssClass="boton" Width="110px" Text="Agregar Docum."></asp:Button>&nbsp;
										<asp:Button ID="btnBajaDocu" runat="server" CssClass="boton" Width="110px" Text="Eliminar Docum."
                                            Visible="true"></asp:Button>&nbsp;
										<asp:Button ID="btnModiDocu" runat="server" CssClass="boton" Width="110px" Text="Modificar Docum."
                                            Visible="true"></asp:Button>&nbsp;
										<asp:Button ID="btnLimpDocu" runat="server" CssClass="boton" Width="110px" Text="Limpiar Docum."
                                            Visible="true"></asp:Button>&nbsp;&nbsp;
                                    <button style="WIDTH: 110px; FONT-WEIGHT: bold" id="btnDescDocu" class="boton" onclick="javascript:mDescargarDocumento('hdnDocuId','tramites_docum','txtDocuDocu');"
                                        runat="server" value="Descargar">
                                        Descargar</button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="WIDTH: 100%" colspan="2">
                    <asp:Panel ID="panObse" runat="server" CssClass="titulo" Width="100%" Visible="False">
                        <table style="WIDTH: 100%" id="tabObse" border="0" cellpadding="0" align="left">
                            <tr>
                                <td style="WIDTH: 100%" colspan="2">
                                    <asp:DataGrid ID="grdObse" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                        HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
                                        OnPageIndexChanged="grdObse_Page" OnEditCommand="mEditarDatosObse">
                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                        <FooterStyle CssClass="footer"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="trao_id"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="trao_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
                                                <HeaderStyle Width="50%"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="trao_obse" HeaderText="Comentarios">
                                                <HeaderStyle Width="50%"></HeaderStyle>
                                            </asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblObseFecha" runat="server" CssClass="titulo">Fecha:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <cc1:DateBox ID="txtObseFecha" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblObseRequ" runat="server" CssClass="titulo">Requisito:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <cc1:ComboBox ID="cmbObseRequ" class="combo" runat="server" CssClass="cuadrotexto" Width="80%"
                                        NomOper="rg_requisitos_cargar" MostrarBotones="False" Filtra="true">
                                    </cc1:ComboBox></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                    <asp:Label ID="lblObseObse" runat="server" CssClass="titulo">Comentario:</asp:Label>&nbsp;</td>
                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                    <cc1:TextBoxTab ID="txtObseObse" runat="server" CssClass="cuadrotexto" Width="80%" Height="41px"
                                        TextMode="MultiLine"></cc1:TextBoxTab></td>
                            </tr>
                            <tr>
                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                            </tr>
                            <tr>
                                <td height="30" valign="middle" colspan="2" align="center">
                                    <asp:Button ID="btnAltaObse" runat="server" CssClass="boton" Width="100px" Text="Agregar Com."></asp:Button>&nbsp;
										<asp:Button ID="btnBajaObse" runat="server" CssClass="boton" Width="100px" Text="Eliminar Com."></asp:Button>&nbsp;
										<asp:Button ID="btnModiObse" runat="server" CssClass="boton" Width="100px" Text="Modificar Com."></asp:Button>&nbsp;
										<asp:Button ID="btnLimpObse" runat="server" CssClass="boton" Width="100px" Text="Limpiar Com."></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        </asp:panel>
			<div style="DISPLAY: inline" id="divgraba" runat="server">
                <asp:Panel ID="panBotones" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblBaja" runat="server" CssClass="titulo" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr height="30">
                            <td valign="middle" align="center"><a id="editar" name="editar"></a>
                                <asp:Button ID="btnAlta" runat="server" CssClass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnBaja" runat="server" CssClass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnModi" runat="server" CssClass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnLimp" runat="server" CssClass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnErr" runat="server" CssClass="boton" Width="80px" Text="Ver Errores" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnStock" runat="server" CssClass="boton" Width="110px" Text="Stock Semen" Visible="false"
                                    CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnCertProp" runat="server" CssClass="boton" Width="90px" Text="Cert.Porp."
                                    CausesValidation="False"></asp:Button></td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        <div style="DISPLAY: none" id="divproce" runat="server">
            <asp:Panel ID="panproce" runat="server" CssClass="titulo" Width="100%">
                <asp:Image ID="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            </asp:Panel>
        </div>
        </TD></TR></TBODY></TABLE>
        <!--- FIN CONTENIDO --->
        </TD>
			<td width="13" background="imagenes/recde.jpg">
                <img height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
        </TR>
			<tr>
                <td width="9">
                    <img height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recinf.jpg">
                    <img height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
                <td width="13">
                    <img height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
            </tr>
        </TBODY></TABLE>
        <!----------------- FIN RECUADRO ----------------->
        <br>
        <div style="DISPLAY: none">
            <asp:TextBox ID="lblMens" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnRazaId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnRequId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDetaId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDocuId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnObseId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnSess" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDatosPop" runat="server" AutoPostBack="True"></asp:TextBox>
            <asp:TextBox ID="hdnCompId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnCompOrig" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnVendOrig" runat="server"></asp:TextBox>
            <asp:TextBox ID="lblAltaId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnMultiProd" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnTramNro" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnMsgError" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDatosTEPop" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnTEId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnSexo" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnOperacion" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnProdId" runat="server"></asp:TextBox>
        </div>
        <script language="JavaScript">
            function mPorcPeti() {
                document.all("divgraba").style.display = "none";
                document.all("divproce").style.display = "inline";
            }
            if (document.all('lblAltaId') != null && document.all('lblAltaId').value != '') {
                alert('Se ha asignado el nro de tramite ' + document.all('lblAltaId').value);
                document.all('lblAltaId').value = '';
            }
            if ('<%=mintTtraId%>' == '11' || '<%=mintTtraId%>' == '30' || '<%=mintTtraId%>' == '31') {
                if (document.all('usrProd:usrProducto:txtSexoId') != null)
                    document.all('usrProd:usrProducto:txtSexoId').value = '1';
            }
        </script>
    </form>
</body>
</html>
