Namespace SRA

Partial Class Especies
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

      End Sub
   Protected WithEvents lnkGenealogia As System.Web.UI.WebControls.LinkButton

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Especies
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "especies_tipos", cmbTipoEspecie, "S")
      clsWeb.gCargarComboBool(cmbAnillo, "S")
      clsWeb.gCargarComboBool(cmbVentaSRA, "S")
      clsWeb.gCargarComboBool(cmbAstado, "S")
      clsWeb.gCargarComboBool(cmbCaractPrec, "S")
            clsWeb.gCargarComboBool(cmbCtrlColor, "S")
            clsWeb.gCargarComboBool(cmbCtrlPelaje, "S")
      clsWeb.gCargarComboBool(cmbCtrlReg, "S")
      clsWeb.gCargarComboBool(cmbGenealo, "S")
      clsWeb.gCargarComboBool(cmbMocho, "S")
            clsWeb.gCargarComboBool(cmbRegPrep, "S")
            clsWeb.gCargarComboBool(cmbReqADN, "S")
            clsWeb.gCargarComboBool(cmbMuesCrias, "S")
            clsWeb.gCargarComboBool(cmbSiemMult, "S")
            clsWeb.gCargarComboBool(cmbFlus, "S")
            clsWeb.gCargarComboBool(cmbSani, "S")
      clsWeb.gCargarComboBool(cmbReqAnalisis, "S")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

      txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_codi")
      txtDescripcion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_desc")
      txtNomNum.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_nomb_nume")
      txtPromVida.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_prom_vida")
            txtMuesCriasPorc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_mues_crias_porc")
            txtMuesCriasMini.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_mues_crias_mini")
      txtDiasGest.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_gesta_dias")
      txtDiasGestMax.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_gesta_dias_max")
      txtDiasGestMin.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_gesta_dias_min")
      txtDiasServCamp.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_serv_dias")
      txtDiasInsem.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_inse_dias")
      txtDiasParto.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_part_dias")
      txtMelli.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_melli")

      txtCaract.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "espe_cara")

   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    txtCodigo.Valor = IIf(.Item("espe_codi").ToString.Length > 0, .Item("espe_codi"), String.Empty)
                    txtDescripcion.Valor = IIf(.Item("espe_desc").ToString.Length > 0, .Item("espe_desc"), String.Empty)
                    cmbTipoEspecie.Valor = IIf(.Item("espe_espt_id").ToString.Length > 0, .Item("espe_espt_id"), DBNull.Value)
                    txtNomNum.Valor = IIf(.Item("espe_nomb_nume").ToString.Length > 0, .Item("espe_nomb_nume"), String.Empty)
                    txtPromVida.Valor = IIf(.Item("espe_prom_vida").ToString.Length > 0, .Item("espe_prom_vida"), String.Empty)
                    cmbGenealo.ValorBool = .Item("espe_genealo")
                    cmbAnillo.ValorBool = .Item("espe_anillo")
                    cmbVentaSRA.ValorBool = .Item("espe_venta")

                    txtDiasGest.Valor = IIf(.Item("espe_gesta_dias").ToString.Length > 0, .Item("espe_gesta_dias"), String.Empty)
                    txtDiasGestMax.Valor = IIf(.Item("espe_gesta_dias_max").ToString.Length > 0, .Item("espe_gesta_dias_max"), String.Empty)
                    txtDiasGestMin.Valor = IIf(.Item("espe_gesta_dias_min").ToString.Length > 0, .Item("espe_gesta_dias_min"), String.Empty)
                    txtDiasServCamp.Valor = IIf(.Item("espe_serv_dias").ToString.Length > 0, .Item("espe_serv_dias"), String.Empty)
                    txtDiasInsem.Valor = IIf(.Item("espe_inse_dias").ToString.Length > 0, .Item("espe_inse_dias"), String.Empty)
                    txtDiasParto.Valor = IIf(.Item("espe_part_dias").ToString.Length > 0, .Item("espe_part_dias"), String.Empty)
                    txtMelli.Valor = IIf(.Item("espe_melli").ToString.Length > 0, .Item("espe_melli"), String.Empty)
                    cmbReqAnalisis.ValorBool = .Item("espe_anal_tipi")
                    cmbReqADN.ValorBool = .Item("espe_anal_adn")
                    cmbMuesCrias.ValorBool = .Item("espe_mues_crias")
                    txtMuesCriasPorc.Valor = IIf(.Item("espe_mues_crias_porc").ToString.Length > 0, .Item("espe_mues_crias_porc"), String.Empty)
                    txtMuesCriasMini.Valor = IIf(.Item("espe_mues_crias_mini").ToString.Length > 0, .Item("espe_mues_crias_mini"), String.Empty)
                    cmbRegPrep.ValorBool = .Item("espe_regi_prep")
                    cmbCtrlReg.ValorBool = .Item("espe_regi_prep_ctrol")
                    txtCaract.Valor = IIf(.Item("espe_cara").ToString.Length > 0, .Item("espe_cara"), String.Empty)
                    cmbCaractPrec.ValorBool = .Item("espe_cara_prec_nume")
                    cmbAstado.ValorBool = .Item("espe_asta")
                    cmbMocho.ValorBool = .Item("espe_mocho")
                    cmbCtrlColor.ValorBool = .Item("espe_color")
                    cmbCtrlPelaje.ValorBool = .Item("espe_pela")
                    cmbSiemMult.ValorBool = .Item("espe_siem_mult")
                    cmbFlus.ValorBool = .Item("espe_flus")
                    cmbSani.ValorBool = .Item("espe_sani")
                    cmbPerf.Valor = IIf(.Item("espe_perf").ToString.Length > 0, .Item("espe_perf"), String.Empty)
                    If Not .IsNull("espe_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("espe_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If
                End With

                mSetearEditor(False)
                mMostrarPanel(True)
                mShowTabs(1)
            End If
        End Sub
   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
      mdsDatos.Tables(0).TableName = mstrTabla
      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      Session(mstrTabla) = mdsDatos
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""

      lblBaja.Text = ""
      grdDato.CurrentPageIndex = 0
      txtCaract.Text = ""
      txtCodigo.Text = ""
      cmbTipoEspecie.Limpiar()
      txtDescripcion.Text = ""
      txtNomNum.Text = ""
      txtPromVida.Text = ""
      cmbGenealo.Limpiar()
      cmbAnillo.Limpiar()
            cmbMuesCrias.Limpiar()
            txtMuesCriasPorc.Text = ""
            txtMuesCriasMini.Text = ""
            txtDiasGest.Text = ""
            cmbFlus.Limpiar()
            cmbSani.Limpiar()
            cmbSiemMult.Limpiar()
      txtDiasGestMax.Text = ""
      txtDiasGestMin.Text = ""
      txtDiasServCamp.Text = ""
      txtDiasInsem.Text = ""
      txtDiasParto.Text = ""
      txtMelli.Text = ""
      cmbReqAnalisis.Limpiar()
      cmbReqADN.Limpiar()

      cmbRegPrep.Limpiar()
      cmbCtrlReg.Limpiar()
      txtCaract.Text = ""
      cmbCaractPrec.Limpiar()
      cmbAstado.Limpiar()
      cmbMocho.Limpiar()
            cmbCtrlColor.Limpiar()
            cmbCtrlPelaje.Limpiar()
      cmbPerf.Limpiar()


      mCrearDataSet("")
      mShowTabs(1)
      mSetearEditor(True)
   End Sub
   Private Sub mImprimir()
      Try
         Dim params As String
         Dim lstrRptName As String = "Especies"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      mEstablecerPerfil()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Private Sub mGuardarDatos()
      mValidaDatos()

            With mdsDatos.Tables(0).Rows(0)

                .Item("espe_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

                .Item("espe_codi") = txtCodigo.Valor
                .Item("espe_desc") = txtDescripcion.Valor
                .Item("espe_espt_id") = cmbTipoEspecie.Valor
                .Item("espe_nomb_nume") = txtNomNum.Valor
                .Item("espe_prom_vida") = IIf(txtPromVida.Valor.Length > 0, txtPromVida.Valor, DBNull.Value)
                .Item("espe_genealo") = cmbGenealo.ValorBool
                .Item("espe_anillo") = cmbAnillo.ValorBool
                .Item("espe_venta") = cmbVentaSRA.ValorBool

                .Item("espe_gesta_dias") = .Item("espe_genealo")
                .Item("espe_gesta_dias_max") = .Item("espe_genealo")
                .Item("espe_gesta_dias_min") = .Item("espe_genealo")
                .Item("espe_serv_dias") = .Item("espe_genealo")
                .Item("espe_inse_dias") = .Item("espe_genealo")
                .Item("espe_part_dias") = .Item("espe_genealo")
                .Item("espe_melli") = .Item("espe_genealo")
                .Item("espe_anal_tipi") = .Item("espe_genealo")
                .Item("espe_anal_adn") = .Item("espe_genealo")
                .Item("espe_mues_crias") = .Item("espe_genealo")
                .Item("espe_mues_crias_porc") = .Item("espe_genealo")
                .Item("espe_mues_crias_mini") = .Item("espe_genealo")
                .Item("espe_regi_prep") = .Item("espe_genealo")
                .Item("espe_regi_prep_ctrol") = .Item("espe_genealo")
                .Item("espe_cara") = .Item("espe_genealo")
                .Item("espe_cara_prec_nume") = .Item("espe_genealo")
                .Item("espe_asta") = .Item("espe_genealo")
                .Item("espe_mocho") = .Item("espe_genealo")
                .Item("espe_color") = .Item("espe_genealo")
                .Item("espe_pela") = .Item("espe_genealo")
                .Item("espe_siem_mult") = .Item("espe_genealo")
                .Item("espe_flus") = .Item("espe_genealo")
                .Item("espe_sani") = .Item("espe_genealo")
                .Item("espe_perf") = .Item("espe_genealo")
            End With
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panGeneral.Visible = False
         lnkGeneral.Font.Bold = False
         panGenealogia1.Visible = False
         lnkGenealogia1.Font.Bold = False
         panGenealogia2.Visible = False
         lnkGenealogia2.Font.Bold = False
         Select Case Tab
            Case 1
               panGeneral.Visible = True
               lnkGeneral.Font.Bold = True
            Case 2
               If cmbGenealo.Valor.ToString = "0" Then
                  mShowTabs(1)
                  Throw New AccesoBD.clsErrNeg("Debe controlar Genealog�a.")
               End If
               panGenealogia1.Visible = True
               lnkGenealogia1.Font.Bold = True
            Case 3
               If cmbGenealo.Valor.ToString = "0" Then
                  mShowTabs(1)
                  Throw New AccesoBD.clsErrNeg("Debe controlar Genealog�a.")
               End If
               panGenealogia2.Visible = True
               lnkGenealogia2.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   'Solapas
   Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkGenealogia1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenealogia1.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkGenealogia2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenealogia2.Click
      mShowTabs(3)
   End Sub
   Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      mImprimir()
   End Sub
#End Region
End Class

End Namespace
