Namespace SRA

Partial Class InspeccionCriador
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents usrProdFilDeta As usrProdDeriv
    Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    Protected WithEvents lblProductoFil As System.Web.UI.WebControls.Label
    Protected WithEvents hdnCriaFilId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPromVida As System.Web.UI.WebControls.Label
    Protected WithEvents txtPromVida As NixorControls.NumberBox
    Protected WithEvents lblEspPromV As System.Web.UI.WebControls.Label
    Protected WithEvents hdnAutorId As System.Web.UI.WebControls.TextBox
    Protected WithEvents hdnDocumId As System.Web.UI.WebControls.TextBox
    Protected WithEvents panCompletar As System.Web.UI.WebControls.Panel
    Protected WithEvents btnEntidades As System.Web.UI.HtmlControls.HtmlButton



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gtab_Inspecciones_Criadores
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrTablaInspecCriaDeta As String = SRA_Neg.Constantes.gtab_Inspec_Cria_Deta
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mdsdatos As DataSet
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then
                mSetearEventos()
                mConsultar()
                mCargarCombos()
                clsWeb.gInicializarControles(Me, mstrConn)

                Session(mstrTabla) = Nothing
            Else
                If panDato.Visible Then
                    mdsdatos = Session(mstrTabla)
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnCerrar.Attributes.Add("onclick", "if(!confirm('Confirmar el cierre de la Inspecci�n?')) return false;")
    End Sub



    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click

        Dim strFitro As String





        If txtRPBusq.Text.ToString <> "" Then
            strFitro = strFitro + " _prdt_rp = '" + txtRPBusq.Text.ToString + "' and "
        End If

        If txtHBABusq.Text.ToString <> "" Then
            strFitro = strFitro + " _prdt_sra_nume = '" + txtHBABusq.Text.ToString + "' and "
        End If


        If txtNombreBusq.Text.ToString <> "" Then
            strFitro = strFitro + " _prdt_nomb like '%" & _
            txtNombreBusq.Text.ToString & "%' and "
        End If

        strFitro = strFitro + "1=1"

        mdsdatos.Tables(mstrTablaInspecCriaDeta).DefaultView.RowFilter = strFitro

        If strFitro = "1=1" Then
            mdsdatos.Tables(mstrTablaInspecCriaDeta).DefaultView.RowFilter = ""
        End If

        With mdsdatos.Tables(mstrTablaInspecCriaDeta)
            .DefaultView.Sort = "_estado desc"
            grdProd.DataSource = .DefaultView

            ' the currentpageindex must be >= 0 and < pagecount<BR>   
            Try

                grdProd.DataBind()
                grdProd.Visible = True

            Catch
                Try
                    grdProd.CurrentPageIndex = 0
                    grdProd.DataBind()
                    grdProd.Visible = True

                Catch
                    ' Me.lblError.Text = "No data for selection"
                    ' Me.lblError.Visible = True

                End Try

            End Try


        End With


    End Sub
    Private Sub mCargarCombos()
        If hdnId.Text <> "" Then

            usrCriador.Valor = mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("_prdt_cria_id")
        End If


        clsWeb.gCargarRefeCmb(mstrConn, "establecimientos", cmbEsbl, "S", "@esbl_cria_id=" + usrCriador.Valor.ToString)
    End Sub

#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        usrCriadorFil.FilClaveUnica = False
        usrCriadorFil.ColClaveUnica = False
        usrCriadorFil.ColDocuNume = False
        usrCriadorFil.ColCUIT = False
        usrCriadorFil.ColClaveUnica = False
        usrCriadorFil.FilClaveUnica = True
        usrCriadorFil.FilAgru = False
        usrCriadorFil.FilCriaNume = True
        usrCriadorFil.FilCUIT = False
        usrCriadorFil.FilDocuNume = True
        usrCriadorFil.FilTarjNume = False
        usrCriadorFil.Criador = True
        usrCriadorFil.Inscrip = True
        usrCriadorFil.ColCriaNume = True

        usrCriador.Tabla = mstrCriadores
        usrCriador.AutoPostback = False
        usrCriador.FilClaveUnica = False
        usrCriador.ColClaveUnica = False
        usrCriador.Ancho = 790
        usrCriador.Alto = 510
        usrCriador.ColDocuNume = False
        usrCriador.ColCUIT = False
        usrCriador.ColClaveUnica = False
        usrCriador.FilClaveUnica = False
        usrCriador.FilCUIT = False
        usrCriador.FilDocuNume = False
        usrCriador.FilTarjNume = False
        usrCriador.Criador = True
        usrCriador.Inscrip = True
        usrCriador.ColCriaNume = True
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridProd_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            'mGuardarCombo()
            grdProd.EditItemIndex = -1
            If (grdProd.CurrentPageIndex < 0 Or grdProd.CurrentPageIndex >= grdProd.PageCount) Then
                grdProd.CurrentPageIndex = 0
            Else
                grdProd.CurrentPageIndex = E.NewPageIndex
            End If
            grdProd.DataSource = mdsdatos.Tables(mstrTablaInspecCriaDeta)
            grdProd.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarCombo()
        Dim dr As DataRow
        'Mantengo los valores del combo de las paguinas de la grilla
        For Each oDataItem As DataGridItem In grdProd.Items
            dr = mdsdatos.Tables(1).Select("icrd_id = " & oDataItem.Cells(0).Text)(0)
            dr.Item("icrd_resu") = IIf(DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked = False, False, DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked)
        Next




        Session(mstrTabla) = mdsdatos
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnGenerar.Enabled = pbooAlta
        btnCerrar.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta

    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsdatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsdatos.Tables(0).TableName = mstrTabla
        mdsdatos.Tables(1).TableName = mstrTablaInspecCriaDeta

        If mdsdatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsdatos.Tables(mstrTabla).Rows.Add(mdsdatos.Tables(mstrTabla).NewRow)
        End If

        'grdDato.DataSource = mdsdatos.Tables(mstrTabla)
        'grdDato.DataBind()

        grdProd.DataSource = mdsdatos.Tables(mstrTablaInspecCriaDeta)
        grdProd.DataBind()

         
        Session(mstrTabla) = mdsdatos
    End Sub
    Private Sub mCargarDeta()
        Dim ds As New DataSet
        Dim ldr As DataRow
        ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        

        For Each dr As DataRow In ds.Tables(0).Rows
            ldr = mdsdatos.Tables(mstrTablaInspecCriaDeta).NewRow
            With ldr
                .Item("icrd_id") = clsSQLServer.gObtenerId(mdsdatos.Tables(mstrTablaInspecCriaDeta), "icrd_id")
                .Item("icrd_prdt_id") = dr.Item("prdt_id")
                .Item("icrd_icri_id") = mdsdatos.Tables(0).Rows(0).Item("icri_id")
                .Item("_prdt_sra_nume") = dr.Item("prdt_sra_nume")
                .Item("_prdt_rp") = dr.Item("prdt_rp")
                .Item("_prdt_nomb") = dr.Item("prdt_nomb")
                .Item("icrd_resu") = False


            End With
            mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows.Add(ldr)
        Next


        ds.Dispose()

    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
        mCrearDataSet(hdnId.Text)
        mCargarCombos()

        mdsdatos.AcceptChanges()

        With mdsdatos.Tables(0).Rows(0)
            usrCriador.Valor = .Item("icri_cria_id")
            txtObser.Valor = .Item("icri_obse")
            txtFecha.Fecha = .Item("icri_prob_fecha")
            txtInspector.Valor = .Item("icri_insp")
            cmbEsbl.Valor = mdsdatos.Tables(0).Rows(0).Item("_esbl_id")
            hdnEscrId.Text = .Item("icri_escr_id")
        End With



        mSetearEditor(False)
        mHabilitarCtrl(mdsdatos.Tables(0).Rows(0).Item("icri_cerra"))
        trFechaInsp.Visible = True
        trFechaInspSepara.Visible = True
        mMostrarPanel(True)
        mShowTabs(1)





    End Sub
    Private Sub mLimpiarFiltros()
        usrCriadorFil.Limpiar()
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
    End Sub
    Private Sub mHabilitarCtrl(ByVal pbool As Boolean)
        usrCriador.Activo = Not pbool
        cmbEsbl.Enabled = Not pbool
        txtFecha.Enabled = Not pbool
        txtFechaInsp.Enabled = Not pbool
        txtInspector.Enabled = Not pbool
        txtObser.Enabled = Not pbool
        btnCerrar.Enabled = Not pbool
        btnBaja.Enabled = Not pbool
        btnModi.Enabled = Not pbool
        btnLimp.Enabled = Not pbool
        'btnList.Enabled = pbool
        'Oculto cmb para Evitar que modifiquen
        grdProd.Columns(4).Visible = Not pbool
        grdProd.Columns(5).Visible = pbool
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        cmbEsbl.Items.Clear()
        mHabilitarCtrl(False)
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnCerrar.Enabled = False
        btnGenerar.Enabled = True
        btnAlta.Enabled = True
        mMostrarPanel(True)
        grdProd.Columns(4).Visible = True
        grdProd.Columns(5).Visible = False
        trFechaInsp.Visible = False
        trFechaInspSepara.Visible = False
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnEscrId.Text = ""
        hdnCerra.Text = ""
        usrCriador.cmbCriaRazaExt.Enabled = True
        usrCriador.Limpiar()
        cmbEsbl.Limpiar()
        txtObser.Text = ""
        txtFecha.Text = ""
        txtFechaInsp.Text = ""
        txtInspector.Text = ""
        grdDato.CurrentPageIndex = 0
        grdProd.CurrentPageIndex = 0
        mCrearDataSet("")
        mShowTabs(1)
        'mSetearEditor(True)
    End Sub
    Private Sub mCerrar()
        mConsultarGrilla()
        grdDato.CurrentPageIndex = 0
        mMostrarPanel(False)
    End Sub
    Private Sub mConsultarGrilla()
        Try
            mConsultar()
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panGeneral.Visible = pbooVisi
        panFiltro.Visible = Not (panDato.Visible)
        btnAgre.Enabled = Not (panDato.Visible)


    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsdatos)

            lobjGenerica.Alta()
            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsdatos)

            lobjGenerica.Modi()
            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsdatos)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If grdProd.Items.Count <= 0 Then
            Throw New AccesoBD.clsErrNeg("Deben Existir productos para la Inspecci�n")
        End If

        If txtFechaInsp.Fecha Is DBNull.Value Then
            If hdnCerra.Text = "1" Then
                Throw New AccesoBD.clsErrNeg("El campo fecha de Inspecci�n tiene que estar completo")
            End If
        End If

        If hdnCerra.Text = "1" And txtInspector.Text = "" Then
            Throw New AccesoBD.clsErrNeg("El Nombre del Inspector debe ser completado para cerrar la Inspecci�n")
        End If

        If txtFecha.Fecha Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("El campo fecha probable tiene que estar completo")
        End If

    End Sub
    Private Sub mGuardarDatos()
        mValidarDatos()
        mGuardarCombo()

        Dim oEstablecimiento As New SRA_Neg.Establecimientos(mstrConn, Session("sUserId").ToString())

        If hdnEscrId.Text = "" Then
            hdnEscrId.Text = oEstablecimiento.GetEstablecCriadorIDByCriaIdEstablecId _
            (usrCriador.Valor, cmbEsbl.Valor)
        End If

        With mdsdatos.Tables(0).Rows(0)
            .Item("icri_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("icri_prob_fecha") = txtFecha.Fecha
            .Item("icri_obse") = txtObser.Valor
            .Item("icri_insp") = txtInspector.Valor
            .Item("icri_cria_id") = usrCriador.Valor
            .Item("icri_escr_id") = hdnEscrId.Text
            .Item("icri_cerra") = IIf(hdnCerra.Text = "1", True, False)
            If hdnCerra.Text = "1" Or Not txtFechaInsp.Fecha Is DBNull.Value Then
                .Item("icri_fecha") = txtFechaInsp.Fecha
            End If
        End With
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        hdnCerra.Text = "1"
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        mCerrar()
    End Sub
    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            mConsultar()
            mMostrarPanel(False)
            btnAgre.Visible = True

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "InspeccionCriador"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&icrd_icri_id=" & mdsdatos.Tables(0).Rows(0).Item("icri_id")
            lstrRpt += "&icri_cria_id=" & mdsdatos.Tables(0).Rows(0).Item("icri_cria_id")
            lstrRpt += "&cerrado=" & IIf(mdsdatos.Tables(0).Rows(0).Item("icri_cerra"), 1, 0)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub
    Private Sub btnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        Try
            If usrCriador.Valor.ToString = "0" And cmbEsbl.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar un Criador y Establecimiento")
            End If
            mCargarProd()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub lnkGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkCompletar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCompletar.Click
        mShowTabs(2)
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
#End Region

#Region "Detalle"
        Public Sub mConsultar()
            Try
                Dim dsDatos As New DataSet

                Dim sFecha As String = String.Empty

                mstrCmd = "exec " + mstrTabla + "_consul "

                mstrCmd += "@icri_cria_id = " + usrCriadorFil.Valor.ToString
                If txtFechaDesdeFil.Fecha.ToString.Length > 0 Then
                    sFecha = txtFechaDesdeFil.Fecha.ToString.Replace(".", "")
                    mstrCmd = mstrCmd + ",@FechaDesde='" + Convert.ToDateTime(sFecha).ToString("yyyyMMdd") + "'"
                    'mstrCmd = mstrCmd + ",@FechaDesde='" + String.Format("yyyyMMdd", txtFechaDesdeFil.Fecha) + "'"
                    'mstrCmd = mstrCmd + ",@FechaDesde='" + FormatDateTime(txtFechaDesdeFil.Fecha, "yyyyMMdd") + "'"
                End If

                If txtFechaHastaFil.Fecha.ToString.Length > 0 Then
                    sFecha = txtFechaHastaFil.Fecha.ToString.Replace(".", "")
                    mstrCmd = mstrCmd + ",@FechaHasta='" + Convert.ToDateTime(sFecha).ToString("yyyyMMdd") + "'"
                    'mstrCmd = mstrCmd + ",@FechaHasta='" + String.Format("yyyyMMdd", txtFechaHastaFil.Fecha) + "'"
                    'mstrCmd = mstrCmd + ",@FechaHasta='" + FormatDateTime(txtFechaHastaFil.Fecha, "yyyyMMdd") + "'"
                End If

                '  clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

                dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)
                grdDato.Visible = True
                grdDato.DataSource = dsDatos

                grdDato.DataBind()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            'panCompletar.Visible = False
            divPanCompletar.Style.Add("display", "none")

            lnkCompletar.Font.Bold = False

            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    If usrCriador.Valor.ToString = "0" Or cmbEsbl.Valor.ToString = "" Then
                        mShowTabs(1)
                        Throw New AccesoBD.clsErrNeg("Debe ingresar el Criador y el Establecimiento")
                    End If
                    If grdProd.Items.Count = 0 Then
                        mShowTabs(1)
                        Throw New AccesoBD.clsErrNeg("No existen productos para el criador seleccionado")
                    End If
                    'panCompletar.Visible = True
                    divPanCompletar.Style.Add("display", "inline")
                    lnkCompletar.Font.Bold = True
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarProd()
        Try
            mstrCmd = "exec productos_xbusq "
            mstrCmd += "@prdt_cria_id = " + usrCriador.Valor.ToString




            mCargarDeta()
            grdProd.DataSource = mdsdatos.Tables(1)
            grdProd.DataBind()

            mActualizarEtiq()


            mShowTabs(2)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region



    Public Sub chkSel_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Written into several lines to clarify, you could go with one line
        'Current checkbox
        Dim box As CheckBox = CType(sender, CheckBox)
        'The TableCell the control is in
        Dim cell As TableCell = CType(box.Parent, TableCell)
        'The DataGridItem the cell belongs to
        Dim dgItem As DataGridItem = CType(cell.Parent, DataGridItem)

        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())


        Dim intProductoId As Integer
        Dim dtProducto As DataTable
        Dim intEstadoIdAnterior As Integer
        Dim intHBA As Integer
        Dim intRazaId As Integer
        Dim Sexo As String
        Dim lngProdId As Long = 0

        Dim dtProdSeleccionado As DataTable



        'With that DataGridItem you get for example ItemIndex of the DataGridItem you
        'are into. It is the same would be e.Item in ItemCommand,ItemDataBound or
        'ItemCreated. And you can then again get the PK of the current row and so on
        'via DataKeys collection.
        If CType(sender, System.Web.UI.WebControls.CheckBox).Checked Then

            Label1.Text = "chksel_oncanged " + grdProd.Items(dgItem.ItemIndex).Cells(0).Text + _
            " esta checked" + " r:" + _
            mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("icrd_resu").ToString() + _
             " _r:" + mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("_icrd_resu")

            For Each lDr As DataRow In _
            mdsdatos.Tables(mstrTablaInspecCriaDeta).Select("icrd_id=" & _
                grdProd.Items(dgItem.ItemIndex).Cells(0).Text)
                lDr.Item("icrd_resu") = True
                lDr.Item("_icrd_resu") = "S�"


            Next

        Else

            Label1.Text = "chksel_oncanged " + grdProd.Items(dgItem.ItemIndex).Cells(0).Text + _
            " esta no checked" + " r:" + _
            mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("icrd_resu").ToString() + _
             " _r:" + mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("_icrd_resu")



            '        For Each lDr As DataRow In _
            'mdsdatos.Tables(mstrTablaInspecCriaDeta).Select("icrd_id=" & _
            '    grdProd.Items(dgItem.ItemIndex).Cells(0).Text)
            '            mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("icrd_resu") = False
            '            mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("_icrd_resu") = "No"



            '        Next



        End If
        mActualizarEtiq()

        mdsdatos.AcceptChanges()


    End Sub

    Protected Sub mActualizarGrilla(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

        


        For Each oDataItem As DataGridItem In grdProd.Items


            If UCase(oDataItem.Cells(5).Text) = "NO" Then

                '               CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = False
                For Each lDr As DataRow In _
                    mdsdatos.Tables(mstrTablaInspecCriaDeta).Select("icrd_id=" & _
                    oDataItem.Cells(0).Text)
                    'mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("icrd_resu") = False
                    'mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("_icrd_resu") = "No"


                    Label1.Text = "chksel_oncanged " + oDataItem.Cells(0).Text + _
                    " esta no checked" + " r:" + ldr.Item("icrd_resu").tostring() + _
                    " _r:" + ldr.Item("_icrd_resu") + oDataItem.Cells(5).Text

                Next
            Else

                '               CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = True
                For Each lDr As DataRow In _
                    mdsdatos.Tables(mstrTablaInspecCriaDeta).Select("icrd_id=" & _
                    oDataItem.Cells(0).Text)

                    Label1.Text = "chksel_oncanged " + oDataItem.Cells(0).Text + _
                    " esta no checked" + " r:" + ldr.Item("icrd_resu").tostring() + _
                    " _r:" + ldr.Item("_icrd_resu") + oDataItem.Cells(5).Text

                    'mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("icrd_resu") = True
                    'mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows(0).Item("_icrd_resu") = "S�"

                Next
            End If
        Next

        mActualizarEtiq()
        mdsdatos.AcceptChanges()

    End Sub
    


    Private Sub mActualizarEtiq()
        Try

            Dim intSeleccionados As Integer = 0
            Dim intTotalProductos As Integer = 0
            Dim intNoSeleccionados As Integer = 0


            For Each lDr As DataRow In mdsdatos.Tables(mstrTablaInspecCriaDeta).Select()
                intTotalProductos = intTotalProductos + 1

                Select Case lDr.Item("icrd_resu")
                    Case True
                        intSeleccionados = intSeleccionados + 1
                    Case False
                        intNoSeleccionados = intNoSeleccionados + 1
                End Select
            Next

            lblTotalProductos.Text = "Total Productos:" & intTotalProductos
            lblTotalSeleccionados.Text = "Total Seleccionados:" & intSeleccionados
            lblTotalNoSeleccionados.Text = "Total no Seleccionados:" & intNoSeleccionados

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub btnSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionarTodos.Click
        mChequearTodos()

    End Sub
    Private Sub mChequearTodos()
        Try
            For Each oDataItem As DataGridItem In grdProd.Items
                CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = True
            Next


            For Each dr As DataRow In mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows

                With dr
                    .Item("_icrd_resu") = "S�"
                    .Item("icrd_resu") = True
                End With
            Next


            mActualizarEtiq()



            mdsdatos.AcceptChanges()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try



    End Sub


    Private Sub mDesChequearTodos()

        Try
            For Each oDataItem As DataGridItem In grdProd.Items
                CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = False
            Next

            For Each dr As DataRow In mdsdatos.Tables(mstrTablaInspecCriaDeta).Rows

                With dr
                    .Item("_icrd_resu") = "No"
                    .Item("icrd_resu") = False
                End With
            Next

            mActualizarEtiq()


            mdsdatos.AcceptChanges()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try



    End Sub

    Private Sub btnDeSeleccionarTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeSeleccionarTodos.Click
        mDesChequearTodos()
    End Sub
End Class

End Namespace
