<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AcreBancConf" CodeFile="AcreBancConf.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Confirmaci�n de Acreditaciones Bancarias</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function CargaCuentas(ComboOrigen,ComboDestino,Opc)
		{
			var sFiltro = document.all(ComboOrigen).value;
			if (sFiltro != '')
			{
				LoadComboXML("cuentas_bancos_cargar", sFiltro, ComboDestino, Opc);
			}
		}
		
		function expandir()
		{
			if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</SCRIPT>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" width="100%" cssclass="opcion">Confirmaci�n de Acreditaciones</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																	ImageOver="btnLimp2.gif" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 17px" align="right">
																			<asp:Label id="lblBancFil" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%">
																			<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																			<td>															
																			<cc1:combobox class="combo" id="cmbBancFil" runat="server" cssclass="cuadrotexto" Width="280"
																				AceptaNull="false" onchange="CargaCuentas('cmbBancFil', 'cmbCubaFil','T');" MostrarBotones="False"
																				filtra="true" NomOper="bancos_cargar"></cc1:combobox>
																			</td>
																			<td>
																				<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																												onclick="mBotonBusquedaAvanzada('bancos','banc_desc','cmbBancFil','Bancos','@con_cuentas=1');"
																												alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																			</td>
																			</tr>
																			</table>
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" align="right">
																			<asp:Label id="lblCubaFil" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%">
																			<cc1:combobox class="combo" id="cmbCubaFil" runat="server" Width="50%" AceptaNull="False" nomoper="cuentas_bancos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" align="right">
																			<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 75%">
																			<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" Tabla="Clientes"
																				FilSociNume="True" MuestraDesc="true" FilDocuNume="True" Ancho="600" FilClaveUnica="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" align="right">
																			<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha Deposito:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%">
																			<cc1:DateBox id="txtFechaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" align="right">
																			<asp:Label id="lblNumeFil" runat="server" cssclass="titulo">Nro. Acreditaci�n Bancaria:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%">
																			<CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" Width="270"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<tr>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" align="right"><asp:Label id="lblEmpCobElectFil" runat="server" cssclass="titulo">Empresa Cobro Electr�nica:</asp:Label>&nbsp;</TD>
																		<td style="WIDTH: 75%"><cc1:combobox id="cmbEmpCobElecFil" class="combo" runat="server" Width="260px" obligatorio="True" /></td>
																	</tr>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<tr>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" align="right"><asp:Label id="lblNroCompCobElectFil" runat="server" cssclass="titulo">Nro. Comprobante Cobro Elect.:</asp:Label>&nbsp;</TD>
																		<td style="WIDTH: 75%"><CC1:TEXTBOXTAB id="txtNroCompCobElectFil" runat="server" cssclass="cuadrotexto" Width="270"></CC1:TEXTBOXTAB></TD>
																	</tr>
																	<TR>
																		<TD style="WIDTH: 25%;" align="right">
																		<TD style="WIDTH: 75%">
																			<asp:checkbox id="chkRech" CssClass="titulo" Runat="server" Visible=false Text="Traer rechazadas durante el d�a"></asp:checkbox>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%;" align="right">
																		<TD style="WIDTH: 75%">
																			<asp:checkbox id="chkApro" CssClass="titulo" Runat="server" Visible=False Text="Traer Confirmadas"></asp:checkbox>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Checked=<%#DataBinder.Eval(Container, "DataItem.chk")%> Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Seleccionar Acreditaci�n" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="paco_id"></asp:BoundColumn>
											<asp:BoundColumn visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_acre_depo_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
												ItemStyle-HorizontalAlign="Center" HeaderText="F. Deposito"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_cemi_nume" ItemStyle-HorizontalAlign="Right" HeaderText="C.Emisor"></asp:BoundColumn>
											<asp:BoundColumn DataField="banc_desc" HeaderText="Banco"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_orig_banc_suc" HeaderText="Sucursal"></asp:BoundColumn>
											<asp:BoundColumn DataField="cuba_desc" HeaderText="Cuenta"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_nume" HeaderText="N�mero"></asp:BoundColumn>
											<asp:BoundColumn DataField="clie_apel" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="30">
								<TD colspan="3">
									<asp:Button id="btnConf" runat="server" cssclass="boton" Width="184px" Text="Confirmar Acreditaciones"
										CausesValidation="False"></asp:Button></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
										BorderStyle="Solid">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
												<TD vAlign="top" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
														ToolTip="Cerrar"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="2">
													<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro.Acred.:</asp:Label>&nbsp;</TD>
															<TD colSpan="3">
																<cc1:textboxtab id="txtNume" runat="server" cssclass="cuadrotextodeshab" Width="100px" enabled="false"></cc1:textboxtab></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblMone" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
															<TD colspan=2>
																<cc1:combobox class="combo" id="cmbMone" runat="server" cssclass="cuadrotextodeshab" Width="90px"
																	enabled="false" Obligatorio="True"></cc1:combobox>
																	<asp:Label id="Label1" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="True" EsDecimal="true"></cc1:numberbox></TD>

														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha Valor:</asp:Label>&nbsp;</TD>
															<TD nowrap>
																<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="true"></cc1:DateBox>
																	<asp:Label id="lblDepoFecha" runat="server" cssclass="titulo">Fecha Dep�sito:</asp:Label>
																	<cc1:DateBox id="txtDepoFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="true"></cc1:DateBox>	
																<asp:Label id="Label2" runat="server" cssclass="titulo">Fecha Ingreso:</asp:Label>&nbsp;																															
															</TD>
															
															<TD>
																<cc1:DateBox id="txtIngrFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="true"></cc1:DateBox></TD>
														</TR>
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 9px" vAlign="top" align="right">
																<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
															<TD style="HEIGHT: 9px" colSpan="2">
																<CC1:TEXTBOXTAB id="txtClie" enabled=False runat="server" cssclass="cuadrotexto" Width="420"></CC1:TEXTBOXTAB>
																<UC1:CLIE id="usrClie" runat="server" AceptaNull="false" Tabla="Clientes" Saltos="1,1" FilSociNume="True"
																	MuestraDesc="true" FilDocuNume="True" Ancho="600" Activo="False"></UC1:CLIE></TD>
														</TR>
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 9px" vAlign="top" align="right">
																<asp:Label id="lblCentroEmisor" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
															<TD style="HEIGHT: 9px" colSpan="3">
																<cc1:combobox class="combo" id="cmbCentroEmisor" runat="server" Width="40%" AceptaNull="true"
																	enabled="false"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblBanc" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
															<TD colspan=2>
																<cc1:combobox class="combo" id="cmbBanc" runat="server" Width="240px" AceptaNull="false" enabled="false"></cc1:combobox>&nbsp;&nbsp;
															<asp:Label id="Label4" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;<cc1:combobox class="combo" id="cmbCuba" runat="server" Width="180px" enabled="false"></cc1:combobox>
															</TD>
															
												    	</TR>
														<TR>
															<TD align="right" nowrap>
																<asp:Label id="lblBancOrig" runat="server" cssclass="titulo">Banco Origen:</asp:Label>&nbsp;
															</TD>
															<TD colspan=2>
																<cc1:combobox class="combo" id="cmbBancOrig" runat="server" Width="240px" enabled="false"></cc1:combobox>
																<asp:Label id="lblSucursal" runat="server" cssclass="titulo">Sucursal:</asp:Label>&nbsp;
																<cc1:textboxtab id="txtSucursal" runat="server" cssclass="cuadrotextodeshab" Width="140px" enabled="false"></cc1:textboxtab>
															</TD>
																												
														</TR>
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR style="VISIBILITY: hidden">
															<TD align="right" colspan=4 background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD colspan=6>
																<table border=0>
																<tr>
																<td width=20></td>
																<td colspan=2>
																	<asp:Label id="Label3" runat="server" cssclass="titulo">Datos de la Anulaci�n:</asp:Label>
																</td>
																</tr>
																<tr>
																<td></td>
																<td valign=top>
																	<asp:Label id="lblMoti" runat="server" cssclass="titulo">Motivo:</asp:Label>&nbsp;
																</td>
																<td valign=top>
																	<cc1:textboxtab id="txtMoti" TextMode="MultiLine" runat="server" enterportab=false cssclass="cuadrotexto" Width="500px" height=60 enabled="true"></cc1:textboxtab>
																</td>
																</tr>
																</table>
															</TD>
														</TR>
														<TR style="VISIBILITY: hidden">
															<TD align="right">
																<asp:Label id="lblConc" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:combobox class="combo" id="cmbConc" runat="server" cssclass="cuadrotexto" Width="230px" NomOper="cuentas_cargar"
																	filtra="true" MostrarBotones="False" Obligatorio="false"></cc1:combobox></TD>
															<TD align="right">
																<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="200px" NomOper="cuentas_cargar"
																	filtra="true" MostrarBotones="False" Obligatorio="false"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD vAlign="middle" align="center" colSpan="4" height="5">
																<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
									<ASP:PANEL id="panBotones" Runat="server">
										<TABLE width="100%">
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Anular" CausesValidation="False"></asp:Button>&nbsp;
													<asp:Button id="btnModiComp" runat="server" cssclass="boton" Width="80px" Visible=false Enabled=false Text="Rechazar" CausesValidation="False"></asp:Button>&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Cerrar" CausesValidation="False"></asp:Button></TD>
											</TR>
										</TABLE>
									</ASP:PANEL>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnEmctId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		//if (document.all["cmbConc"]!= null&&!document.all["cmbConc"].disabled)
		//	document.all["cmbConc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
