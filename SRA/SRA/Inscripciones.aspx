<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Inscripciones" CodeFile="Inscripciones.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Inscripciones</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript">

			/*
		function txtAnio_Change(pAnio, pInse)
		{
		    var sFiltro = pInse + "," ;
		    
		    if (pAnio.value=="")
		       sFiltro =  sFiltro + "-1";
		      else 
		       sFiltro = sFiltro + pAnio.value ;
		       
		    LoadComboXML("ciclos_cargar", sFiltro, "cmbCiclo", "S");
		    
		    mLimpiarPeri();
		}
		*/
		function txtAnioFil_Change(pAnio, pInse)
		{
		    var sFiltro = pInse + "," ;
		    
		    if (pAnio.value=="")
		       sFiltro =  sFiltro + "-1";
		      else 
		       sFiltro = sFiltro + pAnio.value ;
		       
		    LoadComboXML("ciclos_cargar", sFiltro, "cmbCicloFil", "S");
		}
		
		function mSetearMaterias(pstrTipo)
		{
			document.all["cmbMate"].disabled=pstrTipo=="C";
			document.all["lstMate"].disabled=pstrTipo=="M";
			if(document.all["cmbMate"].disabled)
				document.all["cmbMate"].selectedIndex=-1;
			
			if(!document.all["lstMate"].disabled)
			{
				var arrElements = document.getElementsByTagName('span'); 
				for (var i = 0; i < arrElements.length; i++) {
					arrElements[i].disabled=false;
				}
				
				arrElements = document.getElementsByTagName('input'); 
				for (var i = 0; i < arrElements.length; i++) {
					arrElements[i].disabled=false;
				}
			}
		}
		
		function mLimpiarPeri()
		{
		 document.all["txtPerio"].value="";
		 
		 document.all["cmbMate"].length=0;
		 if(document.all["lstMate"].children.length>0)
			document.all["lstMate"].removeChild(document.all["lstMate"].children(0));
		}
		
		
		function usrAlum_onchange()
		{
			var sFiltro=document.all["usrAlum:txtId"].value;
			var vstrRet;
			
			document.all("cmbDA").value=0;
			document.all("cmbBeca").value=0;
			
			
			if (sFiltro!="")
			{
			
				vstrRet = LeerCamposXML("alumnos", sFiltro, "alum_fact_clie_id,_tacl_nume,_tacl_vcto_fecha,_tacl_tarj_id,_alum_beca_id").split("|");
			   
			}
			
			if(vstrRet!=undefined)
			{
				document.all["usrFactClie:txtCodi"].value=vstrRet[0];
				document.all["usrFactClie:txtCodi"].onchange();
				if(vstrRet[1]!="")
				{
					document.all("cmbDA").value=1;
					document.all("cmbTarj").value = vstrRet[3];
					document.all("txtNro").value = vstrRet[1];
					document.all("txtVctoFecha").value = vstrRet[2];
					
					
				}
				document.all("cmbBeca").value = vstrRet[4];
				
			}
			cmbDA_Change();
		}
		
	
		function cmbDA_Change()
		{
			var sFiltro=document.all("cmbDA").value;
			gHabilitarControl(document.all("cmbTarj"), sFiltro=="1");
			// Dario se comenta y se oculta  el tr de documentacion que se 
			// muestra solo con el cambio del combo tarjeta
			//gHabilitarControl(document.all("txtNro"), sFiltro=="1");
			//ActivarFecha("txtVctoFecha", sFiltro=="1");
			gHabilitarControl(document.all("txtNro"), false);
			ActivarFecha("txtVctoFecha", false);
			document.all("trDocu").style.display="none";
		}
		
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}

		function Imprimir()
		{
			if(document.all("hdnCompId").value!="")
			{
				if(window.confirm("�Desea imprimir la factura Nro " + document.all("hdnCompNume").value + "?"))
				{
					try
					{
						//Original
						var valParam=document.all("hdnCompId").value + ";0;Original";
						
					  var sRet = ImprimirReporte("Factura","comp_id;random;estado", valParam, "<%=Session("sImpreTipo")%>",document.all("hdnCompId").value,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
						if(sRet=="0") 
						{
							document.all("hdnImprId").value = '';
							document.all("hdnCompId").value="";
							return;
						}
						
						//Duplicado
						var vstrRet = LeerCamposXML("emisores_ctros", document.all("hdnCtroEmis").value, "emct_dupl").split("|");
						var valParam=document.all("hdnCompId").value + ";0;Duplicado";
						for (var i=1; i <= vstrRet[0]; i++)
						{
							var sRet = ImprimirReporte("Factura","comp_id;random;estado", valParam, "<%=Session("sImpreTipo")%>",document.all("hdnCompId").value,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							if(sRet=="0") 
							{
								document.all("hdnCompId").value="";
								return;
							}
						}
						

						if (window.confirm("�Se imprimi� la factura correctamente?"))
							document.all("hdnCompId").value= "-" + document.all("hdnCompId").value;
						else
							document.all("hdnCompId").value="";
							
						document.all("hdnImprId").value = '';
					}
					catch(e)
					{
						document.all("hdnImprId").value = '';
						alert("Error al intentar efectuar la impresi�n.");
					}
				}
				else
					document.all("hdnCompId").value="";
				
				__doPostBack('hdnCompId','');
			}
		}
		
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');Imprimir();" leftMargin="5" rightMargin="0"
		topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="2"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Inscripciones</asp:label></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
										BorderStyle="Solid">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="3" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="60px" AceptaNull="False"
																				MaxLength="4" MaxValor="2099"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Ciclo:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbCicloFil" class="combo" runat="server" Width="100%" AceptaNull="false" NomOper="ciclos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" vAlign="top" background="imagenes/formfdofields.jpg"
																			align="right">
																			<asp:Label id="lblLegajoFil" runat="server" cssclass="titulo">Legajo:</asp:Label>&nbsp;<BR>
																			<asp:Label id="lboAlumFil" runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;
																		</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrAlumFil" runat="server" AceptaNull="false" Tabla="Alumnos" Saltos="1,1,1"
																				FilLegaNume="True" FilDocuNume="True" FilCUIT="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="3" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="10" colSpan="3"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="9px"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="insc_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="_anio" HeaderText="A&#241;o"></asp:BoundColumn>
											<asp:BoundColumn DataField="insc_insc_fecha" HeaderText="Fecha Incrip." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_ciclo" HeaderText="Ciclo"></asp:BoundColumn>
											<asp:BoundColumn DataField="insc_perio" HeaderText="Per."></asp:BoundColumn>
											<asp:BoundColumn DataField="_alumno" HeaderText="Alumno"></asp:BoundColumn>
											<asp:BoundColumn DataField="_mate_descrip" HeaderText="Materia"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="insc_beca_id" HeaderText="insc_beca_id"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" border="0" cellPadding="0" align="left">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ToolTip="Agregar una Nueva Inscripci�n"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD width="50" align="center"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" BorderWidth="1px"
										BorderStyle="Solid" Height="116px">
										<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
											align="left">
											<TR>
												<TD>
													<P></P>
												</TD>
												<TD height="5">
													<asp:label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:label></TD>
												<TD vAlign="top" align="right">&nbsp;
													<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%; HEIGHT: 225px" colSpan="3" align="center">
													<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
														<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
															<TR>
																<TD style="WIDTH: 120px; HEIGHT: 22px" width="120" align="right">
																	<asp:Label id="lblAnio" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 22px">
																	<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0">
																		<TR>
																			<TD width="15%" align="left">
																				<cc1:numberbox id="txtAnio" runat="server" cssclass="cuadrotexto" Width="60px" MaxLength="4" MaxValor="2099"
																					Obligatorio="true" AutoPostBack="true"></cc1:numberbox></TD>
																			<TD style="WIDTH: 180px" vAlign="top" width="180" align="right">
																				<asp:Label id="lbl" runat="server" cssclass="titulo">Fecha de inscripci�n:</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id="lblFechaInsc" runat="server" cssclass="titulo" Width="60px"></asp:Label></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD style="WIDTH: 120px; HEIGHT: 15px" align="right">
																	<asp:Label id="lblCiclo" runat="server" cssclass="titulo">Ciclo:</asp:Label>&nbsp;
																</TD>
																<TD style="HEIGHT: 15px" align="left">
																	<cc1:combobox id="cmbCiclo" class="combo" runat="server" Width="100%" Obligatorio="True" onchange="mLimpiarPeri();"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 120px; HEIGHT: 20px" align="right">
																	<asp:Label id="lblPeri" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;</TD>
																<TD style="WIDTH: 88%">
																	<cc1:numberbox id="txtPerio" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="100"
																		Obligatorio="true" autopostback="true"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 120px; HEIGHT: 41px" width="120" align="right">&nbsp;</TD>
																<TD style="HEIGHT: 41px">
																	<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0">
																		<TR>
																			<TD vAlign="top" width="5%" align="left">
																				<asp:RadioButton id="rbtnCarr" onclick="mSetearMaterias('C');" runat="server" cssclass="titulo" Checked="True"
																					Text="Carrera" GroupName="RadioGroup1"></asp:RadioButton></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" width="100%">
																				<cc1:checklistbox id="lstMate" runat="server" width="100%" CellSpacing="0" CellPadding="0" Obligatorio="true"
																					RepeatColumns="2" CssClass="chklst" MuestraBotones="False"></cc1:checklistbox></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" width="5%" align="left">
																				<asp:RadioButton id="rbtnMate" onclick="mSetearMaterias('M');" runat="server" cssclass="titulo" Text="Materia"
																					GroupName="RadioGroup1"></asp:RadioButton></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 180px" align="left">
																				<cc1:combobox id="cmbMate" class="combo" runat="server" Width="304px" NomOper="ciclos_materias_cargar"
																					NomOpe="ciclos_materias_cargar"></cc1:combobox></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD style="WIDTH: 120px; HEIGHT: 4px" vAlign="top" align="right">
																	<asp:Label id="lblAlum" runat="server" cssclass="titulo">Legajo:</asp:Label>&nbsp;<BR>
																	<asp:Label id="Label1" runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;
																</TD>
																<TD style="HEIGHT: 4px">
																	<UC1:CLIE id="usrAlum" runat="server" Tabla="Alumnos" Saltos="1,1,1" FilLegaNume="True" FilDocuNume="True"
																		FilCUIT="True" Obligatorio="true"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 120px" vAlign="top" width="120" align="right">
																	<asp:Label id="lblFactClie" runat="server" cssclass="titulo" Width="108px">Cliente Fact.:</asp:Label>&nbsp;
																</TD>
																<TD>
																	<UC1:CLIE id="usrFactClie" runat="server" Tabla="Clientes" Saltos="1,1,1" FilLegaNume="True"
																		FilDocuNume="True" FilCUIT="True" Obligatorio="true" MuestraDesc="False"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 120px; HEIGHT: 24px" align="right">
																	<asp:Label id="lblPlanPago" runat="server" cssclass="titulo" Width="101px">Plan de Pago:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 24px">
																	<cc1:combobox id="cmbPlanPago" class="combo" runat="server" Width="304px" Obligatorio="True"></cc1:combobox>&nbsp;&nbsp;
																	<asp:CheckBox id="chkGene" Checked="true" Text="Generar Factura" CssClass="titulo" Runat="server"></asp:CheckBox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 120px; HEIGHT: 24px" align="right">
																	<asp:Label id="lblBeca" runat="server" cssclass="titulo">Beca:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:combobox id="cmbBeca" class="combo" runat="server" Width="304px"></cc1:combobox>&nbsp;&nbsp;
																	<asp:CheckBox id="chkCtaCte" Checked="False" Text="Excepci�n Cta. Cte." CssClass="titulo" Runat="server"></asp:CheckBox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD colSpan="2" align="left">
																	<TABLE style="MARGIN-TOP: 10px" cellSpacing="0" cellPadding="0" width="50%">
																		<TR>
																			<TD style="WIDTH: 140px; HEIGHT: 24px" vAlign="top" align="right">
																				<asp:Label id="Label2" runat="server" cssclass="titulo" Width="120px">D.A.:</asp:Label>&nbsp;
																			</TD>
																			<TD vAlign="top" align="left">
																				<cc1:combobox id="cmbDA" class="combo" runat="server" Width="60px" Obligatorio="True" onchange="cmbDA_Change();"></cc1:combobox></TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;
																			</TD>
																			<TD vAlign="top" align="left">
																				<cc1:combobox id="cmbTarj" class="combo" runat="server" Width="200px" AceptaNull="False" Obligatorio="True"
																					OnSelectedIndexChanged="cmbTarj_SelectedIndexChanged" AutoPostBack="True"></cc1:combobox></TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblNrorunat" runat="server" cssclass="titulo" Width="80px">N�mero:</asp:Label>&nbsp;
																			</TD>
																			<TD vAlign="top" align="left">
																				<cc1:numberbox id="txtNro" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="true"
																					EsTarjeta="true"></cc1:numberbox></TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblVctoFecha" runat="server" cssclass="titulo" Width="100px">Fecha Vto.:</asp:Label>&nbsp;
																			</TD>
																			<TD vAlign="top" width="100" colSpan="2" align="left">
																				<cc1:DateBox id="txtVctoFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR style="WIDTH: 100%; DISPLAY: none" id="trDocu" runat="server">
																			<TD style="WIDTH: 140px; HEIGHT: 24px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:label id="lblCuit" runat="server" cssclass="titulo">CUIT/CUIL:</asp:label>&nbsp;
																			</TD>
																			<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																				<CC1:CUITBOX id="txtCuit" runat="server" cssclass="cuadrotexto" Width="100px" AceptaNull="False"></CC1:CUITBOX></TD>
																			<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg" align="right">
																				<asp:label id="lblDocu" runat="server" cssclass="titulo">Nro. Documento:</asp:label>&nbsp;
																			</TD>
																			<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg" colSpan="6">
																				<cc1:combobox id="cmbDocuTipo" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:combobox>
																				<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="143px" MaxValor="9999999999999"
																					esdecimal="False"></cc1:numberbox></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</asp:panel>
													<asp:label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:label></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
													align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR height="30">
												<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
													<asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;
													<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Baja"></asp:button>&nbsp;&nbsp;
													<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Modificar"></asp:button>&nbsp;&nbsp;
													<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Limpiar"></asp:button></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server" Width="140px"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnCompId" runat="server" AutoPostBack="True"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnCompNume" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnCtroEmis" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnImprId" runat="server"></ASP:TEXTBOX>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
