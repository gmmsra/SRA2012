<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CobranzasComprobACta_Pop" CodeFile="CobranzasComprobACta_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Aplicaci�n de Cr�ditos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!-----------------CONTENIDO---------------------->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
							<TR>
								<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:label></TD>
								<TD vAlign="top" align="right" colspan="5">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"></asp:imagebutton></TD>
							</TR>
							<tr>
								<td colSpan="6">
									<table>
										<TR>
											<TD align="right" width="15%"><asp:label id="lblCabeTotalApl" runat="server" cssclass="titulo">Total a Pagar:</asp:label>&nbsp;
											</TD>
											<TD width="15%"><asp:label id="lblCabeTotalAplic" runat="server" cssclass="titulo"></asp:label></TD>
											<TD align="right" width="20%"><asp:label id="lblCabeTotalPag" runat="server" cssclass="titulo">Total Pagado:</asp:label>&nbsp;
											</TD>
											<TD width="15%"><asp:label id="lblCabeTotalPago" runat="server" cssclass="titulo"></asp:label></TD>
											<TD align="right" width="10%"><asp:label id="lblCabeTotalDif" runat="server" cssclass="titulo">Diferencia:</asp:label>&nbsp;
											</TD>
											<TD width="10%"><asp:label id="lblCabeTotalDifer" runat="server" cssclass="titulo"></asp:label></TD>
										</TR>
									</table>
								</td>
							</tr>
							<TR height="30">
								<TD vAlign="top" colspan="6">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" border="0">
										<TR>
											<TD>
												<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label></TD>
											<td width="100%">
												<UC1:CLIE id="usrClie" runat="server" width="100%" FilCUIT="True" Tabla="Clientes" Saltos="1,1,1"
													autopostback="true" AceptaNull="False" FilClieNume="True" FilSociNume="True"
													MuestraDesc="False" FilDocuNume="True"></UC1:CLIE></td>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" colspan="6"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px"
										PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="15px"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server" onclick="chkSelCheck(this);"></asp:CheckBox>
													<DIV runat="server" id="divTotal" style="DISPLAY: none"><%#(100*DataBinder.Eval(Container, "DataItem.saldo"))%></DIV>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="sact_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="comp_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="numero" HeaderText="Comprobante"></asp:BoundColumn>
											<asp:BoundColumn DataField="sact_impo" DataFormatString="{0:F2}" HeaderText="Importe Ori." HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="saldo" DataFormatString="{0:F2}" HeaderText="Saldo" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="acti_desc" HeaderText="Actividad"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="A Aplicar">
												<HeaderStyle Width="65px"></HeaderStyle>
												<ItemTemplate>
													<CC1:NUMBERBOX id="txtPagado" runat="server" cssclass="cuadrotexto" Width="65px" EsDecimal="True"
														onchange="txtPagado_change(this);"></CC1:NUMBERBOX>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderStyle-Width="16px" HeaderText="Aut.">
												<ItemTemplate>
													<asp:CheckBox ID="chkAuto" onclick="mAutoriza(this);" Runat="server"></asp:CheckBox>
													<DIV runat="server" id="divSactId" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.sact_id")%></DIV>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="right" width="200">
									<asp:Label id="lblTotal" runat="server" cssclass="titulo">Total Cr�dito Disponible:</asp:Label>&nbsp;</TD>
								<TD>
									<CC1:NUMBERBOX id="txtTotal" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="65px"></CC1:NUMBERBOX></TD>
								<TD align="right">
									<asp:Label id="lblTotalSel" runat="server" cssclass="titulo">Total Seleccionado:</asp:Label>&nbsp;</TD>
								<TD>
									<CC1:NUMBERBOX id="txtTotalSel" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="65px"></CC1:NUMBERBOX></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="2">
									<BUTTON runat="server" class="boton" id="btnAutoUsua" style="WIDTH: 115px" onclick="btnAutoUsua_click();"
										type="button" value="Detalles">Autorizante</BUTTON>
									<asp:Label id="lblAutoUsuaTit" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
								</TD>
								<TD vAlign="middle" align="right" height="40" colspan="2">
									<asp:button id="btnAceptar" Width="80px" cssclass="boton" runat="server" Text="Aceptar"></asp:button></TD>
							</TR>
						</TABLE>
					<!----------------CONTENIDO-------------->
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnLoginPop" runat="server" onchange="hdnLoginPop_change();"></asp:textbox>
				<asp:Label id="lblSess" runat="server"></asp:Label>
			</DIV>
		</form>
		<SCRIPT language="javascript">
	function chkSelCheck(pChk)
	{
		var dTotal=0;
		var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
		var lbooChecked = pChk.checked;
		var oText = document.all(pChk.id.replace("chkSel","txtPagado"));
		var sImporte;
		
		ActivarControl(oText, lbooChecked);
		
		if(!lbooChecked)
			oText.value = "0";
		else
		{
			dTotal = eval(document.all("grdConsulta__ctl" + lintFil.toString() + "_divTotal").innerText);
			dTotal = dTotal.toString();
			//oText.value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2, dTotal.length );
		}
		
		mCalcularTotales();
	}
	
	function txtPagado_change(pTxt)
	{	
		var sImporte = pTxt.value;
		var iComa;
		
		if(sImporte=="")
			sImporte = "0.00";
			
		sImporte = sImporte.replace(",",".");
		iComa = sImporte.indexOf(".");
		if (iComa==-1)
			sImporte += ".00";
		
		if (iComa==sImporte.length-1)
			sImporte += "00";
			
		if (iComa==sImporte.length-2)
			sImporte += "0";
		pTxt.value = sImporte;

		mCalcularTotales();
	}
	
	function mCalcularTotales()
	{
		var dTotal=0;
		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
				{
					//document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value = gRedondear(document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value,2);
					//dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value.replace(".",""));
					dTotal += gRedondear(document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value,2);
				}
			}
		}
		dTotal = dTotal.toString();

		if (dTotal==0)
			document.all("txtTotalSel").value = "0.00"
		else
			document.all("txtTotalSel").value = gRedondear(dTotal,2);
		if (document.all("txtTotalSel").value.indexOf('.')==-1)
			document.all("txtTotalSel").value = document.all("txtTotalSel").value+'.00';
	}
	
	function btnAutoUsua_click()
	{
		gAbrirVentanas("Login_pop.aspx", 3, "200","110");
	}

	function hdnLoginPop_change()
	{
		document.all("lblAutoUsuaTit").innerText = LeerCamposXML("usuarios", "0" + document.all("hdnLoginPop").value, "usua_user");
	}
	
	function mAutoriza(pChk)
	 {
	  try{
	        var pstrId = document.all(pChk.id.replace("chkAuto","divSactId")).innerText;

	        var lbooChecked = 0;
	       	if (pChk.checked)
	     	    lbooChecked = 1;
		    var sFiltro = pstrId + ";" + lbooChecked + ";0" + document.all("hdnLoginPop").value + ";" + document.all("lblSess").innerText;
		    
		    var sRet=EjecutarMetodoXML("Utiles.AutorizarSaldoCtaActi", sFiltro);
		    if(sRet=="0" && lbooChecked==1)
		    {
				pChk.checked=false
				alert("El usuario no tiene permiso para autorizar");
			}
	     }	
	 	catch(e)
		{
		 alert("Error al autorizar");
		}
	 }
		</SCRIPT>
	</BODY>
</HTML>
