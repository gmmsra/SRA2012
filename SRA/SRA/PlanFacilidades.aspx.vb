
Namespace SRA

Partial Class PlanFacilidades '0002
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	Protected WithEvents lblNumeFil As System.Web.UI.WebControls.Label
	Protected WithEvents txtNumeFil As NixorControls.NumberBox
	Protected WithEvents lblImpo As System.Web.UI.WebControls.Label
	Protected WithEvents txtImpo As NixorControls.NumberBox
	Protected WithEvents lblImpoOrig As System.Web.UI.WebControls.Label
	Protected WithEvents txtImpoOrig As NixorControls.NumberBox
	Protected WithEvents lblTaclNume As System.Web.UI.WebControls.Label
	Protected WithEvents Numberbox1 As NixorControls.NumberBox
	Protected WithEvents Label2 As System.Web.UI.WebControls.Label
	Protected WithEvents Numberbox2 As NixorControls.NumberBox
	Protected WithEvents Label3 As System.Web.UI.WebControls.Label
	Protected WithEvents Numberbox3 As NixorControls.NumberBox
	Protected WithEvents Label4 As System.Web.UI.WebControls.Label
	Protected WithEvents Numberbox4 As NixorControls.NumberBox
	Protected WithEvents txtGeneFechaCuota As NixorControls.DateBox




	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

#Region "Definici�n de Variables"
	Private mstrTabla As String = SRA_Neg.Constantes.gTab_PlanPagos
	Private mstrPlanPagosConcep As String = SRA_Neg.Constantes.gTab_PlanPagosConcep
	Private mstrPlanPagosDeta As String = SRA_Neg.Constantes.gTab_PlanPagosDeta
	Private mstrPlanPagosVtos As String = SRA_Neg.Constantes.gTab_PlanPagosVtos
    Private mstrTipo As String
	Private mstrConn As String
	Private mstrParaPageSize As Integer

	Private mdsDatos As DataSet

    Private mintColuCtacInte As Integer = 5
    Private mintColuSociInte As Integer = 7

	Private Enum ColumnasCuota As Integer
		Id = 0
	End Enum

	Private Enum ColumnasDeudas As Integer
		ChkPlan = 0
		CompId = 1
		CompNeto = 3
		Anio = 4
		Perio = 5
		PetiId = 6
		CompInteImpo = 7
		Descuento = 8
		ConcId = 9
		CtacInte = 5
		VtoId = 6
	End Enum
#End Region

#Region "Inicializaci�n de Variables"
	Private Sub mInicializar()
		Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
		grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
		grdCuotas.PageSize = Convert.ToInt32(mstrParaPageSize)
		grdDeudaCtac.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdDeudaSoc.PageSize = Convert.ToInt32(mstrParaPageSize)
        mstrTipo = Request("Tipo").ToString()

        If mstrTipo = "C" Then
            txtAproSociFecha.Enabled = False
            lnkDeudaSoc.Visible = False
        Else
            lnkDeudaCtac.Visible = False
            txtAproCtacFecha.Enabled = False
            cmbActividad.SelectedIndex = cmbActividad.Items.IndexOf(cmbActividad.Items.FindByValue(2))
            cmbActividad.Enabled = False
        End If
	End Sub
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()
			If (Not Page.IsPostBack) Then
				Session(mstrTabla) = Nothing
				mdsDatos = Nothing

				mSetearMaxLength()
				mSetearEventos()
				mCargarCombos()

					txtGeneFecha.Fecha = Today
					txtGeneFecha.Enabled = False
				txtAproSociFecha.Enabled = False
				txtAproCtacFecha.Enabled = False
				txtAproFecha.Enabled = False
				txtCompGene.Enabled = False

				mConsultar()
				clsWeb.gInicializarControles(Me, mstrConn)
			Else
				mCargarCentroCostos(False)
				mdsDatos = Session(mstrTabla)
				mSetearTotales()
			End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mSetearEventos()
		btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
		btnDeudaCtacAprob.Attributes.Add("onclick", "if(!confirm('Confirma la aprobaci�n del pago de la deuda de Cta.Cte.?')) return false;")
		btnDeudaSocAprob.Attributes.Add("onclick", "if(!confirm('Confirma la aprobaci�n del pago de la deuda social?')) return false;")
		btnConf.Attributes.Add("onclick", "if(!confirm('Confirma la generaci�n del comprobante?')) return false;")
	End Sub

	Private Sub mSetearMaxLength()
		Dim lstrLong As Object
		lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
		txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "plpa_obs")
	End Sub

	Private Sub mSetearTotales()
		txtDeudaSocPag.Text = hdnDeudaSocPag.Text
		txtDeudaSocPlan.Text = hdnDeudaSocPlan.Text
		txtDeudaSocDife.Text = hdnDeudaSocDife.Text
		txtDeudaSocCuot.Text = hdnDeudaSocCuot.Text
		txtDeudaSocInte.Text = hdnDeudaSocInte.Text
		txtDeudaSocDesc.Text = hdnDeudaSocDesc.Text

		If txtDeudaSocPlan.Text <> txtDeudaSocTotal.Text And txtDeudaSocPlan.Text <> "0.00" Then
			txtDeudaSocTotal.Text = txtDeudaSocPlan.Text
			txtCuotDeudaSoc.Text = txtDeudaSocPlan.Text
			txtConcDeudaSoc.Text = txtDeudaSocPlan.Text
		ElseIf hdnDeudaSocCuot.Text = "0.00" And txtDeudaSocPlan.Text = "0.00" And txtAproFecha.Text = "" Then
			txtDeudaSocTotal.Text = txtDeudaSocPlan.Text
			txtCuotDeudaSoc.Text = txtDeudaSocPlan.Text
			txtConcDeudaSoc.Text = txtDeudaSocPlan.Text
		Else
			txtDeudaSocTotal.Text = txtDeudaSocTotal.Text
			txtCuotDeudaSoc.Text = txtDeudaSocTotal.Text
			txtConcDeudaSoc.Text = txtDeudaSocTotal.Text
		End If

		txtDeudaCtacPlan.Text = hdnDeudaCtacPlan.Text
		txtConcDeudaCtac.Text = txtDeudaCtacPlan.Text
		txtDeudaCtacDife.Text = hdnDeudaCtacDife.Text
		txtDeudaCtacPag.Text = hdnDeudaCtacPag.Text
		txtCuotDeudaCtac.Text = txtDeudaCtacPlan.Text
		txtConcDeudaCtac.Text = txtDeudaCtacPlan.Text

	End Sub
#End Region

#Region "Seteo de Controles"
	Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, Optional ByVal pbooAproSoci As Boolean = False, Optional ByVal pbooAproCtac As Boolean = False, Optional ByVal pbooAprobado As Boolean = False)
		Select Case pstrTabla
			Case mstrPlanPagosVtos
				btnBajaCuota.Enabled = Not (pbooAlta) And Not pbooAprobado And Not (usrClie.Valor Is DBNull.Value)
				btnModiCuota.Enabled = Not (pbooAlta) And Not pbooAprobado And Not (usrClie.Valor Is DBNull.Value)
				btnAltaCuota.Enabled = pbooAlta And Not pbooAprobado
				grdCuotas.Columns(0).Visible = Not pbooAprobado

			Case mstrPlanPagosDeta
				btnDeudaSocAprob.Enabled = Not pbooAproSoci
				btnDeudaCtacAprob.Enabled = Not pbooAproCtac

			Case mstrPlanPagosConcep
				btnBajaConc.Enabled = Not (pbooAlta) And Not pbooAprobado And Not (usrClie.Valor Is DBNull.Value)
				btnModiConc.Enabled = Not (pbooAlta) And Not pbooAprobado And Not (usrClie.Valor Is DBNull.Value)
				btnAltaConc.Enabled = pbooAlta And Not pbooAprobado
				grdConc.Columns(0).Visible = Not pbooAprobado

			Case Else
                btnBaja.Enabled = Not (pbooAlta) And lblBaja.Text = ""
                btnModi.Enabled = Not (pbooAlta) And Not pbooAprobado And lblBaja.Text = ""
				btnAlta.Enabled = pbooAlta
				btnImpr.Enabled = Not pbooAlta
                btnConf.Enabled = Not pbooAprobado And lblBaja.Text = ""
				usrClie.Activo = pbooAlta
				txtSocio.Enabled = pbooAlta
                If mstrTipo = "C" Then
                    cmbActividad.Enabled = pbooAlta
                End If
                txtValorFecha.Enabled = pbooAlta
                txtObse.Enabled = pbooAlta

                grdCuotas.Columns(0).Visible = Not pbooAprobado
                grdDeudaSoc.Columns(0).Visible = Not pbooAprobado
                grdDeudaCtac.Columns(0).Visible = Not pbooAprobado

                mSetearEditor(mstrPlanPagosDeta, True, pbooAproSoci, pbooAproCtac, pbooAprobado)
                mSetearEditor(mstrPlanPagosConcep, True, pbooAproSoci, pbooAproCtac, pbooAprobado)
                mSetearEditor(mstrPlanPagosVtos, True, pbooAproSoci, pbooAproCtac, pbooAprobado)
		End Select
	End Sub

	Private Sub mCargarCombos()
		clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConc, "S", "@conc_acti_id=" & SRA_Neg.Constantes.Actividades.Socios)
        clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCcos, "N")
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividad, "S")
	End Sub

	Public Sub mCargarDatos(ByVal pstrId As String)
		Dim lbooAproSoci As Boolean
		Dim lbooAproCtac As Boolean
		Dim lbooApro As Boolean

		mCrearDataSet(pstrId)

		With mdsDatos.Tables(mstrTabla).Rows(0)
			hdnId.Text = .Item("plpa_id").ToString()
			usrClie.Valor = .Item("plpa_clie_id")
			txtGeneFecha.Fecha = .Item("plpa_gene_fecha")
			txtSocio.Text = IIf(.Item("_soci_nume") Is DBNull.Value, "", .Item("_soci_nume"))
                txtObse.Valor = .Item("plpa_obse").ToString
                If .Item("plpa_apro_soci_fecha").ToString.Length > 0 Then
                    txtAproSociFecha.Fecha = .Item("plpa_apro_soci_fecha")
                End If
                If .Item("plpa_apro_ctac_fecha").ToString.Length > 0 Then
                    txtAproCtacFecha.Fecha = .Item("plpa_apro_ctac_fecha")
                End If
                If .Item("plpa_apro_fecha").ToString.Length > 0 Then
                    txtAproFecha.Fecha = .Item("plpa_apro_fecha")
                End If
                txtCompGene.Text = .Item("_datos_comp").ToString
                lbooAproSoci = Not .IsNull("plpa_apro_soci_fecha")
                lbooAproCtac = Not .IsNull("plpa_apro_ctac_fecha")
                lbooApro = Not .IsNull("plpa_apro_fecha")
                hdnApro.Text = lbooApro.ToString()

                If Not .IsNull("plpa_acti_id") Then
                    cmbActividad.Valor = .Item("plpa_acti_id")
                End If

                If Not .IsNull("plpa_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("plpa_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

                txtValorFecha.Fecha = .Item("plpa_inte_fecha")
            End With

		hdnSociId.Text = usrClie.SociId.ToString

		mSetearEditor("", False, lbooAproSoci, lbooAproCtac, lbooApro)

        If mstrTipo = "S" Then lnkDeudaSoc.Visible = hdnSociId.Text <> ""

        If Not lbooApro Then
            If hdnSociId.Text <> "" Then
                If lbooAproSoci Then

                    mConsultarDetas(False, lbooAproSoci)
                    mChequearGrilla("Soc")
                Else
                    mConsultarDeudaSoc()
                End If
                'mConsultarDeudaSoc()

                mCalcularTotalesSelec("Soc")

            End If
            mConsultarDetas()
            mConsultarDeudaCtac()
            mChequearGrilla("Ctac")
            mCalcularTotalesDeudaCtac()
            mCalcularTotalesSelec("Ctac")
        Else
            mConsultarDetas(lbooApro)
        End If

        mConsultarConc()
        mConsultarCuota()

        lblTitu.Text = "Registro Seleccionado: " + usrClie.Apel + " - " + CDate(txtGeneFecha.Fecha).ToString("dd/MM/yyyy")

        mMostrarPanel(True)
	End Sub

	Private Sub mAgregar()
		mLimpiar()
		btnBaja.Enabled = False
		btnModi.Enabled = False
		mMostrarPanel(True)
	End Sub

	Private Sub mLimpiar()
        hdnId.Text = ""
        hdnApro.Text = ""
		lblTitu.Text = ""
		usrClie.Limpiar()
		txtCompGene.Text = ""
		txtSocio.Text = ""
			txtGeneFecha.Fecha = Today
			txtValorFecha.Fecha = Today
			txtAproCtacFecha.Text = ""
		txtAproSociFecha.Text = ""
		txtAproFecha.Text = ""
		txtObse.Text = ""

		txtDeudaSocPag.Text = "0.00"
		txtDeudaSocPlan.Text = "0.00"
		txtDeudaSocTotal.Text = "0.00"
		txtDeudaSocInte.Text = "0.00"
		txtDeudaSocCuot.Text = "0.00"
		hdnDeudaSocPag.Text = "0.00"
		hdnDeudaSocPlan.Text = "0.00"
		hdnDeudaSocDife.Text = "0.00"
		hdnDeudaSocDesc.Text = "0.00"

		txtDeudaCtacPag.Text = "0.00"
		txtDeudaCtacPlan.Text = "0.00"
		txtDeudaCtacTotal.Text = "0.00"
		txtSaldoCtaCC.Text = "0.00"
		txtTotalVencCC.Text = "0.00"
		hdnDeudaCtacPag.Text = "0.00"
		hdnDeudaCtacPlan.Text = "0.00"
		hdnDeudaCtacDife.Text = "0.00"
		hdnDeudaSocInte.Text = "0.00"
		hdnDeudaSocCuot.Text = "0.00"

		txtConcDeudaCtac.Text = "0.00"
		txtConcDeudaSoc.Text = "0.00"

		txtCuotasDife.Text = "0.00"
		txtCuotasTotal.Text = "0.00"
		txtCuotDeudaCtac.Text = "0.00"
		txtCuotDeudaSoc.Text = "0.00"
		txtCuotConc.Text = "0.00"
		txtCuotDeudaTotal.Text = "0.00"

		btnDeudaCtacAprob.Enabled = True
		btnDeudaSocAprob.Enabled = True

		lblBaja.Text = ""

		grdCuotas.CurrentPageIndex = 0
		grdDeudaSoc.CurrentPageIndex = 0
		grdDeudaCtac.CurrentPageIndex = 0

        lblDeudaSocInte.Text = "Intereses:"
        grdDeudaCtac.Columns(mintColuCtacInte).HeaderText = "Intereses"
        grdDeudaSoc.Columns(mintColuSociInte).HeaderText = "Intereses"

        grdCuotas.DataSource = Nothing
		grdCuotas.DataBind()
		grdDeudaSoc.DataSource = Nothing
		grdDeudaSoc.DataBind()
		grdDeudaCtac.DataSource = Nothing
		grdDeudaCtac.DataBind()
		Session("grdDeudaSoc") = Nothing
        
		mCrearDataSet("")

		mLimpiarCuota()
		mLimpiarConc()

		mSetearEditor("", True, False)
		mShowTabs(1)
	End Sub
	Private Sub mLimpiarFiltros()
		txtFechaHastaFil.Text = ""
		txtFechaDesdeFil.Text = ""
		usrClieFil.Limpiar()
	End Sub
	Private Sub mCerrar()
		mLimpiar()
		mConsultar()
	End Sub

	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		Dim lbooVisiOri As Boolean = panDato.Visible

		panDato.Visible = pbooVisi
		panBotones.Visible = pbooVisi
		panFiltro.Visible = Not panDato.Visible
		btnAgre.Visible = Not panDato.Visible
		grdDato.Visible = Not panDato.Visible

		If pbooVisi Then
			grdDato.DataSource = Nothing
			grdDato.DataBind()
			mShowTabs(1)
		Else
			Session(mstrTabla) = Nothing
			mdsDatos = Nothing
		End If

		tabLinks.Visible = pbooVisi
	End Sub

	Private Sub mShowTabs(ByVal origen As Byte)
		Try
			If origen <> 1 Then
				mValidarNavega()

			End If

			panDato.Visible = True
			panBotones.Visible = True
			panDeudaSoc.Visible = False
			panDeudaCtac.Visible = False
			panConc.Visible = False
			panCuotas.Visible = False
			panCabecera.Visible = False

			lnkCabecera.Font.Bold = False
			lnkDeudaSoc.Font.Bold = False
			lnkDeudaCtac.Font.Bold = False
			lnkConc.Font.Bold = False
			lnkCuotas.Font.Bold = False

			Select Case origen
				Case 1
					panCabecera.Visible = True
					lnkCabecera.Font.Bold = True
					lblTitu.Text = "Datos de Comisi�n "
				Case 2

					panDeudaSoc.Visible = True
					lnkDeudaSoc.Font.Bold = True
					lblTitu.Text = "Deuda Social de: " & usrClie.Apel

				Case 3

					panDeudaCtac.Visible = True
					lnkDeudaCtac.Font.Bold = True
					lblTitu.Text = "Deuda Cta.Cte. de: " & usrClie.Apel

				Case 4
					mCalcularTotalesConc()
					panConc.Visible = True
					lnkConc.Font.Bold = True
					lblTitu.Text = "Conceptos de: " & usrClie.Apel

				Case 5
					mCalcularTotalesCuotas()
					panCuotas.Visible = True
					lnkCuotas.Font.Bold = True
					lblTitu.Text = "Cuotas para : " & usrClie.Apel
			End Select

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
		mShowTabs(1)
	End Sub

	Private Sub lnkDeudaSoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeudaSoc.Click
		mShowTabs(2)
	End Sub

	Private Sub lnkDeudaCtac_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeudaCtac.Click
		mShowTabs(3)
	End Sub

	Private Sub lnkConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConc.Click
		mShowTabs(4)
	End Sub

	Private Sub lnkCuotas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCuotas.Click
		mShowTabs(5)
	End Sub

	Private Sub mValidarNavega()
		If usrClie.Valor Is DBNull.Value Then
			Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente.")
		End If
	End Sub


	Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
		Dim lstrCta As String = ""
            If cmbConc.Valor.Length > 0 Then
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbConc.Valor)
                lstrCta = lstrCta.Substring(0, 1)
            End If
		If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
			If Not Request.Form("cmbCCos") Is Nothing Or pbooEdit Then
				clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCcos, "id", "descrip", "N")
				If Not Request.Form("cmbCCos") Is Nothing Then
					cmbCcos.Valor = Request.Form("cmbCCos").ToString
				End If
			End If
		Else
			cmbCcos.Items.Clear()

		End If
	End Sub


	Private Sub mValidarConf()
		Dim dTotalPlan As Decimal
		Dim dTotalConc As Decimal
		Dim dTotalVtos As Decimal

		For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosDeta).Select("")
			dTotalPlan += lDr.Item("plde_neto") + lDr.Item("plde_inte_impo") + lDr.Item("plde_desc_impo")
		Next
		For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosConcep).Select("")
			dTotalConc += lDr.Item("plco_impo")
		Next

		For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosVtos).Select("")
			dTotalVtos += lDr.Item("plcu_impo")
		Next

		If dTotalPlan = 0 Then
			Throw New AccesoBD.clsErrNeg("El importe de la deuda seleccionada debe ser mayor que 0.")
		End If

		If dTotalPlan + dTotalConc <> dTotalVtos Then
			Throw New AccesoBD.clsErrNeg("El importe a pagar no coincide con el importe total de las cuotas.")
		End If
	End Sub
#End Region

#Region "Opciones de ABM"
	Private Sub mAlta()
		Try
			Dim ldsTemp As DataSet
			ldsTemp = mGuardarTodo()

			Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTemp)
			lobjGenericoRel.Alta()

			mLimpiar()
			mConsultar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	

	Private Sub mImprimir()
		Try
			Dim params As String
			Dim lstrRptName As String

			lstrRptName = "PlanFacilidades"

			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			'id del plan
			params += "&plpa_id=" + hdnId.Text
			'importes deuda social
			params += "&ds_plan_impo=" + txtDeudaSocPlan.Text
			params += "&ds_apag_impo=" + txtDeudaSocPag.Text
			params += "&ds_dife_impo=" + hdnDeudaSocDife.Text
			'importes deuda cta cte
			params += "&dc_plan_impo=" + txtDeudaCtacPag.Text
			params += "&dc_apag_impo=" + txtDeudaCtacPlan.Text
			params += "&dc_dife_impo=" + hdnDeudaCtacDife.Text
			params += "&dc_afav_impo=" + txtSaldoCtaCC.Text
			params += "&dc_deud_impo=" + txtDeudaCtacTotal.Text
			params += "&dc_venc_impo=" + txtTotalVencCC.Text
			'totales
			params += "&tot_deud_soci_impo=" + txtCuotDeudaSoc.Text
			params += "&tot_deud_ctac_impo=" + txtCuotDeudaCtac.Text
			params += "&tot_desc_impo=" + txtCuotConc.Text

			lstrRpt += params
			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mModi()
		Try
			Dim ldsTemp As DataSet
			ldsTemp = mGuardarTodo()

			Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTemp)
			lobjGenericoRel.Modi()

			If Not ldsTemp.Tables(mstrTabla & "_conf") Is Nothing And lobjGenericoRel.gstrUltId <> "" Then
				hdnImprimir.Text = lobjGenericoRel.gstrUltId
                hdnFactNume.Text = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantesX", hdnImprimir.Text).Tables(0).Rows(0).Item("numero").ToString
                If hdnId.Text.Trim() <> "" Then
                    hdnImprimirInte.Text = clsSQLServer.gCampoValorConsul(mstrConn, "plan_pagos_consul @plpa_id=" & hdnId.Text, "_plpa_nd_inte_comp_id")
                End If
            Else
                mLimpiar()
                mConsultar()
            End If

        Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
	End Sub

    Private Sub mBorrarDeuda(ByVal pstrTipo As String)
        For Each ldr As DataRow In mdsDatos.Tables(mstrPlanPagosDeta).Select("plde_cs=" + pstrTipo)
            ldr.Delete()
        Next
    End Sub

    Private Function mGuardarTodo() As DataSet
        Dim ldsTemp As DataSet

        mGuardarDatos()
        If mstrTipo = "C" Then
            mGuardarDatosDeuda("Ctac")
            mBorrarDeuda("false")
        Else
            mGuardarDatosDeuda("Soc")
            mBorrarDeuda("true")
            'mdsDatos.Tables(mstrPlanPagosVtos).Select("plde_cs=true")(0).Delete()
        End If

        ldsTemp = mdsDatos.Copy
        If Not mdsDatos.Tables(mstrTabla).Rows(0).IsNull("plpa_apro_fecha") Then
            mValidarConf()
            mAgregarTablaProceso(ldsTemp)
        End If

        For Each ldr As DataRow In ldsTemp.Tables(mstrPlanPagosDeta).Select("plde_comp_id<0")
            ldr.Item("plde_comp_id") = DBNull.Value
        Next

        Return (ldsTemp)
 End Function

 Private Sub mBaja()
  Try
   Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
   lobjGenericoRel.Baja(hdnId.Text)

   mConsultar()

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Private Sub mGuardarDatos()
  mValidarDatos()

  With mdsDatos.Tables(mstrTabla).Rows(0)
   .Item("plpa_clie_id") = usrClie.Valor
   .Item("plpa_gene_fecha") = txtGeneFecha.Fecha
                .Item("plpa_obse") = txtObse.Valor
                If txtAproSociFecha.Fecha.Length > 0 Then
                    .Item("plpa_apro_soci_fecha") = txtAproSociFecha.Fecha
                End If
                If txtAproCtacFecha.Fecha.Length > 0 Then
                    .Item("plpa_apro_ctac_fecha") = txtAproCtacFecha.Fecha
                End If
                If txtAproFecha.Fecha.Length > 0 Then
                    .Item("plpa_apro_fecha") = txtAproFecha.Fecha
                End If
                If cmbActividad.Valor.Length > 0 Then
                    .Item("plpa_acti_id") = cmbActividad.Valor
                End If
                '.Item("_datos_comp") = txtCompGene.Text

                If Not .IsNull("plpa_apro_soci_fecha") And .IsNull("plpa_apro_soci_usua_id") Then
                    .Item("plpa_apro_soci_usua_id") = Session("sUserId").ToString()
                End If

                If Not .IsNull("plpa_apro_ctac_fecha") And .IsNull("plpa_apro_ctac_usua_id") Then
                    .Item("plpa_apro_ctac_usua_id") = Session("sUserId").ToString()
                End If

                If Not .IsNull("plpa_apro_fecha") And .IsNull("plpa_apro_usua_id") Then
                    .Item("plpa_apro_usua_id") = Session("sUserId").ToString()
                End If

                .Item("plpa_inte_fecha") = txtValorFecha.Fecha
            End With
 End Sub

 Private Sub mValidarDatos()
  clsWeb.gInicializarControles(Me, mstrConn)
  clsWeb.gValidarControles(Me)
 End Sub

 Public Sub mCrearDataSet(ByVal pstrId As String)
  mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

  mdsDatos.Tables(0).TableName = mstrTabla
  mdsDatos.Tables(1).TableName = mstrPlanPagosDeta
  mdsDatos.Tables(2).TableName = mstrPlanPagosVtos
  mdsDatos.Tables(3).TableName = mstrPlanPagosConcep

  With mdsDatos.Tables(mstrTabla).Rows(0)
   If .IsNull("plpa_id") Then
    .Item("plpa_id") = -1
   End If
  End With


  For Each ldr As DataRow In mdsDatos.Tables(mstrPlanPagosDeta).Select("plde_comp_id IS NULL")
   If ldr.IsNull("plde_comp_id") Then
    ldr.Item("plde_comp_id") = -1 * (ldr.Item("plde_anio").ToString + ldr.Item("plde_perio").ToString)
   End If
  Next

  Session(mstrTabla) = mdsDatos
 End Sub

 Private Sub mAgregarTablaProceso(ByVal ldsEsta As DataSet)
  Dim lDr As DataRow

  ldsEsta.Tables.Add(mstrTabla & "_conf")

  With ldsEsta.Tables(ldsEsta.Tables.Count - 1)
   .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
   .Columns.Add("proc_plpa_id", System.Type.GetType("System.Int32"))
   .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
   .Columns.Add("proc_cemi_nume", System.Type.GetType("System.Int32"))
   .Columns.Add("proc_emct_id", System.Type.GetType("System.Int32"))
   .Columns.Add("proc_host", System.Type.GetType("System.String"))
   lDr = .NewRow
   .Rows.Add(lDr)
  End With

  lDr.Item("proc_id") = -1
  lDr.Item("proc_plpa_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("plpa_id")

  Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
  oFact.CentroEmisorNro(mstrConn)
  lDr.Item("proc_cemi_nume") = oFact.pCentroEmisorNro
  lDr.Item("proc_emct_id") = oFact.pCentroEmisorId
  lDr.Item("proc_host") = oFact.pHost
 End Sub

 Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
  mAlta()
 End Sub

 Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
  mBaja()
 End Sub

 Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
  mModi()
 End Sub

 Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
  mLimpiar()
 End Sub

 Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
  mCerrar()
 End Sub

 Public Sub mConsultar()
  Try
   Dim lstrCmd As New StringBuilder
   lstrCmd.Append("exec " + mstrTabla + "_consul")
   lstrCmd.Append(" @clie_id=" & usrClieFil.Valor)
   lstrCmd.Append(", @fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha))
   lstrCmd.Append(", @fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha))
   lstrCmd.Append(", @Tipo=" & mstrTipo)
   lstrCmd.Append(", @baja=0")

   clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

   mMostrarPanel(False)

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub
#End Region

#Region "Operacion Sobre la Grilla"
	Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdDato.CurrentPageIndex = E.NewPageIndex
			mConsultar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
		Try
			mCargarDatos(e.Item.Cells(1).Text)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

#Region "Grillas"
	Public Sub grdDeudaCtac_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			mGuardarDatosDeuda("Ctac")

			grdDeudaCtac.EditItemIndex = -1
			If (grdDeudaCtac.CurrentPageIndex < 0 Or grdDeudaCtac.CurrentPageIndex >= grdDeudaCtac.PageCount) Then
				grdDeudaCtac.CurrentPageIndex = 0
			Else
				grdDeudaCtac.CurrentPageIndex = E.NewPageIndex
			End If

			mConsultarDeudaCtac()

			mChequearGrilla("Ctac")

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Public Sub grdDeudaSoc_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			If Me.btnAlta.Enabled = True And Me.btnConf.Enabled = True Then
				mGuardarDatosDeuda("Soc")
			End If
			grdDeudaSoc.EditItemIndex = -1
			If (grdDeudaSoc.CurrentPageIndex < 0 Or grdDeudaSoc.CurrentPageIndex >= grdDeudaSoc.PageCount) Then
				grdDeudaSoc.CurrentPageIndex = 0
			Else
				grdDeudaSoc.CurrentPageIndex = E.NewPageIndex
			End If

			'mConsultarDeudaSoc()

            grdDeudaSoc.DataSource = Session("grdDeudaSoc")
			grdDeudaSoc.DataBind()

			mChequearGrilla("Soc")

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mGuardarCuotasAnteriores(Optional ByVal pbooChequearTodas As Boolean = False)
		Dim lstrAnio, lstrPerio As String
		Dim lintPagina As Integer
		Dim lbooTodosChequeados As Boolean
		Dim lintPaginaActual As Integer
		Dim lchkPlan As CheckBox

		lbooTodosChequeados = True

		For Each oDataItem As DataGridItem In grdDeudaSoc.Items
			lchkPlan = DirectCast(oDataItem.FindControl("chkPlan"), CheckBox)

			If pbooChequearTodas Then lchkPlan.Checked = True 'chequeo todo de entrada

			If lchkPlan.Checked Then
				If lstrAnio = "" Then
					lstrAnio = oDataItem.Cells(ColumnasDeudas.Anio).Text
					lstrPerio = oDataItem.Cells(ColumnasDeudas.Perio).Text
				End If
			End If

			lbooTodosChequeados = lbooTodosChequeados And lchkPlan.Checked
		Next

		If Not lbooTodosChequeados Or lstrAnio <> "" Then
			If pbooChequearTodas Then			 'si chequeo todo de entrada, termina en la p�gina 0
				lintPaginaActual = 0
			Else
				mConsultarDeudaSoc()
				If Not Session("grdDeudaSoc") Is Nothing Then				'Si entra por 1ra vez y la session esta vacia da error -- Pantanettig 20/11/2007
                    grdDeudaSoc.DataSource = Session("grdDeudaSoc")
					grdDeudaSoc.DataBind()
					lintPaginaActual = grdDeudaSoc.CurrentPageIndex
				End If
			End If

			lintPagina = grdDeudaSoc.PageCount - 1
			While lintPagina >= 0
				grdDeudaSoc.CurrentPageIndex = lintPagina
				grdDeudaSoc.DataBind()

				If lstrAnio <> "" And (lintPagina < lintPaginaActual Or pbooChequearTodas) Then
					mGuardarDatosDeuda("Soc", True)
				End If
				If Not lbooTodosChequeados And lintPagina > lintPaginaActual Then
					mGuardarDatosDeuda("Soc", False, True)
				End If
				lintPagina -= 1
			End While

			grdDeudaSoc.CurrentPageIndex = lintPaginaActual
			grdDeudaSoc.DataBind()

			mChequearGrilla("Soc")
			If btnAlta.Enabled Then
				mCalcularTotalesSelec("Soc", True)
			Else
				mCalcularTotalesSelec("Soc")
			End If
		End If
	End Sub

	Private Sub grdDeudaCtac_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDeudaCtac.ItemDataBound, grdDeudaSoc.ItemDataBound
		Try
			If e.Item.ItemIndex <> -1 Then
				Dim lbooApro As Boolean = DirectCast(FindControl("btnDeuda" & DirectCast(sender, DataGrid).ID.Replace("grdDeuda", "") & "Aprob"), Button).Enabled
				DirectCast(e.Item.FindControl("chkPlan"), CheckBox).Enabled = lbooApro
			End If

            lblDeudaSocInte.Text = "Intereses al " & txtValorFecha.Text & ":"
            grdDeudaSoc.Columns(mintColuSociInte).HeaderText = "Intereses al " & txtValorFecha.Text
            grdDeudaCtac.Columns(mintColuCtacInte).HeaderText = "Intereses al " & txtValorFecha.Text

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mChequearGrilla(ByVal pstrTipo As String)
		Dim oDataGrid As DataGrid
		Dim lDr As DataRow
		Dim lstrFiltro As String

		oDataGrid = FindControl("grdDeuda" + pstrTipo)

		For Each oDataItem As DataGridItem In oDataGrid.Items
			lDr = Nothing
			If IsNumeric(oDataItem.Cells(ColumnasDeudas.CompId).Text) Then
				lstrFiltro = "plde_comp_id=" & oDataItem.Cells(ColumnasDeudas.CompId).Text
			Else
				lstrFiltro = "plde_anio=" & oDataItem.Cells(ColumnasDeudas.Anio).Text & " AND plde_perio=" & oDataItem.Cells(ColumnasDeudas.Perio).Text
			End If

			With mdsDatos.Tables(mstrPlanPagosDeta).Select(lstrFiltro)
				If .GetUpperBound(0) <> -1 Then
					lDr = .GetValue(0)
				End If
			End With

			CType(oDataItem.FindControl("chkPlan"), System.Web.UI.WebControls.CheckBox).Checked = Not (lDr Is Nothing)
			'If (lDr Is Nothing) Then
			'	CType(oDataItem.FindControl("chkPlan"), System.Web.UI.WebControls.CheckBox).Checked = False
			'Else
			'	CType(oDataItem.FindControl("chkPlan"), System.Web.UI.WebControls.CheckBox).Checked = True				 'Not (lDr Is Nothing)
			'End If

		Next
		
	End Sub

	Public Sub mConsultarDeudaSoc()
		Dim lstrCmd As New StringBuilder
		Dim ds As DataSet
		Dim lDsDescu As DataSet

		lstrCmd.Append("exec rpt_calculo_deuda_consul")
		lstrCmd.Append(" @soci_id=")
		lstrCmd.Append(hdnSociId.Text)
		lstrCmd.Append(", @fecha=")
		lstrCmd.Append(clsSQLServer.gFormatArg(txtValorFecha.Text, SqlDbType.SmallDateTime))
		lstrCmd.Append(", @consulta=1")

		ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

		If ds.Tables(0).Rows.Count > 0 Then
			With ds.Tables(0).Select("", "anio desc,perio desc")(0)
				lDsDescu = SRA_Neg.Comprobantes.gObtenerDescuentoSocioPagoAdel(mstrConn, hdnSociId.Text, .Item("anio"), .Item("perio"), .Item("peti_id"), "0" & .Item("inst_id").ToString, txtValorFecha.Fecha)

				If lDsDescu.Tables(0).Rows.Count > 0 Then
					.Item("descuento") = lDsDescu.Tables(0).Rows(0).Item("cucd_valor")
					.Item("conc_id") = lDsDescu.Tables(0).Rows(0).Item("cucd_conc_id")

					.Item("total") += .Item("descuento")

				End If
			End With


			ds.Tables(0).DefaultView.Sort = "anio,perio,interes"
			grdDeudaSoc.DataSource = ds.Tables(0).DefaultView

			If Not btnAlta.Enabled And Not btnDeudaSocAprob.Enabled And btnConf.Enabled Then


				Dim dTotal, dInte, dCuot, dDesc As Decimal
				If Not grdDeudaSoc.DataSource Is Nothing Then
					With DirectCast(grdDeudaSoc.DataSource, DataView)
						For i As Integer = 0 To .Count - 1
							dTotal += .Item(i).Item("total")
							dCuot += .Item(i).Item("cuota")
							dDesc += .Item(i).Item("descuento")
							If Not .Item(i).Row.IsNull("interes") Then dInte += .Item(i).Item("interes")
						Next
					End With

					dTotal = dCuot + dInte + dDesc
					txtDeudaSocTotal.Text = Format(dTotal, "0.00")
					txtDeudaSocInte.Text = Format(dInte, "0.00")
					txtDeudaSocDesc.Text = Format(dDesc, "0.00")
					txtDeudaSocCuot.Text = Format(dCuot, "0.00")
					txtDeudaSocDife.Text = Format(dTotal, "0.00")
					txtDeudaSocPag.Text = "0.00"
					txtDeudaSocPlan.Text = "0.00"

					hdnDeudaSocDife.Text = txtDeudaSocDife.Text
					hdnDeudaSocPag.Text = txtDeudaSocPag.Text
					hdnDeudaSocPlan.Text = txtDeudaSocPlan.Text
					hdnDeudaSocInte.Text = txtDeudaSocInte.Text
					hdnDeudaSocCuot.Text = txtDeudaSocCuot.Text
					txtConcDeudaSoc.Text = hdnDeudaSocDife.Text
					txtCuotDeudaSoc.Text = hdnDeudaSocDife.Text
					hdnDeudaSocDesc.Text = txtDeudaSocDesc.Text

				End If

			End If
		Else
			ds.Tables(0).DefaultView.Sort = "anio,perio,interes"
			grdDeudaSoc.DataSource = ds.Tables(0).DefaultView
        End If

        grdDeudaSoc.DataBind()
        hdnDeudaSocFecha.Text = txtValorFecha.Text

	End Sub

	Public Sub mConsultarDeudaCtac()
		Dim lstrCmd As New StringBuilder
		Dim ds As DataSet

		lstrCmd.Append("exec cobranzas_deuda_consul")
		lstrCmd.Append(" @clie_id=")
		lstrCmd.Append(usrClie.Valor)
		lstrCmd.Append(", @fecha=")
		lstrCmd.Append(clsSQLServer.gFormatArg(txtGeneFecha.Text, SqlDbType.SmallDateTime))
		lstrCmd.Append(", @tipo='D'")
		lstrCmd.Append(", @agrupar=1")

		ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

        grdDeudaCtac.DataSource = ds
		grdDeudaCtac.DataBind()
		ds.Dispose()
	End Sub

	Private Sub mGuardarDatosDeuda(ByVal pstrTipo As String, Optional ByVal pbooGuardarTodos As Boolean = False, Optional ByVal pbooSacarTodos As Boolean = False)
		Dim lstrId As New StringBuilder
		Dim oGrilla As DataGrid = FindControl("grdDeuda" & pstrTipo)
		Dim lDr As DataRow
		Dim ldsDeuda As DataSet
        Dim lstrFiltro As String
        Dim lbooAlta As Boolean

		For Each oDataItem As DataGridItem In oGrilla.Items
			lDr = Nothing

			If IsNumeric(oDataItem.Cells(ColumnasDeudas.CompId).Text) Then
				lstrFiltro = "plde_comp_id=" & oDataItem.Cells(ColumnasDeudas.CompId).Text
			Else
				lstrFiltro = "plde_anio=" & oDataItem.Cells(ColumnasDeudas.Anio).Text
				lstrFiltro += " AND plde_perio=" & oDataItem.Cells(ColumnasDeudas.Perio).Text
			End If

			'If pstrTipo <> "Soc" Then
			'   lstrFiltro += " AND plde_covt_id=" & oDataItem.Cells(ColumnasDeudas.VtoId).Text
			'End If

			With mdsDatos.Tables(mstrPlanPagosDeta).Select(lstrFiltro)
				If .GetUpperBound(0) <> -1 Then
					lDr = .GetValue(0)
				End If
			End With

			If (DirectCast(oDataItem.FindControl("chkPlan"), CheckBox).Checked Or pbooGuardarTodos) And Not pbooSacarTodos Then
                If lDr Is Nothing Then
                    lbooAlta = True
                Else
                    lbooAlta = False
                End If
                lDr = mdsDatos.Tables(mstrPlanPagosDeta).NewRow
                lDr.Item("plde_id") = clsSQLServer.gObtenerId(lDr.Table, "plde_id")
                lDr.Item("plde_cs") = pstrTipo <> "Soc"
                lDr.Item("plde_neto") = oDataItem.Cells(ColumnasDeudas.CompNeto).Text
                If IsNumeric(oDataItem.Cells(ColumnasDeudas.CompId).Text) Then
                    lDr.Item("plde_comp_id") = oDataItem.Cells(ColumnasDeudas.CompId).Text
                End If

                If pstrTipo = "Soc" Then
                    lDr.Item("plde_peti_id") = oDataItem.Cells(ColumnasDeudas.PetiId).Text
                    lDr.Item("plde_anio") = oDataItem.Cells(ColumnasDeudas.Anio).Text
                    lDr.Item("plde_perio") = oDataItem.Cells(ColumnasDeudas.Perio).Text
                    lDr.Item("plde_coti_id") = SRA_Neg.Constantes.ComprobTipos.Cuota_Social

                    If IsNumeric(oDataItem.Cells(ColumnasDeudas.CompId).Text) Then       'si ya est� devengada
                        lDr.Item("plde_inte_impo") = oDataItem.Cells(ColumnasDeudas.CompInteImpo).Text
                    Else
                        lDr.Item("plde_inte_impo") = 0
                    End If

                    lDr.Item("plde_desc_impo") = oDataItem.Cells(ColumnasDeudas.Descuento).Text
                    If IsNumeric(oDataItem.Cells(ColumnasDeudas.ConcId).Text) Then       'si tiene descuento
                        lDr.Item("plde_conc_id") = oDataItem.Cells(ColumnasDeudas.ConcId).Text
                    Else
                        lDr.Item("plde_conc_id") = DBNull.Value
                    End If

                Else
                    'lDr.Item("plde_covt_id") = oDataItem.Cells(ColumnasDeudas.VtoId).Text
                    lDr.Item("plde_inte_impo") = oDataItem.Cells(ColumnasDeudas.CtacInte).Text
                    lDr.Item("plde_desc_impo") = 0
                End If
                If lbooAlta Then
                   lDr.Table.Rows.Add(lDr)
                End If
            Else
                If Not (lDr Is Nothing) Then
                    lDr.Delete()
                End If
            End If
        Next

        If pstrTipo = "Soc" And Not (pbooGuardarTodos Or pbooSacarTodos) Then
            'guardo los registros de las paginas anteriores si se cheque� las grillas o saco los de las posteriores si se descheque�
            mGuardarCuotasAnteriores()
        End If
	End Sub

	Private Sub mCalcularTotalesDeudaSoc()
		Dim dTotal, dInte, dCuot, dDesc As Decimal

		If Not grdDeudaSoc.DataSource Is Nothing Then
			With DirectCast(grdDeudaSoc.DataSource, DataView)
				For i As Integer = 0 To .Count - 1
					dTotal += .Item(i).Item("total")
					dCuot += .Item(i).Item("cuota")
					dDesc += .Item(i).Item("descuento")
					If Not .Item(i).Row.IsNull("interes") Then dInte += .Item(i).Item("interes")
				Next
			End With

			dTotal = dCuot + dInte + dDesc
			txtDeudaSocTotal.Text = Format(dTotal, "0.00")
			txtDeudaSocInte.Text = Format(dInte, "0.00")
			txtDeudaSocDesc.Text = Format(dDesc, "0.00")
			txtDeudaSocCuot.Text = Format(dCuot, "0.00")
			txtDeudaSocDife.Text = Format(dTotal, "0.00")
			txtDeudaSocPag.Text = "0.00"
			txtDeudaSocPlan.Text = "0.00"

			hdnDeudaSocDife.Text = txtDeudaSocDife.Text
			hdnDeudaSocPag.Text = txtDeudaSocPag.Text
			hdnDeudaSocPlan.Text = txtDeudaSocPlan.Text
			hdnDeudaSocInte.Text = txtDeudaSocInte.Text
			hdnDeudaSocCuot.Text = txtDeudaSocCuot.Text
			txtConcDeudaSoc.Text = hdnDeudaSocDife.Text
			txtCuotDeudaSoc.Text = hdnDeudaSocDife.Text
			hdnDeudaSocDesc.Text = txtDeudaSocDesc.Text

		End If

	End Sub


	Private Sub mCalcularTotalesDeudaCtac()
		Dim dTotal As Decimal

		For Each lDr As DataRow In DirectCast(grdDeudaCtac.DataSource, DataSet).Tables(0).Rows
			dTotal += lDr.Item("comp_neto") + lDr.Item("interes")
		Next

		txtDeudaCtacDife.Valor = dTotal
		txtDeudaCtacPag.Valor = 0
		txtDeudaCtacPlan.Valor = 0

		Dim lstrCmd As String
		Dim ds As DataSet

		lstrCmd = "exec clientes_estado_de_saldos_consul"
		lstrCmd += " @clie_id = " + clsSQLServer.gFormatArg(usrClie.Valor, SqlDbType.Int)
		lstrCmd += ",@fecha = " + clsSQLServer.gFormatArg(txtValorFecha.Text, SqlDbType.SmallDateTime)
		lstrCmd += ",@solo_ctacte = 1"

		ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

		With ds.Tables(0).Rows(0)
			txtDeudaCtacTotal.Valor = IIf(.Item("saldo_cta_cte") = "0", "0.00", .Item("saldo_cta_cte"))
			txtTotalVencCC.Valor = IIf(.Item("saldo_cta_cte_venc") = "0", "0.00", .Item("saldo_cta_cte_venc"))
			txtSaldoCtaCC.Valor = IIf(.Item("imp_a_cta_cta_cte") = "0", "0.00", .Item("imp_a_cta_cta_cte"))
			txtCtaSocial.Valor = IIf(.Item("imp_a_cta_cta_social") = "0", "0.00", .Item("imp_a_cta_cta_social"))
		End With
		txtDeudaCtacPlan.Text = txtDeudaCtacTotal.Text
		hdnDeudaCtacDife.Text = txtDeudaCtacDife.Text
		hdnDeudaCtacPag.Text = txtDeudaCtacPag.Text
		hdnDeudaCtacPlan.Text = txtDeudaCtacPlan.Text
	End Sub

	Private Sub mCalcularTotalesSelec(ByVal pstrTipo As String, Optional ByVal Flag As Boolean = False)
		Dim dTotalPlan As Decimal
		Dim dTotalPag As Decimal
		Dim lstrFiltro As String
		Dim ltxtPaga As NixorControls.NumberBox
		Dim ltxtPlan As NixorControls.NumberBox
		Dim ltxtDife As NixorControls.NumberBox
		Dim ltxtTotal As NixorControls.NumberBox
		Dim ltxtCuot As NixorControls.NumberBox
		Dim ltxtInte As NixorControls.NumberBox
		Dim ltxtDesc As NixorControls.NumberBox
		Dim lstrCmd As New StringBuilder
		Dim lDsDescu As DataSet


		If pstrTipo = "Soc" Then
			lstrFiltro = "plde_cs=False"
		Else
			lstrFiltro = "plde_cs=True"
		End If

		If txtSocio.Text = "" And usrClie.Valor = 0 Then
			With mdsDatos.Tables(1).Select("", "plde_anio desc,plde_perio desc")(0)
				If Not .Item("plde_anio") Is DBNull.Value Then
					lDsDescu = SRA_Neg.Comprobantes.gObtenerDescuentoSocioPagoAdel(mstrConn, hdnSociId.Text, .Item("plde_anio"), .Item("plde_perio"), .Item("plde_peti_id"), "0", txtValorFecha.Fecha)

					If lDsDescu.Tables(0).Rows.Count > 0 Then
						.Item("plde_desc_impo") = lDsDescu.Tables(0).Rows(0).Item("cucd_valor")
						.Item("plde_conc_id") = lDsDescu.Tables(0).Rows(0).Item("cucd_conc_id")
					Else
						.Item("plde_desc_impo") = 0
						.Item("plde_conc_id") = 0

					End If
				End If
			End With
		End If


		For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosDeta).Select(lstrFiltro)

            dTotalPlan += lDr.Item("plde_neto") + IIf(lDr.Item("plde_inte_impo") Is DBNull.Value, 0, lDr.Item("plde_inte_impo")) + lDr.Item("plde_desc_impo")

		Next


		If Flag And pstrTipo = "Soc" Then

			ltxtPlan = FindControl("txtDeuda" & pstrTipo & "Plan")
			ltxtPaga = FindControl("txtDeuda" & pstrTipo & "Pag")
			ltxtDife = FindControl("txtDeuda" & pstrTipo & "Dife")
			ltxtTotal = FindControl("txtDeuda" & pstrTipo & "Total")
			ltxtCuot = FindControl("txtDeuda" & pstrTipo & "Cuot")
			ltxtInte = FindControl("txtDeuda" & pstrTipo & "Inte")
			ltxtDesc = FindControl("txtDeuda" & pstrTipo & "Desc")
			If txtDeudaSocTotal.Text = "0.00" Then
				txtDeudaSocTotal.Text = ltxtPlan.Valor
			End If
		Else
			ltxtPlan = FindControl("txtDeuda" & pstrTipo & "Plan")
			ltxtPaga = FindControl("txtDeuda" & pstrTipo & "Pag")
			ltxtDife = FindControl("txtDeuda" & pstrTipo & "Dife")
			ltxtTotal = FindControl("txtDeuda" & pstrTipo & "Total")
			ltxtCuot = FindControl("txtDeuda" & pstrTipo & "Cuot")
			ltxtInte = FindControl("txtDeuda" & pstrTipo & "Inte")
			ltxtDesc = FindControl("txtDeuda" & pstrTipo & "Desc")
		End If



		ltxtPlan.Text = dTotalPlan.ToString("#######0.00")
		ltxtPaga.Text = dTotalPag.ToString("#######0.00")
		ltxtDife.Text = CDbl(DirectCast(FindControl("txtDeuda" & pstrTipo & "Total"), NixorControls.NumberBox).Valor - ltxtPlan.Valor).ToString("#######0.00")

		DirectCast(FindControl("hdnDeuda" & pstrTipo & "Plan"), TextBox).Text = ltxtPlan.Text
		DirectCast(FindControl("hdnDeuda" & pstrTipo & "Pag"), TextBox).Text = ltxtPaga.Text
		DirectCast(FindControl("hdnDeuda" & pstrTipo & "Dife"), TextBox).Text = ltxtDife.Text

		If mdsDatos.Tables(mstrPlanPagosDeta).Rows.Count <> 0 Then

			DirectCast(FindControl("txtCuotDeuda" & pstrTipo), TextBox).Text = ltxtPlan.Text
			DirectCast(FindControl("txtConcDeuda" & pstrTipo), TextBox).Text = ltxtPlan.Text
		End If
		mCalcularTotalesCuotas()
	End Sub

	Public Sub mConsultarDetas(Optional ByVal pbooAprob As Boolean = False, Optional ByVal Flag As Boolean = False)
		Dim lstrCmd As New StringBuilder
		Dim lDs As New DataSet
		Dim lDt As DataTable = lDs.Tables.Add()
		Dim lDr As DataRow
        Dim dTotal, dInte, dCuot, dDesc As Decimal

		If hdnSociId.Text <> "" Then
			'la deuda social
			With lDt
				.Columns.Add("comp_id", System.Type.GetType("System.Int32"))
				.Columns.Add("coti_desc", System.Type.GetType("System.String"))
				.Columns.Add("cuota", System.Type.GetType("System.Double"))
				.Columns.Add("anio", System.Type.GetType("System.Int32"))
				.Columns.Add("perio", System.Type.GetType("System.Int32"))
				.Columns.Add("peti_id", System.Type.GetType("System.Int32"))
				.Columns.Add("conc_id", System.Type.GetType("System.Int32"))
				.Columns.Add("interes", System.Type.GetType("System.Double"))
				.Columns.Add("descuento", System.Type.GetType("System.Double"))
				.Columns.Add("total", System.Type.GetType("System.Double"))
			End With

			For Each lDrOri As DataRow In mdsDatos.Tables(mstrPlanPagosDeta).Select("plde_anio is not null")
				lDr = lDt.NewRow
				With lDr
					.Item("comp_id") = lDrOri.Item("plde_comp_id")
					.Item("coti_desc") = lDrOri.Item("_coti_desc")
					.Item("cuota") = lDrOri.Item("plde_neto")
					.Item("anio") = lDrOri.Item("plde_anio")
					.Item("perio") = lDrOri.Item("plde_perio")
					.Item("peti_id") = lDrOri.Item("plde_peti_id")
					.Item("conc_id") = lDrOri.Item("plde_conc_id")
					.Item("interes") = lDrOri.Item("plde_inte_impo")
					.Item("descuento") = lDrOri.Item("plde_desc_impo")
					.Item("total") = .Item("cuota") + .Item("interes") + .Item("descuento")



					'aca es donde tendria que sumar los totales...
					If Not btnAlta.Enabled Then

						dTotal += Convert.ToDecimal(.Item("total"))						'4 es el nro de columna
						'dTotal += oDataGrid.Items(2).  ).Item("total")
						dCuot += Convert.ToDecimal(.Item("cuota"))
						'dCuot += oDataGrid.Item(i).Item("cuota")
						dDesc += Convert.ToDecimal(.Item("descuento"))
						'dDesc += oDataGrid.Item(i).Item("descuento")
						'If oDataItem.Cells(7).Text <> "" Then dInte += Convert.ToDecimal(lDrOri.Item("plde_inte_impo"))

						If Not .Item("interes") Then dInte += .Item("interes")


					End If
					'------------------------------------------------
				End With
				lDt.Rows.Add(lDr)
			Next

			If Not btnAlta.Enabled Then
				'dTotal = dTotal + dDesc
				txtDeudaSocTotal.Text = Format(dTotal, "0.00")
				txtDeudaSocInte.Text = Format(dInte, "0.00")
				txtDeudaSocDesc.Text = Format(dDesc, "0.00")
				txtDeudaSocCuot.Text = Format(dCuot, "0.00")
				txtDeudaSocDife.Text = Format(dTotal, "0.00")
				txtDeudaSocPag.Text = "0.00"
				txtDeudaSocPlan.Text = "0.00"

				hdnDeudaSocDife.Text = txtDeudaSocDife.Text
				hdnDeudaSocPag.Text = txtDeudaSocPag.Text
				hdnDeudaSocPlan.Text = txtDeudaSocPlan.Text
				hdnDeudaSocInte.Text = txtDeudaSocInte.Text
				hdnDeudaSocCuot.Text = txtDeudaSocCuot.Text
				hdnDeudaSocDesc.Text = txtDeudaSocDesc.Text


				If btnDeudaSocAprob.Enabled Then
					If Not btnAlta.Enabled Then

						mChequearGrilla("Soc")
					Else
						mCalcularTotalesDeudaSoc()
						lDs.Tables(0).DefaultView.Sort = "anio,perio,interes"
						grdDeudaSoc.DataSource = lDs.Tables(0).DefaultView
						grdDeudaSoc.DataBind()
					End If
				End If
				If pbooAprob Then
					mCalcularTotalesDeudaSoc()
					lDs.Tables(0).DefaultView.Sort = "anio,perio,interes"
					grdDeudaSoc.DataSource = lDs.Tables(0).DefaultView
					grdDeudaSoc.DataBind()
				End If
				If Flag Then
					lDs.Tables(0).DefaultView.Sort = "anio,perio,interes"
					grdDeudaSoc.DataSource = lDs.Tables(0).DefaultView
					grdDeudaSoc.DataBind()
				End If
			End If

			Session("grdDeudaSoc") = grdDeudaSoc.DataSource

		End If

		'la deuda cta.cte
		lDs = New DataSet
		lDt = lDs.Tables.Add()

		With lDt
			.Columns.Add("comp_id", System.Type.GetType("System.Int32"))
			.Columns.Add("numero", System.Type.GetType("System.String"))
			.Columns.Add("comp_neto", System.Type.GetType("System.Double"))
			.Columns.Add("interes", System.Type.GetType("System.Double"))
			'.Columns.Add("covt_id", System.Type.GetType("System.Int32"))
			'.Columns.Add("covt_fecha", System.Type.GetType("System.DateTime"))
			.Columns.Add("comp_fecha", System.Type.GetType("System.DateTime"))
		End With
		dTotal = 0
		For Each lDrOri As DataRow In mdsDatos.Tables(mstrPlanPagosDeta).Select("plde_anio is null")
			lDr = lDt.NewRow
			With lDr
				.Item("comp_id") = lDrOri.Item("plde_comp_id")
				.Item("numero") = lDrOri.Item("_coti_desc")
				.Item("comp_neto") = lDrOri.Item("plde_neto")
				'.Item("covt_id") = lDrOri.Item("plde_covt_id")
				'.Item("covt_fecha") = lDrOri.Item("_covt_fecha")
				.Item("comp_fecha") = lDrOri.Item("_comp_fecha")
				.Item("interes") = lDrOri.Item("plde_inte_impo")

				If Not btnAlta.Enabled Then
                    dTotal = dTotal + lDrOri.Item("plde_neto") + lDrOri.Item("plde_inte_impo")
				End If

			End With
			lDt.Rows.Add(lDr)
		Next

		txtDeudaCtacTotal.Text = Format(dTotal, "0.00")
		txtDeudaCtacPlan.Text = Format(dTotal, "0.00")
		hdnDeudaCtacPlan.Text = Format(dTotal, "0.00")

        grdDeudaCtac.DataSource = lDs
		grdDeudaCtac.DataBind()

		If btnAlta.Enabled And btnDeudaSocAprob.Enabled Then mCalcularTotalesDeudaCtac()

	End Sub
#End Region

#Region "Cuotas"
	Public Sub grdCuotas_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdCuotas.EditItemIndex = -1
			If (grdCuotas.CurrentPageIndex < 0 Or grdCuotas.CurrentPageIndex >= grdCuotas.PageCount) Then
				grdCuotas.CurrentPageIndex = 0
			Else
				grdCuotas.CurrentPageIndex = E.NewPageIndex
			End If
			mConsultarCuota()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Public Sub mConsultarCuota()
		With mdsDatos.Tables(mstrPlanPagosVtos)
			.DefaultView.RowFilter = ""
			.DefaultView.Sort = "plcu_cuota"
			grdCuotas.DataSource = .DefaultView
			grdCuotas.DataBind()
		End With

		mCalcularTotalesCuotas()
	End Sub

	Public Sub mEditarDatosCuota(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
		Try
			Dim ldrCuota As DataRow

			hdnCuotId.Text = E.Item.Cells(1).Text
			ldrCuota = mdsDatos.Tables(mstrPlanPagosVtos).Select("plcu_id=" & hdnCuotId.Text)(0)

			With ldrCuota
				txtCuota.Valor = .Item("plcu_cuota")
				txtFechaCuota.Fecha = .Item("plcu_fecha")
				txtImpoCuota.Valor = .Item("plcu_impo")
			End With

			mSetearEditor(mstrPlanPagosVtos, False)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mActualizarCuota()
		Try
			mGuardarDatosCuota()

			mLimpiarCuota()
			mConsultarCuota()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mGuardarDatosCuota()
		Dim ldrDatos As DataRow
		Dim lUltCuota As Integer

		If txtFechaCuota.Fecha Is DBNull.Value Then
			Throw New AccesoBD.clsErrNeg("Debe indicar la fecha.")
		End If

		If txtImpoCuota.Valor Is DBNull.Value Then
			Throw New AccesoBD.clsErrNeg("Debe indicar el importe.")
		End If

		If hdnCuotId.Text = "" Then
			ldrDatos = mdsDatos.Tables(mstrPlanPagosVtos).NewRow
			ldrDatos.Item("plcu_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrPlanPagosVtos), "plcu_id")

			'valido las fechas
			With mdsDatos.Tables(mstrPlanPagosVtos).Select("", "plcu_cuota desc")
				If .GetUpperBound(0) <> -1 Then
					lUltCuota = .GetValue(0).Item("plcu_cuota")

					If txtFechaCuota.Fecha <= .GetValue(0).Item("plcu_fecha") Then
						Throw New AccesoBD.clsErrNeg("La fecha debe ser mayor a la fecha de la cuota anterior.")
					End If
				End If

				ldrDatos.Item("plcu_cuota") = lUltCuota + 1
			End With
		Else
			ldrDatos = mdsDatos.Tables(mstrPlanPagosVtos).Select("plcu_id=" & hdnCuotId.Text)(0)

			'valido las fechas
			If ldrDatos.Item("plcu_cuota") <> 1 Then
				With mdsDatos.Tables(mstrPlanPagosVtos).Select("plcu_cuota=" & (ldrDatos.Item("plcu_cuota") - 1).ToString)(0)
					If .Item("plcu_fecha") >= txtFechaCuota.Fecha Then
						Throw New AccesoBD.clsErrNeg("La fecha debe ser mayor a la fecha de la cuota anterior.")
					End If
				End With
			End If

			With mdsDatos.Tables(mstrPlanPagosVtos).Select("plcu_cuota=" & (ldrDatos.Item("plcu_cuota") + 1).ToString, "plcu_cuota")
				If .GetUpperBound(0) <> -1 Then
					If .GetValue(0).item("plcu_fecha") <= txtFechaCuota.Fecha Then
						Throw New AccesoBD.clsErrNeg("La fecha debe ser menor a la fecha de la cuota siguiente.")
					End If
				End If
			End With
		End If

            With ldrDatos
                If txtFechaCuota.Fecha.Length > 0 Then
                    .Item("plcu_fecha") = txtFechaCuota.Fecha
                Else
                    .Item("plcu_fecha") = Now.Date
                End If
                If txtImpoCuota.Valor.Length > 0 Then
                    .Item("plcu_impo") = txtImpoCuota.Valor
                Else
                    .Item("plcu_impo") = 0
                End If
            End With

		If hdnCuotId.Text = "" Then
			mdsDatos.Tables(mstrPlanPagosVtos).Rows.Add(ldrDatos)
		End If
	End Sub

	Private Sub mLimpiarCuota()
		hdnCuotId.Text = ""
		txtCuota.Text = ""
		txtFechaCuota.Text = ""
		txtImpoCuota.Text = ""

		mSetearEditor(mstrPlanPagosVtos, True, False)
	End Sub

	Private Sub btnLimpCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpCuota.Click
		Try
			mLimpiarCuota()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnBajaCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaCuota.Click
		Try
			Dim lintCuota As Integer

			mdsDatos.Tables(mstrPlanPagosVtos).Select("plcu_id=" & hdnCuotId.Text)(0).Delete()

			'renumero las cuotas
			For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosVtos).Select("", "plcu_cuota")
				lintCuota += 1
				lDr.Item("plcu_cuota") = lintCuota
			Next

			mConsultarCuota()
			mLimpiarCuota()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnAltaCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaCuota.Click
		mActualizarCuota()
	End Sub

	Private Sub btnModiCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiCuota.Click
		mActualizarCuota()
	End Sub

	Private Sub mCalcularTotalesCuotas()
		Dim dTotal As Decimal

		For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosVtos).Select()
			dTotal += lDr.Item("plcu_impo")
		Next

        txtCuotasTotal.Valor = dTotal
        txtCuotDeudaTotal.Valor = 0
        If mstrTipo = "S" Then
            txtCuotDeudaTotal.Valor = txtCuotDeudaTotal.Valor + CDec(txtCuotDeudaSoc.Valor) + CDec(txtCuotConc.Valor)
        End If
        If mstrTipo = "C" Then
            txtCuotDeudaTotal.Valor = txtCuotDeudaTotal.Valor + CDec(txtCuotDeudaCtac.Valor) + CDec(txtCuotConc.Valor)
        End If
        txtCuotasDife.Valor = CDec(txtCuotDeudaTotal.Valor) - CDec(txtCuotasTotal.Valor)

   End Sub
#End Region

#Region "Conceptos"
	Public Sub grdConc_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdConc.EditItemIndex = -1
			If (grdConc.CurrentPageIndex < 0 Or grdConc.CurrentPageIndex >= grdConc.PageCount) Then
				grdConc.CurrentPageIndex = 0
			Else
				grdConc.CurrentPageIndex = E.NewPageIndex
			End If
			mConsultarConc()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Public Sub mConsultarConc()
		With mdsDatos.Tables(mstrPlanPagosConcep)
			.DefaultView.RowFilter = ""
			.DefaultView.Sort = "plco_conc_id"
			grdConc.DataSource = .DefaultView
			grdConc.DataBind()
		End With

		mCalcularTotalesConc()
	End Sub

	Public Sub mEditarDatosConc(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
		Try
			Dim ldrConc As DataRow

			hdnConcId.Text = E.Item.Cells(1).Text
			ldrConc = mdsDatos.Tables(mstrPlanPagosConcep).Select("plco_id=" & hdnConcId.Text)(0)

			With ldrConc
					cmbConc.Valor = IIf(.Item("plco_conc_id") Is DBNull.Value, "", .Item("plco_conc_id"))
					mCargarCentroCostos(True)
					cmbCcos.Valor = IIf(.Item("plco_ccos_id") Is DBNull.Value, "", .Item("plco_ccos_id"))
					txtImpoConc.Valor = .Item("plco_impo")
			End With

			mSetearEditor(mstrPlanPagosConcep, False)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mActualizarConc()
		Try
			mGuardarDatosConc()

			mLimpiarConc()
			mConsultarConc()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mGuardarDatosConc()
		Dim ldrDatos As DataRow

		If txtImpoConc.Valor Is DBNull.Value Then
			Throw New AccesoBD.clsErrNeg("Debe indicar el importe.")
		End If

		'If txtImpoConc.Valor >= 0 Then
		'   Throw New AccesoBD.clsErrNeg("El importe del descuento debe ser negativo.")
		'End If

		If cmbCcos.Valor Is DBNull.Value Then
			If clsSQLServer.gObtenerEstruc(mstrConn, "conceptosX", cmbConc.Valor.ToString).Tables(0).Rows(0).Item("cta_resu").ToString() = "1" Then
				Throw New AccesoBD.clsErrNeg("La cuenta del concepto es de resultado, debe indicar el centro de costo.")
			End If
		End If

		If hdnConcId.Text = "" Then
			ldrDatos = mdsDatos.Tables(mstrPlanPagosConcep).NewRow
			ldrDatos.Item("plco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrPlanPagosConcep), "plco_id")
		Else
			ldrDatos = mdsDatos.Tables(mstrPlanPagosConcep).Select("plco_id=" & hdnConcId.Text)(0)
		End If

		With ldrDatos
			.Item("plco_impo") = txtImpoConc.Valor
                .Item("plco_conc_id") = cmbConc.Valor
                If cmbCcos.Valor.Length > 0 Then
                    .Item("plco_ccos_id") = cmbCcos.Valor
                End If
                .Item("_conc_desc") = cmbConc.SelectedItem.Text

                If cmbCcos.Valor.Length > 0 Then
                    .Item("_ccos_desc") = cmbCcos.SelectedItem.Text
                End If
            End With

		If hdnConcId.Text = "" Then
			mdsDatos.Tables(mstrPlanPagosConcep).Rows.Add(ldrDatos)
		End If
	End Sub

	Private Sub mLimpiarConc()
		hdnConcId.Text = ""
		cmbConc.Limpiar()
		cmbCcos.Limpiar()

		txtImpoConc.Text = ""

		mSetearEditor(mstrPlanPagosConcep, True, False)
	End Sub

	Private Sub btnLimpConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpConc.Click
		Try
			mLimpiarConc()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnBajaConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaConc.Click
		Try
			Dim lintConc As Integer

			mdsDatos.Tables(mstrPlanPagosConcep).Select("plco_id=" & hdnConcId.Text)(0).Delete()

			mConsultarConc()
			mLimpiarConc()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnAltaConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaConc.Click
		mActualizarConc()
	End Sub

	Private Sub btnModiConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiConc.Click
		mActualizarConc()
	End Sub

	Private Sub mCalcularTotalesConc()
		Dim dTotal As Decimal

		For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosConcep).Select()
			dTotal += lDr.Item("plco_impo")
		Next

		txtConcTotal.Valor = dTotal
        txtCuotConc.Valor = dTotal

        txtConcDife.Valor = 0
        If mstrTipo = "S" Then
            txtConcDife.Valor = txtConcDife.Valor + CDec(txtConcDeudaSoc.Valor)
        End If
        If mstrTipo = "C" Then
            txtConcDife.Valor = txtConcDife.Valor + CDec(txtConcDeudaCtac.Valor)
        End If
        txtConcDife.Valor = txtConcDife.Valor + CDec(txtConcTotal.Valor)
 End Sub
#End Region

	Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
		Try
			grdDato.CurrentPageIndex = 0
			mConsultar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
		mLimpiarFiltros()
	End Sub

	Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
		Try
			mAgregar()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub usrClie_Cambio(ByVal sender As Object) Handles usrClie.Cambio
		Try
			hdnSociId.Text = usrClie.SociId.ToString

            If mstrTipo = "S" Then lnkDeudaSoc.Visible = hdnSociId.Text <> ""

            mRegenerarGrillas()

        Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub txtValorFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValorFecha.TextChanged
		Try
            lblDeudaSocInte.Text = "Intereses al " & txtValorFecha.Text & ":"
            grdDeudaSoc.Columns(mintColuSociInte).HeaderText = "Intereses al " & txtValorFecha.Text
            grdDeudaCtac.Columns(mintColuCtacInte).HeaderText = "Intereses al " & txtValorFecha.Text
            mRegenerarGrillas()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mRegenerarGrillas()

		grdDeudaSoc.CurrentPageIndex = 0
		grdDeudaCtac.CurrentPageIndex = 0
		grdCuotas.CurrentPageIndex = 0
		grdConc.CurrentPageIndex = 0

		mBorrarDetas()
		'mCargarDatos(hdnId.Text)
		If Not usrClie.Valor Is DBNull.Value Then
			If hdnSociId.Text <> "" Then
				mConsultarDeudaSoc()

				mChequearGrilla("Soc")				'Pantanettig -- 22/11/2007
				mCalcularTotalesSelec("Soc")
				If grdDeudaSoc.PageCount > 0 Then
					grdDeudaSoc.CurrentPageIndex = grdDeudaSoc.PageCount - 1
					grdDeudaSoc.DataBind()
					mGuardarCuotasAnteriores(True)
				End If
				'mGuardarDatosDeuda("Soc")
				mConsultarDetas()
				mCalcularTotalesDeudaSoc()
			End If

			mConsultarDeudaCtac()
			mCalcularTotalesDeudaCtac()
			mCalcularTotalesSelec("Ctac")
		End If

		mConsultarCuota()
		mConsultarConc()
	End Sub

	Private Sub btnDeudaCtacAprob_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeudaCtacAprob.Click
		Try
				txtAproCtacFecha.Fecha = Today

				mModi()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		Finally
                txtAproCtacFecha.Fecha = "" ' DBNull.Value
		End Try
	End Sub

	Private Sub btnDeudaSocAprob_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeudaSocAprob.Click
		Try
				txtAproSociFecha.Fecha = Today

				mModi()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		Finally
                txtAproSociFecha.Fecha = "" 'DBNull.Value
		End Try
	End Sub

	Private Sub btnConf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConf.Click
		Try
			If txtAproSociFecha.Fecha Is DBNull.Value Then
					txtAproSociFecha.Fecha = Today
				End If
			If txtAproCtacFecha.Fecha Is DBNull.Value Then
					txtAproCtacFecha.Fecha = Today
				End If

				txtAproFecha.Fecha = Today

				mModi()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		Finally
                txtAproFecha.Fecha = "" 'DBNull.Value"
		End Try
	End Sub

	Private Sub mBorrarDetas()
		For Each lDr As DataRow In mdsDatos.Tables(mstrPlanPagosDeta).Select
			lDr.Delete()
		Next
	End Sub

	Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
		Try
			If hdnImprimio.Text <> "" Then
				Dim lobj As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), SRA_Neg.Constantes.gTab_Comprobantes)

				If hdnImprimir.Text <> "" Then
					lobj.ModiImpreso(hdnImprimir.Text, CBool(CInt(hdnImprimio.Text)))
				End If
			End If

			mLimpiar()
			mConsultar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		Finally
			hdnImprimio.Text = ""
            hdnImprimir.Text = ""
            hdnImprimirInte.Text = ""
		End Try
	End Sub

	Private Sub btnImpr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImpr.Click
		'mModi()
		mImprimir()
	End Sub

End Class
End Namespace
