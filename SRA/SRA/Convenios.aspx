<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Convenios" CodeFile="Convenios.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Convenios</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD width="5"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Convenios</asp:label></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" BorderStyle="Solid"
												Width="97%">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																			IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																			ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 3.93%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																					<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrClieFil" runat="server" Ancho="800" AceptaNull="false" MuestraDesc="False"
																						Saltos="1,1,1,1"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 4.95%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 11.06%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 72%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 3.93%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblActivFil" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbActivFil" runat="server" Width="260px" AceptaNull="false"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 11.06%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 4.95%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 11.06%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 72%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 3.93%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																					<asp:checkbox id="chkVto" Text="Incluir Vencidos" CssClass="titulo" Runat="server"></asp:checkbox></TD>
																			<TR>
																				<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 11.06%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 4.95%; HEIGHT: 2px" align="right" background="imagenes/formdivfin.jpg"
																					height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 5.4%; HEIGHT: 2px" align="right" background="imagenes/formdivfin.jpg"
																					height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 82%; HEIGHT: 2px" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD></TD>
										<TD vAlign="top" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="conv_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="_clie_apel" ReadOnly="True" HeaderText="Cliente">
														<HeaderStyle Width="20%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_activ" ReadOnly="True" HeaderText="Actividad">
														<HeaderStyle Width="20%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="conv_coef" HeaderText="Coeficiente.">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_cont_sldo" ReadOnly="True" HeaderText="Aplica">
														<HeaderStyle Width="5%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="conv_inic_fecha" ReadOnly="True" HeaderText="F.Inicio" DataFormatString="{0:dd/MM/yyyy}">
														<HeaderStyle Width="10%"></HeaderStyle>
														<ItemStyle HorizontalAlign="Left"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="conv_fina_fecha" HeaderText="F.Final" DataFormatString="{0:dd/MM/yyyy}">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="conv_acti_id" HeaderText="conv_acti_id"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="conv_clie_id" HeaderText="conv_clie_id"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="conv_cont_sldo" HeaderText="conv_cont_sldo"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="middle">
											<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
															IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
															ImageOver="btnNuev2.gif" ForeColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Agregar un Nuevo Convenio"></CC1:BOTONIMAGEN></TD>
													<TD></TD>
													<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
															BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
															BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent" ImageDisable="btnImpr0.gif"
															ToolTip="Listar Convenios"></CC1:BOTONIMAGEN></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD></TD>
										<TD align="center">
											<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" BorderWidth="1px"
													BorderStyle="Solid" Height="116px">
													<P align="right">
														<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
															border="0">
															<TR>
																<TD align="right" colSpan="3">
																	<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" align="right">
																	<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<UC1:CLIE id="usrClie" runat="server" Ancho="800" AceptaNull="false" MuestraDesc="False" Saltos="1,1,1,1"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" align="right">
																	<asp:Label id="lblActiv" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<cc1:combobox class="combo" id="cmbActiv" runat="server" Width="260px" AceptaNull="false"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" align="right">
																	<asp:Label id="lblCoef" runat="server" cssclass="titulo">Coeficiente:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<CC1:NUMBERBOX id="txtCoef" runat="server" cssclass="textomonto" Width="50px" CantMax="4" EsDecimal="True"
																		Obligatorio="True"></CC1:NUMBERBOX></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" align="right"></TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<asp:CheckBox id="chkAplicaSaldo" Text="Requiere saldo acreedor en cuenta" CssClass="titulo" Runat="server"></asp:CheckBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" align="right">
																	<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">F.Inicio:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" width="68px" Obligatorio="True"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" align="right">
																	<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">F.Fin:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" width="68px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" vAlign="top" align="right">
																	<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" TextMode="MultiLine"
																		EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD align="center" colSpan="3">
																	<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 130px; HEIGHT: 16px" align="right"></TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16"></TD>
															</TR>
															<TR>
																<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
																	<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																	<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>
																	<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>
																	<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																	height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
														</TABLE>
													</P>
												</asp:panel></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
								   	
		</SCRIPT>
		</TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
