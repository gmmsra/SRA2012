' Dario 2013-09-19 se agrega dato centro emisor al usuario
Namespace SRA

Partial Class usuarios
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents grdMate As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnAltaMail As System.Web.UI.WebControls.Button
    Protected WithEvents btnBajaMail As System.Web.UI.WebControls.Button
    Protected WithEvents btnModiMail As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimpMail As System.Web.UI.WebControls.Button
    Protected WithEvents grdMail As System.Web.UI.WebControls.DataGrid
    Protected WithEvents hdnMealId As System.Web.UI.WebControls.TextBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "usuarios"
    Private mstrActividad As String = "usuarios_actividades"
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mdsDatos As DataSet
    Private mstrConn As String
    Private mstrItemCons As String

    Private Enum Columnas As Integer
        IdUsua = 1

    End Enum

    Private Enum ColumnasDeta As Integer
        IdActiv = 1
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()


            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mCargarCombos()
                mConsultar(grdDato, False)
                tabLinks.Visible = False
                panBotones.Visible = False

                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
                mRetener()
            End If



        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")

        btnBajaActiv.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")


    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "perfiles", cmbPerfil, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "impresoras", cmbImprId, "S")
        ' Dario 2013-09-19 se agrega dato centro emisor al usuario
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", Me.cmbCentroEmisor, "N")
    End Sub

    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Usuarios, String), (mstrConn), (Session("sUserId").ToString()))) Then
        'Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtApel.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "usua_apel")
        txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "usua_nomb")
        txtEmail.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "usua_email")
        txtCargo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "usua_cargo")
        txtTele.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "usua_tele")
        txtUser.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "usua_user")
    End Sub


#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        mstrItemCons = Request.QueryString("id")
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

    End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultar(grdDato, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


#End Region

#Region "Seteo de Controles"

    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla

            Case mstrActividad
                btnBajaActiv.Enabled = Not (pbooAlta)
                btnModiActiv.Enabled = Not (pbooAlta)
                btnAltaActiv.Enabled = pbooAlta
            Case Else
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnAlta.Enabled = pbooAlta
        End Select
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

        mLimpiar()
        mLimpiarActividad()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.IdUsua).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                    hdnId.Text = .Item("usua_id").ToString
                    txtApel.Valor = .Item("usua_apel").ToString
                    txtNomb.Valor = .Item("usua_nomb").ToString
                    txtEmail.Valor = .Item("usua_email").ToString
                    txtCargo.Valor = .Item("usua_cargo").ToString
                    txtTele.Valor = .Item("usua_tele").ToString
                    cmbPerfil.Valor = .Item("usua_perf_id").ToString
                    cmbImprId.Valor = .Item("usua_impr_id").ToString
                    cmbImpr.Valor = .Item("usua_impre").ToString
                    txtUser.Valor = .Item("usua_user").ToString
                    txtPass.Valor = .Item("usua_pass").ToString
                    hdnPass.Text = .Item("usua_pass").ToString
                ' Dario 2013-09-19 se agrega dato centro emisor al usuario
                Me.cmbCentroEmisor.Valor = .Item("usua_emct_id").ToString()
            End With
            mSetearEditor("", False)
            mMostrarPanel(True)
            mShowTabs(1)

        End If
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()

        hdnId.Text = ""

        txtApel.Text = ""
        txtNomb.Text = ""
        txtEmail.Text = ""
        txtCargo.Text = ""
        txtTele.Text = ""
        cmbPerfil.Limpiar()
        cmbImprId.Limpiar()
        ' Dario 2013-09-19 se agrega dato centro emisor al usuario
        Me.cmbCentroEmisor.Limpiar()

        cmbImpr.SelectedIndex = 0
        txtUser.Text = ""
        txtPass.Text = ""
        hdnPass.Text = ""


        mLimpiarActividad()

        mCrearDataSet("")

        lblTitu.Text = ""

        grdActiv.CurrentPageIndex = 0

        mSetearEditor("", True)
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        grdDato.Visible = Not panDato.Visible
        btnAgre.Visible = Not (panDato.Visible)


        lnkCabecera.Font.Bold = True
        lnkActiv.Font.Bold = False

        panCabecera.Visible = True
        panActiv.Visible = False

        tabLinks.Visible = pbooVisi

        lnkCabecera.Font.Bold = True
        lnkActiv.Font.Bold = False
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panCabecera.Visible = False
        panActiv.Visible = False



        lnkCabecera.Font.Bold = False
        lnkActiv.Font.Bold = False

        Dim val As String

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Text = "Datos del plan de mesa: " & txtApel.Text
            Case 2
                clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActiv, "S")
                panActiv.Visible = True
                lnkActiv.Font.Bold = True
                lblTitu.Text = "Actividades del usuario: " & txtApel.Text
                grdActiv.Visible = True
                btnBajaActiv.Enabled = False
                btnModiActiv.Enabled = False
                btnAltaActiv.Enabled = True

        End Select
    End Sub

    Private Sub mCargarComboConDataSet(ByVal cmb As NixorControls.ComboBox, ByVal dataTable As Data.DataTable, ByVal strId As String, ByVal strDesc As String)

        cmb.DataSource = dataTable
        cmb.DataValueField = strId
        cmb.DataTextField = strDesc
        cmb.DataBind()
        cmb.Items.Insert(0, "(Seleccione)")
        cmb.Items(0).Value = ""
    End Sub

    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub


#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjAsamblea As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjAsamblea.Alta()
            mConsultar(grdDato, True)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try

            mGuardarDatos()
            Dim lobjUsuario As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjUsuario.Modi()

            mConsultar(grdDato, True)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar(grdDato, True)

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage

            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
    Private Function mGuardarDatos() As DataSet


        mValidarDatos()

        If (mdsDatos.Tables(1).Select("")).GetUpperBound(0) <= -1 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar al menos una actividad para el usuario.")
        End If

        If (mdsDatos.Tables(1).Select("usac_defa<>0")).GetUpperBound(0) <> 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar una �nica actividad como default para el usuario.")
        End If

        With mdsDatos.Tables(0).Rows(0)
            .Item("usua_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            .Item("usua_apel") = txtApel.Valor
            .Item("usua_nomb") = txtNomb.Valor
            .Item("usua_email") = txtEmail.Valor
            .Item("usua_cargo") = txtCargo.Valor
            .Item("usua_tele") = txtTele.Valor
                .Item("usua_perf_id") = cmbPerfil.Valor
                If cmbImprId.Valor.Length > 0 Then
                    .Item("usua_impr_id") = cmbImprId.Valor
                End If
                .Item("usua_user") = txtUser.Valor
                .Item("usua_impre") = cmbImpr.Valor
                ' Dario 2013-09-19 se agrega dato centro emisor al usuario
                .Item("usua_emct_id") = Me.cmbCentroEmisor.Valor

                If Not btnAlta.Enabled Then
                    If txtPass.Text <> "" Then
                        If txtPass.Text <> hdnPass.Text Then
                            .Item("usua_pass") = SRA_Neg.Encript.gEncriptar(txtPass.Text)
                        Else
                            .Item("usua_pass") = txtPass.Text
                        End If
                    Else
                        .Item("usua_pass") = hdnPass.Text
                    End If
                Else
                    If txtPass.Text <> "" Then
                        .Item("usua_pass") = SRA_Neg.Encript.gEncriptar(txtPass.Text)
                    ElseIf hdnPass.Text <> "" Then
                        .Item("usua_pass") = SRA_Neg.Encript.gEncriptar(hdnPass.Text)
                    End If
                End If




                If .IsNull("usua_pass") Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar la contrase�a")
                End If
            End With

        Return mdsDatos
    End Function


    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrActividad

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If


        grdActiv.DataSource = mdsDatos.Tables(mstrActividad)
        grdActiv.DataBind()


        Session(mstrTabla) = mdsDatos

    End Sub


#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub mRetener()
        If txtPass.Text = "" Then
            txtPass.Text = hdnPass.Text
        End If
    End Sub


#End Region

#Region "Detalle"
    Public Sub mEditarDatosActiv(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrActiv As DataRow
            hdnActivId.Text = E.Item.Cells(1).Text
            ldrActiv = mdsDatos.Tables(mstrActividad).Select("usac_id=" & hdnActivId.Text)(0)

            With ldrActiv
                cmbActiv.Valor = .Item("usac_acti_id")
                chkDefa.Checked = (.Item("usac_defa") <> 0)
            End With

            mSetearEditor(mstrActividad, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal pOk As Boolean)
        Try
            Dim strFin As String = ""
            If (Not pOk) Then
                strFin = "@ejecuta = N "
            End If
            mstrCmd = "exec " + mstrTabla + "_consul " + strFin
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarActividad()

        Dim ldrActiv As DataRow

        'si no posee actividades default ingresa la nueva como default
        If (mdsDatos.Tables(1).Select("usac_defa<>0")).GetUpperBound(0) <= -1 Then
            chkDefa.Checked = True
        End If

        'si se ingresa una actividad como default blanquear el resto
        If chkDefa.Checked Then
            For Each odrActi As DataRow In mdsDatos.Tables(1).Select("usac_defa<>0")
                odrActi.Item("_defa") = ""
                odrActi.Item("usac_defa") = 0
            Next
        End If

        If hdnActivId.Text = "" Then
            ldrActiv = mdsDatos.Tables(mstrActividad).NewRow
            ldrActiv.Item("usac_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrActividad), "usac_id")
        Else
            ldrActiv = mdsDatos.Tables(mstrActividad).Select("usac_id=" & hdnActivId.Text)(0)
        End If

        If (cmbActiv.SelectedItem.Text = "(Seleccione)") Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la actividad.")
        End If

        With ldrActiv
            .Item("usac_acti_id") = cmbActiv.Valor
            .Item("_actividad") = cmbActiv.SelectedItem.Text
            .Item("usac_defa") = chkDefa.Checked
            .Item("_defa") = IIf(chkDefa.Checked, "Si", "")
        End With

        If (mEstaEnElDataSet(mdsDatos.Tables(mstrActividad), ldrActiv, "usac_acti_id", "usac_id")) Then
            Throw New AccesoBD.clsErrNeg("El usuario ya tiene la actividad seleccionada.")
        Else
            If (hdnActivId.Text = "") Then
                mdsDatos.Tables(mstrActividad).Rows.Add(ldrActiv)
            End If
        End If
    End Sub



    Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrActiv As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
        Dim filtro As String
        filtro = campo1 + " = " + ldrActiv.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrActiv.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrActiv.Item(campoId).ToString()
        Return (table.Select(filtro).GetLength(0) > 0)
    End Function

    Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCate As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
        Return mEstaEnElDataSet(table, ldrCate, campo1, campo1, campoId)
    End Function

    Private Sub mLimpiarActividad()
        hdnActivId.Text = ""
        cmbActiv.Limpiar()
        chkDefa.Checked = False
        mSetearEditor(mstrActividad, True)
    End Sub



#End Region



    Public Sub grdActiv_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdActiv.EditItemIndex = -1
            If (grdActiv.CurrentPageIndex < 0 Or grdActiv.CurrentPageIndex >= grdActiv.PageCount) Then
                grdActiv.CurrentPageIndex = 0
            Else
                grdActiv.CurrentPageIndex = E.NewPageIndex
            End If
            grdActiv.DataSource = mdsDatos.Tables(mstrActividad)
            grdActiv.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAltaCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaActiv.Click
        ActualizarActividades()
    End Sub

    Private Sub btnModiCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiActiv.Click
        ActualizarActividades()
    End Sub

    Private Sub ActualizarActividades()
        Try
            mGuardarActividad()

            mLimpiarActividad()
            grdActiv.DataSource = mdsDatos.Tables(mstrActividad)
            grdActiv.DataBind()


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub lnkCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkActiv.Click
        mShowTabs(2)
    End Sub

    Private Sub btnBajaActiv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaActiv.Click
        Try
            mdsDatos.Tables(mstrActividad).Select("usac_id=" & hdnActivId.Text)(0).Delete()
            grdActiv.DataSource = mdsDatos.Tables(mstrActividad)
            grdActiv.DataBind()
            mLimpiarActividad()


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpActiv.Click
        mLimpiarActividad()
    End Sub

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

End Class
End Namespace
