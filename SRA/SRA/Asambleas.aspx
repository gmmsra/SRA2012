<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Asambleas" CodeFile="Asambleas.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Asambleas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function setCheckList(ctrl)
		{
			if (document.all("cmbCateC").value != "")
			{
				var lstrRet = LeerCamposXML("categorias", document.all("cmbCateC").value, "cate_subc");
				if (lstrRet == "1")
				{													
					for (i = 0; i < 3; i++)
					{
						document.getElementById(ctrl + '_' + i).checked = false;					
					}
				}
				else
				{									
					for (i = 0; i < 3; i++)
					{
						document.getElementById(ctrl + '_' + i).checked = true;					
					}
				}
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px" height="33"></TD>
									<TD style="HEIGHT: 33px" vAlign="bottom" colSpan="2" height="33"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Asambleas</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 5px" width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="0%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="asam_id" ReadOnly="True" HeaderText="ID">
													<HeaderStyle Width="2%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="asam_nume" ReadOnly="True" HeaderText="Nro"></asp:BoundColumn>
												<asp:BoundColumn DataField="asam_desc" ReadOnly="True" HeaderText="Descripci&#243;n"></asp:BoundColumn>
												<asp:BoundColumn DataField="asam_abre" HeaderText="Abreviatura"></asp:BoundColumn>
												<asp:BoundColumn DataField="_tipo" HeaderText="Tipo"></asp:BoundColumn>
												<asp:BoundColumn DataField="asam_real_fecha" HeaderText="Realizaci&#243;n" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="asam_anti_fecha" HeaderText="Antig&#252;edad" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="asam_cate_fecha" HeaderText="Categor&#237;a" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ImageDisable="btnNuev0.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Asamblea"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Height="21px" Width="80px" CausesValidation="False"> Asamblea</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkTema" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Height="21px" Width="80px" CausesValidation="False">Temas</asp:linkbutton></td>
												<td width="1" background="imagenes/tab_fondo.bmp"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCate" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Height="21px" Width="80px" CausesValidation="False">Categor�as</asp:linkbutton></td>
												<td width="1" background="imagenes/tab_fondo.bmp"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkestados" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Height="21px" Width="80px" CausesValidation="False">Estados</asp:linkbutton></td>
												<td width="1" background="imagenes/tab_fondo.bmp"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDist" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Height="21px" Width="80px" CausesValidation="False">Distritos</asp:linkbutton></td>
												<td width="1" background="imagenes/tab_fondo.bmp"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" width="99%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" align="right" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblNum" runat="server" cssclass="titulo">Numero:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:NumberBox id="txtNum" runat="server" cssclass="cuadrotexto" Width="64px"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="350px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAbre" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 21px" colSpan="2" height="21">
																			<CC1:TEXTBOXTAB id="txtAbre" runat="server" cssclass="cuadrotexto" Width="144px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 22px" colSpan="2" height="22">
																			<cc1:combobox class="combo" id="cmbTipo" runat="server" Width="352px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblReal" runat="server" cssclass="titulo">Realizaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 21px" colSpan="2" height="21">
																			<cc1:DateBox id="txtReal" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="True"
																				onchange="txtReal_change();"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnti" runat="server" cssclass="titulo">Antig�edad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 13px" colSpan="2" height="13">
																			<cc1:DateBox id="txtAnti" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblCate" runat="server" cssclass="titulo">Padr�n Al:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 19px" colSpan="2" height="19">
																			<cc1:DateBox id="txtCate" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblCierre" runat="server" cssclass="titulo">Cerrar Generaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 19px" colSpan="2" height="19">
																			<cc1:combobox class="combo" id="cmbCierre" runat="server" Width="70px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnio" runat="server" cssclass="titulo">Deuda Al:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:NumberBox id="txtPerio" runat="server" cssclass="cuadrotexto" Width="34px" Obligatorio="True"></CC1:NumberBox>
																			<cc1:combobox class="combo" id="cmbPeti" runat="server" Width="143px" Obligatorio="True"></cc1:combobox>&nbsp;
																			<CC1:NumberBox id="txtAnio" runat="server" cssclass="cuadrotexto" Width="54px" Obligatorio="True"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Height="56px" Width="296px" TextMode="MultiLine"
																				Columns="9"></CC1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCate" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdCate" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdCate_PageChanged"
																				OnEditCommand="mEditarDatosCate" AutoGenerateColumns="False" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="asca_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_cate_desc" HeaderText="Categor&#237;a"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_ci" HeaderText="C.I."></asp:BoundColumn>
																					<asp:BoundColumn DataField="_cf" HeaderText="C.F."></asp:BoundColumn>
																					<asp:BoundColumn DataField="_sin" HeaderText="Sin Inst."></asp:BoundColumn>
																					<asp:BoundColumn DataField="asca_grupo" HeaderText="Grupo"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:Label id="lblCateC" runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<cc1:combobox class="combo" id="cmbCateC" runat="server" Width="400px" onchange="setCheckList('lstInst');"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" vAlign="top" align="right">
																			<asp:Label id="lblInst" runat="server" cssclass="titulo">Instituci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<asp:CheckBoxList id="lstInst" runat="server" width="100%" CellSpacing="0" CellPadding="0" CssClass="titulo">
																				<asp:ListItem Value="1">CONSEJO INSTITUCIONAL</asp:ListItem>
																				<asp:ListItem Value="3">CONSEJO FEDERAL</asp:ListItem>
																				<asp:ListItem Value="0">SIN INSTITUCION</asp:ListItem>
																			</asp:CheckBoxList></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblGrupo" runat="server" cssclass="titulo">Grupo:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:NumberBox id="txtGrupo" runat="server" cssclass="cuadrotexto" Width="64px" AceptaNull="False"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaCate" runat="server" cssclass="boton" Width="123px" Text="Agregar Categor�a"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaCate" runat="server" cssclass="boton" Width="123px" Text="Eliminar Categor�a"></asp:Button>&nbsp;
																			<asp:Button id="btnModiCate" runat="server" cssclass="boton" Width="129px" Text="Modificar Categor�a"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpCate" runat="server" cssclass="boton" Width="119px" Text="Limpiar Categor�a"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="PanEstados" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TableEstados" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdEsta" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdEsta_PageChanged"
																				OnEditCommand="mEditarDatosEsta" AutoGenerateColumns="False" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="ases_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_Estado" HeaderText="Estado">
																						<HeaderStyle Width="98%"></HeaderStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<cc1:combobox class="combo" id="cmbEsta" runat="server" Width="400px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaEsta" runat="server" cssclass="boton" Width="123px" Text="Agregar Estado"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaEsta" runat="server" cssclass="boton" Width="123px" Text="Eliminar Estado"></asp:Button>&nbsp;
																			<asp:Button id="btnModiEsta" runat="server" cssclass="boton" Width="129px" Text="Modificar Estado"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpEsta" runat="server" cssclass="boton" Width="119px" Text="Limpiar Estado"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="PanTemas" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TableTemas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdTema" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdTema_PageChanged"
																				OnEditCommand="mEditarDatosTema" AutoGenerateColumns="False" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="9px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn HeaderStyle-Width="20px">
																						<ItemTemplate>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="aste_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="aste_nume" HeaderText="Nro." HeaderStyle-Width="50px"></asp:BoundColumn>
																					<asp:BoundColumn DataField="aste_tema" HeaderText="Tema"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:Label id="lblTema" runat="server" cssclass="titulo">Tema:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<CC1:TEXTBOXTAB id="txtTema" runat="server" cssclass="cuadrotexto" Width="150px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaTema" runat="server" cssclass="boton" Width="123px" Text="Agregar Tema"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaTema" runat="server" cssclass="boton" Width="123px" Text="Eliminar Tema"></asp:Button>&nbsp;
																			<asp:Button id="btnModiTema" runat="server" cssclass="boton" Width="129px" Text="Modificar Tema"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpTema" runat="server" cssclass="boton" Width="119px" Text="Limpiar Tema"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="PanDistritos" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TableDistritos" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TBODY>
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdDist" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																					HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdDist_PageChanged"
																					OnEditCommand="mEditarDatosDist" AutoGenerateColumns="False" Visible="False">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="asdi_id" ReadOnly="True" HeaderText="ID">
																							<HeaderStyle Width="2%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="_dist_desc" HeaderText="Distrito">
																							<HeaderStyle Width="98%"></HeaderStyle>
																						</asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblDist" runat="server" cssclass="titulo">Distrito:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px">
																				<cc1:combobox class="combo" id="cmbDist" runat="server" Width="400px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="center" colSpan="2" height="30">
																				<asp:Button id="btnAltaDist" runat="server" cssclass="boton" Width="123px" Text="Agregar Distrito"></asp:Button>&nbsp;
																				<asp:Button id="btnBajaDist" runat="server" cssclass="boton" Width="123px" Text="Eliminar Distrito"></asp:Button>&nbsp;
																				<asp:Button id="btnModiDist" runat="server" cssclass="boton" Width="129px" Text="Modificar Distrito"></asp:Button>&nbsp;
																				<asp:Button id="btnLimpDist" runat="server" cssclass="boton" Width="119px" Text="Limpiar Distrito"></asp:Button></TD>
																		</TR>
													</TR>
													<TR>
														<TD vAlign="middle" align="center" colSpan="2" height="30">
															<asp:Button id="btnAltaDistTodos" runat="server" cssclass="boton" Width="100px" Text="Todos"></asp:Button>&nbsp;
															<asp:Button id="btnBajaaDistTodos" runat="server" cssclass="boton" Width="100px" Text="Ninguno"></asp:Button></TD>
													</TR>
												</TABLE>
											</asp:panel>
									</TD>
								</TR>
							</TABLE>
							</asp:panel><ASP:PANEL id="panBotones" Runat="server">
								<TABLE width="100%">
									<TR>
										<TD align="center"></TD>
									</TR>
									<TR height="30">
										<TD align="center"><A id="editar" name="editar"></A>
											<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
											<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Baja"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Modificar"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Limpiar"></asp:Button></TD>
									</TR>
								</TABLE>
							</ASP:PANEL></DIV></td>
					</tr>
				</TBODY>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
			</TR>
			<tr>
				<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
				<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
				<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnEstaId" runat="server"></asp:textbox><asp:textbox id="hdnCateId" runat="server"></asp:textbox><asp:textbox id="hdnTemaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDistId" runat="server"></asp:textbox><asp:textbox id="hdnAsigDragOri" runat="server"></asp:textbox><asp:textbox id="hdnAsigDragDesti" runat="server" AutoPostBack="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		function cargarComboConTiposDePeriodo(pCmb)
		{					
		    cargarCombo("cmbPeriCarre",pCmb) 	
		}		
		
		function cargarCombo(pCmbPeri,pCmb)
		{	
		    var sFiltro = obtenerFiltro(pCmb)			    	    	
		    LoadComboXML("periodo_tipos_lectu_cargar", sFiltro, pCmbPeri, "S");		   			    
		}

		function obtenerFiltro(pCmb)
		{	
		    var sFiltro = "@mate_id = " + pCmb.value;
		    return sFiltro;
		}
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();

		var gstrValor = '';

		function mTurnosDrag(pValor) 
		{
				gstrValor = pValor;
				event.dataTransfer.setData("Text", ""); 
				event.dataTransfer.effectAllowed = "copy";
		}
		function mTurnosDrop(pValor) 
		{
			document.all("hdnAsigDragOri").value = gstrValor;
			document.all("hdnAsigDragDesti").value = pValor;

			event.returnValue = false;                           
			event.dataTransfer.dropEffect = "copy"; 

			__doPostBack('hdnAsigDragDesti','');				
		}
		function mTurnosDropCancel(pObje) 
		{
			event.returnValue = false;    
			event.dataTransfer.dropEffect = "copy"; 
		}
		
		function txtReal_change()
		{
			try
			{
				var sFiltro=document.all["txtReal"].value;
				var vstrRet;
							
				if (sFiltro!="")
				{
					vstrRet = LeerCamposXML("asambleas_fechas", "'" + sFiltro + "'", "anti_fecha,cate_fecha,deuda_anio,deuda_perio,deuda_peti_id").split("|");
				}
				if(vstrRet!=undefined)
				{
					{
						document.all("txtAnti").value = vstrRet[0];
						document.all("txtCate").value = vstrRet[1];
						document.all("txtAnio").value = vstrRet[2];
						document.all("txtPerio").value = vstrRet[3];
						document.all("cmbPeti").value = vstrRet[4];
					}
				}
			}
			catch(e){;}
		}
		</SCRIPT>
		</TD></TR></TBODY></TABLE>
	</BODY>
</HTML>
