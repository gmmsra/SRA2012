Imports System.Data.SqlClient


Namespace SRA


Partial Class AnillosSoli
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents lblAniTipo As System.Web.UI.WebControls.Label
    Protected WithEvents lblAniNume As System.Web.UI.WebControls.Label
    Protected WithEvents lblAniRefe As System.Web.UI.WebControls.Label
    Protected WithEvents txtAniArea As NixorControls.TextBoxTab
    Protected WithEvents txtAniNume As NixorControls.TextBoxTab
    Protected WithEvents txtAniRefe As NixorControls.TextBoxTab
    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Anillos_Movim_Cabe
   Private mstrSoliRaza As String = SRA_Neg.Constantes.gTab_Criaderos_Razas
   Private mstrCriaderos As String = SRA_Neg.Constantes.gTab_Criaderos
   Private mstrSoliDeta As String = SRA_Neg.Constantes.gTab_Anillos_Movim_Deta
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String

   Private Enum Columnas As Integer
      SoliEdit = 0
      SoliId = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Dim lstrFil As String
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mSess(mstrTabla)) = Nothing
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mMostrarPanel(False)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mSess(mstrTabla))
               Dim x As String = Session.SessionID
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "rg_anillos_cargar", cmbAni, "id", "descrip", "S")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @espe_genealo=1", cmbRaza, "id", "descrip_codi", "S")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaAni.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaRaza.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()

      Dim lstrSoliLong As Object
      Dim lintCol As Integer

      lstrSoliLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrSoliLong, "amca_obse")
      txtDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrSoliLong, "amca_dire")

      txtAniCant.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      usrCria.Tabla = mstrCriaderos
      usrCria.AutoPostback = False
      usrCria.Ancho = 790
      usrCria.Alto = 510
      usrCria.FilRaza = True
      usrCria.FilLoca = True

      usrCriaFil.Tabla = mstrCriaderos
      usrCriaFil.AutoPostback = False
      usrCriaFil.Ancho = 790
      usrCriaFil.Alto = 510
      usrCriaFil.FilRaza = True
      usrCriaFil.FilLoca = True

   End Sub

   Private Function mValorParametro(ByVal pstrPara As String) As String
      Dim lstrPara As String
      If pstrPara Is Nothing Then
         lstrPara = ""
      Else
         If pstrPara Is System.DBNull.Value Then
            lstrPara = ""
         Else
            lstrPara = pstrPara
         End If
      End If
      Return (lstrPara)
   End Function

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As String

         lstrCmd = "exec " + SRA_Neg.Constantes.gTab_Anillos_Movim_Cabe + "_consul"
			lstrCmd = lstrCmd & " @crdr_id=" + clsSQLServer.gFormatArg(usrCriaFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & ",@todo = " + IIf(chkTodo.Checked, "1", "0")

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

         grdDato.Visible = True

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrSoliDeta
            btnBajaAni.Enabled = Not (pbooAlta)
            btnModiAni.Enabled = Not (pbooAlta)
            btnAltaAni.Enabled = pbooAlta
         Case mstrSoliRaza
            btnBajaRaza.Enabled = True 'Not (pbooAlta)
            btnModiRaza.Enabled = True 'Not (pbooAlta)
            btnAltaRaza.Enabled = True 'pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
            btnImpr.Enabled = Not (pbooAlta)
      End Select
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(Columnas.SoliId).Text)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(pstrId)

      mCrearDataSet(hdnId.Text)

      grdAni.CurrentPageIndex = 0
      grdRaza.CurrentPageIndex = 0

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            hdnId.Text = .Item("amca_id")
            txtNume.Text = .Item("amca_nume")
            txtObse.Valor = .Item("amca_obse")
            txtDire.Valor = .Item("amca_dire")
            txtNaciFecha.Text = .Item("amca_naci_fecha")
            txtSoliFecha.Text = .Item("amca_soli_fecha")
            usrCria.Valor = .Item("amca_crdr_id")
            hdnCriaId.Text = .Item("amca_crdr_id")

            If Not .IsNull("amca_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("amca_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If

            lblTitu.Text = "Datos de la Solicitud: " & .Item("amca_soli_fecha")
            lblTitu.Visible = False
         End With

         mSetearEditor("", False)
         mSetearEditor(mstrSoliDeta, True)
         mSetearEditor(mstrSoliRaza, True)
         mMostrarPanel(True)
         mShowTabs(1)

      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()

      hdnValorId.Text = "-1"  'para que ignore el id que vino del control
      chkTodo.Checked = False
      usrCriaFil.Limpiar()
      grdDato.Visible = False

   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      hdnCriaId.Text = ""
      lblBaja.Text = ""
      txtObse.Text = ""
      txtDire.Text = ""
      txtNume.Text = ""
      txtNaciFecha.Text = ""
      txtSoliFecha.Text = ""
      usrCria.Limpiar()

      mLimpiarAnillo()

      mCrearDataSet("")

      lblTitu.Text = ""

      grdAni.CurrentPageIndex = 0
      grdRaza.CurrentPageIndex = 0

      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = Not panDato.Visible

      lnkCabecera.Font.Bold = True
      lnkAni.Font.Bold = False

      panDato.Visible = pbooVisi
      panFiltros.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True
		panAni.Visible = False
		panRaza.Visible = False

      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkAni.Font.Bold = False

   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      Dim lstrTitu As String
      If lblTitu.Text <> "" Then
         lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
      End If

      panDato.Visible = True
      panBotones.Visible = True
      btnAgre.Visible = False
      panAni.Visible = False
      panRaza.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkAni.Font.Bold = False
      lnkRaza.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Visible = False
         Case 2
            panAni.Visible = True
            lnkAni.Font.Bold = True
            lblTitu.Text = "Detalle de la Solicitud: " & lstrTitu
            lblTitu.Visible = True
            grdAni.Visible = True
         Case 3
            panRaza.Visible = True
            lnkRaza.Font.Bold = True
            lblTitu.Text = "Razas del Criadero: " & usrCria.Codi & " - " & usrCria.Nomb
            lblTitu.Visible = True
            grdRaza.Visible = True
      End Select

   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrSoliId As String

         mGuardarDatos()
         Dim lobjSoli As New SRA_Neg.Anillos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrSoliDeta, mstrSoliRaza, mdsDatos, Server)

         lstrSoliId = lobjSoli.Alta()

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjSoli As New SRA_Neg.Anillos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrSoliDeta, mstrSoliRaza, mdsDatos, Server)

         lobjSoli.Modi()

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjSoli As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjSoli.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      With mdsDatos.Tables(0).Rows(0)
         .Item("amca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("amca_soli_fecha") = txtSoliFecha.Fecha
         .Item("amca_naci_fecha") = txtNaciFecha.Fecha
         .Item("amca_dire") = txtDire.Valor
         .Item("amca_obse") = txtObse.Valor
         .Item("amca_amti_id") = 1
         .Item("amca_crdr_id") = usrCria.Valor
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrSoliDeta
      mdsDatos.Tables(2).TableName = mstrSoliRaza

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      grdAni.DataSource = mdsDatos.Tables(mstrSoliDeta)
      grdAni.DataBind()

      grdRaza.DataSource = mdsDatos.Tables(mstrSoliRaza)
      grdRaza.DataBind()

      Session(mSess(mstrTabla)) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

#Region "Detalle"
   Public Sub mEditarDatosAni(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrAni As DataRow
         Dim lstrActiProp As String
         Dim lstrActiNoProp As String
         Dim lbooGene As Boolean

         hdnAniId.Text = E.Item.Cells(1).Text
         ldrAni = mdsDatos.Tables(mstrSoliDeta).Select("amde_id=" & hdnAniId.Text)(0)

         With ldrAni
            txtAniCant.Valor = .Item("amde_cant")
            cmbAni.Valor = .Item("amde_anil_id")
         End With

         mSetearEditor(mstrSoliDeta, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosRaza(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrRaza As DataRow

         hdnRazaId.Text = E.Item.Cells(1).Text
         ldrRaza = mdsDatos.Tables(mstrSoliRaza).Select("crrz_id=" & hdnRazaId.Text)(0)

         With ldrRaza
            cmbRaza.Valor = .Item("crrz_raza_id")
         End With

         mSetearEditor(mstrSoliRaza, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdAni_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdAni.EditItemIndex = -1
         If (grdAni.CurrentPageIndex < 0 Or grdAni.CurrentPageIndex >= grdAni.PageCount) Then
            grdAni.CurrentPageIndex = 0
         Else
            grdAni.CurrentPageIndex = E.NewPageIndex
         End If
         grdAni.DataSource = mdsDatos.Tables(mstrSoliDeta)
         grdAni.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdRaza_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdRaza.EditItemIndex = -1
         If (grdRaza.CurrentPageIndex < 0 Or grdRaza.CurrentPageIndex >= grdRaza.PageCount) Then
            grdRaza.CurrentPageIndex = 0
         Else
            grdRaza.CurrentPageIndex = E.NewPageIndex
         End If
         grdRaza.DataSource = mdsDatos.Tables(mstrSoliRaza)
         grdRaza.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mImprimir()

      Try

         Dim lstrRptName As String = "SolicitudAnillos"
         Dim lsbMsg As String
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         Dim lstrId As String = hdnId.Text

         Dim lstrRepo As String = System.Configuration.ConfigurationSettings.AppSettings("conRepo").ToString()
         Dim lstrRepoDire As String = System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString()

         lsbMsg = "<OBJECT ID=RSClientPrint CLASSID='CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3' CODEBASE='http://" & lstrRepo & "/" & lstrRepoDire & "/rptproxy.aspx?rs:Command=Get&rc:GetImage=8.00.1038.00rsclientprint.cab#Version=2000,080,1038,000' VIEWASTEXT></OBJECT>"
         lsbMsg = lsbMsg & "<SCRIPT language='javascript'>"
         lsbMsg = lsbMsg & "RSClientPrint.MarginLeft = 10;"
         lsbMsg = lsbMsg & "RSClientPrint.MarginTop = 10;"
         lsbMsg = lsbMsg & "RSClientPrint.MarginRight = 0;"
         lsbMsg = lsbMsg & "RSClientPrint.MarginBottom = 10;"
         lsbMsg = lsbMsg & "RSClientPrint.PageHeight = 297;"
         lsbMsg = lsbMsg & "RSClientPrint.PageWidth = 210;"
         lsbMsg = lsbMsg & "RSClientPrint.Culture = 3082;"
         lsbMsg = lsbMsg & "RSClientPrint.UICulture = 10;"
         lsbMsg = lsbMsg & "var ldecRand = Math.random();"
         lsbMsg = lsbMsg & "ldecRand = ldecRand * 1000;"
         lsbMsg = lsbMsg & "ldecRand = Math.round(ldecRand);"
         lsbMsg = lsbMsg & "var lstrRand = new Number(ldecRand).toString();"
         lsbMsg = lsbMsg & "try { "
            lsbMsg = lsbMsg & " RSClientPrint.Print('http://" & lstrRepo & "/rptproxy.aspx', '/" & lstrRepoDire & "/" & lstrRptName & "&amca_id=" & lstrId & "&random='+lstrRand,'" & lstrRptName & "');"
         lsbMsg = lsbMsg & "} catch(e) { "
         lsbMsg = lsbMsg & " alert('Error al intentar efectuar la impresi�n');"
         lsbMsg = lsbMsg & "} "
         lsbMsg = lsbMsg & "</SCRIPT>"
         Response.Write(lsbMsg)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub mActualizarAnillo(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosAni(pbooAlta)

         mLimpiarAnillo()
         grdAni.DataSource = mdsDatos.Tables(mstrSoliDeta)
         grdAni.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarRaza(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosRaza(pbooAlta)

         mLimpiarRaza()
         grdRaza.DataSource = mdsDatos.Tables(mstrSoliRaza)
         grdRaza.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosAni(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
      Dim lstrCheck As System.Web.UI.WebControls.CheckBox
      Dim lstrProp As NixorControls.ComboBox
      Dim lstrActiProp As String
      Dim lstrActiNoProp As String
      Dim lstrActiPropDesc As String

      If txtAniCant.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la cantidad.")
      End If

      If cmbAni.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el anillo.")
      End If

      If Not mValidarRaza(cmbAni.Valor, usrCria.Valor) Then
         Throw New AccesoBD.clsErrNeg("El anillo no corresponde a las razas asociadas al criadero.")
      End If

      If hdnAniId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrSoliDeta).NewRow
         ldrDatos.Item("amde_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrSoliDeta), "amde_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrSoliDeta).Select("amde_id=" & hdnAniId.Text)(0)
      End If

      With ldrDatos
         .Item("amde_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
         .Item("amde_anil_id") = cmbAni.Valor
         .Item("_anillo") = cmbAni.SelectedItem.Text
         .Item("amde_cant") = txtAniCant.Valor
      End With

      If hdnAniId.Text = "" Then
         mdsDatos.Tables(mstrSoliDeta).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosRaza(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow

      If cmbRaza.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la raza.")
      End If

      If (mdsDatos.Tables(mstrSoliRaza).Select("crrz_raza_id=" & cmbRaza.Valor)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("La raza indicada ya se encuentra asociada al criadero.")
      End If

      If hdnRazaId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrSoliRaza).NewRow
         ldrDatos.Item("crrz_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrSoliRaza), "crrz_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrSoliRaza).Select("crrz_id=" & hdnRazaId.Text)(0)
      End If

      With ldrDatos
         .Item("crrz_raza_id") = cmbRaza.Valor
         .Item("crrz_crdr_id") = usrCria.Valor
         .Item("_raza_desc") = cmbRaza.SelectedItem.Text
         .Item("crrz_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
      End With

      If hdnRazaId.Text = "" Then
         mdsDatos.Tables(mstrSoliRaza).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mLimpiarAnillo()
      hdnAniId.Text = ""
      txtAniCant.Text = ""
      cmbAni.Limpiar()
      mSetearEditor(mstrSoliDeta, True)
   End Sub

   Private Sub mLimpiarRaza()
      hdnRazaId.Text = ""
      cmbRaza.Limpiar()
      mSetearEditor(mstrSoliRaza, True)
   End Sub

#End Region

   Private Sub lnkRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRaza.Click
      mShowTabs(3)
   End Sub

   Private Sub lnkAni_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAni.Click
      mShowTabs(2)
   End Sub

   Private Sub btnLimpAni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAni.Click
      mLimpiarAnillo()
   End Sub

   Private Sub btnBajaAni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAni.Click
      Try
         With mdsDatos.Tables(mstrSoliDeta).Select("amde_id=" & hdnAniId.Text)(0)
            .Item("amde_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdAni.DataSource = mdsDatos.Tables(mstrSoliDeta)
         grdAni.DataBind()
         mLimpiarAnillo()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaRaza.Click
      Try
         With mdsDatos.Tables(mstrSoliRaza).Select("crrz_id=" & hdnRazaId.Text)(0)
            .Item("crrz_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdRaza.DataSource = mdsDatos.Tables(mstrSoliRaza)
         grdRaza.DataBind()
         mLimpiarRaza()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaAni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAni.Click
      mActualizarAnillo(True)
   End Sub

   Private Sub btnAltaRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaRaza.Click
      mActualizarRaza(True)
   End Sub

   Private Sub btnModiAni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAni.Click
      mActualizarAnillo(False)
   End Sub

   Private Sub btnModiRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiRaza.Click
      mActualizarRaza(False)
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

   Protected Overrides Sub Finalize()
      MyBase.Finalize()
   End Sub

   Private Sub btnImpr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImpr.Click
      mImprimir()
   End Sub

   Private Function mValidarRaza(ByVal pstrAni As String, ByVal pstrCria As String) As Boolean

      Try

         Dim lstrRazas As String = ""
         For Each odrRaza As DataRow In mdsDatos.Tables(mstrSoliRaza).Select()
            If lstrRazas <> "" Then
                lstrRazas = lstrRazas & ","
            End If
            lstrRazas = lstrRazas & odrRaza.Item("crrz_raza_id")
         Next

         Dim lstrCmd As String = "exec rg_anillos_movim_razas_verificar"
         lstrCmd = lstrCmd + " @anrz_anil_id=" + pstrAni
         lstrCmd = lstrCmd + ",@crrz_raza_id=" + pstrCria
         lstrCmd = lstrCmd + ",@razas=" + clsSQLServer.gFormatArg(lstrRazas, SqlDbType.VarChar)

         Dim myConnection As New SqlConnection(Session("sConn").ToString())
         Dim cmdExec = New SqlCommand
         myConnection.Open()
         cmdExec.Connection = myConnection
         cmdExec.CommandType = CommandType.Text
         cmdExec.CommandText = lstrCmd
         Dim dr As SqlDataReader = cmdExec.ExecuteReader()
         While (dr.Read())
            Return (dr.GetValue(0).ToString().Trim())
         End While
         dr.Close()
         myConnection.Close()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
         Return (False)
      End Try

   End Function

End Class
End Namespace
