Imports Common.Constantes


Namespace SRA


Partial Class ControlAnalisis_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtNomb As NixorControls.TextBoxTab
    Protected WithEvents lblDoc As System.Web.UI.WebControls.Label
    Protected WithEvents txtDoc As NixorControls.TextBoxTab

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()

    End Sub

#End Region

    Protected WithEvents lblTitu As System.Web.UI.WebControls.Label

#Region "Definici�n de Variables"
    Public mstrCmd As String
    Public mstrTabla As String
    Public mstrTitulo As String
    Public mstrFiltros As String
    Private mstrConn As String
    Private mbooEsConsul As Boolean

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            mstrTitulo = "An�llisis por especie por Cliente"
            hdnIdCliente.Text = Request.QueryString("filtros")

            If Not Page.IsPostBack Then
                mConsultar(hdnIdCliente.Text)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConsulta.EditItemIndex = -1
            If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
                grdConsulta.CurrentPageIndex = 0
            Else
                grdConsulta.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar(hdnIdCliente.Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mConsultar(ByVal IdCliente As Int32)
        Try
            ' crea Objeto para obtener el dato de facturacion
            Dim _LaboratorioBusiness As New Business.Laboratorio.LaboratorioBusiness
            ' ejecuto el metodo que recupera los datos a mostar en la grilla y en el encabezado
            Dim ds As DataSet = _LaboratorioBusiness.GetAllAnalisisAcumPorActividadCliente(Common.Actividades.Laboratorio _
                                                                                          , IdCliente _
                                                                                          , System.DateTime.Now _
                                                                                         )
            ' de la consulta se retornan dos tablas en el datast, la 0 estan los datos de la 
            ' cabecera y en la 1 el regusltado de la grilla
            ' Dario 2013-12-10 cambio por pedido de lab
            'Me.lblLeyendaFecha.Text = "An�lisis efectuados entre la fecha " + (ds.Tables(0).Rows(0)("fechaDesde").ToString) _
            '                        + " hasta la fecha " + (ds.Tables(0).Rows(0)("fechaHasta").ToString)
            Me.lblLeyendaFecha.Text = "An�lisis efectuados entre la fecha " + Convert.ToDateTime(ds.Tables(0).Rows(0)("fechaDesde")).ToString("dd/MM/yyyy") _
                                    + " hasta la fecha " + System.DateTime.Now.ToString("dd/MM/yyyy HH:mm")
            Me.lblNomb.Text = ds.Tables(0).Rows(0)("clieDenominacion").ToString
            ' cargo el resultado de la consulta en la grilla 
            grdConsulta.DataSource = ds.Tables(1)
            grdConsulta.DataBind()
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        Response.Write("<Script>window.close();</script>")
    End Sub

End Class

End Namespace

