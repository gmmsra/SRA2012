<%--<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrproductonew.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>--%>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InspeccionFenotipica" CodeFile="InspeccionFenotipica.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROHNew" Src="controles/usrProductoNew.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Inspecciones Fenotípicas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function mSetearProducto()
		{
		 if (document.all('hdnCriaId').value!='')
		   {
		     document.all('usrProducto:txtCriaId').value = document.all('hdnCriaId').value;
		   }		
	   } 
		
		function usrCriadorId_onchange()
		{   
			document.all('hdnCriaId').value = document.all('usrCriador:txtId').value; 
			mSetearProducto();
		}
		function usrCriadorFilId_onchange()
		{
			document.all('hdnCriaFilId').value = document.all('usrCriadorFil:txtId').value; 
			mSetearProductoFil();
		}
		
		//function mSetearProductoFil()
		//{
		// if (document.all('hdnCriaFilId').value!='')
		 //  {
		  //   document.all('usrProductoFil:txtCriaId').value = document.all('hdnCriaFilId').value;
		 //  }		
	   //}
	   
	   
	   function usrProducto_cmbProdRaza_onchange(pRaza)
		{
		    if(document.all(pRaza).value!="")
		    {
				document.all("usrCriador:cmbRazaCria").value = document.all(pRaza).value;
				document.all("usrCriador:cmbRazaCria").onchange();
				document.all("usrCriador:cmbRazaCria").disabled = true;
				document.all("txtusrCriador:cmbRazaCria").disabled = true;
			}
			else
			{
				document.all("usrCriador:cmbRazaCriador").value = ""
				document.all("usrCriador:cmbRazaCriador").onchange();
				document.all("usrCriador:cmbRazaCriador").disabled = false;
				document.all("txtusrCriador:cmbRazaCriador").disabled = false;
			}
		}
		
			function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeDesdeFil')!=null)
				{
					if (strRet!='')
					{
	 					//document.all('lblNumeFil').innerHTML = strRet+':';
	 					document.all('lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					//document.all('lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
		}
		function mSetearRazas()
		{
				
			// Manejo del combo Registro Tipo
			if (document.all("cmbRazaFil").value != "")
				{
					if (!isPostBack())
					{
						LoadComboXML("rg_registros_tipos_productos_cargar", document.all("cmbRazaFil").value, "cmbRegiTipoFil", "N");
					}

					if (document.all("cmbRazaFil").value != document.all("hdnRazaAnteriorId").value)
					{
						document.all("hdnRazaAnteriorId").value = document.all("cmbRazaFil").value
						LoadComboXML("rg_registros_tipos_productos_cargar", document.all("cmbRazaFil").value, "cmbRegiTipoFil", "N");
					}
					if (document.all("cmbRazaFil").value != "")
						document.all("cmbRegiTipoFil").disabled = false;
					else
						document.all("cmbRegiTipoFil").disabled = true;
				}
		
		}
		function mSetearHastaCondicional()
		{
			if (document.all('txtCondicionalDesdeFil')!=null)
				document.all('txtCondicionalHastaFil').value = document.all('txtCondicionalDesdeFil').value;
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Inspecciones Fenotípicas</asp:label></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
										Width="100%" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" align="right">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																	CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="10"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 180px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblProductoFil" runat="server" cssclass="titulo">Producto:</asp:label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<anthem:panel id="PnlusrProductoFil" runat="server"><UC1:PROHNew id="usrProductoFil" runat="server" IncluirDeshabilitados="True" MuestraProdNomb="false"
																					MuestraTipoRegistroVariedad="true" MuestraAsocExtr="false" MuestraNroAsocExtr="false" MuestraBotonAgregaImportado="False"
																					EsPropietario="True" Ancho="800" MuestraDesc="false" FilTipo="T" AutoPostBack="False" Saltos="1,2"
																					Tabla="productos" AceptaNull="false"></UC1:PROHNew></anthem:panel></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 22px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblCondicionalDesdeFil" runat="server" cssclass="titulo">Condicional Desde:&nbsp;</asp:label></TD>
																		<TD style="HEIGHT: 22px" background="imagenes/formfdofields.jpg">
																			<CC1:numberbox id="txtCondicionalDesdeFil" onkeypress="return SoloNumeros(event)" runat="server"
																				cssclass="cuadrotexto" Width="100px" Obligatorio="false" onchange="mSetearHastaCondicional();"></CC1:numberbox>
																			<asp:label id="lblCondicionalHastaFil" runat="server" cssclass="titulo">Condicional Hasta:&nbsp;</asp:label>
																			<CC1:numberbox id="txtCondicionalHastaFil" onkeypress="return SoloNumeros(event)" runat="server"
																				cssclass="cuadrotexto" Width="100px" Obligatorio="false"></CC1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" width="68px"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Resultado:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<asp:DropDownList id="cmbResultadoFill" Runat="server"></asp:DropDownList></TD>
																	<TR>
																		<TD height="2" background="imagenes/formdivfin.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="16" width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2" align="center"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="prin_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="prin_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="criadorNume" HeaderText="Criador"></asp:BoundColumn>
											<asp:BoundColumn DataField="_prdt_nomb" HeaderText="Producto"></asp:BoundColumn>
											<asp:BoundColumn DataField="prdt_rp" HeaderText="RP"></asp:BoundColumn>
											<asp:BoundColumn DataField="SraNume" HeaderText="Nro.Incrip"></asp:BoundColumn>
											<asp:BoundColumn DataField="_prin_resu" HeaderText="Resultado">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_MotiRechazo" HeaderText="Moti.Rech."></asp:BoundColumn>
											<asp:BoundColumn DataField="esta_desc" HeaderText="Estado">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="1">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" border="0" cellPadding="0" align="left">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ToolTip="Agregar una Nueva Relación"
													ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD width="100" align="center"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
													ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
											Visible="False" width="100%" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TBODY>
														<TR>
															<TD>
																<P></P>
															</TD>
															<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:label></TD>
															<TD vAlign="top" align="right">&nbsp;
																<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="3" align="center"><asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																	<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 180px" background="imagenes/formfdofields.jpg" align="right">
																			<TD background="imagenes/formfdofields.jpg">
																				<UC1:PROHNew id="usrProducto" runat="server" IncluirDeshabilitados="True" MuestraProdNomb="true"
																					MuestraTipoRegistroVariedad="true" MuestraAsocExtr="false" MuestraNroAsocExtr="false" MuestraBotonAgregaImportado="False"
																					EsPropietario="True" Ancho="800" MuestraDesc="True" FilTipo="S" AutoPostBack="False" Saltos="1,2"
																					Tabla="productos" AceptaNull="false" SetInspecciones="true" MuestraFechaNaci="true"></UC1:PROHNew></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 24px"></TD>
																			<TD>
																				<asp:Label id="lblBajaOrig" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>&nbsp;
																			</TD>
																		</TR>
																		<TR id="trResultado" runat="server">
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblResultado" runat="server" cssclass="titulo">Resultado:</asp:Label>&nbsp;</TD>
																			<TD>
																				<TABLE>
																					<TR>
																						<TD style="HEIGHT: 24px">
																							<anthem:panel id="PnlResultado" runat="server">
<asp:DropDownList id="cmbResultado" runat="server" cssclass="combo" Width="152px" AutoPostBack="True"></asp:DropDownList>&nbsp; 
<asp:Label id="lblInspector" runat="server" cssclass="titulo">Inspector:</asp:Label>&nbsp; 
<cc1:textboxtab id="txtInspector" runat="server" cssclass="cuadrotexto" Width="100px" MaxLength="50"></cc1:textboxtab>&nbsp; 
<asp:Label id="lblMotivoBaj" runat="server" cssclass="titulo">Motivo Baja:</asp:Label>&nbsp; 
<asp:DropDownList id="cmbMotivoBaj" runat="server" cssclass="combo" Width="120px" AutoPostBack="True"></asp:DropDownList></anthem:panel></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 10px" vAlign="top" background="imagenes/formfdofields.jpg"
																				align="right">
																				<asp:Label id="lblObservaciones" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 16px" height="16">
																				<cc1:textboxtab id="txtObservaciones" runat="server" cssclass="textolibre" Width="90%" TextMode="MultiLine"
																					Rows="10" height="75px"></cc1:textboxtab></TD>
																		</TR>
																	</TABLE>
																</asp:panel><anthem:panel id="PnlLeyendaBaja" runat="server">
																	<asp:label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:label>
																</anthem:panel></TD>
														</TR>
														<TR>
															<TD height="1" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR height="30" align="center">
															<td colspan="4">
																<DIV style="DISPLAY: inline" id="divgraba" runat="server"><ASP:PANEL id="panBotones" Runat="server">
																		<TABLE width="100%">
																			<TR>
																				<TD colSpan="3" align="center">
																					<anthem:panel id="PnlBotonesABM" runat="server"></anthem:panel><A id="editar" name="editar"></A>
																					<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																					<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																						Text="Baja"></asp:Button>&nbsp;&nbsp;
																					<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																						Text="Modificar"></asp:Button>&nbsp;&nbsp;
																					<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																						Text="Limpiar"></asp:Button></TD>
																			</TR>
																		</TABLE>
																	</ASP:PANEL></DIV>
																	<DIV style="DISPLAY: none" id="divproce" runat="server">
																	<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
																		<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																	</asp:panel></DIV>
															</td>
														</TR>
														
								</TD>
							</TR>
						</TABLE>
						</P></asp:panel></DIV></td>
				</tr>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnPonderFill" runat="server" text="30"></asp:textbox><asp:textbox id="hdnCriaId" runat="server"></asp:textbox><asp:textbox id="hdnCriaFilId" runat="server"></asp:textbox><asp:textbox id="hdnRazaAnteriorId" runat="server">-1</asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		function mPorcPeti()
		{
			document.all("divgraba").style.display ="none";
			document.all("divproce").style.display ="inline";
		}
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		<DIV></DIV>
		</FORM>
	</BODY>
</HTML>
