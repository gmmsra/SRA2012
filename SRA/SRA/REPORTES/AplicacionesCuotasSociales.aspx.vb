Namespace SRA

Partial Class AplicacionesCuotasSociales
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            txtFechaDesde.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
            txtFechaHasta.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
            mCargarCombos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEdesde, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEhasta, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "T")
   End Sub

   Private Sub mLimpiarFiltros()
        txtFechaDesde.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
        txtFechaHasta.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
        cmbCEdesde.Limpiar()
        cmbCEhasta.Limpiar()
        cmbCate.Limpiar()
        txtAnioDesde.Text = ""
        txtAnioHasta.Text = ""
        txtBimDesde.Text = ""
        txtBimHasta.Text = ""
        txtSociDesde.Text = ""
        txtSociHasta.Text = ""
        cmbCanc.Limpiar()
        cmbRepo.Limpiar()
        cmbBimTipo.SelectedIndex = 0
        lisAjus.Limpiar()
        lisIngr.Limpiar()
        lisProm.Limpiar()
   End Sub

#End Region

#Region "Listar"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      mListar()
   End Sub

   Private Sub mListar()
      Try
         Dim lstrRptName As String = ""
         Dim lstrRpt As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer
         Dim lstrAjus As String = lisAjus.ObtenerIDs()
         Dim lstrIngr As String = lisIngr.ObtenerIDs()
         Dim lstrProm As String = lisProm.ObtenerIDs()

         If cmbRepo.Valor = 4 Or cmbRepo.Valor = 5 Then
            'gestion
            lstrRptName = "AplicacionesCuotasSociales"
         Else
            'composicion
            lstrRptName = "AplicacionesCuotasSocialesCompo"
         End If

         If txtFechaDesde.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha desde.")
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
         End If
         If txtFechaHasta.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha hasta.")
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
         End If

         If (txtBimDesde.Text = "" And txtAnioDesde.Text <> "") Or (txtBimDesde.Text <> "" And txtAnioDesde.Text = "") Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el per�odo desde completo.")
         End If
         If (txtBimHasta.Text = "" And txtAnioHasta.Text <> "") Or (txtBimHasta.Text <> "" And txtAnioHasta.Text = "") Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el per�odo hasta completo.")
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&fecha_desde=" + lintFechaDesde.ToString
         lstrRpt += "&fecha_hasta=" + lintFechaHasta.ToString
         lstrRpt += "&soci_desde=" + IIf(txtSociDesde.Text = "", "0", txtSociDesde.Text)
         lstrRpt += "&soci_hasta=" + IIf(txtSociHasta.Text = "", "0", txtSociHasta.Text)
         lstrRpt += "&emct_desde=" + IIf(cmbCEdesde.Valor.ToString = "", "0", cmbCEdesde.Valor.ToString)
         lstrRpt += "&emct_hasta=" + IIf(cmbCEhasta.Valor.ToString = "", "0", cmbCEhasta.Valor.ToString)
         lstrRpt += "&cate_id=" + IIf(cmbCate.Valor.ToString = "", "0", cmbCate.Valor.ToString)
         lstrRpt += "&cance=" + cmbCanc.Valor
         lstrRpt += "&repo=" + cmbRepo.Valor
         lstrRpt += "&bim_tipo=" + cmbBimTipo.Valor
         lstrRpt += "&bim_desde=" + IIf(txtBimDesde.Text = "", "0", txtBimDesde.Text)
         lstrRpt += "&anio_desde=" + IIf(txtAnioDesde.Text = "", "0", txtAnioDesde.Text)
         lstrRpt += "&bim_hasta=" + IIf(txtBimHasta.Text = "", "0", txtBimHasta.Text)
         lstrRpt += "&anio_hasta=" + IIf(txtAnioHasta.Text = "", "0", txtAnioHasta.Text)
         lstrRpt += "&ajustes=" + lstrAjus
         lstrRpt += "&ingresos=" + lstrIngr
         lstrRpt += "&promo=" + lstrProm

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

End Class

End Namespace
