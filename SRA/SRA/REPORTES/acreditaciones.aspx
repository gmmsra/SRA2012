<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Acreditaciones" enableViewState="True" CodeFile="Acreditaciones.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cr�ditos pendientes de aplicaci�n</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 8px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Cr�ditos pendientes de aplicaci�n</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 8px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="97%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20%; HEIGHT: 15px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																				<TD style="HEIGHT: 15px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 20%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																				<TD background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrClieFil" runat="server" width="100%" Tabla="Clientes" Saltos="1,1,1" FilDocuNume="True"
																						FilCUIT="True" FilSociNume="True" PermiModi="True" Obligatorio="True" CampoVal="Cliente"
																						SoloBusq="true" AceptaNull="False"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 21px" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20%; HEIGHT: 21px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21">
																					<asp:Label id="lblFechaD" runat="server" cssclass="titulo">Fecha desde:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 21px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21">
																					<cc1:DateBox id="txtFechaD" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																					<asp:Label id="lblFechaH" runat="server" cssclass="titulo">Fecha desde:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaH" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 21px" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20%; HEIGHT: 21px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21">
																					<asp:Label id="lblNroComp" runat="server" cssclass="titulo">Comprobante:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 21px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21">
																					<cc1:combobox class="combo" id="cmbComp" runat="server" Width="250px" AceptaNull="False" NomOper="comprob_tipos_cargar"></cc1:combobox></TD>
																			<TR>
																				<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20%" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCompCemiNum" runat="server" cssclass="titulo">Centro Emisor:</asp:Label></TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtCompCemiNum" runat="server" cssclass="cuadrotexto" Width="48px"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20%" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaHisto" runat="server" cssclass="titulo">Hist�rico:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaHisto" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20%" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<asp:CheckBox id="chkRes" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small" Text="Resumido"></asp:CheckBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20%" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 20%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																				<TD width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																			CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"
																			ForeColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
