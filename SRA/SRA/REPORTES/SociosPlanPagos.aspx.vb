Namespace SRA

Partial Class SociosPlanPagos
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
         grdCons.PageSize = Convert.ToInt32(mstrParaPageSize)

         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mCargarCombos()
				'txtFechaAl.Fecha = Now
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnEnvi.Attributes.Add("onclick", "return(mConfirmar());")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCategoria, "T")
		'clsWeb.gCargarCombo(mstrConn, "estados_cargar @esti_id=3", cmbEsta, "id", "descrip", "T") Pantanettig - 04/10/2007
      clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar @acti_id=2", cmbMail, "id", "descrip", "S")
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        If cmbRepo.Valor.ToString = "M" Then
            mConsultarMails(grdCons)
        Else
            mListar(cmbRepo.Valor.ToString)
        End If
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarMails(grdCons)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarMails(ByVal pobjGrid As System.Web.UI.WebControls.DataGrid)
      Try

         Dim lstrCmd As String = "exec plan_pagos_mails_consul "
         lstrCmd += " @fechadesde=" & clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime)
         lstrCmd += ",@fechahasta=" & clsSQLServer.gFormatArg(txtFechaHasta.Text, SqlDbType.SmallDateTime)
         lstrCmd += ",@soci_cate_id=" & clsSQLServer.gFormatArg(cmbCategoria.Valor.ToString, SqlDbType.Int)
         lstrCmd += ",@soci_esta_id=" & clsSQLServer.gFormatArg(cmbEsta.Valor.ToString, SqlDbType.VarChar)

         Dim ds As New DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each dc As DataColumn In ds.Tables(0).Columns
             Dim dgCol As New Web.UI.WebControls.BoundColumn
             dgCol.DataField = dc.ColumnName
             dgCol.HeaderText = dc.ColumnName
             If dc.Ordinal = 0 Or dc.ColumnName.IndexOf("_id") <> -1 Or dc.ColumnName = "mails" Then
                dgCol.Visible = False
             End If
             pobjGrid.Columns.Add(dgCol)
         Next
         pobjGrid.DataSource = ds
         pobjGrid.DataBind()
         Session("ds") = DirectCast(pobjGrid.DataSource, DataSet)
         pobjGrid.Visible = True
         btnEnvi.Enabled = (pobjGrid.Items.Count > 0)
         btnEnvi.Visible = True
         btnList.Visible = False
         cmbRepo.Enabled = False

         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mObtenerMails(ByVal ds As DataSet, ByVal pstrCate As String, ByVal pstrInst As String) As String
      Dim lstrMail As String
      Try
         For Each odrClag As DataRow In ds.Tables(0).Select("cate_id=" & pstrCate)
            If lstrMail <> "" Then
               lstrMail = lstrMail & ";"
            End If
            lstrMail = lstrMail & odrClag.Item("mails")
         Next
         ds.Dispose()
         Return (lstrMail)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Function

   Private Sub mEnviarMails()
      Try
         Dim ds As New DataSet
         Dim lstrMail As String
         Dim lstrInst As String
         Dim lstrCate As String
         Dim lstrAsun As String
         Dim lstrMens As String
         Dim lstrCmd As String
         Dim lstrMailMode As String = System.Configuration.ConfigurationSettings.AppSettings("conMailMode").ToString()

         lstrMailMode = "..\" & lstrMailMode

         ''lstrCmd = "exec socios_general_mails_consul @rptf_id=" & hdnRptId.Text
         ''ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each odrDeta As DataRow In Session("ds").Tables(1).Select
            lstrCate = odrDeta.Item("cate_id")
            If odrDeta.IsNull("inst_id") Then
                lstrInst = ""
            Else
                lstrInst = odrDeta.Item("inst_id")
            End If
            lstrAsun = clsSQLServer.gCampoValorConsul(mstrConn, "mails_modelos_consul @mamo_id=" & cmbMail.Valor.ToString, "mamo_asun")
            lstrCmd = "mails_mode_catego_consul @mmca_acti_id=2,@mmca_cate_id=" & lstrCate & ",@mmca_mamo_id=" & cmbMail.Valor.ToString
            If lstrInst <> "" Then
               lstrCmd = lstrCmd & ",@mmca_inst_id=" & lstrInst
            End If
            lstrMens = clsSQLServer.gCampoValorConsul(mstrConn, lstrCmd, "mmca_mens")
            lstrMail = mObtenerMails(Session("ds"), lstrCate, lstrInst)
            If lstrMail <> "" And lstrMens <> "" Then
                clsMail.gEnviarMail(lstrMail, lstrAsun, lstrMens, lstrMailMode)
            End If
         Next
         ds.Dispose()

         btnEnvi.Visible = False
         btnList.Visible = True
         cmbRepo.Enabled = True
         grdCons.Visible = False

         Throw New AccesoBD.clsErrNeg("Se han enviado los mails a los socios seleccionados.")

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mListar(ByVal pstrTipo As String)
      Try
         Dim lstrRptName As String
			Dim lintFechaDesde As Integer
			Dim lintFechaHasta As Integer


         If pstrTipo = "E" Then
            lstrRptName = "Etiquetas"
         Else
            If cmbTipoListado.Valor.ToString = "R" Then
					lstrRptName = "SociosPlanPagosVigenciaResumido"
            Else
					lstrRptName = "SociosPlanPagosVigenciaDetallado"
            End If
         End If

			If txtFechaDesde.Fecha.ToString = "" Then
				lintFechaDesde = 0
			Else
				lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
			End If

                If txtFechaHasta.Fecha.ToString = "" Then
                    lintFechaHasta = CType(clsFormatear.gFormatFechaString(DateTime.Now.ToString("dd/MM/yyyy"), "Int32"), Integer)
                Else
                    lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
                End If

                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			If cmbRepo.Valor.ToString = "E" Then
                lstrRpt += SRA_Neg.Utiles.ParametrosEtiquetas("FP", , IIf(chkDire.Checked, "True", "False"), , , , , , , IIf(cmbRepoMail.Valor.ToString <> "T", cmbRepoMail.Valor.ToString, ""), IIf(cmbRepoCorreo.Valor.ToString <> "T", cmbRepoCorreo.Valor.ToString, ""), lintFechaDesde.ToString, lintFechaHasta.ToString, cmbCategoria.Valor.ToString, , , , , , , , , , cmbEsta.Valor.ToString)
            Else
                lstrRpt += "&fechadesde=" + lintFechaDesde.ToString
                lstrRpt += "&fechahasta=" + lintFechaHasta.ToString
                lstrRpt += "&soci_cate_id=" + cmbCategoria.Valor.ToString
                lstrRpt += "&soci_esta_id=" + cmbEsta.Valor.ToString
                lstrRpt += "&Mail=" & cmbRepoMail.Valor.ToString
                lstrRpt += "&Correo=" & cmbRepoCorreo.Valor.ToString
                lstrRpt += "&orden=" & cmbOrden.Valor.ToString
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

  Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFiltros()
      grdCons.CurrentPageIndex = 0
		cmbEsta.Limpiar()
		txtFechaDesde.Text = ""
		txtFechaHasta.Text = ""
      cmbCategoria.Limpiar()
      cmbRepo.Enabled = True
      grdCons.Visible = False
      btnList.Visible = True
		btnEnvi.Visible = False
		cmbOrden.Limpiar()
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvi.Click
      mEnviarMails()
   End Sub

End Class

End Namespace
