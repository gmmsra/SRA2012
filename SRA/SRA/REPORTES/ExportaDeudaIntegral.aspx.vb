Imports Business.Facturacion.Reportes.ReportesBusiness
Imports System.Text
Imports System.IO


Namespace SRA


Partial Class ExportaDeudaIntegral
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTipo As System.Web.UI.WebControls.Label

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrConn As String
    Private mstrTabla As String = "rpt_exporta_deuda_integral"
    Private mdsDatos As DataSet


#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                mEstablecerPerfil()
                mSetearEventos()
                clsWeb.gInicializarControles(sender, mstrConn)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEstablecerPerfil()
    End Sub
#End Region

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        Try
            mLimpiarFiltros()
        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Private Sub mExportar()
        Try
            mValidarDatos()

            Me.ConsultarDatos()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try

    End Sub
    ' metodo que ejecuta la consulta de datos de deduda integral segun los parametos 
    ' informados
    Public Sub ConsultarDatos()
        ' creo el objeto business que contiene el metodo de consulta
        Dim _ReportesBusiness As New Business.Facturacion.Reportes.ReportesBusiness
        ' ejecuto la consulta y cargo el dataset con la respuesta
        mdsDatos = _ReportesBusiness.GetExportaDeudaIntegral(Me.usrClieFil.Valor.ToString _
                                                           , IIf(Me.txtFechaHastaFil.Fecha.ToString() = "", System.DateTime.MinValue, Me.txtFechaHastaFil.Fecha) _
                                                             )
        ' ejecuto el metodo que hace la clasificacion del cliente y completo la columna 
        ' clasificacion con el resultado para cada linea
        Me.CalcularClasificacionCliente()
        ' exporto el resultado del dataset modificado
        Me.ExportarResultadoXLS()
        '
    End Sub
    ' metodo que recorre el dataset de resultado de la consulta y clasifica los clienttes 
    ' por la logina de negocio
    Private Sub CalcularClasificacionCliente()
        ' declaro la variable de fecha de corte con fecha recuperada mas un dia y menos dos a�os
        Dim feccorte As DateTime = Convert.ToDateTime(Me.txtFechaHastaFil.Fecha).AddDays(1).AddYears(-2)
        Dim idcliente As Int32 = 0
        Dim respuesta As String = String.Empty
        ' recorro los datos para hacer la clasificacion del cliente
        For Each ldrDatos As DataRow In mdsDatos.Tables(0).Rows
            With ldrDatos
                ' controlo cambio de id de cliente recuperado
                If idcliente <> .Item("cliente") Then
                    ' si es distinto seteo el nuevo cliente en la variable y pongo respuesta en null
                    idcliente = .Item("cliente")
                    respuesta = String.Empty
                End If
                ' si respuesta es null para un cliente llamo funcion de calculo de clasificacion del cliente
                ' esto para no ir a calcular en cada linea sino solo en el cambio de cliente
                ' el calculo es por cliente con todas sus novedades
                If respuesta = "" Then
                    respuesta = Me.ClasificaCliente(idcliente, feccorte)
                End If
                ' seteo en la respuesta de la funcion en la columna clasificacion del dataset
                .Item("Clasificacion") = respuesta
            End With
        Next
        '
    End Sub
    ' metodo que clasifica el cliente
    Private Function ClasificaCliente(ByVal idcliente As Int32, ByVal feccorte As DateTime) As String
        ' declaro y seteo variables para la funcion
        Dim sumtotalcliente As Int32 = 0
        Dim fechasmenores As Int32 = 0
        Dim fechasmayores As Int32 = 0
        ' bucle calculo de cliente
        For Each ldrDatos As DataRow In mdsDatos.Tables(0).Select("cliente=" & idcliente)
            With ldrDatos
                ' controlo cambio de id de cliente recuperado para hacer el calculo de la clasificacion del cliente
                If idcliente <> .Item("cliente") Then GoTo l_calculaRespuesta
                ' condiciones de evaluacion
                If .Item("feccomp") < feccorte _
                   And (.Item("tipocomp") = 29 Or .Item("tipocomp") = 32) _
                   And .Item("interes") = 0 _
                   And (.Item("servicios") <> 0 Or .Item("rechazos") <> 0 Or .Item("planfac") <> 0) Then
                    sumtotalcliente = sumtotalcliente + .Item("servicios") + .Item("interes") + .Item("rechazos") + .Item("planfac")
                    fechasmenores = fechasmenores + 1
                Else
                    If .Item("feccomp") >= feccorte Then
                        sumtotalcliente = sumtotalcliente + .Item("servicios") + .Item("interes") + .Item("rechazos") + .Item("planfac")
                        fechasmayores = fechasmayores + 1
                    End If
                End If
            End With
        Next

        ' segmento que determina la respuesta final de la funcion
l_calculaRespuesta:
        ' si no tiene nodevades mayores y tiene novedades menores y el suma es mayor a 0  es "Filtro"
        If fechasmayores = 0 And fechasmenores > 0 And sumtotalcliente > 0 Then
            Return "Filtro"
        End If

        ' si tiene nodevades mayores y menores es "Otro"
        If fechasmayores > 0 And fechasmenores > 0 And sumtotalcliente > 0 Then
            Return "Otro"
        End If

        ' el por las dudas es siempre "Resto"
        Return "Resto"
    End Function

    ' metodo que genera la exportacion del ds de resultado como xls
    Private Sub ExportarResultadoXLS()
        If (mdsDatos.Tables(0).Rows.Count > 0) Then
            Dim sb As New StringBuilder
            Dim sw As New StringWriter(sb)
            Dim htw As New HtmlTextWriter(sw)
            Dim pagina As New Page
            Dim form As New HtmlForm
            Dim dg As New DataGrid
            dg.EnableViewState = False
            dg.DataSource = mdsDatos.Tables(0)
            dg.DataBind()
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(dg)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            ' armo el nombre del archivo
            Dim nombrearchivo As String = ".xls"
            If Not IsNothing(Me.txtFechaHastaFil.Fecha) Then
                nombrearchivo = "_" & Convert.ToDateTime(Me.txtFechaHastaFil.Fecha).ToString("yyyy/MM/dd") & ".xls"
            End If
            Response.AddHeader("Content-Disposition", "attachment;filename=DeudaIntegralSRA" & nombrearchivo)
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        End If
    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Private Sub mLimpiarFiltros()
        usrClieFil.Limpiar()
        txtFechaHastaFil.Text = ""
    End Sub

    Private Sub mSetearEventos()
        btnImpo.Attributes.Add("onclick", "return(mExportar());")
    End Sub

    Private Overloads Sub btnImpo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImpo.Click
        ' controlo que la fecha tenga alguna cosa sino mensaje de error
        If Me.txtFechaHastaFil.Text.Length = 10 Then
            mExportar()
        Else
            Response.Write("<script language='javascript'>window.alert('Debe de informar la Fecha');</script>")
        End If
    End Sub
End Class
End Namespace
