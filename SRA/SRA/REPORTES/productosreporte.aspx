<%@ Reference Control="~/controles/usrprodfiltro.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PRODFIL" Src="../controles/usrProdFiltro.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ProductosReporte" CodeFile="ProductosReporte.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="../controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reporte de Productos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
	
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion">Reporte de Productos</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD width="100%"><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 90%; HEIGHT: 20px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																			colSpan="2">
																			<UC1:PRODFIL id="usrProdFil" runat="server" AutoPostBack="False" FilDocuNume="True" MuestraDesc="True"
																				FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" Ancho="800"></UC1:PRODFIL></TD>
																	</TR>
																	<TR>
																		<TD background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblOrdenarPor" runat="server" cssclass="titulo" Width="90px" background="../imagenes/formfdofields.jpg"
																				DESIGNTIMEDRAGDROP="2074">Ordenar por:&nbsp;</asp:Label></TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbOrdenarPor" class="combo" runat="server" Width="176px" DESIGNTIMEDRAGDROP="2076"
																				AceptaNull="False">
																				<ASP:LISTITEM Value="0" Selected="True">Sexo - RP Num�rico</ASP:LISTITEM>
																				<ASP:LISTITEM Value="1">Sexo - Fecha 
                              Nacimiento</ASP:LISTITEM>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 20px" vAlign="top" background="../imagenes/formfdofields.jpg"
																			align="right"></TD>
																		<TD style="WIDTH: 90%; HEIGHT: 20px" vAlign="middle" background="../imagenes/formfdofields.jpg">
																			<asp:checkbox style="Z-INDEX: 0" id="chkBaja" Checked="false" Runat="server" CssClass="titulo"
																				Text="Incluir Dados de Baja"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
														<TR>
															<TD height="10" colSpan="3"></TD>
														</TR>
														<TR>
															<TD colSpan="3" align="right">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" CausesValidation="False" ForeColor="Transparent"
																	ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																	BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg" ImagesUrl="../imagenes/" IncludesUrl="../includes/"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
																	ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																	ImageUrl="imagenes/btnImpr.jpg" ImagesUrl="../imagenes/" IncludesUrl="../includes/"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
			</DIV>
			<asp:textbox id="hdnRptId" runat="server" Visible="False"></asp:textbox></form>
	</BODY>
</HTML>
