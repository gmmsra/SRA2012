Namespace SRA

Partial Class Certificado
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


#Region "Definici�n de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mSetearEventos()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

    Sub mSetearEventos()
        txtAnioFil.Attributes.Add("onchange", "txtAnioFil_Change(this,'" & lblInstituto.Text & "');")
    End Sub

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        lblInstituto.Text = Convert.ToInt32(Request.QueryString("fkvalor"))
        lblInstituto.Text = Request.QueryString("fkvalor")
        usrAlumFil.FilInseId = lblInstituto.Text
    End Sub
#End Region

    Private Sub mImprimir()
        Try
            Dim lstrRptName As String = "Certificado"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&anio=" + Me.txtAnioFil.Valor.ToString()
            lstrRpt += "&cicl_id=" + Me.cmbCicloFil.Valor.ToString()
            lstrRpt += "&alum_id=" + Me.usrAlumFil.Valor.ToString()
            lstrRpt += "&tipo=" + Me.cmbCertifTipoFil.Valor.ToString()
            lstrRpt += "&carr_inse_id=" + lblInstituto.Text
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCiclosFil()
        clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCicloFil, "S", "@cicl_inse_id =" & lblInstituto.Text & ",@cicl_anio =" & IIf(txtAnioFil.Text = "", "0", txtAnioFil.Text))
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            mValidar()
            mImprimir()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidar()
        If (Me.cmbCertifTipoFil.Valor Is DBNull.Value) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el tipo de certificaci�n.")
        End If
        'If (Me.txtAnioFil.Valor = 0) Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar el a�o.")
        'End If
        'If (Me.cmbCicloFil.Valor = 0) Then
        '    Throw New AccesoBD.clsErrNeg("Debe seleccionar el ciclo.")
        'End If
    End Sub

End Class

End Namespace
