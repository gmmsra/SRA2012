Imports System.Data.SqlClient


Namespace SRA


Partial Class AlumnosAdhesiones
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mintInse As Integer
   'Private Enum Columnas As Integer
   '   InscId = 1
   '   FechaInsc = 2
   '   Ciclo = 3
   '   Periodo = 4
   '   Alumno = 5
   '   PlanPago = 6
   '   PlanPagoId = 7
   '   AlumnoId = 8
   '   PeriodoId = 9
   '   CicloId = 10

   'End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Request.QueryString("fkvalor") Is Nothing Then
            hdnInseId.Text = Request.QueryString("fkvalor").ToString
         End If

         If (Not Page.IsPostBack) Then
            mCargarCombos()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjetaFil, "T")
   End Sub
#End Region

   Private Sub mImprimir()
      Try
         Dim lstrRptName As String

         lstrRptName = "Alumnos_Adhesiones"

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&inse_id=" + hdnInseId.Text.ToString
         lstrRpt += "&alum_id=" + usrAlumFil.Valor.ToString
         lstrRpt += "&tarj_id=" + IIf(cmbTarjetaFil.Valor.ToString = "", "0", cmbTarjetaFil.Valor.ToString)
         lstrRpt += "&orden=" + cmbOrden.Valor.ToString
         lstrRpt += "&anio=" + cmbInsc.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         mImprimir()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

End Class
End Namespace
