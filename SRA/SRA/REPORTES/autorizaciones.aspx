<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Autorizaciones" CodeFile="Autorizaciones.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Autorizaciones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion">Autorizaciones</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD id="ColumnaPrueba" style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 21.97%; HEIGHT: 7px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 7px" background="../imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 11.72%" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo" Width="87px">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 100%; HEIGHT: 30px" background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 11.72%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 21.97%; HEIGHT: 26px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblAutoTipo" runat="server" cssclass="titulo">Tipo de Autorizaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 26px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbAutoTipo" runat="server" Width="328px" nomoper="autoriza_tipos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 11.72%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 7.9%; HEIGHT: 30px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCompTipo" runat="server" cssclass="titulo">Tipo de Comprobante:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 100%; HEIGHT: 30px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCompTipo" runat="server" Width="88px" nomoper="comprob_tipos_cargar"></cc1:combobox>&nbsp;&nbsp;
																			<asp:Label id="lblCompLetra" runat="server" cssclass="titulo">Letra:</asp:Label>
																			<cc1:combobox class="combo" id="cmbCompLetra" runat="server" Width="70px">
																				<asp:listitem value="" selected="true">(Todos)</asp:listitem>
																				<asp:listitem value="A">A</asp:listitem>
																				<asp:listitem value="B">B</asp:listitem>
																			</cc1:combobox>&nbsp;&nbsp;
																			<asp:Label id="lblCompCemiNum" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>
																			<CC1:NUMBERBOX id="txtCompCemiNum" runat="server" cssclass="cuadrotexto" Width="48px"></CC1:NUMBERBOX>&nbsp;
																			<asp:Label id="lblCompNum" runat="server" cssclass="titulo">N�:</asp:Label>
																			<CC1:NUMBERBOX id="txtCompNum" runat="server" cssclass="cuadrotexto" Width="120px"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 11.72%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 21.97%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCompUser" runat="server" cssclass="titulo">Usuario Autorizante:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCompUser" runat="server" Width="184px" nomoper="usuarios_comp_autorizacion_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 21.97%; HEIGHT: 7px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 7px" background="../imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 11.72%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD> <!-- FIN FOMULARIO -->
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD width="3" height="10"></TD>
															<TD height="10"></TD>
															<TD width="2" height="10"></TD>
														</TR>
														<TR>
															<TD width="3"></TD>
															<TD align="right">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
																	ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																	ImagesUrl="../imagenes/" IncludesUrl="../includes/" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
					</td> <!--- FIN CONTENIDO --->
					<td width="13" background="../imagenes/recde.jpg">
						<IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
					</td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
	</BODY>
</HTML>
