<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociosDeta" CodeFile="SociosDeta.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Listado de Socios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Listado de Socios</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id=Table3 cellSpacing=0 cellPadding=0 width="100%" border=0>
																<TR>
																	<TD style="HEIGHT: 8px" width=24></TD>
																	<TD style="HEIGHT: 8px" width=42></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width=6></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width=26></TD>
																	<TD style="HEIGHT: 8px" vAlign=bottom></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width=24><IMG height=25 src="../imagenes/formfle.jpg" width=24 border=0></TD>
																	<TD style="HEIGHT: 8px" width=40><IMG height=25 src="../imagenes/formtxfiltro.jpg" width=113 border=0></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width=6><IMG height=25 src="../imagenes/formcap.jpg" width=26 border=0></TD>
																	<TD style="HEIGHT: 8px" background=../imagenes/formfdocap.jpg colSpan=3><IMG height=25 src="../imagenes/formfdocap.jpg" width=7 border=0></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lboSociFil" runat="server" cssclass="titulo">Socio:</asp:Label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrSociFil" runat="server" MuestraDesc="False" PermiModi="True" FilSociNume="True"
																						FilCUIT="True" FilDocuNume="True" Saltos="1,1,1" Tabla="Socios" width="100%" AceptaNull="False"
																						SoloBusq="true"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCateFil" runat="server" cssclass="titulo" Visible="False">Categoría:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbCateFil" runat="server" Visible="False" Width="344px" AceptaNull="False"
																						IncludesUrl="../Includes/" ImagesUrl="../Images/"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblDistFil" runat="server" cssclass="titulo" Visible="False">Distrito:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbDistFil" runat="server" Visible="False" Width="344px" AceptaNull="false"
																						IncludesUrl="../Includes/" ImagesUrl="../Images/"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblEntiFil" runat="server" cssclass="titulo" Visible="False">Entidad Rural:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbEntiFil" runat="server" Visible="False" Width="344px" AceptaNull="false"
																						IncludesUrl="../Includes/" ImagesUrl="../Images/"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblInstFil" runat="server" cssclass="titulo" Visible="False">Institución:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbInstFil" runat="server" Visible="False" Width="344px" AceptaNull="false"
																						IncludesUrl="../Includes/" ImagesUrl="../Images/"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD colspan="3" height="10"></TD>
																</TR>
																<TR>
																	<TD colspan="2" align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																			ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																			ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																			CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</table>
						<!--- FIN CONTENIDO ---> 
						</TD>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
						</TR>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnTipo" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE>
		<script language="javascript">
		if (document.all("hdnTipo").value == 'C')
			gSetearTituloFrame('Socios por Categoría');
		else
			if (document.all("hdnTipo").value == 'D')
				gSetearTituloFrame('Socios por Distrito');
			else
				if (document.all("hdnTipo").value == 'E')
					gSetearTituloFrame('Socios por Entidad Rural');
				else
					gSetearTituloFrame('Socios por Institución');
		</script>
		</FORM>
	</BODY>
</HTML>
