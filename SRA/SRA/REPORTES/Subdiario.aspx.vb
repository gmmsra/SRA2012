Namespace SRA

Partial Class Subdiario
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
  
   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)


         If (Not Page.IsPostBack) Then
            txtFechaDesdeFil.Text = Now.Today.ToString("dd/MM/yyyy")
            txtFechaHastaFil.Text = Now.Today.ToString("dd/MM/yyyy")
            clsWeb.gInicializarControles(Me, mstrConn)
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "Subdiario"
         Dim lintFechaD As Integer
         Dim lintFechaH As Integer
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If txtAnioFil.Text = "" And (txtDefinitivoDesde.Text <> "" Or txtDefinitivoHasta.Text <> "") Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el a�o del ejercicio.")
         End If

         If txtFechaDesdeFil.Text = "" Or txtFechaHastaFil.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar las fechas.")
         End If

         If cmbTipoComp.Valor <> 0 Then
            If cmbTipoComp.Valor = 1 Or cmbTipoComp.Valor = 3 Or cmbTipoComp.Valor = 6 Then
                If txtComp.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el n�mero.")
                End If
            Else
                If txtCompNume.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el n�mero.")
                End If
            End If
            If cmbTipoComp.Valor <> 1 And cmbTipoComp.Valor <> 2 And cmbTipoComp.Valor <> 3 And cmbTipoComp.Valor <> 4 And cmbTipoComp.Valor <> 5 And cmbTipoComp.Valor <> 6 And cmbTipoComp.Valor <> 10 Then
                If cmbCtro.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el centro emisor.")
                End If
            End If
            If cmbTipoComp.Valor = 29 Or cmbTipoComp.Valor = 31 Or cmbTipoComp.Valor = 32 Then
                If cmbLetra.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la letra del comprobante.")
                End If
            End If
         End If

         If txtFechaDesdeFil.Text = "" Then
            lintFechaD = 0
         Else
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Text = "" Then
            lintFechaH = 0
         Else
            lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&asto_desde=" + txtAsienDesde.Valor.ToString
         lstrRpt += "&asto_hasta=" + txtAsienHasta.Valor.ToString
         lstrRpt += "&fecha_desde=" + lintFechaD.ToString
         lstrRpt += "&fecha_hasta=" + lintFechaH.ToString
         lstrRpt += "&asto_tipo=" + cmbTipoAsien.Valor.ToString
         lstrRpt += "&asto_cont_desde=" + txtDefinitivoDesde.Valor.ToString
         lstrRpt += "&asto_cont_hasta=" + txtDefinitivoHasta.Valor.ToString
         lstrRpt += "&asie_cont_anio=" + txtAnioFil.Valor.ToString
         lstrRpt += "&ver_comp=" + Math.Abs(CInt(chkComprob.Checked)).ToString
         lstrRpt += "&ver_agru=" + Math.Abs(CInt(chkAgru.Checked)).ToString
         lstrRpt += "&emct_id=" + cmbCemiFil.Valor.ToString
         lstrRpt += "&Total=" + Math.Abs(CInt(chkResu.Checked)).ToString
         lstrRpt += "&analitico=" + Math.Abs(CInt(chkAnalit.Checked)).ToString
         If cmbTipoComp.Valor <> 0 Then
            lstrRpt += "&comp_tipo=" + cmbTipoComp.Valor.ToString()
            If cmbCtro.Valor Is DBNull.Value Then
                lstrRpt += "&comp_cemi_id=0"
            Else
                lstrRpt += "&comp_cemi_id=" + cmbCtro.Valor.ToString()
            End If
            If cmbTipoComp.Valor = 1 Or cmbTipoComp.Valor = 3 Or cmbTipoComp.Valor = 6 Then
                lstrRpt += "&comp_nume=0"
                lstrRpt += "&comp=" + txtComp.Text
            Else
                lstrRpt += "&comp_nume=" + txtCompNume.Text
                lstrRpt += "&comp=0"
            End If
            If cmbLetra.Valor Is DBNull.Value Then
                lstrRpt += "&comp_letra=A"
            Else
                lstrRpt += "&comp_letra=" + cmbLetra.Valor.ToString()
            End If
         Else
            lstrRpt += "&comp_tipo=0"
            lstrRpt += "&comp_cemi_id=0"
            lstrRpt += "&comp_nume=0"
            lstrRpt += "&comp_letra=A"
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "asientos_tipo_cargar", cmbTipoAsien, "id", "wald_descrip", "T")
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCemiFil, "T", "")
      clsWeb.gCargarRefeCmb(mstrConn, "asientos_comprob_tipos", cmbTipoComp, "S", "")
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCtro, "S", "")
      If Not Session("sCentroEmisorId") Is Nothing Then
         cmbCemiFil.Valor = Session("sCentroEmisorId")
      End If
      If Session("sCentroEmisorCentral") <> "S" Then
         cmbCemiFil.Enabled = False
      End If
   End Sub
#End Region
End Class
End Namespace
