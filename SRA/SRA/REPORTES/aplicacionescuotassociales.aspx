<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AplicacionesCuotasSociales" CodeFile="AplicacionesCuotasSociales.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Aplicaci�n de Cuotas Sociales</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="javascript">
	function expandir() 
	{
		try{ parent.frames("menu").CambiarExp();}catch(e){;}
	}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion"> Aplicaci�n de Cuotas Sociales</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" noWrap align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblPerio" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Desde:</asp:Label>
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px" IncludesUrl="Includes/"
																				ImagesUrl="Images/"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px" IncludesUrl="Includes/"
																				ImagesUrl="Images/"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" noWrap align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblSoci" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblSociDesde" runat="server" cssclass="titulo">Desde:</asp:Label>
																			<CC1:numberbox id="txtSociDesde" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"></CC1:numberbox>&nbsp;&nbsp;
																			<asp:Label id="lblSociHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<CC1:numberbox id="txtSociHasta" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"></CC1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" noWrap align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEdesde" runat="server" cssclass="titulo" Width="133px">Centro Emisor Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEdesde" runat="server" Width="240px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEhasta" runat="server" cssclass="titulo">Centro Emisor Hasta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEhasta" runat="server" Width="240px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCate" runat="server" cssclass="titulo">Categoria:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCate" runat="server" Width="180px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblBimDesde" runat="server" cssclass="titulo">Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbBimTipo" runat="server" Width="104px" AceptaNull="False">
																				<asp:ListItem Value="0">Todos</asp:ListItem>
																				<asp:ListItem Value="4">Mes</asp:ListItem>
																				<asp:ListItem Value="5">Bimestres</asp:ListItem>
																				<asp:ListItem Value="8">Semetres</asp:ListItem>
																			</cc1:combobox>&nbsp;
																			<cc1:numberbox id="txtBimDesde" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
																				MaxLength="2" MaxValor="12" CantMax="2"></cc1:numberbox>
																			<asp:Label id="Label1" runat="server" cssclass="titulo">/</asp:Label>
																			<cc1:numberbox id="txtAnioDesde" runat="server" cssclass="cuadrotexto" Width="50px" AceptaNull="False"
																				MaxLength="4" MaxValor="2090" CantMax="4"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblBimHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:numberbox id="txtBimHasta" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
																				MaxLength="2" MaxValor="12" CantMax="2"></cc1:numberbox>
																			<asp:Label id="Label2" runat="server" cssclass="titulo">/</asp:Label>
																			<cc1:numberbox id="txtAnioHasta" runat="server" cssclass="cuadrotexto" Width="50px" AceptaNull="False"
																				MaxLength="4" MaxValor="2090" CantMax="4"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCanc" runat="server" cssclass="titulo">Cancela:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCanc" runat="server" Width="180px" AceptaNull="False">
																				<asp:ListItem Value="0" Selected="True">(Todos)</asp:ListItem>
																				<asp:ListItem Value="1">Cuota Social</asp:ListItem>
																				<asp:ListItem Value="10">Solicitud de Ingreso</asp:ListItem>
																				<asp:ListItem Value="11">Cambio de Categor&#237;a</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblMovi" runat="server" cssclass="titulo">Movimientos:</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblAjus" runat="server" cssclass="titulo">Ajustes con:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:checklistbox id="lisAjus" runat="server" CssClass="chklst" CellPadding="0" CellSpacing="0" width="100%"
																				height="85px" RepeatColumns="2" MuestraBotones="True">
																				<asp:ListItem Value="PFP">ND por PFP</asp:ListItem>
																				<asp:ListItem Value="SAL">Aplic.Saldos y Tranf.</asp:ListItem>
																				<asp:ListItem Value="EME">Emerg.Agropecuaria</asp:ListItem>
																				<asp:ListItem Value="FAL">Fallecimiento</asp:ListItem>
																				<asp:ListItem Value="REN">Renuncia</asp:ListItem>
																				<asp:ListItem Value="INC">Baja por Incobrables</asp:ListItem>
																				<asp:ListItem Value="OTR">Otros Ajustes</asp:ListItem>
																			</cc1:checklistbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblIngr" runat="server" cssclass="titulo">Ingresos con:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:checklistbox id="lisIngr" runat="server" CssClass="chklst" CellPadding="0" CellSpacing="0" width="100%"
																				height="45px" RepeatColumns="2" MuestraBotones="True">
																				<asp:ListItem Value="RCA">Recibos por Caja</asp:ListItem>
																				<asp:ListItem Value="RAB">Recibos por Acred.Bancaria</asp:ListItem>
																				<asp:ListItem Value="RDA">D&#233;bito Autom&#225;tico</asp:ListItem>
																			</cc1:checklistbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblProm" runat="server" cssclass="titulo">Promociones con:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:checklistbox id="lisProm" runat="server" CssClass="chklst" CellPadding="0" CellSpacing="0" width="100%"
																				height="45px" RepeatColumns="2" MuestraBotones="True">
																				<asp:ListItem Value="RCA">Recibos por Caja</asp:ListItem>
																				<asp:ListItem Value="RAB">Recibos por Acred.Bancaria</asp:ListItem>
																				<asp:ListItem Value="RDA">D&#233;bito Autom&#225;tico</asp:ListItem>
																				<asp:ListItem Value="SAL">Aplic.Saldos y Transf.</asp:ListItem>
																			</cc1:checklistbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblRepo" runat="server" cssclass="titulo">Formato de Reporte:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbRepo" runat="server" Width="224px" AceptaNull="False">
																				<asp:ListItem Value="1" Selected="True">Composici&#243;n - Resumido por Categor&#237;a</asp:ListItem>
																				<asp:ListItem Value="2">Composici&#243;n - Resumido por Fecha</asp:ListItem>
																				<asp:ListItem Value="3">Composici&#243;n - Detallado</asp:ListItem>
																				<asp:ListItem Value="4">Gesti&#243;n - Resumido por Categor&#237;a</asp:ListItem>
																				<asp:ListItem Value="5">Gesti&#243;n - Detallado</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2">&nbsp;</TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD colSpan="3" height="10"></TD>
														</TR>
														<TR>
															<TD align="right" colSpan="3">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																	ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																	ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																	CausesValidation="False"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
			<script language="javascript">
chlstTodos("lisAjus");
chlstTodos("lisIngr");
chlstTodos("lisProm");
			</script>
		</form>
	</BODY>
</HTML>
