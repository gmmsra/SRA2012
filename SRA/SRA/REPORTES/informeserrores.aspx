<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InformesErrores" CodeFile="InformesErrores.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Informes por Errorres</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<SCRIPT language="javascript">
			function mDeterminarRP(pobjRP,pobjNume)
			{
				if (document.all(pobjNume).value == '' && pobjRP.value != '')
				{
					var lstrRp = '';
					var lstrChar = '';
					for (i=0;i<pobjRP.value.length;i++)
					{
						lstrChar = pobjRP.value.substring(i,i+1);
						if (booIsNumber(lstrChar))
							lstrRp = lstrRp + lstrChar;
					}
					if (document.all(pobjNume).value == '')
						document.all(pobjNume).value = lstrRp;
				}
			}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Informe por Errores</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3">
										<asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="100%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24" colSpan="4"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblProcFil" runat="server" cssclass="titulo" ToolTip=" ">Proceso:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 25px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbProc" runat="server" cssclass="cuadrotexto" Width="325px" AceptaNull="False"
																					Height="20px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" noWrap align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblCodigoFil" runat="server" cssclass="titulo" Width="100px">Raza:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 17px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="300px" Height="20px"
																					MostrarBotones="False" NomOper="razas_cargar" filtra="true" AutoPostBack="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="Label3" runat="server" cssclass="titulo" ToolTip=" ">Sexo:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 25px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbSexo" runat="server" Width="138px">
																					<asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem>
																					<asp:ListItem Value="0">Hembra</asp:ListItem>
																					<asp:ListItem Value="1">Macho</asp:ListItem>
																				</cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../Imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../Imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblNumeDesde" runat="server" cssclass="titulo">Nro. Desde:</asp:label>&nbsp;</TD>
																			<TD background="../imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtNumeDesde" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																				<asp:label id="lblNumeHasta" runat="server" cssclass="titulo">Nro. Hasta:&nbsp;</asp:label>
																				<CC1:TEXTBOXTAB id="txtNumeHasta" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../Imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../Imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formfdofields.jpg">
																				<asp:Label id="lblRpdesde" runat="server" cssclass="titulo">RP Desde:</asp:Label>&nbsp;</TD>
																			<TD background="../imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtRPDesde" onblur="javascript:mDeterminarRP(this,'txtRPnumeDesde');" runat="server"
																					cssclass="cuadrotexto" Width="150px"></CC1:TEXTBOXTAB>&nbsp;
																				<cc1:numberbox id="txtRPnumeDesde" runat="server" cssclass="cuadrotexto" Width="100px" esdecimal="False"
																					MaxValor="9999999999999" MaxLength="12" Enabled="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../Imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../Imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formfdofields.jpg">
																				<asp:Label id="lblRPHasta" runat="server" cssclass="titulo">RP Hasta:</asp:Label>&nbsp;</TD>
																			<TD background="../imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtRPHasta" onblur="javascript:mDeterminarRP(this,'txtRPnumeHasta');" runat="server"
																					cssclass="cuadrotexto" Width="150px"></CC1:TEXTBOXTAB>&nbsp;
																				<cc1:numberbox id="txtRPnumeHasta" runat="server" cssclass="cuadrotexto" Width="100px" esdecimal="False"
																					MaxValor="9999999999999" MaxLength="12" Enabled="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblCriadorDesde" runat="server" cssclass="titulo">Nro. Criador Desde:</asp:label>&nbsp;</TD>
																			<TD background="../imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtCriaNumeDesde" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																				<asp:label id="lblCriadorhasta" runat="server" cssclass="titulo">Nro. Hasta:&nbsp;</asp:label>
																				<CC1:TEXTBOXTAB id="txtCriaNumeHasta" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../Imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../Imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblPropFil" runat="server" cssclass="titulo">Propietario:&nbsp;</asp:label></TD>
																			<TD background="../imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrPropClie" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
																					FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Clientes"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblAsoNume" runat="server" cssclass="titulo">Asociación:&nbsp;</asp:label></TD>
																			<TD background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbAsoc" runat="server" cssclass="cuadrotexto" Width="100%" AceptaNull="False"
																					NomOper="asociaciones_cargar" filtra="True" mostrarbotones="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 29px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblMsgFil" runat="server" cssclass="titulo">Error Desde:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 30px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbErrorDesde" runat="server" cssclass="cuadrotexto" Width="300px"
																					Height="20px" MostrarBotones="False" NomOper="rg_mensajes_cargar" filtra="true"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 29px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="Label1" runat="server" cssclass="titulo">Error Hasta:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 30px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbErrorHasta" runat="server" cssclass="cuadrotexto" Width="300px"
																					Height="20px" MostrarBotones="False" NomOper="rg_mensajes_cargar" filtra="true"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="Label4" runat="server" cssclass="titulo" ToolTip=" ">Tipo Reporte:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 25px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbTipoReporte" runat="server" Width="180px">
																					<asp:ListItem Selected="True">(Seleccionar)</asp:ListItem>
																					<asp:ListItem Value="1">Errores por Productos</asp:ListItem>
																					<asp:ListItem Value="2">Cant. Productos por Error</asp:ListItem>
																					<asp:ListItem Value="3">Enviar Mails</asp:ListItem>
																				</cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel>
									</TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle" width="100">&nbsp;</TD>
									<TD align="right">
										<CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ToolTip="Imprimir Listado" ImageOver="btnImpr2.gif"
											ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/"
											IncludesUrl="../includes/" BackColor="Transparent" ForeColor="Transparent" ImageUrl="../imagenes/btnImpr.jpg"
											ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN>&nbsp;
										<CC1:BOTONIMAGEN id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
											BackColor="Transparent" ForeColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD colSpan="3"></TD>
								</tr>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><BR>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all[document.all('txtMini')]!= null)
		{
			mSetearValores(document.all('txtMini'),document.all('cmbMini'));
			mSetearValores(document.all('txtMaxi'),document.all('cmbMaxi'));
		}
		</SCRIPT>
	</BODY>
</HTML>
