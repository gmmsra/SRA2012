'Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports AccesoBD
Imports Interfaces.Importacion


Namespace SRA



Partial Class RemitoDeInscripcion
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents cmbOrdenarPor As NixorControls.ComboBox
    Protected WithEvents lblOrdenarPor As System.Web.UI.WebControls.Label





    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GSZ 13/03/2015
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)


            If Not Page.IsPostBack Then

                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()


    End Sub


    Private Sub mValidarDatos()

        If usrCriadorFil.RazaId = "" Then
            Throw New AccesoBD.clsErrNeg("La Raza debe ser distinto de  vacio")
        End If

    End Sub

    Private Sub mListar()
        Try


            Dim lstrRptName As String = "RemitoDeInscripcion"
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim oEspecies As New SRA_Neg.Especie(mstrConn, Session("sUserId").ToString())
            Dim strCriador As String
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim stRazaDescripcion As String
            Dim lintFechaInicioD, lintFechaInicioH As Integer


            stRazaDescripcion = oRaza.GetRazaDescripcionById( _
                                                             IIf(usrCriadorFil.RazaId.ToString = "", "0", usrCriadorFil.RazaId.ToString)).Trim()

            mValidarDatos()


            lstrRpt += "&raza_id=" & IIf(usrCriadorFil.RazaId.ToString = "", "0", _
                                    usrCriadorFil.RazaId.ToString)

            lstrRpt += "&cria_id=" & IIf(usrCriadorFil.Valor = 0, "0", _
                                                 usrCriadorFil.Valor.ToString())

            'GSZ  fecha de inscripcion cpn  -2 por error en FromOADate
            If (txtFechaDesde.Fecha.ToString <> "") Then
                lintFechaInicioD = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer) - 2
            Else
                lintFechaInicioD = 0

            End If

            If (txtFechaHasta.Fecha.ToString <> "") Then
                lintFechaInicioH = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer) - 2
            Else
                lintFechaInicioH = 0

            End If


            lstrRpt += "&dFechaDesde=" + lintFechaInicioD.ToString
            lstrRpt += "&dFechaHasta=" + lintFechaInicioH.ToString
            lstrRpt += "&FiltroHBADesc=" + oEspecies.GetEspecieTipoHBADescByRazaId(usrCriadorFil.RazaId.ToString)


            If (usrCriadorFil.Valor <> 0) Then
                strCriador = usrCriadorFil.Codi.ToString() + "-" + usrCriadorFil.Apel
            Else
                strCriador = " Todos "
            End If

            If (lintFechaInicioD.ToString = "0" And lintFechaInicioH.ToString = "0") Then
                lstrRpt += "&FiltroRangoFecha= Sin Filtro de Fechas"
            Else
                lstrRpt += "&FiltroRangoFecha=" + IIf(lintFechaInicioD.ToString <> "0", "Fecha Desde :" + txtFechaDesde.Text, "-") + _
                           " " + IIf(lintFechaInicioH.ToString <> "0", "  Fecha Hasta:" + txtFechaHasta.Text, "-")
            End If




            lstrRpt += "&FiltroRazaDescrip=" & _
                               oRaza.GetRazaCodiById(usrCriadorFil.RazaId.ToString).ToString + _
                               "-" + stRazaDescripcion

            lstrRpt += "&FiltroCriadorDescrip=" & strCriador


            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click


        usrCriadorFil.Limpiar()
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""


    End Sub
End Class

End Namespace
