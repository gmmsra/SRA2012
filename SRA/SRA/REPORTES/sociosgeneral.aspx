<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Control="~/controles/usrsociosfiltro.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociosGeneral" CodeFile="SociosGeneral.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="../controles/usrSociosFiltro.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
<title>Padr�n General de Socios</title>
<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
<meta content="JavaScript" name="vs_defaultClientScript">
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="../includes/utiles.js"></script>
<script language="JavaScript">	
		function mValidar()
		{
			if (document.all('cmbEtiqSoci').value == 'C')
			{
				if (document.all('usrSocFil_cmbAsam').value != '')
				{
					return true;
				}
				else
				{
					alert('Si selecciona socios con orden, debe indicar una asamblea.');
					if(document.all("spExp1").innerText=="+")
						CambiarExp('1');
					document.all('usrSocFil_cmbAsam').focus();
					return false;
				}
			}
			else
			{
				return true;
			}
		}
		function mCargarListaSociosEtiquetas()
		{
			if (document.all('usrSoci:txtId')!=null)
			{
				if (document.all('hdnEtiqSoci').value != '')
				{
					var soci = document.all('hdnEtiqSoci').value.split(";");
					for (var fila=0; fila <= soci.length-1; fila++)
					{
						if (soci[fila]!='')
						{
							var strRet = LeerCamposXML("socios_lista_datos", soci[fila], "descrip");
							var It = document.createElement("option");
							It.value = soci[fila];
							It.text = strRet;
							document.all('lisSoci').add(It);
						}
					}				
				}
			}
		}
		function mAlta()
		{
			if (document.all('usrSoci:txtId').value == '')
			{
				alert('Debe indicar el socio.');
				document.all('usrSoci:txtCodi').focus();
				return;
			}
			if (document.all('hdnEtiqSoci').value == '')
				document.all('hdnEtiqSoci').value = ';';
			if (document.all('hdnEtiqSoci').value.indexOf(';'+document.all('usrSoci:txtId').value+';') != -1)
			{
				alert('El socio indicado ya se encuentra seleccionado.');
			}
			else
			{
				document.all('hdnEtiqSoci').value = document.all('hdnEtiqSoci').value + document.all('usrSoci:txtId').value + ';';
				var It = document.createElement("option");
				It.value = document.all('usrSoci:txtId').value;
				It.text = document.all('usrSoci:txtCodi').value + ' - ' + document.all('usrSoci:txtApel').value;
				document.all('lisSoci').add(It);				
				imgLimpClieDeriv_click('usrSoci');
				document.all('usrSoci:txtCodi').focus();
			}
		}
		function mBaja()
		{
			if (document.all('lisSoci').selectedIndex > -1)
			{
				var soci = document.all('lisSoci').value;
				document.all('hdnEtiqSoci').value = document.all('hdnEtiqSoci').value.replace(';'+soci+';',';');
				document.all('lisSoci').remove(document.all('lisSoci').selectedIndex);
			}
			else
			{
				alert('Debe seleccionar un socio a eliminar.');
			}
		}
		function chkRepoDeudaClick(pDeuda)
		{
			document.all("chkRepoInte").checked=false;
			document.all("chkRepoInte").parentElement.disabled=!pDeuda.checked;
			document.all("chkRepoInte").disabled=!pDeuda.checked;
		} 
		function mValidarEnvio() 
		{
			if (document.all('cmbMail').value=='')
			{
				alert('Debe indicar un modelo de mail.');
				document.all('cmbMail').focus();
				return(false);
			}
			else
			{
				return(true);
			}
		}
		function expandir() 
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function mSetearTipo()
		{
			if (document.all('cmbRepo').value=='M')
			{
				document.all('lblMail').style.display='';
				document.all('cmbMail').style.display='';
			}
			else
			{
				document.all('lblMail').style.display='none';
				document.all('cmbMail').style.display='none';
			}
			if (document.all('cmbRepo').value=='E')
			{
				document.all('panDire').style.display='';
				document.all('panEtiq').style.display='';
			}
			else
			{
				document.all('panDire').style.display='none';
				document.all('panEtiq').style.display='none';
			}
			if (document.all('cmbRepo').value=='R')
			{
				document.all('panDatoOpci').style.display='';
			}
			else
			{
				document.all('panDatoOpci').style.display='none';		
			}
		}
		function mRptFiltrosLimpiar()
		{
			EjecutarMetodoXML("Utiles.RptFiltrosLimpiar",document.all("hdnRptId").value);
		}		
		</script>
</HEAD>
<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');mCargarListaSociosEtiquetas();" rightMargin="0"
		onunload="javascript:mRptFiltrosLimpiar();">
<form id="frmABM" method="post" runat="server">
<!------------------ RECUADRO ------------------->
<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
<TBODY>
<tr>
<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
</tr>
<tr>
<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
<td vAlign="middle" align="center">
<!----- CONTENIDO ----->
<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
<TBODY>
<TR>
<TD width="100%" colSpan="3"></TD>
</TR>
<TR>
<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Padr�n General de Socios</asp:label></TD>
</TR>
<TR>
<TD width="100%" colSpan="3"></TD>
</TR>
<!--- filtro --->
<TR>
<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
<TR>
<TD style="WIDTH: 100%">
<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD style="HEIGHT: 8px" width="24"></TD>
<TD style="HEIGHT: 8px" width="42"></TD>
<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
<TD style="HEIGHT: 8px" width="26"></TD>
<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
</TR>
<TR>
<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
<TD><!-- FOMULARIO -->
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"></TD>
</TR>
<TR>
<TD background="../imagenes/formfdofields.jpg" height="28">
<TABLE width="600" border="0">
<TR>
<TD style="WIDTH: 265px" noWrap>&nbsp;&nbsp;
<asp:label id="lblRepo" runat="server" cssclass="titulo">Tipo de Reporte:</asp:label>&nbsp;
<cc1:combobox class="combo" id="cmbRepo" runat="server" Width="119px" onchange="javascript:mSetearTipo();">
<asp:ListItem Value="R" Selected="True">Reporte</asp:ListItem>
<asp:ListItem Value="E">Etiquetas</asp:ListItem>
<asp:ListItem Value="M">Mails</asp:ListItem>
<asp:ListItem Value="T">Totales</asp:ListItem>
</cc1:combobox></TD>
<TD style="WIDTH: 114px">
<DIV id="panDire" noWrap>
<asp:checkbox id="chkDire" CssClass="titulo" Runat="server" Text="Con Direcci�n"></asp:checkbox></DIV>
</TD>
<TD>
<asp:label id="lblMail" runat="server" cssclass="titulo">Modelo:</asp:label></TD>
<TD>
<cc1:combobox class="combo" id="cmbMail" runat="server" Width="119px"></cc1:combobox></TD>
</TR>
<TR>
<TD noWrap colSpan="5">
<DIV class="panelediciontransp" id="panDatoOpci" style="WIDTH: 420px" runat="server">
<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD style="WIDTH: 140px" noWrap align="right">
<asp:label id="lblOpciDato" runat="server" cssclass="titulo">Datos Opcionales:&nbsp;</asp:label></TD>
<TD style="WIDTH: 109px">
<asp:CheckBox id="chkRepoTele" CssClass="titulo" Runat="server" Text="Tel�fono" Checked="false"></asp:CheckBox></TD>
<TD></TD>
<TD>
<asp:CheckBox id="chkRepoCateFecha" CssClass="titulo" Runat="server" Text="Fecha Categor�a" Checked="false"></asp:CheckBox></TD>
</TR>
<TR>
<TD style="WIDTH: 96px"></TD>
<TD style="WIDTH: 109px">
<asp:CheckBox id="chkRepoMail" CssClass="titulo" Runat="server" Text="Mail" Checked="false"></asp:CheckBox></TD>
<TD></TD>
<TD>
<asp:CheckBox id="chkRepoIngreFecha" CssClass="titulo" Runat="server" Text="Fecha Ingreso" Checked="false"></asp:CheckBox></TD>
</TR>
<TR>
<TD style="WIDTH: 96px"></TD>
<TD style="WIDTH: 109px">
<asp:CheckBox id="chkRepoDeuda" onclick="chkRepoDeudaClick(this);" CssClass="titulo" Runat="server"
																													Text="Deuda"
																													Checked="false"></asp:CheckBox></TD>
<TD></TD>
<TD></TD>
</TR>
<TR>
<TD style="WIDTH: 96px"></TD>
<TD style="WIDTH: 109px">
<asp:CheckBox id="chkRepoInte" CssClass="titulo" Runat="server" Text="Intereses" Checked="false"
																													Enabled="False"></asp:CheckBox></TD>
<TD>&nbsp;&nbsp;
</TD>
<TD></TD>
</TR>
</TABLE>
</DIV>
</TD>
</TR>
<TR>
<TD noWrap colSpan="5">
<DIV class="panelediciontransp" id="panEtiq" style="WIDTH: 100%" runat="server">
<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD noWrap align="left" height="30"></TD>
<TD noWrap align="left" colSpan="2" height="30">
<asp:label id="lblEtiqSoci" runat="server" cssclass="titulo">Etiquetas de Socios:</asp:label>&nbsp;
<cc1:combobox class="combo" id="cmbEtiqSoci" runat="server" Width="128px">
<asp:ListItem Value="R" Selected="True">SR/A</asp:ListItem>
<asp:ListItem Value="C">Socio con Orden</asp:ListItem>
<asp:ListItem Value="S">Socio sin Orden</asp:ListItem>
</cc1:combobox></TD>
</TR>
<TR>
<TD noWrap align="left"></TD>
<TD noWrap align="left" colSpan="2">
<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD align="left" width="50">
<asp:label id="lblSoci" runat="server" cssclass="titulo">Socio:</asp:label>&nbsp;</TD>
<TD>
<UC1:CLIE id="usrSoci" runat="server" MuestraDesc="False" Tabla="Socios"></UC1:CLIE></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD noWrap align="center"></TD>
<TD noWrap align="left" colSpan="2">
<asp:ListBox id="lisSoci" runat="server" Width="470px" Rows="6"></asp:ListBox></TD>
</TR>
<TR>
<TD noWrap align="center" height="34">&nbsp;&nbsp;
</TD>
<TD noWrap align="left" colSpan="2" height="34">&nbsp;
<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="100px" Text="Agregar"></asp:Button>&nbsp;&nbsp;
<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="100px" Text="Eliminar"></asp:Button></TD>
</TR>
</TABLE>
</DIV>
</TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD background="../imagenes/formfdofields.jpg">
<asp:datagrid id="grdCons" runat="server" BorderStyle="None" BorderWidth="1px" CellSpacing="1"
																						CellPadding="1"
																						width="100%"
																						AllowPaging="True"
																						HorizontalAlign="Center"
																						GridLines="None"
																						OnPageIndexChanged="DataGrid_Page"
																						PageSize="10"
																						ItemStyle-Height="5px"
																						AutoGenerateColumns="False">
<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
<FooterStyle CssClass="footer"></FooterStyle>
<Columns>
<asp:TemplateColumn Visible="false">
<HeaderStyle Width="5%"></HeaderStyle>
<ItemTemplate>
<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																										Height="5">
<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
</asp:LinkButton>
</ItemTemplate>
</asp:TemplateColumn>
</Columns>
<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
</asp:datagrid></TD>
</TR>
<TR>
<TD background="../imagenes/formfdofields.jpg">
<UC1:SOCFIL id="usrSocFil" runat="server"></UC1:SOCFIL></TD>
</TR>
<TR>
<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"></TD>
</TR>
</TABLE>
</TD>
<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
</TR>
<TR>
<TD colSpan="3" height="10"></TD>
</TR>
<TR>
<TD align="right" colSpan="2">
<CC1:BotonImagen id="btnEnvi" runat="server" BorderStyle="None" ImageDisable="btnEnvi0.gif" IncludesUrl="../includes/"
																			ImagesUrl="../imagenes/"
																			ImageUrl="imagenes/btnEnvi.gif"
																			BackColor="Transparent"
																			CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnEnvi.gif"
																			ImageOver="btnEnvi2.gif"
																			ForeColor="Transparent"
																			visible="false"></CC1:BotonImagen>
<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																			ImageUrl="imagenes/btnImpr.jpg"
																			BackColor="Transparent"
																			CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnImpr.gif"
																			ImageOver="btnImpr2.gif"
																			ForeColor="Transparent"></CC1:BotonImagen>
<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																			ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent"
																			CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																			ImageOver="btnLimp2.gif"
																			ForeColor="Transparent"
																			CausesValidation="False"></CC1:BotonImagen></TD>
<TD width="2"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
<!---fin filtro --->
<TR>
<TD vAlign="middle" colSpan="3"></TD>
</TR>
</TBODY>
</TABLE>
<!--- FIN CONTENIDO ---></td>
<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
</tr>
<tr>
<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
</tr>
</TBODY>
</table>
<!----------------- FIN RECUADRO ----------------->
<DIV style="DISPLAY: none">
<ASP:TEXTBOX id="hdnEtiqSoci" runat="server" Width="140px"></ASP:TEXTBOX>
<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
<ASP:TEXTBOX id="hdnRptId" runat="server" Width="140px"></ASP:TEXTBOX>
</DIV>
</form>
</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
<P></P>
<DIV></DIV>
</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
<script language="JavaScript">
	mSetearTipo();	
</script>
</BODY>
</HTML>
