<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CertificadoRegIndivBovinos" CodeFile="CertificadoRegIndivBovinos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Certificado de Registros Genealógicos Bovinos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina"  leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server" >
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Certificado de Registro Genealógico Bovinos</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!-- FOMULARIO -->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TBODY>
																	<TR>
																		<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																		<TD><!-- FOMULARIO -->
																			<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																				<TBODY>
																					<TR>
																						<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																					</TR>
																					<!--<TR>
																						<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																							<asp:Label id="lblCriaNumeDesde" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left"></TD>
																					</TR>
																					-->
																					<TR>
																						<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																							<asp:Label id="lblCriaNumeHasta" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																							<cc1:combobox id="cmbSexoFil" class="combo" runat="server" Width="113px">
																								<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																								<asp:ListItem Value="false">Hembra</asp:ListItem>
																								<asp:ListItem Value="true">Macho</asp:ListItem>
																							</cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																							<asp:Label id="lblSraNumeDesde" runat="server" cssclass="titulo">HBA Desde:</asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																							<CC1:TEXTBOXTAB id="txtSraNumeDesde" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																								obligatorio="True"></CC1:TEXTBOXTAB></TD>
																					</TR>
																					<TR>
																						<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																							<asp:Label id="lblSraNumeHasta" runat="server" cssclass="titulo">HBA Hasta:</asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																							<CC1:TEXTBOXTAB id="txtSraNumeHasta" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																								obligatorio="True"></CC1:TEXTBOXTAB></TD>
																					</TR>
																					<TR>
																						<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 8.91%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																							<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo" Width="58px">Raza/Criador: </asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																							<UC1:CLIE id="usrCriadorFil" runat="server" MostrarBotones="False" AceptaNull="false" Tabla="Criadores"
																								Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" Criador="True"
																								CampoVal="Criador" FilClaveUnica="True" ColCriaNume="True" FilAgru="False" FilTarjNume="False"
																								ColCUIT="True" FilCUIT="True"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 211px" background="../imagenes/formfdofields.jpg" width="211" align="right">
																							<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																							<asp:RadioButton id="rbtnFechaAprobacion" runat="server" cssclass="titulo" Width="144px" checked="True"
																								GroupName="RadioGroup1" Text="Fecha Aprobación"></asp:RadioButton>
																							<asp:RadioButton id="rbtnFechaInscripcion" runat="server" cssclass="titulo" Width="149px" checked="False"
																								GroupName="RadioGroup1" Text="Fecha Inscripción"></asp:RadioButton>&nbsp;&nbsp;
																							<asp:Label style="Z-INDEX: 0" id="lblDesde" runat="server" cssclass="titulo">Desde:</asp:Label>
																							<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																								enabled="true"></cc1:DateBox>
																							<asp:Label style="Z-INDEX: 0" id="lblHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																							<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																								enabled="true"></cc1:DateBox>
																						</TD>
																					</TR>
																					<TR>
																						<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 2.02%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																							<asp:Label id="lblFiltro" runat="server" cssclass="titulo">Filtro:</asp:Label>&nbsp;</TD>
																						<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																							<cc1:combobox id="cmbCertifTipoFil" class="combo" runat="server" Width="113px">
																								<asp:ListItem Selected="True">(Todos)</asp:ListItem>
																								<asp:ListItem Value="false">Pendientes</asp:ListItem>
																								<asp:ListItem Value="true">Originales</asp:ListItem>
																							</cc1:combobox></TD>
																					</TR>
																		</TD>
														</TD>
													</TR>
													<TR>
														<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
										</TD>
										<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
									</TR>
									<TR>
										<TD width="3"></TD>
										<TD align="right">
											<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
												ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
												ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"></CC1:BotonImagen>
											<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
												ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
												OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"
												CausesValidation="False"></CC1:BotonImagen></TD>
										<TD width="2"></TD>
									</TR>
								</TBODY>
							</TABLE>
						</td>
					</tr>
				</TBODY>
				<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
			</table>
			</asp:panel></TD></TR> 
			<!---fin filtro ---> </TBODY></TABLE> 
			<!--- FIN CONTENIDO ---> 
			</TD>
			<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
			</TR>
			<tr>
				<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
				<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
				<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">&nbsp;</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
	</BODY>
</HTML>
