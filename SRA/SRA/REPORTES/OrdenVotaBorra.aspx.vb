Namespace SRA

Partial Class OrdenVotaBorra
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsamFil, "S", "")
   End Sub

   Private Sub mLimpiarFiltros()
      cmbAsamFil.Limpiar()
      chkApod.Checked = True
      chkFirm.Checked = False
      chkSoci.Checked = False
      chkPode.Checked = True
      txtMesaDesde.Text = ""
      txtMesaHasta.Text = ""
   End Sub

#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
            If cmbAsamFil.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la asamblea")
            End If

            Dim lstrRptName As String '= "OrdenVotaMesas"
            Dim lstrUrl As String
            If cmbTipo.Valor.ToString = "R" Then
                lstrRptName = "OrdenVotaMesasResu"
                lstrUrl += "&tipo=R"
            Else
                lstrRptName = "OrdenVotaMesas"
                lstrUrl += "&tipo=D"
            End If
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            hdnRptId.Text = "0"

            lstrRpt += "&asam_id=" + cmbAsamFil.Valor.ToString
            lstrRpt += "&mesa_desde=" + IIf(txtMesaDesde.Text <> "", txtMesaDesde.Text, "0")
            lstrRpt += "&mesa_hasta=" + IIf(txtMesaHasta.Text <> "", txtMesaHasta.Text, "0")
            lstrRpt += "&firm=" + IIf(chkFirm.Checked, "True", "False")
            lstrRpt += "&apod=" + IIf(chkApod.Checked, "True", "False")
            lstrRpt += "&soci=" + IIf(chkSoci.Checked, "True", "False")
            lstrRpt += "&pode=" + IIf(chkPode.Checked, "True", "False")
            lstrRpt += "&orden=" + "0"
            lstrRpt += "&dist_desde=" + "0"
            lstrRpt += "&dist_hasta=" + "0"
            'lstrRpt += "&tipo=" + cmbTipo.Valor.ToString
            lstrRpt += "&rptf_id=" + hdnRptId.Text.ToString
            lstrRpt += lstrUrl
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
