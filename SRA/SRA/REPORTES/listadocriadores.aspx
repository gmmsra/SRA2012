<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ListadoCriadores" enableViewState="True" CodeFile="ListadoCriadores.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Listado de Criadores</title>
		<meta name="vs_showGrid" content="True">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../stylesheet/SRA.css">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
		<script language="JavaScript">
			function mCargarLocalidades(pProv,pcmbLoca)
			{
				var sFiltro = pProv.value;
   				if (sFiltro != '')
				{
					LoadComboXML("localidades_cargar", sFiltro, pcmbLoca, "S");
				}
				else
				{
					document.all(pcmbLoca).innerText='';
				}
			}
		
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('')" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="../imagenes/recsup.jpg"><IMG border="0" src="../imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="../imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="../imagenes/reciz.jpg" width="9"><IMG border="0" src="../imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Listado Criadores</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE class="FdoFld" border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Listado:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtFechaListado" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																						IncludesUrl="Includes/"></cc1:DateBox>&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblRazaDesde" runat="server" cssclass="titulo">Raza: Desde:</asp:Label>&nbsp;</TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbRazaDesde" class="combo" runat="server" cssclass="cuadrotexto" Width="280px"
																						filtra="true" NomOper="razas_cargar" MostrarBotones="False" Height="20px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblRazaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;</TD>
																				<TD background="../imagenes/formfdofields.jpg" align="left">
																					<cc1:combobox id="cmbRazaHasta" class="combo" runat="server" cssclass="cuadrotexto" Width="280px"
																						filtra="true" NomOper="razas_cargar" MostrarBotones="False" Height="20px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 21px" align="right">
																					<asp:label id="lblProv" runat="server" cssclass="titulo">Provincia:</asp:label>&nbsp;</TD>
																				<TD style="HEIGHT: 21px">
																					<cc1:combobox id="cmbProvDesde" class="combo" runat="server" Width="200px" onchange="mCargarLocalidades(this,'cmbLocaDesde')"></cc1:combobox>&nbsp;&nbsp;&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																					<asp:label id="lblLocalidad" runat="server" cssclass="titulo">Localidad: Desde:</asp:label>&nbsp;</TD>
																				<TD>
																					<cc1:combobox id="cmbLocaDesde" class="combo" runat="server" Width="200px" CambiaValor="True"></cc1:combobox>&nbsp;
																					<asp:label id="Label2" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																					<cc1:combobox id="cmbLocaHasta" class="combo" runat="server" Width="205px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																					<asp:label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
																				<TD>
																					<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB>
																					<asp:checkbox id="chkBuscNomb" Text="Buscar en..." CssClass="titulo" Runat="server"></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">&nbsp;</TD>
																				<TD>&nbsp;&nbsp;&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
																<TR>
																	<TD colSpan="3" align="right">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			CambiaValor="False" ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																			BtnImage="edit.gif" OutImage="del.gif" BackColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			CambiaValor="False" ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
																			BtnImage="edit.gif" OutImage="del.gif" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="../imagenes/recde.jpg" width="13"><IMG border="0" src="../imagenes/recde.jpg" width="13" height="10">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="../imagenes/recinf.jpg"><IMG border="0" src="../imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="../imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
