<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PadronAsamblea" enableViewState="True" CodeFile="PadronAsamblea.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Padr�n Asamblea de Socios</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
		<script language="JavaScript" src="../includes/impresion.js"></script>
		<script language="JavaScript">
		function mRptFiltrosLimpiar()
		{
			if (document.all("hdnRptId").value != '')
			   EjecutarMetodoXML("Utiles.RptFiltrosLimpiar",document.all("hdnRptId").value);
		}		
		
		function mSetearGrupo()
		{
			//document.all("hdnGrupo").value = document.all("cmbGrupo").value;			
		}
		
		function mCargarGrupos() 
		{
		    //var sFiltro = document.all("cmbAsamFil").value;
		    //var lstrGrupo = document.all("hdnGrupo").value;
		    //alert('<%=Session("sGrupo")%>');
		    //if (sFiltro != '')
		    //{
			    //LoadComboXML("grupos_por_asambleas_cargar", sFiltro, "cmbGrupo", "T");
			    //for (var lintIndi = 0; lintIndi < document.all("cmbGrupo").length; lintIndi++)
				//{				
					//if (document.all("cmbGrupo").options[lintIndi].value == lstrGrupo)
					//{
						//document.all("cmbGrupo").selectedIndex = lintIndi;
					//}
				//}
			//}
		}		
		
		function HabilitarContrEtiq()
		{		
			document.all("rbtnConDire").checked=true;
			document.all("rbtnConDire").parentElement.disabled=true;
			document.all("rbtnConDire").disabled=true;
			
			document.all("rbtnSinDire").checked=false;
			document.all("rbtnSinDire").parentElement.disabled=true;
			document.all("rbtnSinDire").disabled=true;
			
			document.all("chkTelefono").checked=false;
			document.all("chkTelefono").parentElement.disabled=true;
			document.all("chkTelefono").disabled=true;
			   
			document.all("chkDeuda").checked=false;
			document.all("chkDeuda").parentElement.disabled=true;
			document.all("chkDeuda").disabled=true;
			
			document.all("chkInte").checked=false;
			document.all("chkInte").parentElement.disabled=true;
			document.all("chkInte").disabled=true;
			
			document.all("rbtnOrdeVota").checked=true;
			document.all("rbtnOrdeVota").parentElement.disabled=true;
			document.all("rbtnOrdeVota").disabled=true;

			document.all("rbtnDistOrdeVota").checked=false;
			document.all("rbtnDistOrdeVota").parentElement.disabled=true;
			document.all("rbtnDistOrdeVota").disabled=true;
				
			switch (document.all("cmbListar").value)
			{
			case "E":
				document.all("rbtnOrdeVota").parentElement.disabled=false;
				document.all("rbtnOrdeVota").disabled=false;
				
				document.all("rbtnDistOrdeVota").parentElement.disabled=false;
				document.all("rbtnDistOrdeVota").disabled=false;
				
				document.all("rbtnConDire").parentElement.disabled=false;
				document.all("rbtnConDire").disabled=false;

				document.all("rbtnSinDire").parentElement.disabled=false;
				document.all("rbtnSinDire").disabled=false;
				break;
			case "R":
				document.all("chkTelefono").checked=false;
				document.all("chkTelefono").parentElement.disabled=false;
				document.all("chkTelefono").disabled=false;

				document.all("chkDeuda").checked=false;
				document.all("chkDeuda").parentElement.disabled=false;
				document.all("chkDeuda").disabled=false;
				
				document.all("chkInte").checked=false;
				document.all("chkInte").parentElement.disabled=true;
				document.all("chkInte").disabled=true;
			
				document.all("rbtnOrdeVota").checked=true;
				document.all("rbtnOrdeVota").parentElement.disabled=false;
				document.all("rbtnOrdeVota").disabled=false;

				document.all("rbtnDistOrdeVota").checked=false;
				document.all("rbtnDistOrdeVota").parentElement.disabled=false;
				document.all("rbtnDistOrdeVota").disabled=false;
				break;
			case "S":
				document.all("rbtnOrdeVota").parentElement.disabled=false;
				document.all("rbtnOrdeVota").disabled=false;

				document.all("rbtnDistOrdeVota").parentElement.disabled=false;
				document.all("rbtnDistOrdeVota").disabled=false;
				
				break;
			}
		}
		
		
		function mCambiarCheck(BotonOrigen,BotonDestino,HiddenText)
		{
		    if (document.all[BotonOrigen].checked==true)
				{
				document.all[BotonDestino].checked=false;
				document.all[HiddenText].value=1;
				}
			else 
				{
				document.all[BotonDestino].checked=true;
				document.all[HiddenText].value=0;
				}
		}

		function chkDeudaClick()
		{
			document.all("chkInte").checked=false;
			document.all("chkInte").parentElement.disabled=!document.all("chkDeuda").checked;
			document.all("chkInte").disabled=!document.all("chkDeuda").checked;
		}		
		
		function Imprimir()
		{
			try
			{
				//if (document.all("chkVistaPrevia").checked==false)
				if (false) // imprimir siempre desde el servidor
				{
					if (document.all("cmbAsamFil").value!="")
					{
					var Devuelve="ON";
					var Grupo=document.all("cmbGrupo").value;
					var DistDesde=document.all("cmbDistDesde").value;
					var DistHasta=document.all("cmbDistHasta").value;
					if (document.all("cmbGrupo").value=="")
						Grupo="-1";
						
					if (document.all("cmbDistDesde").value=="")
					    DistDesde="0";
					  
					if (document.all("cmbDistDesde").value=="")
						DistHasta="0";
					
						if (document.all("cmbListar").value=="R")
						{
							//Listado
							if (document.all("hdnOrdenar").value=="1")
								{
								if(document.all("chkDeuda").Checked==true)
									devuelve="OD";
									
								if(document.all("chkInte").Checked==true)
									devuelve+="I";
								}
							else
								Devuelve="D"
							
							var sRet = ImprimirReporte("PadronAsambleas","AsamId;Grupo;DistDesde;DistHasta;Devuelve;ConTelefono;ConDeuda;random", document.all("cmbAsamFil").value + ";" + document.all("cmbGrupo").value + ";" + document.all("cmbDistDesde").value  + ";" + document.all("cmbDistHasta").value + ";" + Devuelve + ";" + document.all("chkTelefono").checked  + ";" + document.all("chkDeuda").checked + ";0");
						}

						if (document.all("cmbListar").value=="E")
						{
							//Etiquetas
							Devuelve="E"
							var sRet = ImprimirReporte("PadronAsambleasEtiquetas","AsamId;Grupo;DistDesde;DistHasta;Devuelve;ConDireccion;random", document.all("cmbAsamFil").value + ";" + Grupo + ";" + DistDesde + ";" + DistHasta + ";" + Devuelve + ";" + document.all("rbtnConDire").checked + ";0");
						}
						
						if (document.all("cmbListar").value=="S")
						{
							//Sobres
							Devuelve="E"
							var sRet = ImprimirReporte("PadronAsambleaSobres","AsamId;Grupo;DistDesde;DistHasta;Devuelve;ConDireccion;random", document.all("cmbAsamFil").value + ";" + Grupo + ";" + DistDesde + ";" + DistHasta + ";" + Devuelve + ";" + document.all("rbtnConDire").checked + ";0");
						}
						
						if (document.all("cmbListar").value=="T")
						{
							//Por Pa�s
							var sRet = ImprimirReporte("PadronAsambleaPorProvincia","Asam_Id", document.all("cmbAsamFil").value);
						}
					}
					else
						alert("Debe seleccionar la Asamblea.");
				}
				else
					{
					document.all("hdnImprimio").value="1";
					//__doPostBack('hdnImprimio','');
					}
			}
			catch(e)
			{
				alert("Error al intentar efectuar la impresi�n");
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="HabilitarContrEtiq();gSetearTituloFrame('');"
		rightMargin="0" onunload="javascript:mRptFiltrosLimpiar();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Padr�n Asamblea de Socios</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="Label2" runat="server" cssclass="titulo">Listar:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbListar" class="combo" runat="server" Width="168px" ImagesUrl="../Images/"
																						IncludesUrl="../Includes/" AceptaNull="false" onchange="HabilitarContrEtiq()">
																						<asp:ListItem Value="E">Etiquetas</asp:ListItem>
																						<asp:ListItem Value="R" Selected="True">Reporte</asp:ListItem>
																						<asp:ListItem Value="T">Total por Provincia</asp:ListItem>
																						<asp:ListItem Value="S">Sobres</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblAsamFil" runat="server" cssclass="titulo">Asamblea:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbAsamFil" class="combo" runat="server" Width="344px" ImagesUrl="../Images/"
																						IncludesUrl="../Includes/" AceptaNull="false" onchange="mCargarGrupos();" autopostback="true"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="Label3" runat="server" cssclass="titulo">Grupo:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbGrupo" class="combo" runat="server" Width="240px" ImagesUrl="../Images/"
																						IncludesUrl="../Includes/" AceptaNull="false" onchange="javascript:mSetearGrupo();" NomOper="grupos_por_asambleas_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblDistFil" runat="server" cssclass="titulo">Distritos:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbDistDesde" class="combo" runat="server" Width="130px" ImagesUrl="../Images/"
																						IncludesUrl="../Includes/" AceptaNull="false"></cc1:combobox>&nbsp;
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:Label>
																					<cc1:combobox id="cmbDistHasta" class="combo" runat="server" Width="130px" ImagesUrl="../Images/"
																						IncludesUrl="../Includes/" AceptaNull="false"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha Reporte:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="68px" ImagesUrl="Images/"
																						IncludesUrl="Includes/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" vAlign="top" background="../imagenes/formfdofields.jpg"
																					align="right">
																					<asp:Label id="lblDatosOpcionales" runat="server" cssclass="titulo">Datos Opcionales:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<TABLE style="WIDTH: 100%; HEIGHT: 100%" border="0" cellSpacing="0" cellPadding="0">
																						<TR>
																							<TD style="WIDTH: 15%" height="20" vAlign="top" background="../imagenes/formfdofields.jpg"
																								align="left">
																								<asp:CheckBox id="chkTelefono" Text="Tel�fono" Runat="server" CssClass="titulo" Checked="false"></asp:CheckBox>
																								<asp:CheckBox id="chkCorreoE" Text="Correo Elect." Runat="server" CssClass="titulo" Checked="false"></asp:CheckBox>
																								<asp:CheckBox id="chkDeuda" onclick="chkDeudaClick();" Text="Deuda" Runat="server" CssClass="titulo"
																									Checked="false"></asp:CheckBox>
																								<asp:CheckBox id="chkInte" Text="Intereses" Runat="server" CssClass="titulo" Checked="false" Enabled="False"></asp:CheckBox></TD>
																							<TD style="WIDTH: 85%" vAlign="middle" background="../imagenes/formfdofields.jpg" align="left">
																								<asp:RadioButton id="rbtnOrdeVota" tabIndex="5" onclick="mCambiarCheck('rbtnOrdeVota','rbtnDistOrdeVota','hdnOrdenar')"
																									runat="server" cssclass="titulo" Width="100%" Text="Por Orden de Votaci�n" checked="True" GroupName="RadioGroup1"></asp:RadioButton>
																								<asp:RadioButton id="rbtnDistOrdeVota" tabIndex="5" onclick="mCambiarCheck('rbtnOrdeVota','rbtnDistOrdeVota','hdnOrdenar')"
																									runat="server" cssclass="titulo" Width="100%" Text="Por Distrito + Orden de Votaci�n" checked="false"
																									GroupName="RadioGroup1"></asp:RadioButton>
																								<asp:RadioButton id="rbtnConDire" tabIndex="5" onclick="mCambiarCheck('rbtnConDire','rbtnSinDire','hdnConDire')"
																									runat="server" cssclass="titulo" Width="100%" Text="Con Direcci�n" Enabled="false" checked="True"
																									GroupName="RadioGroup2"></asp:RadioButton>
																								<asp:RadioButton id="rbtnSinDire" tabIndex="5" onclick="mCambiarCheck('rbtnConDire','rbtnSinDire','hdnConDire')"
																									runat="server" cssclass="titulo" Width="100%" Text="Sin Direcci�n" Enabled="false" checked="False"
																									GroupName="RadioGroup2"></asp:RadioButton></TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
																<TR>
																	<TD height="10" width="3"></TD>
																	<TD height="10" align="right">
																		<asp:CheckBox id="chkVistaPrevia" Visible="False" Text="Incluir vista previa" Runat="server" CssClass="titulo"
																			Checked="True"></asp:CheckBox></TD>
																	<TD height="10" width="2"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			Accion="Imprimir();" ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
																			BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"
																			CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnRptId" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnConDire" runat="server" Width="140px">1</ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnOrdenar" runat="server" Width="140px">1</ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnRptName" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnParamNom" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnParamVal" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnImprimio" runat="server" Width="140px" AutoPostBack="True"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="Textbox1" runat="server" Width="140px" AutoPostBack="True"></ASP:TEXTBOX>
			</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		<script language="javascript">
		//mCargarGrupos();
		//alert(document.all('hdnGrupo').value)
		</script>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
