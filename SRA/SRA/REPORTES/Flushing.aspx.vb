Namespace SRA

Partial Class Flushing
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

#Region "Definición de Variables"
	Private mstrConn As String
	Private mstrActiId As String
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()

			If (Not Page.IsPostBack) Then
				mCargarCombos()
			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mInicializar()
		If Not Request.QueryString("act") Is Nothing Then
			mstrActiId = Request.QueryString("act")
		Else
			mstrActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", Request.QueryString("fkvalor")).Tables(0).Rows(0).Item("inse_acti_id")
        End If

        usrPadre.FilSexo = False
        usrPadre.Sexo = 1
        usrPadre.FilRpNume = True
        usrPadre.IgnogaInexistente = True

        usrMadre.FilSexo = False
        usrMadre.Sexo = 0
        usrMadre.FilRpNume = True
        usrMadre.IgnogaInexistente = True

    End Sub

    Private Sub mCargarCombos()
        'clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
        'SRA_Neg.Utiles.gSetearRaza(cmbRaza)
    End Sub

    Private Sub mLimpiarFiltros()

        txtRecuFecha.Text = ""
        usrPadre.Limpiar()
        usrMadre.Limpiar()


    End Sub
#End Region

	Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		Try
			Dim lstrRptName As String
			Dim lintRF As Integer

			lstrRptName = "Flushing"

			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&flush_raza_id=" & IIf(usrPadre.RazaId.ToString = "", "0", usrPadre.RazaId.ToString)
            lstrRpt += "&flush_cria_id=" & IIf(usrPadre.CriaOrPropId = "", 0, usrPadre.CriaOrPropId)
			lstrRpt += "&flush_padre_id=" + IIf(usrPadre.Valor.ToString = "", "0", usrPadre.Valor.ToString)
			lstrRpt += "&flush_madre_id=" + IIf(usrMadre.Valor.ToString = "", "0", usrMadre.Valor.ToString)
			
			If txtRecuFecha.Text = "" Then
				lintRF = 0
			Else
				lintRF = CType(clsFormatear.gFormatFechaString(txtRecuFecha.Text, "Int32"), Integer)
			End If

			lstrRpt += "&flush_fecha=" & lintRF

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltros()
	End Sub
End Class
End Namespace
