Namespace SRA

Partial Class SociosDetaPagos
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "inscripciones"

   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mEstablecerPerfil()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFiltros()
        usrSociFil.Limpiar()
        txtAnio.Valor = ""
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
   End Sub
#End Region

   Private Sub mListar()
      Try
            Dim lstrRptName As String = "DetallePagoSocio"

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&soci_id=" + usrSociFil.Valor.ToString
            lstrRpt += "&anio=" + txtAnio.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         If usrSociFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el socio!")
            ElseIf txtAnio.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el a�o!")
            Else
                mListar()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub

End Class
End Namespace
