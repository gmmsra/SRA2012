Namespace SRA

Partial Class DepositosProyeccion
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()

            Dim mobj As SRA_Neg.Facturacion
            mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
            mobj.CentroEmisorNro(mstrConn)

            If mobj.ObtenerValorCampo(mstrConn, "EMISORES_CTROS", "emct_central", mobj.pCentroEmisorId.ToString) Then
               cmbCentroEmisor.Enabled = True
            Else
               cmbCentroEmisor.Valor = mobj.pCentroEmisorId
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMoneda, "T")
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         If cmbTipoListado.Valor.ToString = "R" Then
            lstrRptName = "ProyeccionDepositosNoDeta"
         Else
            lstrRptName = "ProyeccionDepositosDeta"
         End If

         If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)


         lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
         lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
         lstrRpt += "&MoneId=" + IIf(cmbMoneda.Valor.ToString = "", "0", cmbMoneda.Valor.ToString)
         lstrRpt += "&CenEmId=" + IIf(cmbCentroEmisor.Valor.ToString = "", "0", cmbCentroEmisor.Valor.ToString)
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class

End Namespace
