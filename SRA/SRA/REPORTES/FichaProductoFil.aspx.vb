Imports System.Data.SqlClient


Namespace SRA


Partial Class FichaProductoFil

    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"

    Private mstrConn As String



#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()


            If (Not Page.IsPostBack) Then

                mEstablecerPerfil()

                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mInicializar()
        usrProducto.Tabla = SRA_Neg.Constantes.gTab_Productos
        usrProducto.AutoPostback = False
        usrProducto.Ancho = 790
        usrProducto.Alto = 510
        usrProducto.IncluirDeshabilitados = True
        usrProducto.FilRpNume = True
    End Sub
    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
        'Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub

#End Region


    Private Sub mImprimir()
        Try
            Dim mobj As SRA_Neg.Pedigree
            mobj = New SRA_Neg.Pedigree(mstrConn)
            mobj.ArmarArbolPedigree(mstrConn, usrProducto.Valor)
            Dim params As String
            Dim lstrRptName As String = "FichaProducto"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            params += "&prdt_id=" + usrProducto.Valor.ToString
            'PADRE
            params += "&P=" + mobj.pPadreA()
            'RAMA DEL ABUELO PATERNO
            params += "&P1=" + mobj.pPadreA1()
            params += "&P11=" + mobj.pPadreA21()
            params += "&P10=" + mobj.pMadreA21()
            'RAMA DE LA ABUELA PATERNA
            params += "&P0=" + mobj.pMadreA1()
            params += "&P01=" + mobj.pPadreA22()
            params += "&P00=" + mobj.pMadreA22()

            'MADRE
            params += "&M=" + mobj.pMadreB()
            'RAMA DEL ABUELO MATERNO 
            params += "&M1=" + mobj.pPadreB1()
            params += "&M11=" + mobj.pPadreB11()
            params += "&M10=" + mobj.pMadreB11()
            'RAMA DE LA ABUELA MATERNA
            params += "&M0=" + mobj.pMadreB1()
            params += "&M01=" + mobj.pPadreB12()
            params += "&M00=" + mobj.pMadreB12()
            params += "&tipo=True"
            params += "&ProdNom=" + mobj.pProd()



            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            If usrProducto.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un producto.")

            Else
                mImprimir()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
