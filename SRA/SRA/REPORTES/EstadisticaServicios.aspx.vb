Namespace SRA

Partial Class EstadisticaServicios
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents lblNume As System.Web.UI.WebControls.Label
	Protected WithEvents Label1 As System.Web.UI.WebControls.Label
	'Protected WithEvents lblFechaHasta As System.Web.UI.WebControls.Label
	'Protected WithEvents lblAltaFechaHasta As System.Web.UI.WebControls.Label


	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

#Region "Definición de Variables"
	Private mstrConn As String
	Private mstrActiId As String
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()

			If (Not Page.IsPostBack) Then
				mCargarCombos()

			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mInicializar()
		If Not Request.QueryString("act") Is Nothing Then
			mstrActiId = Request.QueryString("act")
		Else
			mstrActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", Request.QueryString("fkvalor")).Tables(0).Rows(0).Item("inse_acti_id")
		End If
	End Sub

	Private Sub mCargarCombos()
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaDesde, "id", "descrip_codi", "S")
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaHasta, "id", "descrip_codi", "S")
		clsWeb.gCargarCombo(mstrConn, "rg_servi_tipos_cargar", cmbTipoServ, "id", "descrip", "T")
		clsWeb.gCargarCombo(mstrConn, "cmb_si_no_cargar", cmbConError, "id", "descrip", "T")


	End Sub

	Private Sub mLimpiarFiltros()
		cmbRazaDesde.Limpiar()
		cmbRazaHasta.Limpiar()
		cmbProceso.Limpiar()
		txtCodiDesde.Text = ""
		txtCodiHasta.Text = ""
		txtFechaDenunDesde.Text = ""
		txtFechaDenunHasta.Text = ""
		txtFechaDesde.Text = ""
		txtFechaHasta.Text = ""
		txtNumeDesdeVaca.Text = ""
		txtNumeHastaVaca.Text = ""
		txtNumeDesdeToro.Text = ""
		txtNumeHastaToro.Text = ""
		cmbTipoServ.Limpiar()
		cmbConError.Limpiar()

	End Sub
#End Region

	Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		Try
			Dim lstrRptName As String
			Dim lintFecha As Integer
			Dim lstrAnio As String
			Dim lstrPerio As String
            Dim lintFDD, lintFDH, lintFD, lintFH As Integer

            Dim stRazaDescDesde As String = "", stRazaDescHasta As String = ""



			If cmbProceso.SelectedValue.ToString = "" Then
				Throw New AccesoBD.clsErrNeg("Debe indicar Proceso.")
			End If

			If cmbProceso.SelectedValue.ToString = "1" Then
                lstrRptName = "EstadisticaNacimientos"
                If cmbRazaDesde.Valor <> 0 Then
                    stRazaDescDesde = cmbRazaDesde.SelectedItem.ToString().Trim()
                End If
                If cmbRazaHasta.Valor <> 0 Then
                    stRazaDescHasta = cmbRazaHasta.SelectedItem.ToString().Trim()

                End If



            ElseIf cmbProceso.SelectedValue.ToString = "2" Then
                lstrRptName = "EstadisticaServicios"
            ElseIf cmbProceso.SelectedValue.ToString = "5" Then
                lstrRptName = "EstadisticaProductos"
            ElseIf cmbProceso.SelectedValue.ToString = "9" Then
                lstrRptName = "EstadisticaProductos"
            ElseIf cmbProceso.SelectedValue.ToString = "10" Then
                lstrRptName = "EstadisticaProductos"
            ElseIf cmbProceso.SelectedValue.ToString = "11" Then
                lstrRptName = "EstadisticaProductos"
            ElseIf cmbProceso.SelectedValue.ToString = "12" Then
                lstrRptName = "EstadisticaEmbriones"
            ElseIf cmbProceso.SelectedValue.ToString = "13" Then
                lstrRptName = "EstadisticaEmbriones"
            ElseIf cmbProceso.SelectedValue.ToString = "14" Then
                lstrRptName = "EstadisticaEmbriones"
            ElseIf cmbProceso.SelectedValue.ToString = "15" Then
                lstrRptName = "EstadisticaProductos"
            ElseIf cmbProceso.SelectedValue.ToString = "16" Then
                lstrRptName = "EstadisticaProductos"
            End If


            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&cria_raza_id_desde=" & IIf(cmbRazaDesde.Valor.ToString = "", 0, cmbRazaDesde.Valor)
            lstrRpt += "&cria_raza_id_hasta=" & IIf(cmbRazaHasta.Valor.ToString = "", 0, cmbRazaHasta.Valor)

            lstrRpt += "&cria_nume_desde=" & IIf(txtCodiDesde.Text = "", 0, txtCodiDesde.Text)
            lstrRpt += "&cria_nume_hasta=" & IIf(txtCodiHasta.Text = "", 0, txtCodiHasta.Text)

            If txtFechaDenunDesde.Text = "" Then
                lintFDD = 0
            Else
                'gsz 2015/06/04 se ajusto para que se puedan poner fecha porque cancelaba
                lintFDD = CType(clsFormatear.gFormatFechaString(txtFechaDenunDesde.Text, "Int32"), Integer) - 2

            End If
            If txtFechaDenunHasta.Text = "" Then
                lintFDH = 0
            Else
                'gsz 2015/06/04 se ajusto para que se puedan poner fecha porque cancelaba
                lintFDH = CType(clsFormatear.gFormatFechaString(txtFechaDenunHasta.Text, "Int32"), Integer) - 2
            End If

            If txtFechaDesde.Text = "" Then
                lintFD = 0
            Else
                lintFD = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer) - 2
            End If
            If txtFechaHasta.Text = "" Then
                lintFH = 0
            Else
                lintFH = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer) - 2
            End If

            lstrRpt += "&fecha_denun_desde=" & lintFDD
            lstrRpt += "&fecha_denun_hasta=" & lintFDH

            lstrRpt += "&fecha_desde=" & lintFD
            lstrRpt += "&fecha_hasta=" & lintFH

            lstrRpt += "&padre_nume_desde=" & IIf(txtNumeDesdeToro.Text = "", 0, txtNumeDesdeToro.Text)
            lstrRpt += "&padre_nume_hasta=" & IIf(txtNumeHastaToro.Text = "", 0, txtNumeHastaToro.Text)

            lstrRpt += "&madre_nume_desde=" & IIf(txtNumeDesdeVaca.Text = "", 0, txtNumeDesdeVaca.Text)
            lstrRpt += "&madre_nume_hasta=" & IIf(txtNumeHastaVaca.Text = "", 0, txtNumeHastaVaca.Text)

            lstrRpt += "&tipo_servi=" & IIf(cmbTipoServ.SelectedItem.Text = "(Todos)", "0", cmbTipoServ.Valor)
            lstrRpt += "&conError=" & IIf(cmbConError.SelectedItem.Text = "(Todos)", "T", cmbConError.Valor)
            If Not cmbProceso.SelectedValue.ToString = "1" And Not cmbProceso.SelectedValue.ToString = "2" Then
                lstrRpt += "&trans_id=" & cmbProceso.SelectedValue.ToString
            End If



            If cmbProceso.SelectedValue.ToString = "1" Then

                If stRazaDescDesde <> "" Then
                    lstrRpt += "&stRazaDescDesde=" & stRazaDescDesde
                End If
                If stRazaDescHasta <> "" Then
                    lstrRpt += "&stRazaDescHasta=" & stRazaDescHasta
                End If
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltros()
	End Sub
End Class
End Namespace
