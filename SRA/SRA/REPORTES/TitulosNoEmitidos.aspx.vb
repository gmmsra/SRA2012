Namespace SRA

Partial Class TitulosNoEmitidos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "Titulos"

   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Inicializar"
   Public Sub mInicializar()
      usrAlumFil.FilInseId = Convert.ToInt32(Request.QueryString("fkvalor"))
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mCargarCarreras()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCarreras()
      clsWeb.gCargarCombo(mstrConn, "carreras_cargar @carr_inse_id=" + Request.QueryString("fkvalor"), cmbCarr, "id", "descrip", "S")
   End Sub
#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "Titulos"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         Dim lintFecha As Integer

         If txtFechaHastaFil.Text = "" Then
            lintFecha = 0
         Else
            lintFecha = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&instituto=" + Request.QueryString("fkvalor")
         lstrRpt += "&alum=" + usrAlumFil.Valor.ToString
         lstrRpt += "&carr=" + cmbCarr.Valor.ToString
         lstrRpt += "&fechaHta=" + lintFecha.ToString


         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class

End Namespace
