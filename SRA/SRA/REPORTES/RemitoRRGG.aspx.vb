Namespace SRA

Partial Class RemitoRRGG
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblActividad As System.Web.UI.WebControls.Label
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label
   Protected WithEvents Label2 As System.Web.UI.WebControls.Label



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      Dim mdsDatos As New DataSet
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String
         Dim lintFechaD, lintFechaH As Integer

         lstrRptName = "RemitosRRGG"


         If txtFechaD.Fecha.ToString = "" Then
            lintFechaD = 0
         Else
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaD.Text, "Int32"), Integer)
         End If

         If txtFechaH.Fecha.ToString = "" Then
            lintFechaH = 0
         Else
            lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaH.Text, "Int32"), Integer)
         End If
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If (Not chkFact.Checked And Not chkProf.Checked And Not chkDemo.Checked) Then
                Throw New AccesoBD.clsErrNeg("Debe tildar al menos una opci�n.")
            End If


            Dim lstrTitulo As String
            If chkFact.Checked Then lstrTitulo = "Facturados"
            If chkDemo.Checked Then
                If lstrTitulo <> "" Then lstrTitulo = lstrTitulo + "/ "
                lstrTitulo = lstrTitulo + "Demorados"
            End If
            If chkProf.Checked Then
                If lstrTitulo <> "" Then lstrTitulo = lstrTitulo + "/ "
                lstrTitulo = lstrTitulo + "Pendientes"
            End If

            lstrRpt += "&fechaD=" + lintFechaD.ToString
            lstrRpt += "&fechaH=" + lintFechaH.ToString
            lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
            lstrRpt += "&comp_emct_id=" + IIf(cmbCentroEmisor.Valor.ToString = "", "0", cmbCentroEmisor.Valor.ToString)
            lstrRpt += "&fac=" + IIf(chkFact.Checked, "1", "0")
            lstrRpt += "&dem=" + IIf(chkDemo.Checked, "1", "0")
            lstrRpt += "&pen=" + IIf(chkProf.Checked, "1", "0")
            lstrRpt += "&titulo=" + lstrTitulo
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
