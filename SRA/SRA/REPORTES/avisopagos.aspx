<%@ Reference Control="~/controles/usrsociosfiltro.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AvisoPagos" CodeFile="AvisoPagos.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="../controles/usrSociosFiltro.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Aviso de Pagos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
		function mSetearTipo()
		{
			if (document.all('cmbRepo')!=null)
			{
				document.all('txtDeudaAl').disabled = (document.all('cmbRepo').value!='C');
				document.all('txtVenc').disabled = (document.all('cmbRepo').value!='C');
				document.all('chkInclInte').disabled = (document.all('cmbRepo').value!='C');

				if (document.all('cmbRepo').value=='E')
				{
					document.all('panDire').style.display='';
				}
				else
				{
					document.all('panDire').style.display='none';		
				}
				if (document.all('cmbRepo').value=='C')
				{
					document.all('cmbTexto').style.display='';
					document.all('lblTexto').style.display='';
				}
				else
				{
					document.all('cmbTexto').style.display='none';
					document.all('lblTexto').style.display='none';
				}
			}
		}
		function mRptFiltrosLimpiar()
		{
			EjecutarMetodoXML("Utiles.RptFiltrosLimpiar",document.all("hdnRptId").value);
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="javascript:mRptFiltrosLimpiar();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Avisos de Pago</asp:label></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
										BorderStyle="Solid">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
															<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" noWrap
																			align="right">
																			<asp:label id="lblRepo" runat="server" cssclass="titulo">Tipo Reporte:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<TABLE id="Table2" border="0" cellSpacing="0" cellPadding="0">
																				<TR>
																					<TD>
																						<cc1:combobox id="cmbRepo" class="combo" runat="server" Width="119px" onchange="javascript:mSetearTipo();">
																							<asp:ListItem Value="C" Selected="True">Cupones</asp:ListItem>
																							<asp:ListItem Value="E">Etiquetas</asp:ListItem>
																							<asp:ListItem Value="P">Totales por Provincia</asp:ListItem>
																						</cc1:combobox>&nbsp;
																					</TD>
																					<TD>
																						<DIV id="panDire" noWrap>
																							<asp:checkbox id="chkDire" Text="Con Direcci�n" Runat="server" CssClass="titulo"></asp:checkbox></DIV>
																					</TD>
																					<TD>
																						<asp:label id="lblTexto" runat="server" cssclass="titulo">Texto: </asp:label></TD>
																					<TD>
																						<cc1:combobox id="cmbTexto" class="combo" runat="server" Width="180px"></cc1:combobox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" noWrap
																			align="right">
																			<asp:label id="lblDeudaAl" runat="server" cssclass="titulo">Deuda al:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtDeudaAl" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																			<asp:CheckBox id="chkInclInte" Text="Incluye interes flotante" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" noWrap
																			align="right">
																			<asp:label id="lblVenc" runat="server" cssclass="titulo">Fecha Emisi�n:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtVenc" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" colSpan="2"
																			align="left">
																			<UC1:SOCFIL id="usrSocFil" runat="server"></UC1:SOCFIL></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
														<TR>
															<TD height="10" colSpan="3"></TD>
														</TR>
														<TR>
															<TD colSpan="2" align="right">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
																	ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																	IncludesUrl="../includes/" ImagesUrl="../imagenes/" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																	ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																	IncludesUrl="../includes/" ImagesUrl="../imagenes/" ImageUrl="imagenes/limpiar.jpg" CausesValidation="False"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="middle" colSpan="3"></TD>
							</TR>
						</TABLE>
					</td>
					<!--- FIN CONTENIDO ---> </TD>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnRptId" runat="server" Width="140px"></ASP:TEXTBOX>
			</DIV>
		</form>
		<script language="JavaScript">
			mSetearTipo();
		</script>
	</BODY>
</HTML>
