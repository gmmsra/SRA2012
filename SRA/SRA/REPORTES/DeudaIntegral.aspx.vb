Namespace SRA

Partial Class DeudaIntegral
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mEstablecerPerfil()

            clsWeb.gInicializarControles(sender, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mEstablecerPerfil()
   End Sub
#End Region

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      Try
         mLimpiarFiltros()
      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub mListar()
      Dim lstrRptName As String = "DeudaIntegral"
      Dim lintFechaDesde As Integer
      Dim lintFechaHasta As Integer

      mValidarDatos()

      If txtFechaHastaFil.Text <> "" Then
         lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
      End If

      Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

      lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
      lstrRpt += "&fecha_hasta=" + lintFechaHasta.ToString
        lstrRpt += "&InclInteres=" + Math.Abs(CInt(chkInclInteres.Checked)).ToString
        lstrRpt += "&tipo=" + cmbTipo.Valor.ToString

      lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
      Response.Redirect(lstrRpt)
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      mListar()
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Sub mLimpiarFiltros()
      usrClieFil.Limpiar()
      txtFechaHastaFil.Text = ""
   End Sub
End Class
End Namespace
