' Dario 2013-10-03 se coloca filtro en la carga del combo "tarjetas", cmbTarj
Namespace SRA

Partial Class PagosTarjeta
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtCuotas As NixorControls.TextBoxTab
    Protected WithEvents lblCuotas As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                txtFechaDesdeFil.Fecha = Now
                txtFechaHastaFil.Fecha = Now
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
        ' Dario 2013-10-03 se coloca filtro en la carga del combo "tarjetas", cmbTarj
        'clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S", "@varTodos=N")
        '    
        clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMoneda, "T")
    End Sub
#End Region

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer

            lstrRptName = "PagosTarjeta"

            If txtFechaDesdeFil.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaHastaFil.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)


            lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&emct_id=" + IIf(cmbCentroEmisor.Valor.ToString = "", "0", cmbCentroEmisor.Valor.ToString)
            lstrRpt += "&tarj_id=" + IIf(cmbTarj.Valor.ToString = "", "0", cmbTarj.Valor.ToString)
            lstrRpt += "&paco_tarj_cuot=" + IIf(txtCuota.Text = "", "0", txtCuota.Text)
            lstrRpt += "&mone_id=" + cmbMoneda.Valor.ToString
            lstrRpt += "&moneda=" + IIf(cmbMoneda.Valor.ToString = "", "Todos", cmbMoneda.SelectedItem.ToString)
            lstrRpt += "&cuota=" + IIf(txtCuota.Text = "", "Todos", txtCuota.Text)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
