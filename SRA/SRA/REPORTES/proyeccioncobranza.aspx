<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ProyeccionCobranza" CodeFile="ProyeccionCobranza.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Proyecci�n de Cobranzas</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 8px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Proyecci�n de Cobranzas</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 8px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="97%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 6.22%; HEIGHT: 15px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																				<TD style="WIDTH: 17.3%; HEIGHT: 15px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 6.22%; HEIGHT: 21px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo" Width="87px">Per�odo Vto:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 19.72%; HEIGHT: 30px" noWrap background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="True"></cc1:DateBox>&nbsp;
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 6.22%; HEIGHT: 17px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblClie" runat="server" cssclass="titulo" Width="106px">Cliente:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 19.72%; HEIGHT: 17px" noWrap background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrClie" runat="server" AceptaNull="False" Tabla="Clientes" Saltos="1,1,1" FilDocuNume="True"
																						FilCUIT="True" FilSociNume="True" PermiModi="True" MuestraDesc="False" SoloBusq="true"
																						width="100%"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 6.22%; HEIGHT: 17px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblActi" runat="server" cssclass="titulo" Width="87px">Actividad:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 19.72%; HEIGHT: 17px" noWrap align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbActi" runat="server" Width="240px" ImagesUrl="../Images/" NomOper="emisores_ctros_cargar"
																						AceptaNull="false"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 6.22%; HEIGHT: 8px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="8"></TD>
																				<TD style="WIDTH: 2.84%; HEIGHT: 8px" vAlign="middle" align="left" background="../imagenes/formfdofields.jpg"
																					height="8">
																					<asp:CheckBox id="chkResu" Runat="server" CssClass="titulo" Text="Resumido"></asp:CheckBox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 6.22%; HEIGHT: 8px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="8"></TD>
																				<TD style="WIDTH: 2.84%; HEIGHT: 8px" vAlign="middle" align="left" background="../imagenes/formfdofields.jpg"
																					height="8">
																					<asp:CheckBox id="chkClie" Runat="server" CssClass="titulo" Text="Incluir Datos del Cliente"></asp:CheckBox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 6.22%; HEIGHT: 15px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																				<TD style="WIDTH: 17.3%; HEIGHT: 15px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 6.22%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 158px" width="158" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
