Namespace SRA

Partial Class PadronAsamblea
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hdnGrupo As System.Web.UI.WebControls.TextBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mCargarCombos()
                Me.txtFecha.Fecha() = Now
            End If

            chkInte.Enabled = chkDeuda.Checked
            If Not chkInte.Enabled Then
                chkInte.Checked = False
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarGrupos()
        If cmbAsamFil.Valor.GetType.ToString <> "" Then
            'clsWeb.gCargarRefeCmb(mstrConn, "grupos_por_asambleas", cmbGrupo, "T", "@AsamId=" + cmbAsamFil.Valor.ToString)
            clsWeb.gCargarRefeCmb(mstrConn, "grupos_por_asambleas", cmbGrupo, "", "@AsamId=" + cmbAsamFil.Valor.ToString)
        End If
        'cmbGrupo.Items(0).Value = "-1"
        If Not Request.Form("cmbGrupo") Is Nothing Then
            cmbGrupo.Valor = Request.Form("cmbGrupo").ToString
        End If
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsamFil, "S", "")
        mCargarGrupos()
        clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDistDesde, "T", "")
        clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDistHasta, "T", "")
    End Sub

    Private Sub mLimpiarFiltros()
        cmbAsamFil.Limpiar()
        Me.cmbListar.SelectedValue = "R"
        Me.cmbAsamFil.Limpiar()
        If cmbAsamFil.Valor.GetType.ToString <> "" Then
            clsWeb.gCargarRefeCmb(mstrConn, "grupos_por_asambleas", cmbGrupo, "", "@AsamId=" + cmbAsamFil.Valor.ToString)
        Else
            Me.cmbGrupo.Items.Clear()
        End If
        Me.cmbDistDesde.Limpiar()
        Me.cmbDistHasta.Limpiar()
        Me.chkTelefono.Checked = False
        Me.chkTelefono.Enabled = True
        Me.chkCorreoE.Checked = False
        Me.chkCorreoE.Enabled = True
        Me.chkDeuda.Checked = False
        Me.chkDeuda.Enabled = True
        Me.rbtnOrdeVota.Checked = True
        Me.rbtnOrdeVota.Enabled = True
        Me.rbtnDistOrdeVota.Checked = False
        Me.rbtnDistOrdeVota.Enabled = True
        Me.rbtnSinDire.Checked = False
        Me.rbtnSinDire.Enabled = False
        Me.rbtnConDire.Checked = True
        Me.rbtnConDire.Enabled = False
        Me.txtFecha.Fecha() = Now
    End Sub

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub mListar()
        Try
            Dim lintFecha As Integer
            Dim lstrDevuelve As String

            Session("sGrupo") = cmbGrupo.Valor.ToString()

            mCargarGrupos()

            hdnImprimio.Text = "0"
            If cmbAsamFil.Valor = 0 Then
                mLimpiarFiltros()
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la Asamblea.")
            End If
            If cmbListar.Valor.ToString = "R" And txtFecha.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha del reporte.")
            End If


            lstrDevuelve = IIf(hdnOrdenar.Text = "1", IIf(chkDeuda.Checked, "OD", "ON"), "D")

            Dim lstrRptName As String
            If Me.cmbListar.Valor.ToString = "R" Then   'Todos
                lstrRptName = "PadronAsambleas"
            ElseIf Me.cmbListar.Valor.ToString = "E" Then    'Etiquetas
                lstrRptName = "Etiquetas"
            ElseIf Me.cmbListar.Valor.ToString = "S" Then    'Sobres
                lstrRptName = "PadronAsambleaSobres"
            ElseIf Me.cmbListar.Valor.ToString = "T" Then   'Totales por pais
                lstrRptName = "PadronAsambleaPorProvincia"
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If Me.cmbListar.Valor.ToString <> "E" Then
                If Me.cmbListar.Valor.ToString <> "T" Then
                    lstrRpt += "&AsamId=" + cmbAsamFil.Valor.ToString
                    lstrRpt += "&Grupo=" + cmbGrupo.Valor.ToString
                    lstrRpt += "&DistDesde=" + cmbDistDesde.Valor.ToString
                    lstrRpt += "&DistHasta=" + cmbDistHasta.Valor.ToString
                    If cmbListar.Valor.ToString = "S" Then
                        lstrRpt += "&Devuelve=E"
                        lstrRpt += "&reporte=" + IIf(hdnOrdenar.Text = "1", IIf(chkDeuda.Checked, "OD", "ON"), "D")
                        lstrRpt += "&ConDireccion=" + IIf(hdnConDire.Text = "1", "True", "False")
                    Else
                        lstrRpt += "&Devuelve=" + IIf(hdnOrdenar.Text = "1", IIf(chkDeuda.Checked, "OD", "ON"), "D")
                        If chkInte.Checked Then lstrRpt += "I"
                        lstrRpt += "&ConTelefono=" + chkTelefono.Checked.ToString
                        lstrRpt += "&ConDeuda=" + chkDeuda.Checked.ToString
                        lstrRpt += "&ConCorreoE=" + chkCorreoE.Checked.ToString
                    End If
                Else
                    lstrRpt += "&Asam_Id=" + cmbAsamFil.Valor.ToString
                    lstrRpt += "&Grupo=" + cmbGrupo.Valor.ToString
                    lstrRpt += "&DistDesde=" + cmbDistDesde.Valor.ToString
                    lstrRpt += "&DistHasta=" + cmbDistHasta.Valor.ToString
                End If
            End If

            If Me.cmbListar.Valor.ToString = "R" Then
                lstrRpt += "&fecha=" + txtFecha.Text.ToString
            End If

            If cmbListar.Valor.ToString = "E" Then
                lstrRpt += SRA_Neg.Utiles.ParametrosEtiquetas("PA", "0", IIf(hdnConDire.Text = "1", "True", "False"), cmbAsamFil.Valor.ToString, cmbGrupo.Valor.ToString, cmbDistDesde.Valor.ToString, cmbDistHasta.Valor.ToString, "E", , , , , , , , , , , , , , , , , , , , , "C")
                lstrRpt += "&reporte=" + lstrDevuelve
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
        ''mListar()
    End Sub

    Private Sub cmbAsamFil_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAsamFil.SelectedIndexChanged
        mCargarGrupos()
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub

#End Region

End Class
End Namespace
