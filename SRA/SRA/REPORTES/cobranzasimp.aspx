<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>  
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CobranzasImp" CodeFile="CobranzasImp.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD> 
		<title>Impresi�n de Comprobantes</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript" src="../includes/impresion.js"></script>
		<script language="javascript">
		function ImprimirSeleccion()
		{
			document.all('hdnImprSele').value = "S";
			__doPostBack('hdnImprSele',''); 
		}
		function mSeleccionar(pSele)
		{
			if (document.all("grdDatoBusq").children(0).children.length > 1)
			{
				for (var fila=1; fila <= document.all("grdDatoBusq").children(0).children.length-1; fila++)
				{
					document.all("grdDatoBusq").children(0).children(fila).children(0).children(0).checked = pSele;
				}
			}
		}
		function Imprimir()
		{
			var comprob="";
			var compId="";
			var compIds="";
			var boolOriginal="";
			var compImpreso="";
			var compAnulado="";
			var lstrRpt = "";
			var valParam="";
			
			//Recibo, Transferencia, Aplicaci�n de Cr�ditos.
			if(document.all("cmbCoti").value == "30" || document.all("cmbCoti").value == "13" || document.all("cmbCoti").value == "34")
				lstrRpt = "Recibo"

			if(document.all("cmbCoti").value == "33")
				lstrRpt = "Acuse"

			if(document.all("cmbCoti").value == "29"||document.all("cmbCoti").value == "31"||document.all("cmbCoti").value == "32")	
				lstrRpt = "Factura"

			if(document.all("cmbCoti").value == "28")
				lstrRpt = "Proforma"
		    
			var strComp = document.all('hdnIds').value.split(";");

			//if (document.all("grdDatoBusq").children(0).children.length > 1)
			if (document.all('hdnIds').value != '')
			{
				try
				{
					//for (var fila=1; fila <= document.all("grdDatoBusq").children(0).children.length-1; fila++)
					for (var fila=0; fila <= strComp.length-1; fila++)
					{
						//if (document.all("grdDatoBusq").children(0).children(fila).children(0).children(0).checked)
						//{
							//compId = document.all("grdDatoBusq").children(0).children(fila).children(0).children(1).innerText;
							comprob = strComp[fila].split(",");
							compId = comprob[0]
							boolOriginal = comprob[1]
							compAnulado = comprob[2]

							//boolOriginal = document.all("grdDatoBusq__ctl" + (fila+2).toString() + "_chkOriginal").checked;
							//compAnulado = document.all("grdDatoBusq").children(0).children(fila).children(1).children(1).innerText;
							//compImpreso = document.all("grdDatoBusq").children(0).children(fila).children(1).children(2).innerText;
													
							var Estado="Original";
						    
							if (compAnulado==1) Estado="Anulado";
							else if (boolOriginal==0) Estado="Duplicado";

							//if(compImpreso=="False")
						    //{
								//if(compIds!="") compIds = compIds + ",";
								//compIds = compIds + compId;
						    //}

							valParam=compId + ";0;" + Estado;

							var sRet = ImprimirReporte(lstrRpt,"comp_id;random;estado", valParam, "<%=Session("sImpreTipo")%>",compId,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							
							if(sRet=="0") 
							{
								document.all("hdnImprId").value="";
								return;
							}
							
							if (compImpreso=="False" && Estado=="Original" && lstrRpt=="Factura")
							{
								valParam=compId + ";0;Duplicado";
								var sRet = ImprimirReporte(lstrRpt,"comp_id;random;estado", valParam, "<%=Session("sImpreTipo")%>",compId,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
								if(sRet=="0") 
								{
									document.all("hdnImprId").value="";
									return;
								}
							}
							
							if (lstrRpt=="Recibo")
							{
								ImprimirTalones(compId, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							}
						//}
					}
				}
				catch(e)
				{
					document.all("hdnImprId").value="";
					alert("Error al intentar efectuar la impresi�n");
				}	
			}
			else
			{
				alert('Debe seleccionar los comprobantes a imprimir.');
			}
							
			document.all("hdnImprId").value="";
			document.all('hdnImprSele').value = "";

			compIds = document.all('hdnIdsImpr').value;
			if (compIds!="")
			{
			    if (window.confirm("�Se imprimieron los comprobantes correctamente?"))
				{
					document.all("hdnImprimio").value=compIds;
					__doPostBack('hdnImprimio','');				
				}
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tbody>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="391px">Impresi�n de Comprobantes</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2">
										<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
											BorderStyle="Solid">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD style="WIDTH: 100%">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px" align=right width="100%">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="../imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="100%">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="../imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
																
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" width="100%" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" background="../imagenes/formfdofields.jpg"
																		border="0">
																		<TR height="10">
																			<TD colspan="3"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%" valign=top align="right" background="../imagenes/formfdofields.jpg">
																				<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<UC1:CLIE id="usrClie" runat="server" Autopostback="false" Tabla="Clientes" AceptaNull="False"
																					FilSociNume="True" MuestraDesc="False" Ancho="800" Saltos="1,1,1,1"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%" valign=top align="right" background="../imagenes/formfdofields.jpg">
																				<asp:Label id="lblCtro" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:combobox class="cmbCtro" AceptaNull="False" id="cmbCtro" runat="server" cssclass="cuadrotexto" Width="250px" 
																					MostrarBotones="False" filtra="false" NomOper="emisores_ctros_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;&nbsp;
																				<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;
																				<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblNroComp" runat="server" cssclass="titulo">Letra:</asp:Label>&nbsp;</TD>
																			<TD colspan="2">
																				<cc1:combobox class="combo" id="cmbCompLetra" runat="server" Width="70px">
																					<asp:listitem value="" selected="true">(Todos)</asp:listitem>
																					<asp:listitem value="A">A</asp:listitem>
																					<asp:listitem value="B">B</asp:listitem>
																				</cc1:combobox>&nbsp;&nbsp;
																				<asp:Label id="lblCompCemiNum" visible=false runat="server" cssclass="titulo">Centro Emisor:</asp:Label>
																				<CC1:NUMBERBOX id="txtCompCemiNum" visible=false MaxValor="9999" MaxLength="4" runat="server" cssclass="cuadrotexto" Width="48px"></CC1:NUMBERBOX>&nbsp;
																				<asp:Label id="lblCompNum" runat="server" cssclass="titulo">N�:</asp:Label>
																				<CC1:NUMBERBOX id="txtCompNum" MaxValor="99999999" MaxLength="8" runat="server" cssclass="cuadrotexto" Width="120px"></CC1:NUMBERBOX>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblCoti" runat="server" cssclass="titulo">Tipo:</asp:label>&nbsp;</TD>
																			<TD colspan="2">
																				<cc1:combobox class="combo" id="cmbCoti" runat="server" Width="250px" autopostback="true"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD></td>
																			<TD align="left" colSpan="2">
																				<asp:CheckBox id="chkTodas" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small"
																					Text="Incluir Impresos"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD></td>
																			<TD align="left" colSpan="2">
																				<asp:CheckBox id="chkIncAnulados" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small"
																					Text="Incluir Anulados"></asp:CheckBox></TD>
																		</TR>
																		<TR height="10">
																			<TD colspan="3"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 1.81%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 12.69%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="../imagenes/formde.jpg" height="10"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
															<TR>
																<TD width="3" height="10"></TD>
																<TD align="right" height="10"></TD>
																<TD width="2" height="10"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE> <!---fin filtro --->
										</asp:panel>
									</TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2">
										<asp:datagrid id="grdDatoBusq" runat="server" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
											AutoGenerateColumns="False" width="100%" PageSize="15">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
											<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
											<FooterStyle cssclass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderStyle-Width="2%">
													<ItemTemplate>
														<asp:CheckBox ID="chkSel" Checked=<%#DataBinder.Eval(Container, "DataItem.chk")%> Runat="server"></asp:CheckBox>
														<div style="display:none"><%#DataBinder.Eval(Container, "DataItem.comp_id")%></div>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderStyle-Width="2%" HeaderText="Original" ItemStyle-HorizontalAlign=Center>
													<ItemTemplate>
														<asp:CheckBox ID="chkOriginal" Runat="server" Enabled=<%#DataBinder.Eval(Container, "DataItem.habilitar")%>
															Checked=<%#DataBinder.Eval(Container, "DataItem.original")%>></asp:CheckBox>
														<div style="display:none"><%#DataBinder.Eval(Container, "DataItem.anulado")%></div>
														<div style="display:none"><%#DataBinder.Eval(Container, "DataItem.comp_impre")%></div>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="comp_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="comp_ingr_fecha" ReadOnly="True" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"
													HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="coti_desc" ReadOnly="True" HeaderText="Tipo Comp." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="numero" ReadOnly="True" HeaderText="N�mero" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
													<HeaderStyle Width="17%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="soci_nume" ReadOnly="True" HeaderText="Socio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="cliente" ReadOnly="True" HeaderText="Cliente">
													<HeaderStyle Width="40%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="comp_neto" ReadOnly="True" HeaderText="Importe" DataFormatString="{0:F2}"
																HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid>
									</TD>
								</TR>
								<TR>
									<TD vAlign="middle" align="right" colSpan="3">
										<table border=0 width="100%">
										<tr>
										<td width=90>
											<asp:Button id="btnTodos" runat="server" cssclass="boton" Width="80px" Text="Todos"></asp:Button>														
										</td>
										<td width=90>
											<asp:Button id="btnNinguno" runat="server" cssclass="boton" Width="80px" Text="Ninguno" CausesValidation="False"></asp:Button>
										</td>
										<td align=right>
												<CC1:BotonImagen id="btnEnviarPorMail" runat="server" BorderStyle="None" ImageUrl="../imagenes/mail-all.png"
													BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="mail-all.png" ImageOver="mail-all.png" ForeColor="Transparent"></CC1:BotonImagen>
											<a runat="server" id="btnAceptar" href="javascript:ImprimirSeleccion();"><img src="../imagenes/btnImpr.gif" alt="Imprimir" border="0" /></a>
										</td>
										</tr>
										</table>
									</TD>
								</TR>
							</TABLE><!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</tbody>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<asp:textbox id="hdnIds" runat="server"></asp:textbox>
				<asp:textbox id="hdnIdsImpr" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprSele" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>				
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="hdnImprId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnImprimio" AutoPostBack="True" runat="server"></ASP:TEXTBOX>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["grdDatoBusq"]!= null)
			document.location='#grdDatoBusq';
		if (document.all('hdnImprSele').value == "S")
		{
			document.all('hdnImprSele').value = "";
			Imprimir();
		}
		</SCRIPT>
	</BODY>
</HTML>
