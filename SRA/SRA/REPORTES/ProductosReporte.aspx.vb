Imports Entities


Namespace SRA


Partial Class ProductosReporte
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblCriaNumeHasta As System.Web.UI.WebControls.Label
    Protected WithEvents cmbSexoFil As NixorControls.ComboBox
    Protected WithEvents lblSraNumeDesde As System.Web.UI.WebControls.Label
    Protected WithEvents txtSraNumeDesde As NixorControls.TextBoxTab
    Protected WithEvents lblRegiTipoFil As System.Web.UI.WebControls.Label
    Protected WithEvents cmbRegiTipoFil As NixorControls.ComboBox
    Protected WithEvents lblSraNumeHasta As System.Web.UI.WebControls.Label
    Protected WithEvents txtSraNumeHasta As NixorControls.TextBoxTab


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            usrProdFil.VisibleAdicionales = False
            
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mListar()
        Try
            'GSZ 10-12-2014     Se agregaron parametros para el reporte
            Dim stRazaDescripcion As String, RazaId As String, CriadorId As String
            Dim booParm As Boolean
            Dim strCriador As String

            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
            Dim oClientes As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
            Dim oEspecies As New SRA_Neg.Especie(mstrConn, Session("sUserId").ToString())
            Dim oEstabl As New SRA_Neg.Establecimientos(mstrConn, Session("sUserId").ToString())
            Dim oProductos As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

            Dim lstrRptName As String = "ProductosReporte"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim oParametros As New productosFiltrosEntity

            Try
                If Not usrProdFil.ObtenerIdFiltro Is Nothing Then
                    hdnRptId.Text = usrProdFil.ObtenerIdFiltro.ToString
                End If
            Catch ex As Exception
                Throw New AccesoBD.clsErrNeg(ex.Message)
            End Try

            RazaId = usrProdFil.ObtenerRazaIdFiltro.ToString
            strCriador = usrProdFil.ObtenerCriadorFiltro.ToString
            CriadorId = usrProdFil.ObtenerCriadorIdFiltro.ToString
            booParm = usrProdFil.ObtenerParametros(oParametros)

            stRazaDescripcion = oRaza.GetRazaDescripcionById(IIf(RazaId = "", "0", RazaId)).Trim()

            lstrRpt += "&rprf_id=" + hdnRptId.Text.ToString
            lstrRpt += "&OrdenPor=" + cmbOrdenarPor.Valor

            lstrRpt += "&RazaDescrip=" & oRaza.GetRazaCodiById(RazaId).ToString + "-" + stRazaDescripcion

                lstrRpt += "&FiltroProp=" & strCriador.Replace("&", "and")
                lstrRpt += "&FiltroEstable=" & oEstabl.GetEstablecimientoByRazaIdCriadorId(RazaId, CriadorId)
            lstrRpt += "&FiltroHBAdesde=" + IIf(oParametros.rprf_sra_nume_desde.ToString = "", "0", oParametros.rprf_sra_nume_desde.ToString)
            lstrRpt += "&FiltroHBAhasta=" + IIf(oParametros.rprf_sra_nume_hasta.ToString = "", "0", oParametros.rprf_sra_nume_hasta.ToString)
            lstrRpt += "&FiltroRPDesde=" + IIf(oParametros.rprf_rp_desde.ToString = "", "0", oParametros.rprf_rp_desde.ToString)
            lstrRpt += "&FiltroRPHasta=" + IIf(oParametros.rprf_rp_hasta.ToString = "", "0", oParametros.rprf_rp_hasta.ToString)

                If oParametros.rprf_naci_fecha_desde = Date.MinValue And oParametros.rprf_naci_fecha_hasta = Date.MinValue Then
                    lstrRpt += "&FiltroNaciRangoFecha= Todas"
                Else
                    lstrRpt += "&FiltroNaciRangoFecha=" +
                            IIf(oParametros.rprf_naci_fecha_desde = Date.MinValue, "desde: Sin valor inicial", "desde:" + oParametros.rprf_naci_fecha_desde.Day().ToString() + "/" + oParametros.rprf_naci_fecha_desde.Month().ToString() + "/" + oParametros.rprf_naci_fecha_desde.Year().ToString()) + "-" +
                            IIf(oParametros.rprf_naci_fecha_hasta = Date.MinValue, "hasta: Sin valor final", "hasta:" + oParametros.rprf_naci_fecha_hasta.Day().ToString() + "/" + oParametros.rprf_naci_fecha_hasta.Month().ToString() + "/" + oParametros.rprf_naci_fecha_hasta.Year().ToString())
                End If

                If oParametros.rprf_insc_fecha_desde = Date.MinValue And oParametros.rprf_insc_fecha_hasta = Date.MinValue Then
                lstrRpt += "&FiltroinscRangoFecha= Todas"
            Else
                    lstrRpt += "&FiltroinscRangoFecha=" +
                    IIf(oParametros.rprf_insc_fecha_desde = Date.MinValue, "desde: Sin valor inicial", "desde:" + oParametros.rprf_insc_fecha_desde.Day().ToString() + "/" + oParametros.rprf_insc_fecha_desde.Month().ToString() + "/" + oParametros.rprf_insc_fecha_desde.Year().ToString()) + "-" +
                    IIf(oParametros.rprf_insc_fecha_hasta = Date.MinValue, "hasta: Sin valor final", "hasta:" + oParametros.rprf_insc_fecha_hasta.Day().ToString() + "/" + oParametros.rprf_insc_fecha_hasta.Month().ToString() + "/" + oParametros.rprf_insc_fecha_hasta.Year().ToString())
                End If

            lstrRpt += "&FiltroCria_id=" + oParametros.rprf_cria_id.ToString()
            lstrRpt += "&FiltroHBADesc=" + oEspecies.GetEspecieTipoHBADescByRazaId(RazaId)
            ' GSZ 12/12/2014 Se incorporaron datos del Padre y Madre a los Filtros

            If oParametros.HBAPadreNume <> "" Then
                lstrRpt += "&FiltroHBAPadreDesc=Padre :" + oEspecies.GetEspecieTipoHBADescByRazaId(RazaId) + oParametros.HBAPadreNume + "-" + _
                                         oProductos.GetProductoDescripById(oParametros.rprf_padre_prdt_id.ToString())
            Else
                lstrRpt += "&FiltroHBAPadreDesc=Padre :" + oEspecies.GetEspecieTipoHBADescByRazaId(RazaId) + " " + "Todos"
            End If

            If oParametros.RPPadreNume <> "" Then
                lstrRpt += "&FiltroRPPadreDesc=Padre RP:" + oParametros.RPPadreNume + " " + _
                oProductos.GetProductoDescripById(oParametros.rprf_padre_prdt_id.ToString())
            Else
                lstrRpt += "&FiltroRPPadreDesc=Padre :" + "RP Todos"
                End If

                If oParametros.HBAMadreNume <> "" Then
                    lstrRpt += "&FiltroHBAMadreDesc=Madre:" + oEspecies.GetEspecieTipoHBADescByRazaId(RazaId) + oParametros.HBAMadreNume + _
                           " " + oProductos.GetProductoDescripById(oParametros.rprf_madre_prdt_id.ToString())
                Else
                    lstrRpt += "&FiltroHBAMadreDesc=Madre :" + oEspecies.GetEspecieTipoHBADescByRazaId(RazaId) + " " + "Todos"
                End If

                If oParametros.RPMadreNume <> "" Then
                    lstrRpt += "&FiltroRPMadreDesc=Madre RP:" + oParametros.RPMadreNume + " " + _
                    oProductos.GetProductoDescripById(oParametros.rprf_madre_prdt_id.ToString())
                Else
                    lstrRpt += "&FiltroRPMadreDesc=Madre :" + " RP Todos"
                End If

                lstrRpt += "&incl_bajas=" + IIf(chkBaja.Checked, 1, 0).ToString()

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                clsError.gManejarError(New Exception(lstrRpt))
                Response.Redirect(lstrRpt)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub

    Private Sub btnLimpFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        usrProdFil.Limpiar()
    End Sub
End Class

End Namespace
