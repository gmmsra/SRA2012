
Imports ReglasValida.Validaciones
Imports SRA



Public Class ListadoEtiquetasCertifInscripcion
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    'Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cmbRaza As NixorControls.ComboBox
    'Protected WithEvents hdnRptId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnList As NixorControls.BotonImagen
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    'Protected WithEvents hdnId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnLimpFil As NixorControls.BotonImagen
    'Protected WithEvents lblFechaReporte As System.Web.UI.WebControls.Label

    Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbOrden As NixorControls.ComboBox
    'Protected WithEvents cmbSexo As NixorControls.ComboBox
    Protected WithEvents lblRPDde As System.Web.UI.WebControls.Label
    Protected WithEvents lblRP As System.Web.UI.WebControls.Label
    Protected WithEvents txtRPDde As NixorControls.NumberBox
    Protected WithEvents lblRPHta As System.Web.UI.WebControls.Label
    Protected WithEvents txtRPHta As NixorControls.NumberBox
    'Protected WithEvents lblSexo As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaDesde As NixorControls.DateBox
    'Protected WithEvents txtFechaHasta As NixorControls.DateBox
    'Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDesde As System.Web.UI.WebControls.Label
    'Protected WithEvents lblHasta As System.Web.UI.WebControls.Label
    Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label
    Protected WithEvents usrCriaFil As usrClieDeriv
    'Protected WithEvents lblRaza As System.Web.UI.WebControls.Label
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mCargarCombos()

            End If



        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()


        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
        SRA_Neg.Utiles.gSetearRaza(cmbRaza)

    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try


            Dim lstrRptName As String = "EtiquetasParaCertifInscripRaza"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            'Dim strEstablDescripcion As String = ""
            'Dim strOrden As String = ""


            lstrRpt += "&FechaDesde=" & IIf(txtFechaDesde.Fecha.ToString = "", "19800101", _
                    mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaDesde.Text))))


            lstrRpt += "&FechaHasta=" & IIf(txtFechaHasta.Fecha.ToString = "", "20300101", _
                    mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaHasta.Text))))




            If cmbSexo.Valor.ToString <> "" Then
                'hembra=0
                'macho= 1

                lstrRpt += "&sexo=" + IIf(cmbSexo.Valor = 0, "False", "True")
            End If




            lstrRpt += "&RazaId=" + clsSQLServer.gFormatArg(ValidarNulos(cmbRaza.Valor.ToString, True), SqlDbType.Int)

            If cmbOrden.Valor.ToString <> "" Then
                lstrRpt += "&Orden=" + cmbOrden.Valor.ToString
            End If



            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click

        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""
        cmbRaza.Limpiar()
        cmbOrden.Limpiar()
        cmbSexo.Limpiar()

    End Sub

    Private Sub txtFechaHasta_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class
