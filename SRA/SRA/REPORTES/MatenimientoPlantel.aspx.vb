Namespace SRA

Partial Class MatenimientoPlantel
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

	'El Dise�ador de Web Forms requiere esta llamada.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub

	'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
	'No se debe eliminar o mover.

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
		'No la modifique con el editor de c�digo.
		InitializeComponent()
	End Sub

#End Region

#Region "Definici�n de Variables"

	Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"

	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			If (Not Page.IsPostBack) Then
				mCargarCombos()
				'clsWeb.gInicializarControles(Me, mstrConn)
			End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub


	Private Sub mCargarCombos()
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
	End Sub
#End Region

#Region "Eventos Pagina"

	Private Sub mImprimir()
		Try
			Dim lintFechaD As Integer
			Dim lintFechaH As Integer

			If txtFechaDesde.Fecha.ToString = "" Then
				Throw New AccesoBD.clsErrNeg("Debe indicar Fecha de inicio de Periodo.")
			End If

			If txtFechaHasta.Fecha.ToString = "" Then
				Throw New AccesoBD.clsErrNeg("Debe indicar Fecha de fin de Periodo.")
			End If

			Dim lstrRptName As String = "MantenimientoPlantel"
			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			If txtFechaDesde.Fecha.ToString = "" Then
				lintFechaD = 0
			Else
				lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
			End If
			If txtFechaHasta.Fecha.ToString = "" Then
				lintFechaH = 0
			Else
				lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
			End If

			lstrRpt += "&raza_id=" & IIf(cmbRaza.SelectedValue.ToString = "", "0", cmbRaza.SelectedValue.ToString)
			lstrRpt += "&icluye_socio=" & IIf(chkIncluye.Checked, "True", "False")
			lstrRpt += "&fecha_desde=" & lintFechaD.ToString()
			lstrRpt += "&fecha_hasta=" & lintFechaH.ToString()


			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
		Try
			mImprimir()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click

		txtFechaDesde.Text = ""
		txtFechaHasta.Text = ""
		chkIncluye.Checked = False
	End Sub

#End Region

End Class
End Namespace
