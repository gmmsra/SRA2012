Imports System.Data.SqlClient


Namespace SRA


Partial Class Analitico
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "inscripciones"

   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String

   Private Enum Columnas As Integer
      InscId = 1
      FechaInsc = 2
      Ciclo = 3
      Periodo = 4
      Alumno = 5
      PlanPago = 6
      PlanPagoId = 7
      AlumnoId = 8
      PeriodoId = 9
      CicloId = 10

   End Enum

   
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()


         If (Not Page.IsPostBack) Then

            mEstablecerPerfil()

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub





   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      lblInstituto.Text = Convert.ToInt32(Request.QueryString("fkvalor"))
      lblInstituto.Text = Request.QueryString("fkvalor")
      usrAlumFil.FilInseId = lblInstituto.Text
   End Sub
#End Region

   Private Sub mImprimirAnalitico()
      Try
         Dim lstrRptName As String = "analitico"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&alum_id=" + Me.usrAlumFil.Valor.ToString()
         lstrRpt += "&carr_id=" + cmbCarre.Valor.ToString()

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mChequearDatos()

      If (usrAlumFil.Valor = 0) Then
         Throw New AccesoBD.clsErrNeg("Debe especificar el Alumno.")
      End If
      If (cmbCarre.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe especificar la carrera.")
      End If
   End Sub

   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         mChequearDatos()
         mImprimirAnalitico()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

End Class
End Namespace
