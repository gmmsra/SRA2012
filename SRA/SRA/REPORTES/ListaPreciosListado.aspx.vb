Namespace SRA

Partial Class ListaPreciosListado
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
        Private Sub mCargarCombos()
            Dim mdsDatos As New DataSet
            clsWeb.gCargarRefeCmb(mstrConn, "grupo_aranceles", cmbGrupoAran, "T")
            cmbGrupoAran.SelectedIndex = 0
            clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividadFil, "T")
            clsWeb.gCargarComboBool(Me.cmbListaBotFil, "T")

        End Sub
#End Region

        Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
            Try
                Dim lstrRptName As String
                Dim lintFecha As Integer
                Dim reca_tarj As Decimal
                Dim reca_ctac As Decimal
                Dim gene_inte As Decimal
                Dim inte_porc As Decimal
                Dim inte_monto As Decimal

                Dim dtCriador As New DataTable
                lstrRptName = "ListaPrecios"

                Dim mstrCmd As String
                dtCriador.TableName = "dtCriador"

                If txtFechaFil.Fecha.ToString = "" Then
                    lintFecha = 0
                Else
                    lintFecha = CType(clsFormatear.gFormatFechaString(txtFechaFil.Text, "Int32"), Integer)
                End If

                mstrCmd = "exec GetRecargos "
                mstrCmd = mstrCmd + " @peli_acti_id = " + IIf(cmbActividadFil.Valor.ToString = "", "null", cmbActividadFil.Valor.ToString)
                mstrCmd = mstrCmd + " ,@peli_vige_fecha = " + lintFecha.ToString
                mstrCmd = mstrCmd + ", @aran_grar_id = " + IIf(cmbGrupoAran.Valor.ToString = "", "null", cmbGrupoAran.Valor.ToString)
                mstrCmd = mstrCmd + ",@boti = " + IIf(cmbListaBotFil.SelectedValue = String.Empty, "null", clsSQLServer.gFormatArg(cmbListaBotFil.Valor.ToString, SqlDbType.Int))

                dtCriador = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

                If dtCriador.Rows.Count > 0 Then
                    reca_tarj = IIf(dtCriador.Rows(0).Item("peli_reca_tar") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("peli_reca_tar"))
                    reca_ctac = IIf(dtCriador.Rows(0).Item("peli_reca_ctac") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("peli_reca_ctac"))
                    gene_inte = IIf(dtCriador.Rows(0).Item("acti_gene_inte") Is System.DBNull.Value, 0, Convert.ToInt32(dtCriador.Rows(0).Item("acti_gene_inte")))
                    inte_porc = IIf(dtCriador.Rows(0).Item("acti_inte_porc") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("acti_inte_porc"))
                    inte_monto = IIf(dtCriador.Rows(0).Item("acti_inte_monto") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("acti_inte_monto"))
                End If

                'GSZ 03/11/2015 Se agrego parametros para mostrar en el pie del Listado
                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

                lstrRpt += "&peli_acti_id=" + IIf(cmbActividadFil.Valor.ToString = "", "null", cmbActividadFil.Valor.ToString)
                lstrRpt += "&peli_vige_fecha=" + lintFecha.ToString
                lstrRpt += "&aran_grar_id=" + IIf(cmbGrupoAran.Valor.ToString = "", "null", cmbGrupoAran.Valor.ToString)
                lstrRpt += "&boti=" + IIf(cmbListaBotFil.SelectedValue = String.Empty, "-1", clsSQLServer.gFormatArg(cmbListaBotFil.Valor.ToString, SqlDbType.Int))
                lstrRpt += "&reca_tarj=" + reca_tarj.ToString()
                lstrRpt += "&reca_ctac=" + reca_ctac.ToString()
                lstrRpt += "&gene_inte=" + gene_inte.ToString()
                lstrRpt += "&inte_porc=" + inte_porc.ToString()
                lstrRpt += "&inte_monto=" + inte_monto.ToString()

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                Response.Redirect(lstrRpt)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
