'Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports AccesoBD


Namespace SRA

'Imports Interfaces.Importacion


Partial Class MemoriaHereford
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            mstrConn = clsWeb.gVerificarConexion(Me)


            If Not Page.IsPostBack Then


            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

   
    Private Sub mValidarDatos()

        If (txtFechaDesde.Fecha Is DBNull.Value) Then

            Throw New AccesoBD.clsErrNeg("Debe especificar la fecha Inicial del Periodo.")

        End If
        If (txtFechaHasta.Fecha Is DBNull.Value) Then

            Throw New AccesoBD.clsErrNeg("Debe especificar la fecha Final del Periodo.")

        End If


    End Sub
    Private Sub mListar()
        Try

            Dim params As String
            Dim lstrRptName As String = "MemoriaHereford"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            Dim lintFechaD, lintFechaH As Integer


            mValidarDatos()


            If (txtFechaDesde.Fecha.ToString <> "") Then
                lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer) - 2
            Else
                lintFechaD = 0

            End If

            If (txtFechaHasta.Fecha.ToString <> "") Then
                lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer) - 2
            Else
                lintFechaH = 0

            End If



            params += "&dFechaDesde=" + lintFechaD.ToString
            params += "&dFechaHasta=" + lintFechaH.ToString

            params += "&strFechaDesde=" + txtFechaDesde.Fecha
            params += "&strFechaHasta=" + txtFechaHasta.Fecha


            lstrRpt += params

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click

       
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""


    End Sub
End Class

End Namespace
