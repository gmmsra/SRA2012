<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Socios_Adhesiones" CodeFile="Socios_Adhesiones.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Socios Adhesiones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Socios Adhesiones</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="2"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg" height="10"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD height="10"><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblDesc" runat="server" cssclass="titulo">Tarjetas:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbTarjetaFil" runat="server" Width="248px" nomoper="tarjetas_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblOrden" runat="server" cssclass="titulo">Ordenamiento:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbOrden" runat="server" Width="128px" nomoper="tarjetas_cargar">
																						<asp:ListItem Value="1">Nro Socio</asp:ListItem>
																						<asp:ListItem Value="2">Nombre Socio</asp:ListItem>
																						<asp:ListItem Value="3">Nro Tarjeta</asp:ListItem>
																						<asp:ListItem Value="0" Selected="True">Ninguno</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo de Reporte:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbTipo" runat="server" Width="128px">
																						<asp:ListItem Value="D" Selected="True">Detallado</asp:ListItem>
																						<asp:ListItem Value="R">Resumido</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrSociFil" runat="server" width="100%" SoloBusq="true" AceptaNull="False" MuestraDesc="False"
																						PermiModi="True" FilSociNume="True" FilCUIT="True" FilDocuNume="True" Saltos="1,1,1" Tabla="Socios"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg" height="10"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD colspan="3" height="10"></TD>
																</TR>
																<TR>
																	<TD colspan="3" align="right">
																		<CC1:BotonImagen id="btnImprimir" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
																	<TD width="2" height="10"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3">
											<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="left"></TD>
													<TD align="center" width="50"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
						</td>
					</tr>
				</TBODY>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
			</TR>
			<tr>
				<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
				<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
				<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">&nbsp;</DIV>
		</form>
		<SCRIPT language="javascript">
		function usrAlumFil_onchange()
		{
			var sDato = "-1";
			var sFiltro="@alum_id = ";
			if(document.all["usrAlumFil:txtId"].value != "") 
			{sDato = document.all["usrAlumFil:txtId"].value;}
			sFiltro = sFiltro + sDato + ",@alum_inse_id = " + document.all["lblInstituto"].value;			
			LoadComboXML("alumnos_rpt_consul", sFiltro, "cmbCarre", "S");		   			    			
		}
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
