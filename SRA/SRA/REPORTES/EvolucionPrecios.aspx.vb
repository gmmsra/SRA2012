Namespace SRA

Partial Class EvolucionPrecios
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

 End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      Dim mdsDatos As New DataSet
      clsWeb.gCargarRefeCmb(mstrConn, "aranceles", cmbArancelFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividadFil, "S")
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         lstrRptName = "EvolucionPrecios"

         If cmbActividadFil.Valor.ToString = "0" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la Actividad.")
         End If

         If hdnListaDesde.Text = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(hdnListaDesde.Text, "Int32"), Integer)
         End If

            If hdnListaHasta.Text = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(hdnListaHasta.Text, "Int32"), Integer)
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&acti_id=" + cmbActividadFil.Valor.ToString
            lstrRpt += "&peli_vige_desde=" + lintFechaDesde.ToString
            lstrRpt += "&peli_vige_hasta=" + lintFechaHasta.ToString
            lstrRpt += "&aran_id=" + IIf(cmbArancelFil.Valor.ToString = "", "0", cmbArancelFil.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
