Imports System.Data.SqlClient
Imports AccesoBD
Imports ReglasValida.Validaciones
Imports Interfaces.Importacion


Namespace SRA


Partial Class CertificadoRegIndivPorcinos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

	'El Dise�ador de Web Forms requiere esta llamada.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtCriaNumeDesde As NixorControls.TextBoxTab

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrConn As String
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			If (Not Page.IsPostBack) Then
				clsWeb.gInicializarControles(Me, mstrConn)
				mCargarCombos()
			End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
    End Sub

    Private Sub mCargarCombos()
        usrCriadorFil.FiltroRazas = "@raza_espe_id=6"
    End Sub

    Private Sub mImprimir()
        Try

            If usrCriadorFil.RazaId.ToString() = "" And usrCriadorFil.cmbCriaRazaExt.SelectedValue = "" Then
                clsError.gGenerarMensajes(Me, "La raza debe ser de la  la Especie de Porcinos.")
                Return
            End If


            mValidarDatos()

            Dim params As String
            Dim lstrRptName As String = "CertificadoRegIndivPorcinos"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            'Raza.
            params += "&raza_id=" & IIf(usrCriadorFil.RazaId.ToString = "", "0", usrCriadorFil.RazaId.ToString)

            'Criador.
            params += "&criador_id=" & IIf(usrCriadorFil.Valor.ToString = "0", "0", usrCriadorFil.Valor.ToString)

            'Sexo (0:Hembra // 1: Macho).
            If (cmbSexoFil.Valor.ToString <> "") Then
                params += "&sexo=" & IIf(cmbSexoFil.Valor.ToString = "false", "0", "1")
            End If

            'SW Padre.
            params += "&sw_padre=" & IIf(txtSWPadre.Valor.ToString = "", "0", txtSWPadre.Text)

            'SW Madre.
            params += "&sw_madre=" & IIf(txtSWMadre.Valor.ToString = "", "0", txtSWMadre.Text)

            'Fecha Denuncia.
            params += "&Fecha_Denuncia=" & IIf(txtFechaDenuncia.Fecha.ToString = "", "0", _
                                mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaDenuncia.Text))))

            'Fecha Nacimiento.
            params += "&Fecha_Nacimiento=" & IIf(txtFechaNacimiento.Fecha.ToString = "", "0", _
                                mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaNacimiento.Text))))

            'Fecha Ingreso.
            params += "&Fecha_Ingreso=" & IIf(txtFechaIngreso.Fecha.ToString = "", "0", _
                                mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaIngreso.Text))))

            'Tipo Certificado.
            params += "&tipoCertificado=" & Me.cmbCertifTipoFil.SelectedIndex

            'Usuario.
            params += "&audi_user=" & Session("sUserId").ToString()

            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mValidarDatos()
        'Raza.
        If usrCriadorFil.RazaId.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Raza.")
        End If

        ''Criador.
        'If usrCriadorFil.Codi.ToString = "" Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar el Criador.")
        'End If

        'Fechas.
        If ((txtFechaDenuncia.Text = "") And (txtFechaIngreso.Text = "") And (txtFechaNacimiento.Text = "")) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar al menos una de las Fechas.")
        End If
    End Sub

#End Region

#Region "Eventos de Controles"
	Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		Try
			mImprimir()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        usrCriadorFil.Limpiar()
        cmbSexoFil.Limpiar()
        cmbCertifTipoFil.Limpiar()
        txtSWMadre.Text = ""
        txtSWPadre.Text = ""
        txtFechaDenuncia.Text = ""
        txtFechaIngreso.Text = ""
        txtFechaNacimiento.Text = ""



    End Sub
End Class
End Namespace
