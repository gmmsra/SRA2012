Imports System.Data.SqlClient


Namespace SRA


Partial Class Socios_Adhesiones
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"

   Private mstrConn As String



   Private Enum Columnas As Integer
      InscId = 1
      FechaInsc = 2
      Ciclo = 3
      Periodo = 4
      Alumno = 5
      PlanPago = 6
      PlanPagoId = 7
      AlumnoId = 8
      PeriodoId = 9
      CicloId = 10

   End Enum


#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)


         If (Not Page.IsPostBack) Then
            mCargarCombos()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjetaFil, "T")
   End Sub
#End Region

   Private Sub mImprimirConcepto()
      Try
         Dim params As String
         Dim lstrRptName As String

         If cmbTipo.Valor = "D" Then
            lstrRptName = "Socios_Adhesiones"
         Else
            lstrRptName = "Socios_Adhesiones_Resumido"
         End If

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         params += "&soci_id=" + usrSociFil.Valor.ToString
         params += "&tarj_id=" + IIf(cmbTarjetaFil.Valor.ToString = "", "0", cmbTarjetaFil.Valor.ToString)
         params += "&orden=" + cmbOrden.Valor.ToString

         lstrRpt += params
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         mImprimirConcepto()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

End Class
End Namespace
