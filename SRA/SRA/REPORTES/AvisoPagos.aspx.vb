Namespace SRA

Partial Class AvisoPagos
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lblMail As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = "inscripciones"

   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
                clsWeb.gCargarRefeCmb(mstrConn, "textos_comunicados", cmbTexto, "S")
                txtVenc.Text = Date.Today.ToString("dd/MM/yyyy")
            txtDeudaAl.Text = Date.Today.ToString("dd/MM/yyyy")
            mEstablecerPerfil()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFiltros()
      usrSocFil.Limpiar()
      cmbTexto.Limpiar()
      cmbRepo.SelectedIndex = 0
      chkDire.Checked = False
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
   End Sub
#End Region

   Private Sub mListar()
      Try
         Dim lstrRptName As String
         Dim lintFecha As Integer

         If cmbRepo.Valor.ToString = "E" Then
            lstrRptName = "Etiquetas"
            ElseIf cmbRepo.Valor.ToString = "C" Then
                lstrRptName = "AvisoPagos"
            ElseIf cmbRepo.Valor.ToString = "P" Then
                lstrRptName = "AvisosPagosProvincia"
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            hdnRptId.Text = usrSocFil.ObtenerIdFiltro.ToString

            If txtDeudaAl.Fecha.ToString = "" Then
                lintFecha = 0
            Else
                lintFecha = CType(clsFormatear.gFormatFechaString(txtDeudaAl.Text, "Int32"), Integer)
            End If

            If cmbRepo.Valor.ToString = "E" Then
                lstrRpt += SRA_Neg.Utiles.ParametrosEtiquetas("AP", hdnRptId.Text, IIf(chkDire.Checked, "True", "False"), , , , , , , , , , , , , cmbTexto.Valor.ToString)
            ElseIf cmbRepo.Valor.ToString = "C" Then
                lstrRpt += "&rptf_id=" + hdnRptId.Text
                lstrRpt += "&teco_id=" + IIf(cmbTexto.SelectedItem.Text.ToUpper() = "NINGUNO", "0", cmbTexto.Valor.ToString)
                lstrRpt += "&vencimiento=" + txtVenc.Text
                lstrRpt += "&deuda_al=" + lintFecha.ToString
                lstrRpt += "&fecdeuda_al=" + txtDeudaAl.Text
                lstrRpt += "&IncluInte=" + IIf(chkInclInte.Checked, "True", "False")
            ElseIf cmbRepo.Valor.ToString = "P" Then
                lstrRpt += "&rptf_id=" + hdnRptId.Text
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         If cmbRepo.Valor.ToString = "C" Then
            If cmbTexto.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el texto de comunicado a utilizar.")
            End If
            If txtDeudaAl.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de la deuda.")
            End If
            If txtVenc.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de vencimiento.")
            End If
         End If
         mListar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

End Class
End Namespace
