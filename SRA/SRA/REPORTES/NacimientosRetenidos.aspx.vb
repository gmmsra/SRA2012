Imports ReglasValida.Validaciones


Namespace SRA


Partial Class NacimientosRetenidos
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                '     Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mListar()

        Try
            Dim params As String

            If (usrCriadorFil.RazaId.ToString.Trim = "0" Or usrCriadorFil.RazaId.ToString.Trim = "") Or _
              (usrCriadorFil.Valor.ToString.Trim = "0" Or usrCriadorFil.Valor.ToString.Trim = "") Then
                clsError.gGenerarMensajes(Me, "La raza y el criador  deben ser distinto de vacio.")
                Return
            End If


            Dim lstrRptName As String = "NacimientoRetenidos"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim stRazaDescripcion As String

            params += "&raza_id=" & usrCriadorFil.RazaId.ToString
            params += "&criador_id=" & usrCriadorFil.Valor.ToString

            params += "&Lote=" & IIf(txtLote.Text = "", "0", txtLote.Text)

            params += "&tram_nume=" & IIf(txtNroTramite.Text = "", "0", txtNroTramite.Text)

            stRazaDescripcion = oRaza.GetRazaDescripcionById( _
                                   IIf(usrCriadorFil.cmbCriaRazaExt.SelectedValue.ToString = "", "0", _
                                   usrCriadorFil.cmbCriaRazaExt.SelectedValue.ToString)).Trim()

            params += "&RazaDescripcion=" & _
            oRaza.GetRazaCodiById(usrCriadorFil.cmbCriaRazaExt.SelectedValue.ToString).ToString + _
            "-" + stRazaDescripcion

            params += "&NombreCriador=" & IIf(usrCriadorFil.Apel.ToString.Trim = "", "", _
            usrCriadorFil.Apel.ToString.Trim)

            params += "&CriadorCodi=" & usrCriadorFil.txtCodiExt.Text

            params += "&RPDesde=" & IIf(txtRPDesde.Text = "", "", txtRPDesde.Text)

            params += "&RPHasta=" & IIf(txtRPHasta.Text = "", "", txtRPHasta.Text)

            If (dtfhpresdesde.Text = "") Then
                params += "&fechadesde=0"
            Else
                params += "&fechadesde=" & CType(clsFormatear.gFormatFechaString(dtfhpresdesde.Text, "Int32"), Integer).ToString
            End If

            If (dtfhpreshasta.Text = "") Then
                params += "&fechahasta=0"
            Else
                params += "&fechahasta=" & CType(clsFormatear.gFormatFechaString(dtfhpreshasta.Text, "Int32"), Integer).ToString
            End If

            If (dtfhnacidesde.Text = "") Then
                params += "&fechanacidesde=0"
            Else
                params += "&fechanacidesde=" & CType(clsFormatear.gFormatFechaString(dtfhnacidesde.Text, "Int32"), Integer).ToString
            End If

            If (dtfhnacihasta.Text = "") Then
                params += "&fechanacihasta=0"
            Else
                params += "&fechanacihasta=" & CType(clsFormatear.gFormatFechaString(dtfhnacihasta.Text, "Int32"), Integer).ToString
            End If

            If (chkIncluyeProdBaja.Checked) Then
                params += "&incluyebaja=true"
            Else
                params += "&incluyebaja=false"
            End If

            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt, True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try




    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub

    Private Sub btnLimpFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        txtFechaInscripcion.Text = ""
        txtLote.Text = ""
        txtNroTramite.Text = ""
        chkIncluyeProdBaja.Checked = True
        usrCriadorFil.Limpiar()
    End Sub
End Class

End Namespace
