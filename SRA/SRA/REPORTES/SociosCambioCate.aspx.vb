Imports System.Data.SqlClient


Namespace SRA


Partial Class SociosCambioCate
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "inscripciones"

   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mCargarCombos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnEnvi.Attributes.Add("onclick", "return(mConfirmar());")
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstado, "T", "@esti_id=3")
      clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar @acti_id=2", cmbMail, "id", "descrip", "S")
   End Sub
   Private Sub mLimpiarFiltros()
      grdCons.CurrentPageIndex = 0
      cmbTipoFil.SelectedIndex = 0
      usrSociFil.Valor = 0
      cmbRepo.Enabled = True
      grdCons.Visible = False
      btnList.Visible = True
      btnEnvi.Visible = False
   End Sub

#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
   End Sub
#End Region

   Private Sub mListar(ByVal pstrTipo As String)
      Try
         Dim lstrRptName As String
         Dim lstrRpt As String
         Dim lintFechaD As Integer
         Dim lintFechaH As Integer
         
         If pstrTipo = "E" Then
            lstrRptName = "EtiquetasCambioCate"
         Else
            lstrRptName = "SociosCambioCate"
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If txtFechaDesdeFil.Text = "" Then
            lintFechaD = 0
         Else
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Text = "" Then
            lintFechaH = 0
         Else
            lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&tipo=" + cmbTipoFil.Valor.ToString
         lstrRpt += "&soci_id=" + usrSociFil.Valor.ToString
         lstrRpt += "&fecha_desde=" + lintFechaD.ToString
         lstrRpt += "&fecha_hasta=" + lintFechaH.ToString
         lstrRpt += "&orden=" + cmbOrden.Valor.ToString
         lstrRpt += "&soci_esta_id=" + cmbEstado.Valor.ToString
         lstrRpt += "&Mail=" & cmbRepoMail.Valor.ToString
         lstrRpt += "&Correo=" & cmbRepoCorreo.Valor.ToString
         If pstrTipo = "E" Then
            lstrRpt = lstrRpt & "&ConDireccion=" & IIf(Me.chkDire.Checked, "True", "False")
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
            'If cmbTipoFil.SelectedIndex <= 0 Then
            '   Throw New AccesoBD.clsErrNeg("Debe indicar un tipo de listado.")
            'End If
         If cmbOrden.SelectedIndex <= 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar un orden al listado.")
         End If
         If cmbRepo.Valor.ToString = "M" Then
             mConsultarMails(grdCons)
         Else
             mListar(cmbRepo.Valor.ToString)
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarMails(grdCons)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarMails(ByVal pobjGrid As System.Web.UI.WebControls.DataGrid)
      Try

         Dim lstrCmd As String = "exec socios_cate_mails_consul "
         lstrCmd += " @tipo=" & clsSQLServer.gFormatArg(cmbTipoFil.Valor.ToString, SqlDbType.VarChar)
         lstrCmd += ",@soci_id=" & clsSQLServer.gFormatArg(usrSociFil.Valor.ToString, SqlDbType.Int)
         lstrCmd += ",@fecha_desde=" & clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
         lstrCmd += ",@fecha_hasta=" & clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
         lstrCmd += ",@orden=" & clsSQLServer.gFormatArg(cmbOrden.Valor.ToString, SqlDbType.VarChar)
         lstrCmd += ",@soci_esta_id=" & clsSQLServer.gFormatArg(cmbEstado.Valor.ToString, SqlDbType.Int)

         Dim ds As New DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each dc As DataColumn In ds.Tables(0).Columns
             Dim dgCol As New Web.UI.WebControls.BoundColumn
             dgCol.DataField = dc.ColumnName
             dgCol.HeaderText = dc.ColumnName
             If dc.Ordinal = 0 Or dc.ColumnName.IndexOf("_id") <> -1 Then
                dgCol.Visible = False
             End If
             pobjGrid.Columns.Add(dgCol)
         Next
         pobjGrid.DataSource = ds
         pobjGrid.DataBind()
         Session("ds") = DirectCast(pobjGrid.DataSource, DataSet)
         pobjGrid.Visible = True
         btnEnvi.Enabled = (pobjGrid.Items.Count > 0)
         btnEnvi.Visible = True
         btnList.Visible = False
         cmbRepo.Enabled = False

         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mEnviarMails()
      Try
         Dim ds As New DataSet
         Dim lstrMail As String
         Dim lstrInst As String
         Dim lstrCate As String
         Dim lstrAsun As String
         Dim lstrMens As String
         Dim lstrCmd As String
         Dim lstrMailMode As String = System.Configuration.ConfigurationSettings.AppSettings("conMailMode").ToString()

         lstrMailMode = "..\" & lstrMailMode

         ''lstrCmd = "exec socios_general_mails_consul @rptf_id=" & hdnRptId.Text
         ''ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each odrDeta As DataRow In Session("ds").Tables(1).Select
            lstrCate = odrDeta.Item("cate_id")
            If odrDeta.IsNull("inst_id") Then
                lstrInst = ""
            Else
                lstrInst = odrDeta.Item("inst_id")
            End If
            lstrAsun = clsSQLServer.gCampoValorConsul(mstrConn, "mails_modelos_consul @mamo_id=" & cmbMail.Valor.ToString, "mamo_asun")
            lstrCmd = "mails_mode_catego_consul @mmca_acti_id=2,@mmca_cate_id=" & lstrCate & ",@mmca_mamo_id=" & cmbMail.Valor.ToString
            If lstrInst <> "" Then
               lstrCmd = lstrCmd & ",@mmca_inst_id=" & lstrInst
            End If
            lstrMens = clsSQLServer.gCampoValorConsul(mstrConn, lstrCmd, "mmca_mens")
            lstrMail = mObtenerMails(Session("ds"), lstrCate, lstrInst)
            If lstrMail <> "" And lstrMens <> "" Then
                clsMail.gEnviarMail(lstrMail, lstrAsun, lstrMens, lstrMailMode)
            End If
         Next
         ds.Dispose()

         btnEnvi.Visible = False
         btnList.Visible = True
         cmbRepo.Enabled = True
         grdCons.Visible = False

         Throw New AccesoBD.clsErrNeg("Se han enviado los mails a los socios seleccionados.")

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mObtenerMails(ByVal ds As DataSet, ByVal pstrCate As String, ByVal pstrInst As String) As String
      Dim lstrMail As String
      Try
         For Each odrClag As DataRow In ds.Tables(0).Select("cate_id=" & pstrCate)
            If lstrMail <> "" Then
               lstrMail = lstrMail & ";"
            End If
            lstrMail = lstrMail & odrClag.Item("mails")
         Next
         ds.Dispose()
         Return (lstrMail)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Function

   Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvi.Click
      mEnviarMails()
   End Sub

End Class
End Namespace
