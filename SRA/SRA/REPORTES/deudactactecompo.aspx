<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DeudaCtaCteCompo" CodeFile="DeudaCtaCteCompo.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Composición Deuda Cta. Corriente</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25">
									<asp:label id="lblTitu" runat="server" cssclass="opcion" width="624px">Composición Deuda en Cuenta Corriente</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblActifil" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbActiFil" runat="server" Width="400px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo" Width="87px">A Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEdesde" runat="server" cssclass="titulo" Width="133px">Ctro.Emisor Desde:</asp:Label></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEdesde" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEhasta" runat="server" cssclass="titulo">Ctro.Emisor Hasta:</asp:Label></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEhasta" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblNroDde" runat="server" cssclass="titulo">Nro de Cliente:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtNroDde" runat="server" cssclass="cuadrotexto" Width="64px" AceptaNull="false"
																				MaxValor="999999999" Obligatorio="true"></cc1:numberbox>
																			<asp:Label id="lblNroHta" runat="server" cssclass="titulo">Al:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtNroHta" runat="server" cssclass="cuadrotexto" Width="64px" AceptaNull="false"
																				MaxValor="999999999" Obligatorio="true"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="Label1" runat="server" cssclass="titulo" Width="100%">Tipo de Listado:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:RadioButton id="rbtCompo" runat="server" cssclass="titulo" Text="Composición" Checked="True"
																				GroupName="RadioGroup1"></asp:RadioButton><BR>
																			<asp:RadioButton id="rbtGesti" runat="server" cssclass="titulo" Text="Gestión" GroupName="RadioGroup1"></asp:RadioButton><BR>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD colSpan="3" height="10"></TD>
														</TR>
														<TR>
															<TD align="right" colSpan="3">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
																	ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
