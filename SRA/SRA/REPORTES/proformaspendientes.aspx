<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ProformasPendientes" enableViewState="True" CodeFile="ProformasPendientes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Avisos de Pago Pendiente</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 8px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Avisos de Pago Pendiente</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 8px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="97%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" height="10"></TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg" height="10"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 80%; HEIGHT: 17px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbActi" runat="server" Width="250px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg" height="21">
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" height="21">
																					<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg" height="21">
																					<UC1:CLIE id="usrClieFil" runat="server" Ancho="800" tabla="clientes" Saltos="1,1,1,1" MuestraDesc="False"
																						FilFanta="true" AceptaNull="false"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" height="21">
																					<asp:Label id="lblTipoListado" runat="server" cssclass="titulo">Tipo de Listado:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg" height="21">
																					<cc1:combobox class="combo" id="cmbTipoListado" runat="server" Width="100px" onchange="javascript:mSetearTipo();">
																						<asp:ListItem Selected="True">(Seleccione)</asp:ListItem>
																						<asp:ListItem Value="C">Carta</asp:ListItem>
																						<asp:ListItem Value="L">Listado</asp:ListItem>
																						<asp:ListItem Value="M">Mail</asp:ListItem>
																					</cc1:combobox>&nbsp;
																					<asp:Label id="lblCarta" runat="server" cssclass="titulo">Enviar a:</asp:Label>
																					<cc1:combobox class="combo" id="cmbCarta" runat="server" Width="136px">
																						<asp:ListItem Value="T" Selected="True">Todos los clientes</asp:ListItem>
																						<asp:ListItem Value="M">Solo clientes sin mail</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" height="21">
																					<asp:Label id="lblGastosAdm" runat="server" cssclass="titulo">Gastos Administrativos:</asp:Label>&nbsp;</TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg" height="21">
																					<cc1:numberbox id="txtGastosAdm" runat="server" cssclass="cuadrotexto" Width="75px" esdecima="true"
																						EsDecimal="True"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"
																					height="10"></TD>
																			</TR>
																		</TABLE>
																		<asp:datagrid id="grdCons" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																			ItemStyle-Height="5px" PageSize="10" OnPageIndexChanged="DataGrid_Page" GridLines="None" HorizontalAlign="Center"
																			AllowPaging="True" width="100%" CellPadding="1" CellSpacing="1">
																			<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																			<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn Visible="false">
																					<HeaderStyle Width="5%"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																							Height="5">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD> <!--FIN FORMULARIO-->
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnEnvi" runat="server" BorderStyle="None" visible="false" ImageDisable="btnEnvi0.gif"
																			ForeColor="Transparent" ImageOver="btnEnvi2.gif" ImageBoton="btnEnvi.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/btnEnvi.gif"
																			ImagesUrl="../imagenes/" IncludesUrl="../includes/"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
																			ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																			ImageUrl="imagenes/btnImpr.jpg" ImagesUrl="../imagenes/" IncludesUrl="../includes/"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																			ImageUrl="imagenes/limpiar.jpg" ImagesUrl="../imagenes/" IncludesUrl="../includes/" CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
		<script language="javascript">
	function mSetearTipo()
	{
		if (document.all('cmbTipoListado').value == 'C')
		{
			document.all('lblCarta').style.visibility = '';
			document.all('cmbCarta').style.visibility = '';
		}
		else
		{
			document.all('lblCarta').style.visibility = 'hidden';
			document.all('cmbCarta').style.visibility = 'hidden';
		}
	}
	mSetearTipo();
		</script>
	</BODY>
</HTML>
