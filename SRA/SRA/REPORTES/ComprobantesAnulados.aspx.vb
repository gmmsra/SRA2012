Namespace SRA

Partial Class ComprobantesAnulados
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
            mCargarCombos()

            Dim mobj As SRA_Neg.Facturacion
            mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
            mobj.CentroEmisorNro(mstrConn)

            cmbCentroEmisor.Valor = mobj.pCentroEmisorId
            If Not clsSQLServer.gObtenerValorCampo(mstrConn, "EMISORES_CTROS", cmbCentroEmisor.Valor.ToString, "emct_central") Then
               cmbCentroEmisor.Enabled = False
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbTipoComprobante, "T", "'29,30,31,32,33'")
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividad, "T")
   End Sub
#End Region

#Region "Detalle"
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "ComprobantesAnulados"
         Dim lstrRpt As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If


         lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
         lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
         lstrRpt += "&ComprobTipo=" + IIf(cmbTipoComprobante.Valor.ToString = "", "0", cmbTipoComprobante.Valor.ToString)
         lstrRpt += "&CentroEmisor=" + IIf(cmbCentroEmisor.Valor.ToString = "", "0", cmbCentroEmisor.Valor.ToString)
         lstrRpt += "&Actividad=" + IIf(cmbActividad.Valor.ToString = "", "0", cmbActividad.Valor.ToString)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
