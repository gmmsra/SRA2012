<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociosCambioCate" CodeFile="SociosCambioCate.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Socios a Cambiar de Categor�a</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">		
		function mConfirmar()
		{
			if (document.all('cmbRepo').value == 'M')
			{
				if (document.all('cmbMail').value == '')
				{				
					alert('Debe indicar un modelo de mail.');
					document.all('cmbMail').focus();
					return(false);
				}
				else
				{
					return(confirm('Confirma el env�o de los correos?'));
				}
			}
			else
				return(true);
		}
		function mSetearTipo()
		{
			if (document.all('cmbRepo')!=null)
			{
				if (document.all('cmbRepo').value=='M')
				{
					document.all('lblMail').style.display='';
					document.all('cmbMail').style.display='';					
				}
				else
				{
					document.all('lblMail').style.display='none';
					document.all('cmbMail').style.display='none';
				}
				if (document.all('cmbRepo').value=='E')
				{
					document.all('panDire').style.display='';
				}
				else
				{
					document.all('panDire').style.display='none';		
				}
				if (document.all('cmbRepo').value!='M')
				{
					document.all('lblRepoMail').style.display='';
					document.all('cmbRepoMail').style.display='';
					document.all('lblRepoCorreo').style.display='';
					document.all('cmbRepoCorreo').style.display='';
				}
				else
				{
					document.all('lblRepoMail').style.display='none';
					document.all('cmbRepoMail').style.display='none';
					document.all('lblRepoCorreo').style.display='none';
					document.all('cmbRepoCorreo').style.display='none';
				}	
				if (document.all('cmbRepo').value=='R')
				{
					document.all('lblTipoListado').style.display='none';
					document.all('cmbTipoListado').style.display='none';
				}
				else
				{
					document.all('lblTipoListado').style.display='none';
					document.all('cmbTipoListado').style.display='none';
				}							
			}
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Listado de Socios a Cambiar de Categor�a</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblTipoFil" runat="server" cssclass="titulo">Tipo de Reporte:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbTipoFil" runat="server" Width="320px" ImagesUrl="../Images/">
																						<asp:ListItem Value="(Seleccione un tipo de listado)" Selected="True">(Seleccione un tipo de listado)</asp:ListItem>
																						<asp:ListItem Value="M">Categor&#237;a Menores que cumplen 25 a&#241;os de edad</asp:ListItem>
																						<asp:ListItem Value="V">Categor&#237;a Activos que cumplen 40 a&#241;os de antig&#252;edad</asp:ListItem>
																						<asp:ListItem Value="H">Categor&#237;a Vitalicios que cumplen 10 a&#241;os de antig&#252;edad</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lboSociFil" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrSociFil" runat="server" SoloBusq="true" AceptaNull="False" MuestraDesc="False"
																						PermiModi="True" FilSociNume="True" FilCUIT="True" FilDocuNume="True" Saltos="1,1,1" Tabla="Socios"
																						width="100%"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px" ImagesUrl="Images/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px" ImagesUrl="Images/"></cc1:DateBox></TD>
																			</TR>
																			<TR style="DISPLAY: none">
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR style="DISPLAY: none">
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblEstado" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbEstado" runat="server" Width="240px" ImagesUrl="../Images/"
																						AceptaNull="false" IncludesUrl="../Includes/" NomOper="estados_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Orden del Reporte:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbOrden" runat="server" Width="320px" ImagesUrl="../Images/">
																						<asp:ListItem Value="" Selected="True">(Seleccione el orden del listado)</asp:ListItem>
																						<asp:ListItem Value="S">N&#250;mero de Socio</asp:ListItem>
																						<asp:ListItem Value="N">Nombre</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:label id="lblRepo" runat="server" cssclass="titulo">Tipo de Reporte:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<TABLE id="Table2" border="0">
																						<TR>
																							<TD noWrap>
																								<cc1:combobox class="combo" id="cmbRepo" runat="server" Width="160px" onchange="javascript:mSetearTipo();">
																									<asp:ListItem Value="R" Selected="True">Reporte</asp:ListItem>
																									<asp:ListItem Value="E">Etiquetas</asp:ListItem>
																									<asp:ListItem Value="M">Mails</asp:ListItem>
																								</cc1:combobox></TD>
																							<TD>
																								<asp:label id="lblTipoListado" runat="server" cssclass="titulo">Tipo:</asp:label>
																								<cc1:combobox class="combo" id="cmbTipoListado" runat="server" Width="100px" ImagesUrl="../Images/"
																									AceptaNull="false" IncludesUrl="../Includes/">
																									<asp:ListItem Value="D" Selected="True">Detallado</asp:ListItem>
																									<asp:ListItem Value="R">Resumido</asp:ListItem>
																								</cc1:combobox></TD>
																							<TD>
																								<DIV id="panDire" noWrap>
																									<asp:checkbox id="chkDire" Text="Con Direcci�n" Runat="server" CssClass="titulo"></asp:checkbox></DIV>
																							</TD>
																							<TD>
																								<asp:label id="lblMail" runat="server" cssclass="titulo">Modelo:</asp:label></TD>
																							<TD>
																								<cc1:combobox class="combo" id="cmbMail" runat="server" Width="119px"></cc1:combobox></TD>
																						</TR>
																						<TR>
																							<TD noWrap colSpan="4">
																								<asp:label id="lblRepoMail" runat="server" cssclass="titulo">e-Mail:</asp:label>
																								<cc1:combobox class="combo" id="cmbRepoMail" runat="server" Width="120px">
																									<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
																									<asp:ListItem Value="1">Con</asp:ListItem>
																									<asp:ListItem Value="0">Sin</asp:ListItem>
																								</cc1:combobox>&nbsp;
																								<asp:label id="lblRepoCorreo" runat="server" cssclass="titulo">Correo:</asp:label>
																								<cc1:combobox class="combo" id="cmbRepoCorreo" runat="server" Width="120px">
																									<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
																									<asp:ListItem Value="0">Con</asp:ListItem>
																									<asp:ListItem Value="1">Sin</asp:ListItem>
																								</cc1:combobox></TD>
																							<TD></TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg"
																					colSpan="2">
																					<asp:datagrid id="grdCons" runat="server" BorderWidth="1px" BorderStyle="None" width="98%" AutoGenerateColumns="False"
																						ItemStyle-Height="5px" PageSize="10" OnPageIndexChanged="DataGrid_Page" GridLines="None" HorizontalAlign="Left"
																						AllowPaging="True" CellPadding="1" CellSpacing="1">
																						<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																						<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																						<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																						<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																						<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																						<FooterStyle CssClass="footer"></FooterStyle>
																						<Columns>
																							<asp:TemplateColumn Visible="False">
																								<HeaderStyle Width="5%"></HeaderStyle>
																								<ItemTemplate>
																									<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																										Height="5">
																										<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
																									</asp:LinkButton>
																								</ItemTemplate>
																							</asp:TemplateColumn>
																						</Columns>
																						<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																					</asp:datagrid></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																<TR>
																	<TD colSpan="3" height="10"></TD>
																</TR>
																<TR>
																	<TD align="right" colSpan="2">
																		<CC1:BotonImagen id="btnEnvi" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			visible="false" ImageDisable="btnEnvi0.gif" ForeColor="Transparent" ImageOver="btnEnvi2.gif"
																			ImageBoton="btnEnvi.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																			ImageUrl="imagenes/btnEnvi.gif"></CC1:BotonImagen>&nbsp;
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" ForeColor="Transparent"
																			ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" ForeColor="Transparent"
																			ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg" CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
		<script language="JavaScript">
		mSetearTipo();
		</script>
	</BODY>
</HTML>
