Namespace SRA

Partial Class DepositosImp
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
   Protected WithEvents hdnRptName As System.Web.UI.WebControls.TextBox


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrTabla As String = "depositos"
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mInicializar()
            mCargarCombos()
                cmbBancoFil.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_banc_id")
                clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaFil, "S", cmbBancoFil.Valor)
                cmbCuentaFil.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", "@cuba_defa=1", "cuba_id")
            clsWeb.gInicializarControles(sender, mstrConn)
         End If
            
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
        'clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancoFil, "T", "1")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancoFil, "T", "1")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaFil, "T")
    End Sub

    Public Sub mInicializar()
        Dim lstrParaPageSize As String
        clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
        grdDatoBusq.PageSize = Convert.ToInt32(lstrParaPageSize)
        btnAceptar.Visible = False
    End Sub

    Private Sub mLimpiarFiltro()
        cmbBancoFil.Limpiar()
        cmbCuentaFil.Limpiar()
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        txtBoletaDesdeFil.Text = ""
        txtBoletaHastaFil.Text = ""
        grdDatoBusq.Visible = False
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDatoBusq.EditItemIndex = -1
         If (grdDatoBusq.CurrentPageIndex < 0 Or grdDatoBusq.CurrentPageIndex >= grdDatoBusq.PageCount) Then
            grdDatoBusq.CurrentPageIndex = 0
         Else
            grdDatoBusq.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarBusc()

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub
   Public Sub mConsultarBusc()
      Dim lstrFiltros As New System.text.StringBuilder
      Dim mdsDatos As New DataSet

      With lstrFiltros
         .Append("exec depositos_consul_rpt")
         .Append(" @depo_nume=")
         .Append(IIf(txtBoletaDesdeFil.Valor.ToString = "", "null", "'" + txtBoletaDesdeFil.Valor.ToString + "'"))
         .Append(",@depo_numeH=")
         .Append(IIf(txtBoletaHastaFil.Valor.ToString = "", "null", "'" + txtBoletaHastaFil.Valor.ToString + "'"))
                If txtFechaDesdeFil.Fecha.ToString.Length > 0 Then
                    .Append(", @Fecha='" + Format(CDate(txtFechaDesdeFil.Fecha), "yyyyMMdd") + "'")
                End If
                If txtFechaHastaFil.Fecha.ToString.Length > 0 Then
                    .Append(", @FechaH='" + Format(CDate(txtFechaHastaFil.Fecha), "yyyyMMdd") + "'")
                End If
         .Append(",@Banco=" + IIf(cmbBancoFil.Valor.ToString() = "", "null", cmbBancoFil.Valor.ToString()))
                .Append(",@Cuenta=" + IIf(cmbCuentaFil.Valor.ToString() = "", "null", cmbCuentaFil.Valor.ToString()))
              


                mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, .ToString)

         If mdsDatos.Tables(0).Rows.Count = 0 Then
            grdDatoBusq.CurrentPageIndex = 0
         End If

         grdDatoBusq.DataSource = mdsDatos.Tables(0)
         grdDatoBusq.DataBind()
         grdDatoBusq.Visible = True

      End With
   End Sub
#End Region

   Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      Try
         grdDatoBusq.CurrentPageIndex = 0
         mConsultarBusc()
         btnAceptar.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltro()
   End Sub

    Private Sub cmbBancoFil_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBancoFil.SelectedIndexChanged

    End Sub
End Class

End Namespace

