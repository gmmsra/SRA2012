Namespace SRA

Partial Class AnaliticoConcepAran
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents cmbEstadp As NixorControls.ComboBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrTitu As String
    Private mstrRepo As String
    Private mstrTipo As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEdesde, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEhasta, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "T")
        ' ver esto de las actividades por usuario
        'clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ", False)
    End Sub
#End Region

#Region "Detalle"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer
            lstrRptName = "AnaliticoConcepAran"

            If cmbListar.Valor.ToString = "S" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un listado.")
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtFechaDesde.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
            End If

            If txtFechaHasta.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
            End If

            lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&CEmDesde=" + IIf(cmbCEdesde.Valor.ToString = "", "0", cmbCEdesde.Valor.ToString)
            lstrRpt += "&CEmHasta=" + IIf(cmbCEhasta.Valor.ToString = "", "0", cmbCEhasta.Valor.ToString)
            If txtConceptoDesde.Text <> "" Then
                lstrRpt += "&conc_codi_desde=" + txtConceptoDesde.Text
            End If
            If txtConceptoHasta.Text <> "" Then
                lstrRpt += "&conc_codi_hasta=" + txtConceptoHasta.Text
            End If
            ' Dario 2014-01-07 se agrega el filtro de actividad
            lstrRpt += "&acti_id=" + IIf(cmbActi.Valor.ToString = "", "0", cmbActi.Valor.ToString)
            lstrRpt += "&ActiDesc=" + IIf(cmbActi.SelectedItem.ToString = "", "", cmbActi.SelectedItem.ToString)

            lstrRpt += "&aran_codi_desde=" + IIf(txtArancelDesde.Valor.ToString = "", "0", txtArancelDesde.Valor.ToString)
            lstrRpt += "&aran_codi_hasta=" + IIf(txtArancelHasta.Valor.ToString = "", "0", txtArancelHasta.Valor.ToString)
            lstrRpt += "&Ejecuta=" + cmbListar.Valor.ToString
            ' Dario 2013-04-19 Cambio se agrega estado pago impago o parcial
            lstrRpt += "&Estado=" + IIf(cmbEstado.Valor.ToString = "-1", "N", cmbEstado.Valor.ToString)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class

End Namespace
