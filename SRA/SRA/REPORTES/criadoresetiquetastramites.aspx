<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CriadoresEtiquetasTramites" CodeFile="CriadoresEtiquetasTramites.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Etiquetas Criadores Tramites</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultgrupntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../stylesheet/sra.css">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<SCRIPT language="javascript">
		
		
		function mSetearHastaCriador()
		{
			if (document.all('txtCriaNumeDesde')!=null)
				document.all('txtCriaNumeHasta').value = document.all('txtCriaNumeDesde').value;
		}
			function mDeterminarRP(pobjRP,pobjNume)
			{
				if (document.all(pobjNume).value == '' && pobjRP.value != '')
				{
					var lstrRp = '';
					var lstrChar = '';
					for (i=0;i<pobjRP.value.length;i++)
					{
						lstrChar = pobjRP.value.substring(i,i+1);
						if (booIsNumber(lstrChar))
							lstrRp = lstrRp + lstrChar;
					}
					if (document.all(pobjNume).value == '')
						document.all(pobjNume).value = lstrRp;
				}
			}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="../imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="../imagenes/recsup.jpg"><IMG border="0" src="../imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="../imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="../imagenes/reciz.jpg" width="9"><IMG border="0" src="../imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion"> Etiquetas de Criadores en Tramites</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD background="../imagenes/formfdofields.jpg" width="100%"><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 90%; HEIGHT: 20px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																			colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.91%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo" Width="58px">Raza: </asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<UC1:CLIE id="usrCriadorFil" runat="server" FilCUIT="True" ColCUIT="True" FilTarjNume="False"
																				FilAgru="False" ColCriaNume="True" FilClaveUnica="True" CampoVal="Criador" Criador="True"
																				FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Criadores"
																				AceptaNull="false" MostrarBotones="False" Raza="true"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblCriadorDesde" runat="server" cssclass="titulo">Nro. Criador Desde:</asp:label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtCriaNumeDesde" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																				Width="140px" Obligatorio="false" onchange="mSetearHastaCriador();"></CC1:TEXTBOXTAB>&nbsp;
																			<asp:label id="lblCriadorhasta" runat="server" cssclass="titulo">Nro. Hasta:&nbsp;</asp:label>
																			<CC1:TEXTBOXTAB id="txtCriaNumeHasta" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblPresentaFechaDesde" runat="server" cssclass="titulo">Fecha de Presentacion: Desde:&nbsp;</asp:label></TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtPresentaFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																				IncludesUrl="Includes/"></cc1:DateBox>&nbsp;
																			<asp:label id="lblPresentaFechaHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																			<cc1:DateBox id="txtPresentaFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																				IncludesUrl="Includes/"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblInicioFechaDesde" runat="server" cssclass="titulo">Fecha de Inicio de Tramite: Desde:&nbsp;</asp:label></TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtInicioFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																				IncludesUrl="Includes/"></cc1:DateBox>&nbsp;
																			<asp:label id="lblInicioFechaHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																			<cc1:DateBox id="txtInicioFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																				IncludesUrl="Includes/"></cc1:DateBox></TD>
																	</TR>
																	
															
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblTipoTramiteFil" runat="server" cssclass="titulo">Tipo de Tr�mite:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbTipoTramiteFil" class="combo" runat="server" Width="208px" AceptaNull="False"
																				NomOper="rg_tramites_tipos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 65px" vAlign="top" background="../imagenes/formfdofields.jpg"
																			noWrap align="right">
																			<asp:label style="Z-INDEX: 0" id="lblCriaSelec" runat="server" cssclass="titulo">Nro. Criador Sel.:</asp:label>
																			<CC1:TEXTBOXTAB style="Z-INDEX: 0" id="txtCriaSel" runat="server" cssclass="cuadrotexto" Width="60px"
																				Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		<TD style="Z-INDEX: 0; WIDTH: 90%; HEIGHT: 64px" vAlign="middle" background="../imagenes/formfdofields.jpg">
																			<asp:ListBox id="lstCriadores" runat="server" Width="344px"></asp:ListBox></TD>
																		<TD style="WIDTH: 90%; HEIGHT: 64px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																			colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 20px" vAlign="top" background="../imagenes/formfdofields.jpg"
																			align="right">
																			<asp:Button style="Z-INDEX: 0" id="btnLimpiar" runat="server" Text="Limpiar" ToolTip="Limpiar Criador de Lista de Seleccion Multiple"></asp:Button></TD>
																		<TD style="WIDTH: 90%; HEIGHT: 20px" vAlign="middle" background="../imagenes/formfdofields.jpg">
																			<asp:Button id="btnAgregar" runat="server" Text=">" ToolTip="Agregar Criador de Lista Seleccion Multiple"></asp:Button>
																			<asp:Button style="Z-INDEX: 0" id="btnQuitar" runat="server" Text="<" ToolTip="Quitar Criador de Lista de Seleccion Multiple"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
														<TR>
															<TD height="10" colSpan="3"></TD>
														</TR>
														<TR>
															<TD colSpan="3" align="right">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																	CausesValidation="False" ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																	ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif"
																	OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="10" colSpan="3"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="../imagenes/recde.jpg" width="13"><IMG border="0" src="../imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="../imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="../imagenes/recinf.jpg"><IMG border="0" src="../imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="../imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
			<asp:textbox id="hdnRptId" runat="server" Visible="False"></asp:textbox></form>
	</BODY>
</HTML>
