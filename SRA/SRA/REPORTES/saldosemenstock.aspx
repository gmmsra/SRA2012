<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SaldoSemenStock" CodeFile="SaldoSemenStock.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="../controles/usrProducto.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Recuperación de Semen</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../stylesheet/sra.css">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="javascript">
		function usrCria_onchange()
		{		
			var lstrCriaId = document.all('usrCria:txtId').value;
			
		}	
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function usrCria_cmbRazaCria_onchange()
		{
			if (document.all('usrProd_cmbProdRaza')!=null)
			{
				if (document.all('usrProd_cmbProdRaza').selectedIndex != document.all('usrCria_cmbRazaCria').selectedIndex)
				{
					imgLimpProdDeriv_click('usrProd');
					document.all('usrProd_cmbProdRaza').selectedIndex = document.all('usrCria_cmbRazaCria').selectedIndex;
					document.all('usrProd_cmbProdRaza').onchange();
				}
				if (document.all('usrCria_cmbRazaCria').value != '')
				{
					document.all('usrProd_cmbProdRaza').disabled = true;
					document.all('txtusrProd:cmbProdRaza').disabled = true;	
				}
				else
				{
					document.all('usrProd_cmbProdRaza').disabled = false;
					document.all('txtusrProd:cmbProdRaza').disabled = false;	
				}
			}
		}
		function usrCriaId_onchange()
		{
			document.all('usrProd:txtCriaId').value = document.all('usrCria:txtId').value;
		}
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('')" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="../imagenes/recsup.jpg"><IMG border="0" src="../imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="../imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="../imagenes/reciz.jpg" width="9"><IMG border="0" src="../imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3">
											<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0">
												<tr>
													<TD style="HEIGHT: 25px" vAlign="bottom"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Stock de Semen</asp:label></TD>
													<TD style="WIDTH: 75px; HEIGHT: 25px" vAlign="bottom" align="right">
														<div id="imgClose"><IMG style="CURSOR: hand" class="noprint" onclick="window.close();" border="0" alt="Cerrar"
																src="../imagenes/close3.bmp"></div>
													</TD>
												</tr>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" Visible="True" BorderStyle="Solid"
												BorderWidth="0">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnConsultar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/"
																			IncludesUrl="../includes/" BackColor="Transparent" ImageUrl="../imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/"
																			IncludesUrl="../includes/" BackColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="../imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 21.41%; HEIGHT: 25px" vAlign="top" background="../imagenes/formfdofields.jpg"
																					align="right">
																					<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label></TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="true" Tabla="Clientes" Saltos="1,2" FilDocuNume="True"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 21.41%; HEIGHT: 17px" vAlign="top" background="../imagenes/formfdofields.jpg"
																					align="right">
																					<asp:Label id="lblPadreFil" runat="server" cssclass="titulo">Padre:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																						FilDocuNume="True" AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="False"
																						Ancho="800"></UC1:PROD></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel><asp:panel id="panCria" runat="server" cssclass="titulo" Width="100%" BorderWidth="0" visible="True">
												<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<asp:label id="lblCriaDesc" runat="server" ForeColor="#0054a3" Font-Size="Small" Font-Name="Verdana"
																Font-Names="Verdana">Criador</asp:label></TD>
													</TR>
													<TR>
														<TD>
															<asp:label id="lblClieDesc" runat="server" ForeColor="#0054a3" Font-Size="Small" Font-Name="Verdana"
																Font-Names="Verdana">Cliente</asp:label></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" AutoGenerateColumns="False"
												PageSize="25">
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<FooterStyle CssClass="footer"></FooterStyle>
												<Columns>
													<asp:TemplateColumn HeaderStyle-Width="0%" HeaderText="Raza">
														<ItemTemplate>
															<div><%#MuestraRaza(DataBinder.Eval(Container, "DataItem._RazaId"))%></div>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="sest_pres_fecha" ReadOnly="True" HeaderText="F.Present." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
													<asp:BoundColumn DataField="sest_recu_fecha" ReadOnly="True" HeaderText="F.Recup." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
													<asp:BoundColumn DataField="_Criador" ReadOnly="True" HeaderText="Propietario"></asp:BoundColumn>
													<asp:BoundColumn DataField="_Producto" ReadOnly="True" HeaderText="Producto"></asp:BoundColumn>
													<asp:BoundColumn DataField="sest_cant" ReadOnly="True" HeaderText="Dosis">
														<ItemStyle HorizontalAlign="Right"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn Visible="True" DataField="sest_cria_cant" HeaderText="Crias">
														<ItemStyle HorizontalAlign="Right"></ItemStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"><A id="editar" name="editar"></A></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD align="left"></TD>
										<TD align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
												ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/"
												IncludesUrl="../includes/" BackColor="Transparent" ImageUrl="../imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="../imagenes/recde.jpg" width="13"><IMG border="0" src="../imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="../imagenes/recinf.jpg"><IMG border="0" src="../imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="../imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriadorId" runat="server"></asp:textbox>
				<asp:textbox id="hdnRazaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriadorNomb" runat="server"></asp:textbox>
				<asp:textbox id="hdnProductoId" runat="server"></asp:textbox>
				<asp:textbox id="hdnClienteId" runat="server"></asp:textbox>
				<asp:textbox id="hdnClienteNomb" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
			usrCria_cmbRazaCria_onchange();
			if (document.all["editar"]!= null)
				document.location='#editar';
			if (document.all('hdnCriadorId').value == "")
			{
				div = document.getElementById('imgClose');
				div.style.display='none';
			}	
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
