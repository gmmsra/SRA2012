<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.RecibosFecha" CodeFile="RecibosFecha.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Recibos por Fecha</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25">
									<asp:label id="lblTitu" runat="server" cssclass="opcion" width="624px"> Recibos por Fecha</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" noWrap align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEdesde" runat="server" cssclass="titulo" Width="133px">Centro Emisor Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEdesde" runat="server" Width="240px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEhasta" runat="server" cssclass="titulo">Centro Emisor Hasta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEhasta" runat="server" Width="240px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblPerio" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Desde:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px" IncludesUrl="Includes/"
																				ImagesUrl="Images/"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px" IncludesUrl="Includes/"
																				ImagesUrl="Images/"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo Comprobante:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTipo" onchange="javascript:mSetearTipo();" runat="server" Width="180px" AceptaNull="False">
																				<asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
																				<asp:ListItem Value="1">Recibos</asp:ListItem>
																				<asp:ListItem Value="2">Acuse Recibo</asp:ListItem>
																				<asp:ListItem Value="3">Socios Cambio de Categor&#237;a</asp:ListItem>
																				<asp:ListItem Value="4">Socios Cuota Social</asp:ListItem>
																				<asp:ListItem Value="5">Socios Solicitud de Ingreso</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:checkbox id="chkPFP" Runat="server" CssClass="titulo"
																				Text="Incluir Recibos de ND (PFP)" Checked="false"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:checkbox id="chkSoli" Runat="server" CssClass="titulo"
																				Text="Incluir Recibos de Solicitud de Ingreso" Checked="false"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:checkbox id="chkCate" Runat="server" CssClass="titulo"
																				Text="Incluir Recibos de Cambio de Categor�a" Checked="false"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:checkbox id="chkConta" Runat="server" CssClass="titulo"
																				Text="S�lo Contado a Cuenta" Checked="false"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:checkbox id="chkDeta" Runat="server" CssClass="titulo"
																				Text="Reporte Detallado" Checked="false"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD colSpan="3" height="10"></TD>
														</TR>
														<TR>
															<TD align="right" colSpan="3">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
																	ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	IncludesUrl="../includes/" ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False"
																	OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																	CausesValidation="False"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function mSetearTipo()
		{
			var lbooNoSoci = (document.all('cmbTipo').value!='4');
			document.all('chkCate').disabled = lbooNoSoci;
			document.all('chkSoli').disabled = lbooNoSoci;
			document.all('chkPFP').disabled = lbooNoSoci;
			if (lbooNoSoci)
			{
				document.all('chkCate').checked = false;
				document.all('chkSoli').checked = false;
				document.all('chkPFP').checked = false;				
			}			
		}
		mSetearTipo();
		</SCRIPT>
	</BODY>
</HTML>
