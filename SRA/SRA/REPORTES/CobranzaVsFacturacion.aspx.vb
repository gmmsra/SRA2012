Imports System.Text
Imports System.IO


Namespace SRA

Partial Class CobranzaVsFacturacion
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnGenerar As System.Web.UI.WebControls.Button


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mSetearEventos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub mSetearEventos()
        btnList.Attributes.Add("onclick", "return(generarClick());")
    End Sub

#Region "Detalle"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            If Me.txtFechaHastaFil.Text.Length = 10 Then

                Dim nombreDelArchivo As String = "DeudaPorActividadCliente.xls"
                Dim respuesta As String

                respuesta = Business.Facturacion.CobranzasBusiness.GetDeudaPorActividadCliente(Me.txtFechaHastaFil.Fecha)

                Response.ContentType = "application/xls"
                Response.AppendHeader("Content-Disposition", "attachment; filename=" & nombreDelArchivo & "")
                Response.Write(respuesta)
                Response.End()
                'ExportarResultadoXLS(Business.Facturacion.CobranzasBusiness.GetDeudaPorActividadCliente(Me.txtFechaHastaFil.Fecha))
            Else
                Response.Write("<script language='javascript'>window.alert('Debe de informar la Fecha');</script>")
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub ExportarResultadoXLS(ByVal mdsDatos As DataSet)
        If (mdsDatos.Tables(0).Rows.Count > 0) Then
            Dim sb As New StringBuilder
            Dim sw As New StringWriter(sb)
            Dim htw As New HtmlTextWriter(sw)
            Dim pagina As New Page
            Dim form As New HtmlForm
            Dim dg As New DataGrid
            dg.EnableViewState = False
            dg.DataSource = mdsDatos.Tables(0)
            dg.DataBind()
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(dg)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            ' armo el nombre del archivo
            Dim nombrearchivo As String = ".xls"
            If Not IsNothing(Me.txtFechaHastaFil.Fecha) Then
                nombrearchivo = "_" & Convert.ToDateTime(Me.txtFechaHastaFil.Fecha).ToString("yyyy/MM/dd") & ".xls"
            End If
            divproce.Visible = False
            btnList.Visible = True
            Response.AddHeader("Content-Disposition", "attachment;filename=DeudaPorActividadCliente" & nombrearchivo)
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        End If
    End Sub
#End Region
End Class
End Namespace
