<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ClientesCondicionesEspeciales" enableViewState="True" CodeFile="ClientesCondicionesEspeciales.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Clientes con Condiciones Especiales</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
			function HabilitaMonto()
			{
				document.all("txtMonto").value="";
				document.all("txtMonto").disabled=!(document.all("chkCtaCorr").checked==true);
			}
			function cmbIIBB_OnChange()
			{
				document.all("hdnIIBBId").value = document.all("cmbIIBB").value
			}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 27px" height="27"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Clientes con Condiciones Especiales</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
											BorderStyle="Solid">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClieFil" runat="server" FilFanta="true" Ancho="800" Saltos="1,1,1,1" AceptaNull="false"
																					tabla="clientes"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkFactExen" Text="Facturaci�n Exenta" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkPercIVA" Text="Percepci�n IVA" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																				<asp:Label id="Label1" runat="server" cssclass="titulo">Categor�a IIBB:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbIIBB" tabIndex="3" runat="server" Width="130px" onchange="cmbIIBB_OnChange();"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkPrecSoci" Text="Precio de Socio" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkMiembroCD" Text="Miembro C.D." Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkNDInte" Text="Exento ND Intereses" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkCtaCorr" onclick="HabilitaMonto()" Text="Cuenta Corriente" Runat="server"
																					CssClass="titulo"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">&nbsp;&nbsp;
																				<asp:label id="lblMonto" runat="server" cssclass="titulo">L�mite:</asp:label>
																				<CC1:NUMBERBOX id="txtMonto" runat="server" cssclass="cuadrotexto" Width="104px"></CC1:NUMBERBOX></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkObservaciones" Text="Observaciones" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 15.05%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 15.05%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<!---fin filtro --->
								<tr>
									<td style="HEIGHT: 15px" colspan="2"></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 9px" height="9"></TD>
									<TD align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"
											ForeColor="Transparent" tooltip="Listar" IncludesUrl="../includes/" ImagesUrl="../imagenes/"></CC1:BOTONIMAGEN></TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnIIBBId" runat="server"></asp:textbox>
			</DIV>
		</form>
		</TR></TBODY></TABLE>
	</BODY>
</HTML>
