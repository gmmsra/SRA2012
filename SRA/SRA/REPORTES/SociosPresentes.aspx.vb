Namespace SRA

Partial Class SociosPresentes
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mCargarCombos()
                mEstablecerPerfil()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsamFil, "S", "")

    End Sub

    Private Sub mLimpiarFiltros()
        cmbAsamFil.Limpiar()
        lblCantidad.Text = ""

    End Sub

    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
        'Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub


#End Region

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "PlanSociPresen"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim lstrAsamDesc As String
            Dim lstrAsamFech As String

            If cmbAsamFil.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la asamblea")
            End If

            lstrAsamDesc = clsSQLServer.gCampoValorConsul(mstrConn, "asambleas_consul " & cmbAsamFil.Valor.ToString, "asam_nume")
            lstrAsamDesc = lstrAsamDesc & " - " & cmbAsamFil.SelectedItem.Text
            lstrAsamFech = CDate(clsSQLServer.gCampoValorConsul(mstrConn, "asambleas_consul " & cmbAsamFil.Valor.ToString, "asam_real_fecha")).ToString("dd/MM/yyyy")

            lstrRpt += "&asam_id=" + cmbAsamFil.Valor.ToString
            lstrRpt += "&asam_desc=" + lstrAsamDesc
            lstrRpt += "&asam_fecha=" + lstrAsamFech
            lstrRpt += "&orden=" + cmbOrde.Valor.ToString

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnCantidad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCantidad.Click
        Try
            Dim mstrCmd As String
            Dim intCant As Integer

            mstrCmd = "exec rpt_plan_socios_presen_consul_count "
            mstrCmd += "@asam_id= " + cmbAsamFil.Valor.ToString
            mstrCmd += ",@orden=" + cmbOrde.Valor.ToString

            intCant = clsSQLServer.gExecuteScalarTrans(mstrConn, mstrCmd)
            lblCantidad.Text = intCant.ToString

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
