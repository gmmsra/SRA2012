Namespace SRA

Partial Class SociosCuotasAdel
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      'Introducir aqu� el c�digo de usuario para inicializar la p�gina
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarEstados()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarEstados()
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "T")
   End Sub
#End Region

#Region "Detalle"
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "SociosCuotasAdel"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         Dim lintFechaD As Integer
         Dim lintFechaH As Integer
         Dim lstrAnioDesde As String = ""
         Dim lstrAnioHasta As String = ""

         If txtFechaDesdeFil.Text = "" Then
             Throw New AccesoBD.clsErrNeg("Debe indicar la fecha desde.")
         End If

         If txtFechaHastaFil.Text = "" Then
             Throw New AccesoBD.clsErrNeg("Debe indicar la fecha hasta.")
         End If

         If txtFechaDesdeFil.Text <> "" Then
            lstrAnioDesde = txtFechaDesdeFil.Text.Substring(txtFechaDesdeFil.Text.LastIndexOf("/") + 1)
            If lstrAnioDesde.Length <> 4 Then
                lstrAnioDesde = Convert.ToString(2000 + CInt(lstrAnioDesde))
            End If
         End If

         If txtFechaHastaFil.Text <> "" Then
            lstrAnioHasta = txtFechaHastaFil.Text.Substring(txtFechaHastaFil.Text.LastIndexOf("/") + 1)
            If lstrAnioHasta.Length <> 4 Then
                lstrAnioHasta = Convert.ToString(2000 + CInt(lstrAnioHasta))
            End If
         End If

         If lstrAnioDesde <> lstrAnioHasta Then
             Throw New AccesoBD.clsErrNeg("Las fechas desde y hasta deben corresponder al mismo a�o.")
         End If

         If txtFechaDesdeFil.Text = "" Then
            lintFechaD = 0
         Else
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Text = "" Then
            lintFechaH = 0
         Else
            lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&fecha_desde=" + lintFechaD.ToString
         lstrRpt += "&fecha_hasta=" + lintFechaH.ToString
         lstrRpt += "&cate_id=" + cmbCate.Valor.ToString
         lstrRpt += "&cate_desc=" + cmbCate.SelectedItem.Text
         lstrRpt += "&tipo=" + cmbTipo.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      txtFechaDesdeFil.Text = ""
      txtFechaHastaFil.Text = ""
      cmbCate.Limpiar()
      cmbTipo.Limpiar()
   End Sub
#End Region
End Class
End Namespace
