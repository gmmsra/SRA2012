Namespace SRA

Partial Class DebiAuto
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrActiId As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         minicializar()
         If (Not Page.IsPostBack) Then
            btnList.Attributes.Add("onclick", "return(mValidarEnvio());")
            clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar @acti_id=" + mstrActiId, cmbMail, "id", "descrip", "S")
            mCargarCombos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFiltros()
      cmbDeca.Limpiar()
      cmbEsta.Limpiar()
      cmbTarjeta.Limpiar()
      grdCons.Visible = False
      btnList.Visible = True
      btnEnvi.Visible = False
      cmbRepo.Enabled = True
   End Sub

   Private Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdCons.PageSize = Convert.ToInt32(mstrParaPageSize)
      If Not Request.QueryString("act") Is Nothing Then
         mstrActiId = Request.QueryString("act")
      Else
         mstrActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", Request.QueryString("fkvalor")).Tables(0).Rows(0).Item("inse_acti_id")
      End If
   End Sub

   Private Sub mCargarCombos()
      'clsWeb.gCargarRefeCmb(mstrConn, "debitos_cabe", cmbDeca, "T", "@acti_id=" + mstrActiId)
        clsWeb.gCargarRefeCmb(mstrConn, "debitos_acti", cmbDeca, "", "@acti_id=" + mstrActiId)
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjeta, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "T", SRA_Neg.Constantes.EstaTipos.EstaTipo_DebitosDeta)
   End Sub

#End Region

#Region "Detalle"
   Private Sub mValidarDatos()
        'clsWeb.gInicializarControles(Me, mstrConn)
        'clsWeb.gValidarControles(Me)


   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarMails(grdCons)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarMails(ByVal pobjGrid As System.Web.UI.WebControls.DataGrid)
      Try

         If cmbDeca.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el Debito.")
         End If

         Dim lstrAnio As String = cmbDeca.Valor.ToString().Substring(0, 4)
         Dim lstrPerio As String = cmbDeca.Valor.ToString().Substring(4, 2).Trim()

         Dim lstrCmd As String = "exec debitos_deta_rpt_mails_consul "
         lstrCmd += " @deca_anio=" + lstrAnio
         lstrCmd += ",@deca_perio=" + lstrPerio
         lstrCmd += ",@esta_id=" + cmbEsta.Valor.ToString
         lstrCmd += ",@tarj_id=" + cmbTarjeta.Valor.ToString

         Dim ds As New DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each dc As DataColumn In ds.Tables(0).Columns
             Dim dgCol As New Web.UI.WebControls.BoundColumn
             dgCol.DataField = dc.ColumnName
             dgCol.HeaderText = dc.ColumnName
             If dc.Ordinal = 0 Or dc.ColumnName.IndexOf("_id") <> -1 Then
                dgCol.Visible = False
             End If
             pobjGrid.Columns.Add(dgCol)
         Next
         pobjGrid.DataSource = ds
         pobjGrid.DataBind()
         Session("ds") = DirectCast(pobjGrid.DataSource, DataSet)
         pobjGrid.Visible = True
         btnEnvi.Enabled = (pobjGrid.Items.Count > 0)
         btnEnvi.Visible = True
         btnList.Visible = False
         cmbRepo.Enabled = False

         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
            If cmbDeca.Valor Is DBNull.Value And Not chkResumido.Checked Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el campo D�bito.")
            End If

            If cmbRepo.Valor = "M" Then
                If cmbMail.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar el campo Modelo.")
                End If
            End If

            'mValidarDatos()

            If cmbRepo.Valor.ToString = "M" Then
                mConsultarMails(grdCons)
            Else
                mListar()
            End If
        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mListar()
      Try
         Dim lstrRptName As String = "Debitos"
         Dim lstrAnio As String
         Dim lstrPerio As String

         If cmbDeca.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el Debito.")
         End If

         If cmbRepo.Valor.ToString = "E" Then
             lstrRptName = "Etiquetas"
            Else
                If chkResumido.Checked Then
                    lstrRptName = "DebitosResu"
                Else
                    lstrRptName = "Debitos"
                End If

            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrAnio = cmbDeca.Valor.ToString().Substring(0, 4)
            lstrPerio = cmbDeca.Valor.ToString().Substring(4, 2).Trim()

            If cmbRepo.Valor.ToString = "E" Then
                lstrRpt += SRA_Neg.Utiles.ParametrosEtiquetas("DA", "0", IIf(chkDire.Checked, "True", "False"), , , , , "E", "0", , , , , , , , , , , , , , cmbDeca.Valor.ToString(), , cmbEsta.Valor.ToString(), cmbTarjeta.Valor.ToString, lstrAnio, lstrPerio)
            Else
                lstrRpt += "&deca_anio=" + lstrAnio
                lstrRpt += "&deca_perio=" + lstrPerio
                lstrRpt += "&deca_desc=" + cmbDeca.SelectedItem.Text
                'lstrRpt += "&deca_id=" + cmbDeca.Valor.ToString
                lstrRpt += "&esta_id=" + cmbEsta.Valor.ToString
                lstrRpt += "&tarj_id=" + cmbTarjeta.Valor.ToString
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub mEnviarMails()
      Try
         Dim ds As New DataSet
         Dim lstrMail As String
         Dim lstrInst As String
         Dim lstrCate As String
         Dim lstrAsun As String
         Dim lstrMens As String
         Dim lstrCmd As String
         Dim lstrMailMode As String = System.Configuration.ConfigurationSettings.AppSettings("conMailMode").ToString()

         lstrMailMode = "..\" & lstrMailMode

         ''lstrCmd = "exec socios_general_mails_consul @rptf_id=" & hdnRptId.Text
         ''ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each odrDeta As DataRow In Session("ds").Tables(1).Select
            If odrDeta.IsNull("cate_id") Then
                lstrCate = ""
            Else
                lstrCate = odrDeta.Item("cate_id")
            End If
            If odrDeta.IsNull("inst_id") Then
                lstrInst = ""
            Else
                lstrInst = odrDeta.Item("inst_id")
            End If
            lstrAsun = clsSQLServer.gCampoValorConsul(mstrConn, "mails_modelos_consul @mamo_id=" & cmbMail.Valor.ToString, "mamo_asun")
            lstrCmd = "mails_mode_catego_consul @mmca_acti_id=" & mstrActiId & ",@mmca_mamo_id=" & cmbMail.Valor.ToString
            If lstrCate <> "" Then
               lstrCmd = lstrCmd & ",@mmca_cate_id=" & lstrCate
            End If
            If lstrInst <> "" Then
               lstrCmd = lstrCmd & ",@mmca_inst_id=" & lstrInst
            End If
            lstrMens = clsSQLServer.gCampoValorConsul(mstrConn, lstrCmd, "mmca_mens")
            lstrMail = mObtenerMails(Session("ds"), lstrCate, lstrInst)
            If lstrMail <> "" And lstrMens <> "" Then
                clsMail.gEnviarMail(lstrMail, lstrAsun, lstrMens, lstrMailMode)
            End If
         Next
         ds.Dispose()

         btnEnvi.Visible = False
         btnList.Visible = True
         cmbRepo.Enabled = True
         grdCons.Visible = False

         If mstrActiId = 2 Then
            Throw New AccesoBD.clsErrNeg("Se han enviado los mails a los socios seleccionados.")
         Else
            Throw New AccesoBD.clsErrNeg("Se han enviado los mails a los alumnos seleccionados.")
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Function mObtenerMails(ByVal ds As DataSet, ByVal pstrCate As String, ByVal pstrInst As String) As String
      Dim lstrMail As String
      Try
         If pstrCate <> "" Then
            For Each odrClag As DataRow In ds.Tables(0).Select("cate_id=" & pstrCate)
                If lstrMail <> "" Then
                lstrMail = lstrMail & ";"
                End If
                lstrMail = lstrMail & odrClag.Item("mails")
            Next
         Else
            For Each odrClag As DataRow In ds.Tables(0).Select("cate_id is null")
                If lstrMail <> "" Then
                lstrMail = lstrMail & ";"
                End If
                lstrMail = lstrMail & odrClag.Item("mails")
            Next
         End If
         ds.Dispose()
         Return (lstrMail)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Function

   Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvi.Click
      mEnviarMails()
   End Sub

#End Region

End Class
End Namespace
