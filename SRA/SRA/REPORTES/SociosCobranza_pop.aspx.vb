Namespace SRA

Partial Class SOciosCobranza_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lblClie As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
    Private mstrSoliId As String
    Private mstrSocmId As String
    Private mstrParaPageSize As Integer
   Private mdecTotDebe As Decimal
   Private mdecTotHaber As Decimal
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        mstrSoliId = Request.QueryString("soin_id")
        mstrSocmId = Request.QueryString("socm_id")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
        Dim lstrCmd As New StringBuilder

        If mstrSoliId <> "" Then
            lstrCmd.Append("exec soli_ingreso_cobranza_consul")
            lstrCmd.Append(" @soin_id=")
            lstrCmd.Append(mstrSoliId)
        End If

        If mstrSocmId <> "" Then
            lstrCmd.Append("exec soli_cambios_cobranza_consul")
            lstrCmd.Append(" @socm_id=")
            lstrCmd.Append(mstrSocmId)
        End If

        clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)
   End Sub
#End Region


End Class

End Namespace
