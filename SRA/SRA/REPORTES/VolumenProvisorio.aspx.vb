Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports AccesoBD
Imports Interfaces.Importacion


Namespace SRA



Partial Class VolumenProvisorio
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents cmbOrdenarPor As NixorControls.ComboBox
    Protected WithEvents lblOrdenarPor As System.Web.UI.WebControls.Label



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GSZ 12-11-2014 
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)


            If Not Page.IsPostBack Then

                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()


    End Sub
    Private Sub mValidarDatos()



        
        If usrCriadorFil.RazaId = "" Then
            Throw New AccesoBD.clsErrNeg("La Raza debe ser distinto de  vacio")
        End If

        

    End Sub
    Private Sub mListar()
        Try

            Dim params As String
            Dim lstrRptName As String = "ProdVolumenProvisorio"
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim oEspecies As New SRA_Neg.Especie(mstrConn, Session("sUserId").ToString())
            Dim strCriador As String
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim stRazaDescripcion As String
            stRazaDescripcion = oRaza.GetRazaDescripcionById( _
                                                             IIf(usrCriadorFil.RazaId.ToString = "", "0", usrCriadorFil.RazaId.ToString)).Trim()

            mValidarDatos()

            lstrRpt += "&FiltroHBAdesde=" + IIf(txtSraNumeDesde.Text = "", "0", txtSraNumeDesde.Text)
            lstrRpt += "&FiltroHBAhasta=" + IIf(txtSraNumeHasta.Text = "", "0", txtSraNumeHasta.Text)
            lstrRpt += "&prdt_raza_id=" & IIf(usrCriadorFil.RazaId.ToString = "", "0", _
                                    usrCriadorFil.RazaId.ToString)

            lstrRpt += "&criador_id=" & IIf(usrCriadorFil.Valor = 0, "0", _
                                                 usrCriadorFil.Valor.ToString())




            If Not cmbSexoFil.Valor Is System.DBNull.Value Then
                lstrRpt += "&prdt_sexo=" & IIf(cmbSexoFil.Valor.ToString = "false", "false", "true")
            End If


            lstrRpt += "&prdt_sra_nume_desde=" & IIf(txtSraNumeDesde.Valor.ToString = "", "0", _
                                                   txtSraNumeDesde.Text)

            lstrRpt += "&prdt_sra_nume_hasta=" & IIf(txtSraNumeHasta.Valor.ToString = "", "9999999", _
                                           txtSraNumeHasta.Text)


            lstrRpt += "&FiltroHBADesc=" + oEspecies.GetEspecieTipoHBADescByRazaId(usrCriadorFil.RazaId.ToString)
            lstrRpt += "&RazaDescrip=" & _
                               oRaza.GetRazaCodiById(usrCriadorFil.RazaId.ToString).ToString + _
                               "-" + stRazaDescripcion

            If (usrCriadorFil.Valor <> 0) Then
                strCriador = usrCriadorFil.Codi.ToString() + "-" + usrCriadorFil.Apel
            Else
                strCriador = "-"
            End If


            lstrRpt += "&FiltroProp=" & strCriador


            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click

        cmbSexoFil.Limpiar()
        usrCriadorFil.Limpiar()
        txtSraNumeDesde.Text = ""
        txtSraNumeHasta.Text = ""


    End Sub
End Class

End Namespace
