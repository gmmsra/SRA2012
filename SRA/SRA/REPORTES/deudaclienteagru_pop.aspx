<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DeudaClienteAgru_pop" CodeFile="DeudaClienteAgru_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OTRAS CUENTAS</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<SCRIPT language="javascript">
		function mSelClie(pClieId, pIdSocio){
			// Dario 2013-11-21 lo comentado abajo no camina
			gAbrirVentanas("../Alertas_pop.aspx?titulo=Estado de Saldos&origen=S&clieId="+pClieId+"&FechaValor="+document.all("hdnFechaValor").value+"&sociId="+pIdSocio
						  , 7,"700","400",null, null,100);
			//window.opener.document.all("usrClieFil:txtId").value = pClieId;
			//window.opener.document.all("btnBuscar").click();
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR>
					<TD>
						<asp:Label id="lblTitu" runat="server" width="100%" cssclass="opcion">Otras Cuentas</asp:Label></TD>
					<TD vAlign="top" align="right">&nbsp;<img onclick="window.close();" src="..\images\Close.bmp" alt="Cerrar">
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<asp:Label id="lblClie" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
				</TR>
				<TR height="100%">
					<TD colspan="2" vAlign="top"><asp:datagrid id="grdDato" runat="server" AutoGenerateColumns="False"
							ItemStyle-Height="5px" PageSize="15" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" ShowFooter="True"
							CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:BoundColumn DataField="clie_id" HeaderText="Cliente"></asp:BoundColumn>
								<asp:BoundColumn DataField="clie_apel" HeaderText="Agrupación"></asp:BoundColumn>
								<asp:BoundColumn DataField="decTotalProforma" HeaderText="Proformas" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
									ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
								</asp:BoundColumn>
								<asp:BoundColumn DataField="debe" HeaderText="Cta Social" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
									ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
								</asp:BoundColumn>
								<asp:BoundColumn DataField="haber" HeaderText="Cta Cte." DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
									ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
								</asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
									<ItemTemplate>
										<A href="javascript:mSelClie(<%#DataBinder.Eval(Container, "DataItem.clie_id")%>, <%#DataBinder.Eval(Container, "DataItem.sociId")%>);">
											<img src='../imagenes/edit.gif' border="0" alt="Ver Deuda" style="cursor:hand;" />
										</A>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colspan="2" align="right">
						<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
							ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
							ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
				</TR>
				<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="hdnSociId" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnFechaValor" runat="server"></asp:textbox>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
