<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DepositosImp" CodeFile="DepositosImp.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD> 
		<title>Impresi�n de Boletas de Dep�sitos </title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript" src="../includes/impresion.js"></script>
		<script language="javascript">
		function Imprimir()
		{
			var DepoId="";
			var depoIds="";
			var lstrRpt = "BoletasDeposito";
			var valParam="";
			
			if (document.all("grdDatoBusq").children(0).children.length > 1)
			{
				try
				{
					for (var fila=1; fila <= document.all("grdDatoBusq").children(0).children.length-1; fila++)
					{
						if (document.all("grdDatoBusq").children(0).children(fila).children(0).children(0).checked)
						{
							DepoId = document.all("grdDatoBusq").children(0).children(fila).children(0).children(1).innerText;
							
							if(depoIds!="") depoIds = depoIds + ",";
								depoIds = depoIds + DepoId;
														
							valParam=DepoId;
							var sRet = ImprimirReporte(lstrRpt,"DepoId", valParam, "C",DepoId,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							if(sRet=="0") 
							{
								document.all("hdnImprId").value="";
								return;
							}
							
						}
					}
				}
				catch(e)
				{
					document.all("hdnImprId").value="";
					alert("Error al intentar efectuar la impresi�n");
				}	
			}
		}
		function mCargarCuentas()
		{
			var sFiltro;
			if(document.all["cmbCuentaFil"]!=null)
			{
				sFiltro = "@banc_id=";
				if(document.all["cmbBancoFil"].value!="")
					sFiltro += document.all["cmbBancoFil"].value;
				else
					sFiltro += "-1"

				LoadComboXML("cuentas_bancos_cargar", sFiltro, "cmbCuentaFil", "S");
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tbody>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="391px">Impresi�n de Boletas de Dep�sito</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2">
										<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
											BorderStyle="Solid">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD style="WIDTH: 100%">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px" align="right" width="100%">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="../imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False"
																		OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="100%">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="../imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False"
																		OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" width="100%" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" background="../imagenes/formfdofields.jpg"
																		border="0">
																		<TR>
																			<TD width=140 align="right" nowrap>
																				<asp:Label id="lblBoletaDesdeFil" runat="server" cssclass="titulo">Nro Boleta Desde:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<CC1:TEXTBOXTAB id="txtBoletaDesdeFil" runat="server" cssclass="cuadrotexto" Width="72px"></CC1:TEXTBOXTAB>
																				<asp:Label id="lblBoletaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																				<CC1:TEXTBOXTAB id="txtBoletaHastaFil" runat="server" cssclass="cuadrotexto" Width="72px"></CC1:TEXTBOXTAB>
																			</TD>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 543px" align="right">
																				<asp:Label id="lblBancoFil" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<table border=0 cellpadding=0 cellspacing=0>
																				<tr>
																				<td>															
																				<cc1:combobox class="combo" id="cmbBancoFil" runat="server" cssclass="cuadrotexto" Width="230px" NomOper="bancos_cargar" filtra="true" MostrarBotones="False" onchange="mCargarCuentas();"></cc1:combobox>
																				</td>
																				<td>
																					<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																													onclick="mBotonBusquedaAvanzadaRepo('bancos','banc_desc','cmbBancoFil','Bancos','@con_cuentas=1');"
																													alt="Busqueda avanzada" src="../imagenes/Buscar16.gif" border="0">
																				</td>
																				</tr>
																				</table>
																			</TD>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 543px" align="right">
																				<asp:Label id="lblCuentaFil" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:combobox class="combo" id="cmbCuentaFil" runat="server" Width="230px"></cc1:combobox></TD>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 543px" align="right">
																				<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>
																				<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		
																		<TR>
																			<TD style="WIDTH: 18.77%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 12.69%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="../imagenes/formde.jpg" height="10"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
															<TR>
																<TD width="3" height="10"></TD>
																<TD align="right" height="10"></TD>
																<TD width="2" height="10"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE> <!---fin filtro --->
										</asp:panel>
									</TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2">
										<asp:datagrid id="grdDatoBusq" runat="server" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
											AutoGenerateColumns="False" width="100%" PageSize="15">
											<FooterStyle CssClass="footer"></FooterStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="17px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:CheckBox ID="chkSel" Runat="server"></asp:CheckBox>
														<div style="display:none"><%#DataBinder.Eval(Container, "DataItem.depo_id")%></div>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="depo_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="depo_fecha" ReadOnly="True" HeaderStyle-HorizontalAlign=Center HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="depo_nume" HeaderText="Boleta" ItemStyle-HorizontalAlign=Center ></asp:BoundColumn>
												<asp:BoundColumn DataField="_cuba_nume" ReadOnly="True" HeaderText="Cuenta">
													<HeaderStyle Width="50%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_banc_desc" ReadOnly="True" HeaderText="Banco">
													<HeaderStyle Width="50%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_depo_monto" ReadOnly="True" HeaderText="Monto">
													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
													<ItemStyle HorizontalAlign=Right></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid>
									</TD>
								</TR>
								<TR>
									<TD vAlign="middle" align="right" colSpan="3">
										<a runat="server" id="btnAceptar" href="javascript:Imprimir();"><img src="../imagenes/btnImpr.gif" alt="Imprimir" border="0"></a></TD>
								</TR>
							</TABLE> <!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</tbody>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnImprId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnImprimio" AutoPostBack="True" runat="server"></ASP:TEXTBOX>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["grdDatoBusq"]!= null)
			document.location='#grdDatoBusq';
		</SCRIPT>
	</BODY>
</HTML>
