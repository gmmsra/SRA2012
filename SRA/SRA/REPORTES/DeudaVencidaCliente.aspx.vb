Namespace SRA

Partial Class DeudaVencidaCliente
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                'txtFechaDesdeFil.Fecha = Now
                mCargarCombos()
                chkResumido.Checked = True
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        Try
            mLimpiarFiltros()
        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividad, "T")
    End Sub

    Private Sub mListar()
        Dim lstrRptName As String = "DeudaClientesPorActiResu"
        Dim lintFecha As Integer

        If (mValidarControles() = True) Then

            If (chkResumido.Checked = True) Then
                lstrRptName = "DeudaClientesPorActiResu"
            Else
                lstrRptName = "DeudaClientesPorActi"
            End If

            If txtFecha.Text <> "" Then
                lintFecha = CType(clsFormatear.gFormatFechaString(txtFecha.Text, "Int32"), Integer)
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&acti_id=" + IIf(cmbActividad.SelectedValue = "", "0", cmbActividad.Valor)
            lstrRpt += "&fecha=" + lintFecha.ToString
            lstrRpt += "&clie_id=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            lstrRpt += "&acti_desc=" + IIf(cmbActividad.SelectedValue = "", "0", cmbActividad.SelectedItem.Text)
            lstrRpt += "&ClieDesc=" + IIf(usrClieFil.Apel = "", "0", usrClieFil.Apel)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)
        Else
            Response.Write("<script language='javascript'>window.alert('La fecha es requerida.');</script>")
        End If
    End Sub

    Private Function mValidarControles() As Boolean
        If (txtFecha.Text.Trim.Length = 0) Then
            Return False
        Else
            Return True
        End If
    End Function


    Private Sub mLimpiarFiltros()
        usrClieFil.Limpiar()
        txtFecha.Text = ""
        chkResumido.Checked = True
        cmbActividad.SelectedIndex = 0
    End Sub

End Class

End Namespace
