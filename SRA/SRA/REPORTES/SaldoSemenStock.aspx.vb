Imports ReglasValida.Validaciones


Namespace SRA


Partial Class SaldoSemenStock
    Inherits FormGenerico


#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents usrCriadorFil As usrClieDeriv

    Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblProdDescP As System.Web.UI.WebControls.Label
    Protected WithEvents lblProductoFil As System.Web.UI.WebControls.Label


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_SemenStock
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then
                'Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                Dim dtProducto As DataTable
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim strNombreProducto As String
                Dim boolVieneSeleccionado As Boolean = False

                If Request.QueryString("ProdID") <> "" Then
                    boolVieneSeleccionado = True
                    hdnProductoId.Text = Request.QueryString("ProdID")
                    dtProducto = oProducto.GetProductoById(hdnProductoId.Text, "")
                    usrPadreFil.Valor = hdnProductoId.Text
                    usrPadreFil.RazaId = dtProducto.Rows(0).Item("prdt_raza_id")

                    mMostrarPanel(True)
                End If


                If Not boolVieneSeleccionado Then
                    If Request.QueryString("criadorID") <> "" Then
                        hdnCriadorId.Text = Request.QueryString("criadorID")
                        hdnRazaId.Text = Request.QueryString("RazaId")

                        usrPadreFil.CriaId = hdnCriadorId.Text
                        usrPadreFil.CriaOrPropId = hdnCriadorId.Text

                        lblCriaDesc.Text = "Criador: " + Request.QueryString("criadorNomb")
                        mMostrarPanel(False)
                    Else
                        mMostrarPanel(True)
                    End If

                    If Request.QueryString("clieID") <> "" Then
                        hdnClienteId.Text = Request.QueryString("clieID")
                        lblClieDesc.Text = "Cliente: " + Request.QueryString("clieNomb")
                        mMostrarPanel(False)
                    Else
                        mMostrarPanel(True)
                    End If

                    If Request.QueryString("PId") <> "" Then
                        strNombreProducto = oProducto.GetProductoDescripByProductoId(Request.QueryString("PId"))

                        hdnProductoId.Text = Request.QueryString("PId")
                        usrPadreFil.txtProdNombExt.Text = strNombreProducto.Trim()
                        mMostrarPanel(False)
                    Else
                        mMostrarPanel(True)
                    End If

                End If



                mEstablecerPerfil()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

    End Sub

    Public Sub mInicializar()
        usrPadreFil.ControlCriador_Activo = True
        usrPadreFil.FilSexo = False
        usrPadreFil.Sexo = 1
        'usrCriadorFil.AutoPostback = False
        'usrCriadorFil.FilClaveUnica = False
        'usrCriadorFil.ColClaveUnica = True
        'usrCriadorFil.Ancho = 790
        'usrCriadorFil.Alto = 510
        'usrCriadorFil.ColDocuNume = False
        'usrCriadorFil.ColCUIT = True
        'usrCriadorFil.ColCriaNume = True
        'usrCriadorFil.FilCUIT = True
        'usrCriadorFil.FilDocuNume = True
        'usrCriadorFil.FilAgru = False
        'usrCriadorFil.FilTarjNume = False
    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet

            mstrCmd = "exec saldo_" + mstrTabla + "_consul "
            If hdnCriadorId.Text <> "" Then
                mstrCmd = mstrCmd + "@cria_id=" + IIf(hdnCriadorId.Text = "", "0", hdnCriadorId.Text)
            Else
                mstrCmd = mstrCmd + "@cria_id=" + IIf(usrPadreFil.CriaOrPropId.ToString = "", "0", usrPadreFil.CriaOrPropId)
            End If

            If hdnClienteId.Text <> "" Then
                mstrCmd = mstrCmd + ",@clie_id=" + IIf(hdnClienteId.Text = "", "0", hdnClienteId.Text)
            Else
                mstrCmd = mstrCmd + ",@clie_id=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            End If

            If usrPadreFil.RazaId <> "" Then
                mstrCmd = mstrCmd + ",@raza_id=" + IIf(usrPadreFil.RazaId = "", "0", usrPadreFil.RazaId)
            End If

            If usrPadreFil.txtProdNombExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_nomb= " + clsSQLServer.gFormatArg(usrPadreFil.txtProdNombExt.Text, SqlDbType.VarChar)

            End If
            If usrPadreFil.txtRPExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_rp= " + clsSQLServer.gFormatArg(usrPadreFil.txtRPExt.Text, SqlDbType.VarChar)

            End If

            If usrPadreFil.txtSraNumeExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_sra_nume= " + clsSQLServer.gFormatArg(usrPadreFil.txtSraNumeExt.Text, SqlDbType.Int)

            End If

            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)

            grdDato.DataSource = dsDatos
            grdDato.DataBind()

            grdDato.Visible = True

            If hdnCriadorId.Text <> "" Then
                grdDato.Columns(2).Visible = False
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Function MuestraRaza(ByVal objText1 As Object) As String


        Dim dtRaza As DataTable
        Dim sReturn As String = ""
        Dim intRetornoRaza As Integer


        Dim oRaza As New SRA_Neg.Razas(mstrConn, 1)

        intRetornoRaza = oRaza.GetRazaCodiById(objText1.ToString())

        Return intRetornoRaza
    End Function
#End Region

#Region "Seteo de Controles"

    Private Sub mLimpiarFiltro()
        'usrCriadorFil.Limpiar()
        usrClieFil.Limpiar()

        grdDato.CurrentPageIndex = 0
        grdDato.Visible = False
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panFiltro.Visible = pbooVisi
        panCria.Visible = Not pbooVisi
    End Sub

    Private Sub mListar()
        Try
            Dim lstrRptName As String

            lstrRptName = "SaldoSemenStock"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If hdnCriadorId.Text <> "" Then
                lstrRpt += "&cria_id=" + IIf(hdnCriadorId.Text = "", "0", hdnCriadorId.Text)

            Else
                ' lstrRpt += "&cria_id=" + IIf(usrCriadorFil.Valor.ToString = "", "0", usrCriadorFil.Valor.ToString)
            End If

            If hdnClienteId.Text <> "" Then
                lstrRpt += "&clie_id=" + IIf(hdnClienteId.Text = "", "0", hdnClienteId.Text)
            Else
                lstrRpt += "&clie_id=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            End If


            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim booSexo As Boolean

            booSexo = oProducto.GetSexoByProductoId(hdnProductoId.Text)

            If hdnProductoId.Text <> "" Then
                If booSexo Then
                    lstrRpt += "&prdt_id=" + IIf(hdnProductoId.Text = "", "0", hdnProductoId.Text)

                End If
            Else
                If booSexo Then
                    lstrRpt += "&prdt_id=" + IIf(usrPadreFil.Valor.ToString = "", "0", usrPadreFil.Valor.ToString)

                End If
            End If




            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub

#End Region

    Private Sub btnConsultar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConsultar.Click
        mConsultar()
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub
End Class
End Namespace
