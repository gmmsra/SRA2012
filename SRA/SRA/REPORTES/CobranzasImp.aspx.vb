' Dario 2013-11-06 nuevo metodo que envia los comprobantes por mail 
Imports Business.EmailSenderService ' Dario 2013-11-14


Namespace SRA


Partial Class CobranzasImp
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label

    Protected WithEvents hdnRptName As System.Web.UI.WebControls.TextBox



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mdsDatos As New DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                mInicializar()
                mCargarCombos()
                Session(mSess(mstrTabla)) = Nothing
                clsWeb.gInicializarControles(sender, mstrConn)
            Else
                mdsDatos = Session(mSess(mstrTabla))
                If Not mdsDatos Is Nothing Then
                    mObtenerIDs()
                End If
            End If
            mSetearEventos()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbCoti, "", "@coti_ids='" & SRA_Neg.Constantes.ComprobTipos.Recibo & "," & SRA_Neg.Constantes.ComprobTipos.Recibo_Acuse & "," & SRA_Neg.Constantes.ComprobTipos.Factura & "," & SRA_Neg.Constantes.ComprobTipos.ND & "," & SRA_Neg.Constantes.ComprobTipos.NC & "," & SRA_Neg.Constantes.ComprobTipos.Proforma & "," & SRA_Neg.Constantes.ComprobTipos.Transferencias & "," & SRA_Neg.Constantes.ComprobTipos.AplicacionCreditos & "'")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCtro, "T", "")
        If Not Session("sCentroEmisorId") Is Nothing Then
            cmbCtro.Valor = Session("sCentroEmisorId")
        End If
        If Session("sCentroEmisorCentral") <> "S" Then
            cmbCtro.Enabled = False
        End If
        cmbCoti.Valor = CInt(SRA_Neg.Constantes.ComprobTipos.Factura)
    End Sub

    Private Sub mSetearEventos()
        'btnNinguno.Attributes.Add("onclick", "mSeleccionar(false);return false;")
        'btnTodos.Attributes.Add("onclick", "mSeleccionar(true);return false;")
    End Sub

    Public Sub mInicializar()
        Dim lstrParaPageSize As String
        clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
        grdDatoBusq.PageSize = Convert.ToInt32(lstrParaPageSize)
        btnAceptar.Visible = False
        btnEnviarPorMail.Visible = False
        btnNinguno.Visible = False
        btnTodos.Visible = False

    End Sub

    Private Sub mLimpiarFiltro()
        cmbCoti.Valor = CInt(SRA_Neg.Constantes.ComprobTipos.Factura)
        cmbCtro.Valor = Session("sCentroEmisorId")
        usrClie.Limpiar()
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""
        cmbCompLetra.Limpiar()
        txtCompCemiNum.Text = ""
        txtCompNum.Text = ""
        hdnIds.Text = ""
        hdnIdsImpr.Text = ""
        chkTodas.Checked = False
        chkIncAnulados.Checked = False
        grdDatoBusq.DataSource = Nothing
        grdDatoBusq.DataBind()
        btnAceptar.Visible = False
        btnEnviarPorMail.Visible = False
        btnNinguno.Visible = False
        btnTodos.Visible = False
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            mGuardarChechk()
            grdDatoBusq.EditItemIndex = -1
            If (grdDatoBusq.CurrentPageIndex < 0 Or grdDatoBusq.CurrentPageIndex >= grdDatoBusq.PageCount) Then
                grdDatoBusq.CurrentPageIndex = 0
            Else
                grdDatoBusq.CurrentPageIndex = E.NewPageIndex
            End If
            grdDatoBusq.DataSource = mdsDatos
            grdDatoBusq.DataBind()
            mObtenerIDs()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Public Sub mConsultarBusc()
        Dim lstrFiltros As New System.text.StringBuilder

        With lstrFiltros
            If cmbCoti.Valor.ToString = "28" Then
                .Append("exec proformas_impcomp_busq")
            Else
                .Append("exec " + mstrTabla + "_busq")

            End If
            .Append(" @coti_id=")
            .Append(cmbCoti.Valor)
            .Append(", @clie_id=")
            .Append(usrClie.Valor.ToString())
            .Append(", @emct_id=")
            .Append(cmbCtro.Valor.ToString())
            .Append(", @centro=")
            If Not Session("sCentroEmisorId") Is Nothing Then
                .Append(Session("sCentroEmisorId").ToString())
            Else
                .Append("0")
            End If
            .Append(", @fecha=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaDesde.Fecha))
            .Append(", @fechaHasta=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaHasta.Fecha))
            .Append(", @ComprobLetra='")
            .Append(cmbCompLetra.Valor.ToString())
            '.Append("', @ComprobNroCenEmi=")
            '.Append(IIf(txtCompCemiNum.Valor.ToString = "", "0", txtCompCemiNum.Valor.ToString))
            .Append("', @ComprobNumero=")
            .Append(IIf(txtCompNum.Valor.ToString = "", "0", txtCompNum.Valor.ToString))
            .Append(", @todos=")
            .Append(Math.Abs(CInt(chkTodas.Checked)).ToString)
            .Append(",@incAnulados=")
            .Append(Math.Abs(CInt(chkIncAnulados.Checked)).ToString)
            If cmbCoti.Valor.ToString <> "28" Then .Append(",@reimp=1")


            mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, .ToString)
            Session(mSess(mstrTabla)) = mdsDatos

            If mdsDatos.Tables(0).Rows.Count = 0 Then
                grdDatoBusq.CurrentPageIndex = 0
            End If

            grdDatoBusq.DataSource = mdsDatos.Tables(0)
            grdDatoBusq.DataBind()
            hdnIds.Text = ""
            hdnIdsImpr.Text = ""

        End With
    End Sub
    Private Function mObtenerIDs() As String
        Dim lstrIds As String = ""
        Dim lstrIdsImpr As String = ""
        For Each ldrData As DataRow In mdsDatos.Tables(0).Select("chk=1")
            'comprobantes seleccionados
            If lstrIds <> "" Then
                lstrIds = lstrIds & ";"
            End If
            lstrIds = lstrIds & ldrData.Item("comp_id") & ","
            lstrIds = lstrIds & ldrData.Item("original") & ","
            lstrIds = lstrIds & ldrData.Item("anulado")
            'comprobantes seleccionados no impresos
            If ldrData.Item("comp_impre") <> 1 Then
                If lstrIdsImpr <> "" Then
                    lstrIdsImpr = lstrIdsImpr & ","
                End If
                lstrIdsImpr = lstrIdsImpr & ldrData.Item("comp_id")
            End If
        Next
        hdnIds.Text = lstrIds
        hdnIdsImpr.Text = lstrIdsImpr
    End Function
    Private Sub mGuardarChechk()
        Dim dr As DataRow
        'Mantengo los check de las paguinas de la grilla
        For Each oDataItem As DataGridItem In grdDatoBusq.Items
            If mdsDatos.Tables(0).Select("comp_id = " & oDataItem.Cells(2).Text).Length <> 0 Then
                dr = mdsDatos.Tables(0).Select("comp_id = " & oDataItem.Cells(2).Text)(0)
                dr.Item("chk") = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
                dr.Item("original") = DirectCast(oDataItem.FindControl("chkOriginal"), CheckBox).Checked
            End If
        Next
        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mChequearGrilla(ByVal pbooValor As Boolean)
        For Each ldrData As DataRow In mdsDatos.Tables(0).Select("chk = " & (Not pbooValor).ToString)
            ldrData.Item("chk") = pbooValor
        Next
        Session(mSess(mstrTabla)) = mdsDatos
        grdDatoBusq.DataSource = mdsDatos
        grdDatoBusq.DataBind()
    End Sub

#End Region

    Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
        Try
            Dim lstrId As String
            Dim CompIds As String = hdnImprimio.Text
            Dim lobj As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), IIf(cmbCoti.Valor.ToString = "28", "proformas", mstrTabla))

            While CompIds <> ""
                lstrId = Left(CompIds, IIf(InStr(CompIds, ",") = 0, CompIds.Length, InStr(CompIds, ",") - 1))
                CompIds = Right(CompIds, IIf(InStr(CompIds, ",") = 0, 0, CompIds.Length - InStr(CompIds, ",")))
                lobj.ModiImpreso(lstrId, True)
            End While
            hdnImprimio.Text = ""
            mConsultarBusc()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub cmbCoti_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCoti.SelectedIndexChanged
        Try
            grdDatoBusq.CurrentPageIndex = 0
            mConsultarBusc()
            btnAceptar.Visible = True
            btnEnviarPorMail.Visible = True
            btnNinguno.Visible = True
            btnTodos.Visible = True
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        Try

            grdDatoBusq.CurrentPageIndex = 0
            mConsultarBusc()

            btnAceptar.Visible = True
            btnEnviarPorMail.Visible = True
            btnNinguno.Visible = True
            btnTodos.Visible = True

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    ' Dario 2013-11-06 nuevo metodo que envia los comprobantes por mail 
    ' al cliente que tiene mails sino avisa a quienes no pudo enviar el mail
    Private Sub btnEnviarPorMail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnviarPorMail.Click
        Try
            ' declaro las variobles que se usaran para ooptener los comprobantes que se 
            ' enviaran por mail
            Dim listIdComprobantes As New ArrayList
            Dim listDocNoEnviados As New ArrayList

            Dim bitChk As Boolean = False
            ' crea Objeto facturacion para poder acceder a la nueva funcion de vtos futuros
            Dim _FacturacionBusiness As New Business.Facturacion.FacturacionBusiness
            ' recorro la grilla y tomo los id de comprobamtes los que estan seleccionados
            For Each oDataItem As DataGridItem In grdDatoBusq.Items
                If mdsDatos.Tables(0).Select("comp_id = " & oDataItem.Cells(2).Text).Length <> 0 Then
                    bitChk = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
                    If (bitChk) Then
                        ' los acumulo en la coleccion
                        listIdComprobantes.Add(oDataItem.Cells(2).Text())
                    End If
                End If
            Next
            ' si tiene algun comprobante seleccionado ejecuto el metodo 
            ' para hacer el envio por medio del emailsender de la sra
            If (listIdComprobantes.Count > 0) Then
                Dim compNoEnviados As String

                listDocNoEnviados = EmailSenderBusiness.ReenviarDocumentacionPorMail(listIdComprobantes)

                If (listDocNoEnviados.Count = 0) Then
                    Response.Write("<script language='javascript'>window.alert('Documentos Enviados con exito.');</script>")
                Else
                    ' recorro los items
                    For Each item As String In listDocNoEnviados
                        ' recorro la grilla y para armar el mensaje de comprobantes que no se pudieron enviar
                        For Each oDataItem As DataGridItem In grdDatoBusq.Items
                            If mdsDatos.Tables(0).Select("comp_id = " & oDataItem.Cells(2).Text).Length <> 0 Then
                                bitChk = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
                                If (bitChk) Then
                                    ' los acumulo en la coleccion
                                    If (item = oDataItem.Cells(2).Text()) Then
                                        compNoEnviados = compNoEnviados + oDataItem.Cells(5).Text() + "; "
                                    End If
                                End If
                            End If
                        Next
                    Next
                    'clsError.gGenerarMensajes(Me, "Los siguientes comprobantes no pudieron ser reenviados:" + compNoEnviados)
                    Response.Write("<script language='javascript'>window.alert('Los siguientes comprobantes no pudieron ser reenviados:" + compNoEnviados + "');</script>")
                End If
            Else
                ' mensaje tiene que seleccionar algun documento
                Response.Write("<script language='javascript'>window.alert('Debe de selccionar algun documento.');</script>")
            End If

        Catch ex As Exception
            Dim msg As String
            msg = "Error CobranzasImp.vb.btnEnviarPorMail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnviarPorMail.Click)"
            clsError.gManejarError(msg, "", ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub

    Private Sub btnTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTodos.Click
        Try
            mChequearGrilla(True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnNinguno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNinguno.Click
        Try
            mChequearGrilla(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub hdnImprSele_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprSele.TextChanged
        mGuardarChechk()
        mObtenerIDs()
    End Sub

End Class

End Namespace

