<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DeudaCliente" CodeFile="DeudaCliente.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Resumen de Cuenta</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"> 
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<SCRIPT language="javascript">

	   	var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null);
	   
	   	function mCargarClienteCriador()
		{
			var sFiltro = document.all("txtCriaFil").value;
			var lstrRaza = document.all("cmbRazaFil").value;

 			if (sFiltro != '' && lstrRaza != '')
 			{
 				sFiltro = "@cria_nume=" + sFiltro;
 				sFiltro = sFiltro + ",@cria_raza_id=" + lstrRaza;

				var vstrRet = LeerCamposXML("criadores", sFiltro, "cria_clie_id").split("|");
				
				if(vstrRet[0]=="")
					alert("Criador inexistente");
				else
				{
					document.all["usrClieFil:txtCodi"].value = vstrRet[0]; //selecciona el cliente
					document.all["usrClieFil:txtCodi"].onchange();
				}
			}
		}
			
		function mCargarClienteSocio()
		{
			var sFiltro = document.all("txtSociFil").value;

 			if (sFiltro != '')
 			{
 				sFiltro = "@soci_nume=" + sFiltro + ",@baja=1";
				var vstrRet = LeerCamposXML("socios_datos", sFiltro, "clie_id").split("|");
				
				if(vstrRet[0]=="")
				{	
					alert("Socio inexistente");
					document.all("txtSociFil").value='';					
				}
				else
				{
					document.all["usrClieFil:txtCodi"].value = vstrRet[0]; //selecciona el cliente
					document.all["usrClieFil:txtCodi"].onchange();
				}
			}
		}
		
		function mTarjetasAbrir()
	   	{
		 gAbrirVentanas("../TarjetasHabilitadas_pop.aspx?EsConsul=1&titulo=Tarjetas Habilitadas&tabla=tarjetas_clientes&filtros=" + document.all("usrClieFil:txtId").value , 7);
		}
		
		function mEstadosAbrir()
	   	{
	   	 mAbrirPop("Estado de Saldos","S");
	 	}
	  
	   function mEspecialesAbrir()
	   	{
	   	 mAbrirPop("Especificaciones Especiales","N");
	    }	

      function mAbrirPop(ptitulo,popcion)
       {
         var sFiltroClie = document.all("usrClieFil:txtId").value;
	    if (sFiltroClie != '')
         {
         
          mAbrirVentanas("../alertas_pop.aspx?titulo="+ptitulo+"&amp;"+"origen="+popcion+"&amp;"+"clieId=" + document.all("usrClieFil:txtId").value+"&amp;"+"FechaValor=" + document.all("txtFechaValor").value+"&amp;"+"sociId="+document.all("hdnSocioId").value, 7);
	     }else
	     {
	      alert("Debe ingresar el cliente.")
	     }		
	   }  

	   	function mAbrirVentanas(pstrPagina, pintOrden, pstrAncho, pstrAlto, pstrLeft, pstrTop)
		{
			//vVentanas[pintOrden] = window.open (pstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=" + pstrAncho + "px,height=" + pstrAlto + "px,left=" + pstrLeft + "px,top=" + pstrTop + "px");
			vVentanas[pintOrden] = window.open (pstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=700px,height=400px,left=10px,top=10px");
			
			for(i=0;i<11;i++)
			{
				if(vVentanas[i]!=null)
				{
					try{vVentanas[i].focus();}
					catch(e){vVentanas[i];}
					finally{;}
				}
			}
			
			vVentanas[pintOrden].focus();
		}
		
		function mCerrarVentanas()
		{
			for(i=0;i<11;i++)
			{
				if(vVentanas[i]!=null)
				{
					vVentanas[i].close();
					vVentanas[i]=null;
				}
			}
		}		
			   	    	
		function mAbrirRelac(pCotiId,pCompId,pCompDesc)
		{
			gAbrirVentanas("../consulta_pop.aspx?EsConsul=-1&tabla=comprob_relac&filtros="+pCompId+"&titulo="+pCompDesc, 0, 550, 400, 10, 10)
		}

		function usrClieFil_onchange()
		{
			var vstrRet = LeerCamposXML("clientes_soci", document.all("usrClieFil:txtId").value, "soci_id,soci_nume").split("|");
			document.all("hdnSocioId").value = "";			
			if(vstrRet!="")
			{
				document.all("hdnSocioId").value = vstrRet[0];
				document.all("txtSociFil").value = vstrRet[1];
			}
			mSetearPaneles();
		}

		function mSetearPaneles()
		{
			if (document.all("usrClieFil:txtId")!=null)
			{						
				document.all("btnOtrosDatos").disabled = (document.all("usrClieFil:txtId").value=="");
				if(document.all("usrClieFil:txtId").value!="")
				{
					if(document.all("tblEstadoSaldos")!=null)
					{
						// 13/08/2010 vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClieFil:txtId").value, "saldo_cta_cte_venc,saldo_social_venc,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras,Total_Acuses,Interes_Soc,Interes_Cte").split("|");
						vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClieFil:txtId").value, "saldo_cta_cte,saldo_social,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras,Total_Acuses,Interes_Soc,Interes_Cte").split("|");
						
						document.all("txtSaldoCaCte").value  = vstrRet[0];		// 13/08/2010
						document.all("txtSaldoCtaSoci").value  = vstrRet[1];	// 13/08/2010
						document.all("txtImpCuentaCtaCte").value  = vstrRet[2];
						document.all("txtImpCuentaCtaSocial").value  = vstrRet[3];
						document.all("txtTotalProfRRGG").value  = vstrRet[4];
						document.all("txtTotalProfLabo").value  = vstrRet[5];
						document.all("txtTotalProfExpo").value  = vstrRet[6];
						document.all("txtTotalProfOtras").value  = vstrRet[7];		
						document.all("txtImpAcuses").value  = vstrRet[08];		
						document.all("txtInteresSocial").value  = vstrRet[9];
						document.all("txtInteresCte").value  = vstrRet[10];				
						
						if(vstrRet[0]!=0||vstrRet[1]!=0||vstrRet[2]!=0||vstrRet[3]!=0)
							document.all("divEstadoSaldos").style.display="inline";
						else
							document.all("divEstadoSaldos").style.display="none";
						if(vstrRet[4]!=0||vstrRet[5]!=0||vstrRet[6]!=0||vstrRet[7]!=0||vstrRet[8]!=0)
							document.all("divProformas").style.display="inline";
						else
							document.all("divProformas").style.display="none";

												
					}                
					mTarjetas();				
					mEstadoSaldos();	            
					mEspeciales();
				}
				else
				{
					if(document.all("tblEstadoSaldos")!=null)
					{
						document.all("divEstadoSaldos").style.display="none";
						document.all("divProformas").style.display="none";
						document.all("hdnSocioId").value = '';
						document.all("txtSociFil").value = '';
					}
				}
			}
		}

		function mTarjetas()
        {
			var sRet=EjecutarMetodoXML("Utiles.Tarjetas", document.all("usrClieFil:txtId").value);
			document.all("DivimgTarj").style.display = "none";
			if (sRet == "-1") 
			{
				document.all("DivimgTarj").style.display ="inline";   // mostrar la imagen de la tarjeta  
				mSetearVentaTele();
			}
        } 
		function mSetearVentaTele()
		{   
			if (document.all("usrClieFil:txtId")!=null && document.all("usrClieFil:txtId").value!='')
			{
				// icono venta telefonica	
				var lstrImg = document.all("imgTarjTele").src;
				var strRet = LeerCamposXML("tarjetas_clientes", "@tacl_clie_id="+document.all("usrClieFil:txtId").value, "tacl_vta_tele");
				if (strRet == "0")
					lstrImg = lstrImg.replace('CreditCardTele.gif','CreditCard.gif');
				else
					lstrImg = lstrImg.replace('CreditCard.gif','CreditCardTele.gif');
				document.all("imgTarjTele").src = lstrImg;
			}
		}
        function mEspeciales()
        {
			document.all("DivimgEspecVerde").style.display = "none";
			document.all("DivimgEspecRojo").style.display = "none";
			var sRet=EjecutarMetodoXML("Utiles.Especiales", document.all("usrClieFil:txtId").value);
			if(""!=sRet)
			{      
				if (sRet == "V")
					document.all("DivimgEspecVerde").style.display = "inline";
				else
					document.all("DivimgEspecRojo").style.display = "inline";
			}
         }                 
		
		function mEstadoSaldos()
		{
			document.all("DivimgAlertaSaldosAmarillo").style.display = "none";
			document.all("DivimgAlertaSaldosVerde").style.display = "none";
			document.all("DivimgAlertaSaldosRojo").style.display = "none";
			    
			var sFiltro = document.all("usrClieFil:txtId").value + ";" + document.all("txtFechaValor").value;
			var sRet=EjecutarMetodoXML("Utiles.EstadoSaldos", sFiltro);
			if(""!=sRet)
		   		{      
					if (sRet == "A")
					document.all("DivimgAlertaSaldosAmarillo").style.display = "inline";
					else
					{
						if (sRet == "V") 
							document.all("DivimgAlertaSaldosVerde").style.display = "inline";
						else
							document.all("DivimgAlertaSaldosRojo").style.display = "inline";
					} 
				}   
		}
		
		function mAbrirComp(pCotiId,pCompId){
		var lstrPagina;
		if(pCotiId==30||pCotiId==100)
			lstrPagina = "../CobranzasAnul.aspx?comp_id=";
		else
			lstrPagina = "../ComprobantesAnul.aspx?comp_id=";
		
			lstrPagina += pCompId;
			gAbrirVentanas(lstrPagina, 1)
		}
		
		function mOtrasCuentasAbrir()
	   	{
          gAbrirVentanas("DeudaClienteAgru_pop.aspx?clie_id=" + document.all("usrClieFil:txtId").value, 2, "500","300");
	 	}
	 	
	 	function mSaldosACtaAbrir()
	   	{
          gAbrirVentanas("DeudaClienteSaldos_pop.aspx?clie_id=" + document.all("usrClieFil:txtId").value, 3);
	 	}
	 	
	 	function mProformasAbrir()
	   	{
          gAbrirVentanas("DeudaClienteProformas_pop.aspx?clie_id=" + document.all("usrClieFil:txtId").value, 4);
	 	}
	 	
	 	function mEnviarMail()
	 	{
          __doPostBack('btnMail','');
	 	}
	 	
		function mAcusesAbrir()
	   	{
          gAbrirVentanas("DeudaClienteAcuses.aspx?clie_id=" + document.all("usrClieFil:txtId").value, 5);
	 	}
		</SCRIPT>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion">Resumen de Cuenta</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="97%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="100%" align=right></td>
															<TD style="HEIGHT: 8px" align=right>
																<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="../imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" vAlign="middle" align=right>
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" background="../imagenes/formfdofields.jpg">
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right"></TD>
																		<TD align=right style="WIDTH: 18%; HEIGHT: 14px" align="right" valign=top>
																		</TD>
																		<TD height="40" align="right">
																			<TABLE id="TableAlertas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgTarj" style="DISPLAY: none" runat="server"><A id="imgTarj" href="javascript:mTarjetasAbrir();" runat="server"><IMG alt="Puede operar con tarjetas" src="../imagenes/CreditCard.gif" border="0" runat="server" id="imgTarjTele">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgAlertaSaldosAzul" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldos" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="../imagenes/CircAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosAmarillo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosV" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="../imagenes/CircAmarillo.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosVerde" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosA" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="../imagenes/CircVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosRojo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosR" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="../imagenes/CircRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgEspecAzul" style="DISPLAY: none" runat="server"><A id="imgEspecAzul" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="../imagenes/CircTrianAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecVerde" style="DISPLAY: none" runat="server"><A id="imgEspecVerde" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="../imagenes/CircTrianVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecRojo" style="DISPLAY: none" runat="server"><A id="imgEspecRojo" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="../imagenes/CircTrianRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																						<TD width=20>&nbsp;</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																						<CC1:BOTONIMAGEN id="btnOtrosDatos" runat="server" BorderStyle="None" ForeColor="Transparent" IncludesUrl="../includes/"
																						ImagesUrl="../imagenes/" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnOtrosDatos.gif" ImageOver="btnOtrosDatos2.gif"
																						BackColor="Transparent" ImageEnable="btnOtrosDatos.gif" ImageDisable="btnOtrosDatos0.gif"
																						ToolTip="Otros Datos" CambiaValor="False"></CC1:BOTONIMAGEN>	
																						</TD>
																					</TR>
																				</TABLE>																				
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" valign=top>
																			<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD nowrap width="100%">
																			<table border=0 width="100%" cellpadding=0 cellspacing=0>
																			<tr>
																			<td>
																			<UC1:CLIE id="usrClieFil" runat="server" width="100%" Tabla="Clientes" Saltos="1,1,1" FilDocuNume="True" FilLegaNume="True"
																				FilCUIT="True" FilSociNume="True" PermiModi="True" AceptaNull="False" Obligatorio=True CampoVal=Cliente
																				mbooIncluirDeshabilitados="True"></UC1:CLIE> <!--Dario 2013-05-21 no muestra la opcion de modificar SoloBusq="true" -->
																			</td>
																			</tr>
																			</table>
																		</TD>
																	</TR>
																	<TR>
																		<TD colspan=3 background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD align="right">
																			<asp:Label id="lblCriaFil" runat="server" cssclass="titulo" Width="87px">Raza/Criador</asp:Label>&nbsp;</TD>
																		<TD>
																			<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																			<td>
																				<cc1:combobox class="combo" id="cmbRazaFil" onchange="mCargarClienteCriador();" runat="server" cssclass="cuadrotexto" Width="200px"
																				AceptaNull="False" filtra="true" MostrarBotones="False" NomOper="razas_cargar" Height="20px"></cc1:combobox>
																			</td>
																			<td width=4></td>
																			<td>
																				<cc1:numberbox id="txtCriaFil" onchange="mCargarClienteCriador();" runat="server" cssclass="cuadrotexto" esdecimal="False" MaxValor="9999999999999"
																				Width="100px"></cc1:numberbox>
																			</td>
																			</tr>
																			</table>			
																		</td>
																	</TR>
																	<TR>
																		<TD colspan=3 background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD align="right">
																			<asp:Label id="lblSocifil" runat="server" cssclass="titulo" Width="87px">Socio</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtSociFil" runat="server" onchange="mCargarClienteSocio()" cssclass="cuadrotexto" esdecimal="False" MaxValor="9999999999999"
																						Width="100px"></cc1:numberbox>
																		</td>
																	</TR>
																	<TR>
																		<TD colspan=3 background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD align="right">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo" Width="87px">Desde:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<% If mstrTipo = "" then %>
																	<TR>
																		<TD colspan=3 background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<% End If %>
																	<TR>
																		<TD></TD>
																		<TD vAlign="center" align="right">
																			<asp:Label id="lblDeuda" runat="server" cssclass="titulo" >Deuda:&nbsp;</asp:Label></TD>
																		<TD>
																			<asp:RadioButton id="rbtDeudaA" runat="server" cssclass="titulo" Text="Ambas" Checked="True" GroupName="RadioGroup1"></asp:RadioButton>
																			<asp:RadioButton id="rbtDeudaC" runat="server" cssclass="titulo" Text="Cta.Cte." GroupName="RadioGroup1"></asp:RadioButton>
																			<asp:RadioButton id="rbtDeudaS" runat="server" cssclass="titulo" Text="Social" GroupName="RadioGroup1"></asp:RadioButton></TD>
																	</TR>
																	<TR>
																		<TD colspan=3 background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD vAlign="center" align="right">
																			<asp:Label id="lblOrden" runat="server" cssclass="titulo">Orden:</asp:Label>&nbsp;</TD>
																		<TD>
																			<asp:RadioButton id="rbtOrdenA" runat="server" cssclass="titulo" Text="Asc." Checked="True" GroupName="RadioGroup2"></asp:RadioButton>
																			<asp:RadioButton id="rbtOrdenD" runat="server" cssclass="titulo" Text="Desc." GroupName="RadioGroup2"></asp:RadioButton></TD>
																	</TR>
																	<TR>
																		<TD colspan=3 background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD noWrap align="right"></TD>
																		<TD>
																			<asp:checkbox id="chkSaldo" CssClass="titulo" Runat="server" Text="S�lo comprobantes con saldo"></asp:checkbox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD colspan=3 background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD noWrap align="right"></TD>
																		<TD>
																			<asp:checkbox id="chkListCuad" CssClass="titulo" Runat="server" Text="Listar s�lo Cuadros de Saldo"></asp:checkbox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD colspan=3>
																		<TABLE style="WIDTH: 100%" border=0> 
																		<TR>
																			<TD style="HEIGHT: 14px">
																				<DIV id="divEstadoSaldos" style="DISPLAY: none" runat=server>
																					<TABLE class="FdoFld" runat=server id="tblEstadoSaldos" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 300px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																						cellPadding="0" align="left" border="1">
																						<TR>
																							<TD align="right">
																								<asp:label id="lblSaldoCaCte" runat="server" cssclass="titulo">Saldo de Cta.Cte.:</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtSaldoCaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblSaldoCtaSoci" runat="server" cssclass="titulo">Saldo de Cta. Social :</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtSaldoCtaSoci" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpCuentaCtaCte" runat="server" cssclass="titulo">Importe a cuenta en Cta. Cte.:</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtImpCuentaCtaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpCuentaCtaSocial" runat="server" cssclass="titulo">Importe a cuenta en Cta. Social:</asp:label>&nbsp;
																							</TD>
																							<TD style="WIDTH: 70px">
																								<CC1:NUMBERBOX id="txtImpCuentaCtaSocial" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="Label1" runat="server" cssclass="titulo">Recargo en  Cta. Social:</asp:label>&nbsp;
																							</TD>
																							<TD style="WIDTH: 70px">
																								<CC1:NUMBERBOX id="txtInteresSocial" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="Label2" runat="server" cssclass="titulo">Recargo en Cta. Cte.:</asp:label>&nbsp;
																							</TD>
																							<TD style="WIDTH: 70px">
																								<CC1:NUMBERBOX id="txtInteresCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>


																					</TABLE>
																				</DIV>
																				</td>
																							<TD style="HEIGHT: 14px">
																							<DIV id="divProformas" style="DISPLAY: none" runat="server">
																								<TABLE class="FdoFld" runat="server" id="Table3" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 290px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																									cellPadding="0" align="left" border="0">
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfRRGG" runat="server" cssclass="titulo">Total Proformas RRGG:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfRRGG" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfLabo" runat="server" cssclass="titulo">Total Prof.Laboratorio:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfLabo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfExpo" runat="server" cssclass="titulo">Total Prof.Exposiciones:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfExpo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfOtras" runat="server" cssclass="titulo">Total Otras Proformas:</asp:label>&nbsp;
																										</TD>
																										<TD style="WIDTH: 70px">
																											<CC1:NUMBERBOX id="txtTotalProfOtras" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblImpAcuses" runat="server" cssclass="titulo">Acuses de Recibo Pend.de Proc.:</asp:label>&nbsp;
																										</TD>
																										<TD style="WIDTH: 70px">
																											<CC1:NUMBERBOX id="txtImpAcuses" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>																						

																								</TABLE>
																							</DIV>																							
																			</TD>
																		</TR>
																	</TABLE>																		
																		</TD>
																	</TR>
																	<TR>
																		<TD background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																		<TD background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																		<TD background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																	
																</TABLE>
															</TD>
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD width="3" height="10"></TD>
															<TD height="10"></TD>
															<TD width="2" height="10"></TD>
														</TR>
														<TR>
															<TD vAlign="top" align="center" colSpan="3">
																<asp:datagrid id="grdDatoBusq" runat="server" width="100%" BorderWidth="1px" BorderStyle="None"
																	PageSize="2000" AutoGenerateColumns="False" OnPageIndexChanged="DataGrid_Page" CellPadding="1"
																	GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" ShowFooter=True>
																	<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																	<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
																	<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
																	<FooterStyle cssclass="footer"></FooterStyle>
																	<Columns>
																		<asp:TemplateColumn>
																			<HeaderStyle Width="17px"></HeaderStyle>
																			<ItemTemplate>
																				<A href="javascript:mAbrirRelac(<%#DataBinder.Eval(Container, "DataItem.coti_id")%>,<%#DataBinder.Eval(Container, "DataItem.comp_id")%>,'<%#DataBinder.Eval(Container, "DataItem.numero")%>');">
																					<img runat=server id=lnk1 src='../imagenes/Buscar16.gif' border="0" alt="Ver Relacionados" style="cursor:hand;" />
																				</A>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn>
																			<HeaderStyle Width="13px"></HeaderStyle>
																			<ItemTemplate>
																				<A href="javascript:mAbrirComp(<%#DataBinder.Eval(Container, "DataItem.coti_id")%>,<%#DataBinder.Eval(Container, "DataItem.comp_id")%>);">
																					<img runat=server id=lnk2 src='../imagenes/edit.gif' border="0" alt="Ver Comprobante" style="cursor:hand;" />
																				</A>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:BoundColumn DataField="comp_id" Visible="False"></asp:BoundColumn>
																		<asp:BoundColumn DataField="fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																		<asp:BoundColumn DataField="numero" HeaderText="Comprobante"></asp:BoundColumn>
																		<asp:BoundColumn DataField="impo_ori" HeaderText="Importe Original" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="debe" HeaderText="Debe"  DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="haber" HeaderText="Haber" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="aplicado" HeaderText="Sin Aplicar" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="fecha_vto" HeaderText="Fecha Vto." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																	</Columns>
																	<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
																</asp:datagrid></TD>
														</TR>
														<TR>
															<TD width="3"></TD>
															<TD align="right">
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD><BUTTON class=boton id="btnOtrasCuentas" style="WIDTH: 110px" onclick=mOtrasCuentasAbrir(); type=button runat="server" value="Otras Cuentas">Otras Cuentas</BUTTON></TD>
																		<TD><BUTTON class=boton id="btnSaldosACta" style="WIDTH: 110px" onclick=mSaldosACtaAbrir(); type=button runat="server" value="Saldos a Cuenta">Saldos a Cuenta</BUTTON></TD>
																		<TD><BUTTON class=boton id="btnProformas" style="WIDTH: 85px" onclick=mProformasAbrir(); type=button runat="server" value="Proformas">Proformas</BUTTON></TD>
																		<TD><BUTTON class=boton id="btnAcuses" style="WIDTH: 65px" onclick=mAcusesAbrir(); type=button runat="server" value="Acuses">Acuses</BUTTON></TD>
																		<TD><BUTTON class=boton id="btnMail" style="WIDTH: 90px" onclick=mEnviarMail(); type=button runat="server" value="Enviar Mail">Enviar Mail</BUTTON></TD>
																		<TD align="right">
																			<CC1:BotonImagen id="btnList" visible=false runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
																				ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																				ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	</TR>
																</TABLE></td>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="txtFechaValor" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnSocioId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX textmode=MultiLine id="hdnMail" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX textmode=MultiLine id="hdnMailAsun" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX textmode=MultiLine id="hdnMailDire" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		mSetearPaneles();
		mSetearVentaTele();
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>