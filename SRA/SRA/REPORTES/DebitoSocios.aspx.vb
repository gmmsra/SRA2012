Namespace SRA

Partial Class DebitoSocios
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrActiId As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
                mCargarCombos()
                If (mstrActiId <> 2) Then
                    cmbTipoListado.Items.Clear()
                    cmbTipoListado.Items.Add("(Seleccione)")
                    cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "T"
                    cmbTipoListado.Items.Add("Tarjetas por Banco Pagador")
                    cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "TB"
                    cmbTipoListado.Items.Add("Bancos Pagadores por Tarjeta")
                    cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "BT"
                    cmbTipoListado.Items.Add("Reporte Mensual")
                    cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "SM"
                End If
            End If
        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mInicializar()
      If Not Request.QueryString("act") Is Nothing Then
         mstrActiId = Request.QueryString("act")
      Else
         mstrActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", Request.QueryString("fkvalor")).Tables(0).Rows(0).Item("inse_acti_id")
      End If
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstado, "T", "25")
      clsWeb.gCargarRefeCmb(mstrConn, "debitos_acti", cmbDeca, "T", "@acti_id=" + mstrActiId)
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjeta, "T")
      'clsWeb.gCargarRefeCmb(mstrConn, "debitos_tarjetas_fecha", cmbTarjeta, "T", "null")
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String
         Dim lintFecha As Integer
         Dim lstrAnio As String
         Dim lstrPerio As String

         If cmbDeca.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el Debito.")
         End If

         If cmbTipoListado.Valor.ToString = "T" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el Tipo de Listado.")
         End If

         'If cmbTarjeta.Items.Count > 1 And cmbTarjeta.Valor = 0 Then
         '   Throw New AccesoBD.clsErrNeg("Debe seleccionar la Tarjeta.")
         'End If

         Select Case cmbTipoListado.Valor.ToString
            Case "TB"
               lstrRptName = "TarjetasPorBancos"
            Case "BT"
               lstrRptName = "BancosPorTarjetas"
            Case "TC"
               lstrRptName = "TarjetasPorCategorias"
            Case "CT"
               lstrRptName = "CategoriasPorTarjetas"
            Case "SM"
               lstrRptName = "SociosMensual"
         End Select

         'If txtFechaFil.Fecha.ToString = "" Then
         '   lintFecha = 0
         'Else
         '   lintFecha = CType(clsFormatear.gFormatFechaString(txtFechaFil.Text, "Int32"), Integer)
         'End If

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrAnio = cmbDeca.Valor.ToString().Substring(0, 4)
         lstrPerio = cmbDeca.Valor.ToString().Substring(4, 2).Trim()

         'lstrRpt += "&deca_gene_fecha=" + lintFecha.ToString
         lstrRpt += "&deca_anio=" + lstrAnio
         lstrRpt += "&deca_perio=" + lstrPerio
         lstrRpt += "&deca_desc=" + cmbDeca.SelectedItem.Text
         lstrRpt += "&tarj_id=" + IIf(cmbTarjeta.Valor.ToString = "", "null", cmbTarjeta.Valor.ToString)
         If cmbTipoListado.Valor.ToString <> "SM" Then
            lstrRpt += "&dede_esta_id=" + IIf(cmbEstado.Valor.ToString = "", "null", cmbEstado.Valor.ToString)
            If cmbEstado.Valor.ToString <> "0" Then
                lstrRpt += "&esta_desc=" + cmbEstado.SelectedItem.Text
            Else
                lstrRpt += "&esta_desc= "
            End If
         End If
         lstrRpt += "&deca_acti_id=" + mstrActiId
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
