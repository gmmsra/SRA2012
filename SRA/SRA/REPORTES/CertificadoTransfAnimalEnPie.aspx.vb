Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports AccesoBD
Imports Interfaces.Importacion


Namespace SRA


Partial Class CertificadoTransfAnimalEnPie
    Inherits FormGenerico



#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtCriaNumeDesde As NixorControls.TextBoxTab
    'Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents lblSraNumeDesde As System.Web.UI.WebControls.Label
    Protected WithEvents lblSraNumeHasta As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents lblExp As System.Web.UI.WebControls.Label
    Protected WithEvents lblObservacionFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtObservacionFil As NixorControls.TextBoxTab
    Protected WithEvents lblPadreFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblMadreFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblReceFil As System.Web.UI.WebControls.Label
    Protected WithEvents divBusqAvanzada As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbSraNumeDesde As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mCargarCombos()
        'usrCriadorFil.FiltroRazas = "@raza_espe_id=0"
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=0", cmbRazaFil, "id", "descrip", "S")

        'usrCriadorFil.FiltroRazas = ""
        'clsWeb.gCargarCombo(mstrConn, "razas_cargar  @raza_espe_id=3", cmbRazaFil, "id", "descrip", "S")


    End Sub
    Private Sub mImprimirConcepto()
        Try
            Dim params As String
            Dim lstrRptName As String = "CertificadoTransfAnimalEnPie"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            params += "&hba_desde=" + IIf(txtSraNumeDesde.Text = "", "0", txtSraNumeDesde.Valor.ToString)
            params += "&hba_hasta=" + IIf(txtSraNumeHasta.Text = "", "0", txtSraNumeHasta.Valor.ToString)
            If cmbSexoFil.Valor.ToString <> "" Then
                params += "&sexo=" + cmbSexoFil.Valor.ToString
            End If

            params += "&raza_id=" & IIf(usrCriadorFil.RazaId.ToString = "", "0", _
                                       usrCriadorFil.RazaId.ToString)

            params += "&criador_id=" & IIf(usrCriadorFil.Valor = 0, "0", _
                                                usrCriadorFil.Valor.ToString())

            params += "&FilFechaInscripcion=" & IIf(rbtnFechaInscripcion.Checked, True, False)

            params += "&FechaDesde=" & IIf(txtFechaDesde.Fecha.ToString = "", "0", _
                    mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaDesde.Text))))


            params += "&FechaHasta=" & IIf(txtFechaHasta.Fecha.ToString = "", "0", _
           mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaHasta.Text))))

            params += "&tipoCertificado=" & Me.cmbCertifTipoFil.SelectedIndex

            params += "&audi_user=" & Session("sUserId").ToString()
            lstrRpt += params



            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt, False)





        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            mImprimirConcepto()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class
End Namespace
