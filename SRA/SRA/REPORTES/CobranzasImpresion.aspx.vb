Public Class CobranzasImpresion
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents usrClie As usrClieDeriv
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As NixorControls.BotonImagen
    'Protected WithEvents btnLimpiarFil As NixorControls.BotonImagen
    'Protected WithEvents lblClie As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCtro As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbCtro As NixorControls.ComboBox
    'Protected WithEvents lblFechaDesde As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaDesde As NixorControls.DateBox
    'Protected WithEvents lblFechaHasta As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaHasta As NixorControls.DateBox
    'Protected WithEvents lblNroComp As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbCompLetra As NixorControls.ComboBox
    'Protected WithEvents lblCompCemiNum As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCompCemiNum As NixorControls.NumberBox
    'Protected WithEvents lblCompNum As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCompNum As NixorControls.NumberBox
    'Protected WithEvents lblCoti As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbCoti As NixorControls.ComboBox
    'Protected WithEvents chkTodas As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkIncAnulados As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdDatoBusq As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnTodos As System.Web.UI.WebControls.Button
    'Protected WithEvents btnNinguno As System.Web.UI.WebControls.Button
    'Protected WithEvents hdnIds As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnIdsImpr As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnImprSele As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnImprId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnImprimio As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnAceptar As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents BtnImprimir As NixorControls.BotonImagen

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mdsDatos As New DataSet


#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try

            mstrConn = SRA.clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                mInicializar()
                mCargarCombos()
                Session(mSess(mstrTabla)) = Nothing
                SRA.clsWeb.gInicializarControles(sender, mstrConn)
            Else
                mdsDatos = Session(mSess(mstrTabla))
                If Not mdsDatos Is Nothing Then
                    'mObtenerIDs()
                End If
            End If
            'mSetearEventos()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try




    End Sub
    Private Sub BtnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnImprimir.Click


    End Sub
    Public Sub mConsultarBusc()
        Dim lstrFiltros As New System.Text.StringBuilder

        With lstrFiltros
            If cmbCoti.Valor.ToString = "28" Then
                .Append("exec proformas_impcomp_busq")
            Else
                .Append("exec " + mstrTabla + "_busq")

            End If
            .Append(" @coti_id=")
            .Append(cmbCoti.Valor)
            .Append(", @clie_id=")
            .Append(usrClie.Valor.ToString())
            .Append(", @emct_id=")
            .Append(cmbCtro.Valor.ToString())
            .Append(", @centro=")
            If Not Session("sCentroEmisorId") Is Nothing Then
                .Append(Session("sCentroEmisorId").ToString())
            Else
                .Append("0")
            End If
            .Append(", @fecha=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaDesde.Fecha))
            .Append(", @fechaHasta=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaHasta.Fecha))
            .Append(", @ComprobLetra='")
            .Append(cmbCompLetra.Valor.ToString())
            '.Append("', @ComprobNroCenEmi=")
            '.Append(IIf(txtCompCemiNum.Valor.ToString = "", "0", txtCompCemiNum.Valor.ToString))
            .Append("', @ComprobNumero=")
            .Append(IIf(txtCompNum.Valor.ToString = "", "0", txtCompNum.Valor.ToString))
            .Append(", @todos=")
            .Append(Math.Abs(CInt(chkTodas.Checked)).ToString)
            .Append(",@incAnulados=")
            .Append(Math.Abs(CInt(chkIncAnulados.Checked)).ToString)
            If cmbCoti.Valor.ToString <> "28" Then .Append(",@reimp=1")


            mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, .ToString)
            Session(mSess(mstrTabla)) = mdsDatos

            If mdsDatos.Tables(0).Rows.Count = 0 Then
                grdDatoBusq.CurrentPageIndex = 0
            End If

            grdDatoBusq.DataSource = mdsDatos.Tables(0)
            grdDatoBusq.DataBind()

            SRA.clsWeb.gCargarDataGrid(mstrConn, "select descrip from opciones", grdDatoBusq)
            'hdnIds.Text = ""
            'hdnIdsImpr.Text = ""

        End With
    End Sub



    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        Try

            grdDatoBusq.CurrentPageIndex = 0
            mConsultarBusc()

            btnAceptar.Visible = True
            btnNinguno.Visible = True
            btnTodos.Visible = True

            grdDatoBusq.Visible = True





        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function

    Public Sub mInicializar()
        Dim lstrParaPageSize As String
        clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
        grdDatoBusq.PageSize = Convert.ToInt32(lstrParaPageSize)
        btnAceptar.Visible = False
        btnNinguno.Visible = False
        btnTodos.Visible = False

    End Sub
    Private Sub mCargarCombos()
        SRA.clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbCoti, "", "@coti_ids='" & SRA_Neg.Constantes.ComprobTipos.Recibo & "," & SRA_Neg.Constantes.ComprobTipos.Recibo_Acuse & "," & SRA_Neg.Constantes.ComprobTipos.Factura & "," & SRA_Neg.Constantes.ComprobTipos.ND & "," & SRA_Neg.Constantes.ComprobTipos.NC & "," & SRA_Neg.Constantes.ComprobTipos.Proforma & "'")
        SRA.clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCtro, "T", "")
        If Not Session("sCentroEmisorId") Is Nothing Then
            cmbCtro.Valor = Session("sCentroEmisorId")
        End If
        If Session("sCentroEmisorCentral") <> "S" Then
            cmbCtro.Enabled = False
        End If
        cmbCoti.Valor = CInt(SRA_Neg.Constantes.ComprobTipos.Factura)
    End Sub
End Class
