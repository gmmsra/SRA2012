<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CertificadoRegPeliferos" CodeFile="CertificadoRegPeliferos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Certificado de Registros Peliferos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../stylesheet/SRA.css">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
		 
		    	function cmbCriadorFil_change()
		{
		    if(document.all("cmbCriadorFil").value!="")
		    {
				document.all("usrCriadorFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriadorFil:cmbRazaCria").onchange();
				document.all("usrCriadorFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriadorFil:cmbRazaCria").disabled = true;
				
		
			
			}
			else
			{
				document.all("usrCriadorFil:cmbRazaCria").value = ""
				document.all("usrCriadorFil:cmbRazaCria").onchange();
				document.all("usrCriadorFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriadorFil:cmbRazaCria").disabled = false;
			
			
			}
		}	

/****** CONTROL CLIENTES *******/
/*
function CodiClieDeriv_change(strId, strAutoPB, strArgs, strCampoVal)
{
	var oCoti = document.all(strId + ":txtCodi");
	var opc = oCoti.value;
	if (opc == -1)
		oCoti.value=0;
		
	if (oCoti.value==0)
		document.all(strId + ":txtCodi").value = '';

	var sFiltro = oCoti.value;
	var sIdOri = document.all(strId + ":txtId").value;

	document.all(strId + ":txtApel").value = "";
	document.all(strId + ":txtId").value = "";

       
    var boolResp= RazaOrCriador();
	
	
	if (!booIsNumber(sFiltro)||(sFiltro=="")||(boolResp==true) )
	{
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";

		if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
		{
			if (opc != -1)
			{
				document.all(strId + ":cmbRazaCria").value = document.all(strId + ":txtRazaSessId").value;	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
		}

		if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
	}
	else	
	{
		if (document.all[strId + ":cmbRazaCria"]!= null)
		strArgs = strArgs + ";" + document.all[strId + ":cmbRazaCria"].value;
		    	
		sFiltro += ";" + strArgs;
		
		var sRet=EjecutarMetodoXML("Utiles.BuscarClieDeriv", sFiltro);
			
		if(sRet=="")
		{
			alert(strCampoVal + " inexistente")
			document.all(strId + ":txtApel").value = "";
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = "";
			document.all(strId + ":txtCodi").value = '';

			oCoti.focus();
			
			if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
		}
		else
		{
			var vsRet=sRet.split("|");
			document.all(strId + ":txtApel").value = vsRet[2];
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = vsRet[3];
			if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
			{
				document.all(strId + ":cmbRazaCria").value =  vsRet[4];	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
			document.all(strId + ":txtId").value = vsRet[0];

			try{eval(strId + "Id_onchange();")}
			catch(e){;}
			
			if(strAutoPB=="1")
				__doPostBack(strId + ":txtId",'');
		}
	}
	if (document.getElementById(strId + ":txtApel").value!="")
	{
		var range = document.selection.createRange();
		document.getElementById(strId + ":txtApel").focus();
		if (window.event!=null)
			window.event.returnValue = false;
	}
	
	try{eval(strId + "_onchange();")}
	catch(e){;}
}
*/
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="../imagenes/recsup.jpg"><IMG border="0" src="../imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="../imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="../imagenes/reciz.jpg" width="9"><IMG border="0" src="../imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Certificado de Registro de Peliferos</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!-- FOMULARIO -->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblCriaNumeHasta" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																					<cc1:combobox style="Z-INDEX: 0" id="cmbSexoFil" class="combo" runat="server" Width="136px" Height="38">
																						<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																						<asp:ListItem Value="false">Hembra</asp:ListItem>
																						<asp:ListItem Value="true">Macho</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 22px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label style="Z-INDEX: 0" id="Label1" runat="server" cssclass="titulo" Width="93px">RICH Desde:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 22px" background="../imagenes/formfdofields.jpg" align="left">
																					<CC1:TEXTBOXTAB id="txtSraNumeDesde" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 2px" height="2" background="../imagenes/formdivmed.jpg" colSpan="3"
																					align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 23px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label style="Z-INDEX: 0" id="Label2" runat="server" cssclass="titulo" Width="88px">RICH Hasta:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 23px" background="../imagenes/formfdofields.jpg" align="left">
																					<CC1:TEXTBOXTAB id="txtSraNumeHasta" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																				<TR>
																				<TD style="WIDTH: 8.91%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo" Width="58px">Criador: </asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriadorFil" runat="server" MostrarBotones="False" AceptaNull="false" FilCUIT="True"
																						ColCUIT="True" FilTarjNume="False" FilAgru="False" ColCriaNume="True" FilClaveUnica="True"
																						CampoVal="Criador" Criador="True" FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True"
																						Saltos="1,2" Tabla="Criadores"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		
																			<TR>
																				<TD style="WIDTH: 211px" background="../imagenes/formfdofields.jpg" width="211" align="right">
																					<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<asp:RadioButton id="rbtnFechaAprobacion" runat="server" cssclass="titulo" Width="144px" checked="True"
																						GroupName="RadioGroup1" Text="Fecha Aprobación"></asp:RadioButton>
																					<asp:RadioButton id="rbtnFechaInscripcion" runat="server" cssclass="titulo" Width="149px" checked="False"
																						GroupName="RadioGroup1" Text="Fecha Inscripción"></asp:RadioButton>&nbsp;&nbsp;
																					<asp:Label style="Z-INDEX: 0" id="lblDesde" runat="server" cssclass="titulo">Desde:</asp:Label>
																					<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																						enabled="true"></cc1:DateBox>
																					<asp:Label style="Z-INDEX: 0" id="lblHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																					<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																						enabled="true"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblFiltro" runat="server" cssclass="titulo">Filtro:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																					<cc1:combobox id="cmbCertifTipoFil" class="combo" runat="server" Width="113px">
																						<asp:ListItem Selected="True">(Todos)</asp:ListItem>
																						<asp:ListItem Value="false">Pendientes</asp:ListItem>
																						<asp:ListItem Value="true">Originales</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																		</TABLE> <!-- fin FOMULARIO --></TD>
																	<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																			ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"
																			CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro ---></TBODY></TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="../imagenes/recde.jpg" width="13"><IMG border="0" src="../imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="../imagenes/recinf.jpg"><IMG border="0" src="../imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="../imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">&nbsp;</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
	</BODY>
</HTML>
