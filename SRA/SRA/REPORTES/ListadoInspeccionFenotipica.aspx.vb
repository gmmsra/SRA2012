
Imports ReglasValida.Validaciones
Imports SRA_Neg.Establecimientos


Namespace SRA



Partial Class ListadoInspeccionFenotipica
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblDesde As System.Web.UI.WebControls.Label
    Protected WithEvents lblHasta As System.Web.UI.WebControls.Label
    Protected WithEvents lblRPDde As System.Web.UI.WebControls.Label
 
    Protected WithEvents lblClie As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then

                Me.txtFecha.Fecha() = Now
            End If


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    'GSZ Modificado 18/11/2014

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "ListadoInspeccionFenotipica"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim strEstablDescripcion As String = ""
            '    Dim strOrden As String = ""
            Dim intCliente As Integer = 0
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer

            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim oEstabl As New SRA_Neg.Establecimientos(mstrConn, Session("sUserId").ToString())
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
            Dim stRazaDescripcion As String

            If (usrCriadorFil.RazaId.ToString.Trim = "0" Or usrCriadorFil.RazaId.ToString.Trim = "") Or _
            (usrCriadorFil.Valor.ToString.Trim = "0" Or usrCriadorFil.Valor.ToString.Trim = "") Then
                clsError.gGenerarMensajes(Me, "La raza y el criador  deben ser distinto de vacio.")
                Return
            End If


            If Not (dtfhnacidesde.Fecha Is DBNull.Value) And Not (dtfhnacihasta.Fecha Is DBNull.Value) Then
                If dtfhnacidesde.Fecha > dtfhnacihasta.Fecha Then
                    Throw New AccesoBD.clsErrNeg("La fecha  de Nacimiento Desde no puede ser mayor a la fecha Hasta.")
                End If
            End If


            If dtfhnacidesde.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(dtfhnacidesde.Text, "Int32"), Integer)
            End If

            If dtfhnacihasta.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(dtfhnacihasta.Text, "Int32"), Integer)
            End If


            lstrRpt += "&fechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&fechaHasta=" + lintFechaHasta.ToString


            If cmbSexo.Valor.ToString <> "" Then
                'hembra=0
                'macho= 1

                lstrRpt += "&sexo=" + IIf(cmbSexo.Valor = 0, "False", "True")
            End If

            'If cmbOrden.Valor.ToString <> "" Then
            '    lstrRpt += "&Orden=" + cmbOrden.Valor.ToString
            'End If



            If txtRPDde.Text.ToString <> "" Then
                lstrRpt += "&RPNDesde=" & txtRPDde.Valor
            Else
                lstrRpt += "&RPNDesde=0"
            End If

            If txtRPHta.Text.ToString <> "" Then
                lstrRpt += "&RPNHasta=" & txtRPHta.Valor
            Else
                lstrRpt += "&RPNHasta=0"
            End If

            lstrRpt += "&raza_id=" & usrCriadorFil.RazaId.ToString
            lstrRpt += "&criador_id=" & usrCriadorFil.Valor.ToString

            ' lstrRpt += "&Orden=" & strOrden

            lstrRpt += "&NombreCliente=" & IIf(usrCriadorFil.Apel.ToString.Trim = "", "", _
                        usrCriadorFil.Apel.ToString.Trim)

            If usrCriadorFil.Valor.ToString <> "" Then
                intCliente = oCliente.GetClienteIdByRazaCriador(usrCriadorFil.RazaId.ToString, usrCriadorFil.Valor.ToString)
                strEstablDescripcion = oEstabl.GetEstablecimientobyClienteId(intCliente.ToString)
            End If

            stRazaDescripcion = oRaza.GetRazaDescripcionById( _
                                           IIf(usrCriadorFil.cmbCriaRazaExt.SelectedValue.ToString = "", "0", _
                                           usrCriadorFil.cmbCriaRazaExt.SelectedValue.ToString)).Trim()

            lstrRpt += "&EstablecimientoDesc=" & strEstablDescripcion.Trim()

            lstrRpt += "&RazaDescripcion=" & _
                     oRaza.GetRazaCodiById(usrCriadorFil.cmbCriaRazaExt.SelectedValue.ToString).ToString + _
                     "-" + stRazaDescripcion.Trim()

            lstrRpt += "&cria_nume=" & usrCriadorFil.txtCodiExt.Text

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click


        txtFecha.Text = ""
        txtRPDde.Text = ""
        txtRPHta.Text = ""
        dtfhnacidesde.Text = ""
        dtfhnacihasta.Text = ""
        'cmbOrden.Limpiar()
        cmbSexo.Limpiar()
        usrCriadorFil.Limpiar()



    End Sub

   
End Class

End Namespace
