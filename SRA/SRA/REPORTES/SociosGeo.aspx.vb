Namespace SRA

Partial Class SociosGeo
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFiltros()
      usrSocFil.Limpiar()
      cmbPais.Limpiar()
      cmbPcia.Items.Clear()
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "")
      clsWeb.gCargarCombo(mstrConn, "provincias_cargar @pcia_pais_id=" & cmbPais.Valor.ToString, cmbPcia, "id", "descrip", "T")
   End Sub

#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try

         If cmbPais.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar un pais.")
         End If

         Dim lstrRptName As String = "SociosGeoDistri"

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         hdnRptId.Text = usrSocFil.ObtenerIdFiltro.ToString

         lstrRpt += "&rptf_id=" + hdnRptId.Text
         lstrRpt += "&pais_id=" + cmbPais.Valor.ToString
         lstrRpt += "&pcia_id=" + IIf(cmbPcia.Valor.ToString = "", "0", cmbPcia.Valor.ToString)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
