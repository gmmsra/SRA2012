<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.NacimientosRetenidos" CodeFile="NacimientosRetenidos.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="../controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PRODFIL" Src="../controles/usrProdFiltro.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reporte de Nacimientos Retenidos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
	
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion">Listado de Inscripciones Retenidas</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD width="100%"><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.91%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>
																			<cc1:DateBox id="txtFechaInscripcion" runat="server" cssclass="cuadrotextodeshab" Width="60px"
																				Obligatorio="true" enabled="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.91%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo" Width="58px">Raza/Criador: </asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<UC1:CLIE id="usrCriadorFil" runat="server" Tabla="Criadores" Saltos="1,2" FilSociNume="True"
																				FilTipo="S" MuestraDesc="False" FilDocuNume="True" Criador="True" CampoVal="Criador" FilClaveUnica="True"
																				ColCriaNume="True" FilAgru="False" FilTarjNume="False" ColCUIT="True" FilCUIT="True" MostrarBotones="False"
																				AceptaNull="false"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblLote" runat="server" cssclass="titulo">Lote:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<CC1:TEXTBOXTAB id="txtLote" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																				obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblTramite" runat="server" cssclass="titulo">Tramite:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<CC1:TEXTBOXTAB id="txtNroTramite" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																				obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.28%; HEIGHT: 11px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">RP   Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 11px" background="../imagenes/formfdofields.jpg" align="left">
																			<CC1:TEXTBOXTAB id="txtRPDesde" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																				obligatorio="True"></CC1:TEXTBOXTAB>&nbsp;&nbsp;&nbsp;
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<CC1:TEXTBOXTAB id="txtRPHasta" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																				obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblfhpredesde" runat="server" cssclass="titulo">Fecha Presentacion desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<cc1:DateBox id="dtfhpresdesde" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																				enabled="true"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label id="lblfhpreshasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="dtfhpreshasta" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																				enabled="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblfhnacidesde" runat="server" cssclass="titulo">Fecha naci. desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<cc1:DateBox id="dtfhnacidesde" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																				enabled="true"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label id="lblfhnacihasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="dtfhnacihasta" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																				enabled="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 109px" background="../imagenes/formfdofields.jpg" noWrap align="right"></TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<asp:checkbox style="Z-INDEX: 0" id="chkIncluyeProdBaja" Text="Incluye productos dados de Baja"
																				Runat="server" CssClass="titulo" Checked="true"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
														<TR>
															<TD height="10" colSpan="3"></TD>
														</TR>
														<TR>
															<TD colSpan="3" align="right">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																	ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																	CausesValidation="False"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																	ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
			<asp:textbox id="hdnRptId" runat="server" Visible="False"></asp:textbox></form>
	</BODY>
</HTML>
