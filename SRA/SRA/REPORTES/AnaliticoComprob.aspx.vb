Namespace SRA

Partial Class AnaliticoComprob
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEdesde, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEhasta, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbTipo, "T", "@coti_ids = '29,31,32'")
    End Sub
#End Region

#Region "Detalle"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer
            lstrRptName = "AnaliticoComprob"

            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtFechaDesde.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
            End If

            If txtFechaHasta.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
            End If

            If (Not txtNume.Valor Is DBNull.Value) Or (txtLetra.Text <> String.Empty) Or (Not txtCemiNume.Valor Is DBNull.Value) Then
                If txtNume.Valor Is DBNull.Value Then
                    Throw New clsErrNeg("Debe ingresar el Nro de comprobante.")
                End If
                If txtLetra.Text = String.Empty Then
                    Throw New clsErrNeg("Debe ingresar la letra")
                End If
                If txtCemiNume.Valor Is DBNull.Value Then
                    Throw New clsErrNeg("Debe ingresar el Nro. de centro emisor.")
                End If
                If cmbTipo.SelectedValue = String.Empty Then
                    Throw New clsErrNeg("Debe ingresar el tipo de comprobante.")
                End If
            End If

            lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&CEmDesde=" + IIf(cmbCEdesde.Valor.ToString = "", "0", cmbCEdesde.Valor.ToString)
            lstrRpt += "&CEmHasta=" + IIf(cmbCEhasta.Valor.ToString = "", "0", cmbCEhasta.Valor.ToString)
            lstrRpt += "&CompLetra=" + txtLetra.Text
            lstrRpt += "&CompCemi=" + IIf(txtCemiNume.Valor.ToString() = "", "0", txtCemiNume.Valor)
            lstrRpt += "&CompNume=" + IIf(txtNume.Valor.ToString() = "", "0", txtNume.Valor)
            lstrRpt += "&CompCoti=" + IIf(cmbTipo.Valor.ToString = "", "0", cmbTipo.Valor)
            lstrRpt += "&Totaliza=" + IIf(chkTotalizado.Checked = True, "S", "N")
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class

End Namespace
