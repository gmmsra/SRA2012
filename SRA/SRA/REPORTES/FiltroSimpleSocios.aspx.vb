Namespace SRA

Partial Class FiltroSimpleSocios
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Public mstrTabla As String
   Public mstrTitu As String
   Public mstrReport As String
   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Inicializacion de Variables"
   Private Sub mInicializar()
      mstrTitu = Request.QueryString("ti")
      mstrReport = Request.QueryString("rp")
      lblTitu.Text = mstrTitu

      hdnTipo.Text = Request.QueryString("t")
      Select Case Request.QueryString("t")
         Case "C"
            lblFecha.Visible = True
            txtFecha.Visible = True
      End Select

   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = mstrReport
         Dim lstrRpt As String
         Dim lintFecha As Integer

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)


         If usrSociFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un socio")
         End If


       

         lstrRpt += "&soci_id=" + usrSociFil.Valor.ToString

         If txtFecha.Visible Then
            If txtFecha.Text = "" Then
               Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha hasta la cual se calcularán intereses.")
            Else
               lintFecha = CType(clsFormatear.gFormatFechaString(txtFecha.Text, "Int32"), Integer)
            End If

            lstrRpt += "&fecha=" & lintFecha.ToString


         End If
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
