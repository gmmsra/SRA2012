Namespace SRA

Partial Class SolicitudIngreso
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "soli_ingreso"

   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      'Introducir aqu� el c�digo de usuario para inicializar la p�gina
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
				' mCargarEstados() Pantanettig 02/10/2007
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
	End Sub

	'Pantanettig 02/10/2007
	' Private Sub mCargarEstados()
	'clsWeb.gCargarCombo(mstrConn, "estados_cargar " & SRA_Neg.Constantes.EstaTipos.EstaTipo_Solicitud_Ingreso, cmbEsta, "id", "descrip", "S", False)
	' End Sub
	'-----------------------
#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "SolicitudIngreso"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)


         Dim lintFechaD As Integer
         Dim lintFechaH As Integer

			If txtFechaDesdeFil.Text = "" Then
				lintFechaD = 0
			Else
				lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
			End If

			If txtFechaHastaFil.Text = "" Then
				lintFechaH = 0
			Else
				lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
			End If

			lstrRpt += "&soin_nume_desde=" + txtNroDde.Valor.ToString
			lstrRpt += "&soin_nume_hasta=" + txtNroHta.Valor.ToString
			lstrRpt += "&fecha_desde=" + lintFechaD.ToString
			lstrRpt += "&fecha_hasta=" + lintFechaH.ToString
			lstrRpt += "&soin_clie_id=" + usrClieFil.Valor.ToString
			lstrRpt += "&soin_esta_id=" + cmbEsta.Valor.ToString
            lstrRpt += "&resu=" + IIf(chkResu.Checked, "S", "N")
            lstrRpt += "&esta_descrip=" + cmbEsta.SelectedItem.Text.ToString

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      txtNroDde.Valor = ""
      txtNroHta.Valor = ""
      txtNroDde.Text = ""
      txtNroHta.Text = ""
      usrClieFil.Valor = ""
   End Sub
#End Region


End Class

End Namespace
