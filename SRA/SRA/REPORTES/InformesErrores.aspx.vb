Namespace SRA

Partial Class InformesErrores
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

	'El Dise�ador de Web Forms requiere esta llamada.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
	Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
	Protected WithEvents panDato As System.Web.UI.WebControls.Panel
	Protected WithEvents panBotones As System.Web.UI.WebControls.Panel
	Protected WithEvents lblProv As System.Web.UI.WebControls.Label
	Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
	Protected WithEvents panCabecera As System.Web.UI.WebControls.Panel
	Protected WithEvents cmbMsgFil As NixorControls.ComboBox
	Protected WithEvents lblVige As System.Web.UI.WebControls.Label
	Protected WithEvents Label2 As System.Web.UI.WebControls.Label
	Protected WithEvents lblNumDesde As System.Web.UI.WebControls.Label
	Protected WithEvents lblDescFil As System.Web.UI.WebControls.Label
	'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
	'No se debe eliminar o mover.

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
		'No la modifique con el editor de c�digo.
		InitializeComponent()
	End Sub

#End Region

#Region "Definici�n de Variables"
	Private mstrCmd As String
	Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)

			If (Not Page.IsPostBack) Then
				mSetearMaxLength()
				mSetearEventos()
				mCargarCombos()
				clsWeb.gInicializarControles(Me, mstrConn)
			End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mCargarCombos()
		clsWeb.gCargarRefeCmb(mstrConn, "rg_procesos", cmbProc, "S")
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "T")
		clsWeb.gCargarCombo(mstrConn, "rg_mensajes_cargar", cmbErrorDesde, "codi", "descrip", "T")
		clsWeb.gCargarCombo(mstrConn, "rg_mensajes_cargar", cmbErrorHasta, "codi", "descrip", "T")
		clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbAsoc, "id", "descrip", "T")
	End Sub
	Private Sub mSetearEventos()

	End Sub
	Private Sub mSetearMaxLength()

	End Sub
#End Region

#Region "Inicializacion de Variables"
	'Public Sub mInicializar()
	'   clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
	'   grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
	'End Sub

#End Region

#Region "Eventos de Controles"

	Private Sub mLimpiarFiltros()
		cmbMsgFil.Limpiar()
		cmbProc.Limpiar()

	End Sub

#End Region

	Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltros()
	End Sub

	Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
		Try
			Dim lstrRptName As String
			Dim lstrRpt As String

			If cmbTipoReporte.SelectedValue.ToString() = "" Then
				Response.Write("<SCRIPT language='javascript' for=window event=onload>alert('Debe seleccionar el Tipo de Reporte.');</script>")
			ElseIf cmbProc.SelectedValue.ToString() = "" Then
				Response.Write("<SCRIPT language='javascript' for=window event=onload>alert('Debe seleccionar Proceso.');</script>")
			Else


				If cmbTipoReporte.SelectedValue = 1 Then
					If cmbProc.SelectedValue = 1 Then
						lstrRptName = "InformesErroresNacimientos"
					ElseIf cmbProc.SelectedValue = 2 Then
						lstrRptName = "InformesErroresServicios"
					ElseIf cmbProc.SelectedValue = 6 Then
						lstrRptName = "InformesErroresRecuImpl"
					ElseIf cmbProc.SelectedValue = 12 Or cmbProc.SelectedValue = 13 Or cmbProc.SelectedValue = 14 Or cmbProc.SelectedValue = 5 Or cmbProc.SelectedValue = 9 Or cmbProc.SelectedValue = 10 Or cmbProc.SelectedValue = 11 Or cmbProc.SelectedValue = 15 Or cmbProc.SelectedValue = 16 Then
						lstrRptName = "InformesErroresTramites"
					ElseIf cmbProc.SelectedValue = 7 Then
						lstrRptName = "InformesErroresDeclaNaci"
					ElseIf cmbProc.SelectedValue = 8 Then
						lstrRptName = "InformesErroresPlantelMovi"
					End If

					lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
					lstrRpt += mFiltrosReporte()
					lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
					Response.Redirect(lstrRpt)

				ElseIf cmbTipoReporte.SelectedValue = 2 Then

					lstrRptName = "InfoProductosPorErrores"
					lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
					lstrRpt += mFiltrosReporte()
					lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
					Response.Redirect(lstrRpt)

				ElseIf cmbTipoReporte.SelectedValue = 3 Then
					Dim ds As New DataSet
					lstrRpt = mFiltrosMail()
					ds = clsSQLServer.gExecuteQuery(mstrConn, lstrRpt)
					SRA_Neg.clsMail.gEnviarMailErrores(mstrConn, ds.Tables(1), ds.Tables(0), cmbProc.SelectedValue)
					Response.Write("<SCRIPT language='javascript'>alert('Los mail�s han sido enviados con exito.');</script>")
				End If


			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Function mFiltrosReporte() As String
		Dim lstrRpt As String

		lstrRpt += "&raza_id=" + IIf(cmbRaza.Valor.ToString = "", "0", cmbRaza.Valor.ToString)
		lstrRpt += "&sexo=" + IIf(cmbSexo.SelectedValue.ToString = "", "0", cmbSexo.SelectedValue.ToString)
		lstrRpt += "&cria_nume_desde=" + IIf(txtCriaNumeDesde.Text = "", "0", txtCriaNumeDesde.Text)
		lstrRpt += "&cria_nume_hasta=" + IIf(txtCriaNumeHasta.Text = "", "0", txtCriaNumeHasta.Text)
		lstrRpt += "&prop_clie_id=" + IIf(usrPropClie.ClieId.ToString = "", "0", usrPropClie.ClieId.ToString)
		lstrRpt += "&error_codi_desde=" + IIf(cmbErrorDesde.Valor.ToString = "", "0", cmbErrorDesde.Valor.ToString)
		lstrRpt += "&error_codi_hasta=" + IIf(cmbErrorHasta.Valor.ToString = "", "0", cmbErrorHasta.Valor.ToString)
		lstrRpt += "&sra_nemu_desde=" + IIf(txtNumeDesde.Text = "", "0", txtNumeDesde.Text)
		lstrRpt += "&sra_nume_hasta=" + IIf(txtNumeHasta.Text = "", "0", txtNumeHasta.Text)
		lstrRpt += "&rp_desde=" + IIf(txtRPnumeDesde.Text = "", "0", txtRPnumeDesde.Text)
		lstrRpt += "&rp_hasta=" + IIf(txtRPnumeHasta.Text = "", "0", txtRPnumeHasta.Text)
		lstrRpt += "&asoc_id=" + IIf(cmbAsoc.Valor.ToString = "", "0", cmbAsoc.Valor.ToString)
		lstrRpt += "&proceso=" + IIf(cmbProc.Valor.ToString = "", "0", cmbProc.Valor.ToString)
		lstrRpt += "&icluir_criadores='N'"

		Return (lstrRpt)
	End Function

	Private Function mFiltrosMail() As String
		Dim lstrRpt As String

		lstrRpt += "rg_informes_errores_rpt_consul @raza_id=" + IIf(cmbRaza.Valor.ToString = "", "0", cmbRaza.Valor.ToString)
		lstrRpt += ",@sexo=" + IIf(cmbSexo.SelectedValue.ToString = "", "0", cmbSexo.SelectedValue.ToString)
		lstrRpt += ",@cria_nume_desde=" + IIf(txtCriaNumeDesde.Text = "", "0", txtCriaNumeDesde.Text)
		lstrRpt += ",@cria_nume_hasta=" + IIf(txtCriaNumeHasta.Text = "", "0", txtCriaNumeHasta.Text)
		lstrRpt += ",@prop_clie_id=" + IIf(usrPropClie.ClieId.ToString = "", "0", usrPropClie.ClieId.ToString)
		lstrRpt += ",@error_codi_desde=" + IIf(cmbErrorDesde.Valor.ToString = "", "0", cmbErrorDesde.Valor.ToString)
		lstrRpt += ",@error_codi_hasta=" + IIf(cmbErrorHasta.Valor.ToString = "", "0", cmbErrorHasta.Valor.ToString)
		lstrRpt += ",@sra_nemu_desde=" + IIf(txtNumeDesde.Text = "", "0", txtNumeDesde.Text)
		lstrRpt += ",@sra_nume_hasta=" + IIf(txtNumeHasta.Text = "", "0", txtNumeHasta.Text)
		lstrRpt += ",@rp_desde=" + IIf(txtRPnumeDesde.Text = "", "0", txtRPnumeDesde.Text)
		lstrRpt += ",@rp_hasta=" + IIf(txtRPnumeHasta.Text = "", "0", txtRPnumeHasta.Text)
		lstrRpt += ",@asoc_id=" + IIf(cmbAsoc.Valor.ToString = "", "0", cmbAsoc.Valor.ToString)
		lstrRpt += ",@proceso=" + IIf(cmbProc.Valor.ToString = "", "0", cmbProc.Valor.ToString)
		lstrRpt += ",@icluir_criadores='S'"
		Return (lstrRpt)
	End Function

End Class
End Namespace
