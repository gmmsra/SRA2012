Namespace SRA

Partial Class DebiAutoCupones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_DebitosCabe

   Private Enum ColumnasFil As Integer
      lnkEdit = 0
      comp_id = 1
      fecha = 2
      tico_desc = 3
      comp_nro = 4
      cliente = 5
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mEstablecerPerfil()
            mInicializar()
            mCargarCombos()
            mConsultarBusc()

            clsWeb.gInicializarControles(sender, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mEstablecerPerfil()
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPetiFil, "T", SRA_Neg.Constantes.PeriodoTipos.Bimestre)
      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjFil, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "textos_comunicados", cmbTexto, "S")
   End Sub

   Public Sub mInicializar()
      Dim lstrParaPageSize As String
      clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
      grdDatoBusq.PageSize = Convert.ToInt32(lstrParaPageSize)
   End Sub
#End Region

   Private Sub btnBuscar_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
      Try
         grdDatoBusq.CurrentPageIndex = 0
         mConsultarBusc()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDatoBusq.CurrentPageIndex = E.NewPageIndex
         mConsultarBusc()

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Public Sub mConsultarBusc()
      Dim lstrCmd As New StringBuilder

      lstrCmd.Append("exec " + mstrTabla + "_consul")
      lstrCmd.Append(" @deca_anio=" + txtAnioFil.Valor.ToString)
      lstrCmd.Append(",@deca_peti_id=" + cmbPetiFil.Valor.ToString)
      lstrCmd.Append(",@deca_perio=" + txtPeriFil.Valor.ToString)
      lstrCmd.Append(",@deca_tarj_id=" + cmbTarjFil.Valor.ToString)
      lstrCmd.Append(",@deca_acti_id=" + CType(SRA_Neg.Constantes.Actividades.Socios, String))
      lstrCmd.Append(",@noImp=" + Math.Abs(CInt(chkTodas.Checked)).ToString)
      lstrCmd.Append(",@soci_desde=" + txtSociDesde.Valor.ToString)
      lstrCmd.Append(",@soci_hasta=" + txtSociHasta.Valor.ToString)
      If txtCPDesde.Text <> "" Then
         lstrCmd.Append(",@cp_desde=" + txtCPDesde.Valor.ToString)
      End If
      If txtCPHasta.Text <> "" Then
         lstrCmd.Append(",@cp_hasta=" + txtCPHasta.Valor.ToString)
      End If
      lstrCmd.Append(",@etapa='I'")

      clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDatoBusq)
   End Sub
#End Region

   Private Sub mListar()
      Try

         Dim lstrRptName As String

         If cmbRepo.Valor.ToString = "E" Then
            lstrRptName = "Etiquetas"
         Else
            lstrRptName = "DebitosCupones"
         End If

         Dim lstrId As String

         For Each lItem As WebControls.DataGridItem In grdDatoBusq.Items
            If DirectCast(lItem.FindControl("chkSel"), CheckBox).Checked Then
               lstrId = lItem.Cells(ColumnasFil.comp_id).Text
               Exit For
            End If
         Next

         If lstrId Is Nothing Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un d�bito.")
         End If

         If cmbRepo.Valor.ToString = "C" And cmbTexto.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un texto para el comunicado.")
         End If

         Dim lDs As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, lstrId)
         lDs.Tables(0).TableName = mstrTabla
         lDs.Tables(mstrTabla).Rows(0).Item("deca_impr_fecha") = Today
         Dim lObj As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, lDs)
         lObj.Modi()

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If cmbRepo.Valor.ToString = "E" Then
            lstrRpt += SRA_Neg.Utiles.ParametrosEtiquetas("CU", , IIf(chkDire.Checked, "True", "False"), , , , , , , , IIf(cmbRepoCorreo.Valor.ToString <> "T", cmbRepoCorreo.Valor.ToString, ""), , , , , , cmbOrden.Valor.ToString, cmbTexto.Valor.ToString, txtSociDesde.Valor.ToString, txtSociHasta.Valor.ToString, txtCPDesde.Text.ToString, txtCPHasta.Text.ToString, lstrId)
         Else
            lstrRpt += "&orden=" + cmbOrden.Valor.ToString
            lstrRpt += "&teco_id=" + cmbTexto.Valor.ToString
            If cmbRepoCorreo.Valor.ToString <> "T" Then
               lstrRpt += "&correo=" + IIf(cmbRepoCorreo.Valor.ToString = "0", "False", "True")
            Else
               lstrRpt += "&correo%3aisnull=True"
            End If
            lstrRpt += "&deca_id=" + lstrId
            lstrRpt += "&soci_desde=" + txtSociDesde.Valor.ToString
            lstrRpt += "&soci_hasta=" + txtSociHasta.Valor.ToString
            lstrRpt += "&cp_desde=" + txtCPDesde.Text.ToString
            lstrRpt += "&cp_hasta=" + txtCPHasta.Text.ToString
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      mListar()
   End Sub
End Class

End Namespace
