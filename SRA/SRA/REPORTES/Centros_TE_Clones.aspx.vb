Namespace SRA

Partial Class Centros_TE_Clones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents lblNume As System.Web.UI.WebControls.Label
	Protected WithEvents lblRazaDesde As System.Web.UI.WebControls.Label
	Protected WithEvents cmbRazaDesde As NixorControls.ComboBox
	Protected WithEvents lblRazaHasta As System.Web.UI.WebControls.Label
	Protected WithEvents cmbRazaHasta As NixorControls.ComboBox
	Protected WithEvents Label1 As System.Web.UI.WebControls.Label
	Protected WithEvents lblFechaHasta As System.Web.UI.WebControls.Label


	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

#Region "Definición de Variables"
	Private mstrConn As String
	Private mstrActiId As String
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()

			If (Not Page.IsPostBack) Then
				mCargarCombos()
				
			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mInicializar()
		If Not Request.QueryString("act") Is Nothing Then
			mstrActiId = Request.QueryString("act")
		Else
			mstrActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", Request.QueryString("fkvalor")).Tables(0).Rows(0).Item("inse_acti_id")
		End If
	End Sub

	Private Sub mCargarCombos()
		clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbProvDesde, "S")
		clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbProvHasta, "S")
		
	End Sub

	Private Sub mLimpiarFiltros()
		txtCodiDesde.Text = ""
		txtCodiHasta.Text = ""
		txtCentro.Text = ""
		txtVete.Text = ""
		chkBuscCentro.Checked = False
		chkBuscVete.Checked = False
		chkBaja.Checked = False
		cmbProvDesde.Limpiar()
		cmbProvHasta.Limpiar()
		cmbLocaDesde.Limpiar()
		cmbLocaHasta.Limpiar()
		txtAltaFechaDesde.Text = ""
		txtAltaFechaHasta.Text = ""
		txtBajaFechaDesde.Text = ""
		txtBajaFechaHasta.Text = ""
		

	End Sub
#End Region

	Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		Try
			Dim lstrRptName As String
			Dim lintFecha As Integer
			Dim lstrAnio As String
			Dim lstrPerio As String
			Dim lintFACD, lintFACH, lintFBCD, lintFBCH As Integer

			lstrRptName = "Centros_TE_Clon"
				
			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			lstrRpt += "&imcr_codi_desde=" & IIf(txtCodiDesde.Text = "", 0, txtCodiDesde.Text)
			lstrRpt += "&imcr_codi_hasta=" & IIf(txtCodiHasta.Text = "", 0, txtCodiHasta.Text)

			If txtCentro.Text <> "" Then
				lstrRpt += "&imcr_deno=" & txtCentro.Text

			End If

			If txtVete.Text <> "" Then
				lstrRpt += "&imcr_vete=" & txtVete.Text

			End If

			lstrRpt += "&prov_id=" & IIf(cmbProvDesde.Valor.ToString = "", "0", cmbProvDesde.Valor)

			If cmbProvDesde.SelectedItem.Text <> "(Seleccione)" Then
				lstrRpt += "&prov_desc_desde=" & Trim(cmbProvDesde.SelectedItem.Text)

			End If

			If cmbProvHasta.SelectedItem.Text <> "(Seleccione)" Then
				lstrRpt += "&prov_desc_hasta=" & Trim(cmbProvHasta.SelectedItem.Text)

			End If

			If cmbLocaDesde.Items.Count > 0 Then
				lstrRpt += "&loca_desc_desde=" & Trim(cmbLocaDesde.SelectedItem.Text)

			End If

			If cmbLocaHasta.Items.Count > 0 Then
				lstrRpt += "&loca_desc_hasta=" & Trim(cmbLocaHasta.SelectedItem.Text)

			End If

			lstrRpt += "&buscar_en_deno=" & IIf(chkBuscCentro.Checked, True, False)
			lstrRpt += "&buscar_en_vete=" & IIf(chkBuscVete.Checked, True, False)
			lstrRpt += "&incluir_bajas=" & IIf(chkBaja.Checked, True, False)


			'If txtAltaFechaDesde.Text = "" Then
			'	lintFACD = 0
			'Else
			'	lintFACD = CType(clsFormatear.gFormatFechaString(txtAltaFechaDesde.Text, "Int32"), Integer)
			'End If
			'If txtAltaFechaHasta.Text = "" Then
			'	lintFACH = 0
			'Else
			'	lintFACH = CType(clsFormatear.gFormatFechaString(txtAltaFechaHasta.Text, "Int32"), Integer)
			'End If

			If txtBajaFechaDesde.Text = "" Then
				lintFBCD = 0
			Else
				lintFBCD = CType(clsFormatear.gFormatFechaString(txtBajaFechaDesde.Text, "Int32"), Integer)
			End If
			If txtBajaFechaHasta.Text = "" Then
				lintFBCH = 0
			Else
				lintFBCH = CType(clsFormatear.gFormatFechaString(txtBajaFechaHasta.Text, "Int32"), Integer)
			End If


			'lstrRpt += "&fecha_alta_cria_desde=" & lintFACD
			'lstrRpt += "&fecha_alta_cria_hasta=" & lintFACH
			lstrRpt += "&fecha_baja_desde=" & lintFBCD
			lstrRpt += "&fecha_baja_hasta=" & lintFBCH

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltros()
	End Sub
End Class
End Namespace
