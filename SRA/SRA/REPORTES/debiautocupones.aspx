<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DebiAutoCupones" CodeFile="DebiAutoCupones.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Impresi�n de Cupones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
		function mSetearTipo()
		{
			if (document.all('cmbRepo')!=null)
			{
				if (document.all('cmbRepo').value=='E')
				{
					document.all('panDire').style.display='';
				}
				else
				{
					document.all('panDire').style.display='none';		
				}
				if (document.all('cmbRepo').value=='C')
				{
					document.all('cmbTexto').style.display='';
					document.all('lblTexto').style.display='';
				}
				else
				{
					document.all('cmbTexto').style.display='none';
					document.all('lblTexto').style.display='none';
				}
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion">Impresi�n de Cupones</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" background="../imagenes/formfdofields.jpg"
																	border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="3"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblTarjFil" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTarjFil" runat="server" Width="200px" AceptaNull="False"></cc1:combobox></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Button id="btnBuscar" tabIndex="2" runat="server" cssclass="boton" Width="80px" text="Buscar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg"
																			colSpan="2">
																			<cc1:numberbox id="txtPeriFil" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
																				MaxLength="2" MaxValor="12" CantMax="2"></cc1:numberbox>
																			<cc1:combobox class="combo" id="cmbPetiFil" runat="server" Width="80px" AceptaNull="False"></cc1:combobox>&nbsp;&nbsp;
																			<asp:Label id="Label1" runat="server" cssclass="titulo">/</asp:Label>&nbsp;
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="35px" AceptaNull="False"
																				MaxLength="4" MaxValor="2090" CantMax="4"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" noWrap align="right" background="../imagenes/formfdofields.jpg">&nbsp;&nbsp;
																			<asp:label id="lblSociDesde" runat="server" cssclass="titulo">Socio desde:&nbsp;</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg"
																			colSpan="2">
																			<CC1:NUMBERBOX id="txtSociDesde" runat="server" cssclass="cuadrotexto" Width="75px" AceptaNull="False"></CC1:NUMBERBOX>&nbsp;
																			<asp:label id="lblSociHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																			<CC1:NUMBERBOX id="txtSociHasta" runat="server" cssclass="cuadrotexto" Width="75px" AceptaNull="False"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:label id="lblCPDesde" runat="server" cssclass="titulo">CP desde:&nbsp;</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg"
																			colSpan="2">
																			<CC1:TEXTBOXTAB id="txtCPDesde" runat="server" cssclass="cuadrotexto" Width="75px" MaxLength="8"></CC1:TEXTBOXTAB>&nbsp;
																			<asp:label id="lblCPHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																			<CC1:TEXTBOXTAB id="txtCPHasta" runat="server" cssclass="cuadrotexto" Width="75px" MaxLength="8"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg"
																			colSpan="2">
																			<asp:CheckBox id="chkTodas" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small"
																				Text="Sin Imprimir"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="3"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="../imagenes/formde.jpg" height="10"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD colSpan="3" height="10"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE> <!---fin filtro ---></asp:panel></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDatoBusq" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
										PageSize="15" AutoGenerateColumns="False" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server" onclick="chkSelCheck(this);"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="deca_id" Visible="false"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_gene_fecha" HeaderText="Generado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_envi_fecha" HeaderText="Env�o" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_rece_fecha" HeaderText="Recepci�n" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_tarj_desc" HeaderText="Tarjeta"></asp:BoundColumn>
											<asp:BoundColumn DataField="_perio" HeaderText="Per�odo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cant" HeaderText="Cant."></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="3"></TD>
							</TR>
							<TR>
								<TD background="../imagenes/formfdofields.jpg" height="24"></TD>
								<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" height="24"
									style="WIDTH: 73px"><asp:label id="lblRepoCorreo" runat="server" cssclass="titulo">Correo:&nbsp;</asp:label></TD>
								<TD background="../imagenes/formfdofields.jpg" height="24"><cc1:combobox class="combo" id="cmbRepoCorreo" runat="server" Width="136px">
										<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
										<asp:ListItem Value="0">Con</asp:ListItem>
										<asp:ListItem Value="1">Sin</asp:ListItem>
									</cc1:combobox></TD>
							</TR>
							<TR>
								<TD background="../imagenes/formfdofields.jpg" height="24"></TD>
								<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" height="24"
									style="WIDTH: 73px"><asp:label id="lblOrden" runat="server" cssclass="titulo">Orden:&nbsp;</asp:label></TD>
								<TD width="500" background="../imagenes/formfdofields.jpg" height="24"><cc1:combobox class="combo" id="cmbOrden" runat="server" Width="180px">
										<asp:ListItem Value="N" Selected="True">Por Tarjeta y Nombre</asp:ListItem>
										<asp:ListItem Value="S">Por Tarjeta y Nro.Socio</asp:ListItem>
									</cc1:combobox></TD>
							</TR>
							<TR>
								<TD background="../imagenes/formfdofields.jpg" height="24"></TD>
								<TD vAlign="middle" align="right" background="../imagenes/formfdofields.jpg" height="24"
									style="WIDTH: 73px"></TD>
								<TD background="../imagenes/formfdofields.jpg" height="24">
									<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD></TD>
											<TD noWrap align="right">
												<asp:label id="lblRepo" runat="server" cssclass="titulo">Tipo Reporte:&nbsp;</asp:label>&nbsp;
											</TD>
											<TD>
												<cc1:combobox class="combo" id="cmbRepo" runat="server" Width="119px" onchange="javascript:mSetearTipo();">
													<asp:ListItem Value="C" Selected="True">Cupones</asp:ListItem>
													<asp:ListItem Value="E">Etiquetas</asp:ListItem>
												</cc1:combobox></TD>
											<TD>
												<DIV id="panDire" noWrap>
													<asp:checkbox id="chkDire" Text="Con Direcci�n" Runat="server" CssClass="titulo"></asp:checkbox></DIV>
											</TD>
											<TD noWrap>&nbsp;&nbsp;
												<asp:label id="lblTexto" runat="server" cssclass="titulo">Texto: </asp:label></TD>
											<TD>
												<cc1:combobox class="combo" id="cmbTexto" runat="server" Width="180px"></cc1:combobox></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="3"></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="right" colSpan="3"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ImagesUrl="../imagenes/"
										IncludesUrl="../includes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
										ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BOTONIMAGEN></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		try{
			if (document.all["editar"]!= null)
				document.location='#editar';
			if (document.all["cmbEmprFil"]!= null)
				document.all["cmbEmprFil"].focus();
			if (document.all["cmbEmpr"]!= null)
				document.all["cmbEmpr"].focus();
			}
		catch(e){;}

		function chkSelCheck(pChk)
		{
			var lintFil= eval(pChk.id.replace("grdDatoBusq__ctl","").replace("_chkSel",""));

			if (document.all("grdDatoBusq").children(0).children.length > 1)
			{
				for (var fila=3; fila <= document.all("grdDatoBusq").children(0).children.length; fila++)
				{
					if (fila!=lintFil)
					{
						document.all("grdDatoBusq__ctl" + fila.toString() + "_chkSel").checked = false;
					}
				}
			}
		}
		mSetearTipo();
		</SCRIPT>
	</BODY>
</HTML>
