<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AnaliticoComprob" CodeFile="AnaliticoComprob.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="javascript">
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion">Analitico por Comprobante</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD id="ColumnaPrueba" style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 29.86%; HEIGHT: 7px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 7px" background="../imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo" Width="87px">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEdesde" runat="server" cssclass="titulo" Width="133px">Ctro.Emisor Desde:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEdesde" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCEhasta" runat="server" cssclass="titulo">Ctro.Emisor Hasta:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbCEhasta" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTipo" runat="server" Width="100px" nomoper="comprob_tipos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Nro:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<asp:TextBox id="txtLetra" Width="25" Runat="server"></asp:TextBox>&nbsp;-
																			<cc1:numberbox id="txtCemiNume" Width="50" aling="right" Runat="server"></cc1:numberbox>&nbsp;-
																			<cc1:numberbox id="txtNume" Width="100" aling="right" Runat="server"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 29.86%" vAlign="top" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																			<asp:CheckBox id="chkTotalizado" runat="server" Width="144px" Text="Totalizado" CssClass="titulo"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD> <!-- FIN FOMULARIO -->
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD width="3" height="10"></TD>
															<TD height="10"></TD>
															<TD width="2" height="10"></TD>
														</TR>
														<TR>
															<TD width="3"></TD>
															<TD align="right">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
																	ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
					</td> <!--- FIN CONTENIDO --->
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
					</td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
