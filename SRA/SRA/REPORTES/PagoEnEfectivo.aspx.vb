Imports SRA

Public Class PagoEnEfectivo
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaFil As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaFil As NixorControls.DateBox
    'Protected WithEvents lblCentroEmisor As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbCentroEmisor As NixorControls.ComboBox
    'Protected WithEvents btnList As NixorControls.BotonImagen
    'Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
    End Sub
#End Region

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lintFecha As Integer

            lstrRptName = "ChequesCartera"

            If txtFechaFil.Fecha.ToString = "" Then
                lintFecha = 0
            Else
                lintFecha = CType(clsFormatear.gFormatFechaString(txtFechaFil.Text, "Int32"), Integer)
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)


            lstrRpt += "&cheq_real_depo_fecha=" + lintFecha.ToString
            lstrRpt += "&cheq_emct_id=" + IIf(cmbCentroEmisor.Valor.ToString = "", "null", cmbCentroEmisor.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class