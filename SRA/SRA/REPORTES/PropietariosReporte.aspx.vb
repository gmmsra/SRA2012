Namespace SRA

Partial Class PropietariosReporte
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Public mstrConn As String
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                If (Not Page.IsPostBack) Then
                    '     Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mListar()
            Try
            Dim lstrRptName As String = "PropietariosReporte"
                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

                hdnRptId.Text = usrProdFil.ObtenerIdFiltro.ToString

                lstrRpt += "&rprf_id=" + hdnRptId.Text.ToString

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                Response.Redirect(lstrRpt)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
            mListar()
        End Sub

        Private Sub btnLimpFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
            usrProdFil.Limpiar()
        End Sub
    End Class

End Namespace
