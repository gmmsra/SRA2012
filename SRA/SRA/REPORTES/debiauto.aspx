<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DebiAuto" CodeFile="DebiAuto.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reporte por Estado</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
		function mCargaComboTarjetas()
		{
			var sFiltro = document.all("cmbDeca").value;
			var anio = sFiltro.substring(0,4);
			var perio = sFiltro.substring(4,6);
			sFiltro = "null," + anio + ',' + perio;
			LoadComboXML("debitos_tarjetas_fecha_cargar", sFiltro, "cmbTarjeta", "T");
		}
		function cmbDeca_change(pcmb)
		{
		    //var sFiltro;
			//if (pcmb.value =="")
				//sFiltro =  "-1";
			//else 
				//sFiltro = pcmb.value ;
		    //LoadComboXML("estados_x_debito_cargar", sFiltro, "cmbEsta", "T");
		}
		function mValidarEnvio() 
		{
			 if (document.all('cmbMail').value=='' && (document.all('cmbRepo').value=='M'))
			{
				alert('Debe indicar un modelo de mail.');
				document.all('cmbMail').focus();
				return(false);
			}
			else
			{
				return(true);
			}
		}
		function mSetearTipo()
		{
			if (document.all('cmbRepo').value=='M')
			{
				document.all('lblMail').style.display='';
				document.all('cmbMail').style.display='';
			}
			else
			{
				document.all('lblMail').style.display='none';
				document.all('cmbMail').style.display='none';
			}
			if (document.all('cmbRepo').value=='E')
			{
				document.all('panDire').style.display='';
			}
			else
			{
				document.all('panDire').style.display='none';		
			}
			if(document.all('cmbRepo').value=='R')
			{
				document.all('panResu').style.display='';
			}
			else
			{
				document.all('panResu').style.display='none';
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="391px">Reporte por Estado</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 246px" align="right" background="../imagenes/formfdofields.jpg" height="10">&nbsp;</TD>
																		<TD style="WIDTH: 75%" background="../imagenes/formfdofields.jpg" height="10"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg"
																			colSpan="2"><TABLE id="Table3" border="0">
																				<TR>
																					<TD noWrap align="right">
																						<asp:Label id="lblDeca" runat="server" cssclass="titulo">D�bito:</asp:Label>&nbsp;</TD>
																					<TD colSpan="4">
																						<cc1:combobox class="combo" id="cmbDeca" runat="server" Width="244px" Obligatorio="True" onchange="cmbDeca_change(this);"></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD noWrap align="right">
																						<asp:Label id="lblTarjeta" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																					<TD colSpan="4">
																						<cc1:combobox class="combo" id="cmbTarjeta" runat="server" Width="244px" NomOper="debitos_tarjetas_fecha_cargar"
																							AceptaNull="false" IncludesUrl="../Includes/" ImagesUrl="../Images/">
																							<asp:ListItem Value="(Todos)" Selected="True">(Todos)</asp:ListItem>
																						</cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD noWrap align="right">
																						<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																					<TD colSpan="4">
																						<cc1:combobox class="combo" id="cmbEsta" runat="server" Width="139px" NomOper="estados_x_debito_cargar"
																							AceptaNull="False"></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD noWrap>&nbsp;&nbsp;
																						<asp:label id="lblRepo" runat="server" cssclass="titulo">Tipo de Reporte:</asp:label>&nbsp;</TD>
																					<TD>
																						<cc1:combobox class="combo" id="cmbRepo" runat="server" Width="119px" onchange="javascript:mSetearTipo();">
																							<asp:ListItem Value="R" Selected="True">Reporte</asp:ListItem>
																							<asp:ListItem Value="E">Etiquetas</asp:ListItem>
																							<asp:ListItem Value="M">Mails</asp:ListItem>
																						</cc1:combobox>&nbsp;
																					</TD>
																					<TD>
																						<DIV id="panDire" noWrap>
																							<asp:checkbox id="chkDire" Text="Con Direcci�n" Runat="server" CssClass="titulo"></asp:checkbox></DIV>
																					</TD>
																					<TD>
																						<DIV id="panResu" noWrap>
																							<asp:checkbox id="chkResumido" Text="Resumido" Runat="server" CssClass="titulo"></asp:checkbox></DIV>
																					</TD>
																					<TD>
																						<asp:label id="lblMail" runat="server" cssclass="titulo">Modelo:</asp:label></TD>
																					<TD>
																						<cc1:combobox class="combo" id="cmbMail" runat="server" Width="160px"></cc1:combobox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD colSpan="3" height="10">
																<asp:datagrid id="grdCons" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" CellSpacing="1"
																	CellPadding="1" AllowPaging="True" HorizontalAlign="Left" GridLines="None" OnPageIndexChanged="DataGrid_Page"
																	PageSize="10" ItemStyle-Height="5px" AutoGenerateColumns="False">
																	<FooterStyle CssClass="footer"></FooterStyle>
																	<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																	<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																	<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																	<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																	<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																	<Columns>
																		<asp:TemplateColumn Visible="False">
																			<HeaderStyle Width="5%"></HeaderStyle>
																			<ItemTemplate>
																				<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																					Height="5">
																					<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
																				</asp:LinkButton>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																	<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																</asp:datagrid></TD>
														</TR>
														<TR>
															<TD align="right" colSpan="3">
																<CC1:BotonImagen id="btnEnvi" runat="server" BorderStyle="None" ImageDisable="btnEnvi0.gif" visible="false"
																	ImageUrl="imagenes/btnEnvi.gif" IncludesUrl="../includes/" ImagesUrl="../imagenes/" BackColor="Transparent"
																	CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnEnvi.gif" ImageOver="btnEnvi2.gif"
																	ForeColor="Transparent"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
																	ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	IncludesUrl="../includes/" ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False"
																	OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																	CausesValidation="False"></CC1:BotonImagen></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		mSetearTipo();
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
