Namespace SRA

Partial Class DeudaSocialResu
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = "Titulos"

   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "DeudaPorSocio"
         Dim lstrRpt As String
         Dim lintFechaD As Integer
         Dim lintFechaH As Integer

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If txtFechaDesdeFil.Text <> "" Then
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Text <> "" Then
            lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         If usrSociFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un socio")
         End If

         lstrRpt += "&fkFechaDesde=" + lintFechaD.ToString
         lstrRpt += "&fkFechaHasta=" + lintFechaH.ToString
         lstrRpt += "&fkCliente=" + usrSociFil.ClieId.ToString

         If rbtExtra.Checked Then
            lstrRpt += "&fkFormato=0"
         End If

         If rbtInte.Checked Then
            lstrRpt += "&fkFormato=1"
         End If

         If rbtSaldo.Checked Then
            lstrRpt += "&fkFormato=2"
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
