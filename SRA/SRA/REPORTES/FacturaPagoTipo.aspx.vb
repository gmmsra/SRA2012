Namespace SRA

Partial Class FacturaPagoTipo
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEdesde, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEhasta, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "comprob_pagos_tipos", cmbPagosTipos, "T")
    End Sub
#End Region

#Region "Detalle"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer
            If cmbDetalle.Valor.ToString = "R" Then
                lstrRptName = "FacturacionTipoPagoResu"
            Else
                lstrRptName = "FacturacionTipoPago"
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtFechaDesde.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
            End If

            If txtFechaHasta.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
            End If

            lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&CEmDesde=" + IIf(cmbCEdesde.Valor.ToString = "", "0", cmbCEdesde.Valor.ToString)
            lstrRpt += "&CEmHasta=" + IIf(cmbCEhasta.Valor.ToString = "", "0", cmbCEhasta.Valor.ToString)
            lstrRpt += "&comp_cpti_id=" + IIf(cmbPagosTipos.Valor.ToString = "", "0", cmbPagosTipos.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class

End Namespace
