<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DebiAutoCuponesDA" CodeFile="debiautocuponesDA.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Impresi�n recibimos socios</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultgrupntScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="../INCLUDES/utiles.js"></script>
    <script type="text/javascript" src="../INCLUDES/paneles.js"></script>
    <script type="text/javascript" src="../INCLUDES/valDate.js"></script>
    <script type="text/ecmascript" src="../INCLUDES/datepicker.js"></script>

    <script type="text/javascript" language="javascript">
        function mSetearTipo() {
            if (document.all('cmbRepo') != null) {
                if (document.all('cmbRepo').value == 'E') {
                    document.all('panDire').style.display = '';
                }
                else {
                    document.all('panDire').style.display = 'none';
                }
                if (document.all('cmbRepo').value == 'C') {
                    document.all('cmbTexto').style.display = '';
                    document.all('lblTexto').style.display = '';
                }
                else {
                    document.all('cmbTexto').style.display = 'none';
                    document.all('lblTexto').style.display = 'none';
                }
            }
        }
    </script>
</head>
<body class="pagina" leftmargin="5" topmargin="5" onload="gSetearTituloFrame('');" rightmargin="0">
    <form id="frmABM" method="post" runat="server">
        <!------------------ RECUADRO ------------------->
        <table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
            <tr>
                <td width="9">
                    <img height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
                <td background="../imagenes/recsup.jpg">
                    <img height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
                <td width="13">
                    <img height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9" background="../imagenes/reciz.jpg">
                    <img height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table id="Table1" style="width: 100%; height: 130px" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td width="100%" colspan="3"></td>
                        </tr>
                        <tr>
                            <td style="height: 25px" valign="bottom" colspan="3" height="25">
                                <asp:Label ID="lblTitu" runat="server" Width="391px" CssClass="opcion">Impresi�n recibimos socios</asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">
                                <asp:Panel ID="panFiltro" runat="server" CssClass="titulo" BorderStyle="Solid" Width="100%"
                                    BorderWidth="0" Visible="True">
                                    <table id="TableFil" style="width: 100%" cellspacing="0" cellpadding="0" align="left" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="height: 8px" width="24"></td>
                                                        <td style="height: 8px" width="42"></td>
                                                        <td style="height: 8px" width="26"></td>
                                                        <td style="height: 8px"></td>
                                                        <td style="height: 8px" width="26"></td>
                                                        <td style="height: 8px" width="50"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px" width="24">
                                                            <img height="25" src="../imagenes/formfle.jpg" width="24" border="0"></td>
                                                        <td style="height: 8px" width="42">
                                                            <img height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></td>
                                                        <td style="height: 8px" width="26">
                                                            <img height="25" src="../imagenes/formcap.jpg" width="26" border="0"></td>
                                                        <td style="height: 8px" background="../imagenes/formfdocap.jpg" colspan="3">
                                                            <img height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td width="3" background="../imagenes/formiz.jpg">
                                                            <img height="30" src="../imagenes/formiz.jpg" width="3" border="0"></td>
                                                        <td>
                                                            <!-- FOMULARIO -->
                                                            <table cellspacing="0" cellpadding="0" width="100%" background="../imagenes/formfdofields.jpg"
                                                                border="0">
                                                                <tr>
                                                                    <td style="height: 10px" align="right" background="../imagenes/formfdofields.jpg" colspan="3"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%; height: 24px" align="right" background="../imagenes/formfdofields.jpg">
                                                                        <asp:Label ID="lblTarjFil" runat="server" CssClass="titulo">Tarjeta:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 75%; height: 24px" align="left" background="../imagenes/formfdofields.jpg">
                                                                        <cc1:ComboBox class="combo" ID="cmbTarjFil" runat="server" Width="200px" AceptaNull="False"></cc1:ComboBox></td>
                                                                    <td style="width: 10%; height: 24px" align="right" background="../imagenes/formfdofields.jpg">
                                                                        <asp:Button ID="btnBuscar" TabIndex="2" runat="server" CssClass="boton" Width="80px" Text="Buscar"></asp:Button></td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="width: 25%; height: 24px" align="right" background="../imagenes/formfdofields.jpg">
                                                                        <asp:Label ID="lblFechaDesdeFil" runat="server" CssClass="titulo">Fecha Desde:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 75%; height: 24px" background="../imagenes/formfdofields.jpg">
                                                                        <cc1:DateBox ID="txtFechaDesdeFil" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox>
                                                                        <asp:Label ID="lblFechaHastaFil" runat="server" CssClass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox ID="txtFechaHastaFil" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox></td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="width: 15%; height: 24px" nowrap align="right" background="../imagenes/formfdofields.jpg">&nbsp;&nbsp;
																			<asp:Label ID="lblSociDesde" runat="server" CssClass="titulo">Socio desde:&nbsp;</asp:Label>&nbsp;</td>
                                                                    <td style="width: 85%; height: 24px" align="left" background="../imagenes/formfdofields.jpg"
                                                                        colspan="2">
                                                                        <cc1:NumberBox ID="txtSociDesde" runat="server" CssClass="cuadrotexto" Width="75px" AceptaNull="False"></cc1:NumberBox>&nbsp;
																			<asp:Label ID="lblSociHasta" runat="server" CssClass="titulo">Hasta:&nbsp;</asp:Label>
                                                                        <cc1:NumberBox ID="txtSociHasta" runat="server" CssClass="cuadrotexto" Width="75px" AceptaNull="False"></cc1:NumberBox></td>
                                                                </tr>
                                                                <%--																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 85%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg"
																			colSpan="2">
																			<asp:CheckBox id="chkTodas" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small"
																				Text="Sin Imprimir"></asp:CheckBox></TD>
																	</TR>--%>
                                                                <tr>
                                                                    <td style="height: 10px" align="right" background="../imagenes/formfdofields.jpg" colspan="3"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="2" background="../imagenes/formde.jpg" height="10">
                                                            <img height="2" src="../imagenes/formde.jpg" width="2" border="0"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="10"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!---fin filtro --->
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" colspan="3">
                                <asp:DataGrid ID="grdDatoBusq" runat="server" Width="100%" BorderStyle="None" BorderWidth="1px"
                                    PageSize="15" AutoGenerateColumns="False" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                    AllowPaging="True">
                                    <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                    <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                    <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                    <FooterStyle CssClass="footer"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSel" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="comp_id" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NroCliente" HeaderText="N� Cliente"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NroSocio" HeaderText="N� Socio"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NroComp" HeaderText="Comprobante"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Fecha" HeaderText="Fecha"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Cuota" HeaderText="Cuota"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Neto" HeaderText="Neto"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td colspan="3" height="10"></td>
                        </tr>
                        <tr>
                            <td valign="middle" align="right" colspan="3">
                                <cc1:BotonImagen ID="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ImagesUrl="../imagenes/"
                                    IncludesUrl="../includes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
                                    ImageOver="btnImpr2.gif" ForeColor="Transparent"></cc1:BotonImagen></td>
                        </tr>
                    </table>
                    <!--- FIN CONTENIDO --->
                </td>
                <td width="13" background="../imagenes/recde.jpg">
                    <img height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9">
                    <img height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
                <td background="../imagenes/recinf.jpg">
                    <img height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
                <td width="13">
                    <img height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
            </tr>
        </table>
        <!----------------- FIN RECUADRO ----------------->
        <div style="display: none">
            <asp:TextBox ID="lblMens" runat="server"></asp:TextBox></div>
    </form>
    <script language="javascript">
        try {
            if (document.all["editar"] != null)
                document.location = '#editar';
            if (document.all["cmbEmprFil"] != null)
                document.all["cmbEmprFil"].focus();
            if (document.all["cmbEmpr"] != null)
                document.all["cmbEmpr"].focus();
        }
        catch (e) { ; }

        function chkSelCheck(pChk) {
            var lintFil = eval(pChk.id.replace("grdDatoBusq__ctl", "").replace("_chkSel", ""));

            if (document.all("grdDatoBusq").children(0).children.length > 1) {
                for (var fila = 3; fila <= document.all("grdDatoBusq").children(0).children.length; fila++) {
                    if (fila != lintFil) {
                        document.all("grdDatoBusq__ctl" + fila.toString() + "_chkSel").checked = false;
                    }
                }
            }
        }
        mSetearTipo();
    </script>
</body>
</html>
