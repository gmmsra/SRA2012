Namespace SRA

Partial Class ClientesCondicionesEspeciales
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            txtMonto.Enabled = chkCtaCorr.Checked
            clsWeb.gCargarCombo(mstrConn, "iibb_catego_cargar", cmbIIBB, "id", "descrip", "T")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mLimpiarFiltros()
        usrClieFil.Limpiar()
        chkFactExen.Checked = False
        chkPercIVA.Checked = False
        chkPrecSoci.Checked = False
        chkMiembroCD.Checked = False
        chkNDInte.Checked = False
        chkCtaCorr.Checked = False
        chkObservaciones.Checked = False
        txtMonto.Text = ""
        txtMonto.Enabled = False
    End Sub
#End Region

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "ClientesCondicionesEspeciales"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
            lstrRpt += "&clie_fact_exen=" + IIf(chkFactExen.Checked, "1", "0")
            lstrRpt += "&clie_perc_iva=" + IIf(chkPercIVA.Checked, "1", "0")
            lstrRpt += "&clie_prec_soci=" + IIf(chkPrecSoci.Checked, "1", "0")
            lstrRpt += "&clie_miem_cd=" + IIf(chkMiembroCD.Checked, "1", "0")
            lstrRpt += "&clie_deud_monto=" + IIf(chkCtaCorr.Checked, IIf(txtMonto.Valor.ToString <> "", txtMonto.Valor.ToString, "0"), "-1")
            lstrRpt += "&clie_obse=" + IIf(chkObservaciones.Checked, "1", "0")
            lstrRpt += "&clie_exen_nd=" + IIf(chkNDInte.Checked, "1", "0")
            lstrRpt += "&clie_ibbc_id=" + IIf(hdnIIBBId.Text = "", "0", hdnIIBBId.Text)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
