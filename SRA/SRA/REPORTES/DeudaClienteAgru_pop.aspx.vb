Namespace SRA

Partial Class DeudaClienteAgru_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrClieId As String
   Private mstrParaPageSize As Integer
   Private mdecTotDebe As Decimal
    Private mdecTotHaber As Decimal
    Private mstrSociId As String
    Private mstrFechaValor As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        mstrClieId = Request.QueryString("clieId")
        mstrSociId = Request.QueryString("sociId")
        mstrFechaValor = Request.QueryString("fechaValor")

        hdnSociId.Text = Request.QueryString("sociId")
        hdnFechaValor.Text = Request.QueryString("fechaValor")
        hdnClieId.Text = Request.QueryString("clieId")

        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        mstrClieId = Request.QueryString("clie_id")
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Dim lstrCmd As String
      lstrCmd = "exec rpt_deuda_cliente_agrup_consul"
      lstrCmd += " @clie_id=" + mstrClieId
      clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)
   End Sub

   Private Sub grdDato_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDato.ItemDataBound
      Try
         Select Case e.Item.ItemType
            Case ListItemType.Header
               mdecTotDebe = 0
               mdecTotHaber = 0

            Case ListItemType.Item, ListItemType.AlternatingItem
               With CType(e.Item.DataItem, DataRowView).Row
                  mdecTotDebe += .Item("debe")
                  mdecTotHaber += .Item("haber")
               End With

            Case ListItemType.Footer
               'e.Item.Cells(ColumnasFil.debe).Text = mdecTotDebe.ToString("######.00")
               'e.Item.Cells(ColumnasFil.haber).Text = mdecTotHaber.ToString("######.00")
         End Select

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region


    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub
    Private Sub mListar()
        Dim lstrRptName As String = "ClienteAgrupDeuda"

        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        lstrRpt += "&clie_id=" + mstrClieId.ToString

        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)
    End Sub
End Class

End Namespace
