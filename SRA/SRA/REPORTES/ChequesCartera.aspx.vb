Namespace SRA

Partial Class ChequesCartera
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            txtFechaDesdeFil.Fecha = Now
            txtFechaHastaFil.Fecha = Now
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "cheques_tipos", cmbTipo, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "clearings", cmbClea, "T")
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer
         Dim lintFechaHisto As Integer

         lstrRptName = "ChequesCartera"

         If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         If txtHisto.Fecha.ToString = "" Then
            lintFechaHisto = 0
         Else
            lintFechaHisto = CType(clsFormatear.gFormatFechaString(txtHisto.Text, "Int32"), Integer)
         End If

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
         lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
         lstrRpt += "&cheq_emct_id=" + IIf(cmbCentroEmisor.Valor.ToString = "", "0", cmbCentroEmisor.Valor.ToString)
         lstrRpt += "&cheq_mone_id=" + IIf(cmbMone.Valor.ToString = "", "0", cmbMone.Valor.ToString)
         lstrRpt += "&cheq_chti_id=" + IIf(cmbTipo.Valor.ToString = "", "0", cmbTipo.Valor.ToString)
         lstrRpt += "&cheq_clea_id=" + IIf(cmbClea.Valor.ToString = "", "0", cmbClea.Valor.ToString)
         lstrRpt += "&histo_fecha=" + lintFechaHisto.ToString
         lstrRpt += "&resu=" + IIf(chkResu.Checked, "S", "N")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class

End Namespace
