<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SOciosCobranza_pop" CodeFile="SociosCobranza_pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Cobranza</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<SCRIPT language="javascript">
		
		</SCRIPT>
</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 11.35%; HEIGHT: 75.42%" border="0">
				<TR>
					<TD style="HEIGHT: 39px">
						<asp:Label id="lblTitu" runat="server" width="100%" cssclass="opcion"> Cobranza</asp:Label></TD>
					<TD vAlign="top" align="right" style="HEIGHT: 39px">&nbsp;<img onclick="window.close();" src="..\images\Close.bmp" alt="Cerrar">
					</TD>
				</TR>
				<TR height="100%">
					<TD colspan="2" vAlign="top" style="HEIGHT: 48.67%"><asp:datagrid id="grdDato" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px" PageSize="15"
							OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True"
							BorderStyle="None" width="704px">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:BoundColumn DataField="fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
								<asp:BoundColumn DataField="comprobante" HeaderText="Comprobante"></asp:BoundColumn>
								<asp:BoundColumn DataField="importe" DataFormatString="{0:F2}" HeaderText="Importe" HeaderStyle-HorizontalAlign="Right" 
 ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colspan="2" align="right"></TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
