<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AnaliticoConcepAran" CodeFile="AnaliticoConcepAran.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="javascript">
			function HabilitarCombos()
			{
					document.all("txtArancelDesde").value="";
					document.all("txtArancelHasta").value="";
					document.all("txtArancelDesde").disabled=false;
					document.all("txtArancelHasta").disabled=false;
					
					document.all("txtConceptoDesde").value="";
					document.all("txtConceptoDesde").disabled=false;
					document.all("txtConceptoHasta").value="";
					document.all("txtConceptoHasta").disabled=false;
				
				switch (document.all("cmbListar").value)
					{
					case "C":
							document.all("txtArancelDesde").value="";
							document.all("txtArancelDesde").disabled=true;
							document.all("txtArancelHasta").value="";
							document.all("txtArancelHasta").disabled=true;
						break;
					case "A":
							document.all("txtConceptoDesde").value="";
							document.all("txtConceptoDesde").disabled=true;
							document.all("txtConceptoHasta").value="";
							document.all("txtConceptoHasta").disabled=true;
						break;
				}
			}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');HabilitarCombos();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="391px">Analitico por Concepto/Arancel</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" id="ColumnaPrueba" background="../imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 29.86%; HEIGHT: 7px" background="../imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 7px" background="../imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo" Width="87px">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblCEdesde" runat="server" cssclass="titulo" Width="133px">Ctro.Emisor Desde:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbCEdesde" class="combo" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblCEhasta" runat="server" cssclass="titulo">Ctro.Emisor Hasta:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbCEhasta" class="combo" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10.46%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblListar" runat="server" cssclass="titulo">Listar:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 100%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbListar" class="combo" runat="server" Width="100px" onchange="HabilitarCombos()">
																				<asp:listitem value="S" selected="true">(Seleccione)</asp:listitem>
																				<asp:listitem value="A">Solo Aranceles</asp:listitem>
																				<asp:listitem value="C">Solo Conceptos</asp:listitem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbActi" class="combo" runat="server" Width="250px"></cc1:combobox></TD>
																		<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 29.86%" vAlign="top" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblConceptoDesde" runat="server" cssclass="titulo">Conceptos desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																			<asp:TextBox id="txtConceptoDesde" runat="server" cssclass="cuadrotexto" Width="90px"></asp:TextBox>&nbsp;
																			<asp:Label id="lblConceptoHasta" runat="server" cssclass="titulo">hasta:</asp:Label>&nbsp;
																			<asp:TextBox id="txtConceptoHasta" runat="server" cssclass="cuadrotexto" Width="90px"></asp:TextBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 29.86%" vAlign="top" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblArancelDesde" runat="server" cssclass="titulo">Arancel desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtArancelDesde" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblArancelHasta" runat="server" cssclass="titulo">hasta:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtArancelHasta" runat="server" cssclass="cuadrotexto" Width="90px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10.46%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblEstado" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 100%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbEstado" class="combo" runat="server" Width="100px" onchange="HabilitarCombos()">
																				<asp:listitem value="-1" selected="true">(Todos)</asp:listitem>
																				<asp:listitem value="0">Impagos</asp:listitem>
																				<asp:listitem value="1">Pagos</asp:listitem>
																				<asp:listitem value="2">Parcial</asp:listitem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivfin.jpg" align="right"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivfin.jpg" width="8"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD> <!-- FIN FOMULARIO -->
															<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
														<TR>
															<TD height="10" width="3"></TD>
															<TD height="10"></TD>
															<TD height="10" width="2"></TD>
														</TR>
														<TR>
															<TD width="3"></TD>
															<TD align="right">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
																	ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
					</td> <!--- FIN CONTENIDO --->
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
					</td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
