<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Subdiario" CodeFile="Subdiario.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Asientos Contables</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');mSetearChecks('');mSetearTipo('');"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="391px">Asientos Contables</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
															<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 29px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblAsienDesdeFil" runat="server" cssclass="titulo" Width="120px">Asiento Interno Individual Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 29px" background="../imagenes/formfdofields.jpg">
																			<CC1:NumberBox id="txtAsienDesde" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:NumberBox>&nbsp;
																			<asp:Label id="lblAsienHastaFil" runat="server" cssclass="titulo" Width="120px">Asiento Interno Individual Hasta:&nbsp;</asp:Label>&nbsp;
																			<CC1:NumberBox id="txtAsienHasta" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 2px" align="right" background="../imagenes/formdivmed.jpg" colSpan="2"
																			height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblTipoAsien" runat="server" cssclass="titulo">Tipo de Asiento:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTipoAsien" runat="server" Width="344px" AceptaNull="false"
																				ImagesUrl="../Images/"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblDefinitivoDesde" runat="server" cssclass="titulo">Definitivo Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<CC1:NumberBox id="txtDefinitivoDesde" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:NumberBox>&nbsp;
																			<asp:Label id="lblDefinitivoHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:Label>
																			<CC1:NumberBox id="txtDefinitivoHasta" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:NumberBox>&nbsp;
																			<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">A�o Ejercicio:</asp:Label>&nbsp;
																			<CC1:NumberBox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="60px" AceptaNull="False"
																				MaxLength="4" MaxValor="9999"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblCemiFil" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="cmbCtro" id="cmbCemiFil" runat="server" cssclass="cuadrotexto" Width="250px"
																				AceptaNull="False" MostrarBotones="False" filtra="false" NomOper="emisores_ctros_cargar"></cc1:combobox>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:CheckBox id="chkComprob" onclick="javascript:mSetearChecks('C');" Runat="server" CssClass="titulo"
																				Text="Ver Comprobantes" Checked="false"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 23px" align="right" background="../imagenes/formfdofields.jpg">
																		<TD style="WIDTH: 75%; HEIGHT: 23px" background="../imagenes/formfdofields.jpg">
																			<asp:CheckBox id="chkAgru" onclick="javascript:mSetearChecks('A');" Runat="server" CssClass="titulo"
																				Text="Ver Agrupados" Checked="true"></asp:CheckBox>&nbsp;&nbsp;
																			<asp:CheckBox id="chkAnalit" Runat="server" CssClass="titulo" Text="Analitico"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<asp:CheckBox id="chkResu" onclick="javascript:mSetearChecks('R');" Runat="server" CssClass="titulo"
																				Text="Resumido" Checked="false"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" noWrap align="right" background="../imagenes/formfdofields.jpg">
																			<asp:Label id="lblTipoComp" runat="server" cssclass="titulo">Tipo Comprobante:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" noWrap background="../imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTipoComp" runat="server" Width="140px" AceptaNull="false" ImagesUrl="../Images/"
																				onchange="javascript:mSetearTipo(this.value);"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 62px" noWrap align="right" background="../imagenes/formfdofields.jpg">&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 62px" vAlign="top" noWrap background="../imagenes/formfdofields.jpg">
																			<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
																				<TR>
																					<TD noWrap align="right">
																						<asp:Label id="lblTipoTitu" runat="server" cssclass="titulo">Nro.Factura: </asp:Label>
																						<cc1:combobox class="combo" id="cmbLetra" runat="server" Width="50px" AceptaNull="True">
																							<asp:listitem value="" selected="true">(Sel.)</asp:listitem>
																							<asp:listitem value="A">A</asp:listitem>
																							<asp:listitem value="B">B</asp:listitem>
																						</cc1:combobox></TD>
																					<TD noWrap width="10">
																						<TABLE cellSpacing="0" cellPadding="0" border="0">
																							<TR>
																								<TD>
																									<CC1:NumberBox id="txtCompNume" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																										maxlength="10"></CC1:NumberBox></TD>
																								<TD>
																									<CC1:TEXTBOXTAB id="txtComp" runat="server" cssclass="cuadrotexto" Width="100px" maxlength="20"
																										Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																							</TR>
																						</TABLE>
																					</TD>
																					<TD width="20">
																						<DIV id="divCtro">
																							<cc1:combobox class="combo" id="cmbCtro" runat="server" Width="200px" AceptaNull="True" ImagesUrl="../Images/"></cc1:combobox></DIV>
																					</TD></TR></TABLE></TD></TR>
																</TABLE>
															</TD>
															<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD colSpan="3" height="10"></TD>
														</TR>
														<TR>
															<TD align="right" colSpan="3">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" ImageUrl="imagenes/btnImpr.jpg"
																	BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
																	ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnTipo" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		function mSetearTipo(pTipo)
		{
			if (pTipo == '')
				pTipo = document.all('cmbTipoComp').value;
			if (pTipo == '1' || pTipo == '3' || pTipo == '6')
			{
				document.all('txtCompNume').style.visibility = 'hidden';
				document.all('txtComp').style.visibility = '';
				document.all('txtCompNume').style.width = 1;
				document.all('txtComp').style.width = 140;
			}
			else
			{
				document.all('txtCompNume').style.visibility = '';
				document.all('txtComp').style.visibility = 'hidden';
				document.all('txtCompNume').style.width = 80;
				document.all('txtComp').style.width = 1;
			}
			if (pTipo == '')
			{
				document.all('lblTipoTitu').style.visibility = 'hidden';
				document.all('txtComp').style.visibility = 'hidden';
				document.all('txtCompNume').style.visibility = 'hidden';
			}
			else
			{
				document.all('lblTipoTitu').style.visibility = '';
			}
			if (pTipo == '')
				document.all('lblTipoTitu').innerHTML = '';
			else
				document.all('lblTipoTitu').innerHTML = pTipo;
			if (pTipo == '29' || pTipo == '31' || pTipo == '32')
			{
				document.all('cmbLetra').style.visibility = '';
				document.all('cmbLetra').style.width = 50;
			}
			else
			{
				document.all('cmbLetra').style.visibility = 'hidden';
				document.all('cmbLetra').style.width = 1;
			}
			if (pTipo == '' || pTipo == '10' || pTipo == '1' || pTipo == '2' || pTipo == '5' || pTipo == '6' || pTipo == '3')
				document.all('divCtro').style.visibility = 'hidden';
			else
				document.all('divCtro').style.visibility = '';	
			switch(pTipo)
			{
				case "10":	{ document.all('lblTipoTitu').innerHTML="Nro.Socio:"; break;}
				case "1":	{ document.all('lblTipoTitu').innerHTML="Nro.Cheque:"; break;}
				case "39":	{ document.all('lblTipoTitu').innerHTML="Nro.Comprob.:"; break;}
				case "2":	{ document.all('lblTipoTitu').innerHTML="Nro.Boleta:"; break;}
				case "29":	{ document.all('lblTipoTitu').innerHTML="Factura:"; break;}
				case "3":	{ document.all('lblTipoTitu').innerHTML="Cheque:"; break;}
				case "6":	{ document.all('lblTipoTitu').innerHTML="Cup�n:"; break;}
				case "30":	{ document.all('lblTipoTitu').innerHTML="Recibo:"; break;}
				case "31":	{ document.all('lblTipoTitu').innerHTML="N.Cr�dito:"; break;}
				case "32":	{ document.all('lblTipoTitu').innerHTML="N.D�bito:"; break;}
				case "5":	{ document.all('lblTipoTitu').innerHTML="Nro.Socio:"; break;}
				case "35":	{ document.all('lblTipoTitu').innerHTML="Nro.Comprob.:"; break;}
			}						
		}
		function mSetearChecks(pTipo)
		{
			if (pTipo == '')
			{
				if (document.all('chkComprob').checked)
				  pTipo = 'C';
				if (document.all('chkResu').checked)
				  pTipo = 'R';
				if (document.all('chkAgru').checked)
				  pTipo = 'A';
			}
			if (document.all('chkComprob').checked && pTipo == 'C')
			{
				document.all('chkAgru').checked = false;
				document.all('chkResu').checked = false;				
			}
			if (document.all('chkResu').checked && pTipo == 'R')
			{
				document.all('chkAgru').checked = false;
				document.all('chkComprob').checked = false;				
			}
			if (document.all('chkAgru').checked && pTipo == 'A')
			{
				document.all('chkComprob').checked = false;
				document.all('chkResu').checked = false;				
			}
			if (document.all('chkAgru').checked)
				document.all('chkAnalit').disabled = false;
			else
				document.all('chkAnalit').disabled = true;
			if (document.all('chkAnalit').disabled)
				document.all('chkAnalit').checked = false;
		}
		mSetearTipo('');
		mSetearChecks('');
		if (document.all["editar"]!= null)		
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
