Namespace SRA

Partial Class ListadoIVA
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
            txtHojaMovil.Text = 1
            txtFechaMovil.Text = Now.ToString("dd/MM/yyyy")
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Detalle"


   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "ListadoIVA"
         Dim lstrRpt As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         If txtFechaMovil.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la fecha hoja m�vil.")
         End If

         If txtHojaMovil.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar p�gina inicial hoja m�vil.")
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&CliId=" + usrClieFil.Valor.ToString
         lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
         lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
         lstrRpt += "&FechaMovil=" + txtFechaMovil.Text
         lstrRpt += "&HojaMovil=" + txtHojaMovil.Text
         lstrRpt += "&Resu=" + IIf(chkResu.Checked, "S", "N")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
