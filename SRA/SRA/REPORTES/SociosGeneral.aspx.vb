Namespace SRA

Partial Class SociosGeneral
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
         grdCons.PageSize = Convert.ToInt32(mstrParaPageSize)
         usrSocFil.MostrarDatosOpcionales = False
         btnAlta.Attributes.Add("onclick", "mAlta();return false;")
         btnBaja.Attributes.Add("onclick", "mBaja();return false;")
         btnList.Attributes.Add("onclick", "if(!mValidar()) return false;")
         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            btnEnvi.Attributes.Add("onclick", "return(mValidarEnvio());")
            clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar @acti_id=2", cmbMail, "id", "descrip", "S")
            mEstablecerPerfil()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   'Private Sub mCargarSociosListaEtiquetas()
   '   Dim lstrCmd As String = "socios_lista_datos_cargar "
   '   lstrCmd = lstrCmd & " @soci_lista=" & clsSQLServer.gFormatArg(hdnEtiqSoci.Text, SqlDbType.VarChar)
   '   clsWeb.gCargarCombo(mstrConn, lstrCmd, lisSoci, "id", "descrip", "")
   'End Sub

   Private Sub mLimpiarFiltros()
      usrSocFil.Limpiar()
      hdnEtiqSoci.Text = ""
      lisSoci.Items.Clear()
      cmbEtiqSoci.Limpiar()
      usrSoci.Limpiar()
      chkRepoDeuda.Checked = False
      chkRepoInte.Checked = False
      chkRepoMail.Checked = False
      chkRepoTele.Checked = False
      chkRepoCateFecha.Checked = False
      chkRepoIngreFecha.Checked = False
      usrSocFil.Visible = True
      grdCons.Visible = False
      btnList.Visible = True
      btnEnvi.Visible = False
      cmbRepo.Enabled = True
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub mListar()
      Try
         Dim lstrRptName As String

         Select Case cmbRepo.Valor.ToString
            Case "E"
             lstrRptName = "Etiquetas"
            Case "T"
             lstrRptName = "Socios_General_Totales"
            Case Else
             lstrRptName = "Socios_General"
         End Select

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         usrSocFil.chkTelefonoExt.Checked = chkRepoTele.Checked
         usrSocFil.chkDeudaExt.Checked = chkRepoDeuda.Checked
         usrSocFil.chkInteExt.Checked = chkRepoInte.Checked
         usrSocFil.chkCateFechaExt.Checked = chkRepoCateFecha.Checked
         usrSocFil.chkIngreFechaExt.Checked = chkRepoIngreFecha.Checked

         hdnRptId.Text = usrSocFil.ObtenerIdFiltro.ToString

         If cmbRepo.Valor.ToString = "E" Then
            lstrRpt += SRA_Neg.Utiles.ParametrosEtiquetas("PS", hdnRptId.Text, IIf(chkDire.Checked, "True", "False"), , , , , , , , , , , , , , , , , , , , , , , , , , cmbEtiqSoci.Valor, hdnEtiqSoci.Text.Replace(";;", ";"))
         Else
            lstrRpt += "&rptf_id=" & hdnRptId.Text
            lstrRpt += "&ConMail=" & IIf(chkRepoMail.Checked, "S", "N")
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Function mObtenerMails(ByVal ds As DataSet, ByVal pstrCate As String, ByVal pstrInst As String) As String
      Dim lstrMail As String
      Try
         For Each odrClag As DataRow In ds.Tables(0).Select("cate_id=" & pstrCate)
            If lstrMail <> "" Then
               lstrMail = lstrMail & ";"
            End If
            lstrMail = lstrMail & odrClag.Item("mails")
         Next
         ds.Dispose()
         Return (lstrMail)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Function

   Private Sub mEnviarMails()
      Try
         Dim ds As New DataSet
         Dim lstrMail As String
         Dim lstrInst As String
         Dim lstrCate As String
         Dim lstrAsun As String
         Dim lstrMens As String
         Dim lstrCmd As String
         Dim lstrMailMode As String = System.Configuration.ConfigurationSettings.AppSettings("conMailMode").ToString()

         lstrMailMode = "..\" & lstrMailMode

         ''lstrCmd = "exec socios_general_mails_consul @rptf_id=" & hdnRptId.Text
         ''ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each odrDeta As DataRow In Session("ds").Tables(1).Select
            lstrCate = odrDeta.Item("cate_id")
            If odrDeta.IsNull("inst_id") Then
                lstrInst = ""
            Else
                lstrInst = odrDeta.Item("inst_id")
            End If
            lstrAsun = clsSQLServer.gCampoValorConsul(mstrConn, "mails_modelos_consul @mamo_id=" & cmbMail.Valor.ToString, "mamo_asun")
            lstrCmd = "mails_mode_catego_consul @mmca_acti_id=2, @mmca_cate_id=" & lstrCate & ",@mmca_mamo_id=" & cmbMail.Valor.ToString
            If lstrInst <> "" Then
               lstrCmd = lstrCmd & ",@mmca_inst_id=" & lstrInst
            End If
            lstrMens = clsSQLServer.gCampoValorConsul(mstrConn, lstrCmd, "mmca_mens")
            lstrMail = mObtenerMails(Session("ds"), lstrCate, lstrInst)
            If lstrMail <> "" And lstrMens <> "" Then
                clsMail.gEnviarMail(lstrMail, lstrAsun, lstrMens, lstrMailMode)
            End If
         Next
         ds.Dispose()

         btnEnvi.Visible = False
         btnList.Visible = True
         cmbRepo.Enabled = True
         grdCons.Visible = False
         usrSocFil.Visible = True

         Throw New AccesoBD.clsErrNeg("Se han enviado los mails a los socios seleccionados.")

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      If cmbRepo.Valor.ToString = "M" Then
         mConsultarMails(grdCons)
      Else
         mListar()
      End If
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarMails(grdCons)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarMails(ByVal pobjGrid As System.Web.UI.WebControls.DataGrid)
      Try

         hdnRptId.Text = usrSocFil.ObtenerIdFiltro.ToString()

         Dim lstrCmd As String = "exec socios_general_mails_consul "
         lstrCmd += " @rptf_id=" + hdnRptId.Text
         ''lstrCmd += ",@rptf_top=1"

         Dim ds As New DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

         For Each dc As DataColumn In ds.Tables(0).Columns
             Dim dgCol As New Web.UI.WebControls.BoundColumn
             dgCol.DataField = dc.ColumnName
             dgCol.HeaderText = dc.ColumnName
             If dc.Ordinal = 0 Or dc.ColumnName.IndexOf("_id") <> -1 Then
                dgCol.Visible = False
             End If
             pobjGrid.Columns.Add(dgCol)
         Next
         pobjGrid.DataSource = ds
         pobjGrid.DataBind()
         Session("ds") = DirectCast(pobjGrid.DataSource, DataSet)
         pobjGrid.Visible = True
         btnEnvi.Enabled = (pobjGrid.Items.Count > 0)
         btnEnvi.Visible = True
         btnList.Visible = False
         usrSocFil.Visible = False
         cmbRepo.Enabled = False

         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvi.Click
      mEnviarMails()
   End Sub

End Class
End Namespace
