Namespace SRA

Partial Class RecibosFecha
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            txtFechaDesde.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
            txtFechaHasta.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
            mCargarCombos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEdesde, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEhasta, "T")
   End Sub

   Private Sub mLimpiarFiltros()
        txtFechaDesde.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
        txtFechaHasta.Fecha = New DateTime(Now.Year, Now.Month, Now.Day)
        cmbCEdesde.Limpiar()
        cmbCEhasta.Limpiar()
        cmbTipo.Limpiar()
        chkCate.Checked = False
        chkConta.Checked = False
        chkDeta.Checked = False
        chkPFP.Checked = False
        chkSoli.Checked = False
   End Sub

#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      mListar()
   End Sub

   Private Sub mListar()
      Try
         Dim lstrRptName As String = "RecibosFecha"
         Dim lstrRpt As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         If txtFechaDesde.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha desde.")
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
         End If

         If txtFechaHasta.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la fecha hasta.")
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&tipo=" + cmbTipo.Valor.ToString
         lstrRpt += "&cemi_desde=" + IIf(cmbCEdesde.Valor.ToString = "", "0", cmbCEdesde.Valor.ToString)
         lstrRpt += "&cemi_hasta=" + IIf(cmbCEhasta.Valor.ToString = "", "0", cmbCEhasta.Valor.ToString)
         lstrRpt += "&fecha_desde=" & lintFechaDesde.ToString
         lstrRpt += "&fecha_hasta=" & lintFechaHasta.ToString
         lstrRpt += "&cate=" & IIf(chkCate.Checked, "S", "N")
         lstrRpt += "&conta=" & IIf(chkConta.Checked, "S", "N")
         lstrRpt += "&pfp=" & IIf(chkPFP.Checked, "S", "N")
         lstrRpt += "&soli=" & IIf(chkSoli.Checked, "S", "N")
         lstrRpt += "&deta=" & IIf(chkDeta.Checked, "S", "N")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

End Class

End Namespace
