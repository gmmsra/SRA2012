Namespace SRA

Partial Class PagosAcreditaciones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            txtFechaDesdeFil.Fecha = Now
            txtFechaHastaFil.Fecha = Now
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuba, "T", "@banc_id=-1")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "T", "@con_cuentas=1")
        clsWeb.gCargarRefeCmb(mstrConn, "monedas_sinDefa", cmbMonedaFil, "T")
        Me.CargarComboEmpresasCobradorasElectronicas()
    End Sub

    ' Dario 2013-06-18 metodo que se utiliza para la carge del combo de empresas cobradoras electronicas    
    Private Sub CargarComboEmpresasCobradorasElectronicas()
        Try
            ' coloco el valor por defecto del combo
            cmbEmpCobElecFil.Items.Add(New System.Web.UI.WebControls.ListItem("(Todos)", "-1"))
            ' instancio el objeto cobranzas de negocio
            Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
            ' ejecuto el metodo que recupera los datos a mostar en la grilla
            Dim ds As DataSet = _CobranzasBusiness.GetEmpresasCobranzasElectronicas(Me.User.Identity.Name)
            ' controlo que tenga algo la consulta de empresas
            If (Not ds Is Nothing And ds.Tables.Count > 0) Then
                ' si tiene 0 o mas de uno agrega el seleccione sino deja solo el unico que tiene
                If (ds.Tables(0).Rows.Count > 0) Then
                    cmbEmpCobElecFil.Items.Add(New System.Web.UI.WebControls.ListItem("(Solo Acrd. Bacrarias)", "0"))
                End If
                ' recorre el resultado de la consulta y lo carga en el combo
                For Each dr As DataRow In ds.Tables(0).Rows
                    ' agrego el item 
                    cmbEmpCobElecFil.Items.Add( _
                                                New System.Web.UI.WebControls.ListItem(dr("varDesEmpresaCobElect").ToString() _
                                                                                          , dr("intIdEmprezasCobranzasElectronicas").ToString()))
                Next
            End If
            ds.Dispose()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer

            lstrRptName = "PagosAcreditaciones"

            If txtFechaDesdeFil.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaHastaFil.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&emct_id=" + IIf(cmbCentroEmisor.Valor.ToString = "", "0", cmbCentroEmisor.Valor.ToString)
            lstrRpt += "&paco_cuba_id=" + IIf(cmbCuba.Valor.ToString = "", "0", cmbCuba.Valor.ToString)
            lstrRpt += "&mone_id=" + IIf(cmbMonedaFil.Valor.ToString = "", "0", cmbMonedaFil.Valor.ToString)
            ' Dario 2013-06-18 se agragan dos nuevos filtros
            lstrRpt += "&IntIdEmpCobElec=" & cmbEmpCobElecFil.Valor.ToString
            lstrRpt += "&varNroCompCobElect=" & txtNroCompCobElectFil.Valor.ToString

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
