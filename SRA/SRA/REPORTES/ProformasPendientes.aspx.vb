Namespace SRA

Partial Class ProformasPendientes
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtPuntaje As NixorControls.NumberBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mCargarActividades()
                clsWeb.gInicializarControles(Me, mstrConn)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarActividades()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ", @factura = 1") ', @actiProf = 1 ")
    End Sub
#End Region

    Private Sub mConsultarMails(ByVal pobjGrid As System.Web.UI.WebControls.DataGrid)
        Try

            Dim lstrCmd As String = "exec proformas_pendientes_rpt_mails_consul "
            lstrCmd += " @Acti_id=" + clsSQLServer.gFormatArg(cmbActi.Valor.ToString, SqlDbType.Int)
            lstrCmd += ",@FechaDesde=" + clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd += ",@FechaHasta=" + clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd += ",@Cliente=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            lstrCmd += ",@GastAdmin=" + clsSQLServer.gFormatArg(IIf(txtGastosAdm.Valor.ToString = "", "0", txtGastosAdm.Valor.ToString), SqlDbType.Decimal)

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

            For Each dc As DataColumn In ds.Tables(0).Columns
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                dgCol.DataField = dc.ColumnName
                dgCol.HeaderText = dc.ColumnName
                If dc.Ordinal = 0 Or dc.ColumnName.IndexOf("_id") <> -1 Then
                    dgCol.Visible = False
                End If
                pobjGrid.Columns.Add(dgCol)
            Next
            pobjGrid.DataSource = ds
            pobjGrid.DataBind()
            pobjGrid.Columns(5).Visible = False 'neto
            pobjGrid.Columns(6).Visible = False 'prfr_id
            pobjGrid.Columns(7).Visible = False 'raza
            pobjGrid.Columns(8).Visible = False
            pobjGrid.Columns(9).Visible = False
            pobjGrid.Columns(10).Visible = False
            pobjGrid.Columns(11).Visible = False
            pobjGrid.Columns(12).Visible = False
            pobjGrid.Columns(13).Visible = False
            pobjGrid.Columns(14).Visible = False
            pobjGrid.Columns(15).Visible = False
            pobjGrid.Columns(16).Visible = False
            pobjGrid.Columns(17).Visible = False
            pobjGrid.Columns(18).Visible = False
            pobjGrid.Columns(19).Visible = False
            Session("dsMails") = ds

            pobjGrid.Visible = True
            btnEnvi.Enabled = (pobjGrid.Items.Count > 0)
            btnEnvi.Visible = True
            btnList.Visible = False
            cmbTipoListado.Enabled = False
            cmbActi.Enabled = False
            txtFechaDesdeFil.Enabled = False
            txtFechaHastaFil.Enabled = False
            usrClieFil.Activo = False
            txtGastosAdm.Enabled = False

            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mListar()

        Dim lstrRptName As String
        Dim lintFechaDesde As Integer
        Dim lintFechaHasta As Integer

        Select Case cmbTipoListado.Valor.ToString
            Case "C"
                If cmbActi.Valor.ToString = 4 Then
                    lstrRptName = "PropormaPendienteLabCarta"
                Else
                    lstrRptName = "ProformasPendientesCarta"
                End If
            Case "L"
                If usrClieFil.Valor.ToString = "0" Then
                    lstrRptName = "ProformasPendientesListado"
                Else
                    lstrRptName = "ProformasPendientesListadoIndiv"
                End If
        End Select

        If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
        Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
        End If

        If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
        Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
        End If

        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        If cmbTipoListado.Valor.ToString <> "C" Then
            lstrRpt += "&acti_desc=" + cmbActi.SelectedItem.Text
        Else
            lstrRpt += "&SinMail=" + cmbCarta.Valor
        End If

        lstrRpt += "&Acti_id=" + cmbActi.Valor.ToString
        lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
        lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
        lstrRpt += "&Cliente=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
        lstrRpt += "&GastAdmin=" + IIf(txtGastosAdm.Valor.ToString = "", "0", txtGastosAdm.Valor.ToString.Replace(".", ","))
        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)

    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            If cmbActi.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar Actividad.")
            End If
            If cmbTipoListado.Valor.ToString = "" Or cmbTipoListado.Valor.ToString = "(Seleccione)" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el Tipo de Listado.")
            End If

            If cmbTipoListado.Valor.ToString = "M" Then
                mConsultarMails(grdCons)
            Else
                mListar()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarFiltros()
        cmbActi.Limpiar()
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        usrClieFil.Limpiar()
        txtGastosAdm.Text = ""

        cmbActi.Enabled = True
        txtFechaDesdeFil.Enabled = True
        txtFechaHastaFil.Enabled = True
        usrClieFil.Activo = True
        txtGastosAdm.Enabled = True

        cmbTipoListado.Limpiar()
        cmbTipoListado.Enabled = True
        grdCons.Visible = False
        btnEnvi.Visible = False
        btnList.Visible = True
    End Sub

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdCons.EditItemIndex = -1
            If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
                grdCons.CurrentPageIndex = 0
            Else
                grdCons.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarMails(grdCons)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEnviarMensajeDir(ByVal pstrConn As String, ByVal pstrTemplate As String, ByVal pstrMails As String, ByVal pdsDatos As DataSet, ByVal pstrClie As String, _
                                  ByVal pstrClieId As String, ByVal pstrProforma As String, ByVal pstrNeto As String, ByVal pstrRaza As String, _
                                  ByVal pstrDiclDire As String, ByVal pstrDiclCpos As String, ByVal pstrLocaDesc As String, ByVal pstrPciaDesc As String, _
                                  ByVal pstrSociNume As String, ByVal pstrCriador As String, ByVal pstrRp As String, ByVal pstrSobreTasa As String)

        Dim lstrAsun As String = "Aviso de Pagos Pendientes"
        Dim lstrMsg As String = SRA_Neg.clsMail.mLeerTemplate(pstrTemplate + ".htm")
        Dim lstrText As String
        Dim lstrMsgRet As String
        Dim lintPosiIni As Integer
        Dim lintPosiFin As Integer
        Dim lstrTempHead As String
        Dim lstrTempRepe As String
        Dim lstrTempFoot As String
        Dim lstrTemp As String

        If System.Configuration.ConfigurationSettings.AppSettings("conEnviarMails").ToString() = "1" Then

            lintPosiIni = lstrMsg.IndexOf("<!-- inicio_repeticion -->")
            lintPosiFin = lstrMsg.IndexOf("<!-- fin_repeticion -->")

            If (lintPosiIni <> -1) Then
                If (lintPosiIni <> 0) Then
                    lstrTempHead = lstrMsg.Substring(0, lintPosiIni - 1)
                End If
                lstrTempRepe = lstrMsg.Substring(lintPosiIni + 26, (lintPosiFin - lintPosiIni - 26))
                lstrTempFoot = lstrMsg.Substring(lintPosiFin + 23)
            Else
                lstrTempRepe = lstrMsg
            End If

            lstrMsgRet = lstrTempHead
            Dim ldrRow As DataRow

            With pdsDatos.Tables(0)
                For i As Integer = 0 To .DefaultView.Count - 1
                    lstrTemp = lstrTempRepe
                    ldrRow = .DefaultView.Item(i).Row
                    For Each ldcCol As DataColumn In .Columns
                        If Not ldrRow.IsNull(ldcCol.ColumnName) Then
                            If ldcCol.DataType.Name = "DateTime" Then
                                lstrText = CDate(ldrRow.Item(ldcCol.ColumnName)).ToString("dd/MM/yyyy")
                            Else
                                lstrText = ldrRow.Item(ldcCol.ColumnName).ToString
                            End If
                        Else
                            lstrText = ""
                        End If
                        lstrText = lstrText.Replace(Chr(10) + Chr(13), "<br>")
                        lstrTemp = lstrTemp.Replace("[" + ldcCol.ColumnName + "]", lstrText)
                    Next
                    lstrMsgRet += lstrTemp
                Next
            End With

            lstrMsgRet += lstrTempFoot

            lstrMsgRet = lstrMsgRet.Replace("[cliente]", pstrClie)
            lstrMsgRet = lstrMsgRet.Replace("[clie_id]", pstrClieId)
            lstrMsgRet = lstrMsgRet.Replace("[NumProforma]", pstrProforma)
            lstrMsgRet = lstrMsgRet.Replace("[neto]", pstrNeto)
            lstrMsgRet = lstrMsgRet.Replace("[raza]", pstrRaza)
            lstrMsgRet = lstrMsgRet.Replace("[dicl_dire]", pstrDiclDire)
            lstrMsgRet = lstrMsgRet.Replace("[dicl_cpos]", pstrDiclCpos)
            lstrMsgRet = lstrMsgRet.Replace("[loca_desc]", pstrLocaDesc)
            lstrMsgRet = lstrMsgRet.Replace("[pcia_desc]", pstrPciaDesc)
            lstrMsgRet = lstrMsgRet.Replace("[soci_nume]", pstrSociNume)
            lstrMsgRet = lstrMsgRet.Replace("[criador]", pstrCriador)
            lstrMsgRet = lstrMsgRet.Replace("[rp]", pstrRp)
            lstrMsgRet = lstrMsgRet.Replace("[sobretasa]", pstrSobreTasa)

            SRA_Neg.clsMail.gEnviarMail(pstrMails, lstrAsun, lstrMsgRet, "")

        End If
    End Sub

    Private Sub mEnviarMails()
        Try
            Dim lstrMail As String
            Dim lstrClie As String
            Dim lstrClieId As String
            Dim lstrProf As String
            Dim lstrNeto As String
            Dim lstrRaza As String = ""
            Dim lstrCmd As String
            Dim lstrMailMode As String
            Dim lstrDiclDire As String
            Dim lstrDiclCpos As String
            Dim lstrLocaDesc As String
            Dim lstrPciaDesc As String
            Dim lstrSociNume As String
            Dim lstrCriador As String
            Dim lstrRp As String
            Dim lstrSobreTasa As String
            Dim ds As DataSet = Session("dsMails")
            Dim dsDeta As New DataSet

            If cmbActi.Valor.ToString = 4 Then
                lstrMailMode = "PropormaPendienteLabCarta"
            Else
                lstrMailMode = "PropormaPendienteCarta"
            End If

            lstrMailMode = "..\templates\" & lstrMailMode

            For Each odrDeta As DataRow In ds.Tables(0).Rows
                lstrMail = odrDeta.Item("mails")
                lstrClie = odrDeta.Item("nombre")
                lstrClieId = odrDeta.Item("cliente")
                lstrProf = odrDeta.Item("proforma")
                lstrNeto = odrDeta.Item("neto")
                If Not IsDBNull(odrDeta.Item("raza")) Then lstrRaza = odrDeta.Item("raza")
                lstrDiclDire = Trim(odrDeta.Item("dicl_dire"))
                lstrDiclCpos = Trim(odrDeta.Item("dicl_cpos"))
                lstrLocaDesc = Trim(odrDeta.Item("loca_desc"))
                lstrPciaDesc = Trim(odrDeta.Item("pcia_desc"))
                lstrSociNume = Trim(odrDeta.Item("soci_nume"))
                lstrCriador = Trim(odrDeta.Item("criador"))
                lstrRp = Trim(odrDeta.Item("rp"))
                lstrSobreTasa = Trim(odrDeta.Item("sobretasa"))

                lstrCmd = "exec proformas_pendientes_rpt_mails_consul "
                lstrCmd = lstrCmd & " @prfr_id=" & odrDeta.Item("prfr_id")
                dsDeta = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

                mEnviarMensajeDir(mstrConn, lstrMailMode, lstrMail, dsDeta, lstrClie, lstrClieId, lstrProf, lstrNeto, lstrRaza, _
                                  lstrDiclDire, lstrDiclCpos, lstrLocaDesc, lstrPciaDesc, lstrSociNume, lstrCriador, lstrRp, lstrSobreTasa)
            Next

            btnEnvi.Visible = False
            btnList.Visible = True
            cmbTipoListado.Enabled = True
            grdCons.Visible = False

            mLimpiarFiltros()
            Throw New AccesoBD.clsErrNeg("Se han enviado los mails a los clientes.")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvi.Click
        mEnviarMails()
    End Sub

End Class

End Namespace
