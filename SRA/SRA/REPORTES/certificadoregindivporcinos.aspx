<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CertificadoRegIndivPorcinos" CodeFile="CertificadoRegIndivPorcinos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Certificado de Registros Genealógicos Bovinos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../stylesheet/SRA.css">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="../imagenes/recsup.jpg"><IMG border="0" src="../imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="../imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="../imagenes/reciz.jpg" width="9"><IMG border="0" src="../imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Certificado de Registro Genealógico Porcinos</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!-- FOMULARIO -->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.91%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo" Width="58px">Criador: </asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriadorFil" runat="server" MostrarBotones="False" AceptaNull="false" FilCUIT="True"
																						ColCUIT="True" FilTarjNume="False" FilAgru="False" ColCriaNume="True" FilClaveUnica="True"
																						CampoVal="Criador" Criador="True" FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True"
																						Saltos="1,2" Tabla="Criadores"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCriaNumeHasta" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbSexoFil" runat="server" Width="113px">
																						<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																						<asp:ListItem Value="false">Hembra</asp:ListItem>
																						<asp:ListItem Value="true">Macho</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblSWDesde" runat="server" cssclass="titulo">SW Padre:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtSWPadre" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblSWHasta" runat="server" cssclass="titulo">SW Madre:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtSWMadre" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Fecha Denuncia:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDenuncia" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																						enabled="true"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label2" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaNacimiento" runat="server" cssclass="cuadrotextodeshab" Width="60px"
																						Obligatorio="true" enabled="true"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label3" runat="server" cssclass="titulo">Fecha Ingreso:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaIngreso" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																						enabled="true"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR> <!--<TR>
																				<TD style="WIDTH: 211px" background="../imagenes/formfdofields.jpg" width="211" align="right">
																					<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<asp:RadioButton id="rbtnFechaProcesp" runat="server" cssclass="titulo" Width="128px" checked="True"
																						GroupName="RadioGroup1" Text="Fecha Proceso"></asp:RadioButton>
																					<asp:RadioButton id="rbtnFechaInscripcion" runat="server" cssclass="titulo" Width="149px" checked="False"
																						GroupName="RadioGroup1" Text="Fecha Inscripción"></asp:RadioButton>&nbsp;&nbsp;
																					<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotextodeshab" Width="408px" Obligatorio="true"
																						enabled="true"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>-->
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFiltro" runat="server" cssclass="titulo">Filtro:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbCertifTipoFil" runat="server" Width="113px">
																						<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																						<asp:ListItem Value="false">Pendientes</asp:ListItem>
																						<asp:ListItem Value="true">Originales</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																			ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"
																			CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro ---></TBODY></TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="../imagenes/recde.jpg" width="13"><IMG border="0" src="../imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="../imagenes/recinf.jpg"><IMG border="0" src="../imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="../imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox>&nbsp;</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
	</BODY>
</HTML>
