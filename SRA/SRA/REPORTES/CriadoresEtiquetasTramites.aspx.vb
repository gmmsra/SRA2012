Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports AccesoBD
Imports Interfaces.Importacion


Namespace SRA



Partial Class CriadoresEtiquetasTramites
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GSZ 12-11-2014 
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "rg_tramites_tipos", cmbTipoTramiteFil, "T")

    End Sub
    Private Sub mValidarDatos()

        If usrCriadorFil.Valor.ToString() = "" Then
            Throw New AccesoBD.clsErrNeg("El Criador debe ser distinto de  vacio")
        End If

        If usrCriadorFil.RazaId = "" Then
            Throw New AccesoBD.clsErrNeg("La Raza debe ser distinto de  vacio")
        End If

    End Sub
    Private Sub mListar()
        Try
            'GSZ Creado 04/12/2014
            'GSZ 15/12/2014 Se agrego rango de criador Desde -Hasta
            Dim params As String
            Dim lstrRptName As String = "EtiquetasCriadoresTramites"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim stRazaDescripcion As String
            mValidarDatos()

            params += "&cria_id=" & IIf(usrCriadorFil.Valor = 0, "0", _
                                                      usrCriadorFil.Valor.ToString())


            params += "&raza_id=" & IIf(usrCriadorFil.RazaId.ToString = "", "0", _
                                    usrCriadorFil.RazaId.ToString)
            Dim lintFechaInicioD, lintFechaInicioH As Integer
            Dim lintFechaPresD, lintFechaPresH As Integer




            'GSZ 25/02/2015 Se adaptaron las fechas para reportes y se agrego fecha de inscripcion
            If (txtPresentaFechaDesde.Fecha.ToString <> "") Then
                lintFechaPresD = CType(clsFormatear.gFormatFechaString(txtPresentaFechaDesde.Text, "Int32"), Integer) - 2
            Else
                lintFechaPresD = 0

            End If

            If (txtPresentaFechaHasta.Fecha.ToString <> "") Then
                lintFechaPresH = CType(clsFormatear.gFormatFechaString(txtPresentaFechaHasta.Text, "Int32"), Integer) - 2
            Else
                lintFechaPresH = 0

            End If



            'GSZ 26/02/2015 se agrego fecha de inscripcion cpn  -2 por error en FromOADate

            If (txtInicioFechaDesde.Fecha.ToString <> "") Then
                lintFechaInicioD = CType(clsFormatear.gFormatFechaString(txtInicioFechaDesde.Text, "Int32"), Integer) - 2
            Else
                lintFechaInicioD = 0

            End If

            If (txtInicioFechaHasta.Fecha.ToString <> "") Then
                lintFechaInicioH = CType(clsFormatear.gFormatFechaString(txtInicioFechaHasta.Text, "Int32"), Integer) - 2
            Else
                lintFechaInicioH = 0

            End If


            params += "&dPresDesde=" + lintFechaPresD.ToString
            params += "&dPresHasta=" + lintFechaPresH.ToString

            params += "&dInicDesde=" + lintFechaInicioD.ToString
            params += "&dInicHasta=" + lintFechaInicioH.ToString



            params += "&ttra_id=" & IIf(cmbTipoTramiteFil.Valor.ToString = "", "0", _
                                  cmbTipoTramiteFil.Valor.ToString)

            params += "&cria_nume_desde=" + IIf(txtCriaNumeDesde.Text = "", "0", txtCriaNumeDesde.Text)
            params += "&cria_nume_hasta=" + IIf(txtCriaNumeHasta.Text = "", "0", txtCriaNumeHasta.Text)

            Dim a As Integer
            Dim strCadena As String = ""
            'para recorrer todo el listbox
            'GSZ 29/12/2014 se agrego  lista de Criadores
            For a = 0 To lstCriadores.Items.Count - 1
                strCadena += SRA_Neg.Utiles.LimpiarListbox(lstCriadores.Items(a).ToString().Trim()) + ","
            Next a
            If strCadena.ToString().Trim() <> "" Then
                params += "&CriadoresIds=" + Mid(strCadena, 1, Len(strCadena) - 1)
            End If

            lstrRpt += params

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        usrCriadorFil.Limpiar()
        txtPresentaFechaDesde.Text = ""
        txtPresentaFechaHasta.Text = ""
        txtInicioFechaDesde.Text = ""
        txtInicioFechaHasta.Text = ""
        txtCriaNumeDesde.Text = ""
        txtCriaNumeHasta.Text = ""

        cmbTipoTramiteFil.Limpiar()
        lstCriadores.Items.Clear()
        txtCriaSel.Text = ""
    End Sub

    Private Sub txtCriadorSel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtCriaSel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCriaSel.TextChanged
        Try


            mValidarDatos()
            Dim dtCria As DataTable
            Dim strCliente As String
            Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
            If txtCriaSel.Text <> "" Then
                dtCria = oCriador.GetCriadorByRazaIdCriaNume(usrCriadorFil.RazaId.ToString(), txtCriaSel.Text)

                If (dtCria.Rows.Count > 0) Then
                    strCliente = dtCria.Rows(0).Item("clie_fanta")
                    lstCriadores.Items.Add(txtCriaSel.Text + "-" + strCliente)

                End If

            End If
            txtCriaSel.Text = ""
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub txtCriaSel_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCriaSel.PreRender

    End Sub

    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        If lstCriadores.SelectedIndex <> -1 Then
            lstCriadores.Items.RemoveAt(lstCriadores.SelectedIndex)
        End If
    End Sub

    Private Sub btnLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        lstCriadores.Items.Clear()
        txtCriaSel.Text = ""
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Try


            mValidarDatos()
            Dim dtCria As DataTable
            Dim strCliente As String
            Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
            If txtCriaSel.Text <> "" Then
                dtCria = oCriador.GetCriadorByRazaIdCriaNume(usrCriadorFil.RazaId.ToString(), txtCriaSel.Text)

                If (dtCria.Rows.Count > 0) Then
                    strCliente = dtCria.Rows(0).Item("clie_fanta")
                    lstCriadores.Items.Add(txtCriaSel.Text + "-" + strCliente)

                End If

            End If
            txtCriaSel.Text = ""
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class

End Namespace
