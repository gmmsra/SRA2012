Namespace SRA

Partial Class SaldoEmbrionesStock
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

	'El Dise�ador de Web Forms requiere esta llamada.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

	Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
	Protected WithEvents lblProductoFil As System.Web.UI.WebControls.Label


	'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
	'No se debe eliminar o mover.

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
		'No la modifique con el editor de c�digo.
		InitializeComponent()
	End Sub

#End Region

#Region "Definici�n de Variables"
	Private mstrTabla As String = SRA_Neg.Constantes.gTab_EmbrionesStock
	Private mstrCmd As String
	Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"

	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()
			If (Not Page.IsPostBack) Then
				'Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                Dim dtProducto As DataTable
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim strNombreProducto As String
                Dim boolVieneSeleccionado As Boolean = False

                If Request.QueryString("ProdID") <> "" Then
                    boolVieneSeleccionado = True
                    hdnProductoId.Text = Request.QueryString("ProdID")
                    dtProducto = oProducto.GetProductoById(hdnProductoId.Text, "")
                    If dtProducto.Rows(0).Item("prdt_sexo") Then
                        usrPadreFil.Valor = hdnProductoId.Text
                        usrPadreFil.RazaId = dtProducto.Rows(0).Item("prdt_raza_id")
                    Else
                        usrMadreFil.Valor = hdnProductoId.Text
                        usrMadreFil.RazaId = dtProducto.Rows(0).Item("prdt_raza_id")
                    End If

                    mMostrarPanel(True)

                End If

                If Not boolVieneSeleccionado Then

                    If Request.QueryString("criadorID") <> "" Then
                        hdnCriadorId.Text = Request.QueryString("criadorID")
                        usrMadreFil.CriaId = hdnCriadorId.Text
                        usrMadreFil.CriaOrPropId = hdnCriadorId.Text
                        hdnCriadorId.Text = Request.QueryString("criadorID")
                        lblCriaDesc.Text = "Criador: " + Request.QueryString("criadorNomb")

                        lblCriaDesc.Text = "Criador: " + Request.QueryString("criadorNomb")
                        mMostrarPanel(False)
                    Else
                        mMostrarPanel(True)
                    End If

                    If Request.QueryString("clieID") <> "" Then
                        hdnClienteId.Text = Request.QueryString("clieID")
                        lblClieDesc.Text = "Cliente: " + Request.QueryString("clieNomb")
                        mMostrarPanel(False)
                    Else
                        mMostrarPanel(True)
                    End If

                    If Request.QueryString("PId") <> "" Then
                        strNombreProducto = oProducto.GetProductoDescripByProductoId(Request.QueryString("PId"))

                        hdnProductoId.Text = Request.QueryString("PId")
                        lblProdDescP.Text = "Padre: " + strNombreProducto.Trim()
                        mMostrarPanel(False)
                    Else
                        mMostrarPanel(True)
                    End If


                    If Request.QueryString("MId") <> "" Then
                        strNombreProducto = oProducto.GetProductoDescripByProductoId(Request.QueryString("MId"))

                        hdnProductoIdM.Text = Request.QueryString("MId")
                        lblProdDescM.Text = "Madre: " + strNombreProducto.Trim()
                        mMostrarPanel(False)
                    Else
                        mMostrarPanel(True)
                    End If

                End If

                mEstablecerPerfil()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mEstablecerPerfil()
		Dim lbooPermiAlta As Boolean
		Dim lbooPermiModi As Boolean

	End Sub

	Public Sub mInicializar()

        usrClieFil.Tabla = "clientes"
        usrClieFil.AutoPostback = False
        usrClieFil.ColClaveUnica = True
        usrClieFil.FilClaveUnica = True
        usrClieFil.ColClieNume = True
        usrClieFil.ColCUIT = True
        usrClieFil.FilCUIT = True
        usrClieFil.ColDocuNume = True
        usrClieFil.FilDocuNume = True
        usrClieFil.ColSociNume = True
        usrClieFil.FilSociNume = True
        ' usrClieFil.FilTipo = "T"


		usrPadreFil.FilSexo = False
		usrPadreFil.FilRpNume = True
		usrPadreFil.FilNumExtr = False
		usrPadreFil.Sexo = 1


		usrMadreFil.FilSexo = False
		usrMadreFil.FilRpNume = True
		usrMadreFil.FilNumExtr = False
		usrMadreFil.Sexo = 0

	End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
	Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdDato.EditItemIndex = -1
			If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
				grdDato.CurrentPageIndex = 0
			Else
				grdDato.CurrentPageIndex = E.NewPageIndex
			End If
			mConsultar()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Public Sub mConsultar()
        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet

            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim strSexo As String
            Dim strSexoFil As String

            strSexo = oProducto.GetSexoByProductoId(hdnProductoId.Text)

            mstrCmd = "exec saldo_" + mstrTabla + "_consul "
            If hdnCriadorId.Text <> "" Then
                mstrCmd = mstrCmd + "@cria_id=" + IIf(hdnCriadorId.Text = "", "0", hdnCriadorId.Text)

            Else
                mstrCmd = mstrCmd + "@cria_id=" + IIf(usrMadreFil.CriaOrPropId = "", "0", usrMadreFil.CriaOrPropId)

            End If

            If hdnClienteId.Text <> "" Then
                mstrCmd = mstrCmd + ",@clie_id=" + IIf(hdnClienteId.Text = "", "0", hdnClienteId.Text)
            Else
                mstrCmd = mstrCmd + ",@clie_id=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            End If


            If hdnProductoId.Text <> "" And strSexo = "True" Then
                mstrCmd = mstrCmd + ",@padre_id=" + IIf(hdnProductoId.Text = "", "0", hdnProductoId.Text)

            ElseIf hdnProductoId.Text <> "" And strSexo = "False" Then
                mstrCmd = mstrCmd + ",@madre_id=" + IIf(hdnProductoId.Text = "", "0", hdnProductoId.Text)
            ElseIf strSexo.Length <> 5 Then
                If strSexo = "1" Then
                    mstrCmd = mstrCmd + ",@padre_id=" + IIf(usrPadreFil.Valor.ToString = "", "0", usrPadreFil.Valor.ToString)
                Else
                    mstrCmd = mstrCmd + ",@madre_id=" + IIf(usrMadreFil.Valor.ToString = "", "0", usrMadreFil.Valor.ToString)
                End If

            End If



            If hdnProductoIdM.Text <> "" Then
                mstrCmd = mstrCmd + ",@Madre_id=" + IIf(hdnProductoIdM.Text = "", "0", hdnProductoIdM.Text)

            ElseIf strSexo.Length <> 5 Then
                If strSexo = "1" Then
                    mstrCmd = mstrCmd + ",@padre_id=" + IIf(usrPadreFil.Valor.ToString = "", "0", usrPadreFil.Valor.ToString)
                Else
                    mstrCmd = mstrCmd + ",@madre_id=" + IIf(usrMadreFil.Valor.ToString = "", "0", usrMadreFil.Valor.ToString)
                End If
            End If


            If usrMadreFil.RazaId <> "" Then
                mstrCmd = mstrCmd + ",@raza_id=" + IIf(usrMadreFil.RazaId = "", "0", usrMadreFil.RazaId)
            End If

            If usrPadreFil.txtProdNombExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_nomb= " + clsSQLServer.gFormatArg(usrPadreFil.txtProdNombExt.Text, SqlDbType.VarChar)
                strSexoFil = "1"

            End If
            If usrPadreFil.txtRPExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_rp= " + clsSQLServer.gFormatArg(usrPadreFil.txtRPExt.Text, SqlDbType.VarChar)
                strSexoFil = "1"
            End If

            If usrPadreFil.txtSraNumeExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_sra_nume= " + clsSQLServer.gFormatArg(usrPadreFil.txtSraNumeExt.Text, SqlDbType.Int)
                strSexoFil = "1"
            End If

            If usrMadreFil.txtProdNombExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_nomb= " + clsSQLServer.gFormatArg(usrMadreFil.txtProdNombExt.Text, SqlDbType.VarChar)
                strSexoFil = "0"
            End If
            If usrMadreFil.txtRPExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_rp= " + clsSQLServer.gFormatArg(usrMadreFil.txtRPExt.Text, SqlDbType.VarChar)
                strSexoFil = "0"
            End If

            If usrMadreFil.txtSraNumeExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_sra_nume= " + clsSQLServer.gFormatArg(usrMadreFil.txtSraNumeExt.Text, SqlDbType.Int)

                strSexoFil = "0"
            End If
            If strSexoFil = "0" Or strSexoFil = "1" Then
                mstrCmd = mstrCmd + ", @prdt_Sexo= " + clsSQLServer.gFormatArg(IIf(strSexoFil = "1", "1", "0"), SqlDbType.VarChar)
            End If





            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)

            grdDato.DataSource = dsDatos
            grdDato.DataBind()


            If hdnCriadorId.Text <> "" Then
                grdDato.Columns(1).Visible = False
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
	End Sub
#End Region

#Region "Seteo de Controles"

	Private Sub mLimpiarFiltro()

        usrClieFil.Limpiar()
        usrPadreFil.Limpiar()
		usrMadreFil.Limpiar()
		grdDato.CurrentPageIndex = 0
		grdDato.Visible = False
	End Sub

	Private Sub mCerrar()
		mMostrarPanel(False)
	End Sub

	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		If (pbooVisi) Then
			hdnPage.Text = " "
		Else
			hdnPage.Text = ""
		End If
		panFiltro.Visible = pbooVisi
		panCria.Visible = Not pbooVisi
	End Sub

	Private Sub mListar()
		Try
			Dim lstrRptName As String

			lstrRptName = "SaldoEmbrionesStock"
				
			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If hdnCriadorId.Text <> "" Then
                lstrRpt += "&cria_id=" + IIf(hdnCriadorId.Text = "", "0", hdnCriadorId.Text)

            Else
                lstrRpt += "&cria_id=" + IIf(usrMadreFil.CriaOrPropId = "", "0", usrMadreFil.CriaOrPropId)
            End If

            If hdnClienteId.Text <> "" Then
                lstrRpt += "&clie_id=" + IIf(hdnClienteId.Text = "", "0", hdnClienteId.Text)
            Else
                lstrRpt += "&clie_id=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            End If

            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim booSexo As Boolean

            booSexo = oProducto.GetSexoByProductoId(hdnProductoId.Text)

            If hdnProductoId.Text <> "" Then
                If booSexo Then
                    lstrRpt += "&padre_id=" + IIf(hdnProductoId.Text = "", "0", hdnProductoId.Text)
                Else
                    lstrRpt += "&madre_id=" + IIf(hdnProductoId.Text = "", "0", hdnProductoId.Text)
                End If
            Else
                If booSexo Then
                    lstrRpt += "&padre_id=" + IIf(usrPadreFil.Valor.ToString = "", "0", usrPadreFil.Valor.ToString)
                Else
                    lstrRpt += "&madre_id=" + IIf(usrMadreFil.Valor.ToString = "", "0", usrMadreFil.Valor.ToString)
                End If
            End If



            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

#End Region

#Region "Eventos de Controles"

	Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		mListar()
	End Sub

#End Region

	Private Sub btnConsultar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConsultar.Click
		mConsultar()
	End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltro()
	End Sub
End Class
End Namespace
