Namespace SRA

Partial Class DeudaCliente
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mdecTotDebe As Decimal
   Private mdecTotHaber As Decimal
   Private mdecTotAplic As Decimal
   Private mdecTotOrig As Decimal
   Public mstrTipo As String

   Private Enum ColumnasFil As Integer
      lnk1 = 0
      lnk2 = 1
      comp_id = 2
      fecha = 3
      nume = 4
      impo_ori = 5
      debe = 6
      haber = 7
      aplicado = 8
      vto = 9
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mInicializar()
            mEstablecerPerfil()
            clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazaFil, "T")
            txtFechaValor.Text = DateTime.Now.ToString("dd/MM/yyyy")
            clsWeb.gInicializarControles(sender, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mEstablecerPerfil()
   End Sub

   Public Sub mInicializar()
      btnOtrasCuentas.Visible = False
      btnSaldosACta.Visible = False
      btnProformas.Visible = False
      btnAcuses.Visible = False
      btnMail.Visible = False
      chkListCuad.Visible = False
      mstrTipo = Request.QueryString("t")
      lblDeuda.Visible = (mstrTipo = "")
      rbtDeudaA.Visible = (mstrTipo = "")
      rbtDeudaC.Visible = (mstrTipo = "")
      rbtDeudaS.Visible = (mstrTipo = "")
      rbtDeudaC.Checked = (mstrTipo = "C")
      rbtDeudaS.Checked = (mstrTipo = "S")
      rbtDeudaA.Checked = (mstrTipo = "")
      Select Case mstrTipo
         Case "S"
            lblTitu.Text = "Resumen de Cuenta Social"
         Case "C"
            lblTitu.Text = "Resumen de Cuenta Corriente"
         Case Else
            lblTitu.Text = "Resumen de Cuenta"
      End Select
   End Sub
#End Region

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      Try
         mLimpiarFiltros()
         grdDatoBusq.CurrentPageIndex = 0
         grdDatoBusq.DataSource = Nothing
         grdDatoBusq.DataBind()
         btnList.Visible = False
         btnOtrasCuentas.Visible = False
         btnSaldosACta.Visible = False
         btnProformas.Visible = False
         btnAcuses.Visible = False
         btnMail.Visible = False
         chkListCuad.Visible = False

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      Try
         grdDatoBusq.CurrentPageIndex = 0
         mConsultarBusc()
         btnList.Visible = True
         chkListCuad.Visible = True
         btnOtrasCuentas.Visible = True
         btnSaldosACta.Visible = True
         btnProformas.Visible = True
         btnAcuses.Visible = True
         btnMail.Visible = True

         btnSaldosACta.Disabled = clsSQLServer.gExecuteQuery(mstrConn, "exec saldos_pagar_consul @clie_id=" & usrClieFil.Valor).Tables(0).Rows.Count = 0
         btnProformas.Disabled = clsSQLServer.gExecuteQuery(mstrConn, "exec proformas_busq @prfr_clie_id=" & usrClieFil.Valor).Tables(0).Rows.Count = 0
         btnAcuses.Disabled = clsSQLServer.gExecuteQuery(mstrConn, "exec acuses_busq @clie_id=" & usrClieFil.Valor).Tables(0).Rows.Count = 0

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDatoBusq.CurrentPageIndex = E.NewPageIndex
         mConsultarBusc()

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Public Sub mConsultarBusc()
      Dim lstrCmd As New StringBuilder

      mValidarDatos()
      lstrCmd.Append("exec rpt_deuda_cliente_consul")
      lstrCmd.Append(" @clie_id=" + usrClieFil.Valor.ToString)
      lstrCmd.Append(",@fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha))
      lstrCmd.Append(",@fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha))
      lstrCmd.Append(",@comp_cs=" + IIf(rbtDeudaA.Checked, "NULL", IIf(rbtDeudaC.Checked, "1", "0")))
      lstrCmd.Append(",@orden=" + IIf(rbtOrdenA.Checked, "1", "-1"))
      lstrCmd.Append(",@solo_saldo=" + Math.Abs(CInt(chkSaldo.Checked)).ToString)

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDatoBusq, 99999)
            Session("ResumenCuenta") = grdDatoBusq.DataSource 
   End Sub
#End Region

   Private Sub mEnviarMail()

         Dim lstrMsg As String = ""
         Dim ldecDebe As Decimal = 0
         Dim ldecHaber As Decimal = 0
         Dim ldecSinApli As Decimal = 0
         Dim lstrEnter As String = Chr(13) & Chr(10) 'clsSQLServer.gConsultarValor("select char(13)+char(10) as enter", mstrConn, 0)
         Dim lsbPagina As New System.Text.StringBuilder

         If usrClieFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
         End If

         If Request.QueryString("t") = "S" Then
            hdnMailAsun.Text = "Resumen de Cuenta Social - SRA"
         Else
            hdnMailAsun.Text = "Resumen de Cuenta Corriente - SRA"
         End If

         hdnMailDire.Text = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "cliente_defa_mail", "macl_mail", usrClieFil.Valor.ToString)

         hdnMail.Text = ""
         hdnMail.Text = hdnMail.Text & "Cliente: " & usrClieFil.Valor.ToString & " - " & usrClieFil.txtApelExt.Text
         hdnMail.Text = hdnMail.Text & lstrEnter & lstrEnter
         hdnMail.Text = hdnMail.Text & "Fecha" & " " & vbTab
         hdnMail.Text = hdnMail.Text & "Comprobante" & " " & vbTab & vbTab
         hdnMail.Text = hdnMail.Text & "Importe" & " " & vbTab
         hdnMail.Text = hdnMail.Text & "Debe" & " " & vbTab
         hdnMail.Text = hdnMail.Text & "Haber" & " " & vbTab
         hdnMail.Text = hdnMail.Text & "Sin Aplicar" & " " & vbTab
         hdnMail.Text = hdnMail.Text & "Fecha Vto" & " " & vbTab
         hdnMail.Text = hdnMail.Text & lstrEnter
         For Each oItem As DataGridItem In grdDatoBusq.Items
            hdnMail.Text = hdnMail.Text & clsFormatear.gFormatCadena(oItem.Cells(ColumnasFil.fecha).Text) & " " & vbTab
            hdnMail.Text = hdnMail.Text & (clsFormatear.gFormatCadena(oItem.Cells(ColumnasFil.nume).Text) & Space(30)).Substring(0, 30) & " " & vbTab
            hdnMail.Text = hdnMail.Text & ((clsFormatear.gFormatCadena(oItem.Cells(ColumnasFil.impo_ori).Text))) & " " & vbTab
            hdnMail.Text = hdnMail.Text & ((clsFormatear.gFormatCadena(oItem.Cells(ColumnasFil.debe).Text))) & " " & vbTab
            hdnMail.Text = hdnMail.Text & ((clsFormatear.gFormatCadena(oItem.Cells(ColumnasFil.haber).Text))) & " " & vbTab
            hdnMail.Text = hdnMail.Text & ((clsFormatear.gFormatCadena(oItem.Cells(ColumnasFil.aplicado).Text))) & " " & vbTab
            hdnMail.Text = hdnMail.Text & clsFormatear.gFormatCadena(oItem.Cells(ColumnasFil.vto).Text) & vbTab
            hdnMail.Text = hdnMail.Text & lstrEnter
            ldecDebe = ldecDebe + Convert.ToDecimal(oItem.Cells(ColumnasFil.debe).Text)
            ldecHaber = ldecHaber + Convert.ToDecimal(oItem.Cells(ColumnasFil.haber).Text)
            ldecSinApli = ldecSinApli + Convert.ToDecimal(oItem.Cells(ColumnasFil.aplicado).Text)
         Next
         'hdnMail.Text = hdnMail.Text & lstrEnter
         hdnMail.Text = hdnMail.Text & Space(10) & vbTab
         hdnMail.Text = hdnMail.Text & Space(30) & vbTab
         hdnMail.Text = hdnMail.Text & Space(10) & vbTab
         hdnMail.Text = hdnMail.Text & ldecDebe.ToString & " " & vbTab
         hdnMail.Text = hdnMail.Text & ldecHaber.ToString & " " & vbTab
         hdnMail.Text = hdnMail.Text & ldecSinApli.ToString & " " & vbTab
         hdnMail.Text = hdnMail.Text & vbTab
         hdnMail.Text = hdnMail.Text & lstrEnter
         hdnMail.Text = hdnMail.Text

         lsbPagina.Append("../mail.aspx?titulo=Envio de Mails&clie=" & usrClieFil.Codi)
         clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 780, 540, 10, 10, True)

   End Sub

   Private Sub mListar()
      Dim lstrRptName As String = "DeudaCliente"
      Dim lintFechaDesde As Integer
      Dim lintFechaHasta As Integer

      mValidarDatos()

      If txtFechaDesdeFil.Text <> "" Then
         lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
      End If

      If txtFechaHastaFil.Text <> "" Then
         lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
      End If

      Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

      lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
      If Not rbtDeudaA.Checked Then
         lstrRpt += "&comp_cs=" + IIf(rbtDeudaC.Checked, "1", "0").ToString
      End If
      lstrRpt += "&orden=" + IIf(rbtOrdenA.Checked, "1", "-1").ToString
      lstrRpt += "&cuadros=" + Math.Abs(CInt(chkListCuad.Checked)).ToString
      If chkListCuad.Checked Then
         'para que la carga sea mas rapida ya que solo se muestra el cuadro de totales.
         lstrRpt += "&fecha_desde=" + CType(clsFormatear.gFormatFechaString(DateTime.Now.ToString("dd/MM/yyyy"), "Int32"), Integer).ToString
         lstrRpt += "&fecha_hasta=" + CType(clsFormatear.gFormatFechaString(DateTime.Now.ToString("dd/MM/yyyy"), "Int32"), Integer).ToString
         lstrRpt += "&solo_saldo=1"
      Else
         lstrRpt += "&fecha_desde=" + lintFechaDesde.ToString
         lstrRpt += "&fecha_hasta=" + lintFechaHasta.ToString
         lstrRpt += "&solo_saldo=" + Math.Abs(CInt(chkSaldo.Checked)).ToString
      End If

      lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
      Response.Redirect(lstrRpt)
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      mListar()
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Sub mLimpiarFiltros()
      usrClieFil.Limpiar()
      txtFechaDesdeFil.Text = ""
      txtFechaHastaFil.Text = ""
      txtCriaFil.Text = ""
      txtSociFil.Text = ""
      cmbRazaFil.Limpiar()
      If rbtDeudaC.Visible Then rbtDeudaC.Checked = False
      If rbtDeudaS.Visible Then rbtDeudaS.Checked = False
      If rbtDeudaA.Visible Then rbtDeudaA.Checked = True
      rbtOrdenD.Checked = False
      rbtOrdenA.Checked = True
      chkSaldo.Checked = False
   End Sub

   Private Sub grdDatoBusq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDatoBusq.ItemDataBound
      Try
         Select Case e.Item.ItemType
            Case ListItemType.Header
               mdecTotDebe = 0
               mdecTotHaber = 0
               mdecTotAplic = 0
               mdecTotOrig = 0

            Case ListItemType.Item, ListItemType.AlternatingItem
               With CType(e.Item.DataItem, DataRowView).Row
                  mdecTotDebe += .Item("debe")
                  mdecTotHaber += .Item("haber")
                  mdecTotAplic += .Item("aplicado")
                  '                  mdecTotOrig += .Item("impo_ori")

                  If .Item("ver_rel") = 0 Then
                     e.Item.FindControl("lnk1").Visible = False
                  End If

                  If .Item("ver_det") = 0 Then
                     e.Item.FindControl("lnk2").Visible = False
                  End If
               End With

            Case ListItemType.Footer
               e.Item.Cells(ColumnasFil.debe).Text = mdecTotDebe.ToString("######0.00")
               e.Item.Cells(ColumnasFil.haber).Text = mdecTotHaber.ToString("######0.00")
               e.Item.Cells(ColumnasFil.aplicado).Text = mdecTotAplic.ToString("######0.00")
               '               e.Item.Cells(ColumnasFil.impo_ori).Text = mdecTotOrig.ToString("######0.00")
         End Select

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnOtrosDatos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOtrosDatos.Click
      mAbrirVentana("Otros Datos", , "O", 500)
   End Sub

#Region "Alertas/Ventanas-pop"

   Private Sub mAbrirVentana(ByVal pstrTitulo As String, Optional ByVal pboolTarj As Boolean = False, Optional ByVal pstrOrigen As String = "N", Optional ByVal pintAlto As Integer = 200)
      Try

         Dim lsbPagina As New System.Text.StringBuilder
         Dim lstrFecha As String = "" 'txtFechaValor.Text

         If usrClieFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
         End If

         If pboolTarj Then ' tarjetas
            If pstrOrigen = "L" Then ' leyenda
               lsbPagina.Append("consulta_pop.aspx?EsConsul=0&titulo=" & pstrTitulo & "&tabla=leyendas_factu")
            Else
               lsbPagina.Append("consulta_pop.aspx?EsConsul=1&titulo=" & pstrTitulo & "&tabla=tarjetas_clientes")
               lsbPagina.Append("&filtros=" & usrClieFil.Valor)
            End If
         Else
            lsbPagina.Append("../alertas_pop.aspx?titulo=" & pstrTitulo & "&clieId=" & usrClieFil.Valor.ToString & "&origen=" & pstrOrigen & "&FechaValor=" & lstrFecha & "&sociId=" & usrClieFil.SociId)
         End If

         clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 500, pintAlto, 150, 150)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

   Private Sub btnMail_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMail.ServerClick
       mEnviarMail()
   End Sub

End Class
End Namespace
