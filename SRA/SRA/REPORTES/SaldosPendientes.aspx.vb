Namespace SRA

Partial Class WebForm1
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            txtFechaDesdeFil.Fecha = Now
            txtFechaHastaFil.Fecha = Now
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "T")
   End Sub

   Private Sub mLimpiarFiltros()
      txtFechaDesdeFil.Fecha = Now
      txtFechaHastaFil.Fecha = Now
      cmbActi.Limpiar()
      cmbOrden.SelectedIndex = 0
      chkDatos.Checked = False
   End Sub

#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lintFechaDesde As Integer, lintFechaHasta As Integer

            Dim lstrRptName As String
            If cmbOrden.Valor = "A" Then
                lstrRptName = "SaldosPendientes"
            ElseIf cmbOrden.Valor = "C" Then
                lstrRptName = "SaldosPendientes_Clientes"
            Else
                lstrRptName = "SaldosPendientes_Fecha"
            End If
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtFechaDesdeFil.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaHastaFil.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If

            lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&acti_id=" + IIf(cmbActi.Valor.ToString = "", "0", cmbActi.Valor.ToString)
            lstrRpt += "&orden=" + cmbOrden.Valor
            lstrRpt += "&datos=" + IIf(chkDatos.Checked, "S", "N")

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
