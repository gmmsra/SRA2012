Namespace SRA

Partial Class MailingAlumnos
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cmbPromTipo As NixorControls.ComboBox
    Protected WithEvents lblInstituciones As System.Web.UI.WebControls.Label
    Protected WithEvents lstInstituciones As NixorControls.CheckListBox
    Protected WithEvents lblCargos As System.Web.UI.WebControls.Label

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrParaPageSize As Integer
    Private mintInse As Integer
    Private mintActi As Integer
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
            hdnInse.Text = mintInse

            Select Case mintInse
                Case 1
                    mintActi = 6
                Case 2
                    mintActi = 10
                Case 3
                    mintActi = 13
                Case Else
                    mintActi = 0
            End Select

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                btnEnvi.Attributes.Add("onclick", "return(mValidarEnvio());")
                clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar @acti_id=" + mintActi.ToString(), cmbMail, "id", "descrip", "S")
                mCargarCombos()
                mSetearEventos()
            Else
                mRecuperarParametros()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mRecuperarParametros()

        Dim lstrFiltros As String = ""
        Dim lstrCicl As String = hdnSeleCicl.Text
        Dim lstrMate As String = hdnSeleMate.Text
        Dim lstrPerio As String = hdnSelePerio.Text
        Dim lstrComi As String = hdnSeleComi.Text
        Dim lstrDivi As String = hdnSeleDivi.Text

        'ciclos
        lstrFiltros = mintInse & ","
        If txtAnioFil.Text = "" Then
            lstrFiltros = lstrFiltros & "-1"
        Else
            lstrFiltros = lstrFiltros & txtAnioFil.Text
        End If
        clsWeb.gCargarCombo(mstrConn, "ciclos_cargar " & lstrFiltros, cmbCicloFil, "id", "descrip", "T")

        'comisiones, divisiones y materias
        lstrFiltros = "@mate_inse_id=" & mintInse
        If lstrCicl <> "" Then
           lstrFiltros = lstrFiltros + ",@cicl_id=" & lstrCicl
           clsWeb.gCargarCombo(mstrConn, "materiasXciclo_cargar " & lstrFiltros, cmbMateFil, "id", "descrip", "T")
           clsWeb.gCargarCombo(mstrConn, "comisiones_cargar @cicl_id=" & lstrCicl, cmbComision, "id", "descrip", "T")
           clsWeb.gCargarCombo(mstrConn, "divisiones_cargar @divi_nombre='S',@divi_cicl_id=" & lstrCicl & ",@cicl_inse_id=" & mintInse, cmbDivision, "id", "descrip", "T")
           If lstrMate = "" Then
              lstrMate = "0"
           End If
           clsWeb.gCargarCombo(mstrConn, "periodos_cargar @cicl_id=" & lstrCicl & ",@mate_id=" & lstrMate, cmbPerioFil, "id", "descrip", "T")
        End If

        cmbCicloFil.Valor = lstrCicl
        cmbMateFil.Valor = lstrMate
        cmbPerioFil.Valor = lstrPerio
        cmbComision.Valor = lstrComi
        cmbDivision.Valor = lstrDivi

    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "estudios", lstEstudios, "")
        clsWeb.gCargarRefeCmb(mstrConn, "funciones", lstCargos, "")
        clsWeb.gCargarRefeCmb(mstrConn, "ocupaciones", lstOcupaciones, "")
        clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMate, "T")
        'clsWeb.gCargarRefeCmb(mstrConn, "materiasXciclo", cmbMateFil, "S", "-1")
        'clsWeb.gCargarRefeCmb(mstrConn, "divisiones", cmbDivision, "S", "-1")
        'clsWeb.gCargarRefeCmb(mstrConn, "comisiones", cmbComision, "S", "-1")
        clsWeb.gCargarRefeCmb(mstrConn, "becas", cmbBeca, "N", "@beca_inse_id=" + mintInse.ToString)
    End Sub

    Private Sub mLimpiarFiltros()
        chkRepoMail.Checked = False
        chkRepoTele.Checked = False
        chkRepoIngreFecha.Checked = False
        chkAnte.Checked = False
        cmbRepo.Valor = "R"
        'btnList.Visible = True
        btnEnvi.Visible = False
        cmbRepo.Enabled = True
        lstCargos.Limpiar()
        lstEstudios.Limpiar()
        lstOcupaciones.Limpiar()
        cmbBeca.Limpiar()
        cmbCicloFil.Limpiar()
        cmbComision.Limpiar()
        cmbDestinatarios.Limpiar()
        cmbDivision.Limpiar()
        cmbMateFil.Limpiar()
        cmbPerioFil.Limpiar()
        txtAnioFil.Text = ""
        txtApelDesde.Text = ""
        txtApelHasta.Text = ""
        txtLegajoDesde.Text = ""
        txtLegajoHasta.Text = ""
        grdCons.Visible = False
        cmbCicloFil.Items.Clear()
        cmbComision.Items.Clear()
        cmbDivision.Items.Clear()
        cmbMateFil.Items.Clear()
        cmbPerioFil.Items.Clear()
        hdnSeleCicl.Text = ""
        hdnSeleMate.Text = ""
        hdnSelePerio.Text = ""
        hdnSeleComi.Text = ""
        hdnSeleDivi.Text = ""
        'ldivSoliNume.Style.Add("display", IIf(ltxtSociNume.Valor Is DBNull.Value, "none", "inline"))
    End Sub

    Private Sub mSetearEventos()
        btnPais.Attributes.Add("onclick", "mAgregar('P');return false;")
        btnProv.Attributes.Add("onclick", "mAgregar('R');return false;")
        btnLoca.Attributes.Add("onclick", "mAgregar('L');return false;")
        txtAnioFil.Attributes.Add("onchange", "txtAnioFil_Change(this,'" & mintInse & "');")
        cmbCicloFil.Attributes.Add("onchange", "cmbCiclFil_Change(this,'" & mintInse & "');")
        cmbMateFil.Attributes.Add("onchange", "cmbMateFil_Change(this,'" & mintInse & "');")
        cmbPerioFil.Attributes.Add("onchange", "cmbPerioFil_Change(this,'" & mintInse & "');")
        cmbDivision.Attributes.Add("onchange", "cmbDivision_Change(this,'" & mintInse & "');")
        cmbComision.Attributes.Add("onchange", "cmbComision_Change(this,'" & mintInse & "');")
    End Sub
#End Region

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub mListar()
        Try
            Dim lstrRptName As String
            Dim lstrFiltro As String

            If cmbRepo.Valor.ToString = "E" Then
                lstrRptName = "EtiquetasAlum"
            Else
                lstrRptName = "MailingAlumnos"
            End If

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&apel_desde=" & txtApelDesde.Text
            lstrRpt += "&apel_hasta=" & txtApelHasta.Text
            lstrRpt += "&estu_ids=" & lstEstudios.ObtenerIDs
            lstrRpt += "&carg_ids=" & lstCargos.ObtenerIDs
            lstrRpt += "&ocup_ids=" & lstOcupaciones.ObtenerIDs
            lstrRpt += "&pais_ids=" & hdnSelePais.Text
            lstrRpt += "&prov_ids=" & hdnSeleProv.Text
            lstrRpt += "&loca_ids=" & hdnSeleLoca.Text
            lstrRpt += "&beca_id=" & clsSQLServer.gFormatArg(cmbBeca.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&legajo_desde=" & clsSQLServer.gFormatArg(txtLegajoDesde.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&legajo_hasta=" & clsSQLServer.gFormatArg(txtLegajoHasta.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&anio=" & clsSQLServer.gFormatArg(txtAnioFil.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&cicl_id=" & clsSQLServer.gFormatArg(cmbCicloFil.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&mate_id=" & clsSQLServer.gFormatArg(cmbMateFil.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&perio=" & clsSQLServer.gFormatArg(cmbPerioFil.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&divi_id=" & clsSQLServer.gFormatArg(cmbDivision.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&comi_id=" & clsSQLServer.gFormatArg(cmbComision.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&dest=" & clsSQLServer.gFormatArg(cmbDestinatarios.Valor.ToString, SqlDbType.Int)
            lstrRpt += "&inse_id=" & Request.QueryString("fkvalor")

            If cmbRepo.Valor.ToString = "E" Then
                lstrRpt += "&conDire=" & IIf(RadTodos.Checked, "True", IIf(radConDire.Checked, "True", "False"))
            Else
                lstrRpt += "&conTele=" & chkRepoTele.Checked
                lstrRpt += "&ingre_fecha_mostrar=" & chkRepoIngreFecha.Checked
                lstrRpt += "&conMail=" & chkRepoMail.Checked
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mObtenerMails(ByVal ds As DataSet) As String
        Dim lstrMail As String
        Try
            'For Each odrClag As DataRow In ds.Tables(0).Select("cate_id=" & pstrCate)
            For Each odrClag As DataRow In ds.Tables(0).Select()
                If lstrMail <> "" Then
                    lstrMail = lstrMail & ";"
                End If
                lstrMail = lstrMail & odrClag.Item("Mails")
            Next
            ds.Dispose()
            Return (lstrMail)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

    Private Sub mEnviarMails()
        Try
            Dim ds As New DataSet
            Dim lstrMail As String
            ' Dim lstrInst As String
            ' Dim lstrCate As String
            Dim lstrAsun As String
            Dim lstrMens As String
            Dim lstrCmd As String
            Dim lstrMailMode As String = System.Configuration.ConfigurationSettings.AppSettings("conMailMode").ToString()

            lstrMailMode = "..\" & lstrMailMode

            ' For Each odrDeta As DataRow In Session("dsMails").Tables(0).Select
            'lstrCate = odrDeta.Item("cate_id")
            ' If odrDeta.IsNull("inst_id") Then
            ' lstrInst = ""
            ' Else
            '     lstrInst = odrDeta.Item("inst_id")
            ' End If
            lstrAsun = clsSQLServer.gCampoValorConsul(mstrConn, "mails_modelos_consul @mamo_id=" & cmbMail.Valor.ToString, "mamo_asun")
            lstrCmd = "mails_mode_catego_consul @mmca_mamo_id=" & cmbMail.Valor.ToString
            'If lstrInst <> "" Then
            ' lstrCmd = lstrCmd & ",@mmca_inst_id=" & lstrInst
            ' End If
            lstrMens = clsSQLServer.gCampoValorConsul(mstrConn, lstrCmd, "mmca_mens")
            lstrMail = mObtenerMails(Session("dsMails"))
            'lstrMens = "sdada"
            If lstrMail <> "" And lstrMens <> "" Then
                clsMail.gEnviarMail(lstrMail, lstrAsun, lstrMens, lstrMailMode)
            End If
            ' Next
            ds.Dispose()

            btnEnvi.Visible = False
            'btnList.Visible = True
            cmbRepo.Enabled = True

            mLimpiarFiltros()

            Throw New AccesoBD.clsErrNeg("Se han enviado los mails a las personas seleccionadas.")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        If cmbRepo.Valor.ToString = "M" Then
            mConsultarMails(grdCons, False)
        Else
            mListar()
        End If
    End Sub

    Private Function mObtenerSQL() As String
        Dim lstrCmd As New StringBuilder

        lstrCmd.Append(" @apel_desde=" & clsSQLServer.gFormatArg(txtApelDesde.Text, SqlDbType.VarChar))
        lstrCmd.Append(",@apel_hasta=" & clsSQLServer.gFormatArg(txtApelHasta.Text, SqlDbType.VarChar))
        lstrCmd.Append(",@beca_id=" & clsSQLServer.gFormatArg(cmbBeca.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@legajo_desde=" & clsSQLServer.gFormatArg(txtLegajoDesde.Valor.ToString, SqlDbType.VarChar))
        lstrCmd.Append(",@legajo_hasta=" & clsSQLServer.gFormatArg(txtLegajoHasta.Valor.ToString, SqlDbType.VarChar))
        lstrCmd.Append(",@anio=" & clsSQLServer.gFormatArg(txtAnioFil.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@cicl_id=" & clsSQLServer.gFormatArg(cmbCicloFil.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@mate_id=" & clsSQLServer.gFormatArg(cmbMateFil.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@perio=" & clsSQLServer.gFormatArg(cmbPerioFil.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@divi_id=" & clsSQLServer.gFormatArg(cmbDivision.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@comi_id=" & clsSQLServer.gFormatArg(cmbComision.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@estu_ids=" & clsSQLServer.gFormatArg(lstEstudios.ObtenerIDs, SqlDbType.VarChar))
        lstrCmd.Append(",@carg_ids=" & clsSQLServer.gFormatArg(lstCargos.ObtenerIDs, SqlDbType.VarChar))
        lstrCmd.Append(",@ocup_ids=" & clsSQLServer.gFormatArg(lstOcupaciones.ObtenerIDs, SqlDbType.VarChar))
        lstrCmd.Append(",@pais_ids=" & clsSQLServer.gFormatArg(hdnSelePais.Text, SqlDbType.VarChar))
        lstrCmd.Append(",@prov_ids=" & clsSQLServer.gFormatArg(hdnSeleProv.Text, SqlDbType.VarChar))
        lstrCmd.Append(",@loca_ids=" & clsSQLServer.gFormatArg(hdnSeleLoca.Text, SqlDbType.VarChar))
        lstrCmd.Append(",@dest=" & clsSQLServer.gFormatArg(cmbDestinatarios.Valor.ToString, SqlDbType.Int))
        lstrCmd.Append(",@inse_id=" & Request.QueryString("fkvalor"))

        Return lstrCmd.ToString()
    End Function

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdCons.EditItemIndex = -1
            If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
                grdCons.CurrentPageIndex = 0
            Else
                grdCons.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarMails(grdCons, True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mConsultarMails(ByVal pobjGrid As System.Web.UI.WebControls.DataGrid, ByVal pboolPagi As Boolean)
        Try
            Dim lstrFiltros As String = ""
            If chkAnte.Checked Then
                lstrFiltros = lstrFiltros + " @anio_desde=" & clsSQLServer.gFormatArg(txtAnioDesde.Text, SqlDbType.Int)
                lstrFiltros = lstrFiltros + ",@anio_hasta=" & clsSQLServer.gFormatArg(txtAnioHasta.Text, SqlDbType.Int)
                lstrFiltros = lstrFiltros + ",@mate_id=" & clsSQLServer.gFormatArg(cmbMate.Valor.ToString(), SqlDbType.Int)
                lstrFiltros = lstrFiltros + ",@dest=" & clsSQLServer.gFormatArg("A", SqlDbType.Int)
                lstrFiltros = lstrFiltros + ",@inse_id=" & Request.QueryString("fkvalor")
            Else
                lstrFiltros = mObtenerSQL()
            End If
            '    hdnRptId.Text = usrSocFil.ObtenerIdFiltro.ToString()

            Dim lstrCmd As String = "exec alumnos_mails_consul "
            lstrCmd += lstrFiltros

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

            For Each dc As DataColumn In ds.Tables(0).Columns
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                dgCol.DataField = dc.ColumnName
                dgCol.HeaderText = dc.ColumnName
                If dc.Ordinal = 0 Or dc.ColumnName.IndexOf("_id") <> -1 Then
                    dgCol.Visible = False
                End If
                pobjGrid.Columns.Add(dgCol)
            Next
            If Not pboolPagi Then
                pobjGrid.CurrentPageIndex = 0
            End If
            pobjGrid.DataSource = ds
            pobjGrid.DataBind()
            Session("dsMails") = DirectCast(pobjGrid.DataSource, DataSet)
            pobjGrid.Visible = True
            btnEnvi.Enabled = (pobjGrid.Items.Count > 0)
            btnEnvi.Visible = True
            'btnList.Visible = False
            cmbRepo.Enabled = False
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvi.Click
        mEnviarMails()
    End Sub

End Class

End Namespace
