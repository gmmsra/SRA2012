Namespace SRA

Partial Class FiltroCuotasDeve
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub
#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()

        clsWeb.gCargarRefeCmb(mstrConn, "cuotas_deve_cabe", cmbDeve, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "cuotas_estru_perio", cmbDeve, "S")
   End Sub
   Private Sub mLimpiarFiltros()

      cmbDeve.Limpiar()
     
   End Sub
#End Region

#Region "Detalle"

   Private Sub mValidar()

      If (cmbDeve.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe indicar seleccionar el periodo de devengamiento.")
      End If


   End Sub

   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try

         mValidar()

         Dim lstrRptName As String

         If cmbTipo.Valor.ToString = "R" Then
            lstrRptName = "CuotasDeve"
         Else
            lstrRptName = "CuotasDeveDeta"
         End If
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&id=" + cmbDeve.Valor
            If cmbTipo.Valor.ToString = "R" Then
                lstrRpt += "&ult_id=0"
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
