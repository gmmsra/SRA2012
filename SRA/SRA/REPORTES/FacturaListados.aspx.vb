Namespace SRA

Partial Class FacturaListados
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlPago As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTipoPago As System.Web.UI.WebControls.Label
    Protected WithEvents cmbTipoPago As NixorControls.ComboBox
    Protected WithEvents pnlConceptosAranc As System.Web.UI.WebControls.Panel
    Protected WithEvents txtArancelDesde As NixorControls.NumberBox
    Protected WithEvents lblArancelDesde As System.Web.UI.WebControls.Label
    Protected WithEvents lblArancelHasta As System.Web.UI.WebControls.Label
    Protected WithEvents txtArancelHasta As NixorControls.NumberBox
    Protected WithEvents lblConceptoDesde As System.Web.UI.WebControls.Label
    Protected WithEvents lblConceptoHasta As System.Web.UI.WebControls.Label
    Protected WithEvents txtConceptoHasta As NixorControls.NumberBox
    Protected WithEvents txtConceptoDesde As NixorControls.NumberBox
    Protected WithEvents pnlAranceles As System.Web.UI.WebControls.Panel

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrTitu As String
    Private mstrRepo As String
    Private mstrTipo As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mInicializar()
        mstrTipo = Request.Item("tipo").ToString()
        Select Case mstrTipo
            Case "FPC"
                pnlCostos.Visible = False
                pnlEstados.Visible = False
                lblTitu.Text = "Facturación por Clientes"
                mstrRepo = "FacturaClientes"
            Case "FCC"
                pnlCostos.Visible = True
                pnlEstados.Visible = True
                lblTitu.Text = "Facturación por Centros de Costo"
                mstrRepo = "FacturaCCosto"
            Case "FCA"
                pnlCostos.Visible = False
                pnlEstados.Visible = True
                lblTitu.Text = "Facturación por Concepto/Arancel"
                mstrRepo = "FacturacionConcep"
        End Select
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "aranceles", cmbArancel, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConcepto, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEdesde, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCEhasta, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividad, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCentroCosto, "T")
    End Sub
#End Region

#Region "Detalle"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer
            If cmbDetalle.Valor.ToString = "R" Then
                lstrRptName = mstrRepo + "Resu"
            Else
                lstrRptName = mstrRepo
            End If

            If cmbListar.Valor.ToString = "S" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un listado.")
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtFechaDesde.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
            End If

            If txtFechaHasta.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
            End If

            lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&CEmDesde=" + IIf(cmbCEdesde.Valor.ToString = "", "0", cmbCEdesde.Valor.ToString)
            lstrRpt += "&CEmHasta=" + IIf(cmbCEhasta.Valor.ToString = "", "0", cmbCEhasta.Valor.ToString)
            lstrRpt += "&acti_id=" + IIf(cmbActividad.Valor.ToString = "", "0", cmbActividad.Valor.ToString)
            lstrRpt += "&aran_id=" + IIf(cmbArancel.Valor.ToString = "", "0", cmbArancel.Valor.ToString)
            lstrRpt += "&conc_id=" + IIf(cmbConcepto.Valor.ToString = "", "0", cmbConcepto.Valor.ToString)
            lstrRpt += "&acti_desc=" + cmbActividad.SelectedItem.Text

            If mstrTipo = "FCC" Then
                lstrRpt += "&ccos_id=" + IIf(cmbCentroCosto.Valor.ToString = "", "0", cmbCentroCosto.Valor.ToString)
            End If

            lstrRpt += "&Ejecuta=" + cmbListar.Valor.ToString

            If mstrTipo <> "FPC" Then
                ' Dario 2013-04-19 Cambio se agrega estado pago impago o parcial, solo para los reportes
                ' que no son "Facturación por Clientes"
                lstrRpt += "&Estado=" + IIf(cmbEstado.Valor.ToString = "-1", "N", cmbEstado.Valor.ToString)
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class

End Namespace
