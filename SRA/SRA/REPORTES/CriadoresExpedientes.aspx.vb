Namespace SRA

Partial Class CriadoresExpedientes
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents lblTarjeta As System.Web.UI.WebControls.Label
	Protected WithEvents cmbTarjeta As NixorControls.ComboBox
	Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
	Protected WithEvents cmbEstado As NixorControls.ComboBox
	Protected WithEvents lblDeca As System.Web.UI.WebControls.Label
	Protected WithEvents cmbDeca As NixorControls.ComboBox
	Protected WithEvents lblNume As System.Web.UI.WebControls.Label
	Protected WithEvents Label1 As System.Web.UI.WebControls.Label
	Protected WithEvents lblFechaHasta As System.Web.UI.WebControls.Label
	Protected WithEvents lblFechaInscHasta As System.Web.UI.WebControls.Label


	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

#Region "Definición de Variables"
	Private mstrConn As String
	Private mstrActiId As String
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()

			If (Not Page.IsPostBack) Then
				mCargarCombos()
				If (mstrActiId <> 2) Then
					cmbTipoListado.Items.Clear()
					cmbTipoListado.Items.Add("(Seleccione)")
					cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = ""
					cmbTipoListado.Items.Add("Basico")
					cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "B"
					cmbTipoListado.Items.Add("Completo")
					cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "C"
					cmbTipoListado.Items.Add("Inspección")
					cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "I"
					cmbTipoListado.Items.Add("Etiquetas")
					cmbTipoListado.Items(cmbTipoListado.Items.Count - 1).Value = "E"
				End If
			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mInicializar()
		If Not Request.QueryString("act") Is Nothing Then
			mstrActiId = Request.QueryString("act")
		Else
			mstrActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", Request.QueryString("fkvalor")).Tables(0).Rows(0).Item("inse_acti_id")
		End If
	End Sub

	Private Sub mCargarCombos()
		clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbProvDesde, "S")
		clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbProvHasta, "S")
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaDesde, "id", "descrip_codi", "S")
		SRA_Neg.Utiles.gSetearRaza(cmbRazaDesde)
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaHasta, "id", "descrip_codi", "S")
		SRA_Neg.Utiles.gSetearRaza(cmbRazaHasta)
	End Sub

	Private Sub mLimpiarFiltros()
		cmbRazaDesde.Limpiar()
		cmbRazaHasta.Limpiar()
		txtCrianumeDesde.Text = ""
		txtCriaNumeHasta.Text = ""
		txtApel.Text = ""
		txtNomb.Text = ""
		chkBuscApel.Checked = False
		chkBuscNomb.Checked = False
		chkBaja.Checked = True
		cmbProvDesde.Limpiar()
		cmbProvHasta.Limpiar()
		cmbLocaDesde.Limpiar()
		cmbLocaHasta.Limpiar()
		cmbTipoListado.SelectedIndex = 0
		txtAltaFechaDesde.Text = ""
		txtAltaFechaHasta.Text = ""
		txtBajaFechaDesde.Text = ""
		txtBajaFechaHasta.Text = ""
		txtInscFechaDesde.Text = ""
		txtInscFechaHasta.Text = ""
		txtNaciFechaDesde.Text = ""
		txtNaciFechaHasta.Text = ""

	End Sub
#End Region

	Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		Try
			Dim lstrRptName As String
			Dim lintFecha As Integer
			Dim lstrAnio As String
			Dim lstrPerio As String
			Dim lintFACD, lintFACH, lintFBCD, lintFBCH As Integer
			Dim lintFIPD, lintFIPH, lintFNPD, lintFNPH As Integer


			If cmbTipoListado.Valor.ToString = "" Then
				Throw New AccesoBD.clsErrNeg("Debe seleccionar el Tipo de Listado.")
			End If

			'If cmbTarjeta.Items.Count > 1 And cmbTarjeta.Valor = 0 Then
			'   Throw New AccesoBD.clsErrNeg("Debe seleccionar la Tarjeta.")
			'End If

			Select Case cmbTipoListado.Valor.ToString
				Case "B"
					lstrRptName = "ExpedientesBasico"
				Case "C"
					lstrRptName = "ExpedientesCompleto"
				Case "I"
					lstrRptName = "ExpedientesInspeccion"
				Case "E"
					lstrRptName = "ExpedientesEtiquetas"
			End Select


			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			
			lstrRpt += "&cria_raza_id_desde=" & IIf(cmbRazaDesde.Valor.ToString = "", "0", cmbRazaDesde.Valor)
			lstrRpt += "&cria_raza_id_hasta=" & IIf(cmbRazaHasta.Valor.ToString = "", "0", cmbRazaHasta.Valor)
			lstrRpt += "&cria_nume_desde=" & IIf(txtCrianumeDesde.Text = "", 0, txtCrianumeDesde.Text)
			lstrRpt += "&cria_nume_hasta=" & IIf(txtCriaNumeHasta.Text = "", 0, txtCriaNumeHasta.Text)

			If txtApel.Text <> "" Then
				lstrRpt += "&clie_apel=" & txtApel.Text
			
			End If

			If txtNomb.Text <> "" Then
				lstrRpt += "&clie_nomb=" & txtNomb.Text
			
			End If

			lstrRpt += "&prov_id=" & IIf(cmbProvDesde.Valor.ToString = "", "0", cmbProvDesde.Valor)

			If cmbProvDesde.SelectedItem.Text <> "(Seleccione)" Then
				lstrRpt += "&prov_desc_desde=" & Trim(cmbProvDesde.SelectedItem.Text)
			
			End If

			If cmbProvHasta.SelectedItem.Text <> "(Seleccione)" Then
				lstrRpt += "&prov_desc_hasta=" & Trim(cmbProvHasta.SelectedItem.Text)
			
			End If

			If cmbLocaDesde.Items.Count > 0 Then
				lstrRpt += "&loca_desc_desde=" & Trim(cmbLocaDesde.SelectedItem.Text)
			
			End If

			If cmbLocaHasta.Items.Count > 0 Then
				lstrRpt += "&loca_desc_hasta=" & Trim(cmbLocaHasta.SelectedItem.Text)
			
			End If

			lstrRpt += "&buscar_en_apel=" & IIf(chkBuscApel.Checked, True, False)
			lstrRpt += "&buscar_en_nomb=" & IIf(chkBuscNomb.Checked, True, False)
			lstrRpt += "&incluir_bajas=" & IIf(chkBaja.Checked, True, False)


			If txtAltaFechaDesde.Text = "" Then
				lintFACD = 0
			Else
				lintFACD = CType(clsFormatear.gFormatFechaString(txtAltaFechaDesde.Text, "Int32"), Integer)
			End If
			If txtAltaFechaHasta.Text = "" Then
				lintFACH = 0
			Else
				lintFACH = CType(clsFormatear.gFormatFechaString(txtAltaFechaHasta.Text, "Int32"), Integer)
			End If

			If txtBajaFechaDesde.Text = "" Then
				lintFBCD = 0
			Else
				lintFBCD = CType(clsFormatear.gFormatFechaString(txtBajaFechaDesde.Text, "Int32"), Integer)
			End If
			If txtBajaFechaHasta.Text = "" Then
				lintFBCH = 0
			Else
				lintFBCH = CType(clsFormatear.gFormatFechaString(txtBajaFechaHasta.Text, "Int32"), Integer)
			End If


			If txtInscFechaDesde.Text = "" Then
				lintFIPD = 0
			Else
				lintFIPD = CType(clsFormatear.gFormatFechaString(txtInscFechaDesde.Text, "Int32"), Integer)
			End If
			If txtInscFechaHasta.Text = "" Then
				lintFIPH = 0
			Else
				lintFIPH = CType(clsFormatear.gFormatFechaString(txtInscFechaHasta.Text, "Int32"), Integer)
			End If

			If txtNaciFechaDesde.Text = "" Then
				lintFNPD = 0
			Else
				lintFNPD = CType(clsFormatear.gFormatFechaString(txtNaciFechaDesde.Text, "Int32"), Integer)
			End If
			If txtNaciFechaHasta.Text = "" Then
				lintFNPH = 0
			Else
				lintFNPH = CType(clsFormatear.gFormatFechaString(txtNaciFechaHasta.Text, "Int32"), Integer)
			End If


			lstrRpt += "&fecha_alta_cria_desde=" & lintFACD
			lstrRpt += "&fecha_alta_cria_hasta=" & lintFACH
			lstrRpt += "&fecha_baja_cria_desde=" & lintFBCD
			lstrRpt += "&fecha_baja_cria_hasta=" & lintFBCH
			lstrRpt += "&fecha_insc_prdt_desde=" & lintFIPD
			lstrRpt += "&fecha_insc_prdt_hasta=" & lintFIPH
			lstrRpt += "&fecha_naci_prdt_desde=" & lintFNPD
			lstrRpt += "&fecha_naci_prdt_hasta=" & lintFNPH

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltros()
	End Sub
End Class
End Namespace
