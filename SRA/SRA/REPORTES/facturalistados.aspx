<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.FacturaListados" CodeFile="FacturaListados.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="javascript">
			function HabilitarCombos()
			{
					document.all("cmbArancel").value="";
					document.all("txtcmbArancel").value="";
					document.all("cmbArancel").disabled=false;
					document.all("txtcmbArancel").disabled=false;
					
					document.all("cmbConcepto").value="";
					document.all("cmbConcepto").disabled=false;
					document.all("txtcmbConcepto").value="";
					document.all("txtcmbConcepto").disabled=false;
				
				switch (document.all("cmbListar").value)
					{
					case "C":
							document.all("cmbArancel").value="";
							document.all("cmbArancel").disabled=true;
							document.all("txtcmbArancel").value="";
							document.all("txtcmbArancel").disabled=true;
						break;
					case "A":
							document.all("cmbConcepto").value="";
							document.all("cmbConcepto").disabled=true;
							document.all("txtcmbConcepto").value="";
							document.all("txtcmbConcepto").disabled=true;
						break;
				}
			}
			
			function CargarCombos(pActividad)
			{
				var sFiltro = pActividad.value;
				LoadComboXML("aranceles_cargar", sFiltro, "cmbArancel", "T");
				LoadComboXML("conceptos_cargar", sFiltro, "cmbConcepto", "T");
			}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');HabilitarCombos();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="391px"></asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" id="ColumnaPrueba" background="../imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 29.86%; HEIGHT: 7px" background="../imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 7px" background="../imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo" Width="87px">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblCEdesde" runat="server" cssclass="titulo" Width="133px">Ctro.Emisor Desde:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbCEdesde" class="combo" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblCEhasta" runat="server" cssclass="titulo">Ctro.Emisor Hasta:</asp:Label>&nbsp;</TD>
																		<TD background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbCEhasta" class="combo" runat="server" Width="280px" nomoper="emisores_ctros_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10.46%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblListar" runat="server" cssclass="titulo">Listar:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 100%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbListar" class="combo" runat="server" Width="100px" onchange="HabilitarCombos()">
																				<asp:listitem value="S" selected="true">(Seleccione)</asp:listitem>
																				<asp:listitem value="A">Solo Aranceles</asp:listitem>
																				<asp:listitem value="C">Solo Conceptos</asp:listitem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%; HEIGHT: 2px" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%; HEIGHT: 2px" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 29.86%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblActividad" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbActividad" class="combo" runat="server" Width="280px" nomoper="actividades_cargar"
																				onchange="CargarCombos(this)"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 29.86%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblArancel" runat="server" cssclass="titulo">Arancel:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																			<TABLE border="0" cellSpacing="0" cellPadding="0">
																				<TR>
																					<TD>
																						<cc1:combobox id="cmbArancel" class="combo" runat="server" cssclass="cuadrotexto" Width="350px"
																							nomoper="aranceles_cargar" MostrarBotones="False" filtra="true" TextMaxLength="5"></cc1:combobox></TD>
																					<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																							id="btnAvanBusq" onclick="var lstrFil = 'null'; if (document.all('cmbActividad').value != '') lstrFil = document.all('cmbActividad').value; mBotonBusquedaAvanzadaRepo('aranceles','aran_desc','cmbArancel','Aranceles','@aran_acti_id='+lstrFil);"
																							border="0" alt="Busqueda avanzada" src="../imagenes/Buscar16.gif">
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 29.86%" vAlign="top" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblConcepto" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																			<TABLE border="0" cellSpacing="0" cellPadding="0">
																				<TR>
																					<TD>
																						<cc1:combobox id="cmbConcepto" class="combo" runat="server" cssclass="cuadrotexto" Width="350px"
																							nomoper="conceptos_cargar" MostrarBotones="False" filtra="true" TextMaxLength="5"></cc1:combobox></TD>
																					<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																							id="btnAvanBusq2" onclick="var lstrFil = 'null'; if (document.all('cmbActividad').value != '') lstrFil = document.all('cmbActividad').value; mBotonBusquedaAvanzadaRepo('conceptos','conc_desc','cmbConcepto','Conceptos','@conc_acti_id='+lstrFil);"
																							border="0" alt="Busqueda avanzada" src="../imagenes/Buscar16.gif">
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<asp:Panel id="pnlCostos" cssclass="titulo" BorderStyle="Solid" Width="100%" BorderWidth="0"
																		Visible="False" Runat="server">
																		<TR>
																			<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 29.86%" vAlign="top" background="../imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblCentroCosto" runat="server" cssclass="titulo">Centro de Costo:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<TABLE border="0" cellSpacing="0" cellPadding="0">
																					<TR>
																						<TD>
																							<cc1:combobox id="cmbCentroCosto" class="combo" runat="server" cssclass="cuadrotexto" Width="350px"
																								nomoper="centrosc_cargar" MostrarBotones="False" filtra="true" TextMaxLength="5"></cc1:combobox></TD>
																						<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																								id="btnAvanBusq3" onclick="var lstrFil = 'null'; if (document.all('cmbActividad').value != '') lstrFil = document.all('cmbActividad').value; mBotonBusquedaAvanzadaRepo('centrosc','ccos_desc','cmbCentroCosto','Centros de Costo','');"
																								border="0" alt="Busqueda avanzada" src="../imagenes/Buscar16.gif">
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivmed.jpg"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 29.86%" vAlign="top" background="../imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblDetallado" runat="server" cssclass="titulo">Detallado:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 72%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbDetalle" class="combo" runat="server" Width="100px">
																					<asp:listitem value="D" selected="true">SI</asp:listitem>
																					<asp:listitem value="R">NO</asp:listitem>
																				</cc1:combobox></TD>
																		</TR>
																	</asp:Panel>
																	<asp:Panel id="pnlEstados" cssclass="titulo" BorderStyle="Solid" Width="100%" BorderWidth="0"
																		Runat="server">
																		<TR>
																			<TD style="WIDTH: 10.46%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblEstado" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 100%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbEstado" class="combo" runat="server" Width="100px" onchange="HabilitarCombos()">
																					<asp:listitem value="-1" selected="true">(Todos)</asp:listitem>
																					<asp:listitem value="0">Impagos</asp:listitem>
																					<asp:listitem value="1">Pagos</asp:listitem>
																					<asp:listitem value="2">Parcial</asp:listitem>
																				</cc1:combobox></TD>
																		</TR>
																	</asp:Panel>
																	<TR>
																		<TD style="WIDTH: 15.75%" height="2" background="../imagenes/formdivfin.jpg" align="right"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivfin.jpg" width="8"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD> <!-- FIN FOMULARIO -->
															<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
														<TR>
															<TD height="10" width="3"></TD>
															<TD height="10"></TD>
															<TD height="10" width="2"></TD>
														</TR>
														<TR>
															<TD width="3"></TD>
															<TD align="right">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
																	ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" BackColor="Transparent"
																	ImagesUrl="../imagenes/" IncludesUrl="../includes/" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
					</td> <!--- FIN CONTENIDO --->
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
					</td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
