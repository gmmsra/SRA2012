<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="../controles/usrSociosFiltro.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PagosAcreditaciones" CodeFile="PagosAcreditaciones.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Pagos con Acreditaciones</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
		<SCRIPT language="javascript">
		function CargaCuentas(ComboOrigen,ComboDestino,Opc)
		{
			var sFiltro = document.all(ComboOrigen).value;
			if (sFiltro != '')
			{
				LoadComboXML("cuentas_bancos_cargar", sFiltro, ComboDestino, Opc);
			}
			else
			{
				LoadComboXML("cuentas_bancos_cargar", "@banc_id=-1", ComboDestino, Opc);
			}
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 8px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Pagos con Acreditaciones</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 8px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="97%" Visible="True">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 15px" height="21" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 15%; HEIGHT: 15px" height="21" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					align="right"></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 15px" height="21" vAlign="middle" background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 21px" height="21" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 15%; HEIGHT: 21px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo" Width="87px">Fecha Desde:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 100%; HEIGHT: 30px" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 21px" height="21" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 15%; HEIGHT: 21px" height="21" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					noWrap align="right">
																					<asp:Label id="lblCentroEmisor" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 21px" height="21" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbCentroEmisor" class="combo" runat="server" Width="240px" AceptaNull="false"
																						ImagesUrl="../Images/" NomOper="emisores_ctros_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 17px" height="17" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="HEIGHT: 17px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					align="right">
																					<asp:Label id="lblBanc" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 17px" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<TABLE border="0" cellSpacing="0" cellPadding="0">
																						<TR>
																							<TD>
																								<cc1:combobox id="cmbBanc" class="combo" runat="server" cssclass="cuadrotexto" Width="240px" AceptaNull="false"
																									NomOper="bancos_cargar" MostrarBotones="False" filtra="true" onchange="CargaCuentas('cmbBanc', 'cmbCuba','T');"></cc1:combobox></TD>
																							<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																									id="btnAvanBusq2" onclick="mBotonBusquedaAvanzadaRepo('bancos','banc_desc','cmbBanc','Bancos','@con_cuentas=1');"
																									border="0" alt="Busqueda avanzada" src="../imagenes/Buscar16.gif">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 5px" height="5" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="HEIGHT: 5px" vAlign="middle" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblCuen" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 5px" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbCuba" class="combo" runat="server" Width="291px" AceptaNull="False" nomoper="cuentas_bancos_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 26px" height="26" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 15%; HEIGHT: 26px" height="26" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					noWrap align="right">
																					<asp:Label id="lblMonedaFil" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 26px" height="26" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbMonedaFil" class="combo" runat="server" Width="104px" AceptaNull="false"
																						ImagesUrl="../Images/" NomOper="monedas_sinDefa_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 26px" height="26" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblEmpCobElectFil" runat="server" cssclass="titulo">Empresa Cobro Electrónica:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbEmpCobElecFil" class="combo" runat="server" Width="260px" obligatorio="True"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 26px" height="26" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblNroCompCobElectFil" runat="server" cssclass="titulo">Nro. Comprobante Cobro Elect.:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%" background="../imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtNroCompCobElectFil" runat="server" cssclass="cuadrotexto" Width="270"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 15px" height="21" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 15%; HEIGHT: 15px" height="21" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					align="right"></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 15px" height="21" vAlign="middle" background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" height="2" background="../imagenes/formdivfin.jpg" align="right"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																				<TD style="WIDTH: 15%" height="2" background="../imagenes/formdivfin.jpg" align="right"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																				<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivfin.jpg" width="8"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" ImageUrl="imagenes/btnImpr.jpg"
																			BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
																			ImageOver="btnImpr2.gif" ForeColor="Transparent" IncludesUrl="../includes/"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
