<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DeudaClienteProformas_pop" CodeFile="DeudaClienteProformas_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PROFORMAS</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<SCRIPT language="javascript">
		function mProformaDetAbrir(lstrid,lstrnro)
	   	{
          gAbrirVentanas("../Proforma_detalle_pop.aspx?id=" + lstrid + "&nro=" + lstrnro, 7);
	 	}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR>
					<TD>
						<asp:Label id="lblTitu" runat="server" width="100%" cssclass="opcion">Proformas</asp:Label></TD>
					<TD vAlign="top" align="right">&nbsp;<img onclick="window.close();" src="..\images\Close.bmp" alt="Cerrar">
					</TD>
				</TR>
				<TR>
					<TD colspan="2">
						<asp:Label id="lblClie" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
				</TR>
				<TR height="100%">
					<TD colspan="2" vAlign="top"><asp:datagrid id="grdDato" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px" PageSize="15"
							OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" ShowFooter=True
							AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="17px"></HeaderStyle>
									<ItemTemplate>
										<A href="javascript:mProformaDetAbrir('<%#DataBinder.Eval(Container, "DataItem.id")%>','<%#DataBinder.Eval(Container, "DataItem.N�mero")%>');">
											<img runat=server id=lnk1 src='../imagenes/Buscar16.gif' border="0" alt="Ver Detalle" style="cursor:hand;" />
										</A>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Actividad" HeaderText="Actividad"></asp:BoundColumn>
								<asp:BoundColumn DataField="N�mero" HeaderText="N�mero"></asp:BoundColumn>
								<asp:BoundColumn DataField="F.Ingreso" HeaderText="Fecha Valor" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
								<asp:BoundColumn DataField="Importe" DataFormatString="{0:F2}" HeaderText="Importe" HeaderStyle-HorizontalAlign="Right"
									ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colspan="2" align="right">
						<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" IncludesUrl="../includes/"
							ImagesUrl="../imagenes/" BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
							ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
