<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EstadisticaServicios" enableViewState="True" CodeFile="EstadisticaServicios.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="../controles/usrSociosFiltro.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reportes Estadísticos</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
		<script language="JavaScript">
			function mCargarLocalidades(pProv,pcmbLoca)
			{
				var sFiltro = pProv.value;
   				if (sFiltro != '')
				{
					LoadComboXML("localidades_cargar", sFiltro, pcmbLoca, "S");
				}
				else
				{
					document.all(pcmbLoca).innerText='';
				}
			}
			
			function mSetearControles(pObj)
			{
				var sValor = pObj.value;
   				if (sValor == '1')
				{
					document.all('divTipoServlbl').style.display='';
					document.all('divTipoServ').style.display='';
					document.all('lblFechaDesde').innerHTML = 'Fecha Nacimiento: Desde'
				}
				else if (sValor == '2')
				{
					document.all('divTipoServlbl').style.display='';
					document.all('divTipoServ').style.display='';
					document.all('lblFechaDesde').innerHTML = 'Fecha Servicio: Desde'
				}
				else if (sValor == '10' || sValor == '11' || sValor == '12')
				{
					document.all('divTipoServlbl').style.display='none';
					document.all('divTipoServ').style.display='none';
					document.all('lblFechaDesde').innerHTML = 'Fecha Exportación: Desde'
				}
				else if (sValor == '9' || sValor == '13' || sValor == '15')
				{
					document.all('divTipoServlbl').style.display='none';
					document.all('divTipoServ').style.display='none';
					document.all('lblFechaDesde').innerHTML = 'Fecha Importación: Desde'
				}
				else if (sValor == '5' || sValor == '14' || sValor == '16')
				{
					document.all('divTipoServlbl').style.display='none';
					document.all('divTipoServ').style.display='none';
					document.all('lblFechaDesde').innerHTML = 'Fecha Transferencia: Desde'
				}								
			}
			
			function mNumeDenoConsul(pRaza,pFiltro)
	   		{
	   			var strFiltro = pRaza.value;
				var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
				
 				if (strRet!='')
				{
	 				document.all('lblNumeDesdeVaca').innerHTML = strRet+' Madre Desde:';
	 				document.all('lblNumeDesdeToro').innerHTML = strRet+' Padre Desde:';	 					
	 			}
	 			else
	 			{
	 				document.all('lblNumeDesdeVaca').innerHTML = 'Nro. Madre Desde:';
	 				document.all('lblNumeDesdeToro').innerHTML = 'Nro. Padre Desde:';	 					
	 			}
			}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Estadistica de Movimientos y Servicios</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblProceso" runat="server" cssclass="titulo">Proceso:</asp:Label>&nbsp;</TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbProceso" runat="server" cssclass="cuadrotexto" Width="280px"
																						onchange="javascript:mSetearControles(this);" Height="20px">
																						<asp:ListItem Value="" Selected>(Seleccione)</asp:ListItem>
																						<asp:ListItem Value="1">Denuncias de Nacimiento</asp:ListItem>
																						<asp:ListItem Value="2">Denuncias de Servicio</asp:ListItem>
																						<asp:ListItem Value="5">Transferencias de Productos</asp:ListItem>
																						<asp:ListItem Value="16">Transferencia de Semen</asp:ListItem>
																						<asp:ListItem Value="14">Transferencia de Embriones</asp:ListItem>
																						<asp:ListItem Value="9">Importacion de Productos</asp:ListItem>
																						<asp:ListItem Value="13">Importacion de Embriones</asp:ListItem>
																						<asp:ListItem Value="15">Importacion de Semen</asp:ListItem>
																						<asp:ListItem Value="10">Exportacion de Productos</asp:ListItem>
																						<asp:ListItem Value="11">Exportacion de Semen</asp:ListItem>
																						<asp:ListItem Value="12">Exportacion de Embriones</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblRazaDesde" runat="server" cssclass="titulo">Raza: Desde:</asp:Label>&nbsp;</TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaDesde" runat="server" cssclass="cuadrotexto" Width="280px"
																						onchange="javascript:mNumeDenoConsul(this,true);" filtra="true" NomOper="razas_cargar" MostrarBotones="False"
																						Height="20px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblRazaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;</TD>
																				<TD align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaHasta" runat="server" cssclass="cuadrotexto" Width="280px"
																						filtra="true" NomOper="razas_cargar" MostrarBotones="False" Height="20px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 30%; HEIGHT: 17px" align="right">
																					<asp:label id="lblCodiDesde" runat="server" cssclass="titulo">Criador: Desde:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 70%">
																					<cc1:numberbox id="txtCodiDesde" runat="server" cssclass="cuadrotexto" Width="140px" Visible="true"
																						esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox>&nbsp;
																					<asp:label id="lblCodiHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																					<cc1:numberbox id="txtCodiHasta" runat="server" cssclass="cuadrotexto" Width="140px" Visible="true"
																						esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblFechaDenunFecha" runat="server" cssclass="titulo">Fecha Denuncia: Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtFechaDenunDesde" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																						IncludesUrl="Includes/"></cc1:DateBox>&nbsp;
																					<asp:label id="lblFechaDenunHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtFechaDenunHasta" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																						IncludesUrl="Includes/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha: Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																						IncludesUrl="Includes/"></cc1:DateBox>&nbsp;
																					<asp:label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																						IncludesUrl="Includes/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblNumeDesdeVaca" runat="server" cssclass="titulo">Nro. Madre Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<CC1:numberbox id="txtNumeDesdeVaca" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:numberbox>
																					<asp:label id="lblNumeHastaVaca" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<CC1:numberbox id="txtNumeHastaVaca" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblNumeDesdeToro" runat="server" cssclass="titulo">Nro. Padre Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<CC1:numberbox id="txtNumeDesdeToro" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:numberbox>
																					<asp:label id="lblNumeHastaToro" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<CC1:numberbox id="txtNumeHastaToro" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 21px" align="right">
																					<DIV id="divTipoServlbl" style="DISPLAY: none">
																						<asp:label id="lblProv" runat="server" cssclass="titulo">Tipo Servicio:</asp:label>&nbsp;</DIV>
																				</TD>
																				<TD style="HEIGHT: 21px">
																					<DIV id="divTipoServ" style="DISPLAY: none">
																						<cc1:combobox class="combo" id="cmbTipoServ" runat="server" Width="200px"></cc1:combobox></DIV>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																					<asp:label id="lblPais" runat="server" cssclass="titulo">Con Errores:</asp:label>&nbsp;</TD>
																				<TD>
																					<cc1:combobox class="combo" id="cmbConError" runat="server" Width="150px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR> <!--<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha de Alta: Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtAltaFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox>&nbsp;
																					<asp:label id="lblAltaFechaHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtAltaFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR> -->
																			<TR>
																				<TD style="HEIGHT: 17px" align="right" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD align="right" colSpan="3">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
