Namespace SRA

Partial Class SociosPagoACta
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      'Introducir aqu� el c�digo de usuario para inicializar la p�gina
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarEstados()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarEstados()
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "T")
   End Sub

#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String

            'If chkResumido.Checked Then
            '   lstrRptName = "SociosPagoACtaResumido"
            'Else
            lstrRptName = "SociosPagoACtaDetallado"
            'End If

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         Dim lintFechaD As Integer

         If txtFechaDesdeFil.Text = "" Then
            lintFechaD = 0
         Else
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&Fecha=" + lintFechaD.ToString
         lstrRpt += "&socio_d=" + IIf(txtSocioDesde.Text = "", "0", txtSocioDesde.Text)
         lstrRpt += "&socio_h=" + IIf(txtSocioHasta.Text = "", "0", txtSocioHasta.Text)
         lstrRpt += "&cate_id=" + cmbCate.Valor.ToString
            lstrRpt += "&cate_desc=" + IIf(cmbCate.Valor = 0, "Todos", cmbCate.SelectedItem.Text)
            lstrRpt += "&resu=" + IIf(chkResumido.Checked, "S", "N")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      txtFechaDesdeFil.Text = ""
      txtSocioDesde.Text = ""
      txtSocioHasta.Text = ""
      cmbCate.Limpiar()
      chkResumido.Checked = False
   End Sub
#End Region
End Class
End Namespace
