<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="../controles/usrSociosFiltro.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.MailingAlumnos" CodeFile="MailingAlumnos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Mailing de Alumnos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaSfcript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript">
		function mRecargarCombos()
		{
			if (document.all('txtAnioFil').value !='')
			{				
				var lstrAnio = document.all('txtAnioFil').value;
				var lstrInse = document.all('hdnInse').value;
				LoadComboXML("ciclos_cargar", lstrInse+','+lstrAnio, "cmbCicloFil", "T");
				var lstrCiclo = document.all('hdnSeleCicl').value;
				if (lstrCiclo != '')
				{
					document.all('cmbCicloFil').value = lstrCiclo;
					LoadComboXML("materiasXciclo_cargar", "@cicl_id="+lstrCiclo, "cmbMateFil", "T");
					var lstrMate = document.all('hdnSeleMate').value;
					LoadComboXML("divisiones_cargar","@divi_nombre='S',@divi_cicl_id="+lstrCiclo, "cmbDivision", "T");
					LoadComboXML("comisiones_cargar","@cicl_id="+lstrCiclo, "cmbComision", "T");		   
					document.all('cmbMateFil').value = lstrMate;
					document.all('cmbDivision').value = document.all('hdnSeleDivi').value;
					document.all('cmbComision').value = document.all('hdnSeleComi').value;					
					if (lstrMate != '')
					{
						LoadComboXML("periodos_cargar","@cicl_id="+lstrCiclo+",@mate_id="+lstrMate, "cmbPerioFil", "T");
						document.all('cmbPerioFil').value = document.all('hdnSelePerio').value;
					}
				}
		    }			
		}
		function mAnteriores()
		{
			if (document.all('cmbRepo').value!='M' || !document.all('chkAnte').checked)
			{
				document.all('panAnte').style.display='none';
				document.all('divBusqAvan').style.display='';
				document.all('cmbDestinatarios').style.display='';
				document.all('lblDestinatarios').style.display='';
			}
			else
			{
				document.all('panAnte').style.display='';
				document.all('divBusqAvan').style.display='none'
				if (document.all('panBuscAvan1').style.display != 'none')
					CambiarExp('1');
				document.all('cmbDestinatarios').style.display='none';
				document.all('lblDestinatarios').style.display='none';
			}		
		}
		function chkRepoDeudaClick(pDeuda)
		{
			document.all("chkRepoInte").checked=false;
			document.all("chkRepoInte").parentElement.disabled=!pDeuda.checked;
			document.all("chkRepoInte").disabled=!pDeuda.checked;
		} 
		function mValidarEnvio() 
		{
			if (document.all('cmbMail').value=='')
			{
				alert('Debe indicar un modelo de mail.');
				document.all('cmbMail').focus();
				return(false);
			}
			else
			{
				return(true);
			}
		}
		function expandir() 
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function mSetearTipo()
		{
			if (document.all('cmbRepo').value=='M')
			{
				document.all('lblMail').style.display='';
				document.all('cmbMail').style.display='';
				document.all('panAlumAnte').style.display='';				
			}
			else
			{
				document.all('lblMail').style.display='none';
				document.all('cmbMail').style.display='none';
				document.all('panAlumAnte').style.display='none';
				document.all('chkAnte').checked = false;
				mAnteriores();
			}
			if (document.all('cmbRepo').value=='E')
			{
				document.all('panDire').style.display='';
			}
			else
			{
				document.all('panDire').style.display='none';		
			}
			if (document.all('cmbRepo').value=='R')
			{
				document.all('panDatoOpci').style.display='';
			}
			else
			{
				document.all('panDatoOpci').style.display='none';		
			}
			if(document.all('cmbDestinatarios').value=='T' || document.all('cmbDestinatarios').value=='A')
			{
				document.all('panAlum').style.display='';
			}
			else
			{
					document.all('panAlum').style.display='none';
			}
		}
		function mRptFiltrosLimpiar()
		{
			EjecutarMetodoXML("Utiles.RptFiltrosLimpiar",document.all("hdnRptId").value);
		}		
		
		function CambiarExp(pstrNum)
		{
			if(document.all("spExp" + pstrNum).innerText=="+")
			{
				document.all("panBuscAvan" + pstrNum).style.display = "inline";
				document.all("spExp" + pstrNum).innerText="-";
			}
			
			else
			{
				document.all("panBuscAvan" + pstrNum).style.display = "none";
				document.all("spExp" + pstrNum).innerText="+";
			}

		}
		function mAgregar(pTipo)
		{
			switch(pTipo)
			{
				case "P":	// paises
				{ gAbrirVentanas("../MultiSeleGene.aspx?ctrl=hdnSelePais&tit=Paises&tab=paises&sel="+document.all('hdnSelePais').value, 0, "640","380","50"); break; }
				case "R":	// provincias
				{ gAbrirVentanas("../MultiSeleGene.aspx?ctrl=hdnSeleProv&tit=Provincias&tab=provincias&sel="+document.all('hdnSeleProv').value+"&fil="+document.all('hdnSelePais').value, 0, "640","380","50"); break; }
				case "L":	// localidades
				{ gAbrirVentanas("../MultiSeleGene.aspx?ctrl=hdnSeleLoca&tit=Localidades&tab=localidades&sel="+document.all('hdnSeleLoca').value+"&fil="+document.all('hdnSeleProv').value, 0, "640","380","50"); break; }
			}
		}
		function mActualizarSeleccion(pSele,pTabla)
		{
			var lstrResu = document.all(pSele).value;
			var pstrCombo = "";
			switch(pTabla)
			{
				case "paises":		{ pstrCombo="lisPais"; break;}
				case "provincias":	{ pstrCombo="lisProv"; break;}
				case "localidades":	{ pstrCombo="lisLoca"; break;}
			}
			if (lstrResu != '')
			{
				var sFiltro = "'" + lstrResu + "'";
				LoadComboXML(pTabla+"_multi_cargar", sFiltro, pstrCombo, "");		
				switch(pTabla)
				{
					case "paises":
					{ 
						sFiltro = "'"+document.all("hdnSeleProv").value+"','"+document.all("hdnSelePais").value+"'";
						if (document.all("hdnSeleProv").value!='')
						{
							LoadComboXML("provincias_multi_cargar", sFiltro, "lisProv", "");		
							document.all("hdnSeleProv").value = mDeterminarSeleccion("lisProv");
						}
						sFiltro = "'"+document.all("hdnSeleLoca").value+"','"+document.all("hdnSeleProv").value+"'";
						if (document.all("hdnSeleLoca").value!='')
						{
							LoadComboXML("localidades_multi_cargar", sFiltro, "lisLoca", "");		
							document.all("hdnSeleLoca").value = mDeterminarSeleccion("lisLoca");
						}
						break;
					}
					case "provincias":
					{ 
						sFiltro = "'"+document.all("hdnSeleLoca").value+"','"+document.all("hdnSeleProv").value+"'";
						if (document.all("hdnSeleLoca").value!='')
						{
							LoadComboXML("localidades_multi_cargar", sFiltro, "lisLoca", "");		
							document.all("hdnSeleLoca").value = mDeterminarSeleccion("lisLoca");
						}
						break;
					}
				}	
			}
			else
			{
				LimpiarCombo(document.all(pstrCombo));
				switch(pTabla)
				{
					case "paises":
					{ 
						LimpiarCombo(document.all("lisProv")); 
						LimpiarCombo(document.all("lisLoca")); 
						document.all('hdnSelePais').value = '';
						document.all('hdnSeleProv').value = '';
						document.all('hdnSeleLoca').value = '';
						break;
					}
					case "provincias":
					{ 
						LimpiarCombo(document.all("lisLoca")); 
						document.all('hdnSeleProv').value = '';
						document.all('hdnSeleLoca').value = '';
						break;
					}
					case "localidades":
					{ 
						document.all('hdnSeleLoca').value = '';
						break;
					}
				}		
			}
		}
		function mDeterminarSeleccion(pCombo)
		{
			var lstrSele = '';
			var combo = document.all(pCombo);
			for (j=0;j<combo.length;j++)
			{
				if (lstrSele != '')
					lstrSele = lstrSele + ',';
				lstrSele = lstrSele + combo.options[j].value;		
			}
			return(lstrSele);
		}
		function txtAnioFil_Change(pAnio, pInse)
		{
		    var sFiltro = pInse + "," ;
		    
		    if (pAnio.value=="")
		       sFiltro =  sFiltro + "-1";
		      else 
		       sFiltro = sFiltro + pAnio.value ;
		       
		    LoadComboXML("ciclos_cargar", sFiltro, "cmbCicloFil", "T");
		    LoadComboXML("periodos_cargar","@cicl_id=0,@mate_id=0", "cmbPerioFil", "T");
		    LoadComboXML("materiasXciclo_cargar", "@cicl_id=0", "cmbMateFil", "T");
		    LoadComboXML("divisiones_cargar","@divi_nombre='S',@divi_cicl_id=0", "cmbDivision", "T");
		    LoadComboXML("comisiones_cargar","@cicl_id=0", "cmbComision", "T");		   
		    
		    document.all('hdnSeleCicl').value = '';
		    document.all('hdnSeleMate').value = '';
		    document.all('hdnSelePerio').value = '';
		    document.all('hdnSeleComi').value = '';
		    document.all('hdnSeleDivi').value = '';		    
		}
		
		function cmbComision_Change(pValor, pInse)
		{
			document.all('hdnSeleComi').value = pValor.value;
		}		
		function cmbDivision_Change(pValor, pInse)
		{
			document.all('hdnSeleDivi').value = pValor.value;
		}
		function cmbMateFil_Change(pValor, pInse)
		{
			document.all('hdnSeleMate').value = pValor.value;
			if (pValor.value != '')
				LoadComboXML("periodos_cargar","@cicl_id="+document.all('hdnSeleCicl').value+",@mate_id=" + pValor.value, "cmbPerioFil", "T");
			else
				LoadComboXML("periodos_cargar","@cicl_id="+document.all('hdnSeleCicl').value+",@mate_id=0", "cmbPerioFil", "T");
		}
		function cmbPerioFil_Change(pValor, pInse)
		{
			document.all('hdnSelePerio').value = pValor.value;
		}
		
		function cmbCiclFil_Change(pCiclo, pInse)
		{
		    var sFiltro = "@mate_inse_id=" + pInse;
		    if(pCiclo.value!="")
				sFiltro = sFiltro + ",@cicl_id=" + pCiclo.value;

		    document.all('hdnSeleCicl').value = pCiclo.value;
		    
		    LoadComboXML("materiasXciclo_cargar", sFiltro, "cmbMateFil", "T");
		    LoadComboXML("divisiones_cargar","@divi_nombre='S',@divi_cicl_id=" + pCiclo.value + ",@cicl_inse_id=" + pInse, "cmbDivision", "T");
		    LoadComboXML("comisiones_cargar","@cicl_id=" + pCiclo.value, "cmbComision", "T");		   
		    LoadComboXML("periodos_cargar","@cicl_id=0,@mate_id=0", "cmbPerioFil", "T");
		}
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');mRecargarCombos();"
		rightMargin="0" onunload="javascript:mRptFiltrosLimpiar();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Mailing de Alumnos</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TBODY>
														<TR>
															<TD style="WIDTH: 100%">
																<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 8px" width="24"></TD>
																		<TD style="HEIGHT: 8px" width="42"></TD>
																		<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																		<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																		<TD style="HEIGHT: 8px" width="26"></TD>
																		<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																		<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																		<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																		<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD>
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																		<TD><!-- FOMULARIO -->
																			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				</TR>
																				<TR>
																					<TD background="../imagenes/formfdofields.jpg" height="28">
																						<TABLE border="0">
																							<TR>
																								<TD noWrap>&nbsp;&nbsp;
																									<asp:label id="lblRepo" runat="server" cssclass="titulo">Tipo de Reporte:</asp:label></TD>
																								<TD>
																									<cc1:combobox class="combo" id="cmbRepo" runat="server" Width="119px" onchange="javascript:mSetearTipo();">
																										<asp:ListItem Value="R" Selected="True">Reporte</asp:ListItem>
																										<asp:ListItem Value="E">Etiquetas</asp:ListItem>
																										<asp:ListItem Value="M">Mails</asp:ListItem>
																									</cc1:combobox>&nbsp;
																								</TD>
																								<TD>
																									<DIV id="panDire" noWrap>
																										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																											<TR>
																												<TD></TD>
																											</TR>
																											<TR>
																												<TD>
																													<asp:radiobutton id="RadTodos" GroupName="RadioGroup1" Checked="True" CssClass="Titulo" Runat="server"
																														Text="Todas"></asp:radiobutton>
																													<asp:radiobutton id="radConDire" GroupName="RadioGroup1" CssClass="Titulo" Runat="server" Text="Con Direcci�n"></asp:radiobutton>
																													<asp:radiobutton id="radSinDire" GroupName="RadioGroup1" CssClass="Titulo" Runat="server" Text="Sin Direcci�n"></asp:radiobutton></TD>
																											</TR>
																										</TABLE>
																									</DIV>
																								</TD>
																								<TD>
																									<asp:label id="lblMail" runat="server" cssclass="titulo">Modelo:</asp:label></TD>
																								<TD>
																									<cc1:combobox class="combo" id="cmbMail" runat="server" Width="119px"></cc1:combobox></TD>
																							</TR>
																							<TR>
																								<TD noWrap></TD>
																								<TD colSpan="2">
																									<DIV id="panAlumAnte">
																										<asp:checkbox id="chkAnte" onclick="javascript:mAnteriores();" Checked="false" CssClass="titulo"
																											Runat="server" Text="Alumnos A�os Anteriores"></asp:checkbox></DIV>
																								</TD>
																								<TD></TD>
																								<TD></TD>
																							</TR>
																							<TR>
																								<TD noWrap colSpan="5">
																									<DIV id="panAnte" runat="server">
																										<TABLE class="panelediciontransp" id="Table4" cellSpacing="0" cellPadding="0" width="90%"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 103px" noWrap align="right" height="4"></TD>
																												<TD noWrap colSpan="3" height="4"></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 103px" noWrap align="right">
																													<asp:label id="lblAnioDesde" runat="server" cssclass="titulo">A�o Desde: </asp:label>&nbsp;</TD>
																												<TD noWrap colSpan="3">
																													<cc1:numberbox id="txtAnioDesde" runat="server" cssclass="cuadrotexto" Width="60px" AceptaNull="False"
																														MaxLength="4" MaxValor="2099"></cc1:numberbox>&nbsp;
																													<asp:label id="lblAnioHasta" runat="server" cssclass="titulo">A�o Hasta:</asp:label>&nbsp;
																													<cc1:numberbox id="txtAnioHasta" runat="server" cssclass="cuadrotexto" Width="60px" AceptaNull="False"
																														MaxLength="4" MaxValor="2099"></cc1:numberbox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 103px" align="right">
																													<asp:label id="lblMate" runat="server" cssclass="titulo">Materia:</asp:label>&nbsp;</TD>
																												<TD style="WIDTH: 109px" colSpan="3">
																													<cc1:combobox class="combo" id="cmbMate" runat="server" Width="340px" onchange="javascript:mSetearTipo();"></cc1:combobox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 103px" align="right" height="4"></TD>
																												<TD style="WIDTH: 109px" colSpan="3" height="4"></TD>
																											</TR>
																										</TABLE>
																									</DIV>
																								</TD>
																							</TR>
																							<TR>
																								<TD noWrap>&nbsp;&nbsp;
																									<asp:label id="lblDestinatarios" runat="server" cssclass="titulo">Destinatarios:</asp:label></TD>
																								<TD>
																									<cc1:combobox class="combo" id="cmbDestinatarios" runat="server" Width="119px" onchange="javascript:mSetearTipo();">
																										<asp:ListItem Value="T" Selected="True">Todos</asp:ListItem>
																										<asp:ListItem Value="A">Alumnos</asp:ListItem>
																										<asp:ListItem Value="C">Contactos</asp:ListItem>
																										<asp:ListItem Value="P">Profesores</asp:ListItem>
																									</cc1:combobox>&nbsp;
																								</TD>
																							</TR>
																							<TR>
																								<TD noWrap colSpan="5">
																									<DIV class="panelediciontransp" id="panDatoOpci" style="WIDTH: 420px" runat="server">
																										<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
																											<TR>
																												<TD style="WIDTH: 140px" noWrap align="right">
																													<asp:label id="lblOpciDato" runat="server" cssclass="titulo">Datos Opcionales:&nbsp;</asp:label></TD>
																												<TD style="WIDTH: 109px">
																													<asp:checkbox id="chkRepoTele" Checked="false" CssClass="titulo" Runat="server" Text="Tel�fono"></asp:checkbox></TD>
																												<TD></TD>
																												<TD>
																													<asp:checkbox id="chkRepoIngreFecha" Checked="false" CssClass="titulo" Runat="server" Text="Fecha Ingreso"></asp:checkbox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 96px"></TD>
																												<TD style="WIDTH: 109px">
																													<asp:checkbox id="chkRepoMail" Checked="false" CssClass="titulo" Runat="server" Text="Mail"></asp:checkbox></TD>
																												<TD></TD>
																												<TD></TD>
																											</TR>
																										</TABLE>
																									</DIV>
																								</TD>
																							</TR>
																						</TABLE>
																					</TD>
																				</TR>
																				<TR>
																					<TD background="../imagenes/formfdofields.jpg">
																						<asp:datagrid id="grdCons" runat="server" BorderWidth="1px" BorderStyle="None" CellSpacing="1"
																							CellPadding="1" width="100%" AutoGenerateColumns="False" ItemStyle-Height="5px" PageSize="10"
																							OnPageIndexChanged="DataGrid_Page" GridLines="None" HorizontalAlign="Center" AllowPaging="True">
																							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																							<FooterStyle CssClass="footer"></FooterStyle>
																							<Columns>
																								<asp:TemplateColumn Visible="false">
																									<HeaderStyle Width="5%"></HeaderStyle>
																									<ItemTemplate>
																										<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																											Height="5">
																											<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
																										</asp:LinkButton>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																							</Columns>
																							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD>
																				</TR>
																				<TR>
																					<TD background="../imagenes/formfdofields.jpg">
																						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																							<TR>
																								<TD nowrap colspan=2 >
																									<DIV id="divBusqAvan" runat="server">
																									<SPAN id="spExp1" style="FONT-WEIGHT: bold; FONT-SIZE: 14px; CURSOR: hand; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 4px"
																										onclick="CambiarExp('1')">+</SPAN>																									
																									<asp:label id="lblExp1" runat="server" cssclass="titulo">B�squeda Avanzada</asp:label></TD>
																									</div>
																							</TR>
																							<TR>
																								<TD colSpan="2">
																									<DIV class="panelediciontransp" id="panBuscAvan1" style="DISPLAY: none; WIDTH: 100%">
																										<TABLE width="100%" align="center" border="0">
																											<TR>
																												<TD vAlign="top" noWrap colSpan="2">
																													<DIV id="panAlum">
																														<TABLE id="tableBusq" cellSpacing="0" cellPadding="0" width="100%" border="0">
																															<TR>
																																<TD vAlign="top" align="right">
																																	<asp:label id="lblEstudios" runat="server" cssclass="titulo">Estudios:</asp:label>&nbsp;</TD>
																																<TD vAlign="top" width="100%" colSpan="2">
																																	<cc1:checklistbox id="lstEstudios" runat="server" CssClass="chklst" CellSpacing="0" CellPadding="0"
																																		width="100%" height="100px" RepeatColumns="2" MuestraBotones="True"></cc1:checklistbox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right">
																																	<asp:label id="lblCargo" runat="server" cssclass="titulo">Cargos:</asp:label>&nbsp;</TD>
																																<TD vAlign="top" width="100%" colSpan="2">
																																	<cc1:checklistbox id="lstCargos" runat="server" CssClass="chklst" CellSpacing="0" CellPadding="0"
																																		width="100%" height="100px" RepeatColumns="2" MuestraBotones="True"></cc1:checklistbox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right">
																																	<asp:label id="lblOcupaciones" runat="server" cssclass="titulo">Ocupaciones:</asp:label>&nbsp;</TD>
																																<TD vAlign="top" width="100%" colSpan="2">
																																	<cc1:checklistbox id="lstOcupaciones" runat="server" CssClass="chklst" CellSpacing="0" CellPadding="0"
																																		width="100%" height="100px" RepeatColumns="2" MuestraBotones="True"></cc1:checklistbox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right">
																																	<asp:label id="lblBeca" runat="server" cssclass="titulo">Beca:</asp:label>&nbsp;
																																</TD>
																																<TD>
																																	<cc1:combobox class="combo" id="cmbBeca" runat="server" Width="250px"></cc1:combobox>&nbsp;
																																</TD>
																															</TR>
																															<TR>
																																<TD style="WIDTH: 140px" align="right" height="24">
																																	<asp:label id="lblLegajoDesde" runat="server" cssclass="titulo">Legajo Desde:</asp:label>&nbsp;</TD>
																																<TD style="WIDTH: 74px" height="24">
																																	<cc1:numberbox id="txtLegajoDesde" runat="server" cssclass="cuadrotexto" Width="60px"></cc1:numberbox></TD>
																																<TD height="24">
																																	<asp:label id="lblLegajoHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																																	<cc1:numberbox id="txtLegajoHasta" runat="server" cssclass="cuadrotexto" Width="60px"></cc1:numberbox></TD>
																															</TR>
																															<TR>
																																<TD style="WIDTH: 140px" align="right" height="24">
																																	<asp:label id="lblApelDesde" runat="server" cssclass="titulo">Apellido/Nombre:</asp:label>&nbsp;</TD>
																																<TD style="WIDTH: 74px" height="24">
																																	<asp:textbox id="txtApelDesde" runat="server" cssclass="cuadrotexto" Width="200px"></asp:textbox>&nbsp;</TD>
																																<TD height="24">
																																	<asp:label id="lblApelHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																																	<asp:textbox id="txtApelHasta" runat="server" cssclass="cuadrotexto" Width="200px"></asp:textbox></TD>
																															</TR>
																														</TABLE>
																													</DIV>
																												</TD>
																											</TR>
																											<TR>
																												<TD vAlign="top" align="left" colSpan="2">
																													<DIV id="panProf">
																														<TABLE id="tableProf" cellSpacing="0" cellPadding="0" width="100%" border="0">
																															<TR>
																																<TD vAlign="top" align="right" width="122" height="24">
																																	<asp:label id="lblAnioFil" runat="server" cssclass="titulo">A�o:</asp:label>&nbsp;
																																</TD>
																																<TD>
																																	<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="60px" AceptaNull="False"
																																		MaxLength="4" MaxValor="2099"></cc1:numberbox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right" width="122" height="24">
																																	<asp:label id="Label2" runat="server" cssclass="titulo">Ciclo:</asp:label>&nbsp;
																																</TD>
																																<TD>
																																	<cc1:combobox class="combo" id="cmbCicloFil" runat="server" Width="100%" AceptaNull="false" NomOper="ciclos_cargar"></cc1:combobox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right" width="122" height="24">
																																	<asp:label id="Label1" runat="server" cssclass="titulo">Materia:</asp:label>&nbsp;
																																</TD>
																																<TD>
																																	<cc1:combobox class="combo" id="cmbMateFil" runat="server" Width="250px"></cc1:combobox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right" width="122" height="24">
																																	<asp:label id="lblPerioFil" runat="server" cssclass="titulo">Per�odo:</asp:label>&nbsp;</TD>
																																<TD>
																																	<cc1:combobox class="combo" id="cmbPerioFil" runat="server" Width="250px"></cc1:combobox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right" width="122" height="24">
																																	<asp:label id="lblDivision" runat="server" cssclass="titulo">Divisiones</asp:label>&nbsp;
																																</TD>
																																<TD>
																																	<cc1:combobox class="combo" id="cmbDivision" runat="server" Width="250px"></cc1:combobox></TD>
																															</TR>
																															<TR>
																																<TD vAlign="top" align="right" width="122" height="24">
																																	<asp:label id="lblComision" runat="server" cssclass="titulo">Comisiones</asp:label>&nbsp;
																																</TD>
																																<TD>
																																	<cc1:combobox class="combo" id="cmbComision" runat="server" Width="250px"></cc1:combobox></TD>
																															</TR>
																														</TABLE>
																													</DIV>
																												</TD>
																											</TR>
																											<TR>
																												<TD colSpan="2">
																													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																														<TR>
																															<TD width="12"><SPAN id="spExp3" style="FONT-WEIGHT: bold; FONT-SIZE: 14px; CURSOR: hand; COLOR: black; FONT-FAMILY: Verdana; HEIGHT: 4px"
																																	onclick="CambiarExp('3')">+</SPAN></TD>
																															<TD width="98%">
																																<asp:label id="lblExp3" runat="server" cssclass="titulo">B�squeda Por Paises/Provincias/Localidades</asp:label></TD>
																														</TR>
																														<TR>
																															<TD colSpan="2">
																																<DIV class="panelediciontransp" id="panBuscAvan3" style="DISPLAY: none; Z-INDEX: 101; WIDTH: 100%">
																																	<TABLE width="100%" align="center" border="0">
																																		<TR>
																																			<TD vAlign="top" align="center">
																																				<asp:label id="lblPais" runat="server" cssclass="titulo">Paises:</asp:label></TD>
																																			<TD vAlign="top" align="center"></TD>
																																			<TD vAlign="top" align="center">
																																				<asp:label id="lblProv" runat="server" cssclass="titulo">Provincias:</asp:label></TD>
																																			<TD vAlign="top"></TD>
																																			<TD vAlign="top" align="center">
																																				<asp:label id="lblLoca" runat="server" cssclass="titulo">Localidades:</asp:label></TD>
																																		</TR>
																																		<TR>
																																			<TD vAlign="top" align="center" width="32%">
																																				<cc1:lista id="lisPais" onclick="this.selectedIndex=-1;" runat="server" width="98%" classtitulos="titulo"
																																					AutoPostBack="false" Height="120px" Font-Size="XX-Small"></cc1:lista></TD>
																																			<TD vAlign="top" align="center"></TD>
																																			<TD vAlign="top" align="center" width="32%">
																																				<cc1:lista id="lisProv" onclick="this.selectedIndex=-1;" runat="server" width="98%" classtitulos="titulo"
																																					AutoPostBack="false" Height="120px" Font-Size="XX-Small"></cc1:lista></TD>
																																			<TD vAlign="top"></TD>
																																			<TD vAlign="top" align="center" width="32%">
																																				<cc1:lista id="lisLoca" onclick="this.selectedIndex=-1;" runat="server" width="98%" classtitulos="titulo"
																																					AutoPostBack="false" Height="120px" Font-Size="XX-Small"></cc1:lista></TD>
																																		</TR>
																																		<TR>
																																			<TD vAlign="bottom" align="center" width="32%" height="30">
																																				<asp:button id="btnPais" runat="server" cssclass="boton" Width="130px" Text="Agregar Paises"></asp:button></TD>
																																			<TD vAlign="top" align="center" height="30"></TD>
																																			<TD vAlign="bottom" align="center" width="32%" height="30">
																																				<asp:button id="btnProv" runat="server" cssclass="boton" Width="130px" Text="Agregar Provincias"></asp:button></TD>
																																			<TD vAlign="top" height="30"></TD>
																																			<TD vAlign="bottom" align="center" width="32%" height="30">
																																				<asp:button id="btnLoca" runat="server" cssclass="boton" Width="130px" Text="Agregar Localidades"></asp:button></TD>
																																		</TR>
																																		<TR>
																																			<TD vAlign="top" align="right">
																																				<DIV style="DISPLAY: none">
																																					<cc1:textboxtab id="hdnInse" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSeleCicl" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSeleMate" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSelePerio" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSeleComi" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSeleDivi" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSelePais" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSeleProv" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab>
																																					<cc1:textboxtab id="hdnSeleLoca" runat="server" cssclass="cuadrotexto" Width="100"></cc1:textboxtab></DIV>
																																			</TD>
																																			<TD vAlign="top"></TD>
																																			<TD vAlign="top"></TD>
																																			<TD vAlign="top"></TD>
																																			<TD vAlign="top"></TD>
																																		</TR>
																																	</TABLE>
																																</DIV>
																															</TD>
																														</TR>
																													</TABLE>
																												</TD>
																											</TR>
																										</TABLE>
																									</DIV>
																								</TD>
																							</TR>
																						</TABLE>
																						<DIV></DIV>
																					</TD>
																				</TR>
																				<TR>
																					<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				</TR>
																			</TABLE>
																		</TD>
																		<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="3" height="10"></TD>
																	</TR>
																	<TR>
																		<TD align="right" colSpan="2">
																			<CC1:BOTONIMAGEN id="btnEnvi" runat="server" BorderStyle="None" ImageDisable="btnEnvi0.gif" IncludesUrl="../includes/"
																				ImagesUrl="../imagenes/" ImageUrl="imagenes/btnEnvi.gif" BackColor="Transparent" CambiaValor="False"
																				OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnEnvi.gif" ImageOver="btnEnvi2.gif" ForeColor="Transparent"
																				visible="false"></CC1:BOTONIMAGEN>
																			<CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																				ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																				BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BOTONIMAGEN>
																			<CC1:BOTONIMAGEN id="btnLimpFil" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																				ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																				BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																				CausesValidation="False"></CC1:BOTONIMAGEN></TD>
																		<TD width="2"></TD>
																	</TR>
																</TABLE>
											</asp:panel></TD>
									</TR>
								</TBODY>
							</TABLE>
						</td>
					</tr>
					<!---fin filtro --->
					<TR>
						<TD vAlign="middle" colSpan="3"></TD>
					</TR>
				</TBODY>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
			<tr>
				<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
				<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
				<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnRptId" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		<script language="JavaScript">
			mSetearTipo();
			mAnteriores();			
		</script>
	</BODY>
</HTML>
