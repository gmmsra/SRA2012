<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ComprobanteReporte" CodeFile="ComprobanteReporte.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Impresi�n de Comprobantes</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript" src="../includes/impresion.js"></script>
		<script language="javascript">
		/*function ImprimirSeleccion()
		{
			document.all('hdnImprSele').value = "S";
			__doPostBack('hdnImprSele',''); 
		}
		function mSeleccionar(pSele)
		{
			if (document.all("grdDatoBusq").children(0).children.length > 1)
			{
				for (var fila=1; fila <= document.all("grdDatoBusq").children(0).children.length-1; fila++)
				{
					document.all("grdDatoBusq").children(0).children(fila).children(0).children(0).checked = pSele;
				}
			}
		}
		function Imprimir()
		{
			var comprob="";
			var compId="";
			var compIds="";
			var boolOriginal="";
			var compImpreso="";
			var compAnulado="";
			var lstrRpt = "";
			var valParam="";
			
			if(document.all("cmbCoti").value == "30")
				lstrRpt = "Recibo"

			if(document.all("cmbCoti").value == "33")
				lstrRpt = "Acuse"

			if(document.all("cmbCoti").value == "29"||document.all("cmbCoti").value == "31"||document.all("cmbCoti").value == "32")	
				lstrRpt = "Factura"

			if(document.all("cmbCoti").value == "28")
				lstrRpt = "Proforma"
		    
			var strComp = document.all('hdnIds').value.split(";");

			//if (document.all("grdDatoBusq").children(0).children.length > 1)
			if (document.all('hdnIds').value != '')
			{
				try
				{
					//for (var fila=1; fila <= document.all("grdDatoBusq").children(0).children.length-1; fila++)
					for (var fila=0; fila <= strComp.length-1; fila++)
					{
						//if (document.all("grdDatoBusq").children(0).children(fila).children(0).children(0).checked)
						//{
							//compId = document.all("grdDatoBusq").children(0).children(fila).children(0).children(1).innerText;
							comprob = strComp[fila].split(",");
							compId = comprob[0]
							boolOriginal = comprob[1]
							compAnulado = comprob[2]

							//boolOriginal = document.all("grdDatoBusq__ctl" + (fila+2).toString() + "_chkOriginal").checked;
							//compAnulado = document.all("grdDatoBusq").children(0).children(fila).children(1).children(1).innerText;
							//compImpreso = document.all("grdDatoBusq").children(0).children(fila).children(1).children(2).innerText;
													
							var Estado="Original";
						    
							if (compAnulado==1) Estado="Anulado";
							else if (boolOriginal==0) Estado="Duplicado";

							//if(compImpreso=="False")
						    //{
								//if(compIds!="") compIds = compIds + ",";
								//compIds = compIds + compId;
						    //}

							valParam=compId + ";0;" + Estado;

							var sRet = ImprimirReporte(lstrRpt,"comp_id;random;estado", valParam, "<%=Session("sImpreTipo")%>",compId,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							
							if(sRet=="0") 
							{
								document.all("hdnImprId").value="";
								return;
							}
							
							if (compImpreso=="False" && Estado=="Original" && lstrRpt=="Factura")
							{
								valParam=compId + ";0;Duplicado";
								var sRet = ImprimirReporte(lstrRpt,"comp_id;random;estado", valParam, "<%=Session("sImpreTipo")%>",compId,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
								if(sRet=="0") 
								{
									document.all("hdnImprId").value="";
									return;
								}
							}
							
							if (lstrRpt=="Recibo")
							{
								ImprimirTalones(compId, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
							}
						//}
					}
				}
				catch(e)
				{
					document.all("hdnImprId").value="";
					alert("Error al intentar efectuar la impresi�n");
				}	
			}
			else
			{
				alert('Debe seleccionar los comprobantes a imprimir.');
			}
							
			document.all("hdnImprId").value="";
			document.all('hdnImprSele').value = "";

			compIds = document.all('hdnIdsImpr').value;
			if (compIds!="")
			{
			    if (window.confirm("�Se imprimieron los comprobantes correctamente?"))
				{
					document.all("hdnImprimio").value=compIds;
					__doPostBack('hdnImprimio','');				
				}
			}
		}*/
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tbody>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" cssclass="opcion" width="798px">Impresi�n de Comprobantes (Reporte)</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
											BorderStyle="Solid">
											<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px" width="100%" align="right">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="../imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False"
																		OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="100%">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="../imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False"
																		OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" width="100%" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" background="../imagenes/formfdofields.jpg">
																		<TR height="10">
																			<TD colSpan="3"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%" vAlign="top" background="../imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<UC1:CLIE id="usrClie" runat="server" Autopostback="false" Tabla="Clientes" AceptaNull="False"
																					FilSociNume="True" MuestraDesc="False" Ancho="800" Saltos="1,1,1,1"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" height="2" background="../imagenes/formdivmed.jpg" colSpan="3"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%" vAlign="top" background="../imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblCtro" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:combobox id="cmbCtro" class="cmbCtro" runat="server" cssclass="cuadrotexto" Width="250px"
																					AceptaNull="False" MostrarBotones="False" filtra="false" NomOper="emisores_ctros_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" height="2" background="../imagenes/formdivmed.jpg" colSpan="3"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;&nbsp;
																				<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;
																				<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" height="2" background="../imagenes/formdivmed.jpg" colSpan="3"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblNroComp" runat="server" cssclass="titulo">Letra:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:combobox id="cmbCompLetra" class="combo" runat="server" Width="70px">
																					<ASP:LISTITEM selected="true" value="">(Todos)</ASP:LISTITEM>
																					<ASP:LISTITEM value="A">A</ASP:LISTITEM>
																					<ASP:LISTITEM value="B">B</ASP:LISTITEM>
																				</cc1:combobox>&nbsp;&nbsp;
																				<asp:Label id="lblCompCemiNum" runat="server" cssclass="titulo" visible="false">Centro Emisor:</asp:Label>
																				<CC1:NUMBERBOX id="txtCompCemiNum" runat="server" cssclass="cuadrotexto" Width="48px" visible="false"
																					MaxValor="9999" MaxLength="4"></CC1:NUMBERBOX>&nbsp;
																				<asp:Label id="lblCompNum" runat="server" cssclass="titulo">N�:</asp:Label>
																				<CC1:NUMBERBOX id="txtCompNum" runat="server" cssclass="cuadrotexto" Width="120px" MaxValor="99999999"
																					MaxLength="8"></CC1:NUMBERBOX></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" height="2" background="../imagenes/formdivmed.jpg" colSpan="3"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:label id="lblCoti" runat="server" cssclass="titulo">Tipo:</asp:label>&nbsp;</TD>
																			<TD colSpan="2">
																				<cc1:combobox id="cmbCoti" class="combo" runat="server" Width="250px" autopostback="true"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" height="2" background="../imagenes/formdivmed.jpg" colSpan="2"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD></TD>
																			<TD colSpan="2" align="left">
																				<asp:CheckBox id="chkTodas" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small"
																					Text="Incluir Impresos"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD></TD>
																			<TD colSpan="2" align="left">
																				<asp:CheckBox id="chkIncAnulados" runat="server" cssclass="titulo" Height="8px" Font-Size="XX-Small"
																					Text="Incluir Anulados"></asp:CheckBox></TD>
																		</TR>
																		<TR height="10">
																			<TD colSpan="3"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 1.81%" height="2" background="../imagenes/formdivfin.jpg" align="right"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 12.69%" height="2" background="../imagenes/formdivfin.jpg" align="right"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 88%" height="2" background="../imagenes/formdivfin.jpg" width="8"><IMG src="../imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD height="10" background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
															</TR>
															<TR>
																<TD height="10" width="3"></TD>
																<TD height="10" align="right"></TD>
																<TD height="10" width="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE> <!---fin filtro ---></asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDatoBusq" runat="server" width="100%" BorderWidth="1px" BorderStyle="None"
											AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" AutoGenerateColumns="False"
											PageSize="15">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<ItemTemplate>
														<asp:CheckBox id="chksel" runat="server"></asp:CheckBox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="comp_id" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="comp_fecha" HeaderText="Fecha"></asp:BoundColumn>
												<asp:BoundColumn DataField="coti_desc" HeaderText="Tipo Comp."></asp:BoundColumn>
												<asp:BoundColumn DataField="numero" HeaderText="Numero"></asp:BoundColumn>
												<asp:BoundColumn DataField="soci_nume" HeaderText="Socio"></asp:BoundColumn>
												<asp:BoundColumn DataField="cliente" HeaderText="Cliente"></asp:BoundColumn>
												<asp:BoundColumn DataField="comp_neto" HeaderText="Importe"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD vAlign="middle" align="right" colSpan="3">
										<table width="100%" border="0">
											<tr>
												<td width="90"><asp:button id="btnTodos" runat="server" cssclass="boton" Width="80px" Text="Todos"></asp:button></td>
												<td width="90"><asp:button id="btnNinguno" runat="server" cssclass="boton" Width="80px" Text="Ninguno" CausesValidation="False"></asp:button></td>
												<td align="right">
													<CC1:BotonImagen id="btnEnviarPorMail" runat="server" BorderStyle="None" ImageUrl="../imagenes/mail-all.png"
														BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False"
														OutImage="del.gif" BtnImage="edit.gif" ImageBoton="mail-all.png" ImageOver="mail-all.png" ForeColor="Transparent"></CC1:BotonImagen>
													<A id="btnAceptar" href="javascript:ImprimirSeleccion();" runat="server"></A>
													<CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
														ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/"
														IncludesUrl="../includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN>&nbsp;
												</td>
											</tr>
										</table>
									</TD>
								</TR>
							</TABLE> <!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</tbody>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><asp:textbox id="hdnIds" runat="server"></asp:textbox><asp:textbox id="hdnIdsImpr" runat="server"></asp:textbox><asp:textbox id="hdnImprSele" runat="server"></asp:textbox><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnSess" runat="server"></asp:textbox><ASP:TEXTBOX id="hdnImprId" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnImprimio" runat="server" AutoPostBack="True"></ASP:TEXTBOX></DIV>
		</form>
		<!--<SCRIPT language="javascript">
		if (document.all["grdDatoBusq"]!= null)
			document.location='#grdDatoBusq';
		if (document.all('hdnImprSele').value == "S")
		{
			document.all('hdnImprSele').value = "";
			Imprimir();
		}
		</SCRIPT>-->
	</BODY>
</HTML>
