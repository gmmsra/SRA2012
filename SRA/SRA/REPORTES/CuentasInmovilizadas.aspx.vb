Namespace SRA

Partial Class Cuentas_Inmovilizadas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents lblTipo As System.Web.UI.WebControls.Label
	Protected WithEvents cmbTipo As NixorControls.ComboBox



	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: This method call is required by the Web Form Designer
		'Do not modify it using the code editor.
		InitializeComponent()
	End Sub

#End Region

#Region "Definición de Variables"
	Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)

			If Not Page.IsPostBack Then
				
				clsWeb.gInicializarControles(sender, mstrConn)
			End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mListar()
		Dim lstrRptName As String
		Dim lintFechaD As Integer

		lstrRptName = "CuentasInmovilizadas"
		If txtFechaD.Fecha.ToString = "" Then
			lintFechaD = 0
		Else
			lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaD.Text, "Int32"), Integer)
		End If

		Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

		lstrRpt += "&fecha_desde=" + lintFechaD.ToString
		lstrRpt += "&clie_id=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
		lstrRpt += "&sin_movimiento=" + IIf(chkincluInmovi.Checked = True, "TRUE", "FALSE")


		lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
		Response.Redirect(lstrRpt)
	End Sub

	Private Sub mLimpiarFiltros()

		txtFechaD.Text = ""
		usrClieFil.Limpiar()
		chkincluInmovi.Checked = False

	End Sub

#End Region
#Region "Operaciones sobre la Pagina"

	Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		mListar()
	End Sub


	Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltros()
	End Sub
#End Region
End Class
End Namespace
