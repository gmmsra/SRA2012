<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EvolucionPrecios" enableViewState="True" CodeFile="EvolucionPrecios.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="../controles/usrSociosFiltro.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Evolución de Precios</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');mCargarPrecios(false);mSelecCombos();" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 8px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Evolución de Precios</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 8px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="97%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20.3%; HEIGHT: 15px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 15px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 21px" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20.3%; HEIGHT: 21px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21">
																					<asp:Label id="lblActividadFil" runat="server" cssclass="titulo">Actividad:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 21px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21">
																					<cc1:combobox class="combo" onchange="javascript:mCargarPrecios(true);" id="cmbActividadFil" runat="server" Width="272px" NomOper="actividades_cargar"
																						ImagesUrl="../Images/" IncludesUrl="../Includes/" AceptaNull="false"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 20.04%" noWrap align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblVigenciaDesde" runat="server" cssclass="titulo" Width="135px">Comparar Vigencia:</asp:Label>&nbsp;</TD>
																				<TD background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbListaDesdeFil" onchange="javascript:mSelecLista(this,'D');" runat="server" Width="272px" NomOper="actividades_cargar"
																						ImagesUrl="../Images/" IncludesUrl="../Includes/" AceptaNull="false"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 20.04%" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblVigenciaHasta" runat="server" cssclass="titulo" Width="107px">Con Vigencia:</asp:Label>&nbsp;</TD>
																				<TD background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbListaHastaFil" onchange="javascript:mSelecLista(this,'H');" runat="server" Width="272px" NomOper="actividades_cargar"
																						ImagesUrl="../Images/" IncludesUrl="../Includes/" AceptaNull="false"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 21px" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20.3%; HEIGHT: 21px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21">
																					<asp:Label id="lblArancelFil" runat="server" cssclass="titulo">Arancel:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 21px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21"><TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbArancelFil" runat="server" cssclass="cuadrotexto" Width="350px"
																									TextMaxLength="5" nomoper="aranceles_cargar" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzadaRepo('aranceles','aran_desc','cmbArancelFil','Aranceles','');"
																									alt="Busqueda avanzada" src="../imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 15px" background="../imagenes/formfdofields.jpg" height="21"></TD>
																				<TD style="WIDTH: 20.3%; HEIGHT: 15px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 15px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																					height="21"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 20.3%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
			<ASP:TEXTBOX id="hdnListaDesde" runat="server" Width="140px"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="hdnListaHasta" runat="server" Width="140px"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
			</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
		<script language=javascript>
		function mSelecLista(pLista,pTipo)
		{
			if (pTipo == 'D')
				document.all('hdnListaDesde').value = pLista.options[pLista.selectedIndex].text;
			else
				document.all('hdnListaHasta').value = pLista.options[pLista.selectedIndex].text;
		}
		function mSelecCombos()
		{
			if (document.all('hdnListaDesde').value != '')
				gBuscarTextoCombo(document.all('hdnListaDesde').value, document.all('cmbListaDesdeFil'));
			if (document.all('hdnListaHasta').value != '')
				gBuscarTextoCombo(document.all('hdnListaHasta').value, document.all('cmbListaHastaFil'));
		}
		function mCargarPrecios(pLimp)
		{
			var sFiltro = document.all('cmbActividadFil').value;
			if (sFiltro == '')
				sFiltro = '0';
			LoadComboXML("precios_lista_copi_cargar", sFiltro, "cmbListaDesdeFil", "T");
			LoadComboXML("precios_lista_copi_cargar", sFiltro, "cmbListaHastaFil", "T");
			if (pLimp)
			{
				document.all('hdnListaDesde').value = '';
				document.all('hdnListaHasta').value = '';
			}
		}
		</script>
	</BODY>
</HTML>
