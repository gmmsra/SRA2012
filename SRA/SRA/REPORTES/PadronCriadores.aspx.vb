
Imports ReglasValida.Validaciones


Namespace SRA



Partial Class PadronCriadores
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaReporte As System.Web.UI.WebControls.Label
    Protected WithEvents txtFechaListado As NixorControls.DateBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then


            End If


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

 

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "PadronCriadores"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&FechaDesde=" & IIf(txtFechaDesde.Fecha.ToString = "", "0", _
                     mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaDesde.Text))))


            lstrRpt += "&FechaHasta=" & IIf(txtFechaHasta.Fecha.ToString = "", "0", _
                     mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaHasta.Text))))

            If cmbSexoFil.Valor.ToString <> "" Then
                lstrRpt += "&sexo=" & cmbSexoFil.Valor.ToString
            End If


            If txtRPAranDdeRRGG.Text.ToString <> "" Then
                lstrRpt += "&RPNDesde=" & txtRPAranDdeRRGG.Valor
            Else
                lstrRpt += "&RPNDesde=0"

            End If


            If txtRPAranHtaRRGG.Text.ToString <> "" Then
                lstrRpt += "&RPNHasta=" & txtRPAranHtaRRGG.Valor
            Else
                lstrRpt += "&RPNHasta=0"
            End If
           


            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click

        txtRPAranDdeRRGG.Text = ""
        txtRPAranHtaRRGG.Text = ""
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""
        cmbSexoFil.Limpiar()
    End Sub
End Class

End Namespace
