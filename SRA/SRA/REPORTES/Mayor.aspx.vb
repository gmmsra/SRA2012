Namespace SRA

Partial Class Mayor
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents pnlPago As System.Web.UI.WebControls.Panel
    Protected WithEvents lblTipoPago As System.Web.UI.WebControls.Label
    Protected WithEvents cmbTipoPago As NixorControls.ComboBox
    Protected WithEvents pnlConceptosAranc As System.Web.UI.WebControls.Panel
    Protected WithEvents txtArancelDesde As NixorControls.NumberBox
    Protected WithEvents lblArancelDesde As System.Web.UI.WebControls.Label
    Protected WithEvents lblArancelHasta As System.Web.UI.WebControls.Label
    Protected WithEvents txtArancelHasta As NixorControls.NumberBox
    Protected WithEvents lblConceptoDesde As System.Web.UI.WebControls.Label
    Protected WithEvents lblConceptoHasta As System.Web.UI.WebControls.Label
    Protected WithEvents txtConceptoHasta As NixorControls.NumberBox
    Protected WithEvents txtConceptoDesde As NixorControls.NumberBox
    Protected WithEvents pnlAranceles As System.Web.UI.WebControls.Panel

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrTitu As String
    Private mstrRepo As String
    Private mstrTipo As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                txtFechaDesde.Fecha = Now
                txtFechaHasta.Fecha = Now
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mInicializar()

    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuentaDesde, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuentaHasta, "T")
    End Sub
#End Region

#Region "Detalle"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer
            lstrRptName = "MayorResu"

            'If cmbDetalle.Valor.ToString = "R" Then
            'lstrRptName = lstrRptName + "Resu"
            'End If

            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtFechaDesde.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
            End If

            If txtFechaHasta.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
            End If

            lstrRpt += "&fecha_desde=" + lintFechaDesde.ToString
            lstrRpt += "&fecha_hasta=" + lintFechaHasta.ToString
            lstrRpt += "&cuenta_desde=" + IIf(cmbCuentaDesde.Valor.ToString = "", "0", cmbCuentaDesde.Valor.ToString)
            lstrRpt += "&cuenta_hasta=" + IIf(cmbCuentaHasta.Valor.ToString = "", "0", cmbCuentaHasta.Valor.ToString)
            lstrRpt += "&detalle=" + cmbDetalle.Valor.ToString

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class

End Namespace
