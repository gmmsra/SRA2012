Imports System.Management


Namespace SRA

Partial Class ReportViewer
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mstrConn As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Page.RegisterStartupScript("clientScript", "<script language=javascript>window.top.close();</script>")

        'comp_id;random;estado         Param()
        mstrConn = clsWeb.gVerificarConexion(Me)

        Dim reporte As String = Request.QueryString("Param").ToString()
        Dim vNomParams As Array = Request.QueryString("vNomParams").ToString().Split(",")

        Dim vValParams As Array = Request.QueryString("vValParams").ToString().Split(",")


        Dim strValParam As String
        Dim index As Integer = 0
        Dim ldecRand As Integer

        For Each item As String In vNomParams
            If item.ToUpper() = "RANDOM" Then
                vValParams(index) = (New Random).Next()
            End If

            strValParam += "&" + item + "=" + vValParams(index)
            index = index + 1
        Next



        For Each printer As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            Dim pepe As String
            pepe = printer
        Next printer

        clsWeb.gImprimirReporte(mstrConn, Session("sUserId").ToString(), "Factura", 11 _
                                , Request.QueryString("vNomParams").ToString().Replace(",", ";") _
                                , Request.QueryString("vValParams").ToString().Replace(",", ";"), Request)

        'Dim oquery As System.Management.ObjectQuery = New System.Management.ObjectQuery("SELECT * FROM Win32_Printer")

        'Dim mosearcher As System.Management.ManagementObjectSearcher = New System.Management.ManagementObjectSearcher(oquery)

        'Dim moc As System.Management.ManagementObjectCollection = mosearcher.Get()

        'For Each mo As ManagementObject In moc
        '    Dim pdc As System.Management.PropertyDataCollection = mo.Properties
        '    For Each pd As System.Management.PropertyData In pdc
        '        If CBool(mo("Network")) Then
        '            cmbPrinters.Items.Add(mo(pd.Name))
        '        End If
        '    Next pd
        'Next mo

        'me.Response.Redirect(url);


        Me.Response.Write("1")
    End Sub
End Class

End Namespace
