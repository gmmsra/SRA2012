Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports AccesoBD
Imports Interfaces.Importacion


Namespace SRA



Partial Class CriadoresMovCantReporte
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblNaciFechaFil As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label





    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GSZ 07-11-2014 Se agrego fecha denuncia y Raza
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)


            If Not Page.IsPostBack Then
                'Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()

        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "T")

        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
    End Sub
    Private Sub mListar()
        Try
            Dim params As String
            Dim lstrRptName As String = "ListadoMovCantCriadores"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim stRazaDescripcion As String
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())


            lstrRpt += "&FechaDenuDesde=" & IIf(txtDenuFechaDesdeFil.Fecha.ToString = "", "0", _
                    mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtDenuFechaDesdeFil.Text))))


            lstrRpt += "&FechaDenuHasta=" & IIf(txtDenuFechaHastaFil.Fecha.ToString = "", "0", _
             mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtDenuFechaHastaFil.Text))))

            If cmbRazaFil.Valor.ToString <> "" Then
                lstrRpt += "&raza_id=" & cmbRazaFil.Valor.ToString
            End If

            stRazaDescripcion = oRaza.GetRazaDescripcionById( _
                                                     IIf(cmbRazaFil.Valor.ToString = "", "0", _
                                                     cmbRazaFil.Valor.ToString)).Trim()

            lstrRpt += "&RazaDescripcion=" & _
                        oRaza.GetRazaCodiById(cmbRazaFil.Valor.ToString).ToString + _
                        "-" + stRazaDescripcion


            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click

            txtDenuFechaDesdeFil.Fecha = String.Empty
            txtDenuFechaHastaFil.Fecha = String.Empty

        cmbRazaFil.Limpiar()

    End Sub
End Class

End Namespace
