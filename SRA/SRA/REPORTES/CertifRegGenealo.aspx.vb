Imports System.Data.SqlClient


Namespace SRA


Partial Class CertifRegGenealo
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label
   Protected WithEvents txtCriaNumeDesde As NixorControls.TextBoxTab

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
   Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub
    Private Sub mLimpiar()

        txtApedido.Text = ""
        txtNroTramite.Text = ""
        txtPais.Text = ""
        txtParaPresentar.Text = ""

        usrProductoFil.Limpiar()

        usrProductoFil.cmbSexoProdExt.Valor = ""


    End Sub
    Private Sub mImprimirConcepto()
        Try
            mValidarDatos()
            Dim params As String
            Dim lstrRptName As String = "CertificadoRegistrosGenealo"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            params += "&tram_nume=" & txtNroTramite.Valor
            params += "&cria_id=" + usrProductoFil.CriaOrPropId
            params += "&prdt_id=" + usrProductoFil.Valor.ToString
            params += "&Pais=" + txtPais.Text
            params += "&aPedido=" + txtApedido.Text
            params += "&paraPresentarA=" + txtParaPresentar.Text

            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mInicializar()
    

        

    End Sub
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         mImprimirConcepto()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiar()
    End Sub
End Class
End Namespace
