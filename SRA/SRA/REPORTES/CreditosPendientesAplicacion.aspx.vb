Namespace SRA

Partial Class CreditosPendientesAplicacion

    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
             mCargarCombo()
           
         End If


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

   Private Sub mCargarCombo()
      '7:Ajustes
      '30:Recibo
      '31:NC

        clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbComp, "T", "'7,9,14,30,31,33,35,36,39'")
   End Sub
   Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String
         Dim lintFechaD, lintFechaH, lintFechaHisto As Integer

         If chkRes.Checked Then
            lstrRptName = "CreditosPendientesAplicacionResu"
         Else
            lstrRptName = "CreditosPendientesAplicacion"
         End If


         If txtFechaD.Fecha.ToString = "" Then
            lintFechaD = 0
         Else
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaD.Text, "Int32"), Integer)
         End If

         If txtFechaH.Fecha.ToString = "" Then
            lintFechaH = 0
         Else
            lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaH.Text, "Int32"), Integer)
         End If

         If txtFechaHisto.Fecha.ToString = "" Then
            lintFechaHisto = 0
         Else
            lintFechaHisto = CType(clsFormatear.gFormatFechaString(txtFechaHisto.Text, "Int32"), Integer)
         End If
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)


         lstrRpt += "&fechaD=" + lintFechaD.ToString
         lstrRpt += "&fechaH=" + lintFechaH.ToString
         lstrRpt += "&fechaHisto=" + lintFechaHisto.ToString
         lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
         lstrRpt += "&Cemi=" + IIf(txtCompCemiNum.Valor.ToString = "", "0", txtCompCemiNum.Valor.ToString)
         lstrRpt += "&comp_coti_id=" + IIf(cmbComp.Valor.ToString = "", "0", cmbComp.Valor.ToString)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
