Namespace SRA

Partial Class Autorizaciones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            clsWeb.gInicializarControles(Me, mstrConn)
            mSetearMaxLength()
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "autoriza_tipos", cmbAutoTipo, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbCompTipo, "T", "'29,30,31,32'")
      clsWeb.gCargarRefeCmb(mstrConn, "usuarios_comp_autorizacion", cmbCompUser, "T")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, "comprobantes")

      txtCompCemiNum.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "comp_cemi_nume")
      txtCompNum.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "comp_nume")
   End Sub
#End Region

#Region "Detalle"
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "ComprobantesAutor"
         Dim lstrRpt As String
         Dim lintFechaDesde As Integer
         Dim lintFechaHasta As Integer

         lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If txtFechaDesdeFil.Fecha.ToString = "" Then
            lintFechaDesde = 0
         Else
            lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If

         If txtFechaHastaFil.Fecha.ToString = "" Then
            lintFechaHasta = 0
         Else
            lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If


         lstrRpt += "&FechaDesde=" + lintFechaDesde.ToString
         lstrRpt += "&FechaHasta=" + lintFechaHasta.ToString
         lstrRpt += "&AutoTipo=" + IIf(cmbAutoTipo.Valor.ToString = "", "0", cmbAutoTipo.Valor.ToString)
         lstrRpt += "&ComprobTipo=" + IIf(cmbCompTipo.Valor.ToString = "", "0", cmbCompTipo.Valor.ToString)
         lstrRpt += "&ComprobLetra=" + IIf(cmbCompLetra.Valor.ToString = "", "", cmbCompLetra.Valor.ToString)
         lstrRpt += "&ComprobNroCenEmi=" + IIf(txtCompCemiNum.Valor.ToString = "", "0", txtCompCemiNum.Valor.ToString)
         lstrRpt += "&ComprobNumero=" + IIf(txtCompNum.Valor.ToString = "", "0", txtCompNum.Valor.ToString)
         lstrRpt += "&Usuario=" + IIf(cmbCompUser.Valor.ToString = "", "0", cmbCompUser.Valor.ToString)


         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
