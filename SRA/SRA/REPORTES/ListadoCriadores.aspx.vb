Namespace SRA

Partial Class ListadoCriadores
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTarjeta As System.Web.UI.WebControls.Label
    Protected WithEvents cmbTarjeta As NixorControls.ComboBox
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    Protected WithEvents cmbEstado As NixorControls.ComboBox
    Protected WithEvents lblDeca As System.Web.UI.WebControls.Label
    Protected WithEvents cmbDeca As NixorControls.ComboBox
    Protected WithEvents lblNume As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaHasta As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaInscHasta As System.Web.UI.WebControls.Label
    Protected WithEvents lblInscFechaDesde As System.Web.UI.WebControls.Label
    Protected WithEvents txtInscFechaDesde As NixorControls.DateBox
    Protected WithEvents lblInscFechaHasta As System.Web.UI.WebControls.Label
    Protected WithEvents txtInscFechaHasta As NixorControls.DateBox
    Protected WithEvents lblNombreDesde As System.Web.UI.WebControls.Label
    Protected WithEvents txtNombreDesde As NixorControls.TextBoxTab
    Protected WithEvents lblHasta As System.Web.UI.WebControls.Label
    Protected WithEvents txtNombreHasta As NixorControls.TextBoxTab


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrActiId As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
           
            If (Not Page.IsPostBack) Then
                mCargarCombos()
                
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mCargarCombos()

        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbProvDesde, "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaDesde, "id", "descrip_codi", "S")
        SRA_Neg.Utiles.gSetearRaza(cmbRazaDesde)
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaHasta, "id", "descrip_codi", "S")
        SRA_Neg.Utiles.gSetearRaza(cmbRazaHasta)
    End Sub
    

    Private Sub mLimpiarFiltros()
        txtFechaListado.Text = ""
        cmbRazaDesde.Limpiar()
        cmbRazaHasta.Limpiar()
        txtNomb.Text = ""
        chkBuscNomb.Checked = False
        cmbProvDesde.Limpiar()
        cmbLocaDesde.Limpiar()
        cmbLocaHasta.Limpiar()
    End Sub
#End Region

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim params As String
            Dim lstrRptName As String = "ListadoCriadores"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim oProvincia As New SRA_Neg.Provincia(mstrConn, Session("sUserId").ToString())



            lstrRpt += "&cria_raza_id_desde=" & IIf(cmbRazaDesde.Valor.ToString = "", "0", cmbRazaDesde.Valor)
            lstrRpt += "&cria_raza_id_hasta=" & IIf(cmbRazaHasta.Valor.ToString = "", "0", cmbRazaHasta.Valor)
            If txtNomb.Text <> "" Then
                lstrRpt += "&clie_fanta=" & txtNomb.Text
            Else
                lstrRpt += "&clie_fanta=" & System.DBNull.Value
            End If

            lstrRpt += "&prov_id=" & IIf(cmbProvDesde.Valor.ToString = "", "0", cmbProvDesde.Valor)



            If cmbLocaDesde.Items.Count > 0 Then
                lstrRpt += "&loca_desc_desde=" & Trim(cmbLocaDesde.SelectedItem.Text)

            Else
                lstrRpt += "&loca_desc_desde=" & System.DBNull.Value
            End If

            If cmbLocaHasta.Items.Count > 0 Then
                lstrRpt += "&loca_desc_hasta=" & Trim(cmbLocaHasta.SelectedItem.Text)

            Else
                lstrRpt += "&loca_desc_hasta=" & System.DBNull.Value
            End If

            lstrRpt += "&buscar_en_nomb=" & IIf(chkBuscNomb.Checked, True, False)

            If txtFechaListado.Text <> "" Then
                lstrRpt += "&FechaListado=" & txtFechaListado.Text
            Else
                lstrRpt += "&FechaListado=" & Date.Now.ToShortDateString()
            End If

            If cmbProvDesde.Valor.ToString <> "" Then
                lstrRpt += "&ProvinciaDesc=" + oProvincia.GetProvinciaDescripcionById(cmbProvDesde.Valor)
            End If



            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)



            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
End Class
End Namespace
