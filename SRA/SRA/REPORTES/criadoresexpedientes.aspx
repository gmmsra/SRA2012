<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="../controles/usrSociosFiltro.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CriadoresExpedientes" enableViewState="True" CodeFile="CriadoresExpedientes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reportes Estad�sticos</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
		<script language="JavaScript">
			function mCargarLocalidades(pProv,pcmbLoca)
			{
				var sFiltro = pProv.value;
   				if (sFiltro != '')
				{
					LoadComboXML("localidades_cargar", sFiltro, pcmbLoca, "S");
				}
				else
				{
					document.all(pcmbLoca).innerText='';
				}
			}
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Reportes Expedientes</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblRazaDesde" runat="server" cssclass="titulo">Raza: Desde:</asp:Label>&nbsp;</TD>
																				<TD background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaDesde" runat="server" cssclass="cuadrotexto" Width="280px"
																						Height="20px" MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblRazaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;</TD>
																				<TD align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaHasta" runat="server" cssclass="cuadrotexto" Width="280px"
																						Height="20px" MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" align="right">
																					<asp:label id="lblNumeDesde" runat="server" cssclass="titulo">Nro. Criador: Desde:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 75%">
																					<cc1:numberbox id="txtCrianumeDesde" runat="server" cssclass="cuadrotexto" Width="140px" Visible="true"
																						MaxLength="12" MaxValor="9999999999999" esdecimal="False"></cc1:numberbox>&nbsp;
																					<asp:label id="lblnumeHasta" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																					<cc1:numberbox id="txtCriaNumeHasta" runat="server" cssclass="cuadrotexto" Width="140px" Visible="true"
																						MaxLength="12" MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																					<asp:label id="lblApel" runat="server" cssclass="titulo">Apellido/Raz�n Social:</asp:label>&nbsp;</TD>
																				<TD>
																					<CC1:TEXTBOXTAB id="txtApel" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB><asp:checkbox id="chkBuscApel" Runat="server" CssClass="titulo" Text="Buscar en..."></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																					<asp:label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
																				<TD>
																					<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB><asp:checkbox id="chkBuscNomb" Runat="server" CssClass="titulo" Text="Buscar en..."></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																				<TD>
																					<asp:checkbox id="chkBaja" Runat="server" CssClass="titulo" Text="Incluir Dados de Baja"></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 21px" align="right">
																					<asp:label id="lblProv" runat="server" cssclass="titulo">Provincia: Desde:</asp:label>&nbsp;</TD>
																				<TD style="HEIGHT: 21px">
																					<cc1:combobox class="combo" id="cmbProvDesde" runat="server" Width="200px" onchange="mCargarLocalidades(this,'cmbLocaDesde')"></cc1:combobox>&nbsp;
																					<asp:label id="lblProvHatsa" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																					<cc1:combobox class="combo" id="cmbProvHasta" runat="server" Width="205px" onchange="mCargarLocalidades(this,'cmbLocaHasta')"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																					<asp:label id="lblPais" runat="server" cssclass="titulo">Localidad: Desde:</asp:label>&nbsp;</TD>
																				<TD>
																					<cc1:combobox class="combo" id="cmbLocaDesde" runat="server" Width="200px"></cc1:combobox>&nbsp;
																					<asp:label id="Label2" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																					<cc1:combobox class="combo" id="cmbLocaHasta" runat="server" Width="205px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha de Alta: Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtAltaFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox>&nbsp;
																					<asp:label id="lblAltaFechaHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtAltaFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblInscFecha" runat="server" cssclass="titulo">Fecha de Baja: Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtBajaFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox>&nbsp;
																					<asp:label id="lblBajaFechacHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtBajaFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblInscFechaDesde" runat="server" cssclass="titulo">Productos Inscrip: Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtInscFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox>&nbsp;
																					<asp:label id="lblInscFechaHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtInscFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblNaciFechaDesde" runat="server" cssclass="titulo">Productos Denun.: Desde:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtNaciFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox>&nbsp;
																					<asp:label id="lblNaciFechaHasta" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																					<cc1:DateBox id="txtNaciFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" IncludesUrl="Includes/"
																						ImagesUrl="Images/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right">
																					<asp:label id="lblTipoListado" runat="server" cssclass="titulo">Tipo Listado:</asp:label>&nbsp;</TD>
																				<TD>
																					<cc1:combobox class="combo" id="cmbTipoListado" runat="server" Width="200px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" align="right" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD align="right" colSpan="3">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																			ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" IncludesUrl="../includes/"
																			ImagesUrl="../imagenes/" ImageUrl="../imagenes/limpiar.jpg" BackColor="Transparent" CambiaValor="False"
																			OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
