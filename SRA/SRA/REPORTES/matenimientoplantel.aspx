<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.MatenimientoPlantel" CodeFile="MatenimientoPlantel.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Matenimiento de Plantel</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">		
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('Matenimiento de Plantel');"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 14px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Matenimiento de Plantel</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 14px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="97%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="2"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg" height="10"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD height="10"><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 100%" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1">
																				</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 20px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%" align="left" background="../imagenes/formfdofields.jpg" colSpan="2">
																					<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="280px" filtra="true"
																						NomOper="razas_cargar" MostrarBotones="False" Height="19px" AutoPostBack="False"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1">
																				</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Per�odo desde:</asp:Label>&nbsp;
																				</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
																					<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%" background="../imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1">
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" width="25%" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblIncluSocios" runat="server" cssclass="titulo">Incluir Socios:</asp:Label>&nbsp;
																				</TD>
																				<TD width="75%" background="../imagenes/formfdofields.jpg">
																					<asp:checkbox id="chkIncluye" Text="" CssClass="titulo" Runat="server"></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%" background="../imagenes/formdivfin.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1">
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg" height="10"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3" height="10"></TD>
																	<TD align="right" height="10"></TD>
																	<TD width="2" height="10"></TD>
																</TR>
																<TR>
																	<TD width="3" height="10"></TD>
																	<TD align="right" height="10">
																		<CC1:BotonImagen id="btnImprimir" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="../includes/"
																			ImagesUrl="../imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
																			ImageOver="btnImpr2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen>&nbsp;&nbsp;&nbsp;
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="../includes/"
																			ImagesUrl="../imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																			ImageOver="btnLimp2.gif" ForeColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD width="2" height="10"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3">
											<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="left"></TD>
													<TD align="center" width="50"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
			<!--</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>--></form>
	</BODY>
</HTML>
