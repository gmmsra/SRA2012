<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ListadoInspeccionFenotipica" CodeFile="ListadoInspeccionFenotipica.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Listado de Inspección Fenotípica</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultgrupntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../stylesheet/sra.css">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
	
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="../imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="../imagenes/recsup.jpg"><IMG border="0" src="../imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="../imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="../imagenes/reciz.jpg" width="9"><IMG border="0" src="../imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD width="100%"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion"> Listado de Inspeccion Fenotipica</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD background="../imagenes/formfdofields.jpg">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD background="../imagenes/formfdofields.jpg" width="100%"><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 90%; HEIGHT: 20px" vAlign="middle" background="../imagenes/formfdofields.jpg"
																			colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaReporte" runat="server" cssclass="titulo">Fecha Reporte:&nbsp;</asp:Label></TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="68px" IncludesUrl="Includes/"
																				ImagesUrl="Images/"></cc1:DateBox></TD>
																	</TR> <!--	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Orden del Reporte:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbOrden" class="combo" runat="server" Width="320px" ImagesUrl="../Images/">
																				<asp:ListItem Value="" Selected="True">(Seleccione el orden del listado)</asp:ListItem>
																				<asp:ListItem Value="R">RP</asp:ListItem>
																				<asp:ListItem Value="N">Fecha Nacimiento</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	-->
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.91%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo" Width="58px">Raza/Criador: </asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<UC1:CLIE id="usrCriadorFil" runat="server" AceptaNull="false" MostrarBotones="False" FilCUIT="True"
																				ColCUIT="True" FilTarjNume="False" FilAgru="False" ColCriaNume="True" FilClaveUnica="True"
																				CampoVal="Criador" Criador="True" FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True"
																				Saltos="1,2" Tabla="Criadores"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox id="cmbSexo" class="combo" runat="server" Width="113px" AceptaNull="true">
																				<asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem>
																				<asp:ListItem Value="0">Hembra</asp:ListItem>
																				<asp:ListItem Value="1">Macho</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">R.P. (desde):</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%" background="../imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtRPDde" runat="server" cssclass="textomonto" Width="100px"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblRPHta" runat="server" cssclass="titulo">R.P.(hasta):</asp:Label>&nbsp;
																			<cc1:numberbox id="txtRPHta" runat="server" cssclass="textomonto" Width="100px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblfhnacidesde" runat="server" cssclass="titulo">Fecha Nacimiento desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																			<cc1:DateBox id="dtfhnacidesde" runat="server" cssclass="cuadrotextodeshab" Width="60px" enabled="true"
																				Obligatorio="true"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label style="Z-INDEX: 0" id="lblfhnacihasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox style="Z-INDEX: 0" id="dtfhnacihasta" runat="server" cssclass="cuadrotextodeshab"
																				Width="60px" enabled="true" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
														<TR>
															<TD height="10" background="../imagenes/formfdofields.jpg" colSpan="3"></TD>
														</TR>
														<TR>
															<TD background="../imagenes/formfdofields.jpg" colSpan="3" align="right">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																	ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																	CausesValidation="False"></CC1:BotonImagen>
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																	ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD width="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="../imagenes/recde.jpg" width="13"><IMG border="0" src="../imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="../imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="../imagenes/recinf.jpg"><IMG border="0" src="../imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="../imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
			<asp:textbox id="hdnRptId" runat="server" Visible="False"></asp:textbox></form>
	</BODY>
</HTML>
