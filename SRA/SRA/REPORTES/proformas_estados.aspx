<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Proformas_estados" CodeFile="Proformas_estados.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Proformas pendientes y/o facturadas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0"
		onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTitu" runat="server" width="391px" cssclass="opcion">Proformas pendientes y/o facturadas</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="97%"
										BorderWidth="0" Visible="True">
            <TABLE id=TableFil style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
            align=left border=0>
              <TR>
                <TD style="WIDTH: 100%">
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD style="HEIGHT: 8px" vAlign=middle align=right>
<CC1:BotonImagen id=btnLimpiarFil runat="server" BorderStyle="None" ImageUrl="../imagenes/limpiar.jpg" ForeColor="Transparent" BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD></TR></TABLE></TD></TR>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24><IMG height=25 
                        src="../imagenes/formfle.jpg" width=24 border=0></TD>
                      <TD style="HEIGHT: 8px" width=42><IMG height=25 
                        src="../imagenes/formtxfiltro.jpg" width=113 
border=0></TD>
                      <TD style="HEIGHT: 8px" width=26><IMG height=25 
                        src="../imagenes/formcap.jpg" width=26 border=0></TD>
                      <TD style="HEIGHT: 8px" 
                      background=../imagenes/formfdocap.jpg colSpan=3><IMG 
                        height=25 src="../imagenes/formfdocap.jpg" width=7 
                        border=0></TD></TR></TABLE></TD></TR>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD width=3 background=../imagenes/formiz.jpg><IMG 
                        height=30 src="../imagenes/formiz.jpg" width=3 
                      border=0></TD>
                      <TD><!-- FOMULARIO -->
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" 
                        background=../imagenes/formfdofields.jpg border=0>
                          <TR>
                            <TD style="WIDTH: 2%; HEIGHT: 21px" 
                            background=../imagenes/formfdofields.jpg 
                            height=21></TD>
                            <TD style="WIDTH: 18.48%; HEIGHT: 21px" 
                            vAlign=middle align=right 
                            background=../imagenes/formfdofields.jpg height=21>
<asp:Label id=lblFechaD runat="server" cssclass="titulo">Fecha desde:</asp:Label>&nbsp;</TD>
                            <TD style="WIDTH: 88%; HEIGHT: 21px" vAlign=middle 
                            background=../imagenes/formfdofields.jpg height=21>
<cc1:DateBox id=txtFechaD runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp; 
<asp:Label id=lblFechaH runat="server" cssclass="titulo">Fecha desde:</asp:Label>&nbsp; 
<cc1:DateBox id=txtFechaH runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD></TR>
                          <TR>
                            <TD background=../imagenes/formdivmed.jpg 
                              height=2><IMG height=2 
                              src="../imagenes/formdivmed.jpg" width=1></TD>
                            <TD background=../imagenes/formdivmed.jpg 
                              height=2><IMG height=2 
                              src="../imagenes/formdivmed.jpg" width=1></TD>
                            <TD background=../imagenes/formdivmed.jpg 
                              height=2><IMG height=2 
                              src="../imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD background=../imagenes/formfdofields.jpg></TD>
                            <TD vAlign=middle align=right 
                            background=../imagenes/formfdofields.jpg>
<asp:Label id=lblTipo runat="server" cssclass="titulo">Proformas:&nbsp;</asp:Label></TD>
                            <TD vAlign=middle 
                            background=../imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbTipo runat="server" Width="192px">
																				<asp:ListItem Value="0" Selected="True">Todas</asp:ListItem>
																				<asp:ListItem Value="1">Facturadas</asp:ListItem>
																				<asp:ListItem Value="2">Pendientes</asp:ListItem>
																			</cc1:combobox></TD></TR>
                          <TR>
                            <TD background=../imagenes/formdivfin.jpg 
                              height=2><IMG height=2 
                              src="../imagenes/formdivfin.jpg" width=1></TD>
                            <TD background=../imagenes/formdivfin.jpg 
                              height=2><IMG height=2 
                              src="../imagenes/formdivfin.jpg" width=1></TD>
                            <TD background=../imagenes/formdivfin.jpg 
                              height=2><IMG height=2 
                              src="../imagenes/formdivfin.jpg" 
                          width=1></TD></TR></TABLE></TD>
                      <TD width=2 background=../imagenes/formde.jpg><IMG 
                        height=2 src="../imagenes/formde.jpg" width=2 
                      border=0></TD></TR>
                    <TR>
                      <TD width=3 height=10></TD>
                      <TD height=10></TD>
                      <TD width=2 height=10></TD></TR>
                    <TR>
                      <TD width=3></TD>
                      <TD align=right>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" 
border=0>
                          <TR>
                            <TD align=right>
<CC1:BotonImagen id=btnList runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent" BackColor="Transparent" IncludesUrl="../includes/" ImagesUrl="../imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BotonImagen></TD></TR></TABLE></TD>
                      <TD 
            width=2></TD></TR></TABLE></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
