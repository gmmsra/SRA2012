<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SeguimientoComprobantes" CodeFile="SeguimientoComprobantes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Seguimiento de Comprobantes</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
		function setControls(ComboOrigen)
		{
			//limpiar controles menos combo origen
			var sFiltro = "";
			
			document.all("hdnComboOrigen").value = ComboOrigen;
			if (document.all(ComboOrigen) != null)
			{ 
				if (document.all(ComboOrigen).value == "")
				{
					document.all("cmbTarjetaFil").disabled = false;
					document.all("cmbDineroElect").disabled = false;
					document.all("cmbRetencion").disabled = false;
					document.all("cmbChequeBco").disabled = false;
					document.all("txtcmbChequeBco").disabled = false;
					document.all("cmbBancAcred").disabled = false;
					document.all("txtcmbBancAcred").disabled = false;
					document.all("cmbBillete").disabled = false;				
					
					document.all("txtTarjNro").value = "";
					document.all("txtCupon").value = "";
					document.all("txtCargo").value = "";
					document.all("txtCertif").value = "";
					document.all("txtChequeNro").value = "";
					document.all("txtBoleta").value = "";
					document.all("txtBilleteNro").value = "";
					document.all("txtCertif").value = "";
					
					document.all("txtImpoTarj").value = "";
					document.all("txtImpoRete").value = "";
					document.all("txtImpoCheq").value = "";
					document.all("txtImpoDine").value = "";
					document.all("txtImpoBill").value = "";
					document.all("txtImpoAcre").value = "";

					document.all("txtTarjNro").disabled = false;
					document.all("txtCupon").disabled = false;
					document.all("txtCargo").disabled = false;
					document.all("txtCertif").disabled = false;
					document.all("txtChequeNro").disabled =  false;
					document.all("txtBoleta").disabled = false;
					document.all("txtBilleteNro").disabled = false;	
					document.all("txtAutoriz").disabled = false;

					document.all("txtImpoTarj").disabled = false;
					document.all("txtImpoRete").disabled = false;
					document.all("txtImpoCheq").disabled = false;
					document.all("txtImpoDine").disabled = false;
					document.all("txtImpoBill").disabled = false;
					document.all("txtImpoAcre").disabled = false;
				}
				else
				{
					document.all("cmbTarjetaFil").disabled = !("cmbTarjetaFil" == ComboOrigen);
					document.all("cmbDineroElect").disabled = !("cmbDineroElect" == ComboOrigen);
					document.all("cmbRetencion").disabled = !("cmbRetencion" == ComboOrigen);
					document.all("cmbChequeBco").disabled = !("cmbChequeBco" == ComboOrigen);
					document.all("txtcmbChequeBco").disabled = !("cmbChequeBco" == ComboOrigen);
					document.all("cmbBancAcred").disabled = !("cmbBancAcred" == ComboOrigen);
					document.all("txtcmbBancAcred").disabled = !("cmbBancAcred" == ComboOrigen);
					document.all("cmbBillete").disabled = !("cmbBillete" == ComboOrigen);
					
					document.all("txtTarjNro").disabled = !("cmbTarjetaFil" == ComboOrigen);
					document.all("txtCupon").disabled = !("cmbTarjetaFil" == ComboOrigen);
					document.all("txtCargo").disabled = !("cmbDineroElect" == ComboOrigen);
					document.all("txtCertif").disabled = !("cmbRetencion" == ComboOrigen);
					document.all("txtChequeNro").disabled =  !("cmbChequeBco" == ComboOrigen);
					document.all("txtBoleta").disabled = !("cmbBancAcred" == ComboOrigen);
					document.all("txtBilleteNro").disabled = !("cmbBillete" == ComboOrigen);
					document.all("txtAutoriz").disabled = !("cmbTarjetaFil" == ComboOrigen);

					document.all("txtImpoTarj").disabled = !("cmbTarjetaFil" == ComboOrigen);
					document.all("txtImpoDine").disabled = !("cmbDineroElect" == ComboOrigen);
					document.all("txtImpoRete").disabled = !("cmbRetencion" == ComboOrigen);
					document.all("txtImpoCheq").disabled =  !("cmbChequeBco" == ComboOrigen);
					document.all("txtImpoAcre").disabled = !("cmbBancAcred" == ComboOrigen);
					document.all("txtImpoBill").disabled = !("cmbBillete" == ComboOrigen);
				}
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 24px" vAlign="bottom" colSpan="3" height="24"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Seguimiento de Comprobantes</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3">
											<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg" height="10"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD height="10"><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 719px; HEIGHT: 19px" align="right" background="../imagenes/formfdofields.jpg"
																					colSpan="4"></TD>
																				<TD style="WIDTH: 116px; HEIGHT: 19px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="HEIGHT: 18px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 13.23%" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblTarjetaFil" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 35.13%" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbTarjetaFil" runat="server" Width="40%" onchange="setControls('cmbTarjetaFil');"></cc1:combobox>&nbsp;
																					<asp:Label id="lblTarjNro" runat="server" cssclass="titulo">N�Tarj.:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtTarjNro" runat="server" cssclass="cuadrotexto" Width="30%"></CC1:TEXTBOXTAB>&nbsp;
																				</TD>
																				<TD style="WIDTH: 10.78%" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCodAuto" runat="server" cssclass="titulo">N�Cup�n:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 12.22%" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:TextBoxTab id="txtCupon" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:TextBoxTab></TD>
																				<TD style="WIDTH: 11.15%" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Importe :</asp:Label></TD>
																				<TD style="WIDTH: 20%" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtImpoTarj" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																						EsDecimal="true"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px" align="right" background="../imagenes/formdivmed.jpg" colSpan="4"
																					height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 116px" align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																				<TD align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 127px" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 378px" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 103px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblAuroriz" runat="server" cssclass="titulo">N�Autoriz:</asp:Label>
																				<TD style="WIDTH: 117px" background="../imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtAutoriz" runat="server" cssclass="cuadrotexto" Width="80px"></CC1:TEXTBOXTAB></TD>
																				<TD style="WIDTH: 116px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px" align="right" background="../imagenes/formdivmed.jpg" colSpan="4"
																					height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 116px" align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																				<TD align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 127px; HEIGHT: 21px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblDineroElect" runat="server" cssclass="titulo">Dinero Elect.:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 378px; HEIGHT: 21px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbDineroElect" runat="server" Width="200" onchange="setControls('cmbDineroElect');"></cc1:combobox></TD>
																				<TD style="WIDTH: 103px; HEIGHT: 21px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCargo" runat="server" cssclass="titulo">Cargo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 117px; HEIGHT: 21px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:TextBoxTab id="txtCargo" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:TextBoxTab></TD>
																				<TD style="WIDTH: 116px; HEIGHT: 21px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label4" runat="server" cssclass="titulo">Importe :</asp:Label></TD>
																				<TD style="HEIGHT: 21px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtImpoDine" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																						EsDecimal="true"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px" align="right" background="../imagenes/formdivmed.jpg" colSpan="4"
																					height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 116px" align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																				<TD align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 127px; HEIGHT: 20px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblRentecion" runat="server" cssclass="titulo">Retenci�n:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 378px; HEIGHT: 20px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRetencion" runat="server" Width="80%" onchange="setControls('cmbRetencion');"></cc1:combobox></TD>
																				<TD style="WIDTH: 103px; HEIGHT: 20px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCertif" runat="server" cssclass="titulo">Certif.:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 117px; HEIGHT: 20px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:TextBoxTab id="txtCertif" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:TextBoxTab></TD>
																				<TD style="WIDTH: 116px; HEIGHT: 20px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label5" runat="server" cssclass="titulo">Importe :</asp:Label></TD>
																				<TD style="HEIGHT: 20px" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtImpoRete" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																						EsDecimal="true"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px" align="right" background="../imagenes/formdivmed.jpg" colSpan="4"
																					height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 116px" align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																				<TD align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 127px; HEIGHT: 17px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblChequeBco" runat="server" cssclass="titulo">Cheque Banco:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 378px" background="../imagenes/formfdofields.jpg">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbChequeBco" runat="server" cssclass="cuadrotexto" Width="200"
																									onchange="setControls('cmbChequeBco');" ImagesUrl="../Imagenes/" IncludesUrl="../Includes/"
																									Filtra="True" BusquedaAvanzada="False" MostrarBotones="False"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzadaRepo('bancos','banc_desc','cmbChequeBco','Bancos','');"
																									alt="Busqueda avanzada" src="../imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<TD style="WIDTH: 103px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblChequeNro" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 117px" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<CC1:TextBoxTab id="txtChequeNro" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:TextBoxTab></TD>
																				<TD style="WIDTH: 116px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label6" runat="server" cssclass="titulo">Importe :</asp:Label></TD>
																				<TD vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtImpoCheq" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																						EsDecimal="true"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px" align="right" background="../imagenes/formdivmed.jpg" colSpan="4"
																					height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 116px" align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																				<TD align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 127px; HEIGHT: 17px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblBancAcred" runat="server" cssclass="titulo">Acreditaci�n Banco:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 378px" background="../imagenes/formfdofields.jpg">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbBancAcred" runat="server" cssclass="cuadrotexto" Width="200"
																									onchange="setControls('cmbBancAcred');" ImagesUrl="../Imagenes/" IncludesUrl="../Includes/"
																									Filtra="True" MostrarBotones="False"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzadaRepo('bancos','banc_desc','cmbBancAcred','Bancos','@con_cuentas=1');"
																									alt="Busqueda avanzada" src="../imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<TD style="WIDTH: 103px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblBoleta" runat="server" cssclass="titulo">Boleta:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 117px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:TextBoxTab id="txtBoleta" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:TextBoxTab></TD>
																				<TD style="WIDTH: 116px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label7" runat="server" cssclass="titulo">Importe :</asp:Label></TD>
																				<TD vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtImpoAcre" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																						EsDecimal="true"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px" align="right" background="../imagenes/formdivmed.jpg" colSpan="4"
																					height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 116px" align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																				<TD align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 127px; HEIGHT: 23px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblBillete" runat="server" cssclass="titulo">Billete:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 378px; HEIGHT: 23px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbBillete" runat="server" Width="248px" onchange="setControls('cmbBillete');">
																						<asp:ListItem Selected="True" value="">(Ninguno)</asp:ListItem>
																						<asp:ListItem Value="1">Pesos</asp:ListItem>
																						<asp:ListItem Value="2">Dolares</asp:ListItem>
																					</cc1:combobox></TD>
																				<TD style="WIDTH: 103px; HEIGHT: 23px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblBilleteNro" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 117px; HEIGHT: 23px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:TextBoxTab id="txtBilleteNro" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"></CC1:TextBoxTab></TD>
																				<TD style="WIDTH: 116px; HEIGHT: 23px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="Label8" runat="server" cssclass="titulo">Importe :</asp:Label></TD>
																				<TD style="HEIGHT: 23px" vAlign="top" background="../imagenes/formfdofields.jpg">
																					<CC1:NUMBERBOX id="txtImpoBill" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																						EsDecimal="true"></CC1:NUMBERBOX></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px" align="right" background="../imagenes/formdivmed.jpg" colSpan="4"
																					height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 116px" align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																				<TD align="right" background="../imagenes/formdivmed.jpg" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 127px; HEIGHT: 17px" vAlign="middle" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 594px" background="../imagenes/formfdofields.jpg" colSpan="3">
																					<asp:checkbox id="chkDetalle" Text="Detallado" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																				<TD style="WIDTH: 116px" background="../imagenes/formfdofields.jpg"></TD>
																				<TD background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 719px; HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"
																					colSpan="4"></TD>
																				<TD style="WIDTH: 116px; HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg" height="10"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3" height="10"></TD>
																	<TD align="right" height="10"></TD>
																	<TD width="2" height="10"></TD>
																</TR>
																<TR>
																	<TD width="3" height="10"></TD>
																	<TD align="right" height="10">
																		<CC1:BotonImagen id="btnImprimir" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			BackColor="Transparent" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
																			ImageOver="btnImpr2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD width="2" height="10"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnComboOrigen" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all("hdnComboOrigen").value != "")	
			setControls(document.all("hdnComboOrigen").value);
		</SCRIPT>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
