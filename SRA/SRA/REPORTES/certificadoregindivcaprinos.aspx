<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CertificadoRegIndivCaprinos" CodeFile="CertificadoRegIndivCaprinos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Certificado de Registros Genealógicos Caprinos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Certificado de Registro Genealógico Caprinos</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!-- FOMULARIO -->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="100%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0" DESIGNTIMEDRAGDROP="55"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR> <!--
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCriaNumeDesde" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="300px"
																						filtra="true" NomOper="razas_cargar" MostrarBotones="False"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR> -->
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCriaNumeHasta" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbSexoFil" runat="server" Width="113px" Obligatorio="true">
																						<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																						<asp:ListItem Value="false">Hembra</asp:ListItem>
																						<asp:ListItem Value="true">Macho</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblSraNumeDesde" runat="server" cssclass="titulo">RIL Desde:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtSraNumeDesde" runat="server" cssclass="cuadrotexto" Width="112px" obligatorio="True"
																						AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblSraNumeHasta" runat="server" cssclass="titulo">RIL Hasta:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<CC1:TEXTBOXTAB id="txtSraNumeHasta" runat="server" cssclass="cuadrotexto" Width="112px" obligatorio="True"
																						AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.91%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo" Width="58px">Raza/Criador: </asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriadorFil" runat="server" AceptaNull="false" FilCUIT="True" ColCUIT="True"
																						FilTarjNume="False" FilAgru="False" ColCriaNume="True" FilClaveUnica="True" CampoVal="Criador"
																						Criador="True" FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True" Saltos="1,2"
																						Tabla="Criadores" MostrarBotones="False"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 211px" align="right" width="211" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg">
																					<asp:RadioButton id="rbtnFechaAprobacion" runat="server" cssclass="titulo" Width="144px" Text="Fecha Aprobación"
																						GroupName="RadioGroup1" checked="True"></asp:RadioButton>
																					<asp:RadioButton id="rbtnFechaInscripcion" runat="server" cssclass="titulo" Width="149px" Text="Fecha Inscripción"
																						GroupName="RadioGroup1" checked="False"></asp:RadioButton>&nbsp;&nbsp;
																					<asp:Label id="lblDesde" runat="server" cssclass="titulo">Desde:</asp:Label>
																					<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																						enabled="true"></cc1:DateBox>
																					<asp:Label id="lblHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																					<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotextodeshab" Width="60px" Obligatorio="true"
																						enabled="true"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2.02%; HEIGHT: 24px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFiltro" runat="server" cssclass="titulo">Filtro:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" align="left" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbCertifTipoFil" runat="server" Width="113px" CampoVal="Sexo">
																						<asp:ListItem Selected="True">(Todos)</asp:ListItem>
																						<asp:ListItem Value="false">Pendientes</asp:ListItem>
																						<asp:ListItem Value="true">Originales</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																			ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"
																			CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro ---></TBODY></TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox>&nbsp;</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
	</BODY>
</HTML>
