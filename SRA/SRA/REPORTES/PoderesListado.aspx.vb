Namespace SRA

Partial Class PoderesListado
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsamFil, "", "")
      Me.cmbAsamFil.Items.Add("General")
      Me.cmbAsamFil.Items(Me.cmbAsamFil.Items.Count - 1).Selected = True
   End Sub

   Private Sub mLimpiarFiltros()
      Me.usrSocFil.Limpiar()
      Me.usrSocFilApoder.Limpiar()
   End Sub
#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Dim listaEnt As String
      Try

         Dim lstrRptName As String
         lstrRptName = "ListadoPoderes"

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         lstrRpt += "&AsamId=" + IIf(cmbAsamFil.Valor.ToString = "General", "0", cmbAsamFil.Valor.ToString)
         lstrRpt += "&SocioPoder=" + IIf(Me.usrSocFil.Valor.ToString = "", "0", Me.usrSocFil.Valor.ToString)
         lstrRpt += "&SocioApoder=" + IIf(Me.usrSocFilApoder.Valor.ToString = "", "0", Me.usrSocFilApoder.Valor.ToString)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


End Class
End Namespace
