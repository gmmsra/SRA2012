Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports AccesoBD
Imports Interfaces.Importacion


Namespace SRA



Partial Class CriadoresEtiquetas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public mstrConn As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GSZ 12-11-2014 
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)


            If Not Page.IsPostBack Then
                'Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()

      
    End Sub
    Private Sub mValidarDatos()

       
       
        If usrCriadorFil.Valor.ToString() = "" Then
            Throw New AccesoBD.clsErrNeg("El Criador debe ser distinto de  vacio")
        End If

        If usrCriadorFil.RazaId = "" Then
            Throw New AccesoBD.clsErrNeg("La Raza debe ser distinto de  vacio")
        End If

        If cmbSexoFil.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe elegir el sexo")
        End If

    End Sub
    Private Sub mListar()
        Try

            Dim params As String
            Dim lstrRptName As String = "EtiquetasCriadores"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            Dim lintFechaD, lintFechaH As Integer

            Dim stRazaDescripcion As String
            mValidarDatos()



            params += "&hba_desde=" & IIf(txtSraNumeDesde.Valor.ToString = "", "0", _
                                        txtSraNumeDesde.Text)

            params += "&hba_hasta=" & IIf(txtSraNumeHasta.Valor.ToString = "", "9999999", _
                                           txtSraNumeHasta.Text)

            'GSZ 26/02/2015 se agrego fecha de inscripcion cpn  -2 por error en FromOADate

            If (txtInscripFechaDesde.Fecha.ToString <> "") Then
                lintFechaD = CType(clsFormatear.gFormatFechaString(txtInscripFechaDesde.Text, "Int32"), Integer) - 2
            Else
                lintFechaD = 0

            End If

            If (txtInscripFechaHasta.Fecha.ToString <> "") Then
                lintFechaH = CType(clsFormatear.gFormatFechaString(txtInscripFechaHasta.Text, "Int32"), Integer) - 2
            Else
                lintFechaH = 0

            End If



            params += "&dInscrDesde=" + lintFechaD.ToString
            params += "&dInscrHasta=" + lintFechaH.ToString

            params += "&criador_id=" & IIf(usrCriadorFil.Valor = 0, "0", _
                                                      usrCriadorFil.Valor.ToString())


            params += "&raza_id=" & IIf(usrCriadorFil.RazaId.ToString = "", "0", _
                                    usrCriadorFil.RazaId.ToString)


            params += "&sexo=" & IIf(cmbSexoFil.Valor.ToString = "false", "0", "1")

            lstrRpt += params

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        mListar()
    End Sub


    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click

        cmbSexoFil.Limpiar()
        usrCriadorFil.Limpiar()
        txtSraNumeDesde.Text = ""
        txtSraNumeHasta.Text = ""
     

    End Sub
End Class

End Namespace
