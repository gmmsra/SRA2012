<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="../controles/usrProducto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CertifRegGenealo" CodeFile="CertifRegGenealo.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Certificado Exportaci�n</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../stylesheet/SRA.css">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="../imagenes/recsup.jpg"><IMG border="0" src="../imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="../imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="../imagenes/reciz.jpg" width="9"><IMG border="0" src="../imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Certificado Exportaci�n</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!-- FOMULARIO -->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblNroTramite" runat="server" cssclass="titulo">Nro. Tr�mite:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																					<CC1:TEXTBOXTAB id="txtNroTramite" runat="server" cssclass="cuadrotexto" Width="112px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblProductoFil" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																					<UC1:PROH id="usrProductoFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																						FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" AutoPostBack="False"
																						Ancho="800"></UC1:PROH></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblApedido" runat="server" cssclass="titulo">A Pedido:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																					<CC1:TEXTBOXTAB id="txtApedido" runat="server" cssclass="cuadrotexto" Width="312px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblParaPresentar" runat="server" cssclass="titulo">Para Presentar a:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																					<CC1:TEXTBOXTAB id="txtParaPresentar" runat="server" cssclass="cuadrotexto" Width="312px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 8.28%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblPais" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 25%; HEIGHT: 24px" background="../imagenes/formfdofields.jpg" align="left">
																					<CC1:TEXTBOXTAB id="txtPais" runat="server" cssclass="cuadrotexto" Width="312px" AceptaNull="False"
																						obligatorio="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																			ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"
																			CausesValidation="False"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro ---></TBODY></TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="../imagenes/recde.jpg" width="13"><IMG border="0" src="../imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="../imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="../imagenes/recinf.jpg"><IMG border="0" src="../imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="../imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
	</BODY>
</HTML>
