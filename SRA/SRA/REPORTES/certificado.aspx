<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Certificado" CodeFile="Certificado.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Certificaciones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
				
		function txtAnio_Change(pAnio, pInse)
		{
		    var sFiltro = pInse + "," ;
		    
		    if (pAnio.value=="")
		       sFiltro =  sFiltro + "-1";
		      else 
		       sFiltro = sFiltro + pAnio.value ;
		       
		    LoadComboXML("ciclos_cargar", sFiltro, "cmbCiclo", "S");
		    mLimpiarComboMaterias();
		    
		    
		}
		
		function txtAnioFil_Change(pAnio, pInse)
		{
		    var sFiltro = pInse + "," ;
		    
		    if (pAnio.value=="")
		       sFiltro =  sFiltro + "-1";
		      else 
		       sFiltro = sFiltro + pAnio.value ;
		       
		    LoadComboXML("ciclos_cargar", sFiltro, "cmbCicloFil", "S");
		  
		    
		    
		}
		
		function mCargarMaterias()
		{
		  var sFiltro = "@mate_curs_inte = 1 , @cicl_id =" ;
		    
		    if (document.all["cmbCiclo"].value=="")
		       sFiltro =  sFiltro + "-1";
		      else 
		       sFiltro = sFiltro + document.all["cmbCiclo"].value ;
		       
		    LoadComboXML("ciclos_materias_cargar", sFiltro, "cmbMate", "S");
		}
	
		function mLimpiarComboMaterias()
		{
		 var sFiltro = "@mate_curs_inte = 1 , @cicl_id = -1";
		 LoadComboXML("ciclos_materias_cargar", sFiltro, "cmbMate", "S");
		 document.all["rbtnCarr"].checked=1;
		 		
		}
		
		
		
		function usrAlumFil_onchange()
		{
			var sFiltro=document.all["usrAlum:txtId"].value;
			var lstrRet = "";
			if (sFiltro!="")
			{
				lstrRet = LeerCamposXML("alumnos", sFiltro, "alum_fact_clie_id");
			}
			document.all["usrFactClie:txtCodi"].value=lstrRet;
			document.all["usrFactClie:txtCodi"].onchange();
		}
		
		
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 14px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Certificaciones</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 14px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
												Width="97%" Visible="True">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="2"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="../imagenes/formiz.jpg" height="10"><IMG height="30" src="../imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD height="10"><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 1.81%" align="right" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 12.69%" align="right" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 88%" background="../imagenes/formdivmed.jpg" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="60px" MaxLength="4"
																						MaxValor="2099" AceptaNull="False"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="../imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Ciclo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbCicloFil" runat="server" Width="100%" AceptaNull="false" NomOper="ciclos_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 1.81%; HEIGHT: 25px" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 12.69%; HEIGHT: 25px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">&nbsp;<BR>
																					<asp:Label id="lboAlumFil" runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;
																				</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 25px" vAlign="bottom" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrAlumFil" runat="server" AceptaNull="false" Obligatorio="true" FilCUIT="True"
																						FilDocuNume="True" FilLegaNume="True" Saltos="1,1,1" Tabla="Alumnos" SoloBusq="true"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 1.81%; HEIGHT: 14px" background="../imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 12.69%; HEIGHT: 25px" vAlign="top" align="right" background="../imagenes/formfdofields.jpg">&nbsp;<BR>
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Certif.Tipo:</asp:Label>&nbsp;
																				</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 25px" vAlign="bottom" background="../imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbCertifTipoFil" runat="server" Width="113px" AceptaNull="True"
																						onchange="mSelecSexo(this,'lblDonaFil');">
																						<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																						<asp:ListItem Value="0">Aprobaci�n</asp:ListItem>
																						<asp:ListItem Value="1">Asistencia</asp:ListItem>
																					</cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 1.81%" background="../imagenes/formfdofields.jpg" height="10"></TD>
																				<TD style="WIDTH: 12.69%" vAlign="top" align="right" background="../imagenes/formfdofields.jpg"
																					height="10"></TD>
																				<TD style="WIDTH: 88%" vAlign="bottom" background="../imagenes/formfdofields.jpg" height="10"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 1.81%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 12.69%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="../imagenes/formde.jpg" height="10"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD width="3" height="10"></TD>
																	<TD align="right" height="10"></TD>
																	<TD width="2" height="10"></TD>
																</TR>
																<TR>
																	<TD width="3" height="10"></TD>
																	<TD align="right" height="10">
																		<CC1:BotonImagen id="btnImprimir" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
																	<TD width="2" height="10"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3">
											<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="left"></TD>
													<TD align="center" width="50"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
						</td>
					</tr>
				</TBODY>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
			</TR>
			<tr>
				<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
				<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
				<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		function usrAlumFil_onchange()
		{
			var sDato = "-1";
			var sFiltro="@alum_id = ";
			if(document.all["usrAlumFil:txtId"].value != "") 
			{sDato = document.all["usrAlumFil:txtId"].value;}
			sFiltro = sFiltro + sDato + ",@alum_inse_id = " + document.all["lblInstituto"].value;			
			LoadComboXML("alumnos_carreras_cargar", sFiltro, "cmbCarre", "S");		   			    			
		}
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
