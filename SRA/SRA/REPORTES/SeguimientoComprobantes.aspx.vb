Imports System.Data.SqlClient


Namespace SRA


Partial Class SeguimientoComprobantes
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Textboxtab2 As NixorControls.TextBoxTab

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"

   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      Dim lstrFiltro As String = "@banc_id=" & clsSQLServer.gFormatArg(cmbChequeBco.Valor.ToString, SqlDbType.Int)
      Dim lstrFiltro2 As String = "@banc_id=" & clsSQLServer.gFormatArg(cmbBancAcred.Valor.ToString, SqlDbType.Int)

      clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjetaFil, "N")
      clsWeb.gCargarRefeCmb(mstrConn, "dinero_elec", cmbDineroElect, "N")
      clsWeb.gCargarRefeCmb(mstrConn, "retenciones_tipos", cmbRetencion, "N")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbChequeBco, "N")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancAcred, "N", "@con_cuentas=1")
        'clsWeb.gCargarRefeCmb(mstrConn, "monedas_sinDefa", cmbBillete, "N")

   End Sub
#End Region

   Private Sub mImprimirConcepto()
      Try
         Dim params As String
         Dim lstrRptName As String

            If chkDetalle.Checked.ToString = "False" Then
                lstrRptName = "SeguimientoComprobantesResu"
            Else
                lstrRptName = "SeguimientoComprobantes_Deta"
            End If
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If cmbTarjetaFil.Valor.ToString <> "" Then
                params += "&paco_pati_id=" + CType(SRA_Neg.Constantes.PagosTipos.Tarjeta, String)
                params += "&paco_tarj_id=" + IIf(cmbTarjetaFil.Valor.ToString = "", "null", cmbTarjetaFil.Valor.ToString)
                params += "&paco_nume=" + IIf(txtCupon.Valor.ToString = "", "0", txtCupon.Valor.ToString)
                params += "&descrip=" + cmbTarjetaFil.SelectedItem.ToString
                params += "&paco_tarj_autori=" + IIf(txtAutoriz.Valor.ToString = "", "0", txtAutoriz.Valor.ToString)
                params += "&paco_impo=" + Replace(txtImpoTarj.Valor.ToString, ".", ",")
            End If
            If cmbDineroElect.Valor.ToString <> "" Then
                params += "&paco_pati_id=" + CType(SRA_Neg.Constantes.PagosTipos.DineroElec, String)
                params += "&paco_dine_id=" + IIf(cmbDineroElect.Valor.ToString = "", "null", cmbDineroElect.Valor.ToString)
                params += "&paco_nume=" + IIf(txtCargo.Valor.ToString = "", "0", txtCargo.Valor.ToString)
                params += "&descrip=" + cmbDineroElect.SelectedItem.ToString
                params += "&paco_impo=" + Replace(txtImpoDine.Valor.ToString, ".", ",")
            End If
            If cmbRetencion.Valor.ToString <> "" Then
                params += "&paco_pati_id=" + CType(SRA_Neg.Constantes.PagosTipos.Retenciones, String)
                params += "&paco_rtip_id=" + IIf(cmbRetencion.Valor.ToString = "", "null", cmbRetencion.Valor.ToString)
                params += "&paco_nume=" + IIf(txtCertif.Valor.ToString = "", "0", txtCertif.Valor.ToString)
                params += "&descrip=" + cmbRetencion.SelectedItem.ToString
                params += "&paco_impo=" + Replace(txtImpoRete.Valor.ToString, ".", ",")
            End If
            If cmbChequeBco.Valor.ToString <> "" Then
                params += "&paco_pati_id=" + CType(SRA_Neg.Constantes.PagosTipos.Cheque, String)
                params += "&paco_orig_banc_id=" + IIf(cmbChequeBco.Valor.ToString = "", "null", cmbChequeBco.Valor.ToString)
                params += "&paco_nume=" + IIf(txtChequeNro.Valor.ToString = "", "0", txtChequeNro.Valor.ToString)
                params += "&descrip=" + cmbChequeBco.SelectedItem.ToString
                params += "&paco_impo=" + Replace(txtImpoCheq.Valor.ToString, ".", ",")
            End If
            If cmbBancAcred.Valor.ToString <> "" Then
                params += "&paco_pati_id=" + CType(SRA_Neg.Constantes.PagosTipos.AcredBancaria, String)
                params += "&paco_orig_banc_id=" + IIf(cmbBancAcred.Valor.ToString = "", "null", cmbBancAcred.Valor.ToString)
                params += "&paco_nume=" + IIf(txtBoleta.Valor.ToString = "", "0", txtBoleta.Valor.ToString)
                params += "&descrip=" + cmbBancAcred.SelectedItem.ToString
                params += "&paco_impo=" + Replace(txtImpoAcre.Valor.ToString, ".", ",")
            End If
            If cmbBillete.Valor.ToString <> "" Then
                params += "&paco_pati_id=" + CType(SRA_Neg.Constantes.PagosTipos.Efectivo, String)
                params += "&paco_mone_id=" + IIf(cmbBillete.Valor.ToString = "", "null", cmbBillete.Valor.ToString)
                params += "&bill_nume=" + IIf(txtBilleteNro.Valor.ToString = "", "0", txtBilleteNro.Valor.ToString)
                params += "&descrip=" + cmbBillete.SelectedItem.ToString
                params += "&paco_impo=" + Replace(txtImpoBill.Valor.ToString, ".", ",")
            End If

            params += "&detalle=" + chkDetalle.Checked.ToString
            params += "&paco_tarj_nume=" + IIf(txtTarjNro.Valor.ToString = "", "0", txtTarjNro.Valor.ToString)

            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         mValidarDatos()
         mImprimirConcepto()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()

        If cmbBancAcred.Valor.ToString <> "" And txtBoleta.Text = "" And txtImpoAcre.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero de Boleta o un Importe")
        End If
        If cmbBillete.Valor.ToString <> "" And txtBilleteNro.Text = "" And txtImpoBill.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero de Billete o un Importe")
        End If
        If cmbChequeBco.Valor.ToString <> "" And txtChequeNro.Text = "" And txtImpoCheq.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero de Cheque o un Importe")
        End If
        If cmbDineroElect.Valor.ToString <> "" And txtCargo.Text = "" And txtImpoDine.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero de Cargo o un Importe")
        End If
        If cmbRetencion.Valor.ToString <> "" And txtCertif.Text = "" And txtImpoRete.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero de Certificado o un Importe")
        End If
        If cmbTarjetaFil.Valor.ToString <> "" And txtTarjNro.Text = "" And txtCupon.Text = "" And txtAutoriz.Text = "" And txtImpoTarj.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero de Tarjeta, Autorizaci�n, Cup�n o un Importe")
        End If

        If cmbBancAcred.Valor.ToString = "" And cmbBillete.Valor.ToString = "" And cmbChequeBco.Valor.ToString = "" And cmbDineroElect.Valor.ToString = "" And cmbRetencion.Valor.ToString = "" And cmbTarjetaFil.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar alg�n valor para poder listar")
        End If

   End Sub


End Class

End Namespace
