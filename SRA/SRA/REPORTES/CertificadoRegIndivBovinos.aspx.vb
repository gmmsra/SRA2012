Imports System.Data.SqlClient
Imports AccesoBD
Imports ReglasValida.Validaciones
Imports Interfaces.Importacion


Namespace SRA





Partial Class CertificadoRegIndivBovinos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtCriaNumeDesde As NixorControls.TextBoxTab
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                mCargarCombos()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        'GSZ clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=3", cmbRazaFil, "id", "descrip", "S")
        ' GSZ SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
        usrCriadorFil.FiltroRazas = "@raza_espe_id=3"
    End Sub
    Private Sub mImprimir()
        Try
            Dim params As String
            Dim lstrRptName As String = "CertificadoRegIndivBovinos"



            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If usrCriadorFil.RazaId.ToString() = "" And usrCriadorFil.cmbCriaRazaExt.SelectedValue = "" Then
                clsError.gGenerarMensajes(Me, "La raza debe ser de la  la Especie de Bovinos.")
                Return
            End If



            params += "&hba_desde=" & IIf(txtSraNumeDesde.Valor.ToString = "", "0", _
                                       txtSraNumeDesde.Text)

            params += "&hba_hasta=" & IIf(txtSraNumeHasta.Valor.ToString = "", "999999999", _
                                           txtSraNumeHasta.Text)
            If cmbSexoFil.Valor.ToString <> "" Then
                params += "&sexo=" + cmbSexoFil.Valor.ToString
            End If
            params += "&raza_id=" & IIf(usrCriadorFil.RazaId.ToString = "", "0", _
                                    usrCriadorFil.RazaId.ToString)

            params += "&criador_id=" & IIf(usrCriadorFil.Valor = 0, "0", _
                                                usrCriadorFil.Valor.ToString())

            params += "&FilFechaInscripcion=" & IIf(rbtnFechaInscripcion.Checked, True, False)

            params += "&FechaDesde=" & IIf(txtFechaDesde.Fecha.ToString = "", "0", _
                    mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaDesde.Text))))


            params += "&FechaHasta=" & IIf(txtFechaHasta.Fecha.ToString = "", "0", _
            mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaHasta.Text))))

            params += "&tipoCertificado=" & Me.cmbCertifTipoFil.SelectedIndex

            params += "&audi_user=" & Session("sUserId").ToString()
            lstrRpt += params

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt, True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Eventos de Controles"
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            mImprimir()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        cmbCertifTipoFil.Limpiar()
        cmbSexoFil.Limpiar()
        usrCriadorFil.Limpiar()
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""
        txtSraNumeDesde.Text = ""
        txtSraNumeHasta.Text = ""

    End Sub
End Class
End Namespace
