<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="../controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DeudaVencidaCliente" CodeFile="DeudaVencidaCliente.aspx.vb" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DeudaVencidaCliente</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
	</HEAD>
	<body class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" onunload="gCerrarVentanas();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 8px; HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25">
											<asp:label id="lblTituAbm" runat="server" cssclass="opcion">Deuda Vencida</asp:label>
										</TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD style="WIDTH: 8px"></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="97%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" vAlign="middle" align="right">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																			ImageUrl="../imagenes/limpiar.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"
																			BorderStyle="None"></CC1:BotonImagen></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="WIDTH: 21.97%; HEIGHT: 26px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblActividad" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 72%; HEIGHT: 26px" background="../imagenes/formfdofields.jpg" colSpan="2">
																					<cc1:combobox id="cmbActividad" class="combo" runat="server" nomoper="actividades_cargar" Width="328px"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 21.97%; HEIGHT: 26px" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																				<TD height="21" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="False" Saltos="1,1,1" SoloBusq="true"
																						CampoVal="Cliente" PermiModi="True" FilSociNume="True" FilCUIT="True" FilDocuNume="True"
																						Tabla="Clientes" width="100%"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 11.72%" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblFecha" runat="server" cssclass="titulo" Width="87px">Fecha:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 100%; HEIGHT: 30px" background="../imagenes/formfdofields.jpg" colSpan="2">
																					<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="68px" obligatorio="true" ></cc1:DateBox>&nbsp;&nbsp;
																				</TD>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 11.72%" background="../imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblResumido" runat="server" cssclass="titulo" Width="87px">Resumido:</asp:Label>&nbsp;</TD>
																				<TD height="21" vAlign="middle" background="../imagenes/formfdofields.jpg">
																					<asp:checkbox id="chkResumido" Text="" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																			</TR>
																		</TABLE>
																	</TD> <!--FIN FORMULARIO-->
																	<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
																<TR>
																	<TD width="3"></TD>
																	<TD align="right">
																		<CC1:BotonImagen id="btnList" runat="server" IncludesUrl="../includes/" ImagesUrl="../imagenes/"
																			ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"
																			BorderStyle="None"></CC1:BotonImagen></TD>
																	<TD width="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
	</body>
</HTML>
