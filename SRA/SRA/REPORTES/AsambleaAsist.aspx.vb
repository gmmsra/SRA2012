Namespace SRA

Partial Class AsambleaAsist
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = "Titulos"

   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mCargarCombos()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsamFil, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCateFil, "T")
   End Sub
#End Region

#Region "Detalle"

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "AsambleasAsist"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If cmbAsamFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la asamblea")
         End If

         lstrRpt += "&asam_id=" + cmbAsamFil.Valor.ToString
         lstrRpt += "&cate_id=" + cmbCateFil.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class

End Namespace
