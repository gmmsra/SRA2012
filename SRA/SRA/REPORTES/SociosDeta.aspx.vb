Imports System.Data.SqlClient


Namespace SRA


Partial Class SociosDeta
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "inscripciones"

   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mEstablecerPerfil()
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      Select Case Request.QueryString("t")
         Case "C"
            clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCateFil, "T")
         Case "D"
            clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDistFil, "T")
         Case "E"
            clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEntiFil, "T")
         Case "I"
            clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInstFil, "T")
      End Select
   End Sub

   Private Sub mLimpiarFiltros()
        cmbCateFil.Limpiar()
        cmbDistFil.Limpiar()
        cmbEntiFil.Limpiar()
        cmbInstFil.Limpiar()
        usrSociFil.Valor = 0
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      hdnTipo.Text = Request.QueryString("t")
      Select Case Request.QueryString("t")
         Case "C"
            lblTituAbm.Text = "Socios por Categor�a"
            lblCateFil.Visible = True
            cmbCateFil.Visible = True
         Case "D"
            lblTituAbm.Text = "Socios por Distrito"
            lblDistFil.Visible = True
            cmbDistFil.Visible = True
         Case "E"
            lblTituAbm.Text = "Socios por Entidad Rural"
            lblEntiFil.Visible = True
            cmbEntiFil.Visible = True
         Case "I"
            lblTituAbm.Text = "Socios por Instituci�n"
            lblInstFil.Visible = True
            cmbInstFil.Visible = True
      End Select
   End Sub
#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String

         Select Case Request.QueryString("t")
            Case "C"
               lstrRptName = "Socios_x_Categoria"
            Case "D"
               lstrRptName = "Socios_x_Distrito"
            Case "E"
               lstrRptName = "Socios_x_Entidad"
            Case "I"
               lstrRptName = "Socios_x_Institucion"
         End Select

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&soci_id=" + usrSociFil.Valor.ToString
         lstrRpt += "&cate_id=" + cmbCateFil.Valor.ToString
         lstrRpt += "&dist_id=" + cmbDistFil.Valor.ToString
         lstrRpt += "&enti_id=" + cmbEntiFil.Valor.ToString
         lstrRpt += "&inst_id=" + cmbInstFil.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
