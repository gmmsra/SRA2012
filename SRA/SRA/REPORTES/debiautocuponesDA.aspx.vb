Namespace SRA

    Partial Class DebiAutoCuponesDA
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definición de Variables"
        Private mstrConn As String
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_DebitosCabe

        Private Enum ColumnasFil As Integer
            lnkEdit = 0
            comp_id = 1
            fecha = 2
            tico_desc = 3
            comp_nro = 4
            cliente = 5
        End Enum
#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)

                If Not Page.IsPostBack Then
                    mEstablecerPerfil()
                    mInicializar()
                    mCargarCombos()
                    'mConsultarBusc()

                    clsWeb.gInicializarControles(sender, mstrConn)
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mEstablecerPerfil()
        End Sub

        Private Sub mCargarCombos()
            clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjFil, "S")
            'clsWeb.gCargarRefeCmb(mstrConn, "textos_comunicados", cmbTexto, "S")
        End Sub

        Public Sub mInicializar()
            Dim lstrParaPageSize As String
            clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
            grdDatoBusq.PageSize = Convert.ToInt32(lstrParaPageSize)
        End Sub
#End Region

        Private Sub btnBuscar_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
            Try
                grdDatoBusq.CurrentPageIndex = 0
                mConsultarBusc()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

#Region "Operaciones sobre el DataGrid"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDatoBusq.CurrentPageIndex = E.NewPageIndex
                mConsultarBusc()

            Catch ex As Exception
                clsError.gManejarError(Me.Page, ex)
            End Try
        End Sub

        Public Sub mConsultarBusc()
            Dim lstrCmd As New StringBuilder

            lstrCmd.Append("exec sp_ConsRecibimosSocios")
            lstrCmd.Append(" @deca_tarj_id=" + cmbTarjFil.Valor.ToString)
            lstrCmd.Append(",@fecha_desde=" + clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime))
            lstrCmd.Append(",@fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime))
            lstrCmd.Append(",@soci_desde=" + txtSociDesde.Valor.ToString)
            lstrCmd.Append(",@soci_hasta=" + txtSociHasta.Valor.ToString)

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDatoBusq)
        End Sub
#End Region

        Private Sub mListar()
            Try

                Dim lstrRptName As String = "Recibo"
                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                Dim lintFechaDesde As Integer
                Dim lintFechaHasta As Integer

                If txtFechaDesdeFil.Fecha.ToString = "" Then
                    lintFechaDesde = 0
                Else
                    lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
                End If

                If txtFechaHastaFil.Fecha.ToString = "" Then
                    lintFechaHasta = 0
                Else
                    lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
                End If

                Dim lstrId As String = String.Empty
                For Each lItem As WebControls.DataGridItem In grdDatoBusq.Items
                    If DirectCast(lItem.FindControl("chkSel"), CheckBox).Checked Then
                        lstrId = lItem.Cells(ColumnasFil.comp_id).Text
                        Exit For
                    End If
                Next

                lstrRpt += "&comp_id=" + lstrId
                'lstrRpt += "&deca_tarj_id=" + cmbTarjFil.Valor.ToString
                'lstrRpt += "&fecha_desde=" + lintFechaDesde.ToString
                'lstrRpt += "&fecha_hasta=" + lintFechaHasta.ToString
                'lstrRpt += "&soci_desde=" + txtSociDesde.Valor.ToString
                'lstrRpt += "&soci_hasta=" + txtSociHasta.Valor.ToString

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                Response.Redirect(lstrRpt)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
            mListar()
        End Sub
    End Class

End Namespace
