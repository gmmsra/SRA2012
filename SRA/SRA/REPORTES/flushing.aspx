<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="../controles/usrProducto.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Flushing" enableViewState="True" CodeFile="Flushing.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reportes Estadísticos</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/valcambios.js"></script>
		<script language="JavaScript" src="../includes/valnumber.js"></script>
		<script language="JavaScript">
			function usrPadre_usrCriadorFil_cmbRazaCria_onchange() {
				
			document.all("usrMadre_usrCriadorFil_cmbRazaCria").value= document.all('usrPadre_usrCriadorFil_cmbRazaCria').value;
				document.all("txtusrMadre:usrCriadorFil:cmbRazaCria").value= document.all("txtusrPadre:usrCriadorFil:cmbRazaCria").value;
				document.all('usrMadre_usrCriadorFil_cmbRazaCria').selectedIndex = document.all('usrPadre_usrCriadorFil_cmbRazaCria').selectedIndex;	
			
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Flushing</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"></TD>
																	<TD style="WIDTH: 181px; HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" vAlign="bottom"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="../imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="40"><IMG border="0" src="../imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="WIDTH: 6px; HEIGHT: 8px" width="6"><IMG border="0" src="../imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="../imagenes/formfdocap.jpg" width="7" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="../imagenes/formiz.jpg" width="3"><IMG border="0" src="../imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE class="FdoFld" border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" align="right">
																					<asp:Label id="lblPadre" runat="server" cssclass="titulo">Padre:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%">
																					<UC1:PROH id="usrPadre" runat="server" MuestraDesc="False" Tabla="productos" Saltos="1,2"
																						Ancho="800" AutoPostBack="False" FilSociNume="True" FilTipo="S" FilDocuNume="True" AceptaNull="false"></UC1:PROH></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" align="right">
																					<asp:Label id="lblMadre" runat="server" cssclass="titulo">Madre:&nbsp;</asp:Label></TD>
																				<TD style="WIDTH: 75%">
																					<UC1:PROH id="usrMadre" runat="server" MuestraDesc="False" Tabla="productos" Saltos="1,2"
																						Ancho="800" AutoPostBack="False" FilSociNume="True" FilTipo="S" FilDocuNume="True" AceptaNull="false"></UC1:PROH></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="../imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="../imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right">
																					<asp:label id="lblRecuFecha" runat="server" cssclass="titulo">Fecha Recuperación:&nbsp;</asp:label></TD>
																				<TD>
																					<cc1:DateBox id="txtRecuFecha" runat="server" cssclass="cuadrotexto" Width="70px" ImagesUrl="Images/"
																						IncludesUrl="Includes/"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 17px" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE> <!--FIN FORMULARIO--></TD>
																	<TD background="../imagenes/formde.jpg" width="2"><IMG border="0" src="../imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
																<TR>
																	<TD colSpan="3" align="right">
																		<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen>
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImagesUrl="../imagenes/" IncludesUrl="../includes/"
																			ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" BackColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0">
						</td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
