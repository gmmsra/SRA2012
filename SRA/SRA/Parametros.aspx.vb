' Dario 2013-08-16 nuevo dato para facturar proformas antiguas sin recargo
' Dario 2013-09-06 nuevo dato para facturar laboratorio con descuento por cantidada concepto de descuento 
Namespace SRA

Partial Class Parametros
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents btnAlta As System.Web.UI.WebControls.Button
    Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimp As System.Web.UI.WebControls.Button
    Protected WithEvents hdnPosic As System.Web.UI.WebControls.TextBox

    Protected WithEvents lblTipoComprobante As System.Web.UI.WebControls.Label
    Protected WithEvents lblCuentaBcoDeposito As System.Web.UI.WebControls.Label



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "parametros"
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                mCargarCombos()

                ' Dario 2013-05-27 Nueva solapa pagos mis cuentas
                Me.cmbTipoComprPmC.SelectedValue = "42" ' Recibo de Acred.

                mSetearMaxLength()
                mConsultar()
                mMostrarPanel()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMonedaLoc, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMonedaExt, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "iva_posic", cmbIVAPosic, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCategorias, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "asociaciones", cmbSraAsoc, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInstituciones, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbProvincias, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "clientes", cmbClientes, "N", "@clie_gene=1")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros_codydesc", cmbEmisoresCtro, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "exposiciones", cmbExpo, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "meses", cmbMesCierre, "S")
        ' Dario 2013-05-27 Nueva solapa pagos mis cuentas
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbDescuentoPmC, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbTipoComprPmC, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuentaBcoDepositoPmC, "S", "@muestraBanco = S")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBcoOrigDepositoPmC, "S")
        ' Dario 2013-09-05 cambio para bonificacion de laboratorio
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConceptoDescuentoBonifLabAuto, "S")


    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtDocuPath.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_clie_docu_path")
        txtFotoPath.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_clie_foto_path")
        txtTempPath.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_temp_path")
        txtRazaPath.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_clie_raza_path")
        txtCantGrilla.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_page_size_grilla")
        txtCantBoviServ.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_servi_bovi_maxi_cant")
        txtCantCaprServ.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_servi_capr_maxi_cant")
        txtEnvioEmails.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_desde_mail")
        txtAdmEmails.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_admin_mail")
        txtClaveUnica.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_cunica_nume")
        txtNroRRGG.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_rrgg_tram")
        txtMontoInt.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_min_inte")
        txtFactLab.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_labo_dias_turnos")
        txtDiasVtoCAI.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_dias_aviso_cai")
        txtAceptaCheque.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_dias_aviso_cheque")
        txtLeyendaDol.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_leyen_dolar")
        txtDiasCierreArqueo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_dias_cierre_arqueo")
        txtDiasIng.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_soin_vige_dias")
        txtCantAnios.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_soci_anios_vitalicio")
        txtBimestre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_bime_en_curso")
        txtMayorEdad.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_menor_edad")
        txtMenorEdad.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_menor_ingreso_edad")
        txtCantBim.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_soci_maxi_bime")
        txtSociUltNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_soci_ult_nume")
        'txtCalif.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_aprob_calif")
        txtTopePoder.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_soci_apod_tope")
        txtRecaProfDias.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "para_recalcu_prof_dias")
    End Sub
    Private Sub mMostrarPanel()
        panDato.Visible = True
        panBotones.Visible = True
        Session(mstrTabla) = Nothing
        tabLinks.Visible = True
        panCabecera.Visible = True
        lnkCabecera.Font.Bold = True
        lblTitu.Text = "Par�metros Generales: "
    End Sub
    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            Dim ldsEsta As New DataSet

            lstrCmd.Append("exec " + mstrTabla + "_consul")
            ldsEsta = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            With ldsEsta.Tables(0).Rows(0)
                cmbCategorias.Valor = .Item("para_cate_acti_id")
                cmbClientes.Valor = .Item("para_reci_clie_id")
                cmbInstituciones.Valor = .Item("para_inst_defa")
                cmbIVAPosic.Valor = .Item("para_iva_cons_final")
                cmbMonedaExt.Valor = .Item("para_dolar_mone_id")
                cmbMonedaLoc.Valor = .Item("para_pesos_mone_id")
                cmbSraAsoc.Valor = .Item("para_sra_asoc_id")
                cmbProvincias.Valor = .Item("para_capi_pcia_id")
                cmbEmisoresCtro.Valor = .Item("para_cemi_nume")
                cmbExpo.Valor = .Item("para_expo")

                txtRecaProfDias.Valor = .Item("para_recalcu_prof_dias")
                txtDocuPath.Valor = .Item("para_clie_docu_path")
                txtFotoPath.Valor = .Item("para_clie_foto_path")
                txtTempPath.Valor = .Item("para_temp_path")
                txtRazaPath.Valor = .Item("para_clie_raza_path")
                'txtCriaFirmaPath.Valor = .Item("para_clie_cria_firma_path")
                txtAdmEmails.Valor = .Item("para_admin_mail")
                txtBimestre.Valor = .Item("para_bime_en_curso")
                'txtCalif.Valor = .Item("para_aprob_calif")
                txtCantAnios.Valor = .Item("para_soci_anios_vitalicio")
                txtCantBim.Valor = .Item("para_soci_maxi_bime")
                txtSociUltNume.Valor = .Item("para_soci_ult_nume")
                txtCantGrilla.Valor = .Item("para_page_size_grilla")
                txtCantBoviServ.Valor = .Item("para_servi_bovi_maxi_cant")
                txtCantCaprServ.Valor = .Item("para_servi_capr_maxi_cant")
                txtClaveUnica.Valor = .Item("para_cunica_nume")
                txtDiasIng.Valor = .Item("para_soin_vige_dias")
                txtDiasVtoCAI.Valor = .Item("para_dias_aviso_cai")
                txtEnvioEmails.Valor = .Item("para_desde_mail")
                txtFactLab.Valor = .Item("para_labo_dias_turnos")
                'txtFechaCierre.Fecha = .Item("para_ejer_fina_fecha")
                'txtFechaInicio.Fecha = .Item("para_ejer_inic_fecha")
                'cmbMesCierre.Valor = CInt(CDate(.Item("para_ejer_fina_fecha")).ToString("MM")).ToString()
                cmbMesCierre.Valor = .Item("para_ejer_cierre_mes")
                txtFechaIntFlot.Fecha = .Item("para_soci_inte_fecha")
                txtFechaRevaluo.Fecha = .Item("para_reva_ulti_fecha")
                txtLeyendaDol.Valor = .Item("para_leyen_dolar")
                txtMayorEdad.Valor = .Item("para_menor_edad")
                txtMenorEdad.Valor = .Item("para_menor_ingreso_edad")
                txtMontoInt.Valor = .Item("para_min_inte")
                txtNroRRGG.Valor = .Item("para_rrgg_tram")
                'txtTasaInt.Valor = .Item("para_soci_inte")
                txtAceptaCheque.Valor = .Item("para_dias_aviso_cheque")
                txtDiasCierreArqueo.Valor = .Item("para_dias_cierre_arqueo")
                txtTopePoder.Valor = .Item("para_soci_apod_tope")
                If .IsNull("para_soci_apod_firm") Then
                    cmbSociTope.Limpiar()
                Else
                    If .Item("para_soci_apod_firm") = False Then
                        cmbSociTope.Valor = "N"
                    Else
                        cmbSociTope.Valor = "S"
                    End If
                End If
                txtProSocioFallecido.Valor = .Item("para_soci_fall_prec")
                chkIIBBPerc.Checked = .Item("para_iibb_perc")
                chkIVAPerc.Checked = .Item("para_iva_perc")
                ' Dario 2013-05-28 nuevo panel pagos mis cuentas
                Me.txtNroEstablePmC.Valor = .Item("para_EstableNumeroPmC")
                Me.txtEmpresaAsingnadoPmC.Valor = .Item("para_BcoAsignadoNumeroPmC")
                Me.txtNroBanelcoPmC.Valor = .Item("para_EmpresaNumeroPmC")
                Me.cmbDescuentoPmC.Valor = .Item("para_conc_id")
                If .IsNull("para_coti_id") Then
                    Me.cmbTipoComprPmC.Valor = 42
                Else
                    Me.cmbTipoComprPmC.Valor = .Item("para_coti_id")
                End If

                Me.cmbCuentaBcoDepositoPmC.Valor = .Item("para_cuba_id")
                Me.cmbBcoOrigDepositoPmC.Valor = .Item("para_banc_id")
                Me.txtPorcComisionPmC.Valor = .Item("para_Porc_ComisionPmC")
                Me.txtMontoMinimoComisionPmC.Valor = .Item("para_MontoMinimo_ComisionPmC")
                Me.txtIvaComisionPmC.Valor = .Item("para_Iva_ComisionPmC")
                ' Dario 2013-08-16 nuevo dato para facturar proformas antiguas sin recargo
                Me.txtFacProformaSinRecargoDesdeFecha.Fecha = .Item("para_FacProformaSinRecargoDesdeFecha")
                ' Dario 2013-05-09 cambio para bonif laboratirio
                Me.cmbConceptoDescuentoBonifLabAuto.Valor = .Item("para_IdConceptoBonifLab")
            End With

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panEstu.Visible = False
        panOcup.Visible = False
        panCabecera.Visible = False
        ' Dario 2013-05-27 Nueva solapa pagos mis cuentas
        panPagosMisCuentas.Visible = False

        lnkCabecera.Font.Bold = False
        lnkFacturacion.Font.Bold = False
        lnkSocios.Font.Bold = False
        ' Dario 2013-05-27 Nueva solapa pagos mis cuentas
        lnkPagosMisCuentas.Font.Bold = False

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Text = "Par�metros Generales: "
            Case 2
                panEstu.Visible = True
                lnkFacturacion.Font.Bold = True
                lblTitu.Text = "Par�metros de Facturaci�n: "
            Case 3
                panOcup.Visible = True
                lnkSocios.Font.Bold = True
                lblTitu.Text = "Par�metros de Socios: "
            Case 5 ' Dario 2013-05-27 Nueva solapa pagos mis cuentas
                panPagosMisCuentas.Visible = True
                lnkPagosMisCuentas.Font.Bold = True
                lblTitu.Text = "Par�metros de Pagos mis Cuentas: "

        End Select
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkFacturacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFacturacion.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkSocios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSocios.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkAlumnos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mShowTabs(4)
    End Sub
    ' Dario 2013-05-27 Nueva solapa pagos mis cuentas
    Private Sub lnkPagosMisCuentas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPagosMisCuentas.Click
        mShowTabs(5)
    End Sub
#End Region

#Region "Opciones de Modificaci�n"
    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Modi()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla)

        With ldsEsta.Tables(0).Rows(0)
            .Item("para_cate_acti_id") = cmbCategorias.Valor()
            .Item("para_reci_clie_id") = cmbClientes.Valor()
            .Item("para_inst_defa") = cmbInstituciones.Valor()
            .Item("para_iva_cons_final") = cmbIVAPosic.Valor()
            .Item("para_dolar_mone_id") = cmbMonedaExt.Valor()
            .Item("para_pesos_mone_id") = cmbMonedaLoc.Valor()
            .Item("para_capi_pcia_id") = cmbProvincias.Valor()
            .Item("para_sra_asoc_id") = cmbSraAsoc.Valor()
            .Item("para_expo") = cmbExpo.Valor()

            .Item("para_recalcu_prof_dias") = txtRecaProfDias.Valor
            .Item("para_clie_docu_path") = txtDocuPath.Valor
            .Item("para_clie_foto_path") = txtFotoPath.Valor
            .Item("para_clie_raza_path") = txtRazaPath.Valor
            .Item("para_temp_path") = txtTempPath.Valor
            '.Item("para_clie_cria_firma_path") = txtCriaFirmaPath.Valor
            .Item("para_admin_mail") = txtAdmEmails.Valor
            .Item("para_bime_en_curso") = txtBimestre.Valor
            '.Item("para_aprob_calif") = txtCalif.Valor
            .Item("para_soci_anios_vitalicio") = txtCantAnios.Valor
            .Item("para_soci_maxi_bime") = txtCantBim.Valor
            .Item("para_soci_ult_nume") = txtSociUltNume.Valor
            .Item("para_page_size_grilla") = txtCantGrilla.Valor
            .Item("para_servi_bovi_maxi_cant") = txtCantBoviServ.Valor
            .Item("para_servi_capr_maxi_cant") = txtCantCaprServ.Valor
            .Item("para_cunica_nume") = txtClaveUnica.Valor
            .Item("para_soin_vige_dias") = txtDiasIng.Valor
            .Item("para_dias_aviso_cai") = txtDiasVtoCAI.Valor
            .Item("para_desde_mail") = txtEnvioEmails.Valor
            .Item("para_labo_dias_turnos") = txtFactLab.Valor
            '.Item("para_ejer_fina_fecha") = txtFechaCierre.Fecha
            '.Item("para_ejer_inic_fecha") = txtFechaInicio.Fecha
            '.Item("para_ejer_inic_fecha") = mCalcularInicioEjer()
            '.Item("para_ejer_fina_fecha") = DateAdd(DateInterval.Day, -1, mCalcularInicioEjer())
            .Item("para_ejer_cierre_mes") = cmbMesCierre.Valor
            .Item("para_soci_inte_fecha") = txtFechaIntFlot.Fecha
            .Item("para_reva_ulti_fecha") = txtFechaRevaluo.Fecha
            .Item("para_leyen_dolar") = txtLeyendaDol.Valor
            .Item("para_menor_edad") = txtMayorEdad.Valor
            .Item("para_menor_ingreso_edad") = txtMenorEdad.Valor
            .Item("para_min_inte") = txtMontoInt.Valor
            .Item("para_rrgg_tram") = txtNroRRGG.Valor
            '.Item("para_soci_inte") = txtTasaInt.Valor
            .Item("para_dias_aviso_cheque") = txtAceptaCheque.Valor
            .Item("para_dias_cierre_arqueo") = txtDiasCierreArqueo.Valor
            .Item("para_soci_apod_tope") = txtTopePoder.Valor
            Select Case cmbSociTope.Valor
                Case "S"
                    .Item("para_soci_apod_firm") = True
                Case "N"
                    .Item("para_soci_apod_firm") = False
                Case Else
                    .Item("para_soci_apod_firm") = DBNull.Value
            End Select
            .Item("para_soci_fall_prec") = txtProSocioFallecido.Valor.ToString
            .Item("para_iibb_perc") = chkIIBBPerc.Checked
            .Item("para_iva_perc") = chkIVAPerc.Checked

            ' Dario 2013-05-28 nuevo panel pagos mis cuentas
            .Item("para_EstableNumeroPmC") = Me.txtNroEstablePmC.Valor
            .Item("para_BcoAsignadoNumeroPmC") = Me.txtEmpresaAsingnadoPmC.Valor
            .Item("para_EmpresaNumeroPmC") = Me.txtNroBanelcoPmC.Valor
            .Item("para_conc_id") = Me.cmbDescuentoPmC.Valor
            .Item("para_coti_id") = Me.cmbTipoComprPmC.Valor
            .Item("para_cuba_id") = Me.cmbCuentaBcoDepositoPmC.Valor
            .Item("para_banc_id") = Me.cmbBcoOrigDepositoPmC.Valor
            .Item("para_Porc_ComisionPmC") = Me.txtPorcComisionPmC.Valor
            .Item("para_MontoMinimo_ComisionPmC") = Me.txtMontoMinimoComisionPmC.Valor
            .Item("para_Iva_ComisionPmC") = Me.txtIvaComisionPmC.Valor
            ' Dario 2013-08-16 nuevo dato para facturar proformas antiguas sin recargo
            .Item("para_FacProformaSinRecargoDesdeFecha") = Me.txtFacProformaSinRecargoDesdeFecha.Fecha
            ' Dario 2013-05-09 cambio para bonif laboratirio
            .Item("para_IdConceptoBonifLab") = Me.cmbConceptoDescuentoBonifLabAuto.Valor

        End With

        Return ldsEsta
    End Function

    Private Function mCalcularInicioEjer() As DateTime
        If cmbMesCierre.Valor = 12 Then
            Return (Convert.ToDateTime("01/01/" & (Today.Year + 1).ToString()))
        Else
            Return (Convert.ToDateTime("01/" & (cmbMesCierre.Valor + 1) & "/" & Today.Year.ToString()))
        End If
    End Function

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If txtDiasIng.Valor > 365 Then
            Throw New AccesoBD.clsErrNeg("Dias de Vigencia de la solicitud de Ingreso debe ser menor a 365 (Solapa Socios)")
        End If
        If txtCantAnios.Valor >= 100 Then
            Throw New AccesoBD.clsErrNeg("La cantidad de a�os para ser socio vitalicio debe ser menor a 100 (Solapa Socios)")
        End If
        If txtMayorEdad.Valor >= 30 Then
            Throw New AccesoBD.clsErrNeg("El campo mayor�a de edad debe ser menor a 30 (Solapa Socios)")
        End If
        If txtMenorEdad.Valor >= 30 Then
            Throw New AccesoBD.clsErrNeg("La Categor�a de Menores debe ser menor a 30 (Solapa Socios)")
        End If
        If txtBimestre.Valor >= 100 Then
            Throw New AccesoBD.clsErrNeg("La cantidad de bimestres adeudados debe ser menor a 100 (Solapa Socios)")
        End If
        'If txtTasaInt.Valor >= 100 Then
        '   Throw New AccesoBD.clsErrNeg("La Tasa de Inter�s Flotantes debe ser menor a 100 (Solapa Socios)")
        'End If
        If txtAceptaCheque.Valor >= 30 Then
            Throw New AccesoBD.clsErrNeg("La cantidad de d�as para aceptar cheques debe ser menor a 30 (Solapa Facturaci�n)")
        End If

    End Sub
#End Region

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

End Class
End Namespace
