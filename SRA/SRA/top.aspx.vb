'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_top_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
'a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'top.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Namespace SRA

'Partial Class top
Partial Class Migrated_top

Inherits top

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblUser.Text = Session("sUser").ToString()
        lblBase.Text = Session("sBase").ToString()
        lblBase.Text = lblBase.Text.ToUpper()
        lblDate.Text = clsFormatear.gFormatFecha(DateTime.Now, "es-AR")
    End Sub

End Class
End Namespace
