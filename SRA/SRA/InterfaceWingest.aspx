<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InterfaceWingest" CodeFile="InterfaceWingest.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Interface Wingest</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px; HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Interface Wingest</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px" width="2"></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="99%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="0px" BorderStyle="Solid"
															width="99%" Height="116px">
															<P align="right">
																<TABLE id="Table2" style="WIDTH: 98%; HEIGHT: 106px" cellPadding="0" align="center" border="0">
																	<TR>
																		<TD vAlign="top" colSpan="4">
																			<asp:Label id="lblFechaRecep" runat="server" cssclass="titulo">Fecha Recepci&oacute;n:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaRecep" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 56px" colSpan="3">
																			<asp:Label id="lblArch" runat="server" cssclass="titulo">Archivo: </asp:Label><INPUT class="fileboton" id="filArch" style="WIDTH: 490px; HEIGHT: 22px" type="file" size="72"
																				name="filArch" runat="server"></TD>
																		<TD style="WIDTH: 56px" align="left">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo: </asp:Label><BR>
																			<cc1:combobox class="combo" id="cmbTipo" runat="server" Width="80px" AceptaNull="False">
																				<asp:ListItem Value="DBF" Selected="True">dBase IV</asp:ListItem>
																				<asp:ListItem Value="XLS">MS Excel</asp:ListItem>
																			</cc1:combobox>&nbsp;
																			<asp:Label id="lblArchivoOrigen" runat="server" Visible="False"></asp:Label>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" colSpan="4">
																			<asp:Label id="lblErr" runat="server" cssclass="titulo">Resultados: </asp:Label><BR>
																			<asp:ListBox id="lisResu" runat="server" cssclass="listaerrores" Height="167px" Width="99%"></asp:ListBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="right" colSpan="4" height="28"><BUTTON class="boton" id="btnArchivo" style="WIDTH: 90px" onclick="btnArchivo_click();"
																				type="button" runat="server" value="Detalles">Ver Archivos</BUTTON>
																			<asp:Button id="btnImpo" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Importar"></asp:Button>&nbsp;&nbsp;&nbsp;
																			<asp:Button id="btnListar" runat="server" cssclass="boton" Width="120px" CausesValidation="False"
																				Text="Listar Resultado"></asp:Button>&nbsp;&nbsp;&nbsp;</TD>
																	</TR>
																</TABLE>
															</P>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD colSpan="3"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="txtIdCabe" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:datagrid id="grdConsulta" runat="server" BorderWidth="1px" BorderStyle="None" width="100%"
					AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" PageSize="15" ItemStyle-Height="5px" AutoGenerateColumns="False">
					<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
					<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
					<FooterStyle CssClass="footer"></FooterStyle>
					<Columns>
						<asp:TemplateColumn>
							<HeaderStyle Width="5%"></HeaderStyle>
							<ItemTemplate>
								<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
									Height="5">
									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
				</asp:datagrid></DIV>
			<DIV style="DISPLAY: none">&nbsp;</DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Interface Wingest');
		if (document.frmABM.lblMens.value != '') {
		   alert(document.frmABM.lblMens.value);
		   document.frmABM.lblMens.value = '';
		}
		
		function btnArchivo_click()
		{
			gAbrirVentanas("InterfaceWingestConsul_pop.aspx?", 0, "600","440","50","50");
		}
		</SCRIPT>
	</BODY>
</HTML>
