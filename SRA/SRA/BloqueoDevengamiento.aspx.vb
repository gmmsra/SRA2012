Namespace SRA

Partial Class BloqueoDevengamiento
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   Protected WithEvents txtDocuNumeFil As NixorControls.NumberBox
   Protected WithEvents cmbDocuTipoFil As NixorControls.ComboBox
   Protected WithEvents lblDocuFil As System.Web.UI.WebControls.Label
   Protected WithEvents txtCuitFil As NixorControls.CUITBox
   Protected WithEvents lblCuitFil As System.Web.UI.WebControls.Label
   Protected WithEvents txtNumeFil As NixorControls.NumberBox
   Protected WithEvents lblNumeFil As System.Web.UI.WebControls.Label
   Protected WithEvents txtApelFil As NixorControls.TextBoxTab
   Protected WithEvents lblApelFil As System.Web.UI.WebControls.Label


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub
#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Socios

   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Edit = 0
      Id = 1
      Nume = 2
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then

            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            ' mCargarCombo()
            mConsultar()

         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


#End Region

#Region "Seteo de Controles"

   Public Sub mCargarDatos(ByVal pstrId As String)

      mCrearDataSet(pstrId)


      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("soci_id").ToString()
         UsrSoci.Valor = .Item("soci_id")
         txtFecha.Fecha = .Item("_soci_bloq_deve_fecha")
      End With

      mMostrarPanel(True)
   End Sub

   Private Sub mCerrar()
      mConsultar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
   End Sub

   Private Sub mCargarCombo()
      'clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "S")
   End Sub

#End Region

#Region "Opciones de ABM"

  

   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
      mdsDatos.Tables(0).TableName = mstrTabla
      'Session(mstrTabla) = mdsDatos
   End Sub

#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(Columnas.Id).Text)
         mLectura(True)
         btnModi.Text = "Desbloquear"
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLectura(ByVal pOk As Boolean)

      Me.panCabecera.Enabled = Not pOk

   End Sub

#End Region

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Public Sub mConsultar()
      Try

         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_busq ")
         lstrCmd.Append("@incluir_bajas=" + IIf(chkBaja.Checked, "1", "0"))
         'lstrCmd.Append(", @soci_apel=" + clsSQLServer.gFormatArg(txtApelFil.Text, SqlDbType.VarChar))
         lstrCmd.Append(", @soci_id=" + usrSocioFil.Valor.ToString)
         lstrCmd.Append(", @soci_clie_id=" + usrClieFil.Valor.ToString)
         'lstrCmd.Append(", @soci_nume=" + txtNumeFil.Valor.ToString)
         'lstrCmd.Append(", @clie_cuit=" + txtCuitFil.TextoPlano.ToString)
         'lstrCmd.Append(", @clie_doti_id=" + cmbDocuTipoFil.Valor.ToString)
         'lstrCmd.Append(", @clie_docu_nume=" + clsSQLServer.gFormatArg(txtDocuNumeFil.Valor.ToString, SqlDbType.Int))
         lstrCmd.Append(", @bloq = 1")


         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtFecha.Text = ""
      UsrSoci.Valor = 0

   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnModi.Text = "Bloquear"
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()
      usrClieFil.Valor = 0
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
      mLimpiarFiltros()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModificar()
   End Sub

   Private Sub mModificar()
      Try
         If btnModi.Text = "Bloquear" Then
            mBloquear()
         Else
            mDesbloquear()
         End If
         mCerrar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mDesbloquear()
      Dim strConsul = "@soci_id = " + hdnId.Text + ", @soci_bloq_deve=0"
      mActualizarSocio(strConsul)
      mLectura(False)
   End Sub

   Private Sub mBloquear()

      If (UsrSoci.Valor Is System.DBNull.Value Or UsrSoci.Valor = 0) Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el socio.")
      End If

      If (txtFecha.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha.")
      End If

      Dim strConsul = "@soci_id = " + UsrSoci.Valor + ", @soci_bloq_deve=1, @soci_bloq_deve_fecha = " + clsSQLServer.gFormatArg(txtFecha.Text, SqlDbType.SmallDateTime)
      mActualizarSocio(strConsul)
   End Sub

   Private Sub mActualizarSocio(ByVal pStrConsul As String)
      Dim consulta As String = "socios_bloqueo " + pStrConsul
      clsSQLServer.gExecuteQuery(mstrConn, consulta)
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltro()
   End Sub

   Private Sub mLimpiarFiltro()
      'txtApelFil.Text = ""
      'txtNumeFil.Text = ""
      'txtCuitFil.Text = ""
      'cmbDocuTipoFil.Limpiar()
      'txtDocuNumeFil.Text = ""
      usrSocioFil.Limpiar()
      usrClieFil.Limpiar()
      chkBaja.Checked = False
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String

         lstrRptName = "BloqueoDevengamiento"

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&soci_id=" + usrSocioFil.Valor.ToString
         lstrRpt += "&soci_clie_id=" + usrClieFil.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
