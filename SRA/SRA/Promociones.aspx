<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Promociones" CodeFile="Promociones.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Promociones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="javascript:gSetearTituloFrame();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Promociones</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" OnPageIndexChanged="DataGrid_Page"
										OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="prom_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="prom_desc" HeaderText="Descripci�n"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2">
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageDisable="btnNuev0.gif" ForeColor="Transparent"
													ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
													ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"
													ToolTip="Agregar un Nuevo Profesor"></CC1:BOTONIMAGEN></TD>
											<TD align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
													ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
													BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
											<TD align="right">
												<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
													<tr>
														<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
														<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
														<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
														<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" CausesValidation="False" Width="80px" Height="21px"> Promoci�n</asp:linkbutton></td>
														<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
														<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCate" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
																CausesValidation="False" Width="80px" Height="21px">Categor�as</asp:linkbutton></td>
														<td width="1"></td>
														<td width="1" background="imagenes/tab_fondo.bmp"></td>
														<td width="1"></td>
														<td width="1" background="imagenes/tab_fondo.bmp"></td>
														<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
													</tr>
												</table>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" width="100%" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD width="100%" height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">
															<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" width="140">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="100%" Obligatorio="true"></CC1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCate" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdCate" runat="server" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1"
																				GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnPageIndexChanged="grdCate_PageChanged"
																				width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosCate">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderStyle-Width="2%">
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="prca_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_cate_desc" HeaderText="Categor&#237;a"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prca_porc" DataFormatString="{0:F2}" HeaderText="Porcentaje"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prca_impo" DataFormatString="{0:F2}" HeaderText="Importe"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:Label id="lblCate" runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<cc1:combobox class="combo" id="cmbCate" runat="server" Width="400px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																		<TD width="80%">
																			<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="60px" EsDecimal="True"
																				AceptaNull="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblPorc" runat="server" cssclass="titulo">Porcentaje:</asp:Label>&nbsp;</TD>
																		<TD width="80%">
																			<cc1:numberbox id="txtPorc" runat="server" cssclass="cuadrotexto" Width="40px" EsDecimal="True"
																				AceptaNull="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaCate" runat="server" cssclass="boton" Width="123px" Text="Agregar Categor�a"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaCate" runat="server" cssclass="boton" Width="123px" Text="Eliminar Categor�a"></asp:Button>&nbsp;
																			<asp:Button id="btnModiCate" runat="server" cssclass="boton" Width="129px" Text="Modificar Categor�a"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpCate" runat="server" cssclass="boton" Width="119px" Text="Limpiar Categor�a"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD align="center" colSpan="3">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center" colSpan="3">
															<ASP:PANEL id="panBotones" Runat="server">
																<TABLE width="100%">
																	<TR height="30">
																		<TD align="center"><A id="editar" name="editar"></A>
																			<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																			<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																</TABLE>
															</ASP:PANEL></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnPrcaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null && document.all["panDato"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
