<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Localidades" CodeFile="Localidades.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Localidades</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO -----> 
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Localidades</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" BorderStyle="Solid"
										Width="100%">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR height="10">
																		<TD background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblNombreFil" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtNombreFil" runat="server" cssclass="cuadrotexto" Width="250px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblExclPais" runat="server" cssclass="titulo">Pa�s:</asp:label>&nbsp;</TD>
																		<TD style="HEIGHT: 20px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbExclPais" runat="server" Width="248px" AceptaNull="False" onchange="mCargarProvincias(this,'cmbExclPcia')"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblExclPcia" runat="server" cssclass="titulo">Provincia:</asp:label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbExclPcia" runat="server" Width="248px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<!---fin filtro --->
							<tr height="10">
								<td colSpan="2"></td>
							</tr>
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mSeleccionarDatos"
										OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="Linkbutton2" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit"
														Height="5">
														<img src='images/sele.gif' border="0" alt="Seleccionar Localidad" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" CausesValidation="false" Text="Editar" CommandName="Update">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="loca_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="loca_desc" ReadOnly="True" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="30%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_pcia_desc" ReadOnly="True" HeaderText="Provincia">
												<HeaderStyle Width="30%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="loca_cpos" ReadOnly="True" HeaderText="C&#243;d. Postal"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="2">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
													BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
													ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Localidad" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="center" width="50"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
													BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
													ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD align="center"><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
										BorderStyle="Solid">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 268px">
													<P></P>
												</TD>
												<TD height="5"></TD>
												<TD vAlign="top" align="right" rowSpan="2">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
													<asp:label id="lblPais" runat="server" cssclass="titulo">Pa�s:</asp:label>&nbsp;</TD>
												<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
													<cc1:combobox class="combo" id="cmbPais" runat="server" Width="248px" AceptaNull="False" onchange="mCargarProvincias(this,'cmbProv')"></cc1:combobox></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
													<asp:label id="lblProv" runat="server" cssclass="titulo">Provincia:</asp:label>&nbsp;</TD>
												<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
													<cc1:combobox class="combo" id="cmbProv" runat="server" Width="248px" AceptaNull="False"></cc1:combobox></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 268px" align="right">
													<asp:Label id="lblCodi" runat="server" cssclass="titulo" Width="83px">Descripci�n:</asp:Label></TD>
												<TD>
													<CC1:TEXTBOXTAB id="txtDescrip" runat="server" cssclass="cuadrotexto" Width="199px"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">&nbsp;
													<asp:Label id="lblCP" runat="server" cssclass="titulo" Width="90px">Cod. Pos.:</asp:Label></TD>
												<TD style="HEIGHT: 16px" colSpan="2" height="16">
													<CC1:CODPOSTALBOX id="txtDireCP" runat="server" cssclass="cuadrotexto" Width="77px"></CC1:CODPOSTALBOX></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 13px" align="center" colSpan="3">
													<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
													<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Baja"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Modificar"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Limpiar"></asp:Button></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnPopup" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function mCargarProvincias(pPais,pcmbPcia)
		{
		   var sFiltro = pPais.value;
   		if (sFiltro != '')
			   {
			   LoadComboXML("provincias_cargar", sFiltro, pcmbPcia, "T");
			   }
			else
			{
				document.all(pcmbPcia).innerText='';
			}
			if (pPais.value == '<%=Session("sPaisDefaId")%>')
				document.all('hdnValCodPostalPais').value = 'S';
			else
				document.all('hdnValCodPostalPais').value = 'N';
		}		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		if (document.all["txtCodiNum"]!= null)
			document.all["txtCodiNum"].focus();
		//if (document.all("hdnPopup").value != '')
		//	window.opener.mCargarLocalidades("<%=mstrTipo%>")
		</SCRIPT>
	</BODY>
</HTML>
