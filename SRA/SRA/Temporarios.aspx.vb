Namespace SRA

Partial Class Temporarios
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' Protected WithEvents cmbRazaFil As NixorControls.ComboBox



   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Temporarios
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            mSetearEventos()
            mSetearMaxLength()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "S")
      SRA_Neg.Utiles.gSetearRaza(cmbRaza)
        '  clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "T")
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstado, "N", "@esti_id=85")
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=85")
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtRPNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_rp")
      txtRPNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_rp")
      txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_nombre")
      txtNombFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_nombre")
      txtApod.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_apodo")
      txtApodFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_apodo")
      'txtFalleCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptembaja_codi")
      txtPadreRP.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_padre_rp")
      txtPadreSRA.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_padre_sra_nume")
      txtPadreNombre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_padre_nombre")
      txtMadreRP.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_madre_rp")
      txtMadreSRA.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_madre_sra_nume")
      txtMadreNombre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_madre_nombre")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ptem_obse")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd += "@ptem_raza_id=" + IIf(usrCriadorFil.RazaId = "", "null", usrCriadorFil.RazaId.ToString())
         mstrCmd += ",@ptem_sexo=" + IIf(cmbSexoFil.Valor.ToString = "", "null", cmbSexoFil.Valor.ToString)
         mstrCmd += ",@ptem_rp=" + IIf(txtRPNumeFil.Valor.ToString = "", "null", txtRPNumeFil.Valor.ToString)
         mstrCmd += ",@ptem_nombre=" + IIf(txtNombFil.Valor.ToString = "", "null", "'" + txtNombFil.Valor.ToString + "'")
         mstrCmd += ",@ptem_apodo=" + IIf(txtApodFil.Valor.ToString = "", "null", "'" + txtApodFil.Valor.ToString + "'")
         mstrCmd += ",@ptem_cria_id=" + usrCriadorFil.Valor.ToString
         mstrCmd += ",@ptem_prdt_id=" + usrProductoFil.Valor.ToString
         mstrCmd += ",@ptem_esta_id=" + cmbEstadoFil.Valor.ToString

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      Dim ldsDatos As DataSet
      ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      If ldsDatos.Tables(0).Rows.Count > 0 Then
         With ldsDatos.Tables(0).Rows(0)
            cmbRaza.Valor = .Item("ptem_raza_id")
            cmbSexo.ValorBool = .Item("ptem_sexo")
            txtRPNume.Valor = .Item("ptem_rp")
            usrCria.Valor = .Item("ptem_cria_id")
            txtNomb.Valor = .Item("ptem_nombre")
            txtApod.Valor = .Item("ptem_apodo")
            usrProducto.Valor = .Item("ptem_prdt_id")
            txtFechaEnganche.Fecha = .Item("ptem_enga_fecha")
            txtPadreRP.Valor = .Item("ptem_padre_rp")
            txtPadreSRA.Valor = .Item("ptem_padre_sra_nume")
            txtPadreNombre.Valor = .Item("ptem_nombre")
            txtMadreRP.Valor = .Item("ptem_madre_rp")
            txtMadreSRA.Valor = .Item("ptem_madre_sra_nume")
            txtMadreNombre.Valor = .Item("ptem_nombre")
            cmbEstado.Valor = .Item("ptem_esta_id")
            txtObse.Valor = .Item("ptem_obse")
            txtFalleFecha.Fecha = .Item("ptem_baja_fecha")
            txtInscFecha.Fecha = .Item("ptem_ingr_fecha")
            txtNaciFecha.Fecha = .Item("ptem_naci_fecha")
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
      End If
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      cmbRaza.Limpiar()
      SRA_Neg.Utiles.gSetearRaza(cmbRaza)
      cmbSexo.Limpiar()
      txtRPNume.Text = ""
      usrCria.Limpiar()
      txtNomb.Text = ""
      txtApod.Text = ""
      usrProducto.Limpiar()
      txtFechaEnganche.Text = ""
      txtPadreRP.Text = ""
      txtPadreSRA.Text = ""
      txtPadreNombre.Text = ""
      txtMadreRP.Text = ""
      txtMadreSRA.Text = ""
      txtMadreNombre.Text = ""
      cmbEstado.Limpiar()
      txtObse.Text = ""
      txtFalleFecha.Text = ""
      txtInscFecha.Text = ""
      txtNaciFecha.Text = ""
      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarFiltro()
        'cmbRazaFil.Limpiar()
        'SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
      cmbSexoFil.Limpiar()
      txtNombFil.Text = ""
      txtApodFil.Text = ""
      txtRPNumeFil.Text = ""
      usrCriadorFil.Limpiar()
      usrProductoFil.Limpiar()
      cmbEstadoFil.Limpiar()

      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("ptem_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("ptem_raza_id") = cmbRaza.Valor
         .Item("ptem_sexo") = cmbSexo.ValorBool
         .Item("ptem_rp") = txtRPNume.Valor
         .Item("ptem_cria_id") = usrCria.Valor
         .Item("ptem_nombre") = txtNomb.Valor
         .Item("ptem_apodo") = txtApod.Valor
         .Item("ptem_prdt_id") = usrProducto.Valor
         .Item("ptem_enga_fecha") = txtFechaEnganche.Fecha
         .Item("ptem_padre_rp") = txtPadreRP.Valor
         .Item("ptem_padre_sra_nume") = txtPadreSRA.Valor
         .Item("ptem_padre_nombre") = txtPadreNombre.Valor
         .Item("ptem_madre_rp") = txtMadreRP.Valor
         .Item("ptem_madre_sra_nume") = txtMadreSRA.Valor
         .Item("ptem_padre_nombre") = txtMadreNombre.Valor
         .Item("ptem_esta_id") = cmbEstado.Valor
         .Item("ptem_obse") = txtObse.Valor
         .Item("ptem_baja_fecha") = txtFalleFecha.Fecha
         .Item("ptem_ingr_fecha") = txtInscFecha.Fecha
         .Item("ptem_naci_fecha") = txtNaciFecha.Fecha
         .Item("ptem_audi_user") = Session("sUserId").ToString()
      End With

      Return ldsEsta
   End Function
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc, Context)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc, context)
         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltro()
   End Sub
#End Region

End Class
End Namespace
