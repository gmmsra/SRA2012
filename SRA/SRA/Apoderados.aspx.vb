Namespace SRA

Partial Class Apoderados
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTable As String = "apoderados"


   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private pSocioId As Integer = 0

   Private Enum Columnas As Integer
      Id = 1
      SoinId = 2
      SocmId = 3
      chkSel = 8
      cmbResu = 9
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()

      pSocioId = Request.QueryString("soci_id")
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdApod.PageSize = Convert.ToInt32(mstrParaPageSize)
		lblMens.Text = ""
		chkTodo.Attributes.Add("onclick", "mDescargarCombo();")
		chkSele.Attributes.Add("onclick", "mCargarCombo();")
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            mMostrarBotones(True)
            usrSocioFil.Valor = pSocioId
            usrSocioFil.Activo = (pSocioId = 0)

            usrSocio.Valor = pSocioId
            usrSocio.Activo = (pSocioId = 0)

            btnBusc.Visible = (pSocioId = 0)
            btnLimpiarFil.Visible = (pSocioId = 0)

				mConsultarApoderados()
				mCargarCombos()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

         mSetearAsamblea()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

	Private Sub mCargarCombos()
		clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsamFil, "", "")
		Me.cmbAsamFil.Items.Add("General")
		Me.cmbAsamFil.Items(Me.cmbAsamFil.Items.Count - 1).Selected = True
	End Sub


#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdApod_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdApod.EditItemIndex = -1
         If (grdApod.CurrentPageIndex < 0 Or grdApod.CurrentPageIndex >= grdApod.PageCount) Then
            grdApod.CurrentPageIndex = 0
         Else
            grdApod.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultarApoderados()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try


                Dim lDsDatos As DataSet

                mLimpiarApoderado()
                hdnApodId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
                lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTable, hdnApodId.Text)

                If lDsDatos.Tables(mstrTable).Rows.Count > 0 Then
                    With lDsDatos.Tables(mstrTable).Rows(0)

                        hdnApodId.Text = IIf(.Item("apod_id").ToString.Trim.Length > 0, .Item("apod_id"), String.Empty)
                        txtVenc.Fecha = IIf(.Item("apod_venc_fecha").ToString.Trim.Length > 0, .Item("apod_venc_fecha"), String.Empty)
                        usrApoderado.Valor = IIf(.Item("apod_apod_soci_id").ToString.Trim.Length > 0, .Item("apod_apod_soci_id"), String.Empty)
                        usrSocio.Valor = IIf(.Item("apod_soci_id").ToString.Trim.Length > 0, .Item("apod_soci_id"), String.Empty)
                        chkSele.Checked = Not .Item("apod_gral")

                        mSetearAsamblea()


                        'clsWeb.gCargarRefeCmb(mstrConn, "asambleas_futuras", cmbAsam, "N")

                        'Pantanettig  29/11/2007
                        Dim arFecha As DateTime

                        If .Item("_asam_real_fecha") Is DBNull.Value Then
                            arFecha = "01/01/1900"
                        Else
                            clsWeb.gCargarCombo(mstrConn, "asambleas_cargar " + Convert.ToString(.Item("apod_asam_id")), cmbAsam, "id", "descrip", "")
                            arFecha = IIf(.Item("_asam_real_fecha").ToString.Trim.Length > 0, .Item("_asam_real_fecha"), String.Empty)
                        End If

                        If chkSele.Checked Then
                            cmbAsam.Valor = IIf(.Item("apod_asam_id").ToString.Trim.Length > 0, .Item("apod_asam_id"), String.Empty)
                        End If
                        '-----------------------
                        If Not chkSele.Checked And .Item("_asam_cerrada") = "NO" Then
                            cmbAsam.Limpiar()
                        End If

                        If .Item("_general").ToString().ToUpper() = "SI" Then
                            Me.BtnAltaApod.Enabled = Not True
                            Me.btnBajaApod.Enabled = True
                            Me.btnModiApod.Enabled = True
                            Me.btnModiApod.Enabled = True
                            Me.usrApoderado.Activo = True
                            Me.txtVenc.Enabled = True
                            Me.chkTodo.Enabled = True
                            Me.chkSele.Enabled = True
                            Me.cmbAsam.Enabled = True

                        ElseIf (arFecha >= Now() And .Item("_asam_cerrada").ToString().ToUpper() = "NO") And .Item("_general").ToString().ToUpper() = "NO" Then
                            Me.BtnAltaApod.Enabled = Not True
                            Me.btnBajaApod.Enabled = True
                            Me.btnModiApod.Enabled = True
                            Me.btnModiApod.Enabled = True
                            Me.usrApoderado.Activo = True
                            Me.txtVenc.Enabled = True
                            Me.chkTodo.Enabled = True
                            Me.chkSele.Enabled = True
                            Me.cmbAsam.Enabled = True

                        Else
                            Me.BtnAltaApod.Enabled = False
                            Me.btnBajaApod.Enabled = False
                            Me.btnModiApod.Enabled = False
                            Me.usrApoderado.Activo = False
                            Me.txtVenc.Enabled = False
                            Me.usrSocio.Activo = False
                            Me.chkTodo.Enabled = False
                            Me.chkSele.Enabled = False
                            Me.cmbAsam.Enabled = False

                        End If
                        If Me.cmbAsam.Valor Is DBNull.Value Then
                            Me.cmbAsam.Enabled = False
                        End If
                    End With
                End If

                mostrarPanel(True)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
   End Sub

   Private Sub mSetearAsamblea()
      cmbAsam.Enabled = chkSele.Checked

      If Not cmbAsam.Enabled Then
         cmbAsam.Limpiar()
      Else
         If Not Request.Form(cmbAsam.UniqueID) Is Nothing Then
            cmbAsam.Valor = Request.Form(cmbAsam.UniqueID)
         End If
      End If
   End Sub

   Private Sub mConsultarApoderados()
      Dim socioId As Integer = 0
      Dim apoderadoId As Integer = 0

      If Not (usrSocioFil.Valor Is DBNull.Value) Then
         socioId = usrSocioFil.Valor
      End If
      If Not (usrApoderadoFil.Valor Is DBNull.Value) Then
         apoderadoId = usrApoderadoFil.Valor
      End If

		Dim mstrCmd As String = "exec " + mstrTable + "_consul"
		mstrCmd = mstrCmd + " @asam_id = " + IIf(cmbAsamFil.Valor.ToString = "General", "0", cmbAsamFil.Valor.ToString)
		mstrCmd = mstrCmd + ", @socio_id = " + socioId.ToString()
      mstrCmd = mstrCmd + ",@apoderado_id = " + apoderadoId.ToString()
      Dim ds As New DataSet
      ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      grdApod.DataSource = ds
      grdApod.DataBind()
      ds.Dispose()
   End Sub

   Private Sub mLimpiarApoderado()
      hdnApodId.Text = ""

      If pSocioId = 0 Then
         usrSocio.Limpiar()
      End If

        usrSocio.Activo = True
        usrApoderado.Activo = True
        txtVenc.Enabled = True
        usrApoderado.Limpiar()
		Me.chkTodo.Enabled = True
		Me.chkSele.Enabled = True
		chkTodo.Checked = True
		chkSele.Checked = False

      mSetearAsamblea()

      txtVenc.Text = ""
      mMostrarBotones(True)
   End Sub

   Private Sub mMostrarBotones(ByVal pOk As Boolean)
      Me.BtnAltaApod.Enabled = pOk
		Me.btnBajaApod.Enabled = Not pOk
		Me.btnModiApod.Enabled = Not pOk
   End Sub

   Private Sub mEliminarApoderado()
      Try
         Dim lintPage As Integer = grdApod.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTable)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnApodId.Text, SqlDbType.Int))

         grdApod.CurrentPageIndex = 0

         mConsultarApoderados()

         If (lintPage < grdApod.PageCount) Then
            grdApod.CurrentPageIndex = lintPage
            mConsultarApoderados()
         End If

         mLimpiarApoderado()
         mostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarApoderado() As Data.DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTable, hdnApodId.Text)
      Dim asam As Int64
      With ldsEsta.Tables(0).Rows(0)
                .Item("apod_apod_soci_id") = IIf(usrApoderado.Valor.ToString.Trim.Trim > 0, usrApoderado.Valor, DBNull.Value)
                .Item("apod_soci_id") = IIf(usrSocio.Valor.ToString.Trim.Length > 0, usrSocio.Valor, DBNull.Value)
                .Item("apod_venc_fecha") = IIf(txtVenc.Fecha.Trim.Length > 0, txtVenc.Fecha, DBNull.Value)
			'Pantanettig 29/11/2007
			If chkSele.Checked Then
				.Item("apod_gral") = False
                    .Item("apod_asam_id") = IIf(cmbAsam.Valor().Trim.Length > 0, cmbAsam.Valor(), DBNull.Value)
			Else
				.Item("apod_gral") = True
				.Item("apod_asam_id") = DBNull.Value
			End If
			'----------------------
		End With
      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
      If (usrSocio.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe especificar el socio.")
      End If
      If (usrApoderado.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe especificar el apoderado.")
      End If

      If (usrApoderado.Valor = usrSocio.Valor) Then
         Throw New AccesoBD.clsErrNeg("Un socio no puede ser el apoderado de si mismo.")
      End If



   End Sub

   Private Sub mAltaApoderado()
      Try

         mValidarDatos()

         Dim mdsDatos As DataSet = mGuardarApoderado()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTable, mdsDatos)

         lobjGenerica.Alta()


			mConsultarApoderados()
			'clsWeb.gCargarRefeCmb(mstrConn, "asambleas_futuras", cmbAsam, "N")

         mLimpiarApoderado()
         mostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModiApoderado()
      Try

         mValidarDatos()

         Dim mdsDatos As DataSet = mGuardarApoderado()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTable, mdsDatos)

         lobjGenerica.Modi()

         mLimpiarApoderado()
         mConsultarApoderados()

         mLimpiarApoderado()
         mostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub BtnAltaApod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAltaApod.Click
      mAltaApoderado()
   End Sub

   Private Sub btnBajaApod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaApod.Click
      mEliminarApoderado()
   End Sub

   Private Sub btnModiApod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiApod.Click
      mModiApoderado()
   End Sub

   Private Sub btnLimpApod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpApod.Click
      mLimpiarApoderado()
   End Sub

   Private Sub usrSocioFil_Cambio(ByVal sender As System.Object) Handles usrSocioFil.Cambio
      Try
         mLimpiarApoderado()
         mConsultarApoderados()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub usrApoderadoFil_Cambio(ByVal sender As System.Object) Handles usrApoderadoFil.Cambio
      Try
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub NuevoApoderado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoApoderado.Click
      mLimpiarApoderado()
      clsWeb.gCargarRefeCmb(mstrConn, "asambleas_poderes", cmbAsam, "S")
      mostrarPanel(True)
   End Sub

   Private Sub mostrarPanel(ByVal pOk As Boolean)
      PanDetalle.Visible = pOk
      btnNuevoApoderado.Enabled = Not pOk
   End Sub

   Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mLimpiarApoderado()
      mostrarPanel(False)
   End Sub

   Private Sub mConsultar()
      mostrarPanel(False)
      mLimpiarApoderado()
      grdApod.CurrentPageIndex = 0
      mConsultarApoderados()
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltro()
   End Sub

   Private Sub mLimpiarFiltro()
      usrSocioFil.Limpiar()
      usrApoderadoFil.Limpiar()
   End Sub

End Class

End Namespace
