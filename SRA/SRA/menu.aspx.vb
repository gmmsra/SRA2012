Namespace SRA

Partial Class menu
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

   Private mstrConn As String

   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mCargarMenu()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarMenu()
      Dim ldsMenu As DataSet = clsSQLServer.gExecuteQuery(mstrConn, "opciones_menu_consul " + Session("sUserId").ToString())
      Dim lTr, lTr2, lTr3, lTr4 As HtmlTableRow
      Dim lTd, lTd2, lTd3, lTd4 As HtmlTableCell
      Dim lA As HtmlAnchor
      Dim lBr, lHr As HtmlGenericControl
      Dim lTable1, lTable2, lTable3, lTable4 As HtmlTable
      Dim lintSubOrden, lintSubSubOrden, lintSubSubSubOrden As Integer

      For Each lDr As DataRow In ldsMenu.Tables(0).Select("opcion=0", "orden")
         lTr = New HtmlTableRow
         TabMenu.Controls.Add(lTr)

         lTd = New HtmlTableCell
         lTr.Cells.Add(lTd)

         lTd.Attributes.Add("class", "clMenu")

         lA = New HtmlAnchor
         lTd.Controls.Add(lA)

         lA.Attributes.Add("class", "clMenuLinks")
         lA.Attributes.Add("onmouseover", "menuonmouseover();")
         lA.Style.Add("width", "100%")
         lA.HRef = "#"
         lA.Attributes.Add("onfocus", "this.blur()")

         lA.InnerText = lDr.Item("descrip")
         lA.Attributes.Add("onclick", "Menumenu('" & lDr.Item("orden") & "');")

         lBr = New HtmlGenericControl
         lBr.InnerHtml = "<BR>"
         lTd.Controls.Add(lBr)

         lTable1 = New HtmlTable
         lTd.Controls.Add(lTable1)

         lTable1.ID = "Tabla" & lDr.Item("orden")
         lTable1.CellPadding = 0
         lTable1.CellSpacing = 1
         lTable1.Attributes.Add("class", "clMenuSub2")

         lTr = New HtmlTableRow
         lTable1.Rows.Add(lTr)

         lTd = New HtmlTableCell
         lTr.Cells.Add(lTd)

         For Each lDrDet As DataRow In ldsMenu.Tables(0).Select("opcion=" & lDr.Item("codi"), "orden")
            lA = New HtmlAnchor
            lTd.Controls.Add(lA)
            lA.Attributes.Add("onmouseover", "menuonmouseover();")
            If lDrDet.Item("descrip") <> "-" Then
               lA.InnerText = lDrDet.Item("descrip")

               Select Case lDrDet.Item("tipo")
                  Case "P"
                     lA.Attributes.Add("class", "clSubLinks2")
                     lA.Target = "main"
                     lA.HRef = lDrDet.Item("ubicacion").ToString

                  Case "C"
                     lintSubOrden = lDrDet.Item("codi")

                     lA.Attributes.Add("class", "clSubLinks")
                     lA.Attributes.Add("onfocus", "if(this.blur)this.blur();")
                     lA.Attributes.Add("onclick", "subMenumenu('" & lDr.Item("orden") & "','" & lintSubOrden & "'); return false;")
                     lA.HRef = "#"
                     lBr = New HtmlGenericControl
                     lBr.InnerHtml = "<BR>"
                     lTd.Controls.Add(lBr)

                     lTable2 = New HtmlTable
                     lTd.Controls.Add(lTable2)

                     lTable2.ID = "Tabla" & lDr.Item("orden") & "_" & lintSubOrden
                     lTable2.CellPadding = 0
                     lTable2.CellSpacing = 0
                     lTable2.Attributes.Add("class", "clMenuSub2")
                     lTable2.Width = "100%"

                     For Each lDrDetDet As DataRow In ldsMenu.Tables(0).Select("opcion=" & lDrDet.Item("codi"), "orden")
                        lTr2 = New HtmlTableRow
                        lTable2.Controls.Add(lTr2)

                        lTd2 = New HtmlTableCell
                        lTr2.Cells.Add(lTd2)

                        lA = New HtmlAnchor
                        lTd2.Controls.Add(lA)

                        lA.Attributes.Add("onmouseover", "menuonmouseover();")
                        If lDrDetDet.Item("descrip") <> "-" Then
                           lA.InnerText = lDrDetDet.Item("descrip")

                           Select Case lDrDetDet.Item("tipo")
                              Case "P"
                                 lA.Attributes.Add("class", "clSubLinks2")
                                 lA.Target = "main"
                                 lA.HRef = lDrDetDet.Item("ubicacion").ToString

                              Case "C"
                                 lintSubSubOrden = lDrDetDet.Item("codi")

                                 lA.Attributes.Add("class", "clSubLinks")
                                 lA.Attributes.Add("onfocus", "if(this.blur)this.blur();")
                                 lA.Attributes.Add("onclick", "subMenumenu('" & lDr.Item("orden") & "','" & lintSubSubOrden & "'); return false;")
                                 lA.HRef = "#"
                                 lBr = New HtmlGenericControl
                                 lBr.InnerHtml = "<BR>"
                                 lTd2.Controls.Add(lBr)

                                 lTable3 = New HtmlTable
                                 lTd2.Controls.Add(lTable3)

                                 lTable3.ID = "Tabla" & lDr.Item("orden") & "_" & lintSubSubOrden
                                 lTable3.CellPadding = 0
                                 lTable3.CellSpacing = 0
                                 lTable3.Attributes.Add("class", "clMenuSub3")
                                 lTable3.Width = "100%"

                                 For Each lDrDetDetDet As DataRow In ldsMenu.Tables(0).Select("opcion=" & lDrDetDet.Item("codi"), "orden")
                                    lTr3 = New HtmlTableRow
                                    lTable3.Controls.Add(lTr3)

                                    lTd3 = New HtmlTableCell
                                    lTr3.Cells.Add(lTd3)

                                    lA = New HtmlAnchor
                                    lTd3.Controls.Add(lA)
                                    lA.Attributes.Add("class", "clSubLinks2")
                                    lA.Attributes.Add("onmouseover", "menuonmouseover();")

                                    If lDrDetDetDet.Item("descrip") <> "-" Then
                                       lA.Target = "main"
                                       lA.HRef = lDrDetDetDet.Item("ubicacion").ToString
                                       lA.InnerText = lDrDetDetDet.Item("descrip")

                                       Select Case lDrDetDetDet.Item("tipo")
                                          Case "P"
                                             lA.Attributes.Add("class", "clSubLinks2")
                                             lA.Target = "main"
                                             lA.HRef = lDrDetDetDet.Item("ubicacion").ToString

                                          Case "C"
                                             lintSubSubSubOrden = lDrDetDetDet.Item("codi")

                                             lA.Attributes.Add("class", "clSubLinks")
                                             lA.Attributes.Add("onfocus", "if(this.blur)this.blur();")
                                             lA.Attributes.Add("onclick", "subMenumenu('" & lDr.Item("orden") & "','" & lintSubSubSubOrden & "'); return false;")
                                             lA.HRef = "#"
                                             lBr = New HtmlGenericControl
                                             lBr.InnerHtml = "<BR>"
                                             lTd3.Controls.Add(lBr)

                                             lTable4 = New HtmlTable
                                             lTd3.Controls.Add(lTable4)

                                             lTable4.ID = "Tabla" & lDr.Item("orden") & "_" & lintSubSubSubOrden
                                             lTable4.CellPadding = 0
                                             lTable4.CellSpacing = 0
                                             lTable4.Attributes.Add("class", "clMenuSub3")
                                             lTable4.Width = "100%"

                                             For Each lDrDetDetDetDet As DataRow In ldsMenu.Tables(0).Select("opcion=" & lDrDetDetDet.Item("codi"), "orden")
                                                lTr4 = New HtmlTableRow
                                                lTable4.Controls.Add(lTr4)

                                                lTd4 = New HtmlTableCell
                                                lTr4.Cells.Add(lTd4)

                                                lA = New HtmlAnchor
                                                lTd4.Controls.Add(lA)
                                                lA.Attributes.Add("class", "clSubLinks2")
                                                lA.Attributes.Add("onmouseover", "menuonmouseover();")
                                                lA.Target = "main"
                                                lA.HRef = lDrDetDetDetDet.Item("ubicacion").ToString
                                                lA.InnerText = lDrDetDetDetDet.Item("descrip")
                                             Next
                                       End Select
                                    Else
                                       lHr = New HtmlGenericControl
                                       lHr.InnerHtml = "<hr1 size=15>"
                                       lA.Controls.Add(lHr)
                                       lA.Attributes.Add("class", "clSubLinks2")
                                    End If
                                 Next
                           End Select
                        Else
                           lHr = New HtmlGenericControl
                           lHr.InnerHtml = "<hr1 size=15>"
                           lA.Controls.Add(lHr)
                           lA.Attributes.Add("class", "clSubLinks2")
                        End If
                     Next
               End Select
            Else
               lHr = New HtmlGenericControl
               lHr.InnerHtml = "<hr1 size=15>"
               lA.Controls.Add(lHr)
               lA.Attributes.Add("class", "clSubLinks2")
            End If
         Next
      Next

      lTr = New HtmlTableRow
      TabMenu.Controls.Add(lTr)

      lTd = New HtmlTableCell
      lTr.Cells.Add(lTd)

      lTd.Attributes.Add("class", "clMenu")

      lA = New HtmlAnchor
      lTd.Controls.Add(lA)

      lA.Attributes.Add("class", "clMenuLinks")
      lA.Attributes.Add("onmouseover", "menuonmouseover();")
      lA.HRef = "login.aspx"
      lA.Target = "main"
      lA.InnerText = "Salir"
      lA.Attributes.Add("onfocus", "this.blur()")
      lBr = New HtmlGenericControl
      lBr.InnerHtml = "<BR>"
      lTd.Controls.Add(lBr)
   End Sub
End Class
End Namespace
