Imports SRA_Neg


Namespace SRA


    Partial Class EventosSocios
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Protected WithEvents lblAlumFil As System.Web.UI.WebControls.Label
        Protected WithEvents lblFechaInsc As System.Web.UI.WebControls.Label
        Protected WithEvents txtObser As NixorControls.TextBoxTab
        Protected WithEvents Label3 As System.Web.UI.WebControls.Label
        Protected WithEvents lblCocktail As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"
        Private mstrTabla As String = "socios_eventos"
        Private mstrParaPageSize As Integer
        Private mstrCmd As String
        Private mdsDatos As DataSet
        Private mstrConn As String
        Private mstrItemCons As String
        Private mintInse As Int32


        Private Enum Columnas As Integer
            Id = 1
        End Enum


#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)

                If (Not Page.IsPostBack) Then
                    mInicializar()
                    mSetearEventos()
                    mEstablecerPerfil()
                    mCargarCombos()

                    Dim iCentroEmisor As Integer = Session("sCentroEmisorId")
                    hdnEmctId.Text = iCentroEmisor.ToString

                    clsWeb.gInicializarControles(Me, mstrConn)

                Else
                    If panDato.Visible Then
                        mdsDatos = Session(mstrTabla)
                    End If
                End If

                'mConsultar(grdDato)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mCargarCombos()
            Dim iCentroEmisor As Integer = Session("sCentroEmisorId")
            Dim strFiltro As String

            clsWeb.gCargarRefeCmb(mstrConn, "eventos", cmbEvenFil, "SS")
            clsWeb.gCargarRefeCmb(mstrConn, "eventos", cmbEvento, "SS", "@EntreFechas='S'")
            'If cmbEvento.Items.Count = 1 Then mAgregoSeleccione()
            cmbEvento.SelectedIndex = -1
            clsWeb.gCargarRefeCmb(mstrConn, "eventos_aux", cmbEvenAux, "SS")
            'strFiltro = "@even_id=" + cmbEvento.Valor + ", " + "@evlu_emct_id=" + iCentroEmisor.ToString()
            'clsWeb.gCargarRefeCmb(mstrConn, "eventos_lugares_por_evento", cmbLugarUno, "Z", strFiltro)
            'clsWeb.gCargarRefeCmb(mstrConn, "eventos_lugares_por_evento", cmbLugarDos, "Z", strFiltro)
        End Sub

        Private Sub mAgregoSeleccione()
            cmbEvento.Items.Insert(0, "(Seleccione)")
            cmbEvento.Items(0).Value = ""
            cmbEvento.SelectedIndex = -1
        End Sub

        Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        End Sub

        Private Sub mEstablecerPerfil()
            Dim lbooPermiAlta As Boolean
            Dim lbooPermiModi As Boolean

            'If (Not clsSQLServer.gMenuPermi(CType(Opciones.EventosSocios, String), (mstrConn), (Session("sUserId").ToString()))) Then
            'Response.Redirect("noaccess.aspx")
            'End If

            'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.EventosSocios_Alta, String), (mstrConn), (Session("sUserId").ToString()))
            'btnAlta.Visible = lbooPermiAlta

            'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.EventosSocios_Baja, String), (mstrConn), (Session("sUserId").ToString()))

            'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.EventosSocios_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
            'btnModi.Visible = lbooPermiModi

            'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
            'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
        End Sub


#End Region

#Region "Inicializacion de Variables"
        Public Sub mInicializar()
            mstrItemCons = Request.QueryString("id")
            clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            If cmbEvento.Valor <> 0 Then
                mDatosEvento(cmbEvento.Valor)
            End If

            cmbLugarUno.Valor = hdnLugarUno.Text
            cmbLugarDos.Valor = hdnLugarDos.Text
            cmbLugarTres.Valor = hdnLugarTres.Text
        End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.EditItemIndex = -1
                If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = 0
                Else
                    grdDato.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultar(grdDato)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


#End Region

#Region "Seteo de Controles"

        Private Sub mLimpiarFiltros()
            hdnId.Text = ""
            usrSociFil.Limpiar()
            cmbEvenFil.SelectedIndex = 0

            mConsultarGrilla()

        End Sub

        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            btnBaja.Enabled = Not (pbooAlta)
            'btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
        End Sub

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                mLimpiar()

                hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

                mCrearDataSet(hdnId.Text)


                If mdsDatos.Tables(0).Rows.Count > 0 Then
                    With mdsDatos.Tables(0).Rows(0)
                        lblTitu.Text = "Socio/Evento: " + .Item("_socio") + "/" + .Item("_evento")
                        If Not IsDBNull(.Item("soev_evlu1_id")) Then cmbLugarUno.Valor = .Item("soev_evlu1_id")
                        If Not IsDBNull(.Item("soev_evlu2_id")) Then cmbLugarDos.Valor = .Item("soev_evlu2_id")
                        If Not IsDBNull(.Item("soev_evlu3_id")) Then cmbLugarTres.Valor = .Item("soev_evlu3_id")
                        If Not IsDBNull(.Item("soev_entr1_fecha")) Then txtFechaRetiroUno.Valor = .Item("soev_entr1_fecha")
                        If Not IsDBNull(.Item("soev_entr2_fecha")) Then txtFechaRetiroDos.Valor = .Item("soev_entr2_fecha")
                        If Not IsDBNull(.Item("soev_entr3_fecha")) Then txtFechaRetiroTres.Valor = .Item("soev_entr3_fecha")
                        txtNroDde1.Valor = .Item("soev_ent1_desde_nume")
                        txtNroHta1.Valor = .Item("soev_ent1_hasta_nume")
                        txtNroDde2.Valor = .Item("soev_ent2_desde_nume")
                        txtNroHta2.Valor = .Item("soev_ent2_hasta_nume")
                        txtNroDde3.Valor = .Item("soev_ent3_desde_nume")
                        txtNroHta3.Valor = .Item("soev_ent3_hasta_nume")
                        usrSocios.Valor = .Item("soev_soci_id")
                        cmbEvento.Valor = .Item("soev_even_id")
                        cmbEvenAux.Valor = .Item("soev_even_id")
                        txtEnt1.Text = .Item("_even_evtr1_id")
                        txtEnt2.Text = .Item("_even_evtr2_id")
                        txtEnt3.Text = .Item("_even_evtr3_id")
                        mControlaNum(.Item("_cont_nume1"), 1)
                        mControlaNum(.Item("_cont_nume2"), 2)
                        mControlaNum(.Item("_cont_nume3"), 3)
                        txtObse.Valor = .Item("soev_obse")
                        If Not .IsNull("soev_baja_fecha") Then
                            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("soev_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                        Else
                            lblBaja.Text = ""
                        End If
                    End With

                    mSetearEditor("", False)
                    mMostrarPanel(True)


                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mAgregar()
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = True
            btnAlta.Visible = False
            btnBaja.Visible = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
        End Sub

        Private Sub mLimpiar()

            hdnId.Text = ""

            mCrearDataSet("")
            txtSociCategoria.Text = ""
            txtSociEstado.Text = ""
            txtSocioDeuda.Text = ""
            lblBaja.Text = ""
            cmbLugarUno.Valor = ""
            cmbLugarDos.Valor = ""
            cmbLugarTres.Valor = ""
            hdnLugarUno.Text = ""
            hdnLugarDos.Text = ""
            hdnLugarTres.Text = ""
            txtNroDde1.Text = ""
            txtNroHta1.Text = ""
            txtNroDde2.Text = ""
            txtNroHta2.Text = ""
            txtNroDde3.Text = ""
            txtNroHta3.Text = ""
            txtCantiUno.Text = ""
            txtCantiDos.Text = ""
            txtCantiTres.Text = ""
            lblConNume1.Text = ""
            lblConNume2.Text = ""
            lblConNume3.Text = ""
            txtEnt1.Text = ""
            txtEnt2.Text = ""
            txtEnt3.Text = ""
            txtObse.Text = ""
            lblTitu.Text = ""
            hdnCocktailRRII.Text = ""
            txtFechaRetiroUno.Text = ""
            txtFechaRetiroDos.Text = ""
            txtFechaRetiroTres.Text = ""
            usrSocios.Limpiar()
            cmbEvento.SelectedIndex = -1
            'cmbEvento.Valor = cmbEvenFil.Valor
            If Not cmbEvento.Valor Is DBNull.Value Then
                mDatosEvento(cmbEvento.Valor)
            End If

            mSetearEditor("", True)

        End Sub

        Private Sub mCerrar()
            mConsultarGrilla()
        End Sub

        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            If (pbooVisi) Then
                hdnPage.Text = " "
            Else
                hdnPage.Text = ""
            End If
            panDato.Visible = pbooVisi
            'grdDato.Visible = Not panDato.Visible
            'panFiltro.Visible = Not panDato.Visible
            btnAgre.Visible = Not panDato.Visible
            btnList.Visible = Not pbooVisi
            If pbooVisi Then
                grdDato.DataSource = Nothing
                grdDato.DataBind()

            Else
                Session(mstrTabla) = Nothing
                mdsDatos = Nothing
            End If

            panDato.Visible = pbooVisi

            panCabecera.Visible = True

        End Sub

#End Region

#Region "Opciones de ABM"
        Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()

        End Sub
        Private Sub mAlta()
            Try
                Dim lstrId As String
                Dim blnExisteSocio As Boolean

                blnExisteSocio = mConsultarExisteSocio()

                mGuardarDatos(False)

                Dim lobjEventosSocios As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
                lstrId = lobjEventosSocios.Alta()

                ' GM -----------------------
                Dim _mdsDatos As New DataSet
                _mdsDatos = mdsDatos
                Dim _lstrId As String
                _lstrId = lstrId

                mLimpiar()

                'mdsDatos = _mdsDatos
                'lstrId = _lstrId
                ' --------------------------

                If chkImpri.Checked Then
                    mImprimirTalon(_lstrId, _mdsDatos)
                End If

                'mConsultar(grdDato)
                'mMostrarPanel(False)
                'mLimpiar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub mModi()
            Try

                mGuardarDatos(False)
                Dim lobjEventosSocios As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
                Dim blnExiste As Boolean
                Dim strBaja As String
                blnExiste = mConsultarExisteSocio()

                If (hdnLugarUno.Text = "" And hdnLugarDos.Text = "" And hdnLugarTres.Text = "") Then
                    strBaja = usrSocios.Valor & ", " & cmbEvento.Valor
                    lobjEventosSocios.Baja(strBaja)
                Else
                    If blnExiste Then
                        lobjEventosSocios.Modi()
                    Else
                        lobjEventosSocios.Alta()
                    End If
                End If

                ' GM -----------------------
                Dim _mdsDatos As New DataSet
                _mdsDatos = mdsDatos
                Dim _lstrId As String
                _lstrId = hdnId.Text

                mLimpiar()

                'mdsDatos = _mdsDatos
                'hdnId.Text = _lstrId
                ' --------------------------

                If chkImpri.Checked Then
                    mImprimirTalon(_lstrId, _mdsDatos)
                End If

                'mConsultar(grdDato)
                'mLimpiar()
                ''mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mBaja()
            Try
                Dim lintPage As Integer = grdDato.CurrentPageIndex

                Dim lobjEventosSocios As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
                lobjEventosSocios.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

                grdDato.CurrentPageIndex = 0


                mConsultar(grdDato)

                If (lintPage < grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = lintPage

                End If

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mControlaNum(ByVal pstrvalor As String, ByVal pintEnt As Integer)
            If pstrvalor = "S" Then
                If pintEnt = 1 Then
                    lblConNume1.Text = "Controla numeraci�n"
                    txtNroDde1.Enabled = True
                    txtNroHta1.Enabled = True
                ElseIf pintEnt = 2 Then
                    lblConNume2.Text = "Controla numeraci�n"
                    txtNroDde2.Enabled = True
                    txtNroHta2.Enabled = True
                ElseIf pintEnt = 3 Then
                    lblConNume3.Text = "Controla numeraci�n"
                    txtNroDde3.Enabled = True
                    txtNroHta3.Enabled = True
                End If
            Else
                If pintEnt = 1 Then
                    lblConNume1.Text = ""
                    txtNroDde1.Enabled = False
                    txtNroHta1.Enabled = False
                ElseIf pintEnt = 2 Then
                    lblConNume2.Text = ""
                    txtNroDde2.Enabled = False
                    txtNroHta2.Enabled = False
                ElseIf pintEnt = 3 Then
                    lblConNume3.Text = ""
                    txtNroDde3.Enabled = False
                    txtNroHta3.Enabled = False
                End If
            End If
        End Sub

        Private Sub mValidaDatos(ByVal pboolAlta As Boolean)


            With mdsDatos.Tables(0).Rows(0)

                If (usrSocios.Valor Is DBNull.Value) Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el socio.")
                Else
                    If usrSocios.Valor = 0 Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar el socio.")
                    End If
                End If
                If (cmbEvento.SelectedItem.Text = "(Seleccione)") Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar un evento.")
                End If
                ' si controla numeracion
                If hdnConNume1.Text <> "" Then
                    If txtCantiUno.Valor.Trim.Length > 0 Then
                        If txtCantiUno.Valor > 1 Then
                            If txtNroDde1.Valor.Trim.Length = 0 Or Me.txtNroHta1.Valor.Trim.Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("El evento controla numeraci�n de entradas. Debe ingresar el rango de entradas entregadas.")
                            End If
                        End If
                    End If
                    If Not (txtNroDde1.Valor.Trim.Length = 0) And Not (txtNroHta1.Valor.Trim.Length = 0) Then
                        If CType(txtNroDde1.Valor, Int32) > CType(txtNroHta1.Valor, Int32) Then
                            Throw New AccesoBD.clsErrNeg("N�mero desde no puede ser mayor al n�mero hasta.")
                        End If
                    End If
                Else
                    ' si no controla numeracion
                    If Not (txtNroDde1.Valor.Trim.Length = 0) Or Not (txtNroHta1.Valor.Trim.Length = 0) Then
                        Throw New AccesoBD.clsErrNeg("El evento no controla numeraci�n de entradas. No debe ingresar el rango de entradas entregadas.")
                    End If
                End If

                ' si controla numeracion 
                If hdnConNume2.Text <> "" Then

                    If txtCantiDos.Valor.Trim.Length > 0 Then
                        If txtCantiDos.Valor > 1 Then
                            If txtNroDde2.Valor.Trim.Length = 0 Or Me.txtNroHta2.Valor.Trim.Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("El evento controla numeraci�n de entradas. Debe ingresar el rango de entradas entregadas.")
                            End If
                        End If
                    End If
                    If Not (txtNroDde2.Valor.Trim.Length = 0) And Not (txtNroHta2.Valor.Trim.Length = 0) Then
                        If CType(txtNroDde2.Valor, Int32) > CType(txtNroHta2.Valor, Int32) Then
                            Throw New AccesoBD.clsErrNeg("N�mero desde no puede ser mayor al n�mero hasta.")
                        End If
                    End If
                Else
                    ' si no controla numeracion
                    If Not (txtNroDde2.Valor.Trim.Length = 0) Or Not (txtNroHta2.Valor.Trim.Length = 0) Then
                        Throw New AccesoBD.clsErrNeg("El evento no controla numeraci�n de entradas. No debe ingresar el rango de entradas entregadas.")
                    End If
                End If

                ' si controla numeracion
                If hdnConNume3.Text <> "" Then
                    If txtCantiTres.Valor.Trim.Length > 0 Then
                        If txtCantiTres.Valor > 1 Then
                            If txtNroDde3.Valor.Trim.Length = 0 Or Me.txtNroHta3.Valor.Trim.Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("El evento controla numeraci�n de entradas. Debe ingresar el rango de entradas entregadas.")
                            End If
                        End If
                    End If
                    If Not (txtNroDde3.Valor.Trim.Length = 0) And Not (txtNroHta3.Valor.Trim.Length = 0) Then
                        If CType(txtNroDde3.Valor, Int32) > CType(txtNroHta3.Valor, Int32) Then
                            Throw New AccesoBD.clsErrNeg("N�mero desde no puede ser mayor al n�mero hasta.")
                        End If
                    End If
                Else
                    ' si no controla numeracion
                    If Not (txtNroDde3.Valor.Trim.Length = 0) Or Not (txtNroHta3.Valor.Trim.Length = 0) Then
                        Throw New AccesoBD.clsErrNeg("El evento no controla numeraci�n de entradas. No debe ingresar el rango de entradas entregadas.")
                    End If
                End If
            End With
        End Sub

        Private Function mGuardarDatos(ByVal pboolAlta As Boolean) As DataSet

            mValidaDatos(pboolAlta)

            With mdsDatos.Tables(0).Rows(0)
                .Item("soev_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("soev_soci_id") = usrSocios.Valor
                .Item("soev_even_id") = cmbEvento.Valor

                Dim fh1 As Array = ((hdnFechaRetiroUno.Text).Split(" ")(0)).Replace("-", "/").Split("/")
                Dim fh2 As Array = ((hdnFechaRetiroDos.Text).Split(" ")(0)).Replace("-", "/").Split("/")
                Dim fh3 As Array = ((hdnFechaRetiroTres.Text).Split(" ")(0)).Replace("-", "/").Split("/")

                If Not hdnLugarUno.Text = "" Then .Item("soev_entr1_fecha") = IIf(hdnFechaRetiroUno.Text = "", System.DateTime.Now, Convert.ToDateTime(fh1(1) + "/" + fh1(0) + "/" + fh1(2))) Else .Item("soev_entr1_fecha") = DBNull.Value
                If Not hdnLugarDos.Text = "" Then .Item("soev_entr2_fecha") = IIf(hdnFechaRetiroDos.Text = "", System.DateTime.Now, Convert.ToDateTime(fh2(1) + "/" + fh2(0) + "/" + fh2(2))) Else .Item("soev_entr2_fecha") = DBNull.Value
                If Not hdnLugarTres.Text = "" Then .Item("soev_entr3_fecha") = IIf(hdnFechaRetiroTres.Text = "", System.DateTime.Now, Convert.ToDateTime(fh3(1) + "/" + fh3(0) + "/" + fh3(2))) Else .Item("soev_entr3_fecha") = DBNull.Value
                If Not txtNroDde1.Valor.Trim.Length = 0 Then .Item("soev_ent1_desde_nume") = txtNroDde1.Valor Else .Item("soev_ent1_desde_nume") = DBNull.Value
                If Not txtNroHta1.Valor.Trim.Length = 0 Then .Item("soev_ent1_hasta_nume") = txtNroHta1.Valor Else .Item("soev_ent1_hasta_nume") = DBNull.Value
                If Not txtNroDde2.Valor.Trim.Length = 0 Then .Item("soev_ent2_desde_nume") = txtNroDde2.Valor Else .Item("soev_ent2_desde_nume") = DBNull.Value
                If Not txtNroHta2.Valor.Trim.Length = 0 Then .Item("soev_ent2_hasta_nume") = txtNroHta2.Valor Else .Item("soev_ent2_hasta_nume") = DBNull.Value
                If Not txtNroDde3.Valor.Trim.Length = 0 Then .Item("soev_ent3_desde_nume") = txtNroDde3.Valor Else .Item("soev_ent3_desde_nume") = DBNull.Value
                If Not txtNroHta3.Valor.Trim.Length = 0 Then .Item("soev_ent3_hasta_nume") = txtNroHta3.Valor Else .Item("soev_ent3_hasta_nume") = DBNull.Value
                '.Item("soev_ent1_desde_nume") = txtNroDde1.Valor
                '.Item("soev_ent1_hasta_nume") = txtNroHta1.Valor
                '.Item("soev_ent2_desde_nume") = txtNroDde2.Valor
                '.Item("soev_ent2_hasta_nume") = txtNroHta2.Valor
                '.Item("soev_ent3_desde_nume") = txtNroDde3.Valor
                '.Item("soev_ent3_hasta_nume") = txtNroHta3.Valor
                .Item("soev_obse") = txtObse.Valor

                If Not hdnLugarUno.Text = "" Then .Item("soev_evlu1_id") = hdnLugarUno.Text Else .Item("soev_evlu1_id") = DBNull.Value
                If Not hdnLugarDos.Text = "" Then .Item("soev_evlu2_id") = hdnLugarDos.Text Else .Item("soev_evlu2_id") = DBNull.Value
                If Not hdnLugarTres.Text = "" Then .Item("soev_evlu3_id") = hdnLugarTres.Text Else .Item("soev_evlu3_id") = DBNull.Value

                For i As Integer = 0 To .Table.Columns.Count - 1
                    If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
                        .Item(i) = DBNull.Value
                    End If
                Next
            End With

            Return mdsDatos
        End Function

        Public Sub mCrearDataSet(ByVal pstrId As String)

            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla

            If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
                mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
            End If

            Session(mstrTabla) = mdsDatos

        End Sub


#End Region

#Region "Eventos de Controles"



        Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub

        Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub

        Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub


#End Region

#Region "Detalle"

        Private Sub mConsultarGrilla()
            Try
                'mConsultar(grdDato)
                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try

        End Sub

        Private Sub mDatosEvento(ByVal pintEven As Integer)
            'Dim ldsDatos As DataSet
            'Try
            '    mstrCmd = "exec eventos_socios_consul " & pintEven
            '    ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
            '    If ldsDatos.Tables(0).Rows.Count <> 0 Then
            '        With ldsDatos.Tables(0).Rows(0)
            '            txtEnt1.Text = .Item("even_evtr1_desc")
            '            txtEnt2.Text = .Item("even_evtr2_desc")
            '            If .Item("even_cont1_nume") Then
            '                lblConNume1.Text = "Controla numeraci�n"
            '            Else
            '                lblConNume1.Text = ""
            '            End If
            '            If .Item("even_cont2_nume") Then
            '                lblConNume2.Text = "Controla numeraci�n"
            '            Else
            '                lblConNume2.Text = ""
            '            End If
            '        End With
            '    End If
            'Catch ex As Exception
            '    clsError.gManejarError(Me, ex)
            'End Try
        End Sub

        Public Sub mConsultar(ByVal grdDato As DataGrid, Optional ByVal pBuscFiltro As Byte = 0)
            Try
                mstrCmd = "exec " + mstrTabla + "_consul"
                mstrCmd += "  @soev_soci_id=" + usrSociFil.Valor.ToString
                mstrCmd += ",  @soev_even_id=" + cmbEvenFil.Valor.ToString
                clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Function mConsultarExisteSocio() As Boolean
            Try
                Dim dtsDatos As DataSet

                mstrCmd = "exec eventos_socio_existe_consul "
                mstrCmd += "  @soci_id=" + usrSocios.Valor.ToString
                mstrCmd += ",  @even_id=" + cmbEvento.Valor.ToString

                dtsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd.ToString)

                If dtsDatos.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Function

        Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
            Try
                grdDato.CurrentPageIndex = 0
                mConsultar(grdDato, 1)
                mMostrarPanel(False)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        'Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        '    Try

        '        Dim lstrRptName As String = "SociosEventos"
        '        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        '        lstrRpt += "&soev_soci_id=" + usrSociFil.Valor.ToString
        '        lstrRpt += "&soev_even_id=" + cmbEvenFil.Valor.ToString

        '        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        '        Response.Redirect(lstrRpt)

        '    Catch ex As Exception
        '        clsError.gManejarError(Me, ex)
        '    End Try
        'End Sub

        Private Sub mImprimirTalon(ByVal pstrId As String, ByVal _mdsDatos As DataSet)
            Try
                Dim soev_soci_id As Int32
                Dim soev_even_id As Int32
                With _mdsDatos.Tables(0).Rows(0)
                    soev_soci_id = .Item("soev_soci_id")
                    soev_even_id = .Item("soev_even_id")
                End With

                Dim params As String
                Dim lstrRptName As String = "SociosEventosTalon"
                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

                If (pstrId.Trim().Length > 0) Then
                    params += "&soev_id=" & pstrId
                Else
                    params += "&soev_id=0"
                End If

                params += "&soci_id=" & soev_soci_id.ToString
                params += "&even_id=" & soev_even_id.ToString

                lstrRpt += params

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

                Response.Redirect(lstrRpt, True)

                'Dim lsbMsg As String
                'Dim lstrRepo As String = System.Configuration.ConfigurationSettings.AppSettings("conRepo").ToString()
                'Dim lstrRepoDire As String = System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString()

                'lsbMsg = "<OBJECT ID=RSClientPrint CLASSID='CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3' CODEBASE='http://" & lstrRepo & "/" & lstrRepoDire & "/rptproxy.aspx?rs:Command=Get&rc:GetImage=8.00.1038.00rsclientprint.cab#Version=2000,080,1038,000' VIEWASTEXT></OBJECT>"
                'lsbMsg = lsbMsg & "<SCRIPT language='javascript'>"
                'lsbMsg = lsbMsg & "RSClientPrint.MarginLeft = 10;"
                'lsbMsg = lsbMsg & "RSClientPrint.MarginTop = 10;"
                'lsbMsg = lsbMsg & "RSClientPrint.MarginRight = 0;"
                'lsbMsg = lsbMsg & "RSClientPrint.MarginBottom = 10;"
                'lsbMsg = lsbMsg & "RSClientPrint.PageHeight = 297;"
                'lsbMsg = lsbMsg & "RSClientPrint.PageWidth = 210;"
                'lsbMsg = lsbMsg & "RSClientPrint.Culture = 3082;"
                'lsbMsg = lsbMsg & "RSClientPrint.UICulture = 10;"
                'lsbMsg = lsbMsg & "var ldecRand = Math.random();"
                'lsbMsg = lsbMsg & "ldecRand = ldecRand * 1000;"
                'lsbMsg = lsbMsg & "ldecRand = Math.round(ldecRand);"
                'lsbMsg = lsbMsg & "var lstrRand = new Number(ldecRand).toString();"
                'lsbMsg = lsbMsg & "try { "
                'lsbMsg = lsbMsg & " RSClientPrint.Print('http://" & lstrRepo & "/sra/rptproxy.aspx', '/" & lstrRepoDire & "/SociosEventosTalon&soev_id=" & pstrId & "&soci_id=" & soev_soci_id.ToString & "&even_id=" & soev_even_id.ToString & "&random='+lstrRand,'SociosEventosTalon');"
                'lsbMsg = lsbMsg & "} catch(e) { "
                'lsbMsg = lsbMsg & " alert('Error al intentar efectuar la impresi�n');"
                'lsbMsg = lsbMsg & "} "
                ''lsbMsg = lsbMsg & "document.location.href='EventosSocios.aspx';"
                'lsbMsg = lsbMsg & "</SCRIPT>"
                'Response.Write(lsbMsg)

                'lstrRpt += "&soev_id=" + pstrId
                'lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                'Response.Redirect(lstrRpt)

            Catch ex As Exception
                mLimpiar()
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
        End Sub

        Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            mAgregar()
        End Sub

#End Region


    End Class
End Namespace
