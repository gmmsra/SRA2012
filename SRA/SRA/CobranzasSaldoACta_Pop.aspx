<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CobranzasSaldoACta_Pop" CodeFile="CobranzasSaldoACta_Pop.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Saldos A Cuenta</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="Salida();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colspan="2" vAlign="top" align="right">&nbsp;
									<asp:ImageButton id="imgClose" runat="server" ImageUrl="imagenes\Close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
							</TR>
							<TR>
								<TD align="right">
									<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
								<TD width="100%">
									<UC1:CLIE id="usrClie" runat="server" width="100%" FilCUIT="True" Tabla="Clientes" Saltos="1,1,1"
										autopostback="true" AceptaNull="False" FilClieNume="True" FilSociNume="True" MuestraDesc="False"
										FilDocuNume="True" FilClaveUnica="True"></UC1:CLIE>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2" vAlign="top" align="center" onclick="Variable=1;">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										CellPadding="1" GridLines="None" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="sact_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="sact_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="_acti_desc" HeaderText="Actividad"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR HEIGHT="10">
								<TD colspan="2"></TD>
							</TR>
							<TR>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" BorderStyle="Solid" BorderWidth="1px">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="right">
														<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
													<TD>
														<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Obligatorio="True" EsDecimal="true"
															Width="70px"></cc1:numberbox></TD>
													<TD align="right">
														<asp:Label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:Label></TD>
													<TD>
														<cc1:combobox class="combo" id="cmbActi" runat="server" Width="350px"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD onclick="Variable=1;" align="center" colSpan="4"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" CausesValidation="False" cssclass="boton" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" CausesValidation="False" cssclass="boton" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" CausesValidation="False" cssclass="boton" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		var Variable;
		Variable="";
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtImpo"]!= null)
			document.all["txtImpo"].focus();
			
		function Salida()
		{
			if(window.event.srcElement==null && Variable=="")
			{
				try
				{
					window.opener.document.all['hdnDatosPop'].value='1';
					window.opener.__doPostBack('hdnDatosPop','');
				}
				catch(e){;}
			}
		}
		
		function usrClie_onchange()
		{
			Variable=1;
		}
		</SCRIPT>
	</BODY>
</HTML>
