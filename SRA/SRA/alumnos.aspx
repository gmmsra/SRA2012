<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Migrated_alumnos" CodeFile="alumnos.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Alumnos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function btnDA_click()
		{
			gAbrirVentanas("DebitoAutomatico.aspx?alum=S&origen=Alumnos&id=" + document.all("hdnId").value+"&amp;"+"clienteId=" + document.all("usrClie:txtId").value  , 3, "700","300","100","100");
		}

		function mCargarProvincias(pPais)
		{
		    var sFiltro = pPais.value;
		    LoadComboXML("provincias_cargar", sFiltro, "cmbExclPcia", "S");
		}
		function mCargarLocalidades(pPcia)
		{
		    var sFiltro = pPcia.value 
		    LoadComboXML("localidades_cargar", sFiltro, "cmbExclLoca", "N");
		}
		function usrClie_onchange()
		{
			if(document.all("usrClie:txtId").value!="")
			{
				document.all("usrFactClie:txtCodi").value = document.all("usrClie:txtCodi").value;
				document.all("usrFactClie:txtCodi").onchange();

				var sRet=EjecutarMetodoXML("Utiles.ObtenerIdAlumXCliente", document.all("usrClie:txtId").value + ";" + document.all("hdnInseId").value);
				if(""!=sRet)
				{
					if (Ventana!= null)
					{
						if(Ventana.confirm("El cliente ya posee un alumno asociado. Desea utilizar ese alumno?"))
						{
							document.all("hdnAlumId").value=sRet;
							__doPostBack('hdnAlumId','');
						}
					}
				}
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Alumnos</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																	ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																	OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE class="FdoFld" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 17px" align="right">
																			<asp:Label id="lblApelFil" runat="server" cssclass="titulo">Apellido:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%">
																			<CC1:TEXTBOXTAB id="txtApelFil" runat="server" cssclass="cuadrotexto" Width="250px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:label id="lblLegaFil" runat="server" cssclass="titulo">Legajo:</asp:label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtLegaFil" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																				AutoPostback="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:label id="lblCuitFil" runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;</TD>
																		<TD>
																			<CC1:CUITBOX id="txtCuitFil" runat="server" cssclass="cuadrotexto" Width="200px" AceptaNull="False"></CC1:CUITBOX></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:label id="lblDocuFil" runat="server" cssclass="titulo">N� Documento:</asp:label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbDocuTipoFil" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:combobox>
																			<cc1:numberbox id="txtDocuNumeFil" runat="server" cssclass="cuadrotexto" Width="143px" esdecimal="False"
																				MaxValor="9999999999999"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD>
																			<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" SoloBusq="True" Ancho="800" FilDocuNume="True"
																				MuestraDesc="False" FilTipo="A" FilLegaNume="True" Saltos="1,2" Tabla="Clientes"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mSeleccionar">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="15px"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="Linkbutton3" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
														<img src="images/sele.gif" border="0" alt="Seleccionar Alumno" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
														<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="alum_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="alum_lega" HeaderText="Legajo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_clie_apel" HeaderText="Apellido"></asp:BoundColumn>
											<asp:BoundColumn DataField="_clie_docu_nume" HeaderText="Nro.Doc"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_tacl_id" HeaderText="_tacl_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_tacl_tarj"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_tacl_nume"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_tacl_vcto_fecha"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="alum_beca_id" HeaderText="alum_beca_id"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="middle"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
										ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
										ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Agregar un Nuevo Alumno"></CC1:BOTONIMAGEN></TD>
								<TD align="right" colSpan="2">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Alumno</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkEstu" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" Height="21px" CausesValidation="False"> Estudios</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkOcup" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" Height="21px" CausesValidation="False"> Ocupaciones</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkFunc" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" Height="21px" CausesValidation="False"> Cargos</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkExplo" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" Height="21px" CausesValidation="False"> Explotaciones</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD width="20%" align="right">
																			<asp:Label id="lblLega" runat="server" cssclass="titulo">Legajo:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtLega" runat="server" cssclass="cuadrotexto" Width="80px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" width="20%" align="right">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<UC1:CLIE id="usrClie" runat="server" FilDocuNume="True" FilLegaNume="True" Saltos="1,1,1"
																				Tabla="Clientes" Obligatorio="true" FilCUIT="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblIngrFecha" runat="server" cssclass="titulo">Fecha de ingreso:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtIngrFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR style="DISPLAY: none">
																		<TD vAlign="top" width="20%" align="right">
																			<asp:Label id="lblFactClie" runat="server" cssclass="titulo">Cliente Fact.:</asp:Label>&nbsp;</TD>
																		<TD>
																			<UC1:CLIE id="usrFactClie" runat="server" FilDocuNume="True" MuestraDesc="False" FilLegaNume="True"
																				Saltos="1,1,1" Tabla="Clientes" Obligatorio="true" FilCUIT="True"></UC1:CLIE></TD>
																	</TR>
																	<TR style="DISPLAY: none">
																		<TD width="20%" align="right">
																			<asp:Label id="lblBeca" runat="server" cssclass="titulo">Beca:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbBeca" class="combo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" EnterPorTab="False"
																				TextMode="MultiLine" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<TABLE width="100%">
																				<TR>
																					<TD style="WIDTH: 120px" align="center">
																						<asp:Label id="lblDAut" runat="server" cssclass="titulo" Width="120px" Height="6px">D�b.Autom�tico:</asp:Label></TD>
																					<TD style="WIDTH: 5%" align="left">
																						<asp:Label id="lblDA" runat="server" cssclass="titulo">NO</asp:Label></TD>
																					<TD align="left">
																						<asp:Label id="lblDebAuto" runat="server" cssclass="titulo"></asp:Label></TD>
																					<TD style="WIDTH: 10%" align="right"><BUTTON style="WIDTH: 90px" id="btnDA" class="boton" onclick="btnDA_click();" runat="server"
																							value="Detalles">D�bito Auto.</BUTTON>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panEstu" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdEstu" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosEstu"
																				OnPageIndexChanged="grdEstu_PageChanged" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="escl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estu_desc" ReadOnly="True" HeaderText="Estudio"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblEstu" runat="server" cssclass="titulo">Estudio:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbEstu" class="combo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaEstu" runat="server" cssclass="boton" Width="100px" Text="Agregar Estu."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaEstu" runat="server" cssclass="boton" Width="100px" Text="Eliminar Estu."></asp:Button>&nbsp;
																			<asp:Button id="btnModiEstu" runat="server" cssclass="boton" Width="100px" Text="Modificar Estu."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpEstu" runat="server" cssclass="boton" Width="100px" Text="Limpiar Estu."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panOcup" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="Table4" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2" align="left">
																			<asp:datagrid id="grdOcup" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosOcup"
																				OnPageIndexChanged="grdOcup_PageChanged" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="occl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_ocup_desc" HeaderText="Ocupaci&#243;n"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblOcup" runat="server" cssclass="titulo">Ocupaci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbOcup" class="combo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaOcup" runat="server" cssclass="boton" Width="100px" Text="Agregar Ocup."></asp:Button>&nbsp;
																			<asp:Button id="btnBajaOcup" runat="server" cssclass="boton" Width="100px" Text="Eliminar Ocup."></asp:Button>&nbsp;
																			<asp:Button id="btnModiOcup" runat="server" cssclass="boton" Width="100px" Text="Modificar Ocup."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpOcup" runat="server" cssclass="boton" Width="100px" Text="Limpiar Ocup."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panFunc" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="Table5" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdFunc" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosFunc"
																				OnPageIndexChanged="grdFunc_PageChanged" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="fucl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_func_desc" HeaderText="Cargo"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblFunc" runat="server" cssclass="titulo">Cargo:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbFunc" class="combo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaFunc" runat="server" cssclass="boton" Width="100px" Text="Agregar Cargo"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaFunc" runat="server" cssclass="boton" Width="100px" Text="Eliminar Cargo"></asp:Button>&nbsp;
																			<asp:Button id="btnModiFunc" runat="server" cssclass="boton" Width="100px" Text="Modificar Cargo"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpFunc" runat="server" cssclass="boton" Width="100px" Text="Limpiar Cargo"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panExpl" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="tabExpl" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdExpl" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDatosExpl"
																				OnPageIndexChanged="grdExpl_PageChanged" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="excl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_exti_desc" HeaderText="Tipo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_pcia_desc" HeaderText="Provincia"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_loca_desc" HeaderText="Localidad"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExti" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbExti" class="combo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExclPais" runat="server" cssclass="titulo">Pais:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:combobox id="cmbExclPais" class="combo" runat="server" Width="260px" onchange="mCargarProvincias(this)"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExclPcia" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbExclPcia" class="combo" runat="server" Width="260px" onchange="mCargarLocalidades(this)"
																				NomOper="provincias_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExclLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbExclLoca" class="combo" runat="server" Width="260px" NomOper="localidades_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaExpl" runat="server" cssclass="boton" Width="100px" Text="Agregar Expl"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaExpl" runat="server" cssclass="boton" Width="100px" Text="Eliminar Expl"></asp:Button>&nbsp;
																			<asp:Button id="btnModiExpl" runat="server" cssclass="boton" Width="100px" Text="Modificar Expl"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpExpl" runat="server" cssclass="boton" Width="100px" Text="Limpiar Expl"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnEsclId" runat="server"></asp:textbox><asp:textbox id="hdnOcclId" runat="server"></asp:textbox><asp:textbox id="hdnFuclId" runat="server"></asp:textbox><asp:textbox id="hdnExclId" runat="server"></asp:textbox><asp:textbox id="hdnAlumId" runat="server"></asp:textbox><asp:textbox id="hdnInseId" runat="server"></asp:textbox><asp:textbox id="hdnTarjId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
