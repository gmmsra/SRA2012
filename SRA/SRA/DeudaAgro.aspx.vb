Namespace SRA

Partial Class DeudaAgro
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtFecha As NixorControls.DateBox
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrConn As String
    Private mstrDeuda As String

    Private Enum Columnas As Integer
        Chk = 0
        CompId = 1
        Anio = 2
        Perio = 3
        PetiId = 4
        FechaIni = 6
        FechaFina = 7
        ImpoCuota = 8
        Sele = 9
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If Not Page.IsPostBack Then
                mInicializar()
                mConsultar()
                If mstrDeuda <> "" Then
                    mSeleccionar(mstrDeuda, False)
                Else
                    mChequearTodos()
                End If
                If txtDeudaAnterior.Text <> "0.00" Then
                    lblBimAnteDesc.Text = clsSQLServer.gCampoValorConsul(mstrConn, "deuda_social_descrip_consul " & usrSoci.Valor.ToString, "deuda_descrip")
                    mCalcularBimestreDeuda()
                Else
                    lblBimAnteDesc.Text = ""
                End If
                hdnBimAnteDesc.Text = lblBimAnteDesc.Text
                'mCalcularTotales()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mInicializar()
        If Request("anio") Is Nothing And Request("sel") Is Nothing Then
            btnAceptar.Visible = False
        End If

        mstrDeuda = Request("Deuda")

        usrSoci.EnableViewState = False
        usrSoci.Valor = Request("soci_id")
        usrSoci.Activo = False
        hdnSociAnte.Text = Request("soci_id")
        txtFechaDesde.Fecha = Request("fecha_desde")
        txtFechaHasta.Fecha = Request("fecha_hasta")

    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"

    Private Sub mDeterminarDeuda()
        Dim lstrDeuda As String = ""
        Dim lDr As DataRow

        Dim ds As DataSet = Session("ds")
        For Each lDr In ds.Tables(0).Rows
            If lDr.Item("_chk") = 1 Then
               If lstrDeuda <> "" Then lstrDeuda = lstrDeuda & ","
               lstrDeuda = lstrDeuda & lDr.Item("comp_id").ToString()
            End If
        Next

        Session("sDeuda") = lstrDeuda
        hdnSele.Text = lstrDeuda

    End Sub

    Private Sub mSeleccionar(ByVal pstrDeuda As String, ByVal pbooTodo As Boolean)
        Dim lstrDeuda As String = ""
        Dim ldecImpo As Decimal = 0
        Dim lDr As DataRow

        Session("sDeuda") = pstrDeuda
        pstrDeuda = "," & pstrDeuda & ","

        Dim ds As DataSet = DirectCast(grdConsulta.DataSource, DataSet)
        For Each lDr In ds.Tables(0).Rows
            If pbooTodo Or pstrDeuda.IndexOf("," & lDr.Item("comp_id") & ",") <> -1 Then
               lDr.Item("_chk") = 1
               If lstrDeuda <> "" Then lstrDeuda = lstrDeuda & ","
               lstrDeuda = lstrDeuda & lDr.Item("comp_id").ToString()
               ldecImpo = ldecImpo + Convert.ToDecimal(lDr.Item("cuota"))
            Else
               lDr.Item("_chk") = 0
            End If
        Next

        grdConsulta.DataBind()
        Session("ds") = ds

        For Each oDataItem As DataGridItem In grdConsulta.Items
            If oDataItem.Cells(Columnas.Sele).Text = 1 Then
                DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked = True
            Else
                DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked = False
            End If
        Next

        Session("sDeuda") = lstrDeuda
        hdnSele.Text = lstrDeuda
        mDeudaAnterior()
        txtTotalTotal.Text = ldecImpo.ToString()

    End Sub

    Private Sub mDeudaAnterior()
        Dim ldecDeuda As Decimal = 0
        Dim lbooAnte As Boolean = True
        Dim lDr As DataRow

        Dim ds As DataSet = DirectCast(grdConsulta.DataSource, DataSet)
        For Each lDr In ds.Tables(0).Rows
            If lbooAnte And lDr.Item("_chk") = 0 Then
                ldecDeuda += lDr.Item("cuota")
            End If
            If lDr.Item("_chk") = 1 Then lbooAnte = False
        Next

        txtDeudaAnterior.Text = Format(ldecDeuda + ds.Tables(1).Rows(0).Item("deuda_anterior"), "#,##0.00").ToString()

    End Sub

    Private Sub mGuardarTodosIds()
        Dim lDr As DataRow
        Dim lstrDeuda As String = ""
        Dim ldecImpo As Decimal = 0
        Dim ds As DataSet = Session("ds")

        For Each oDataItem As DataGridItem In grdConsulta.Items
            If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
                oDataItem.Cells(Columnas.Sele).Text = 1
                lDr = ds.Tables(0).Select("comp_id = " & oDataItem.Cells(Columnas.CompId).Text)(0)
                lDr.Item("_chk") = 1
            Else
                oDataItem.Cells(Columnas.Sele).Text = 0
                lDr = ds.Tables(0).Select("comp_id = " & oDataItem.Cells(Columnas.CompId).Text)(0)
                lDr.Item("_chk") = 0
            End If
        Next

        For Each lDr In Session("ds").Tables(0).Rows
            If lDr.Item("_chk") = 1 Then
               If lstrDeuda <> "" Then lstrDeuda = lstrDeuda & ","
               lstrDeuda = lstrDeuda & lDr.Item("comp_id").ToString()
               ldecImpo = ldecImpo + Convert.ToDecimal(lDr.Item("cuota"))
            End If
        Next

        txtTotalTotal.Text = ldecImpo
        Session("sDeuda") = lstrDeuda
        hdnSele.Text = lstrDeuda

    End Sub

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            Dim lstrId As String

            mGuardarTodosIds()

            grdConsulta.CurrentPageIndex = E.NewPageIndex

            mConsultar()

            mSeleccionar(hdnSele.Text, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Dim lstrCmd As New StringBuilder
        Dim ds As DataSet
        Dim ldsDatos As DataSet
        Dim lstrFecha As String
        Dim lDr As DataRow
        Dim lstrCate As String = ""

        If usrSoci.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el socio.")
        End If
        If txtFechaDesde Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la fecha desde.")
        End If
        If txtFechaHasta Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la fecha hasta.")
        End If

        lstrCate = clsSQLServer.gCampoValorConsul(mstrConn, "socios_consul @soci_id=" & usrSoci.Valor.ToString(), "soci_cate_id")

        lstrCmd.Append("exec deuda_agropecuaria_consul")
        lstrCmd.Append(" @soci_id=")
        lstrCmd.Append(usrSoci.Valor)
        lstrCmd.Append(", @fecha_desde=")
        If lstrCate = 1 Then
            'Agregado Roxi: tomar desde principio de a�o para menores porque pagan a�o completo.
            lstrCmd.Append(clsSQLServer.gFormatArg("01/01/" & txtFechaDesde.Text.Substring(6, 4), SqlDbType.SmallDateTime))
        Else
            lstrCmd.Append(clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime))
        End If
        lstrCmd.Append(", @fecha_hasta=")
        If lstrCate = 1 Then
            'Agregado Roxi: tomar hasta fin de a�o para menores porque pagan a�o completo.
            lstrCmd.Append(clsSQLServer.gFormatArg("31/12/" & txtFechaHasta.Text.Substring(6, 4), SqlDbType.SmallDateTime))
        Else
            lstrCmd.Append(clsSQLServer.gFormatArg(txtFechaHasta.Text, SqlDbType.SmallDateTime))
        End If

        ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)
        grdConsulta.DataSource = ds
        grdConsulta.DataBind()
        Session("ds") = ds
        ds.Dispose()

    End Sub
#End Region

    Private Function mValidarPeriodos() As Boolean

        Dim lintSele As Integer = 0
        Dim lDr As DataRow
        
        Dim ds As DataSet = Session("ds")
        For Each lDr In ds.Tables(0).Rows
            If lDr.Item("_chk") = 1 Then
               If lintSele = 2 Then
                  Return (False)
               Else
                  lintSele = 1
               End If
            Else
               If lintSele = 1 Then
                  lintSele = 2
               End If
            End If
        Next

        Return (True)

    End Function

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim lstrId As New StringBuilder
        Dim lstrDeuda As New StringBuilder
        Dim lstrFecha As New StringBuilder
        Dim lstrClieAnt, lstrBuqeAnt As String

        Try

            mGuardarTodosIds()
            mDeterminarDeuda()

            If Not mValidarPeriodos() Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar periodos consecutivos.")
                Return
            End If

            lstrId.Append(hdnSele.Text)
            lstrDeuda.Append(lstrId.ToString)
            lstrDeuda.Append(Chr(6))
            lstrDeuda.Append(txtTotalTotal.Text)
            lstrDeuda.Append(Chr(6))
            lstrDeuda.Append(txtDeudaAnterior.Text)
            lstrDeuda.Append(Chr(6))
            lstrDeuda.Append(hdnBimDesc.Text)

            Dim lsbMsg As New StringBuilder

            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append("if(window.opener.document.all['hdnDatosPop']!=null) { ")
            lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", lstrDeuda.ToString))
            lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop',''); } ")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")

            Response.Write(lsbMsg.ToString)

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        Response.Write("<Script>window.close();</script>")
    End Sub
    Private Function mGuardarDatos(Optional ByVal pbooTodos As Boolean = False) As String
        Dim lstrId As String = ""
        Dim ldecTotal As Decimal = 0
        For Each oDataItem As DataGridItem In grdConsulta.Items
            If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Or pbooTodos Then
                If lstrId <> "" Then lstrId = lstrId & ","
                lstrId = lstrId & oDataItem.Cells(Columnas.CompId).Text
                ldecTotal = ldecTotal + Convert.ToDecimal(oDataItem.Cells(Columnas.ImpoCuota).Text)
            End If
        Next
        txtTotalTotal.Text = ldecTotal
        Return (lstrId.Replace("&nbsp;", ""))
    End Function

    Private Sub mCalcularBimestreDeuda()
        If lblBimDesc.Text <> "" Then
            Dim lstrPerio As String = "BIM"
            If lblBimAnteDesc.Text.Substring(0, 3).ToUpper() = "SEM" Then
                lstrPerio = "SEM"
            End If
            Dim lstrDeudaDesde As String = lblBimAnteDesc.Text.Substring(3, lblBimAnteDesc.Text.ToUpper().LastIndexOf(lstrPerio) - 3).Replace("al", "").Trim()
            Dim lstrSeleDesde As String = lblBimDesc.Text.Substring(3, lblBimDesc.Text.ToUpper().LastIndexOf(lstrPerio) - 3).Replace("al", "").Trim()
            lstrSeleDesde = lstrSeleDesde.Replace(".", "")
            Dim lintAnio As Integer = Convert.ToInt32(lstrSeleDesde.Substring(0, 2))
            Dim lintBim As Integer = Convert.ToInt32(lstrSeleDesde.Substring(3, 1))
            lintBim = lintBim - 1
            If lintBim = 0 Then
                lintBim = IIf(lstrPerio = "BIM", 6, 2)
                lintAnio = lintAnio - 1
            End If
            If txtDeudaAnterior.Text <> "0.00" Then
                lblBimAnteDesc.Text = lstrPerio & " " & lstrDeudaDesde & " al " & lstrPerio & " 0" & lintAnio.ToString & "/" & lintBim.ToString()
                hdnBimAnteDesc.Text = lblBimAnteDesc.Text
            Else
                lblBimAnteDesc.Text = ""
                hdnBimAnteDesc.Text = ""
            End If
            If lblBimAnteDesc.Text <> "" Then

            End If
            mCalcularBimestreDeuda()
        End If
    End Sub

    Private Sub mCalcularTotales()
        Dim dTotalCuot As Decimal
        Dim lstrFecha As String
        Dim lDr As DataRow
        Dim ds As DataSet = DirectCast(grdConsulta.DataSource, DataSet)
        For Each lDr In ds.Tables(0).Rows
            dTotalCuot += lDr.Item("cuota")
        Next

        txtDeudaAnterior.Text = Format(ds.Tables(1).Rows(0).Item("deuda_anterior"), "#,##0.00").ToString
        txtTotalTotal.Text = dTotalCuot.ToString("######0.00")

        If txtDeudaAnterior.Text <> "0.00" Then
            lblBimAnteDesc.Text = clsSQLServer.gCampoValorConsul(mstrConn, "deuda_social_descrip_consul " & usrSoci.Valor.ToString, "deuda_descrip")
            mCalcularBimestreDeuda()
        Else
            lblBimAnteDesc.Text = ""
        End If
        hdnBimAnteDesc.Text = lblBimAnteDesc.Text
    End Sub

    Private Sub mChequearTodos()
        mSeleccionar(mstrDeuda, True)
    End Sub

    Private Sub usrSoci_Cambio(ByVal sender As Object) Handles usrSoci.Cambio
        Try
            If hdnSociAnte.Text <> usrSoci.Valor Then
                mConsultar()
                mChequearTodos()
                mCalcularTotales()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub txtFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFecha.TextChanged
        Try
            mConsultar()
            mChequearTodos()
            mCalcularTotales()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

End Class

End Namespace
