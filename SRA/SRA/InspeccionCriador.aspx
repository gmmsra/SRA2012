<%@ Reference Control="~/controles/usrcriador.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InspeccionCriador" CodeFile="InspeccionCriador.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Inspecci�n Criadores</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function usrCriadorId_onchange() {
			var sFiltro = '';
			if (document.all('usrCriador:txtId').value != '') {
				sFiltro = document.all('usrCriador:txtId').value;
			//	sFiltro = '1514509';
				LoadComboXML("establecimientos_cargar", sFiltro, "cmbEsbl", "S");
				getEstablecimiento();
			
			} else
				document.all("cmbEsbl").innerText = "";
		}
		function getEstablecimiento() {
			var vstrRet
			vstrRet = LeerCamposXML("establec_criador", "@escr_cria_id=" + document.all("usrCriador:txtId").value 
			+ ", @escr_esbl_id=" + document.all("cmbEsbl").value, "escr_id").split("|");
			document.all("hdnEscrId").value = vstrRet[0];
		}
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD style="HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Inspecci�n Criadores</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="2"></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
												Width="100%" Visible="True">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" align="right">
																		<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																			BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																			BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																			BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																			BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD height="10" background="imagenes/formfdofields.jpg" width="2" colSpan="2"><IMG border="0" src="imagenes/formfdofields.jpg" width="2" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 180px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:label id="lblCriadorFil" runat="server" cssclass="titulo">Criador:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriadorFil" runat="server" escriador="true" AceptaNull="false" Tabla="Criadores"
																						Saltos="1,2" AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True"
																						Ancho="800"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" width="68px"></cc1:DateBox>&nbsp;
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;
																					<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD height="10" background="imagenes/formfdofields.jpg" width="2" colSpan="2"><IMG border="0" src="imagenes/formfdofields.jpg" width="2" height="2"></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivfin.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="2" height="16"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
												AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
												<FooterStyle CssClass="footer"></FooterStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="icri_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_criador" HeaderText="Criador"></asp:BoundColumn>
													<asp:BoundColumn DataField="_esbl_desc" HeaderText="Establecimiento"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR height="1">
										<TD></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="middle">
											<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
															BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
															BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ToolTip="Agregar una Nueva Relaci�n"
															ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> General</asp:linkbutton></TD>
														<TD style="WIDTH: 23px" width="23"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCompletar" runat="server"
																cssclass="solapa" Width="75px" Height="21px" CausesValidation="False">Completar</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCriador" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<UC1:CLIE id="usrCriador" runat="server" escriador="true" AceptaNull="false" Tabla="Criadores"
																				Saltos="1,2" AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True"
																				Ancho="800"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblEsbl" runat="server" cssclass="titulo">Establecimiento:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbEsbl" class="combo" runat="server" Width="336px" nomoper="establecimientos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 18px" align="right">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha Probable:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" width="68px" Obligatorio="true"></cc1:DateBox>&nbsp;</TD>
																	</TR>
																	<TR id="trFechaInspSepara" runat="server">
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR id="trFechaInsp" runat="server">
																		<TD style="WIDTH: 258px; HEIGHT: 18px" align="right">
																			<asp:Label id="lblFechaInsp" runat="server" cssclass="titulo">Fecha Inspecci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px">
																			<cc1:DateBox id="txtFechaInsp" runat="server" cssclass="cuadrotexto" width="68px"></cc1:DateBox>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 18px" align="right">
																			<asp:Label id="lblInspector" runat="server" cssclass="titulo">Inspector:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px">
																			<CC1:TEXTBOXTAB id="txtInspector" runat="server" cssclass="cuadrotexto" Width="220px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 258px; HEIGHT: 18px" align="right">
																			<asp:Label id="lblObser" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 18px">
																			<CC1:TEXTBOXTAB id="txtObser" runat="server" cssclass="cuadrotexto" Width="400px" AceptaNull="False"
																				Height="54px" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="2">
															<DIV id="divPanCompletar" runat="server">
																<TABLE style="WIDTH: 100%" id="TablaProducto" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdProd" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGridProd_Page" CellPadding="1" GridLines="None"
																			OnItemDataBound="mActualizarGrilla"	CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:BoundColumn Visible="false" DataField="icrd_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prdt_sra_nume" HeaderText="HBA/SBA" HeaderStyle-Width="10%"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prdt_rp" HeaderText="RP" HeaderStyle-Width="5%"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prdt_nomb" HeaderText="Nombre"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderStyle-Width="10px" HeaderText="Sel.">
																						<ItemTemplate>
																						<asp:CheckBox ID="chkSel"  AutoPostBack="True"  OnCheckedChanged="chkSel_OnCheckedChanged" visible=True  height="5px" Checked=<%#DataBinder.Eval(Container, "DataItem.icrd_resu")%> Runat="server">
																							</asp:CheckBox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					
																					<asp:BoundColumn DataField="_icrd_resu" ReadOnly="True" HeaderText="Visto" HeaderStyle-Width="5%">
																						<ItemStyle Height="10px"></ItemStyle>
																					</asp:BoundColumn>
																						<asp:BoundColumn DataField="_sexo" HeaderText="Sexo" HeaderStyle-Width="10%"></asp:BoundColumn>
																				
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																			<tr>
																			<asp:Label Visible=false id="Label1" runat="server" ></asp:Label>
																			</tr>
																			
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblHBA" runat="server" cssclass="titulo">HBA:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtHBABusq" runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblNombre" runat="server" cssclass="titulo">Nombre:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtNombreBusq" runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">RP:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtRPBusq" runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																		</TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnFiltrar" runat="server" cssclass="boton" Width="90px" CausesValidation="False"
																				Text="Filtrar"></asp:Button>
																			<asp:Button id="btnSeleccionarTodos" runat="server" cssclass="boton" Width="123px" CausesValidation="False"
																				Text="Seleccionar Todos"></asp:Button>
																			<asp:Button id="btnDeSeleccionarTodos" runat="server" cssclass="boton" Width="123px" CausesValidation="False"
																				Text="Desmarcar Todos"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblTotalSeleccionados" runat="server" cssclass="titulo" ForeColor="Brown"></asp:Label>
																			<asp:Label id="lblTotalProductos" runat="server" cssclass="titulo" ForeColor="Black"></asp:Label>
																			<asp:Label id="lblTotalNoSeleccionados" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="right">
																			<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
																				BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																				BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	</TR>
																</TABLE>
															</DIV>
														</TD>
													</TR>
													<TR>
														<TD height="1" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button>&nbsp;
															<asp:Button id="btnGenerar" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Generar"></asp:Button>&nbsp;
															<asp:Button id="btnCerrar" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Cerrar"></asp:Button></TD>
													</TR>
												</TABLE>
											</asp:panel>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnEscrId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="hdnCerra" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnBtn" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<script language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
			
		</script>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
