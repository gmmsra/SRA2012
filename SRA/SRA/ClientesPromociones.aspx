<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ClientesPromociones" CodeFile="ClientesPromociones.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Clientes/Promociones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Clientes/Promociones</asp:label></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
										Width="97%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" align="right">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen>
																<asp:ImageButton id="btnClosePrinc" runat="server" Visible="False" ImageUrl="images\Close.bmp" ToolTip="Cerrar"
																	CausesValidation="False"></asp:ImageButton></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																	CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="10"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 13.43%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" Ancho="800" AceptaNull="false" FilDocuNume="True"
																				MuestraDesc="true" FilSociNume="True" Saltos="1,1" Tabla="Clientes"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" background="imagenes/formfdofields.jpg" height="6"></TD>
																		<TD style="WIDTH: 13.43%" align="right" background="imagenes/formfdofields.jpg" height="6"></TD>
																		<TD style="WIDTH: 88%" background="imagenes/formfdofields.jpg" height="6"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 16px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 13.43%; HEIGHT: 15px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblPromFil" runat="server" cssclass="titulo">Promoción:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 15px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbPromFil" runat="server" Width="100%" AceptaNull="False" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" background="imagenes/formfdofields.jpg" height="6"></TD>
																		<TD style="WIDTH: 13.43%" align="right" background="imagenes/formfdofields.jpg" height="6"></TD>
																		<TD style="WIDTH: 88%" background="imagenes/formfdofields.jpg" height="6"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 13.43%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3" height="16"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="prcl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="prcl_clie_id" HeaderText="Cliente">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_socio" HeaderText="Socio">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_cliente" HeaderText="Nombre/Raz&#243;n Social"></asp:BoundColumn>
											<asp:BoundColumn DataField="prcl_desde_fecha" HeaderText="Desde" DataFormatString="{0:dd/MM/yyyy}"
												HeaderStyle-Width="69px"></asp:BoundColumn>
											<asp:BoundColumn DataField="prcl_hasta_fecha" HeaderText="Hasta" DataFormatString="{0:dd/MM/yyyy}"
												HeaderStyle-Width="69px"></asp:BoundColumn>
											<asp:BoundColumn DataField="_prom_desc" HeaderText="Promoción"></asp:BoundColumn>
											<asp:BoundColumn DataField="prcl_porc" DataFormatString="{0:F2}" HeaderText="Porc."></asp:BoundColumn>
											<asp:BoundColumn DataField="prcl_impo" DataFormatString="{0:F2}" HeaderText="Impo."></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="1">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left" width="99%"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Relación"
													ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="center" width="100"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
													ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
											Visible="False" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" align="center" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 131px; HEIGHT: 24px" align="right">
																			<asp:Label id="lblProm" runat="server" cssclass="titulo">Promoción:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 24px">
																			<cc1:combobox class="combo" id="cmbProm" runat="server" Width="400px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 131px; HEIGHT: 9px" vAlign="top" align="right">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 9px">
																			<UC1:CLIE id="usrClie" runat="server" Ancho="800" FilDocuNume="True" MuestraDesc="true" FilSociNume="True"
																				Saltos="1,1" Tabla="Clientes" Obligatorio="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDesdeFecha" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD>
																			<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
																				<TR>
																					<TD style="WIDTH: 97px" align="left" width="97">
																						<cc1:DateBox id="txtDesdeFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																					<TD noWrap align="left" colSpan="2">&nbsp;&nbsp;&nbsp;
																						<asp:Label id="lblHastaFecha" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;
																						<cc1:DateBox id="txtHastaFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																		<TD width="80%">
																			<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="60px"
																				EsDecimal="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblPorc" runat="server" cssclass="titulo">Porcentaje:</asp:Label>&nbsp;</TD>
																		<TD width="80%">
																			<cc1:numberbox id="txtPorc" runat="server" cssclass="cuadrotexto" Width="40px"
																				EsDecimal="True"></cc1:numberbox></TD>
																	</TR>
																</TABLE>
															</asp:panel>
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR height="30">
														<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		//if (document.all["txtDesc"]!= null)
			//document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
