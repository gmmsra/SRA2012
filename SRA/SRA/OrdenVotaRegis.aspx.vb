Namespace SRA

Partial Class OrdenVotaRegis
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    '   Protected WithEvents lblPres As System.Web.UI.WebControls.Label
    '   Protected WithEvents cmbPres As NixorControls.ComboBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_OrdenVota

    Private mstrConn As String
    Private mstrParaPageSize As Integer
    Private mdsDatos As DataSet


    Private Enum Columnas As Integer
        Id = 1
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then

                mCargarCombos()

                clsWeb.gInicializarControles(Me, mstrConn)

                mMostrarPanel(False)
            Else
                mdsDatos = Session(mstrTabla)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsam, "S")
        mSetearSocio()
        clsWeb.gCargarComboBool(cmbVoto, "")
    End Sub
#End Region

#Region "Generacion"
    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Modi()

            mMostrarPanel(False)


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet
        Dim ldsEsta As DataSet = mCrearDataSet()

        mValidarDatos()

        With ldsEsta.Tables(0).Rows(0)
            .Item("orvo_apod_id") = DBNull.Value
            .Item("orvo_firm_id") = DBNull.Value
                'If Not cmbApodFirm.Valor Is DBNull.Value Then
                If cmbApodFirm.Valor.Length > 0 Then
                    If cmbApodFirm.Valor.ToString.Substring(0, 1) = "A" Then
                        .Item("orvo_apod_id") = cmbApodFirm.Valor.ToString.Substring(1)
                    Else
                        .Item("orvo_firm_id") = cmbApodFirm.Valor.ToString.Substring(1)
                    End If
                End If

                .Item("orvo_voto") = cmbVoto.ValorBool
            '         .Item("orvo_pres") = cmbPres.ValorBool
        End With

        Return ldsEsta
    End Function

    Private Function mCrearDataSet() As DataSet
        Dim ldsEsta As New DataSet
        ldsEsta = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
        ldsEsta.Tables(0).TableName = mstrTabla
        Return ldsEsta
    End Function

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Private Sub cmbAsam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAsam.SelectedIndexChanged
        mSetearSocio()
    End Sub

    Private Sub mSetearSocio()
        usrSociFil.Limpiar()
        usrSociFil.Visible = Not cmbAsam.Valor Is DBNull.Value
        lblSociFil.Visible = Not cmbAsam.Valor Is DBNull.Value
    End Sub

        Private Sub mCargarCombosSocio()
            If (cmbAsam.Valor.ToString = "") Then
                Throw New AccesoBD.clsErrNeg("Tiene que seleccionar una asamblea")
            Else
                clsWeb.gCargarRefeCmb(mstrConn, "firmantes_apoderados", cmbApodFirm, "N", cmbAsam.Valor.ToString + "," + usrSociFil.Valor.ToString)
            End If
        End Sub

        Private Sub mLimpiar()
        usrSociFil.Limpiar()
        hdnId.Text = ""
        lblBaja.Text = ""
        txtDatos.Text = ""
        cmbApodFirm.Limpiar()
        cmbVoto.Limpiar()
        '      cmbPres.Limpiar()
    End Sub
#End Region

    Private Sub mCargarDatos()
        Try
            Dim lstrCmd As New StringBuilder
            Dim ds As DataSet
            Dim dsOrva As DataSet

            lstrCmd.Append("exec orden_vota_datos_consul")
            lstrCmd.Append(" @orvo_soci_id=")
            lstrCmd.Append(usrSociFil.Valor.ToString)
            lstrCmd.Append(", @orvo_asam_id=")
            lstrCmd.Append(cmbAsam.Valor.ToString)

            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            If ds.Tables(0).Rows.Count = 0 Then
                txtDatos.Text = ""
                Throw New AccesoBD.clsErrNeg("El socio no est� incluido!")
            End If

            With ds.Tables(0).Rows(0)
                txtDatos.Text = .Item("datos").ToString
                mMostrarPanel(True)

                hdnId.Text = ds.Tables(0).Rows(0).Item("orvo_id").ToString

                If .Item("orvo_esta_id") <> .Item("soci_esta_id") Then
                    lblEsta.Text = "Estado actual: " + .Item("esta_desc").ToString
                End If

                If .Item("orvo_cate_id") <> .Item("soci_cate_id") Then
                    lblCate.Text = "Categor�a actual: " + .Item("cate_desc").ToString
                End If
            End With

            dsOrva = mCrearDataSet()

            With dsOrva.Tables(0).Rows(0)
                hdnSociId.Text = .Item("orvo_soci_id").ToString
                If Not .IsNull("orvo_apod_id") Then
                    cmbApodFirm.Valor = "A" + .Item("orvo_apod_id").ToString
                End If
                If Not .IsNull("orvo_firm_id") Then
                    cmbApodFirm.Valor = "F" + .Item("orvo_firm_id").ToString
                End If

                If Not .IsNull("orvo_voto") Then
                    cmbVoto.ValorBool = .Item("orvo_voto")
                Else
                    cmbVoto.ValorBool = False 'No
                End If

                '            cmbPres.ValorBool = .Item("orvo_pres")

                If Not .IsNull("orvo_voto") AndAlso .Item("orvo_voto") Then
                    lblBaja.Text = "El socio ya vot�! Fecha de actualizaci�n del registro:" & CDate(.Item("orvo_audi_fecha")).ToString("dd/MM/yyyy HH:mm")
                End If

                If Not .IsNull("orvo_apod_id") Then
                    mCargarDatosVotante("apoderados", .Item("orvo_apod_id"))
                ElseIf Not .IsNull("orvo_firm_id") Then
                    mCargarDatosVotante(SRA_Neg.Constantes.gTab_Firmantes, .Item("orvo_firm_id"))
                Else
                    mCargarDatosVotante(SRA_Neg.Constantes.gTab_Socios, hdnSociId.Text)
                End If

                Dim lstrVenc As String = clsSQLServer.gCampoValorConsul(mstrConn, "asambleas_consul @asam_id=" & .Item("orvo_asam_id"), "_vencida")
                btnModi.Enabled = (lstrVenc = "NO")
                cmbApodFirm.Enabled = (lstrVenc = "NO")
                cmbVoto.Enabled = (lstrVenc = "NO")

            End With
                If cmbVoto.ValorBool Then

                    ''If Not cmbVoto.ValorBool Is DBNull.Value AndAlso cmbVoto.ValorBool Then
                    Throw New AccesoBD.clsErrNeg("Atenci�n: el socio ya vot�!")
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        panCabecera.Visible = True

        If Not pbooVisi Then
            mLimpiar()
        End If
    End Sub

    Private Sub usrSociFil_Cambio(ByVal sender As Object) Handles usrSociFil.Cambio
        Try
            If Not cmbAsam.Valor Is DBNull.Value And Not usrSociFil.Valor Is DBNull.Value Then
                mCargarCombosSocio()
                mCargarDatos()
                usrSociFil.Limpiar()
            Else
                mMostrarPanel(False)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarDatosVotante(ByVal pstrTabla As String, ByVal pstrId As String)
        Try
            Dim lstrCmd As New StringBuilder
            Dim ds As DataSet
            Dim lstrFoto As String
            Dim lstrFirma As String
            Dim lstrCarpeta As String

            If pstrId = "" Then
                pstrId = hdnSociId.Text
                pstrTabla = SRA_Neg.Constantes.gTab_Socios
            End If

            lstrCmd.Append("exec ")
            lstrCmd.Append(pstrTabla)
            lstrCmd.Append("_datos_consul ")
            lstrCmd.Append(pstrId)

            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            If ds.Tables(0).Rows.Count = 0 Then
                txtDatos.Text = ""
                Throw New AccesoBD.clsErrNeg("No se encontraron los datos!")
            End If

            With ds.Tables(0).Rows(0)
                txtDatosVotante.Text = .Item("datos").ToString
                lstrFoto = .Item("clie_foto").ToString
                lstrFirma = .Item("clie_firma").ToString
            End With

            lstrCarpeta = Request.Headers.GetValues("Referer")(0).ToString
            lstrCarpeta = lstrCarpeta.Substring(0, lstrCarpeta.LastIndexOf("/") + 1)
            lstrCarpeta += clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_foto_path") + "/"

                imgFoto.ImageUrl = ""
                imgFirma.ImageUrl = ""
                hdnFirma.Text = ""
                hdnFoto.Text = ""

                If lstrFoto <> "" Then
                    imgFoto.ImageUrl = lstrCarpeta + lstrFoto.Substring(lstrFoto.LastIndexOf("\") + 1)
                    hdnFoto.Text = lstrFoto
                Else
                imgFoto.ImageUrl = "images/nodisponible.gif"
            End If
            If lstrFirma <> "" Then
                    imgFirma.ImageUrl = lstrCarpeta + lstrFirma.Substring(lstrFirma.LastIndexOf("\") + 1)
                    hdnFirma.Text = lstrFirma
                Else
                imgFirma.ImageUrl = "images/nodisponible.gif"
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub cmbApodFirm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbApodFirm.SelectedIndexChanged
        Dim lstrValor As String
        Dim lstrTabla As String

        lstrValor = cmbApodFirm.Valor.ToString
        If lstrValor <> "" Then
            If lstrValor.Substring(0, 1) = "A" Then
                lstrTabla = "apoderados"
            Else
                lstrTabla = SRA_Neg.Constantes.gTab_Firmantes
            End If
            lstrValor = lstrValor.Substring(1)
        End If

        mCargarDatosVotante(lstrTabla, lstrValor)
    End Sub
End Class

End Namespace
