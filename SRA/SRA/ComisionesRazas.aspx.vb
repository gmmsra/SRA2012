Namespace SRA

Partial Class ComisionesRazas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ComisionesRazas
   Private mstrTablaAgenda As String = SRA_Neg.Constantes.gTab_ComisionesAgenda
   Private mstrTablaDocum As String = SRA_Neg.Constantes.gTab_ComisionesDocum
   Private mstrTablaCriadores As String = SRA_Neg.Constantes.gTab_ComisionesCriadores
   Private mstrTablaActas As String = SRA_Neg.Constantes.gTab_ComisionesActas
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
            Session("MilSecc") = Now.Millisecond.ToString
            Session(mSess(mstrTabla)) = Nothing
         Else
            If panDato.Visible Then
               mdsDatos = Session(mSess(mstrTabla))
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaAgenda)

      txtAgenObser.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cage_obse")
      txtAgenTema.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cage_tema")
      txtAgenDocAd.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cage_path")
      filAgenDocAd.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cage_path")

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocum)
      txtDocumRefer.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cdoc_refe")
      txtDocumActa.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cdoc_acta")
      txtDocumDoc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cdoc_path")
      filDocumDoc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "cdoc_path")

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaCriadores)
      txtCriadRefer.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "ccri_refe")
   End Sub
   Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "T")
      SRA_Neg.Utiles.gSetearRaza(cmbRaza)
      SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridAgen_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdAgenda.EditItemIndex = -1
         If (grdAgenda.CurrentPageIndex < 0 Or grdAgenda.CurrentPageIndex >= grdAgenda.PageCount) Then
            grdAgenda.CurrentPageIndex = 0
         Else
            grdAgenda.CurrentPageIndex = E.NewPageIndex
         End If
         grdAgenda.DataSource = mdsDatos.Tables(mstrTablaAgenda)
         grdAgenda.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridCriad_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCriad.EditItemIndex = -1
         If (grdCriad.CurrentPageIndex < 0 Or grdCriad.CurrentPageIndex >= grdCriad.PageCount) Then
            grdCriad.CurrentPageIndex = 0
         Else
            grdCriad.CurrentPageIndex = E.NewPageIndex
         End If
         grdCriad.DataSource = mdsDatos.Tables(mstrTablaCriadores)
         grdCriad.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridDocum_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDocum.EditItemIndex = -1
         If (grdDocum.CurrentPageIndex < 0 Or grdDocum.CurrentPageIndex >= grdDocum.PageCount) Then
            grdDocum.CurrentPageIndex = 0
         Else
            grdDocum.CurrentPageIndex = E.NewPageIndex
         End If
         grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
         grdDocum.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
         mstrCmd = mstrCmd + " @raza_id=" + IIf(cmbRazaFil.Valor.ToString = "", "null", cmbRazaFil.Valor.ToString)
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorAgen(ByVal pbooAlta As Boolean)
      btnBajaAgen.Enabled = Not (pbooAlta)
      btnModiAgen.Enabled = Not (pbooAlta)
      btnAltaAgen.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorCriad(ByVal pbooAlta As Boolean)
      btnBajaCriad.Enabled = Not (pbooAlta)
      btnModiCriad.Enabled = Not (pbooAlta)
      btnAltaCriad.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorDocum(ByVal pbooAlta As Boolean)
      btnBajaDocum.Enabled = Not (pbooAlta)
      btnModiDocum.Enabled = Not (pbooAlta)
      btnAltaDocum.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            cmbRaza.Valor = .Item("craz_raza_id")
            hdnRazaId.Text = cmbRaza.Valor.ToString
            If Not .IsNull("craz_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("craz_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)

         lblTitu.Text = "Registro Seleccionado: " + cmbRaza.SelectedItem.Text

      End If
   End Sub
   Public Sub mEditarDatosAgen(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrAgen As DataRow

         hdnAgenId.Text = E.Item.Cells(1).Text
         ldrAgen = mdsDatos.Tables(mstrTablaAgenda).Select("cage_id=" & hdnAgenId.Text)(0)

         With ldrAgen
            txtAgenFecha.Fecha = .Item("cage_fecha")
            txtAgenObser.Valor = .Item("cage_obse")
            txtAgenTema.Valor = .Item("cage_tema")

            If .IsNull("cage_path") Then
               txtAgenDocAd.Valor = ""
               imgDelAgenDoc.Visible = False
            Else
               imgDelAgenDoc.Visible = True
               txtAgenDocAd.Valor = .Item("cage_path")
            End If

            If Not .IsNull("cage_baja_fecha") Then
               lblBajaAgen.Text = "Agenda dada de baja en fecha: " & CDate(.Item("cage_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaAgen.Text = ""
            End If
         End With

         mSetearEditorAgen(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosCriad(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrCriad As DataRow

         hdnCriadId.Text = E.Item.Cells(1).Text
         ldrCriad = mdsDatos.Tables(mstrTablaCriadores).Select("ccri_id=" & hdnCriadId.Text)(0)

         With ldrCriad
            usrCria.Valor = .Item("ccri_cria_id")
            txtCriadFechaDesde.Fecha = .Item("ccri_desde_fecha")
            txtCriadFechaHasta.Fecha = .Item("ccri_hasta_fecha")
            txtCriadRefer.Valor = .Item("ccri_refe")
            If Not .IsNull("ccri_baja_fecha") Then
               lblBajaCriad.Text = "Integrante dado de baja en fecha: " & CDate(.Item("ccri_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaCriad.Text = ""
            End If
         End With
         mSetearEditorCriad(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosDocum(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDocum As DataRow

         hdnDocumId.Text = E.Item.Cells(1).Text
         ldrDocum = mdsDatos.Tables(mstrTablaDocum).Select("cdoc_id=" & hdnDocumId.Text)(0)

         With ldrDocum
            txtDocumFecha.Fecha = .Item("cdoc_fecha")
            txtDocumActa.Valor = .Item("cdoc_acta")
            txtDocumRefer.Valor = .Item("cdoc_refe")

            If .IsNull("cdoc_path") Then
               txtDocumDoc.Valor = ""
               imgDelDocDoc.Visible = False
            Else
               imgDelDocDoc.Visible = True
               txtDocumDoc.Valor = .Item("cdoc_path")
            End If

            If Not .IsNull("cdoc_baja_fecha") Then
               lblBajaDocum.Text = "Documento dado de baja en fecha: " & CDate(.Item("cdoc_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaDocum.Text = ""
            End If
         End With

         mSetearEditorDocum(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaAgenda
      mdsDatos.Tables(2).TableName = mstrTablaCriadores
      mdsDatos.Tables(3).TableName = mstrTablaDocum
      mdsDatos.Tables(4).TableName = mstrTablaActas

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      grdAgenda.DataSource = mdsDatos.Tables(mstrTablaAgenda)
      grdAgenda.DataBind()

      grdCriad.DataSource = mdsDatos.Tables(mstrTablaCriadores)
      grdCriad.DataBind()

      grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
      grdDocum.DataBind()

      Session(mSess(mstrTabla)) = mdsDatos
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar(Optional ByVal booltodo As Boolean = True)
      hdnId.Text = ""
      If booltodo Then
         cmbRaza.Limpiar()
         SRA_Neg.Utiles.gSetearRaza(cmbRaza)
      End If
      hdnRazaId.Text = cmbRaza.Valor.ToString
      lblBaja.Text = ""


      lblTitu.Text = ""
      mLimpiarAgen()
      mLimpiarCriad()
      mLimpiarDocum()

      grdDato.CurrentPageIndex = 0
      grdAgenda.CurrentPageIndex = 0
      grdCriad.CurrentPageIndex = 0
      grdDato.CurrentPageIndex = 0

      mCrearDataSet("")
      mShowTabs(1)
      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarFil()
      cmbRazaFil.Limpiar()
      SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub mLimpiarAgen()
      hdnAgenId.Text = ""
      txtAgenFecha.Text = ""
      txtAgenObser.Text = ""
      txtAgenTema.Text = ""
      txtAgenDocAd.Text = ""
      imgDelAgenDoc.Visible = False
      lblBajaAgen.Text = ""
      mSetearEditorAgen(True)
   End Sub
   Private Sub mLimpiarCriad()
      hdnCriadId.Text = ""
      usrCria.Limpiar()
      txtCriadFechaDesde.Text = ""
      txtCriadFechaHasta.Text = ""
      txtCriadRefer.Text = ""
      lblBajaCriad.Text = ""
      mSetearEditorCriad(True)
   End Sub
   Private Sub mLimpiarDocum()
      hdnDocumId.Text = ""
      txtDocumFecha.Text = ""
      txtDocumActa.Text = ""
      txtDocumRefer.Text = ""
      txtDocumDoc.Text = ""
      imgDelDocDoc.Visible = False
      lblBajaDocum.Text = ""
      mSetearEditorDocum(True)
   End Sub

   Private Sub mCerrar()
      grdDato.CurrentPageIndex = 0
      mConsultar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mValidaDatos()
         mGuardarDatos()
         Dim lobjComisiones As New SRA_Neg.ComisionesRazas(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
         lobjComisiones.Alta()

         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mValidaDatos()
         mGuardarDatos()
         Dim lobjComisiones As New SRA_Neg.ComisionesRazas(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
         lobjComisiones.Alta()

         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenericaRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
         lobjGenericaRel.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If grdAgenda.Items.Count = 0 And grdCriad.Items.Count = 0 And grdDocum.Items.Count = 0 Then
         Throw New AccesoBD.clsErrNeg("No ingres� ning�n dato para la Comisi�n.")
      End If
   End Sub
   Private Sub mGuardarDatos()
      With mdsDatos.Tables(0).Rows(0)
         .Item("craz_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("craz_raza_id") = cmbRaza.Valor
         .Item("craz_baja_fecha") = DBNull.Value
         .Item("craz_audi_user") = Session("sUserId").ToString()
      End With
   End Sub
   'Seccion de agenda
   Private Sub mActualizarAgen()
      Try
         mGuardarDatosAgen()

         mLimpiarAgen()
         grdAgenda.DataSource = mdsDatos.Tables(mstrTablaAgenda)
         grdAgenda.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaAgen()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaAgenda).Select("cage_id=" & hdnAgenId.Text)(0)
         row.Delete()
         grdAgenda.DataSource = mdsDatos.Tables(mstrTablaAgenda)
         grdAgenda.DataBind()
         mLimpiarAgen()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatosAgen()
      If txtAgenFecha.Fecha.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha.")
      End If

      If txtAgenTema.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Temario.")
      End If
   End Sub
   Private Sub mGuardarDatosAgen()
      Dim ldrAgen As DataRow
      Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_craz_docu_path")

      mValidarDatosAgen()
      If hdnAgenId.Text = "" Then
         ldrAgen = mdsDatos.Tables(mstrTablaAgenda).NewRow
         ldrAgen.Item("cage_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaAgenda), "cage_id")
      Else
         ldrAgen = mdsDatos.Tables(mstrTablaAgenda).Select("cage_id=" & hdnAgenId.Text)(0)
      End If

      With ldrAgen
         .Item("cage_fecha") = txtAgenFecha.Fecha
         .Item("cage_obse") = txtAgenObser.Valor
         .Item("cage_tema") = txtAgenTema.Valor

         If filAgenDocAd.Value <> "" Then
            .Item("cage_path") = filAgenDocAd.Value
         Else
            .Item("cage_path") = txtAgenDocAd.Valor
         End If
         If Not .IsNull("cage_path") Then
            .Item("cage_path") = .Item("cage_path").Substring(.Item("cage_path").LastIndexOf("\") + 1)
         End If
         .Item("_parampath") = lstrCarpeta + "_" + mstrTablaAgenda + "_" + Session("MilSecc") + "_" + Replace(.Item("cage_id"), "-", "m") + "__" + .Item("cage_path")

         .Item("_estado") = "Activo"
         .Item("cage_craz_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("cage_audi_user") = Session("sUserId").ToString()
         .Item("cage_baja_fecha") = DBNull.Value

         SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filAgenDocAd)
      End With
      If (hdnAgenId.Text = "") Then
         mdsDatos.Tables(mstrTablaAgenda).Rows.Add(ldrAgen)
      End If
   End Sub
   'Seccion de Criadores
   Private Sub mActualizarCriad()
      Try
         mGuardarDatosCriad()

         mLimpiarCriad()
         grdCriad.DataSource = mdsDatos.Tables(mstrTablaCriadores)
         grdCriad.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaCriad()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaCriadores).Select("ccri_id=" & hdnCriadId.Text)(0)
         row.Delete()
         grdCriad.DataSource = mdsDatos.Tables(mstrTablaCriadores)
         grdCriad.DataBind()
         mLimpiarCriad()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatosCriad()
      If usrCria.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Criador.")
      End If
   End Sub
   Private Sub mGuardarDatosCriad()
      Dim ldrCriad As DataRow
      mValidarDatosCriad()
      If hdnCriadId.Text = "" Then
         ldrCriad = mdsDatos.Tables(mstrTablaCriadores).NewRow
         ldrCriad.Item("ccri_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaCriadores), "ccri_id")
      Else
         ldrCriad = mdsDatos.Tables(mstrTablaCriadores).Select("ccri_id=" & hdnCriadId.Text)(0)
      End If

      With ldrCriad
         .Item("ccri_cria_id") = usrCria.Valor
         .Item("ccri_desde_fecha") = txtCriadFechaDesde.Fecha
         .Item("ccri_hasta_fecha") = txtCriadFechaHasta.Fecha
         .Item("ccri_refe") = txtCriadRefer.Valor
         .Item("ccri_craz_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("ccri_baja_fecha") = DBNull.Value

         .Item("_estado") = "Activo"
         .Item("_criador") = usrCria.Apel
      End With
      If (hdnCriadId.Text = "") Then
         mdsDatos.Tables(mstrTablaCriadores).Rows.Add(ldrCriad)
      End If
   End Sub
   'Seccion de Documentos
   Private Sub mActualizarDocum()
      Try
         mGuardarDatosDocum()
         mLimpiarDocum()
         grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
         grdDocum.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaDocum()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDocum).Select("cdoc_id=" & hdnDocumId.Text)(0)
         row.Delete()
         grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
         grdDocum.DataBind()
         mLimpiarDocum()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatosDocum()
      If txtDocumDoc.Valor.ToString = "" And filDocumDoc.Value = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
      End If

      If txtDocumRefer.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
      End If
   End Sub
   Private Sub mGuardarDatosDocum()
      Dim ldrDocum As DataRow
      Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_craz_docu_path")
      mValidarDatosDocum()
      If txtDocumActa.Valor.ToString <> "" Then
         If mdsDatos.Tables(mstrTablaDocum).Select(IIf(hdnDocumId.Text = "", "", "cdoc_id <> " + hdnDocumId.Text + " and ") + "cdoc_acta=" & txtDocumActa.Valor.ToString).GetUpperBound(0) <> -1 Then
            Throw New AccesoBD.clsErrNeg("El n�mero de Acta ya existe.")
         End If
      End If

      If hdnDocumId.Text = "" Then
         ldrDocum = mdsDatos.Tables(mstrTablaDocum).NewRow
         ldrDocum.Item("cdoc_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocum), "cdoc_id")
      Else
         ldrDocum = mdsDatos.Tables(mstrTablaDocum).Select("cdoc_id=" & hdnDocumId.Text)(0)
      End If

      With ldrDocum
         .Item("cdoc_fecha") = txtDocumFecha.Fecha
         .Item("cdoc_acta") = txtDocumActa.Valor
         .Item("cdoc_refe") = txtDocumRefer.Valor

         If filDocumDoc.Value <> "" Then
            .Item("cdoc_path") = filDocumDoc.Value
         Else
            .Item("cdoc_path") = txtDocumDoc.Valor
         End If
         If Not .IsNull("cdoc_path") Then
            .Item("cdoc_path") = .Item("cdoc_path").Substring(.Item("cdoc_path").LastIndexOf("\") + 1)
         End If
         .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocum + "_" + Session("MilSecc") + "_" + Replace(.Item("cdoc_id"), "-", "m") + "__" + .Item("cdoc_path")

         .Item("cdoc_craz_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("cdoc_audi_user") = Session("sUserId").ToString()
         .Item("cdoc_baja_fecha") = DBNull.Value
         .Item("_estado") = "Activo"
         .Item("_acta") = IIf(txtDocumActa.Valor.ToString = "", "0", "1")

         SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocumDoc)
      End With
      If (hdnDocumId.Text = "") Then
         mdsDatos.Tables(mstrTablaDocum).Rows.Add(ldrDocum)
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panGeneral.Visible = False
         lnkGeneral.Font.Bold = False
         panAgenda.Visible = False
         lnkAgenda.Font.Bold = False
         panCriadores.Visible = False
         lnkCriadores.Font.Bold = False
         panDocumentos.Visible = False
         lnkDocumentos.Font.Bold = False
         Select Case Tab
            Case 1
               panGeneral.Visible = True
               lnkGeneral.Font.Bold = True
            Case 2
               panAgenda.Visible = True
               lnkAgenda.Font.Bold = True
            Case 3
               panCriadores.Visible = True
               lnkCriadores.Font.Bold = True
               usrCria.cmbCriaRazaExt.Valor = cmbRaza.Valor
               usrCria.cmbCriaRazaExt.Enabled = False
            Case 4
               panDocumentos.Visible = True
               lnkDocumentos.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   'Botones generales
   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   'Botones de Agenda
   Private Sub btnAltaAgen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAgen.Click
      mActualizarAgen()
   End Sub
   Private Sub btnBajaAgen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAgen.Click
      mBajaAgen()
   End Sub
   Private Sub btnModiAgen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAgen.Click
      mActualizarAgen()
   End Sub
   Private Sub btnLimpAgen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAgen.Click
      mLimpiarAgen()
   End Sub
   'Botones de Criadores
   Private Sub btnAltaCriad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCriad.Click
      mActualizarCriad()
   End Sub
   Private Sub btnBajaCriad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCriad.Click
      mBajaCriad()
   End Sub
   Private Sub btnModiCriad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCriad.Click
      mActualizarCriad()
   End Sub
   Private Sub btnLimpCriad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpCriad.Click
      mLimpiarCriad()
   End Sub
   'Botones de Documentos
   Private Sub btnAltaDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocum.Click
      mActualizarDocum()
   End Sub
   Private Sub btnBajaDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocum.Click
      mBajaDocum()
   End Sub
   Private Sub btnModiDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocum.Click
      mActualizarDocum()
   End Sub
   Private Sub btnLimpDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocum.Click
      mLimpiarDocum()
   End Sub
   'Solapas
   Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkAgenda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAgenda.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkCriadoes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCriadores.Click
      mShowTabs(3)
   End Sub
   Private Sub lnkDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocumentos.Click
      mShowTabs(4)
   End Sub
   Private Sub cmbRaza_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRaza.SelectedIndexChanged
      mLimpiar(False)
   End Sub
#End Region

End Class
End Namespace
