' Dario 2014-05-28 Cambios de nombres asignacion de campos y volar planilla 1
' Dario 2014-10-01 se comenta para que se pueda ingresar productos sin sranume
' Dario 2014-12-15 se rehabilita el control de vendedor y se oculta el criador del producto
' Dario 2015-03-26 se agrega filtro comprador y se incorporan datos de cant semen crias embr
Imports ReglasValida.Validaciones
Imports SRA_Entidad


Namespace SRA


Partial Class TransferenciaAnimalPie
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents usrCriaFil As usrClieDeriv
    Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents usrCriaProp As usrClieDeriv
    Protected WithEvents rowDivProp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents cmbRazaCria As NixorControls.ComboBox
    Protected WithEvents lblSraNumeFil As System.Web.UI.WebControls.Label
    ' Protected WithEvents txtSraNumeFil As NixorControls.NumberBox
    Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblInicFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblCantEmbr As System.Web.UI.WebControls.Label
    Protected WithEvents txtCantEmbr As NixorControls.NumberBox
    Protected WithEvents cmbCriaComp As NixorControls.ComboBox
    Protected WithEvents hdnCriaComp As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPorcComp As System.Web.UI.WebControls.Label
    Protected WithEvents txtPorcComp As NixorControls.NumberBox
    Protected WithEvents lblObseComp As System.Web.UI.WebControls.Label
    Protected WithEvents txtObseComp As NixorControls.TextBoxTab
    Protected WithEvents btnModiComp As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimpComp As System.Web.UI.WebControls.Button
    Protected WithEvents lblNombComp As System.Web.UI.WebControls.Label
    Protected WithEvents lblRecuFecha As System.Web.UI.WebControls.Label
    Protected WithEvents txtRecuFecha As NixorControls.DateBox
    Protected WithEvents btnTeDenu As System.Web.UI.WebControls.Button
    Protected WithEvents txtTeDesc As NixorControls.TextBoxTab
    Protected WithEvents cmbVari As NixorControls.ComboBox
    Protected WithEvents lblPelaAPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaAPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaBPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaBPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaCPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaCPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaDPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaDPeli As NixorControls.ComboBox
    Protected WithEvents lblExp As System.Web.UI.WebControls.Label
    Protected WithEvents lblClieVend As System.Web.UI.WebControls.Label
    'Protected WithEvents grdComp As System.Web.UI.WebControls.DataGrid


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Tramites
    Private mstrTablaRequisitos As String = SRA_Neg.Constantes.gTab_Tramites_Deta
    Private mstrTablaDocumentos As String = SRA_Neg.Constantes.gTab_Tramites_Docum
    Private mstrTablaObservaciones As String = SRA_Neg.Constantes.gTab_Tramites_Obse
    Private mstrTablaProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrTablaEspecies As String = SRA_Neg.Constantes.gTab_Especies
    Private mstrTablaTramitesProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
    Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis
    Private mstrTablaProductosNumeros As String = SRA_Neg.Constantes.gTab_ProductosNumeros
    Private mstrTablaProductosDocum As String = SRA_Neg.Constantes.gTab_ProductosDocum
    Private mstrTramiteId As String = ""


    Private mstrParaPageSize As Integer
    Private mdsDatosPadre As DataSet
    Private mdsDatosMadre As DataSet
    Private mdsDatosProd As DataSet
    Private dsVali As DataSet
    Private mstrCmd As String
    Public mintProce As Integer
    Private mdsDatos As DataSet
    Private mstrConn As String
    Private mstrTrapId As String
    Public mstrTitulo As String
    Public mintTtraId As Integer



#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Dim strTramNume As String

            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                clsWeb.gInicializarControles(Me, mstrConn)

                Session("mitramite") = Nothing
                Session("sessProductos") = Nothing
                mstrTramiteId = Request.QueryString("Tram_Id")

                If mstrTramiteId <> "" Then
                    hdnId.Text = mstrTramiteId
                    Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
                    cmbEsta.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                    cmbEstadoFil.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                    mEditarTramite(mstrTramiteId)
                    mMostrarPanel(True)
                    lnkRequ.Font.Bold = False
                    lnkDocu.Font.Bold = False
                    lnkObse.Font.Bold = False

                    lnkRequ.Enabled = False
                    lnkDocu.Enabled = False
                    lnkObse.Enabled = False

                    panRequ.Visible = False
                    panDocu.Visible = False
                    panObse.Visible = False
                End If

                If mstrTramiteId = "" Then
                    mSetearMaxLength()
                    mSetearEventos()
                    mCargarCombos()
                    mMostrarPanel(False)
                End If
            Else
                mstrTramiteId = Request.QueryString("Tram_Id")
                If mstrTramiteId <> "" Then
                    Response.Write("<Script>window.close();</script>")
                End If
            End If

            mdsDatos = Session("mitramite")
            mdsDatosProd = Session("sessProductos")
            mstrTrapId = Session("mstrTrapId")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        Dim lstrSRA As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")
        clsWeb.gCargarCombo(mstrConn, "rg_requisitos_cargar", cmbRequRequ, "id", "descrip_codi", "S")
    End Sub
    Private Sub mSetearEventos()
        btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
        btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
        btnBaja.Attributes.Add("onclick", "if(!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?')){return false;} else {mPorcPeti();}")
        'btnAgre.Attributes.Add("onclick", "return btnAgre_click();") Dario 2014-05-28
        ' btnTeDenu.Attributes.Add("onclick", "mCargarTE();return(false);")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
        btnCriaCopropiedadComp.Attributes.Add("onclick", "mCriaCopropiedadComp();return false;")

        btnCriaCopropiedadVend.Attributes.Add("onclick", "mCriaCopropiedadVend();return false;")
        Me.usrProd.cmbSexoProdExt.onchange = "return usrProd_cmbProdSexo_change();"

    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nume")
        txtNroControl.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nro_control")


        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, SRA_Neg.Constantes.gTab_Productos)
        '  txtSraNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_sra_nume")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocumentos)
        txtDocuObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trdo_refe")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaObservaciones)
        txtObseObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trao_obse")

    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mHabilitarDatosTE(ByVal pbooHabi As Boolean)



        
    End Sub
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdRequ.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdDocu.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdObse.PageSize = Convert.ToInt32(mstrParaPageSize)

        mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos
        mintProce = ReglasValida.Validaciones.Procesos.TransferenciaProductos

        mstrTitulo = "Transferencia de Productos"

        'usrProd.MostrarFechaNacimiento = True


        rowProp.Style.Add("display", "none")
      
        mHabilitarDatosTE(False)
        btnAgre.AlternateText = "Nueva " & mstrTitulo

        lblTituAbm.Text = mstrTitulo

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub grdDato_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            If Page.IsPostBack Then

                mConsultar(True)
            Else
                mConsultar(False)

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdRequ_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdRequ.EditItemIndex = -1
            If (grdRequ.CurrentPageIndex < 0 Or grdRequ.CurrentPageIndex >= grdRequ.PageCount) Then
                grdRequ.CurrentPageIndex = 0
            Else
                grdRequ.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdDocu_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDocu.EditItemIndex = -1
            If (grdDocu.CurrentPageIndex < 0 Or grdDocu.CurrentPageIndex >= grdDocu.PageCount) Then
                grdDocu.CurrentPageIndex = 0
            Else
                grdDocu.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarDocu()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdObse.EditItemIndex = -1
            If (grdObse.CurrentPageIndex < 0 Or grdObse.CurrentPageIndex >= grdObse.PageCount) Then
                grdObse.CurrentPageIndex = 0
            Else
                grdObse.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    
    Private Sub mConsultar(ByVal boolMostrarTodos As Boolean)

        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet

            lstrCmd = "exec  tramites_animalEnPie_busq "
            lstrCmd += " @tram_ttra_id=" + mintTtraId.ToString

            If Not boolMostrarTodos Then
                If txtNumeFil.Valor <> 0 Then
                    lstrCmd += " , @tram_nume=" + clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int)
                End If
                If hdnId.Text <> "" Then
                    lstrCmd += " , @tram_id=" + clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                End If
            End If

            If Not usrProdFil Is Nothing Then
                If usrProdFil.RazaId.ToString() <> "" Then
                    lstrCmd += " , @tram_raza_id=" + clsSQLServer.gFormatArg(usrProdFil.RazaId, SqlDbType.Int)
                End If
                If usrProdFil.Valor <> 0 Then
                    lstrCmd += ",@prod_id=" + clsSQLServer.gFormatArg(usrProdFil.Valor, SqlDbType.Int)
                End If

                lstrCmd += " ,@sexo=" + IIf(usrProdFil.cmbSexoProdExt.SelectedValue.ToString = "", "null", usrProdFil.cmbSexoProdExt.SelectedValue.ToString)
            End If
            lstrCmd += ",@prdt_nombre=" + IIf(usrProdFil.txtProdNombExt.Valor.ToString().Trim() = "", "NULL", "'" + usrProdFil.txtProdNombExt.Valor + "'")
            lstrCmd += ",@sra_nume=" + IIf(usrProdFil.txtSraNumeExt.Valor.ToString() = "", "null", usrProdFil.txtSraNumeExt.Valor.ToString())
            lstrCmd += ",@tram_fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
            lstrCmd += ",@tram_fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)
            lstrCmd += ",@tram_oper_fecha=" + clsFormatear.gFormatFecha2DB(txtFechaTransferenciaFil.Fecha)
            lstrCmd += ",@tram_esta_id=" + cmbEstadoFil.Valor.ToString

            '  lstrCmd += ",@tram_pais_id=" + cmbPaisFil.Valor.ToString
            lstrCmd += ",@mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")

            If Not usrCompradorFil Is Nothing Then
                If usrCompradorFil.Valor.ToString() <> "0" And usrCompradorFil.Valor.ToString() <> "" Then
                    lstrCmd += ",@cria_id=" + clsSQLServer.gFormatArg(usrCompradorFil.Valor.ToString(), SqlDbType.Int)
                End If
            End If

            dsDatos = clsWeb.gCargarDataSet(mstrConn, lstrCmd)
            grdDato.Visible = True
            grdDato.DataSource = dsDatos
            grdDato.DataBind()


            grdDato.Visible = True

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mConsultarRequ()
        grdRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
        grdRequ.DataBind()
    End Sub
    Private Sub mConsultarDocu()
        grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocumentos)
        grdDocu.DataBind()
    End Sub
    Private Sub mConsultarObse()
        mdsDatos.Tables(mstrTablaObservaciones).DefaultView.Sort = "trao_fecha desc,_requ_desc"
        grdObse.DataSource = mdsDatos.Tables(mstrTablaObservaciones)
        grdObse.DataBind()
    End Sub
    
    Private Function mCrearDataSet(ByVal pstrId As String, ByVal pSpEstructura As String, _
                    ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                    ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


        Dim ldsDatosTransf As New DataSet
        Dim tblDatos As New DataTable(pTableName)

        ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
        tblDatos = ldsDatosTransf.Tables(0)
        tblDatos.TableName = pTableName

        dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
        Session("miTramite") = dsDatosNewDataSet

        Return True
    End Function




    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaRequisitos
        mdsDatos.Tables(2).TableName = mstrTablaDocumentos
        mdsDatos.Tables(3).TableName = mstrTablaObservaciones
        mdsDatos.Tables(4).TableName = mstrTablaTramitesProductos


        grdRequ.CurrentPageIndex = 0
        grdDato.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0


        mConsultarRequ()
        mConsultarDocu()
        mConsultarObse()


        Session("mitramite") = mdsDatos
    End Sub

    Public Sub mCrearDataSetProd(ByVal pstrId As String)

        mdsDatosProd = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaProductos, pstrId)

        mdsDatosProd.Tables(0).TableName = mstrTablaProductos
        mdsDatosProd.Tables(1).TableName = mstrTablaAnalisis
        mdsDatosProd.Tables(2).TableName = mstrTablaProductosNumeros
        mdsDatosProd.Tables(3).TableName = mstrTablaProductosDocum



        Session("sessProductos") = mdsDatosProd
    End Sub

    Private Function mCrearDataSetProd(ByVal pstrId As String, ByVal pSpEstructura As String, _
                      ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                      ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


        Dim ldsDatosTransf As New DataSet
        Dim tblDatos As New DataTable(pTableName)

        ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
        tblDatos = ldsDatosTransf.Tables(0)
        tblDatos.TableName = pTableName

        dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
        Session("sessProductos") = dsDatosNewDataSet

        Return True
    End Function


#End Region

#Region "Seteo de Controles"
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mEditarTramite(E.Item.Cells(2).Text)
    End Sub

    Private Sub mCargarCriadores(ByVal pobjCombo As NixorControls.ComboBox, _
    ByVal pstrClieId As String, ByVal pstrRaza As String)

        If pobjCombo.Visible Then
            mstrCmd = "criadores_cliente_cargar "
            If pstrClieId <> "" Then
                mstrCmd = mstrCmd & " @clie_id=" & pstrClieId
            Else
                mstrCmd = mstrCmd & " @clie_id=0"
            End If
            If pstrRaza <> "" Then
                mstrCmd = mstrCmd & ",@raza_id=" & pstrRaza
            End If
            clsWeb.gCargarCombo(mstrConn, mstrCmd, pobjCombo, "id", "descrip", "S")
        End If
    End Sub

        Public Sub mEditarTramite(ByVal pstrTram As String)
            Try
                mLimpiar()

                Dim strProdId As String
                Dim dtProducto As DataTable
                Dim dtProductos As DataTable
                Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
                Dim cont As Integer
                Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

                hdnId.Text = clsFormatear.gFormatCadena(pstrTram)

                'Dim intClienteId As Integer
                'Dim intVendClienteId As Integer

                'intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
                'intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaVend.RazaId, usrCriaVend.Valor)

                mCrearDataSet(hdnId.Text)

                strProdId = ""
                dtProducto = oTramite.GetProductoByTramiteId(hdnId.Text, "")
                If dtProducto.Rows.Count > 0 Then
                    strProdId = dtProducto.Rows(0).Item("prdt_id")
                End If

                If strProdId = "" Then
                    Throw New AccesoBD.clsErrNeg("Validaci�n: El Tramite no esta vinculado a ningun producto .")
                End If
                mCrearDataSetProd(strProdId)

                Session("mitramite") = mdsDatos
                Session("sessProductos") = mdsDatosProd


                With mdsDatos.Tables(mstrTabla).Rows(0)
                    hdnTramNro.Text = "Tr�mite Nro: " & .Item("tram_nume")
                    lblTitu.Text = hdnTramNro.Text
                    hdnRazaId.Text = .Item("tram_raza_id")
                    txtInicFecha.Fecha = IIf(.Item("tram_inic_fecha").ToString.Length = 0, String.Empty, .Item("tram_inic_fecha"))
                    txtFinaFecha.Fecha = IIf(.Item("tram_fina_fecha").ToString.Length = 0, String.Empty, .Item("tram_fina_fecha"))
                    txtFechaTram.Fecha = IIf(.Item("tram_pres_fecha").ToString.Length = 0, String.Empty, .Item("tram_pres_fecha"))
                    txtFechaTransferencia.Fecha = IIf(.Item("tram_oper_fecha").ToString.Length = 0, String.Empty, .Item("tram_oper_fecha"))
                    cmbEsta.Valor = IIf(IsDBNull(.Item("tram_esta_id")), 0, .Item("tram_esta_id"))
                    txtNroControl.Text = ValidarNulos(.Item("tram_nro_control"), False)
                    usrClieProp.Valor = IIf(IsDBNull(.Item("tram_prop_clie_id")), 0, .Item("tram_prop_clie_id"))
                    usrClieProp.Activo = .IsNull("tram_prop_clie_id")

                    usrProd.Valor = hdnId.Text
                    If dtProducto.Rows.Count > 0 Then
                        usrProd.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_id"), False)
                        'usrProd.RazaId = ValidarNulos(dtProducto.Rows(0).Item("prdt_raza_id"), False)
                        'usrProd.CriaOrPropId = .Item("tram_vend_cria_id")
                    End If

                    usrCriaComp.Valor = IIf(IsDBNull(.Item("tram_comp_cria_id")), 0, .Item("tram_comp_cria_id"))
                    usrCriaVend.Valor = IIf(IsDBNull(.Item("tram_vend_cria_id")), 0, .Item("tram_vend_cria_id"))

                    If Not .IsNull("tram_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("tram_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If

                    strProdId = ""
                    If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 1 Then
                        With mdsDatos.Tables(mstrTablaTramitesProductos).Rows(0)
                            hdnDetaId.Text = .Item("trpr_id")
                            strProdId = ValidarNulos(.Item("trpr_prdt_id"), False)
                        End With
                    Else
                        hdnDetaId.Text = "0"
                    End If

                    'If mdsDatosProd.Tables(mstrTablaProductos).Rows.Count > 0 Then
                    '    If oTramite.GetFechaTransByTramiteId(pstrTram).ToString <> "" Then
                    '        txtFechaTransferencia.Fecha = oTramite.GetFechaTransByTramiteId(pstrTram)
                    '    End If

                    'End If

                    If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count > 1 Then
                        usrProd.Visible = False

                        lblTranPlan.Visible = True
                        hdnMultiProd.Text = "S"
                    Else
                        'If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count > 0 Then
                        '    '    usrProd.Tramite = hdnId.Text
                        '    usrProd.Valor = hdnDetaId.Text
                        'Else
                        '    '    usrProd.Tramite = hdnId.Text
                        '    usrProd.Valor = .Item("trpr_prdt_id")
                        'End If


                        If dtProducto.Rows.Count > 0 Then
                            usrProd.Valor = dtProducto.Rows(0).Item("prdt_id")

                            'usrProd.txtCodiExt.Valor = dtProducto.Rows(0).Item("prdt_ori_asoc_nume")
                            '  usrProd.usrProductoExt.txtCodiExtLab.Valor = dtProducto.Rows(0).Item("prdt_ori_asoc_nume_lab")
                            usrProd.txtProdNombExt.Valor = dtProducto.Rows(0).Item("prdt_nomb")
                            usrProd.cmbProdAsocExt.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_ori_asoc_id"), True)
                            usrProd.txtRPExt.Text = ValidarNulos(dtProducto.Rows(0).Item("prdt_rp"), True)
                            usrProd.txtSraNumeExt.Valor = dtProducto.Rows(0).Item("prdt_sra_nume")
                            usrProd.cmbSexoProdExt.Valor = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                            usrProd.cmbSexoProdExt.SelectedValue = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                            usrProd.Sexo = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                            hdnSexo.Text = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                        End If

                        ' Dario 2015-03-27
                        If (ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("tram_dosi_cant"), True) = "" And _
                        ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("tram_embr_cant"), True) = "" _
                            And ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("tram_cria_cant"), True) = "") Then
                            chkReservaEstock.Checked = False
                            Me.txtCantidadReser.Text = ""
                            Me.txtCantCriasReser.Text = ""
                            Me.txtCantidadReser.Enabled = False
                        Else
                            chkReservaEstock.Checked = True
                            Me.txtCantidadReser.Enabled = True
                            ' Dario 2015-03-27 se agrega datos de crias semen y embriones
                            Me.txtCantCriasReser.Text = ValidarNulos(.Item("tram_cria_cant"), True)
                            If (usrProd.cmbSexoProdExt.ValorBool = True) Then
                                Me.txtCantidadReser.Text = ValidarNulos(.Item("tram_dosi_cant"), True)
                                Me.lblReserva.Text = "Con Reserva de Semen: "
                            Else
                                Me.txtCantidadReser.Text = ValidarNulos(.Item("tram_embr_cant"), True)
                                Me.lblReserva.Text = "Con Reserva de Embriones: "
                            End If
                        End If

                        usrProd.Visible = True

                        lblTranPlan.Visible = False
                        hdnMultiProd.Text = ""
                    End If
                    'datos del �ltimo analisis
                    If mdsDatosProd.Tables(mstrTablaAnalisis).Rows.Count > 0 Then
                        mdsDatosProd.Tables(mstrTablaAnalisis).DefaultView.Sort() = "prta_fecha DESC"
                        With mdsDatosProd.Tables(mstrTablaAnalisis).Select()(0)
                            txtNroAnal.Valor = .Item("prta_nume")
                            'txtTipoAnal.Valor = .Item("_tipo")
                            txtResulAnal.Valor = .Item("_resul_codi") & "-" & .Item("_resul")
                        End With
                        mdsDatosProd.Tables(mstrTablaAnalisis).DefaultView.RowFilter = ""
                    End If

                    If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count > 0 Then
                        With mdsDatos.Tables(mstrTablaTramitesProductos).Rows(0)
                            ' usrProd.usrMadreExt.Valor = .Item("trpr_madre_asoc_id")
                            ' usrProd.usrPadreExt.Valor = .Item("trpr_padre_asoc_id")

                        End With
                    End If

                    mSetearEditor("tramites", False)
                    mMostrarPanel(True)
                End With
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
    Public Sub mEditarDatosRequ(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDeta As DataRow

            hdnRequId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)

            With ldrDeta
                cmbRequRequ.Valor = .Item("trad_requ_id")
                chkRequPend.Checked = .Item("trad_pend")
                lblRequManu.Text = .Item("_manu")
            End With

            mSetearEditor(mstrTablaRequisitos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosDocu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDoc As DataRow

            hdnDocuId.Text = E.Item.Cells(1).Text
            ldrDoc = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)

            With ldrDoc
                If .IsNull("trdo_path") Then
                    txtDocuDocu.Valor = ""
                    imgDelDocuDoc.Visible = False
                Else
                    imgDelDocuDoc.Visible = True
                    txtDocuDocu.Valor = .Item("trdo_path")
                End If
                txtDocuObse.Valor = .Item("trdo_refe")
            End With

            mSetearEditor(mstrTablaDocumentos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrObse As DataRow

            hdnObseId.Text = E.Item.Cells(1).Text
            ldrObse = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)

            With ldrObse
                cmbObseRequ.Valor = .Item("_trad_requ_id")
                txtObseObse.Valor = .Item("trao_obse")
                lblObseFecha.Text = CDate(.Item("trao_fecha")).ToString("dd/MM/yyyy")
            End With

            mSetearEditor(mstrTablaObservaciones, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarFiltros()
        txtNumeFil.Text = ""
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        cmbEstadoFil.Limpiar()
        txtNumeFil.Text = ""
        usrProdFil.Limpiar()
        txtFechaTransferenciaFil.text = ""
        usrCompradorFil.Limpiar()
        grdDato.Visible = False
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnRazaId.Text = ""
        lblBaja.Text = ""
        hdnMultiProd.Text = ""
        hdnTEId.Text = ""
        txtNroControl.Text = ""
        txtInicFecha.Fecha = Now
        txtFechaTram.Fecha = Now
            txtFechaTransferencia.Fecha = "" 'System.DBNull.Value
        txtFinaFecha.Text = ""
        chkReservaEstock.Checked = False
        Me.txtCantidadReser.Text = ""
        Me.txtCantidadReser.Enabled = False
        Me.txtCantCriasReser.Text = ""
        Me.txtCantCriasReser.Enabled = False
        cmbEsta.Limpiar()
        usrProd.Limpiar()
        lblTranPlan.Visible = False
        usrProd.Visible = True
        usrCriaComp.Limpiar()
        usrCriaVend.Limpiar()
        usrClieProp.Limpiar()
        usrClieProp.Activo = mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos
        lblTitu.Text = ""
        hdnTramNro.Text = ""
        grdRequ.CurrentPageIndex = 0
        grdDocu.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0
        mLimpiarRequ()
        mLimpiarDocu()
        mLimpiarObse()
        mCrearDataSet("")
        mCrearDataSetProd("")
        mSetearEditor(mstrTabla, True)
        mShowTabs(1)
        mCargarPlantilla()
    End Sub
    Private Sub mLimpiarRequ()
        hdnRequId.Text = ""
        cmbRequRequ.Limpiar()
        chkRequPend.Checked = False
        lblRequManu.Text = "S�"

        mSetearEditor(mstrTablaRequisitos, True)
    End Sub
    Private Sub mLimpiarDocu()
        hdnDocuId.Text = ""
        txtDocuObse.Valor = ""
        txtDocuDocu.Valor = ""
        imgDelDocuDoc.Visible = False

        mSetearEditor(mstrTablaDocumentos, True)
    End Sub
    Private Sub mLimpiarObse()
        hdnObseId.Text = ""
        cmbObseRequ.Limpiar()
        txtObseObse.Text = ""
        txtObseFecha.Fecha = Now

        mSetearEditor(mstrTablaObservaciones, True)
    End Sub
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTabla
                btnAlta.Enabled = pbooAlta
                    'MF 22/07/2013 si el tramite es vigente no se puede editar
                    'If Not cmbEsta.Valor Is System.DBNull.Value Then
                    If cmbEsta.Text.Trim().Length > 0 Then
                        If cmbEsta.Valor = SRA_Neg.Constantes.Estados.Tramites_Vigente.ToString() Then
                            'btnBaja.Enabled = False 2015-01-12 Dario para dejar eliminar los tramites
                            btnModi.Enabled = False
                            btnErr.Visible = False
                            btnCertProp.Enabled = True
                        Else
                            btnBaja.Enabled = Not (pbooAlta)
                            btnModi.Enabled = Not (pbooAlta)
                            btnErr.Visible = Not (pbooAlta)
                            btnCertProp.Enabled = False
                        End If
                    End If

                Case mstrTablaRequisitos
                btnAltaRequ.Enabled = pbooAlta
                    'MF 22/07/2013 si el tramite es vigente no se puede editar
                    If Not cmbEsta.Valor Is System.DBNull.Value And cmbEsta.Valor <> "" Then
                        If cmbEsta.Valor = SRA_Neg.Constantes.Estados.Tramites_Vigente Then
                            btnBajaRequ.Enabled = False
                            btnModiRequ.Enabled = False
                        Else
                            btnBajaRequ.Enabled = Not (pbooAlta)
                            btnModiRequ.Enabled = Not (pbooAlta)
                        End If
                    End If

                Case mstrTablaDocumentos
                btnAltaDocu.Enabled = pbooAlta
                    'If Not cmbEsta.Valor Is System.DBNull.Value Then
                    '    'MF 22/07/2013 si el tramite es vigente no se puede editar
                    '    If cmbEsta.Valor = SRA_Neg.Constantes.Estados.Tramites_Vigente Then
                    '        btnBajaDocu.Enabled = False
                    '        btnModiDocu.Enabled = False
                    '    Else
                    '        btnBajaDocu.Enabled = Not (pbooAlta)
                    '        btnModiDocu.Enabled = Not (pbooAlta)
                    '    End If
                    'End If

                    'GM 17/09/2022
                    If cmbEsta.Valor.ToString.Length > 0 Then
                        If cmbEsta.Valor = SRA_Neg.Constantes.Estados.Tramites_Vigente Then
                            btnBajaDocu.Enabled = False
                            btnModiDocu.Enabled = False
                        Else
                            btnBajaDocu.Enabled = Not (pbooAlta)
                            btnModiDocu.Enabled = Not (pbooAlta)
                        End If
                    End If


                Case mstrTablaObservaciones
                btnAltaObse.Enabled = pbooAlta
                    'MF 22/07/2013 si el tramite es vigente no se puede editar
                    If Not cmbEsta.Valor.Trim().Length > 0 Then ' Is System.DBNull.Value Then
                        If cmbEsta.Valor = SRA_Neg.Constantes.Estados.Tramites_Vigente.ToString() Then
                            btnBajaObse.Enabled = False
                            btnModiObse.Enabled = False
                        Else
                            btnBajaObse.Enabled = Not (pbooAlta)
                            btnModiObse.Enabled = Not (pbooAlta)
                        End If
                    End If
            End Select
    End Sub
    Private Sub mAgregar()
        Try
            mstrTrapId = SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos ' hdnDatosPop.Text Dario 2014-05-28
            mLimpiar()
            hdnOperacion.Text = "A"
            'GSZ 20/08/2015 Se agrego porque a mariana esporadicamente le aparece en baja y no se puede modificar,
            ' se adapto por si el combo pone baja y es nuevo lo pueda modificar


            If (hdnOperacion.Text = "A" And cmbEsta.SelectedValue = SRA_Neg.Constantes.Estados.Tramites_Baja) Then
                cmbEsta.Enabled = True
            Else
                If cmbEsta.SelectedValue = SRA_Neg.Constantes.Estados.Tramites_Vigente Then
                    cmbEsta.Enabled = False
                End If

            End If

            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
            'hdnDatosPop.Text = ""
            Session("mstrTrapId") = mstrTrapId
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        lnkCabecera.Font.Bold = True
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False
        lnkVend.Font.Bold = False
        lnkComp.Font.Bold = False

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False

        panLinks.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet
            Dim ldsDatosProd As DataSet

            Dim eError As New ErrorEntity
            Dim strSexo As String
            Dim strRaza As String
            Dim strTramiteProdId As String
            Dim lstrTramiteId As String
            Dim strProdId As String
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

            hdnOperacion.Text = "A"
            ldsDatos = Nothing
            ldsDatosProd = Nothing

            Dim ValorActual As Integer
            ValorActual = SRA_Neg.Constantes.Estados.Tramites_Vigente


            cmbEsta.Valor = ValorActual.ToString()
            ' gsz 19/12/2014 se agrego para asegurar que el alta  comoienza como vigente

            ' gsz 19/02/2015 se quito porque si es una alta no debe tomar nada de la sesion
            'mdsDatos = Session("mitramite")
            'mdsDatosProd = Session("sessProductos")

                ldsDatos = mGuardarDatos()
                If txtFechaTransferencia.Fecha.Length > 0 Then
                    mdsDatosProd.Tables("productos").Rows(0).Item("prdt_tran_fecha") = txtFechaTransferencia.Fecha
                End If

                If Not mdsDatosProd Is Nothing Then
                    ldsDatosProd = mdsDatosProd
                End If

                If Not ldsDatosProd Is Nothing Then
                    strProdId = Convert.ToString(usrProd.Valor.ToString())
                    hdnProdId.Text = usrProd.Valor.ToString
                End If

                ldsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_id") = usrProd.Valor
                strSexo = ValidarNulos(ldsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_sexo"), True)

                'mdsDatosMadre = mGuardarDatosPadre(usrProd.usrMadreExt, usrProd.usrMadreExt.Valor.ToString, False)
                'mdsDatosPadre = mGuardarDatosPadre(usrProd.usrPadreExt, usrProd.usrPadreExt.Valor.ToString, False)

                Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), _
                mstrTabla, ldsDatos, ldsDatosProd, Context, _
                False, mintTtraId, mdsDatosMadre, mdsDatosPadre)

                lstrTramiteId = lTramites.Alta()
                mdsDatos.Tables("tramites").Rows(0).Item("tram_id") = lstrTramiteId
                ldsDatos.Tables("tramites").Rows(0).Item("tram_id") = lstrTramiteId
                Dim intClienteId As Integer
                intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)

                Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())

                lblAltaId.Text = oTramite.mObtenerTramite(lstrTramiteId)
                mdsDatos.Tables("tramites").Rows(0).Item("tram_nume") = lblAltaId.Text
                ldsDatos.Tables("tramites").Rows(0).Item("tram_nume") = lblAltaId.Text

                ldsDatosProd.Tables("productos").Rows(0).Item("prdt_tram_nume") = lblAltaId.Text
                ldsDatosProd.Tables("productos").Rows(0).Item("prdt_prop_cria_id") = usrCriaComp.Valor

                ldsDatosProd.Tables("productos").Rows(0).Item("prdt_prop_clie_id") = intClienteId

                strRaza = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_raza_id"), True)
                strTramiteProdId = ValidarNulos(ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_id"), True)

                mdsDatos.AcceptChanges()
                ldsDatos.AcceptChanges()
                ldsDatosProd.AcceptChanges()

                strProdId = ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_prdt_id").ToString()

                lTramites.Tramites_updateByTramiteId(lstrTramiteId, lblAltaId.Text)
                lTramites.AplicarReglasTramite(lstrTramiteId, mintProce, mstrTablaTramitesProductos, strSexo, strRaza, eError)
                If eError.errorDescripcion <> "" Then
                    lTramites.GrabarEstadoRetenidaEnTramites(lstrTramiteId)
                    Throw New AccesoBD.clsErrNeg("se detectaron errores validatorios, por favor verifiquelo.")
                End If

                mConsultar(False)
                ' GSZ 19/12/2014  se quito para evitar que hagan varios tramites seguidos  presionando alta,,alta,alta.....
                ' mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                    mConsultar(True)
                End If
            Finally
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
            End Try
    End Sub
    
    Private Function mGuardarDatosPadre(ByVal pusrProd As SRA.usrProdDeriv, _
        ByVal pstrId As String, ByVal pbooCierre As Boolean) As DataSet

        Dim ldsDatos As DataSet
        Dim lstrId As String
        Dim ldrProd As DataRow
        Dim ldrNume As DataRow

        ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, SRA_Neg.Constantes.gTab_Productos, "", "")

        With ldsDatos
            .Tables(0).TableName = SRA_Neg.Constantes.gTab_Productos
            .Tables(1).TableName = SRA_Neg.Constantes.gTab_ProductosNumeros

        End With

        If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select.GetUpperBound(0) = -1 Then
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).NewRow
            ldrProd.Table.Rows.Add(ldrProd)
        Else
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select()(0)
        End If

        'productos
        With ldrProd
            lstrId = clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
            .Item("prdt_id") = lstrId
            .Item("prdt_raza_id") = pusrProd.cmbProdRazaExt.Valor
            .Item("prdt_sexo") = pusrProd.cmbSexoProdExt.ValorBool
            .Item("prdt_nomb") = pusrProd.txtProdNombExt.Valor
            .Item("prdt_tran_fecha") = txtFechaTransferencia.Fecha
            '.Item("prdt_rp_nume") = IIf(IsNumeric(Trim(pusrProd.txtRPExt.Text)), Trim(pusrProd.txtRPExt.Text), DBNull.Value)
            .Item("prdt_rp") = IIf(Trim(pusrProd.txtRPExt.Text) = "", DBNull.Value, pusrProd.txtRPExt.Text.Trim())
            .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", "E", .Item("prdt_ndad"))
            .Item("prdt_ori_asoc_id") = pusrProd.cmbProdAsocExt.Valor
            .Item("prdt_ori_asoc_nume") = pusrProd.txtCodiExt.Valor
            '.Item("prdt_ori_asoc_nume_lab") = pusrProd.txtCodiExtLab.Valor
            .Item("generar_numero") = pbooCierre

        End With

        ''productos_numeros
        'If pusrProd.txtCodiExt.Text <> "" Then
        '    If ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select.GetUpperBound(0) = -1 Then
        '        ldrNume = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).NewRow
        '        ldrNume.Table.Rows.Add(ldrNume)
        '    Else
        '        ldrNume = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select()(0)
        '    End If
        '    With ldrNume
        '        .Item("ptnu_id") = clsSQLServer.gFormatArg("", SqlDbType.Int)
        '        .Item("ptnu_prdt_id") = lstrId
        '        .Item("ptnu_asoc_id") = pusrProd.cmbProdAsocExt.Valor
        '        .Item("ptnu_nume") = pusrProd.txtCodiExt.Text
        '    End With
        'End If

        Return ldsDatos
    End Function
    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet
            Dim ldsDatosProd As DataSet
            Dim eError As New ErrorEntity
            Dim strSexo As String
            Dim strRaza As String
            Dim strTramiteProdId As String
            Dim strTramiteId As String
            Dim intClienteId As Integer
            Dim strProductoId As String
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
            Dim cont As Integer
            Dim intTramNume As Integer = 0
            Dim dtTramite As DataTable

            intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)

            hdnOperacion.Text = "M"
            ldsDatos = Nothing
            ldsDatosProd = Nothing

            ldsDatos = mGuardarDatos()

            mdsDatosProd.Tables("productos").Rows(0).Item("prdt_tran_fecha") = txtFechaTransferencia.Fecha
            mdsDatosProd.Tables("productos").Rows(0).Item("prdt_sra_nume") = ValidarNulos(usrProd.txtSraNumeExt.Text, False)

            If Not mdsDatosProd Is Nothing Then
                ldsDatosProd = mdsDatosProd
            End If
            'mdsDatosMadre = mGuardarDatosPadre(usrProd.usrMadreExt, usrProd.usrMadreExt.Valor.ToString, False)
            'mdsDatosPadre = mGuardarDatosPadre(usrProd.usrPadreExt, usrProd.usrPadreExt.Valor.ToString, False)

            Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())

            dtTramite = oTramite.GetTramiteById(hdnId.Text, "")

            intTramNume = ValidarNulos(dtTramite.Rows(0).Item("tram_nume"), False)
            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_tram_nume") = intTramNume
            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_prop_cria_id") = usrCriaComp.Valor
            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_tran_fecha") = txtFechaTransferencia.Fecha
            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_sra_nume") = _
            ValidarNulos(usrProd.txtSraNumeExt.Text, False)

            ldsDatosProd.Tables("productos").Rows(0).Item("prdt_prop_clie_id") = intClienteId

            Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), mstrTabla, _
            ldsDatos, ldsDatosProd, context, False, _
            mintTtraId, mdsDatosMadre, mdsDatosPadre)

            strSexo = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_sexo"), True)
            strRaza = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_raza_id"), True)
            strTramiteProdId = ValidarNulos(ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_id"), True)
            strTramiteId = ValidarNulos(ldsDatos.Tables("tramites").Rows(0).Item("tram_id"), True)
            strProductoId = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_id"), True)

            lTramites.Modi()

            eError.errorDescripcion = ""

            lTramites.AplicarReglasTramite(hdnId.Text, mintProce, mstrTablaTramitesProductos, strSexo, strRaza, eError)
            If eError.errorDescripcion <> "" Then
                lTramites.GrabarEstadoRetenidaEnTramites(hdnId.Text)
                Throw New AccesoBD.clsErrNeg("Se detectaron errores validatorios, por favor verifiquelo.")
            End If

            mLimpiar()
            mConsultar(False)
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                mConsultar(False)
            End If
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            cmbEsta.SelectedValue = SRA_Neg.Constantes.Estados.Tramites_Baja

            mModi()

            grdDato.CurrentPageIndex = 0

            mConsultar(True)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mValidarDatos(ByVal pbooCierre As Boolean, ByVal pbooCierreBaja As Boolean)
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
        'If ValidarNulos(txtNroControl.Valor, False) = 0 Then
        '    Me.panBotones.Style.Add("display", "inline")
        '    Me.divproce.Style.Add("display", "none")
        '    Throw New AccesoBD.clsErrNeg("El numero de Control debe ser distinto de cero.")
        'End If

        ' Dario 2014-10-01 se comenta para que se pueda ingresar productos sin sranume
        'If usrProd.txtSraNumeExt.Valor Is DBNull.Value Then
        '    Me.panBotones.Style.Add("display", "inline")
        '    Me.divproce.Style.Add("display", "none")
        '    Throw New AccesoBD.clsErrNeg("El Producto debe tener HBA o SBA ")
        'End If
        If usrCriaComp.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el comprador.")
        Else
            If usrCriaComp.Valor = "0" Then
                Throw New AccesoBD.clsErrNeg("El Comprador no tiene datos de Criador.")
            End If
        End If

        If usrCriaVend.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el vendedor.")
        Else
            If usrCriaVend.Valor = "0" Then
                Throw New AccesoBD.clsErrNeg("El Vendedor no tiene datos de Criador.")
            End If
        End If

        '' Dario 2014-10-31 comentado ahora no lo quierenal control de nro de control
        ' creo el objeto de productos 
        'Dim objProductosBusiness As New Business.Productos.ProductosBusiness
        '' ejecuto la validacion que me retorna un string con el mensaje de error si existe
        'Dim idRaza As String
        'idRaza = IIf(usrProd.RazaId = "", usrCriaComp.RazaId, usrProd.RazaId)
        ''
        'Dim msg As String = objProductosBusiness.ValidaNumeroControlTramitesPropiedad(txtNroControl.Valor, Convert.ToInt32("0" + usrProd.CriaOrPropId), usrCriaComp.Valor, idRaza, 1, 0, Convert.ToInt16(Common.CodigosServiciosTiposRRGG.TransferenciaAnimalesPie))
        'If (Len(msg.Trim) > 0) Then
        '    Throw New AccesoBD.clsErrNeg(msg)
        'End If

        If usrProd.RazaId = "" Or usrProd.RazaId = "0" Then
            Throw New AccesoBD.clsErrNeg("El Raza del producto debe de estar informada.")
        End If

        'If usrProd.CriaOrPropId.ToString().Trim() = "" Or usrProd.RazaId = "" Or usrProd.RazaId = "0" Then
        '    Throw New AccesoBD.clsErrNeg("El vendedor del producto debe de estar informado.")
        'End If

        If IsDate(txtFechaTransferencia.Text) Then
                If CDate(txtFechaTransferencia.Fecha) > DateTime.Now Then
                    Me.panBotones.Style.Add("display", "inline")
                    Me.divproce.Style.Add("display", "none")
                    Throw New AccesoBD.clsErrNeg("La Fecha de Transferencia no puede ser superior a la fecha actual.")
                End If
                If CDate(txtFechaTram.Fecha) > DateTime.Now Then
                    Me.panBotones.Style.Add("display", "inline")
                    Me.divproce.Style.Add("display", "none")
                    Throw New AccesoBD.clsErrNeg("La Fecha del Tr�mite no puede ser superior a la fecha actual.")
                End If
            End If

        'Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
        'If Not oTramite.ValidarPaisesTransferenciaByRazaCriador(usrCriaVend.cmbCriaRazaExt.Valor, _
        '                               usrCriaVend.Valor, _
        '                               usrCriaComp.cmbCriaRazaExt.Valor, _
        '                               usrCriaComp.Valor) Then
        '    Throw New AccesoBD.clsErrNeg("El comprador y el vendedor deben ser argentinos.")
        'End If
        If Not pbooCierreBaja Then
            If hdnSexo.Text <> "" Then
                usrProd.cmbSexoProdExt.Valor = hdnSexo.Text
            End If
            If usrProd.cmbSexoProdExt.Valor.ToString = "" Then
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Sexo del Producto para el Tr�mite.")
            End If
            If clsSQLServer.gCampoValorConsul(mstrConn, "razas_consul @raza_id=" + usrProd.RazaId.ToString, "raza_espe_id") <> SRA_Neg.Constantes.Especies.Peliferos Then
                If usrProd.txtProdNombExt.Valor.ToString = "" Then
                    Me.panBotones.Style.Add("display", "inline")
                    Me.divproce.Style.Add("display", "none")
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre del Producto para el Tr�mite.")
                End If
            End If
        End If
    End Sub
    Private Function mGuardarDatos() As DataSet
        Dim ldsDatosProd As DataSet
        Dim lbooCierre As Boolean = False
        Dim lbooCierreBaja As Boolean = False
        Dim ldrDatos As DataRow
        Dim dtProducto As DataTable
        Dim ProductoId As String
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

        If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_pend = 1").GetUpperBound(0) < 0 And _
                grdRequ.Items.Count > 0 Then
            lbooCierre = True
        End If

            If cmbEsta.Valor.ToString = SRA_Neg.Constantes.Estados.Tramites_Baja.ToString Then
                lbooCierreBaja = True
            End If

            mValidarDatos(lbooCierre, lbooCierreBaja)

        If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then
            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("tram_ttra_id") = mintTtraId
                .Item("tram_nro_control") = ValidarNulos(txtNroControl.Text, False)
                '   If hdnMultiProd.Text <> "S" Then
                .Item("tram_raza_id") = usrProd.RazaId
                '     End If
                .Item("tram_pres_fecha") = txtFechaTram.Fecha
                .Item("tram_esta_id") = cmbEsta.Valor
                Dim intClienteId As Integer
                Dim intVendClienteId As Integer
                intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
                intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaVend.RazaId, usrCriaVend.Valor)

                .Item("tram_pais_id") = oCliente.GetPaisByClienteId(intClienteId)
                .Item("tram_prop_clie_id") = intClienteId
                .Item("tram_vend_clie_id") = intVendClienteId
                .Item("tram_comp_clie_id") = intClienteId
                .Item("tram_vend_cria_id") = usrCriaVend.Valor ' usrProd.CriaOrPropId
                .Item("tram_comp_cria_id") = usrCriaComp.Valor
                    .Item("tram_prop_cria_id") = usrCriaComp.Valor
                    If txtFechaTransferencia.Fecha.Length > 0 Then
                        .Item("tram_oper_fecha") = txtFechaTransferencia.Fecha
                    Else
                        .Item("tram_oper_fecha") = Today
                    End If


                    If .IsNull("tram_inic_fecha") Then
                        .Item("tram_inic_fecha") = Today
                    End If

                    .Item("tram_baja_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                    .Item("tram_fina_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)

                    ' Dario 2015-03-27 se agrega datos de crias semen y embriones
                    .Item("tram_cria_cant") = clsSQLServer.gFormatArg(Me.txtCantCriasReser.Text, SqlDbType.Int)
                    If (usrProd.cmbSexoProdExt.ValorBool = True) Then
                        .Item("tram_dosi_cant") = clsSQLServer.gFormatArg(Me.txtCantidadReser.Text, SqlDbType.Int)
                    Else
                        .Item("tram_embr_cant") = clsSQLServer.gFormatArg(Me.txtCantidadReser.Text, SqlDbType.Int)
                    End If

                    dtProducto = oProducto.GetProductoById(usrProd.Valor, "")
                    hdnProdId.Text = usrProd.Valor

                    If dtProducto.Rows.Count = 0 Then
                        Throw New AccesoBD.clsErrNeg("El producto no existe en tabla de productos.")
                    Else
                        ldsDatosProd = mGuardarDatosProd()
                        ldsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_id") = _
                        usrProd.Valor
                        'ldsDatosProd.Tables(mstrTablaProductos).Rows(0).Item("prdt_tran_fecha") = _
                        'txtFechaTransferencia.Fecha
                    End If
                End With
        End If

        'GUARDA LOS DATOS DE TRAMITES_PRODUCTOS
        If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 0 Then
            ldrDatos = mdsDatos.Tables(mstrTablaTramitesProductos).NewRow
            ldrDatos.Item("trpr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaTramitesProductos), "trpr_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaTramitesProductos).Select()(0)
        End If

        With ldrDatos
            .Item("trpr_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("trpr_prdt_id") = usrProd.Valor
                If usrProd.cmbProdAsocExt.Valor.Length > 0 Then
                    .Item("trpr_asoc_id") = usrProd.cmbProdAsocExt.Valor
                End If
                '.Item("trpr_padre_asoc_id") = usrProd.usrPadreExt.Valor
                '.Item("trpr_madre_asoc_id") = usrProd.usrMadreExt.Valor
                .Item("trpr_audi_user") = Session("sUserId").ToString()
                .Item("trpr_baja_fecha") = DBNull.Value
                .Item("trpr_sexo") = usrProd.cmbSexoProdExt.ValorBool
                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 0 Then
                    .Table.Rows.Add(ldrDatos)
                End If
            End With

        mdsDatos.AcceptChanges()
        cmbEsta.Enabled = False
        Return mdsDatos
    End Function
    Private Sub mActualizarRequ(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosRequ(pbooAlta)
            mLimpiarRequ()
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarRequ(ByVal pbooAlta As Boolean)
        If cmbRequRequ.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
        End If
        If pbooAlta Then
            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Requisito existente.")
            End If
        End If
    End Sub
    Private Sub mValidarModiRequ(ByVal pbooAlta As Boolean)
        If grdRequ.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
        End If
        If pbooAlta Then
            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Requisito existente.")
            End If
        End If
    End Sub
    Private Sub mGuardarDatosRequ(ByVal pbooAlta As Boolean)
        Dim ldrDatos As DataRow
        If hdnOperacion.Text = "M" Then
            mValidarModiRequ(pbooAlta)
        Else
            mValidarRequ(pbooAlta)
        End If

        If hdnRequId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
            ldrDatos.Item("trad_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRequisitos), "trad_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)
        End If

        With ldrDatos
            .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("trad_requ_id") = cmbRequRequ.Valor
            .Item("trad_pend") = chkRequPend.Checked
            .Item("trad_obli") = True
            .Item("trad_manu") = IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
            .Item("trad_baja_fecha") = DBNull.Value
            .Item("trad_audi_user") = Session("sUserId").ToString()
            .Item("_requ_desc") = cmbRequRequ.SelectedItem.Text
            .Item("_pend") = IIf(chkRequPend.Checked, "S�", "No")
            .Item("_manu") = "S�"
            .Item("_estado") = "Activo"

            If hdnRequId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With

    End Sub
    'DOCUMENTOS
    Private Sub mActualizarDocu()
        Try
            mGuardarDatosDocu()
            mLimpiarDocu()
            mConsultarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDocu()
        If txtDocuDocu.Valor.ToString = "" And filDocuDocu.Value = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
        End If

        If txtDocuObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
        End If
    End Sub
    Private Sub mGuardarDatosDocu()
        Dim ldrDatos As DataRow
        Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_tram_docu_path")

        mValidarDocu()

        If hdnDocuId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).NewRow
            ldrDatos.Item("trdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocumentos), "trdo_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)
        End If

        With ldrDatos
            .Item("trdo_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            If filDocuDocu.Value <> "" Then
                .Item("trdo_path") = filDocuDocu.Value
            Else
                .Item("trdo_path") = txtDocuDocu.Valor
            End If
            If Not .IsNull("trdo_path") Then
                .Item("trdo_path") = .Item("trdo_path").Substring(.Item("trdo_path").LastIndexOf("\") + 1)
            End If
            .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(.Item("trdo_id"), "-", "m") + "__" + .Item("trdo_path")
            .Item("trdo_refe") = txtDocuObse.Valor
            .Item("trdo_baja_fecha") = DBNull.Value
            .Item("trdo_audi_user") = Session("sUserId").ToString()
            .Item("_estado") = "Activo"

            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocuDocu)

            If hdnDocuId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub
    Private Sub mActualizarObse()
        Try
            mGuardarDatosObse()
            mLimpiarObse()
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarObse()
        If txtObseObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Observaci�n.")
        End If
        If txtObseFecha.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de la Observaci�n.")
        End If
    End Sub
    Private Sub mGuardarDatosObse()
        Dim ldrDatos As DataRow
        mValidarObse()

        If hdnObseId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).NewRow
            ldrDatos.Item("trao_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaObservaciones), "trao_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)
        End If

        With ldrDatos
            .Item("trao_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            If cmbObseRequ.Valor.ToString = "" Then
                .Item("trao_trad_id") = DBNull.Value
            Else
                .Item("trao_trad_id") = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_requ_id=" + cmbObseRequ.Valor.ToString)(0).Item("trad_id")
            End If
            .Item("_requ_desc") = IIf(cmbObseRequ.Valor.ToString = "", "COMENTARIO GENERAL", cmbObseRequ.SelectedItem.Text)
            .Item("_trad_requ_id") = IIf(cmbObseRequ.Valor.ToString = "", "0", cmbObseRequ.Valor)
            .Item("trao_obse") = txtObseObse.Valor
            .Item("trao_fecha") = txtObseFecha.Fecha
            .Item("trao_audi_user") = Session("sUserId").ToString()

            If hdnObseId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub
    Private Function mGuardarDatosProd() As DataSet
        Dim ldsDatosProd As DataSet
        Dim ldrDatosProd As DataRow
        Dim lbooCierre As Boolean = False
        Dim lbooCierreBaja As Boolean = False
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())


        mValidarDatos(lbooCierre, lbooCierreBaja)

        hdnProdId.Text = usrProd.Valor

        If mdsDatosProd.Tables(mstrTablaProductos).Select.GetUpperBound(0) = -1 Then
            ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).NewRow
            ldrDatosProd.Table.Rows.Add(ldrDatosProd)
        Else
            ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).Select()(0)
        End If

        With ldrDatosProd
            If hdnProdId.Text <> "" Then
                .Item("prdt_id") = clsSQLServer.gFormatArg(hdnProdId.Text, SqlDbType.Int)
            Else
                .Item("prdt_id") = clsSQLServer.gFormatArg(ldrDatosProd.ItemArray(0).ToString(), SqlDbType.Int)
            End If
            .Item("prdt_raza_id") = usrProd.RazaId
            .Item("prdt_sexo") = usrProd.cmbSexoProdExt.ValorBool
            .Item("prdt_nomb") = usrProd.txtProdNombExt.Valor
            .Item("prdt_rp") = Trim(usrProd.txtRPExt.Text)
            .Item("prdt_sra_nume") = ValidarNulos(Trim(usrProd.txtSraNumeExt.Text), False)
                ' .Item("prdt_rp_extr") = txtRPExtr.Valor
                If txtFechaTransferencia.Fecha.ToString.Length > 0 Then
                    .Item("prdt_tran_fecha") = txtFechaTransferencia.Fecha
                End If
                .Item("prdt_ndad") = "N"
                If usrProd.cmbProdAsocExt.Valor.ToString.Length > 0 Then
                    .Item("prdt_ori_asoc_id") = usrProd.cmbProdAsocExt.Valor.ToString
                End If
                .Item("prdt_ori_asoc_nume") = usrProd.txtCodiExt.Valor.ToString
                '.Item("prdt_prop_clie_id") = _
                'oCliente.GetClienteIdByRazaCriador(usrProd.RazaId, usrProd.CriaOrPropId)
                .Item("generar_numero") = False
                .Item("prdt_prop_cria_id") = usrProd.CriaOrPropId ' Dario 2014-12-09 se cambia para que no pise el cria_id
            End With

        mdsDatosProd.AcceptChanges()
        ldsDatosProd = mdsDatosProd
        ldsDatosProd.AcceptChanges()

        Return (ldsDatosProd)
    End Function
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panCabecera.Visible = False
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False

        lnkCabecera.Font.Bold = False
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False

        lblTitu.Text = ""
        Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())

        If origen <> 1 And usrProd.Valor.ToString <> "" Then
            lblTitu.Text = "Producto: " & usrProd.RazaId & " - " & _
           oRaza.GetRazaDescripcionById(usrProd.RazaId) & _
            " - " & usrProd.txtProdNombExt.Valor.ToString
        End If

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Text = hdnTramNro.Text
            Case 2
                panRequ.Visible = True
                lnkRequ.Font.Bold = True
            Case 3
                panDocu.Visible = True
                lnkDocu.Font.Bold = True
            Case 4
                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = "trad_baja_fecha is null"

                cmbObseRequ.DataTextField = "_requ_desc"
                cmbObseRequ.DataValueField = "trad_requ_id"
                cmbObseRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
                cmbObseRequ.DataBind()
                cmbObseRequ.Items.Insert(0, "(Seleccione)")
                cmbObseRequ.Items(0).Value = ""
                cmbObseRequ.Valor = ""

                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = ""

                panObse.Visible = True
                lnkObse.Font.Bold = True

        End Select
    End Sub
    Private Sub mLimpiarPersonas(ByVal pstrTabla As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
        For Each lDr As DataRow In mdsDatos.Tables(pstrTabla).Select()
            If lDr.Item("trpe_id") > 0 Then
                lDr.Delete()
            Else
                mdsDatos.Tables(pstrTabla).Rows.Remove(lDr)
            End If
        Next

        If mdsDatos.Tables(pstrTabla).Select.GetUpperBound(0) = -1 Then pctrHdnOrig.Text = ""

        pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
        pgrdGrilla.DataBind()
    End Sub
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnAltaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRequ.Click
        mActualizarRequ(True)
    End Sub
    Private Sub btnLimpRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRequ.Click
        mLimpiarRequ()
    End Sub
    Private Sub btnBajaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRequ.Click
        Try
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaObservaciones).Select("trao_trad_id=" + hdnRequId.Text)
                odrDeta.Delete()
            Next

            mConsultarObse()
            mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0).Delete()
            grdRequ.CurrentPageIndex = 0
            mConsultarRequ()
            mLimpiarRequ()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRequ.Click
        mActualizarRequ(False)
    End Sub
    Private Sub btnAltaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocu.Click
        mActualizarDocu()
    End Sub
    Private Sub btnLimpDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocu.Click
        mLimpiarDocu()
    End Sub
    Private Sub btnBajaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocu.Click
        Try
            mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0).Delete()
            grdDocu.CurrentPageIndex = 0
            mConsultarDocu()
            mLimpiarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocu.Click
        mActualizarDocu()
    End Sub
    Private Sub btnAltaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaObse.Click
        mActualizarObse()
    End Sub
    Private Sub btnLimpObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpObse.Click
        mLimpiarObse()
    End Sub
    Private Sub btnBajaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaObse.Click
        Try
            mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0).Delete()
            grdObse.CurrentPageIndex = 0
            mConsultarObse()
            mLimpiarObse()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiObse.Click
        mActualizarObse()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar(False)
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRequ.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkObse.Click
        mShowTabs(4)
    End Sub
    Private Sub btnCertProp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCertProp.Click
        Try
            If usrProd.Valor.ToString = "" And usrProd.Valor.ToString = "0" Then
                Dim lstrMens As String = ""

                If usrProd.cmbProdRazaExt.Valor.ToString <> "" Then
                    lstrMens = Trim(clsSQLServer.gObtenerValorCampo(mstrConn, "razas_especie", usrProd.cmbProdRazaExt.Valor.ToString, "espe_nomb_nume").ToString)
                End If

                Throw New AccesoBD.clsErrNeg("El producto no posee " & lstrMens & " asignado.")
            End If
            mImprimirCertificado()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

    Private Sub mImprimirCertificado()
        Try
            Dim params As String
            Dim lstrRptName As String
            Dim lstrRpt As String

            params += "&hba_desde=" + usrProd.txtSraNumeExt.Valor
            params += "&hba_hasta=" + usrProd.txtSraNumeExt.Valor

            params += "&sexo=" + IIf(usrProd.Sexo = 0, "False", "True")

            If usrProd.RazaId.ToString <> "" Then
                params += "&raza_id=" + usrProd.RazaId.ToString
            Else
                params += "&raza_id=" & IIf(usrCriaComp.RazaId.ToString = "", "0", usrCriaComp.RazaId.ToString)
            End If

            params += "&criador_id=0"
            params += "&FilFechaInscripcion=" & True
            params += "&FechaDesde=0"
            params += "&FechaHasta=0"
            params += "&tipoCertificado= 0"
            params += "&audi_user=" & Session("sUserId").ToString()

            Dim strEspe As String = clsSQLServer.gCampoValorConsul(mstrConn, "razas_consul @raza_id=" + usrProd.RazaId.ToString, "raza_espe_id")

            Select Case strEspe
                Case SRA_Neg.Constantes.Especies.Ovinos
                    If usrProd.Sexo = 0 Then
                        'Hembra.
                        lstrRptName = "CertificadoRegPropLanarHembra"
                    Else
                        'Macho.
                        lstrRptName = "CertificadoRegPropLanarHembra"
                    End If
                    params += "&prdt_id=" & usrProd.Valor
                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)
                Case SRA_Neg.Constantes.Especies.Caprinos
                    lstrRptName = "CertificadoRegPropCaprinoHembra"
                    params += "&prdt_id=" & usrProd.Valor
                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)
                Case SRA_Neg.Constantes.Especies.Bovinos
                    params += "&propie=" + usrCriaComp.Codi + " - " + usrCriaComp.Apel
                    lstrRptName = "CertificadoRegPropBovinos"
                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)
                Case SRA_Neg.Constantes.Especies.Peliferos
                    lstrRptName = "CertificadoRegPropPeliferos"
                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)
                Case SRA_Neg.Constantes.Especies.Equinos
                    Dim strIdRegistro As Object = clsSQLServer.gCampoValorConsul(mstrConn, "productos_consul @prdt_id=" + usrProd.Valor, "prdt_regt_id")
                    If Not strIdRegistro Is Nothing Then
                        params += "&reg_tipo=" + strIdRegistro
                    End If
                    lstrRptName = "CertificadoRegPropEquino"
                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)
                Case SRA_Neg.Constantes.Especies.Camelidos
                    lstrRptName = "CertificadoRegPropCamelidos"
                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)
                Case Else
                    Return
            End Select

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#Region "Opciones de POP"
    Private Sub mCargarPlantilla()
        If mstrTrapId <> "" Then

            Dim lDs As New DataSet
            lDs = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Tramites_Plantilla, "@SinBaja=1,@trap_id=" + mstrTrapId)
            Dim ldrDatos As DataRow

            mdsDatos.Tables(mstrTablaRequisitos).Clear()

            For Each ldrOri As DataRow In lDs.Tables(1).Select
                ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                With ldrDatos
                    .Item("trad_id") = clsSQLServer.gObtenerId(.Table, "trad_id")
                    .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    .Item("trad_baja_fecha") = DBNull.Value
                    .Item("trad_requ_id") = ldrOri.Item("trpd_requ_id")
                    .Item("trad_obli") = ldrOri.Item("trpd_obli")
                    .Item("trad_pend") = True
                    .Item("trad_manu") = False
                    .Item("_requ_desc") = ldrOri.Item("_requisito")
                    .Item("_obli") = ldrOri.Item("_obligatorio")
                    .Item("_pend") = "S�"
                    .Item("_manu") = "No"
                    .Item("_estado") = ldrOri.Item("_estado")

                    .Table.Rows.Add(ldrDatos)
                End With
            Next

            Dim lstrRazas As String = ""
            Dim lstrEspecies As String = ""

            For Each ldrOri As DataRow In lDs.Tables(2).Select
                With ldrOri
                    If .IsNull("trdr_raza_id") Then
                        If lstrEspecies.Length > 0 Then lstrEspecies += ","
                        lstrEspecies += .Item("trdr_espe_id").ToString
                    Else
                        If lstrRazas.Length > 0 Then lstrRazas += ","
                        lstrRazas += .Item("trdr_raza_id").ToString
                    End If
                End With
            Next

            'usrProd.usrProductoExt.FiltroRazas = "@raza_ids = " & IIf(lstrRazas = "", "null", "'" & lstrRazas & "'") & ",@raza_espe_ids = " & IIf(lstrEspecies = "", "null", "'" & lstrEspecies & "'")
            'usrProd.usrProductoExt.mCargarRazas()

            mConsultarRequ()
        End If
    End Sub
    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            mstrTrapId = hdnDatosPop.Text
            mAgregar()
            hdnDatosPop.Text = ""
            Session("mstrTrapId") = mstrTrapId

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub hdnDatosTEPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosTEPop.TextChanged
        Try
            hdnTEId.Text = ""
            txtRecuFecha.Text = ""
            txtTeDesc.Text = ""
            txtCantEmbr.Text = ""


            If (hdnDatosTEPop.Text <> "") Then
                hdnTEId.Text = hdnDatosTEPop.Text
                hdnDatosTEPop.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub grdDato_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdDato.SelectedIndexChanged

    End Sub
    Function MessageBox(ByRef oPage As Page, ByVal sAviso As String)
        If sAviso.Trim.Length > 0 Then
            Dim s As String = "alert('" & sAviso & "') "
            oPage.RegisterStartupScript("OnLoad", s)
        End If
    End Function

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload

    End Sub
End Class
End Namespace
