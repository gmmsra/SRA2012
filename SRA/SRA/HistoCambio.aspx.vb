Imports System.Data.SqlClient


Namespace SRA


Partial Class HistoCambio
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lblProv As System.Web.UI.WebControls.Label
   Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable

   Protected WithEvents txtRefeFil As NixorControls.TextBoxTab
   Protected WithEvents btnLimpFil As System.Web.UI.WebControls.ImageButton

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "movim_socios"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrTipo As String

   Private Enum Columnas As Integer
      ClieEdit = 0
      ClieId = 1
      ClieFech = 2
      ClieCate = 3
      ClieDist = 4
      ClieEsta = 5
      ClieApel = 6
		ClieNomb = 7
		clieNroActa = 8
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mMostrarPanel(False)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      If mstrTipo = "C" Then
         clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S")
         clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDist, "S")
      Else
         clsWeb.gCargarCombo(mstrConn, "estados_cargar " & SRA_Neg.Constantes.EstaTipos.EstaTipo_Socios, cmbEsta, "id", "descrip", "S", False)
      End If
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta
      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi
      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mstrTipo = Request.QueryString("t").ToUpper
      hdnTipo.Text = mstrTipo
      Select Case mstrTipo
         Case "C"
            lblTituAbm.Text = "Hist�rico de Cambios de Categor�a"
         Case "E"
            lblTituAbm.Text = "Hist�rico de Cambios de Estado"
         Case "N"
            lblTituAbm.Text = "Hist�rico de Cambios de Raz�n Social"
      End Select

      grdDato.Columns(Columnas.ClieCate).Visible = (mstrTipo = "C")
		grdDato.Columns(Columnas.ClieDist).Visible = (mstrTipo = "C")
		If mstrTipo <> "N" Then
			grdDato.Columns(Columnas.clieNroActa).Visible = True
		Else
			grdDato.Columns(Columnas.clieNroActa).Visible = False
		End If
		grdDato.Columns(Columnas.ClieEsta).Visible = (mstrTipo = "E")
		grdDato.Columns(Columnas.ClieApel).Visible = (mstrTipo = "N")
		grdDato.Columns(Columnas.ClieNomb).Visible = (mstrTipo = "N")
		lblCate.Visible = (mstrTipo = "C")
		cmbCate.Visible = (mstrTipo = "C")
		lblDist.Visible = (mstrTipo = "C")
		cmbDist.Visible = (mstrTipo = "C")
		lblEsta.Visible = (mstrTipo = "E")
		cmbEsta.Visible = (mstrTipo = "E")
		lblApel.Visible = (mstrTipo = "N")
		txtApel.Visible = (mstrTipo = "N")
		lblNomb.Visible = (mstrTipo = "N")
		txtNomb.Visible = (mstrTipo = "N")
	End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()

      Try

         'If usrSociFil.Valor Is System.DBNull.Value Then
            'Throw New AccesoBD.clsErrNeg("Debe seleccionar un socio.")
         'End If

         mstrCmd = "exec " & mstrTabla & "_consul "
         If Not usrSociFil.Valor Is DBNull.Value Then
            mstrCmd = mstrCmd & " @moso_soci_id = " & clsSQLServer.gFormatArg(usrSociFil.Valor, SqlDbType.Int)
         Else
            mstrCmd = mstrCmd & " null"
         End If
         mstrCmd = mstrCmd & ",@tipo = " & clsSQLServer.gFormatArg(mstrTipo, SqlDbType.VarChar)
         mstrCmd = mstrCmd & ",@fechadesde = " & clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
         mstrCmd = mstrCmd & ",@fechahasta = " & clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      'btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.ClieId).Text)
      'lblTitu.Text = "Socio: " & usrSociFil.Valor & " - " & usrSociFil.Apel
      'hdnSociId.Text = usrSociFil.Valor

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtFech.Fecha = .Item("moso_movi_fecha")
            lblTitu.Text = "Socio: " & .Item("_nombre_socio")
            hdnSociId.Text = .Item("moso_soci_id")
            Select Case mstrTipo
               Case "C"
                  cmbCate.Valor = .Item("moso_cate_id")
                            cmbDist.Valor = .Item("moso_dist_id").ToString
                        Case "E"
                  cmbEsta.Valor = .Item("moso_esta_id")
               Case "N"
                  txtApel.Valor = .Item("moso_apel")
                  txtNomb.Valor = .Item("moso_nomb")
            End Select
         End With

         mSetearEditor(False)
         mMostrarPanel(True)

      End If
   End Sub

   Private Sub mAgregar()
      Try
         If Me.usrSociFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un socio.")
         Else
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            'btnAlta.Enabled = True
            mMostrarPanel(True)
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      txtFech.Text = ""
      cmbCate.Limpiar()
      cmbDist.Limpiar()
      cmbEsta.Limpiar()
      txtApel.Text = ""
      txtNomb.Text = ""

      mCrearDataSet("")
      mSetearEditor(True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)

      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panDato.Visible = pbooVisi
      'panFiltros.Visible = Not panDato.Visible
      'grdDato.Visible = Not panDato.Visible

   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Alta()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      With mdsDatos.Tables(0).Rows(0)
         .Item("moso_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("moso_movi_fecha") = txtFech.Fecha
         .Item("moso_soci_id") = hdnSociId.Text
         Select Case mstrTipo
            Case "C"
                        .Item("moso_cate_id") = cmbCate.Valor
                        If cmbDist.Valor.ToString.Length > 0 Then
                            .Item("moso_dist_id") = cmbDist.Valor
                        End If
                    Case "E"
                        .Item("moso_esta_id") = cmbEsta.Valor
                    Case "N"
                        .Item("moso_apel") = txtApel.Valor
                        .Item("moso_nomb") = txtNomb.Valor
                End Select
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      Session(mstrTabla) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"
   Private Sub btnAgre_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs)
      mAgregar()
   End Sub

   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs)
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs)
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region


    'Private Sub usrSociFil_Cambio(ByVal sender As System.Object) Handles usrSociFil.Cambio
    '   mConsultar()
    'End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      mConsultar()
   End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer

            lstrRptName = "HistoCambio"

            If txtFechaDesdeFil.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaHastaFil.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If

         
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&moso_soci_id=" + IIf(usrSociFil.Valor Is DBNull.Value, "0", usrSociFil.Valor.ToString)
            lstrRpt += "&tipo=" + mstrTipo.ToString
            lstrRpt += "&fechadesde=" + lintFechaDesde.ToString
            lstrRpt += "&fechahasta=" + lintFechaHasta.ToString

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
