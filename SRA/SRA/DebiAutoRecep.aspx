<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DebiAutoRecep" CodeFile="DebiAutoRecep.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>D�bito Autom�tico - Recepci�n</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3" style="HEIGHT: 25px" vAlign="bottom" height="25" width="100%"><asp:label cssclass="opcion" id="lblTituAbm" runat="server">D�bito Autom�tico - Recepci�n</asp:label></TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD vAlign="top" colSpan="3" width="100%">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																	ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblTarjFil" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbTarjFil" class="combo" runat="server" Width="200px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblPeriFil" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtPeriFil" runat="server" cssclass="cuadrotexto" Width="20px" AceptaNull="False"
																				MaxLength="2" MaxValor="12" CantMax="2"></cc1:numberbox>
																			<cc1:combobox id="cmbPetiFil" class="combo" runat="server" Width="80px" AceptaNull="False"></cc1:combobox>&nbsp;&nbsp;
																			<asp:Label id="Label1" runat="server" cssclass="titulo">/</asp:Label>&nbsp;
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="35px" AceptaNull="False"
																				MaxLength="4" MaxValor="2090" CantMax="4"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD colSpan="3" vAlign="top" align="center" width="100%">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="deca_id" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_gene_fecha" HeaderText="Generado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_envi_fecha" HeaderText="Env�o" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="deca_rece_fecha" HeaderText="Recepci�n" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_tarj_desc" HeaderText="Tarjeta"></asp:BoundColumn>
											<asp:BoundColumn DataField="_perio" HeaderText="Per�odo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cant" HeaderText="Cant."></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="right" width="100%" colspan="3">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> D�bito</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" CausesValidation="False" Height="21px"> Socios</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
											BorderWidth="1px" Visible="False">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD height="5" colSpan="2">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
																ToolTip="Cerrar"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD width="200" align="right">
																			<asp:Label id="lblGeneFecha" runat="server" cssclass="titulo">Fecha Generaci�n:</asp:Label></TD>
																		<TD>
																			<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0">
																				<TR>
																					<TD>
																						<cc1:DateBox id="txtGeneFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																					<TD align="right">
																						<asp:Label id="lblEnviFecha" runat="server" cssclass="titulo">Fecha Env�o:</asp:Label>&nbsp;</TD>
																					<TD>
																						<cc1:DateBox id="txtEnviFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD width="200" align="right">
																			<asp:Label id="lblReceFecha" runat="server" cssclass="titulo">Fecha Recepci�n:</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtReceFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnio" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																		<TD>
																			<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0">
																				<TR>
																					<TD>
																						<CC1:NumberBox id="txtAnio" runat="server" cssclass="cuadrotextodeshab" Width="64px" enabled="false"
																							Obligatorio="True"></CC1:NumberBox></TD>
																					<TD align="right">
																						<asp:Label id="lblPerio" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;</TD>
																					<TD height="5">
																						<CC1:NumberBox id="txtPerio" runat="server" cssclass="cuadrotextodeshab" Width="64px" enabled="false"
																							Obligatorio="True"></CC1:NumberBox>&nbsp;&nbsp;
																						<cc1:combobox id="cmbPeti" class="combo" runat="server" Width="243px" enabled="false" Obligatorio="True"></cc1:combobox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label></TD>
																		<TD>
																			<cc1:combobox id="cmbTarj" class="combo" runat="server" Width="150px" Obligatorio="True" Enabled="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="4">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="Table4" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2" align="left">
																			<asp:datagrid id="grdDeta" runat="server" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosDeta"
																				OnPageIndexChanged="grdDeta_PageChanged" AutoGenerateColumns="False" width="95%">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="dede_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_clie_apel" HeaderText="Socio"></asp:BoundColumn>
																					<asp:BoundColumn DataField="dede_envio_impo" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																						HeaderText="Env�o"></asp:BoundColumn>
																					<asp:BoundColumn DataField="dede_total_impo" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																						HeaderText="Total"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_coti_desc" HeaderText="Cuota"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_auto" HeaderText="Auto"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_esta_desc" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="3" align="center">
																			<asp:panel id="panClie" runat="server" cssclass="titulo" Width="95%" BorderWidth="1px">
																				<TABLE style="WIDTH: 95%" id="TabClie" border="0" cellPadding="0" align="left">
																					<TR>
																						<TD vAlign="top" width="50" align="right">
																							<asp:Label id="lblSoci" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;
																						</TD>
																						<TD>
																							<UC1:CLIE id="usrSoci" runat="server" FilDocuNume="True" FilClie="True" FilSociNume="True"
																								autopostback="true" Alto="560" CampoVal="Socio" Ancho="780" Saltos="1,1,1" Tabla="Socios"
																								FilCUIT="True"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"></TD>
																						<TD>
																							<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																								<TR>
																									<TD vAlign="top" align="right">
																										<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																									<TD>
																										<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotextodeshab" Width="70px" Enabled="false"
																											EsDecimal="True"></cc1:numberbox></TD>
																									<TD vAlign="top" align="right">
																										<asp:Label id="lblTaclNume" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;
																									</TD>
																									<TD>
																										<asp:TextBox id="txtTaclNume" runat="server" cssclass="cuadrotextodeshab" Width="120px" Enabled="false"></asp:TextBox></TD>
																									<TD vAlign="top" align="right">
																										<asp:Label id="lblAutoTit" runat="server" cssclass="titulo">Autom�tico:</asp:Label>&nbsp;
																									</TD>
																									<TD>
																										<asp:Label id="lblAuto" runat="server" cssclass="desc">NO</asp:Label></TD>
																									<TD vAlign="top" align="right">
																										<asp:Label id="lblCotiDesc" runat="server" cssclass="titulo"></asp:Label>&nbsp;
																									</TD>
																								</TR>
																							</TABLE>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:&nbsp;</asp:Label></TD>
																						<TD>
																							<cc1:combobox id="cmbEsta" class="combo" runat="server" Width="281px"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD height="30" vAlign="middle" colSpan="2" align="center">
																							<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Aceptar"></asp:Button>&nbsp;
																							<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" Text="Limpiar"></asp:Button>&nbsp;
																						</TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel>
										<ASP:PANEL Runat="server" ID="panBotones">
											<TABLE width="100%">
												<TR>
													<TD colSpan="2" align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD width="100%" align="center"><A id="editar" name="editar"></A>
														<DIV style="DISPLAY: inline" id="divgraba" runat="server">
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="100px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnApro" runat="server" cssclass="boton" Width="100px" Text="Aprobar Todos"></asp:Button>&nbsp;
															<asp:Button id="btnConf" runat="server" cssclass="boton" Width="100px" CausesValidation="False"
																Text="Confirmar" Font-Bold="True"></asp:Button></DIV>
														<DIV style="DISPLAY: none" id="divproce" runat="server">
															<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                  </asp:panel></DIV>
														</SPAN></TD>
													<TD align="right">
														<CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
															ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
															ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BOTONIMAGEN></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
				<asp:textbox id="hdnDedeId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function mExportar()
		{
			document.all("divgraba").style.display ="none";
			document.all("divproce").style.display ="inline";
		}
		
		var winArch;
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
