Namespace SRA

Partial Class InspeccionFenotipica
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblCriador As System.Web.UI.WebControls.Label
    Protected WithEvents lblProducto As System.Web.UI.WebControls.Label




    Protected WithEvents lblSexoFil As System.Web.UI.WebControls.Label

    Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos_Inspec
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            'mCargarCombos()

            If (Not Page.IsPostBack) Then
                mSetearEventos()
                mCargarCombos()
                clsWeb.gCargarCombo(mstrConn, "baja_motivos_cargar @tipoSeleccion=I", cmbMotivoBaj, "id", "descrip", "S")
                'mConsultar(True)
                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        If (cmbResultado.Items.Count = 0) Then
            cmbResultado.Items.Insert(0, "Rechazado")
            cmbResultado.Items.Insert(1, "Aprobado")
            cmbResultado.Items.Insert(2, "Sin Definici�n")
        End If
        If (cmbResultadoFill.Items.Count = 0) Then
            cmbResultadoFill.Items.Insert(0, "Rechazado")
            cmbResultadoFill.Items.Insert(1, "Aprobado")
            cmbResultadoFill.Items.Insert(2, "Sin Definici�n")
            cmbResultadoFill.Items.Insert(3, "Todos")
            cmbResultadoFill.SelectedIndex = 3
        End If

        Me.PnlResultado.UpdateAfterCallBack = True
    End Sub
    Private Function mValorParametro(ByVal pstrPara As String) As String
        Dim lstrPara As String
        If pstrPara Is Nothing Then
            lstrPara = ""
        Else
            If pstrPara Is System.DBNull.Value Then
                lstrPara = ""
            Else
                lstrPara = pstrPara
            End If
        End If
        Return (lstrPara)
    End Function
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
#End Region

#Region "Inicializacion de Variables"

    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        usrProducto.Tabla = mstrProductos
        usrProducto.FilCriaNume = True
        usrProducto.ColCriaNume = True
        usrProducto.FilAsocNume = True
        usrProducto.ColAsocNume = True
        usrProducto.FilNaciFecha = True
        usrProducto.ColNaciFecha = True
        usrProducto.FilProp = True
        usrProducto.FilRpNume = True
        usrProducto.ColRpNume = True
        usrProducto.FilSraNume = True
        usrProducto.ColSraNume = True
        usrProducto.FilLaboNume = True
        usrProducto.ColCondicional = True
        usrProducto.MuestraNroCondicional = True
        usrProducto.MuestraDesc = True
    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            If Page.IsPostBack Then

                mConsultar(True)
            Else
                mConsultar(False)

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lDsDatos As DataSet

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
        lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        mSetearEditor("", False)
        Me.mCargarCombos()

        With lDsDatos.Tables(0).Rows(0)
            If Not .Item("_prdt_cria_id") Is DBNull.Value Then usrProducto.CriaOrPropId = .Item("_prdt_cria_id")
            usrProducto.Valor = .Item("prin_prdt_id")
            txtFecha.Fecha = .Item("prin_fecha")
            Me.txtInspector.Text = .Item("prin_varInspector")
            If (.Item("prdt_bamo_id") Is Nothing Or .Item("prdt_bamo_id") Is System.DBNull.Value) Then
                Me.cmbMotivoBaj.SelectedValue = ""
                Me.lblBajaOrig.Text = ""
            Else
                Try
                    Me.cmbMotivoBaj.SelectedValue = .Item("prdt_bamo_id")
                Catch
                    Me.cmbMotivoBaj.SelectedValue = ""
                    Dim fechabajaorig As DateTime = .Item("_fechabaja")
                    Me.lblBajaOrig.Text = fechabajaorig.ToShortDateString() & " - " & .Item("_MotiRechazo")
                End Try
            End If

            mMostrarPanel(True)
            lblBaja.Text = ""
            btnAlta.Enabled = True
            btnModi.Enabled = True
            btnBaja.Enabled = True
            cmbMotivoBaj.Enabled = True
            cmbResultado.Enabled = True

            ' Dario 2014-11-17 busca si deja es preparatorio
            ' parametria de razas raza_regi_prep
            Dim Raza_Cria As String = ""
            If (Not usrProducto.RazaId Is Nothing) Then
                Raza_Cria = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "razas", "raza_regi_prep", " @raza_id=" & usrProducto.RazaId)
            End If

            If (usrProducto.txtSraNumeExt.Text <> "" And Raza_Cria = "False") Then
                btnAlta.Enabled = False
                btnModi.Enabled = False
                btnBaja.Enabled = False
                cmbMotivoBaj.Enabled = False
                cmbResultado.Enabled = False
                Me.lblBaja.Text = "El producto ya tiene n�mero de inscripci�n, no se puede modificar la inspecci�n"
            Else
                If .Item("prin_resu") Is DBNull.Value Then
                    cmbResultado.SelectedIndex = 2 ' sin definicion
                    btnModi.Enabled = True
                    btnBaja.Enabled = False
                    cmbMotivoBaj.Enabled = False
                    cmbMotivoBaj.SelectedValue = ""
                    cmbResultado.Enabled = True
                    btnAlta.Enabled = False
                Else
                    If (.Item("prin_resu") = "1") Then
                        cmbResultado.SelectedIndex = IIf(.Item("prin_resu"), 1, 0)
                        btnAlta.Enabled = False
                        btnModi.Enabled = False
                        btnBaja.Enabled = False
                        cmbMotivoBaj.Enabled = False
                        cmbResultado.Enabled = False
                        Me.lblBaja.Text = "El producto ya tiene n�mero de inscripci�n, no se puede modificar la inspecci�n"
                    Else
                        cmbResultado.SelectedIndex = IIf(.Item("prin_resu"), 1, 0)
                        btnAlta.Enabled = False
                        btnModi.Enabled = True
                        btnBaja.Enabled = False
                        cmbMotivoBaj.Enabled = True
                        cmbResultado.Enabled = True
                    End If
                End If
            End If
            txtObservaciones.Valor = .Item("prin_obse")
            If Not .Item("_prdt_cria_id") Is DBNull.Value Then hdnCriaId.Text = .Item("_prdt_cria_id")
            If Not .IsNull("prin_baja_fecha") Then
                lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prin_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            End If
        End With
        Me.PnlResultado.UpdateAfterCallBack = True
        Me.PnlBotonesABM.UpdateAfterCallBack = True
        Me.PnlLeyendaBaja.UpdateAfterCallBack = True
    End Sub
    Private Sub mLimpiarFiltros()
        usrProductoFil.Limpiar()
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        txtCondicionalDesdeFil.Text = ""
        txtCondicionalHastaFil.Text = ""
        '     usrProductoFil.Sexo = 0
    End Sub
    Private Sub mAgregar()
        'mLimpiar()
        usrProducto.Limpiar()
        hdnId.Text = ""
        Me.txtFecha.Text = System.DateTime.Now.ToString("dd/MM/yyyy")
        Me.cmbResultado.SelectedIndex = 2
        Me.txtObservaciones.Text = ""
        Me.txtInspector.Text = ""
        'Me.txtFecha.Text = ""
        Me.lblBaja.Text = ""
        mMostrarPanel(True)
        'Me.usrProducto.Valor = ""
        Me.btnBaja.Enabled = False
        Me.btnModi.Enabled = False
        Me.btnAlta.Enabled = True
        Me.cmbResultado.Enabled = True
        Me.cmbMotivoBaj.SelectedValue = ""
        Me.cmbMotivoBaj.Enabled = False

            '    Me.usrProducto.usrClieDerivNewExt.Activo = True
            'Me.usrProducto.usrClieDerivNewExt.cmbCriaRazaExt.SelectedValue = Me.usrProductoFil.usrClieDerivNewExt.cmbCriaRazaExt.SelectedValue
            'Me.usrProducto.usrClieDerivNewExt.RazaCodi = Me.usrProductoFil.usrClieDerivNewExt.RazaCodi
            'Me.usrProducto.usrClieDerivNewExt.txtApelExt.Text = Me.usrProductoFil.usrClieDerivNewExt.txtApelExt.Text
            'Me.usrProducto.usrClieDerivNewExt.txtCodiExt.Text = Me.usrProductoFil.usrClieDerivNewExt.txtCodiExt.Text
        Me.usrProducto.cmbSexoProdExt.SelectedValue = Me.usrProductoFil.cmbSexoProdExt.SelectedValue
        Me.PnlResultado.UpdateAfterCallBack = True
        Me.PnlBotonesABM.UpdateAfterCallBack = True
        Me.PnlLeyendaBaja.UpdateAfterCallBack = True
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnCriaId.Text = ""
        usrProducto.Limpiar()
        cmbResultado.SelectedIndex = 2
        txtObservaciones.Text = ""
        txtFecha.Text = ""
        lblBaja.Text = ""
        Me.lblBajaOrig.Text = ""
        mSetearEditor("", True)
    End Sub
    Private Sub mCerrar()
        mConsultarGrilla()
    End Sub
    Private Sub mConsultarGrilla()
        Try
            'mConsultar(True)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        btnAgre.Visible = Not (panDato.Visible)
        btnList.Enabled = Not (panDato.Visible)
        btnList.Visible = Not (panDato.Visible)
        panFiltro.Visible = Not (panDato.Visible)
        grdDato.Visible = Not (panDato.Visible)
        cmbMotivoBaj.Enabled = Not (panDato.Visible)
        cmbResultado.Enabled = Not (panDato.Visible)
        panCabecera.Visible = True
        If lblBaja.Text <> "" Then
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnLimp.Enabled = False
        Else
            btnBaja.Enabled = True
            btnModi.Enabled = True
            btnLimp.Enabled = True
        End If

    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim objProductoInspecionesFeno As New Entities.ProductoInspecionesFenoEntity
            objProductoInspecionesFeno = Me.mGuardarDatosEntidad()

            Dim objProductosBusiness As New Business.Productos.ProductosBusiness

            Dim respuesta As Boolean = objProductosBusiness.CreaModificaInspeccionFeno(objProductoInspecionesFeno)

            If (respuesta) Then
                mProcesar()

                mConsultar(False)

                mMostrarPanel(False)
            Else
                Throw New Exception("Error en la operacion de actualizacion")
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim objProductoInspecionesFeno As New Entities.ProductoInspecionesFenoEntity
            objProductoInspecionesFeno = Me.mGuardarDatosEntidad()

            Dim objProductosBusiness As New Business.Productos.ProductosBusiness

            Dim respuesta As Boolean = objProductosBusiness.CreaModificaInspeccionFeno(objProductoInspecionesFeno)

            If (respuesta) Then
                mProcesar()

                mConsultar(False)

                mMostrarPanel(False)
            Else
                Throw New Exception("Error en la operacion de actualizacion")
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim ldsDatos As DataSet = mGuardarDatosBaja()
            Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

            lobjinstnEnti.Modi()

            mConsultar(True)

            mMostrarPanel(False)
            'Dim lintPage As Integer = grdDato.CurrentPageIndex

            'Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            'lobjinstnEnti.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            'grdDato.CurrentPageIndex = 0

            'mConsultar()

            'If (lintPage < grdDato.PageCount) Then
            '    grdDato.CurrentPageIndex = lintPage
            'End If

            'mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
        If Me.cmbMotivoBaj.Enabled And Me.cmbMotivoBaj.SelectedValue.ToString.Length = 0 Then
            Throw New Exception("Debe seleccionar un motivo de baja")
        End If
    End Sub
    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        With ldsEsta.Tables(0).Rows(0)
            .Item("prin_prdt_id") = usrProducto.Valor
            .Item("prin_fecha") = txtFecha.Fecha
            If cmbResultado.SelectedIndex <> "2" Then
                .Item("prin_resu") = cmbResultado.SelectedIndex
            Else
                .Item("prin_resu") = DBNull.Value
            End If
            .Item("prin_obse") = txtObservaciones.Text
            .Item("prin_tipo") = "P"
        End With

        Return ldsEsta
    End Function
    Private Function mGuardarDatosEntidad() As Entities.ProductoInspecionesFenoEntity
        Dim respuesta As New Entities.ProductoInspecionesFenoEntity
        mValidarDatos()

        If (hdnId.Text <> "") Then
            respuesta.prin_id = Convert.ToInt32(hdnId.Text)
        Else
            respuesta.prin_id = -1
        End If

        respuesta.prin_prdt_id = usrProducto.Valor
        respuesta.prin_fecha = txtFecha.Fecha
        If cmbResultado.SelectedIndex <> "2" Then
            respuesta.prin_resu = cmbResultado.SelectedIndex
        Else
            respuesta.prin_resu = -1
        End If

        respuesta.prin_obse = txtObservaciones.Text
        respuesta.prin_tipo = "P"
        respuesta.prin_audi_user = Session("sUserId")
        respuesta.prin_bitTrim = usrProducto.Trimestre()
        If (Me.usrProducto.FechaNaci() <> "") Then
            respuesta.prdt_naci_fecha = Convert.ToDateTime(Me.usrProducto.FechaNaci())
        Else
            respuesta.prdt_naci_fecha = System.DateTime.MinValue
        End If
        respuesta.prin_clie_id = -1
        respuesta.prin_varInspector = Me.txtInspector.Text
        If (Me.usrProducto.GetSexoProd() <> "") Then
            respuesta.prdt_sexo = Me.usrProducto.GetSexoProd()
        End If

        respuesta.prdt_px = Me.usrProducto.PX()
        If (respuesta.prin_resu = 0) Then
            respuesta.prdt_bamo_id = Convert.ToInt32(Me.cmbMotivoBaj.SelectedValue)
            respuesta.prdt_baja_fecha = System.DateTime.Now
        Else
            respuesta.prdt_bamo_id = -1
            respuesta.prdt_baja_fecha = System.DateTime.MinValue
        End If
        If (Me.usrProducto.cmbTipoRegistroExt.SelectedValue <> "") Then
            respuesta.prdt_regt_id = Convert.ToInt32(Me.usrProducto.cmbTipoRegistroExt.SelectedValue)
        Else
            respuesta.prdt_regt_id = -1
        End If
        If (Me.usrProducto.cmbVariadadExt.SelectedValue <> "") Then
            respuesta.prdt_vari_id = Convert.ToInt32(Me.usrProducto.cmbVariadadExt.SelectedValue)
        Else
            respuesta.prdt_vari_id = -1
        End If

        Return respuesta
    End Function
    Private Function mGuardarDatosBaja() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        With ldsEsta.Tables(0).Rows(0)
            .Item("prin_prdt_id") = usrProducto.Valor
            .Item("prin_fecha") = txtFecha.Fecha
            If cmbResultado.SelectedIndex <> 2 Then
                .Item("prin_resu") = cmbResultado.SelectedIndex
            Else
                .Item("prin_resu") = DBNull.Value
            End If
            .Item("prin_obse") = txtObservaciones.Text
            .Item("prin_tipo") = "P"
            .Item("prin_baja_fecha") = Date.Now
        End With

        Return ldsEsta
    End Function
#End Region

#Region "Eventos de Controles"
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            mConsultar(False)
            mMostrarPanel(False)
            btnAgre.Visible = True

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "InspeccionFenotipica"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer

            If (ValidarFiltros() = False) Then
                Return
            End If

            If txtFechaDesdeFil.Fecha.ToString = "" Then
                lintFechaDesde = 0
            Else
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaHastaFil.Fecha.ToString = "" Then
                lintFechaHasta = 0
            Else
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If

            lstrRpt += "&prin_tipo=P"
            lstrRpt += "&fechaDesde=" + lintFechaDesde.ToString
            lstrRpt += "&fechaHasta=" + lintFechaHasta.ToString
            lstrRpt += "&cria_id=" + usrProductoFil.CriaOrPropId.ToString
            lstrRpt += "&prin_prdt_id=" + usrProductoFil.Valor.ToString

            If usrProductoFil.RazaId <> "" And usrProductoFil.RazaId <> "0" Then
                lstrRpt = lstrRpt & "&prdt_raza_id=" & usrProductoFil.RazaId
            Else
                lstrRpt = lstrRpt & "&prdt_raza_id=0"
            End If

            If Not txtCondicionalDesdeFil.Valor Is DBNull.Value Then
                lstrRpt = lstrRpt & "&NroCondicionalDesde=" & txtCondicionalDesdeFil.Text
            Else
                lstrRpt = lstrRpt & "&NroCondicionalDesde=0"
            End If

            If Not txtCondicionalHastaFil.Valor Is DBNull.Value Then
                lstrRpt = lstrRpt & "&NroCondicionalHasta=" & txtCondicionalHastaFil.Text
            Else
                lstrRpt = lstrRpt & "&NroCondicionalHasta=0"
            End If

            If (usrProductoFil.cmbTipoRegistroExt.SelectedValue <> "" And Not usrProductoFil.cmbTipoRegistroExt.SelectedValue Is DBNull.Value) Then
                lstrRpt = lstrRpt & "&IdTipoRegistro=" & usrProductoFil.cmbTipoRegistroExt.SelectedValue
            Else
                lstrRpt = lstrRpt & "&IdTipoRegistro=0"
            End If

            If (usrProductoFil.cmbVariadadExt.SelectedValue <> "" And Not usrProductoFil.cmbVariadadExt.SelectedValue Is DBNull.Value) Then
                lstrRpt = lstrRpt & "&IdVariadad=" & usrProductoFil.cmbVariadadExt.SelectedValue
            Else
                lstrRpt = lstrRpt & "&IdVariadad=0"
            End If

            If (usrProductoFil.GetSexoProd() <> "" And Not usrProductoFil.GetSexoProd() Is DBNull.Value) Then
                lstrRpt = lstrRpt & "&sexo=" & usrProductoFil.GetSexoProd()
            Else
                lstrRpt = lstrRpt & "&sexo=2"
            End If

            If Not usrProductoFil.txtSraNumeExt.Text Is DBNull.Value And usrProductoFil.txtSraNumeExt.Text <> "" Then
                lstrRpt = lstrRpt & "&SraNume=" & usrProductoFil.txtSraNumeExt.Text
            Else
                lstrRpt = lstrRpt & "&SraNume=0"
            End If

            If Not usrProductoFil.txtRPExt.Text Is DBNull.Value And usrProductoFil.txtRPExt.Text <> "" Then
                lstrRpt = lstrRpt & "&RP=" & usrProductoFil.txtRPExt.Text
            Else
                lstrRpt = lstrRpt & "&RP="
            End If

            lstrRpt = lstrRpt & "&ResultInspec=" & cmbResultadoFill.SelectedIndex.ToString

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub
#End Region

    Private Sub mProcesar()
        Try
            Dim lstrMsgError As String
            Dim lstrProdId As String
            Dim lstrRazaId As String
            Dim lstrSexo As String
            Dim lstrId As String
            Dim lstrbamoId As String

            If (cmbResultado.SelectedIndex = 1) Then
                lstrProdId = usrProducto.ProdId
                lstrRazaId = usrProducto.RazaId 'clsSQLServer.gObtenerValorCampo(mstrConn, "productos", "@prdt_id=" & lstrProdId, "prdt_raza_id")
                lstrSexo = usrProducto.Sexo
                'IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
                Dim pdinid As Object = clsSQLServer.gObtenerValorCampo(mstrConn, "productos", "@prdt_id=" & lstrProdId, "prdt_pdin_id")
                lstrId = IIf(pdinid Is System.DBNull.Value, "-1", pdinid)
                ' Dario 2015-08-20 se agrega que no nuemre si esta dado de baja
                Dim bamoId As Object = clsSQLServer.gObtenerValorCampo(mstrConn, "productos", "@prdt_id=" & lstrProdId, "prdt_bamo_id")
                lstrbamoId = IIf(bamoId Is System.DBNull.Value, "0", "-1")

                If (lstrId <> "-1" And cmbResultado.SelectedIndex = 1 And lstrbamoId <> "-1" _
                    And (usrProducto.txtSraNumeExt.Valor Is System.DBNull.Value Or usrProducto.txtSraNumeExt.Text = "")) Then
                    'Limpiar errores anteriores.
                    ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId)

                    'Aplicar Reglas.
                    lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId, lstrRazaId, lstrSexo, Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), 0, 0)
                End If

                If lstrMsgError <> "" Then
                    AccesoBD.clsError.gGenerarMensajes(Me, lstrMsgError)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#Region "Detalle"

    Public Function ValidarFiltros() As Boolean
        Dim respuesta As Boolean = False
        Dim ponderFiltros As Int16 = 0
        Try
            If (usrProductoFil.RazaId <> "0" And usrProductoFil.RazaId <> "") Then
                ponderFiltros = ponderFiltros + 15
            End If

            If (usrProductoFil.CriaOrPropId <> "0" And usrProductoFil.CriaOrPropId <> "") Then
                ponderFiltros = ponderFiltros + 10
            End If

            If Not txtFechaDesdeFil.Fecha Is DBNull.Value Then
                ponderFiltros = ponderFiltros + 8
            End If

            If Not txtFechaHastaFil.Fecha Is DBNull.Value Then
                ponderFiltros = ponderFiltros + 8
            End If

            If Not txtCondicionalDesdeFil.Valor Is DBNull.Value Then
                ponderFiltros = ponderFiltros + 8
            End If

            If Not txtCondicionalHastaFil.Valor Is DBNull.Value Then
                ponderFiltros = ponderFiltros + 8
            End If

            If (usrProductoFil.cmbTipoRegistroExt.SelectedValue <> "" And Not usrProductoFil.cmbTipoRegistroExt.SelectedValue Is DBNull.Value) Then
                ponderFiltros = ponderFiltros + 1
            End If

            If (usrProductoFil.cmbVariadadExt.SelectedValue <> "" And Not usrProductoFil.cmbVariadadExt.SelectedValue Is DBNull.Value) Then
                ponderFiltros = ponderFiltros + 1
            End If

            If (usrProductoFil.Sexo <> "" And Not usrProductoFil.Sexo Is DBNull.Value) Then
                ponderFiltros = ponderFiltros + 4
            End If

            If Not usrProductoFil.txtSraNumeExt.Text Is DBNull.Value And usrProductoFil.txtSraNumeExt.Text <> "" Then
                ponderFiltros = ponderFiltros + 50
            End If

            If Not usrProductoFil.txtRPExt.Text Is DBNull.Value And usrProductoFil.txtRPExt.Text <> "" Then
                ponderFiltros = ponderFiltros + 20
            End If

            If (cmbResultadoFill.SelectedIndex <> 3) Then
                ponderFiltros = ponderFiltros + 10
            End If

            If (ponderFiltros < Convert.ToInt32("0" & hdnPonderFill.Text)) Then
                Throw New System.Exception("Selecci�n de filtros insuficientes, agregue mas filtros a la b�squeda")
            Else
                respuesta = True
            End If

            Return respuesta
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

    Public Sub mConsultar(ByVal boolMostrarTodos As Boolean)
        If (ValidarFiltros() = False) Then
            Return
        End If
        Try

            Dim dsDatos As New DataSet

            mstrCmd = "exec " & mstrTabla & "_consul "

            mstrCmd += "@prin_tipo = 'P'"
            If Not txtFechaDesdeFil.Fecha Is DBNull.Value Then
                mstrCmd = mstrCmd & ",@FechaDesde=" & clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
            End If

                If txtFechaHastaFil.Fecha.ToString.Length > 0 Then
                    mstrCmd = mstrCmd & ",@FechaHasta=" & clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)
                End If

                If usrProductoFil.RazaId.ToString.Length > 0 And usrProductoFil.RazaId <> "0" Then
                    mstrCmd = mstrCmd & ",@prdt_raza_id= " & usrProductoFil.RazaId
                End If

                If (usrProductoFil.CriaOrPropId.ToString.Length > 0 And usrProductoFil.CriaOrPropId.ToString <> "0") Then
                    mstrCmd = mstrCmd & ",@cria_id= " & usrProductoFil.CriaOrPropId.ToString
                End If

                If (usrProductoFil.Valor.ToString.Length > 0 And usrProductoFil.Valor.ToString <> "0") Then
                    mstrCmd = mstrCmd & ",@prin_prdt_id=" & usrProductoFil.Valor.ToString
                End If

                If txtCondicionalDesdeFil.Valor.Length > 0 Then
                    mstrCmd = mstrCmd & ",@NroCondicionalDesde=" & txtCondicionalDesdeFil.Text
                End If

                If txtCondicionalHastaFil.Valor.Length > 0 Then
                    mstrCmd = mstrCmd & ",@NroCondicionalHasta=" & txtCondicionalHastaFil.Text
                End If

                If (usrProductoFil.cmbTipoRegistroExt.SelectedValue.Length > 0 And usrProductoFil.cmbTipoRegistroExt.SelectedValue.Length > 0) Then
                    mstrCmd = mstrCmd & ",@IdTipoRegistro=" & usrProductoFil.cmbTipoRegistroExt.SelectedValue
                End If

                If (usrProductoFil.cmbVariadadExt.SelectedValue.Length <> 0 And usrProductoFil.cmbVariadadExt.SelectedValue.Length > 0) Then
                    mstrCmd = mstrCmd & ",@IdVariadad=" & usrProductoFil.cmbVariadadExt.SelectedValue
                End If

            If (usrProductoFil.GetSexoProd() <> "" And Not usrProductoFil.GetSexoProd() Is DBNull.Value) Then
                mstrCmd = mstrCmd & ",@sexo=" & usrProductoFil.GetSexoProd()
            End If

                If Not usrProductoFil.txtSraNumeExt.Text.Length = 0 And usrProductoFil.txtSraNumeExt.Text.Length > 0 Then
                    mstrCmd = mstrCmd & ",@SraNume=" & usrProductoFil.txtSraNumeExt.Text
                End If

                If usrProductoFil.txtRPExt.Text.Length > 0 And usrProductoFil.txtRPExt.Text <> "" Then
                    mstrCmd = mstrCmd & ",@RP='" & usrProductoFil.txtRPExt.Text & "'"
                End If

            mstrCmd = mstrCmd & ",@ResultInspec=" & cmbResultadoFill.SelectedIndex.ToString

            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)
            grdDato.DataSource = dsDatos.Tables(0)
            If dsDatos.Tables(0).Rows.Count / grdDato.PageSize <= grdDato.CurrentPageIndex Then
                grdDato.CurrentPageIndex = 0
            End If
            grdDato.Visible = True
            grdDato.DataSource = dsDatos
            grdDato.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region
#Region "Metodos que se ejecutan en RazaChange de los controles producto"
    'Private Sub usrProductoFil_Cambio(ByVal sender As DropDownList) Handles usrProductoFil.RazaChange

    'End Sub
    'Private Sub usrProducto_Cambio(ByVal sender As DropDownList) Handles usrProducto.RazaChange

    'End Sub

    Private Sub txtId_Cambio(ByVal sender As string) Handles usrProducto.IdProdSelected
        Dim lDsDatos As DataSet

        'Dim obj As usrProductoNew = CType(sender, usrProductoNew)

        'If (obj.txtIdExt.Text <> "") Then
        If (sender <> "") Then
            'lDsDatos = clsSQLServer.gExecuteQuery(mstrConn, "exec " & mstrTabla & "_consul @prin_prdt_id= " & obj.Valor)
            'lDsDatos = clsSQLServer.gExecuteQuery(mstrConn, "exec " & mstrTabla & "_consul @prin_prdt_id= " & sender)
            lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, " @prin_prdt_id= " & sender)
            mSetearEditor("", False)
            Me.mCargarCombos()

            If (lDsDatos.Tables(0).Rows.Count = 1) Then
                With lDsDatos.Tables(0).Rows(0)
                    hdnId.Text = .Item("prin_id")
                    If Not .Item("_prdt_cria_id") Is DBNull.Value Then usrProducto.CriaOrPropId = .Item("_prdt_cria_id")
                    usrProducto.Valor = .Item("prin_prdt_id")
                    txtFecha.Fecha = .Item("prin_fecha")
                    Me.txtInspector.Text = .Item("prin_varInspector")

                    If (.Item("prdt_bamo_id") Is Nothing Or .Item("prdt_bamo_id") Is System.DBNull.Value) Then
                        Me.cmbMotivoBaj.SelectedValue = ""
                        Me.lblBajaOrig.Text = ""
                    Else
                        Try
                            Me.cmbMotivoBaj.SelectedValue = .Item("prdt_bamo_id")
                        Catch
                            Me.cmbMotivoBaj.SelectedValue = ""
                            Dim fechabajaorig As DateTime = .Item("_fechabaja")
                            Me.lblBajaOrig.Text = fechabajaorig.ToShortDateString() & " - " & .Item("_MotiRechazo")
                        End Try
                    End If

                    mMostrarPanel(True)
                    lblBaja.Text = ""
                    btnAlta.Enabled = True
                    btnModi.Enabled = True
                    btnBaja.Enabled = True
                    cmbMotivoBaj.Enabled = True
                    cmbResultado.Enabled = True

                    ' Dario 2014-11-17 busca si deja es preparatorio
                    ' parametria de razas raza_regi_prep
                    Dim Raza_Cria As String = ""
                    If (Not usrProducto.RazaId Is Nothing Or usrProducto.RazaId > -1) Then
                        Raza_Cria = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "razas", "raza_regi_prep", " @raza_id=" & usrProducto.RazaId)
                    End If

                    If (usrProducto.txtSraNumeExt.Text <> "" And Raza_Cria = "False") Then
                        btnAlta.Enabled = False
                        btnModi.Enabled = False
                        btnBaja.Enabled = False
                        cmbMotivoBaj.Enabled = False
                        cmbResultado.Enabled = False
                        Me.lblBaja.Text = "El producto ya tiene n�mero de inscripci�n, no se puede modificar la inspecci�n"
                    Else
                        If .Item("prin_resu") Is DBNull.Value Then
                            cmbResultado.SelectedIndex = 2 ' sin definicion
                            btnModi.Enabled = True
                            btnBaja.Enabled = False
                            cmbMotivoBaj.Enabled = False
                            cmbMotivoBaj.SelectedValue = ""
                            cmbResultado.Enabled = True
                            btnAlta.Enabled = False
                        Else
                            If (.Item("prin_resu") = "1") Then
                                cmbResultado.SelectedIndex = IIf(.Item("prin_resu"), 1, 0)
                                btnAlta.Enabled = False
                                btnModi.Enabled = False
                                btnBaja.Enabled = False
                                cmbMotivoBaj.Enabled = False
                                cmbResultado.Enabled = False
                                Me.lblBaja.Text = "La inspecci�n esta Aprobada no se puede ser modificada"
                            Else
                                cmbResultado.SelectedIndex = IIf(.Item("prin_resu"), 1, 0)
                                btnAlta.Enabled = False
                                btnModi.Enabled = True
                                btnBaja.Enabled = False
                                cmbMotivoBaj.Enabled = True
                                cmbResultado.Enabled = True
                            End If
                        End If
                    End If
                    txtObservaciones.Valor = .Item("prin_obse")
                    If Not .Item("_prdt_cria_id") Is DBNull.Value Then hdnCriaId.Text = .Item("_prdt_cria_id")
                    If Not .IsNull("prin_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prin_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    End If
                End With
                Me.PnlResultado.UpdateAfterCallBack = True
                Me.PnlBotonesABM.UpdateAfterCallBack = True
                Me.PnlLeyendaBaja.UpdateAfterCallBack = True
            Else
                ' Dario 2014-11-19 busca si deja es preparatorio
                ' parametria de razas raza_regi_prep
                Dim Raza_Cria As String = ""
                If (Not usrProducto.RazaId Is Nothing Or usrProducto.RazaId > -1) Then
                    Raza_Cria = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "razas", "raza_regi_prep", " @raza_id=" & usrProducto.RazaId)
                End If

                mMostrarPanel(True)
                hdnId.Text = ""
                Me.txtInspector.Text = ""
                Me.cmbMotivoBaj.SelectedValue = ""
                Me.txtObservaciones.Valor = ""
                lblBaja.Text = ""
                btnAlta.Enabled = True
                btnModi.Enabled = False
                btnBaja.Enabled = False
                cmbResultado.Enabled = True
                cmbResultado.SelectedIndex = 2 ' sin definicion
                If (usrProducto.txtSraNumeExt.Text <> "" And Raza_Cria = "False") Then
                    btnAlta.Enabled = False
                    cmbMotivoBaj.Enabled = False
                    cmbResultado.Enabled = False
                    Me.lblBaja.Text = "El producto ya tiene n�mero de inscripci�n, no se puede modificar la inspecci�n"
                End If
                Me.PnlResultado.UpdateAfterCallBack = True
                Me.PnlBotonesABM.UpdateAfterCallBack = True
                Me.PnlLeyendaBaja.UpdateAfterCallBack = True
            End If
        End If
    End Sub

#End Region

    Private Sub cmbResultado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbResultado.SelectedIndexChanged
        If (cmbResultado.SelectedValue = "Rechazado") Then
            btnBaja.Enabled = True
            btnModi.Enabled = True
            btnLimp.Enabled = True
            Me.cmbMotivoBaj.Enabled = True
        Else
            Me.cmbMotivoBaj.Enabled = False
            Me.cmbMotivoBaj.SelectedIndex = -1
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnLimp.Enabled = False
        End If
        Me.PnlResultado.UpdateAfterCallBack = True
    End Sub
End Class

End Namespace
