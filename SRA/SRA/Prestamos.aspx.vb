Imports ReglasValida.Validaciones
Imports SRA_Entidad
Imports SRA_Neg.Utiles


Namespace SRA


Partial Class Prestamos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents usrCria As usrClieDeriv
    '  Protected WithEvents usrProdFilDeta As usrProducto

    Protected WithEvents usrProd As usrProducto
    Protected WithEvents txtDisplayPageNo As System.Web.UI.WebControls.Label
    Protected WithEvents lblClieFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblCria As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipoMovimiento As System.Web.UI.WebControls.Label
    Protected WithEvents lblCtroImpla As System.Web.UI.WebControls.Label

    Protected WithEvents lblProducto As System.Web.UI.WebControls.Label
    Protected WithEvents btnAltaDeta As System.Web.UI.WebControls.Button
    Protected WithEvents btnBajaDeta As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimpDeta As System.Web.UI.WebControls.Button
    Protected WithEvents cmbTipoMovimientoFil As NixorControls.ComboBox
    Protected WithEvents lblCliePrestaFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblCliePresta As System.Web.UI.WebControls.Label
    Protected WithEvents lblCliePrestataFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblCliePrestata As System.Web.UI.WebControls.Label

    Protected WithEvents lblSraNumeFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtSraNumeFil As NixorControls.NumberBox
    Protected WithEvents txtSexoBusq As NixorControls.TextBoxTab
    Protected WithEvents lblTipoMovimientoEmbargoFil As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private PageCount As Integer
    Private maxRec As Integer
    Private pageSize As Integer
    Private currentPage As Integer
    Private recNo As Integer
    Private dtSource As DataTable
    Public mintProce As Integer


    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Prestamos
    Private mstrTablaDetalle As String = SRA_Neg.Constantes.gTab_PrestamosDeta
    Private mstrCmd As String
    Private mstrConn As String
    Public strConn As String
    Public sUserId As String
    Private mdsDatos As DataSet
    Private mstrParaPageSize As Integer
    Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Const mProdTodos = 0
    Const mProdUno = 1
    Const mProdConjun = 2
    Private mstrTramiteId As String = ""
    Private mstrTipo As String = ""
    Private boolAgregar As Boolean = False




#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            mstrConn = clsWeb.gVerificarConexion(Me)
            strConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            sUserId = Session("sUserId").ToString()

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mSetearMaxLength()
                mSetearEventos()
                mConsultar(True)
                mCargarCombos()
                clsWeb.gInicializarControles(Me, mstrConn)
                mstrTramiteId = Request.QueryString("Tram_Id")
                mstrTipo = Request.QueryString("Tipo")

                If mstrTramiteId <> "" Then
                    hdnId.Text = mstrTramiteId
                    Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())

                    mEditarDatos(mstrTramiteId)
                    mMostrarPanel(True)
                End If
                If mstrTipo <> "" Then
                    If mstrTipo = 0 Then
                        cmbTipoFil.Valor = 0
                    ElseIf mstrTipo = 1 Then
                        cmbTipoFil.Valor = 1

                    ElseIf mstrTipo = 2 Then
                        cmbTipoFil.Valor = 2
                    End If


                    cmbTipo.Valor = cmbTipoFil.Valor
                    cmbTipo.Enabled = False
                End If


                If mstrTramiteId = "" Then
                    mSetearMaxLength()
                    mSetearEventos()
                    mCargarCombos()
                    mMostrarPanel(False)
                End If
            Else

                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
            If mstrTipo <> "" Then
                If mstrTipo = 0 Then

                    cmbTipoFil.Valor = 0
                ElseIf mstrTipo = 1 Then
                    cmbTipoFil.Valor = 1

                ElseIf mstrTipo = 2 Then
                    cmbTipoFil.Valor = 2
                End If


                cmbTipo.Valor = cmbTipoFil.Valor
                cmbTipo.Enabled = False
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        '  btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")

    End Sub


    Public Sub mInicializar()

        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        mintProce = ReglasValida.Validaciones.Procesos.Prestamos
        usrCriaPresta.Tabla = mstrCriadores
        usrCriaPresta.Criador = True
        usrCriaPresta.Ancho = 790
        usrCriaPresta.Alto = 510
        usrCriaPresta.ColDocuNume = False
        usrCriaPresta.ColCUIT = True
        usrCriaPresta.ColCriaNume = True
        usrCriaPresta.FilCUIT = True
        usrCriaPresta.FilDocuNume = True
        usrCriaPresta.FilAgru = False
        usrCriaPresta.FilTarjNume = False

        usrCriaPrestaFil.Tabla = mstrCriadores
        usrCriaPrestaFil.Criador = True
        usrCriaPrestaFil.AutoPostback = False
        usrCriaPrestaFil.FilClaveUnica = False
        usrCriaPrestaFil.ColClaveUnica = True
        usrCriaPrestaFil.Ancho = 790
        usrCriaPrestaFil.Alto = 510
        usrCriaPrestaFil.ColDocuNume = False
        usrCriaPrestaFil.ColCUIT = True
        usrCriaPrestaFil.ColCriaNume = True
        usrCriaPrestaFil.FilCUIT = True
        usrCriaPrestaFil.FilDocuNume = True
        usrCriaPrestaFil.FilAgru = False
        usrCriaPrestaFil.FilTarjNume = False

        usrCriaPrestataFil.Tabla = mstrCriadores
        usrCriaPrestataFil.Criador = True
        usrCriaPrestataFil.AutoPostback = False
        usrCriaPrestataFil.FilClaveUnica = False
        usrCriaPrestataFil.ColClaveUnica = True
        usrCriaPrestataFil.Ancho = 790
        usrCriaPrestataFil.Alto = 510
        usrCriaPrestataFil.ColDocuNume = False
        usrCriaPrestataFil.ColCUIT = True
        usrCriaPrestataFil.ColCriaNume = True
        usrCriaPrestataFil.FilCUIT = True
        usrCriaPrestataFil.FilDocuNume = True
        usrCriaPrestataFil.FilAgru = False
        usrCriaPrestataFil.FilTarjNume = False

        usrCriaPrestata.Tabla = mstrCriadores
        usrCriaPrestata.Criador = True
        usrCriaPrestata.AutoPostback = False
        usrCriaPrestata.FilClaveUnica = False
        usrCriaPrestata.ColClaveUnica = True
        usrCriaPrestata.Ancho = 790
        usrCriaPrestata.Alto = 510
        usrCriaPrestata.ColDocuNume = False
        usrCriaPrestata.ColCUIT = True
        usrCriaPrestata.ColCriaNume = True
        usrCriaPrestata.FilCUIT = True
        usrCriaPrestata.FilDocuNume = True
        usrCriaPrestata.FilAgru = False
        usrCriaPrestata.FilTarjNume = False
       
        'usrProdFilDeta.Tabla = mstrProductos
        'usrProdFilDeta.AutoPostback = False
        'usrProdFilDeta.Ancho = 790
        'usrProdFilDeta.Alto = 510
        'usrProdFilDeta.CriaId = hdnCriaId.Text

        'usrProdFilDeta.BloqCriaFil = True

        'usrProdFilDeta.PermiModi = False


        'usrProdFilDeta.MuestraAsocExtr = False
        'usrProdFilDeta.MuestraNroAsocExtr = False

        usrProdFil.cmbProdAsocExt.Enabled = False
        usrProdFil.txtCodiExt.Enabled = False

        usrProdFil.MuestraDesc = False

        'usrProdFilDeta.Raza = usrCriaPresta.cmbCriaRazaExt.Valor
        'usrProdFilDeta.CriaOrPropId = hdnCriaId.Text

        'usrProdFilDeta.IncluirDeshabilitados = True
        ' usrClieFil.IncluirDeshabilitados = True


        usrProdFil.MuestraAsocExtr = False
        usrProdFil.MuestraNroAsocExtr = False

        'usrProdFilDeta.usrRazaCriadorExt.ColCriaNume = False
        'usrProdFilDeta.usrRazaCriadorExt.Raza = False
        'usrProdFilDeta.usrRazaCriadorExt.FilCriaNume = False
        'usrProdFilDeta.usrRazaCriadorExt.cmbCriaRazaExt.Enabled = False

        'usrProdFilDeta.usrRazaCriadorExt.txtCodiExt.Enabled = False
  

    End Sub


    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pstm_obse")
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Prestamos) + ",@defa=1")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Prestamos) + ",@defa=1")


        cmbEstadoFil.Items.Add(New System.Web.UI.WebControls.ListItem("Ingresado Finalizado", "11"))
        Dim i As Int16

        For i = 1 To cmbEstadoFil.Items.Count - 1
            If cmbEstadoFil.Items(i).Text.ToUpper() = "INGRESADO" Then
                cmbEstadoFil.Items(i).Text = "Ingresado Activo"
            End If

        Next

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

    

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDetalle.EditItemIndex = -1
            If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
                grdDetalle.CurrentPageIndex = 0
            Else
                grdDetalle.CurrentPageIndex = E.NewPageIndex
            End If
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            Try
                grdDetalle.CurrentPageIndex = 0
                grdDetalle.DataBind()
                grdDetalle.Visible = True
            Catch
                ' Me.lblError.Text = "No data for selection"
                ' Me.lblError.Visible = True

            End Try
        Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
    End Sub
    Public Function boolTodosVacios() As Boolean

        boolTodosVacios = True

        If usrCriaPrestaFil.Valor <> 0 Then
            boolTodosVacios = False
        End If

        If usrCriaPrestataFil.Valor <> 0 Then
            boolTodosVacios = False
        End If
        If Not txtFechaDesdeFil.Fecha Is System.DBNull.Value Then
            boolTodosVacios = False
        End If
        If Not txtFechaHastaFil.Fecha Is System.DBNull.Value Then
            boolTodosVacios = False
        End If

        If cmbEstadoFil.Valor.ToString <> "0" Then
            boolTodosVacios = False
        End If

        If usrProdFil.txtSraNumeExt.Valor.ToString <> "" Then
            boolTodosVacios = False
        End If

        If usrProdFil.cmbSexoProdExt.Valor.ToString <> "" Then
            boolTodosVacios = False
        End If

        If cmbTipoFil.SelectedIndex <> 0 Then
            boolTodosVacios = False
        End If


        If usrProdFil.RazaId <> "" Then
            boolTodosVacios = False

        Else
            If Not Session("sRazaId") Is Nothing Then
                boolTodosVacios = False
            End If

        End If

        If txtNroPrestamoFil.Text <> "" Then
            boolTodosVacios = False
        End If

        If usrProdFil.txtProdNombExt.Text <> "" Then
            boolTodosVacios = False
        End If
        If usrProdFil.txtRPExt.Text <> "" Then
            boolTodosVacios = False
        End If

    End Function

    Public Sub mConsultar()
        Try
            Dim dsDatos As New DataSet
            Dim valor As Int16
            If (Page.IsPostBack) And hdnValidar.Text <> "N" Then
                If (usrProdFil.cmbProdRazaExt Is Nothing And usrCriaPrestaFil.cmbCriaRazaExt.Valor = 0 _
                    And usrCriaPrestataFil.cmbCriaRazaExt.Valor = 0 And boolTodosVacios()) Then
                    Throw New AccesoBD.clsErrNeg("Debe especificar al menos una Raza para Consultar.")

                End If
            End If



            mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + " @pstm_hace_cria_id=" + IIf(usrCriaPrestaFil.Valor.ToString = "", "0", usrCriaPrestaFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@pstm_reci_cria_id=" + IIf(usrCriaPrestataFil.Valor.ToString = "", "0", usrCriaPrestataFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)

            If Not txtFechaHastaFil.Fecha Is System.DBNull.Value Then
                mstrCmd = mstrCmd + ",@fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)
            Else
                mstrCmd = mstrCmd + ",@fecha_hasta= null"
            End If

            If cmbEstadoFil.Valor.ToString <> "0" Then
                If cmbEstadoFil.Valor.ToString = SRA_Neg.Constantes.Estados.Prestamos_Ingresado Or _
                 cmbEstadoFil.Valor.ToString = SRA_Neg.Constantes.Estados.Prestamos_Retenido Or _
                 cmbEstadoFil.Valor.ToString = SRA_Neg.Constantes.Estados.Prestamos_Anulado Then
                    mstrCmd = mstrCmd + ",@pstm_esta_id=" + cmbEstadoFil.Valor.ToString
                End If

                valor = SRA_Neg.Constantes.Estados.Prestamos_Ingresado

                If cmbEstadoFil.Valor.ToString = "11" Then ' estado 11 es ficticio ya que se crea solo para la consulta
                    mstrCmd = mstrCmd + ",@pstm_esta_id=" + valor.ToString
                    mstrCmd = mstrCmd + ",@estadoActual =" + "Finalizado"
                End If


                If cmbEstadoFil.Valor.ToString = SRA_Neg.Constantes.Estados.Prestamos_Ingresado Then
                    If InStr(1, cmbEstadoFil.SelectedItem.Text, "Activo") > 0 Then
                        mstrCmd = mstrCmd + ",@estadoActual =" + "Activo"
                    Else
                        mstrCmd = mstrCmd + ",@estadoActual =" + "Finalizado"
                    End If
                End If
            End If

            If usrProdFil.txtSraNumeExt.Valor.ToString <> "" Then
                mstrCmd = mstrCmd + ", @sra_nume = " + usrProdFil.txtSraNumeExt.Valor.ToString
            End If

            If usrProdFil.cmbSexoProdExt.Valor.ToString <> "" Then
                mstrCmd = mstrCmd + ", @sexo = " + usrProdFil.cmbSexoProdExt.Valor.ToString()
            End If


            If cmbTipoFil.SelectedIndex <> 0 Then
                mstrCmd = mstrCmd + ", @pstm_tipo = " + cmbTipoFil.Valor.ToString
            End If


            If usrProdFil.RazaId <> "" Then
                mstrCmd = mstrCmd + ", @raza_id = " + usrProdFil.RazaId
            Else
                If Session("sRazaId") Is Nothing Then
                    mstrCmd = mstrCmd + ", @raza_id = 0"
                Else
                    mstrCmd = mstrCmd + ", @raza_id = " + Session("sRazaId")
                End If
            End If

            If txtNroPrestamoFil.Text <> "" Then
                mstrCmd = mstrCmd + ", @pstm_id = " + txtNroPrestamoFil.Text
            End If

            If usrProdFil.txtProdNombExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @NombreProd = '" + usrProdFil.txtProdNombExt.Text + "'"
            End If
            If usrProdFil.txtRPExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @RP = '" + usrProdFil.txtRPExt.Text + "'"
            End If


            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)

            If dsDatos.Tables.Count > 0 Then
                grdDato.DataSource = dsDatos.Tables(0)
                If dsDatos.Tables(0).Rows.Count / grdDato.PageSize <= grdDato.CurrentPageIndex Then
                    grdDato.CurrentPageIndex = 0
                End If
            End If

            grdDato.DataBind()
            grdDato.Visible = True

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
     
        If cmbTipo.Valor = mProdTodos Then
            btnSeleccionarTodos.Enabled = False
            btnDeSeleccionarTodos.Enabled = False

        Else
            btnSeleccionarTodos.Enabled = True
            btnDeSeleccionarTodos.Enabled = True
        End If

        If Not boolAgregar Then
            grdDetalle.Enabled = True
            grdDetalle.Columns(0).Visible = True
        Else
            If cmbTipo.Valor = mProdTodos Then
                grdDetalle.Enabled = False
                grdDetalle.Columns(0).Visible = False
            ElseIf cmbTipo.Valor = mProdConjun Then
                grdDetalle.Enabled = True
                grdDetalle.Columns(0).Visible = True
            ElseIf cmbTipo.Valor = mProdUno Then
                grdDetalle.Enabled = True
                grdDetalle.Columns(0).Visible = True
            End If
        End If
    End Sub


    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mLimpiar()
            If Not boolAgregar Then
                grdDetalle.Enabled = True
                grdDetalle.Columns(0).Visible = True
            Else
                grdDetalle.Enabled = False
                grdDetalle.Columns(0).Visible = False
            End If
            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

            mCrearDataSet(hdnId.Text)

            txtNroPrestamo.Text = hdnId.Text


            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)

                    usrCriaPresta.Valor = .Item("pstm_hace_cria_id")
                    usrCriaPrestata.Valor = .Item("pstm_reci_cria_id")
                    txtFechaDesde.Fecha = .Item("pstm_inic_fecha")
                    txtFechaHasta.Fecha = .Item("pstm_fina_fecha")
                    txtFechaPresentacion.Fecha = .Item("pstm_fecha_mov")
                    cmbTipo.Valor = .Item("pstm_tipo")
                    usrCriaPresta.cmbCriaRazaExt.Valor = .Item("pstm_raza_id")
                    usrCriaPrestata.cmbCriaRazaExt.Valor = .Item("pstm_raza_id")
                        txtObse.Valor = .Item("pstm_obse").ToString
                        hdnCriaId.Text = .Item("pstm_hace_cria_id")

                    If Left(.Item("_estado"), 1) = "P" Then
                        cmbTipoMovimiento.Valor = "P"
                    End If
                    If Left(.Item("_estado"), 1) = "D" Then
                        cmbTipoMovimiento.Valor = "X"
                    End If


                    If Not .IsNull("pstm_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & _
                        CDate(.Item("pstm_baja_fecha")).ToString("dd/MM/yyyy HH:mm")

                    Else
                        lblBaja.Text = ""
                    End If


                End With
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim dtProductos As DataTable
                Dim CriadorPrestamistaId As Integer = 0
                Dim CriadorPrestatarioId As Integer = 0
                Dim RazaId As Integer = 0
                Dim intPrestamoId As Integer = 0
                Dim ds As New DataSet


                If mdsDatos.Tables(0).Rows.Count > 0 Then
                    intPrestamoId = mdsDatos.Tables(0).Rows(0).Item("pstm_id")
                    CriadorPrestamistaId = mdsDatos.Tables(0).Rows(0).Item("pstm_hace_cria_id")
                    CriadorPrestatarioId = mdsDatos.Tables(0).Rows(0).Item("pstm_reci_cria_id")
                    If usrCriaPresta.cmbCriaRazaExt.Valor <> 0 Then
                        RazaId = usrCriaPresta.cmbCriaRazaExt.Valor
                    End If

                    If CriadorPrestamistaId.ToString() = "0" Or CriadorPrestamistaId.ToString() = "" Or _
                       RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                        Throw New AccesoBD.clsErrNeg("La Raza y el propietario Prestamista deben ser distinto de Vacio")

                        Return
                    End If
                    If CriadorPrestatarioId.ToString() = "0" Or CriadorPrestatarioId.ToString() = "" Or _
                      RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                        Throw New AccesoBD.clsErrNeg("La Raza y el propietario Prestatario deben ser distinto de Vacio")

                        Return
                    End If
                    ' si es modificacion solo se agregan solo 300 no  todo 
                    '   dtProductos = oProducto.GetProductosTop300ByClienteId(ClienteId, RazaId, "ProductosPropios")
                End If

                Dim ldrDeta As DataRow

                If hdnDetaId.Text = "" Then
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                    ldrDeta.Item("pstd_id") = _
                    clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "pstd_id")
                Else
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("pstd_id=" & hdnDetaId.Text)(0)
                End If
                Dim PosMax As Integer = 0

                For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                    If lDr.Item("_pos") > PosMax Then
                        PosMax = lDr.Item("_pos")

                    End If
                Next

                If Not dtProductos Is Nothing Then

                    For Each rw As DataRow In dtProductos.Rows
                        If Not ExistInDataset(mdsDatos, mdsDatos.Tables(mstrTablaDetalle), "pstd_prdt_id=" & rw.Item("prdt_id")) Then
                            PosMax = PosMax + 1
                            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                            ldrDeta.Item("pstd_id") = PosMax
                            ldrDeta.Item("pstd_pstm_id") = intPrestamoId
                            ldrDeta.Item("pstd_prdt_id") = rw.Item("prdt_id")
                            ldrDeta.Item("_producto") = rw.Item("prdt_sra_nume").ToString() & _
                            " | " & "RP " & rw.Item("prdt_RP").ToString() & _
                               " | " & rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_estado") = "Activo"
                            ldrDeta.Item("_pos") = PosMax
                            ldrDeta.Item("_hba") = ValidarNulos(rw.Item("prdt_sra_nume"), False).ToString()
                            ldrDeta.Item("_rp") = ValidarNulos(rw.Item("prdt_RP").ToString(), False).ToString()
                            ldrDeta.Item("_nombre") = rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_razaCodi") = rw.Item("_razaCodi").ToString()
                            ldrDeta.Item("_razaDesc") = rw.Item("_razaDesc").ToString()
                            ldrDeta.Item("_razaId") = rw.Item("_razaId").ToString()
                            ldrDeta.Item("_CHK") = rw.Item("_CHK").ToString()
                            ldrDeta.Item("_sexo") = rw.Item("_sexo").ToString()
                            ldrDeta.Item("_estadoProducto") = rw.Item("_estadoProducto").ToString()


                            If (hdnDetaId.Text = "") Then
                                mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
                            End If

                        End If

                    Next
                End If
                mdsDatos.AcceptChanges()


                With mdsDatos.Tables(mstrTablaDetalle)
                    .DefaultView.RowFilter = ""
                    .DefaultView.Sort = "_estado desc"
                    grdDetalle.DataSource = .DefaultView
                    ' the currentpageindex must be >= 0 and < pagecount<BR>   
                    Try

                        grdDetalle.DataBind()
                        grdDetalle.Visible = True

                    Catch
                        Try
                            grdDetalle.CurrentPageIndex = 0
                            grdDetalle.DataBind()
                            grdDetalle.Visible = True

                        Catch
                            ' Me.lblError.Text = "No data for selection"
                            ' Me.lblError.Visible = True

                        End Try

                    End Try
                End With

                mActualizarEtiq()

                mSetearEditor(False)

                mMostrarPanel(True)
                mShowTabs(1)
                If cmbTipo.Valor <> "0" Then
                    mShowTabs(2)
                End If


            End If

        Catch ex As Exception

            clsError.gManejarError(Me, ex)

        End Try
    End Sub
    Public Sub mEditarDatos(ByVal pNroPrestamo As String)
        Try


            mLimpiar()

            If Not boolAgregar Then
                grdDetalle.Enabled = True
                grdDetalle.Columns(0).Visible = True
            Else
                grdDetalle.Enabled = False
                grdDetalle.Columns(0).Visible = False
            End If

            hdnId.Text = pNroPrestamo

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    txtNroPrestamo.Text = pNroPrestamo
                    usrCriaPresta.Valor = .Item("pstm_hace_cria_id")
                    usrCriaPrestata.Valor = .Item("pstm_reci_cria_id")
                        txtFechaDesde.Fecha = IIf(.Item("pstm_inic_fecha") Is DBNull.Value, "", .Item("pstm_inic_fecha"))
                        txtFechaHasta.Fecha = IIf(.Item("pstm_fina_fecha") Is DBNull.Value, "", .Item("pstm_fina_fecha"))
                        txtFechaPresentacion.Fecha = .Item("pstm_fecha_mov")
                    cmbTipo.Valor = .Item("pstm_tipo")
                        txtObse.Valor = IIf(.Item("pstm_obse") Is DBNull.Value, "", .Item("pstm_obse"))
                        hdnCriaId.Text = .Item("pstm_hace_cria_id")


                    cmbTipoMovimiento.Valor = Left(.Item("_estado"), 1)

                    If Not .IsNull("pstm_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & _
                        CDate(.Item("pstm_baja_fecha")).ToString("dd/MM/yyyy HH:mm")

                    Else
                        lblBaja.Text = ""
                    End If


                End With
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim dtProductos As DataTable
                Dim CriadorId As Integer = 0
                Dim ClienteId As Integer = 0
                Dim RazaId As Integer = 0
                Dim intPrestamoId As Integer = 0
                Dim ds As New DataSet
                Dim CriadorPrestamistaId As Integer = 0
                Dim CriadorPrestatarioId As Integer = 0

                Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
                If mdsDatos.Tables(0).Rows.Count > 0 Then


                    intPrestamoId = mdsDatos.Tables(0).Rows(0).Item("pstm_id")
                    CriadorPrestamistaId = mdsDatos.Tables(0).Rows(0).Item("pstm_hace_cria_id")
                    CriadorPrestatarioId = mdsDatos.Tables(0).Rows(0).Item("pstm_reci_cria_id")
                        'If usrCriaPresta.RazaId <> 0 Then
                        If usrCriaPresta.RazaId.Trim().Length > 0 Then
                            RazaId = usrCria.cmbCriaRazaExt.Valor
                        End If

                        If CriadorPrestamistaId.ToString() = "0" Or CriadorPrestamistaId.ToString() = "" Or _
                      RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                        Throw New AccesoBD.clsErrNeg("La Raza y el propietario Prestamista deben ser distinto de Vacio")

                        Return
                    End If
                    If CriadorPrestatarioId.ToString() = "0" Or CriadorPrestatarioId.ToString() = "" Or _
                      RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                        Throw New AccesoBD.clsErrNeg("La Raza y el propietario Prestatario deben ser distinto de Vacio")

                        Return
                    End If


                    ClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaPresta.RazaId, usrCriaPresta.Valor)
                    ' si es modificacion solo se agregan solo 300 no  todo 
                    ' dtProductos = oProducto.GetProductosTop300ByClienteId(ClienteId, RazaId, "ProductosPropios")
                End If



                Dim ldrDeta As DataRow

                If hdnDetaId.Text = "" Then
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                    ldrDeta.Item("pstd_id") = _
                    clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "pstd_id")
                Else
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("pstd_id=" & hdnDetaId.Text)(0)
                End If
                Dim PosMax As Integer = 0

                For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                    If lDr.Item("_pos") > PosMax Then
                        PosMax = lDr.Item("_pos")

                    End If
                Next

                If Not dtProductos Is Nothing Then

                    For Each rw As DataRow In dtProductos.Rows
                        If Not ExistInDataset(mdsDatos, mdsDatos.Tables(mstrTablaDetalle), "pstd_prdt_id=" & rw.Item("prdt_id")) Then
                            PosMax = PosMax + 1
                            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                            ldrDeta.Item("pstd_pstm_id") = intPrestamoId
                            ldrDeta.Item("pstd_prdt_id") = rw.Item("prdt_id")
                            ldrDeta.Item("_producto") = rw.Item("prdt_sra_nume").ToString() & _
                            " | " & "RP " & rw.Item("prdt_RP").ToString() & _
                               " | " & rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_estado") = "Activo"
                            ldrDeta.Item("_pos") = PosMax
                            ldrDeta.Item("_hba") = ValidarNulos(rw.Item("prdt_sra_nume"), False).ToString()
                            ldrDeta.Item("_rp") = ValidarNulos(rw.Item("prdt_RP").ToString(), False).ToString()
                            ldrDeta.Item("_nombre") = rw.Item("prdt_nomb").ToString()
                            ldrDeta.Item("_razaCodi") = rw.Item("_razaCodi").ToString()
                            ldrDeta.Item("_razaDesc") = rw.Item("_razaDesc").ToString()
                            ldrDeta.Item("_razaId") = rw.Item("_razaId").ToString()
                            ldrDeta.Item("_CHK") = rw.Item("_CHK").ToString()
                            ldrDeta.Item("_sexo") = rw.Item("_sexo").ToString()
                            ldrDeta.Item("_estadoProducto") = rw.Item("_estadoProducto").ToString()


                            If (hdnDetaId.Text = "") Then
                                mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
                            End If

                        End If

                    Next
                End If
                mdsDatos.AcceptChanges()


                With mdsDatos.Tables(mstrTablaDetalle)
                    .DefaultView.RowFilter = ""
                    .DefaultView.Sort = "_estado desc"
                    grdDetalle.DataSource = .DefaultView
                    ' the currentpageindex must be >= 0 and < pagecount<BR>   
                    Try

                        grdDetalle.DataBind()
                        grdDetalle.Visible = True

                    Catch
                        Try
                            grdDetalle.CurrentPageIndex = 0
                            grdDetalle.DataBind()
                            grdDetalle.Visible = True

                        Catch
                            ' Me.lblError.Text = "No data for selection"
                            ' Me.lblError.Visible = True

                        End Try

                    End Try


                End With


                mActualizarEtiq()

                mSetearEditor(False)
                mSetearEditorDeta(False)
                mMostrarPanel(True)
                mShowTabs(1)
                If cmbTipo.Valor <> "0" Then
                    mShowTabs(2)
                End If


            End If

        Catch ex As Exception

            clsError.gManejarError(Me, ex)

        End Try
    End Sub

    Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim intCriaId As Int32
            Dim intRazaId As Int32
            Dim ldrDeta As DataRow
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim dtRaza As DataTable


            hdnDetaId.Text = E.Item.Cells(1).Text


            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("pstd_id=" & hdnDetaId.Text)(0)


            With ldrDeta

                intRazaId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("_razaId"), False)
                intCriaId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("pstm_hace_cria_id"), False)
                'If intRazaId = 0 And intCriaId = 0 Then
                '    intRazaId = usrProdFilDeta.RazaId
                '    intCriaId = usrProdFilDeta.CriaOrPropId
                'End If



                'usrProdFilDeta.Valor = .Item("pstd_prdt_id")
                'usrProdFilDeta.RazaId = intRazaId.ToString()
                'usrProdFilDeta.CriaOrPropId = intCriaId

                If Not .IsNull("pstd_baja_fecha") Then
                    lblBajaDeta.Text = "Producto dado de baja en fecha: " & CDate(.Item("pstd_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaDeta.Text = ""
                End If
            End With
            mSetearEditorDeta(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDetalle

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
        grdDetalle.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mAgregar()
        boolAgregar = True
        lblAlta.Text = "A"
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
        cmbTipoMovimiento.Valor = "P"
        cmbTipoMovimiento.Enabled = False

    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnCriaCodi.Text = ""
        cmbTipo.Limpiar()
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""
        txtNroPrestamo.Text = ""



        cmbTipoReporte.Limpiar()
        hdnCriaId.Text = ""

        txtObse.Text = ""
        lblBaja.Text = ""

        usrCriaPresta.Limpiar()
        usrCriaPrestata.Limpiar()

        grdDato.CurrentPageIndex = 0
        grdDetalle.CurrentPageIndex = 0
        cmbEsta.Enabled = False
        cmbTipoMovimiento.Enabled = False


        mstrTipo = Request.QueryString("Tipo")

        If mstrTipo <> "" Then
            If mstrTipo = 0 Then
                cmbTipo.Valor = 0
            ElseIf mstrTipo = 1 Then
                cmbTipo.Valor = 1

            ElseIf mstrTipo = 2 Then
                cmbTipo.Valor = 2
            End If
            cmbTipo.Enabled = False

            cmbTipo.Valor = cmbTipoFil.Valor
            cmbTipo.Enabled = False
        End If
        mLimpiarDeta()

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarDeta()
        hdnDetaId.Text = ""
        'usrProdFilDeta.Valor = ""
        'usrProdFilDeta.CriaOrPropId = hdnCriaId.Text


        lblBajaDeta.Text = ""
        mSetearEditorDeta(True)
    End Sub
    Private Sub mLimpiarFil()
        grdDato.CurrentPageIndex = 0
        txtNroPrestamoFil.Text = ""

        usrCriaPrestaFil.Limpiar()
        usrCriaPrestataFil.Limpiar()
        usrProdFil.Limpiar()
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        usrProdFil.cmbSexoProdExt.Valor = 0
        usrProdFil.Limpiar()
        cmbEstadoFil.Limpiar()
        cmbTipoFil.Limpiar()
        cmbTipoReporte.Limpiar()


        hdnValidar.Text = "N"
        mConsultar()
        hdnValidar.Text = "S"
    End Sub

    Private Sub mCerrar()

        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        btnAgre.Visible = panFiltro.Visible
        btnList.Visible = panFiltro.Visible

        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)

    End Sub


#End Region

#Region "Opciones de ABM"
    Private Sub mLimpiarActivos()

        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Activo'")

            lDr.Delete()

        Next
        mdsDatos.AcceptChanges()
    End Sub
    Private Sub mConsultar(ByVal boolMostrarTodos As Boolean)
        Try

            Dim dsDatos As New DataSet

            If (Page.IsPostBack) And hdnValidar.Text <> "N" Then
                If (usrProdFil.cmbProdRazaExt Is Nothing And usrProdFil.CriaOrPropId = "0" _
                              And usrProdFil.CriaOrPropId = "0" And boolTodosVacios()) Then
                    Throw New AccesoBD.clsErrNeg("Debe especificar al menos una Raza para Consultar.")

                End If
            End If


            mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + " @pstm_hace_cria_id=" + _
            IIf(usrProdFil.CriaOrPropId = "", "0", usrProdFil.CriaOrPropId)

            mstrCmd = mstrCmd + ", @pstm_reci_cria_id=" + _
                        IIf(usrCriaPrestataFil.Valor = 0, "0", usrCriaPrestataFil.Valor)


            mstrCmd = mstrCmd + ", @fecha_desde = " + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
            mstrCmd = mstrCmd + ", @fecha_hasta = " + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)

            If usrProdFil.cmbSexoProdExt.Valor.ToString <> "" Then
                mstrCmd = mstrCmd + ", @sexo = " + usrProdFil.cmbSexoProdExt.Valor.ToString()
            End If

            If usrProdFil.txtProdNombExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @NombreProd = '" + usrProdFil.txtProdNombExt.Text + "'"
            End If
            If usrProdFil.txtRPExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @RP = '" + usrProdFil.txtRPExt.Text + "'"
            End If


            If usrProdFil.txtSraNumeExt.Valor.ToString <> "" Then
                mstrCmd = mstrCmd + ", @sra_nume = " + usrProdFil.txtSraNumeExt.Valor.ToString
            End If


            If usrProdFil.RazaId <> "" Then
                mstrCmd = mstrCmd + ", @raza_id = " + usrProdFil.RazaId

            Else
                If Session("sRazaId") Is Nothing Then
                    mstrCmd = mstrCmd + ", @raza_id = 0"
                Else
                    mstrCmd = mstrCmd + ", @raza_id = " + Session("sRazaId")
                End If
            End If

            'If cmbTipoMovimientoFil.SelectedIndex <> 0 Then
            '    mstrCmd = mstrCmd + ",@tipo_movimiento=" + cmbTipoMovimientoFil.Valor.ToString()
            'End If

            If cmbTipoFil.SelectedIndex <> 0 Then
                mstrCmd = mstrCmd + ",@pstm_tipo=" + cmbTipoFil.Valor.ToString()
            End If
            If txtNroPrestamoFil.Text <> "" Then
                mstrCmd = mstrCmd + ",@pstm_id=" + txtNroPrestamoFil.Text
            End If



            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)
            grdDato.Visible = True
            grdDato.DataSource = dsDatos

            grdDato.DataBind()


            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mAlta()
        Dim lTransac As SqlClient.SqlTransaction
        Try
            mLimpiarActivos()
            lblAlta.Text = "A"
            mValidaCantProdUnico()
            mGuardarDatos()

            Dim lobjPrestamos As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString(), "prestamos", mdsDatos)
            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim lstrMsgError As String
            Dim booSexo As Boolean



            lblAltaId.Text = lobjPrestamos.mActualPrestamos("A")
            booSexo = oProducto.GetSexoByProductoId(hdnId.Text)

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)


            If (lblAltaId.Text <> "-1") Then
                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_PrestamosDeta, lblAltaId.Text)

                'Aplicar Reglas.
                lstrMsgError = gValidarReglas(lTransac, SRA_Neg.Constantes.gTab_Prestamos, lblAltaId.Text, _
                                Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Prestamos.ToString(), mdsDatos)


            End If

            If lobjPrestamos.GetPrestamoErrorByNroPrestamo(lblAltaId.Text) Then
                mdsDatos.Tables(0).Rows(0).Item("pstm_esta_id") = SRA_Neg.Constantes.Estados.Prestamos_Retenido
                cmbTipoMovimiento.SelectedValue = "R"
                clsSQLServer.gModi(lTransac, Session("sUserId").ToString(), "prestamos", mdsDatos.Tables(0).Rows(0))

            End If

            clsSQLServer.gCommitTransac(lTransac)

            If lstrMsgError <> "" Then
                AccesoBD.clsError.gGenerarMensajes(Me, lstrMsgError)
            End If


            mLimpiar()
            mConsultar(False)

            mMostrarPanel(False)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(Me, ex)

        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim intCriaId As Int32
            Dim ldrDeta As DataRow
            Dim lstrMsgError As String
            Dim lTransac As SqlClient.SqlTransaction
            Dim booSexo As Boolean
            Dim intPrestamoId As Int32


            mValidarFechaFinalizacion()


            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            mLimpiarActivos()
            If hdnDetaId.Text <> "" Then
                ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("pstd_id=" & hdnDetaId.Text)(0)
                With ldrDeta
                    .Item("_pstd_fina_fecha") = txtFechaFinalProducto.Text
                End With
            End If
            mValidarDatosDeta()
            Dim lobjPrestamos As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString(), "prestamos", mdsDatos)


            intPrestamoId = mdsDatos.Tables(0).Rows(0).Item("pstm_id")
            'ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_Prestamos, intPrestamoId)

            lobjGenerica.Modi()

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)


            If (intPrestamoId <> "-1") Then
                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_Prestamos, intPrestamoId)

                'Aplicar Reglas.

                'public static string gValidarReglas(SqlTransaction lTransac,  string pstrTabla,
                '			string pstrCabeceraID, string pstrRaza,  string pstrUsua, bool pbooGrabarError, 
                '			string pProceID,  DataSet dsDatos)

                lstrMsgError = gValidarReglas(lTransac, SRA_Neg.Constantes.gTab_Prestamos, intPrestamoId, _
                                              Session("sUserId").ToString(), True, _
                                              ReglasValida.Validaciones.Procesos.Prestamos.ToString(), mdsDatos)


            End If

            If lobjPrestamos.GetPrestamoErrorByNroPrestamo(intPrestamoId) Then
                mdsDatos.Tables(0).Rows(0).Item("pstm_esta_id") = SRA_Neg.Constantes.Estados.Prestamos_Retenido
                cmbTipoMovimiento.SelectedValue = "R"
                clsSQLServer.gModi(lTransac, Session("sUserId").ToString(), "prestamos", mdsDatos.Tables(0).Rows(0))

            End If

            clsSQLServer.gCommitTransac(lTransac)



            boolAgregar = False

            mConsultar(False)

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarFechaFinalizacion()
        If GetPrestamo() = 0 And (GetDevuelto() = GetTotal()) Then
            If txtFechaHasta.Text Is System.DBNull.Value Then

                Throw New AccesoBD.clsErrNeg("La Fecha de Finalizacion  del Prestamo ." + _
                       " no puede estar nula dado que el/los productos Prestados ya Fueron Devueltos.")

            End If
        End If

        If GetPrestamo() <> 0 Or (GetDevuelto() <> GetTotal()) Then
                If txtFechaHasta.Text.Trim().Length > 0 Then ' <> "" Then

                    Throw New AccesoBD.clsErrNeg("La Fecha de Finalizacion  del Prestamo ." +
                       " no puede estar Cargada dado que aun hay Productos Prestados")

                End If
            End If
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex
            Dim oPrestamos As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString())
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)


            '   lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))


            oPrestamos.GrabarBajaEnPrestamo(hdnId.Text)
            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If (txtFechaDesde.Fecha Is DBNull.Value) Then

            Throw New AccesoBD.clsErrNeg("Debe especificar la fecha de Inicio  del Prestamo.")

        End If

        If Not (txtFechaDesde.Fecha Is DBNull.Value) And Not (txtFechaHasta.Fecha Is DBNull.Value) Then
            If txtFechaDesde.Fecha > txtFechaHasta.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
            End If
        End If


        'prestamo de un solo producto o varios
        Dim oPrestamo As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString())
        Dim intCantProdPorPrestamo As Integer


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Prestado'")

            intCantProdPorPrestamo = intCantProdPorPrestamo + 1

        Next

        If cmbTipo.Valor = 1 And intCantProdPorPrestamo <> 1 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar un solo producto. Son " & intCantProdPorPrestamo & " la Cantidad de Productos incluidos en este Prestamo. " + Chr(13) + _
            ". De lo contrario cambie el tipo de prestamo")
        End If

        If (cmbTipo.Valor = 0 Or cmbTipo.Valor = 2) And intCantProdPorPrestamo < 1 Then
            Throw New AccesoBD.clsErrNeg("No seleccion� ningun producto. Son " & intCantProdPorPrestamo & " la Cantidad de Productos incluidos en este Prestamo. " + Chr(13) + _
            ". De lo contrario cambie el tipo de prestamo")
        End If


        If (cmbTipo.Valor = 0 Or cmbTipo.Valor = 2) And intCantProdPorPrestamo = 1 Then
            Throw New AccesoBD.clsErrNeg("Hay un solo producto. Son " & intCantProdPorPrestamo & " la Cantidad de Productos incluidos en este Prestamo. " + Chr(13) + _
            ". De lo contrario cambie el tipo de prestamo")
        End If



        If cmbTipo.Valor = mProdConjun Then
            mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = "pstd_baja_fecha is null"
            If mdsDatos.Tables(mstrTablaDetalle).DefaultView.Count < 2 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar mas de un producto. De lo contrario cambie el tipo de prestamo.")
            End If
        End If

        mValidaCantProdUnico()
    End Sub
        Private Sub mValidaDatos(ByVal boolAlta)
            Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())

            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)
            If Not IsDBNull(txtFechaDesde.Fecha) And IsDate(txtFechaDesde.Fecha) And Not IsDBNull(txtFechaHasta.Fecha) And IsDate(txtFechaHasta.Fecha) Then
                Dim feDesde As DateTime
                Dim feHasta As DateTime

                feDesde = Convert.ToDateTime(txtFechaDesde.Fecha)
                feHasta = Convert.ToDateTime(txtFechaHasta.Fecha)

                'If txtFechaDesde.Fecha > txtFechaHasta.Fecha Then
                If feDesde > feHasta Then
                    Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
                End If
            End If

            'prestamo de un solo producto o varios
            If boolAlta Then

                mValidaGrupoHBA()
                mValidaCantProdUnico()


            End If


            If usrCriaPresta.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Prestamista.")
            End If
            If usrCriaPrestata.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Prestatario.")
            End If



        End Sub
        Private Sub mGuardarDatos()

            Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
            mValidaDatos(True)
            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim intProductoId As Integer
            Dim dtProducto As DataTable
            Dim intEstadoIdAnterior As Integer
            Dim intHBA As Integer
            Dim intRazaId As Integer
            Dim Sexo As String

            If (txtFechaDesde.Fecha Is DBNull.Value) Then

                Throw New AccesoBD.clsErrNeg("Debe especificar la fecha de Inicio del Prestamo.")

            End If

            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

            With mdsDatos.Tables(0).Rows(0)
                .Item("pstm_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("pstm_hace_cria_id") = usrCriaPresta.Valor
                .Item("pstm_hace_clie_id") = oCliente.GetClienteIdByRazaCriador(usrCriaPresta.cmbCriaRazaExt.Valor, usrCriaPresta.Valor)
                .Item("pstm_inic_fecha") = txtFechaDesde.Fecha
                .Item("pstm_raza_id") = usrCriaPresta.cmbCriaRazaExt.Valor
                .Item("pstm_fina_fecha") = IIf(txtFechaHasta.Text.Trim().Length = 0, DBNull.Value, txtFechaHasta.Fecha)
                .Item("pstm_tipo") = cmbTipo.Valor
                .Item("pstm_reci_clie_id") = oCliente.GetClienteIdByRazaCriador(usrCriaPrestata.cmbCriaRazaExt.Valor, usrCriaPrestata.Valor)
                .Item("pstm_reci_cria_id") = usrCriaPrestata.Valor
                .Item("pstm_esta_id") = cmbEsta.Valor
                .Item("pstm_obse") = txtObse.Valor
                .Item("pstm_fecha_mov") = txtFechaPresentacion.Fecha
                If EstaDevuelto() Then

                    If GetPrestamo() = 0 And (GetDevuelto() = GetTotal()) Then
                        .Item("pstm_fina_fecha") = Date.Now.ToShortDateString()
                        cmbTipoMovimiento.Valor = "D"
                    End If

                End If

            End With
            mdsDatos.AcceptChanges()

            lblAlta.Text = ""

        End Sub

        Private Sub mVerificarProducto()
            'Try
            '    'verificar que el producto  seleccionado pueda ser prestado.
            '    Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

            '    Dim lstrfiltro As String
            '    If usrProdFilDeta.Valor <> 0 Then   'And Not txtFechaDesde.Fecha Is DBNull.Value Then
            '        lstrfiltro = " @clie_id=" + clsSQLServer.gFormatArg(oCliente.GetClienteIdByRazaCriador(usrCriaPresta.cmbCriaRazaExt.Valor, usrCriaPresta.Valor), SqlDbType.Int)
            '        lstrfiltro = lstrfiltro + ",@cria_id=" + clsSQLServer.gFormatArg(usrCriaPresta.Valor, SqlDbType.Int)
            '        lstrfiltro = lstrfiltro + ",@prdt_id=" + clsSQLServer.gFormatArg(usrProdFilDeta.Valor, SqlDbType.Int)
            '        lstrfiltro = lstrfiltro + ",@tipo='P'"


            '    End If
            'Catch ex As Exception
            '    clsError.gManejarError(Me, ex)
            'End Try
        End Sub
        'Seccion de detalle
        Private Sub mActualizarDeta()
        Try

            mVerificarProducto()
            mGuardarDatosDeta()

            mLimpiarDeta()
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaDeta()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("pstd_id=" & hdnDetaId.Text)(0)
            row.Delete()
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()
            mLimpiarDeta()
            If (cmbTipo.Valor = 0) Then
                cmbTipo.Valor = 2
                Throw New AccesoBD.clsErrNeg("El prestamo se cambio automaticamente  a Grupo de HBA.")
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosDeta()

        Dim lstrFiltro As String



        If hdnDetaId.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seelceccionar un producto para modificarle la Fecha de Finalizaci�n.")
        End If

        If txtFechaFinalProducto.Text = "" Then
            txtFechaFinalProducto.Text = DateTime.Now.ToShortDateString()

        End If

        Dim lDr As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("pstd_id=" & hdnDetaId.Text)(0)


        If txtFechaFinalProducto.Text <> "" Then
            lDr.Item("_estado") = "Devuelto"
            lDr.Item("pstd_fina_fecha") = txtFechaFinalProducto.Text
        Else

            lDr.Item("_estado") = "Prestado"
            lDr.Item("pstd_fina_fecha") = System.DBNull.Value
        End If


        lstrFiltro = " @cria_id=" + clsSQLServer.gFormatArg(usrCriaPresta.Valor, SqlDbType.Int)
        lstrFiltro = lstrFiltro + ",@prdt_id=" + clsSQLServer.gFormatArg(lDr.Item("pstd_prdt_id"), SqlDbType.Int)
        lstrFiltro = lstrFiltro + ",@fecha_ini=" + clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime)
    
    End Sub


    Private Sub mValidarDatosDeta()

        mValidaGrupoHBA()
        mValidaCantProdUnico()

    End Sub
    Private Sub mValidaGrupoHBA()
        Dim intCant As Integer = 0

        If txtFechaDesde.Text = "" Then

            Throw New AccesoBD.clsErrNeg("La Fecha de Inicio  del Prestamo ." + _
                                   " no puede estar vacio.")

        End If

        If cmbTipo.Valor = mProdConjun Then

            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Prestado'")

                intCant = intCant + 1

            Next

            If hdnId.Text <> "" Then
                If GetPrestamo() = 0 And (GetDevuelto() = GetTotal()) Then

                    '  mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_baja_fecha") = _
                    '                 Now.ToString("dd/MM/yyyy HH:mm")


                    '  lblBaja.Text = "Registro dado de baja en fecha: " & _
                    'mdsDatos.Tables(mstrTabla).Rows(0).Item("emba_baja_fecha") = _
                    'Now.ToString("dd/MM/yyyy HH:mm")

                    mdsDatos.Tables(mstrTabla).Rows(0).Item("pstm_fina_fecha") = _
                    Now.ToString("dd/MM/yyyy HH:mm")



                End If

            Else
                If intCant < 2 Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar mas de 1 producto. De lo contrario cambie el tipo de Prestamo.")
                End If
            End If
        End If
        mdsDatos.AcceptChanges()



    End Sub


    Private Sub mValidaCantProdUnico()

        If cmbTipo.Valor = mProdUno Then
            mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = "pstd_baja_fecha is null"
            If mdsDatos.Tables(mstrTablaDetalle).DefaultView.Count <> 1 Then
                Throw New AccesoBD.clsErrNeg("Solo puede ingresar un producto. De lo contrario cambie el tipo de prestamo.")
            End If
        End If


    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panDato.Visible = True
            panBotones.Visible = True
            btnAgre.Visible = False

            panFiltro.Visible = False
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panDetalle.Visible = False
            lnkDetalle.Font.Bold = False
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    panDetalle.Visible = True
                    lnkDetalle.Font.Bold = True
                    'usrProdFilDeta.RazaId = usrCriaPresta.cmbCriaRazaExt.Valor
                    'usrProdFilDeta.CriaOrPropId = hdnCriaId.Text
                    If btnAlta.Enabled = True Then
                        mConsultarDeta()
                    End If

            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mConsultarDeta()

        Try



            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim dtProductos As DataTable
            Dim CriadorId As Integer = 0
            Dim RazaId As Integer = 0
            Dim intPrestamoId As Integer = 0
            Dim ds As New DataSet





            If mdsDatos.Tables(0).Rows.Count > 0 Then
                CriadorId = usrCriaPresta.Valor.ToString()
                If usrCriaPresta.RazaId <> "" Then
                    RazaId = usrCriaPresta.RazaId
                End If

                If CriadorId.ToString() = "0" Or CriadorId.ToString() = "" Or _
                                   RazaId.ToString() = "0" Or RazaId.ToString() = "" Then
                    Throw New AccesoBD.clsErrNeg("La Raza y el propietario deben ser distinto de Vacio")

                    Return
                End If

                dtProductos = oProducto.GetProductosByCriadorId(CriadorId, RazaId, "ProductosPropios")
            End If



            Dim ldrDeta As DataRow

            If hdnDetaId.Text = "" Then
                ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                ldrDeta.Item("pstd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "pstd_id")
            Else
                ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("pstd_id=" & hdnDetaId.Text)(0)
            End If
            Dim PosMax As Integer = 0

            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                If lDr.Item("_pos") > PosMax Then
                    PosMax = lDr.Item("_pos")

                End If
            Next
            intPrestamoId = -1



            For Each rw As DataRow In dtProductos.Rows
                If Not ExistInDataset(mdsDatos, mdsDatos.Tables(mstrTablaDetalle), _
                "pstd_prdt_id=" & rw.Item("prdt_id")) Then
                    PosMax = PosMax + 1
                    ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
                    ldrDeta.Item("pstd_id") = PosMax
                    ldrDeta.Item("pstd_pstm_id") = intPrestamoId
                    ldrDeta.Item("pstd_Fina_fecha") = rw.Item("_pstd_Fina_fecha")
                    ldrDeta.Item("pstd_prdt_id") = rw.Item("prdt_id")
                    ldrDeta.Item("_producto") = rw.Item("prdt_sra_nume").ToString() & _
                    " | " & "RP " & rw.Item("prdt_RP").ToString() & _
                       " | " & rw.Item("prdt_nomb").ToString()
                    If cmbTipo.Valor = mProdTodos Then
                        ldrDeta.Item("_estado") = "Prestado"
                        ' Si es nuevo y presta todos  la primera vez siempre es prestado
                    Else
                        ldrDeta.Item("_estado") = "Activo"
                        ' Si es nuevo y presta POR HBA o Por Lote son todos activos dado que hay que mostrarlos para seleccionar
                        ' ' Significa que hasta que no este seleccionado no esta ni prestado  , no devuelto
                    End If

                    ldrDeta.Item("_pos") = PosMax
                    ldrDeta.Item("_hba") = ValidarNulos(rw.Item("prdt_sra_nume"), False).ToString()
                    ldrDeta.Item("_rp") = rw.Item("prdt_RP").ToString()
                    ldrDeta.Item("_nombre") = rw.Item("prdt_nomb").ToString()
                    ldrDeta.Item("_razaCodi") = rw.Item("_razaCodi").ToString()
                    ldrDeta.Item("_razaDesc") = rw.Item("_razaDesc").ToString()
                    ldrDeta.Item("_razaId") = rw.Item("_razaId").ToString()
                    ldrDeta.Item("_CHK") = rw.Item("_CHK").ToString()
                    ldrDeta.Item("_sexo") = rw.Item("_sexo").ToString()
                    ldrDeta.Item("_estadoProducto") = rw.Item("_estadoProducto").ToString()

                    If (hdnDetaId.Text = "") Then
                        mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
                    End If

                End If

            Next

            If btnAlta.Enabled Then
                If cmbTipo.Valor = mProdTodos Then
                    mActualizarCHK(True)
                Else
                    '   no hago nada dado que deben quedar en estado activo Hasta que se toque en la grilla mActualizarCHK(False)
                End If
            End If


            mdsDatos.AcceptChanges()

            With mdsDatos.Tables(mstrTablaDetalle)
                .DefaultView.RowFilter = ""
                .DefaultView.Sort = "_estado desc"
                grdDetalle.DataSource = .DefaultView

                ' the currentpageindex must be >= 0 and < pagecount<BR>   
                Try

                    grdDetalle.DataBind()
                    grdDetalle.Visible = True

                Catch
                    Try
                        grdDetalle.CurrentPageIndex = 0
                        grdDetalle.DataBind()
                        grdDetalle.Visible = True

                    Catch
                        ' Me.lblError.Text = "No data for selection"
                        ' Me.lblError.Visible = True

                    End Try

                End Try




            End With
            mActualizarEtiq()
            mdsDatos.AcceptChanges()

            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()


        Catch ex As Exception

            clsError.gManejarError(Me, ex)

        End Try

    End Sub
    Public Function ExistInDataset(ByVal pDsExit As DataSet, ByVal pTabla As DataTable, ByVal Filtro As String) As Boolean



        For Each lDrExist As DataRow In pDsExit.Tables(pTabla.ToString).Select(Filtro)
            If lDrExist.Item("pstd_prdt_id").ToString() = "" Then
                Return False
            End If
            If CInt(lDrExist.Item("pstd_prdt_id")) <= 0 Then
                Return False
            Else
                Return True
            End If

        Next



    End Function
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mstrTramiteId = Request.QueryString("Tram_Id")
        If mstrTramiteId <> "" Then
            Response.Write("<Script>window.close();</script>")
        End If

        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()

    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    'Botones Generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones Detalle
    Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click


        mActualizarDeta()



    End Sub
    Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        mBajaDeta()
    End Sub
    Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        Try
            mGuardarDatosDeta()

            mLimpiarDeta()

            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)

        End Try

    End Sub
    Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        mLimpiarDeta()
    End Sub
    'Solapas
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
        mShowTabs(2)

    End Sub

    Private Sub cmbTipo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTipo.SelectedIndexChanged
        mDetalle()
        If cmbTipo.Valor = mProdTodos Then
            mCrearDataSet("")
        End If
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintFechaD As Integer
            Dim lintFechaH As Integer

            If cmbTipoReporte.SelectedValue = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el Tipo de Reportes.")
            End If

            If cmbTipoReporte.SelectedValue = "R" Then
                lstrRptName = "PrestamosResumido"
            Else
                lstrRptName = "PrestamosCompleto"
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            lstrRpt += "&pstm_hace_cria_id=" + IIf(usrCriaPrestaFil.Valor.ToString = "", "0", usrCriaPrestaFil.Valor.ToString)
            lstrRpt += "&pstm_reci_cria_id=" + IIf(usrCriaPrestataFil.Valor.ToString = "", "0", usrCriaPrestataFil.Valor.ToString)


            If txtNroPrestamo.Text <> "" Then
                lstrRpt += "&pstm_id=" + txtNroPrestamo.Text
            Else
                lstrRpt += "&pstm_id=0"
            End If
            lstrRpt += "&pstm_hace_clie_id=0"

            lstrRpt += "&pstm_reci_clie_id=0"


            If cmbTipoFil.SelectedIndex <> 0 Then
                lstrRpt += "&pstm_tipo=" + cmbTipoFil.Valor.ToString()

            End If

            If usrProdFil.txtSraNumeExt.Valor.ToString <> "" Then
                lstrRpt += "&sra_nume=" + usrProdFil.txtSraNumeExt.Valor.ToString()
            Else
                lstrRpt += "&sra_nume=0"

            End If

            If usrProdFil.cmbSexoProdExt.Valor.ToString <> "" Then
                lstrRpt += "&sexo=" + usrProdFil.cmbSexoProdExt.Valor.ToString()
            End If
            If usrProdFil.RazaId <> "" Then
                lstrRpt += "&raza_id=" + usrProdFil.RazaId
            Else
                '     lstrRpt += "&raza_id=0"

            End If


            '"Estado del Prestamo:" & Switch(Parameters!tipo_movimiento.Value = "E", "Ingresado Activo", 
            'tipo_movimiento.Value = "X", "Ingresado Finalizado", 
            'tipo_movimiento.Value = "N", "Anulado"


            If cmbEstadoFil.Valor.ToString <> "0" Then
                If cmbEstadoFil.Valor.ToString = SRA_Neg.Constantes.Estados.Prestamos_Ingresado Or _
                 cmbEstadoFil.Valor.ToString = SRA_Neg.Constantes.Estados.Prestamos_Retenido Or _
                 cmbEstadoFil.Valor.ToString = SRA_Neg.Constantes.Estados.Prestamos_Anulado Then
                    lstrRpt += "&pstm_esta_id=" + cmbEstadoFil.Valor.ToString

                End If


                Dim valor As Int16

                valor = SRA_Neg.Constantes.Estados.Prestamos_Ingresado


                If cmbEstadoFil.Valor.ToString = "11" Then ' estado 11 es ficticio ya que se crea solo para la consulta
                    lstrRpt += "&pstm_esta_id=" + valor.ToString

                End If


                lstrRpt += "&EstadoActual=" + cmbEstadoFil.SelectedItem.Text.Trim()
            End If

            If txtFechaDesdeFil.Text = "" Then
                lintFechaD = 0
            Else
                lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaHastaFil.Text = "" Then
                lintFechaH = 0
            Else
                lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If


            lstrRpt += "&fecha_desde=" + lintFechaD.ToString()
            lstrRpt += "&fecha_hasta=" + lintFechaH.ToString()

            If usrProdFil.txtProdNombExt.Text <> "" Then
                lstrRpt += "&NombreProd=" + usrProdFil.txtProdNombExt.Text
            End If
            If usrProdFil.txtRPExt.Text <> "" Then
                lstrRpt += "&RP=" + usrProdFil.txtRPExt.Text
            Else

                lstrRpt += "&RP=0"
            End If


            If usrProdFil.RazaId <> "" Then
                Dim objRazas As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
                lstrRpt += "&Raza_codi=" + objRazas.GetRazaCodiById(usrProdFil.RazaId).ToString()

            Else
                lstrRpt += "&Raza_codi=0"

            End If
            If usrCriaPrestaFil.Valor.ToString <> "0" Then
                Dim objCriadores As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
                lstrRpt += "&Cria_codi=" + objCriadores.GetCriaNumeByCriadorId(usrCriaPrestaFil.Valor.ToString).ToString() _
                + "-" + usrCriaPrestaFil.txtApelExt.Text.Trim()

            Else
                '  lstrRpt += "&Cria_codi=0"

            End If


            If usrCriaPrestataFil.Valor.ToString <> "0" Then
                Dim objCriadores As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
                lstrRpt += "&Cria_codi_prestatario=" + objCriadores.GetCriaNumeByCriadorId(usrCriaPrestataFil.Valor.ToString).ToString() _
                + usrCriaPrestataFil.txtApelExt.Text.Trim

            Else
                '  lstrRpt += "&Cria_codi_prestatario=0"

            End If


            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)


            Response.Redirect(lstrRpt)


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mDetalle()

        If cmbTipo.Valor = mProdTodos Then
            btnSeleccionarTodos.Enabled = False
            btnDeSeleccionarTodos.Enabled = False


        Else
            btnSeleccionarTodos.Enabled = True
            btnDeSeleccionarTodos.Enabled = True

        End If

    End Sub
    Protected Sub mActualizarGrilla(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

        Dim oPrestamo As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString())
        Dim cant As Integer = 0
        For Each oDataItem As DataGridItem In grdDetalle.Items


            If UCase(oDataItem.Cells(5).Text) = "INGRESADO FINALIZADO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If

            If UCase(oDataItem.Cells(5).Text) = "INGRESADO ACTIVO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = True
                If oPrestamo.GetPrestamoErrorByProdId(oDataItem.Cells(10).Text) Then

                    oDataItem.Cells(5).Text = "INGRESADO RETENIDO"
                End If


            End If
            If UCase(oDataItem.Cells(5).Text) = "DEVUELTO" And IsDate(UCase(oDataItem.Cells(8).Text)) Then
                If Convert.ToDateTime((UCase(oDataItem.Cells(8).Text))) > System.DateTime.Now Then
                    CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
                    oDataItem.Cells(5).Text = "INGRESADO ACTIVO"
                Else
                    CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = True
                    oDataItem.Cells(5).Text = "INGRESADO FINALIZADO"
                End If

            End If
            If UCase(oDataItem.Cells(5).Text) = "ACTIVO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If

            If CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = True And _
            IsDate(UCase(oDataItem.Cells(8).Text)) Then
                If Convert.ToDateTime((UCase(oDataItem.Cells(8).Text))) >= System.DateTime.Now.ToString("dd/MM/yyyy") Then
                    oDataItem.Cells(5).Text = "INGRESADO FINALIZADO"
                    CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
                End If
            Else
                If IsDate(UCase(oDataItem.Cells(8).Text)) Then
                    If Convert.ToDateTime((UCase(oDataItem.Cells(8).Text))) >= System.DateTime.Now.ToString("dd/MM/yyyy") Then
                        CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
                    End If
                End If
            End If
        Next

        mActualizarEtiq()

    End Sub
 
    Public Sub chkSelCheck_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Written into several lines to clarify, you could go with one line
        'Current checkbox
        Dim box As CheckBox = CType(sender, CheckBox)
        'The TableCell the control is in
        Dim cell As TableCell = CType(box.Parent, TableCell)
        'The DataGridItem the cell belongs to
        Dim dgItem As DataGridItem = CType(cell.Parent, DataGridItem)


        Dim oPrestamo As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString())
        Dim intProductoId As Integer
        Dim dtProducto As DataTable
        Dim intEstadoIdAnterior As Integer
        Dim intHBA As Integer
        Dim intRazaId As Integer
        Dim Sexo As String
        Dim lngProdId As Long = 0
        Dim lngPrestamoId As Long = 0

        Dim dtProdSeleccionado As DataTable

        'With that DataGridItem you get for example ItemIndex of the DataGridItem you
        'are into. It is the same would be e.Item in ItemCommand,ItemDataBound or
        'ItemCreated. And you can then again get the PK of the current row and so on
        'via DataKeys collection.

        If lblAlta.Text = "A" And cmbTipo.Valor = mProdTodos Then
            CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True

            For Each lDr As DataRow In _
                        mdsDatos.Tables(mstrTablaDetalle).Select()

                lDr.Item("_estado") = "Prestado"
                lDr.Item("pstd_fina_fecha") = System.DBNull.Value  ' se nodifico posicion de grilla
                grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO ACTIVO"
            Next
        End If


        If CType(sender, System.Web.UI.WebControls.CheckBox).Checked Then

            ' Si esta tildado esta Prestado 

            For Each lDr As DataRow In _
            mdsDatos.Tables(mstrTablaDetalle).Select("_pos=" & _
            grdDetalle.Items(dgItem.ItemIndex).Cells(7).Text)

                lngProdId = lDr.Item("pstd_prdt_id")
                lngPrestamoId = lDr.Item("pstd_pstm_id")


                dtProdSeleccionado = oPrestamo.GetPrestamoDetalleByProdId(lngPrestamoId, lngProdId, "")
                If dtProdSeleccionado.Rows.Count > 0 Then
                    If ValidarNulos(dtProdSeleccionado.Rows(0).Item("pstd_fina_fecha"), False).ToString() <> "0" And _
txtFechaHasta.Text <> "" Then
                        AccesoBD.clsError.gGenerarMensajes(Me, "El Producto ya se habia Devuelto Anteriormente")
                        Exit Sub

                    Else
                        lDr.Item("_estado") = "Prestado"
                        lDr.Item("pstd_fina_fecha") = System.DBNull.Value
                        grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = "" '' se nodifico posicion de grilla
                        grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO ACTIVO"
                        CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True
                    End If
                Else

                    grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = "" '' se nodifico posicion de grilla

                    lDr.Item("_estado") = "Prestado"
                    lDr.Item("pstd_fina_fecha") = System.DBNull.Value
                    grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = "" '' se nodifico posicion de grilla
                    grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO ACTIVO"
                    CType(sender, System.Web.UI.WebControls.CheckBox).Checked = True
                End If

            Next

        Else

            grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text = DateTime.Now.ToShortDateString()  ' se nodifico posicion de grilla

            For Each lDr As DataRow In _
            mdsDatos.Tables(mstrTablaDetalle).Select("_pos=" & _
               grdDetalle.Items(dgItem.ItemIndex).Cells(7).Text)

                lDr.Item("_estado") = "Devuelto"
                lngProdId = lDr.Item("pstd_prdt_id")
                lngPrestamoId = lDr.Item("pstd_pstm_id")
                lDr.Item("pstd_fina_fecha") = grdDetalle.Items(dgItem.ItemIndex).Cells(8).Text  ' se nodifico posicion de grilla
                grdDetalle.Items(dgItem.ItemIndex).Cells(5).Text = "INGRESADO FINALIZADO"
                dtProducto = oPrestamo.GetPrestamoDetalleByProdId(lngPrestamoId, lngProdId, "")
                'If dtProducto.Rows.Count > 0 Then
                '    intEstadoIdAnterior = ValidarNulos(dtProducto.Rows(0).Item("prdt_esta_id"), False)
                '    ldr.Item("pstd_prdt_esta_id_Ant") = intEstadoIdAnterior
                'End If

            Next


        End If




        mdsDatos.AcceptChanges()
        mActualizarEtiq()
        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mActualizarEtiq()
        Try


            Dim oPrestamos As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString())
            Dim dtPrestamos As DataTable
            Dim intPrestamoId As Integer = 0
            If hdnId.Text = "" Then
                intPrestamoId = -1
            Else
                intPrestamoId = mdsDatos.Tables(0).Rows(0).Item("pstm_id")
            End If


            dtPrestamos = oPrestamos.GetPrestamosDetallesById(intPrestamoId, "ProductosTotal")

            Dim intSeleccionados As Integer = 0
            Dim intTotalProductos As Integer = 0
            Dim intTotalPrestados As Integer = 0
            Dim intTotalActivos As Integer = 0
            Dim intTotalDevueltos As Integer = 0
            Dim intTotalRetenidos As Integer = 0
            Dim ldrDatos As DataRow


            For Each lDr As DataRow In mdsDatos.Tables("PRESTAMOS").Select()
                intTotalProductos = intTotalProductos + 1
                If lDr.Item("_estado").ToString() <> "" Then
                    Select Case UCase(lDr.Item("_estado"))
                        Case "PRESTADO"
                            intTotalPrestados = intTotalPrestados + 1

                        Case "ACTIVO"
                            intTotalActivos = intTotalActivos + 1

                        Case "DEVUELTO"
                            intTotalDevueltos = intTotalDevueltos + 1

                        Case "INGRESADO FINALIZADO"
                            intTotalDevueltos = intTotalDevueltos + 1

                        Case "INGRESADO ACTIVO"
                            'If lDr.Item("_CHK") = "1" Then
                            intTotalPrestados = intTotalPrestados + 1
                            ' End If


                        Case "INGRESADO RETENIDO"
                            ' If lDr.Item("_CHK") = "1" Then
                            intTotalRetenidos = intTotalRetenidos + 1
                            'End If
                        Case Else
                            '  intTotalLevanta = intTotalLevanta + 1

                    End Select

                End If
            Next


            lblTotalProductos.Text = "Total Productos:" & intTotalProductos
            lblTotalActivos.Text = "Total Activo:" & intTotalActivos
            lblTotalPrestados.Text = "Total Prestados:" & intTotalPrestados
            lblTotalDevuelto.Text = "Total Devueltos:" & intTotalDevueltos
            lblTotalRetenidos.Text = "Total Retenidos:" & intTotalRetenidos
        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub grdDato_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdDato.PageIndexChanged


    End Sub
    Private Sub btnSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionarTodos.Click
        mChequearTodos()

        mActualizarEtiq()

    End Sub
    Public Sub messageBox(ByVal message As String, ByVal page As Object)
        Dim lbl As New Label
        lbl.Text = """"
        page.Controls.Add(lbl)
    End Sub
    Private Sub mChequearTodos()
        messageBox("El estado debe ser distinto de Devuelto", Me)


        mActualizarCHK(True)

        For Each oDataItem As DataGridItem In grdDetalle.Items
            If UCase(oDataItem.Cells(7).Text) <> "PRESTADO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If
            If UCase(oDataItem.Cells(7).Text) <> "DEVUELTO" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = True
            End If
        Next

        mdsDatos.AcceptChanges()
        mActualizarEtiq()
    End Sub
    Private Sub mActualizarCHK(ByVal boolCHKTodos As Boolean)
        If boolCHKTodos Then
            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                lDr.Item("_CHK") = "1"
                lDr.Item("_estado") = "Prestado"
            Next
        Else
            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                lDr.Item("_CHK") = "0"
                lDr.Item("_estado") = "Devuelto"
            Next
        End If



        mdsDatos.AcceptChanges()
        mActualizarEtiq()
    End Sub
    Private Sub mDesChequearTodos()

        mActualizarCHK(False)

        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado<>'Devuelto'")
            lDr.Item("_estado") = "Prestado"
        Next

        For Each oDataItem As DataGridItem In grdDetalle.Items

            If UCase(oDataItem.Cells(7).Text) <> "Devuelto" Then
                CType(oDataItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked = False
            End If
        Next
        mdsDatos.AcceptChanges()

        mActualizarEtiq()
    End Sub
    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
        Dim strFitro As String



        'If usrProdFilDeta.txtSraNumeExt.Valor.ToString <> "" Then
        '    strFitro = strFitro + " _hba = " + usrProdFilDeta.txtSraNumeExt.Valor.ToString + " and "
        'End If


        'If usrProdFilDeta.txtRPExt.Text.ToString <> "" Then
        '    strFitro = strFitro + " _rp = '" + usrProdFilDeta.txtRPExt.Text.ToString + "' and "
        'Else
        '    If txtRPBusq.Text.ToString <> "" Then
        '        strFitro = strFitro + " _rp = '" + txtRPBusq.Text.ToString + "' and "
        '    End If
        'End If



        If txtRPBusq.Text.ToString <> "" Then
            strFitro = strFitro + " _rp = '" + txtRPBusq.Text.ToString + "' and "
        End If


        If txtHBABusq.Text.ToString <> "" Then
            strFitro = strFitro + " _hba = '" + txtHBABusq.Text.ToString + "' and "
        End If



        'If usrProdFilDeta.RazaId.ToString <> "" Then
        '    strFitro = strFitro + " _RazaId = " + usrProdFilDeta.RazaId.ToString + " and "
        'End If

        'If txtFechaFinalProducto.Text <> "" Then
        '    strFitro = strFitro + " pstd_fina_fecha = '" + txtFechaFinalProducto.Text + "' and "
        'End If

        'If usrProdFilDeta.cmbSexoProdExt.Valor.ToString <> "" Then
        '    strFitro = strFitro + " _sexo = " + _
        '    IIf(usrProdFilDeta.cmbSexoProdExt.Valor.ToString = "1", "'Macho'", "'Hembra'") + " and "
        'End If

        If cmbSexoBusq.Valor.ToString() <> "" Then
            strFitro = strFitro + " _sexo = " + _
            IIf(cmbSexoBusq.Valor.ToString() = "1", "'Macho'", "'Hembra'") + " and "
        End If

        'If usrProdFilDeta.txtProdNombExt.Valor.ToString <> "" Then
        '    strFitro = strFitro + " _nombre like '%" & _
        '    usrProdFilDeta.txtProdNombExt.Valor.ToString & "%' and "
        'End If

        strFitro = strFitro + "1=1"

        mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = strFitro

        If strFitro = "1=1" Then
            mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = ""
        End If

        With mdsDatos.Tables(mstrTablaDetalle)
            .DefaultView.Sort = "_estado desc"
            grdDetalle.DataSource = .DefaultView

            ' the currentpageindex must be >= 0 and < pagecount<BR>   
            Try

                grdDetalle.DataBind()
                grdDetalle.Visible = True

            Catch
                Try
                    grdDetalle.CurrentPageIndex = 0
                    grdDetalle.DataBind()
                    grdDetalle.Visible = True

                Catch
                    ' Me.lblError.Text = "No data for selection"
                    ' Me.lblError.Visible = True

                End Try

            End Try


        End With

    End Sub
    Private Function GetActivos() As Integer
        Dim iRetorno As Integer = 0

        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Activo'")
            iRetorno = iRetorno + 1
        Next

        Return iRetorno
    End Function

    Private Function EstaDevuelto() As Boolean
        Try
            EstaDevuelto = False

            Dim oPrestamos As New SRA_Neg.Prestamo(mstrConn, Session("sUserId").ToString())
            Dim dtPrestamos As DataTable
            Dim intPrestamoId As Integer = 0
            If hdnId.Text = "" Then
                intPrestamoId = -1
            Else
                intPrestamoId = mdsDatos.Tables(0).Rows(0).Item("pstm_id")
            End If


            dtPrestamos = oPrestamos.GetPrestamosDetallesById(intPrestamoId, "ProductosTotal")

            Dim intSeleccionados As Integer = 0
            Dim intTotalProductos As Integer = 0
            Dim intTotalPrestados As Integer = 0
            Dim intTotalActivos As Integer = 0
            Dim intTotalDevuelto As Integer = 0


            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
                intTotalProductos = intTotalProductos + 1

                Select Case UCase(lDr.Item("_estado"))
                    Case "PRESTADO"
                        intTotalPrestados = intTotalPrestados + 1
                    Case "ACTIVO"
                        intTotalActivos = intTotalActivos + 1
                    Case "DEVUELTO"
                        intTotalDevuelto = intTotalDevuelto + 1
                    Case Else
                        '  intTotalLevanta = intTotalLevanta + 1

                End Select


            Next


            If (intTotalProductos = intTotalDevuelto) Then
                EstaDevuelto = True
            End If

        Catch ex As Exception

        End Try
    End Function

    Private Function GetDevuelto() As Integer

        Dim iRetorno As Integer = 0


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Devuelto'")
            iRetorno = iRetorno + 1
        Next

        Return iRetorno
    End Function

    Private Function GetPrestamo() As Integer

        Dim iRetorno As Integer = 0


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select("_estado='Prestado'")
            iRetorno = iRetorno + 1
        Next

        Return iRetorno
    End Function

    Private Function GetTotal() As Integer

        Dim iRetorno As Integer = 0


        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDetalle).Select()
            iRetorno = iRetorno + 1
        Next

        Return iRetorno
    End Function

    Public Function MuestraEstado(ByVal objText1 As Object) As String

        Dim dtEstado As DataTable
        Dim sReturn As String = ""

        Dim oEstado As New SRA_Neg.Estado(mstrConn, 1)

        dtEstado = oEstado.GetEstadoById(objText1.ToString(), "")
        If dtEstado.Rows.Count > 0 Then
            sReturn = dtEstado.Rows(0).Item("ESTA_DESC").ToString()
        End If

        Return sReturn
    End Function
    Public Function MuestraRaza(ByVal objText1 As Object) As String


        Dim dtRaza As DataTable
        Dim sReturn As String = ""

        Dim intRetornoRaza As Integer

        Dim oRaza As New SRA_Neg.Razas(mstrConn, 1)

        intRetornoRaza = oRaza.GetRazaCodiById(objText1.ToString())
        oRaza.GetRazaDescripcionById(intRetornoRaza)
        Return sReturn
    End Function
    Private Sub btnDeSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeSeleccionarTodos.Click
        mDesChequearTodos()
    End Sub

    Private Sub grdDetalle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdDetalle.SelectedIndexChanged

    End Sub

    Private Sub grdDetalle_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDetalle.ItemCommand

        Try
            Dim strPrestamoDetaId As String = String.Empty
            Dim lsbMsg As New StringBuilder
            Dim lsbPagina As New System.Text.StringBuilder

            If e.CommandName = "Select" Then
                strPrestamoDetaId = e.Item.Cells.Item(1).Text

                lsbPagina.Append("errores.aspx?tabla=prestamos_errores&proce=" + mintProce.ToString() + "&ID=" + strPrestamoDetaId)
                clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 600, 250, 100, 100)

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

End Class

End Namespace
