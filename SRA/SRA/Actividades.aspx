<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Actividades" CodeFile="Actividades.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Actividades</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultgrupntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame();" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="2"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Actividades</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3" align="center"><asp:datagrid id="grdDato" runat="server" width="794px" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="9px"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="acti_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="acti_desc" HeaderText="Descipci&#243;n"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cuct_desc" HeaderText="Cuenta"></asp:BoundColumn>
											<asp:BoundColumn DataField="_siste" HeaderText="SIST"></asp:BoundColumn>
											<asp:BoundColumn DataField="_fact" HeaderText="Fact."></asp:BoundColumn>
											<asp:BoundColumn DataField="_gene_inte" HeaderText="Gen.Int."></asp:BoundColumn>
											<asp:BoundColumn DataField="_adhe" HeaderText="P.adhe."></asp:BoundColumn>
											<asp:BoundColumn DataField="_AplicaSobreTasaPagoRetrasado" HeaderText="Ap. Sobretasa"></asp:BoundColumn>
											<asp:BoundColumn DataField="_AdmiteProforma" HeaderText="A. Prof"></asp:BoundColumn>
											<asp:BoundColumn DataField="_PermitePagoEnCuotas" HeaderText="P. Pago en Cuotas"></asp:BoundColumn>
											<asp:BoundColumn DataField="_PermitePagoParciales" HeaderText="P. Pagos Parciales"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD vAlign="middle" width="100%">&nbsp;
									<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
										BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
										BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif"
										ToolTip="Nueva Actividad"></CC1:BOTONIMAGEN></TD>
								<TD width="50" align="right"><CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
										BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
										ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center"><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
										Visible="False">
										<TABLE style="WIDTH: 100%" id="Table2" class="FdoFld" border="0" cellPadding="0" align="left">
											<TR>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
												<TD vAlign="top" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblNroActi" runat="server" cssclass="titulo">Nro.Actividad:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<CC1:TEXTBOXTAB id="txtNroActi" runat="server" cssclass="cuadrotexto" Width="72px" Obligatorio="True"
																	enabled="false"></CC1:TEXTBOXTAB></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="270" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblCuct" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<TABLE border="0" cellSpacing="0" cellPadding="0">
																	<TR>
																		<TD>
																			<cc1:combobox id="cmbCuct" class="cuadrotexto" runat="server" cssclass="combo" Width="300px" Obligatorio="False"
																				MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true" TextMaxLength="7"></cc1:combobox></TD>
																		<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																				id="btnAvanBusq1" onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuct','Cuentas Contables','');"
																				border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																		</TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="Label2" runat="server" cssclass="titulo">Centro de Costo (R+):</asp:Label>&nbsp;</TD>
															<TD colSpan="12">
																<TABLE border="0" cellSpacing="0" cellPadding="0">
																	<TR>
																		<TD>
																			<cc1:combobox id="cmbCentroCosto" class="cuadrotexto" runat="server" cssclass="combo" Width="300px"
																				Obligatorio="False" MostrarBotones="False" NomOper="centrosc_cargar" filtra="true" TextMaxLength="6"></cc1:combobox></TD>
																		<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																				id="btnAvanBusq2" onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCentroCosto','Centros de Costo','@cuenta=4');"
																				border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																		</TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="Label1" runat="server" cssclass="titulo">Centro de Costo (R-):</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<TABLE border="0" cellSpacing="0" cellPadding="0">
																	<TR>
																		<TD>
																			<cc1:combobox id="cmbCentroCostoNega" class="cuadrotexto" runat="server" cssclass="combo" Width="300px"
																				Obligatorio="False" MostrarBotones="False" NomOper="centrosc_cargar" filtra="true" TextMaxLength="6"></cc1:combobox></TD>
																		<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																				id="btnAvanBusq3" onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCentroCostoNega','Centros de Costo','@cuenta=5');"
																				border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																		</TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="Label3" runat="server" cssclass="titulo">Centro de Costo (Mora):</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<TABLE border="0" cellSpacing="0" cellPadding="0">
																	<TR>
																		<TD>
																			<cc1:combobox id="cmbCentroCostoMora" class="cuadrotexto" runat="server" cssclass="combo" Width="300px"
																				Obligatorio="False" MostrarBotones="False" NomOper="centrosc_cargar" filtra="true" TextMaxLength="6"></cc1:combobox></TD>
																		<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																				id="btnAvanBusq4" onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCentroCostoMora','Centros de Costo','@cuenta=4');"
																				border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																		</TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblFact" runat="server" cssclass="titulo">Factura:</asp:Label>&nbsp;</TD>
															<TD colSpan="12">
																<cc1:combobox id="cmbFact" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblAdhe" runat="server" cssclass="titulo">Precio adherente:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:combobox id="cmbAdhe" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="14"
																align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblGeneInte" runat="server" cssclass="titulo">Genera Inter�s:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:combobox id="cmbGeneInte" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblIntePorc" runat="server" cssclass="titulo">Inter�s - Porcentaje:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:numberbox id="txtIntePorc" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																	EsDecimal="True"></cc1:numberbox></TD>
														</TR>
														<TR id="trMora" runat="server">
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblInteMoraPorc" runat="server" cssclass="titulo">Inter�s Mora - Porcentaje:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:numberbox id="txtInteMoraPorc" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																	EsDecimal="True"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblInteMonto" runat="server" cssclass="titulo">Inter�s - Monto:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:numberbox id="txtInteMonto" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																	EsDecimal="True"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblVctoCantDias" runat="server" cssclass="titulo">Vto. - D�as Fecha Factura:</asp:Label>&nbsp;
															</TD>
															<TD>
																<cc1:numberbox id="txtVctoCantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;
															</TD>
															<TD>
																<asp:Label id="lblVcto2CantDias" runat="server" cssclass="titulo">Vto.2:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtVcto2CantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
															<TD>
																<asp:Label id="lblVcto3CantDias" runat="server" cssclass="titulo">Vto.3:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtVcto3CantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
															<TD>
																<asp:Label id="lblVcto4CantDias" runat="server" cssclass="titulo">Vto.4:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtVcto4CantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
															<TD>
																<asp:Label id="lblVcto5CantDias" runat="server" cssclass="titulo">Vto.5:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtVcto5CantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
															<TD>
																<asp:Label id="lblVcto6CantDias" runat="server" cssclass="titulo">Vto.6:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtVcto6CantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
															<TD>
																<asp:Label id="lblVcto7CantDias" runat="server" cssclass="titulo">Vto.7:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtVcto7CantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
															<TD>
																<asp:Label id="lblVcto8CantDias" runat="server" cssclass="titulo">Vto.8:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtVcto8CantDias" runat="server" cssclass="cuadrotexto" Width="45px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblVctoFijoDia" runat="server" cssclass="titulo">Vto. - D�a del Mes:</asp:Label>&nbsp;
															</TD>
															<TD colSpan="15">
																<cc1:numberbox id="txtVctoFijoDia" runat="server" cssclass="cuadrotexto" Width="45px"></cc1:numberbox>&nbsp;</TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblDiscPrecioUnitario" runat="server" cssclass="titulo">Discrimina Precio Unitario:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:combobox id="cmbDiscriminaPrecioUnitario" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblAdmiteProforma" runat="server" cssclass="titulo">Admite Proforma:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:combobox id="cmbAdmiteProforma" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblAplicaSobreTasaPagoRetrasado" runat="server" cssclass="titulo">Aplica SobreTasa por Pago Retrasado:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:combobox id="cmbAplicaSobreTasaPagoRetrasado" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblPermitePagoEnCuotas" runat="server" cssclass="titulo">Permite Pago en Cuotas:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:combobox id="cmbPermitePagoEnCuotas" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="16" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 314px" align="right">
																<asp:Label id="lblPermitePagoParciales" runat="server" cssclass="titulo">Permite Pago Parciales:</asp:Label>&nbsp;</TD>
															<TD colSpan="15">
																<cc1:combobox id="cmbPermitePagoParciales" class="combo" runat="server" Width="50px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel><ASP:PANEL id="panBotones" Runat="server">
										<TABLE width="100%">
											<TR>
												<TD align="center">
													<asp:Label id="lblSiste" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja"></asp:Button>&nbsp;
													<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Modificar"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Limpiar"></asp:Button></TD>
											</TR>
										</TABLE>
									</ASP:PANEL></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnEmctId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null&&!document.all["txtDesc"].disabled)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
