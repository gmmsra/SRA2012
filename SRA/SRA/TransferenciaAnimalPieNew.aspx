<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TransferenciaAnimalPieNew" CodeFile="TransferenciaAnimalPieNew.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="CRIA" Src="controles/usrCriador.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>
        <%=mstrTitulo%>
    </title>
    <meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultpilontScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/utiles.js"></script>
    <script type="text/javascript" src="includes/paneles.js"></script>
    <script type="text/javascript" src="INCLUDES/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="INCLUDES/transferenciaProductos.js"></script>

    <script type="text/javascript" language="JavaScript">

        function expandir() {
            if (parent.frames.length > 0) try { parent.frames("menu").CambiarExp(); } catch (e) {; }
        }

        function setearControles() {
        }
    </script>
    <style type="text/css">
        .auto-style1 {
            FONT: bold 8pt/10pt Verdana, Arial, Gill Sans;
            COLOR: #003784;
            height: 24px;
        }

        .auto-style2 {
            height: 24px;
        }
    </style>
</head>
<body class="pagina" leftmargin="5" topmargin="5" rightmargin="0"
    onunload="gCerrarVentanas();">
    <form id="frmABM" method="post" runat="server">
        <%--onsubmit="gCerrarVentanas();"--%>
        <!------------------ RECUADRO ------------------->
        <table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
            <tr>
                <td width="9">
                    <img height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recsup.jpg">
                    <img height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
                <td width="13">
                    <img height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9" background="imagenes/reciz.jpg">
                    <img height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table id="Table1" style="WIDTH: 100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="HEIGHT: 25px" valign="bottom" colspan="3" height="25">
                                    <asp:Label ID="lblTituAbm" runat="server" CssClass="opcion">Transferencia de Animal en Pie</asp:Label></td>
                            </tr>
                            <tr>
                                <td valign="middle" nowrap colspan="3">
                                    <asp:Panel ID="panFiltros" runat="server" CssClass="titulo" Width="100%" BorderWidth="1px"
                                        BorderStyle="none">
                                        <table id="Table3" border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td style="HEIGHT: 8px" width="24"></td>
                                                            <td style="HEIGHT: 8px" width="42"></td>
                                                            <td style="HEIGHT: 8px" width="26"></td>
                                                            <td style="HEIGHT: 8px"></td>
                                                            <td style="HEIGHT: 8px" width="26">
                                                                <cc1:BotonImagen ID="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
                                                                    ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></cc1:BotonImagen></td>
                                                            <td style="HEIGHT: 8px" valign="middle" width="26">
                                                                <cc1:BotonImagen ID="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
                                                                    ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></cc1:BotonImagen></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="HEIGHT: 8px" width="24">
                                                                <img border="0" src="imagenes/formfle.jpg" width="24" height="25"></td>
                                                            <td style="HEIGHT: 8px" width="42">
                                                                <img border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></td>
                                                            <td style="HEIGHT: 8px" width="26">
                                                                <img border="0" src="imagenes/formcap.jpg" width="26" height="25"></td>
                                                            <td style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colspan="3">
                                                                <img border="0" src="imagenes/formfdocap.jpg" height="25"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="HEIGHT: 50px">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
                                                        <tr>
                                                            <td background="imagenes/formiz.jpg" width="3">
                                                                <img border="0" src="imagenes/formiz.jpg" width="3" height="17"></td>
                                                            <td style="HEIGHT: 100%">
                                                                <!-- FOMULARIO -->
                                                                <table class="FdoFld" border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
                                                                    <tr>
                                                                        <td style="WIDTH: 15%" height="15" align="right">
                                                                            <asp:Label ID="lblNumeFil" runat="server" CssClass="titulo"> Nro.Tr�mite:</asp:Label>&nbsp;</td>
                                                                        <td style="WIDTH: 85%">
                                                                            <cc1:NumberBox ID="txtNumeFil" runat="server" CssClass="cuadrotexto" MaxValor="999999999" Width="140px"
                                                                                AceptaNull="False"></cc1:NumberBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" align="right">
                                                                            <asp:Label ID="lblEstadoFil" runat="server" CssClass="titulo">Estado:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:ComboBox ID="cmbEstadoFil" class="combo" runat="server" Width="200px" AceptaNull="False"
                                                                                NomOper="estados_cargar"></cc1:ComboBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="http://localhost:16852/imagenes/formdivmed.jpg" colspan="3" align="right">
                                                                            <img src="http://localhost:16852/imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" align="right">
                                                                            <asp:Label ID="lblFechaDesdeFil" runat="server" CssClass="titulo">Fecha Desde:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtFechaDesdeFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:Label ID="lblFechaHastaFil" runat="server" CssClass="titulo">Hasta:&nbsp;</asp:Label>
                                                                            <cc1:DateBox ID="txtFechaHastaFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15" align="right">
                                                                            <asp:Label ID="lblFechaTransferenciaFil" runat="server" CssClass="titulo">Fecha Transferencia:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtFechaTransferenciaFil" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="WIDTH: 153px" height="15" align="right">
                                                                            <asp:Label ID="lblProdFil" runat="server" CssClass="titulo">Producto:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <uc1:PROH ID="usrProdFil" runat="server" AceptaNull="false" MuestraDesc="false" AutoPostback="False"
                                                                                IncluirDeshabilitados="true" FilTipo="S" EsPropietario="True" Ancho="800" Saltos="1,2"
                                                                                Tabla="productos"></uc1:PROH>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="HEIGHT: 10px" align="right">
                                                                            <asp:Label ID="lblCompradorFil" runat="server" CssClass="titulo">Comprador:&nbsp;</asp:Label></td>
                                                                        <td>
                                                                            <uc1:CLIE ID="usrCompradorFil" runat="server" AceptaNull="false" MuestraDesc="False" AutoPostback="False"
                                                                                FilTipo="S" Saltos="1,2" Tabla="Criadores" FilDocuNume="True" MostrarBotones="False" FilSociNume="True"
                                                                                Criador="true" CampoVal="Criador"></uc1:CLIE>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td background="imagenes/formfdofields.jpg" nowrap align="right"></td>
                                                                        <td background="imagenes/formfdofields.jpg" width="72%">
                                                                            <asp:CheckBox ID="chkVisto" Text="Incluir Registros Vistos y/o Aprobados" CssClass="titulo" runat="server"></asp:CheckBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colspan="2" align="right"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2">
                                                                <img border="0" src="imagenes/formde.jpg" width="2" height="2"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" colspan="3">
                                    <asp:DataGrid ID="grdDato" runat="server" Width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdDato_Page"
                                        OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                        AllowPaging="True">
                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                        <FooterStyle CssClass="footer"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:pointer;" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="tram_id"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_inic_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha Inic."></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_oper_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha Transf."></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="tram_nume" HeaderText="N� Tr�mite" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="raza" HeaderText="Raza" HeaderStyle-Width="20%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="sexo" HeaderText="Sexo"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="prdt_nomb" HeaderText="Nombre" HeaderStyle-Width="30%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="_numero" HeaderText="N� Producto" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="tram_comp_clie_id" HeaderText="Comprador" HeaderStyle-Width="10%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="tram_vend_clie_id" HeaderText="Vendedor" HeaderStyle-Width="10%"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="true" DataField="clie_apel" ReadOnly="True" HeaderText="Cmprador Apellido/Raz&#243;n Social"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="True" DataField="esta_desc" HeaderText="Estado"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td colspan="3" height="10"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" colspan="3">
                                    <cc1:BotonImagen ID="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
                                        ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                        IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif"
                                        ToolTip="Agregar una nueva Transferencia" OnClientClick="setearControles()"></cc1:BotonImagen></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3">
                                    <asp:Panel ID="panLinks" runat="server" CssClass="titulo" Width="100%" Height="100%" Visible="False">
                                        <table id="TablaSolapas" border="0" cellspacing="0" cellpadding="0" width="100%" runat="server">
                                            <tr>
                                                <td style="WIDTH: 0.24%">
                                                    <img border="0" src="imagenes/tab_a.bmp" width="9" height="27" alt=""></td>
                                                <td style="background-image: url(imagenes/tab_b.bmp)">
                                                    <img border="0" src="imagenes/tab_b.bmp" width="8" height="27" alt=""></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_c.bmp" width="31" height="27" alt=""></td>
                                                <td style="background-image: url(imagenes/tab_fondo.bmp)" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkCabecera" runat="server"
                                                        CssClass="solapa" Width="80px" Height="21px" CausesValidation="False"> Tr�mite </asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27" alt=""></td>
                                                <td style="background-image: url(imagenes/tab_fondo.bmp)" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkRequ" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Requisitos</asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27" alt=""></td>
                                                <td style="background-image: url(imagenes/tab_fondo.bmp)" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkDocu" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Documentaci�n </asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27" alt=""></td>
                                                <td style="background-image: url(imagenes/tab_fondo.bmp)" width="1">
                                                    <asp:LinkButton Style="TEXT-ALIGN: center; TEXT-DECORATION: none" ID="lnkObse" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> Comentarios</asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27" alt=""></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>

                            <%-- ... --%>

                            <tr>
                                <td align="center" colspan="3">

                                    <%-- Nuevo GM --%>
                                    <asp:Panel ID="panDato_aux" runat="server" CssClass="titulo" Style="width: 100%; border: 1px solid green; height: 100%">
                                        <table class="" style="width: 100%; height: 106px; border-collapse: collapse; border-spacing: 0; border-style: solid; border-width: 1px; width: 100%; text-align: left; background-color: #EAEAEA">
                                            <tbody>
                                                <tr>
                                                    <td style="height: 5px">
                                                        <asp:Label ID="lblTitu_aux" runat="server" CssClass="titulo" Width="100%"></asp:Label>
                                                    </td>
                                                    <td style="vertical-align: top; text-align: right">
                                                        <asp:ImageButton ID="imgClose_aux" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="height: 1px; background-color: #B4B4B4;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="width: 100%; text-align: center">
                                                        <asp:Panel ID="panCabecera_aux" runat="server" CssClass="titulo" Style="width: 100%">
                                                            <table id="tabCabecera_aux" style="width: 100%; text-align: left; border: 0; border-spacing: 0; border-collapse: collapse">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width: 18%; text-align: right; white-space: nowrap">
                                                                            <asp:Label ID="lblInicFecha1_aux" runat="server" CssClass="titulo" Style="padding-right: 5px">Fecha Carga:</asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtInicFecha_aux" runat="server" CssClass="cuadrotexto" Style="width: 68px" Enabled="False"></cc1:DateBox>
                                                                            <asp:Label ID="lblFinaFecha_aux" runat="server" CssClass="titulo" Style="padding-left: 15px; padding-right: 5px">Fecha Cierre:</asp:Label>
                                                                            <cc1:DateBox ID="txtFinaFecha_aux" runat="server" CssClass="cuadrotexto" Style="width: 68px" Enabled="False"></cc1:DateBox>
                                                                            <input id="chkRetenerTransferencia" type="checkbox" runat="server" style="margin-left: 80px" />
                                                                            <asp:Label ID="lblRetenerTransferencia" runat="server" CssClass="titulo" Style="padding-left: 2px">Retener transferencia</asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 18%; text-align: right; white-space: nowrap">
                                                                            <asp:Label ID="lblFechaTransferencia1_aux" runat="server" CssClass="titulo" Style="padding-right: 5px">Fecha de Transferencia:</asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtFechaTransferencia_aux" runat="server" CssClass="cuadrotexto" Style="width: 68px" Enabled="True" 
                                                                                EnterPorTab="true" EsTransferencia="True" MetodoPostValid ="buscarVendedores" onchange="myFunction()"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 18%; text-align: right; white-space: nowrap">
                                                                            <asp:Label ID="lblFechaTram1_aux" runat="server" CssClass="titulo" Style="padding-right: 5px">Fecha Recepi�n Tramite:</asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtFechaTram_aux" runat="server" CssClass="cuadrotexto" Style="width: 68px" EnterPorTab="true"></cc1:DateBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 18%; text-align: right; white-space: nowrap">
                                                                            <asp:Label ID="lblNroControl_aux" runat="server" CssClass="titulo">Nro. de Control:&nbsp</asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <cc1:NumberBox ID="txtNroControl_aux" runat="server" CssClass="cuadrotexto" Style="width: 81px" MaxValor="999999999" Visible="true" MaxLength="3" EsPorcen="False" EsDecimal="False"></cc1:NumberBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 18%; text-align: right; white-space: nowrap">
                                                                            <asp:Label ID="lblEsta_aux" runat="server" CssClass="titulo" Style="padding-right: 5px">Estado:</asp:Label>

                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="txtEstado" runat="server" CssClass="titulo"></asp:Label>
                                                                        </td>
                                                                    </tr>
<%--                                                                    <tr>
                                                                        <td></td>
                                                                        <td>
                                                                            <cc1:NumberBox ID="NumberBox1" runat="server" EsDecimal="True"></cc1:NumberBox>
                                                                        </td>
                                                                    </tr>--%>
                                                                    <tr>
                                                                        <td colspan="2" style="height: 1px; background-color: #B4B4B4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 15px; text-align: right; vertical-align: middle">
                                                                            <asp:Label ID="lblProducto_aux" runat="server" CssClass="titulo" Style="padding-right: 5px">Producto a transferir:</asp:Label>
                                                                        </td>
                                                                        <td id="tdProductoTransferir" runat="server">
                                                                            <uc1:PROH ID="usrProd_aux" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2" Ancho="800"
                                                                                EsPropietario="True" Copropiedad="True" FilTipo="S" AutoPostback="False" MuestraDesc="false"
                                                                                MuestraSoloRazaClieDerivr="true" IncluirDeshabilitados="true"></uc1:PROH>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="height: 1px; background-color: #B4B4B4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 15px; text-align: right; vertical-align: middle">
                                                                            <asp:Label ID="lblVendedores" runat="server" CssClass="titulo" Style="padding-right: 5px">Vendedores:</asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DataGrid ID="gridVendedores" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                                                                HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" PageSize="5"
                                                                                OnUpdateCommand="gridVendedores_UpdateCommand"
                                                                                OnPageIndexChanged="gridVendedores_PageIndexChanged">
                                                                                <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                                <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                                <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                                                <FooterStyle CssClass="footer"></FooterStyle>
                                                                                <Columns>
                                                                                    <asp:TemplateColumn>
                                                                                        <HeaderStyle Width="20px"></HeaderStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="LinkButtonVendedores" runat="server" Text="Editar" CausesValidation="false" CommandName="Update"> <img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:pointer;" /></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="tram_vc_id"></asp:BoundColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="estado"></asp:BoundColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="tram_vc_clie_id"></asp:BoundColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="tram_vc_cria_id"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="raza_codi_desc" HeaderText="Raza">
                                                                                        <HeaderStyle Width="30%"></HeaderStyle>
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="cria_nume" HeaderText="N� Prop.">
                                                                                        <HeaderStyle Width="8%"></HeaderStyle>
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="clie_fanta" HeaderText="Vendedor"></asp:BoundColumn>
                                                                                </Columns>
                                                                                <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td></td>
                                                                        <td id="tdUsrCriaVend_aux" runat="server" style="padding-top: 8px">
                                                                            <uc1:CLIE ID="usrCriaVend_aux" runat="server" Copropiedad="True" AceptaNull="False" Tabla="Criadores"
                                                                                Saltos="1,2" Ancho="800" Criador="true" FilDocuNume="True" MuestraDesc="False"></uc1:CLIE>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td style="height: 30px; vertical-align: middle; text-align: center" colspan="2">
                                                                            <asp:Button ID="btnAgregarVendedor" runat="server" CssClass="boton" Text="Agregar" OnClientClick="return agregarVendedor();"></asp:Button>&nbsp;
										                                    <asp:Button ID="btnModificarVendedor" runat="server" CssClass="boton" Text="Modificar" OnClientClick="return modificarVendedor();"></asp:Button>&nbsp;
                                                                            <asp:Button ID="btnEliminarVendedor" runat="server" CssClass="boton" Text="Eliminar" OnClientClick="return  confirm('� Desea eliminar el registro ?');"></asp:Button>&nbsp;
										                                    <asp:Button ID="btnCancelarVendedor" runat="server" CssClass="boton" Text="Cancelar"></asp:Button>&nbsp
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="height: 1px; background-color: #B4B4B4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 15px; text-align: right; vertical-align: middle">
                                                                            <asp:Label ID="lblCompradores" runat="server" CssClass="titulo" Style="padding-right: 5px">Compradores:</asp:Label>
                                                                        </td>
                                                                        <td>


                                                                            <asp:DataGrid ID="gridCompradores" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                                                                HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" PageSize="5"
                                                                                OnUpdateCommand="gridCompradores_UpdateCommand"
                                                                                OnPageIndexChanged="gridCompradores_PageIndexChanged">
                                                                                <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                                <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                                <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                                                <FooterStyle CssClass="footer"></FooterStyle>
                                                                                <Columns>
                                                                                    <asp:TemplateColumn>
                                                                                        <HeaderStyle Width="20px"></HeaderStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="LinkButtonVendedores" runat="server" Text="Editar" CausesValidation="false" CommandName="Update"> <img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:pointer;" /></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="tram_vc_id"></asp:BoundColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="estado"></asp:BoundColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="tram_vc_clie_id"></asp:BoundColumn>
                                                                                    <asp:BoundColumn Visible="False" DataField="tram_vc_cria_id"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="raza_codi_desc" HeaderText="Raza">
                                                                                        <HeaderStyle Width="30%"></HeaderStyle>
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="cria_nume" HeaderText="N� Prop.">
                                                                                        <HeaderStyle Width="8%"></HeaderStyle>
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="clie_fanta" HeaderText="Comprador"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="tram_vc_porcentaje" HeaderText="%">
                                                                                        <HeaderStyle Width="8%"></HeaderStyle>
                                                                                    </asp:BoundColumn>
                                                                                </Columns>
                                                                                <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td id="tdUsrCriaComp_aux" runat="server" style="padding-top: 8px">
                                                                            <uc1:CLIE ID="usrCriaComp_aux" runat="server" Copropiedad="True" AceptaNull="False" Tabla="Criadores"
                                                                                Saltos="1,2" Ancho="800" Criador="true" FilDocuNume="True" MuestraDesc="False"></uc1:CLIE>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td class="titulo" style="padding-left: 5px">Porcentaje:
                                                                            <cc1:NumberBox ID="txtPorcentajeCompra" runat="server" CssClass="cuadrotexto" Width="80px" EsDecimal="false" AutoPostBack="false" MaxLength="9" name="txtPorcentajeCompra" autocomplete="off"></cc1:NumberBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td></td>
                                                                        <td style="height: 30px; vertical-align: middle; text-align: center" colspan="2">
                                                                            <asp:Button ID="btnAgregarComprador" runat="server" CssClass="boton" Text="Agregar" OnClientClick="return agregarComprador();"></asp:Button>&nbsp;
										                                    <asp:Button ID="btnModificarComprador" runat="server" CssClass="boton" Text="Modificar" OnClientClick="return modificarComprador();"></asp:Button>&nbsp;
										                                    <asp:Button ID="btnEliminarComprador" runat="server" CssClass="boton" Text="Eliminar" OnClientClick="return  confirm('� Desea eliminar el registro ?');"></asp:Button>&nbsp;
										                                    <asp:Button ID="btnCancelarComprador" runat="server" CssClass="boton" Text="Cancelar"></asp:Button>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="height: 1px; background-color: #B4B4B4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 15px; text-align: right; vertical-align: middle">
                                                                            <asp:Label ID="Label1" runat="server" CssClass="titulo" Style="padding-right: 5px">Reservas:</asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <table id="tbReservas" runat="server">
                                                                                <tr>
                                                                                    <td></td>
                                                                                    <td class="titulo" style="text-align: center">Cantidad</td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="titulo" style="text-align: right">Reserva semen:
                                                                                        <input id="chkReservaSemen" type="checkbox" runat="server" value="">
                                                                                    </td>
                                                                                    <td>
                                                                                        <cc1:NumberBox ID="txtCantReservaSemen" runat="server" CssClass="cuadrotexto" Width="80px" EsDecimal="True" AutoPostBack="false" MaxLength="9" autocomplete="off"></cc1:NumberBox>
                                                                                    </td>
                                                                                    <td class="titulo" style="text-align: right; padding-left: 50px">Reserva material gen�tico:
                                                                                        <input id="chkReservaMaterialGenetico" type="checkbox" runat="server" value="">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="auto-style1" style="text-align: right">Reserva de crias:
                                                                                        <input id="chkReservaCrias" type="checkbox" runat="server" value="">
                                                                                    </td>
                                                                                    <td class="auto-style2">
                                                                                        <cc1:NumberBox ID="txtCantCriasReser" runat="server" CssClass="cuadrotexto" Width="80px" MaxValor="999999999" AutoPostBack="false" MaxLength="9" autocomplete="off"></cc1:NumberBox>
                                                                                    </td>
                                                                                    <td class="auto-style1" style="text-align: right">Reserva el derecho de clonar:
                                                                                        <input id="chkReservaDerechoClonar" type="checkbox" runat="server" value="">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="titulo" style="text-align: right; padding-left: 20px">Reserva de embriones:
                                                                                        <input id="chkReservaEmbriones" type="checkbox" runat="server" name="" value="">
                                                                                    </td>
                                                                                    <td>
                                                                                        <cc1:NumberBox ID="txtCantReservaEmbriones" runat="server" CssClass="cuadrotexto" Width="80px" EsDecimal="True" AutoPostBack="false" MaxLength="9" autocomplete="off"></cc1:NumberBox>
                                                                                    </td>
                                                                                    <td class="titulo" style="text-align: right">Autoriza a comprador a clonar:
                                                                                        <input id="chkAutorizaCompradorClonar" type="checkbox" runat="server" value="">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="titulo" style="text-align: right">Reserva de vientre:
                                                                                        <input id="chkReservaVientre" type="checkbox" runat="server" value="">
                                                                                    </td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr>

                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="height: 1px; background-color: #B4B4B4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 15px; text-align: right; vertical-align: middle">
                                                                            <asp:Label ID="Label2" runat="server" CssClass="titulo" Style="padding-right: 5px">Observaciones:</asp:Label>
                                                                        </td>
                                                                        <td style="padding: 5px 0px">
                                                                            <cc1:TextBoxTab ID="txtObservaciones" runat="server" CssClass="cuadrotexto" Width="98%" AceptaNull="False" Height="41px" TextMode="MultiLine"></cc1:TextBoxTab>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <%-- Fin Nuevo --%>








                                    <asp:Panel ID="panDato" runat="server" CssClass="titulo" Width="100%" BorderWidth="1px" BorderStyle="Solid" Height="100%">
                                        <table style="WIDTH: 100%; HEIGHT: 100%" id="tabDato" class="FdoFld" border="0" cellpadding="0"
                                            align="left">
                                            <tbody>
                                                <%--                                                <tr>
                                                    <td height="5">
                                                        <asp:Label ID="lblTitu" runat="server" CssClass="titulo" Width="100%"></asp:Label></td>
                                                    <td valign="top" align="right">
                                                        <asp:ImageButton ID="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></td>
                                                </tr>--%>
                                                <%--                                                <tr>
                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                        <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                </tr>--%>
                                                <tr>
                                                    <td style="WIDTH: 100%" colspan="2" align="center">
                                                        <asp:Panel ID="panCabecera" runat="server" CssClass="titulo" Width="100%">
                                                            <table style="WIDTH: 100%" id="tabCabecera" border="0" cellpadding="0" align="left">
                                                                <tbody>
                                                                    <tr>
                                                                        <%--                                                                        <td width="18%" nowrap align="right">
                                                                            <asp:Label ID="lblInicFecha1" runat="server" CssClass="titulo">Fecha Carga:&nbsp;</asp:Label></td>
                                                                        <td>
                                                                            <cc1:DateBox ID="txtInicFecha" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																				<asp:Label ID="lblFinaFecha" runat="server" CssClass="titulo">Fecha Cierre:</asp:Label>
                                                                            <cc1:DateBox ID="txtFinaFecha" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox></td>
                                                                    </tr>--%>
                                                                        <%--<tr>--%>
                                                                        <%--                                                                        <td width="18%" nowrap align="right">
                                                                            <asp:Label ID="lblFechaTransferencia1" runat="server" CssClass="titulo">Fecha de Transferencia:&nbsp;</asp:Label></td>
                                                                        <td>--%>
                                                                        <%--                                                                            <cc1:DateBox ID="txtFechaTransferencia" runat="server" CssClass="cuadrotexto" Width="68px" Enabled="True"></cc1:DateBox>--%>
                                                                        <%--                                                                        <caption>
                                                                            &nbsp;&nbsp;&nbsp;</td>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right" nowrap>
                                                                                    <asp:Label ID="lblFechaTram1" runat="server" CssClass="titulo">Fecha Recepi�n Tramite:&nbsp;</asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <cc1:DateBox ID="txtFechaTram" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox>
                                                                                </td>
                                                                            </tr>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right" width="18%">
                                                                                    <asp:Label ID="lblNroControl" runat="server" CssClass="titulo">Nro. de Control:</asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <cc1:NumberBox ID="txtNroControl" runat="server" CssClass="cuadrotexto" EsDecimal="False" EsPorcen="False" MaxLength="3" MaxValor="999999999" Visible="true" Width="81px"></cc1:NumberBox>
                                                                                </td>
                                                                            </tr>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label ID="lblEsta" runat="server" CssClass="titulo">Estado:&nbsp;</asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <cc1:ComboBox ID="cmbEsta" runat="server" AutoPostBack="True" class="combo" CssClass="cuadrotexto" Enabled="False" Width="180px">
                                                                                    </cc1:ComboBox>
                                                                                </td>
                                                                            </tr>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></img></td>
                                                                            </tr>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right" height="15" valign="top">
                                                                                    <asp:Label ID="lblCriaComp" runat="server" CssClass="titulo">Comprador Raza/Criador:</asp:Label>
                                                                                    &nbsp;</td>
                                                                                <td>
                                                                                    <uc1:CLIE ID="usrCriaComp" runat="server" AceptaNull="False" Ancho="800" Copropiedad="True" Criador="true" FilDocuNume="True" MuestraDesc="False" Saltos="1,2" Tabla="Criadores" />
                                                                                </td>
                                                                                <td valign="top">
                                                                                    <asp:Button ID="btnCriaCopropiedadComp" runat="server" CausesValidation="False" CssClass="boton" Text="Copropiedad" Width="90px" />
                                                                                </td>
                                                                                </td>
                                                                            </tr>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></img></td>
                                                                            </tr>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right" height="15" valign="top">
                                                                                    <asp:Label ID="lblCriaVend" runat="server" CssClass="titulo">Vendedor Raza/Criador:</asp:Label>
                                                                                    &nbsp;</td>
                                                                                <td>
                                                                                    <uc1:CLIE ID="usrCriaVend" runat="server" AceptaNull="False" Ancho="800" Copropiedad="True" Criador="true" FilDocuNume="True" MuestraDesc="False" Saltos="1,2" Tabla="Criadores" />
                                                                                </td>
                                                                                <td valign="top">
                                                                                    <asp:Button ID="btnCriaCopropiedadVend" runat="server" CausesValidation="False" CssClass="boton" Style="Z-INDEX: 0" Text="Copropiedad" Width="90px" />
                                                                                </td>
                                                                            </tr>--%>
                                                                        <%--                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></img></td>
                                                                            </tr>--%>
                                                                    <tr>
                                                                        <%--                                                                                <td align="right" background="imagenes/formfdofields.jpg">
                                                                                    <asp:Label ID="lblProducto" runat="server" CssClass="titulo">Vendedor Raza/Criador:</asp:Label>
                                                                                    &nbsp;</td>
                                                                                <td background="imagenes/formfdofields.jpg" style="WIDTH: 80%; HEIGHT: 17px">
                                                                                    <uc1:PROH ID="usrProd" runat="server" AceptaNull="false" Ancho="800" AutoPostback="False" Copropiedad="True" EsPropietario="True" FilTipo="S" IncluirDeshabilitados="true" MuestraDesc="false" MuestraSoloRazaClieDerivr="true" Saltos="1,2" Tabla="productos" />
                                                                                </td>--%>
                                                    </td>
                                                </tr>
                                                <%-- ... --%>
                                                <%--                                                                            <tr id="Tr2" runat="server" style="DISPLAY: inline">
                                                                                <td align="right" valign="top">
                                                                                    <asp:Label ID="lblReserva" runat="server" CssClass="titulo">Reserva:&nbsp;</asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <input id="chkReservaEstock" runat="server" checked name="chkReservaEstock" onclick="chkReservaEstock_checked(this)" type="checkbox" value="">&nbsp;
                                                                                    <cc1:NumberBox ID="txtCantidadReser" runat="server" autocomplete="off" AutoPostBack="false" CssClass="cuadrotexto" Enabled="false" MaxLength="9" MaxValor="999999999" name="txtCantidadReser" Width="50px"></cc1:NumberBox>
                                                                                    &nbsp;&nbsp;
                                                                                    <asp:Label ID="lblReserCrias" runat="server" CssClass="titulo">Crias:&nbsp;</asp:Label>
                                                                                    <cc1:NumberBox ID="txtCantCriasReser" runat="server" autocomplete="off" AutoPostBack="false" CssClass="cuadrotexto" Enabled="false" MaxLength="9" MaxValor="999999999" name="txtCantidadReser" Width="50px"></cc1:NumberBox>
                                                                                    </input></td>
                                                                            </tr>--%>
                                                <%--                                                                            <tr id="rowDosisCria" runat="server" style="DISPLAY: none">
                                                                                <td colspan="2">
                                                                                    <table align="left" border="0" cellpadding="0" style="WIDTH: 100%">
                                                                                        <tr>
                                                                                            <td align="right" width="18%"></td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                                                <img height="2" src="imagenes/formdivmed.jpg" width="1"></img></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>--%>
                                                <!--	<TR>
																	<TD colSpan="3" align="center">
																		<TABLE style="MARGIN-TOP: 10px" id="Table8" class="marco" cellSpacing="0" cellPadding="0"
																			width="100%">
																			<TR>
																				<TD colSpan="3" align="left"><U>
																						<asp:Label id="lblTituAnal" runat="server" cssclass="titulo">Datos del �ltimo an�lisis</asp:Label></U></TD>
																			</TR>
																			<TR>
																				<TD noWrap align="left">
																					<asp:Label id="lblNroAnal" runat="server" cssclass="titulo">N�mero:&nbsp;</asp:Label>
																					<cc1:numberbox id="txtNroAnal" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></cc1:numberbox>
																					<asp:Label id="lblTipoAnal" runat="server" cssclass="titulo">Tipo de analisis:&nbsp;</asp:Label>
																					<CC1:TEXTBOXTAB id="txtTipoAnal" runat="server" cssclass="cuadrotexto" Width="100px" Enabled="False"
																						MaxLength="4"></CC1:TEXTBOXTAB>
																					<asp:Label id="lblResulAnal" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label>
																					<CC1:TEXTBOXTAB id="txtResulAnal" runat="server" cssclass="cuadrotexto" Width="50%" Enabled="False"
																						MaxLength="4"></CC1:TEXTBOXTAB></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>-->
                                                <%--                                                                            <tr id="rowProp" runat="server" style="DISPLAY: inline">
                                                                                <td align="right" valign="top">
                                                                                    <asp:Label ID="lblClieProp" runat="server" CssClass="titulo">Propietario Actual:&nbsp;</asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <uc1:CLIE ID="usrClieProp" runat="server" AceptaNull="true" Activo="False" FilDocuNume="True" MuestraDesc="False" Saltos="1,2" Tabla="Clientes" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="rowExpo" runat="server" style="DISPLAY: inline">
                                                                                <td align="right" valign="top"></td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr id="rowDivExpo" runat="server" style="DISPLAY: inline">
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></img></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" colspan="2" nowrap>
                                                                                    <asp:Label ID="lblTranPlan" runat="server" CssClass="titulo"><br>Se ha detectado m�s de un producto para este tr�mite.<br>Puede modificar los mismos desde la opci�n de Transferencia por Planilla.<br></asp:Label>
                                                                                </td>
                                                                            </tr>--%>
                                                                        </caption>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 100%" colspan="2">
                                    <asp:Panel ID="panRequ" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                        <table style="WIDTH: 100%" id="tabRequ" border="0" cellpadding="0" align="left">
                                            <tr>
                                                <td style="WIDTH: 100%" colspan="2">
                                                    <asp:DataGrid ID="grdRequ" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                                        HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
                                                        OnPageIndexChanged="grdRequ_Page" OnEditCommand="mEditarDatosRequ">
                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"> <img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:pointer;" /></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn Visible="False" DataField="trad_id"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
                                                                <HeaderStyle Width="40%"></HeaderStyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="_pend" HeaderText="Pendiente"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="_manu" HeaderText="Manual"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                                        </Columns>
                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                    </asp:DataGrid></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblRequRequ" runat="server" CssClass="titulo">Requisito:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <cc1:ComboBox ID="cmbRequRequ" class="combo" runat="server" CssClass="cuadrotexto" Width="80%"
                                                        NomOper="rg_requisitos_cargar" MostrarBotones="False" Filtra="true">
                                                    </cc1:ComboBox></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <asp:CheckBox ID="chkRequPend" Text="Pendiente" CssClass="titulo" runat="server" Checked="False"></asp:CheckBox></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblRequManuTit" runat="server" CssClass="titulo">Manual:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <asp:Label ID="lblRequManu" runat="server" CssClass="Desc">Si</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblRequFinaFechaTit" runat="server" CssClass="titulo">Fecha Finalizaci�n:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <asp:Label ID="lblRequFinaFecha" runat="server" CssClass="Desc"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td height="30" valign="middle" colspan="2" align="center">
                                                    <asp:Button ID="btnAltaRequ" runat="server" CssClass="boton" Width="100px" Text="Agregar Req."></asp:Button>&nbsp;
										<asp:Button ID="btnBajaRequ" runat="server" CssClass="boton" Width="100px" Text="Eliminar Req."></asp:Button>&nbsp;
										<asp:Button ID="btnModiRequ" runat="server" CssClass="boton" Width="100px" Text="Modificar Req."></asp:Button>&nbsp;
										<asp:Button ID="btnLimpRequ" runat="server" CssClass="boton" Width="100px" Text="Limpiar Req."></asp:Button></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 100%" valign="top" colspan="2" align="center">
                                    <asp:Panel ID="panDocu" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                        <table style="WIDTH: 100%" id="tabDocu" border="0" cellpadding="0" align="left">
                                            <tr>
                                                <td style="WIDTH: 100%" colspan="2" align="center">
                                                    <asp:DataGrid ID="grdDocu" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                                        HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
                                                        OnUpdateCommand="mEditarDatosDocu" OnPageIndexChanged="grdDocu_Page">
                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                <HeaderStyle Width="15px"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update"> <img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:pointer;" /></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn Visible="False" DataField="trdo_id" HeaderText="trdo_id"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="trdo_path" HeaderText="Documento">
                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="trdo_refe" HeaderText="Referencia">
                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                                        </Columns>
                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                    </asp:DataGrid></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblDocuDocu" runat="server" CssClass="titulo">Documento:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <cc1:TextBoxTab ID="txtDocuDocu" runat="server" CssClass="cuadrotexto" Width="340px" ReadOnly="True"></cc1:TextBoxTab><img style="CURSOR: hand" id="imgDelDocuDoc" onclick="javascript:mLimpiarPath('txtDocuDocu','imgDelDocuDoc');"
                                                        alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<br>
                                                    <input style="WIDTH: 340px; HEIGHT: 22px" id="filDocuDocu" name="File1" size="49" type="file"
                                                        runat="server">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblDocuObse" runat="server" CssClass="titulo">Referencia:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <cc1:TextBoxTab ID="txtDocuObse" runat="server" CssClass="cuadrotexto" Width="80%" AceptaNull="False"
                                                        Height="41px" TextMode="MultiLine"></cc1:TextBoxTab></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td height="34" valign="middle" colspan="2" align="center">
                                                    <asp:Button ID="btnAltaDocu" runat="server" CssClass="boton" Width="110px" Text="Agregar Docum."></asp:Button>&nbsp;
										<asp:Button ID="btnBajaDocu" runat="server" CssClass="boton" Width="110px" Text="Eliminar Docum."
                                            Visible="true"></asp:Button>&nbsp;
										<asp:Button ID="btnModiDocu" runat="server" CssClass="boton" Width="110px" Text="Modificar Docum."
                                            Visible="true"></asp:Button>&nbsp;
										<asp:Button ID="btnLimpDocu" runat="server" CssClass="boton" Width="110px" Text="Limpiar Docum."
                                            Visible="true"></asp:Button>&nbsp;&nbsp;
                                    <button style="WIDTH: 110px; FONT-WEIGHT: bold" id="btnDescDocu" class="boton" onclick="javascript:mDescargarDocumento('hdnDocuId','tramites_docum','txtDocuDocu');"
                                        runat="server" value="Descargar">
                                        Descargar</button></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 100%" colspan="2">
                                    <asp:Panel ID="panObse" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                        <table style="WIDTH: 100%" id="tabObse" border="0" cellpadding="0" align="left">
                                            <tr>
                                                <td style="WIDTH: 100%" colspan="2">
                                                    <asp:DataGrid ID="grdObse" runat="server" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True"
                                                        HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
                                                        OnPageIndexChanged="grdObse_Page" OnEditCommand="mEditarDatosObse">
                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                        <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"> <img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:pointer;" /></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn Visible="False" DataField="trao_id"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="trao_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" HeaderText="Fecha"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="trao_obse" HeaderText="Comentarios">
                                                                <HeaderStyle Width="50%"></HeaderStyle>
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                    </asp:DataGrid></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblObseFecha" runat="server" CssClass="titulo">Fecha:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <cc1:DateBox ID="txtObseFecha" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:DateBox></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblObseRequ" runat="server" CssClass="titulo">Requisito:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <cc1:ComboBox ID="cmbObseRequ" class="combo" runat="server" CssClass="cuadrotexto" Width="80%"
                                                        NomOper="rg_requisitos_cargar" MostrarBotones="False" Filtra="true">
                                                    </cc1:ComboBox></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
                                                    <asp:Label ID="lblObseObse" runat="server" CssClass="titulo">Comentario:</asp:Label>&nbsp;</td>
                                                <td style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
                                                    <cc1:TextBoxTab ID="txtObseObse" runat="server" CssClass="cuadrotexto" Width="80%" Height="41px"
                                                        TextMode="MultiLine"></cc1:TextBoxTab></td>
                                            </tr>
                                            <tr>
                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                            </tr>
                                            <tr>
                                                <td height="30" valign="middle" colspan="2" align="center">
                                                    <asp:Button ID="btnAltaObse" runat="server" CssClass="boton" Width="100px" Text="Agregar Com."></asp:Button>&nbsp;
										<asp:Button ID="btnBajaObse" runat="server" CssClass="boton" Width="100px" Text="Eliminar Com."></asp:Button>&nbsp;
										<asp:Button ID="btnModiObse" runat="server" CssClass="boton" Width="100px" Text="Modificar Com."></asp:Button>&nbsp;
										<asp:Button ID="btnLimpObse" runat="server" CssClass="boton" Width="100px" Text="Limpiar Com."></asp:Button></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                    </table>
                    </asp:panel>

			<div style="DISPLAY: inline" id="divgraba" runat="server">
                <asp:Panel ID="panBotones" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblBaja" runat="server" CssClass="titulo" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr style="height: 30px">
                            <td valign="middle" align="center"><a id="editar" name="editar"></a>
                                <asp:Button ID="btnAlta" runat="server" CssClass="boton" Width="80px" Text="Alta" OnClientClick="return validarAM();"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnBaja" runat="server" CssClass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnModi" runat="server" CssClass="boton" Width="80px" Text="Modificar" CausesValidation="False" OnClientClick="return validarAM();"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnLimp" runat="server" CssClass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button>&nbsp; &nbsp;
								<asp:Button ID="btnErr" runat="server" CssClass="boton" Width="80px" Text="Ver Errores" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
								<asp:Button ID="btnStock" runat="server" CssClass="boton" Width="110px" Text="Stock Semen" Visible="false"
                                    CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnCertProp" runat="server" CssClass="boton" Width="90px" Text="Cert.Porp."
                                    CausesValidation="False"></asp:Button></td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
                    <div style="DISPLAY: none" id="divproce" runat="server">
                        <asp:Panel ID="panproce" runat="server" CssClass="titulo" Width="100%">
                            <asp:Image ID="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        </asp:Panel>
                    </div>
                </td>
            </tr>
            </TBODY>
        </table>
        <!--- FIN CONTENIDO --->
        </TD>
			<td width="13" background="imagenes/recde.jpg">
                <img height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
        </TR>
			<tr>
                <td width="9">
                    <img height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recinf.jpg">
                    <img height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
                <td width="13">
                    <img height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
            </tr>
        </TBODY></TABLE>
        <!----------------- FIN RECUADRO ----------------->
        <br>
        <div style="DISPLAY: none">
            <asp:TextBox ID="lblMens" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnRazaId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnRequId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDetaId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDocuId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnObseId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnSess" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDatosPop" runat="server" AutoPostBack="True"></asp:TextBox>
            <asp:TextBox ID="hdnCompId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnCompOrig" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnVendOrig" runat="server"></asp:TextBox>
            <asp:TextBox ID="lblAltaId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnMultiProd" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnTramNro" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnMsgError" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnDatosTEPop" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnTEId" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnSexo" runat="server"></asp:TextBox>
            <asp:TextBox ID="hdnProdId" runat="server"></asp:TextBox>

            <asp:HiddenField ID="hdnEstadoId" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnOperacion" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPorcCompra" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnTotalPorcVenta" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnTotalPorcCompra" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnTram_vc_id" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnTram_vc_clie_id" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnTram_vc_cria_id" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnTram_vc_porcentaje" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnProductoId" runat="server" />
            <asp:HiddenField ID="hdnVendedorId" runat="server" />
            <asp:HiddenField ID="hdnCompradorId" runat="server" />
            <asp:HiddenField ID="hdnGridVendedoresCount" runat="server" />
            <asp:HiddenField ID="hdnGridCompradoresCount" runat="server" />
            <asp:HiddenField ID="hdnSumaPorcentajeCompra" runat="server" />
            <asp:Button ID="btnLimpiarGrids" runat="server" Text="Button" />
            <asp:Button ID="btnBuscarPropietariosQueVenden" runat="server" Text="Button" />
            <asp:HiddenField ID="hdnFechaTransferencia" runat="server" />
            <asp:HiddenField ID="hdnIdTramiteReserva" runat="server" />
        </div>

        <script type="text/javascript" language="JavaScript">
            function mPorcPeti() {
                document.all("divgraba").style.display = "none";
                document.all("divproce").style.display = "inline";
            }
            if (document.all('lblAltaId') != null && document.all('lblAltaId').value != '') {
                alert('Se ha asignado el nro de tramite ' + document.all('lblAltaId').value);
                document.all('lblAltaId').value = '';
            }
            if ('<%=mintTtraId%>' == '11' || '<%=mintTtraId%>' == '30' || '<%=mintTtraId%>' == '31') {
                if (document.all('usrProd:usrProducto:txtSexoId') != null)
                    document.all('usrProd:usrProducto:txtSexoId').value = '1';
            }

            function gValDateMio(txtstr) {
                TF = false
                if (txtstr.value != '') {
                    if (isdate(txtstr.value) == 'N') {
                        TF = false;
                    }
                    else {
                        TF = true;
                    };
                    if (TF == false) {
                        alert('Por favor, ingrese el formato "dd/mm/aaaa" ');
                        txtstr.value = ''
                        txtstr.focus();
                        txtstr.select();
                        return true;
                    }
                    else {
                        txtstr.value = isdate(txtstr.value);
                    };
                }
                return false;
            }
        </script>
    </form>

</body>


</html>

