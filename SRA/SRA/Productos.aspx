<%@ Reference Control="~/controles/usrcriador.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Migrated_Productos" CodeFile="Productos.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CRIA" Src="controles/usrCriador.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Productos</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var gstrCria = '';   
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
		
		
		
		function mBuscarInscribeSRA()
		{
		
			if (document.all("txtcmbRazaFil")!=null )
				{
				    if (!isPostBack())
					{
			  	        var strFiltro = "@raza_id = " + document.all("cmbRazaFil").value;
					    var strRet = LeerCamposXML("razas", strFiltro, "raza_InscribeSRA");
					    return strRet;
					}
			}
		}
			
		

		function mRazaInscribeSRA()
		{
		if (document.all("txtcmbRazaFil").value != '')
				{
					if (mBuscarInscribeSRA()!="1")
					{
						 // Vigente y solo puede ser extranjero
							document.all("cmbEstaFil").disabled = false;
							document.all("cmbNaciFil").disabled = false;
						 	document.all("cmbEstaFil").value = "73";
						 	document.all("cmbNaciFil").value = "E";
						 							 	
					 		document.all("cmbEstaFil").disabled = true;
				    	 	document.all("cmbNaciFil").disabled = true;
			  		}
					else
					{
						 document.all("cmbEstaFil").value = "";
						 document.all("cmbNaciFil").value = "N";
						 document.all("cmbEstaFil").disabled = false;
						 document.all("cmbNaciFil").disabled = false;
					}
						
				}
			
		}	
		
	
			
		function mSetearRazas()
		{
			if (document.all("txtcmbRazaFil")!=null && document.all("txtusrMadreFil:cmbProdRaza")!= null)
			{
				
				document.all("txtusrMadreFil:cmbProdRaza").value = document.all("txtcmbRazaFil").value;
				mBuscarIdXCodi("usrMadreFil:cmbProdRaza", "razas_cargar", "Id");
				document.all("txtusrPadreFil:cmbProdRaza").value = document.all("txtcmbRazaFil").value;
				mBuscarIdXCodi("usrPadreFil:cmbProdRaza", "razas_cargar", "Id");
			}
			
			// Manejo del combo Registro Tipo
			if (document.all("txtcmbRazaFil")!=null)
			{
				if (document.all("cmbRazaFil").value != "")
				{
					if (!isPostBack())
					{
						LoadComboXML("rg_registros_tipos_productos_cargar", document.all("cmbRazaFil").value, "cmbRegiTipoFil", "N");
					}

					if (document.all("cmbRazaFil").value != document.all("hdnRazaAnteriorId").value)
					{
						document.all("hdnRazaAnteriorId").value = document.all("cmbRazaFil").value
						LoadComboXML("rg_registros_tipos_productos_cargar", document.all("cmbRazaFil").value, "cmbRegiTipoFil", "N");
					}
					if (document.all("cmbRazaFil").value != "")
						document.all("cmbRegiTipoFil").disabled = false;
					else
						document.all("cmbRegiTipoFil").disabled = true;
				}
			}
		}
		function isPostBack()
		{
			var postback = "<%= Page.IsPostBack%>";
			if (postback == "True")
				return true;
			else
				return false;
		}
		
		function mPropietarios()
	   	{
	   		gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Propietarios&tabla=Prop_consul&Rpt=L&reporte=ListadoPropietarios&filtros='"+ document.all("hdnId").value+"'", 7, "600","300","50","100");
		}
				
		function mSetearMotivo()
		{
			if (document.all("txtFalleFecha")!=null)
			{
				//document.all("cmbBaja").disabled = (document.all("txtFalleFecha").value == "")
				//if (document.all("cmbBaja").disabled)
				//	document.all("cmbBaja").selectedIndex = 0;
			}
		}
		function btnTramites_click()
		{
			gAbrirVentanas("consulta_Tramites_pop.aspx?pTipo=1&prdt_id=" + document.all("hdnId").value, 4, "900", "500", "70", "100");  	
		}
		function btnServicios_click()
		{
			gAbrirVentanas("ConsultaFiltros.aspx?EsConsul=1&titulo=SERVICIOS&tabla=productos_servicios&prdt_sexo="+document.all('cmbSexo').value+"&Rpt=L&filtros=" + document.all("hdnId").value, 2, "700","500","100","50");
		}
		function btnParejas_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PAREJAS&tabla=productos_parejas&Rpt=L&reporte=ListadoParejas&filtros=" + document.all("hdnId").value, 3, "700","400","50","50");
		}
		function btnPremios_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PREMIOS&tabla=productos_premios&Rpt=L&reporte=ListadoPremios&filtros=" + document.all("hdnId").value, 4, "600","400","100","50");
		}
		function btnEstados_click()
		{
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=ESTADOS&tabla=productos_estados&filtros=" + document.all("hdnId").value, 5, "600","400","100","50");
		}
		
		
		function mPropCopropiedad()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Copropiedad&tabla=criadores_por_cliente_raza&Rpt=L&filtros="+ document.all("hdnPropietario").value, 7, "900","300","50","100");
		}
		
		function btnAuditoria_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=RELACIONES&tabla=productos_relaciones_auditoria&Rpt=L&filtros=" + document.all("hdnId").value, 3, "700","400","50","50");
		}	
		
		
		function btnEmbrionesStock_click()
		{
		
			var lstrProdId =document.all("hdnId").value;
			
			gAbrirVentanas("Reportes/SaldoEmbrionesStock.aspx?ProdID=" + lstrProdId , 1, "750","550","20","40");
		}
		
		function btnSemenStock_click()
		{
			var lstrProdId =document.all("hdnId").value;
		
			gAbrirVentanas("Reportes/SaldoSemenStock.aspx?ProdID=" + lstrProdId , 1, "750","550","20","40");
	
		}
		
		function btnPerform_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PERFORMANCE&tabla=perform_carne&filtros=" + document.all("hdnId").value +"&reporte=PerformCarne&Rpt=L", 6, "600","400","100","50");
		}
		function btnPedigree_click()
		{
		
			gAbrirVentanas("pedigree.aspx?prod=" + document.all("hdnId").value + "&raza=" + document.all("cmbRaza").value + "&codi=" + document.all("txtcmbRaza").value+"&SoloConsulta="+document.all("hdnSoloConsulta").value + "&mensajeConsulta="+document.all("hdnMensajeConsulta").value, 7, "","","","",true);
		}
		
		function btntatu_click()
		{
		  if (document.all("hdnBloqueoCambioTatuaje").value=="false")
		  {
			gAbrirVentanas("cambioDeTatuaje.aspx?prdt_id=" + document.all("hdnId").value, 4, "750", "600", "100", "100");  
		  }
		  else
		  {
			alert('Ingreso por opcion de Solo Consulta de productos y/o No se permite modificar el Tatuaje');
		  }
	     	

		}
		
		function mSelecSexo()
		{
			if (document.all("cmbSexoFil") != null)
			{
				if (document.all("cmbSexoFil").value != "")
					{
						if (document.all("cmbSexoFil").value == "1") //Macho
							document.all("trDadorFil").style.display = "inline";
						else
							document.all("trDadorFil").style.display = "none";
					}
				else
					document.all("trDadorFil").style.display = "none";
			}
		}

		
																
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeDesdeFil')!=null)
				{
					if (strRet!='')
					{
	 					//document.all('lblNumeFil').innerHTML = strRet+':';
	 					document.all('lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					//document.all('lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
 			
 			
 			
		}
		function mAsociaciones()
	   	{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Asociaciones&tabla=productos_numeros&filtros='"+ document.all("hdnId").value +"'" + "&tituprod=" + document.all("txtNomb").value, 7, "600","300","50","100");
		}
		
		/*function mRelaciones()
	   	{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Relaciones&tabla=productos_relaciones&filtros='"+ document.all("hdnId").value +"'", 7, "600","300","50","100");
		}*/
		function mRelaciones(pTipo)
	   	{
	   	
	   		var strConsulta=document.all("hdnSoloConsulta").value.substring(0,1) 
	   	gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Hijos&tabla=PR&prdt_sexo="+document.all('cmbSexo').value+"&prdt_raza="+document.all('cmbRaza').value +"&SC="+strConsulta+ "&Rpt=L&reporte=ListadoHijos&filtros="+ document.all("hdnId").value + ",'" + pTipo + "'", 7, "900","400","50","100");
		//GSZ 24/04/2015	gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Hijos&tabla=PR&prdt_sexo=0&prdt_raza=59&SC=f&Rpt=L&reporte=ListadoHijos&filtros=3618664", 7, "900","400","50","100");
		}
		function mCertificado()
	   	{
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Errores&tabla=productos_errores&filtros="+ document.all("hdnId").value, 7, "600","300","50","100");
		}	    
		function mErrores()
	   	{
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Errores&tabla=productos_errores&Rpt=L&reporte=ListadoErrores&filtros="+ document.all("hdnId").value, 7, "600","300","50","100");
			var lstrProc = '<%=mintProce%>';
		    var lstrId = '<%=mintPdinId%>';	
		    var strConsulta=document.all("hdnSoloConsulta").value.substring(0,1)
		    var strExtranjero=document.all("hdnExtranjero").value.substring(0,1)
		    		
		    /*cuando son animales en  pie hay que pasar el trpr_id tramprodid*/
			gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce="+lstrProc+"&id=" + lstrId +"&SC="+strConsulta+"&EsExtranjero="+strExtranjero, 1, "750","400","20","40");
		}
	    
	    function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function mCargarPelajes(pRaza)
		{
		    var sFiltro = pRaza.value;
		    mNumeDenoConsul(pRaza,false);
		    if (sFiltro!='')
		    {
				LoadComboXML("rg_registros_tipos_cargar", sFiltro, "cmbRegiTipo", "N");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",1", "cmbPelaA", "S");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",2", "cmbPelaB", "S");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",3", "cmbPelaC", "S");
				LoadComboXML("rg_pelajes_cargar", sFiltro+",4", "cmbPelaD", "S");
				LoadComboXML("rg_variedades_cargar", sFiltro, "cmbVari", "S");
			}
			else
			{
				document.all('cmbRegiTipo').selectedIndex = 0;
				document.all('cmbPelaA').selectedIndex = 0;
				document.all('cmbPelaB').selectedIndex = 0;
				document.all('cmbPelaC').selectedIndex = 0;
				document.all('cmbPelaD').selectedIndex = 0;
				document.all('cmbVari').selectedIndex = 0;
			}
		}
		
		function CambiarExp()
		{
			if(document.all("spExp").innerText=="+")
			{
				document.all("panBuscAvan").style.display = "inline";
				document.all("spExp").innerText="-";
			}
			
			else
			{
				document.all("panBuscAvan").style.display = "none";
				document.all("spExp").innerText="+";
			}

		}
		
		function isNumber(evt)
		{
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}

		function isDecimalNumber(evt)
		{
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode == 46)
				return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}
		
		function mSetearHastaHBA()
		{
			if (document.all('txtNumeDesdeFil')!=null)
				document.all('txtNumeHastaFil').value = document.all('txtNumeDesdeFil').value;
		}
		function mSetearHastaRP()
		{
			if (document.all('txtRPNumeDesdeFil')!=null)
				document.all('txtRPNumeHastaFil').value = document.all('txtRPNumeDesdeFil').value;
		}
		function mSetearHastaCondicional()
		{
			if (document.all('txtCondicionalDesdeFil')!=null)
				document.all('txtCondicionalHastaFil').value = document.all('txtCondicionalDesdeFil').value;
		}
		function HaceRPNumero(objOrigen, objDestino , evt) 
		{
			if(document.all(objOrigen).value == '')
			{
				document.all(objDestino).value ='';
			}
							   
			var charCode = (evt.which) ? evt.which : event.keyCode

			if (charCode > 47 && charCode < 58)
			{
				document.all(objDestino).value = document.all(objDestino).value + String.fromCharCode(charCode);
			}

			return true;
		}
		</script>
	</HEAD>
	<BODY class="pagina" onunload="gCerrarVentanas();" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="top" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Productos</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" colSpan="3" noWrap><asp:panel id="panFiltros" runat="server" cssclass="titulo" width="100%" BorderWidth="1px"
											BorderStyle="none">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD style="WIDTH: 100%">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26"><CC1:BOTONIMAGEN id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26"><CC1:BOTONIMAGEN id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
																		ImageUrl="imagenes/limpiar.jpg"></CC1:BOTONIMAGEN></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%">
														<TABLE class="FdoFld" border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																<TD style="HEIGHT: 100%">
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TBODY>
																			<TR>
																				<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 10px" align="right"><asp:label id="lblRazaFil" runat="server" cssclass="titulo">Raza:&nbsp;</asp:label></TD>
																				<TD>
																					<TABLE border="0" cellSpacing="0" cellPadding="0">
																						<TR>
																							<TD><cc1:combobox id="cmbRazaFil" class="combo" runat="server" cssclass="cuadrotexto" filtra="true"
																									MostrarBotones="False" NomOper="razas_cargar" Height="20px" onchange="javascript:mNumeDenoConsul(this,true);mSetearRazas();Combo_change(this);"
																									AceptaNull="False" Width="180px"></cc1:combobox></TD>
																							<TD width="50" align="right"><asp:label id="lblSexoFil" runat="server" cssclass="titulo">&nbsp;&nbsp;Sexo:&nbsp;</asp:label></TD>
																							<TD><cc1:combobox id="cmbSexoFil" class="combo" runat="server" onchange="mSelecSexo();" AceptaNull="True"
																									Width="100px"><asp:ListItem Selected="True" value="">(Todos)</asp:ListItem><asp:ListItem Value="0">Hembra</asp:ListItem><asp:ListItem Value="1">Macho</asp:ListItem></cc1:combobox>&nbsp;</TD>
																							<TD width="50" align="right"><asp:label id="lblOrdenarPor" runat="server" cssclass="titulo" Width="90px" DESIGNTIMEDRAGDROP="2074">Ordenar por:&nbsp;</asp:label></TD>
																							<TD><cc1:combobox id="cmbOrdenarPor" class="combo" runat="server" NomOper="rg_registros_tipos_productos_cargar"
																									AceptaNull="False" Width="90px" DESIGNTIMEDRAGDROP="2076"><asp:ListItem Selected="True" value="0">HBA</asp:ListItem><asp:ListItem Value="1">RP Num�rico</asp:ListItem><asp:ListItem Value="2">Criador</asp:ListItem><asp:ListItem Value="3">Propietario</asp:ListItem><asp:ListItem Value="4">Condicional</asp:ListItem><asp:ListItem Value="5">Asoc. - N� ext.</asp:ListItem></cc1:combobox></TD>
																							<TD width="50" align="right">&nbsp;
																								<asp:radiobutton id="rbtnAsc" runat="server" cssclass="titulo" Width="80%" checked="True" GroupName="RadioGroup1"
																									Text="Ascendente"></asp:radiobutton></TD>
																							<TD width="50" align="right">&nbsp;
																								<asp:radiobutton id="rbtnDesc" runat="server" cssclass="titulo" Width="80%" checked="False" GroupName="RadioGroup1"
																									Text="Descendente"></asp:radiobutton></TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right"><asp:label id="lblCondicionalDesdeFil" runat="server" cssclass="titulo">Condicional Desde:&nbsp;</asp:label></TD>
																				<TD><CC1:TEXTBOXTAB id="txtCondicionalDesdeFil" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																						onchange="mSetearHastaCondicional();" Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																					<asp:label id="lblCondicionalHastaFil" runat="server" cssclass="titulo">Condicional Hasta:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtCondicionalHastaFil" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																						Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right"><asp:label id="lblRegiTipoFil" runat="server" cssclass="titulo" DESIGNTIMEDRAGDROP="2074">Registro:&nbsp;</asp:label></TD>
																				<TD><cc1:combobox id="cmbRegiTipoFil" class="combo" runat="server" NomOper="rg_registros_tipos_productos_cargar"
																						AceptaNull="False" Width="77px" DESIGNTIMEDRAGDROP="2076" enabled="true"></cc1:combobox>&nbsp;
																					<asp:label id="lblNumeFil" runat="server" cssclass="titulo" Visible="False"> Nro.:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="false"
																						Visible="False"></CC1:TEXTBOXTAB>&nbsp;
																					<asp:label id="lblNumeDesdeFil" runat="server" cssclass="titulo">Nro. Desde:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtNumeDesdeFil" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																						onchange="mSetearHastaHBA();" Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																					<asp:label id="lblNumeHastaFil" runat="server" cssclass="titulo">Nro. Hasta:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtNumeHastaFil" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																						Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right"><asp:label id="lblAsoNumeFil" runat="server" cssclass="titulo">Asociaci�n:&nbsp;</asp:label></TD>
																				<TD>
																					<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD><cc1:combobox id="cmbAsocFil" class="combo" runat="server" cssclass="cuadrotexto" filtra="True"
																									NomOper="asociaciones_cargar" onchange="Combo_change(this)" AceptaNull="False" Width="400"
																									mostrarbotones="False"></cc1:combobox></TD>
																							<TD><asp:label id="lblAsocNumeFil" runat="server" cssclass="titulo">Nro. Extranjero:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtAsoNumeFil" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																									Width="80px" Obligatorio="false" valign="left"></CC1:TEXTBOXTAB><asp:label id="lblRPExtranjeroFil" runat="server" cssclass="titulo">RP Extranjero:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtRPExtranjeroFil" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"
																									valign="left"></CC1:TEXTBOXTAB></TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR id="trDadorFil" runat="server">
																				<TD style="HEIGHT: 10px" align="right"><asp:label id="lblNroSRAFil" runat="server" cssclass="titulo">Nro.Dador SRA:&nbsp;</asp:label></TD>
																				<TD><cc1:numberbox id="txtDadorNroSRAFil" runat="server" cssclass="cuadrotexto" Width="90px" Obligatorio="false"
																						esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox>&nbsp;
																					<asp:label id="lblDadorNroSenasaFil" runat="server" cssclass="titulo">Nro.Dador Senasa:&nbsp;</asp:label><cc1:textboxtab id="txtDadorNroSenasaFil" runat="server" cssclass="cuadrotexto" Width="90px" Obligatorio="false"></cc1:textboxtab>&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right"><asp:label id="lblRPNumeFil" runat="server" cssclass="titulo">RP:&nbsp;</asp:label></TD>
																				<TD><CC1:TEXTBOXTAB id="txtRPFil" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																					<asp:label id="lblRPDesdeFil" runat="server" cssclass="titulo">RP Num�rico Desde:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtRPNumeDesdeFil" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																						onchange="mSetearHastaRP();" Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																					<asp:label id="lblRPHastaFil" runat="server" cssclass="titulo">RP Num�rico Hasta:&nbsp;</asp:label><CC1:TEXTBOXTAB id="txtRPNumeHastaFil" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																						Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
																				</TD>
																</TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right"><asp:label id="lblCriaFil" runat="server" cssclass="titulo">Criador:&nbsp;</asp:label></TD>
																<TD><UC1:CRIA id="usrCriaFil" runat="server" MostrarBotones="False" AceptaNull="false" AutoPostBack="False"
																		CampoVal="Criador" Criador="True" FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True"
																		Saltos="1,2" Tabla="Criadores"></UC1:CRIA></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right"><asp:label id="lblPropFil" runat="server" cssclass="titulo">Propietario:&nbsp;</asp:label></TD>
																<TD><UC1:CRIA id="usrPropCriaFil" runat="server" MostrarBotones="False" AceptaNull="false" AutoPostBack="False"
																		CampoVal="Criador" Criador="True" FilDocuNume="True" MuestraDesc="False" FilTipo="S" FilSociNume="True"
																		Saltos="1,2" Tabla="Criadores"></UC1:CRIA></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right"><asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:label></TD>
																<TD><CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right"><asp:label id="lblApodoFil" runat="server" cssclass="titulo">Apodo:&nbsp;</asp:label></TD>
																<TD><CC1:TEXTBOXTAB id="txtApodoFil" runat="server" cssclass="cuadrotexto" Width="400px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
															</TR>
															<tr>
																<TD style="HEIGHT: 10px" align="right"><asp:label id="Label3" runat="server" cssclass="titulo">Castrado:&nbsp;</asp:label></TD>
																<TD><cc1:combobox id="cmbCastradoFil" class="combo" runat="server" Width="113px"><asp:ListItem Selected="True">(Todos)</asp:ListItem><asp:ListItem Value="1">Si</asp:ListItem><asp:ListItem Value="0">No</asp:ListItem></cc1:combobox>
																<TD></TD>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 19px" align="right"><asp:label id="lbldescorneFil" runat="server" cssclass="titulo">Descorne:&nbsp;</asp:label></TD>
																<TD style="HEIGHT: 19px"><cc1:combobox id="cmbDescorneFil" class="combo" runat="server" Width="113px"><asp:ListItem Selected="True">(Todos)</asp:ListItem><asp:ListItem Value="1">Si</asp:ListItem><asp:ListItem Value="0">No</asp:ListItem></cc1:combobox>
																<TD style="HEIGHT: 19px"></TD>
															<TR>
																<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																	align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right"></TD>
																<TD><asp:checkbox id="chkBusc" Text="Buscar en..." Runat="server" CssClass="titulo"></asp:checkbox>&nbsp;&nbsp;&nbsp;</TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right"><asp:label id="lblNaciFechaFil" runat="server" cssclass="titulo">Fecha Nacimiento:&nbsp;</asp:label></TD>
																<TD><cc1:datebox id="txtNaciFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:datebox><asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label><cc1:datebox id="txtNaciFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:datebox></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right"><asp:label id="lblInscFechaFil" runat="server" cssclass="titulo">Fecha Inscripci�n:&nbsp;</asp:label></TD>
																<TD><cc1:datebox id="txtInscFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:datebox><asp:label id="lblFechaInscHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label><cc1:datebox id="txtInscFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:datebox></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
															<TR>
																<TD align="right"><asp:label id="lblLaboNume1Fil" runat="server" cssclass="titulo">N� An�lisis:&nbsp;</asp:label></TD>
																<TD><cc1:numberbox id="txtLaboNumeFil" runat="server" cssclass="cuadrotexto" Width="100px" esdecimal="False"
																		MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right"><asp:label id="lblTramNumeFil" runat="server" cssclass="titulo">N� Tr�mite:&nbsp;</asp:label></TD>
																<TD><cc1:numberbox id="txtTramNumeFil" runat="server" cssclass="cuadrotexto" Width="160px" esdecimal="False"
																		MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right"><asp:label id="lblCuigFil" runat="server" cssclass="titulo">Nro.Oficial:&nbsp;</asp:label></TD>
																<TD><CC1:TEXTBOXTAB id="txtCuigFil" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD align="right"><asp:label id="lblCtroFil" runat="server" cssclass="titulo" Visible="False">Centro de Transplante:&nbsp;</asp:label></TD>
																<TD><cc1:combobox id="cmbCtroFil" class="combo" runat="server" AceptaNull="False" Width="200px" Visible="False"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right"><asp:label id="lblCtrolFactuNumeFil" runat="server" cssclass="titulo" Visible="False">N� Ctrol Fact.:&nbsp;</asp:label></TD>
																<TD><cc1:numberbox id="txtCtrolFactuNumeFil" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"
																		Visible="False" esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right"><asp:label id="lblEstaFil" runat="server" cssclass="titulo">Estado:&nbsp;</asp:label></TD>
																<TD><cc1:combobox id="cmbEstaFil" class="combo" runat="server" AceptaNull="False" Width="140px" AutoPostBack="True"></cc1:combobox>&nbsp;
																	<asp:checkbox id="chkBaja" Text="Incluir Dados de Baja" Runat="server" CssClass="titulo" Checked="True"></asp:checkbox></TD>
															</TR>
															<TR>
																<TD align="right"><asp:label id="lblNaciFil" runat="server" cssclass="titulo">Nacionalidad:&nbsp;</asp:label></TD>
																<TD><cc1:combobox id="cmbNaciFil" class="combo" runat="server" AceptaNull="False" Width="140px"><asp:ListItem Value="N">Nacional</asp:ListItem><asp:ListItem Value="E">Extranjero</asp:ListItem></cc1:combobox></TD>
															</TR>
															<TR>
																<TD colSpan="2">
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR id="divBusqAvanzada" runat="server" colSpan="2">
																			<TD align="right"><SPAN style="FONT-FAMILY: Verdana; HEIGHT: 4px; COLOR: black; FONT-SIZE: 14px; CURSOR: hand; FONT-WEIGHT: bold"
																					id="spExp" onclick="CambiarExp()">+</SPAN></TD>
																			<TD width="100%"><asp:label id="lblExp" runat="server" cssclass="titulo">B�squeda avanzada</asp:label></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2">
																				<DIV style="Z-INDEX: 101; WIDTH: 100%; DISPLAY: none" id="panBuscAvan" class="panelediciontransp">
																					<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
																						<TR>
																							<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
																						</TR>
																						<TR>
																							<TD style="HEIGHT: 10px" align="right"><asp:label id="lblObservacionFil" runat="server" cssclass="titulo">Observaci�n:</asp:label>&nbsp;</TD>
																							<TD><CC1:TEXTBOXTAB id="txtObservacionFil" runat="server" cssclass="cuadrotexto" Height="46px" Width="80%"
																									TextMode="MultiLine" EnterPorTAb="False"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 15%" align="right"><asp:label id="lblPadreFil" runat="server" cssclass="titulo">Padre:</asp:label>&nbsp;</TD>
																							<TD><UC1:PROD id="usrPadreFil" runat="server" AceptaNull="false" AutoPostBack="False" FilDocuNume="True"
																									MuestraDesc="True" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" Ancho="800"></UC1:PROD></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right"><asp:label id="lblMadreFil" runat="server" cssclass="titulo">Madre:</asp:label>&nbsp;</TD>
																							<TD><UC1:PROD id="usrMadreFil" runat="server" AceptaNull="false" AutoPostBack="False" FilDocuNume="True"
																									MuestraDesc="True" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos" Ancho="800"></UC1:PROD></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right"><asp:label id="lblReceFil" runat="server" cssclass="titulo" Visible="False">Receptora:</asp:label>&nbsp;</TD>
																							<TD><UC1:PROD id="usrReceFil" runat="server" AceptaNull="false" Visible="False" AutoPostBack="False"
																									FilDocuNume="True" MuestraDesc="True" FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="productos"
																									Ancho="800"></UC1:PROD></TD>
																						</TR>
																						<TR>
																							<TD style="HEIGHT: 10px" colSpan="2" align="right"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE></TD>
								</TR>
							</TBODY>
						</TABLE>
						</asp:panel></td>
				</tr>
				<TR>
					<TD vAlign="top" colSpan="3" align="right"><BR>
						<asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
							HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
							OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mSeleccionarProducto">
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
							<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="15px"></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton id="Linkbutton3" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
											<img src='images/sele.gif' border="0" alt="Seleccionar Producto" style="cursor:hand;" />
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="10px"></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
											<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="false" DataField="prdt_id" ReadOnly="True" HeaderText="Id Producto"></asp:BoundColumn>
								<asp:BoundColumn Visible="true" DataField="_Baja" ReadOnly="True" HeaderText="" ItemStyle-ForeColor="#ff0000"
									ItemStyle-Font-Bold="True" ItemStyle-Font-Size="12"></asp:BoundColumn>
								<asp:BoundColumn Visible="true" DataField="_Nopresent" ReadOnly="True" HeaderText="" ItemStyle-ForeColor="#2E64FE"
									ItemStyle-Font-Bold="True" ItemStyle-Font-Size="12"></asp:BoundColumn>
								<asp:BoundColumn Visible="true" DataField="_Irregularidades" ReadOnly="True" HeaderText="" ItemStyle-ForeColor="#B40486"
									ItemStyle-Font-Bold="True" ItemStyle-Font-Size="8"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="_raza" HeaderText="Raza">
									<HeaderStyle Width="50px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="_raza_codi" HeaderText="Raza"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="_sexo" ReadOnly="True" HeaderText="Sexo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="prdt_nomb" ReadOnly="True" HeaderText="Nombre">
									<HeaderStyle Width="30%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="_asoc" ReadOnly="True" HeaderText="Asoc."></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="_nume" ReadOnly="True" HeaderText="N.Extr."></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="_numelab" ReadOnly="True" HeaderText="N.Extr.Lab"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="prdt_rp_extr" ReadOnly="True" HeaderText="RP.Extr."></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="_regi" ReadOnly="True" HeaderText="Registro"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="prdt_px" ReadOnly="True" HeaderText=""></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="prdt_sra_nume" ReadOnly="True" HeaderText="Nro."></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="_descProd" ReadOnly="True" HeaderText="Descripci�n"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="prdt_rp" ReadOnly="True" HeaderText="RP"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="_propietario" ReadOnly="True" HeaderText="Propietario"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="prdt_naci_fecha" ReadOnly="True" HeaderText="F.Nacim."
									DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
								<asp:BoundColumn Visible="True" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
								<asp:BoundColumn Visible="false" DataField="prdt_raza_id" HeaderText="raza_id"></asp:BoundColumn>
								<asp:BoundColumn Visible="false" DataField="_raza_codi" HeaderText="raza_codi"></asp:BoundColumn>
								<asp:BoundColumn Visible="false" DataField="prdt_sexo" HeaderText="SexoId"></asp:BoundColumn>
								<asp:BoundColumn Visible="true" DataField="prdt_castrado" HeaderText="Castrado"></asp:BoundColumn>
								<asp:BoundColumn Visible="true" DataField="prdt_descorne" HeaderText="Descorne"></asp:BoundColumn>
								<asp:BoundColumn Visible="true" DataField="_Prefijo" ReadOnly="True" HeaderText="Prefijo"></asp:BoundColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD vAlign="middle" align="right">
						<TABLE id="tabLinks" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
							<TR>
								<TD width="1"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="28"></TD>
								<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="28"></TD>
								<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="28"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
										cssclass="solapa" Height="21px" Width="80px" CausesValidation="False"> Producto </asp:linkbutton></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"></TD>
								<TD width="27"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkAso" runat="server" cssclass="solapa"
										Height="21px" Width="70px" CausesValidation="False"> Asociaciones </asp:linkbutton></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"></TD>
								<TD width="27"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDocumentos" runat="server"
										cssclass="solapa" Height="21px" Width="70px" CausesValidation="False"> Diagramas </asp:linkbutton></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"></TD>
								<TD width="27"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkObse" runat="server" cssclass="solapa"
										Height="21px" Width="70px" CausesValidation="False"> Observaciones </asp:linkbutton></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkOtrosDatos" runat="server"
										cssclass="solapa" Height="21px" Width="75px" CausesValidation="False"> Otros Datos </asp:linkbutton></TD>
								<TD width="27"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"></TD>
								<TD background="imagenes/tab_fondo.bmp" width="1"><asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkPesadasDeps" runat="server"
										cssclass="solapa" Height="21px" Width="100px" CausesValidation="False"> Pesadas y Deps </asp:linkbutton></TD>
								<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="28"></TD>
							</TR>
						</TABLE>
					</TD>
					<TD align="right"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD colSpan="2" align="center">
						<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderWidth="1px" BorderStyle="Solid"
								Height="116px" Visible="False">
								<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
									align="left">
									<TR>
										<TD style="HEIGHT: 10px" align="left">
											<asp:label id="lblTitu" runat="server" cssclass="titulo"></asp:label>
											<asp:label id="lblFactura" runat="server" cssclass="titulo" ForeColor="#2E64FE"></asp:label></TD>
										<TD vAlign="top" align="right">
											<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
												ToolTip="Cerrar"></asp:imagebutton></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 10px" align="left">
											<asp:Label style="Z-INDEX: 0" id="lblMensaje" runat="server" cssclass="titulo" ForeColor="Red"
												Width="1080px"></asp:Label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3" align="center">
											<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
												<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 25%" colSpan="4" align="left">
															<TABLE border="0" cellSpacing="0" cellPadding="0">
																<TR>
																	<TD style="HEIGHT: 10px" noWrap align="right">&nbsp;
																		<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																	<TD noWrap>
																		<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="200px" AceptaNull="True"
																			onchange="javascript:mCargarPelajes(this);" Height="20px" NomOper="razas_cargar" MostrarBotones="False"
																			filtra="true" Obligatorio="true" Enabled="False"></cc1:combobox></TD>
																	<TD noWrap>&nbsp;
																		<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:&nbsp;</asp:Label></TD>
																	<TD noWrap>
																		<cc1:combobox id="cmbSexo" class="combo" runat="server" Width="88px" AceptaNull="true" Obligatorio="True"
																			Enabled="False"><asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem><asp:ListItem Value="0">Hembra</asp:ListItem><asp:ListItem Value="1">Macho</asp:ListItem></cc1:combobox></TD>
																	<TD noWrap>&nbsp;&nbsp;
																		<asp:Label id="lblNdad" runat="server" cssclass="titulo" ForeColor="Red" Visible="True"></asp:Label>&nbsp;&nbsp;
																		<asp:Label id="lblEmbargado" runat="server" cssclass="titulo" ForeColor="Red" Visible="True"></asp:Label></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" noWrap align="right">
															<asp:Label id="lblRegiTipo" runat="server" cssclass="titulo" DESIGNTIMEDRAGDROP="2074">Registro:&nbsp;</asp:Label></TD>
														<TD>
															<cc1:combobox id="cmbRegiTipo" class="combo" runat="server" Width="77px" NomOper="rg_registros_tipos_cargar"
																DESIGNTIMEDRAGDROP="2076"></cc1:combobox>
															<asp:Label id="lblSraNume" runat="server" cssclass="titulo">Nro.:&nbsp;</asp:Label>
															<cc1:numberbox id="txtSraNume" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="false"
																MaxLength="12" MaxValor="9999999999999" esdecimal="False" Enabled="False"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblCondicional" runat="server" cssclass="titulo" DESIGNTIMEDRAGDROP="2074">Condicional:&nbsp;</asp:Label></TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtCondicional" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																Width="100px" Obligatorio="false" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">
															<asp:label id="lblAsoNume" runat="server" cssclass="titulo">Asociaci�n:&nbsp;</asp:label></TD>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD>
																		<cc1:combobox id="cmbAsociacion" class="combo" runat="server" cssclass="cuadrotexto" Width="400"
																			AceptaNull="False" onchange="Combo_change(this)" NomOper="asociaciones_cargar" filtra="True"
																			mostrarbotones="False" Enabled="False"></cc1:combobox></TD>
																	<TD align="left">
																		<asp:Label id="lblAsociacionNume" runat="server" cssclass="titulo">Nro. Extranjero:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtAsoNume" onkeypress="return isNumber(event)" runat="server" cssclass="cuadrotexto"
																			Width="100px" Obligatorio="false" valign="left" Enabled="False"></CC1:TEXTBOXTAB>
																		<asp:Label id="lblRPExtranjero" runat="server" cssclass="titulo">RP Extranjero:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtRPExtranjero" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="false"
																			valign="left" Enabled="False"></CC1:TEXTBOXTAB></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">&nbsp;
															<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">F.Nac:&nbsp;</asp:Label></TD>
														<TD>
															<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="false"></cc1:DateBox>
															<asp:Label id="lblNroTramInsc" runat="server" cssclass="titulo">Nro. Tr�mite Insc.:&nbsp;</asp:Label>
															<cc1:numberbox id="txtTram" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"
																MaxLength="12" MaxValor="9999999999999" esdecimal="False" Enabled="false"></cc1:numberbox>&nbsp;
															<asp:Label id="lblTramFecha" runat="server" cssclass="titulo">F.Tr�mite:&nbsp;</asp:Label>
															<cc1:DateBox id="txtTramFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="false"
																Enabled="false"></cc1:DateBox>&nbsp;
															<asp:Label id="lblTramNroControl" runat="server" cssclass="titulo">Nro. Control:&nbsp;</asp:Label>
															<cc1:numberbox id="txtTramNroControl" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"
																MaxLength="12" MaxValor="9999999999999" esdecimal="False" Enabled="false"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD noWrap align="right">
															<asp:Label id="lblInspeccionFenotitipca" runat="server" cssclass="titulo">Inspecci�n Fenot�pica:</asp:Label></TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtCaliFenotipica" runat="server" cssclass="cuadrotexto" Width="250px" MaxLength="4"
																Enabled="False"></CC1:TEXTBOXTAB>
															<asp:Label id="lblInpecciondeVida" runat="server" cssclass="titulo">Inspeccion de Vida:&nbsp;</asp:Label>
															<cc1:DateBox id="txtFechaInspeccionDeVida" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
															<asp:DropDownList id="cmbResultadoInspecdeVida" runat="server" cssclass="combo" Width="152px" AutoPostBack="True">
																<asp:ListItem Value="0">Sin Definici&#243;n</asp:ListItem>
																<asp:ListItem Value="1">Aprobado</asp:ListItem>
																<asp:ListItem Value="2">Rechazado</asp:ListItem>
															</asp:DropDownList>&nbsp;
															<asp:Label id="lblCali" runat="server" cssclass="titulo">Ult.Calific:&nbsp;</asp:Label>
															<CC1:TEXTBOXTAB id="txtCali" runat="server" cssclass="cuadrotexto" Width="250px" MaxLength="4" Enabled="False"></CC1:TEXTBOXTAB>
															<asp:checkbox style="Z-INDEX: 0" id="chkDescorne" Text="Con Descorne" CssClass="titulo" Runat="server"
																TextAlign="Left"></asp:checkbox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 22px" align="right">&nbsp;
															<asp:Label id="lblPX" runat="server" cssclass="titulo">Mocho:&nbsp;</asp:Label></TD>
														<TD style="HEIGHT: 22px" noWrap align="left">
															<CC1:TEXTBOXTAB id="txtPX" runat="server" cssclass="cuadrotexto" Width="40px" MaxLength="4"></CC1:TEXTBOXTAB>&nbsp;
															<asp:Label id="lblVari" runat="server" cssclass="titulo">Var:&nbsp;</asp:Label>
															<cc1:combobox id="cmbVari" class="combo" runat="server" Width="67px" NomOper="rg_registros_tipos_cargar"
																DESIGNTIMEDRAGDROP="2076"></cc1:combobox>&nbsp;
															<asp:Label id="lblRPNormal" runat="server" cssclass="titulo">RP:&nbsp;</asp:Label>
															<CC1:TEXTBOXTAB id="txtRP" onkeypress="return HaceRPNumero('txtRP','txtRPNume', event)" runat="server"
																cssclass="cuadrotexto" Width="100px" Obligatorio="true" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
															<asp:Label id="lblRpNume" runat="server" cssclass="titulo">RP Num�rico:&nbsp;</asp:Label>
															<cc1:numberbox id="txtRPNume" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="true"
																MaxLength="12" MaxValor="9999999999999" esdecimal="False" Enabled="False"></cc1:numberbox>&nbsp;<BUTTON style="WIDTH: 105px" id="btntatu" class="boton" onclick="btntatu_click();" runat="server"
																value="CambioTatuaje">Cambio Tatuaje</BUTTON>
															<asp:Label id="lblInscFechaProd" runat="server" cssclass="titulo">F.Inscrip:&nbsp;</asp:Label>
															<cc1:DateBox id="txtInscFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="false"></cc1:DateBox>
															<asp:checkbox id="chkCastrado" runat="server" Width="88px" Text="                                       Castrado"
																Visible="True" AutoPostBack="True" CssClass="titulo" Runat="server" OnCheckedChanged="chkCastrado_CheckedChanged"></asp:checkbox>
															<asp:label style="Z-INDEX: 0" id="lblCastradoFecha" runat="server" cssclass="titulo">&nbsp;Fecha Castrado:&nbsp;</asp:label>
															<cc1:DateBox style="Z-INDEX: 0" id="txtCastradoFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<asp:Button id="btnPostback" runat="server" Width="0" Height="0" Text="Button"></asp:Button>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">&nbsp;
															<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:Label></TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"></CC1:TEXTBOXTAB>&nbsp;
															<asp:Label id="lblApod" runat="server" cssclass="titulo">Apodo:&nbsp;</asp:Label>
															<CC1:TEXTBOXTAB id="txtApod" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="false"></CC1:TEXTBOXTAB>&nbsp;
															<asp:Label id="lblApro" runat="server" cssclass="titulo"></asp:Label></TD>
													</TR>
													<TR id="trPrefijo" runat="server">
														<TD style="HEIGHT: 10px" align="right">&nbsp;
															<asp:Label id="lblPrefijo" runat="server" cssclass="titulo" Visible="False">Prefijo:&nbsp;</asp:Label></TD>
														<TD>
															<asp:Label id="lblPrefijoDato" runat="server" cssclass="titulo" Visible="False">&nbsp;</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblCri" runat="server" cssclass="titulo">Criador:&nbsp;</asp:Label></TD>
														<TD>
															<UC1:CRIA id="usrCria" runat="server" AceptaNull="false" MostrarBotones="False" Tabla="Criadores"
																Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" Criador="True"
																CampoVal="Criador" Enabled="true" FilClaveUnica="True" ColCriaNume="True" FilAgru="False"
																FilTarjNume="False" ColCUIT="True" FilCUIT="True" Copropiedad="True"></UC1:CRIA></TD> <!--<TD align="left">
																			<asp:Button id="btnCriaCopropiedad" runat="server" cssclass="boton" Width="90px" Text="Copropiedad"
																				CausesValidation="False"></asp:Button></TD>--></TR> <!--
																	<TR>
																		<TD style="HEIGHT: 10px" align="right">&nbsp;
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:numberbox id="txtCria" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"
																				esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox>
																			<CC1:TEXTBOXTAB id="txtCriaDesc" runat="server" cssclass="cuadrotexto" Width="320px" Obligatorio="false"
																				ReadOnly="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	-->
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">
															<asp:label id="lblProp" runat="server" cssclass="titulo">Propietario:&nbsp;</asp:label></TD>
														<TD>
															<UC1:CRIA id="usrProp" runat="server" AceptaNull="false" MostrarBotones="False" Tabla="Criadores"
																Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" Criador="True"
																CampoVal="Criador" Enabled="true" FilClaveUnica="True" ColCriaNume="True" FilAgru="False"
																FilTarjNume="False" ColCUIT="True" FilCUIT="True" Copropiedad="True"></UC1:CRIA></TD> <!--<TD align="left">
																				<asp:Button id="btnPropCopropiedad" runat="server" cssclass="boton" Width="90px" Text="Copropiedad" CausesValidation="False"></asp:Button></TD>--></TR> <!--
																	<TR>
																		<TD style="HEIGHT: 10px" align="right">&nbsp;
																			<asp:Label id="lblProdProp" runat="server" cssclass="titulo">Propietario:</asp:Label></TD>
																		<TD><SPAN class="titulo">Cliente:</SPAN>
																			<cc1:numberbox id="txtPropClie" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="false"
																				ReadOnly="True"></cc1:numberbox>
																			<CC1:TEXTBOXTAB id="txtPropDesc" runat="server" cssclass="cuadrotexto" Width="45%" Obligatorio="false"
																				ReadOnly="True"></CC1:TEXTBOXTAB><SPAN class="titulo">Expediente:</SPAN>
																			<cc1:numberbox id="txtProp" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="false"
																				ReadOnly="True"></cc1:numberbox>
																			<asp:Label id="lblpropietario" runat="server" ForeColor="RoyalBlue" Font-Bold="True" Font-Size="XX-Small"></asp:Label>
																			<asp:Label id="lblTranFecha" runat="server" cssclass="titulo">F.Transf:</asp:Label>
																			<asp:Label id="txtTranFecha" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	-->
													<TR>
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblFechaTransf" runat="server" cssclass="titulo">Fecha Transf.:&nbsp;</asp:Label></TD>
														<TD>
															<cc1:DateBox id="txtFechaTransferencia" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="false"></cc1:DateBox>&nbsp;
															<asp:Label id="lblNroTransf" runat="server" cssclass="titulo">Nro. Transf.:&nbsp;</asp:Label>&nbsp;
															<cc1:numberbox id="txtNroTransf" runat="server" cssclass="cuadrotexto" Width="100px" Enabled="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblNroControlTransf" runat="server" cssclass="titulo">Nro. Control Transf.:&nbsp;</asp:Label>&nbsp;
															<cc1:numberbox id="txtNroControlTransf" runat="server" cssclass="cuadrotexto" Width="100px" Enabled="False"></cc1:numberbox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR id="trColorCabeza" runat="server">
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblPelaA" runat="server" cssclass="titulo">Color:&nbsp;</asp:Label></TD>
														<TD>
															<cc1:combobox id="cmbPelaA" class="combo" runat="server" Width="400px" AceptaNull="True"></cc1:combobox>&nbsp;
															<asp:Label id="lblPelaB" runat="server" cssclass="titulo">Cabeza:&nbsp;</asp:Label>
															<cc1:combobox id="cmbPelaC" class="combo" runat="server" Width="402px" AceptaNull="True"></cc1:combobox>&nbsp;
														</TD>
													</TR>
													<TR id="trCuerpoMiembros" runat="server">
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblPelaC" runat="server" cssclass="titulo">Cuerpo:&nbsp;</asp:Label></TD>
														<TD>
															<cc1:combobox id="cmbPelaB" class="combo" runat="server" Width="400px" AceptaNull="True"></cc1:combobox>&nbsp;
															<asp:Label id="lblPelaD" runat="server" cssclass="titulo">Miembros:&nbsp;</asp:Label>
															<cc1:combobox id="cmbPelaD" class="combo" runat="server" Width="386px" AceptaNull="True"></cc1:combobox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR id="trCondGenet" runat="server">
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblCondGenet" runat="server" cssclass="titulo">Cond. Genetica:&nbsp;</asp:Label></TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtCondGeneticas" runat="server" cssclass="cuadrotexto" Width="50%" Obligatorio="false"
																ReadOnly="true"></CC1:TEXTBOXTAB>
															<asp:Label id="lblComentario" runat="server" cssclass="titulo">&nbsp;Separador punto y coma&nbsp;</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">&nbsp;
															<asp:Label id="lblPadre" runat="server" cssclass="titulo">Padre:</asp:Label></TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtPadreDesc" runat="server" cssclass="cuadrotexto" Width="90%" Obligatorio="false"
																ReadOnly="True"></CC1:TEXTBOXTAB>
															<asp:Label id="lblPxPadre" runat="server" cssclass="titulo" Visible="False">Mocho:</asp:Label>
															<CC1:TEXTBOXTAB id="txtPxPadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB>
															<asp:Label id="lblRpPadre" runat="server" cssclass="titulo" Visible="False">RP:</asp:Label>
															<CC1:TEXTBOXTAB id="txtRpPadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB>
															<CC1:TEXTBOXTAB id="txtPadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB>
															<asp:Label id="lblNombrePadre" runat="server" cssclass="titulo" Visible="False">Nombre:</asp:Label>
															<CC1:TEXTBOXTAB id="txtNombrePadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">&nbsp;
															<asp:Label id="lblMadre" runat="server" cssclass="titulo">Madre:</asp:Label></TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtMadreDesc" runat="server" cssclass="cuadrotexto" Width="90%" Obligatorio="false"
																ReadOnly="True"></CC1:TEXTBOXTAB>
															<asp:Label id="lblPxMadre" runat="server" cssclass="titulo" Visible="False">Mocho:</asp:Label>
															<CC1:TEXTBOXTAB id="txtPxMadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB>
															<asp:Label id="lblRpMadre" runat="server" cssclass="titulo" Visible="False">RP:</asp:Label>
															<CC1:TEXTBOXTAB id="txtRpMadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB>
															<CC1:TEXTBOXTAB id="txtMadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB>
															<asp:Label id="lblNombreMadre" runat="server" cssclass="titulo" Visible="False">Nombre:</asp:Label>
															<CC1:TEXTBOXTAB id="txtNombreMadre" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="false"
																Visible="False" ReadOnly="True"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="3" align="center">
															<TABLE style="MARGIN-TOP: 10px" id="Table8" class="marco" cellSpacing="0" cellPadding="0"
																width="100%">
																<TR>
																	<TD colSpan="3" align="left"><U>
																			<asp:Label id="lblTituAnal" runat="server" cssclass="titulo" Font-Size="10">Datos del �ltimo an�lisis</asp:Label></U></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 125px" noWrap align="right">
																		<asp:Label id="lblResulFinal" runat="server" cssclass="titulo">Resultado Final:&nbsp;&nbsp;</asp:Label></TD>
																	<TD style="WIDTH: 140px" noWrap align="right">
																		<asp:Label id="lblNroAnal" runat="server" cssclass="titulo">Nro.:&nbsp;</asp:Label>
																		<cc1:numberbox id="txtNroAnal" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></cc1:numberbox>&nbsp;
																	</TD>
																	<TD noWrap align="left">
																		<asp:Label id="lblResulAnal" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtResulAnal" runat="server" cssclass="cuadrotexto" Width="350px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																		<asp:Label id="lblPureza" runat="server" cssclass="titulo">Pureza:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtPureza" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																		<asp:Label id="lblTipoDeAnal" runat="server" cssclass="titulo">Tipo:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtTipoAnal" runat="server" cssclass="cuadrotexto" Width="30px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																		<asp:Label id="lblFechaTest" runat="server" cssclass="titulo">Fecha Test:&nbsp;</asp:Label>
																		<cc1:DateBox id="txtFechaAnal" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 125px" noWrap align="right">
																		<asp:Label id="Label2" runat="server" cssclass="titulo" Visible="False">Resultado Final:</asp:Label></TD>
																	<TD style="WIDTH: 140px" noWrap align="right"></TD>
																	<TD noWrap>
																		<asp:Label id="lblEstadoAnal" runat="server" cssclass="titulo">Estado:&nbsp;</asp:Label>&nbsp;&nbsp;&nbsp;
																		<cc1:TEXTBOXTAB id="txtEstadoAnal" runat="server" cssclass="cuadrotexto" Width="95%" Enabled="False"></cc1:TEXTBOXTAB>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 125px" noWrap align="right">
																		<asp:Label id="lblParciales" runat="server" cssclass="titulo">Parciales:&nbsp;&nbsp;</asp:Label></TD>
																	<TD style="WIDTH: 140px" noWrap align="right">
																		<asp:Label id="lblTieneADN" runat="server" cssclass="titulo">Tiene ADN:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtTieneADN" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																	</TD>
																	<TD noWrap align="left">
																		<asp:Label id="lblResulADN" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtResulADN" runat="server" cssclass="cuadrotexto" Width="350px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 125px" width="125" align="right"></TD>
																	<TD style="WIDTH: 140px" noWrap align="right">
																		<asp:Label id="lblTieneTS" runat="server" cssclass="titulo">Tiene TS:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtTieneTS" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																	</TD>
																	<TD noWrap align="left">
																		<asp:Label id="lblResulTS" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtResulTS" runat="server" cssclass="cuadrotexto" Width="350px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 125px" width="125" align="right"></TD>
																	<TD style="WIDTH: 140px" noWrap align="right">
																		<asp:Label id="lblTieneSNPS" runat="server" cssclass="titulo">Tiene SNPS:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtTieneSNPS" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																	</TD>
																	<TD noWrap align="left">
																		<asp:Label id="lblResulSNPS" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtResulSNPS" runat="server" cssclass="cuadrotexto" Width="350px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																		<asp:Label id="lblResulSNPSExtranj" runat="server" cssclass="titulo">Resultado SNPS Extranjero:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtResulSNPSExtranjero" runat="server" cssclass="cuadrotexto" Width="56px" Enabled="False"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 125px" width="125" align="right"></TD>
																	<TD style="WIDTH: 140px" noWrap align="right">
																		<asp:Label id="lblTienePelo" runat="server" cssclass="titulo">Tiene Pelo:&nbsp;</asp:Label>
																		<CC1:TEXTBOXTAB id="txtTienePelo" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></CC1:TEXTBOXTAB>&nbsp;
																	</TD>
																	<TD noWrap align="left">
																		<asp:Label id="lblFechaUltRecepcion" runat="server" cssclass="titulo">F.Ultima Recepci�n:&nbsp;</asp:Label>
																		<cc1:DateBox id="txtFechaUltAnalisis" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"
																			Enabled="False"></cc1:DateBox>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 125px" width="125" align="right"></TD>
																	<TD style="WIDTH: 100%" colSpan="3" noWrap align="left">
																		<asp:Label id="lblEstudio" runat="server" cssclass="titulo" ForeColor="Red" Width="100%"> Nro estudio:9277 - Fecha:25/08/2015 - Estado:  Finalizado  - Conclusi�n:  Resuelto </asp:Label></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label></TD>
														<TD>
															<cc1:combobox id="cmbEsta" class="combo" runat="server" Width="140px" AceptaNull="True" Enabled="False"></cc1:combobox>&nbsp;
															<asp:Label id="lblFalleFecha" runat="server" cssclass="titulo">Fecha Baja:&nbsp;</asp:Label>
															<cc1:DateBox id="txtFalleFecha" runat="server" cssclass="cuadrotexto" Width="68px" onchange="javascript:mSetearMotivo();"
																Obligatorio="False" Enabled="False"></cc1:DateBox>&nbsp;
															<asp:Label id="lblMoti" runat="server" cssclass="titulo">Descrip.Baja:&nbsp;</asp:Label>
															<cc1:combobox id="cmbBaja" class="combo" runat="server" Width="200px" AceptaNull="True" Enabled="False"></cc1:combobox>&nbsp;
															<asp:label id="lblCuig" runat="server" cssclass="titulo">Nro.Oficial:&nbsp;</asp:label>
															<CC1:TEXTBOXTAB id="txtCuig" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="3" align="center">
															<asp:Button id="btnProp" runat="server" cssclass="boton" Width="90px" Text="Propiedades" CausesValidation="False"></asp:Button>&nbsp;
															<asp:Button id="btnRelaHijo" runat="server" cssclass="boton" Width="90px" Text="Hijos" CausesValidation="False"></asp:Button>&nbsp;
															<asp:Button id="btnCertInsc" runat="server" cssclass="boton" Width="90px" Text="Cert.Inscrip."
																CausesValidation="False"></asp:Button>&nbsp;
															<asp:Button id="btnErr" runat="server" cssclass="boton" Width="90px" Text="Errores" CausesValidation="False"></asp:Button></TD>
													</TR>
													<TR>
														<TD height="30" vAlign="bottom" colSpan="3" align="center"><BUTTON style="WIDTH: 75px" id="btnTramites" class="boton" onclick="btnTramites_click();"
																runat="server" value="Tr�mites">Tr�mites</BUTTON>&nbsp;<BUTTON style="WIDTH: 75px" id="btnServicios" class="boton" onclick="btnServicios_click();"
																runat="server" value="Servicios">Servicios</BUTTON>&nbsp;<BUTTON style="WIDTH: 75px" id="btnParejas" class="boton" onclick="btnParejas_click();"
																runat="server" value="Parejas">Parejas</BUTTON>&nbsp;<BUTTON style="WIDTH: 75px" id="btnPremios" class="boton" onclick="btnPremios_click();"
																runat="server" value="Premios">Premios</BUTTON>&nbsp;<BUTTON style="WIDTH: 85px" id="btnPerform" class="boton" onclick="btnPerform_click();"
																runat="server" value="Performance">Performance</BUTTON>&nbsp;<BUTTON style="WIDTH: 75px" id="btnPedigree" class="boton" onclick="btnPedigree_click();"
																runat="server" value="Pedigree">Pedigree</BUTTON>&nbsp;<BUTTON style="WIDTH: 75px" id="btnAuditoria" class="boton" onclick="btnAuditoria_click();"
																runat="server" value="Pedigree">Auditor�a</BUTTON>&nbsp;<BUTTON style="WIDTH: 85px" id="btnSemenStock" class="boton" onclick="btnSemenStock_click();"
																runat="server" value="Pedigree">Semen Stock</BUTTON>&nbsp;<BUTTON style="WIDTH: 105px" id="btnEmbrionesStock" class="boton" onclick="btnEmbrionesStock_click();"
																runat="server" value="Pedigree">Embriones Stock</BUTTON></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" vAlign="top" colSpan="3" align="center">
											<asp:panel id="panAna" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 100%" colSpan="2" align="center">
															<asp:datagrid id="grdAna" runat="server" BorderStyle="None" BorderWidth="1px" width="95%" Visible="true"
																OnEditCommand="mEditarDatosAnalisis" OnPageIndexChanged="grdAna_PageChanged" AutoGenerateColumns="False"
																CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																<FooterStyle CssClass="footer"></FooterStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="20px"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="false" DataField="prta_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="prta_fecha" HeaderText="Fecha" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																	<asp:BoundColumn DataField="prta_nume" ReadOnly="True" HeaderText="N�mero" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_tipo" ReadOnly="True" HeaderText="Tipo" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_resul" HeaderText="Resultado">
																		<HeaderStyle Width="100%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="prta_resul_fecha" HeaderText="F.Resultado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																	<asp:BoundColumn DataField="prta_baja_fecha" HeaderText="Fecha Baja" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD height="16" vAlign="bottom" colSpan="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblAnaNume" runat="server" cssclass="titulo">Nro.An�lsis:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtAnaNume" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="false"
																Visible="true" MaxLength="12" MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 12px" align="right">
															<asp:Label id="lblAnaTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 12px">
															<cc1:combobox id="cmbAnaTipo" class="combo" runat="server" Width="150px"><asp:listitem value="" selected="true">(Seleccione)</asp:listitem><asp:listitem value="1">ADN</asp:listitem><asp:listitem value="0">Tipificaci�n</asp:listitem></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblAnaFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:DateBox id="txtAnaFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblAnaResul" runat="server" cssclass="titulo">Resultado:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 10px">
															<cc1:combobox id="cmbAnaResul" class="combo" runat="server" Width="320px"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblAnaFechaResul" runat="server" cssclass="titulo">Fecha Resultado:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 10px">
															<cc1:DateBox id="txtAnaFechaResul" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="5" vAlign="bottom" colSpan="2"></TD>
													</TR>
													<TR>
														<TD height="34" vAlign="middle" colSpan="2" align="center">
															<asp:Button id="btnAltaAna" runat="server" cssclass="boton" Width="110px" Text="Agregar An�lisis"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnBajaAna" runat="server" cssclass="boton" Width="110px" Text="Eliminar An�lisis"
																Visible="true"></asp:Button>&nbsp;
															<asp:Button id="btnModiAna" runat="server" cssclass="boton" Width="110px" Text="Modificar An�lisis"
																Visible="true"></asp:Button>&nbsp;
															<asp:Button id="btnLimpAna" runat="server" cssclass="boton" Width="110px" Text="Limpiar An�lisis"></asp:Button></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3">
											<asp:panel id="panAsociacion" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="TablaAsociacion" border="0" cellPadding="0" align="left">
													<TR>
														<TD vAlign="top" colSpan="2" align="center">
															<asp:datagrid id="grdAsociacion" runat="server" BorderStyle="None" BorderWidth="1px" width="100%"
																OnEditCommand="mEditarDatosAsociacion" OnPageIndexChanged="DataGridAsociacion_Page" AutoGenerateColumns="False"
																CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="ptnu_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_asoc_deno" ReadOnly="True" HeaderText="Asociaci�n">
																		<HeaderStyle Width="80%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="ptnu_nume" ReadOnly="True" HeaderText="N�mero">
																		<HeaderStyle Width="20%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="prdt_rp_extr" ReadOnly="True" HeaderText="RP.Extr.">
																		<HeaderStyle Width="20%"></HeaderStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR> <!--<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblAsoc" runat="server" cssclass="titulo">Asociaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox id="cmbAsoc" class="combo" runat="server" cssclass="cuadrotexto" filtra="True" MostrarBotones="False"
																				NomOper="asociaciones_cargar" Height="20px" onchange="Combo_change(this)" Width="264px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblNroAsoc" runat="server" cssclass="titulo">Nro:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtNroAsoc" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBajaAsoc" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnAltaAsociacion" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaAsociacion" runat="server" cssclass="boton" Width="80px" Text="Baja"
																				CausesValidation="False"></asp:Button>
																			<asp:Button id="btnModiAsociacion" runat="server" cssclass="boton" Width="80px" Text="Modificar"
																				CausesValidation="False"></asp:Button>
																			<asp:Button id="btnLimpAsociacion" runat="server" cssclass="boton" Width="80px" Text="Limpiar"
																				CausesValidation="False"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>--></TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3">
											<asp:panel id="panDocumentos" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="TablaDocumentos" border="0" cellPadding="0" align="left">
													<TR>
														<TD vAlign="top" colSpan="3" align="center">
															<asp:datagrid id="grdDocum" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" Visible="True"
																OnEditCommand="mEditarDatosDocum" OnPageIndexChanged="DataGridDocum_Page" AutoGenerateColumns="False"
																CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="prdo_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="prdo_path" HeaderText="Documento"></asp:BoundColumn>
																	<asp:BoundColumn DataField="prdo_refe" HeaderText="Referencia"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_prdo_imag" HeaderText="Imagen"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 14px" vAlign="top" align="right">
															<asp:Label id="lblFoto" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 14px" height="14" colSpan="2">
															<CC1:TEXTBOXTAB id="txtFoto" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG style="CURSOR: hand" id="imgDelFoto" onclick="javascript:mLimpiarFoto();" alt="Limpiar Foto"
																src="imagenes\del.gif" runat="server"><BUTTON style="WIDTH: 90px" id="btnFotoVer" class="boton" runat="server" value="Ver Foto">Ver 
																Foto</BUTTON><BR>
															<INPUT style="WIDTH: 340px; HEIGHT: 22px" id="filDocumDoc" size="49" type="file" name="filDocumDoc"
																runat="server">
														</TD>
													</TR>
													<TR style="DISPLAY: none">
														<TD style="WIDTH: 27%; HEIGHT: 14px" vAlign="top" align="right">
															<asp:Image id="pictureBox" Runat="server"></asp:Image></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="Label1" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbReferencia" class="combo" runat="server" Width="320px"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblEsImg" runat="server" cssclass="titulo">Es Im�gen:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<asp:checkbox id="chkEsImg" Text="" CssClass="titulo" Runat="server"></asp:checkbox></TD>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center">
															<asp:Label id="lblBajaDocum" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD colSpan="3" align="center">
															<asp:Button id="btnAltaDocum" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBajaDocum" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>
															<asp:Button id="btnModiDocum" runat="server" cssclass="boton" Width="80px" Text="Modificar"
																CausesValidation="False"></asp:Button>
															<asp:Button id="btnLimpDocum" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel>
											<asp:panel id="panObse" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="Table4" border="0" cellPadding="0" align="left">
													<TR>
														<TD vAlign="top" colSpan="3" align="center">
															<asp:datagrid id="grdObse" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" Visible="True"
																OnEditCommand="mEditarDatosObse" OnPageIndexChanged="DataGridObse_Page" AutoGenerateColumns="False"
																CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<FooterStyle CssClass="footer"></FooterStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="prdo_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="prdo_refe" HeaderText="Observaciones"></asp:BoundColumn>
																	<asp:BoundColumn DataField="prdo_path" HeaderText="Documento"></asp:BoundColumn>
																	<asp:BoundColumn Visible="False" DataField="_prdo_imag" HeaderText="Imagen"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblObseRefe" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:TEXTBOXTAB id="txtObseRefe" runat="server" cssclass="cuadrotexto" Width="426px" Height="46px"
																EnterPorTAb="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 14px" vAlign="top" align="right">
															<asp:Label id="lblObseDocu" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 14px" height="14" colSpan="2">
															<CC1:TEXTBOXTAB id="txtObseDocu" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG style="CURSOR: hand" id="imgDelDocu" onclick="javascript:mLimpiarDocu();" alt="Limpiar Docum."
																src="imagenes\del.gif" runat="server"><BUTTON style="WIDTH: 90px" id="btnDocuVer" class="boton" runat="server" value="Ver Docum.">Ver 
																Docum.</BUTTON><BR>
															<INPUT style="WIDTH: 340px; HEIGHT: 22px" id="filObseDocu" size="49" type="file" name="filObseDocu"
																runat="server">
														</TD>
													</TR>
													<TR>
														<asp:Label id="lblObservProdInsc" runat="server" cssclass="titulo" ForeColor="Blue"></asp:Label></TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center">
															<asp:Label id="lblBajaObse" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD colSpan="3" align="center">
															<asp:Button id="btnAltaObse" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBajaObse" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>
															<asp:Button id="btnModiObse" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>
															<asp:Button id="btnLimpObse" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3">
											<asp:panel id="panOtrosDatos" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="TablaOtrosDatos" border="0" cellPadding="0" align="left">
													<TR>
														<TD align="right">
															<asp:Label id="lblSiete" runat="server" cssclass="titulo">Sietemesino:&nbsp;</asp:Label></TD>
														<TD style="HEIGHT: 10px">
															<asp:CheckBox id="chkSiete" CssClass="titulo" Runat="server"></asp:CheckBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblMelli" runat="server" cssclass="titulo">Mellizo:&nbsp;</asp:Label></TD>
														<TD style="HEIGHT: 10px">
															<cc1:combobox id="cmbMelli" class="combo" runat="server" Width="218px"><asp:ListItem Selected="True">(Seleccione)</asp:ListItem><asp:ListItem Value="1">ADN</asp:ListItem><asp:ListItem Value="0">Tipificaci&#243;n</asp:ListItem></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR id="trDador" runat="server">
														<TD align="right">
															<asp:Label id="lblDador" runat="server" cssclass="titulo">Dador:&nbsp;</asp:Label></TD>
														<TD style="HEIGHT: 10px">
															<asp:Label id="lblDadorEstado" runat="server" cssclass="titulo" ForeColor="Red">Habilitado</asp:Label>&nbsp;
															<asp:Label id="lblNroDador" runat="server" cssclass="titulo">Nro.Dador:</asp:Label>
															<cc1:numberbox id="txtDona" runat="server" cssclass="cuadrotexto" Width="90px" Obligatorio="false"
																MaxLength="12" MaxValor="9999999999999" esdecimal="False" Enabled="false"></cc1:numberbox>&nbsp;
															<asp:Label id="lblDadorNroSenasa" runat="server" cssclass="titulo">Nro.Senasa:&nbsp;</asp:Label>
															<cc1:TextboxTab id="txtNroSenasa" runat="server" cssclass="cuadrotexto" Width="90px" Obligatorio="false"
																Enabled="False"></cc1:TextboxTab>&nbsp;
															<asp:Label id="lblDadorDonanteFechaDesde" runat="server" cssclass="titulo">F.Desde:&nbsp;</asp:Label>
															<cc1:DateBox id="txtDadorDonanteFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"
																Obligatorio="false" Enabled="false"></cc1:DateBox>&nbsp;
															<asp:Label id="lblDadorDonanteNroControl" runat="server" cssclass="titulo">Nro.Control:&nbsp;</asp:Label>
															<cc1:numberbox id="txtDadorDonanteNroControl" runat="server" cssclass="cuadrotexto" Width="90px"
																Obligatorio="false" MaxLength="12" MaxValor="9999999999999" esdecimal="False" Enabled="false"></cc1:numberbox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR id="TrServicio" runat="server">
														<TD align="left">
															<asp:Label id="lblDatosServicio" runat="server" cssclass="titulo">Datos del Servicio:&nbsp;</asp:Label></TD>
														<TD style="HEIGHT: 10px">
															<asp:Label id="lblTipoServ" runat="server" cssclass="titulo" Width="173px">Tipo Servicio:&nbsp;</asp:Label>
															<asp:Label id="txtServTipo" runat="server" cssclass="titulo" Width="264px"></asp:Label>&nbsp;&nbsp;
															<asp:Label id="lblServFecha" runat="server" cssclass="titulo" Width="124px">Fecha Servicio:&nbsp;</asp:Label>
															<asp:Label id="txtServFecha" runat="server" cssclass="titulo"></asp:Label></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblTranEmbr" runat="server" cssclass="titulo" Width="158px">Transpl. Embrionario:&nbsp;</asp:Label>
															<asp:CheckBox id="chkTranEmbr" CssClass="titulo" Runat="server"></asp:CheckBox>
														<TD style="HEIGHT: 10px" colSpan="4" noWrap>
															<asp:Label id="lblRece" runat="server" cssclass="titulo" Width="72px">Receptora:&nbsp;</asp:Label>
															<asp:Label id="txtRece" runat="server" cssclass="titulo" Width="124px"></asp:Label>&nbsp;&nbsp;
															<asp:Label id="lblImplFecha" runat="server" cssclass="titulo" Width="124px">Fecha Implante:&nbsp;</asp:Label>
															<asp:Label id="txtImplFecha" runat="server" cssclass="titulo"></asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR id="trDonante" runat="server">
														<TD align="right">
															<asp:Label id="lblDonante" runat="server" cssclass="titulo">Donante:&nbsp;</asp:Label></TD>
														<TD style="HEIGHT: 10px">
															<asp:Label id="lblDonanteEstado" runat="server" cssclass="titulo" ForeColor="Red">Habilitado</asp:Label>&nbsp;
															<asp:Label id="lblNroDonante" runat="server" cssclass="titulo">Nro.Donante:</asp:Label>
															<cc1:numberbox id="txtDonante" runat="server" cssclass="cuadrotexto" Width="90px" Obligatorio="false"
																MaxLength="12" MaxValor="9999999999999" esdecimal="False" Enabled="false"></cc1:numberbox>&nbsp;
															<asp:Label id="lblHembraDonanteFechaDesde" runat="server" cssclass="titulo">F.Desde:&nbsp;</asp:Label>
															<cc1:DateBox id="txtHembraDonanteFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"
																Obligatorio="false" Enabled="false"></cc1:DateBox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3" align="center">
											<asp:panel id="panPesadasDeps" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="TablaPesadasYDeps" border="0" cellPadding="0" align="left">
													<TR width="100%">
														<TD style="HEIGHT: 10px" align="left">
															<asp:Label id="lblPesadasDeps" runat="server" cssclass="titulo" Font-Size="10" Font-Underline="True">PESADAS:</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="lblNacimientoPD" runat="server" cssclass="titulo">Nacimiento</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="lblDestetePD" runat="server" cssclass="titulo">Destete</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="lblPosDetetePD" runat="server" cssclass="titulo">PosDestete</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="Label4" runat="server" cssclass="titulo">Circ.Escr.</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="right">
															<asp:Label id="lblAnioCorridaPD" runat="server" cssclass="titulo">A�o Corrida:</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtAnioCorridaNacimiento" runat="server" cssclass="cuadrotexto" Width="70px"
																Obligatorio="False" enabled="False" MaxLength="4" MaxValor="2079" esdecimal="false"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtAnioCorridaDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																enabled="False" MaxLength="4" MaxValor="2079" esdecimal="false"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtAnioCorridaPosDestete" runat="server" cssclass="cuadrotexto" Width="70px"
																Obligatorio="False" enabled="False" MaxLength="4" MaxValor="2079" esdecimal="false"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtAnioCorridaCircEscr" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																enabled="False" MaxLength="4" MaxValor="2079" esdecimal="false"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="right">
															<asp:Label id="lblFechaPD" runat="server" cssclass="titulo">Fecha:</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:DateBox id="txtFechaPesadasNacimiento" runat="server" cssclass="cuadrotexto" Width="70px"
																Obligatorio="false" Enabled="false"></cc1:DateBox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:DateBox id="txtFechaPesadasDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="false"
																Enabled="false"></cc1:DateBox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:DateBox id="txtFechaPesadasPosDestete" runat="server" cssclass="cuadrotexto" Width="70px"
																Obligatorio="false" Enabled="false"></cc1:DateBox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:DateBox id="txtFechaPesadasCircEscr" runat="server" cssclass="cuadrotexto" Width="70px"
																Obligatorio="false" Enabled="false"></cc1:DateBox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="right">
															<asp:Label id="lblMedicionPD" runat="server" cssclass="titulo">Medici�n:</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtMedicionNacimiento" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																enabled="False" MaxLength="5" MaxValor="99999" esdecimal="false"></cc1:numberbox>
															<asp:Label id="Label6" runat="server" cssclass="titulo">(kg)</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtMedicionDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																enabled="False" MaxLength="5" MaxValor="99999" esdecimal="false"></cc1:numberbox>
															<asp:Label id="Label7" runat="server" cssclass="titulo">(kg)</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtMedicionPosDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																enabled="False" MaxLength="5" MaxValor="99999" esdecimal="false"></cc1:numberbox>
															<asp:Label id="Label8" runat="server" cssclass="titulo">(kg)</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtMedicionCircEscr" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="8" MaxValor="99999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox>
															<asp:Label id="Label9" runat="server" cssclass="titulo">(cm)</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="right">
															<asp:Label id="Label5" runat="server" cssclass="titulo">Manejo:</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtManejoDestete" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="False"
																enabled="False" MaxLength="2" MaxValor="99" esdecimal="false"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtManejoPosDestete" runat="server" cssclass="cuadrotexto" Width="152px" Obligatorio="False"
																enabled="False" MaxLength="2" MaxValor="99" esdecimal="false"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR height="33" width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="right">
															<asp:Label id="Label10" runat="server" cssclass="titulo">OBS:</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<CC1:TEXTBOXTAB id="txtOBSDestete" runat="server" cssclass="cuadrotexto" Width="160px" Height="32px"
																MaxLength="250" EnterPorTAb="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<CC1:TEXTBOXTAB id="txtOBSPosDestete" runat="server" cssclass="cuadrotexto" Width="160px" Height="32px"
																MaxLength="250" EnterPorTAb="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" align="left">
															<asp:Label id="Label11" runat="server" cssclass="titulo" Font-Size="10" Font-Underline="True">DEPS:</asp:Label>
															<cc1:DateBox id="txtDEPSFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="false"
																Enabled="false"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="Label16" runat="server" cssclass="titulo">Nacimiento</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="Label17" runat="server" cssclass="titulo">Destete</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="Label18" runat="server" cssclass="titulo">PosDestete</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="Label19" runat="server" cssclass="titulo">Circ.Escr.</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="Label20" runat="server" cssclass="titulo">Leche Materna</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<asp:Label id="Label21" runat="server" cssclass="titulo">Leche y Crianza</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="right">
															<asp:Label id="Label14" runat="server" cssclass="titulo">EPD:</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtEPDNacimiento" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="6" MaxValor="999999" esdecimal="True" Enabled="false" CantMax="1"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtEPDDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="6" MaxValor="999999" esdecimal="True" Enabled="false" CantMax="1"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtEPDPosDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="6" MaxValor="999999" esdecimal="True" Enabled="false" CantMax="1"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtEPDCircEscr" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtEPDLecheMaterna" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtEPDLecheYCrianza" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="8" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR width="100%">
														<TD style="HEIGHT: 10px" width="12%" align="right">
															<asp:Label id="Label15" runat="server" cssclass="titulo">Exactitud:</asp:Label></TD>
														<TD style="HEIGHT: 10px" width="4%" align="left"></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtExactitudNacimiento" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtExactitudDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtExactitudPosDestete" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtExactitudCircEscr" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left">
															<cc1:numberbox id="txtExactitudLecheMaterna" runat="server" cssclass="cuadrotexto" Width="70px"
																Obligatorio="False" MaxLength="7" MaxValor="9999999" esdecimal="True" Enabled="false" CantMax="2"></cc1:numberbox></TD>
														<TD style="HEIGHT: 10px" width="14%" align="left"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
								</TABLE>
							</asp:panel>
							<DIV style="DISPLAY: inline" id="divgraba" runat="server"><ASP:PANEL id="panBotones" Runat="server">
									<TABLE width="100%">
										<TR>
											<TD align="center">
												<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
												<asp:Button id="btnRevertirBaja" runat="server" cssclass="boton" Width="80px" Text="Revertir baja"
													Visible="False" CausesValidation="False"></asp:Button></TD>
										</TR>
										<TR height="30">
											<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
												<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta" Visible="false"></asp:Button>&nbsp;
												<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" Visible="false"
													CausesValidation="False"></asp:Button>
												<asp:Button id="btnProce" runat="server" cssclass="boton" Width="80px" Text="Procesar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
												<asp:Button id="btnInscribirExtr" runat="server" cssclass="boton" Width="110px" Text="Insc Extranjero"
													CausesValidation="False"></asp:Button>&nbsp;&nbsp;
												<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
												<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" Visible="False"
													CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											</TD>
										</TR>
									</TABLE>
								</ASP:PANEL></DIV>
							<DIV style="DISPLAY: none" id="divproce" runat="server"><asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            </asp:panel></DIV>
						</DIV>
						<A id="Ancla" name="Ancla"></A>
					</TD>
				</TR>
			</table> <!--- FIN CONTENIDO ---> </TD>
			<TD background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></TD>
			</TR>
			<TR>
				<TD width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></TD>
				<TD background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></TD>
				<TD width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></TD>
			</TR>
			</TBODY></TABLE> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnAnaId" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnSRA" runat="server"></asp:textbox><asp:textbox id="hdnEspe" runat="server"></asp:textbox><asp:textbox id="hdnDocumId" runat="server"></asp:textbox><asp:textbox id="hdnAsocId" runat="server"></asp:textbox><asp:textbox id="hdnCriaId" runat="server"></asp:textbox><asp:textbox id="hdnCriaFilId" runat="server"></asp:textbox><asp:textbox id="hdnObseId" runat="server"></asp:textbox><asp:textbox id="hdnPdinId" runat="server"></asp:textbox><asp:textbox id="hdnRazaAnteriorId" runat="server">-1</asp:textbox><ASP:TEXTBOX id="hdnCriadorID" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnPropietario" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnRp" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnRpNume" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnSoloConsulta" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnMensajeConsulta" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnSRANoInscribe" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnExtranjero" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnBloqueoCambioTatuaje" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnCambioTatuajePop" runat="server" AutoPostBack="True"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
	
		function mPorcPeti()
		{
			document.all("divgraba").style.display ="none";
			document.all("divproce").style.display ="inline";
		}
	
		if (document.all["Ancla"]!= null)
			document.location='#Ancla';
		if (document.all["lblBaja"]!= null)
		{
			if (document.all["lblBaja"].innerHTML == ":")
				document.all["lblBaja"].innerHTML = "";
		}
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all('cmbRazaFil')!=null)
			mNumeDenoConsul(document.all('cmbRazaFil'),true);
		if (document.all('cmbRaza')!=null)
			mNumeDenoConsul(document.all('cmbRaza'),false);
		
		gSetearTituloFrame('Productos');
		
		function mLimpiarDocu() 
		{
			document.all("txtObseDocu").value = '';
				
		}
		function mLimpiarFoto() 
		{
			document.all("txtFoto").value = '';
				
		}

		   
		function cmbRazaFil_onchange(pstrNomb)
		{
		 
			if(document.all(pstrNomb)!=null && document.all("txtusrCriaFil:cmbRazaCria")!=null)
			{
				document.all("usrCriaFil:cmbRazaCria").value = document.all(pstrNomb).value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
			}
			if(document.all(pstrNomb)!=null && document.all("txtusrPropCriaFil:cmbRazaCria")!=null)
			{
				document.all("usrPropCriaFil:cmbRazaCria").value = document.all(pstrNomb).value;
				document.all("usrPropCriaFil:cmbRazaCria").onchange();
			}
		}
		
		 
		
		if(document.all("cmbRazaFil")!=null)
		{
			document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
			//document.all("usrCriaFil:cmbRazaCria").onchange();
			document.all("usrPropCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
			//document.all("usrPropCriaFil:cmbRazaCria").onchange();				
		}

		mSetearMotivo();
		 
		mSetearRazas();
		mSelecSexo();

		</SCRIPT>
		</TD></TR></TBODY></TABLE></CC1:CRIA>
	</BODY>
</HTML>
