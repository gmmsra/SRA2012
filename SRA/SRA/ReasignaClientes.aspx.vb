Namespace SRA

Partial Class ReasignaClientes
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    Protected WithEvents btnNuevoCliente As System.Web.UI.WebControls.Button


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTable As String = "clientes_reasigna"
   Private mstrConn As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdReas.PageSize = Convert.ToInt32(mstrParaPageSize)
      lblMens.Text = ""
      btnGrab.Attributes.Add("onclick", "if(!confirm('Confirma la reasignación del cliente?')) return false;")
      mMostrarPanel(False)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mConsultarReasignaciones()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            ' Dario 2013-11-21 cuando se hace una busqueda se coloca la pagina 1
            mConsultar(0)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub

   Public Sub grdReas_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            ' Dario 2013-11-21 Se modifica porque no camina el paginado de la grilla
            Dim index As Int16 = 0
            grdReas.EditItemIndex = -1
            If (grdReas.CurrentPageIndex > 0 Or grdReas.CurrentPageIndex < grdReas.PageCount) Then
                index = E.NewPageIndex
            End If
            ' Se pasa indice a la funcion que luego setea la pagina seleccionada
            mConsultar(index)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim lDsDatos As DataSet

         mLimpiar()
         hdnReasId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
         lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTable, hdnReasId.Text)

         If lDsDatos.Tables(mstrTable).Rows.Count > 0 Then
            With lDsDatos.Tables(mstrTable).Rows(0)
               hdnReasId.Text = .Item("clre_id")
               txtClieAnte.Text = .Item("clre_ante_clie_id")
               usrClieNuevo.Valor = .Item("clre_clie_id")
               txtUsua.Text = .Item("_usuario")
            End With
        End If
        usrClieAnte.Visible = False
        txtClieAnte.Visible = True

        btnGrab.Enabled = False
        btnAutoUsua.Visible = False

        mMostrarPanel(True)

  Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarReasignaciones()

      Dim mstrCmd As String = "exec " + mstrTable + "_consul"
      mstrCmd = mstrCmd + " @clre_ante_clie_id= " + clsSQLServer.gFormatArg(txtClieAnteFil.Text, SqlDbType.Int)
      If Not usrClieNuevoFil.Valor Is DBNull.Value Then
        mstrCmd = mstrCmd + ",@clre_clie_id= " + clsSQLServer.gFormatArg(usrClieNuevoFil.Valor, SqlDbType.Int)
      End If
      If txtFechaDesde.Text <> "" Then
        mstrCmd = mstrCmd + ",@fecha_desde= " + clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime)
      End If
      If txtFechaHasta.Text <> "" Then
        mstrCmd = mstrCmd + ",@fecha_hasta= " + clsSQLServer.gFormatArg(txtFechaHasta.Text, SqlDbType.SmallDateTime)
      End If

      Dim ds As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      grdReas.Visible = True
      grdReas.DataSource = ds
      grdReas.DataBind()
      ds.Dispose()

   End Sub

   Private Sub mLimpiar()
      hdnReasId.Text = ""
      usrClieNuevo.Limpiar()
      usrClieAnte.Limpiar()
      txtClieAnte.Text = ""
      txtUsua.Text = ""
      hdnLoginPop.Text = ""
      grdReas.Visible = False
   End Sub

   Private Function mGuardarCliente() As Data.DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTable, hdnReasId.Text)
      With ldsEsta.Tables(0).Rows(0)
         .Item("clre_clie_id") = usrClieNuevo.Valor
         .Item("clre_ante_clie_id") = usrClieAnte.Valor
         .Item("clre_auti_id") = SRA_Neg.Constantes.AutorizaTipos.ReasignacionClientes
         If hdnLoginPop.Text <> "" Then
            .Item("clre_user") = hdnLoginPop.Text
         Else
            .Item("clre_user") = Session("sUserId").ToString()
         End If
      End With
      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      If (usrClieAnte.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe especificar el cliente a eliminar.")
      End If
      If (usrClieNuevo.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe especificar el cliente por el cual se reemplará.")
      End If
      If (hdnLoginPop.Text = "" And txtUsua.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe especificar el autorizante.")
      End If
   End Sub

   Private Sub mGrabar()
      Try

         mValidarDatos()

         Dim mdsDatos As DataSet = mGuardarCliente()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTable, mdsDatos)

         lobjGenerica.Alta()

         mLimpiar()
         mConsultarReasignaciones()
         mMostrarPanel(False)

      Catch ex As Exception
         mMostrarPanel(True)
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnGrab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrab.Click
      mGrabar()
   End Sub

   Private Sub btnLimpclre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
      mLimpiar()
   End Sub

   Private Sub mMostrarPanel(ByVal pOk As Boolean)
      PanDetalle.Visible = pOk
      grdReas.Visible = Not pOk
   End Sub

   Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mLimpiar()
      mMostrarPanel(False)
   End Sub

   Private Sub mListar()
        Try
            Dim lintFechaDesde As Integer
            Dim lintFechaHasta As Integer
            Dim lstrRptName As String = "ReasignaClientes"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtClieAnteFil.Text <> "" Then
                lstrRpt += "&clre_ante_clie_id=" + txtClieAnteFil.Valor.ToString()
            End If
            If Not usrClieNuevoFil.Valor Is DBNull.Value Then
                lstrRpt += "&clre_clie_id=" + usrClieNuevoFil.Valor.ToString()
            End If

            If txtFechaDesde.Text <> "" Then
                lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
                lstrRpt += "&fecha_desde=" & lintFechaDesde
            End If

            If txtFechaHasta.Text <> "" Then
                lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)
                lstrRpt += "&fecha_hasta=" & lintFechaHasta
            End If

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub
    ' Dario 2013-11-21 se agrega parametro de indice de pagina para hacer el seteo de la misma en la grilla
    Private Sub mConsultar(ByVal IndexPage As Int32)
        mMostrarPanel(False)
        mLimpiar()
        grdReas.CurrentPageIndex = IndexPage
        mConsultarReasignaciones()
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub

    Private Sub mLimpiarFiltro()
        usrClieNuevoFil.Limpiar()
        txtClieAnteFil.Text = ""
        txtFechaDesde.Text = ""
        txtFechaHasta.Text = ""
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
        mListar()
    End Sub

    Private Function mVerificarPermisos() As Boolean
        Dim lstrPermi As String = "0"
        Try
            Dim lstrSql As String

            lstrSql = "usuarios_autoriza_tipos_valida_consul "
            lstrSql = lstrSql + " @usua_id=" + Session("sUserId").ToString()
            lstrSql = lstrSql + ",@auto_usua_id=" + Session("sUserId").ToString()
            lstrSql = lstrSql + ",@auti_id=" + Convert.ToInt32(SRA_Neg.Constantes.AutorizaTipos.ReasignacionClientes).ToString()

            lstrPermi = clsSQLServer.gCampoValorConsul(mstrConn, lstrSql, "salida")

            If lstrPermi <> "0" Then
                txtUsua.Text = clsSQLServer.gCampoValorConsul(mstrConn, "usuarios_consul @usua_id=" + Session("sUserId").ToString(), "_nombre")
            End If
            Return (lstrPermi = "0")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            Return (lstrPermi = "0")
        End Try
    End Function

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        usrClieAnte.Visible = True
        txtClieAnte.Visible = False
        btnGrab.Enabled = True
        mLimpiar()
        btnAutoUsua.Visible = mVerificarPermisos()
        mMostrarPanel(True)
    End Sub

End Class

End Namespace
