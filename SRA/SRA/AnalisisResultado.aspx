<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AnalisisResultado" CodeFile="AnalisisResultado.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Resultados de An�lisis</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="2"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Resultados de An�lisis</asp:label></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
										Width="100%" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" align="right">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																	CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="10"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblEspeciesFil" runat="server" cssclass="titulo">Especies:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbEspeciesFil" class="combo" runat="server" Width="120px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="16" width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2" align="center"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="ares_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="ares_codi" HeaderText="C&#243;digo"></asp:BoundColumn>
											<asp:BoundColumn DataField="ares_desc" HeaderText="Descripcion"></asp:BoundColumn>
											<asp:BoundColumn DataField="_espe_desc" HeaderText="Especie"></asp:BoundColumn>
											<asp:BoundColumn DataField="_resu" HeaderText="Resultado">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="1">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" border="0" cellPadding="0" align="left">
										<TR>
											<TD width="99%" align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
													ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ToolTip="Agregar una Nueva Relaci�n" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD width="100" align="center"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
													ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
											Visible="False" width="100%" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3" align="center">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD width="20%" align="right">
																			<asp:Label id="lblEspecies" runat="server" cssclass="titulo">Especies:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<cc1:combobox id="cmbEspecies" class="combo" runat="server" Width="120px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD width="20%" align="right">
																			<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<cc1:numberbox style="Z-INDEX: 0" id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="False" MaxValor="1000"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD width="20%" align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 19px" width="20%" align="right">
																			<asp:Label id="lblResu" runat="server" cssclass="titulo">Resultado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 19px">
																			<cc1:combobox id="cmbResu" class="combo" runat="server" Width="90px" Obligatorio="True">
																				<asp:ListItem Value="1" Selected="True">Bien</asp:ListItem>
																				<asp:ListItem Value="2">Normal</asp:ListItem>
																				<asp:ListItem Value="3">Cuestionado</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</asp:panel>
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR>
														<TD height="1" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR height="30">
														<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		//if (document.all["txtDesc"]!= null)
			//document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
