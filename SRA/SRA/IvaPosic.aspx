<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.IvaPosic" CodeFile="IvaPosic.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Iva</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language=javascript>
		function mCambiarCombo()
		{
			document.all("cmbDiscri").disabled = false;
			if (document.all("cmbSobre").value=="1")
			{
				document.all("cmbDiscri").value = "1";
				document.all("cmbDiscri").disabled = true;
			}
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25" width="100%">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server">IVA</asp:label>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center" width="100%">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="ivap_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="ivap_desc" HeaderText="Descripción"></asp:BoundColumn>
											<asp:BoundColumn DataField="ivap_letra" HeaderText="Letra"></asp:BoundColumn>
											<asp:BoundColumn DataField="_ivap_reque_cuit" HeaderText="Req.CUIT"></asp:BoundColumn>
											<asp:BoundColumn DataField="_ivap_discri" HeaderText="Discrimina"></asp:BoundColumn>
											<asp:BoundColumn DataField="_ivap_sobre" HeaderText="Sobretasa"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="1"></TD>
								<TD vAlign="middle" width="100%">
									<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar una Nueva División"></CC1:BotonImagen>
								</TD>
								<TD align="right">
									<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
											BorderWidth="1px" Visible="False">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="200px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblRequeCuit" runat="server" cssclass="titulo">Req.CUIT:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbRequeCuit" runat="server" Width="70px" Obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblLetra" runat="server" cssclass="titulo">Letra:</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtLetra" runat="server" cssclass="cuadrotexto" Width="40px" Obligatorio="true"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDiscri" runat="server" cssclass="titulo">Discrimina:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbDiscri" runat="server" Width="70px" Obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblSobre" runat="server" cssclass="titulo">Sobretasa:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<cc1:combobox class="combo" onchange="mCambiarCombo()" id="cmbSobre" runat="server" Width="70px" Obligatorio="true"></cc1:combobox></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel>
										<ASP:PANEL Runat="server" ID="panBotones">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button>&nbsp;&nbsp;&nbsp;
													</TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
