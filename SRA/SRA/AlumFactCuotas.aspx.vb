Namespace SRA

Partial Class AlumFactCuotas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrTabla As String = "alumnos_fact"
   Private mstrTablaDeta As String = "alumnos_fact_deta"
   Private mstrTablaAsiento As String = "facturacion_asiento"
   Private mstrTablaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
   Private mstrInseId As String
	Private mdsDatos As DataSet
	Private mbooGraba As Boolean = False

   Private Enum Columnas As Integer
      lnkEdit = 0
      comp_id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mstrInseId = Request("fkvalor")
         usrAlum.FilInseId = mstrInseId
         hdnImprimir.Text = ""

         If Not Page.IsPostBack Then
            mEstablecerPerfil()
            mInicializar()
            mSetearEventos()
            mCargarCombos()
            clsWeb.gInicializarControles(sender, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mEstablecerPerfil()
   End Sub

   Private Sub mSetearEventos()
      btnAlta.Attributes.Add("onclick", "if(!confirm('Confirma la grabación de las facturas?')) return false;")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "meses", cmbMes, "")
      cmbMes.Valor = Today.Month
   End Sub

   Private Sub mInicializar()
      Dim lstrParaPageSize As String
      clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
      grdDeta.PageSize = Convert.ToInt32(lstrParaPageSize)
      grdConc.PageSize = Convert.ToInt32(lstrParaPageSize)

			txtFecha.Fecha = Today
			txtAnio.Valor = Today.Year
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub grdDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.CurrentPageIndex = E.NewPageIndex
         mConsultarCuotas()

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Public Sub grdConc_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConc.CurrentPageIndex = E.NewPageIndex
         mConsultarConc()

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         hdnId.Text = e.Item.Cells(Columnas.comp_id).Text
         mConsultarConc()
         PanConc.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"

	Private Sub mCerrarConsul()
		mMostrarPanel(False)
	End Sub

	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		panDeta.Visible = pbooVisi
		panBotones.Visible = pbooVisi
		PanConc.Visible = pbooVisi
		panFact.Visible = pbooVisi

	End Sub
#End Region

#Region "Eventos"
	Private Sub btnGene_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGene.Click
		mGenerarFact()
	End Sub

	Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
		mAlta()
	End Sub

	Private Sub imgCerrar_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
		mCerrarConsul()
	End Sub
	Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
		mCerrar()
	End Sub
	Private Sub btnBajaFact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
		mBajaFact()
	End Sub
#End Region

#Region "Metodos privados"
	Private Sub mGenerarFact()
		Try
			Dim lstrFiltros As New System.text.StringBuilder
			Dim ldsDatos As DataSet
			Dim ldrFaca, ldrFade As DataRow
			Dim lMesesInte As Integer
			Dim ldecImpo As Decimal

			mbooGraba = False
			With lstrFiltros
				.Append("exec " + mstrTabla + "_busq")
				.Append(" @inse_id=")
				.Append(mstrInseId)
				.Append(",@anio=")
				.Append(txtAnio.Valor.ToString)
				.Append(",@alum_id=")
				.Append(usrAlum.Valor.ToString())
				.Append(",@mes=")
				.Append(cmbMes.Valor.ToString())
				ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, .ToString)
			End With

			mdsDatos = mCrearDsGenerar()

			For Each ldrTemp As DataRow In ldsDatos.Tables(0).Rows
				ldrFaca = mdsDatos.Tables(mstrTabla).NewRow
				With ldrFaca
					.Item("faca_insc_id") = ldrTemp.Item("insc_id")
					.Item("faca_clie_id") = ldrTemp.Item("insc_fact_clie_id")
					.Item("faca_alum_id") = ldrTemp.Item("alum_id")
					.Item("faca_plco_id") = ldrTemp.Item("plco_id")
					.Item("faca_alum_lega") = ldrTemp.Item("alum_lega")
					.Item("faca_clie_apel") = ldrTemp.Item("clie_apel")
					.Item("faca_carr_desc") = ldrTemp.Item("carr_desc")
					.Item("faca_papl_desc") = ldrTemp.Item("papl_desc")
					.Item("faca_impo") = 0
					.Table.Rows.Add(ldrFaca)
				End With

				With ldrTemp
					'CUOTA
					mAgregarFade(ldrFaca, .Item("papl_cuot_conc_id"), .Item("papl_ccos_id"), .Item("plco_impo"), .Item("conc_desc"))

					If .Item("meses_inte") = 0 And (Not .IsNull("tacl_id") And Not .IsNull("deau_conc_id")) AndAlso .Item("plco_deau_impo") <> 0 Then
						'D.A.
						mAgregarFade(ldrFaca, .Item("deau_conc_id"), .Item("inse_deau_ccos_id"), -1 * .Item("plco_deau_impo"), .Item("deau_conc_desc"))
					End If

					If (Not .IsNull("soci_id") And Not .IsNull("soci_conc_id")) AndAlso .Item("plco_soci_impo") <> 0 Then
						'SOCIO
						mAgregarFade(ldrFaca, .Item("soci_conc_id"), .Item("inse_soci_ccos_id"), -1 * .Item("plco_soci_impo"), .Item("soci_conc_desc"))

						If .Item("soci_suspe") = 1 And Not .IsNull("soci_susp_conc_id") Then
							'SOCIO SUSPENDIDO
							mAgregarFade(ldrFaca, .Item("soci_susp_conc_id"), .Item("inse_soci_susp_ccos_id"), .Item("plco_soci_impo"), .Item("soci_susp_conc_desc"))
						End If
					End If

					'170507 --no se cobra interes por mes de mora 
					' If .Item("meses_inte") > 0 Then
					' INTERESES
					' mAgregarFade(ldrFaca, .Item("inse_inte_conc_id"), .Item("inse_inte_ccos_id"), .Item("inte_impo") * .Item("meses_inte"), .Item("inte_conc_desc"))
                    'End If

                    If Not .IsNull("beca_cuot_conc_id") Then
                        'BECA
                        ldecImpo = IIf(.Item("beca_impo") <> 0, .Item("beca_impo"), Math.Round((ldrFaca.Item("faca_impo") * .Item("beca_porc")) / 100, 2))
                        mAgregarFade(ldrFaca, .Item("beca_cuot_conc_id"), .Item("beca_ccos_id"), -1 * ldecImpo, .Item("beca_conc_desc"))
                    End If
                End With
            Next

        panBotones.Visible = True
        panDeta.Visible = True
        PanConc.Visible = False
        panFact.Visible = True

        mConsultarCuotas()
        mConsultarFact()

        Session(mstrTabla) = mdsDatos

  Catch ex As Exception
			clsError.gManejarError(Me.Page, ex)
		End Try
	End Sub

	Private Sub mAgregarFade(ByVal ldrFaca As DataRow, ByVal pintConcId As Integer, ByVal pintCcosId As Integer, ByVal pdecImpo As Decimal, ByVal pstrDesc As String)
		Dim ldrFade As DataRow = mdsDatos.Tables(mstrTablaDeta).NewRow
		With ldrFade
			.Item("fade_faca_id") = ldrFaca.Item("faca_id")
			.Item("fade_conc_id") = pintConcId
			.Item("fade_ccos_id") = pintCcosId
			.Item("fade_conc_desc") = pstrDesc
			.Item("fade_impo") = pdecImpo

			.Table.Rows.Add(ldrFade)

			ldrFaca.Item("faca_impo") += .Item("fade_impo")
		End With
	End Sub

	Private Sub mConsultarCuotas()
		grdDeta.DataSource = mdsDatos.Tables(mstrTabla)
		grdDeta.DataBind()
	End Sub

	Private Sub mConsultarFact()
		grdFact.DataSource = mObtenerDsComp()
		grdFact.DataBind()
	End Sub

	Private Sub mConsultarConc()
		mdsDatos.Tables(mstrTablaDeta).DefaultView.RowFilter = "fade_faca_id = " + hdnId.Text
		grdConc.DataSource = mdsDatos.Tables(mstrTablaDeta).DefaultView
		grdConc.DataBind()
	End Sub

	Private Function mCrearDsGenerar() As DataSet
		Dim ldsDatos As New DataSet
		Dim lintCol As DataColumn

		ldsDatos.Tables.Add(mstrTabla)
		ldsDatos.Tables.Add(mstrTablaDeta)

		With ldsDatos.Tables(mstrTabla)
			lintCol = .Columns.Add("faca_id", Type.GetType("System.Int32"))
			lintCol.AllowDBNull = False
			lintCol.Unique = True
			lintCol.AutoIncrement = True

			.Columns.Add("faca_insc_id", System.Type.GetType("System.Int32"))
			.Columns.Add("faca_clie_id", System.Type.GetType("System.Int32"))
			.Columns.Add("faca_alum_id", System.Type.GetType("System.Int32"))
			.Columns.Add("faca_plco_id", System.Type.GetType("System.Int32"))
			.Columns.Add("faca_alum_lega", System.Type.GetType("System.Double"))
			.Columns.Add("faca_clie_apel", System.Type.GetType("System.String"))
			.Columns.Add("faca_papl_desc", System.Type.GetType("System.String"))
			.Columns.Add("faca_carr_desc", System.Type.GetType("System.String"))
			.Columns.Add("faca_impo", System.Type.GetType("System.Double"))

			.Columns.Add("fade_impo", System.Type.GetType("System.Double"))
			.Columns.Add("fade_conc_id", System.Type.GetType("System.Int32"))
			.Columns.Add("fade_ccos_id", System.Type.GetType("System.Int32"))
		End With

		With ldsDatos.Tables(mstrTablaDeta)
			lintCol = .Columns.Add("fade_id", Type.GetType("System.Int32"))
			lintCol.AllowDBNull = False
			lintCol.Unique = True
			lintCol.AutoIncrement = True

			.Columns.Add("fade_faca_id", System.Type.GetType("System.Int32"))
			.Columns.Add("fade_impo", System.Type.GetType("System.Double"))
			.Columns.Add("fade_conc_id", System.Type.GetType("System.Int32"))
			.Columns.Add("fade_ccos_id", System.Type.GetType("System.Int32"))
			.Columns.Add("fade_conc_desc", System.Type.GetType("System.String"))
		End With
		Return ldsDatos
	End Function

	Private Sub mLimpiar()
		txtAnio.Valor = Today.Year
		cmbMes.Valor = Today.Month

		PanConc.Visible = False
		panDeta.Visible = False
		panFact.Visible = False
		panBotones.Visible = False
		grdDeta.CurrentPageIndex = 0
		grdDeta.DataSource = Nothing
		grdDeta.DataBind()

		grdConc.CurrentPageIndex = 0
		grdConc.DataSource = Nothing
		grdConc.DataBind()

		Session(mstrTabla) = Nothing
		mdsDatos = Nothing
		mbooGraba = False
	End Sub

	Private Sub mAlta()
		Try
			mbooGraba = True
			Dim ldsDatos As DataSet = mObtenerDsComp()
			Dim lobjNeg As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), SRA_Neg.Constantes.gTab_Comprobantes, ldsDatos)
            lobjNeg.ActualizaMulRelConnection()

			hdnImprimir.Text = lobjNeg.gstrMultiplesId

			mLimpiar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mBajaFact()
		Try
			For Each ldrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select("fade_faca_id=" & hdnId.Text)
				ldrDeta.Delete()
			Next

			mdsDatos.Tables(mstrTabla).Select("faca_id=" & hdnId.Text)(0).Delete()
			mCerrar()
			mConsultarCuotas()
			mConsultarFact()

		Catch ex As Exception
			clsError.gManejarError(Me.Page, ex)
		End Try
	End Sub

	Private Sub mCerrar()
		hdnId.Text = ""
		PanConc.Visible = False
		grdConc.CurrentPageIndex = 0
		grdConc.DataSource = Nothing
		grdConc.DataBind()

	End Sub

	Private Function mObtenerDsComp() As DataSet
		Dim ldsDatos As DataSet
		Dim ldrComp, ldrCode, ldrCoco, ldrCopc, ldrCovt, ldrAsie As DataRow
		Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
		Dim lstrHost, lstrCemiNume, lstrEmctId As String
		Dim lintActiId, lintClieIdAnt As Integer
		Dim ldecFactTotal As Double

		oFact.CentroEmisorNro(mstrConn)
		lstrCemiNume = oFact.pCentroEmisorNro
		lstrEmctId = oFact.pCentroEmisorId
		lstrHost = oFact.pHost
		lintActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", mstrInseId).Tables(0).Rows(0).Item("inse_acti_id")

		ldsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, "")
		ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Rows(0).Delete()

		SRA_Neg.Utiles.gAgregarTabla(mstrConn, ldsDatos, SRA_Neg.Constantes.gTab_ComprobPagosCuotas)

		ldsDatos.Tables.Add(mstrTablaAsiento)
		With ldsDatos.Tables(mstrTablaAsiento)
			.Columns.Add("proc_id", System.Type.GetType("System.Int32"))
			.Columns.Add("proc_comp_id", System.Type.GetType("System.Int32"))
			.Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
		End With

		For Each ldrFaca As DataRow In mdsDatos.Tables(mstrTabla).Select("", "faca_clie_id")
			If lintClieIdAnt <> ldrFaca.Item("faca_clie_id") Then
				ldrComp = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).NewRow

				'COMPROBANTES
				With ldrComp
					.Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
					.Item("comp_fecha") = txtFecha.Fecha
					.Item("comp_ingr_fecha") = .Item("comp_fecha")
					.Item("comp_impre") = 0
					.Item("comp_clie_id") = ldrFaca.Item("faca_clie_id")
					.Item("comp_dh") = 0					 '0 = debe
					.Item("comp_cance") = 0					' 0 = pendiente
					.Item("comp_cs") = 1					' cta cte
					.Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Factura
					.Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Alumnos_FacturacionCuotas
					.Item("comp_mone_id") = 1
					.Item("comp_neto") = ldrFaca.Item("faca_impo")
					ldecFactTotal = ldecFactTotal + ldrFaca.Item("faca_impo")
					.Item("comp_letra") = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("_ivap_letra")
					.Item("comp_cpti_id") = 2					' 2 = cta.cte
					.Item("comp_acti_id") = lintActiId
					.Item("comp_cemi_nume") = lstrCemiNume
					.Item("comp_emct_id") = lstrEmctId
					.Item("comp_host") = lstrHost
					.Item("_comp_desc") = clsSQLServer.gObtenerEstruc(mstrConn, "clientesX", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("clie_apel")
					.Table.Rows.Add(ldrComp)
				End With

				'COMPROB_DETA
				ldrCode = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobDeta).NewRow
				With ldrCode
					.Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
					.Item("code_comp_id") = ldrComp.Item("comp_id")
					.Item("code_coti") = 1
                    '.Item("code_obse") = "Cuota Alumno"

					.Item("code_pers") = True
					.Item("code_impo_ivat_tasa") = 0
					.Item("code_impo_ivat_tasa_redu") = 0
					.Item("code_impo_ivat_tasa_sobre") = 0
					.Item("code_impo_ivat_perc") = 0

					.Table.Rows.Add(ldrCode)
				End With

				'COMPROB_VTOS
				ldrCovt = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).NewRow
				With ldrCovt
					.Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
					.Item("covt_comp_id") = ldrComp.Item("comp_id")
					.Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, ldrComp.Item("comp_acti_id"), ldrComp.Item("comp_fecha"), ldrFaca.Item("faca_plco_id"))

					.Item("covt_porc") = 100
					.Item("covt_impo") = ldrComp.Item("comp_neto")
					.Item("covt_cance") = 0

					.Table.Rows.Add(ldrCovt)
				End With


				'ASIENTOS
				ldrAsie = ldsDatos.Tables(mstrTablaAsiento).NewRow
				With ldrAsie
					.Item("proc_id") = clsSQLServer.gObtenerId(.Table, "proc_id")
					.Item("proc_comp_id") = ldrComp.Item("comp_id")
					.Table.Rows.Add(ldrAsie)
				End With
			Else
				ldrComp.Item("comp_neto") += ldrFaca.Item("faca_impo")
				ldrCovt.Item("covt_impo") = ldrComp.Item("comp_neto")
			End If

			lintClieIdAnt = ldrFaca.Item("faca_clie_id")

			'COMPROB_PAGOS_CUOTAS
			ldrCopc = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobPagosCuotas).NewRow
			ldrCopc.Item("copc_id") = clsSQLServer.gObtenerId(ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobPagosCuotas), "copc_id")

			With ldrCopc
				.Item("copc_comp_id") = ldrComp.Item("comp_id")
				.Item("copc_plco_id") = ldrFaca.Item("faca_plco_id")
				.Item("copc_alum_id") = ldrFaca.Item("faca_alum_id")
				.Item("copc_insc_id") = ldrFaca.Item("faca_insc_id")
				.Item("copc_impo") = ldrFaca.Item("faca_impo")

				.Table.Rows.Add(ldrCopc)
			End With

			'COMPROB_CONCEPTOS
			For Each ldrFade As DataRow In mdsDatos.Tables(mstrTablaDeta).Select("fade_faca_id=" & ldrFaca.Item("faca_id").ToString)
				With ldsDatos.Tables(mstrTablaConcep).Select("coco_comp_id=" + ldrComp.Item("comp_id").ToString + " AND coco_conc_id=" + ldrFade.Item("fade_conc_id").ToString)
					If .GetUpperBound(0) = -1 Then
						ldrCoco = ldsDatos.Tables(mstrTablaConcep).NewRow
						With ldrCoco
							.Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
							.Item("coco_comp_id") = ldrComp.Item("comp_id")
							.Item("coco_conc_id") = ldrFade.Item("fade_conc_id")
							.Item("coco_ccos_id") = ldrFade.Item("fade_ccos_id")
							.Item("coco_impo") = 0
							.Table.Rows.Add(ldrCoco)
						End With
					Else
						ldrCoco = .GetValue(0)
					End If
				End With

				With ldrCoco
					.Item("coco_impo") += ldrFade.Item("fade_impo")
					.Item("coco_impo_ivai") = .Item("coco_impo")
				End With
			Next
		Next

		If Not mbooGraba Then
			If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Rows.Count > 0 Then			 'Agrego la fila de total
				ldrComp = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).NewRow
				ldrComp.Item("_comp_desc") = "Total"
				ldrComp.Item("comp_neto") = ldecFactTotal
				ldrComp.Table.Rows.Add(ldrComp)
			End If
		End If


		mCancelaFacturasNegativas(ldsDatos)

		Return (ldsDatos)
	End Function

	Private Sub mCancelaFacturasNegativas(ByVal pdsDatos As DataSet)
		Dim ldecNeto As Decimal

  For Each ldrComp As DataRow In pdsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Select("comp_neto<=0 and comp_id is not null")
   'COMPROBANTES
   With ldrComp
    ldecNeto = .Item("comp_neto")
    .Item("comp_neto") = 0
    .Item("comp_cance") = 1     ' 1 = cancelado

    'COMPROB_VTOS
    For Each ldrCovt As DataRow In pdsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).Select("covt_comp_id=" + .Item("comp_id").ToString)
     With ldrCovt
      .Item("covt_porc") = 100
      .Item("covt_impo") = 0
      .Item("covt_cance") = 1
     End With
    Next

    'COMPROB_PAGOS_CUOTAS
    For Each ldrCopc As DataRow In pdsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobPagosCuotas).Select("copc_comp_id=" + .Item("comp_id").ToString)
     With ldrCopc
      .Item("copc_impo") = 0
     End With
    Next

    'COMPROB_CONCEPTOS
    For Each ldrCoco As DataRow In pdsDatos.Tables(mstrTablaConcep).Select("coco_comp_id=" + .Item("comp_id").ToString + " AND coco_impo<0", "coco_id DESC")
     If ldecNeto >= 0 Then Exit For
     With ldrCoco
      If ldecNeto <= .Item("coco_impo") Then
       ldecNeto += .Item("coco_impo")
       .Delete()
      Else
       .Item("coco_impo") += Math.Abs(ldecNeto)
       Exit For
      End If
     End With
    Next
   End With
  Next
	End Sub
#End Region
End Class
End Namespace
