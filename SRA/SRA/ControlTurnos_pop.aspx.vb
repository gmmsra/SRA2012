Namespace SRA

Partial Class ControlTurnos_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()

   End Sub

#End Region

   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label

#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String
   Public mstrTitulo As String
   Public mstrFiltros As String
   Private mstrConn As String
   Private mbooEsConsul As Boolean

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultarCabecera()
            mConsultar(False)
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrTitulo = Request.QueryString("titulo")
      mstrFiltros = Request.QueryString("filtros")
      If Not Request.QueryString("EsConsul") Is Nothing Then
         mbooEsConsul = CBool(CInt(Request.QueryString("EsConsul")))
      End If

      If mbooEsConsul Then
         grdConsulta.Columns(0).Visible = False
      End If
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mConsultarCabecera()
      Dim ds As New DataSet
      mstrCmd = "exec ClientesX_busq "
      mstrCmd += mstrFiltros
      ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      'ds = mConsDatos("ClientesX", mstrFiltros)
      With ds.Tables(0).Rows(0)
         txtNomb.Valor = .Item("clie_apel")
         txtDoc.Valor = .Item("docu")
      End With
   End Sub

   Private Sub mGrabar()
      
      Dim lstrId As String

      Try

         lstrId = ""
         For Each oDataItem As DataGridItem In grdConsulta.Items
            If DirectCast(oDataItem.FindControl("chkSele"), CheckBox).Checked Then
               If lstrId <> "" Then lstrId = lstrId & ";"
               lstrId = lstrId & oDataItem.Cells(1).Text
            End If
         Next

         For i As Integer = 0 To Session("mulPaginas")
            If Not Session("mulPag" & i.ToString) Is Nothing Then
               Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
               If i <> grdConsulta.CurrentPageIndex And lstrIdsSess <> "" Then
                  If lstrId <> "" Then lstrId = lstrId & ";"
                  lstrId = lstrId & lstrIdsSess
               End If
               Session("mulPag" & i.ToString) = Nothing
            End If
         Next

         If lstrId <> "" Then

            mstrCmd = "exec lab_turnos_ctrol_baja "
            mstrCmd = mstrCmd & " " & clsSQLServer.gFormatArg(lstrId, SqlDbType.VarChar)
            mstrCmd = mstrCmd & "," & Session("sUserId").ToString()
            clsSQLServer.gExecute(mstrConn, mstrCmd)

            mConsultar(False)
            
            'Dim lsbMsg As New StringBuilder
            'lsbMsg.Append("<SCRIPT language='javascript'>")
            'lsbMsg.Append("window.close();</SCRIPT>")
            'Response.Write(lsbMsg.ToString)
        End If

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try

   End Sub

   Private Sub mSeleccionItem(ByVal pstrId As String, Optional ByVal pbooTarjVencida As Boolean = False)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosTarjPop'].value='{0}';", pstrId))
      lsbMsg.Append(IIf(pbooTarjVencida, "window.opener.document.all['hdnTarjVenc'].value='1';", "window.opener.document.all['hdnTarjVenc'].value='0';"))
      lsbMsg.Append("window.opener.__doPostBack('hdnDatosTarjPop','');")

      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
   Private Function mConsDatos(ByVal pstrTabla As String, ByVal pstrFiltro As String) As DataSet
      mstrCmd = "exec " + pstrTabla + "_busq "
      mstrCmd += mstrFiltros

      Dim ds As New DataSet
      ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      Return ds
   End Function
   Public Sub mConsultar(ByVal pbooPage As Boolean)

      Try

         mstrCmd = "exec " & mstrTabla & "_busq @baja='N'"
         If mstrFiltros <> "" Then
            mstrCmd = mstrCmd & ",@clie_id=" & mstrFiltros
         Else
            mstrCmd = mstrCmd & ",@clie_id=-1"
            txtNomb.Text = ""
         End If

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdConsulta)


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region



   Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

   Private Sub hdnOK_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      If hdnOK.Text <> "" Then
         mSeleccionItem(hdnOK.Text)
      End If
   End Sub

   Private Sub btnAcep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAcep.Click
      mGrabar()
   End Sub

End Class

End Namespace

