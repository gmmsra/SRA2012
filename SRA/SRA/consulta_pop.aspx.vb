Imports System.Data.SqlClient


Namespace SRA


Partial Class consulta_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnBusc As NixorControls.BotonImagen
    Protected WithEvents btnLimpFil As NixorControls.BotonImagen

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo. 
        InitializeComponent()

        If Page.IsPostBack Then
            For i As Integer = 0 To 2
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                grdConsulta.Columns.Add(dgCol)
            Next
        End If
    End Sub

#End Region

    Protected WithEvents hdnsinOtorgHBA As System.Web.UI.WebControls.TextBox



#Region "Definici�n de Variables"
    Public mstrCmd As String
    Public mstrTabla As String
    Public mstrHdn As String
    Public mstrTitulo As String
    Public mstrFiltros As String
    Public mstrSexo As String
    Public mstrRaza As String
    Public mstrRpt As String
    Public mstrReporte As String
    Private mstrConn As String
    Private mbooEsConsul As Boolean
    Private mbooAutopostback As Boolean
    Private mstrTituProd As String
    Private mboolSoloConsulta As Boolean = False
    Private mstrSoloConsulta As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If Not Page.IsPostBack Then
                mConsultar(True) ' Dario 2014-12-09 true por false
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        
        If Request.QueryString("tabla") = "PR" Then
            mstrTabla = "productos_relaciones"
        Else
            mstrTabla = Request.QueryString("tabla")
        End If
        mstrTitulo = Request.QueryString("titulo")
        mstrFiltros = Request.QueryString("filtros")
        mstrSexo = Request.QueryString("prdt_sexo")
        mstrRaza = Request.QueryString("prdt_raza")
        mboolSoloConsulta = False
        mstrSoloConsulta = Request.QueryString("SC")


        If Not Request.QueryString("SC") Is Nothing Then
            If Request.QueryString("SC") <> "" Then


                If mstrSoloConsulta = "TRUE" Or mstrSoloConsulta = "T" Then

                    mboolSoloConsulta = True
                Else

                    mboolSoloConsulta = False
                End If


            End If
        End If




        If Not Session("mensajeConsulta") Is Nothing Then
            hdnsinOtorgHBA.text = Session("mensajeConsulta").ToString()
        End If


        hdnSoloConsulta.Text = mboolSoloConsulta.ToString()
        mstrHdn = Request.QueryString("hdn")
        mstrRpt = IIf(Request.QueryString("rpt") = "", "P", "L")
        mstrReporte = Request.QueryString("reporte")
        If IsNothing(mstrReporte) Then
            btnList.Enabled = False
        Else
            btnList.Enabled = True
        End If

        lblTituAbm.Text = mstrTitulo
        If Request.QueryString("tituprod") <> "" Then
            lblTituProd.Text = mConsultarTituloProducto()
        Else
            mstrTituProd = Request.QueryString("tituprod")
        End If

        If Not Request.QueryString("EsConsul") Is Nothing Then
            mbooEsConsul = CBool(CInt(Request.QueryString("EsConsul")))
        End If

        If Not Request.QueryString("Autopostback") Is Nothing Then
            mbooAutopostback = CBool(CInt(Request.QueryString("Autopostback")))
        Else
            mbooAutopostback = True
        End If

        If mbooEsConsul Then
            grdConsulta.Columns(0).Visible = False
        End If

        If mstrTitulo.ToUpper() = "HIJOS" Then
            trHijos.Style.Add("display", "inline")
            trFiltroHijos.Style.Add("display", "inline")
            trFiltro.Style.Add("display", "inline")
            ' Dario 2014-11-13    
            usrClieFil.RazaId = mstrRaza
            usrClieFil.cmbCriaRazaExt.Enabled = False
        Else
            trHijos.Style.Add("display", "none")
            trFiltroHijos.Style.Add("display", "none")
            trFiltro.Style.Add("display", "none")
        End If

        grdConsulta.PageSize = 100 'NO PAGINAR PARA PODER IMPRIMIR TODO EL CONTENIDO

        If (mstrRpt = "P") Then
            btnImpri.Attributes.Add("onclick", "mImprimir();return(false);")
        ElseIf (mstrRpt = "L") Then
            btnImpri.Visible = False
            btnList.Visible = True
            lblTituProd.Visible = True
        End If

    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConsulta.EditItemIndex = -1
            If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
                grdConsulta.CurrentPageIndex = 0
            Else
                grdConsulta.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultar(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdHijos_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdHijos.EditItemIndex = -1
            If (grdHijos.CurrentPageIndex < 0 Or grdHijos.CurrentPageIndex >= grdHijos.PageCount) Then
                grdHijos.CurrentPageIndex = 0
            Else
                grdHijos.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultar(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

        Dim lsbMsg As New StringBuilder

        If mstrTabla <> "establecimientos_expedientes" Then
            lsbMsg.Append("<SCRIPT language='javascript'>")

            If mstrHdn = "" Then
                mstrHdn = "hdnDatosPop"
            End If

            lsbMsg.Append(String.Format("window.opener.document.all['" & mstrHdn & "'].value='{0}';", E.Item.Cells(1).Text))
            If mbooAutopostback Then
                lsbMsg.Append("window.opener.__doPostBack('" & mstrHdn & "','');")
            End If

            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)
        End If

    End Sub

    Public Function mConsultarTituloProducto() As String
        Try
            'Ejecuta el Stored Procedure.
            Dim myConnection As New SqlConnection(mstrConn)
            Dim cmdExecCommand As New SqlCommand
            cmdExecCommand.Connection = myConnection
            cmdExecCommand.CommandType = CommandType.Text
            cmdExecCommand.CommandText = " SELECT prdt_nomb FROM Productos WHERE prdt_id = " + mstrFiltros.Substring(0, mstrFiltros.IndexOf(","))

            myConnection.Open()
            Dim strNombre As String = cmdExecCommand.ExecuteScalar()
            myConnection.Close()

            Return (strNombre.Trim.ToString())

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

    Public Sub mConsultar(ByVal pbooPage As Boolean)
        Try
            mstrCmd = "exec " + mstrTabla + "_busq "
            mstrCmd += mstrFiltros

            If mstrTitulo.ToUpper() = "HIJOS" And usrClieFil.Valor.ToString() <> "0" Then
                mstrCmd += ", @prop_cria_id =" + usrClieFil.Valor.ToString()
            End If

            Dim ds As New DataSet
            Dim lintFila As Integer
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            If pbooPage Then
                Dim i As Integer = grdConsulta.Columns.Count - 1
                While i > 0
                    grdConsulta.Columns.Remove(grdConsulta.Columns(i))
                    i -= 1
                End While
            End If
            Dim intCol As Integer = 0

            For Each dc As DataColumn In ds.Tables(0).Columns
                intCol = intCol + 1
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                dgCol.DataField = dc.ColumnName
                If mstrTabla = "productos_relaciones" Then
                    Select Case dc.ColumnName.ToUpper
                        Case "PAREJA"
                            dgCol.HeaderText = IIf(mstrSexo = "1", "Madre", "Padre")
                        Case "NRO"
                            dgCol.HeaderText = clsSQLServer.gConsultarValor("razas_especie_consul @raza_id=" + mstrRaza, mstrConn, 3)
                        Case Else
                            dgCol.HeaderText = dc.ColumnName
                    End Select
                Else
                    dgCol.HeaderText = dc.ColumnName
                End If
                If dc.Ordinal = 0 Then
                    dgCol.Visible = False
                End If
                If (LCase(dgCol.HeaderText) <> "_nacimiento") Then
                    grdConsulta.Columns.Add(dgCol)
                End If
            Next
            grdConsulta.DataSource = ds
            grdConsulta.DataBind()
            ds.Dispose()

            If mstrTitulo.ToUpper() = "HIJOS" Then

                mstrCmd = "exec  productos_relaciones_por_anio "
                Dim sfiltros As String

                If usrClieFil.Valor.ToString() <> "0" Then
                    sfiltros += ", null,@cria_id=" + usrClieFil.Valor.ToString()
                End If

                mstrCmd += mstrFiltros + sfiltros

                Dim dsHijos As New DataSet

                dsHijos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

                If pbooPage Then
                    Dim i As Integer = grdHijos.Columns.Count - 1
                    While i > 0
                        grdHijos.Columns.Remove(grdHijos.Columns(i))
                        i -= 1
                    End While
                End If

                intCol = 0

                For Each dc As DataColumn In dsHijos.Tables(0).Columns
                    intCol = intCol + 1
                    Dim dgCol As New Web.UI.WebControls.BoundColumn
                    dgCol.DataField = dc.ColumnName
                    If mstrTabla = "productos_relaciones" Then
                        Select Case dc.ColumnName.ToUpper
                            Case "PAREJA"
                                dgCol.HeaderText = IIf(mstrSexo = "1", "Madre", "Padre")
                            Case "NRO"
                                dgCol.HeaderText = clsSQLServer.gConsultarValor("razas_especie_consul @raza_id=" + mstrRaza, mstrConn, 3)
                            Case Else
                                dgCol.HeaderText = dc.ColumnName
                        End Select
                    Else
                        dgCol.HeaderText = dc.ColumnName
                    End If
                    'If dc.Ordinal = 0 Then
                    '    dgCol.Visible = False
                    'End If

                    grdHijos.Columns.Add(dgCol)
                Next
                grdHijos.DataSource = dsHijos
                grdHijos.DataBind()
                dsHijos.Dispose()
            End If

            Select Case mstrTabla
                Case "establecimientos_expedientes"
                    For lintFila = 0 To grdConsulta.Items.Count - 1
                        grdConsulta.Items(lintFila).Cells(0).Text = "<img border=0 src='images/edit.gif' style='cursor:hand;' onclick=javascript:mEstablecimiento('" & grdConsulta.Items(lintFila).Cells(1).Text & "');>"
                    Next lintFila
                Case "productos_relaciones"
                    For lintFila = 0 To grdConsulta.Items.Count - 1
                        grdConsulta.Items(lintFila).Cells(0).Text = "<img border=0 src='images/edit.gif' style='cursor:hand;' onclick=javascript:mRelaciones('" & grdConsulta.Items(lintFila).Cells(1).Text & "');>"
                    Next lintFila
            End Select

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            If grdConsulta.Items.Count > 0 Then
                Dim lstrRptName As String = mstrReporte
                Dim vStrFiltros() As String

                Dim sFil As String

                Dim oCriadores As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())

                sFil = mstrFiltros.Replace("'", "")
                vStrFiltros = sFil.Split(",")
                If Not IsNothing(mstrReporte) Then

                    Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    Select Case lstrRptName
                        Case "ListadoPropietarios"
                            lstrRpt += "&prdp_prdt_id=" + vStrFiltros(0)
                            lstrRpt += "&nomb=" + mstrTituProd.ToString
                        Case "ListadoHijos"
                            lstrRpt += "&ptre_prdt_id=" + vStrFiltros(0)
                            lstrRpt += "&tipo=" + IIf(vStrFiltros(1) = 0, "False", "True")

                            Dim nomMadre As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "productos", "prdt_nomb", " @prdt_id=" & vStrFiltros(0))
                            lstrRpt += "&madre=" + nomMadre

                            If usrClieFil.Valor.ToString() <> "0" Then
                                lstrRpt += "&prop_cria_id=" + usrClieFil.Valor.ToString()
                                lstrRpt += "&propDescripcion=" + usrClieFil.Apel


                            End If


                        Case "ListadoErrores"
                            lstrRpt += "&prdt_id=" + vStrFiltros(0)
                            lstrRpt += "&nomb=" + mstrTituProd.ToString
                        Case "ListadoParejas"
                            lstrRpt += "&prdt_id=" + vStrFiltros(0)
                            lstrRpt += "&nomb=" + mstrTituProd.ToString
                        Case "ListadoPremios"
                            lstrRpt += "&prdt_id=" + vStrFiltros(0)
                            lstrRpt += "&nomb=" + mstrTituProd.ToString
                        Case "PerformCarne"
                            lstrRpt += "&pfca_prdt_id=" + vStrFiltros(0)
                    End Select

                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click

    End Sub

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click

    End Sub

    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
        mConsultar(True) ' Dario 2014-12-09 true por false
    End Sub

    Private Sub btnLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        usrClieFil.Limpiar()
        mConsultar(True) 'Dario 2014-12-09 true por false
    End Sub
End Class

End Namespace
