<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Exposiciones" CodeFile="Exposiciones.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Exposiciones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<body class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="TableABM" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Exposiciones</asp:label></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 56px" height="56"></TD>
								<TD style="HEIGHT: 56px" vAlign="top" height="56"><asp:datagrid id="grdDato" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
										ItemStyle-Height="5px" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px"
										AllowPaging="True" BorderStyle="None" width="100%">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
														Height="5">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="expo_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="expo_codi" HeaderText="C�digo">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="expo_desde_fecha" HeaderText="Fecha de Inicio" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="expo_hasta_fecha" HeaderText="Fecha de Cierre" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cierre_fact" HeaderText="Cierre Facturaci�n">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageDisable="btnNuev0.gif" ForeColor="Transparent"
													ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
													ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar Nueva Exposicion" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD vAlign="top">
									<DIV align="left"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
											Visible="False" Width="100%">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 282px"></TD>
														<TD></TD>
														<TD vAlign="top" align="right" width="15">
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 282px; HEIGHT: 16px" align="right">
															<asp:Label id="lblCodiNum" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" align="left">
															<CC1:NUMBERBOX id="txtCodiNum" runat="server" cssclass="cuadrotexto" Width="136px" obligatorio="true"></CC1:NUMBERBOX></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 282px; HEIGHT: 16px" align="right">
															<asp:Label id="lblFechaInicio" runat="server" cssclass="titulo">Fecha de Inicio:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" align="left">
															<cc1:DateBox id="txtFechaInicio" runat="server" cssclass="cuadrotexto" Width="70px" obligatorio="true"
																AceptaNull="True"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 282px; HEIGHT: 16px" align="right">
															<asp:Label id="lblFechaCierre" runat="server" cssclass="titulo">Fecha de Cierre:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" align="left">
															<cc1:DateBox id="txtFechaCierre" runat="server" cssclass="cuadrotexto" Width="70px" obligatorio="true"
																AceptaNull="True"></cc1:DateBox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 282px; HEIGHT: 21px" align="right">
															<asp:Label id="lblCierreFact" runat="server" cssclass="titulo">Cierre de Facturaci�n:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 21px" colSpan="2" height="21">
															<cc1:combobox class="combo" id="cmbCierreFact" runat="server" Width="50px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="center" colSpan="3">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center" colSpan="3"><A id="editar" name="editar"></A>
															<asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnPage" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnId" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
		</SCRIPT>
		</A>
	</body>
</HTML>
