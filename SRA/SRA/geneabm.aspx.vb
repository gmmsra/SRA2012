' Dario 2013-09-04 cambio por 14% de lab bonif
' Dario 2013-10-11 cambio por 14% de lab bonif cmbCCosConcepBonifLab
Imports System.Data.SqlClient


Namespace SRA


Partial Class geneabm
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub










   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String
    Public mstrGpAran As String
    Private mstrConn As String
    Public mstrTitu As String
    Private mstrPKey As String
    Private mstrOpc As String
    Private mstrDeno As String
    Private mstrCmd As String
    Private mintId As Integer
    Private mintCodi As Integer
    Private mintDesc As Integer
    Private mintDefa As Integer
    Private mintAdic As Integer
    Private mintAdic2 As Integer
    Private mintAdic3 As Integer
    Private mintAdic4 As Integer
    Private mintAdic5 As Integer
    Private mintEstado As Integer
    Private mintGriFechaBaja As Integer
    Private mintFechaBaja As Integer
    Private mbooCodi As Boolean
    Private mbooCodiNum As Boolean
    Private mstrFK As String
    Private mstrFKDeno As String
    Private mstrFKId As String
    Private mstrFKvalor As String
    Private mstrDefa As String
    Private mstrDefadeno As String
    Private mstrdefacampo As String
    Public mstrAdic As String
    Private mstrAdicDeno As String
    Private mstrAdicType As String
    Private mstrAdicTabla As String
    Private mstrAdicPrec As String

    Public mstrAdic2 As String
    Private mstrAdicDeno2 As String
    Private mstrAdicType2 As String
    Private mstrAdicPrec2 As String
    'GSZ  22/05/2015
    Public mstrAdic3 As String
    Private mstrAdic3Deno As String
    Private mstrAdic3Type As String
    Private mstrAdic3Prec As String
    'GSZ  22/05/2015
    Public mstrAdic4 As String
    Private mstrAdic4Deno As String
    Private mstrAdic4Type As String
    Private mstrAdic4Prec As String



    'GSZ  28/05/2015
    Public mstrAdic5 As String
    Private mstrAdic5Deno As String
    Private mstrAdic5Type As String
    Private mstrAdic5Prec As String


    Private mstrParaPageSize As Integer
    Private mstrItemCons As String
    Private mstrDesc2 As String
    Private mstrDescDeno As String
    Private mstrFiltro As String
    Private mstrFiltroDeno As String
	Private mstrFiltroConsul As String = ""
	Private mstrFiltroConsulRPT As String = ""
	Private mstrFiltroRpt As String = ""
    Private mstrCmbOpc As String
#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mCargarCentroCostos()

            If Not mbooCodi And Not mbooCodiNum Then
               grdDato.Columns(mintCodi).Visible = False
               lblCodi.Visible = False
            End If

            If Not mbooCodi Then
               txtCodi.Visible = False
            End If

            If Not mbooCodiNum Then
               txtCodiNum.Visible = False
            End If

                If mstrAdic = "" Then
                    grdDato.Columns(mintAdic).Visible = False
                    lblAdic.Visible = False
                    txtAdicNum.Visible = False
                    txtAdic.Visible = False
                End If

                If mstrAdic2 = "" Then
                    grdDato.Columns(mintAdic2).Visible = False
                    lblAdic2.Visible = False
                    txtAdicNum2.Visible = False
                    txtAdic2.Visible = False
                    cmbAdic2.Visible = False
                End If
                'GSZ  22/05/2015

                If mstrAdic3 = "" Then
                    grdDato.Columns(mintAdic3).Visible = False
                    lblAdic3.Visible = False
                    txtAdic3Num.Visible = False
                    txtAdic3.Visible = False
                    cmbAdic3.Visible = False
                End If


                ''GSZ  22/05/2015
                If mstrAdic4 = "" Then
                    grdDato.Columns(mintAdic4).Visible = False
                    lblAdic4.Visible = False
                    txtAdic4Num.Visible = False
                    txtAdic4.Visible = False
                    cmbAdic4.Visible = False
                End If

                ''GSZ  28/05/2015
                If mstrAdic5 = "" Then
                    grdDato.Columns(mintAdic5).Visible = False
                    lblAdic5.Visible = False
                    txtAdic5Num.Visible = False
                    txtAdic5.Visible = False
                    cmbAdic5.Visible = False
                End If


                If mstrFK = "" Then
                    lblFK.Visible = False
                    cmbFK.Visible = False
                Else
                    clsWeb.gCargarRefeCmb(mstrConn, mstrFK, cmbFK, "")
                End If

                If mstrDefa = "" Then
                    grdDato.Columns(mintDefa).Visible = False
                    lblDefa.Visible = False
                    chkDefa.Visible = False
                End If

                ' Dario 2013-09-04 cambio por 14% de lab bonif
                If (mstrGpAran = "") Then
                    Me.lblProcBonifPorCantLab1.Visible = False
                    Me.lblCantidadABonificar1.Visible = False
                    Me.txtProcBonifPorCantLab1.Visible = False
                    If mstrAdic2 = "" Then
                        grdDato.Columns(mintAdic2).Visible = False
                    Else
                        grdDato.Columns(mintAdic2).Visible = True
                    End If
                    Me.txtCantidadABonificar1.Visible = False
                    grdDato.Columns(mintDefa).Visible = False
                    ' Dario 2013-10-29 agregado de nuevo % de descuento
                    Me.lblProcBonifPorCantLab2.Visible = False
                    Me.lblCantidadABonificar2.Visible = False
                    Me.txtProcBonifPorCantLab2.Visible = False
                    Me.txtCantidadABonificar2.Visible = False
                    ' Dario 2013-10-11 Cambio por 14% se agrega centro de costo
                    Me.cmbCCosConcepBonifLab.Visible = False
                    Me.lblCCosConcepBonifLab.Visible = False
                    Me.btnAvanBusq2.Visible = False
                Else
                    grdDato.Columns(mintAdic2).Visible = True
                    grdDato.Columns(mintDefa).Visible = True
                End If

                mConsultar()

                If Trim(mstrItemCons) <> "" And mstrTabla = "lineas" Then
                    mCargarDatos()
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    ' Dario 2013-10-11 carga el combo de centro de costo con el id de concepto que se encuentra en parametros
    Private Sub mCargarCentroCostos()
        ' Si es grupo aranceles lo que muestra carga el combo de centros de costos
        If (mstrGpAran = "SI") Then
            Dim lstrCta As String = ""
            ' obtiene el id de concepto que saca de la tabla parametos
            Dim idConcepto As Int32 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "parametros", "para_IdConceptoBonifLab", "")
            ' si no es null ejecuto la busqueda
            If idConcepto > 0 Then
                ' busco e tipo de cuenta 
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & idConcepto).Substring(0, 1)
                ' si es 1,2,3 o <> "" cargo el combo
                If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
                    clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCCosConcepBonifLab, "id", "descrip", "N")
                Else
                    ' si no limpio la coleccion
                    cmbCCosConcepBonifLab.Items.Clear()
                End If
            End If
        End If
    End Sub


    Private Sub mCargarDatos()
        Dim lstrCmd As String

        mLimpiar()

        lstrCmd = "exec lineas_consul @line_id = " + mstrItemCons

        Dim myConnection As New SqlConnection(Session("sConn").ToString())
        Dim cmdExec = New SqlCommand
        myConnection.Open()
        cmdExec.Connection = myConnection
        cmdExec.CommandType = CommandType.Text
        cmdExec.CommandText = lstrCmd
        Dim dr As SqlDataReader = cmdExec.ExecuteReader()
        While (dr.Read())
            hdnId.Text = dr.GetValue(0).ToString.Trim
            txtDesc.Text = dr.GetValue(1).ToString.Trim
        End While
        dr.Close()
        myConnection.Close()

        mSetearEditor(False)
        mMostrarPanel(True)
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        If (Not clsSQLServer.gMenuPermi(mstrOpc, (mstrConn), (Session("sUserId").ToString()))) Then
            Response.Redirect("noaccess.aspx")
        End If

        lbooPermiAlta = clsSQLServer.gMenuPermi(Integer.Parse(mstrOpc) + 1, (mstrConn), (Session("sUserId").ToString()))
        btnAlta.Visible = lbooPermiAlta

        btnBaja.Visible = clsSQLServer.gMenuPermi(Integer.Parse(mstrOpc) + 2, (mstrConn), (Session("sUserId").ToString()))

        lbooPermiModi = clsSQLServer.gMenuPermi(Integer.Parse(mstrOpc) + 3, (mstrConn), (Session("sUserId").ToString()))
        btnModi.Visible = lbooPermiModi

        btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        Dim lintCol As Integer

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

        If mbooCodi Or mbooCodiNum Then
            txtCodi.MaxLength = Integer.Parse(lstrLongs(1, 1))
            txtCodiNum.MaxLength = Integer.Parse(lstrLongs(1, 1))

            txtDesc.MaxLength = Integer.Parse(lstrLongs(2, 1))
            lintCol = 3
        Else
            txtDesc.MaxLength = Integer.Parse(lstrLongs(1, 1))
            lintCol = 2
        End If

        If mstrAdic <> "" Then
            Select Case mstrAdicType
                Case "int", "decimal"
                    txtAdicNum.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdicNum.Visible = True
                    'el tipo decimal no usa MaxLength, se debe setear CantMax
                    txtAdicNum.CantMax = Integer.Parse(lstrLongs(lintCol, 1)) - Integer.Parse(lstrLongs(lintCol, 2)) - 1

                Case "bit", "boolean"
                    cmbAdic.Visible = True
                    clsWeb.gCargarComboBool(cmbAdic, "S")

                Case "tabla"
                    cmbAdic.Visible = True
                    clsWeb.gCargarRefeCmb(mstrConn, mstrAdicTabla, cmbAdic, mstrCmbOpc)

                Case Else
                    txtAdic.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic.Visible = True
            End Select
        End If

        If mstrAdic2 <> "" Then
            lintCol = lintCol + 1
            Select Case mstrAdicType2
                Case "int", "decimal"
                    txtAdicNum2.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdicNum2.Visible = True
                    'el tipo decimal no usa MaxLength, se debe setear CantMax
                    txtAdicNum2.CantMax = Integer.Parse(lstrLongs(lintCol, 1)) - Integer.Parse(lstrLongs(lintCol, 2)) - 1

                Case "bit", "boolean"
                    cmbAdic2.Visible = True
                    clsWeb.gCargarComboBool(cmbAdic2, "S")

                Case "tabla"
                    cmbAdic2.Visible = True
                    clsWeb.gCargarRefeCmb(mstrConn, mstrAdicTabla, cmbAdic2, mstrCmbOpc)

                Case Else
                    txtAdic2.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic2.Visible = True
            End Select
        End If



        If mstrAdic3 <> "" Then
            lintCol = lintCol + 1
            Select Case mstrAdic3Type
                Case "int", "decimal"
                    txtAdic3Num.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic3Num.Visible = True
                    'el tipo decimal no usa MaxLength, se debe setear CantMax
                    txtAdic3Num.CantMax = Integer.Parse(lstrLongs(lintCol, 1)) - Integer.Parse(lstrLongs(lintCol, 2)) - 1

                Case "bit", "boolean"
                    cmbAdic3.Visible = True
                    clsWeb.gCargarComboBool(cmbAdic3, "S")

                Case "tabla"
                    cmbAdic3.Visible = True
                    clsWeb.gCargarRefeCmb(mstrConn, mstrAdicTabla, cmbAdic3, mstrCmbOpc)

                Case Else
                    txtAdic3.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic3.Visible = True
            End Select
        End If

        'GSZ  22/05/2015
        If mstrAdic4 <> "" Then
            lintCol = lintCol + 1
            Select Case mstrAdic4Type
                Case "int", "decimal"
                    txtAdic4Num.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic4Num.Visible = True
                    'el tipo decimal no usa MaxLength, se debe setear CantMax
                    txtAdic4Num.CantMax = Integer.Parse(lstrLongs(lintCol, 1)) - Integer.Parse(lstrLongs(lintCol, 2)) - 1

                Case "bit", "boolean"
                    cmbAdic4.Visible = True
                    clsWeb.gCargarComboBool(cmbAdic4, "S")

                Case "tabla"
                    cmbAdic4.Visible = True
                    clsWeb.gCargarRefeCmb(mstrConn, mstrAdicTabla, cmbAdic4, mstrCmbOpc)

                Case Else
                    txtAdic4.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic4.Visible = True
            End Select
        End If




        'GSZ  22/05/2015
        If mstrAdic5 <> "" Then
            lintCol = lintCol + 1
            Select Case mstrAdic5Type
                Case "int", "decimal"
                    txtAdic5Num.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic5Num.Visible = True
                    'el tipo decimal no usa MaxLength, se debe setear CantMax
                    txtAdic5Num.CantMax = Integer.Parse(lstrLongs(lintCol, 1)) - Integer.Parse(lstrLongs(lintCol, 2)) - 1

                Case "bit", "boolean"
                    cmbAdic5.Visible = True
                    clsWeb.gCargarComboBool(cmbAdic5, "S")

                Case "tabla"
                    cmbAdic5.Visible = True
                    clsWeb.gCargarRefeCmb(mstrConn, mstrAdicTabla, cmbAdic5, mstrCmbOpc)

                Case Else
                    txtAdic5.MaxLength = Integer.Parse(lstrLongs(lintCol, 1))
                    txtAdic5.Visible = True
            End Select
        End If



    End Sub

#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        mintId = 1
        mintCodi = 2
        mintDesc = 3
        mintAdic = 4
        mintAdic2 = 5
        mintAdic3 = 6
        mintAdic4 = 7
        mintAdic5 = 8
        mintDefa = 9
        mintEstado = 10
        mintGriFechaBaja = 11
        'GSZ  22/05/2015
        'GSZ  28/05/2015
      

        mstrDesc2 = Request.QueryString("desc")
        mstrDescDeno = Request.QueryString("descdeno")
        mstrDefadeno = Request.QueryString("defadeno")
        mstrdefacampo = Request.QueryString("defacampo")

        If mstrDescDeno = "" Then
            mstrDescDeno = "Descripci�n"
        End If

        mstrFiltroRpt = Request.QueryString("filtro")
        mstrFiltroDeno = Request.QueryString("filtroDeno")
        If (mstrFiltroDeno <> "") Then
            lblFil.Text = mstrFiltroDeno
            panFiltro.Visible = True
        End If

        mstrItemCons = Request.QueryString("id")
        mstrTabla = Request.QueryString("ta")

        mstrTitu = Request.QueryString("ti")
        mstrPKey = Request.QueryString("pk")
        mstrOpc = Request.QueryString("opc")
        mstrDeno = Request.QueryString("deno")
        mstrFKvalor = Request.QueryString("fkvalor")
        mbooCodi = Request.QueryString("codi") = "1"
        mbooCodiNum = Request.QueryString("codi") = "2"
        mstrFK = Request.QueryString("fk")
        mstrFKDeno = Request.QueryString("fkdeno")
        mstrFKId = Request.QueryString("fkid")
        mstrDefa = Request.QueryString("defa")
        mstrAdic = Request.QueryString("adic")
        mstrAdicDeno = Request.QueryString("adicdeno")
        mstrAdicType = Request.QueryString("adictype")
        mstrAdicPrec = Request.QueryString("adicprec")
        mstrAdicTabla = Request.QueryString("adictabla")
        mstrCmbOpc = IIf(IsNothing(Request.QueryString("cmbOpc")), "", Request.QueryString("cmbOpc"))
        mstrAdic2 = Request.QueryString("adic2")
        mstrAdicDeno2 = Request.QueryString("adicdeno2")
        mstrAdicType2 = Request.QueryString("adictype2")
        mstrAdicPrec2 = Request.QueryString("adicprec2")

        'GSZ  22/05/2015
        mstrAdic3 = Request.QueryString("adic3")
        mstrAdic3Deno = Request.QueryString("adic3deno")
        mstrAdic3Type = Request.QueryString("adic3type")
        mstrAdic3Prec = Request.QueryString("adic3prec")
        'GSZ  22/05/2015
        mstrAdic4 = Request.QueryString("adic4")
        mstrAdic4Deno = Request.QueryString("adic4deno")
        mstrAdic4Type = Request.QueryString("adic4type")
        mstrAdic4Prec = Request.QueryString("adic4prec")



        'GSZ  28/05/2015
        mstrAdic5 = Request.QueryString("adic5")
        mstrAdic5Deno = Request.QueryString("adic5deno")
        mstrAdic5Type = Request.QueryString("adic5type")
        mstrAdic5Prec = Request.QueryString("adic5prec")


        'mstrFiltroRpt = Request.QueryString("filtrorpt")

        lblTituAbm.Text = mstrTitu
        lblFK.Text = mstrFKDeno + ": "
        btnAgre.ToolTip = "Agregar un/a Nuevo/a " + mstrDeno
        lblDefa.Text = mstrDefadeno

        lblDesc.Text = mstrDescDeno + ": "
        CType(grdDato.Columns(mintDesc), Web.UI.WebControls.BoundColumn).HeaderText = mstrDescDeno


        lblAdic.Text = mstrAdicDeno + ": "
        CType(grdDato.Columns(mintAdic), Web.UI.WebControls.BoundColumn).HeaderText = mstrAdicDeno
        If mstrAdicType = "decimal" Then
            txtAdicNum.EsDecimal = True
        End If

        lblAdic2.Text = mstrAdicDeno2 + ": "
        CType(grdDato.Columns(mintAdic2), Web.UI.WebControls.BoundColumn).HeaderText = mstrAdicDeno2
        If mstrAdicType2 = "decimal" Then
            txtAdicNum2.EsDecimal = True
        End If


        lblAdic3.Text = mstrAdic3Deno + ": "
        CType(grdDato.Columns(mintAdic3), Web.UI.WebControls.BoundColumn).HeaderText = mstrAdic3Deno
        If mstrAdic3Type = "decimal" Then
            txtAdic3Num.EsDecimal = True
        End If

        lblAdic4.Text = mstrAdic4Deno + ": "
        CType(grdDato.Columns(mintAdic4), Web.UI.WebControls.BoundColumn).HeaderText = mstrAdic4Deno
        If mstrAdic4Type = "decimal" Then
            txtAdic4Num.EsDecimal = True
        End If


        lblAdic5.Text = mstrAdic5Deno + ": "
        CType(grdDato.Columns(mintAdic5), Web.UI.WebControls.BoundColumn).HeaderText = mstrAdic5Deno
        If mstrAdic5Type = "decimal" Then
            txtAdic5Num.EsDecimal = True
        End If


        ' Dario 2013-09-04 cambio por 14% de lab bonif
        If (mstrTabla.ToUpper() = "GRUPO_ARANCELES") Then
            mstrGpAran = "SI"
            CType(grdDato.Columns(mintAdic2), Web.UI.WebControls.BoundColumn).HeaderText = "1� % Bonif. x Cant."
            CType(grdDato.Columns(mintDefa), Web.UI.WebControls.BoundColumn).HeaderText = "2� % Bonif. x Cant."
        Else
            mstrGpAran = ""
        End If

        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mObtenerFiltro()
        mstrFiltroConsul = ""

        'Si se ha pasado como parametro el campo para filtrar
        'se genera el codigo requerido para el filtro
        If Not (mstrFiltro Is Nothing) Then
            mstrFiltroConsul = "@" + mstrFiltro + " = '" + txtFil.Text + "'"
            'De ser necesaria, se agrega la coma
            If ((mstrFKvalor <> "") Or (mstrFK <> "")) Then
                mstrFiltroConsul += ","
            End If
        End If
    End Sub

    Private Sub mObtenerFiltroRPT()
        mstrFiltroConsulRPT = ""
        If Not (mstrFiltroRpt Is Nothing) Then
            mstrFiltroConsulRPT = "@" + mstrFiltroRpt + " = " + clsSQLServer.gFormatArg(cmbFK.SelectedValue, SqlDbType.Int)
            'De ser necesaria, se agrega la coma
            If ((mstrFKvalor <> "") Or (mstrFK <> "")) Then
                'mstrFiltroConsulRPT += ","
            End If
        End If

    End Sub

    Public Sub mConsultar()
        Try
            Dim pintCol As Integer

            mstrCmd = "exec " + mstrTabla + "_consul "

            'Se arma el filtro
            mObtenerFiltro()
            mstrCmd += mstrFiltroConsul

            If mstrFK <> "" Then
                mstrCmd += " @" + mstrFKId + "=" + clsSQLServer.gFormatArg(cmbFK.SelectedValue, SqlDbType.Int)
            End If

            If mstrFKvalor <> "" Then
                mstrCmd += " @" + mstrFKId + "=" + clsSQLServer.gFormatArg(mstrFKvalor, SqlDbType.Int)
            End If

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            CType(grdDato.Columns(mintId), Web.UI.WebControls.BoundColumn).DataField = ds.Tables(0).Columns(pintCol).ColumnName
            pintCol += 1
            If mbooCodi Or mbooCodiNum Then
                CType(grdDato.Columns(mintCodi), Web.UI.WebControls.BoundColumn).DataField = ds.Tables(0).Columns(pintCol).ColumnName
                pintCol += 1
                CType(grdDato.Columns(mintDesc), Web.UI.WebControls.BoundColumn).DataField = ds.Tables(0).Columns(pintCol).ColumnName
            Else
                CType(grdDato.Columns(mintCodi), Web.UI.WebControls.BoundColumn).DataField = ""
                CType(grdDato.Columns(mintDesc), Web.UI.WebControls.BoundColumn).DataField = ds.Tables(0).Columns(pintCol).ColumnName
            End If
            pintCol += 1
            If mstrFK <> "" Then
                pintCol += 1
            End If

            If mstrAdic <> "" Then
                CType(grdDato.Columns(mintAdic), Web.UI.WebControls.BoundColumn).DataField = mstrAdic
                pintCol += 1
            Else
                CType(grdDato.Columns(mintAdic), Web.UI.WebControls.BoundColumn).DataField = ""
            End If

            If mstrAdic3 <> "" Then
                CType(grdDato.Columns(mintAdic3), Web.UI.WebControls.BoundColumn).DataField = mstrAdic3
                pintCol += 1
            Else
                CType(grdDato.Columns(mintAdic3), Web.UI.WebControls.BoundColumn).DataField = ""
            End If

            If mstrAdic4 <> "" Then
                CType(grdDato.Columns(mintAdic4), Web.UI.WebControls.BoundColumn).DataField = mstrAdic4
                pintCol += 1
            Else
                CType(grdDato.Columns(mintAdic4), Web.UI.WebControls.BoundColumn).DataField = ""
            End If


            If mstrAdic5 <> "" Then
                CType(grdDato.Columns(mintAdic5), Web.UI.WebControls.BoundColumn).DataField = mstrAdic5
                pintCol += 1
            Else
                CType(grdDato.Columns(mintAdic5), Web.UI.WebControls.BoundColumn).DataField = ""
            End If


            ' Dario 2013-09-04 cambio por 14% de lab bonif
            If (mstrGpAran <> "") Then
                CType(grdDato.Columns(mintAdic2), Web.UI.WebControls.BoundColumn).DataField = "_PorcBonifPorCantidad1"
                CType(grdDato.Columns(mintDefa), Web.UI.WebControls.BoundColumn).DataField = "_PorcBonifPorCantidad2"
            Else
                If mstrAdic2 <> "" Then
                    CType(grdDato.Columns(mintAdic2), Web.UI.WebControls.BoundColumn).DataField = mstrAdic2
                    pintCol += 1
                Else
                    CType(grdDato.Columns(mintAdic2), Web.UI.WebControls.BoundColumn).DataField = ""
                End If

                If mstrDefa = "1" Then
                    CType(grdDato.Columns(mintDefa), Web.UI.WebControls.BoundColumn).DataField = ds.Tables(0).Columns(pintCol).ColumnName
                Else
                    CType(grdDato.Columns(mintDefa), Web.UI.WebControls.BoundColumn).DataField = ""
                End If
            End If






            grdDato.DataSource = ds
            grdDato.DataBind()
            ds.Dispose()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub grdDato_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDato.ItemDataBound
        Try
            If e.Item.ItemIndex <> -1 Then
                With CType(e.Item.DataItem, DataRowView).Row
                    If mintFechaBaja = 0 Then
                        For i As Integer = 0 To .Table.Columns.Count - 1
                            If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
                                mintFechaBaja = i
                                Exit For
                            End If
                        Next
                    End If

                    If mstrAdic <> "" AndAlso Not .IsNull(mstrAdic) Then
                        Select Case mstrAdicType
                            Case "bit", "boolean"
                                e.Item.Cells(mintAdic).Text = IIf(.Item(mstrAdic), "SI", "NO")
                            Case "tabla"
                                e.Item.Cells(mintAdic).Text = .Item(.Table.Columns.Count - 1)
                        End Select
                    End If

                    If mstrAdic2 <> "" AndAlso Not .IsNull(mstrAdic2) Then
                        Select Case mstrAdicType2
                            Case "bit", "boolean"
                                e.Item.Cells(mintAdic2).Text = IIf(.Item(mstrAdic2), "SI", "NO")
                            Case "tabla"
                                e.Item.Cells(mintAdic2).Text = .Item(.Table.Columns.Count - 1)
                        End Select
                    End If

                    If mstrAdic3 <> "" AndAlso Not .IsNull(mstrAdic3) Then
                        Select Case mstrAdic3Type
                            Case "bit", "boolean"
                                e.Item.Cells(mintAdic3).Text = IIf(.Item(mstrAdic3), "SI", "NO")
                            Case "tabla"
                                e.Item.Cells(mintAdic3).Text = .Item(.Table.Columns.Count - 1)
                        End Select
                    End If


                    If mstrAdic4 <> "" AndAlso Not .IsNull(mstrAdic4) Then
                        Select Case mstrAdic4Type
                            Case "bit", "boolean"
                                e.Item.Cells(mintAdic4).Text = IIf(.Item(mstrAdic4), "SI", "NO")
                            Case "tabla"
                                e.Item.Cells(mintAdic4).Text = .Item(.Table.Columns.Count - 1)
                        End Select
                    End If

                    If mstrAdic5 <> "" AndAlso Not .IsNull(mstrAdic5) Then
                        Select Case mstrAdic5Type
                            Case "bit", "boolean"
                                e.Item.Cells(mintAdic5).Text = IIf(.Item(mstrAdic5), "SI", "NO")
                            Case "tabla"
                                e.Item.Cells(mintAdic5).Text = .Item(.Table.Columns.Count - 1)
                        End Select
                    End If

                    If mintFechaBaja <> 0 Then
                        If .IsNull(mintFechaBaja) Then
                            e.Item.Cells(mintEstado).Text = "ACTIVO"
                        Else
                            e.Item.Cells(mintEstado).Text = "INACTIVO"
                            e.Item.Cells(mintGriFechaBaja).Text = DirectCast(.Item(mintFechaBaja), DateTime).ToString("dd/MM/yyyy HH:mm")
                        End If
                    End If

                    If mstrDefa <> "" Then
                        Dim pIntCol As Integer = 2
                        If mbooCodi Or mbooCodiNum Then
                            pIntCol += 1
                        End If

                        If mstrFK <> "" Then
                            pIntCol += 1
                        End If

                        If mstrAdic <> "" Then
                            pIntCol += 1
                        End If

                        If mstrAdic2 <> "" Then
                            pIntCol += 1

                        End If

                        If mstrAdic3 <> "" Then
                            pIntCol += 1
                        End If

                        If mstrAdic4 <> "" Then
                            pIntCol += 1
                        End If

                        If mstrAdic5 <> "" Then
                            pIntCol += 1
                        End If


                        e.Item.Cells(mintDefa).Text = "Hello"

                        If .IsNull(pIntCol) Then
                            e.Item.Cells(mintDefa).Text = "NO"
                        Else
                            e.Item.Cells(mintDefa).Text = IIf(CBool(.Item(pIntCol)), "SI", "NO").ToString
                        End If
                    End If
                End With
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lDsDatos As DataSet
        Dim lstrPre As String

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(mintId).Text)
        lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
        lstrPre = Left(mstrPKey, 5)

        With lDsDatos.Tables(0).Rows(0)
            If mbooCodi Then
                txtCodi.Valor = .Item(lstrPre & "codi")
            ElseIf mbooCodiNum Then
                txtCodiNum.Valor = .Item(lstrPre & "codi")
            End If

            txtDesc.Valor = .Item(mstrDesc2)
            If mstrAdic <> "" Then
                Select Case mstrAdicType
                    Case "int", "decimal"
                        txtAdicNum.Valor = .Item(mstrAdic)
                    Case "bit", "boolean"
                        cmbAdic.ValorBool = .Item(mstrAdic)

                    Case "tabla"
                        cmbAdic.Valor = .Item(mstrAdic)

                    Case Else
                        txtAdic.Valor = .Item(mstrAdic)
                End Select
            End If


            ' Dario 2013-09-04 cambio por 14% de lab bonif
            If (mstrGpAran <> "") Then
                    Me.txtProcBonifPorCantLab1.Valor = IIf(.Item("grar_PorcBonifPorCantidad").ToString.Length = 0, String.Empty, .Item("grar_PorcBonifPorCantidad"))
                    Me.txtCantidadABonificar1.Valor = IIf(.Item("grar_CantABonificar").ToString.Length > 0, .Item("grar_CantABonificar"), String.Empty)
                ' Dario 2013-10-29 cambio por 14% de lab bonif
                    Me.txtProcBonifPorCantLab2.Valor = IIf(.Item("grar_PorcBonifPorCantidad2").ToString.Length > 0, .Item("grar_PorcBonifPorCantidad2"), String.Empty)
                    Me.txtCantidadABonificar2.Valor = IIf(.Item("grar_CantABonificar2").ToString.Length > 0, .Item("grar_CantABonificar2"), String.Empty)
                ' Dario 2013-10-11 cambio por 14% nuevo dato
                    Me.cmbCCosConcepBonifLab.Valor = IIf(.Item("grar_CCosConcepBonifLab").ToString.Length > 0, .Item("grar_CCosConcepBonifLab"), String.Empty)
            End If

            If mstrAdic2 <> "" Then
                Select Case mstrAdicType2
                    Case "int", "decimal"
                        txtAdicNum2.Valor = .Item(mstrAdic2)
                    Case "bit", "boolean"
                        cmbAdic2.ValorBool = .Item(mstrAdic2)
                    Case "tabla"
                        cmbAdic2.Valor = .Item(mstrAdic2)
                    Case Else
                        txtAdic2.Valor = .Item(mstrAdic2)
                End Select
            End If

            If mstrAdic3 <> "" Then
                Select Case mstrAdic3Type
                    Case "int", "decimal"
                        txtAdic3Num.Valor = .Item(mstrAdic3)
                    Case "bit", "boolean"
                        cmbAdic3.ValorBool = .Item(mstrAdic3)
                    Case "tabla"
                        cmbAdic3.Valor = .Item(mstrAdic3)
                    Case Else
                        txtAdic3.Valor = .Item(mstrAdic3)
                End Select
            End If

            If mstrAdic4 <> "" Then
                Select Case mstrAdic4Type
                    Case "int", "decimal"
                        txtAdic4Num.Valor = .Item(mstrAdic4)
                    Case "bit", "boolean"
                        cmbAdic4.ValorBool = .Item(mstrAdic4)
                    Case "tabla"
                        cmbAdic4.Valor = .Item(mstrAdic4)
                    Case Else
                        txtAdic4.Valor = .Item(mstrAdic4)
                End Select
            End If

            If mstrAdic5 <> "" Then
                Select Case mstrAdic5Type
                    Case "int", "decimal"
                        txtAdic5Num.Valor = .Item(mstrAdic5)
                    Case "bit", "boolean"
                        cmbAdic5.ValorBool = .Item(mstrAdic5)
                    Case "tabla"
                        cmbAdic5.Valor = .Item(mstrAdic5)
                    Case Else
                        txtAdic5.Valor = .Item(mstrAdic5)
                End Select
            End If


            If mstrDefa <> "" Then
                chkDefa.Checked = .Item(mstrdefacampo)
            End If

            If Not .IsNull(lstrPre & "baja_fecha") Then
                lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item(lstrPre & "baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
                lblBaja.Text = ""
            End If
        End With

        mSetearEditor(False)
        mMostrarPanel(True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        txtCodi.Text = ""
        txtCodiNum.Text = ""
        txtDesc.Text = ""
        txtAdic.Text = ""
        txtAdicNum.Text = ""
        txtAdic2.Text = ""
        txtAdicNum2.Text = ""
        txtAdic3.Text = ""
        txtAdic3Num.Text = ""
        txtAdic4.Text = ""
        txtAdic4Num.Text = ""
        txtAdic5.Text = ""
        txtAdic5Num.Text = ""


        ' Dario 2013-09-04 cambio por 14% de lab bonif
        Me.txtCantidadABonificar1.Text = ""
        Me.txtProcBonifPorCantLab1.Text = ""
        ' Dario 2013-10-29 cambio por 14% de lab bonif 2
        Me.txtCantidadABonificar2.Text = ""
        Me.txtProcBonifPorCantLab2.Text = ""
        ' Dario 2013-10-11 cambio por 14 % de lab bonif
        Me.cmbCCosConcepBonifLab.Limpiar()
        cmbAdic.Limpiar()
        cmbAdic2.Limpiar()
        cmbAdic3.Limpiar()
        cmbAdic4.Limpiar()
        cmbAdic5.Limpiar()


        lblBaja.Text = ""
        chkDefa.Checked = False
        mSetearEditor(True)
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mValidarDatos()

            Dim ldsEstruc As DataSet = mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

            lobjGenerica.Alta()

            grdDato.CurrentPageIndex = 0
            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            mValidarDatos()

            Dim ldsEstruc As DataSet = mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

            lobjGenerica.Modi()

            grdDato.CurrentPageIndex = 0
            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mValidarDatos()
        If (mbooCodiNum And (Not IsNumeric(txtCodiNum.Text) OrElse Integer.Parse(txtCodiNum.Text) = 0)) OrElse (mbooCodi And txtCodi.Text = "") Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el c�digo")
        End If

        If txtDesc.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + mstrDescDeno)
        End If

        If ((txtAdicNum.Text = "") And (txtAdicNum.Visible)) OrElse ((txtAdic.Text = "") And (txtAdic.Visible)) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + mstrAdicDeno)
        End If

        If ((txtAdicNum2.Text = "") And (txtAdicNum2.Visible)) OrElse ((txtAdic2.Text = "") And (txtAdic2.Visible)) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + mstrAdicDeno2)
        End If

        If ((txtAdic3Num.Text = "") And (txtAdic3Num.Visible)) OrElse ((txtAdic3.Text = "") And (txtAdic3.Visible)) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + mstrAdic3Deno)
        End If
        If ((txtAdic4Num.Text = "") And (txtAdic4Num.Visible)) OrElse ((txtAdic4.Text = "") And (txtAdic4.Visible)) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + mstrAdic4Deno)
        End If
        If ((txtAdic5Num.Text = "") And (txtAdic5Num.Visible)) OrElse ((txtAdic5.Text = "") And (txtAdic5.Visible)) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + mstrAdic5Deno)
        End If

      
    End Sub

        Private Function mGuardarDatos() As DataSet
            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
            Dim lintCpo As Integer

            With ldsEsta.Tables(0).Rows(0)
                .Item(lintCpo) = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                lintCpo += 1

                If mbooCodi Then
                    .Item(lintCpo) = txtCodi.Text.ToUpper
                    lintCpo += 1
                ElseIf mbooCodiNum Then
                    .Item(lintCpo) = txtCodiNum.Valor
                    lintCpo += 1
                End If

                .Item(lintCpo) = txtDesc.Text.ToUpper
                lintCpo += 1

                If mstrAdic <> "" Then
                    Select Case mstrAdicType
                        Case "int", "decimal"
                            .Item(mstrAdic) = txtAdicNum.Valor
                        Case "bit", "boolean"
                            .Item(mstrAdic) = IIf(cmbAdic.Valor = "1", 1, 0)
                        Case "tabla"
                            .Item(mstrAdic) = cmbAdic.Valor
                        Case Else
                            .Item(mstrAdic) = txtAdic.Valor
                    End Select
                    lintCpo += 1
                End If

                ' Dario 2013-09-04 cambio por 14% de lab bonif
                If (mstrGpAran <> "") Then
                    If Me.txtProcBonifPorCantLab1.Text.Length > 0 Then
                        .Item("grar_PorcBonifPorCantidad") = Me.txtProcBonifPorCantLab1.Text.Replace(",", ".")
                    Else
                        .Item("grar_PorcBonifPorCantidad") = 0
                    End If
                    If Me.txtCantidadABonificar1.Valor.Length > 0 Then
                        .Item("grar_CantABonificar") = Me.txtCantidadABonificar1.Valor
                    End If
                    If Me.txtProcBonifPorCantLab2.Text.Length > 0 Then
                        .Item("grar_PorcBonifPorCantidad2") = Me.txtProcBonifPorCantLab2.Text.Replace(",", ".")
                    End If
                    If Me.txtCantidadABonificar2.Valor.ToString.Length > 0 Then
                        .Item("grar_CantABonificar2") = Me.txtCantidadABonificar2.Valor
                    End If
                    If Me.cmbCCosConcepBonifLab.Valor.Length > 0 Then
                        .Item("grar_CCosConcepBonifLab") = Me.cmbCCosConcepBonifLab.Valor
                    End If
                End If

                If mstrAdic2 <> "" Then
                    Select Case mstrAdicType2
                        Case "int", "decimal"
                            .Item(mstrAdic2) = txtAdicNum2.Valor
                        Case "bit", "boolean"
                            .Item(mstrAdic2) = cmbAdic2.ValorBool
                        Case "tabla"
                            .Item(mstrAdic2) = cmbAdic2.Valor
                        Case Else
                            .Item(mstrAdic2) = txtAdic2.Valor
                    End Select
                    lintCpo += 1
                End If

                'GSZ 2015-05-27 Se agregaron la se�al si sale en web y si sale si esta retenida
                If mstrAdic3 <> "" Then
                    Select Case mstrAdic3Type
                        Case "int", "decimal"
                            .Item(mstrAdic3) = txtAdic3Num.Valor
                        Case "bit", "boolean"
                            .Item(mstrAdic3) = cmbAdic3.ValorBool
                        Case "tabla"
                            .Item(mstrAdic3) = cmbAdic3.Valor
                        Case Else
                            .Item(mstrAdic3) = txtAdic3.Valor
                    End Select
                    lintCpo += 1
                End If

                If mstrAdic4 <> "" Then
                    Select Case mstrAdic4Type
                        Case "int", "decimal"
                            .Item(mstrAdic4) = txtAdic4Num.Valor
                        Case "bit", "boolean"
                            .Item(mstrAdic4) = cmbAdic4.ValorBool
                        Case "tabla"
                            .Item(mstrAdic4) = cmbAdic4.Valor
                        Case Else
                            .Item(mstrAdic4) = txtAdic4.Valor
                    End Select
                    lintCpo += 1
                End If


                'GSZ 2015-05-28 Se agregaron la se�al TIPICA inspeccion
                If mstrAdic5 <> "" Then
                    Select Case mstrAdic5Type
                        Case "int", "decimal"
                            .Item(mstrAdic5) = txtAdic5Num.Valor
                        Case "bit", "boolean"
                            .Item(mstrAdic5) = cmbAdic5.ValorBool
                        Case "tabla"
                            .Item(mstrAdic5) = cmbAdic5.Valor
                        Case Else
                            .Item(mstrAdic5) = txtAdic5.Valor
                    End Select
                    lintCpo += 1
                End If




                If mstrFK <> "" Then
                    .Item(mstrFKId) = cmbFK.Valor
                    lintCpo += 1
                End If
                If mstrFKvalor <> "" Then
                    .Item(mstrFKId) = CType(mstrFKvalor, Int32)
                    lintCpo += 1
                End If

                If mstrDefa <> "" Then
                    .Item(lintCpo) = chkDefa.Checked
                    lintCpo += 1
                End If

                For i As Integer = 0 To .Table.Columns.Count - 1
                    If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
                        .Item(i) = DBNull.Value
                    End If
                Next
            End With

            Return ldsEsta
        End Function
#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub cmbFK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFK.SelectedIndexChanged
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
#End Region


    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "GeneABM"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&id=" + Request.QueryString("id")
            lstrRpt += "&ta=" + Request.QueryString("ta")
            lstrRpt += "&ti=" + Request.QueryString("ti")
            lstrRpt += "&pk=" + Request.QueryString("pk")
            lstrRpt += "&opc=" + Request.QueryString("opc")
            lstrRpt += "&deno=" + Request.QueryString("deno")
            lstrRpt += "&codi=" + Request.QueryString("codi")
            lstrRpt += "&fk=" + Request.QueryString("fk")
            lstrRpt += "&fkdeno=" + Request.QueryString("fkdeno")
            lstrRpt += "&fkid=" + Request.QueryString("fkid")
            lstrRpt += "&defa=" + Request.QueryString("defa")
            lstrRpt += "&adic=" + Request.QueryString("adic")
            lstrRpt += "&adicdeno=" + Request.QueryString("adicdeno")
            lstrRpt += "&adictype=" + Request.QueryString("adictype")
            lstrRpt += "&adicprec=" + Request.QueryString("adicprec")

            lstrRpt += "&adic2=" + Request.QueryString("adic2")
            lstrRpt += "&adicdeno2=" + Request.QueryString("adicdeno2")
            lstrRpt += "&adictype2=" + Request.QueryString("adictype2")
            lstrRpt += "&adicprec2=" + Request.QueryString("adicprec2")


            lstrRpt += "&adic3=" + Request.QueryString("adic3")
            lstrRpt += "&adic3deno=" + Request.QueryString("adic3deno")
            lstrRpt += "&adic3type=" + Request.QueryString("adic3type")
            lstrRpt += "&adic3prec=" + Request.QueryString("adic3prec")

            lstrRpt += "&adic4=" + Request.QueryString("adic4")
            lstrRpt += "&adic4deno=" + Request.QueryString("adic4deno")
            lstrRpt += "&adic4type=" + Request.QueryString("adic4type")
            lstrRpt += "&adic4prec=" + Request.QueryString("adic4prec")

            'GSZ 2015-05-28 Se agregaron la se�al TIPICA inspeccion
            lstrRpt += "&adic5=" + Request.QueryString("adic5")
            lstrRpt += "&adic5deno=" + Request.QueryString("adic5deno")
            lstrRpt += "&adic5type=" + Request.QueryString("adic5type")
            lstrRpt += "&adic5prec=" + Request.QueryString("adic5prec")


            mObtenerFiltroRPT()
            lstrRpt += "&filtroConsul= " + mstrFiltroConsulRPT
            lstrRpt += "&fkvalor=" & cmbFK.Valor.ToString
            lstrRpt += "&descdeno=" + mstrDescDeno

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConsultar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        txtFil.Text = ""
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
End Class
End Namespace
