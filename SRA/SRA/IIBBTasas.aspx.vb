Namespace SRA

Partial Class IIBBTasas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_IIBBTasas
    Private mstrConn As String
    Private mstrParaPageSize As Integer
#End Region

#Region "Inicialización de Variables"
    Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
    End Sub
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()

                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNoImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "ibbt_no_impo")
        txtPerc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "ibbt_perc")
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub

    Private Sub mCargarDatos(ByVal pstrId As String)
        Dim lbooActivo As Boolean
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("ibbt_id").ToString()

            txtVigeFecha.Fecha = .Item("ibbt_vige_fecha")
            txtPerc.Valor = .Item("ibbt_perc")
            txtNoImpo.Valor = .Item("ibbt_no_impo")

            If Not .IsNull("ibbt_baja_fecha") Then
                lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("ibbt_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
                lblBaja.Text = ""
            End If
        End With

        lblTitu.Text = "Registro Seleccionado: " + CDate(txtVigeFecha.Fecha).ToString("dd/MM/yyyy")

        mSetearEditor("", False)
        mMostrarPanel(True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        lblTitu.Text = ""
        txtVigeFecha.Text = ""
        txtPerc.Text = ""
        txtNoImpo.Text = ""
        lblBaja.Text = ""

        mSetearEditor("", True)
    End Sub

    Private Sub mCerrar()
        mLimpiar()
        mConsultar()
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        Dim lbooVisiOri As Boolean = panDato.Visible

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
            grdDato.DataBind()
        End If
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()

            Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
            lobjGenericoRel.Alta()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()

            Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
            lobjGenericoRel.Modi()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenericoRel.Baja(hdnId.Text)

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Private Function mGuardarDatos() As DataSet
        Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        mValidaDatos()

        With ldsDatos.Tables(0).Rows(0)
            .Item("ibbt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("ibbt_vige_fecha") = txtVigeFecha.Fecha
            .Item("ibbt_perc") = txtPerc.Valor
            .Item("ibbt_no_impo") = txtNoImpo.Valor
        End With

        Return ldsDatos
    End Function

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "_consul")
            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        Try
            mAgregar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            mCargarDatos(e.Item.Cells(1).Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class

End Namespace
