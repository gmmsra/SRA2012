' Dario 2013-08-16 se agrega nuevo campo para retornar fecha hdnDatosPopProformaFechaIngre
Namespace SRA

Partial Class proforma_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTotalDeuda As System.Web.UI.WebControls.Label
    Protected WithEvents txtTotalDeuda As NixorControls.NumberBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
        ' Dario 2013-08-16 se amplia de 2 a 7 para que mantenga en la grilla la columna de fecha ingre
        If Page.IsPostBack Then
            For i As Integer = 0 To 7
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                grdproforma.Columns.Add(dgCol)
            Next
        End If
    End Sub

#End Region

    Protected WithEvents lblTitu As System.Web.UI.WebControls.Label


#Region "Definici�n de Variables"
    Public mstrCmd As String
    Public mstrTabla As String = SRA_Neg.Constantes.gTab_Proformas
    Private mstrConn As String
    Private mstrActivId, mstrClieId, mstrFechaValor As String
    Private mobj As SRA_Neg.Facturacion
    Private mbooEsConsul As Boolean
    Private mintRecalcuProforma As Integer

#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        mintRecalcuProforma = clsSQLServer.gCampoValorConsul(mstrConn, "parametros_consul", "para_recalcu_prof_dias")
        mstrActivId = IIf(Request.QueryString("actividad") = "", 0, Request.QueryString("actividad"))
        mstrClieId = IIf(Request.QueryString("cliente") = "", 0, Request.QueryString("cliente"))
        mstrFechaValor = IIf(Request.QueryString("fecha_valor") = "", "", Request.QueryString("fecha_valor"))
        mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())

        If Not Request.QueryString("EsConsul") Is Nothing Then
            mbooEsConsul = CBool(CInt(Request.QueryString("EsConsul")))
        End If

        If mbooEsConsul Then
            grdproforma.Columns(0).Visible = False
        End If
    End Sub
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If Not Page.IsPostBack Then
                mConsultar(False)
                mCalcularTotales()
            End If
        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdproforma.EditItemIndex = -1
            If (grdproforma.CurrentPageIndex < 0 Or grdproforma.CurrentPageIndex >= grdproforma.PageCount) Then
                grdproforma.CurrentPageIndex = 0
            Else
                grdproforma.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lsbMsg As New StringBuilder
        lsbMsg.Append("<SCRIPT language='javascript'>")
        lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPopProforma'].value='{0}';", E.Item.Cells(1).Text))
        lsbMsg.Append(String.Format("window.opener.document.all['hdnProf30Dias'].value='{0}';", E.Item.Cells(3).Text))
        ' Dario 2013-08-16 se agrega nuevo campo para retornar fecha hdnDatosPopProformaFechaIngre
        lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPopProformaFechaIngre'].value='{0}';", E.Item.Cells(6).Text))
        If E.Item.Cells(3).Text = "1" Then
            lsbMsg.Append("if(confirm('Recalcula los valores de la proforma para su facturaci�n? (pasaron mas de " & mintRecalcuProforma.ToString() & " d�as desde su emisi�n)')) {window.opener.document.all['hdnConf'].value = 1;} else {window.opener.document.all['hdnConf'].value = 0;}")
        End If
        lsbMsg.Append("window.opener.__doPostBack('hdnDatosPopProforma','');")
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)
    End Sub

    Public Sub mConsultar(ByVal pbooPage As Boolean)
        Try
            mstrCmd = "exec proformas_busq @prfr_clie_id=" + mstrClieId + ", @prfr_acti_id= '" + mstrActivId + "', @fecha= " + clsSQLServer.gFormatArg(mstrFechaValor, SqlDbType.SmallDateTime)


            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            If pbooPage Then
                Dim i As Integer = grdproforma.Columns.Count - 1
                While i > 0
                    grdproforma.Columns.Remove(grdproforma.Columns(i))
                    i -= 1
                End While
            End If

            For Each dc As DataColumn In ds.Tables(0).Columns
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                dgCol.DataField = dc.ColumnName
                dgCol.HeaderText = dc.ColumnName

                If dc.Ordinal = 0 Or dc.ColumnName = "Modif_Valores" Then
                    dgCol.Visible = False
                End If
                If dc.ColumnName = "F.Ingreso" Then
                    dgCol.DataFormatString = "{0:dd/MM/yyyy}"
                End If
                If dc.ColumnName = "F.Valor" Then
                    dgCol.DataFormatString = "{0:dd/MM/yyyy}"
                End If
                If dc.ColumnName = "Importe" Or dc.ColumnName = "Deuda" Then
                    ' dgCol.DataFormatString = "{0:c}"
                    dgCol.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                End If


                grdproforma.Columns.Add(dgCol)
            Next
            grdproforma.DataSource = ds
            grdproforma.DataBind()
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCalcularTotales()
        Dim dTotalImpo As Decimal = 0
        Dim lDr As DataRow

        For Each lDr In DirectCast(grdproforma.DataSource, DataSet).Tables(0).Rows
            dTotalImpo += lDr.Item("importe")
        Next
        txtTotalImpo.Valor = dTotalImpo
    End Sub
#End Region

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        Response.Write("<Script>window.close();</script>")
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim lstrId As New StringBuilder
        Try
            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

End Class

End Namespace
