<%@ Reference Control="~/controles/usrcliente.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TarjetasClientes" CodeFile="TarjetasClientes.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Tarjetas por Clientes</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		
		function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function usrCliente_onchange()
		{
			mTraerDatos();
		}
		
		
		function mTraerDatos()
		{
		if(document.all("hdnId").value=="")
		{
			document.all("txtFechaVenc").value= "";
			document.all("hdnCheck").checked = false;
			document.all("hdnBancoEmisor").value="";
			document.all("hdnDireccion").value="";
			document.all("hdnCodigo").value="";
			document.all("hdnSocio").value="";
			document.all("hdnLegajo").value="";
			document.all("hdnInstituto").value="";
			
			if(document.all("cmbTarjeta").value!="")
				{
				if(document.all("usrCliente:txtId").value!="")
					{
					if(document.all("txtNumero").value!="")
						{
						var vstrRet;
						vstrRet = LeerCamposXML("tarjetas_clientes", "@tacl_clie_id=" + document.all("usrCliente:txtId").value + ", @tacl_tarj_id=" + document.all("cmbTarjeta").value + ", @tacl_nume='" + document.all("txtNumero").value + "'", "tacl_id,tacl_vcto_fecha,tacl_vta_tele,tacl_banc_id,tacl_domi,tacl_code_segu,_socio,_alumno,_instituto").split("|");
						if(vstrRet!="") 
							{
								document.all("hdnId").value= vstrRet[0];
								document.all("txtFechaVenc").value= vstrRet[1];
								document.all("hdnCheck").checked = vstrRet[2]=="1";
								document.all("hdnBancoEmisor").value=vstrRet[3];
								document.all("hdnDireccion").value=vstrRet[4];
								document.all("hdnCodigo").value=vstrRet[5];
								document.all("hdnSocio").value=vstrRet[6];
								document.all("hdnLegajo").value=vstrRet[7];
								document.all("hdnInstituto").value=vstrRet[8];
							}
						}
					}
				}
		}
		}
		
		</SCRIPT>
	</HEAD>
	<body class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td style="WIDTH: 9px" width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD></TD>
									<TD width="100%" colSpan="2"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 27px" height="27"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Tarjetas por Clientes</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
											Width="100%" Visible="True">
											<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 70px">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																		<TR>
																			<TD style="WIDTH: 2.92%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClieFil" runat="server" Ancho="800" tabla="clientes" AceptaNull="false" Saltos="1,2"
																					MuestraDesc="False" FilFanta="true"></UC1:CLIE></TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2.92%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblTarjetaFil" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbTarjetaFil" class="combo" runat="server" Width="328px" nomoper="tarjetas_cargar"></cc1:combobox></TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 95px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" Height="100%" width="100%"
											AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
											AutoGenerateColumns="False" PageSize="15">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="10px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="tacl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="tacl_clie_id" HeaderText="N�" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cliente" HeaderText="Cliente">
													<HeaderStyle Width="70%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_tarjeta" HeaderText="Tarjeta">
													<HeaderStyle Width="30%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_VentaTele" HeaderText="Vta.Telef." ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="_DebSoc" HeaderText="Debita C.Social" ItemStyle-HorizontalAlign="Center"
													HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="_DebAlum" HeaderText="Debita Instituto" ItemStyle-HorizontalAlign="Center"
													HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<TD></TD>
									<TD></TD>
								</tr>
								<TR>
									<TD style="HEIGHT: 9px" height="9"></TD>
									<TD style="HEIGHT: 9px" vAlign="middle" height="9"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
											ForeColor="Transparent" ToolTip="Nuevo" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD></TD>
									<TD></TD>
								</tr>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" Height="124%" width="100%">
											<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
															cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> General</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkVtaTelef" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Vta.telef�nica</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkExtenciones" runat="server"
															cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Extensiones</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" Height="116px" width="100%">
											<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
												align="left">
												<TR>
													<TD style="WIDTH: 100%" vAlign="top" align="right">
														<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
															<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																<TR>
																	<TD style="WIDTH: 120px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<UC1:CLIE id="usrCliente" runat="server" Ancho="900" AceptaNull="false" Saltos="1,2" MuestraDesc="False"
																			Obligatorio="True" SoloBusq="true" FilSociNume="True" FilTipo="S" Tabla="Clientes" FilDocuNume="True"></UC1:CLIE></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 120px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblTitular" runat="server" cssclass="titulo">Titular:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtTitular" runat="server" cssclass="cuadrotexto" Width="376px" Obligatorio="False"></CC1:TEXTBOXTAB>&nbsp;
																		<asp:Label id="lblTituRef" runat="server" ForeColor="RoyalBlue" Font-Size="XX-Small" Font-Italic="True">(Si difiere del Titular)</asp:Label></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 120px; HEIGHT: 14px" align="right">
																		<asp:Label id="lblTarjeta" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 14px" colSpan="4">
																		<cc1:combobox id="cmbTarjeta" class="combo" runat="server" Width="256px" 
																			Obligatorio="True"  AceptaNull="False"   AutoPostBack="True" 
																			OnSelectedIndexChanged="cmbTarjeta_SelectedIndexChanged"></cc1:combobox>
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 120px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblCuenta" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtNumero" runat="server" cssclass="cuadrotexto" Width="176px" Obligatorio="True"
																			onchange="mTraerDatos()"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR style="WIDTH: 100%; DISPLAY: none" id="trCUITCUIL" runat="server">
																	<TD style="WIDTH: 120px; HEIGHT: 16px" background="imagenes/formfdofields.jpg" align="right">
																		<asp:label id="lblCuit" runat="server" cssclass="titulo">CUIT/CUIL:</asp:label>&nbsp;
																	</TD>
																	<TD style="HEIGHT: 16px" background="imagenes/formfdofields.jpg" colSpan="4">
																		<CC1:CUITBOX id="txtCuit" runat="server" cssclass="cuadrotexto" Width="200px" AceptaNull="False"></CC1:CUITBOX></TD>
																</TR>
																<TR style="WIDTH: 100%; DISPLAY: none" id="trDocu" runat="server">
																	<TD style="WIDTH: 120px; HEIGHT: 16px" background="imagenes/formfdofields.jpg" align="right">
																		<asp:label id="lblDocu" runat="server" cssclass="titulo">Nro. Documento:</asp:label>&nbsp;
																	</TD>
																	<TD style="HEIGHT: 16px" background="imagenes/formfdofields.jpg" colSpan="4">
																		<cc1:combobox id="cmbDocuTipo" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:combobox>
																		<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="143px" esdecimal="False"
																			MaxValor="9999999999999"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="5"
																		align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 120px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblFechaVenc" runat="server" cssclass="titulo">Vencimiento:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<cc1:DateBox id="txtFechaVenc" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																			obligatorio="true"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="5" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panVtaTelef" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE style="WIDTH: 100%" id="TablaVtaTelef" border="0" cellPadding="0" align="left">
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 16px" align="right"></TD>
																	<TD colSpan="4">
																		<asp:CheckBox id="chkVentaTelefono" Text="Venta Telef�nica" Checked="false" CssClass="titulo"
																			Runat="server"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 16px" align="right">
																		<asp:Label id="lblBcoEmisor" runat="server" cssclass="titulo">Banco Emisor:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 20%; HEIGHT: 16px" colSpan="4" align="left">
																		<cc1:combobox id="cmbBcoEmisor" class="combo" runat="server" cssclass="cuadrotexto" Width="260px"
																			NomOper="bancos_cargar" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																	<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																			id="btnBancoBusq" language="javascript" onclick="BusqCombo('bancos','','banc_desc','cmbBcoEmisor');"
																			border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 16px" align="right">
																		<asp:Label id="lblDomicilio" runat="server" cssclass="titulo">Domicilio Resumen:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="5">
																		<cc1:textboxtab id="txtDomicilio" runat="server" cssclass="textolibre" Width="408px" TextMode="MultiLine"
																			Rows="3" height="46px"></cc1:textboxtab></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 16px" align="right">
																		<asp:Label id="lblCodigoSeg" runat="server" cssclass="titulo">C�digo Seguridad:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="5">
																		<CC1:TEXTBOXTAB id="txtCodigoSeg" runat="server" cssclass="cuadrotexto" Width="80px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="6" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD colSpan="5" align="left">&nbsp;&nbsp;&nbsp;&nbsp;
																		<asp:Label id="Label1" runat="server" cssclass="titulo" Font-Size="10">Debito Autom�tico</asp:Label>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="6" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 235px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblSocios" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="5">
																		<CC1:TEXTBOXTAB id="txtSocio" runat="server" cssclass="cuadrotexto" Width="100px" enabled="false"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 235px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblLegajo" runat="server" cssclass="titulo">Legajo:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="5">
																		<CC1:TEXTBOXTAB id="txtLegajo" runat="server" cssclass="cuadrotexto" Width="100px" enabled="false"></CC1:TEXTBOXTAB>&nbsp;&nbsp;
																		<asp:Label id="lblInstituto" runat="server" cssclass="titulo"></asp:Label>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="6" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panExtensiones" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE style="WIDTH: 100%" id="TablaExten" border="0" cellPadding="0" align="left">
																<TR>
																	<TD vAlign="top" colSpan="2" align="center">
																		<asp:datagrid id="grdExten" runat="server" BorderStyle="None" BorderWidth="1px" PageSize="10"
																			AutoGenerateColumns="False" OnEditCommand="mEditarDatosExten" OnPageIndexChanged="DataGridExten_Page"
																			CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True"
																			width="100%">
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																			<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="2%"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="tacx_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn DataField="tacx_exte" ReadOnly="True" HeaderText="Nombre y Apellido">
																					<HeaderStyle Width="100%"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 177px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblExtenNombre" runat="server" cssclass="titulo">Nombre/Apellido:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px">
																		<cc1:textboxtab id="txtExtenNombre" runat="server" cssclass="textolibre" Width="420px" TextMode="MultiLine"
																			Rows="3" height="46px"></cc1:textboxtab></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR height="30">
																	<TD colSpan="2" align="center">
																		<asp:Button id="btnAltaExten" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																		<asp:Button id="btnBajaExten" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Baja"></asp:Button>
																		<asp:Button id="btnModiExten" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Modificar"></asp:Button>
																		<asp:Button id="btnLimpExten" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Limpiar"></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										<DIV></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td style="WIDTH: 9px" width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnExtenId" runat="server"></asp:textbox>
				<asp:checkbox id="hdnCheck" Checked="false" CssClass="titulo" Runat="server"></asp:checkbox>
				<asp:textbox id="hdnBancoEmisor" runat="server"></asp:textbox>
				<asp:textbox id="hdnDireccion" runat="server"></asp:textbox>
				<asp:textbox id="hdnCodigo" runat="server"></asp:textbox>
				<asp:textbox id="hdnSocio" runat="server"></asp:textbox>
				<asp:textbox id="hdnLegajo" runat="server"></asp:textbox>
				<asp:textbox id="hdnInstituto" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
		</SCRIPT>
	</body>
</HTML>
