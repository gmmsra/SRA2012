
<%--<%@ Reference Control="/CONTROLES/usrcliente.ascx" %>--%>

<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CambioPadres" CodeFile="CambioPadres.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cambio de Padres</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		
		 function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		
		function mSetearProducto()
		 {
		 
		  if (document.all('hdnCriaId').value!='')
		   {
		     document.all('usrProd:txtCriaId').value = document.all('hdnCriaId').value;
		    }
		    //document.all('txtusrProd:cmbProdRaza').onchange();
				
	     } 
	
		
		
		function mDetalle()	
		{
		try 
		  {		 
		   var str = document.all("cmbTipo").value;
  		   if (str==0)
		    {
		     document.all('lnkDetalle').disabled=true;
		    }
		   else
		     document.all('lnkDetalle').disabled=false;
	       }
	        catch(e){} 
		}			
		function usrRazaCriador_txtCriaNume_onleave()
		{
			alert('Criador')
		}
		function usrRazaCriador_cmbRazaCria_onchange()
		{
		   if (document.all('txtusrRazaCriador:cmbRazaCria').value!=null)
		   {		  
		     //mSetearControlesRaza(document.all('usrRazaCriador:cmbRazaCria').value);
     	     mSetearRaza(document.all('usrRazaCriador:cmbRazaCria').value);
     	     //mPadresManuales();
			 //mSetearSexo();
			 //document.location='#editar';
			 document.all('txtusrRazaCriador:cmbRazaCria').focus();
		   }
		    		 
		}
		function mSetearRaza(strFiltro)
		{
		    document.all('usrProducto:hdnRazaCruza').value = document.all('usrRazaCriador:cmbRazaCria').value;
			LoadComboXML("razas_padres_cargar", document.all('usrProducto:hdnRazaCruza').value+',1', "usrProducto:cmbProdRaza", "");
			document.all('usrProducto_imgLimp').onclick();
			document.all("usrProducto:cmbProdRaza").value = document.all('usrRazaCriador:cmbRazaCria').value
			document.all("txtusrProducto:cmbProdRaza").value = document.all('txtusrRazaCriador:cmbRazaCria').value
			
	  	}
	  	
	  	
		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('')" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO -------------------> &nbsp;
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="panCabecera" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Cambio de Padres</asp:label></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
												Width="100%" Visible="True">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR style="DISPLAY: none">
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR style="DISPLAY: none">
																				<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" background="imagenes/formfdofields.jpg"
																					align="right">
																					<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																				<TD vAlign="middle" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrClienteFil" runat="server" MostrarBotones="False" CampoVal="Cliente" AutoPostBack="True"
																						AceptaNull="false" Ancho="800" Tabla="Clientes" Saltos="1,2" FilSociNume="True" FilTipo="S"
																						MuestraDesc="False" FilDocuNume="True"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" background="imagenes/formfdofields.jpg"
																					align="right">
																					<asp:label id="Label1" runat="server" cssclass="titulo">Criador:</asp:label>&nbsp;</TD>
																				<TD vAlign="middle" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriadorFil" runat="server" MostrarBotones="False" CampoVal="Criador" AutoPostBack="True"
																						AceptaNull="false" Ancho="800" Tabla="Criadores" Saltos="1,2" FilSociNume="True" FilTipo="S"
																						MuestraDesc="False" FilDocuNume="True"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" background="imagenes/formfdofields.jpg"
																					align="right">
																					<asp:label id="lblProdFil" runat="server" cssclass="titulo">Producto:</asp:label>&nbsp;</TD>
																				<TD vAlign="middle" background="imagenes/formfdofields.jpg">
																					<UC1:PROD id="usrProdFil" runat="server" AutoPostBack="False" AceptaNull="false" Ancho="800"
																						Tabla="productos" Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True"></UC1:PROD></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivfin.jpg" colSpan="2"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="10" colSpan="3"></TD>
													</TR>
													<TR>
														<TD vAlign="top" colSpan="3" align="center">
															<asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
																HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
																OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<FooterStyle CssClass="footer"></FooterStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="capa_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="capa_soli_fecha" ReadOnly="True" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_clie_desc" ReadOnly="True" HeaderText="Cliente"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_cria_codi" ReadOnly="True" HeaderText="Raza / Criador"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_prdt_desc" ReadOnly="True" HeaderText="Producto" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_resu_desc" ReadOnly="True" HeaderText="Resuelto"></asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<tr>
										<TD height="10" colSpan="3"></TD>
									</tr>
									<TR>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
												ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
												IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Argerar un Nuevo Registro"
												ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
										<TD align="right"></TD>
										<TD width="100%" align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
												ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
									</TR>
									<tr>
										<TD height="10" colSpan="3"></TD>
									</tr>
									<TR>
										<TD colSpan="3" align="center"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False" Font-Bold="True">Solicitud</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDetalle" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Detalle</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												Visible="False" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="lblSoliFecha" runat="server" cssclass="titulo">Fecha Solicitud:</asp:Label></TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtSoliFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="lblRazaCria" runat="server" cssclass="titulo">Raza/Criador:&nbsp;</asp:Label></TD>
																		<TD vAlign="middle">
																			<UC1:CLIE id="usrRazaCriador" runat="server" AutoPostBack="False" AceptaNull="false" Ancho="800"
																				Tabla="Criadores" Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR style="DISPLAY: none">
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR style="DISPLAY: none">
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:&nbsp;</asp:label></TD>
																		<TD vAlign="middle">
																			<UC1:CLIE id="usrCliente" runat="server" AceptaNull="false" Ancho="800" Tabla="Clientes" Saltos="1,2"
																				FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" Obligatorio="False"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:label id="lblPrdt" runat="server" cssclass="titulo">Producto:</asp:label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<UC1:PROD id="usrProducto" runat="server" AutoPostBack="False" AceptaNull="false" Ancho="800"
																				Tabla="productos" Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True"
																				Obligatorio="True"></UC1:PROD></TD>
																	</TR>
																	<TR style="DISPLAY: none">
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR style="DISPLAY: none">
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:label id="lblAnalisis" runat="server" cssclass="titulo">An�lisis:</asp:label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:NumberBox id="txtAnalisis" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"
																				MaxValor="2147483647" CantMax="5"></cc1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:combobox id="cmbTipo" class="combo" runat="server" Width="208px" AutoPostBack="True" AceptaNull="False"
																				Obligatorio="true">
																				<asp:listitem value="P" selected="true">Padre </asp:listitem>
																				<asp:listitem value="M">Madre</asp:listitem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panDetalle" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TablaDetalle" border="0" cellPadding="0" align="left">
																	<TBODY>
																		<TR>
																			<TD vAlign="top" colSpan="2" align="center">
																				<asp:datagrid id="grdDetalle" runat="server" BorderWidth="1px" BorderStyle="None" Visible="True"
																					width="100%" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None"
																					CellPadding="1" OnPageIndexChanged="DataGridDeta_Page" OnEditCommand="mEditarDatosDeta" AutoGenerateColumns="False">
																					<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																					<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="2%"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="capd_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="capd_pare" HeaderText="PARE_ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_descrip" ReadOnly="True" HeaderText="Descripcion"></asp:BoundColumn>
																						<asp:BoundColumn DataField="capd_hba" ReadOnly="True" HeaderText="HBA"></asp:BoundColumn>
																						<asp:BoundColumn DataField="capd_rp" HeaderText="RP"></asp:BoundColumn>
																						<asp:BoundColumn DataField="capd_anal_nume" HeaderText="ANALISIS"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="capd_tipo" HeaderText="TIPO_ID"></asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																				<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																			<TD vAlign="middle">
																				<cc1:combobox id="cmbTipoDeta" class="combo" runat="server" Width="208px" AutoPostBack="True"
																					AceptaNull="False" Obligatorio="true">
																					<asp:ListItem Value="0" Selected="True">Cambio de Ternero</asp:ListItem>
																					<asp:ListItem Value="1">Parejas Posibles</asp:ListItem>
																				</cc1:combobox>
																				<asp:Label id="lblDetalleDePadres" runat="server" cssclass="titulo">Detalle de Padres</asp:Label>&nbsp;</TD></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
															<asp:Label id="Label3" runat="server" cssclass="titulo">Padre:</asp:Label>&nbsp;</TD>
														<TD>
															<UC1:PROD id="usrProductoDeta" runat="server" AutoPostBack="False" AceptaNull="false" Ancho="800"
																Tabla="productos" Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True"></UC1:PROD></TD>
													</TR>
													<TR style="display:none">
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR style="display:none">
														<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
															<asp:Label id="Label4" runat="server" cssclass="titulo">Analisis:</asp:Label>&nbsp;</TD>
														<TD vAlign="middle">
															<cc1:NumberBox id="txtAnalisiDeta" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"
																MaxValor="2147483647" CantMax="5"></cc1:NumberBox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD width="100%" colSpan="2">
															<asp:panel id="panDatosMadre" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE width="100%">
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="Label5" runat="server" cssclass="titulo">Madre:</asp:Label>&nbsp;</TD>
																		<TD>
																			<UC1:PROD id="usrProdMadre" runat="server" AutoPostBack="False" AceptaNull="false" Ancho="800"
																				Tabla="productos" Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="Label6" runat="server" cssclass="titulo">Analisis:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:NumberBox id="txtAnalisisMadre" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="false"
																				MaxValor="2147483647" CantMax="5"></cc1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center">
															<asp:Label id="lblBajaDeta" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD colSpan="2" align="center">
															<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="90px" Text="Alta prod."></asp:Button>
															<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="90px" CausesValidation="False"
																Text="Baja prod."></asp:Button>
															<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="90px" CausesValidation="False"
																Text="Modificar prod."></asp:Button>
															<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="90px" CausesValidation="False"
																Text="Limpiar prod."></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
								</TBODY>
							</TABLE>
							</asp:panel><ASP:PANEL id="panBotones" Runat="server">
								<TABLE width="100%">
									<TR>
										<TD align="center"><A id="editar" name="editar"></A>
											<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
									</TR>
									<TR height="30">
										<TD align="center">
											<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
											<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Baja"></asp:Button>&nbsp;
											<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Modificar"></asp:Button>&nbsp;
											<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Limpiar"></asp:Button></TD>
									</TR>
								</TABLE>
							</ASP:PANEL>
							<DIV></DIV>
						</td>
					</tr>
				</TBODY>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox><asp:textbox id="hdnCriaCodi" runat="server"></asp:textbox><asp:textbox id="hdnCriaId" runat="server"></asp:textbox><asp:textbox id="hdnPareId" runat="server"></asp:textbox><asp:textbox id="hdnUltiPareNume" runat="server"></asp:textbox><asp:textbox id="hdnTipoDeta" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		
		mDetalle();
		try
		{
		 mSetearProducto();
		} catch(e){} 
		
	
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
