Imports System.Data.SqlClient


Namespace SRA


Partial Class ConsultaFiltros
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo. 
      InitializeComponent()

      If Page.IsPostBack Then
         For i As Integer = 0 To 2
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            grdConsulta.Columns.Add(dgCol)
         Next
      End If
   End Sub

#End Region


#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String
   Public mstrTitulo As String
   Public mstrFiltros As String
    Public mstrSexo As String
    Public mstrRpt As String
   Private mstrConn As String
   Private mbooEsConsul As Boolean
   Private mbooAutopostback As Boolean
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try

         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mCargarCombos()
            mConsultar(False)
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"

   Sub mLimpiarFiltros()
      txtFechaDesdeFil.Text = ""
      txtFechaHastaFil.Text = ""
      cmbEstaFil.Limpiar()
      cmbTipoFil.Limpiar()
   End Sub

   Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "rg_servi_tipos_cargar", cmbTipoFil, "id", "descrip", "T")
   End Sub

   Public Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrTitulo = Request.QueryString("titulo")
      mstrFiltros = Request.QueryString("filtros")
      mstrSexo = Request.QueryString("sexo")
        mstrRpt = IIf(Request.QueryString("rpt") = "", "P", "L")
      lblTituAbm.Text = mstrTitulo

      If Not Request.QueryString("EsConsul") Is Nothing Then
         mbooEsConsul = CBool(CInt(Request.QueryString("EsConsul")))
      End If

      If Not Request.QueryString("Autopostback") Is Nothing Then
         mbooAutopostback = CBool(CInt(Request.QueryString("Autopostback")))
      Else
         mbooAutopostback = True
      End If

      If mbooEsConsul Then
         grdConsulta.Columns(0).Visible = False
      End If

      grdConsulta.PageSize = 100 'NO PAGINAR PARA PODER IMPRIMIR TODO EL CONTENIDO
        If (mstrRpt = "P") Then
            btnImpri.Attributes.Add("onclick", "mImprimir();return(false);")
        ElseIf (mstrRpt = "L") Then
            btnImpri.Visible = False
            btnList.Visible = True
        End If

   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Dim lsbMsg As New StringBuilder

      lsbMsg.Append("<SCRIPT language='javascript'>")

      lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(1).Text))
      If mbooAutopostback Then
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
      End If

      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)

   End Sub

   Public Sub mConsultar(ByVal pbooPage As Boolean)
      Try
         Dim ds As New DataSet
         Dim lintFila As Integer

         mstrCmd = "exec " + mstrTabla + "_busq "
         mstrCmd += mstrFiltros
                If txtFechaDesdeFil.Text.Trim().Length > 0 Then
                    mstrCmd += ",@fecha_desde=" + clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
                End If
                If txtFechaHastaFil.Text.Trim().Length > 0 Then
                    mstrCmd += ",@fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
                End If
                If cmbTipoFil.Text.Trim().Length > 0 Then
                    mstrCmd += ",@tipo=" + clsSQLServer.gFormatArg(cmbTipoFil.Valor.ToString, SqlDbType.Int)
                End If
                If cmbEstaFil.Text.Trim().Length > 0 Then
                    mstrCmd += ",@estado=" + clsSQLServer.gFormatArg(cmbEstaFil.Valor.ToString, SqlDbType.VarChar)
                End If

                ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

         'If pbooPage Then
            Dim i As Integer = grdConsulta.Columns.Count - 1
            While i > 0
               grdConsulta.Columns.Remove(grdConsulta.Columns(i))
               i -= 1
            End While
         'End If

         For Each dc As DataColumn In ds.Tables(0).Columns
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            dgCol.DataField = dc.ColumnName
            Select Case dc.ColumnName.ToUpper
                Case "PAREJA"
                    dgCol.HeaderText = IIf(mstrSexo = "1", "Madre", "Padre")
                Case Else
                    dgCol.HeaderText = dc.ColumnName
            End Select
            If dc.Ordinal = 0 Then
               dgCol.Visible = False
            End If

            grdConsulta.Columns.Add(dgCol)
         Next
         grdConsulta.DataSource = ds
         grdConsulta.DataBind()
         ds.Dispose()

         Select Case mstrTabla
            Case "establecimientos_expedientes"
               For lintFila = 0 To grdConsulta.Items.Count - 1
                  grdConsulta.Items(lintFila).Cells(0).Text = "<img border=0 src='images/edit.gif' style='cursor:hand;' onclick=javascript:mEstablecimiento('" & grdConsulta.Items(lintFila).Cells(1).Text & "');>"
               Next lintFila
            Case "productos_relaciones"
               For lintFila = 0 To grdConsulta.Items.Count - 1
                  grdConsulta.Items(lintFila).Cells(0).Text = "<img border=0 src='images/edit.gif' style='cursor:hand;' onclick=javascript:mRelaciones('" & grdConsulta.Items(lintFila).Cells(1).Text & "');>"
               Next lintFila
         End Select

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
  mLimpiarFiltros()
End Sub

Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
  mConsultar(False)
End Sub

	Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
		Try
			Dim lstrRptName As String = "ListadoServicios"
			Dim vStrFiltros() As String
			Dim sFil As String

			sFil = mstrFiltros.Replace("'", "")
			vStrFiltros = sFil.Split(",")
			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			lstrRpt += "&prdt_id=" + vStrFiltros(0)
                If txtFechaDesdeFil.Text.Trim().Length > 0 Then
                    lstrRpt += "&fecha_desde=" + txtFechaDesdeFil.Fecha
                End If

                If txtFechaHastaFil.Text.Trim().Length > 0 Then
                    lstrRpt += "&fecha_hasta=" + txtFechaHastaFil.Fecha
                End If

                lstrRpt += "&tipo=" + IIf(cmbTipoFil.Valor.ToString = "", "0", cmbTipoFil.Valor.ToString)
			lstrRpt += "&estado=" + cmbEstaFil.Valor.ToString


			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
End Class

End Namespace
