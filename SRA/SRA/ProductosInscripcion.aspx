<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Control="~/controles/usrcriadero.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CRIA" Src="controles/usrCriadero.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ProductosInscripcion" CodeFile="ProductosInscripcion.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Denuncia de Nacimiento</title>
		<META content="text/html; charset=windows-1252" http-equiv="Content-Type">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript"> 

	  //estas mismas variables estan en ProductosInscripcion.aspx.vb y Utiles.vb	
	   var EspeBovinosNOHolando = '0'
       var EspeBovinos = '3'
       var EspePeliferos = '10'
       var EspeCaprinos = '9'
       var EspeOvinos = '12'
       var RazaHolando = '2'
       var RazaCriolla = '45'

		function mModificar()
		{
			__doPostBack('btnModi','');
		}


		function usrCriaComp_cmbRazaCria_onchange(pRaza)
		{
		
		
			if (document.all('txtusrCriaComp:cmbRazaCria').value!=null)
		  {		
		  		mSetearRazasPadres(pRaza,'Padre');
		  		mSetearRazasPadres(pRaza,'Madre');
		  
		   }
	}
	
	function usrProducto_usrPadre_cmbProdRaza_onchange(pRaza)
	{
		mSetearRazasPadres(pRaza,'Padre');
				mSetearRazasPadres(pRaza,'Madre');
	}
	
	function mSetearRazasPadres(pRaza,pControl)
	{
		document.all('usrProducto' + pControl + ':usrCriadorFil:cmbRazaCria').value=	document.all(pRaza).value;
		document.all('usrProducto' + pControl + ':usrCriadorFil:cmbRazaCria').onchange();
		
	}
	
	function mSetearCriador(pCriador,pControl)
	{
		document.all('usrProducto' + pControl + ':usrCriadorFil:txtCodi').value=	document.all(pCriador).value;
		document.all('usrProducto' + pControl + ':usrCriadorFil:txtCodi').onchange();
	
	}
	


function usrCria_cmbRazaCria_onchange(pRaza)
		{
	
	   if (document.all('txtusrCria:cmbRazaCria').value!=null)
		  {		
		  			
				document.all('usrPadre:usrCriadorFil:cmbRazaCria').value =document.all(pRaza).value;
				document.all('usrPadre:usrCriadorFil:cmbRazaCria').onchange();
						
						
				document.all("usrPadre:usrCriadorFil:cmbRazaCria").disabled = true;
				document.all("txtusrPadre:usrCriadorFil:cmbRazaCria").disabled = true;	
							
				document.all('usrMadre:usrCriadorFil:cmbRazaCria').value =document.all(pRaza).value;
				 document.all('usrMadre:usrCriadorFil:cmbRazaCria').onchange();
						
						
				document.all("usrMadre:usrCriadorFil:cmbRazaCria").disabled = true;
				document.all("txtusrMadre:usrCriadorFil:cmbRazaCria").disabled = true;	
				
		
				
		   }
	}
		

		function mVerErrores()
		{
		//"&Raza=" + document.all("usrCria").value + 
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=Errores&tabla=rg_denuncias_errores&filtros=" + document.all("hdnId").value + ",1", 1, "600","300","70","100");
			gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce=1&id=" + document.all("hdnId").value + "&Prdct=" + document.all("hdnNombre").value + "&opc=1" , 1, "700","400","20","40");
		}
		
		function mSetearTipoNaci()
		{
			if (document.all('chkServManual')!=null && !document.all('chkServManual').checked)
			{
				if (document.all('optTE')!=null)
				{
					if ((document.all('optTE').checked && document.all('lblServ').innerHTML == 'Servicio:')
						|| (document.all('optServ').checked && document.all('lblServ').innerHTML == 'Transplante:'))
					{
						document.all('hdnTEId').value='';
						document.all('hdnServId').value='';
						document.all('txtServ').value='';
						document.all('txtServiFecha').value='';
						document.all('txtMadre').value='';
						document.all('txtPadre').value='';
						document.all('chkServManual').checked = false;
						if (document.all('optTE').checked)
						{
							document.all('btnServicios').disabled=true;
							document.all('btnTE').disabled=false;
							document.all('panTE').style.display='';
							document.all('panServicio').style.display='none';
							document.all('lblServ').innerHTML='Transplante:';
						}
						else
						{
							document.all('btnServicios').disabled=false;
							document.all('btnTE').disabled=true;		
							document.all('panTE').style.display='none';
							document.all('panServicio').style.display='';
							document.all('lblServ').innerHTML='Servicio:';
							mLimpiarTE();
						}
					}				
				}
			}
		}
  
		function mSetearServTE()
		{
			if (document.all('chkServManual')!=null)
			{
				if (!document.all('chkServManual').checked)
				{
					if (document.all('hdnTEId').value!='' || document.all('txtImplFecha').value!='')
					{
						document.all('btnServicios').disabled=true;
						document.all('btnTE').disabled=false;
						document.all('panTE').style.display='';
						document.all('panServicio').style.display='none';
						document.all('optTE').checked=true;
						document.all('lblServ').innerHTML='Transplante:';
					}
					else
					{						
						document.all('btnServicios').disabled=false;
						document.all('btnTE').disabled=true;		
						document.all('panTE').style.display='none';
						document.all('panServicio').style.display='';
						document.all('optServ').checked=true;
						document.all('lblServ').innerHTML='Servicio:';
					}
					if (document.all('hdnServId').value=='')
					{
						//document.all('txtServ').value='';
						//document.all('txtServiFecha').value='';
					}
					if (document.all('hdnTEId').value=='')
					{
						//mLimpiarTE();
					}
				}
				else
				{
					document.all('btnServicios').disabled=true;
					document.all('btnTE').disabled=true;				
				}
			}
		}

		function mLimpiarTE()
		{
			document.all('txtCruza').value='';
			document.all('txtCara').value='';
			document.all('txtTatu').value='';
			document.all('txtFechaRecu').value='';
			document.all('txtImplFecha').value='';
			document.all('txtTeNume').value='';
		}
		
		function mGuardarSexo(pSexo)
		{
			document.all('hdnSexoSele').value = pSexo.selectedIndex;
		}					
		function mSetearSexo()
		{
  			if (document.all('usrCria:cmbRazaCria')!=null)
			{
				var lstrSexo = '-1';
				if (document.all('hdnSexoSele')!=null)
					lstrSexo = document.all('hdnSexoSele').value;
				if (lstrSexo != '')
				{
					if (document.all('cmbSexo')!=null)
		 				document.all('cmbSexo').selectedIndex = lstrSexo;
					if (document.all('cmbSexoBov')!=null)
		 				document.all('cmbSexoBov').selectedIndex = lstrSexo;
					if (document.all('cmbSexoOviCapri')!=null)
		 				document.all('cmbSexoOviCapri').selectedIndex = lstrSexo;
				}				
			}
		}
		
		
		
		function mDeterminarRP(pobjRP,pobjNume)
		{
			if (document.all(pobjNume).value == '' && pobjRP.value != '')
			{
				var lstrRp = '';
				var lstrChar = '';
				for (i=0;i<pobjRP.value.length;i++)
				{
					lstrChar = pobjRP.value.substring(i,i+1);
					if (booIsNumber(lstrChar))
						lstrRp = lstrRp + lstrChar;
				}
				if (document.all(pobjNume).value == '')
					document.all(pobjNume).value = lstrRp;
			}
		}
			
		function btnDesc_click()
		{
			gAbrirVentanas("Descarga.aspx?tabla=rg_productos_inscrip_docum&id=" + document.all("hdnDocuId").value, 0, "10","10","100","100");
		}
		
		function mFechaNacimiento()
		{
			if (document.all('usrCria:cmbRazaCria')!=null && document.all('usrCria:cmbRazaCria').value == '')
			{
				return('');
			}
			var lstrNaci = "";			

			if (document.all('panBovinos').style.display == '')
				lstrNaci = document.all('txtNaciFechaBov').value;
   			else
				if (document.all('panSexoOviCapri').style.display == '')
					lstrNaci = document.all('txtNaciFechaOviCapri').value;
				else
					if (document.all('panBovHolando').style.display == '')
						lstrNaci = document.all('txtNaciFechaHolan').value;
					else
						if (document.all('panFechaRP').style.display == '')
							lstrNaci = document.all('txtNaciFecha').value;
						else
							if (document.all('panPeliferos').style.display == '')
								lstrNaci = document.all('txtFechaNacPeli').value;

			return(lstrNaci);
		}
		
		function btnServicios_click()
		{
			var lstrNaci = mFechaNacimiento();
			var lstrRaza = document.all('usrCria:cmbRazaCria').value;
			gAbrirVentanas("TeServiConsulta.aspx?EsConsul=0&titulo=SERVICIOS&tabla=rg_servi_denuncias_naci&prdt_naci="+lstrNaci+"&prdt_raza="+lstrRaza+"&filtros=" + document.all('usrCria:txtId').value, 2, "700","400","100","50");
		}

		function btnTE_click()
		{
			var lstrNaci = mFechaNacimiento();
			var lstrRaza = document.all('usrCria:cmbRazaCria').value;
			gAbrirVentanas("TeServiConsulta.aspx?EsConsul=0&titulo=Transplantes Embrionarios&tabla=te_denun_deta_naci&prdt_naci="+lstrNaci+"&prdt_raza="+lstrRaza+"&filtros=" + document.all('usrCria:txtId').value, 2, "750","400","100","50");
		}
	
	   function mDiagrama(strFiltro)
	    {	     	
		 var str = LeerCamposXML("razas", strFiltro, "raza_diag");
		
		 if (str==true)
		    {
		     
		     document.all('lnkDocu').disabled=false;
		    }
		   else
		    document.all('lnkDocu').disabled=true;
	    }
	   function mPadresManuales()
	    {
	      //document.all('usrMadre_imgLimp').onclick();
	      //document.all('usrPadre_imgLimp').onclick();

		  mSetearPadresManual();	 
		} 
		 
		function mSetearPadresManual()
		 {
		  if(document.all('chkServManual').checked)
		  {
		     document.all('btnServicios').disabled = true;
			 document.all('btnTE').disabled = true;
		     document.all('hdnServId').value = '';
		     document.all('txtServ').value = '';
		     document.all('txtServiFecha').value = '';
		     document.all('panServicio').style.display = 'none';
		     document.all('panTE').style.display = 'none';
		     //document.all('panMadre').style.display = 'none';
		     //document.all('panMadreManual').style.display = '';
		     //document.all('panPadre').style.display = 'none';
		     //document.all('panPadreManual').style.display = '';
		     document.all('txtMadre').value = '';
		     document.all('txtPadre').value = '';
		     mLimpiarTE();
		  }else
		   {
			 //if (document.all('hdnServId').value=='' && document.all('hdnTEId').value=='')
			 //{
				document.all('btnServicios').disabled = false;
				document.all('btnTE').disabled = true;
				document.all('optServ').checked = true;
				document.all('lblServ').innerHTML='Servicio:';
				document.all('panServicio').style.display = '';
				document.all('panTE').style.display = 'none';
				//document.all('panMadre').style.display = '';
				//document.all('panMadreManual').style.display = 'none';
				//document.all('panPadre').style.display = '';
				//document.all('panPadreManual').style.display = 'none';
			 //}
		   }
		    mSetearRazaProductos();
		 } 
		
		
		function btnPedigree_click()
		{
			gAbrirVentanas("pedigree.aspx?prod=" + document.all("hdnPrdt_id").value, 7, "","","","",true);
		}
		
		
function mCargarObserNoHolando()
{
 if (document.all('panBovObs').style.display == '')
   {
     if (document.all('cmbBovParto').value == '' )
      LoadComboXML("cs_productos_parto_cargar", "", "cmbBovParto", " ");
     if (document.all('cmbBovCria').value == '' )
      LoadComboXML("cs_productos_cria_cargar", "", "cmbBovCria", " ");
   }	  
}

	
function mCargarPelajes(strFiltro)
{
	mNumeDenoConsul(strFiltro,false);
	if (strFiltro!='')
	{
       vsRet=EjecutarMetodoXML("Utiles.EspeRazaJs", strFiltro).split(";");
      
       switch (vsRet[0])
		{
			case EspePeliferos:
			   {
			    if (document.all('cmbPelaAPeli').value == '')
				 {
					LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbPelaAPeli", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",2", "cmbPelaBPeli", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",3", "cmbPelaCPeli", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",4", "cmbPelaDPeli", "S");
				 }
				break;
			   }
	        case RazaHolando:	  // 2 - Bovino Holando
			   {
			     if (document.all('cmbHolanPelaA').value == null)
				 {
				  LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbHolanPelaA", "S");
				 }
				break;
			   }
		    case EspeBovinosNOHolando:	  // 2 - Bovino  - NO Holando -
			   {
			   if (document.all('cmbBovPelaA').value == '' )
			    {
			    	LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbBovPelaA", "S");
			    }
				break;
			   } 
		    case RazaCriolla:	  // 2 - Equino Criolla
			   {
			   if (document.all('cmbCrioPelaA').value == '')
			    {
					LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbCrioPelaA", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",2", "cmbCrioPelaB", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",3", "cmbCrioPelaC", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",4", "cmbCrioPelaD", "S");
			 	}
			 	break;
		       } 
		    default:
		      {
		       if (document.all('cmbPelaA').value == '')
			    {
				
					LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbPelaA", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",2", "cmbPelaB", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",3", "cmbPelaC", "S");
					LoadComboXML("rg_pelajes_cargar", strFiltro+",4", "cmbPelaD", "S");
			    }
			   break;
		      }
		}

	}
}
	
function mSetearCampos(strFiltro)
 {	
   var lstrRaza = strFiltro;
   document.all('panTipoCria').style.display = 'none'; 
   document.all('panBovHolando').style.display = 'none'; 
   document.all('panColorPelajeHolando').style.display = 'none';
   document.all('panColorPelajeCriolla').style.display = 'none';
   document.all('panCriadero').style.display = 'none';
   document.all('panPeliferos').style.display = 'none';
   document.all('panBovinos').style.display = 'none'; 
   document.all('panColorPelajeBov').style.display = 'none'; 
   document.all('panTipoCriaBov').style.display = 'none';
   document.all('panSexoOviCapri').style.display = 'none';
   document.all('panBovObs').style.display = 'none';
  

   document.all('panFechaRP').style.display = '';
   document.all('panColorPelaje').style.display = '';
   document.all('panEstab').style.display = '';
   document.all('panNombre').style.display = '';
   document.all('panSexo').style.display = '';
  
   vsRet=EjecutarMetodoXML("Utiles.EspeRazaJs", strFiltro).split(";");
  
  switch (vsRet[0])
   {
     case EspePeliferos:
       {
		document.all('panEstab').style.display = 'none';
		document.all('panColorPelaje').style.display = 'none';
		document.all('panFechaRP').style.display = 'none';
		document.all('panNombre').style.display = 'none';
		document.all('panPeliferos').style.display = '';
		document.all('panCriadero').style.display = '';
		break;
       }
    case EspeCaprinos:    //pasa a la sig. linea, es decir ejecuta EspeOvinos  
    case EspeOvinos:
       {
		document.all('panSexo').style.display = 'none';
		document.all('panFechaRP').style.display = 'none';
		document.all('panColorPelaje').style.display = 'none';
		document.all('panSexoOviCapri').style.display = '';
		break;
	   }
	case EspeBovinosNOHolando:
       {
       
		//document.all('panEstab').style.display = 'none';
		document.all('panColorPelaje').style.display = 'none';
		document.all('panFechaRP').style.display = 'none';
		document.all('panSexo').style.display = 'none';
		document.all('panBovinos').style.display = '';
		document.all('panColorPelajeBov').style.display = '';
		document.all('panTipoCriaBov').style.display = ''; 
		document.all('panBovObs').style.display = '';
		mCargarObserNoHolando();
		
        break;
       } 
    case RazaHolando:	  // 2 - Bovino Holando
	   {
	    document.all('panFechaRP').style.display = 'none';
		document.all('panColorPelaje').style.display = 'none'; 
		document.all('panTipoCria').style.display = '';
		document.all('panBovHolando').style.display = ''; 
		document.all('panColorPelajeHolando').style.display = '';
		break;
	   }
	case RazaCriolla:	  // 45 - Equino Criolla
	   {
	    document.all('panColorPelaje').style.display = 'none';
		document.all('panColorPelajeCriolla').style.display = '';
		break;
	   } 
    
   }
 }  

	
	
	
		
		function mCambioServicio(pServ)
		{
		    if (pServ.value!='')
		    {
				var strFiltro = 'null,'+pServ.value;
				var strRet = LeerCamposXML("rg_servi_denuncias_deta", strFiltro, "_madre");
				document.all('txtMadre').value = strRet;
				var strRet2 = LeerCamposXML("rg_servi_denuncias_deta", strFiltro, "_padre");
				document.all('txtPadre').value = strRet2;
			}
			else
			{
				document.all('txtMadre').value = '';
				document.all('txtPadre').value = '';
			}
		}
		
		function mNumeDenoConsultar(pRaza,pFiltro)
		 {
		 var strCria= pRaza.value;
			mNumeDenoConsul(strCria,pFiltro);
			mCargarTipoRegistro(strCria);
		 }
		 
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   	
		    var strRet = LeerCamposXML("razas_especie", pRaza, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.Producto:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 				else
	 					document.all('lblNumeFil').innerHTML = 'Nro.Producto:';
	 			}
 			}
		}
	    function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		
		function mSetearRaza(strFiltro)
		{
		    mSetearRazaProductos();
		    mSetearCampos(strFiltro);
		    mCargarPelajes(strFiltro);  
	  	}
	

		
		
		function mSetearRazaProductos()
		 {
			if (document.all('usrPadre:hdnRazaCruza')!=null && document.all('usrPadre:hdnRazaCruza').value != document.all('usrCria:cmbRazaCria').value)
			{
				if (document.all('usrCria:cmbRazaCria').value != document.all('usrPadre:usrCriadorFil:cmbRazaCria').value) 
				{
					document.all('usrPadre:hdnRazaCruza').value = document.all('usrCria:cmbRazaCria').value;
					document.all('usrMadre:hdnRazaCruza').value = document.all('usrCria:cmbRazaCria').value;
					LoadComboXML("razas_padres_cargar", document.all('usrPadre:hdnRazaCruza').value+',1', "usrPadre:usrCriadorFil:cmbRazaCria", "");
					LoadComboXML("razas_padres_cargar", document.all('usrMadre:hdnRazaCruza').value+',0', "usrMadre:usrCriadorFil:cmbRazaCria", "");
					document.all('usrMadre_imgLimp').onclick();
					document.all('usrPadre_imgLimp').onclick();			 			 
					document.all("usrPadre:cmbProdRaza").value = document.all('usrCria:cmbRazaCria').value
					document.all("usrMadre:cmbProdRaza").value = document.all('usrCria:cmbRazaCria').value;
					document.all("txtusrPadre:cmbProdRaza").value = document.all('txtusrCria:cmbRazaCria').value
					document.all("txtusrMadre:cmbProdRaza").value = document.all('txtusrCria:cmbRazaCria').value;
				}
			}
		 return
		    if (document.all('txtusrCria:cmbRazaCria')!=null && document.all('txtusrCria:cmbRazaCria').value!='' && document.all('txtusrPadre:cmbProdRaza')!=null && document.all('hdnId').value=='')
			{
				var Padre = document.all('txtusrPadre:cmbProdRaza').value;
				var Madre = document.all('txtusrMadre:cmbProdRaza').value;
				LoadComboXML("razas_cargar_padres", document.all('usrCria:cmbRazaCria').value+",0", "usrPadre:cmbProdRaza", "");
				LoadComboXML("razas_cargar_padres", document.all('usrCria:cmbRazaCria').value+",1", "usrMadre:cmbProdRaza", "");
				document.all('txtusrPadre:cmbProdRaza').value = Padre;
				document.all('txtusrMadre:cmbProdRaza').value = Madre;
				document.all('txtusrPadre:cmbProdRaza').onchange();
				document.all('txtusrMadre:cmbProdRaza').onchange();

				if (document.all('txtusrPadre:cmbProdRaza').value=='')
				 {
				    document.all('txtusrPadre:cmbProdRaza').value = document.all('txtusrCria:cmbRazaCria').value;
					if (document.all('hdnCriaId').value!='')
					 {
					   document.all('usrPadre:txtCriaId').value = document.all('hdnCriaId').value;
					 }
				
					document.all('txtusrPadre:cmbProdRaza').onchange();
				 }
				 
				 if (document.all('txtusrMadre:cmbProdRaza').value=='')
				 {				
					document.all('txtusrMadre:cmbProdRaza').value = document.all('txtusrCria:cmbRazaCria').value;
					if (document.all('hdnCriaId').value!='')
					 {
					   document.all('usrMadre:txtCriaId').value = document.all('hdnCriaId').value;
					 }
				
					document.all('txtusrMadre:cmbProdRaza').onchange();
				 }
				 
				  if (document.all('txtusrRece:cmbProdRaza').value=='')
				 {				
				    document.all('txtusrRece:cmbProdRaza').value = document.all('txtusrCria:cmbRazaCria').value;
					document.all('txtusrRece:cmbProdRaza').onchange();	
				 }				
			}
			
		 }
			
		function usrCria_onchange()
		{
		 document.all('hdnCriaId').value = document.all('usrCria:txtId').value;
		 document.all('hdnFiltroProdCriaId').value = document.all('hdnCriaId').value;
		 //mSetearRazaProductos();
		}
		
		
		 function SeteaColorPelaje(strFiltro) 
		  {
		    if (document.all('txtusrPadre:cmbProdRaza')!=null)
				{
					document.all('txtusrPadre:cmbProdRaza').value = document.all('txtusrCria:cmbRazaCria').value;
					document.all('txtusrPadre:cmbProdRaza').onchange();
					document.all('txtusrMadre:cmbProdRaza').value = document.all('txtusrCria:cmbRazaCria').value;
					document.all('txtusrMadre:cmbProdRaza').onchange();
				}
			mSetearCampos(strFiltro);	
			mCargarPelajes(strFiltro);
		 }
		
		
		function mSetearControlesRaza(strFiltro)
		 {
		 
		  var vstrRet = LeerCamposXML("razas", strFiltro, "raza_color,raza_diag").split("|");
		  if (vstrRet[0]==0)
			 document.all('panColorPelajeCriolla').style.display = 'none';
			else
			 { 
			  document.all('panColorPelajeCriolla').style.display ='';
			 } 
			
		  if (vstrRet[1]==0)
		     document.all('lnkDocu').disabled = true;
			else
			 document.all('lnkDocu').disabled = false;
		 }
		
		/*

		function usrCriaId_onchange()
		{   
		   document.all('hdnCriaId').value = document.all('usrCria:txtId').value;
		   document.all('hdnFiltroProdCriaId').value = document.all('usrCria:txtId').value;
	   	   mCargarPrefijos();
		   mCargarEstablecimientos();	   	   
		}
		
		*/
		
		function mCargarEstablecimientos()
		{
		  if (document.all('panEstab').style.display == '')
		  {
			  var lstrCria = document.all('usrCria:txtId').value;
			  //if (document.all('cmbEsbl').value == "" )
			  if (lstrCria != '')
				LoadComboXML("establecimientos_cargar", lstrCria, "cmbEsbl", "S");
			  else
				document.all('cmbEsbl').selectedIndex = 0;
		  }
		}
		function mCargarPrefijos()
		{
		    if (document.all('usrCria:txtId').value!='' && document.all('cmbPref')!=null)
			{
			    LoadComboXML("prefijos_cargar", document.all('usrCria:txtId').value, "cmbPref", "S");
			}
			else
			{
				for(i=document.all('cmbPref').length;i>-1;i--)
					document.all('cmbPref').remove(i);
			}
		}
		function mCargarTipoRegistro(pRaza)
		{
		    if (pRaza!='' && document.all('cmbTipoRegiFil')!=null)
			{
			    LoadComboXML("rg_registros_tipos_cargar", pRaza, "cmbTipoRegiFil", "S");
			}
			else
			{
				LoadComboXML("rg_registros_tipos_cargar", pRaza, "cmbTipoRegiFil", "S");
				//for(i=document.all('cmbTipoRegiFil').length;i>-1;i--)
					//document.all('cmbTipoRegiFil').remove(i);
			}
		}
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="98%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Denuncia de Nacimiento</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderStyle="none" BorderWidth="1px"
											width="99%">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD colSpan="4">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 276px" colSpan="4">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="2" colSpan="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																<TD>
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TR>
																			<TD height="6" background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD height="6" background="imagenes/formfdofields.jpg" width="20%" noWrap align="right"></TD>
																			<TD height="6" background="imagenes/formfdofields.jpg" width="72%" align="right"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo" Visible="False"> Nro.Producto:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" Visible="False" AceptaNull="False"
																					Width="140px"></CC1:TEXTBOXTAB></TD>
																		</TR> <!--	<TR>
																			<TD align="right" width="1%" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" width="20%" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																				
																			<TD width="72%" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="294px"
																					AceptaNull="False" filtra="true" MostrarBotones="False" NomOper="razas_inscrip_cargar" Height="20px"
																					onchange="javascript:mNumeDenoConsultar(this,true);"></cc1:combobox></TD>
																		</TR>-->
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:Label id="lblCriaFil" runat="server" cssclass="titulo">Raza/Criador:&nbsp;</asp:Label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="false" Tabla="Criadores" Saltos="1,2"
																					AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True"
																					Ancho="800"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="24" background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD height="24" background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:&nbsp;</asp:label></TD>
																			<TD height="24" background="imagenes/formfdofields.jpg" width="72%">
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="400px"
																					Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<asp:checkbox id="chkBusc" Runat="server" CssClass="titulo" Text="Buscar en..."></asp:checkbox>&nbsp;&nbsp;&nbsp;
																				<asp:checkbox id="chkBaja" Runat="server" CssClass="titulo" Text="Incluir Dados de Baja"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="lblNaciFechaFil" runat="server" cssclass="titulo">Fecha Nacimiento:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<cc1:DateBox id="txtNaciFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtNaciFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="Label1" runat="server" cssclass="titulo">Fecha Presentaci&oacute;n:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<cc1:DateBox id="txtFechaPreseDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:label id="Label2" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtFechaPreseHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="Label3" runat="server" cssclass="titulo">Fecha Ingreso:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<cc1:DateBox id="txtFechaIngreDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:label id="Label4" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtFechaIngreHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="lblRPNumeFil" runat="server" cssclass="titulo">Nro. RP:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<CC1:TEXTBOXTAB id="txtRPNumeFil" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="140px"
																					Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="Label5" runat="server" cssclass="titulo">Tipo Registro:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<cc1:combobox id="cmbTipoRegiFil" class="combo" runat="server" Width="200px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD vAlign="top" background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="lblPadreFil" runat="server" cssclass="titulo"><br>Padre:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<UC1:PROH id="usrPadreFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																					AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True"
																					Ancho="800"></UC1:PROH></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD vAlign="top" background="imagenes/formfdofields.jpg" width="20%" noWrap align="right">
																				<asp:label id="lblMadreFil" runat="server" cssclass="titulo"><br>Madre:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<UC1:PROH id="usrMadreFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																					AutoPostBack="False" FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True"
																					Ancho="800"></UC1:PROH></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" width="1%" align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="20%" noWrap align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<asp:checkbox id="chkVisto" Runat="server" CssClass="titulo" Text="Incluir Registros Vistos y/o Aprobados"
																					Checked="true"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 1%" height="4" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 20%" height="8" background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																			<TD style="WIDTH: 72%" height="4" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 1%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 20%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																			<TD style="WIDTH: 72%" height="2" background="imagenes/formdivfin.jpg" width="8"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="right" colSpan="2"><br>
										<asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton2" runat="server" CausesValidation="false" CommandName="Update">
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />
															<!--<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />--></asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="lnkEditar" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="pdin_id" ReadOnly="True" HeaderText="Id Producto"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="pdin_sra_nume" ReadOnly="True" HeaderText="Nro.Producto">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="pdin_nro_control" ReadOnly="True" HeaderText="Nro.Control">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_raza" HeaderText="Raza">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_sexo" ReadOnly="True" HeaderText="Sexo">
													<HeaderStyle Width="5%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_criador" ReadOnly="True" HeaderText="Criador">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="pdin_nomb" ReadOnly="True" HeaderText="Nombre">
													<HeaderStyle Width="30%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="pdin_rp" ReadOnly="True" HeaderText="RP">
													<HeaderStyle Width="6%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_planilla" ReadOnly="True" HeaderText="Planilla">
													<HeaderStyle Width="6%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_fecha_naci" ReadOnly="True" HeaderText="Fec. Nac.">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_fecha_valor" ReadOnly="True" HeaderText="Fec. Presen.">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_registro" ReadOnly="True" HeaderText="Reg.">
													<HeaderStyle Width="5%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_estado" HeaderText="Estado">
													<HeaderStyle Width="10%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="pdin_raza_id" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_raza_codi" HeaderText="Raza"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="left" colSpan="2"><CC1:BOTONIMAGEN id="btnAgre" disabled runat="server" BorderStyle="None" ForeColor="Transparent"
											ImageOver="btnNuev2.gif" ImageBoton="btnNuev0.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
											IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una nueva inscripci�n" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<TR>
									<TD></TD>
									<A id="editar" name="editar"></A>
									<TD vAlign="top" align="right">
										<TABLE id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<TR>
												<TD width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
												<TD background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Producto </asp:linkbutton></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp"></TD>
												<TD width="27"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDocu" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" Height="21px" CausesValidation="False"> Diagrama </asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
											</TR>
										</TABLE>
									</TD>
									<TD align="right"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												width="99%" Visible="False" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD height="5"></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3" align="center">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TBODY>
																		<TR>
																			<TD width="20%" align="right">
																				<asp:Label id="lblFechaValor" runat="server" cssclass="titulo">Fecha Denuncia:</asp:Label></TD>
																			<TD align="left">
																				<cc1:DateBox id="txtFechaValor" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD width="20%" align="right">
																				<asp:Label id="lblInscFecha" runat="server" cssclass="titulo">Fecha Inscripci�n:</asp:Label>&nbsp;</TD>
																			<TD align="left">
																				<cc1:DateBox id="txtInscFecha" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox>
																				<asp:Label id="lblFechaAlta" runat="server" cssclass="titulo">Fecha Ingreso:</asp:Label>
																				<asp:Label id="txtFechaAlta" runat="server" cssclass="titulo"></asp:Label></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD width="18%" align="right">
																				<asp:Label id="lblNroControl" runat="server" cssclass="titulo">Nro. de Control:</asp:Label></TD>
																			<TD>
																				<cc1:numberbox id="txtNroControl" runat="server" cssclass="cuadrotexto" Visible="true" Width="81px"
																					MaxLength="3" EsPorcen="False" esdecimal="False" MaxValor="999999999"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV id="panSexo">
																					<TABLE id="tblSexo" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD width="20%" align="right">
																								<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																							<TD align="left">
																								<cc1:combobox id="cmbSexo" class="combo" runat="server" Width="138px" onchange="javascript:mGuardarSexo(this);">
																									<asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem>
																									<asp:ListItem Value="0">Hembra</asp:ListItem>
																									<asp:ListItem Value="1">Macho</asp:ListItem>
																								</cc1:combobox>&nbsp;&nbsp;
																							</TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblCria" runat="server" cssclass="titulo">Raza/Criador:&nbsp;</asp:Label></TD>
																			<TD>
																				<UC1:CLIE id="usrCria" runat="server" Tabla="Criadores" Saltos="1,2" AutoPostBack="False"
																					FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" Ancho="800" Obligatorio="True"
																					CampoVal="Criador"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR> <!--- panel para Bovinos, menos raza Holando (nacimiento/rp/sexo/col/tipo cria/nombre/padre/madre/servicio)!-->
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panBovinos">
																					<TABLE id="tblBovino" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblNaciFechaBov" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
																							<TD style="HEIGHT: 3px">
																								<cc1:DateBox id="txtNaciFechaBov" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblRpBov" runat="server" cssclass="titulo">RP(nro/texto):</asp:Label>&nbsp;</TD>
																							<TD>
																								<CC1:TEXTBOXTAB onblur="javascript:mDeterminarRP(this,'txtRpnumeBov');" id="txtRpBov" runat="server"
																									cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																								<cc1:numberbox id="txtRpnumeBov" runat="server" cssclass="cuadrotexto" Width="106px" MaxLength="12"
																									esdecimal="False" MaxValor="9999999999999"></cc1:numberbox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblSexoBov" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																							<TD align="left">
																								<cc1:combobox id="cmbSexoBov" class="combo" runat="server" Width="138px" onchange="javascript:mGuardarSexo(this);">
																									<asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem>
																									<asp:ListItem Value="0">Hembra</asp:ListItem>
																									<asp:ListItem Value="1">Macho</asp:ListItem>
																								</cc1:combobox>&nbsp;&nbsp;
																							</TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD colSpan="2" align="right">
																								<DIV style="DISPLAY: none" id="panColorPelajeBov">
																									<TABLE id="tblColorBov" border="0" cellSpacing="0" cellPadding="0" width="100%">
																										<TR>
																											<TD style="WIDTH: 20%" align="right">
																												<asp:Label id="lblColorBov" runat="server" cssclass="titulo">Color:</asp:Label>&nbsp;</TD>
																											<TD>
																												<cc1:combobox id="cmbBovPelaA" class="combo" runat="server" cssclass="cuadrotexto" Width="40%"
																													NomOper="rg_pelajes_cargar" MostrarBotones="False" filtra="True"></cc1:combobox></TD>
																										</TR>
																										<TR>
																											<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																										</TR>
																										<TR>
																											<TD colSpan="2" align="right">
																												<DIV style="DISPLAY: none" id="panTipoCriaBov">
																													<TABLE id="tblCriaTipoBov" border="0" cellSpacing="0" cellPadding="0" width="100%">
																														<TR>
																															<TD style="WIDTH: 20%" align="right">
																																<asp:Label id="lblcmbCriaTipoBov" runat="server" cssclass="titulo">C�digo Mellizo:</asp:Label>&nbsp;</TD>
																															<TD>
																																<cc1:combobox id="cmbCriaTipoBov" class="combo" runat="server" Width="200px" NomOper="rg_cria_tipos"></cc1:combobox></TD>
																														</TR>
																													</TABLE>
																												</DIV>
																											</TD>
																										</TR>
																									</TABLE>
																								</DIV>
																							</TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR> <!--- panel todas las razas menos especie peliferos !-->
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV id="panEstab">
																					<TABLE id="tblstab" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblEsbl" runat="server" cssclass="titulo">Establecimiento:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:combobox id="cmbEsbl" class="combo" runat="server" Width="336px" nomoper="establecimientos_cargar"></cc1:combobox></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR> <!--- panel para peliferos (criadero)!-->
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panCriadero">
																					<TABLE id="tblCriadero" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblCriadero" runat="server" cssclass="titulo">Criadero:</asp:Label>&nbsp;</TD>
																							<TD>
																								<UC1:CRIA id="usrCriader" runat="server" Tabla="Criaderos" Saltos="1,2" AutoPostBack="True"
																									FilSociNume="True" FilTipo="T" MuestraDesc="False" Ancho="800" CampoVal="Criadero" CampoCodi="crdr_nume"></UC1:CRIA></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR> <!--- panel todas las razas menos especie peliferos !-->
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV id="panNombre">
																					<TABLE id="tblNombre" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" vAlign="middle" align="right">
																								<asp:Label id="lblPref" runat="server" cssclass="titulo">Prefijo:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:combobox id="cmbPref" class="combo" runat="server" Width="140px"></cc1:combobox>&nbsp;&nbsp;&nbsp;
																								<asp:Label id="lblNomb" runat="server" cssclass="titulo">Nombre:</asp:Label>
																								<CC1:TEXTBOXTAB id="txtNomb" runat="server" cssclass="cuadrotexto" Width="220px"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" vAlign="middle" align="right">
																								<asp:Label id="lblPesoNacer" runat="server" cssclass="titulo">Peso al Nacer:&nbsp;</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:numberbox id="txtPesoNacer" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="false"
																									MaxLength="12" esdecimal="False" MaxValor="9999999999999"></cc1:numberbox>&nbsp;
																								<asp:Label id="lblEpdNacer" runat="server" cssclass="titulo">DEP al Nacer:&nbsp;</asp:Label>
																								<cc1:numberbox id="txtEpdNacer" runat="server" cssclass="cuadrotexto" Width="106px" Obligatorio="false"
																									MaxLength="12" esdecimal="True" MaxValor="9999999999999" CantMax="3"></cc1:numberbox>&nbsp;
																								<asp:Label id="lblSiete" runat="server" cssclass="titulo">Sietemesino:&nbsp;</asp:Label>
																								<cc1:numberbox id="txtSiete" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="20px"
																									MaxLength="1" MaxValor="9" CantMax="1"></cc1:numberbox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR> <!--- panel para Ovinos y caprinos (sexo/RP/fecha Nacimiento) !-->
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panSexoOviCapri">
																					<TABLE id="tblSexoOviCapri" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD width="20%" align="right">
																								<asp:Label id="lblSexoOviCapri" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																							<TD align="left">
																								<cc1:combobox id="cmbSexoOviCapri" class="combo" runat="server" Width="138px" onchange="javascript:mGuardarSexo(this);">
																									<asp:ListItem Selected="True" value="">(Seleccionar)</asp:ListItem>
																									<asp:ListItem Value="0">Hembra</asp:ListItem>
																									<asp:ListItem Value="1">Macho</asp:ListItem>
																								</cc1:combobox>&nbsp;&nbsp;
																							</TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblRPOviCapri" runat="server" cssclass="titulo">Tatuaje(nro/texto):</asp:Label>&nbsp;</TD>
																							<TD>
																								<CC1:TEXTBOXTAB onblur="javascript:mDeterminarRP(this,'txtRPNumeOviCapri');" id="txtRPOviCapri"
																									runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																								<cc1:numberbox id="txtRPNumeOviCapri" runat="server" cssclass="cuadrotexto" Width="106px" MaxLength="12"
																									esdecimal="False" MaxValor="9999999999999"></cc1:numberbox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblNaciFechaOviCapri" runat="server" cssclass="titulo">Nacio:</asp:Label>&nbsp;</TD>
																							<TD style="HEIGHT: 3px">
																								<cc1:DateBox id="txtNaciFechaOviCapri" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR> <!--- panel para Holando (RP/nacimiento)!-->
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panBovHolando">
																					<TABLE id="tblBovHolando" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblRPHolan" runat="server" cssclass="titulo">RP(nro/texto):</asp:Label>&nbsp;</TD>
																							<TD>
																								<CC1:TEXTBOXTAB onblur="javascript:mDeterminarRP(this,'txtRPNumeHolan');" id="txtRPHolan" runat="server"
																									cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																								<cc1:numberbox id="txtRPNumeHolan" runat="server" cssclass="cuadrotexto" Width="106px" MaxLength="12"
																									esdecimal="False" MaxValor="9999999999999"></cc1:numberbox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblNaciFechaHolan" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
																							<TD style="HEIGHT: 3px">
																								<cc1:DateBox id="txtNaciFechaHolan" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV id="panFechaRP">
																					<TABLE id="tblFechaRP" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
																							<TD style="HEIGHT: 3px">
																								<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblRP" runat="server" cssclass="titulo">RP:</asp:Label>&nbsp;</TD>
																							<TD>
																								<CC1:TEXTBOXTAB onblur="javascript:mDeterminarRP(this,'txtRPNume');" id="txtRP" runat="server" cssclass="cuadrotexto"
																									Width="170px"></CC1:TEXTBOXTAB>&nbsp;
																								<cc1:numberbox id="txtRPNume" runat="server" cssclass="cuadrotexto" Width="106px" MaxLength="12"
																									esdecimal="False" MaxValor="9999999999999"></cc1:numberbox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR> <!--- panel para Holando (tipo de cria)!-->
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV id="panTipoCria">
																					<TABLE id="tblTipoCria" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblCriaTipo" runat="server" cssclass="titulo">C�digo Mellizo:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:combobox id="cmbCriaTipo" class="combo" runat="server" Width="200px"></cc1:combobox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR> <!--- panel para criolla (color)!-->
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panColorPelajeCriolla">
																					<TABLE id="tblColorPelajeCrio" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblCrioPelaA" runat="server" cssclass="titulo">Color:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 35%">
																								<cc1:combobox id="cmbCrioPelaA" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																							<TD style="WIDTH: 10%" align="right">
																								<asp:Label id="lblCrioPelaB" runat="server" cssclass="titulo">Cabeza:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 35%">
																								<cc1:combobox id="cmbCrioPelaB" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblCrioPelaC" runat="server" cssclass="titulo">Cuerpo:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:combobox id="cmbCrioPelaC" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																							<TD align="right">
																								<asp:Label id="lblCrioPelaD" runat="server" cssclass="titulo">Miembros:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:combobox id="cmbCrioPelaD" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR> <!--- panel para peliferos (RP/marca/color/nacimiento/incrip.reg)!-->
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panPeliferos">
																					<TABLE id="tblPeliferos" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lbRPPeli" runat="server" cssclass="titulo">RP:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 80%" colSpan="3">
																								<CC1:TEXTBOXTAB onblur="javascript:mDeterminarRP(this,'txtRPNumePeli');" id="txtRPPeli" runat="server"
																									cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB>
																								<cc1:numberbox id="txtRPNumePeli" runat="server" cssclass="cuadrotexto" Width="106px" MaxLength="12"
																									esdecimal="False" MaxValor="9999999999999"></cc1:numberbox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblMarcaPeli" runat="server" cssclass="titulo">Marca:</asp:Label>&nbsp;</TD>
																							<TD colSpan="3">
																								<CC1:TEXTBOXTAB id="txtMarcaPeli" runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblPelaAPeli" runat="server" cssclass="titulo">Color:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 35%">
																								<cc1:combobox id="cmbPelaAPeli" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																							<TD style="WIDTH: 10%" align="right">
																								<asp:Label id="lblPelaBPeli" runat="server" cssclass="titulo">Cabeza:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 35%">
																								<cc1:combobox id="cmbPelaCPeli" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblPelaCPeli" runat="server" cssclass="titulo">Cuerpo:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:combobox id="cmbPelaBPeli" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																							<TD align="right">
																								<asp:Label id="lblPelaDPeli" runat="server" cssclass="titulo">Miembros:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:combobox id="cmbPelaDPeli" class="combo" runat="server" cssclass="cuadrotexto" Width="100%"
																									MostrarBotones="False" filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblFechaNacPeli" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;</TD>
																							<TD colSpan="2">
																								<cc1:DateBox id="txtFechaNacPeli" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblRegPadresPeli" runat="server" cssclass="titulo">Inscrip.en registro:</asp:Label>&nbsp;</TD>
																							<TD colSpan="3">
																								<CC1:TEXTBOXTAB id="txtRegPadresPeli" runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right"></TD>
																			<TD>
																				<asp:RadioButton id="optServ" onclick="javascript:mSetearTipoNaci();" runat="server" CssClass="titulo"
																					Text="Servicio" Checked="True" GroupName="grpTipo"></asp:RadioButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																				<asp:RadioButton id="optTE" onclick="javascript:mSetearTipoNaci();" runat="server" CssClass="titulo"
																					Text="Transplante Embrionario" GroupName="grpTipo"></asp:RadioButton></TD>
																		</TR>
																		<TR>
																			<TD align="right"></TD>
																			<TD><BUTTON style="WIDTH: 150px" id="btnServicios" class="boton" onclick="btnServicios_click();"
																					runat="server" value="Seleccionar Servicio">Seleccionar Servicio</BUTTON>&nbsp;&nbsp;<BUTTON style="WIDTH: 150px" id="btnTE" class="boton" onclick="btnTE_click();" runat="server"
																					value="Seleccionar Transplante">Seleccionar Transplante</BUTTON></TD>
																		</TR>
																		<TR>
																			<TD align="right"></TD>
																			<TD>
																				<asp:checkbox id="chkServManual" onclick="mPadresManuales();" Runat="server" CssClass="titulo"
																					Text="Ingresar padres manualmente"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="left">
																				<DIV style="BORDER-BOTTOM: 1px solid; BORDER-LEFT: 1px solid; BORDER-TOP: 1px solid; BORDER-RIGHT: 1px solid"
																					id="panServicio">
																					<TABLE id="tabServ" border="0" cellSpacing="0" cellPadding="2" width="96%">
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblServ" runat="server" cssclass="titulo">Servicio:</asp:Label>&nbsp;</TD>
																							<TD>
																								<CC1:TEXTBOXTAB id="txtServ" runat="server" cssclass="cuadrotexto" Width="284px" Obligatorio="false"
																									Enabled="False"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:Label id="lblServiFecha" runat="server" cssclass="titulo">Fecha Servicio:</asp:Label>&nbsp;</TD>
																							<TD>
																								<cc1:DateBox id="txtServiFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="left">
																				<DIV style="BORDER-BOTTOM: 1px solid; BORDER-LEFT: 1px solid; DISPLAY: none; BORDER-TOP: 1px solid; BORDER-RIGHT: 1px solid"
																					id="panTE">
																					<TABLE style="WIDTH: 622px; HEIGHT: 18px" id="tabTE" border="0" cellSpacing="0" cellPadding="2"
																						width="622">
																						<TR>
																							<TD style="WIDTH: 162px" align="right">
																								<asp:Label id="lblTeNume" runat="server" cssclass="titulo">Nro.Denuncia:</asp:Label>&nbsp;</TD>
																							<TD noWrap>
																								<cc1:numberbox id="txtTeNume" runat="server" cssclass="cuadrotexto" Width="84px" MaxLength="12"
																									esdecimal="False" MaxValor="9999999999999"></cc1:numberbox>&nbsp;
																								<asp:Label id="lblFechaServiTE" runat="server" cssclass="titulo">Fecha Servicio:</asp:Label>
																								<cc1:DateBox id="txtFechaServiTE" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox>&nbsp;
																								<asp:Label id="lblImplFecha" runat="server" cssclass="titulo">Fecha Implante:</asp:Label>
																								<cc1:DateBox id="txtImplFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox>&nbsp;
																								<asp:Label id="lblFechaRecu" runat="server" cssclass="titulo">Fecha Recup.:</asp:Label>
																								<cc1:DateBox id="txtFechaRecu" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox></TD>
																						</TR>
																						<TR>
																							<TD noWrap align="right">
																								<P>
																									<asp:Label id="lblRece" runat="server" cssclass="titulo" Height="12px">Receptora:</asp:Label>&nbsp;</P>
																							</TD>
																							<TD noWrap>
																								<asp:Label id="lblCara" runat="server" cssclass="titulo">Caravana:</asp:Label>
																								<CC1:TEXTBOXTAB onblur="javascript:if(document.all('btnAltaDeta')!=null &amp;&amp; !document.all('btnAltaDeta').disabled) document.all('btnAltaDeta').focus();"
																									id="txtCara" runat="server" cssclass="cuadrotexto" Width="100px" MaxLength="20" EnterPorTab="True"></CC1:TEXTBOXTAB>&nbsp;
																								<asp:Label id="lblTatu" runat="server" cssclass="titulo">Tatuaje:</asp:Label>
																								<CC1:TEXTBOXTAB id="txtTatu" runat="server" cssclass="cuadrotexto" Width="100px" MaxLength="20"
																									EnterPorTab="True"></CC1:TEXTBOXTAB>&nbsp;
																								<asp:Label id="lblCruza" runat="server" cssclass="titulo">Cruzas:</asp:Label>
																								<CC1:TEXTBOXTAB id="txtCruza" runat="server" cssclass="cuadrotexto" Width="80px" MaxLength="20"
																									EnterPorTab="True"></CC1:TEXTBOXTAB></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV id="panPadreManual">
																					<TABLE id="tblPadreManual" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblPadreManual" runat="server" cssclass="titulo">Padre:</asp:Label>&nbsp;</TD>
																							<TD vAlign="middle">
																								<UC1:PROH id="usrPadre" runat="server" Tabla="productos" Saltos="1,2" AutoPostBack="False"
																									FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True" Ancho="800"></UC1:PROH></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panPadre">
																					<TABLE id="tblPadre" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblPadre" runat="server" cssclass="titulo">Padre:</asp:Label>&nbsp;</TD>
																							<TD vAlign="middle">
																								<CC1:TEXTBOXTAB id="txtPadre" runat="server" cssclass="cuadrotexto" BackColor="WhiteSmoke" Width="90%"
																									ReadOnly="True"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV id="panMadreManual">
																					<TABLE id="tblMadreManual" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblMadreManual" runat="server" cssclass="titulo">Madre:</asp:Label>&nbsp;</TD>
																							<TD vAlign="middle">
																								<UC1:PROH id="usrMadre" runat="server" Tabla="productos" Saltos="1,2" AutoPostBack="False"
																									FilSociNume="True" FilTipo="S" MuestraDesc="True" FilDocuNume="True" Ancho="800"></UC1:PROH></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="2" align="right">
																				<DIV style="DISPLAY: none" id="panMadre">
																					<TABLE id="tblMadre" border="0" cellSpacing="0" cellPadding="0" width="100%">
																						<TR>
																							<TD style="WIDTH: 20%" align="right">
																								<asp:Label id="lblmadre" runat="server" cssclass="titulo">Madre:</asp:Label>&nbsp;</TD>
																							<TD vAlign="middle">
																								<CC1:TEXTBOXTAB id="txtMadre" runat="server" cssclass="cuadrotexto" BackColor="WhiteSmoke" Width="90%"
																									ReadOnly="True"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
													</TR> <!--- panel para holando (color)!-->
													<TR>
														<TD colSpan="2" align="right">
															<DIV style="DISPLAY: none" id="panColorPelajeHolando">
																<TABLE id="tblColorPelajeHolando" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblHolanPelaA" runat="server" cssclass="titulo">Color:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbHolanPelaA" class="combo" runat="server" cssclass="cuadrotexto" Width="50%"
																				NomOper="rg_pelajes_cargar" MostrarBotones="False" filtra="True"></cc1:combobox>&nbsp;&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</DIV>
														</TD>
													</TR>
													<TR>
														<TD colSpan="2" align="right">
															<DIV style="DISPLAY: none" id="panColorPelaje">
																<TABLE id="tblColorPela" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblPelaA" runat="server" cssclass="titulo">Color:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 35%">
																			<cc1:combobox id="cmbPelaA" class="combo" runat="server" cssclass="cuadrotexto" Width="100%" MostrarBotones="False"
																				filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																		<TD style="WIDTH: 10%" align="right">
																			<asp:Label id="lblPelaB" runat="server" cssclass="titulo">Cabeza:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 35%">
																			<cc1:combobox id="cmbPelaC" class="combo" runat="server" cssclass="cuadrotexto" Width="100%" MostrarBotones="False"
																				filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblPelaC" runat="server" cssclass="titulo">Cuerpo:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbPelaB" class="combo" runat="server" cssclass="cuadrotexto" Width="100%" MostrarBotones="False"
																				filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																		<TD align="right">
																			<asp:Label id="lblPelaD" runat="server" cssclass="titulo">Miembros:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox id="cmbPelaD" class="combo" runat="server" cssclass="cuadrotexto" Width="100%" MostrarBotones="False"
																				filtra="True" nomoper="rg_pelajes_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</DIV>
														</TD>
													</TR>
													<TR>
														<TD colSpan="2" align="right"></TD>
													</TR>
													<TR> <!--- panel para Bovinos menos Holando (parto/cria)!-->
														<TD colSpan="2" align="right">
															<DIV style="DISPLAY: none" id="panBovObs">
																<TABLE id="tblBovinoObs" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 20%" align="right">
																			<asp:Label id="lblBovParto" runat="server" cssclass="titulo">Parto:</asp:Label>&nbsp;</TD>
																		<TD noWrap>
																			<cc1:combobox id="cmbBovParto" class="combo" runat="server" Width="35%" NomOper="cs_productos_parto_cargar"></cc1:combobox>&nbsp;&nbsp;&nbsp;
																			<asp:Label id="lablBovCria" runat="server" cssclass="titulo">Tipo de Cria:</asp:Label>&nbsp;
																			<cc1:combobox id="cmbBovCria" class="combo" runat="server" Width="200px" NomOper="cs_productos_cria_cargar"></cc1:combobox></TD>
																	</TR>
																</TABLE>
															</DIV>
														</TD>
													</TR>
													<TR>
														<TD vAlign="top" align="right">
															<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
														<TD>
															<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="41px" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD vAlign="middle" align="right">
															<asp:Label id="lblSraNume" runat="server" cssclass="titulo">Nro.Producto:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtSraNume" runat="server" cssclass="cuadrotexto" Width="106px" MaxLength="12"
																esdecimal="False" MaxValor="9999999999999" enabled="false"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
														<TD>
															<asp:Label id="txtEsta" runat="server" cssclass="titulo" Width="100px"></asp:Label>&nbsp;&nbsp;
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel>
										&nbsp;&nbsp;
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" vAlign="top" align="center" colSpan="3"><asp:panel id="panDocu" runat="server" cssclass="titulo" Visible="False" Width="100%">
											<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" vAlign="top" colSpan="2" align="center">
														<asp:datagrid id="grdDocu" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdDocu_Page"
															OnUpdateCommand="mEditarDatosDocum" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
															CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="15px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																			<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="pido_id" ReadOnly="True" HeaderText="cldo_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="pido_docu_path" HeaderText="Documento"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="16" vAlign="bottom" colSpan="2"></TD>
												</TR>
												<TR>
													<TD align="right">
														<asp:Label id="lblFoto" runat="server" cssclass="titulo">Diagrama:</asp:Label></TD>
													<TD style="HEIGHT: 10px">
														<CC1:TEXTBOXTAB id="txtDocu" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG style="CURSOR: hand" id="imgDelDoc" onclick="javascript:mLimpiarPath('txtDocu','imgDelDoc');"
															alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<BR>
														<INPUT style="WIDTH: 340px; HEIGHT: 22px" id="filDocu" name="File1" size="49" type="file"
															runat="server"></TD>
												</TR>
												<TR>
													<TD vAlign="top" align="right">
														<asp:Label id="lblDocuObse" runat="server" cssclass="titulo">Observaciones:</asp:Label></TD>
													<TD style="HEIGHT: 10px">
														<CC1:TEXTBOXTAB id="txtDocuObse" runat="server" cssclass="cuadrotexto" Width="332px" Height="41px"
															TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="5" vAlign="bottom" colSpan="2"></TD>
												</TR>
												<TR>
													<TD height="34" vAlign="middle" colSpan="2" align="center">
														<asp:Button id="btnAltaDocu" runat="server" cssclass="boton" Width="110px" Text="Agregar Diagram."></asp:Button>&nbsp;
														<asp:Button id="btnBajaDocu" runat="server" cssclass="boton" Visible="true" Width="110px" Text="Eliminar Diagram."></asp:Button>&nbsp;
														<asp:Button id="btnModiDocu" runat="server" cssclass="boton" Visible="true" Width="110px" Text="Modificar Diagram."></asp:Button>&nbsp;
														<asp:Button id="btnLimpDocu" runat="server" cssclass="boton" Visible="true" Width="110px" Text="Limpiar Diagram."></asp:Button>&nbsp;&nbsp;<BUTTON style="WIDTH: 110px; FONT-WEIGHT: bold" id="btnDescDocu" class="boton" onclick="javascript:mDescargarDocumento('hdnDocuId','rg_productos_inscrip_docum','txtDocu');"
															runat="server" value="Descargar">Descargar</BUTTON>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
							</TBODY>
						</TABLE>
						</asp:panel>
						<DIV style="DISPLAY: inline" id="divgraba" runat="server">
							<ASP:PANEL id="panBotones" Runat="server">
								<TABLE width="100%">
									<TR>
										<TD align="center">
											<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
									</TR>
									<TR>
										<TD align="center">
											<asp:Label id="lblAprob" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
									</TR>
									<TR height="30">
										<TD vAlign="middle" align="center"><BUTTON style="WIDTH: 75px" id="btnPedigree" class="boton" onclick="btnPedigree_click();"
												runat="server" value="Pedigree">Pedigree</BUTTON>
										</TD>
									</TR>
								</TABLE>
								<TABLE width="100%">
									<TR height="30">
										<TD vAlign="middle" align="center">
											<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnErr" runat="server" cssclass="boton" Width="80px" Text="Errores"></asp:Button>&nbsp;
										</TD>
									</TR>
								</TABLE>
							</ASP:PANEL>
						</DIV>
						<DIV style="DISPLAY: none" id="divproce" runat="server">
							<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							</asp:panel>
						</DIV>
						</DIV>
					</td>
				</tr>
			</table>
			<DIV></DIV>
			<A id="Ancla" name="Ancla"></A></TD></TR></TBODY></TABLE> 
			<!--- FIN CONTENIDO ---> 
			</TBODY></TABLE> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="hdnPrdt_id" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnSexoSele" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnValorId" runat="server"></asp:textbox>
				<asp:textbox id="hdnModi" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnServId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnDocuId" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnSRA" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosTEPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnTEId" runat="server"></asp:textbox>
				<asp:textbox id="hdnFiltroProdCriaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnNombre" runat="server"></asp:textbox>
				<asp:textbox id="hdnSexo" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function mPorcPeti()
		{
			document.all("divgraba").style.display ="none";
			document.all("divproce").style.display ="inline";
		}
		
		if (document.all["Ancla"]!= null)
			document.location='#Ancla';
		try 
		{
		if (document.all('cmbRazaFil')!=null)
		   mNumeDenoConsul(document.all('cmbRazaFil').value,true);
		
		if (document.all('usrCria:cmbRazaCria')!=null)
		   mNumeDenoConsul(document.all('usrCria:cmbRazaCria').value,true);
		} catch(e){} 
		
		try 
		{
		 if (document.all('usrCria:cmbRazaCria').value!='')
		 {
		   	mNumeDenoConsul(document.all('usrCria:cmbRazaCria').value,false);
		   	mSetearCampos(document.all('usrCria:cmbRazaCria').value);
		 	mSetearPadresManual();
		 	mDiagrama(document.all('usrCria:cmbRazaCria').value);
		 	mCargarPelajes(document.all('usrCria:cmbRazaCria').value);
		 	//mCargarEstablecimientos();
		 	//mCargarPrefijos();
		 }
		
		 } catch(e){ } 

		if (document.all('btnDescDocu')!=null)
			document.all('btnDescDocu').disabled = (document.all("hdnDocuId").value=='');

	 	mSetearRazaProductos();
		mSetearServTE();
		/*
		if (document.all('txtusrCria:cmbRazaCria')!=null)
		{ 
			if (document.all('usrCria:cmbRazaCria').value!='')
			{
				var strRet = LeerCamposXML("razas", document.all('usrCria:cmbRazaCria').value, "raza_color");
				var strRaza=EjecutarMetodoXML("Utiles.EspeRazaJs", document.all('usrCria:cmbRazaCria').value).split(";");
				var strColor = '';
				if (strRet=='0') // la raza no lleva color
					strColor = 'none';
				if (document.all("cmbPelaAPeli")!=null)
				{
					document.all("cmbPelaAPeli").style.display=strColor;
					document.all("cmbPelaBPeli").style.display=strColor;
					document.all("cmbPelaCPeli").style.display=strColor;
					document.all("cmbPelaDPeli").style.display=strColor;
				}
				if (document.all("cmbHolanPelaA")!=null)
					document.all("cmbHolanPelaA").style.display=strColor;
				if (document.all("cmbBovPelaA")!=null)
				{
					document.all("lblColorBov").style.display=strColor;
					document.all("cmbBovPelaA").style.display=strColor;
					document.all("txtcmbBovPelaA").style.display=strColor;
				}
				if (document.all("cmbCrioPelaA")!=null)
				{
					document.all("cmbCrioPelaA").style.display=strColor;
					document.all("cmbCrioPelaB").style.display=strColor;
					document.all("cmbCrioPelaC").style.display=strColor;
					document.all("cmbCrioPelaD").style.display=strColor;
				} 
				if (document.all("cmbPelaA")!=null)
				{
					document.all("cmbPelaA").style.display=strColor;
					document.all("cmbPelaB").style.display=strColor;
					document.all("cmbPelaC").style.display=strColor;
					document.all("cmbPelaD").style.display=strColor;
				} 
			} 
		}
		*/
		mSetearSexo();
	
		</SCRIPT>
	</BODY>
</HTML>
