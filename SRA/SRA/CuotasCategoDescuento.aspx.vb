Imports System.Data
Imports AccesoBD

Namespace SRA

    Partial Class CuotasCategoDescuento
        Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

        'El Dise�ador de Web Forms requiere esta llamada.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblCateFil As System.Web.UI.WebControls.Label

        'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
        'No se debe eliminar o mover.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
            'No la modifique con el editor de c�digo.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"
        Private mstrTabla As String = "cuotas_catego_descuen"
        Private mstrParaPageSize As Integer
        Private mstrCmd As String
        Private mdsDatos As DataSet
        Private mstrConn As String

        Private Enum Columnas As Integer
            Id = 1

        End Enum
#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()
                If (Not Page.IsPostBack) Then
                    mSetearEventos()
                    mCargarCombos()
                    mConsultar()
                Else
                    If panDato.Visible Then
                        mdsDatos = Session(mstrTabla)
                    End If
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCargarCombos()
            clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCateFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "categorias_aux", cmbCateAux, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInstFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInst, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConcDesc, "S", "@conc_form_id = 22")
            cmbInstFil.Items.Insert(1, "(Ninguna)")
            cmbInstFil.Items(1).Value = "N"
        End Sub
        Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        End Sub

#End Region

#Region "Inicializacion de Variables"

        Public Sub mInicializar()

            clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)


        End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.EditItemIndex = -1
                If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = 0
                Else
                    grdDato.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mSelecCate()
            cmbCateAux.Valor = cmbCate.Valor
            If cmbCateAux.SelectedItem.Text = "S" Then
                cmbInst.Enabled = True
            Else
                cmbInst.Enabled = False
                cmbInst.SelectedIndex = 0
            End If
        End Sub

        Public Sub mConsultar()
            Try
                mstrCmd = "exec " + mstrTabla & "_consul"
                mstrCmd += " @cucd_cate_id = " + cmbCateFil.Valor.ToString
                mstrCmd += ",@cucd_inst_id = " + IIf(cmbInstFil.Valor.ToString = "N", "0", cmbInstFil.Valor.ToString)
                mstrCmd += ",@anio= " + txtAnioFil.Valor.ToString
                mstrCmd += ",@cucd_inst_ninguna = " + IIf(cmbInstFil.Valor.ToString = "N", "1", "0")

                clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try

        End Sub
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
        End Sub
        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mLimpiar()
            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    cmbCate.Valor = IIf(.Item("cucd_cate_id") Is DBNull.Value, "", .Item("cucd_cate_id"))
                    txtFechaIni.Fecha = IIf(.Item("cucd_desde_fecha") Is DBNull.Value, "", .Item("cucd_desde_fecha"))
                    txtFechaHasta.Fecha = IIf(.Item("cucd_hasta_fecha") Is DBNull.Value, "", .Item("cucd_hasta_fecha"))
                    txtCantCtas.Valor = IIf(.Item("cucd_cant") Is DBNull.Value, "", .Item("cucd_cant"))
                    txtImporte.Valor = IIf(.Item("cucd_valor") Is DBNull.Value, "", .Item("cucd_valor"))
                    cmbInst.Valor = IIf(.Item("cucd_inst_id") Is DBNull.Value, "", .Item("cucd_inst_id"))
                    cmbConcDesc.Valor = IIf(.Item("cucd_conc_id") Is DBNull.Value, "", .Item("cucd_conc_id"))
                    'mSelecCate()

                End With

                mSetearEditor(False)
                mMostrarPanel(True)

            End If
        End Sub
        Private Sub mAgregar()
            Try
                mLimpiar()
                btnBaja.Enabled = False
                btnModi.Enabled = False
                btnAlta.Enabled = True
                mMostrarPanel(True)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mLimpiar()

            hdnId.Text = ""
            txtFechaIni.Text = ""
            txtFechaHasta.Text = ""
            txtImporte.Text = ""
            txtCantCtas.Text = ""
            cmbConcDesc.Limpiar()
            cmbCate.Limpiar()
            cmbInst.Limpiar()
            lblTitu.Text = ""

            mCrearDataSet("")
            mSetearEditor(True)

        End Sub
        Private Sub mCerrar()
            mMostrarPanel(False)
        End Sub
        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)

            If (pbooVisi) Then
                hdnPage.Text = " "
            Else
                hdnPage.Text = ""
            End If
            panDato.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            btnAgre.Enabled = Not (panDato.Visible)
            panDato.Visible = pbooVisi
        End Sub

#End Region

#Region "Opciones de ABM"
        Private Sub mAlta()
            Try
                mValidaDatos()
                mGuardarDatos()
                Dim lobjCuotas As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

                lobjCuotas.Alta()

                mConsultar()

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mModi()
            Try
                mValidaDatos()
                mGuardarDatos()
                Dim lobjCuotas As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

                lobjCuotas.Modi()


                mConsultar()

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mBaja()
            Try
                Dim lintPage As Integer = grdDato.CurrentPageIndex

                Dim lobjCuotas As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
                lobjCuotas.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

                grdDato.CurrentPageIndex = 0

                mConsultar()

                If (lintPage < grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = lintPage
                    mConsultar()
                End If

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mGuardarDatos()
            Dim lintCpo As Integer

            With mdsDatos.Tables(0).Rows(0)
                .Item("cucd_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("cucd_cate_id") = cmbCate.Valor
                .Item("cucd_desde_fecha") = txtFechaIni.Fecha
                .Item("cucd_hasta_fecha") = txtFechaHasta.Fecha
                .Item("cucd_valor") = txtImporte.Valor
                .Item("cucd_cant") = txtCantCtas.Valor
                .Item("cucd_inst_id") = IIf(Request("cmbInst") Is Nothing, DBNull.Value, cmbInst.Valor)
                .Item("cucd_conc_id") = cmbConcDesc.Valor
                .Item("cucd_otra_inst") = IIf(cmbInst.Valor.ToString = "", 0, 1)
            End With


        End Sub
        Public Sub mCrearDataSet(ByVal pstrId As String)

            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla

            If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
                mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
            End If

            Session(mstrTabla) = mdsDatos

        End Sub
        Private Sub mValidaDatos()
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)

            If txtImporte.Valor <= 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar un importe mayor a 0")
            End If
            If txtCantCtas.Valor <= 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la cantidad de cuotas mayor a 0")
            End If
        End Sub
#End Region

#Region "Eventos de Controles"
        Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub
        Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub
        Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub
        Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
        End Sub
        Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub
        Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
            Try
                If cmbCateFil.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar una categor�a.")
                Else
                    mConsultar()
                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            mAgregar()
        End Sub

        Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
            Try
                Dim lstrRptName As String = "CuotasCategoDescuen"
                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

                lstrRpt += "&cate_id=" + cmbCateFil.Valor.ToString
                lstrRpt += "&inst_id=" + IIf(cmbInstFil.Valor.ToString = "N", "0", cmbInstFil.Valor.ToString)
                lstrRpt += "&inst_ninguna=" + IIf(cmbInstFil.Valor.ToString = "N", "1", "0")
                lstrRpt += "&anio=" + txtAnioFil.Valor.ToString

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                Response.Redirect(lstrRpt)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnHisto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHisto.Click

            Try
                If cmbCate.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la categor�a.")
                End If


                Dim lstrPagina As String = "MovimCuotasCatego_Pop.aspx?mcuc_cate_id=" + cmbCate.Valor.ToString & "&categoria=" & cmbCate.SelectedItem.Text
                clsWeb.gGenerarPopUp(Me, lstrPagina, 650, 400, 10, 75)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

    End Class
End Namespace
