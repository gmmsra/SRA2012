<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PlantillasTramites" CodeFile="PlantillasTramites.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Plantillas de Tr�mites</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Plantillas de Tr�mites</asp:label></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderStyle="Solid"
												BorderWidth="0">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="17" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblTipoTramiteFil" runat="server" cssclass="titulo">Tipo de Tr�mite:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbTipoTramiteFil" runat="server" Width="208px" NomOper="rg_tramites_tipos_cargar"
																						AceptaNull="False"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<tr>
										<TD colSpan="3" height="10"></TD>
									</tr>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="trap_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="_tipo_tramite" ReadOnly="True" HeaderText="Tipo de Tr�mite">
														<HeaderStyle Width="40%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="trap_desc" ReadOnly="True" HeaderText="Descripci�n">
														<HeaderStyle Width="60%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<tr>
										<TD colSpan="3" height="10"></TD>
									</tr>
									<TR>
										<TD align="left" colSpan="2"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
												ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Argerar un Nuevo Registro"></CC1:BOTONIMAGEN></TD>
										<TD align="right"><CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
												ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
									</TR>
									<tr>
										<TD colSpan="3" height="10"></TD>
									</tr>
									<TR>
										<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> Tr�mite</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkDetalle" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Detalle</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
														<TD width="1" background="imagenes/tab_fondo.bmp">
															<asp:linkbutton id="lnkRazas" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
																Width="80px" Height="21px" CausesValidation="False"> Razas</asp:linkbutton></TD>
														<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3">
											<asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderStyle="Solid" BorderWidth="1px" width="100%" Height="116px">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="lblTipoTramite" runat="server" cssclass="titulo">Tipo de Tr�mite:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 30px" vAlign="middle">
																			<cc1:combobox class="combo" id="cmbTipoTramite" runat="server" Width="208px" NomOper="rg_tramites_tipos_cargar"
																				AceptaNull="true" obligatorio="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 30px" vAlign="middle">
																			<cc1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="80%" obligatorio="true"></cc1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 30px" vAlign="top" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 60px" vAlign="middle">
																			<cc1:textboxtab id="txtObse" runat="server" cssclass="textolibre" Width="90%" TextMode="MultiLine"
																				Rows="10" height="100%"></cc1:textboxtab></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panDetalle" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD vAlign="top" align="center" colSpan="2">
																			<asp:datagrid id="grdDetalle" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosDeta" OnPageIndexChanged="DataGridDeta_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%" PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="trpd_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_requisito" ReadOnly="True" HeaderText="Requisito">
																						<HeaderStyle Width="100%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_obligatorio" ReadOnly="True" HeaderText="Obligatorio"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 20px" vAlign="top" align="right">
																			<asp:Label id="lblDetaReq" runat="server" cssclass="titulo">Requisito:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 20px" vAlign="middle">
																			<cc1:combobox class="combo" id="cmbDetaReq" runat="server" cssclass="cuadrotexto" Width="320px"
																				NomOper="rg_requisitos_cargar" Height="20px" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 20px" vAlign="top" align="right">
																		<TD style="WIDTH: 75%; HEIGHT: 20px" vAlign="middle">
																			<asp:checkbox id="chkDetaOblig" CssClass="titulo" Runat="server" Text="Obligatorio"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:Label id="lblBajaDeta" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="2">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panRazas" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TablaRazas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD vAlign="top" align="center" colSpan="2">
																			<asp:datagrid id="grdRazas" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosRaza" OnPageIndexChanged="DataGridRaza_Page" CellPadding="1"
																				GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%"
																				PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="trdr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_espe_desc" ReadOnly="True" HeaderText="Especie">
																						<HeaderStyle Width="40%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_raza_desc" ReadOnly="True" HeaderText="Raza">
																						<HeaderStyle Width="40%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblEspecie" runat="server" cssclass="titulo">Especie:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox class="combo" id="cmbEspecie" runat="server" Width="176px" onchange="mCargarRaza('cmbEspecie','cmbRaza')"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 22px" vAlign="top" align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 22px">
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="razas_cargar"
																				filtra="true" MostrarBotones="False" onchange="setCmbEspecie('cmbEspecie','cmbRaza')"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:Label id="lblBajaRaza" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="2">
																			<asp:Button id="btnAltaRaza" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaRaza" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiRaza" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpRaza" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox><asp:textbox id="hdnRazaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
			
		function mCargarRaza(pEspe,pRaza)
		{
		    var sFiltro = "";
		    var sOpcion="S";
		    
		    if (document.all(pEspe).value != '')
				sFiltro = document.all(pEspe).value;
			if (pRaza=="cmbRazaFil")	
				sOpcion="T";
 	       LoadComboXML("razas_cargar", sFiltro, pRaza, sOpcion);
	    }
	    
	    function setCmbEspecie(pEspe,pRaza)
	    {
			if (document.all(pRaza).value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all(pRaza).value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all(pEspe).value = vstrRet[0];
			}		
	    }
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
