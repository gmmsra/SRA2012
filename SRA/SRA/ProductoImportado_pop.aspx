<%@ Reference Control="~/controles/usrproductoextranjero.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ProductoImportado_pop" CodeFile="ProductoImportado_pop.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROEXT" Src="controles/usrProductoExtranjero.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Producto Importado</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
		function isNumber(evt)
		{
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}
		function mSetearRaza(strFiltro)
		{
		    mCargarPelajes(strFiltro);  
	  	}
	  	function mCargarPelajes(strFiltro)
		{
			mNumeDenoConsul(strFiltro,false);
			if (strFiltro!='')
			{
				vsRet = EjecutarMetodoXML("Utiles.EspeRazaJs", strFiltro).split(";");
     
				if (document.all('cmbPelaAPeli').value == '')
				{
					LoadComboXML("rg_pelajes_cargar", strFiltro+",1", "cmbPelaAPeli", "S");
				 }
		      }
		}

		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!-----	CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="2"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Producto	Importado</asp:label></TD>
							</TR>
							<TR>
								<TD colSpan="2" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Height="116px" Visible="False"
										BorderWidth="1px" BorderStyle="Solid">
										<P align="right">
											<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
												align="left">
												<TBODY>
													<TR>
														<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo"></asp:label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="images\Close.bmp"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 17px"><cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotextoMayusculas" Height="20px"
																Obligatorio="true" onchange="javascript:Combo_change(this);" NomOper="razas_cargar" MostrarBotones="False" filtra="true" AceptaNull="False"
																AutoPostBack="True" Width="180px"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblSexo" runat="server" cssclass="titulo">Sexo:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 17px"><asp:dropdownlist id="cmbSexo" runat="server" AutoPostBack="True" Width="100px" CssClass="combo">
																<asp:ListItem Selected="True" value="">(Todos)</asp:ListItem>
																<asp:ListItem Value="0">Hembra</asp:ListItem>
																<asp:ListItem Value="1">Macho</asp:ListItem>
															</asp:dropdownlist></TD>
													</TR>
													<TR id="trFechaNaci" runat="server">
														<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right"><asp:label id="lblFechaNaci" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"><cc1:datebox id="txtFechaNaci" runat="server" cssclass="cuadrotexto" width="68px"></cc1:datebox>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblAsoc" runat="server" cssclass="titulo">Asociación:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 17px"><anthem:panel id="PnlcmbProdAsoc" runat="server">
																<cc1:combobox id="cmbAsociacion" class="combo" runat="server" cssclass="cuadrotextoMayusculas"
																	Width="500" AceptaNull="False" filtra="True" NomOper="asociaciones_cargar" onchange="Combo_change(this)"
																	Obligatorio="true" mostrarbotones="False"></cc1:combobox>
															</anthem:panel></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblNroExtranjero" runat="server" cssclass="titulo">Nro. Extranjero:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 17px"><CC1:TEXTBOXTAB id="txtNroExtranjero" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="true"
																Width="80px" valign="left" MaxLength="20"></CC1:TEXTBOXTAB>&nbsp;
															<asp:label id="lblRPExtranjero" runat="server" cssclass="titulo">RP	Extranjero:</asp:label>&nbsp;
															<CC1:TEXTBOXTAB id="txtRP" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="false" Width="80px"
																valign="left" MaxLength="8"></CC1:TEXTBOXTAB>&nbsp;
															<asp:label id="lblRPNac" runat="server" cssclass="titulo">RP Nacional:</asp:label>&nbsp;
															<CC1:TEXTBOXTAB id="txtRPNac" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="false"
																Width="80px" valign="left" MaxLength="8"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR id="trTipoRegistro" runat="server">
														<TD align="right"><asp:label id="lblTipoRegistro" runat="server" cssclass="titulo">Tipo Reg.:</asp:label>&nbsp;</TD>
														<td><anthem:panel id="PnlTipoRegistro" runat="server">
																<TABLE>
																	<TR>
																		<TD id="tdRazaCria" width="50" noWrap align="left" runat="server">
																			<asp:TextBox id="txtTipoRegistroCodi" runat="server" cssclass="cuadrotexto" Width="50px" AutoPostBack="True"></asp:TextBox></TD>
																		<TD id="tdRazaCria1" width="120" noWrap align="left" runat="server">
																			<asp:DropDownList id="cmbTipoRegistro" runat="server" cssclass="combo" Width="120px" AutoPostBack="True"></asp:DropDownList></TD>
																		<TD align="right">
																			<asp:label id="lblVariedad" runat="server" cssclass="titulo">Variedad:</asp:label>&nbsp;</TD>
																		<TD id="Td1" width="50" noWrap align="left" runat="server">
																			<asp:TextBox id="txtVariedadCodi" runat="server" cssclass="cuadrotexto" Width="50px" AutoPostBack="True"></asp:TextBox></TD>
																		<TD id="Td2" width="120" noWrap align="left" runat="server">
																			<asp:DropDownList id="cmbVariedad" runat="server" cssclass="combo" Width="120px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																</TABLE>
															</anthem:panel></td>
													</TR>
													<TR id="trPelaAPeli" runat="server">
														<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblPelaAPeli" runat="server" cssclass="titulo">Color:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 17px"><anthem:panel id="pnlPelaje" runat="server">
																<cc1:combobox id="cmbPelaAPeli" class="combo" runat="server" cssclass="cuadrotexto" Width="160px"
																	filtra="True" MostrarBotones="False" nomoper="rg_pelajes_cargar"></cc1:combobox>
															</anthem:panel></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblNombre" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
														<TD style="WIDTH: 80%; HEIGHT: 17px"><CC1:TEXTBOXTAB id="txtNombre" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="false"
																Width="500px" valign="left" MaxLength="35"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<tr>
														<td align="right"><asp:label id="lblApodo" runat="server" cssclass="titulo">Apodo:</asp:label>&nbsp;</td>
														<td align="left"><cc1:textboxtab id="txtApodo" runat="server" cssclass="textolibreMayusculas" Height="20" Width="500"
																TextMode="MultiLine" Rows="1"></cc1:textboxtab></td>
													</tr>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center"><asp:button id="btnNewServ" runat="server" cssclass="boton" CausesValidation="False" Width="110px"
																Text="Informar Servicio"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnGuardar" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Guardar"></asp:button>&nbsp;
															<asp:button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Limpiar"></asp:button></TD>
													</TR>
							</TR>
						</TABLE>
						</P></asp:panel></td>
				</tr>
				<tr>
					<TD colSpan="2" align="center"><asp:panel id="panServicio" runat="server" cssclass="titulo" width="100%" Height="116px" Visible="false"
							BorderWidth="1px" BorderStyle="Solid">
							<P align="right">
								<TABLE style="WIDTH: 100%" id="Table33" class="FdoFld" border="0" cellPadding="0" align="left">
									<TR>
										<TD height="25">
											<asp:label id="lblTitu2" runat="server" cssclass="titulo">Informar Servicio</asp:label></TD>
										<TD vAlign="top" align="right">&nbsp;
											<asp:imagebutton id="imgCloseServ" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
									</TR>
									<TR id="Tr1" runat="server">
										<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
											<asp:label id="Label1" runat="server" cssclass="titulo">Fecha Servicio:</asp:label>&nbsp;</TD>
										<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
											<cc1:datebox id="txtFechaServicio" runat="server" width="68px" cssclass="cuadrotexto"></cc1:datebox>&nbsp;</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 20%; HEIGHT: 10px" vAlign="middle" background="imagenes/formfdofields.jpg"
											noWrap align="right">
											<asp:Label id="lblSeti" runat="server" cssclass="titulo">Tipo de Servicio:</asp:Label>&nbsp;</TD>
										<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
											<cc1:combobox id="cmbSeti" class="combo" runat="server" Width="140px" AceptaNull="True" Obligatorio="False"></cc1:combobox></TD>
									</TR>
									<TR>
										<TD colSpan="2">
											<TABLE>
												<TR>
													<TD style="WIDTH: 10%; HEIGHT: 25px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblProducto1" runat="server" cssclass="titulo">Padre:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
														<UC1:PROEXT id="usrProdppe" runat="server" AutoPostBack="False" AceptaNull="false" MuestraBotonAgregaImportado="false"
															TipoControl="padre" AutoGeneraHBA="True" Tabla="productos" Saltos="1,2" Ancho="830" EsPropietario="True"
															FilTipo="S" MuestraDesc="false"></UC1:PROEXT></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD colSpan="2" align="center">
											<asp:button id="bntAgrePadre" runat="server" cssclass="boton" CausesValidation="False" Width="100px"
												Text="Agregar Padre"></asp:button>&nbsp;
											<asp:button id="btnGuardarServ" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
												Text="Guardar"></asp:button>&nbsp;
											<asp:button id="btnLimpServ" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
												Text="Limpiar"></asp:button></TD>
									</TR>
								</TABLE>
							</P>
						</asp:panel></TD>
				</tr>
				<TR>
					<TD colSpan="2" align="center"><asp:panel id="panDatoPadre" runat="server" cssclass="titulo" width="100%" Height="116px" Visible="False"
							BorderWidth="1px" BorderStyle="Solid">
							<P align="right">
								<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
									align="left">
									<TBODY>
										<TR>
											<TD height="5"><asp:label id="lblTituPadre" runat="server" cssclass="titulo">Padre Servicio</asp:label></TD>
											<TD vAlign="top" align="right">&nbsp;
												<asp:imagebutton id="imgClosePadre" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="images\Close.bmp"></asp:imagebutton></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblRazaPadre" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;</TD>
											<TD style="WIDTH: 80%; HEIGHT: 17px"><cc1:combobox id="cmbRazaPadre" class="combo" runat="server" cssclass="cuadrotextoMayusculas"
													Height="20px" Obligatorio="true" onchange="javascript:Combo_change(this);" NomOper="razas_cargar" MostrarBotones="False" filtra="true"
													AceptaNull="False" AutoPostBack="True" Width="180px"></cc1:combobox></TD>
										</TR>
										<TR>
											<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblSexoPadre" runat="server" cssclass="titulo">Sexo:</asp:label>&nbsp;</TD>
											<TD style="WIDTH: 80%; HEIGHT: 17px"><cc1:combobox id="cmbSexoPadre" class="combo" runat="server" Obligatorio="true" AceptaNull="True"
													Width="100px">
													<asp:ListItem value="">(Todos)</asp:ListItem>
													<asp:ListItem Value="0">Hembra</asp:ListItem>
													<asp:ListItem Selected="True" Value="1">Macho</asp:ListItem>
												</cc1:combobox></TD>
										</TR>
										<TR id="trFechaNaciPadre" runat="server">
											<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right"><asp:label id="lblFechaNaciPadre" runat="server" cssclass="titulo">Fecha Nacimiento:</asp:label>&nbsp;</TD>
											<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"><cc1:datebox id="txtFechaNaciPadre" runat="server" cssclass="cuadrotexto" width="68px"></cc1:datebox>&nbsp;
											</TD>
										</TR>
										<TR>
											<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblAsocPadre" runat="server" cssclass="titulo">Asociación:</asp:label>&nbsp;</TD>
											<TD style="WIDTH: 80%; HEIGHT: 17px"><anthem:panel id="PnlcmbProdAsocPadre" runat="server">
													<cc1:combobox id="cmbAsociacionPadre" class="combo" runat="server" cssclass="cuadrotextoMayusculas"
														Width="500" AceptaNull="False" filtra="True" NomOper="asociaciones_cargar" onchange="Combo_change(this)"
														Obligatorio="true" mostrarbotones="False"></cc1:combobox>
												</anthem:panel></TD>
										</TR>
										<TR>
											<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblNroExtranjeroPadre" runat="server" cssclass="titulo">Nro. Extranjero:</asp:label>&nbsp;</TD>
											<TD style="WIDTH: 80%; HEIGHT: 17px"><CC1:TEXTBOXTAB id="txtNroExtranjeroPadre" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="true"
													Width="80px" valign="left" MaxLength="20"></CC1:TEXTBOXTAB>&nbsp;
												<asp:label id="lblRPExtranjeroPadre" runat="server" cssclass="titulo">RP	Extranjero:</asp:label>&nbsp;
												<CC1:TEXTBOXTAB id="txtRPPadre" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="false"
													Width="80px" valign="left" MaxLength="8"></CC1:TEXTBOXTAB>&nbsp;
												<asp:label id="lblRPNacPadre" runat="server" cssclass="titulo">RP Nacional:</asp:label>&nbsp;
												<CC1:TEXTBOXTAB id="txtRPNacPadre" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="false"
													Width="80px" valign="left" MaxLength="8"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
										</TR>
										<TR id="trTipoRegistroPadre" runat="server">
											<TD align="right"><asp:label id="lblTipoRegistroPadre" runat="server" cssclass="titulo">Tipo Reg.:</asp:label>&nbsp;</TD>
											<td><anthem:panel id="PnlTipoRegistroPadre" runat="server">
													<TABLE>
														<TR>
															<TD id="tdRazaCriaPadre" width="50" noWrap align="left" runat="server">
																<asp:TextBox id="txtTipoRegistroCodiPadre" runat="server" cssclass="cuadrotexto" Width="50px"
																	AutoPostBack="True"></asp:TextBox></TD>
															<TD id="tdRazaCria1Padre" width="120" noWrap align="left" runat="server">
																<asp:DropDownList id="cmbTipoRegistroPadre" runat="server" cssclass="combo" Width="120px" AutoPostBack="True"></asp:DropDownList></TD>
															<TD align="right">
																<asp:label id="lblVariedadPadre" runat="server" cssclass="titulo">Variedad:</asp:label>&nbsp;</TD>
															<TD id="Td1Padre" width="50" noWrap align="left" runat="server">
																<asp:TextBox id="txtVariedadCodiPadre" runat="server" cssclass="cuadrotexto" Width="50px" AutoPostBack="True"></asp:TextBox></TD>
															<TD id="Td2Padre" width="120" noWrap align="left" runat="server">
																<asp:DropDownList id="cmbVariedadPadre" runat="server" cssclass="combo" Width="120px" AutoPostBack="True"></asp:DropDownList></TD>
														</TR>
													</TABLE>
												</anthem:panel></td>
										</TR>
										<TR id="trPelaAPeliPadre" runat="server">
											<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblPelaAPeliPadre" runat="server" cssclass="titulo">Color:</asp:label>&nbsp;</TD>
											<TD style="WIDTH: 80%; HEIGHT: 17px"><anthem:panel id="pnlPelajePadre" runat="server">
													<cc1:combobox id="cmbPelaAPeliPadre" class="combo" runat="server" cssclass="cuadrotexto" Width="160px"
														filtra="True" MostrarBotones="False" nomoper="rg_pelajes_cargar"></cc1:combobox>
												</anthem:panel></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
												align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" align="right"><asp:label id="lblNombrePadre" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
											<TD style="WIDTH: 80%; HEIGHT: 17px"><CC1:TEXTBOXTAB id="txtNombrePadre" runat="server" cssclass="cuadrotextoMayusculas" Obligatorio="false"
													Width="500px" valign="left" MaxLength="35"></CC1:TEXTBOXTAB></TD>
										</TR>
										<TR>
											<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
										</TR>
										<tr>
											<td align="right"><asp:label id="lblApodoPadre" runat="server" cssclass="titulo">Apodo:</asp:label>&nbsp;</td>
											<td align="left"><cc1:textboxtab id="txtApodoPadre" runat="server" cssclass="textolibreMayusculas" Height="20" Width="500"
													TextMode="MultiLine" Rows="1"></cc1:textboxtab></td>
										</tr>
										<TR>
											<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
										</TR>
										<TR>
											<TD colSpan="2" align="center"><asp:button id="btnGuardarPadre" runat="server" cssclass="boton" CausesValidation="False" Width="100px"
													Text="Guardar Padre"></asp:button>&nbsp;
												<asp:button id="btnLimpPadre" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
													Text="Limpiar"></asp:button></TD>
										</TR>
				</TR>
			</table>
			</P></asp:panel></TD></TR></TBODY></TABLE> 
			<!-----------------	FIN	RECUADRO ----------------->
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnBitGenNroHBA" runat="server"></asp:textbox>
				<asp:textbox id="hdnNacionalidad" runat="server"></asp:textbox>
				<asp:textbox id="hdnFechaParametro" runat="server"></asp:textbox>
				<asp:textbox id="hdnTipoProducto" runat="server"></asp:textbox>
				<asp:textbox id="hdnTipoControl" runat="server"></asp:textbox>
				<asp:textbox id="hdnIdCtrlPropietario" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		</SCRIPT>
	</BODY>
</HTML>
