Namespace SRA

Partial Class ProductoImportado_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents btnAgre As NixorControls.BotonImagen
    'Protected WithEvents cmbSexo As NixorControls.ComboBox
    Protected WithEvents lblVariadad As System.Web.UI.WebControls.Label
    Protected WithEvents lblProducto As System.Web.UI.WebControls.Label
    Protected WithEvents pnl As System.Web.UI.WebControls.Panel
    Protected WithEvents lmgCloseServ As System.Web.UI.WebControls.ImageButton



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
    'Private strTipoControl As String
    Private strTipoProducto As String
    Private strRazaID As String
    Private strRazaIDPadre As String


#End Region

#Region "Operaciones sobre la Pagina"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
                panDato.Visible = True
                panServicio.Visible = False
                CargarDatosURL()
                Me.SetearControles()
                clsWeb.gCargarRefeCmb(mstrConn, "rg_servi_tipos", cmbSeti, "S")
            Else
                mdsDatos = Session(mstrTabla)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Metodos Privados"

    Private Sub CargarDatosURL()

        Me.hdnTipoControl.Text = mValorParametro(Request.QueryString("TipoControl"))
        Me.hdnIdCtrlPropietario.Text = mValorParametro(Request.QueryString("IdCtrlPropietarioParam"))

        If (Me.hdnTipoControl.Text = "prodImp" And Me.hdnIdCtrlPropietario.Text = "") Then
            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append("alert('No se puede ingresar el producto si no se informa el comprador');window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)
        Else
            cmbRaza.Valor = mValorParametro(Request.QueryString("Raza"))
            strRazaID = mValorParametro(Request.QueryString("Raza"))
            cmbSexo.SelectedValue = mValorParametro(Request.QueryString("Sexo"))

            Me.CargarCombos()
            If (cmbSexo.SelectedValue Is DBNull.Value Or cmbSexo.SelectedValue.ToString() = "") Then
                Me.cmbSexo.Enabled = True
            Else
                Me.cmbSexo.Enabled = False
            End If

            cmbAsociacion.Valor = mValorParametro(Request.QueryString("Asoc"))
            txtNroExtranjero.Valor = mValorParametro(Request.QueryString("NroExtr"))
            txtRP.Valor = mValorParametro(Request.QueryString("RPExtr"))
            txtNombre.Valor = mValorParametro(Request.QueryString("NombreProd"))
            txtApodo.Valor = mValorParametro(Request.QueryString("ApodoProd"))
            hdnBitGenNroHBA.Text = mValorParametro(Request.QueryString("GenHBA"))
            hdnNacionalidad.Text = mValorParametro(Request.QueryString("Ndad"))
            hdnFechaParametro.Text = mValorParametro(Request.QueryString("FParam"))
            hdnTipoProducto.Text = mValorParametro(Request.QueryString("TipoProducto"))

            ' Dario 2014-07-03
            Me.txtFechaNaci.Text = mValorParametro(Request.QueryString("FechaNaci"))
            Me.txtRPNac.Text = mValorParametro(Request.QueryString("RPNac"))
            Me.cmbTipoRegistro.SelectedValue = mValorParametro(Request.QueryString("TipoRegistro"))
            If (Me.cmbTipoRegistro.SelectedValue <> "") Then
                Me.cmbTipoRegistro_SelectedIndexChanged(Nothing, Nothing)
            End If
            Me.cmbVariedad.SelectedValue = mValorParametro(Request.QueryString("Variedad"))
            If (Me.cmbVariedad.SelectedValue <> "") Then
                Me.cmbVariedad_SelectedIndexChanged(Nothing, Nothing)
            End If
            Me.cmbPelaAPeli.SelectedValue = mValorParametro(Request.QueryString("PelaAPeli"))
            Me.PnlTipoRegistro.UpdateAfterCallBack = True
        End If
    End Sub
    ' Dario 2014-07-01    
    Private Sub SetearControles()
        Me.cmbSexo.Enabled = False
        Me.trPelaAPeli.Style.Add("display", "none")
        Me.btnNewServ.Visible = False

        If (Me.hdnTipoControl.Text = "padre") Then
            Me.cmbSexo.SelectedValue = "1"
        ElseIf (Me.hdnTipoControl.Text = "madre") Then
            Me.cmbSexo.SelectedValue = "0"
        Else
            Me.cmbSexo.SelectedValue = ""
            Me.cmbSexo.Enabled = True
            Me.trPelaAPeli.Style.Add("display", "inline")
        End If
    End Sub

    Private Function mValorParametro(ByVal pstrPara As String) As String
        Dim lstrPara As String
        If pstrPara Is Nothing Then
            lstrPara = ""
        Else
            If pstrPara Is System.DBNull.Value Then
                lstrPara = ""
            Else
                lstrPara = pstrPara
            End If
        End If
        Return (lstrPara)
    End Function

    Private Sub CargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaPadre, "id", "descrip", "S")

        If (cmbRaza.Valor Is DBNull.Value Or cmbRaza.Valor.ToString() = "" Or strRazaID = "") Then
            SRA_Neg.Utiles.gSetearRaza(cmbRaza)
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= 0", cmbAsociacion, "id", "descrip", "S", False, True)

            Me.txtVariedadCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = 0", cmbVariedad, "id", "descrip", "S", False, True)

            Me.txtTipoRegistroCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =0", cmbTipoRegistro, "id", "descrip", "S", False, True)

            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id= 0,@pela_rgub_id=1", cmbPelaAPeli, "id", "descrip", "S")
        Else
            cmbRaza.Valor = strRazaID
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= " & cmbRaza.Valor, cmbAsociacion, "id", "descrip", "S", False, True)

            Me.txtVariedadCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = " & cmbRaza.Valor, cmbVariedad, "id", "descrip", "S", False, True)

            Me.txtTipoRegistroCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =" & cmbRaza.Valor, cmbTipoRegistro, "id", "descrip", "S", False, True)

            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & cmbRaza.Valor & ",@pela_rgub_id=1", cmbPelaAPeli, "id", "descrip", "S")
        End If

        Me.PnlcmbProdAsoc.UpdateAfterCallBack = True
        Me.PnlTipoRegistro.UpdateAfterCallBack = True
        Me.pnlPelaje.UpdateAfterCallBack = True

        If (cmbRazaPadre.Valor Is DBNull.Value Or cmbRazaPadre.Valor.ToString() = "" Or strRazaIDPadre = "") Then
            SRA_Neg.Utiles.gSetearRaza(cmbRaza)
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= 0", cmbAsociacionPadre, "id", "descrip", "S", False, True)

            Me.txtVariedadCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = 0", cmbVariedadPadre, "id", "descrip", "S", False, True)

            Me.txtTipoRegistroCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =0", cmbTipoRegistroPadre, "id", "descrip", "S", False, True)

            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id= 0,@pela_rgub_id=1", cmbPelaAPeliPadre, "id", "descrip", "S")
        Else
            cmbRazaPadre.Valor = strRazaIDPadre
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= " & cmbRazaPadre.Valor, cmbAsociacion, "id", "descrip", "S", False, True)

            Me.txtVariedadCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = " & cmbRaza.Valor, cmbVariedadPadre, "id", "descrip", "S", False, True)

            Me.txtTipoRegistroCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =" & cmbRaza.Valor, cmbTipoRegistroPadre, "id", "descrip", "S", False, True)

            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & cmbRaza.Valor & ",@pela_rgub_id=1", cmbPelaAPeliPadre, "id", "descrip", "S")
        End If

        Me.PnlcmbProdAsocPadre.UpdateAfterCallBack = True
        Me.PnlTipoRegistroPadre.UpdateAfterCallBack = True
        Me.pnlPelajePadre.UpdateAfterCallBack = True
    End Sub

    Private Function ValidarDatos() As Boolean
        If cmbRaza.Valor = 0 Then
            Throw New Exception("Debe seleccionar una raza.")
            Return False
        End If

        If cmbSexo.SelectedValue Is DBNull.Value Then
            Throw New Exception("Debe seleccionar el sexo.")
            Return False
        End If

        If cmbAsociacion.Valor = 0 Then
            Throw New Exception("Debe seleccionar una asociaci�n.")
            Return False
        End If

        ' Dario 2014-06-30
        'If txtNroExtranjero.Text.Length = 0 Then
        '    Throw New Exception("Debe completar el n�mero de extranjero.")
        '    Return False
        'End If

        If txtNombre.Text.Length = 0 Then
            Throw New Exception("Debe completar el nombre.")
            Return False
        End If

        Return True

    End Function

    Private Sub mSeleccionarProducto(ByVal pstrProdId As String, ByVal pstrProdRazaCodi As String, ByVal pstrProdSexo As String _
                                    , ByVal pstrProdAsocCodi As String, ByVal pstrProdNumeExtr As String, ByVal pstrProdNomb As String _
                                    , ByVal pstrProdApodo As String, ByVal pstrProdRPExtr As String, ByVal pstrGenNroHBA As String _
                                    , ByVal pstrNacionalidad As String, ByVal pDateFechaParametro As String, ByVal Descrip As String _
                                    , ByVal pstrFechaNaci As String, ByVal pstrRPNac As String, ByVal pstrTipoRegistro As String _
                                    , ByVal pstrVariedad As String, ByVal pstrPelaAPeli As String)

        Dim lsbMsg As New StringBuilder

        lsbMsg.Append("<SCRIPT language='javascript'>")

        'Raza ID.
        lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdRaza'].value='{1}';", Request.QueryString("ctrlId"), pstrProdRazaCodi))
        'Raza_onchange().
        lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdRaza'].onchange();", Request.QueryString("ctrlId")))
        'Asociaci�n ID.
        lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdAsoc'].value='{1}';", Request.QueryString("ctrlId"), pstrProdAsocCodi))
        'Asociaci�n_onchange().
        lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdAsoc'].onchange();", Request.QueryString("ctrlId")))
        'Nro. Extranjero.
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdNumeExtr)))
        'RP Extranjero.
        lsbMsg.Append(String.Format("if(window.opener.document.all['{0}:txtRPExtr'] != null)", Request.QueryString("ctrlId")))
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtRPExtr'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdRPExtr)))
        'Producto ID.
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtId'].value='{1}';", Request.QueryString("ctrlId"), pstrProdId))
        'Nombre.
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtProdNomb'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdNomb)))
        ' Apodo
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtApodo'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdApodo)))

        ' GenNroHBA
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtAutoGeneraHBA'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrGenNroHBA)))
        ' Nacionalidad
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtNacionalidad'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrNacionalidad)))
        ' Descripcion
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtDesc'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena("HBA: " + Descrip)))

        'Sexo.
        lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:cmbProdSexo']!=null)", Request.QueryString("ctrlId")))
        lsbMsg.Append("{")
        Select Case pstrProdSexo
            Case 1 'Macho.
                lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].value = 1;", Request.QueryString("ctrlId")))
                lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtSexoId'].value = 1;", Request.QueryString("ctrlId")))
            Case 0 'Hembra.
                lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].value = 0;", Request.QueryString("ctrlId")))
                lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtSexoId'].value = 0;", Request.QueryString("ctrlId")))
            Case Else
                lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].value = '';", Request.QueryString("ctrlId")))
                lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtSexoId'].value = '';", Request.QueryString("ctrlId")))
        End Select
        lsbMsg.Append("}")

        ' Dario 2014-07-03
        ' fecha de nacimiento
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:hidFechaNaci'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrFechaNaci)))
        ' rp nacional
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:hidRPNac'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrRPNac)))
        ' tipo registro
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:hidTipoRegistro'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrTipoRegistro)))
        ' variedad
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:hidVariedad'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrVariedad)))
        ' color
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:hidPelaAPeli'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrPelaAPeli)))
        ' fin Dario 2014-07-03

        'NroExtranjero.onchange().
        lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:txtCodi']!=null && window.opener.document.all['{0}:txtCodi'].value!='')", Request.QueryString("ctrlId")))
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].onchange();", Request.QueryString("ctrlId")))
        lsbMsg.Append("else{")
        'RPExtr.onchange().
        lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:txtRPExtr']!=null && window.opener.document.all['{0}:txtRPExtr'].value!='')", Request.QueryString("ctrlId")))
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtRPExtr'].onchange();", Request.QueryString("ctrlId")))
        lsbMsg.Append("}")

        lsbMsg.Append(String.Format("window.opener.__doPostBack(window.opener.document.all['{0}:txtId'].name,'');", Request.QueryString("ctrlId")))
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")

        Response.Write(lsbMsg.ToString)
    End Sub

    Private Sub Guardar()
        Try
            If ValidarDatos() Then
                Dim dsResultado As New DataSet
                'Obtengo el Producto ID.
                If hdnBitGenNroHBA.Text = "" Then
                    hdnBitGenNroHBA.Text = "False"
                End If

                    dsResultado = Business.Productos.ProductosBusiness.GuardarProductoImportado(
                                        IIf(Me.cmbRaza.Valor <> "", Me.cmbRaza.Valor, -1) _
                                    , cmbSexo.SelectedValue _
                                    , IIf(Me.cmbAsociacion.Valor <> "", Me.cmbAsociacion.Valor, -1), txtNroExtranjero.Text _
                                    , IIf(txtRP.Text.Length > 0, txtRP.Text, String.Empty) _
                                    , IIf(txtNombre.Text.Length > 0, txtNombre.Text, String.Empty) _
                                    , IIf(txtApodo.Text.Length > 0, txtApodo.Text, String.Empty) _
                                    , hdnBitGenNroHBA.Text _
                                    , hdnNacionalidad.Text _
                                    , hdnFechaParametro.Text _
                                    , hdnTipoProducto.Text _
                                    , IIf(txtFechaNaci.Text.Length > 0, txtFechaNaci.Text, String.Empty) _
                                    , IIf(txtRPNac.Text.Length > 0, txtRPNac.Text, String.Empty) _
                                    , IIf(Me.cmbTipoRegistro.SelectedValue = "", -1, Me.cmbTipoRegistro.SelectedValue) _
                                    , IIf(Me.cmbVariedad.SelectedValue = "", -1, Me.cmbVariedad.SelectedValue) _
                                    , IIf(Me.cmbPelaAPeli.SelectedValue = "", -1, Me.cmbPelaAPeli.SelectedValue) _
                                    , IIf(usrProdppe.Valor > 0, usrProdppe.Valor, -1) _
                                    , IIf(Me.cmbSeti.SelectedValue <> "", Me.cmbSeti.SelectedValue, -1) _
                                    , IIf(Me.txtFechaServicio.Text <> "", Me.txtFechaServicio.Fecha, System.DateTime.MinValue) _
                                    , IIf(Me.hdnIdCtrlPropietario.Text <> "", Me.hdnIdCtrlPropietario.Text, -1) _
                                    , Me.Session("sUserId"))

                    If (dsResultado.Tables.Count > 0 And dsResultado.Tables(0).Rows.Count > 0) Then
                    'Pego los datos del producto obtenido en el control usrProductoExtranjero.
                    mSeleccionarProducto(dsResultado.Tables(0).Rows(0).Item("prdt_id"), _
                                         dsResultado.Tables(0).Rows(0).Item("raza_codi"), cmbSexo.SelectedValue, _
                                         dsResultado.Tables(0).Rows(0).Item("asoc_codi"), txtNroExtranjero.Text, _
                                         IIf(txtNombre.Text.Length > 0, txtNombre.Text, String.Empty), _
                                         IIf(txtApodo.Text.Length > 0, txtApodo.Text, String.Empty), _
                                         IIf(txtRP.Text.Length > 0, txtRP.Text, String.Empty), _
                                         IIf(hdnBitGenNroHBA.Text.Length > 0, hdnBitGenNroHBA.Text, String.Empty), _
                                         IIf(hdnNacionalidad.Text.Length > 0, hdnNacionalidad.Text, String.Empty), _
                                         IIf(Not dsResultado.Tables(0).Rows(0).Item("prdt_sra_nume") Is System.DBNull.Value, dsResultado.Tables(0).Rows(0).Item("prdt_sra_nume"), ""), _
                                         IIf(hdnFechaParametro.Text.Length > 0, hdnFechaParametro.Text, ""), _
                                         IIf(txtFechaNaci.Text.Length > 0, txtFechaNaci.Text, ""), _
                                         IIf(txtRPNac.Text.Length > 0, txtRPNac.Text, ""), _
                                         Me.cmbTipoRegistro.SelectedValue, _
                                         Me.cmbVariedad.SelectedValue, _
                                         IIf(Me.cmbPelaAPeli.Valor Is DBNull.Value, "", Me.cmbPelaAPeli.Valor))
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Eventos"

    Private Sub btnLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmbRaza.Limpiar()
        cmbAsociacion.Limpiar()
        txtNroExtranjero.Text = String.Empty
        txtRP.Text = String.Empty
        txtNombre.Text = String.Empty
        txtApodo.Text = String.Empty
        Me.cmbPelaAPeli.Limpiar()
        Me.txtFechaNaci.Text = ""
        Me.txtTipoRegistroCodi.Text = ""
        Me.txtVariedadCodi.Text = ""
        txtRPNac.Text = ""
        cmbTipoRegistroPadre.SelectedValue = ""
        cmbVariedadPadre.SelectedValue = ""
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try
            Guardar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub imgCloseServ_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCloseServ.Click
        Me.panServicio.Visible = False
        Me.panDato.Visible = True
    End Sub

    Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        Dim lsbMsg As New StringBuilder
        lsbMsg.Append("<SCRIPT language='javascript'>")
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)
    End Sub

#End Region

    Private Sub cmbRaza_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRaza.SelectedIndexChanged
        clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= " & sender.SelectedValue, cmbAsociacion, "id", "descrip", "S", False, True)
        Me.PnlcmbProdAsoc.UpdateAfterCallBack = True
        strRazaID = sender.SelectedValue

        Me.txtVariedadCodi.Text = ""
        clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = " & sender.SelectedValue, cmbVariedad, "id", "descrip", "S", False, True)

        Me.txtTipoRegistroCodi.Text = ""
        clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =" & sender.SelectedValue, cmbTipoRegistro, "id", "descrip", "S", False, True)

        Me.PnlTipoRegistro.UpdateAfterCallBack = True

        If (cmbRaza.Valor Is DBNull.Value Or cmbRaza.Valor.ToString() = "") Then
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id= 0,@pela_rgub_id=1", cmbPelaAPeli, "id", "descrip", "S")
        Else
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & cmbRaza.Valor & ",@pela_rgub_id=1", cmbPelaAPeli, "id", "descrip", "S")
        End If

        Me.pnlPelaje.UpdateAfterCallBack = True
    End Sub

    Private Sub txtTipoRegistroCodi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTipoRegistroCodi.TextChanged
        Dim idRegT As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_id" _
                             , " @regt_raza_id=" & cmbRaza.Valor & ",@regt_desc=" & Me.txtTipoRegistroCodi.Text)

        If (idRegT = "") Then
            cmbTipoRegistro.SelectedValue = idRegT
            Me.txtTipoRegistroCodi.Text = ""
        Else
            cmbTipoRegistro.SelectedValue = idRegT

        End If
        Me.PnlTipoRegistro.UpdateAfterCallBack = True
    End Sub

    Private Sub cmbTipoRegistro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoRegistro.SelectedIndexChanged
        If (Me.cmbTipoRegistro.SelectedValue <> "") Then
            Dim codiRegT As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_desc", " @regt_id=" & Me.cmbTipoRegistro.SelectedValue)

            If (codiRegT = "") Then
                Me.txtTipoRegistroCodi.Text = ""
            Else
                Me.txtTipoRegistroCodi.Text = codiRegT
            End If
            Me.PnlTipoRegistro.UpdateAfterCallBack = True
        End If
    End Sub
    Private Sub txtVariedadCodi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVariedadCodi.TextChanged
        Dim idVaridedad As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_variedades", "vari_id" _
                             , " @vari_raza_id=" & cmbRaza.Valor & ",@vari_codi=" & Me.txtVariedadCodi.Text)

        If (idVaridedad = "") Then
            cmbVariedad.SelectedValue = idVaridedad
            Me.txtVariedadCodi.Text = ""
        Else
            cmbVariedad.SelectedValue = idVaridedad

        End If
        Me.PnlTipoRegistro.UpdateAfterCallBack = True
    End Sub

    Private Sub cmbVariedad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVariedad.SelectedIndexChanged
        If (Me.cmbVariedad.SelectedValue <> "") Then
            Dim codiVaridedad As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_variedades", "vari_codi", " @vari_id=" & Me.cmbVariedad.SelectedValue)

            If (codiVaridedad = "") Then
                Me.txtVariedadCodi.Text = ""
            Else
                Me.txtVariedadCodi.Text = codiVaridedad
            End If
            Me.PnlTipoRegistro.UpdateAfterCallBack = True
        End If
    End Sub

    Private Sub btnNewServ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewServ.Click
        Me.panDato.Visible = False
        Me.panServicio.Visible = True
    End Sub

    Private Sub btnGuardarServ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarServ.Click
        Me.panServicio.Visible = False
        Me.panDato.Visible = True
    End Sub

    Private Sub btnLimpServ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpServ.Click

    End Sub

    Private Sub cmbSexo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSexo.SelectedIndexChanged
        If (cmbSexo.SelectedValue = "0" And Me.hdnTipoControl.Text = "prodImp") Then
            Me.btnNewServ.Visible = True
        Else
            Me.btnNewServ.Visible = False
        End If
    End Sub

    Private Sub bntAgrePadre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntAgrePadre.Click
        Me.panDatoPadre.Visible = True
        Me.panServicio.Visible = False
        Me.cmbSexoPadre.Enabled = False

        If (cmbRazaPadre.Valor Is DBNull.Value Or cmbRazaPadre.Valor.ToString() = "" Or strRazaIDPadre = "") Then
            SRA_Neg.Utiles.gSetearRaza(cmbRazaPadre)
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= 0", cmbAsociacionPadre, "id", "descrip", "S", False, True)

            Me.txtVariedadCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = 0", cmbVariedadPadre, "id", "descrip", "S", False, True)

            Me.txtTipoRegistroCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =0", cmbTipoRegistroPadre, "id", "descrip", "S", False, True)

            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id= 0,@pela_rgub_id=1", cmbPelaAPeliPadre, "id", "descrip", "S")
        Else
            cmbRazaPadre.Valor = strRazaIDPadre
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= " & cmbRazaPadre.Valor, cmbAsociacion, "id", "descrip", "S", False, True)

            Me.txtVariedadCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = " & cmbRaza.Valor, cmbVariedadPadre, "id", "descrip", "S", False, True)

            Me.txtTipoRegistroCodi.Text = ""
            clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =" & cmbRaza.Valor, cmbTipoRegistroPadre, "id", "descrip", "S", False, True)

            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & cmbRaza.Valor & ",@pela_rgub_id=1", cmbPelaAPeliPadre, "id", "descrip", "S")
        End If

        Me.PnlcmbProdAsocPadre.UpdateAfterCallBack = True
        Me.PnlTipoRegistroPadre.UpdateAfterCallBack = True
        Me.pnlPelajePadre.UpdateAfterCallBack = True
    End Sub

    Private Sub imgClosePadre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClosePadre.Click
        Me.panDatoPadre.Visible = False
        Me.panServicio.Visible = True
    End Sub

    Private Sub cmbRazaPadre_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRazaPadre.SelectedIndexChanged
        clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0, @asoc_raza_id= " & sender.SelectedValue, cmbAsociacionPadre, "id", "descrip", "S", False, True)
        Me.PnlcmbProdAsocPadre.UpdateAfterCallBack = True
        strRazaIDPadre = sender.SelectedValue

        Me.txtVariedadCodiPadre.Text = ""
        clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id = " & sender.SelectedValue, cmbVariedadPadre, "id", "descrip", "S", False, True)

        Me.txtTipoRegistroCodiPadre.Text = ""
        clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id =" & sender.SelectedValue, cmbTipoRegistroPadre, "id", "descrip", "S", False, True)

        Me.PnlTipoRegistroPadre.UpdateAfterCallBack = True

        If (cmbRazaPadre.Valor Is DBNull.Value Or cmbRazaPadre.Valor.ToString() = "") Then
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id= 0,@pela_rgub_id=1", cmbPelaAPeliPadre, "id", "descrip", "S")
        Else
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & cmbRazaPadre.Valor & ",@pela_rgub_id=1", cmbPelaAPeliPadre, "id", "descrip", "S")
        End If

        Me.pnlPelajePadre.UpdateAfterCallBack = True
    End Sub

    Private Sub txtTipoRegistroCodiPadre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTipoRegistroCodiPadre.TextChanged
        Dim idRegT As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_id" _
                             , " @regt_raza_id=" & cmbRazaPadre.Valor & ",@regt_desc=" & Me.txtTipoRegistroCodiPadre.Text)

        If (idRegT = "") Then
            cmbTipoRegistroPadre.SelectedValue = idRegT
            Me.txtTipoRegistroCodiPadre.Text = ""
        Else
            cmbTipoRegistroPadre.SelectedValue = idRegT

        End If
        Me.PnlTipoRegistroPadre.UpdateAfterCallBack = True
    End Sub

    Private Sub cmbTipoRegistroPadre_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoRegistroPadre.SelectedIndexChanged
        If (Me.cmbTipoRegistro.SelectedValue <> "") Then
            Dim codiRegT As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_desc", " @regt_id=" & Me.cmbTipoRegistroPadre.SelectedValue)

            If (codiRegT = "") Then
                Me.txtTipoRegistroCodiPadre.Text = ""
            Else
                Me.txtTipoRegistroCodiPadre.Text = codiRegT
            End If
            Me.PnlTipoRegistroPadre.UpdateAfterCallBack = True
        End If
    End Sub
    Private Sub txtVariedadCodiPadre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVariedadCodiPadre.TextChanged
        Dim idVaridedad As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_variedades", "vari_id" _
                             , " @vari_raza_id=" & cmbRazaPadre.Valor & ",@vari_codi=" & Me.txtVariedadCodiPadre.Text)

        If (idVaridedad = "") Then
            cmbVariedadPadre.SelectedValue = idVaridedad
            Me.txtVariedadCodiPadre.Text = ""
        Else
            cmbVariedadPadre.SelectedValue = idVaridedad

        End If
        Me.PnlTipoRegistroPadre.UpdateAfterCallBack = True
    End Sub

    Private Sub cmbVariedadPadre_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVariedadPadre.SelectedIndexChanged
        If (Me.cmbVariedad.SelectedValue <> "") Then
            Dim codiVaridedad As String = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_variedades", "vari_codi", " @vari_id=" & Me.cmbVariedadPadre.SelectedValue)

            If (codiVaridedad = "") Then
                Me.txtVariedadCodiPadre.Text = ""
            Else
                Me.txtVariedadCodiPadre.Text = codiVaridedad
            End If
            Me.PnlTipoRegistroPadre.UpdateAfterCallBack = True
        End If
    End Sub

    Private Sub btnGuardarPadre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarPadre.Click
        Try
            GuardarPadreServ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub GuardarPadreServ()
        Try
            If ValidarDatosPadreServ() Then
                Dim dsResultado As New DataSet
                'Obtengo el Producto ID.
                If hdnBitGenNroHBA.Text = "" Then
                    hdnBitGenNroHBA.Text = "False"
                End If

                dsResultado = Business.Productos.ProductosBusiness.GuardarProductoImportado( _
                                    cmbRazaPadre.Valor, cmbSexoPadre.SelectedValue _
                                    , cmbAsociacionPadre.Valor, txtNroExtranjeroPadre.Text _
                                    , IIf(txtRPPadre.Text.Length > 0, txtRPPadre.Text, String.Empty) _
                                    , IIf(txtNombrePadre.Text.Length > 0, txtNombrePadre.Text, String.Empty) _
                                    , IIf(txtApodoPadre.Text.Length > 0, txtApodoPadre.Text, String.Empty) _
                                    , hdnBitGenNroHBA.Text _
                                    , hdnNacionalidad.Text _
                                    , hdnFechaParametro.Text _
                                    , hdnTipoProducto.Text _
                                    , IIf(txtFechaNaciPadre.Text.Length > 0, txtFechaNaciPadre.Text, String.Empty) _
                                    , IIf(txtRPNacPadre.Text.Length > 0, txtRPNacPadre.Text, String.Empty) _
                                    , IIf(Me.cmbTipoRegistroPadre.SelectedValue = "", -1, Me.cmbTipoRegistroPadre.SelectedValue) _
                                    , IIf(Me.cmbVariedadPadre.SelectedValue = "", -1, Me.cmbVariedadPadre.SelectedValue) _
                                    , IIf(Me.cmbPelaAPeliPadre.SelectedValue = "", -1, Me.cmbPelaAPeliPadre.SelectedValue) _
                                    , -1 _
                                    , -1 _
                                    , System.DateTime.MinValue _
                                    , -1 _
                                    , Me.Session("sUserId"))

                If (dsResultado.Tables.Count > 0 And dsResultado.Tables(0).Rows.Count > 0) Then
                    'Pego los datos del producto obtenido en el control usrProductoExtranjero.
                    Me.usrProdppe.Valor = dsResultado.Tables(0).Rows(0).Item("prdt_id")
                    Me.panDatoPadre.Visible = False
                    Me.panServicio.Visible = True
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function ValidarDatosPadreServ() As Boolean
        If cmbRazaPadre.Valor = 0 Then
            Throw New Exception("Debe seleccionar una raza.")
            Return False
        End If

        If cmbSexoPadre.SelectedValue Is DBNull.Value Then
            Throw New Exception("Debe seleccionar el sexo.")
            Return False
        End If

        If cmbAsociacionPadre.Valor = 0 Then
            Throw New Exception("Debe seleccionar una asociaci�n.")
            Return False
        End If

        ' Dario 2014-06-30
        'If txtNroExtranjero.Text.Length = 0 Then
        '    Throw New Exception("Debe completar el n�mero de extranjero.")
        '    Return False
        'End If

        If txtNombrePadre.Text.Length = 0 Then
            Throw New Exception("Debe completar el nombre.")
            Return False
        End If

        Return True

    End Function

    Private Sub btnLimpPadre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpPadre.Click
        txtFechaNaciPadre.Text = ""
        cmbAsociacionPadre.Valor = ""
        txtNroExtranjeroPadre.Text = ""
        txtRPPadre.Text = ""
        txtRPNacPadre.Text = ""
        txtTipoRegistroCodiPadre.Text = ""
        cmbTipoRegistroPadre.SelectedValue = ""
        txtVariedadCodiPadre.Text = ""
        cmbVariedadPadre.SelectedValue = ""
        cmbPelaAPeliPadre.Valor = ""
        txtNombrePadre.Text = ""
        txtApodoPadre.Text = ""
    End Sub
End Class

End Namespace


