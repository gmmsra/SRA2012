Namespace SRA

Partial Class MovimCuotasCatego_Pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region



#Region "Definici�n de Variables"
   Private mstrTabla As String = "movim_cuotas_catego"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer

   Public Sub mInicializar()

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdConsulta.PageSize = Convert.ToInt32(mstrParaPageSize)
      lblCate.Text = Request("categoria")


   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            mConsultar()

         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Public Sub mConsultar()
      Try

         mstrCmd = "exec " + mstrTabla & "_consul "
         mstrCmd = mstrCmd + "@mcuc_cate_id = " & Request("mcuc_cate_id")

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdConsulta)


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

  
   Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

End Class

End Namespace
