<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Login_pop" CodeFile="Login_pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Ingreso de Usuario</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR>
					<td align="right"><font class="titulo">Usuario:</font></td>
					<td align="left">
						<cc1:textboxtab id="UserName" class="cuadrotexto" runat="server" autocomplete="off" Width="100px"></cc1:textboxtab>
					</td>
				</TR>
				<TR>
					<td align="right"><font class="titulo">Password:</font></td>
					<td align="left">
						<cc1:textboxtab id="UserPass" class="cuadrotexto" runat="server" Width="100px" TextMode="Password"
							autocomplete="off" EnterPorTab="False"></cc1:textboxtab></td>
				</TR>
				<TR>
					<TD vAlign="middle" align="right" height="40" colspan="2">
						<asp:button id="btnAceptar" runat="server" Text="Aceptar" Width="80px" cssclass="boton"></asp:button>&nbsp;
						<BUTTON class="boton" id="btnCancelar" style="WIDTH: 85px" language="javascript" onclick="window.close();"
							type="button" runat="server" value="Detalles">Cancelar</BUTTON>
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<script language="javascript">
			document.frmABM.UserName.focus();
		</script>
	</BODY>
</HTML>
