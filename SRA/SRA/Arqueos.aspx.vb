Namespace SRA

Partial Class Arqueos
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Arqueos
   Private mstrTablaDetalle As String = "detalle"

    Private mstrConn As String
    Private mstrParaPageSize As Integer
    Private mstrCentroEmisor As String
    Private mstrOrig As String
    Private mstrDest As String
    Private mstrNum As String
    Private mstrFecha As String

   Private Enum Columnas As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            mEstablecerPerfil()
         If (Not Page.IsPostBack) Then
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mSetearEventos()
				mCargarCombos()
				mstrCentroEmisor = Session("sCentroEmisorCentral")
				cmbEmctFil.Valor = hdnEmctId.Text
				If mstrCentroEmisor = "N" Then
					cmbEmctFil.Enabled = False
				Else
					cmbEmctFil.Enabled = True
				End If
				mConsultar()
				If Session("hdnId") <> Nothing Then
					mCargarDatos(Session("hdnId"))
					Session("hdnId") = Nothing
                End If
                txtFecha.Fecha = DateTime.Today
                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmctFil, "@ori_emct_id=" + hdnEmctId.Text)
      clsWeb.gCargarComboBool(cmbCierre, "")
      cmbCierre.ValorBool = False
   End Sub

   Private Sub mSetearEventos()
      Dim lstrMsg
        btnCerrar.Attributes.Add("onclick", "if(!confirm('Confirma el cierre?')) return false;")

      Dim lds As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "facturas_contado_pend_acuses", hdnEmctId.Text)
      For Each lDr As DataRow In lds.Tables(0).Rows
         If lstrMsg <> "" Then
            lstrMsg += ","
         End If
         lstrMsg += lDr.Item("numero").ToString
      Next

      If lstrMsg <> "" Then
         lstrMsg = "Existen facturas contado pendientes de cancelar por acuses de recibos. Confirma la generación del arqueo? (" + lstrMsg + ")"
         btnCierre.Attributes.Add("onclick", "if(!confirm('" + lstrMsg + "')) return false;")
      End If
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, ByVal pbooCerrada As Boolean)
        btnCerrar.Enabled = pbooAlta And Not pbooCerrada
        btnAbrir.Enabled = pbooCerrada And Not pbooAlta
        btnList.Enabled = True
        btnListCons.Enabled = True
        btnListRend.Enabled = pbooCerrada And Not pbooAlta And Not mAdministracionCentral(hdnEmctId.Text)
   End Sub

   Private Function mAdministracionCentral(ByVal pstrCemiId As String)
      Dim lbooCemiAdm As Boolean = False
      Try
         If pstrCemiId = "" Then
            lbooCemiAdm = False
         Else
            lbooCemiAdm = clsSQLServer.gCampoValorConsul(mstrConn, "emisores_ctros_consul @emct_id=" & pstrCemiId, "emct_central")
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      Finally
         mAdministracionCentral = lbooCemiAdm
      End Try
   End Function

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)
      Dim lstrCemiId As String = ""
      Dim lbooCemiAdm As Boolean = False

      With ldsEsta.Tables(0).Rows(0)
         hdnId.Text = .Item("arqu_id").ToString()
         txtNume.Valor = .Item("arqu_nume")
         txtEmct.Text = .Item("_emct_desc")
         txtFecha.Fecha = .Item("arqu_fecha")
         btnCerrar.Enabled = .IsNull("arqu_cierre_fecha")
         btnAbrir.Enabled = Not .IsNull("arqu_cierre_fecha")
         'cmbCierre.ValorBool = Not .IsNull("arqu_cierre_fecha")
         'cmbCierre.Enabled = .IsNull("arqu_cierre_fecha")
         'btnCierre.Enabled = .IsNull("arqu_cierre_fecha")
         If Session("sCentroEmisorId") <> .Item("arqu_emct_id") Then
            cmbCierre.Enabled = False
            btnCierre.Enabled = False
         End If
         lstrCemiId = .Item("arqu_emct_id")
      End With

      btnListRend.Enabled = Not btnCerrar.Enabled And Not mAdministracionCentral(lstrCemiId)

      grdDetalle.DataSource = ldsEsta.Tables(mstrTablaDetalle)
      grdDetalle.DataBind()

      lblTitu.Text = "Registro Seleccionado: " + txtEmct.Text + " - " + txtFecha.Text

        'mSetearEditor("", False, cmbCierre.ValorBool)
        mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
        mAlta()
        mLimpiar()
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      txtNume.Text = ""
      With clsSQLServer.gObtenerEstruc(mstrConn, "emisores_ctros", hdnEmctId.Text).Tables(0).Rows(0)
         txtEmct.Text = .Item("emct_codi").ToString + "-" + .Item("emct_desc")
      End With

            txtFecha.Fecha = Today
            cmbCierre.ValorBool = False
      btnCierre.Enabled = True
      cmbCierre.Enabled = True

      grdDetalle.DataSource = Nothing
      grdDetalle.DataBind()

      mSetearEditor("", True, False)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrId As String
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            'Elimino los arqueos sin fecha de cierre segun el centro emisor asociado
            lobjGenerico.Baja(ldsEstruc.Tables(0).Rows(0).Item("arqu_emct_id"))
            'Genero un nuevo arqueo
            lstrId = lobjGenerico.Alta()

         mCargarDatos(lstrId)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi(ByVal pbooCierre As Boolean)
      Try
            Dim ldsEstruc As DataSet = mGuardarDatos(pbooCierre)

            'Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla + IIf(pbooCierre, "_cierre", ""), ldsEstruc)
            'gsz Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla + "_cierre", ldsEstruc)

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc, "arqueos_cierre_modi")

            lobjGenerico.ModiSP()

         If pbooCierre Then
            mConsultar()
         Else
            mCargarDatos(hdnId.Text)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Function mGuardarDatos(Optional ByVal pbooCierre As Boolean = False) As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = CrearDataSet(hdnId.Text)

        With ldsEsta.Tables(0).Rows(0)
            .Item("arqu_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                If txtNume.Valor.Length > 0 Then
                    .Item("arqu_nume") = txtNume.Valor
                End If
                .Item("arqu_fecha") = txtFecha.Fecha
                .Item("arqu_emct_id") = hdnEmctId.Text
                If pbooCierre Then
                    .Item("arqu_cierre_fecha") = Today
                Else
                    .Item("arqu_cierre_fecha") = DBNull.Value
                End If
                ''If cmbCierre.ValorBool Then
                '  .Item("arqu_cierre_fecha") = Today
                'Else

                ' End If
            End With
        Return ldsEsta
    End Function

    Private Function CrearDataSet(ByVal pstrId As String) As DataSet
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        ldsEsta.Tables(0).TableName = mstrTabla

        If ldsEsta.Tables.Count > 1 Then
            ldsEsta.Tables(1).TableName = mstrTablaDetalle
        End If

        Return ldsEsta
    End Function


    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @arqu_emct_id =" + cmbEmctFil.Valor.ToString)
         lstrCmd.Append(",@fecha=" & clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
         lstrCmd.Append(",@solo_cerrados=1")

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
            Dim lstrCierreFecha As String
            lstrCierreFecha = clsSQLServer.gObtenerValorCampo(mstrConn, mstrTabla, "@arqu_id=" & e.Item.Cells(1).Text, "arqu_cierre_fecha").ToString
            If lstrCierreFecha = String.Empty Then
                mAlta()
            Else
                mCargarDatos(e.Item.Cells(1).Text)
            End If

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos"
    Private Sub btnAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mAlta()
    End Sub

    Private Sub btnCierre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCierre.Click
        mModi(True)
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            mImprimir("Arqueos")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnListCons_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListCons.Click
        Try
            mImprimir("Consolidado")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mImprimir(ByVal pstrReporte)
        'If Not cmbCierre.ValorBool Then
        '    mModi(False)
        'End If

        Dim lstrRptName As String = pstrReporte
        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        Session("hdnId") = hdnId.Text

        lstrRpt += "&arqu_id=" + hdnId.Text
        lstrRpt += "&random=" + Now.Millisecond.ToString

        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)

    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        Try
            'mAgregar()
            mAlta()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Try
            mModi(True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAbrir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAbrir.Click
        Try
            mModi(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnListRend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListRend.Click
        Try
            mImprimirRendicion()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mTraerDatos(ByVal pstrRendId As String)
        Dim ds As DataSet
        Dim lstrCmd As String = "rendiciones_rpt_consul @rend_id=" & pstrRendId & ",@recep=0"
        ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

        With ds.Tables(0).Rows(0)
            mstrOrig = .Item("orig_emct_desc").ToString
            mstrDest = .Item("dest_emct_desc").ToString
            mstrNum = .Item("rend_nume").ToString
            mstrFecha = .Item("rend_fecha").ToString
        End With
    End Sub

    Private Sub mImprimirRendicion()

        Dim lstrRptName As String = "Rendicion"
        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        Session("hdnId") = hdnId.Text

        Dim lstrRendId As String
        lstrRendId = clsSQLServer.gCampoValorConsul(mstrConn, "arqueos_rendicion_consul " + hdnId.Text, "rend_id")

        If lstrRendId = "" Then
            Throw New AccesoBD.clsErrNeg("No se ha generado rendición para el arqueo seleccionado.")
            Return
        End If

        mTraerDatos(lstrRendId)

        lstrRpt += "&rend_id=" + lstrRendId
        lstrRpt += "&recep=0"
        lstrRpt += "&orig=" + "'" + mstrOrig + "'"
        lstrRpt += "&dest=" + mstrDest
        lstrRpt += "&num=" + mstrNum
        lstrRpt += "&fecha=" + mstrFecha
        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)

    End Sub
    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        btnAbrir.Visible = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Arqueos_Abrir, String), (mstrConn), (Session("sUserId").ToString()))

        btnCerrar.Visible = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Arqueos_Cerrar, String), (mstrConn), (Session("sUserId").ToString()))

    End Sub
End Class

End Namespace
