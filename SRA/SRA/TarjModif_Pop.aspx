<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TarjModif_Pop" CodeFile="TarjModif_Pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Datos de la tarjeta</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="Salida();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD>
									<P></P>
								</TD>
								<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:label></TD>
								<TD vAlign="top" align="right">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="images\Close.bmp"></asp:imagebutton></TD>
							</TR>
							<TR>
								<TD>
									<P></P>
								</TD>
								<TD colspan="3" height="5"><asp:label id="lblTitu2" runat="server" cssclass="titulo" width="100%"></asp:label></TD>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderWidth="1px" BorderStyle="Solid">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD style="WIDTH: 20%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblDomi" runat="server" cssclass="titulo">Domicilio:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtDomi" runat="server" cssclass="textolibredeshab" Width="400px" TextMode="MultiLine"
															EnterPorTab="False" AceptaNull="true" Height="28px"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblSegu" runat="server" cssclass="titulo">C�d.Seguridad:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
														<cc1:numberbox id="txtSegu" runat="server" cssclass="cuadrotexto" Width="70px" TextMode="Password"
															AceptaNull="false" CantMax="4"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD onclick="Variable=1;" align="center" colSpan="3"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Ingresar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnlimpiar" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button>&nbsp;&nbsp;
													</TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		var Variable;
		Variable="";
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtSoliFecha"]!= null)
			document.all["txtSoliFecha"].focus();
			
		function Salida()
		{
			if(window.event.srcElement==null && Variable=="")
			{
				try
				{
					window.opener.document.all['hdnDatosPop'].value='1';
					window.opener.__doPostBack('hdnDatosPop','');
				}
				catch(e){;}
			}
		}
		</SCRIPT>
	</BODY>
</HTML>
