<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="controles/usrSociosFiltro.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InterRRGGExpo" CodeFile="InterRRGGExpo.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Intefaz RRGG</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
function expandir()
{
	try{ parent.frames("menu").CambiarExp();}catch(e){;}
}	
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Interfaz RRGG</asp:label></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3">
											<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnCons" runat="server" BorderStyle="None" ForeColor="Transparent" IncludesUrl="includes/"
																			ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																			ImageOver="btnCons2.gif" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ForeColor="Transparent" IncludesUrl="includes/"
																			ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																			ImageOver="btnLimp2.gif" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg"
																					colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%; HEIGHT: 14px" vAlign="top" background="imagenes/formfdofields.jpg"
																					colSpan="2">&nbsp;&nbsp;
																					<asp:label id="lblDesde" runat="server" cssclass="titulo">Fecha Desde: </asp:label>&nbsp;
																					<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="False"
																						Obligatorio="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																					<asp:label id="lblHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:label>&nbsp;
																					<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="False"
																						Obligatorio="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																					<cc1:combobox class="combo" id="cmbPend" runat="server" Width="136px" AceptaNull="False">
																						<asp:ListItem Value="1">Pendientes</asp:ListItem>
																						<asp:ListItem Value="0">Todos</asp:ListItem>
																					</cc1:combobox>&nbsp;
																				</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD vAlign="middle" align="left" colSpan="3" height="30">
											<asp:label id="lblPrev" runat="server" cssclass="titulo" Visible="False">Vista Previa con los primeros 30 registros</asp:label></TD>
									</TR>
									<TR>
										<TD vAlign="top" colspan="3" align="center">
											<asp:datagrid id="grdCons" runat="server" BorderStyle="None" BorderWidth="1px" CellSpacing="1"
												CellPadding="1" width="100%" AllowPaging="True" HorizontalAlign="Center" GridLines="None"
												OnPageIndexChanged="DataGrid_Page" PageSize="10" ItemStyle-Height="5px" AutoGenerateColumns="False">
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<FooterStyle CssClass="footer"></FooterStyle>
												<Columns>
													<asp:TemplateColumn Visible="false">
														<HeaderStyle Width="5%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																Height="5">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid>
										</TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD vAlign="middle" colspan="3" align="left"><A id="editar" name="editar"></A>
											<asp:panel id="panGrab" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												Width="50%" Visible="False" BorderColor="#003784">
												<TABLE id="Table5" cellSpacing="10" cellPadding="0" width="90%" align="center" border="0">
													<TR>
														<TD style="WIDTH: 164px" align="right" height="8"></TD>
														<TD height="8"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 164px" noWrap align="left">
															<asp:label id="lblArch" runat="server" cssclass="titulo">Archivo a generar:</asp:label>
															<cc1:TextBoxtab id="txtArch" runat="server" cssclass="cuadrotexto" Width="300px">novfac.dbf</cc1:TextBoxtab>
															<cc1:TextBoxtab id="txtArchDeta" runat="server" cssclass="cuadrotexto" Width="300px" Visible="False">novfacd.dbf</cc1:TextBoxtab></TD>
														<TD vAlign="bottom" align="center">
															<DIV id="divgraba" style="DISPLAY: inline">
																<asp:panel id="pangraba" runat="server" cssclass="titulo" width="100%">
																	<CC1:BotonImagen id="btnGrab" runat="server" BorderStyle="None" ForeColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnGrab.gif"
																		ImageOver="btnGrab2.gif" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Grabar"
																		ImageDisable="btnGrab0.gif"></CC1:BotonImagen>
																</asp:panel></DIV>
															<DIV id="divproce" style="DISPLAY: none">
																<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
																	<asp:Image id="Image1" runat="server" ImageUrl="imagenes/btnProce.gif"></asp:Image>
																</asp:panel></DIV>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 164px" vAlign="top" noWrap align="left" height="30"></TD>
														<TD vAlign="bottom" height="6"></TD>
													</TR>
												</TABLE>
											</asp:panel>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnExpo" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnExpoDeta" runat="server" Width="140px"></ASP:TEXTBOX>
			</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TR></TBODY></TABLE></TR></TBODY></TABLE>
		<script language="javascript">
function mProcesar()
{
	if (document.all("txtFechaDesde").value == '')
	{
		alert('Debe seleccionar la fecha desde.');
		return(false);
	}

	if (document.all("txtFechaHasta").value == '')
	{
		alert('Debe seleccionar la fecha hasta.');
		return(false);
	}

	//if (document.all("txtArch").value == '' || document.all("txtArchDeta").value == '')
	//{
		//alert('Debe indicar los nombres de los archivos.');
		//return(false);
	//}
	
	var bRet;
	bRet=confirm('Confirma la generación del archivo indicado?');
	
	if(bRet)
	{	
		document.all("divgraba").style.display ="none";
		document.all("divproce").style.display ="inline";
	}
	return(bRet); 
}
	if (document.all["editar"]!= null && document.all("panGrab")!=null )
		document.location='#editar';
	if (document.all('lblMens') != null)
	{
		if (document.all('lblMens').value != '')
			alert(document.all('lblMens').value);
	}	
	if (document.all("hdnExpo").value!='')
	{
		//var winArch = window.open(document.all("hdnExpo").value);
		//var winArch = window.open(document.all("hdnExpo").value.replace('.dbf','.cdx'));
		//document.all("hdnExpo").value='';
		//var winArch = window.open(document.all("hdnExpoDeta").value);
		//var winArch = window.open(document.all("hdnExpoDeta").value.replace('.dbf','.cdx'));
		//document.all("hdnExpoDeta").value='';
	}
		</script>
		</FORM>
	</BODY>
</HTML>
