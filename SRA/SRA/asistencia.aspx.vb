Imports System.Data.SqlClient


Namespace SRA


Partial Class asistencia
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtRefeFil As NixorControls.TextBoxTab

   Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "comisiones_asiste"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mintInse As Integer
   Private mbooPermiGrabar As Boolean

   Private Enum Columnas As Integer
      ClieCoinId = 1
      ClieAlumId = 2
      ClieAlumLega = 3
      ClieAlumNomb = 4
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
			
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()
			mEstablecerPerfil()
			If (Not Page.IsPostBack) Then
				Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

				mCargarCombos()
			Else
				If hdnExec.Text <> "" Then
					mGrabar(hdnExec.Text)
					hdnExec.Text = ""
				End If
			End If

		Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "ciclos_cargar @cicl_inse_id=" & mintInse.ToString, cmbCiclFil, "id", "descrip", "S", True)

        clsWeb.gCargarCombo(mstrConn, "comisiones_cargar @cicl_id=" & cmbCiclFil.Valor, cmbComiFil, "id", "descrip", "S", True)
        If Not Request.Form("cmbComiFil") Is Nothing Then
            cmbComiFil.Valor = Request.Form("cmbComiFil")
      End If

      mCargarMeses()

      clsWeb.gCargarCombo(mstrConn, "alumnos_inscriptos_cargar @comi_id=" & cmbComiFil.Valor, cmbAlumFil, "id", "descrip", "S", True)
      If Not Request.Form("cmbAlumFil") Is Nothing Then
         cmbAlumFil.Valor = Request.Form("cmbAlumFil")
      End If
   End Sub

   Private Sub mCargarMeses()
      clsWeb.gCargarRefeCmb(mstrConn, "mesesXComision", cmbMesFil, "S", cmbComiFil.Valor)
   End Sub

   Private Sub mEstablecerPerfil()

      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If

      mbooPermiGrabar = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = (grdDato.Items.Count > 0 And mbooPermiGrabar)
      btnList.Visible = (grdDato.Items.Count > 0 And mbooPermiGrabar)

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
        btnModi.Attributes.Add("onclick", "javascript:return(mGrabar());")
        If Page.IsPostBack Then
         If Not Request.Form("cmbComiFil") Is Nothing Then
            cmbComiFil.Valor = Request.Form("cmbComiFil")
         End If
         If Not Request.Form("cmbAlumFil") Is Nothing Then
            cmbAlumFil.Valor = Request.Form("cmbAlumFil")
         End If
      End If
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

   Public Sub mGrabar(ByVal pstrCmd As String)
		Dim vstrTarjNombArch() As String
		Dim i As Integer

		Try
			vstrTarjNombArch = pstrCmd.Split(";")

			For i = 0 To (vstrTarjNombArch.Length - 1)
				mstrCmd = "exec " & mstrTabla & "_grabar "
				mstrCmd = mstrCmd & " " & clsSQLServer.gFormatArg(vstrTarjNombArch(i).ToString(), SqlDbType.VarChar)
				mstrCmd = mstrCmd & "," & Session("sUserId").ToString()
				clsSQLServer.gExecute(mstrConn, mstrCmd)
			Next

			mConsultar()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
			mConsultar()
		End Try

   End Sub

   Public Sub mConsultar()

      Try

         Dim ds As New DataSet
         Dim lintFila As Integer
         Dim lintColu As Integer
         Dim lintAsis As Integer

         panDato.Visible = False

         If cmbCiclFil.Valor = 0 Then
             Throw New AccesoBD.clsErrNeg("Debe indicar el ciclo.")
         End If
         If cmbComiFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la comisi�n.")
         End If
         If cmbMesFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el mes.")
         End If

         mstrCmd = "exec " & mstrTabla & "_consul "
         mstrCmd = mstrCmd & " @inse_id=" & mintInse
         mstrCmd = mstrCmd & ",@cicl_id=" & cmbCiclFil.Valor
         mstrCmd = mstrCmd & ",@comi_id=" & cmbComiFil.Valor
         mstrCmd = mstrCmd & ",@alum_id=" & cmbAlumFil.Valor
         mstrCmd = mstrCmd & ",@mes=" & cmbMesFil.Valor
         If txtFechFil.Text <> "" Then
            mstrCmd = mstrCmd & ",@fecha=" & clsSQLServer.gFormatArg(txtFechFil.Text, SqlDbType.SmallDateTime)
         End If

         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

         Dim i As Integer = grdDato.Columns.Count - 1
         While i > 0
            grdDato.Columns.Remove(grdDato.Columns(i))
            i -= 1
         End While

         lintFila = 0
         For Each dc As DataColumn In ds.Tables(0).Columns
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            dgCol.DataField = dc.ColumnName
            Select Case dc.ColumnName
					Case "orden"
						dgCol.HeaderText = ""
					Case "coin_id"
						dgCol.HeaderText = "coin_id"
					Case "alum_id"
						dgCol.HeaderText = "alum_id"
					Case "lega_nume"
						dgCol.HeaderText = "Leg."
					Case "nombre"
						dgCol.HeaderText = "Nombre y Apellido"
					Case Else
						dgCol.HeaderText = ds.Tables(1).Rows(lintFila).Item("fecha")
						lintFila = lintFila + 1
						dgCol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
						dgCol.ItemStyle.HorizontalAlign = HorizontalAlign.Center
				End Select
            If dc.Ordinal = Columnas.ClieAlumId - 1 Or dc.Ordinal = Columnas.ClieCoinId - 1 Then
               dgCol.Visible = False
            End If
            grdDato.Columns.Add(dgCol)
         Next
         grdDato.DataSource = ds
         grdDato.DataBind()
         ds.Dispose()

         lintAsis = 1
			For lintColu = Columnas.ClieAlumNomb + 2 To grdDato.Columns.Count - 1
				For lintFila = 0 To grdDato.Items.Count - 1
					grdDato.Items(lintFila).Cells(lintColu).Text = "<input name='chkAsis" & lintAsis.ToString & "' tag='" & grdDato.Items(lintFila).Cells(lintColu).Text & "' type=checkbox" & IIf(grdDato.Items(lintFila).Cells(lintColu).Text.Substring(0, 1) = "S", " checked", "") & ">"
					lintAsis = lintAsis + 1
				Next
			Next

			panDato.Visible = True
			grdDato.Visible = True
			btnModi.Visible = (grdDato.Items.Count > 0 And mbooPermiGrabar)
			btnList.Visible = (grdDato.Items.Count > 0)
			imgClose.Visible = True

		Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

#End Region

#Region "Seteo de Controles"

   Private Sub mLimpiarFiltros()

      hdnId.Text = ""
      cmbCiclFil.SelectedIndex = 0
      cmbComiFil.Items.Clear()
      cmbAlumFil.Items.Clear()
      cmbMesFil.Items.Clear()
      txtFechFil.Text = ""
      grdDato.Visible = False
      btnModi.Visible = False
		btnList.Visible = False
		panDato.Visible = False

   End Sub

#End Region

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "AsistenciaComisiones"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         Dim lintFecha As Integer

         If txtFechFil.Text = "" Then
            lintFecha = 0
         Else
            lintFecha = CType(clsFormatear.gFormatFechaString(txtFechFil.Text, "Int32"), Integer)
         End If

         lstrRpt += "&inse_id=" & mintInse
         lstrRpt += "&cicl_id=" & cmbCiclFil.Valor
         lstrRpt += "&comi_id=" & cmbComiFil.Valor
         lstrRpt += "&alum_id=" & cmbAlumFil.Valor
         lstrRpt += "&mes=" & cmbMesFil.Valor
         lstrRpt += "&fecha=" & lintFecha.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

	Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
		panDato.Visible = False
		grdDato.Visible = False
		btnModi.Visible = False
		btnList.Visible = False
		imgClose.Visible = False
	End Sub
End Class
End Namespace
