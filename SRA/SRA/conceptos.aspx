<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Conceptos" CodeFile="conceptos.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Conceptos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<body class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td style="WIDTH: 9px" width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD></TD>
									<TD width="100%" colSpan="2"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Conceptos</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3">
										<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderStyle="Solid"
											BorderWidth="0">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%; HEIGHT: 17px" align="right">
																				<asp:Label id="lblCodigoDesdeFil" runat="server" cssclass="titulo">C�digo Desde:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 85%">
																				<CC1:TEXTBOXTAB id="txtCodigoDesdeFil" runat="server" cssclass="cuadrotexto" Width="160px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblCodigoHastaFil" runat="server" cssclass="titulo">C�digo Hasta:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtCodigoHastaFil" runat="server" cssclass="cuadrotexto" Width="160px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblDescripcionFil" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtDescripcionFil" runat="server" cssclass="cuadrotexto" Width="350px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblActividadesFil" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbActividadesFil" runat="server" Width="328px" nomoper="actividades_cargar"
																					AceptaNull="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel>
									</TD>
								</TR>
								<TR>
									<TD colSpan="3"></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD vAlign="top" align="center" colSpan="3">
										<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
											OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
											<FooterStyle CssClass="footer"></FooterStyle>
											<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
											<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="conc_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="conc_codi" ReadOnly="True" HeaderText="C&#243;digo">
													<HeaderStyle Width="2%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="conc_desc" ReadOnly="True" HeaderText="Descripci�n">
													<HeaderStyle Width="60%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_utilrecib" HeaderText="Util.Recibo" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="_grav" HeaderText="Grav" FooterStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="_formula" ReadOnly="True" HeaderText="F&#243;rmula">
													<HeaderStyle Width="30%"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid>
									</TD>
								</TR>
								<TR>
									<TD vAlign="bottom" colSpan="3">
										<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
														ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
														IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Concepto"
														ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
												<TD></TD>
												<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
														ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
														BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar" ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<tr>
									<TD colSpan="3" height="10"></TD>
								</tr>
								<TR>
									<TD align="center" colSpan="3">
										<asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
											<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False" Font-Bold="True"> General</asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkActividades" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Actividades</asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
												</TR>
											</TABLE>
										</asp:panel>
									</TD>
								</TR>
								<TR>
									<TD vAlign="top" align="center" colSpan="3">
										<asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" width="100%" Height="116px">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD style="WIDTH: 100%" vAlign="top" align="right">
														<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%">
														<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 20%" align="right">
																		<asp:label id="lblCodi" runat="server" cssclass="titulo">C�digo:</asp:label></TD>
																	<TD style="WIDTH: 80%">
																		<CC1:TEXTBOXTAB id="txtCodi" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:label></TD>
																	<TD>
																		<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="328px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right"></TD>
																	<TD>
																		<asp:CheckBox id="chkUtilRecibo" Checked="false" Text="Utilizar en Recibos" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:label id="lblIVA" runat="server" cssclass="titulo">I.V.A.:</asp:label></TD>
																	<TD>
																		<asp:radiobutton id="rbtGrav" onclick=" mHabilitarCombo(1)" runat="server" cssclass="titulo" Checked="True"
																			Text="No gravado" GroupName="RadioGroup1"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		<asp:Label id="lblIIBBGrava" runat="server" cssclass="titulo">Percepci�n IIBB:</asp:Label>&nbsp;
																		<asp:CheckBox id="chkIIBBGrava" Checked="false" Text="Gravado" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD align="right">
																	<TD>
																		<asp:radiobutton id="rbtIva" onclick=" mHabilitarCombo(0)" runat="server" cssclass="titulo" Text="Seleccionar"
																			GroupName="RadioGroup1"></asp:radiobutton>&nbsp;
																		<cc1:combobox class="combo" id="cmbGravaTasa" runat="server" Width="160px" CampoVal="Tipo de Tasa"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:label id="lblForm" runat="server" cssclass="titulo">F�rmula:</asp:label></TD>
																	<TD vAlign="top">
																		<TABLE width="100%">
																			<TR>
																				<TD vAlign="top" width="40%">
																					<cc1:combobox class="combo" id="cmbForm" runat="server" Width="100%" Obligatorio="True" onchange=" mMostrarDescripFormula();"></cc1:combobox></TD>
																				<TD vAlign="top">
																					<cc1:textboxtab id="txtFormDesc" runat="server" cssclass="textolibre" Width="100%" ReadOnly="True"
																						TextMode="MultiLine" Rows="2" height="46px"></cc1:textboxtab></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 500px" align="right" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:label id="lblCuen" runat="server" cssclass="titulo">Cuenta:&nbsp;</asp:label></TD>
																	<TD>
																		<TABLE cellSpacing="0" cellPadding="0" border="0">
																			<TR>
																				<TD>
																					<cc1:combobox class="combo" id="cmbCuen" runat="server" cssclass="cuadrotexto" Width="240" Obligatorio="True"
																						filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False" TextMaxLength="7"></cc1:combobox></TD>
																				<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																						onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuen','Cuentas Contables','@CodiPrim=[1,2,4,5]');"
																						alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																				</TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:label id="lblCuenOpt" runat="server" cssclass="titulo">Cuenta Optativa:</asp:label></TD>
																	<TD align="left">
																		<TABLE width="100%">
																			<TR>
																				<TD width="60%">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCuenOpt" runat="server" cssclass="cuadrotexto" Width="240"
																									filtra="true" NomOper="centas_ctables_cargar" MostrarBotones="False" TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuenOpt','Cuentas Contables','@CodiPrim=[2132],@CodiPrim2=[1136]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<TD width="40%">
																					<asp:label id="Label1" runat="server" cssclass="titulo">(Exposiciones y Per�odos)</asp:label></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:panel id="panActividades" runat="server" cssclass="titulo" Width="100%" Visible="False">
															<TABLE id="TablaActividades" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD vAlign="top" align="center" colSpan="2">
																		<asp:datagrid id="grdActi" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																			OnEditCommand="mEditarDatosActi" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
																			CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%" PageSize="20">
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																			<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="2%"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="acco_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="acco_acti_id" ReadOnly="True" HeaderText="actividad"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_ActiDesc" ReadOnly="True" HeaderText="Actividad">
																					<HeaderStyle Width="100%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 25%; HEIGHT: 12px" align="right" width="270">
																		<asp:Label id="lblActividades" runat="server" cssclass="titulo" Width="56px">Actividad:</asp:Label></TD>
																	<TD style="HEIGHT: 12px">
																		<cc1:combobox class="combo" id="cmbActividades" runat="server" Width="328px" nomoper="actividades_cargar"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="center" colSpan="2">
																		<asp:Label id="lblBajaActi" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																</TR>
																<TR height="30">
																	<TD align="center" colSpan="2">
																		<asp:Button id="btnAltaActi" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																		<asp:Button id="btnBajaActi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Baja"></asp:Button>
																		<asp:Button id="btnModiActi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Modificar"></asp:Button>
																		<asp:Button id="btnLimpActi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Limpiar"></asp:Button></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel>
										<ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center" colSpan="2">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button>&nbsp;
													</TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										<DIV></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td style="WIDTH: 9px" width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="hdnId" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnActiId" runat="server"></asp:textbox>
				<asp:textbox id="hdnHabComboIVA" runat="server">1</asp:textbox>
				<asp:textbox id="lblMens" runat="server"></asp:textbox>
				<cc1:combobox class="combo" id="cmbFormAux" runat="server" Width="232px"></cc1:combobox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
			if (document.all["editar"]!= null)
				document.location='#editar';
			if (document.all["txtCodi"]!= null)
				document.all["txtCodi"].focus();
		
			function ValorFijoChange()
			{
				if(window.event.srcElement.value!="")
					document.all["cmbTari"].selectedIndex=2;
				else
					document.all["cmbTari"].selectedIndex=1;
			}
		
			function mHabilitarCombo(pDesa)
			{
				document.all("cmbGravaTasa").disabled = pDesa;
				if(document.all("cmbGravaTasa").disabled)
				{
					document.all("cmbGravaTasa").value="";
					document.all("hdnHabComboIVA").value="1";
				}
				else
					document.all("hdnHabComboIVA").value="0";
			}
	
			function mMostrarDescripFormula()
			{
				document.all["cmbFormAux"].value = document.all["cmbForm"].value;
				if(document.all["cmbFormAux"].selectedIndex!=-1)
				{
					if (document.all["cmbFormAux"].selectedIndex!=0)
					{
					document.all["txtFormDesc"].value = document.all["cmbFormAux"].item(document.all["cmbFormAux"].selectedIndex).text;
					}else
					{
					document.all["txtFormDesc"].value = '';
					}
				} 
			}
		</SCRIPT>
		</TR></TBODY></TABLE>
	</body>
</HTML>
