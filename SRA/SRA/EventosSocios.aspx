<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EventosSocios" CodeFile="EventosSocios.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Entrega de Entradas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">	
			function mConsultarCantidades()
			{			   
			
			  if (document.all("usrSocios:txtId")!=null)
			  {		  		   	
				document.all["hdnCocktailRRII"].value = '';
				
				if (document.all("usrSocios:txtId").value=='' || document.all('cmbEvento').options[document.all('cmbEvento').selectedIndex].value == '')
				{
					document.all["txtCantiUno"].value = '';
					document.all["txtCantiDos"].value = '';
					document.all["txtCantiTres"].value = '';
					document.all["txtSociCategoria"].value = '';
					document.all["txtSociEstado"].value = '';
					document.all["txtSocioDeuda"].value = '';
					document.all["txtFechaRetiroUno"].innerText = '';
                    document.all["hdnFechaRetiroUno"].value = '';
                    //document.all["txtFechaRetiroUno"].value = '';
					document.all["cmbLugarUno"].value = '';
					document.all["hdnLugarUno"].value = '';
                    document.all["hdnFechaRetiroDos"].value = '';
					//document.all["txtFechaRetiroDos"].innerText = '';
					document.all["cmbLugarDos"].value = '';
					document.all["hdnLugarDos"].value = '';
                    document.all["hdnFechaRetiroTres"].value = '';
					//document.all["txtFechaRetiroTres"].innerText = '';
					document.all["cmbLugarTres"].value = '';
					document.all["hdnLugarTres"].value = '';
					document.all["txtObse"].value = '';
					document.all["txtNroDde1"].value = '';
					document.all["txtNroHta1"].value = '';
					document.all["txtNroDde2"].value = '';
					document.all["txtNroHta2"].value = '';
					document.all["txtNroDde3"].value = '';
					document.all["txtNroHta3"].value = '';
                    document.all["lblCargo"].innerText = "";
				}	
				else
				{
					var sFiltro = document.all("usrSocios:txtId").value+';'+document.all('cmbEvento').options[document.all('cmbEvento').selectedIndex].value;
					var vsRet;
					//Carga los datos correspondientes al Socio y al evento del Socio.
					vsRet = EjecutarMetodoXML("Utiles.DatosEventoCate", sFiltro).split(";");
					debugger;
					document.all["txtCantiUno"].value = vsRet[0];
					document.all["txtCantiDos"].value = vsRet[1];
					document.all["txtCantiTres"].value = vsRet[10];
					document.all["txtSociCategoria"].value = vsRet[2];
					document.all["txtSociEstado"].value = vsRet[3];
					document.all["txtSocioDeuda"].value = vsRet[4];
					document.all["txtFechaRetiroUno"].innerText = vsRet[5];
                    document.all["hdnFechaRetiroUno"].value = vsRet[5];
					document.all["cmbLugarUno"].value = vsRet[6];
					document.all["hdnLugarUno"].value = vsRet[6];
                    document.all["hdnFechaRetiroDos"].innerText = vsRet[7];
                    document.all["txtFechaRetiroDos"].value = vsRet[7];
					document.all["cmbLugarDos"].value = vsRet[8];
					document.all["hdnLugarDos"].value = vsRet[8];
                    document.all["hdnFechaRetiroTres"].value = vsRet[11];
					document.all["txtFechaRetiroTres"].innerText = vsRet[11];
					document.all["cmbLugarTres"].value = vsRet[12];
					document.all["hdnLugarTres"].value = vsRet[12];
					document.all["txtObse"].value = vsRet[9];
					document.all["txtNroDde1"].value = vsRet[13];
					document.all["txtNroHta1"].value = vsRet[14];
					document.all["txtNroDde2"].value = vsRet[15];
					document.all["txtNroHta2"].value = vsRet[16];
					document.all["txtNroDde3"].value = vsRet[17];
					document.all["txtNroHta3"].value = vsRet[18];
					document.all["hdnId"].value = vsRet[19];

                    document.all["lblCargo"].innerText = vsRet[20];
					//Si el valor del Lugar 1 es cero, selecciono "(No Retira)".
					if (vsRet[6] == 0)
					{
						document.all('cmbLugarUno').selectedIndex = 0;
					}
					
					//Si el valor del Lugar 2 es cero, selecciono "(No Retira)".
					if (vsRet[8] == 0)
					{
						document.all('cmbLugarDos').selectedIndex = 0;
					}
					
					//Si el valor del Lugar 3 es cero, selecciono "(No Retira)".
					if (vsRet[12] == 0)
					{
						document.all('cmbLugarTres').selectedIndex = 0;
					}
					
					//Verifico si la Cantidad 1 es cero.
					if (vsRet[0] == 0)
					{
						//Deshabilito el combo.
						document.all["cmbLugarUno"].disabled = true;
					}
					else
					{
						//Habilito el combo.
						document.all["cmbLugarUno"].disabled = false;
					}
					
					//Verifico si la Cantidad 2 es cero.
					if (vsRet[1] == 0)
					{
						//Deshabilito el combo.
						document.all["cmbLugarDos"].disabled = true;
					}
					else
					{
						//Habilito el combo.
						document.all["cmbLugarDos"].disabled = false;
					}
					
					//Verifico si la Cantidad 3 es cero.
					if (vsRet[10] == 0)
					{
						//Deshabilito el combo.
						document.all["cmbLugarTres"].disabled = true;
					}
					else
					{
						//Habilito el combo.
						document.all["cmbLugarTres"].disabled = false;
					}
				  }
                  //__doPostBack('', '');
			  }
			}
			function usrSocios_onchange()
			{
				mCargarCombos_OnChange();		
				mSelecEvento(document.all["cmbEvento"].value);	
				mConsultarCantidades();
			}
			function mSelecEvento(pEvenId)
		    {	
				var sFiltro = pEvenId;
				var vsRet;
				vsRet = EjecutarMetodoXML("Utiles.DatosEvento", sFiltro).split(";");
				
				//Verifico si seleccion� alg�n evento.
				if (vsRet[0] != '')
				{
					document.all("txtEnt1").value = vsRet[4];
					document.all("txtEnt2").value = vsRet[5];
					document.all("txtEnt3").value = vsRet[8];
				}
				else
				{
					document.all("txtEnt1").value = '';
					document.all("txtEnt2").value = '';
					document.all("txtEnt3").value = '';
				}
				
				mConsultarCantidades();
				
				if (vsRet[2]=='S')
				{
					document.all["lblConNume1"].innerText = "Controla numeraci�n";
					document.all["hdnConNume1"].value = "Controla numeraci�n";
					if (document.all["txtCantiUno"].value > 0)
						document.all["txtNroDde1"].disabled = false;
					if (document.all["txtCantiUno"].value > 1)
						document.all["txtNroHta1"].disabled = false;
				}
				else
				{
					document.all["lblConNume1"].innerText = "";
					document.all["hdnConNume1"].value = "";
					document.all["txtNroDde1"].value = "";
					document.all["txtNroHta1"].value = "";
					document.all["txtNroDde1"].disabled = true;
					document.all["txtNroHta1"].disabled = true;
				}
				
				if (vsRet[3]=='S')
				{
					document.all["lblConNume2"].innerText = "Controla numeraci�n";
					document.all["hdnConNume2"].value = "Controla numeraci�n";
					if (document.all["txtCantiDos"].value > 0)
						document.all["txtNroDde2"].disabled = false;
					if (document.all["txtCantiDos"].value > 1)
						document.all["txtNroHta2"].disabled = false;
				}
				else
				{
					document.all["lblConNume2"].innerText = "";
					document.all["hdnConNume2"].value = "";
					document.all["txtNroDde2"].value = "";
					document.all["txtNroHta2"].value = "";
					document.all["txtNroDde2"].disabled = true;
					document.all["txtNroHta2"].disabled = true;
				}
				
				if (vsRet[7]=='S')
				{
					document.all["lblConNume3"].innerText = "Controla numeraci�n";
					document.all["hdnConNume3"].value = "Controla numeraci�n";
					if (document.all["txtCantiTres"].value > 0)
						document.all["txtNroDde3"].disabled = false;
					if (document.all["txtCantiTres"].value > 1)
						document.all["txtNroHta3"].disabled = false;
				}
				else
				{
					document.all["lblConNume3"].innerText = "";
					document.all["hdnConNume3"].value = "";
					document.all["txtNroDde3"].value = "";
					document.all["txtNroHta3"].value = "";
					document.all["txtNroDde3"].disabled = true;
					document.all["txtNroHta3"].disabled = true;
				}
				
				if (document.all("txtEnt1").value == '')
				{
					document.all["txtCantiUno"].value = '';
				}

			    if (document.all("txtEnt2").value == '')
				{
					document.all["txtCantiDos"].value = '';
				}
					
				if (document.all("txtEnt3").value == '')
				{
					document.all["txtCantiTres"].value = '';
				}				
				
				mCargarCombos_OnChange();
		   }
		   
		   function mCargarCombos_OnChange()
		   {
				if (document.all["cmbEvento"].value != '')
				{
					var sFiltro = '@even_id=' + document.all["cmbEvento"].value; //+ ', @evlu_emct_id=' + document.all["hdnEmctId"].value;
					LoadComboXML("eventos_lugares_por_evento_cargar", sFiltro, "cmbLugarUno", "Z");
					LoadComboXML("eventos_lugares_por_evento_cargar", sFiltro, "cmbLugarDos", "Z");
					LoadComboXML("eventos_lugares_por_evento_cargar", sFiltro, "cmbLugarTres", "Z");
				}
		   }
		      
		   function cmbLugarUno_OnChange()
		   {	
				//Verifico si hay algo seleccionado.
				if (document.all["cmbLugarUno"].value == "")
					{
						//Limpio la fecha.
						document.all["txtFechaRetiroUno"].innerText = '';
						document.all["hdnFechaRetiroUno"].value = '';
						//Limpio el valor.
						document.all["hdnLugarUno"].value = '';
					}
				else
					{
						//Muesto la fecha.
						document.all["txtFechaRetiroUno"].innerText = getDate();
						document.all["hdnFechaRetiroUno"].value = getDate();
						//Paso valor para tomar desde codebehind.
					document.all["hdnLugarUno"].value = document.all["cmbLugarUno"].value;
                    
					}
		   }

		   function cmbLugarDos_OnChange()
		   {	
				//Verifico si hay algo seleccionado.
				if (document.all["cmbLugarDos"].value == "")
					{
						//Limpio la fecha.
						document.all["txtFechaRetiroDos"].innerText = '';
						document.all["hdnFechaRetiroDos"].value = '';
						//Limpio el valor.
						document.all["hdnLugarDos"].value = '';
					}
				else
					{
						//Muesto la fecha.
						document.all["txtFechaRetiroDos"].innerText = getDate();
						document.all["hdnFechaRetiroDos"].value = getDate();
						//Paso valor para tomar desde codebehind.
						document.all["hdnLugarDos"].value = document.all["cmbLugarDos"].value;
					}
		   }
		   
		   function cmbLugarTres_OnChange()
		   {	
				//Verifico si hay algo seleccionado.
				if (document.all["cmbLugarTres"].value == "")
					{
						//Limpio la fecha.
						document.all["txtFechaRetiroTres"].innerText = '';
						document.all["hdnFechaRetiroTres"].value = '';
						//Limpio el valor.
						document.all["hdnLugarTres"].value = '';
					}
				else
					{
						//Muesto la fecha.
						document.all["txtFechaRetiroTres"].innerText = getDate();
						document.all["hdnFechaRetiroTres"].value = getDate();
						//Paso valor para tomar desde codebehind.
						document.all["hdnLugarTres"].value = document.all["cmbLugarTres"].value;
					}
		   }
		   
		   function btnDeuda_click()
		   {
				if (document.all("usrSocios:txtId").value != "")
				{
					gAbrirVentanas("SociDeudaAplica_Pop.aspx?soci_id=" + document.all("usrSocios:txtId").value + "&Origen=Socios", 14, "","500","50");				
				}
		   }
		   
		   function btnCocktailRRII_Click()
		   {
				if (document.all("usrSocios:txtId").value != "")
				{
					var apellido = document.all("usrSocios:txtApel").value;
					var sApellido = apellido.toString();
					if (sApellido.indexOf(",") != -1) {sApellido = sApellido.substring(0, sApellido.indexOf(","));}					
					gAbrirVentanas("EventosSociosCocktailRRII_pop.aspx?soci_id=" + document.all("usrSocios:txtId").value + "&soci_apel=" + sApellido, 14, "700","500","50");
				}
		   }
		   
		   function btnList_Click()
		   {
			   gAbrirVentanas("EventosSociosReporte_pop.aspx?", 14, "800", "600", "50");
               //gAbrirVentanas("EventosSociosReporte_pop.aspx?", 14, "800", "600", "", "", false);
			  //gAbrirVentanas(pstrPagina                      , pintOrden, pstrAncho, pstrAlto, pstrLeft, pstrTop, pbooMax)
		   }
		   
		   function getDate()
		   {
				var currentTime = new Date()
				var day = currentTime.getDate()
				var month = currentTime.getMonth() + 1
				var year = currentTime.getFullYear()
				var sDay = day.toString();
				var sMonth = month.toString();

				if (sDay.length == 1)
					{
						day = '0' + day;
					}
				
				if (sMonth.length == 1)
					{
						month = '0' + month;
					}
					
				var currentDate = day + "/" + month + "/" + year;
				
				return currentDate;
		   }
		   		   
        </script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Entrega de Entradas</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="False" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:label id="lblSociFil" runat="server" cssclass="titulo">Socio:</asp:label>&nbsp;</TD>
																				<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrSociFil" runat="server" Obligatorio="true" AceptaNull="false" FilCUIT="True"
																						Tabla="Socios" Saltos="1,1,1" FilSociNume="True" PermiModi="False" MuestraDesc="False"
																						FilDocuNume="True" width="100%"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblEvenFil" runat="server" cssclass="titulo">Evento:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbEvenFil" class="combo" runat="server" Width="460px" AceptaNull="false"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<!---fin filtro --->
									<TR id="trCati" height="25" runat="server">
										<TD vAlign="top" colSpan="3" align="center"><asp:datagrid id="grdDato" runat="server" Visible="False" BorderWidth="1px" BorderStyle="None"
												width="100%" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" OnEditCommand="mEditarDatos"
												OnPageIndexChanged="DataGrid_Page" AllowPaging="True">
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
												<FooterStyle CssClass="footer"></FooterStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="soev_id" ReadOnly="True" HeaderText="Id"></asp:BoundColumn>
													<asp:BoundColumn DataField="_socio" HeaderText="Socio">
														<HeaderStyle Width="30%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_evento" HeaderText="Evento">
														<HeaderStyle Width="30%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="soev_soci_id" HeaderText="idSocio"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="soev_even_id" HeaderText="idEvento"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD vAlign="middle" colSpan="3">
											<TABLE style="WIDTH: 100%; HEIGHT: 100%" border="0" cellPadding="0" align="left">
												<TR>
													<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
															BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
															BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ToolTip="Agregar una Nueva Relaci�n"></CC1:BOTONIMAGEN></TD>
													<TD></TD>
													<TD onclick="btnList_Click();" width="50" align="center"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
															BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
															ImageOver="btnImpr2.gif"></CC1:BOTONIMAGEN></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD colSpan="3" align="center">
											<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
													BorderStyle="Solid" width="100%" Height="50px">
													<P align="right">
														<TABLE style="WIDTH: 100%; HEIGHT: 50px" id="Table2" class="FdoFld" border="0" cellPadding="0"
															align="left">
															<TR>
																<TD>
																	<P></P>
																</TD>
																<TD style="WIDTH: 501px" height="5">
																	<asp:label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:label></TD>
																<TD vAlign="top" align="right">&nbsp;
																	<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%" height="30" colSpan="3" align="center">
																	<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																		<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblEvento" runat="server" cssclass="titulo">Evento:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 23px" height="23" colSpan="2">
																					<cc1:combobox id="cmbEvento" class="combo" runat="server" Width="90%" AceptaNull="false" onchange="mSelecEvento(this.value);"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:label id="lblSocio" runat="server" cssclass="titulo">Socio:</asp:label>&nbsp;</TD>
																				<TD style="HEIGHT: 23px" height="23" colSpan="2">
																					<UC1:CLIE id="usrSocios" runat="server" Obligatorio="true" AceptaNull="false" FilCUIT="True"
																						Tabla="Socios" Saltos="1,1,1,1" FilSociNume="True" PermiModi="False" MuestraDesc="False"
																						FilDocuNume="True" width="100%" FilClieNume="True"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblSociCatego" runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 24px" height="23" colSpan="2">
																					<CC1:TEXTBOXTAB id="txtSociCategoria" runat="server" cssclass="cuadrotextodeshab" Width="120px"
																						ReadOnly="True"></CC1:TEXTBOXTAB>&nbsp;
																					<asp:Label id="lblSociEsta" runat="server" cssclass="titulo">Estado:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtSociEstado" runat="server" cssclass="cuadrotextodeshab" Width="180px" ReadOnly="True"></CC1:TEXTBOXTAB>&nbsp;
																					<asp:Label ID="lblCargo" runat="server" Text="" Font-Size="Larger" Font-Bold="true" ForeColor="Red"></asp:Label>
																				</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblDeuda" cssclass="titulo" Runat="server">Deuda:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 25px" noWrap>
																					<cc1:TEXTBOXTAB id="txtSocioDeuda" runat="server" cssclass="cuadrotextodeshab" Width="500px" ReadOnly="True"></cc1:TEXTBOXTAB>&nbsp;<BUTTON style="WIDTH: 110px" id="btnDeuda" class="boton" onclick="btnDeuda_click();" runat="server"
																						value="Deuda">C�lculo Cobranza</BUTTON>&nbsp;</TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblEnt1" runat="server" cssclass="titulo">Entrada:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 23px" height="23" colSpan="2">
																					<asp:textbox id="txtEnt1" runat="server" cssclass="titulo" BorderStyle="None" BackColor="Transparent"
																						ReadOnly="True" Font-Bold="True"></asp:textbox>&nbsp;</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblNroDde1" runat="server" cssclass="titulo">Nro. desde:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 4px" noWrap>
																					<CC1:NUMBERBOX id="txtNroDde1" runat="server" cssclass="cuadrotexto" Width="75px"></CC1:NUMBERBOX>&nbsp;
																					<asp:Label id="lblNroHta1" runat="server" cssclass="titulo">Nro. hasta:</asp:Label>
																					<CC1:NUMBERBOX id="txtNroHta1" runat="server" cssclass="cuadrotexto" Width="75px"></CC1:NUMBERBOX>
																					<asp:Label id="lblConNume1" runat="server" cssclass="titulo" Width="160px"></asp:Label>
																					<asp:Label id="lblCant1" runat="server" cssclass="titulo">Cantidad:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtCantiUno" runat="server" cssclass="cuadrotextodeshab" Width="40px" ReadOnly="True"
																						Font-Bold="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblLugarUno" cssclass="titulo" Runat="server">Lugar:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 4px" noWrap>
																					<CC1:combobox id="cmbLugarUno" runat="server" cssclass="combo" Width="240px" onchange="cmbLugarUno_OnChange();"></CC1:combobox>&nbsp;
																					<asp:Label id="lblFechaRetUno" runat="server" cssclass="titulo">Retir�:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtFechaRetiroUno" runat="server" cssclass="cuadrotextodeshab" Width="80px"
																						ReadOnly="True" MaxLength="10"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblEnt2" runat="server" cssclass="titulo">Entrada:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 4px">
																					<asp:textbox id="txtEnt2" runat="server" cssclass="titulo" BorderStyle="None" BackColor="Transparent"
																						ReadOnly="True" Font-Bold="True"></asp:textbox>&nbsp;</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblNroDde2" runat="server" cssclass="titulo">Nro. desde:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 4px">
																					<CC1:NUMBERBOX id="txtNroDde2" runat="server" cssclass="cuadrotexto" Width="75px"></CC1:NUMBERBOX>&nbsp;
																					<asp:Label id="lblNroHta2" runat="server" cssclass="titulo">Nro. hasta:</asp:Label>
																					<CC1:NUMBERBOX id="txtNroHta2" runat="server" cssclass="cuadrotexto" Width="75px"></CC1:NUMBERBOX>
																					<asp:Label id="lblConNume2" runat="server" cssclass="titulo" Width="160px"></asp:Label>
																					<asp:Label id="lblCant2" runat="server" cssclass="titulo">Cantidad:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtCantiDos" runat="server" cssclass="cuadrotextodeshab" Width="40px" ReadOnly="True"
																						Font-Bold="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblLugarDos" cssclass="titulo" Runat="server">Lugar:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 4px" noWrap>
																					<CC1:combobox id="cmbLugarDos" runat="server" cssclass="combo" Width="240px" onchange="cmbLugarDos_OnChange();"></CC1:combobox>&nbsp;
																					<asp:Label id="lblFechaRetDos" runat="server" cssclass="titulo">Retir�:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtFechaRetiroDos" runat="server" cssclass="cuadrotextodeshab" Width="80px"
																						ReadOnly="True" MaxLength="10"></CC1:TEXTBOXTAB>&nbsp;<BUTTON style="WIDTH: 110px" id="btnCocktailRRII" class="boton" onclick="btnCocktailRRII_Click();"
																						runat="server" value="Deuda" disabled="disabled" enableviewstate="True">Cocktail RRII</BUTTON>&nbsp;
																					<asp:textbox id="hdnCocktailRRII" runat="server" BorderStyle="None" ForeColor="Red" BackColor="Transparent"
																						ReadOnly="True" Font-Bold="True" AutoPostBack="True"></asp:textbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblEnt3" runat="server" cssclass="titulo">Entrada:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 4px">
																					<asp:textbox id="txtEnt3" runat="server" cssclass="titulo" BorderStyle="None" BackColor="Transparent"
																						ReadOnly="True" Font-Bold="True"></asp:textbox>&nbsp;</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblNroDde3" runat="server" cssclass="titulo">Nro. desde:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 4px">
																					<CC1:NUMBERBOX id="txtNroDde3" runat="server" cssclass="cuadrotexto" Width="75px"></CC1:NUMBERBOX>&nbsp;
																					<asp:Label id="lblNroHta3" runat="server" cssclass="titulo">Nro. hasta:</asp:Label>
																					<CC1:NUMBERBOX id="txtNroHta3" runat="server" cssclass="cuadrotexto" Width="75px"></CC1:NUMBERBOX>
																					<asp:Label id="lblConNume3" runat="server" cssclass="titulo" Width="160px"></asp:Label>
																					<asp:Label id="lblCant3" runat="server" cssclass="titulo">Cantidad:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtCantiTres" runat="server" cssclass="cuadrotextodeshab" Width="40px" ReadOnly="True"
																						Font-Bold="True"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
																					<asp:Label id="lblLugarTres" cssclass="titulo" Runat="server">Lugar:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 4px" noWrap>
																					<CC1:combobox id="cmbLugarTres" runat="server" cssclass="combo" Width="240px" onchange="cmbLugarTres_OnChange();"></CC1:combobox>&nbsp;
																					<asp:Label id="lblFechaRetTres" runat="server" cssclass="titulo">Retir�:</asp:Label>
																					<CC1:TEXTBOXTAB id="txtFechaRetiroTres" runat="server" cssclass="cuadrotextodeshab" Width="80px"
																						ReadOnly="True" MaxLength="10"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 160px; HEIGHT: 24px" vAlign="top" align="right">
																					<asp:Label id="lblObse" runat="server" cssclass="titulo" Width="101px">Observaciones:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 24px">
																					<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="textolibredeshab" Width="360px" Obligatorio="True"
																						AceptaNull="False" Height="54px" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																		</TABLE>
																	</asp:panel>
																	<asp:CheckBox id="chkImpri" Visible="False" Runat="server" Text="Imprimir Tal�n" Checked="True"
																		CssClass="titulo"></asp:CheckBox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%" colSpan="3" align="center">
																	<asp:label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:label></TD>
															</TR>
															<TR height="30">
																<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
																	<asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;&nbsp;
																	<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Baja"></asp:button>&nbsp;&nbsp;
																	<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Grabar"></asp:button>&nbsp;&nbsp;
																	<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar"></asp:button></TD>
															</TR>
														</TABLE>
													</P>
												</asp:panel></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="hdnId" runat="server"></ASP:TEXTBOX>
				<cc1:combobox id="cmbEvenAux" class="combo" runat="server" Width="100%" AceptaNull="false"></cc1:combobox>
				<ASP:TEXTBOX id="hdnEmctId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnLugarUno" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnLugarDos" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnLugarTres" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnConNume1" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnConNume2" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnConNume3" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnFechaRetiroUno" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnFechaRetiroDos" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnFechaRetiroTres" runat="server"></ASP:TEXTBOX>
				 
			</DIV>
		</form>
		<SCRIPT language="javascript">	
				
		if (document.all["txtEntreFecha"]!= null)
			document.all["txtEntreFecha"].focus();
		if (document.all["lblConNume1"]!=null)
		{		
			if (document.all["lblConNume1"].innerText == '')
			{
				document.all["txtNroDde1"].value = "";
				document.all["txtNroHta1"].value = "";
				document.all["txtNroDde1"].disabled = true;
				document.all["txtNroHta1"].disabled = true;
			}
			else
			{
				document.all["txtNroDde1"].disabled = false;
				document.all["txtNroHta1"].disabled = false;
			}
			if (document.all["lblConNume2"].innerText == '')
			{
				document.all["txtNroDde2"].value = "";
				document.all["txtNroHta2"].value = "";
				document.all["txtNroDde2"].disabled = true;
				document.all["txtNroHta2"].disabled = true;
			}
			else
			{
				document.all["txtNroDde2"].disabled = false;
				document.all["txtNroHta2"].disabled = false;
			}
			if (document.all["lblConNume3"].innerText == '')
			{
				document.all["txtNroDde3"].value = "";
				document.all["txtNroHta3"].value = "";
				document.all["txtNroDde3"].disabled = true;
				document.all["txtNroHta3"].disabled = true;
			}
			else
			{
				document.all["txtNroDde3"].disabled = false;
				document.all["txtNroHta3"].disabled = false;
			}
			if (document.all("txtEnt1").value == '')
			{
				document.all["txtCantiUno"].value = '';
			}
			if (document.all("txtEnt2").value == '')
			{
				document.all["txtCantiDos"].value = '';
			}
			if (document.all("txtEnt3").value == '')
			{
				document.all["txtCantiTres"].value = '';
			}
			
			if (document.all["hdnCocktailRRII"].value != "")
			{	
				mCargarCombos_OnChange();
							
				if (document.all["hdnLugarUno"].value != "")
				{
					document.all["cmbLugarUno"].value = document.all["hdnLugarUno"].value;
				}
				else
				{
					document.all["cmbLugarUno"].value = "";
				}
				
				
				if (document.all["hdnCocktailRRII"].value == 'RRII - No entreg�')
				{
					document.all["cmbLugarDos"].value = document.all["hdnLugarDos"].value;
					document.all["cmbLugarDos"].disabled = false;
				}
				else
				{
					document.all["hdnLugarDos"].value = "";
					document.all["cmbLugarDos"].disabled = true;
				}
			}
		}

		</SCRIPT>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
