<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.BajaVentas" CodeFile="BajaVentas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Denuncia de Baja</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		
		
		function usrProp_cmbRazaCria_onchange(pRaza)
		{
		
		
	      if (document.all('txtusrProp:cmbRazaCria').value!=null)
		  {		
		
		       
		  		  	  
		        document.all('usrCria:cmbRazaCria').value =document.all(pRaza).value;
				document.all('usrCria:cmbRazaCria').onchange();
		  	  		
		  		document.all('usrProductoDenBaja:usrCriadorFil:cmbRazaCria').value =document.all(pRaza).value;
				document.all('usrProductoDenBaja:usrCriadorFil:cmbRazaCria').onchange();
						
				document.all("usrProductoDenBaja:usrCriadorFil:cmbRazaCria").disabled = true;
				document.all("txtusrProductoDenBaja:usrCriadorFil:cmbRazaCria").disabled = true;
				
		   }
	}
	
	
		function usrProp_onchange(pRaza)
		{
		
		
		if (document.all('txtusrProp:cmbRazaCria').value!=null)
		  {		
		  		document.all('usrProductoDenBaja:usrCriadorFil:cmbRazaCria').value =document.all(pRaza).value;
				document.all('usrProductoDenBaja:usrCriadorFil:cmbRazaCria').onchange();
	
				
				document.all("usrProductoDenBaja:usrCriadorFil:cmbRazaCria").disabled = true;
				document.all("txtusrProductoDenBaja:usrCriadorFil:cmbRazaCria").disabled = true;
				
		   }
	}
	
		
		
		
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		function mVerErrores()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Errores&tabla=baja_denun_errores&filtros=" + document.all("hdnDetaId").value, 1, "600","300","70","100");
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Denuncias de Baja</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="99%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="2" background="imagenes/formiz.jpg" colSpan="3"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg" height="6"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg" height="6"></TD>
																			<TD style="WIDTH: 88%" background="imagenes/formfdofields.jpg" height="6"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblPropFil" runat="server" cssclass="titulo">Propietario:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrPropFil" runat="server" MostrarBotones="False" CampoVal="Criador" FilSociNume="True"
																					FilDocuNume="True" AceptaNull="false" AutoPostBack="True" Ancho="800" Tabla="Criadores"
																					Saltos="1,2" FilTipo="S" MuestraDesc="False"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:&nbsp;</asp:Label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox>&nbsp; &nbsp;
																				<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" height="20"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg" height="20"></TD>
																			<TD background="imagenes/formfdofields.jpg" height="20"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 18.15%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD height="10"></TD>
									<TD vAlign="top" align="right" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="left" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="99%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="true" DataField="bjde_id" ReadOnly="True" HeaderText="Nro. Denuncia"></asp:BoundColumn>
												<asp:BoundColumn DataField="bjde_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="_propietario" HeaderText="Propietario"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="_bjdd_motivo_baja_id"></asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Motivo de Baja">
													<ItemTemplate>
														<div><%#MuestraMotivoBaja(DataBinder.Eval(Container, "DataItem._bjdd_motivo_baja_id"))%></div>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Denuncia" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Height="21px" Width="80px" CausesValidation="False"> Productos </asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												width="99%" Height="100px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:datagrid id="grdDeta" runat="server" width="98%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdDeta_PageChanged"
																				AutoGenerateColumns="False" Visible="true" OnEditCommand="mEditarDatosDeta">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="bjdd_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_sexo" HeaderText="Sexo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="bjdd_rp" HeaderText="RP"></asp:BoundColumn>
																					<asp:BoundColumn DataField="bjdd_sra_nume" HeaderText="Nro."></asp:BoundColumn>
																					<asp:BoundColumn DataField="bjdd_denu_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="bjdd_nyap" HeaderText="Nombre" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="bjdd_docu" HeaderText="Documento"></asp:BoundColumn>
																					<asp:BoundColumn DataField="bjdd_dire" HeaderText="Dirección"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 167px" align="right" width="167">
																			<asp:Label id="lblMoti" runat="server" cssclass="titulo">Motivo deBaja:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbBaja" runat="server" AceptaNull="True" AutoPostBack="True"
																				Width="200px" Visible="True"></cc1:combobox>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblProducto" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<UC1:PROH id="usrProductoDenBaja" runat="server" AceptaNull="false" AutoPostBack="False" Ancho="800"
																				Tabla="productos" Saltos="1,2" FilTipo="S" MuestraDesc="True" EsPropietario="True"></UC1:PROH></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 167px" align="right" width="167">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:&nbsp;</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR id="rowVenta" style="DISPLAY: none" runat="server">
																		<TD colSpan="2">
																			<TABLE style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																				<TR>
																					<TD style="WIDTH: 167px" align="right" width="167">
																						<asp:Label id="lblNroInscripcion" runat="server" cssclass="titulo">Nro.Inscrip.:&nbsp;</asp:Label></TD>
																					<TD>
																						<cc1:numberbox id="txtNroInscripcion" runat="server" cssclass="cuadrotexto" Width="140px" esdecimal="False"
																							MaxValor="9999999999999"></cc1:numberbox></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 167px" align="right" width="167">
																						<asp:Label id="lblDocu" runat="server" cssclass="titulo">Nro.Documento:&nbsp;</asp:Label></TD>
																					<TD>
																						<CC1:TEXTBOXTAB id="txtDocu" runat="server" cssclass="cuadrotexto" Width="214px" EnterPorTab="False"
																							MaxLength="20"></CC1:TEXTBOXTAB></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 167px" align="right" width="167">
																						<asp:Label id="lblDire" runat="server" cssclass="titulo">Dirección:&nbsp;</asp:Label></TD>
																					<TD>
																						<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotexto" Width="400px" EnterPorTab="False"
																							MaxLength="20"></CC1:TEXTBOXTAB></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 167px" align="right" width="167">
																			<asp:Label id="lblObservacion" runat="server" cssclass="titulo">Observación:&nbsp;</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtObservacion" runat="server" cssclass="cuadrotexto" Width="688px" Height="46px"
																				EnterPorTab="False" MaxLength="20" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 167px" align="right" width="167">
																			<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD>
																			<asp:Label id="txtEsta" runat="server" cssclass="titulo"></asp:Label>
																			<asp:Button id="btnErr" runat="server" cssclass="boton" Width="60px" Text="Errores"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 167px" align="right" width="167" height="16"></TD>
																		<TD height="16"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="100px" Text="Agregar Prod."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Eliminar Prod."></asp:Button>&nbsp;
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Modificar Prod."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="100px" Text="Limpiar Prod."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Visible="False" Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnValorId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnModi" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnEspe" runat="server"></asp:textbox>
				<asp:textbox id="hdnItem" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaCarga" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriadorId" runat="server"></asp:textbox>
			</DIV>
			<DIV style="DISPLAY: none"><asp:textbox id="hdnRaza" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Denuncia de Baja');
		</SCRIPT>
	</BODY>
</HTML>
