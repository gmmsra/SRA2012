<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociDocum" CodeFile="SociDocum.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Documentación</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0"><%-- onunload="mCerrar()">--%>
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD>
									<P></P>
								</TD>
								<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo">Documentación</asp:label></TD>
								<TD vAlign="top" align="right">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"></asp:imagebutton></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderWidth="1px" BorderStyle="Solid">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD vAlign="top" align="left" width="100%" colSpan="2" height="10"></TD>
												</TR>
												<TR>
													<TD vAlign="top" align="left" width="100%" colSpan="2">
														<TABLE id="Table8" cellSpacing="0" cellPadding="0" border="0">
															<TR>
																<TD></TD>
																<TD vAlign="top" align="right" width="50">
																	<asp:label id="lblFoto" runat="server" cssclass="titulo">Foto:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 251px" vAlign="top">
																	<CC1:TEXTBOXTAB id="txtFoto" runat="server" cssclass="cuadrotexto" ReadOnly="True" Width="300px"></CC1:TEXTBOXTAB><BR>
																	<INPUT id="filFoto" style="WIDTH: 300px" type="file" name="filFoto" runat="server"></TD>
																<TD vAlign="top"><IMG id="imgDelFoto" style="CURSOR: hand" onclick="javascript:mLimpiarFoto();" alt="Limpiar Foto"
																		src="imagenes\del.gif" runat="server">&nbsp;</TD>
																<TD align="center" width="90">
																	<asp:Image id="imgFoto" Width="80px" Runat="server" Height="80px" Border="1"></asp:Image></TD>
																<TD vAlign="top"><BUTTON class="boton" onclick="javascript:mVerImagen('Foto');" id="btnFotoVer" style="WIDTH: 80px" type="button" runat="server" value="Ver Foto">Ver 
																		Foto</BUTTON></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD vAlign="top" align="left" width="100%" colSpan="2" height="10"></TD>
												</TR>
												<TR>
													<TD vAlign="top" align="left" width="100%" colSpan="2">
														<TABLE id="Table9" cellSpacing="0" cellPadding="0" border="0">
															<TR>
																<TD vAlign="top" align="right" width="50">
																	<asp:label id="lblFirma" runat="server" cssclass="titulo">Firma:</asp:label>&nbsp;</TD>
																<TD vAlign="top">
																	<CC1:TEXTBOXTAB id="txtFirma" runat="server" cssclass="cuadrotexto" ReadOnly="True" Width="302px"></CC1:TEXTBOXTAB><BR>
																	<INPUT id="filFirma" style="WIDTH: 300px" type="file" name="filFirma" runat="server"></TD>
																<TD vAlign="top"><IMG id="imgDelFirm" style="CURSOR: hand" onclick="javascript:mLimpiarFirma();" alt="Limpiar Firma"
																		src="imagenes\del.gif" runat="server">&nbsp;
																</TD>
																<TD align="center" width="90">
																	<asp:Image id="imgFirma" Width="80px" Runat="server" Height="80px" Border="1"></asp:Image></TD>
																<TD vAlign="top"><BUTTON class="boton" id="btnFirmaVer" style="WIDTH: 80px" type="button" runat="server"
																		onclick="javascript:mVerImagen('Firma');" value="Ver Firma">Ver Firma</BUTTON></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD vAlign="middle" align="center" colSpan="3" height="40"><A id="editar" name="editar"></A>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
													</TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		function mSetearImagenes()
		{
			if (document.all('txtFoto').value != '')
			{
				document.all('imgFoto').src = 'fotos/'+document.all('txtFoto').value;
			}
			else
			{
				document.all('imgFoto').src = 'images/none.jpg';
				document.all('btnFotoVer').disabled = true;
			}	
			if (document.all('txtFirma').value != '')
			{
				document.all('imgFirma').src = 'fotos/'+document.all('txtFirma').value;
			}
			else
			{
				document.all('imgFirma').src = 'images/none.jpg';
				document.all('btnFirmaVer').disabled = true;
			}
		}
		function mLimpiarFoto()
		{
			document.all("txtFoto").value = '';
			mSetearImagenes();
		}		
		function mLimpiarFirma()
		{
			document.all("txtFirma").value = '';
			mSetearImagenes();
		}
		function mVerImagen(pTipo)
		{
			if (pTipo == 'Foto')			
				gAbrirVentanas('fotos/'+document.all('txtFoto').value);
			else
				gAbrirVentanas('fotos/'+document.all('txtFirma').value);
		}
					
		if (document.all('txtFoto')!=null)
			mSetearImagenes()
		</SCRIPT>
	</BODY>
</HTML>
