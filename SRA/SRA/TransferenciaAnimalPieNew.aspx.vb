﻿Imports ReglasValida.Validaciones
Imports SRA_Entidad
Imports Business
Imports Entities
Imports System.Collections.Generic
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.Serialization

Namespace SRA

    Partial Class TransferenciaAnimalPieNew
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'Protected WithEvents usrCriaFil As usrClieDeriv
        Protected WithEvents cmbRazaFil As NixorControls.ComboBox
        'Protected WithEvents usrCriaProp As usrClieDeriv
        Protected WithEvents rowDivProp As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
        '  Protected WithEvents cmbRazaFil As NixorControls.ComboBox
        Protected WithEvents cmbRazaCria As NixorControls.ComboBox
        Protected WithEvents lblSraNumeFil As System.Web.UI.WebControls.Label
        ' Protected WithEvents txtSraNumeFil As NixorControls.NumberBox
        Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label
        Protected WithEvents lblInicFecha As System.Web.UI.WebControls.Label
        Protected WithEvents lblCantEmbr As System.Web.UI.WebControls.Label
        Protected WithEvents txtCantEmbr As NixorControls.NumberBox
        Protected WithEvents cmbCriaComp As NixorControls.ComboBox
        Protected WithEvents hdnCriaComp As System.Web.UI.WebControls.TextBox
        Protected WithEvents lblPorcComp As System.Web.UI.WebControls.Label
        Protected WithEvents txtPorcComp As NixorControls.NumberBox
        Protected WithEvents lblObseComp As System.Web.UI.WebControls.Label
        Protected WithEvents txtObseComp As NixorControls.TextBoxTab
        Protected WithEvents btnModiComp As System.Web.UI.WebControls.Button
        Protected WithEvents btnLimpComp As System.Web.UI.WebControls.Button
        Protected WithEvents lblNombComp As System.Web.UI.WebControls.Label
        Protected WithEvents lblRecuFecha As System.Web.UI.WebControls.Label
        Protected WithEvents txtRecuFecha As NixorControls.DateBox
        Protected WithEvents btnTeDenu As System.Web.UI.WebControls.Button
        Protected WithEvents txtTeDesc As NixorControls.TextBoxTab
        Protected WithEvents cmbVari As NixorControls.ComboBox
        Protected WithEvents lblPelaAPeli As System.Web.UI.WebControls.Label
        Protected WithEvents cmbPelaAPeli As NixorControls.ComboBox
        Protected WithEvents lblPelaBPeli As System.Web.UI.WebControls.Label
        Protected WithEvents cmbPelaBPeli As NixorControls.ComboBox
        Protected WithEvents lblPelaCPeli As System.Web.UI.WebControls.Label
        Protected WithEvents cmbPelaCPeli As NixorControls.ComboBox
        Protected WithEvents lblPelaDPeli As System.Web.UI.WebControls.Label
        Protected WithEvents cmbPelaDPeli As NixorControls.ComboBox
        Protected WithEvents lblExp As System.Web.UI.WebControls.Label
        Protected WithEvents lblClieVend As System.Web.UI.WebControls.Label
        'Protected WithEvents grdComp As System.Web.UI.WebControls.DataGrid


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definición de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_Tramites
        Private mstrTablaRequisitos As String = SRA_Neg.Constantes.gTab_Tramites_Deta
        Private mstrTablaDocumentos As String = SRA_Neg.Constantes.gTab_Tramites_Docum
        Private mstrTablaObservaciones As String = SRA_Neg.Constantes.gTab_Tramites_Obse
        Private mstrTablaProductos As String = SRA_Neg.Constantes.gTab_Productos
        Private mstrTablaEspecies As String = SRA_Neg.Constantes.gTab_Especies
        Private mstrTablaTramitesProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
        Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis
        Private mstrTablaProductosNumeros As String = SRA_Neg.Constantes.gTab_ProductosNumeros
        Private mstrTablaProductosDocum As String = SRA_Neg.Constantes.gTab_ProductosDocum
        Private mstrTramiteId As String = ""

        Private mstrParaPageSize As Integer
        Private mdsDatosPadre As DataSet
        Private mdsDatosMadre As DataSet
        Private mdsDatosProd As DataSet
        Private dsVali As DataSet
        Private mstrCmd As String
        Public mintProce As Integer
        Private mdsDatos As DataSet
        Private mstrConn As String
        Private mstrTrapId As String
        Public mstrTitulo As String
        Public mintTtraId As Integer

        ' Objeto en reemplazo de mdsDatos.
        Private objDatos As TransferenciaAnimalEnPie_TramiteEntity
        Private objDatosProd As TransferenciaAnimalEnPie_ProductoEntity
        Private objTramitesVC As List(Of TramitesVendedoresCompradoresEntity)
        Public objVendedores As List(Of TramitesVendedoresCompradoresEntity)
        Private objCompradores As List(Of TramitesVendedoresCompradoresEntity)
#End Region

#Region "Operaciones sobre la Pagina"

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()
                If (Not Page.IsPostBack) Then

                    usrCriaVend_aux.cmbCriaRazaExt.Enabled = False
                    usrCriaComp_aux.cmbCriaRazaExt.Enabled = False

                    objVendedores = New List(Of TramitesVendedoresCompradoresEntity)
                    Session("objVendedores") = objVendedores

                    objCompradores = New List(Of TramitesVendedoresCompradoresEntity)
                    Session("objCompradores") = objCompradores

                    btnAgregarComprador.Enabled = True
                    btnModificarComprador.Enabled = False
                    btnEliminarComprador.Enabled = False
                    btnCancelarComprador.Enabled = False

                    btnAgregarVendedor.Enabled = True
                    btnModificarVendedor.Enabled = False
                    btnEliminarVendedor.Enabled = False
                    btnCancelarVendedor.Enabled = False

                    Me.chkReservaCrias.Disabled = True
                    Me.txtCantCriasReser.Enabled = False
                    Me.chkReservaEmbriones.Disabled = False
                    Me.txtCantReservaEmbriones.Enabled = True

                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "expandir", "<SCRIPT>expandir();</SCRIPT>")
                    clsWeb.gInicializarControles(Me, mstrConn)

                    Session("mitramite") = Nothing
                    Session("sessProductos") = Nothing
                    mstrTramiteId = Request.QueryString("Tram_Id")

                    If mstrTramiteId <> "" Then
                        hdnId.Text = mstrTramiteId
                        Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
                        'cmbEsta.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                        cmbEstadoFil.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                        'mEditarTramite(mstrTramiteId)
                        mMostrarPanel(True)
                        lnkRequ.Font.Bold = False
                        lnkDocu.Font.Bold = False
                        lnkObse.Font.Bold = False

                        lnkRequ.Enabled = False
                        lnkDocu.Enabled = False
                        lnkObse.Enabled = False

                        panRequ.Visible = False
                        panDocu.Visible = False
                        panObse.Visible = False
                    End If

                    If mstrTramiteId = "" Then
                        mSetearMaxLength()
                        mSetearEventos()
                        mCargarCombos()
                        mMostrarPanel(False)
                    End If
                Else
                    mstrTramiteId = Request.QueryString("Tram_Id")
                    If mstrTramiteId <> "" Then
                        Response.Write("<Script>window.close();</script>")
                    End If
                End If

                ' Persistencia de objetos.
                objDatos = Session("mitramite")
                objDatosProd = Session("sessProductos")
                mstrTrapId = Session("mstrTrapId")
                objVendedores = Session("objVendedores")
                objCompradores = Session("objCompradores")

                hdnGridVendedoresCount.Value = objVendedores.Count
                hdnGridCompradoresCount.Value = objCompradores.Count

                hdnFechaTransferencia.Value = txtFechaTransferencia_aux.Text

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mCargarCombos()
            Dim lstrSRA As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
            'clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Trámites) + ",@defa=1")
            clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Trámites) + ",@defa=1")
            clsWeb.gCargarCombo(mstrConn, "rg_requisitos_cargar", cmbRequRequ, "id", "descrip_codi", "S")
        End Sub

        Private Sub mSetearEventos()
            btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
            btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
            btnBaja.Attributes.Add("onclick", "if(!confirm('Está dando de baja el Trámite, el cual será cerrado.¿Desea continuar?')){return false;} else {mPorcPeti();}")
            'btnAgre.Attributes.Add("onclick", "return btnAgre_click();") Dario 2014-05-28
            ' btnTeDenu.Attributes.Add("onclick", "mCargarTE();return(false);")
            btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
            'btnCriaCopropiedadComp.Attributes.Add("onclick", "mCriaCopropiedadComp();return false;")

            'btnCriaCopropiedadVend.Attributes.Add("onclick", "mCriaCopropiedadVend();return false;")
            'Me.usrProd.cmbSexoProdExt.onchange = "return usrProd_cmbProdSexo_change();"

        End Sub

        Private Sub mSetearMaxLength()
            Dim lstrLong As Object
            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
            txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nume")
            'txtNroControl.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nro_control")


            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, SRA_Neg.Constantes.gTab_Productos)
            '  txtSraNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_sra_nume")

            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocumentos)
            txtDocuObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trdo_refe")

            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaObservaciones)
            txtObseObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trao_obse")

        End Sub

#End Region

#Region "Inicializacion de Variables"

        Public Sub mHabilitarDatosTE(ByVal pbooHabi As Boolean)




        End Sub

        Public Sub mInicializar()
            clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdRequ.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdDocu.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdObse.PageSize = Convert.ToInt32(mstrParaPageSize)

            mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos
            mintProce = ReglasValida.Validaciones.Procesos.TransferenciaProductos

            mstrTitulo = "Transferencia de Productos New"

            'usrProd.MostrarFechaNacimiento = True


            'rowProp.Style.Add("display", "none")

            mHabilitarDatosTE(False)
            btnAgre.AlternateText = "Nueva " & mstrTitulo

            lblTituAbm.Text = mstrTitulo

        End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

        Public Sub grdDato_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.EditItemIndex = -1
                If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = 0
                Else
                    grdDato.CurrentPageIndex = E.NewPageIndex
                End If
                If Page.IsPostBack Then

                    mConsultar(True)
                Else
                    mConsultar(False)

                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdRequ_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdRequ.EditItemIndex = -1
                If (grdRequ.CurrentPageIndex < 0 Or grdRequ.CurrentPageIndex >= grdRequ.PageCount) Then
                    grdRequ.CurrentPageIndex = 0
                Else
                    grdRequ.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarRequ()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdDocu_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDocu.EditItemIndex = -1
                If (grdDocu.CurrentPageIndex < 0 Or grdDocu.CurrentPageIndex >= grdDocu.PageCount) Then
                    grdDocu.CurrentPageIndex = 0
                Else
                    grdDocu.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarDocu()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdObse.EditItemIndex = -1
                If (grdObse.CurrentPageIndex < 0 Or grdObse.CurrentPageIndex >= grdObse.PageCount) Then
                    grdObse.CurrentPageIndex = 0
                Else
                    grdObse.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarObse()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mConsultar(ByVal boolMostrarTodos As Boolean)

            Try

                ' Parámetros para el sp =================
                Dim tram_id? As Integer = Nothing
                Dim tram_nume? As Integer = Nothing
                Dim tram_raza_id? As Integer = Nothing
                Dim sexo? As Integer = Nothing
                Dim sra_nume? As Integer = Nothing
                Dim cria_id? As Integer = Nothing
                Dim prod_id? As Integer = Nothing
                Dim prdt_nombre As String = Nothing
                Dim tram_fecha_desde? As Date = Nothing
                Dim tram_fecha_hasta? As Date = Nothing
                Dim tram_oper_fecha? As Date = Nothing
                Dim tram_ttra_id? As Integer = Nothing
                Dim tram_esta_id? As Integer = Nothing
                Dim tram_pais_id? As Integer = Nothing
                Dim mostrar_vistos? As Integer = Nothing
                Dim prod_nacionalidad As String = Nothing
                Dim cria_vend_id? As Integer = Nothing

                grdDato.Visible = True
                limpiarGridDato()
                grdDato.DataSource = Nothing
                grdDato.DataBind()
                ' =======================================

                tram_ttra_id = mintTtraId

                If Not boolMostrarTodos Then
                    If txtNumeFil.Valor <> 0 Then
                        tram_nume = clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int)
                    End If
                    If hdnId.Text <> "" Then
                        tram_id = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    End If
                End If

                If Not usrProdFil Is Nothing Then
                    If usrProdFil.RazaId.ToString() <> "" Then
                        tram_raza_id = clsSQLServer.gFormatArg(usrProdFil.RazaId, SqlDbType.Int)
                    End If
                    If usrProdFil.Valor <> 0 Then
                        prod_id = clsSQLServer.gFormatArg(usrProdFil.Valor, SqlDbType.Int)
                    End If
                    If (usrProdFil.cmbSexoProdExt.SelectedValue.ToString.Length > 0) Then
                        sexo = usrProdFil.cmbSexoProdExt.SelectedValue.ToString
                    End If
                End If

                prdt_nombre = IIf(usrProdFil.txtProdNombExt.Valor.ToString().Trim() = String.Empty, Nothing, "'" + usrProdFil.txtProdNombExt.Valor + "'")

                If (usrProdFil.txtSraNumeExt.Valor.ToString().Length > 0) Then
                    sra_nume = Convert.ToInt32(usrProdFil.txtSraNumeExt.Valor)
                End If

                If (txtFechaDesdeFil.Fecha.Length > 0) Then
                    tram_fecha_desde = clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
                End If

                If (txtFechaHastaFil.Fecha.Length > 0) Then
                    tram_fecha_hasta = clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)
                End If

                If (txtFechaTransferenciaFil.Fecha.Length > 0) Then
                    tram_oper_fecha = clsFormatear.gFormatFecha2DB(txtFechaTransferenciaFil.Fecha)
                End If

                tram_esta_id = Convert.ToInt32(cmbEstadoFil.Valor)

                mostrar_vistos = IIf(chkVisto.Checked, 1, 0)

                If Not usrCompradorFil Is Nothing Then
                    If usrCompradorFil.Valor.ToString() <> "0" And usrCompradorFil.Valor.ToString() <> "" Then
                        cria_id = clsSQLServer.gFormatArg(usrCompradorFil.Valor.ToString(), SqlDbType.Int)
                    End If
                End If

                ' Obtiene los datos para el grid.
                Dim obj = New List(Of TransferenciaAnimalEnPie_GridEntity)
                obj = TransferenciaAnimalEnPieBusiness.Tramites_animalEnPie_Busq(tram_id, tram_nume, tram_raza_id, sexo, sra_nume, cria_id, prod_id, prdt_nombre, tram_fecha_desde, tram_fecha_hasta, tram_oper_fecha, tram_ttra_id, tram_esta_id, tram_pais_id, mostrar_vistos, prod_nacionalidad, cria_vend_id)

                ' Carga el grid con los datos obtenidos.
                grdDato.Visible = True
                If obj.Count > 0 Then
                    grdDato.DataSource = obj
                    grdDato.DataBind()
                Else
                    limpiarGridDato()
                End If

                mMostrarPanel(False)

            Catch ex As Exception
                grdDato.Visible = True
                grdDato.DataSource = Nothing
                grdDato.DataBind()
                clsError.gManejarError(Me, ex)
            End Try

        End Sub

        Private Sub mConsultarRequ()
            grdRequ.DataSource = objDatos.TramitesDeta  ' mdsDatos.Tables(mstrTablaRequisitos)
            grdRequ.DataBind()
        End Sub

        Private Sub mConsultarDocu()
            grdDocu.DataSource = objDatos.TramitesDocum  '' mdsDatos.Tables(mstrTablaDocumentos)
            grdDocu.DataBind()
        End Sub

        Private Sub mConsultarObse()
            ''mdsDatos.Tables(mstrTablaObservaciones).DefaultView.Sort = "trao_fecha desc,_requ_desc"
            objDatos.TramitesObse.Sort(Function(x, y) x.trao_fecha.CompareTo(y.trao_fecha) And x._requ_desc.CompareTo(y._requ_desc))
            grdObse.DataSource = objDatos.TramitesObse '' mdsDatos.Tables(mstrTablaObservaciones)
            grdObse.DataBind()
        End Sub

        Private Function mCrearDataSet(ByVal pstrId As String, ByVal pSpEstructura As String, _
                        ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                        ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


            Dim ldsDatosTransf As New DataSet
            Dim tblDatos As New DataTable(pTableName)

            ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
            tblDatos = ldsDatosTransf.Tables(0)
            tblDatos.TableName = pTableName

            dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
            Session("miTramite") = dsDatosNewDataSet

            Return True
        End Function

        Public Sub mCrearDataSet(ByVal pstrId As String)
            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrTablaRequisitos
            mdsDatos.Tables(2).TableName = mstrTablaDocumentos
            mdsDatos.Tables(3).TableName = mstrTablaObservaciones
            mdsDatos.Tables(4).TableName = mstrTablaTramitesProductos

            grdRequ.CurrentPageIndex = 0
            grdDato.CurrentPageIndex = 0
            grdObse.CurrentPageIndex = 0

            mConsultarRequ()
            mConsultarDocu()
            mConsultarObse()

            Session("mitramite") = mdsDatos
        End Sub

        Public Sub mCrearDataSetProd(ByVal pstrId As String)

            mdsDatosProd = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaProductos, pstrId)

            mdsDatosProd.Tables(0).TableName = mstrTablaProductos
            mdsDatosProd.Tables(1).TableName = mstrTablaAnalisis
            mdsDatosProd.Tables(2).TableName = mstrTablaProductosNumeros
            mdsDatosProd.Tables(3).TableName = mstrTablaProductosDocum

            Session("sessProductos") = mdsDatosProd
        End Sub

        Private Function mCrearDataSetProd(ByVal pstrId As String, ByVal pSpEstructura As String, _
                          ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                          ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


            Dim ldsDatosTransf As New DataSet
            Dim tblDatos As New DataTable(pTableName)

            ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
            tblDatos = ldsDatosTransf.Tables(0)
            tblDatos.TableName = pTableName

            dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
            Session("sessProductos") = dsDatosNewDataSet

            Return True
        End Function

#End Region

#Region "Seteo de Controles"

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            hdnOperacion.Value = "M"
            mEditarTramite_New(E.Item.Cells(1).Text)
        End Sub

        Private Sub mCargarCriadores(ByVal pobjCombo As NixorControls.ComboBox, _
        ByVal pstrClieId As String, ByVal pstrRaza As String)

            If pobjCombo.Visible Then
                mstrCmd = "criadores_cliente_cargar "
                If pstrClieId <> "" Then
                    mstrCmd = mstrCmd & " @clie_id=" & pstrClieId
                Else
                    mstrCmd = mstrCmd & " @clie_id=0"
                End If
                If pstrRaza <> "" Then
                    mstrCmd = mstrCmd & ",@raza_id=" & pstrRaza
                End If
                clsWeb.gCargarCombo(mstrConn, mstrCmd, pobjCombo, "id", "descrip", "S")
            End If
        End Sub

        Public Sub mEditarTramite_New(ByVal pstrTram As String)
            Try
                mLimpiar_New()

                Dim strProdId As String = String.Empty

                hdnId.Text = clsFormatear.gFormatCadena(pstrTram)

                Dim tr As New TramitesReservasEntity
                tr = TramitesReservasBusiness.GetTramiteReservas(Convert.ToInt32(hdnId.Text))
                hdnIdTramiteReserva.Value = tr.IdTramiteReserva
                If (tr.IdTramiteReserva > 0) Then
                    GetTramiteReserva(tr)
                End If

                objDatos = New TransferenciaAnimalEnPie_TramiteEntity
                objDatos = TransferenciaAnimalEnPieBusiness.Tramites_Consul(Convert.ToInt32(hdnId.Text), Nothing, Nothing)

                Dim oProducto As New ProductosEntity()
                oProducto = TransferenciaAnimalEnPieBusiness.GetProductoByTramiteId(Convert.ToInt32(hdnId.Text))

                If oProducto.prdt_id <> 0 Then
                    strProdId = oProducto.prdt_id
                End If

                If strProdId = String.Empty Then
                    Throw New AccesoBD.clsErrNeg("Validación: El Tramite no esta vinculado a ningun producto .")
                End If

                objDatosProd = New TransferenciaAnimalEnPie_ProductoEntity
                objDatosProd = TransferenciaAnimalEnPieBusiness.Productos_Consul(Convert.ToInt32(strProdId))

                Session("mitramite") = objDatos
                Session("sessProductos") = objDatosProd

                hdnTramNro.Text = objDatos.Tramites.First().tram_nume.ToString()
                hdnRazaId.Text = objDatos.Tramites.First().tram_raza_id.ToString()
                lblTitu_aux.Text = "Trámite Nro: " & objDatos.Tramites.First().tram_nume.ToString() 'hdnTramNro.Text
                txtInicFecha_aux.Fecha = objDatos.Tramites.First().tram_inic_fecha.ToString()
                txtFinaFecha_aux.Fecha = objDatos.Tramites.First().tram_fina_fecha.ToString()
                txtFechaTram_aux.Fecha = objDatos.Tramites.First().tram_pres_fecha.ToString()
                txtFechaTransferencia_aux.Fecha = objDatos.Tramites.First().tram_oper_fecha.ToString()
                txtEstado.Text = objDatos.Tramites.First().esta_abre.ToString()
                hdnEstadoId.Value = Convert.ToInt32(objDatos.Tramites.First().tram_esta_id)
                txtNroControl_aux.Text = ValidarNulos(objDatos.Tramites.First().tram_nro_control.ToString(), False)
                usrProd_aux.Valor = hdnId.Text

                objTramitesVC = New List(Of TramitesVendedoresCompradoresEntity)
                objTramitesVC = TramitesVendedoresCompradoresBusiness.ConsTramitesCompradoresVendedores(Convert.ToInt32(hdnId.Text))
                objVendedores = New List(Of TramitesVendedoresCompradoresEntity)
                objCompradores = New List(Of TramitesVendedoresCompradoresEntity)

                If (objTramitesVC.Count > 0) Then

                    If objTramitesVC.Where(Function(x) x.tram_vc_tran_tipo = "V").Count > 0 Then
                        objVendedores = objTramitesVC.Where(Function(x) x.tram_vc_tran_tipo = "V").OrderBy(Function(o) o.clie_fanta).ToList()
                        gridVendedores.AutoGenerateColumns = False
                        gridVendedores.DataSource = objVendedores
                        gridVendedores.DataBind()
                        Dim porcVentas As Integer
                        porcVentas = objVendedores.Sum(Function(x) x.tram_vc_porcentaje)
                    End If

                    Session("objVendedores") = objVendedores

                    If objTramitesVC.Where(Function(x) x.tram_vc_tran_tipo = "C").Count > 0 Then
                        objCompradores = objTramitesVC.Where(Function(x) x.tram_vc_tran_tipo = "C").OrderBy(Function(o) o.clie_fanta).ToList()
                        gridCompradores.AutoGenerateColumns = False
                        gridCompradores.DataSource = objCompradores
                        gridCompradores.DataBind()
                        Dim porCompras As Integer
                        porCompras = objVendedores.Sum(Function(x) x.tram_vc_porcentaje)
                    End If

                    Session("objCompradores") = objCompradores
                End If

                If oProducto.prdt_a_pela_id <> 0 Then
                    usrProd_aux.Valor = ValidarNulos(oProducto.prdt_id.ToString(), False)
                End If

                If Not IsDBNull(objDatos.Tramites.First().tram_baja_fecha) And Not IsNothing(objDatos.Tramites.First().tram_baja_fecha) Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(objDatos.Tramites.First().tram_baja_fecha).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = String.Empty
                End If

                strProdId = String.Empty
                If objDatos.TramitesProductos.Count = 1 Then
                    hdnDetaId.Text = objDatos.TramitesProductos.First().trpr_id
                    strProdId = ValidarNulos(objDatos.TramitesProductos.First().trpr_prdt_id.ToString(), False)
                Else
                    hdnDetaId.Text = "0"
                End If

                If objDatos.TramitesProductos.Count > 1 Then
                    usrProd_aux.Visible = False
                    hdnMultiProd.Text = "S"
                Else
                    If oProducto.prdt_id <> 0 Then
                        usrProd_aux.Valor = oProducto.prdt_id
                        usrProd_aux.txtProdNombExt.Valor = oProducto.prdt_nomb
                        usrProd_aux.cmbProdAsocExt.Valor = ValidarNulos(oProducto.prdt_ori_asoc_id.ToString(), True)
                        usrProd_aux.txtRPExt.Text = ValidarNulos(oProducto.prdt_rp.ToString(), True)
                        usrProd_aux.txtSraNumeExt.Valor = ValidarNulos(oProducto.prdt_sra_nume.ToString(), True)
                        usrProd_aux.cmbSexoProdExt.Valor = IIf(oProducto.prdt_sexo, "1", "0")
                        usrProd_aux.cmbSexoProdExt.SelectedValue = IIf(oProducto.prdt_sexo, "1", "0")
                        usrProd_aux.Sexo = IIf(oProducto.prdt_sexo, "1", "0")
                        hdnSexo.Text = IIf(oProducto.prdt_sexo, "1", "0")
                    End If
                    If (ValidarNulos(objDatos.Tramites.First().tram_dosi_cant.ToString(), True) = String.Empty And _
                        ValidarNulos(objDatos.Tramites.First().tram_embr_cant.ToString(), True) = String.Empty _
                        And ValidarNulos(objDatos.Tramites.First().tram_cria_cant.ToString(), True) = String.Empty) Then
                    End If
                    usrProd_aux.Visible = True
                    hdnMultiProd.Text = String.Empty
                End If
                'datos del último analisis
                If objDatosProd.ProductosAnalisis.Count > 0 Then
                    objDatosProd.ProductosAnalisis.OrderBy(Function(x) x.prta_fecha).Reverse().ToList()
                    txtNroAnal.Valor = objDatosProd.ProductosAnalisis.First().prta_nume
                    txtResulAnal.Valor = objDatosProd.ProductosAnalisis.First()._resul_codi & "-" & objDatosProd.ProductosAnalisis.First()._resul
                End If

                mConsultarRequ()
                mConsultarDocu()
                mConsultarObse()

                hdnTram_vc_id.Value = String.Empty
                btnModificarComprador.Enabled = False
                btnEliminarComprador.Enabled = False
                btnCancelarComprador.Enabled = False

                If usrProd_aux.cmbSexoProdExt.ValorBool Then 'macho
                    txtCantCriasReser.Enabled = True
                    chkReservaCrias.Disabled = False
                    txtCantReservaEmbriones.Enabled = False
                    chkReservaEmbriones.Disabled = True
                Else 'hembra
                    txtCantCriasReser.Enabled = False
                    chkReservaCrias.Disabled = True
                    txtCantReservaEmbriones.Enabled = True
                    chkReservaEmbriones.Disabled = False
                End If

                ' Método para modificar controles según el nombres de tabla
                mSetearEditor("tramites", False)

                hdnSumaPorcentajeCompra.Value = objCompradores.Sum(Function(x) x.tram_vc_porcentaje)

                ScriptManager.RegisterStartupScript(Page, Page.GetType, "dcpat", "deshabilitarCtrolProductoATransferir();", True)

                mMostrarPanel(True)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosRequ(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnRequId.Text = E.Item.Cells(1).Text
                Dim taepdr As New TransferenciaAnimalEnPie_TramitesDetaEntity
                taepdr = objDatos.TramitesDeta.Find(Function(x) x.trad_id = Convert.ToInt32(hdnRequId.Text))
                cmbRequRequ.Valor = taepdr.trad_requ_id
                chkRequPend.Checked = taepdr.trad_pend
                lblRequManu.Text = taepdr._manu
                mSetearEditor(mstrTablaRequisitos, False)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosDocu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnDocuId.Text = E.Item.Cells(1).Text

                Dim docu As New TransferenciaAnimalEnPie_TramitesDocumEntity
                docu = objDatos.TramitesDocum.Find(Function(x) x.trdo_id = Convert.ToInt32(hdnDocuId.Text))
                If IsNothing(docu.trdo_path) Then
                    txtDocuDocu.Valor = ""
                    imgDelDocuDoc.Visible = False
                Else
                    imgDelDocuDoc.Visible = True
                    txtDocuDocu.Valor = docu.trdo_path
                End If
                txtDocuObse.Valor = docu.trdo_refe
                mSetearEditor(mstrTablaDocumentos, False)

                'Dim ldrDoc As DataRow

                'ldrDoc = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)

                'With ldrDoc
                '    If .IsNull("trdo_path") Then
                '        txtDocuDocu.Valor = ""
                '        imgDelDocuDoc.Visible = False
                '    Else
                '        imgDelDocuDoc.Visible = True
                '        txtDocuDocu.Valor = .Item("trdo_path")
                '    End If
                '    txtDocuObse.Valor = .Item("trdo_refe")
                'End With

                'mSetearEditor(mstrTablaDocumentos, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnObseId.Text = E.Item.Cells(1).Text

                Dim obs As New TransferenciaAnimalEnPie_TramitesObseEntity
                obs = objDatos.TramitesObse.Find(Function(x) x.trao_id = hdnObseId.Text)
                cmbObseRequ.Valor = obs._trad_requ_id
                txtObseObse.Valor = obs.trao_obse
                lblObseFecha.Text = CDate(obs.trao_fecha.ToString("dd/MM/yyyy"))
                mSetearEditor(mstrTablaObservaciones, False)


                'Dim ldrObse As DataRow


                'ldrObse = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)

                'With ldrObse
                '    cmbObseRequ.Valor = .Item("_trad_requ_id")
                '    txtObseObse.Valor = .Item("trao_obse")
                '    lblObseFecha.Text = CDate(.Item("trao_fecha")).ToString("dd/MM/yyyy")
                'End With

                'mSetearEditor(mstrTablaObservaciones, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mLimpiarFiltros()
            txtNumeFil.Text = ""
            txtFechaDesdeFil.Text = ""
            txtFechaHastaFil.Text = ""
            cmbEstadoFil.Limpiar()
            txtNumeFil.Text = ""
            usrProdFil.Limpiar()
            txtFechaTransferenciaFil.Text = ""
            usrCompradorFil.Limpiar()

            grdDato.Visible = False
        End Sub

        Private Sub mLimpiar_New()
            hdnId.Text = String.Empty
            hdnRazaId.Text = String.Empty
            lblBaja.Text = String.Empty
            hdnMultiProd.Text = String.Empty
            hdnTEId.Text = String.Empty
            txtNroControl_aux.Text = String.Empty
            txtInicFecha_aux.Fecha = Now
            txtFechaTram_aux.Fecha = Now
            txtFechaTransferencia_aux.Fecha = String.Empty
            txtFinaFecha_aux.Text = String.Empty
            txtFinaFecha_aux.Enabled = True
            'txtEstado.Text = SRA_Neg.Constantes.TramiteRetenida
            usrProd_aux.Visible = True
            usrProd_aux.Limpiar()
            lblTitu_aux.Text = String.Empty
            chkRetenerTransferencia.Checked = IIf(hdnOperacion.Value = "A", True, False)
            chkReservaSemen.Checked = False
            txtCantReservaSemen.Text = String.Empty
            chkReservaCrias.Checked = False
            txtCantCriasReser.Text = String.Empty
            chkReservaEmbriones.Checked = False
            txtCantReservaEmbriones.Text = String.Empty
            chkReservaVientre.Checked = False
            chkReservaMaterialGenetico.Checked = False
            chkReservaDerechoClonar.Checked = False
            chkAutorizaCompradorClonar.Checked = False
            hdnTramNro.Text = String.Empty
            grdRequ.CurrentPageIndex = 0
            grdDocu.CurrentPageIndex = 0
            grdObse.CurrentPageIndex = 0

            ' Métodos para limpiar controles según los nombres de tabla
            mLimpiarRequ()
            mLimpiarDocu()
            mLimpiarObse()

            ' limpiar grids.
            limpiarGridRecu()
            limpiarGridDocu()
            limpiarGridObse()
            LimpiarGridVendedores()
            LimpiarGridCompradores()

            grdRequ.CurrentPageIndex = 0
            grdDato.CurrentPageIndex = 0
            grdObse.CurrentPageIndex = 0

            ' Limpia los objetos 
            objDatos = TransferenciaAnimalEnPieBusiness.LimpiarTramite()
            objDatosProd = TransferenciaAnimalEnPieBusiness.LimpiarProducto
            Session("mitramite") = objDatos
            Session("sessProductos") = objDatosProd

            ' Método para modificar controles según el nombres de tabla
            mSetearEditor(mstrTabla, True)

            hdnTotalPorcCompra.Value = 0
            hdnTotalPorcVenta.Value = 0

            mShowTabs(1)

            ' Tener en cuenta este código comentado para cuando se trata de la acción Agregar la cual le asigna un valor 
            ' y lo persisten en session.
            'mCargarPlantilla()

        End Sub

        Private Sub limpiarGridDato()
            Dim dt As New DataTable
            dt.Columns.Add("tram_id")
            dt.Columns.Add("tram_inic_fecha")
            dt.Columns.Add("tram_oper_fecha")
            dt.Columns.Add("tram_nume")
            dt.Columns.Add("raza")
            dt.Columns.Add("sexo")
            dt.Columns.Add("prdt_nomb")
            dt.Columns.Add("_numero")
            dt.Columns.Add("tram_comp_clie_id")
            dt.Columns.Add("tram_vend_clie_id")
            dt.Columns.Add("clie_apel")
            dt.Columns.Add("esta_desc")
            grdDato.DataSource = dt
            grdDato.DataBind()
        End Sub

        Private Sub limpiarGridRecu()
            Dim dt As New DataTable
            dt.Columns.Add("trad_id")
            dt.Columns.Add("_requ_desc")
            dt.Columns.Add("_pend")
            dt.Columns.Add("_manu")
            dt.Columns.Add("_estado")
            grdRequ.DataSource = dt
            grdRequ.DataBind()
        End Sub

        Private Sub limpiarGridDocu()
            Dim dt As New DataTable
            dt.Columns.Add("trdo_id")
            dt.Columns.Add("trdo_path")
            dt.Columns.Add("trdo_refe")
            dt.Columns.Add("_estado")
            grdDocu.DataSource = dt
            grdDocu.DataBind()
        End Sub

        Private Sub limpiarGridObse()
            Dim dt As New DataTable
            dt.Columns.Add("trao_id")
            dt.Columns.Add("trao_fecha")
            dt.Columns.Add("_requ_desc")
            dt.Columns.Add("trao_obse")
            grdObse.DataSource = dt
            grdObse.DataBind()
        End Sub

        Private Sub mLimpiarRequ()
            hdnRequId.Text = ""
            cmbRequRequ.Limpiar()
            chkRequPend.Checked = False
            lblRequManu.Text = "Sí"

            mSetearEditor(mstrTablaRequisitos, True)
        End Sub

        Private Sub mLimpiarDocu()
            hdnDocuId.Text = ""
            txtDocuObse.Valor = ""
            txtDocuDocu.Valor = ""
            imgDelDocuDoc.Visible = False

            mSetearEditor(mstrTablaDocumentos, True)
        End Sub

        Private Sub mLimpiarObse()
            hdnObseId.Text = ""
            cmbObseRequ.Limpiar()
            txtObseObse.Text = ""
            txtObseFecha.Fecha = Now

            mSetearEditor(mstrTablaObservaciones, True)
        End Sub

        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            Select Case pstrTabla
                Case mstrTabla
                    btnAlta.Enabled = pbooAlta
                    If txtEstado.Text.Trim.Length > 0 Then
                        If txtEstado.Text = SRA_Neg.Constantes.TramiteVigente Then
                            'btnModi.Enabled = False
                            CambiarEstadoControlesReservas(False)
                            btnErr.Visible = False
                            btnCertProp.Enabled = True
                        Else
                            btnBaja.Enabled = Not (pbooAlta)
                            btnModi.Enabled = Not (pbooAlta)
                            btnErr.Visible = Not (pbooAlta)
                            btnCertProp.Enabled = False
                        End If
                    End If
                Case mstrTablaRequisitos
                    btnAltaRequ.Enabled = pbooAlta
                    If txtEstado.Text.Trim.Length > 0 Then
                        If txtEstado.Text = SRA_Neg.Constantes.TramiteVigente Then
                            btnBajaRequ.Enabled = False
                            btnModiRequ.Enabled = False
                        Else
                            btnBajaRequ.Enabled = Not (pbooAlta)
                            btnModiRequ.Enabled = Not (pbooAlta)
                        End If
                    End If
                Case mstrTablaDocumentos
                    btnAltaDocu.Enabled = pbooAlta
                    If txtEstado.Text.Trim.Length > 0 Then
                        If txtEstado.Text = SRA_Neg.Constantes.TramiteVigente Then
                            btnBajaDocu.Enabled = False
                            btnModiDocu.Enabled = False
                        Else
                            btnBajaDocu.Enabled = Not (pbooAlta)
                            btnModiDocu.Enabled = Not (pbooAlta)
                        End If
                    End If
                Case mstrTablaObservaciones
                    btnAltaObse.Enabled = pbooAlta
                    If txtEstado.Text.Trim.Length > 0 Then
                        If txtEstado.Text = SRA_Neg.Constantes.TramiteVigente Then
                            btnBajaObse.Enabled = False
                            btnModiObse.Enabled = False
                        Else
                            btnBajaObse.Enabled = Not (pbooAlta)
                            btnModiObse.Enabled = Not (pbooAlta)
                        End If
                    End If
            End Select
        End Sub

        Private Sub mAgregarNew()
            Try
                mLimpiar_New()

                mstrTrapId = SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos ' hdnDatosPop.Text Dario 2014-05-28

                txtEstado.Text = SRA_Neg.Constantes.TramiteRetenida

                hdnEstadoId.Value = SRA_Neg.Constantes.Estados.Tramites_Retenida

                btnBaja.Enabled = False
                btnModi.Enabled = False
                btnAlta.Enabled = True
                mMostrarPanel(True)
                Session("mstrTrapId") = mstrTrapId

                objVendedores = New List(Of TramitesVendedoresCompradoresEntity)
                Session("objVendedores") = objVendedores

                objCompradores = New List(Of TramitesVendedoresCompradoresEntity)
                Session("objCompradores") = objCompradores

                hdnGridVendedoresCount.Value = objVendedores.Count
                hdnGridCompradoresCount.Value = objCompradores.Count

                hdnSumaPorcentajeCompra.Value = "0"

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mCerrar()
            mMostrarPanel(False)
        End Sub

        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            lnkCabecera.Font.Bold = True
            lnkRequ.Font.Bold = False
            lnkDocu.Font.Bold = False
            lnkObse.Font.Bold = False

            panDato.Visible = pbooVisi
            panDato_aux.Visible = pbooVisi

            panBotones.Visible = pbooVisi

            panFiltros.Visible = Not panDato.Visible
            grdDato.Visible = Not panDato.Visible

            panCabecera.Visible = True
            panCabecera_aux.Visible = True

            panRequ.Visible = False
            panDocu.Visible = False
            panObse.Visible = False

            panLinks.Visible = pbooVisi
            btnAgre.Visible = Not panDato.Visible

        End Sub

#End Region

#Region "Opciones de ABM"

        Private Sub mAltaNew()
            Try
                Dim eError As New ErrorEntity
                Dim strSexo As String
                Dim strRaza As String
                Dim strTramiteProdId As String
                Dim strProdId As String
                Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
                Dim tramp_id = 0
                Dim objComprador As New TramitesVendedoresCompradoresEntity
                Dim objVendedor As New TramitesVendedoresCompradoresEntity

                objVendedores = Session("objVendedores")
                objCompradores = Session("objCompradores")

                If txtFechaTransferencia_aux.Text.Trim.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Ingrese fecha de transferencia")
                End If

                If hdnProductoId.Value.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Seleccione Producto a transferir")
                End If

                If IsNothing(usrProd_aux.cmbSexoProdExt.ValorBool) Then
                    Throw New AccesoBD.clsErrNeg("Seleccione el sexo del producto a transferir")
                End If

                If usrProd_aux.txtRPExt.Text.Trim.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Ingrese RP")
                End If

                If usrProd_aux.txtSraNumeExt.Text.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Ingrese HBA")
                End If

                If IsNothing(objCompradores) Or IsNothing(objVendedores) Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un Vendedor y un Comprador")
                End If

                If objVendedores.Count = 0 Or objCompradores.Count = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un Vendedor y un Comprador")
                End If

                If objCompradores.Sum(Function(x) x.tram_vc_porcentaje) <> 100 Then
                    Throw New AccesoBD.clsErrNeg("La suma de porcentajes de compra debe ser del 100%")
                End If

                If objCompradores.Count > 0 Then
                    objComprador = objCompradores.First()
                End If

                If objVendedores.Count > 0 Then
                    objVendedor = objVendedores.First()
                End If

                Dim ValorActual As Integer
                ValorActual = SRA_Neg.Constantes.Estados.Tramites_Vigente

                objDatos = mGuardarDatosNew(objComprador, objVendedor)

                If objDatosProd.Productos.Count > 0 Then
                    If txtFechaTransferencia_aux.Fecha.Length > 0 Then
                        objDatosProd.Productos.First().prdt_tran_fecha = txtFechaTransferencia_aux.Fecha
                    Else
                        objDatosProd.Productos.First().prdt_tran_fecha = Nothing
                    End If
                End If

                If objDatosProd Is Nothing Then
                    strProdId = Convert.ToString(usrProd_aux.Valor.ToString())
                    hdnProdId.Text = usrProd_aux.Valor.ToString
                End If

                objDatosProd.Productos.First().prdt_id = usrProd_aux.Valor
                strSexo = ValidarNulos(objDatosProd.Productos.First().prdt_sexo, True)

                tramp_id = TransferenciaAnimalEnPieBusiness.Tramites_Alta(objDatos)

                objVendedor.tram_vc_tram_id = tramp_id
                objComprador.tram_vc_tram_id = tramp_id

                TramitesVendedoresCompradoresBusiness.InsertTramiteCompradorVendedor(objVendedor)
                TramitesVendedoresCompradoresBusiness.InsertTramiteCompradorVendedor(objComprador)

                objDatos.Tramites.First().tram_id = tramp_id

                hdnId.Text = tramp_id

                GuardarTramiteReservas(True)

                Dim intClienteId As Integer
                Dim intCriaId As Integer
                intClienteId = objCompradores.First().tram_vc_clie_id
                intCriaId = objCompradores.First().tram_vc_cria_id

                Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
                lblAltaId.Text = oTramite.mObtenerTramite(tramp_id)

                ' Revisar si este bloque es necesario ------------------------------------------------
                objDatos.Tramites.First().tram_id = lblAltaId.Text
                objDatosProd.Productos.First().prdt_tram_nume = Convert.ToInt32(lblAltaId.Text)
                objDatosProd.Productos.First().prdt_prop_cria_id = intCriaId
                objDatosProd.Productos.First().prdt_prop_clie_id = intClienteId
                strRaza = ValidarNulos(objDatosProd.Productos.First().prdt_raza_id, True)
                strTramiteProdId = ValidarNulos(objDatos.TramitesProductos.First().trpr_prdt_id, True)
                ' ------------------------------------------------------------------------------------

                ' GM 10/12/2021.
                validarTransferenciaProductorN(objDatos.TramitesProductos.First().trpr_id, objDatos.Tramites.First().tram_audi_user)

                mConsultar(False)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                    mConsultar(True)
                End If
            Finally
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
            End Try
        End Sub

        Private Sub validarTransferenciaProductorN(tramiteProdId As Integer, usua_id As Integer)
            Dim msj As String = TransferenciaAnimalEnPieBusiness.P_rg_ValidarTransferenciaProductosN(tramiteProdId, usua_id, chkRetenerTransferencia.Checked)
            If (msj.Trim.Length > 0) Then
                Throw New AccesoBD.clsErrNeg(msj)
            End If
        End Sub

        Private Sub mBajaNew()
            Try
                Dim lintPage As Integer = grdDato.CurrentPageIndex

                'cmbEsta.SelectedValue = SRA_Neg.Constantes.Estados.Tramites_Baja

                mModiNew()

                grdDato.CurrentPageIndex = 0

                mConsultar(True)
                mMostrarPanel(False)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            Finally
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
            End Try
        End Sub

        Private Sub mModiNew()
            Try

                If hdnEstadoId.Value = SRA_Neg.Constantes.Estados.Tramites_Vigente Then
                    Dim prdt_id = usrProd_aux.Valor
                    Dim fecha = Convert.ToDateTime(txtFechaTransferencia_aux.Text)
                    Dim tram_id = Convert.ToInt32(hdnId.Text)

                    Dim result As String = TransferenciaAnimalEnPieBusiness.P_rg_TranferenciaProductoValidaCambioFecha(prdt_id, fecha, tram_id)

                    If result.Trim.Length > 0 Then
                        Throw New AccesoBD.clsErrNeg(result)
                    End If
                End If

                Dim objComprador As New TramitesVendedoresCompradoresEntity
                Dim objVendedor As New TramitesVendedoresCompradoresEntity

                objCompradores = Session("objCompradores")
                objVendedores = Session("objVendedores")

                If objCompradores.Count = 0 Or objVendedores.Count = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar compradores y vendedores")
                End If

                If objCompradores.Sum(Function(x) x.tram_vc_porcentaje) <> 100 Or objCompradores.Sum(Function(x) x.tram_vc_porcentaje) <> 100 Then
                    Throw New AccesoBD.clsErrNeg("Los porcentajes de Vendedores y Compradores deben ser del 100%")
                End If

                If objCompradores.Count > 0 Then
                    objComprador = objCompradores.First()
                End If

                If objVendedores.Count > 0 Then
                    objVendedor = objVendedores.First()
                End If

                ' Actualiza los datos del objeto.
                objDatos = mGuardarDatosNew(objComprador, objVendedor)

                TransferenciaAnimalEnPieBusiness.Tramites_Modi(objDatos)

                objVendedores = Session("objVendedores")

                objCompradores = Session("objCompradores")

                TramitesVendedoresCompradoresBusiness.Tramites_Vendedores_Compradores_Actualizar(Convert.ToInt32(hdnId.Text), objVendedores, objCompradores)

                TramitesVendedoresCompradoresBusiness.Tramites_Vendedores_Compradores_Eliminar(Convert.ToInt32(hdnId.Text), objVendedores, objCompradores)

                If hdnIdTramiteReserva.Value = 0 Then
                    GuardarTramiteReservas(True)
                Else
                    GuardarTramiteReservas(False)
                End If

                validarTransferenciaProductorN(objDatos.TramitesProductos.First().trpr_id, objDatos.Tramites.First().tram_audi_user)

                mLimpiar_New()
                mConsultar(False)
                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                    mConsultar(False)
                End If
            Finally
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
            End Try

        End Sub

        Private Sub GuardarTramiteReservas(esNuevoRegistro As Boolean)
            Dim tr As New TramitesReservasEntity
            tr.Tram_id = Convert.ToInt32(hdnId.Text)
            tr.ReservaSemen = IIf(chkReservaSemen.Checked, 1, 0)
            If txtCantReservaSemen.Text.Length > 0 Then
                tr.CantReservaSemen = Convert.ToInt32(txtCantReservaSemen.Text)
            Else
                tr.CantReservaSemen = 0
            End If
            tr.ReservaCria = IIf(chkReservaCrias.Checked, 1, 0)
            If txtCantCriasReser.Text.Length > 0 Then
                tr.CantReservaCria = Convert.ToInt32(txtCantCriasReser.Text)
            Else
                tr.CantReservaCria = 0
            End If
            tr.ReservaEmbrion = IIf(chkReservaEmbriones.Checked, 1, 0)
            If txtCantReservaEmbriones.Text.Length > 0 Then
                tr.CantReservaEmbrion = Convert.ToInt32(txtCantReservaEmbriones.Text)
            Else
                tr.CantReservaEmbrion = 0
            End If
            tr.ReservaVientre = IIf(chkReservaVientre.Checked, 1, 0)
            tr.ReservaMaterialGenetico = IIf(chkReservaMaterialGenetico.Checked, 1, 0)
            tr.ReservaDerechoClonar = IIf(chkReservaDerechoClonar.Checked, 1, 0)
            tr.AutorizaCompradorClonar = IIf(chkAutorizaCompradorClonar.Checked, 1, 0)
            tr.IdUsuarioAudi = Convert.ToInt32(Session("sUserId"))

            If esNuevoRegistro Then
                TramitesReservasBusiness.InsertTramiteReservas(tr)
            Else
                TramitesReservasBusiness.UpdateTramiteReservas(tr)
            End If
        End Sub

        Private Sub GetTramiteReserva(ByVal tr As TramitesReservasEntity)
            chkReservaSemen.Checked = tr.ReservaSemen
            txtCantReservaSemen.Text = IIf(tr.CantReservaSemen = 0, String.Empty, tr.CantReservaSemen)
            chkReservaCrias.Checked = tr.ReservaCria
            txtCantCriasReser.Text = IIf(tr.CantReservaCria = 0, String.Empty, tr.CantReservaCria)
            chkReservaEmbriones.Checked = tr.ReservaEmbrion
            txtCantReservaEmbriones.Text = IIf(tr.CantReservaEmbrion = 0, String.Empty, tr.CantReservaCria)
            chkReservaVientre.Checked = tr.ReservaVientre
            chkReservaMaterialGenetico.Checked = tr.ReservaMaterialGenetico
            chkReservaDerechoClonar.Checked = tr.ReservaDerechoClonar
            chkAutorizaCompradorClonar.Checked = tr.AutorizaCompradorClonar
        End Sub


        Private Function mGuardarDatosPadre(ByVal pusrProd As SRA.usrProdDeriv, _
            ByVal pstrId As String, ByVal pbooCierre As Boolean) As DataSet

            Dim ldsDatos As DataSet
            Dim lstrId As String
            Dim ldrProd As DataRow
            Dim ldrNume As DataRow

            ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, SRA_Neg.Constantes.gTab_Productos, "", "")

            With ldsDatos
                .Tables(0).TableName = SRA_Neg.Constantes.gTab_Productos
                .Tables(1).TableName = SRA_Neg.Constantes.gTab_ProductosNumeros

            End With

            If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select.GetUpperBound(0) = -1 Then
                ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).NewRow
                ldrProd.Table.Rows.Add(ldrProd)
            Else
                ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select()(0)
            End If

            'productos
            With ldrProd
                lstrId = clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
                .Item("prdt_id") = lstrId
                .Item("prdt_raza_id") = pusrProd.cmbProdRazaExt.Valor
                .Item("prdt_sexo") = pusrProd.cmbSexoProdExt.ValorBool
                .Item("prdt_nomb") = pusrProd.txtProdNombExt.Valor
                '.Item("prdt_tran_fecha") = txtFechaTransferencia.Fecha
                '.Item("prdt_rp_nume") = IIf(IsNumeric(Trim(pusrProd.txtRPExt.Text)), Trim(pusrProd.txtRPExt.Text), DBNull.Value)
                .Item("prdt_rp") = IIf(Trim(pusrProd.txtRPExt.Text) = "", DBNull.Value, pusrProd.txtRPExt.Text.Trim())
                .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", "E", .Item("prdt_ndad"))
                .Item("prdt_ori_asoc_id") = pusrProd.cmbProdAsocExt.Valor
                .Item("prdt_ori_asoc_nume") = pusrProd.txtCodiExt.Valor
                '.Item("prdt_ori_asoc_nume_lab") = pusrProd.txtCodiExtLab.Valor
                .Item("generar_numero") = pbooCierre

            End With

            ''productos_numeros
            'If pusrProd.txtCodiExt.Text <> "" Then
            '    If ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select.GetUpperBound(0) = -1 Then
            '        ldrNume = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).NewRow
            '        ldrNume.Table.Rows.Add(ldrNume)
            '    Else
            '        ldrNume = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select()(0)
            '    End If
            '    With ldrNume
            '        .Item("ptnu_id") = clsSQLServer.gFormatArg("", SqlDbType.Int)
            '        .Item("ptnu_prdt_id") = lstrId
            '        .Item("ptnu_asoc_id") = pusrProd.cmbProdAsocExt.Valor
            '        .Item("ptnu_nume") = pusrProd.txtCodiExt.Text
            '    End With
            'End If

            Return ldsDatos
        End Function

        Private Sub mValidarDatos(ByVal pbooCierre As Boolean, ByVal pbooCierreBaja As Boolean)
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)
            'If ValidarNulos(txtNroControl.Valor, False) = 0 Then
            '    Me.panBotones.Style.Add("display", "inline")
            '    Me.divproce.Style.Add("display", "none")
            '    Throw New AccesoBD.clsErrNeg("El numero de Control debe ser distinto de cero.")
            'End If

            ' Dario 2014-10-01 se comenta para que se pueda ingresar productos sin sranume
            'If usrProd.txtSraNumeExt.Valor Is DBNull.Value Then
            '    Me.panBotones.Style.Add("display", "inline")
            '    Me.divproce.Style.Add("display", "none")
            '    Throw New AccesoBD.clsErrNeg("El Producto debe tener HBA o SBA ")
            'End If
            'If usrCriaComp.Valor.ToString = "" Then
            '    Throw New AccesoBD.clsErrNeg("Debe indicar el comprador.")
            'Else
            '    If usrCriaComp.Valor = "0" Then
            '        Throw New AccesoBD.clsErrNeg("El Comprador no tiene datos de Criador.")
            '    End If
            'End If

            'If usrCriaVend.Valor.ToString = "" Then
            '    Throw New AccesoBD.clsErrNeg("Debe indicar el vendedor.")
            'Else
            '    If usrCriaVend.Valor = "0" Then
            '        Throw New AccesoBD.clsErrNeg("El Vendedor no tiene datos de Criador.")
            '    End If
            'End If

            '' Dario 2014-10-31 comentado ahora no lo quierenal control de nro de control
            ' creo el objeto de productos 
            'Dim objProductosBusiness As New Business.Productos.ProductosBusiness
            '' ejecuto la validacion que me retorna un string con el mensaje de error si existe
            'Dim idRaza As String
            'idRaza = IIf(usrProd.RazaId = "", usrCriaComp.RazaId, usrProd.RazaId)
            ''
            'Dim msg As String = objProductosBusiness.ValidaNumeroControlTramitesPropiedad(txtNroControl.Valor, Convert.ToInt32("0" + usrProd.CriaOrPropId), usrCriaComp.Valor, idRaza, 1, 0, Convert.ToInt16(Common.CodigosServiciosTiposRRGG.TransferenciaAnimalesPie))
            'If (Len(msg.Trim) > 0) Then
            '    Throw New AccesoBD.clsErrNeg(msg)
            'End If

            'If usrProd.RazaId = "" Or usrProd.RazaId = "0" Then
            '    Throw New AccesoBD.clsErrNeg("El Raza del producto debe de estar informada.")
            'End If

            'If usrProd.CriaOrPropId.ToString().Trim() = "" Or usrProd.RazaId = "" Or usrProd.RazaId = "0" Then
            '    Throw New AccesoBD.clsErrNeg("El vendedor del producto debe de estar informado.")
            'End If

            'If IsDate(txtFechaTransferencia.Text) Then
            '    If CDate(txtFechaTransferencia.Text) > DateTime.Now Then
            '        Me.panBotones.Style.Add("display", "inline")
            '        Me.divproce.Style.Add("display", "none")
            '        Throw New AccesoBD.clsErrNeg("La Fecha de Transferencia no puede ser superior a la fecha actual.")
            '    End If
            '    'If CDate(txtFechaTram.Text) > DateTime.Now Then
            '    '    Me.panBotones.Style.Add("display", "inline")
            '    '    Me.divproce.Style.Add("display", "none")
            '    '    Throw New AccesoBD.clsErrNeg("La Fecha del Trámite no puede ser superior a la fecha actual.")
            '    'End If
            'End If

            'Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
            'If Not oTramite.ValidarPaisesTransferenciaByRazaCriador(usrCriaVend.cmbCriaRazaExt.Valor, _
            '                               usrCriaVend.Valor, _
            '                               usrCriaComp.cmbCriaRazaExt.Valor, _
            '                               usrCriaComp.Valor) Then
            '    Throw New AccesoBD.clsErrNeg("El comprador y el vendedor deben ser argentinos.")
            'End If
            If Not pbooCierreBaja Then
                If hdnSexo.Text <> "" Then
                    'usrProd.cmbSexoProdExt.Valor = hdnSexo.Text
                End If
                'If usrProd.cmbSexoProdExt.Valor.ToString = "" Then
                '    Me.panBotones.Style.Add("display", "inline")
                '    Me.divproce.Style.Add("display", "none")
                '    Throw New AccesoBD.clsErrNeg("Debe ingresar el Sexo del Producto para el Trámite.")
                'End If
                'If clsSQLServer.gCampoValorConsul(mstrConn, "razas_consul @raza_id=" + usrProd.RazaId.ToString, "raza_espe_id") <> SRA_Neg.Constantes.Especies.Peliferos Then
                '    If usrProd.txtProdNombExt.Valor.ToString = "" Then
                '        Me.panBotones.Style.Add("display", "inline")
                '        Me.divproce.Style.Add("display", "none")
                '        Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre del Producto para el Trámite.")
                '    End If
                'End If
            End If
        End Sub

        Private Function mGuardarDatosNew(objComprador As TramitesVendedoresCompradoresEntity, objVendedor As TramitesVendedoresCompradoresEntity) As TransferenciaAnimalEnPie_TramiteEntity
            Dim lbooCierre As Boolean = False
            Dim lbooCierreBaja As Boolean = False
            Dim dtProducto As DataTable
            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

            If objDatos.TramitesDeta.Where(Function(x) x.trad_pend = 1).Count = 0 And grdRequ.Items.Count > 0 Then
                lbooCierre = True
            End If

            If txtEstado.Text = SRA_Neg.Constantes.TramiteDeBaja Then
                lbooCierreBaja = True
            End If

            mValidarDatos(lbooCierre, lbooCierreBaja)
            Dim sTram_oper_fecha As String = String.Empty
            Dim sFc_Obtener_Tipo_Regimen As String = String.Empty

            If objDatos.Tramites.Count > 0 Then
                Dim item As TransferenciaAnimalEnPie_TramitesEntity = objDatos.Tramites.First()
                Dim idx = objDatos.Tramites.IndexOf(item)
                With item
                    item.tram_id = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    item.tram_ttra_id = mintTtraId
                    item.tram_nro_control = ValidarNulos(txtNroControl_aux.Text, False)
                    item.tram_raza_id = usrProd_aux.RazaId
                    item.tram_pres_fecha = txtFechaTram_aux.Fecha
                    item.tram_esta_id = Convert.ToInt32(hdnEstadoId.Value)
                    item.tram_pais_id = oCliente.GetPaisByClienteId(objComprador.tram_vc_clie_id)
                    item.tram_prop_clie_id = objComprador.tram_vc_clie_id
                    item.tram_vend_clie_id = objVendedor.tram_vc_clie_id
                    item.tram_comp_clie_id = objComprador.tram_vc_clie_id
                    item.tram_vend_cria_id = objVendedor.tram_vc_cria_id
                    item.tram_comp_cria_id = objComprador.tram_vc_cria_id
                    item.tram_prop_cria_id = objComprador.tram_vc_cria_id

                    If txtFechaTransferencia_aux.Fecha.Length = 0 Then
                        item.tram_oper_fecha = Nothing
                    Else
                        item.tram_oper_fecha = txtFechaTransferencia_aux.Fecha
                        sTram_oper_fecha = Convert.ToDateTime(txtFechaTransferencia_aux.Fecha).ToString("yyyyMMdd")
                    End If

                    If IsNothing(item.tram_inic_fecha) Then
                        item.tram_inic_fecha = Today
                    End If

                    item.tram_baja_fecha = IIf(lbooCierreBaja, Today, Nothing)
                    item.tram_fina_fecha = IIf(lbooCierreBaja, Today, Nothing)

                    If usrProd_aux.cmbSexoProdExt.ValorBool Then 'macho
                        sFc_Obtener_Tipo_Regimen = TransferenciaAnimalEnPieBusiness.Fc_Obtener_Tipo_Regimen(
                            Convert.ToInt32(Session("sUserId")), usrProd_aux.ProdId, sTram_oper_fecha, 6)
                        Select Case sFc_Obtener_Tipo_Regimen
                            Case "D"
                                item.tram_cria_cant = 0
                                item.tram_dosi_cant = clsSQLServer.gFormatArg(Me.txtCantCriasReser.Text, SqlDbType.Int)
                            Case "C"
                                item.tram_cria_cant = clsSQLServer.gFormatArg(Me.txtCantCriasReser.Text, SqlDbType.Int)
                                item.tram_dosi_cant = 0
                            Case Else
                                item.tram_cria_cant = 0
                                item.tram_dosi_cant = clsSQLServer.gFormatArg(Me.txtCantCriasReser.Text, SqlDbType.Int)
                        End Select
                    Else  'hembra
                        item.tram_cria_cant = 0
                        item.tram_dosi_cant = 0
                        item.tram_embr_cant = clsSQLServer.gFormatArg(Me.txtCantReservaEmbriones.Text, SqlDbType.Int)
                    End If

                    item.tram_audi_user = Session("sUserId").ToString()
                    ''dtProducto = oProducto.GetProductoById(usrProd_aux.Valor, String.Empty)
                    dtProducto = oProducto.GetProductoById(hdnProductoId.Value, String.Empty)
                    hdnProdId.Text = usrProd_aux.Valor
                    If dtProducto.Rows.Count = 0 Then
                        Throw New AccesoBD.clsErrNeg("El producto no existe en tabla de productos.")
                    Else
                        objDatosProd = mGuardarDatosProdNew(objComprador, objVendedor)
                        objDatosProd.Productos.First().prdt_id = usrProd_aux.Valor
                    End If
                End With
            End If

            'GUARDA LOS DATOS DE TRAMITES_PRODUCTOS
            Dim tp As New TransferenciaAnimalEnPie_TramitesProductosEntity
            tp.trpr_tram_id = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            tp.trpr_prdt_id = Convert.ToInt32(usrProd_aux.Valor)
            If usrProd_aux.cmbProdAsocExt.Valor = String.Empty Then
                tp.trpr_asoc_id = 0
            Else
                tp.trpr_asoc_id = Convert.ToInt32(usrProd_aux.cmbProdAsocExt.Valor)
            End If
            tp.trpr_audi_user = Session("sUserId").ToString()
            tp.trpr_baja_fecha = Nothing
            tp.trpr_sexo = usrProd_aux.cmbSexoProdExt.ValorBool

            If objDatos.TramitesProductos.Count = 0 Then
                Dim random As New Random()
                Dim idRandom = random.Next + 1
                tp.trpr_id = idRandom
                objDatos.TramitesProductos.Add(tp)
            Else
                Dim p As TransferenciaAnimalEnPie_TramitesProductosEntity = objDatos.TramitesProductos.First()
                If Not IsNothing(p) Then
                    Dim item As TransferenciaAnimalEnPie_TramitesProductosEntity = objDatos.TramitesProductos.First()
                    Dim idx = objDatos.TramitesProductos.IndexOf(item)
                    With item
                        item.trpr_tram_id = tp.trpr_tram_id
                        item.trpr_prdt_id = tp.trpr_prdt_id
                        item.trpr_asoc_id = tp.trpr_asoc_id
                        item.trpr_audi_user = tp.trpr_audi_user
                        item.trpr_baja_fecha = tp.trpr_baja_fecha
                        item.trpr_sexo = tp.trpr_sexo
                    End With
                    objDatos.TramitesProductos(idx) = item
                End If
            End If
            Return objDatos
        End Function

        Private Sub mActualizarRequ(ByVal pbooAlta As Boolean)
            Try
                mGuardarDatosRequ(pbooAlta)
                mLimpiarRequ()
                mConsultarRequ()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mValidarRequ(ByVal pbooAlta As Boolean)
            If cmbRequRequ.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
            End If
            If pbooAlta Then
                If Not IsNothing(objDatos.TramitesDeta) And objDatos.TramitesDeta.Count > 0 Then
                    Dim cant = 0
                    If hdnRequId.Text = String.Empty Then
                        cant = objDatos.TramitesDeta.Where(Function(x) IsNothing(x.trad_baja_fecha) And x.trad_requ_id = cmbRequRequ.Valor.ToString).Count
                    Else
                        cant = objDatos.TramitesDeta.Where(Function(x) IsNothing(x.trad_baja_fecha) And x.trad_requ_id = cmbRequRequ.Valor.ToString And x.trad_id <> hdnRequId.Text).Count
                    End If

                    If cant > 0 Then
                        Throw New AccesoBD.clsErrNeg("Requisito existente.")
                    End If
                    'If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                    '    Throw New AccesoBD.clsErrNeg("Requisito existente.")
                    'End If

                End If
            End If
        End Sub

        Private Sub mValidarModiRequ(ByVal pbooAlta As Boolean)
            If grdRequ.Items.Count = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
            End If
            If pbooAlta Then
                If Not IsNothing(objDatos.TramitesDeta) And objDatos.TramitesDeta.Count > 0 Then
                    Dim cant = 0
                    If hdnRequId.Text = String.Empty Then
                        cant = objDatos.TramitesDeta.Where(Function(x) IsNothing(x.trad_baja_fecha) And x.trad_requ_id = cmbRequRequ.Valor.ToString).Count
                    Else
                        cant = objDatos.TramitesDeta.Where(Function(x) IsNothing(x.trad_baja_fecha) And x.trad_requ_id = cmbRequRequ.Valor.ToString And x.trad_id <> hdnRequId.Text).Count
                    End If

                    If cant > 0 Then
                        Throw New AccesoBD.clsErrNeg("Requisito existente.")
                    End If
                    'If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                    '    Throw New AccesoBD.clsErrNeg("Requisito existente.")
                    'End If
                End If
            End If
        End Sub

        Private Sub mGuardarDatosRequ(ByVal pbooAlta As Boolean)
            ''Dim ldrDatos As DataRow
            If hdnOperacion.Value = "M" Then
                mValidarModiRequ(pbooAlta)
            Else
                mValidarRequ(pbooAlta)
            End If

            Dim taenpdr As New TransferenciaAnimalEnPie_TramitesDetaEntity

            taenpdr.trad_tram_id = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            taenpdr.trad_requ_id = cmbRequRequ.Valor
            taenpdr.trad_pend = chkRequPend.Checked
            taenpdr.trad_obli = True
            ''.Item("trad_manu") = IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
            taenpdr.trad_baja_fecha = Nothing
            taenpdr.trad_audi_user = Session("sUserId").ToString()
            taenpdr._requ_desc = cmbRequRequ.SelectedItem.Text
            taenpdr._pend = IIf(chkRequPend.Checked, "Sí", "No")
            taenpdr._manu = "Sí"
            taenpdr._estado = "Activo"

            If hdnRequId.Text = "" Then
                'Dim tmp As New List(Of TransferenciaAnimalEnPie_TramitesDetaEntity)
                'tmp.Add(New TransferenciaAnimalEnPie_TramitesDetaEntity() With {.trad_tram_id = 0})

                'ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                'ldrDatos.Item("trad_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRequisitos), "trad_id")

                Dim random As New Random()
                Dim idRandom = random.Next + 1
                taenpdr.trad_id = idRandom
                taenpdr.trad_manu = True
                objDatos.TramitesDeta.Add(taenpdr)
            Else
                ''ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)

                Dim p As TransferenciaAnimalEnPie_TramitesDetaEntity = objDatos.TramitesDeta.Find(Function(x) x.trad_id = hdnRequId.Text)
                If Not IsNothing(p) Then
                    Dim item As TransferenciaAnimalEnPie_TramitesDetaEntity = objDatos.TramitesDeta.Find(Function(x) x.trad_id = hdnRequId.Text)
                    Dim idx = objDatos.TramitesDeta.IndexOf(item)
                    With item
                        item.trad_tram_id = taenpdr.trad_tram_id
                        item.trad_requ_id = taenpdr.trad_requ_id
                        item.trad_pend = taenpdr.trad_pend
                        item.trad_obli = taenpdr.trad_obli
                        item.trad_manu = IIf(IsNothing(item.trad_manu), Nothing, item.trad_manu)
                        item.trad_baja_fecha = taenpdr.trad_baja_fecha
                        item.trad_audi_user = taenpdr.trad_audi_user
                        item._requ_desc = taenpdr._requ_desc
                        item._pend = taenpdr._pend
                        item._manu = taenpdr._manu
                        item._estado = taenpdr._estado
                    End With
                    objDatos.TramitesDeta(idx) = item
                End If

            End If

            'With ldrDatos
            '    .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            '    .Item("trad_requ_id") = cmbRequRequ.Valor
            '    .Item("trad_pend") = chkRequPend.Checked
            '    .Item("trad_obli") = True
            '    .Item("trad_manu") = IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
            '    .Item("trad_baja_fecha") = DBNull.Value
            '    .Item("trad_audi_user") = Session("sUserId").ToString()
            '    .Item("_requ_desc") = cmbRequRequ.SelectedItem.Text
            '    .Item("_pend") = IIf(chkRequPend.Checked, "Sí", "No")
            '    .Item("_manu") = "Sí"
            '    .Item("_estado") = "Activo"

            '    If hdnRequId.Text = "" Then
            '        .Table.Rows.Add(ldrDatos)
            '    End If
            'End With

        End Sub

        'DOCUMENTOS
        Private Sub mActualizarDocu()
            Try
                mGuardarDatosDocu()
                mLimpiarDocu()
                mConsultarDocu()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mValidarDocu()
            If txtDocuDocu.Valor.ToString = "" And filDocuDocu.Value = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
            End If

            If txtDocuObse.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
            End If
        End Sub

        Private Sub mGuardarDatosDocu()

            Dim ldrDatos As DataRow
            Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_tram_docu_path")

            mValidarDocu()

            Dim file As String = IIf(filDocuDocu.PostedFile.FileName.ToString().Length > 0, filDocuDocu.PostedFile.FileName, String.Empty)
            Dim nomFile As String = IIf(file.Length > 0, file.Substring(file.LastIndexOf("\") + 1), String.Empty)

            Dim random As New Random()
            Dim idRandom = random.Next + 1

            Dim docu As New TransferenciaAnimalEnPie_TramitesDocumEntity


            docu.trdo_tram_id = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            docu.trdo_path = IIf(nomFile.Length > 0, nomFile, txtDocuDocu.Valor)
            docu.trdo_refe = txtDocuObse.Valor
            docu.trdo_audi_fecha = Now
            docu.trdo_audi_user = Session("sUserId").ToString()
            docu.trdo_baja_fecha = Nothing
            docu.trdo_filename_adjunto = Nothing
            docu._estado = "Activo"

            'If hdnDocuId.Text = String.Empty Then
            '    docu.trdo_id = idRandom
            '    docu._parampath = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(docu.trdo_id, "-", "m") + "__" + docu.trdo_path
            'Else
            '    docu._parampath = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(docu.trdo_id, "-", "m") + "__" + docu.trdo_path
            'End If

            '' No se pasa a objetos es método por cuestiones de tiempo.
            '' Guarda en una carpeta del servidor el archivo seleccionado. 
            'SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, docu._parampath, Context, filDocuDocu)

            If hdnDocuId.Text = String.Empty Then
                docu.trdo_id = idRandom
                docu._parampath = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(docu.trdo_id, "-", "m") + "__" + docu.trdo_path
                objDatos.TramitesDocum.Add(docu)
            Else
                Dim tmp As TransferenciaAnimalEnPie_TramitesDocumEntity = objDatos.TramitesDocum.Find(Function(x) x.trdo_id = hdnDocuId.Text)
                If Not IsNothing(tmp) Then
                    Dim item As TransferenciaAnimalEnPie_TramitesDocumEntity = objDatos.TramitesDocum.Find(Function(x) x.trdo_id = hdnDocuId.Text)
                    Dim idx = objDatos.TramitesDocum.IndexOf(item)
                    docu._parampath = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(item.trdo_id, "-", "m") + "__" + docu.trdo_path
                    With item
                        item.trdo_tram_id = docu.trdo_tram_id
                        item.trdo_path = docu.trdo_path
                        item.trdo_refe = docu.trdo_refe
                        item.trdo_audi_fecha = docu.trdo_audi_fecha
                        item.trdo_audi_user = docu.trdo_audi_user
                        item.trdo_baja_fecha = docu.trdo_baja_fecha
                        item.trdo_filename_adjunto = docu.trdo_filename_adjunto
                        item._estado = docu._estado
                    End With
                    objDatos.TramitesDocum(idx) = item
                End If


            End If

            ' No se pasa a objetos es método por cuestiones de tiempo.
            ' Guarda en una carpeta del servidor el archivo seleccionado. 
            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, docu._parampath, Context, filDocuDocu)


            'If hdnDocuId.Text = "" Then
            '    ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).NewRow
            '    ldrDatos.Item("trdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocumentos), "trdo_id")
            'Else
            '    ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)
            'End If

            'With ldrDatos
            '    .Item("trdo_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            '    If filDocuDocu.Value <> "" Then
            '        .Item("trdo_path") = filDocuDocu.Value
            '    Else
            '        .Item("trdo_path") = txtDocuDocu.Valor
            '    End If
            '    If Not .IsNull("trdo_path") Then
            '        .Item("trdo_path") = .Item("trdo_path").Substring(.Item("trdo_path").LastIndexOf("\") + 1)
            '    End If
            '    .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(.Item("trdo_id"), "-", "m") + "__" + .Item("trdo_path")
            '    .Item("trdo_refe") = txtDocuObse.Valor
            '    .Item("trdo_baja_fecha") = DBNull.Value
            '    .Item("trdo_audi_user") = Session("sUserId").ToString()
            '    .Item("_estado") = "Activo"

            '    SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocuDocu)

            '    If hdnDocuId.Text = "" Then
            '        .Table.Rows.Add(ldrDatos)
            '    End If
            'End With


        End Sub

        Private Sub mActualizarObse()
            Try
                mGuardarDatosObse()
                mLimpiarObse()
                mConsultarObse()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mValidarObse()
            If txtObseObse.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Observación.")
            End If
            If txtObseFecha.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de la Observación.")
            End If
        End Sub

        Private Sub mGuardarDatosObse()
            mValidarObse()

            Dim random As New Random()
            Dim idRandom = random.Next + 1

            Dim obs As New TransferenciaAnimalEnPie_TramitesObseEntity
            obs.trao_tram_id = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            If cmbObseRequ.Valor.ToString = String.Empty Then
                obs.trao_trad_id = Nothing
            Else
                obs.trao_trad_id = objDatos.TramitesDeta.Where(Function(x) x.trad_requ_id = cmbObseRequ.Valor.ToString).First.trad_id
            End If
            obs._requ_desc = IIf(cmbObseRequ.Valor.ToString = "", "COMENTARIO GENERAL", cmbObseRequ.SelectedItem.Text)
            obs._trad_requ_id = IIf(cmbObseRequ.Valor.ToString = String.Empty, 0, Convert.ToInt32(cmbObseRequ.Valor))
            obs.trao_obse = txtObseObse.Valor
            obs.trao_fecha = txtObseFecha.Fecha
            obs.trao_audi_user = Session("sUserId").ToString()

            If hdnObseId.Text = String.Empty Then
                obs.trao_id = idRandom
                objDatos.TramitesObse.Add(obs)
            Else

                Dim tmp As TransferenciaAnimalEnPie_TramitesObseEntity = objDatos.TramitesObse.Find(Function(x) x.trao_id = hdnObseId.Text)
                If Not IsNothing(tmp) Then
                    Dim item As TransferenciaAnimalEnPie_TramitesObseEntity = objDatos.TramitesObse.Find(Function(x) x.trao_id = hdnObseId.Text)
                    Dim idx = objDatos.TramitesObse.IndexOf(item)
                    With item
                        item.trao_obse = obs.trao_obse
                        item.trao_tram_id = obs.trao_tram_id
                        item.trao_trad_id = obs.trao_trad_id
                        item.trao_fecha = obs.trao_fecha
                        item.trao_audi_fecha = obs.trao_audi_fecha
                        item.trao_audi_user = obs.trao_audi_user
                    End With
                    objDatos.TramitesObse(idx) = item
                End If

            End If

        End Sub

        Private Function mGuardarDatosProd() As DataSet
            Dim ldsDatosProd As DataSet
            Dim ldrDatosProd As DataRow
            Dim lbooCierre As Boolean = False
            Dim lbooCierreBaja As Boolean = False
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

            mValidarDatos(lbooCierre, lbooCierreBaja)

            If mdsDatosProd.Tables(mstrTablaProductos).Select.GetUpperBound(0) = -1 Then
                ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).NewRow
                ldrDatosProd.Table.Rows.Add(ldrDatosProd)
            Else
                ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).Select()(0)
            End If

            With ldrDatosProd
                If hdnProdId.Text <> "" Then
                    .Item("prdt_id") = clsSQLServer.gFormatArg(hdnProdId.Text, SqlDbType.Int)
                Else
                    .Item("prdt_id") = clsSQLServer.gFormatArg(ldrDatosProd.ItemArray(0).ToString(), SqlDbType.Int)
                End If
                '.Item("prdt_raza_id") = usrProd.RazaId
                '.Item("prdt_sexo") = usrProd.cmbSexoProdExt.ValorBool
                '.Item("prdt_nomb") = usrProd.txtProdNombExt.Valor
                '.Item("prdt_rp") = Trim(usrProd.txtRPExt.Text)
                '.Item("prdt_sra_nume") = ValidarNulos(Trim(usrProd.txtSraNumeExt.Text), False)
                ' .Item("prdt_rp_extr") = txtRPExtr.Valor
                '.Item("prdt_tran_fecha") = txtFechaTransferencia.Fecha
                .Item("prdt_ndad") = "N"
                '.Item("prdt_ori_asoc_id") = usrProd.cmbProdAsocExt.Valor
                '.Item("prdt_ori_asoc_nume") = usrProd.txtCodiExt.Valor
                '.Item("prdt_prop_clie_id") = _
                'oCliente.GetClienteIdByRazaCriador(usrProd.RazaId, usrProd.CriaOrPropId)
                .Item("generar_numero") = False
                '.Item("prdt_prop_cria_id") = usrProd.CriaOrPropId ' Dario 2014-12-09 se cambia para que no pise el cria_id
            End With

            mdsDatosProd.AcceptChanges()
            ldsDatosProd = mdsDatosProd
            ldsDatosProd.AcceptChanges()

            Return (ldsDatosProd)
        End Function

        Private Function mGuardarDatosProdNew(objComprador As TramitesVendedoresCompradoresEntity, objVendedor As TramitesVendedoresCompradoresEntity) As TransferenciaAnimalEnPie_ProductoEntity
            hdnProdId.Text = usrProd_aux.Valor

            Dim prod As New TransferenciaAnimalEnPie_ProductosEntity
            prod.prdt_raza_id = usrProd_aux.RazaId
            prod.prdt_sexo = usrProd_aux.cmbSexoProdExt.ValorBool
            prod.prdt_nomb = usrProd_aux.txtProdNombExt.Valor
            prod.prdt_rp = Trim(usrProd_aux.txtRPExt.Text)
            prod.prdt_sra_nume = ValidarNulos(Trim(usrProd_aux.txtSraNumeExt.Text), False)
            If Not IsNothing(prod.prdt_tran_fecha) Then
                prod.prdt_tran_fecha = txtFechaTransferencia_aux.Fecha
            End If
            prod.prdt_ndad = "N"
            If usrProd_aux.cmbProdAsocExt.Valor = String.Empty Then
                prod.prdt_ori_asoc_id = 0
            Else
                prod.prdt_ori_asoc_id = Convert.ToInt32(usrProd_aux.cmbProdAsocExt.Valor)
            End If

            prod.prdt_ori_asoc_nume = usrProd_aux.txtCodiExt.Valor
            prod.generar_numero = False
            prod.prdt_prop_cria_id = usrProd_aux.CriaOrPropId

            Dim idprod = String.Empty
            If hdnProdId.Text <> String.Empty Then
                idprod = clsSQLServer.gFormatArg(hdnProdId.Text, SqlDbType.Int)
            Else
                idprod = clsSQLServer.gFormatArg(objDatosProd.Productos.First().ToString(), SqlDbType.Int)
            End If

            prod.prdt_tran_fecha = txtFechaTransferencia_aux.Fecha
            prod.prdt_sra_nume = ValidarNulos(usrProd_aux.txtSraNumeExt.Text, False)
            prod.prdt_tram_nume = Convert.ToInt32(IIf(hdnTramNro.Text.Trim.Length = 0, 0, hdnTramNro.Text))
            prod.prdt_prop_cria_id = objComprador.tram_vc_cria_id
            prod.prdt_prop_clie_id = objComprador.tram_vc_clie_id

            If objDatosProd.Productos.Count = 0 Then
                prod.prdt_id = idprod
                objDatosProd.Productos.Add(prod)
            Else
                Dim item As TransferenciaAnimalEnPie_ProductosEntity = objDatosProd.Productos.First()
                Dim idx = objDatosProd.Productos.IndexOf(item)
                With item
                    item.prdt_raza_id = prod.prdt_raza_id
                    item.prdt_sexo = prod.prdt_sexo
                    item.prdt_nomb = prod.prdt_nomb
                    item.prdt_rp = prod.prdt_rp
                    item.prdt_sra_nume = prod.prdt_sra_nume
                    item.prdt_tram_nume = prod.prdt_tram_nume
                    item.prdt_tran_fecha = prod.prdt_tran_fecha
                    item.prdt_ndad = prod.prdt_ndad
                    item.prdt_ori_asoc_id = prod.prdt_ori_asoc_id
                    item.prdt_ori_asoc_nume = prod.prdt_ori_asoc_nume
                    item.generar_numero = prod.generar_numero
                    item.prdt_prop_cria_id = prod.prdt_prop_cria_id
                    item.prdt_prop_clie_id = prod.prdt_prop_clie_id
                End With
                objDatosProd.Productos(idx) = item
            End If

            Return objDatosProd
        End Function

#End Region

#Region "Eventos de Controles"

        Private Sub mShowTabs(ByVal origen As Byte)

            panDato.Visible = True

            If origen > 1 Then
                panDato_aux.Visible = False

            Else
                panDato_aux.Visible = True

            End If

            panBotones.Visible = True

            panCabecera.Visible = False
            panCabecera_aux.Visible = False

            panRequ.Visible = False
            panDocu.Visible = False
            panObse.Visible = False

            lnkCabecera.Font.Bold = False
            lnkRequ.Font.Bold = False
            lnkDocu.Font.Bold = False
            lnkObse.Font.Bold = False

            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())

            Select Case origen
                Case 1
                    panCabecera.Visible = True
                    panCabecera_aux.Visible = True
                    lnkCabecera.Font.Bold = True
                    CambiarEstadoControles()
                Case 2
                    panRequ.Visible = True
                    lnkRequ.Font.Bold = True
                Case 3
                    panDocu.Visible = True
                    lnkDocu.Font.Bold = True
                Case 4
                    If Not IsNothing(objDatos.TramitesDeta) And objDatos.TramitesDeta.Count > 0 Then
                        Dim requisitos As New List(Of TransferenciaAnimalEnPie_TramitesDetaEntity)
                        requisitos = objDatos.TramitesDeta.Where(Function(x) IsNothing(x.trad_baja_fecha)).ToList()
                        cmbObseRequ.DataTextField = "_requ_desc"
                        cmbObseRequ.DataValueField = "trad_requ_id"
                        cmbObseRequ.DataSource = requisitos
                    Else
                        cmbObseRequ.DataSource = objDatos.TramitesDeta
                    End If
                    cmbObseRequ.DataBind()
                    cmbObseRequ.Items.Insert(0, "(Seleccione)")
                    cmbObseRequ.Items(0).Value = String.Empty
                    cmbObseRequ.Valor = String.Empty

                    panObse.Visible = True
                    lnkObse.Font.Bold = True
            End Select

        End Sub

        Private Sub mLimpiarPersonas(ByVal pstrTabla As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
            For Each lDr As DataRow In mdsDatos.Tables(pstrTabla).Select()
                If lDr.Item("trpe_id") > 0 Then
                    lDr.Delete()
                Else
                    mdsDatos.Tables(pstrTabla).Rows.Remove(lDr)
                End If
            Next

            If mdsDatos.Tables(pstrTabla).Select.GetUpperBound(0) = -1 Then pctrHdnOrig.Text = ""

            pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
            pgrdGrilla.DataBind()
        End Sub

        Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAltaNew()
        End Sub

        Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar_New()
        End Sub

        Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBajaNew()
        End Sub

        Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModiNew()
        End Sub

        Private Sub btnAltaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRequ.Click
            mActualizarRequ(True)
        End Sub

        Private Sub btnLimpRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRequ.Click
            mLimpiarRequ()
        End Sub

        Private Sub btnBajaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRequ.Click
            Try
                Dim idx = objDatos.TramitesObse.FindIndex(Function(x) x.trao_trad_id = hdnRequId.Text)

                If idx <> -1 Then
                    objDatos.TramitesObse.RemoveAt(idx)
                End If

                'For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaObservaciones).Select("trao_trad_id=" + hdnRequId.Text)
                '    odrDeta.Delete()
                'Next

                mConsultarObse()

                ''mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0).Delete()

                idx = objDatos.TramitesDeta.FindIndex(Function(x) x.trad_id = hdnRequId.Text)

                If idx <> -1 Then
                    objDatos.TramitesDeta.RemoveAt(idx)
                End If

                grdRequ.CurrentPageIndex = 0
                mConsultarRequ()
                mLimpiarRequ()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnModiRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRequ.Click
            mActualizarRequ(False)
        End Sub

        Private Sub btnAltaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocu.Click
            mActualizarDocu()
        End Sub

        Private Sub btnLimpDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocu.Click
            mLimpiarDocu()
        End Sub

        Private Sub btnBajaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocu.Click
            Try
                Dim idx = objDatos.TramitesDocum.FindIndex(Function(x) x.trdo_id = hdnDocuId.Text)

                If idx <> -1 Then
                    objDatos.TramitesDocum.RemoveAt(idx)
                End If

                'mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0).Delete()

                grdDocu.CurrentPageIndex = 0
                mConsultarDocu()
                mLimpiarDocu()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnModiDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocu.Click
            mActualizarDocu()
        End Sub

        Private Sub btnAltaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaObse.Click
            mActualizarObse()
        End Sub

        Private Sub btnLimpObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpObse.Click
            mLimpiarObse()
        End Sub

        Private Sub btnBajaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaObse.Click
            Try
                Dim idx = objDatos.TramitesObse.FindIndex(Function(x) x.trao_id = hdnObseId.Text)

                If idx <> -1 Then
                    objDatos.TramitesObse.RemoveAt(idx)
                End If

                '' mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0).Delete()

                grdObse.CurrentPageIndex = 0
                mConsultarObse()
                mLimpiarObse()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnModiObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiObse.Click
            mActualizarObse()
        End Sub

        Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            hdnOperacion.Value = "A"
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "scrcvc", "setearCmbRazaCriaVendedorComprador();", True)
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "dcpat", "deshabilitarCtrolProductoATransferir();", True)
            mAgregarNew()
        End Sub

        Private Sub imgClose_aux_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose_aux.Click
            mCerrar()
        End Sub

        Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
            mLimpiarFiltros()
        End Sub

        Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
            grdDato.CurrentPageIndex = 0
            mConsultar(False)
        End Sub

        Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
            mShowTabs(1)
        End Sub

        Private Sub lnkRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRequ.Click
            mShowTabs(2)
        End Sub

        Private Sub lnkDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
            mShowTabs(3)
        End Sub

        Private Sub lnkObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkObse.Click
            mShowTabs(4)
        End Sub

        Private Sub btnCertProp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCertProp.Click
            Try
                mImprimirCertificado()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

#End Region

        Private Sub mImprimirCertificado()
            Try
                Dim params As String
                Dim lstrRptName As String
                Dim lstrRpt As String

                params += "&criador_id=0"
                params += "&FilFechaInscripcion=" & True
                params += "&FechaDesde=0"
                params += "&FechaHasta=0"
                params += "&tipoCertificado= 0"
                params += "&audi_user=" & Session("sUserId").ToString()

                Dim strEspe As String = clsSQLServer.gCampoValorConsul(mstrConn, "razas_consul @raza_id=" + usrProd_aux.RazaId.ToString, "raza_espe_id")

                Select Case strEspe
                    Case SRA_Neg.Constantes.Especies.Ovinos
                        If usrProd_aux.Sexo = 0 Then
                            'Hembra.
                            lstrRptName = "CertificadoRegPropLanarHembra"
                        Else
                            'Macho.
                            lstrRptName = "CertificadoRegPropLanarHembra"
                        End If
                        params += "&prdt_id=" & usrProd_aux.Valor
                        lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                        lstrRpt += params
                        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                        Response.Redirect(lstrRpt)
                    Case SRA_Neg.Constantes.Especies.Caprinos
                        lstrRptName = "CertificadoRegPropCaprinoHembra"
                        params += "&prdt_id=" & usrProd_aux.Valor
                        lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                        lstrRpt += params
                        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                        Response.Redirect(lstrRpt)
                    Case SRA_Neg.Constantes.Especies.Bovinos
                        'params += "&propie=" + usrCriaComp.Codi + " - " + usrCriaComp.Apel
                        lstrRptName = "CertificadoRegPropBovinos"
                        lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                        lstrRpt += params
                        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                        Response.Redirect(lstrRpt)
                    Case SRA_Neg.Constantes.Especies.Peliferos
                        lstrRptName = "CertificadoRegPropPeliferos"
                        lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                        lstrRpt += params
                        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                        Response.Redirect(lstrRpt)
                    Case SRA_Neg.Constantes.Especies.Equinos
                        Dim strIdRegistro As Object = clsSQLServer.gCampoValorConsul(mstrConn, "productos_consul @prdt_id=" + usrProd_aux.Valor, "prdt_regt_id")
                        If Not strIdRegistro Is Nothing Then
                            params += "&reg_tipo=" + strIdRegistro
                        End If
                        lstrRptName = "CertificadoRegPropEquino"
                        lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                        lstrRpt += params
                        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                        Response.Redirect(lstrRpt)
                    Case SRA_Neg.Constantes.Especies.Camelidos
                        lstrRptName = "CertificadoRegPropCamelidos"
                        lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                        lstrRpt += params
                        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                        Response.Redirect(lstrRpt)
                    Case Else
                        Return
                End Select

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

#Region "Opciones de POP"

        Private Sub mCargarPlantilla()
            If mstrTrapId <> "" Then

                Dim lDs As New DataSet
                lDs = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Tramites_Plantilla, "@SinBaja=1,@trap_id=" + mstrTrapId)
                Dim ldrDatos As DataRow

                mdsDatos.Tables(mstrTablaRequisitos).Clear()

                For Each ldrOri As DataRow In lDs.Tables(1).Select
                    ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                    With ldrDatos
                        .Item("trad_id") = clsSQLServer.gObtenerId(.Table, "trad_id")
                        .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                        .Item("trad_baja_fecha") = DBNull.Value
                        .Item("trad_requ_id") = ldrOri.Item("trpd_requ_id")
                        .Item("trad_obli") = ldrOri.Item("trpd_obli")
                        .Item("trad_pend") = True
                        .Item("trad_manu") = False
                        .Item("_requ_desc") = ldrOri.Item("_requisito")
                        .Item("_obli") = ldrOri.Item("_obligatorio")
                        .Item("_pend") = "Sí"
                        .Item("_manu") = "No"
                        .Item("_estado") = ldrOri.Item("_estado")

                        .Table.Rows.Add(ldrDatos)
                    End With
                Next

                Dim lstrRazas As String = ""
                Dim lstrEspecies As String = ""

                For Each ldrOri As DataRow In lDs.Tables(2).Select
                    With ldrOri
                        If .IsNull("trdr_raza_id") Then
                            If lstrEspecies.Length > 0 Then lstrEspecies += ","
                            lstrEspecies += .Item("trdr_espe_id").ToString
                        Else
                            If lstrRazas.Length > 0 Then lstrRazas += ","
                            lstrRazas += .Item("trdr_raza_id").ToString
                        End If
                    End With
                Next

                'usrProd.usrProductoExt.FiltroRazas = "@raza_ids = " & IIf(lstrRazas = "", "null", "'" & lstrRazas & "'") & ",@raza_espe_ids = " & IIf(lstrEspecies = "", "null", "'" & lstrEspecies & "'")
                'usrProd.usrProductoExt.mCargarRazas()

                mConsultarRequ()
            End If
        End Sub

        Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
            Try
                mstrTrapId = hdnDatosPop.Text
                'mAgregar()
                hdnDatosPop.Text = ""
                Session("mstrTrapId") = mstrTrapId

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

#End Region

        Private Sub hdnDatosTEPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosTEPop.TextChanged
            Try
                hdnTEId.Text = ""
                txtRecuFecha.Text = ""
                txtTeDesc.Text = ""
                txtCantEmbr.Text = ""


                If (hdnDatosTEPop.Text <> "") Then
                    hdnTEId.Text = hdnDatosTEPop.Text
                    hdnDatosTEPop.Text = ""
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub



#Region "GRID VENDEDORES"

        Protected Sub gridVendedores_UpdateCommand(source As Object, e As DataGridCommandEventArgs)
            Dim tram_vc_id = e.Item.Cells(1).Text
            hdnTram_vc_id.Value = e.Item.Cells(1).Text
            hdnTram_vc_clie_id.Value = e.Item.Cells(3).Text
            hdnTram_vc_cria_id.Value = e.Item.Cells(4).Text
            usrCriaVend_aux.Valor = hdnTram_vc_cria_id.Value

            objVendedores = Session("objVendedores")
            Dim idx = objVendedores.FindIndex(Function(x) x.tram_vc_id = hdnTram_vc_id.Value)
            Dim vendedor = objVendedores(idx)

            gridVendedores.Enabled = False
            gridCompradores.Enabled = False
            btnAgregarVendedor.Enabled = False
            btnModificarVendedor.Enabled = True
            btnEliminarVendedor.Enabled = True
            btnCancelarVendedor.Enabled = True

            tdUsrCriaComp_aux.Disabled = True
            txtPorcentajeCompra.Enabled = False
            btnAgregarComprador.Enabled = False

            CambiarEstadoControlesReservas(False)

            txtObservaciones.Enabled = False

            ScriptManager.RegisterStartupScript(Page, Page.GetType, "dcpat", "deshabilitarCtrolProductoATransferir();", True)
        End Sub

        Protected Sub gridVendedores_PageIndexChanged(source As Object, e As DataGridPageChangedEventArgs)
            Try
                gridVendedores.EditItemIndex = -1
                If (gridVendedores.CurrentPageIndex < 0 Or gridVendedores.CurrentPageIndex >= gridVendedores.PageCount) Then
                    gridVendedores.CurrentPageIndex = 0
                Else
                    gridVendedores.CurrentPageIndex = e.NewPageIndex
                End If

                objVendedores = Session("objVendedores")
                gridVendedores.DataSource = objVendedores
                gridVendedores.DataBind()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnAgregarVendedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarVendedor.Click
            Try
                Dim usrControl As New DatosUsrCtrolCLIEEntity
                usrControl = DatosUsrCtrolCLIEBusiness.GetDatosUsrCtrolCLIE(usrCriaVend_aux.Valor)

                objVendedores = Session("objVendedores")
                objVendedores.Add(New TramitesVendedoresCompradoresEntity() With {
                                   .tram_vc_id = GetNroAleatorio(),
                                   .tram_vc_tram_audi_fecha = Date.Now,
                                   .tram_vc_tram_audi_user = 1,
                                   .tram_vc_tram_baja_fecha = Nothing,
                                   .tram_vc_clie_id = Convert.ToInt32(usrCriaVend_aux.ClieId),
                                   .tram_vc_cria_id = Convert.ToInt32(usrCriaVend_aux.Valor),
                                   .tram_vc_porcentaje = 0,
                                   .tram_vc_tran_tipo = "V",
                                   .tram_vc_obser = String.Empty,
                                    .raza_codi_desc = usrControl.raza_codi_desc,
                                   .cria_nume = usrControl.cria_nume,
                                   .clie_fanta = usrControl.clie_fanta,
                                   .estado = String.Empty})

                Session("objVendedores") = objVendedores
                gridVendedores.DataSource = objVendedores
                gridVendedores.DataBind()

                CambiarEstadoControles()
                CambiarEstadoControlesReservas(True)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try

        End Sub

        Protected Sub btnModificarVendedor_Click(sender As Object, e As EventArgs) Handles btnModificarVendedor.Click
            Try
                objVendedores = Session("objVendedores")

                Dim item = objVendedores.Find(Function(x) x.tram_vc_id = Convert.ToInt32(hdnTram_vc_id.Value))
                Dim index = objVendedores.IndexOf(item)

                If item.tram_vc_cria_id <> Convert.ToInt32(usrCriaVend_aux.Valor) Or item.tram_vc_clie_id <> Convert.ToInt32(usrCriaVend_aux.ClieId) Then
                    ' Buscar datos en db.
                    Dim usrControl As New DatosUsrCtrolCLIEEntity
                    usrControl = DatosUsrCtrolCLIEBusiness.GetDatosUsrCtrolCLIE(usrCriaVend_aux.Valor)
                    item.raza_codi_desc = usrControl.raza_codi_desc
                    item.cria_nume = usrControl.cria_nume
                    item.clie_fanta = usrControl.clie_fanta
                    item.tram_vc_cria_id = Convert.ToInt32(usrCriaVend_aux.Valor)
                    item.tram_vc_clie_id = Convert.ToInt32(usrCriaVend_aux.ClieId)
                End If
                item.tram_vc_porcentaje = 0

                objVendedores(index) = item

                Session("objVendedores") = objVendedores
                gridVendedores.DataSource = objVendedores
                gridVendedores.DataBind()

                CambiarEstadoControlesAccionVendedor()
                CambiarEstadoControles()
                CambiarEstadoControlesReservas(True)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Protected Sub btnEliminarVendedor_Click(sender As Object, e As EventArgs) Handles btnEliminarVendedor.Click
            objVendedores = Session("objVendedores")
            Dim idx = objVendedores.FindIndex(Function(x) x.tram_vc_id = hdnTram_vc_id.Value)
            objVendedores.RemoveAt(idx)
            gridVendedores.CurrentPageIndex = 0
            Session("objVendedores") = objVendedores
            gridVendedores.DataSource = objVendedores
            gridVendedores.DataBind()

            CambiarEstadoControlesAccionVendedor()
            CambiarEstadoControles()
            CambiarEstadoControlesReservas(True)

        End Sub

        Protected Sub btnCancelarVendedor_Click(sender As Object, e As EventArgs) Handles btnCancelarVendedor.Click
            CambiarEstadoControlesAccionVendedor()
            CambiarEstadoControles()
            CambiarEstadoControlesReservas(True)
        End Sub

        Private Sub LimpiarGridVendedores()
            Dim dt As New DataTable
            dt.Columns.Add("tram_vc_id")
            dt.Columns.Add("estado")
            dt.Columns.Add("tram_vc_clie_id")
            dt.Columns.Add("tram_vc_cria_id")
            dt.Columns.Add("raza_codi_desc")
            dt.Columns.Add("cria_nume")
            dt.Columns.Add("clie_fanta")
            gridVendedores.DataSource = dt
            gridVendedores.DataBind()
        End Sub

        Private Sub CambiarEstadoControlesAccionVendedor()
            gridVendedores.Enabled = True
            gridCompradores.Enabled = True
            btnAgregarVendedor.Enabled = True
            btnModificarVendedor.Enabled = False
            btnEliminarVendedor.Enabled = False
            btnCancelarVendedor.Enabled = False

            tdUsrCriaComp_aux.Disabled = False
            txtPorcentajeCompra.Enabled = True
            btnAgregarComprador.Enabled = True
            tbReservas.Disabled = False
            txtObservaciones.Enabled = True

            If hdnOperacion.Value = "A" Then
                tdProductoTransferir.Disabled = False
            End If

        End Sub

#End Region

#Region "GRID COMPRADORES"

        Protected Sub gridCompradores_UpdateCommand(source As Object, e As DataGridCommandEventArgs)
            Dim tram_vc_id = e.Item.Cells(1).Text
            hdnTram_vc_id.Value = e.Item.Cells(1).Text
            hdnTram_vc_clie_id.Value = e.Item.Cells(3).Text
            hdnTram_vc_cria_id.Value = e.Item.Cells(4).Text
            hdnPorcCompra.Value = e.Item.Cells(8).Text
            usrCriaComp_aux.Valor = hdnTram_vc_cria_id.Value

            objCompradores = Session("objCompradores")
            Dim idx = objCompradores.FindIndex(Function(x) x.tram_vc_id = hdnTram_vc_id.Value)
            Dim comprador = objCompradores(idx)

            txtPorcentajeCompra.Text = comprador.tram_vc_porcentaje.ToString()

            gridVendedores.Enabled = False
            gridCompradores.Enabled = False
            btnAgregarComprador.Enabled = False
            btnModificarComprador.Enabled = True
            btnEliminarComprador.Enabled = True
            btnCancelarComprador.Enabled = True

            tdUsrCriaVend_aux.Disabled = True
            btnAgregarVendedor.Enabled = False

            CambiarEstadoControlesReservas(False)

            txtObservaciones.Enabled = False

            ScriptManager.RegisterStartupScript(Page, Page.GetType, "dcpat", "deshabilitarCtrolProductoATransferir();", True)
        End Sub

        Protected Sub gridCompradores_PageIndexChanged(source As Object, e As DataGridPageChangedEventArgs)
            Try
                gridCompradores.EditItemIndex = -1
                If (gridCompradores.CurrentPageIndex < 0 Or gridCompradores.CurrentPageIndex >= gridCompradores.PageCount) Then
                    gridCompradores.CurrentPageIndex = 0
                Else
                    gridCompradores.CurrentPageIndex = e.NewPageIndex
                End If

                objCompradores = Session("objCompradores")
                gridCompradores.DataSource = objCompradores
                gridCompradores.DataBind()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Protected Sub btnAgregarComprador_Click(sender As Object, e As EventArgs) Handles btnAgregarComprador.Click
            Try
                Dim usrControl As New DatosUsrCtrolCLIEEntity
                usrControl = DatosUsrCtrolCLIEBusiness.GetDatosUsrCtrolCLIE(usrCriaComp_aux.Valor)

                objCompradores = Session("objCompradores")
                objCompradores.Add(New TramitesVendedoresCompradoresEntity() With {
                                   .tram_vc_id = GetNroAleatorio(),
                                   .tram_vc_tram_audi_fecha = Date.Now,
                                   .tram_vc_tram_audi_user = 1,
                                   .tram_vc_tram_baja_fecha = Nothing,
                                   .tram_vc_clie_id = Convert.ToInt32(usrCriaComp_aux.ClieId),
                                   .tram_vc_cria_id = Convert.ToInt32(usrCriaComp_aux.Valor),
                                   .tram_vc_porcentaje = Convert.ToInt32(txtPorcentajeCompra.Text),
                                   .tram_vc_tran_tipo = "C",
                                   .tram_vc_obser = String.Empty,
                                    .raza_codi_desc = usrControl.raza_codi_desc,
                                   .cria_nume = usrControl.cria_nume,
                                   .clie_fanta = usrControl.clie_fanta,
                                   .estado = String.Empty})

                Session("objCompradores") = objCompradores
                gridCompradores.DataSource = objCompradores
                gridCompradores.DataBind()

                txtPorcentajeCompra.Text = String.Empty

                CambiarEstadoControles()
                CambiarEstadoControlesReservas(True)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Protected Sub btnModificarComprador_Click(sender As Object, e As EventArgs) Handles btnModificarComprador.Click
            Try
                objCompradores = Session("objCompradores")

                Dim item = objCompradores.Find(Function(x) x.tram_vc_id = Convert.ToInt32(hdnTram_vc_id.Value))
                Dim index = objCompradores.IndexOf(item)

                If item.tram_vc_cria_id <> Convert.ToInt32(usrCriaComp_aux.Valor) Or item.tram_vc_clie_id <> Convert.ToInt32(usrCriaComp_aux.ClieId) Then
                    ' Buscar datos en db.
                    Dim usrControl As New DatosUsrCtrolCLIEEntity
                    usrControl = DatosUsrCtrolCLIEBusiness.GetDatosUsrCtrolCLIE(usrCriaComp_aux.Valor)
                    item.raza_codi_desc = usrControl.raza_codi_desc
                    item.cria_nume = usrControl.cria_nume
                    item.clie_fanta = usrControl.clie_fanta
                    item.tram_vc_cria_id = Convert.ToInt32(usrCriaComp_aux.Valor)
                    item.tram_vc_clie_id = Convert.ToInt32(usrCriaComp_aux.ClieId)
                End If
                item.tram_vc_porcentaje = Convert.ToInt32(txtPorcentajeCompra.Text)

                objCompradores(index) = item

                Session("objCompradores") = objCompradores
                gridCompradores.DataSource = objCompradores
                gridCompradores.DataBind()

                CambiarEstadoControlesAccionComprador()
                CambiarEstadoControles()
                CambiarEstadoControlesReservas(True)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Protected Sub btnEliminarComprador_Click(sender As Object, e As EventArgs) Handles btnEliminarComprador.Click
            objCompradores = Session("objCompradores")
            Dim idx = objCompradores.FindIndex(Function(x) x.tram_vc_id = hdnTram_vc_id.Value)
            objCompradores.RemoveAt(idx)
            gridCompradores.CurrentPageIndex = 0
            Session("objCompradores") = objCompradores
            gridCompradores.DataSource = objCompradores
            gridCompradores.DataBind()
            hdnSumaPorcentajeCompra.Value = Convert.ToInt32(hdnSumaPorcentajeCompra.Value) - Convert.ToInt32(hdnPorcCompra.Value)

            CambiarEstadoControlesAccionComprador()
            CambiarEstadoControles()
            CambiarEstadoControlesReservas(True)
        End Sub

        Protected Sub btnCancelarComprador_Click(sender As Object, e As EventArgs) Handles btnCancelarComprador.Click
            CambiarEstadoControlesAccionComprador()
            CambiarEstadoControles()
            CambiarEstadoControlesReservas(True)
        End Sub

        Private Sub LimpiarGridCompradores()
            Dim dt As New DataTable
            dt.Columns.Add("tram_vc_id")
            dt.Columns.Add("estado")
            dt.Columns.Add("tram_vc_clie_id")
            dt.Columns.Add("tram_vc_cria_id")
            dt.Columns.Add("raza_codi_desc")
            dt.Columns.Add("cria_nume")
            dt.Columns.Add("clie_fanta")
            dt.Columns.Add("tram_vc_porcentaje")
            gridCompradores.DataSource = dt
            gridCompradores.DataBind()
        End Sub

        Private Sub CambiarEstadoControlesAccionComprador()
            gridVendedores.Enabled = True
            gridCompradores.Enabled = True
            btnAgregarComprador.Enabled = True
            btnModificarComprador.Enabled = False
            btnEliminarComprador.Enabled = False
            btnCancelarComprador.Enabled = False
            txtPorcentajeCompra.Text = String.Empty

            tdUsrCriaVend_aux.Disabled = False
            btnAgregarVendedor.Enabled = True
            tbReservas.Disabled = False
            txtObservaciones.Enabled = True
        End Sub

#End Region

#Region "OBTENER NRO ALEATORIO"

        Private Function GetNroAleatorio() As Integer
            Dim random As New Random()
            Dim idRandom = random.Next + 1
            Return idRandom
        End Function

#End Region

#Region "LIMPIAR GRILLAS CUANDO CAMBIA DE PRODUCTO"

        Protected Sub btnLimpiarGrids_Click(sender As Object, e As EventArgs) Handles btnLimpiarGrids.Click
            objVendedores = New List(Of TramitesVendedoresCompradoresEntity)

            Dim id = usrProd_aux.Valor

            'Dim fEcha As Date
            'fEcha = Convert.ToDateTime("2021-01-01")

            'Dim dato = usrProd_aux.txtSraNumeExt.Text
            'objVendedores = TramitesVendedoresCompradoresBusiness.ConsTransferenciaPorductoPropietariosQueVenden(5154879, Nothing, 1)

            objCompradores = New List(Of TramitesVendedoresCompradoresEntity)
            Session("objVendedores") = objVendedores
            Session("objCompradores") = objCompradores
            gridVendedores.DataSource = objVendedores
            gridVendedores.DataBind()
            gridCompradores.DataSource = objCompradores
            gridCompradores.DataBind()
            hdnGridVendedoresCount.Value = objVendedores.Count
            hdnGridCompradoresCount.Value = objCompradores.Count
            hdnSumaPorcentajeCompra.Value = objCompradores.Sum(Function(x) x.tram_vc_porcentaje)
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "_setearCmbRazaCriaVendedorComprador", "setearCmbRazaCriaVendedorComprador();", True)
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "_limpiarCtrolPropductoATransferir", "limpiarCtrolPropductoATransferir();", True)



        End Sub

#End Region

#Region "CAMBIAR ESTADO DE CONTROLES SEGUN SEA UN ALTA O MODIFICACION DE UN TRAMITE"

        Private Sub CambiarEstadoControles()
            ScriptManager.RegisterStartupScript(Page, Page.GetType, "scrcvc", "setearCmbRazaCriaVendedorComprador();", True)
            If hdnOperacion.Value = "M" Then
                ScriptManager.RegisterStartupScript(Page, Page.GetType, "dcpat", "deshabilitarCtrolProductoATransferir();", True)
            End If
        End Sub

#End Region

#Region "CAMBIAR ESTADO DE CONTROLES DEL SECTOR RESERVAS"

        Private Sub CambiarEstadoControlesReservas(ByVal habilitar As Boolean)
            chkReservaSemen.Disabled = IIf(habilitar, False, True)
            txtCantReservaSemen.Enabled = IIf(habilitar, True, False)
            chkReservaCrias.Disabled = IIf(habilitar, False, True)
            txtCantCriasReser.Enabled = IIf(habilitar, True, False)
            chkReservaEmbriones.Disabled = IIf(habilitar, False, True)
            txtCantReservaEmbriones.Enabled = IIf(habilitar, True, False)
            chkReservaVientre.Disabled = IIf(habilitar, False, True)
            chkReservaMaterialGenetico.Disabled = IIf(habilitar, False, True)
            chkReservaDerechoClonar.Disabled = IIf(habilitar, False, True)
            chkAutorizaCompradorClonar.Disabled = IIf(habilitar, False, True)
            If habilitar Then
                ScriptManager.RegisterStartupScript(Page, Page.GetType, "setControlesSegunSexo", "setControlesSegunSexo(false);", True)
            End If
        End Sub

#End Region

        Protected Sub btnBuscarPropietariosQueVenden_Click(sender As Object, e As EventArgs) Handles btnBuscarPropietariosQueVenden.Click
            objVendedores = New List(Of TramitesVendedoresCompradoresEntity)

            Dim id = usrProd_aux.Valor

            Dim fecha As Date
            fecha = Convert.ToDateTime(txtFechaTransferencia_aux.Text).ToShortDateString

            Dim dato = usrProd_aux.txtSraNumeExt.Text
            objVendedores = TramitesVendedoresCompradoresBusiness.ConsTransferenciaProductoPropietariosQueVenden(id, fecha, 1)
            gridVendedores.DataSource = objVendedores
            gridVendedores.DataBind()

            Session("objVendedores") = objVendedores

        End Sub

    End Class

End Namespace
