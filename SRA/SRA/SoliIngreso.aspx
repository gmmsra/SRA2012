<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SoliIngreso" CodeFile="SoliIngreso.aspx.vb" %>

<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Solicitud de Ingreso</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultgrupntScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="stylesheet/sra.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="includes/utiles.js"></script>
    <script language="JavaScript" src="includes/paneles.js"></script>
    <script language="JavaScript">
        function usrClie_onchange() {
            if (document.all("usrClie:txtId").value == '') {
                document.all('txtNaci').value = '';
                document.all('txtEman').value = '';
            }
            else {
                var sFiltro = document.all("usrClie:txtId").value;
                var vsRet;
                vsRet = EjecutarMetodoXML("Utiles.DatosIngreSocio", sFiltro).split(";");
                document.all('txtNaci').value = vsRet[0];
                document.all('txtEman').value = vsRet[1];
                document.all('hdnPeti').value = vsRet[2];
                document.all('txtNaci').disabled = (document.all('hdnPeti').value == 'J');
                document.all('txtEman').disabled = (document.all('hdnPeti').value == 'J');
                vsRet = LeerCamposXML("clientes_entidades_pertenece", document.all("usrClie:txtId").value, "enti").split("|");
                document.all('lblEntid').innerText = vsRet
                if (document.all('hdnPeti').value == 'J') {
                    document.all('panNaci').style.display = 'none';
                    document.all('panEman').style.display = 'none';
                }
                else {
                    document.all('panNaci').style.display = '';
                    document.all('panEman').style.display = '';
                }
            }
        }

        function txtPresFecha_Change() {
            sFiltro = document.all("txtPresFecha").value;
            document.all("txtVigeFecha").value = EjecutarMetodoXML("Utiles.ObtenerFechaVigencia", sFiltro);
        }
        function mCobranzaAbrir() {
            gAbrirVentanas("reportes/SociosCobranza_pop.aspx?soin_id=" + document.all("hdnId").value + " &socm_id=", 1, "750", "300");
        }
        function usrTituClie_onchange() {
            var lstrClie = document.all('usrTituClie:txtId').value;
            document.all('usrTituSoci:txtId').value = '';
            document.all('usrTituSoci:txtCodi').value = '';
            document.all('usrTituSoci:txtApel').value = '';
        }

        function usrTituSoci_onchange() {
            var lstrSoci = document.all('usrTituSoci:txtId').value;
            document.all('usrTituClie:txtId').value = '';
            document.all('usrTituClie:txtCodi').value = '';
            document.all('usrTituClie:txtApel').value = '';
        }

        function cmbCate_Change() {
            var vstrRet;
            if (document.all("cmbCate").value != "")
                vstrRet = LeerCamposXML("categorias", document.all("cmbCate").value, "cate_dist,cate_titu").split("|");

            if (vstrRet != undefined) {
                gHabilitarControl(document.all("cmbDist"), vstrRet[0] == "1");
                gHabilitarControl(document.all("usrTituClie:txtCodi"), vstrRet[1] == "0");
                gHabilitarControl(document.all("usrTituClie:txtApel"), vstrRet[1] == "0");
                gHabilitarControl(document.all("usrTituSoci:txtCodi"), vstrRet[1] == "0");
                gHabilitarControl(document.all("usrTituSoci:txtApel"), vstrRet[1] == "0");

                document.all("usrTituClie_imgBusc").disabled = vstrRet[1] == "1";
                document.all("usrTituClie_imgBuscAvan").disabled = vstrRet[1] == "1";
                document.all("usrTituSoci_imgBusc").disabled = vstrRet[1] == "1";
                document.all("usrTituSoci_imgBuscAvan").disabled = vstrRet[1] == "1";
            }
        }
        function mConsultarEstadoAnterior() {
            var sFiltro = document.all("txtNroSociAnt").value;
            var vstrRet;

            if (sFiltro != '') {
                sFiltro = "@soci_nume=" + sFiltro;
                var vstrRet = LeerCamposXML("socio_ultimo_estado", sFiltro, "ulti_esta,estado,habilita").split("|");

                if (vstrRet[0] == "") {
                    alert("El numero no corresponde a un socio dado de baja");
                    document.all('txtNroSociAnt').value = ''
                }
                else {
                    document.all('hdnEstaUlti').innerText = vstrRet[0];
                    document.all('lblEstaUlti').innerText = vstrRet[1];
                    if (vstrRet[2] == "0") {
                        document.all('txtMoti').disabled = false;
                    }
                }
            }
        }
    </script>
</head>
<body class="pagina" leftmargin="5" topmargin="5" onload="gSetearTituloFrame('');" rightmargin="0">
    <form id="frmABM" method="post" runat="server">
        <!------------------ RECUADRO ------------------->
        <table cellspacing="0" cellpadding="0" width="97%" align="center" border="0">
            <tr>
                <td width="9">
                    <img height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recsup.jpg">
                    <img height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
                <td width="13">
                    <img height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9" background="imagenes/reciz.jpg">
                    <img height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table id="Table1" style="width: 100%; height: 130px" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td width="100%" colspan="3"></td>
                        </tr>
                        <tr>
                            <td style="height: 25px" valign="bottom" colspan="3" height="25">
                                <asp:Label ID="lblTituAbm" runat="server" CssClass="opcion" Width="391px">Solicitud de Ingreso</asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">
                                <asp:Panel ID="panFiltro" runat="server" CssClass="titulo" BorderStyle="Solid" BorderWidth="0"
                                    Width="100%" Visible="True">
                                    <table id="TableFil" style="width: 100%" cellspacing="0" cellpadding="0" align="left" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="height: 8px" width="24"></td>
                                                        <td style="height: 8px" width="42"></td>
                                                        <td style="height: 8px" width="26"></td>
                                                        <td style="height: 8px"></td>
                                                        <td style="height: 8px" width="26">
                                                            <cc1:BotonImagen ID="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
                                                                ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
                                                                ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></cc1:BotonImagen></td>
                                                        <td style="height: 8px" width="26"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px" width="24">
                                                            <img height="25" src="imagenes/formfle.jpg" width="24" border="0"></td>
                                                        <td style="height: 8px" width="42">
                                                            <img height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></td>
                                                        <td style="height: 8px" width="26">
                                                            <img height="25" src="imagenes/formcap.jpg" width="26" border="0"></td>
                                                        <td style="height: 8px" background="imagenes/formfdocap.jpg" colspan="3">
                                                            <img height="25" src="imagenes/formfdocap.jpg" border="0"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td width="3" background="imagenes/formiz.jpg">
                                                            <img height="30" src="imagenes/formiz.jpg" width="3" border="0"></td>
                                                        <td width="100%">
                                                            <!-- FOMULARIO -->
                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td style="height: 10px" align="right" background="imagenes/formfdofields.jpg" colspan="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" background="imagenes/formfdofields.jpg">
                                                                        <asp:Label ID="lblNumeFil" runat="server" CssClass="titulo">Nro.Solicitud:</asp:Label>&nbsp;</td>
                                                                    <td background="imagenes/formfdofields.jpg">
                                                                        <cc1:NumberBox ID="txtNumeFil" runat="server" CssClass="cuadrotexto" Width="80px" AceptaNull="False"></cc1:NumberBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                        <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%; height: 10px" align="right" background="imagenes/formfdofields.jpg">
                                                                        <asp:Label ID="lblFechaDesdeFil" runat="server" CssClass="titulo">Fecha Desde:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 85%; height: 17px" background="imagenes/formfdofields.jpg">
                                                                        <cc1:DateBox ID="txtFechaDesdeFil" runat="server" Width="68px" CssClass="cuadrotexto"></cc1:DateBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                        <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%; height: 10px" align="right" background="imagenes/formfdofields.jpg">
                                                                        <asp:Label ID="lblFechaHastaFil" runat="server" CssClass="titulo">Fecha Hasta:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 85%; height: 17px" background="imagenes/formfdofields.jpg">
                                                                        <cc1:DateBox ID="txtFechaHastaFil" runat="server" CssClass="cuadrotexto" Width="68px"></cc1:DateBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                        <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%; height: 10px" align="right" background="imagenes/formfdofields.jpg">
                                                                        <asp:Label ID="lblApelFil" runat="server" CssClass="titulo">Apellido:</asp:Label>&nbsp;</td>
                                                                    <td style="width: 85%; height: 17px" background="imagenes/formfdofields.jpg">
                                                                        <cc1:TextBoxTab ID="txtApelFil" runat="server" CssClass="cuadrotexto" Width="270" AceptaNull="False"></cc1:TextBoxTab></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 10px" align="right" background="imagenes/formfdofields.jpg" colspan="2"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="2" background="imagenes/formde.jpg">
                                                            <img height="2" src="imagenes/formde.jpg" width="2" border="0"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" height="10"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" colspan="3">
                                <asp:DataGrid ID="grdDato" runat="server" Width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
                                    OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
                                    AllowPaging="True">
                                    <FooterStyle CssClass="footer"></FooterStyle>
                                    <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                    <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                    <HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderStyle Width="9px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="soin_id"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_soin_nume" HeaderText="Nro"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_clie_apel" HeaderText="Apellido"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="soin_pres_fecha" HeaderText="Fecha Pres." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_soin_resu" HeaderText="Aprobada"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="soin_vige_fecha" HeaderText="Fecha Vige." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_cate_desc" HeaderText="Categor&#237;a"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_dist_desc" HeaderText="Distrito"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="soin_ante_soci" HeaderText="soin_ante_soci"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td colspan="3" height="10"></td>
                        </tr>
                        <tr>
                            <td valign="middle" width="100%" colspan="3">&nbsp;
									<cc1:BotonImagen ID="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
                                        ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
                                        ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar una Nueva Solicitud de Ingreso"
                                        ImageDisable="btnNuev0.gif"></cc1:BotonImagen></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <div>
                                    <asp:Panel ID="panDato" runat="server" CssClass="titulo" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                                        Visible="False">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <p align="right">
                                                        <table class="FdoFld" id="Table2" style="width: 100%; height: 106px" cellpadding="0" align="left"
                                                            border="0">
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                                <td height="5">
                                                                    <asp:Label ID="lblTitu" runat="server" Width="100%" CssClass="titulo"></asp:Label></td>
                                                                <td valign="top" align="right">&nbsp;
																		<asp:ImageButton ID="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100%" colspan="3">
                                                                    <asp:Panel ID="panCabecera" runat="server" CssClass="titulo" Width="100%">
                                                                        <table id="TableCabecera" style="width: 100%" cellpadding="0" align="left" border="0">
                                                                            <tr>
                                                                                <td style="width: 213px" nowrap align="right">
                                                                                    <asp:Label ID="lblNume" runat="server" CssClass="titulo">Nro:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <cc1:NumberBox ID="txtNume" runat="server" CssClass="cuadrotexto" Width="90px" MaxLength="8" MaxValor="100000000000"
                                                                                        Obligatorio="true" Enabled="False"></cc1:NumberBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" valign="top" align="right">
                                                                                    <asp:Label ID="lblClie" runat="server" CssClass="titulo">Cliente:</asp:Label>&nbsp;
                                                                                </td>
                                                                                <td width="80%">
                                                                                    <uc1:CLIE ID="usrClie" runat="server" Obligatorio="true" FilCUIT="True" Tabla="Clientes" Saltos="1,1,1"
                                                                                        Ancho="780" Alto="560" FilClie="True" FilDocuNume="True"></uc1:CLIE>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px; height: 18px" valign="top" align="right">
                                                                                    <asp:Label ID="lblEnti" runat="server" CssClass="titulo">Pertenece a enti.:</asp:Label>&nbsp;</td>
                                                                                <td style="height: 18px">
                                                                                    <asp:Label ID="lblEntid" runat="server" CssClass="titulo" Font-Size="10"></asp:Label>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" align="right" colspan="3">
                                                                                    <table id="Table4" cellspacing="0" cellpadding="0" width="80%" border="0">
                                                                                        <tr>
                                                                                            <td style="width: 236px">
                                                                                                <div id="panNaci" style="display: none" nowrap>
                                                                                                    <asp:Label ID="lblNaciFech" runat="server" CssClass="titulo">Fecha Nacimiento:</asp:Label>&nbsp;
																										<cc1:DateBox ID="txtNaci" runat="server" CssClass="cuadrotexto" Width="70px" Obligatorio="False"></cc1:DateBox>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div id="panEman" style="display: none" nowrap>
                                                                                                    <asp:Label ID="lblEmanFecha" runat="server" CssClass="titulo">F.Emancipación:</asp:Label>
                                                                                                    <cc1:DateBox ID="txtEman" runat="server" CssClass="cuadrotexto" Width="70px" Obligatorio="False"></cc1:DateBox>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" align="right">
                                                                                    <asp:Label ID="lblPreFecha" runat="server" CssClass="titulo">Presentación:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                                                                        <tr>
                                                                                            <td style="width: 97px" align="left" width="97">
                                                                                                <cc1:DateBox ID="txtPresFecha" runat="server" CssClass="cuadrotexto" Width="70px" Obligatorio="true"
                                                                                                    OnChange="txtPresFecha_Change();"></cc1:DateBox></td>
                                                                                            <td nowrap align="left" colspan="2">&nbsp;&nbsp;&nbsp;
																									<asp:Label ID="lblVigeFecha" runat="server" CssClass="titulo">Vigencia:</asp:Label>&nbsp;
																									<cc1:DateBox ID="txtVigeFecha" runat="server" CssClass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" align="right">
                                                                                    <asp:Label ID="lblCate" runat="server" CssClass="titulo">Categoría:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <cc1:ComboBox class="combo" ID="cmbCate" runat="server" Width="160px" Obligatorio="true" onchange="cmbCate_Change();"></cc1:ComboBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" align="right">
                                                                                    <asp:Label ID="lblDist" runat="server" CssClass="titulo">Distrito:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <cc1:ComboBox class="combo" ID="cmbDist" runat="server" Width="160px" Obligatorio="true"></cc1:ComboBox></td>
                                                                                            <td style="padding-left: 10px">
                                                                                                <asp:Label ID="llbDirePcia" runat="server" CssClass="titulo">Provincia:</asp:Label></td>
                                                                                            <td>
                                                                                                <cc1:ComboBox ID="cmbDirePcia" class="combo" runat="server" Width="260px" NomOper="provincias_cargar" onchange="mCargarLocalidades('')"></cc1:ComboBox></td>
                                                                                            <td style="padding-left: 10px">
                                                                                                <asp:Label ID="lblDireLoca" runat="server" CssClass="titulo">Localidad:</asp:Label></td>
                                                                                            <td>
                                                                                                <cc1:ComboBox ID="cmbDireLoca" class="combo" runat="server" Width="260px" NomOper="localidades_cargar"></cc1:ComboBox></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" valign="top" nowrap align="right">
                                                                                    <asp:Label ID="lblPresSoci" runat="server" CssClass="titulo">Socio Presentante:</asp:Label>&nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    <uc1:CLIE ID="usrPresSoci" runat="server" Obligatorio="True" Tabla="Socios" Saltos="1,1,2"
                                                                                        Ancho="800" FilDocuNume="True" CampoVal="Socio Presentante" MuestraDesc="False" FilClieNume="True"></uc1:CLIE>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" valign="middle" align="right" rowspan="2">
                                                                                    <asp:Label ID="lblTituSoci" runat="server" CssClass="titulo">Titular:</asp:Label>&nbsp;
                                                                                </td>
                                                                                <td nowrap>
                                                                                    <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td nowrap align="right" width="60">
                                                                                                <asp:Label ID="lblTituClieId" runat="server" CssClass="titulo">Cliente:</asp:Label>&nbsp;</td>
                                                                                            <td>
                                                                                                <uc1:CLIE ID="usrTituClie" runat="server" Obligatorio="False" Tabla="Clientes" Saltos="1,1,2"
                                                                                                    Ancho="800" FilDocuNume="True" CampoVal="Cliente Titular" MuestraDesc="False" FilClieNume="True"
                                                                                                    CateTitu="1"></uc1:CLIE>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="middle" nowrap>
                                                                                    <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                        <tr>
                                                                                            <td align="right" width="60">
                                                                                                <asp:Label ID="lblTituSociId" runat="server" CssClass="titulo">Socio:</asp:Label>&nbsp;</td>
                                                                                            <td>
                                                                                                <uc1:CLIE ID="usrTituSoci" runat="server" Obligatorio="False" Tabla="Socios" Saltos="1,1,2"
                                                                                                    Ancho="800" FilDocuNume="True" CampoVal="Socio Titular" MuestraDesc="False" FilClieNume="True"></uc1:CLIE>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" valign="middle" align="right">
                                                                                    <asp:Label ID="Label1" runat="server" CssClass="titulo">Nro Socio Ant.:</asp:Label>&nbsp;</td>
                                                                                <td>
                                                                                    <table border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <cc1:NumberBox ID="txtNroSociAnt" runat="server" CssClass="cuadrotexto" Width="90px" MaxLength="8"
                                                                                                    MaxValor="100000000000" onchange="javascript:mConsultarEstadoAnterior()"></cc1:NumberBox></td>
                                                                                            <td align="right" width="110">
                                                                                                <asp:Label ID="Label3" runat="server" CssClass="titulo">Motivo de Baja:</asp:Label>&nbsp;</td>
                                                                                            <td valign="baseline" align="left" width="110">
                                                                                                <asp:Label ID="lblEstaUlti" runat="server" CssClass="titulo" Width="110px"></asp:Label>&nbsp;</td>
                                                                                            <td valign="middle" align="right">
                                                                                                <asp:Label ID="Label2" runat="server" CssClass="titulo">Antecedentes:</asp:Label>&nbsp;</td>
                                                                                            <td>
                                                                                                <cc1:TextBoxTab ID="txtMoti" runat="server" CssClass="cuadrotexto" Visible="True" Width="376px"
                                                                                                    Height="50px" EnterPorTab="False" TextMode="MultiLine" Enabled="False"></cc1:TextBoxTab></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <tr>
                                                                                    <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                        <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                                </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" valign="top" align="right">
                                                                                    <asp:Label ID="lblEstaTit" runat="server" CssClass="titulo">Estado:</asp:Label>&nbsp;</td>
                                                                                <td width="60">
                                                                                    <asp:Label ID="lblEsta" runat="server" CssClass="titulo" Width="512px"></asp:Label>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" background="imagenes/formdivmed.jpg" colspan="3" height="2">
                                                                                    <img height="2" src="imagenes/formdivmed.jpg" width="1"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 213px" valign="top" align="right">
                                                                                    <asp:Label ID="lblSociTit" runat="server" CssClass="titulo">Socio Generado:</asp:Label>&nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblSoci" runat="server" CssClass="titulo" Font-Size="10"></asp:Label>&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="panBotones" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblBaja" runat="server" CssClass="titulo" ForeColor="Red"></asp:Label></td>
                                            </tr>
                                            <tr height="30">
                                                <td align="center"><a id="editar" name="editar"></a>
                                                    <asp:Button ID="btnAlta" runat="server" CssClass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button ID="btnBaja" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnModi" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnLimp" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Limpiar"></asp:Button>&nbsp;&nbsp;
                                                    <button class="boton" id="btnCobra" style="width: 80px" onclick="mCobranzaAbrir();" type="button"
                                                                runat="server" value="Deuda">
                                                                Cobranza</button></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!--- FIN CONTENIDO --->
                </td>
                <td width="13" background="imagenes/recde.jpg">
                    <img height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
            </tr>
            <tr>
                <td width="9">
                    <img height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
                <td background="imagenes/recinf.jpg">
                    <img height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
                <td width="13">
                    <img height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
            </tr>
        </table>
        <!----------------- FIN RECUADRO ----------------->
        <div style="display: none">
            <asp:TextBox ID="lblMens" runat="server"></asp:TextBox><asp:TextBox ID="hdnAltaId" runat="server"></asp:TextBox><asp:TextBox ID="hdnId" runat="server"></asp:TextBox><asp:TextBox ID="hdnPeti" runat="server" BorderStyle="Solid" BorderWidth="1px" ForeColor="#FF8080"></asp:TextBox><asp:TextBox ID="hdnEstaUlti" runat="server"></asp:TextBox></div>
    </form>
    <script type="text/jscript" language="javascript">

        if (document.all["editar"] != null)
            document.location = '#editar';

        if (document.all["txtNume"] != null && !document.all["txtNume"].disabled)
            document.all["txtNume"].focus();

        if (document.all('txtNaci') != null) {
            if (document.all('txtPresFecha').disabled) {
                document.all('txtNaci').disabled = true;
                document.all('txtEman').disabled = true;
            }
            else {
                document.all('txtNaci').disabled = (document.all('hdnPeti').value == 'J');
                document.all('txtEman').disabled = (document.all('hdnPeti').value == 'J');
            }
            if (document.all('hdnPeti').value == 'J') {
                document.all('panNaci').style.display = 'none';
                document.all('panEman').style.display = 'none';
            }
            else {
                document.all('panNaci').style.display = '';
                document.all('panEman').style.display = '';
            }
        }

        if (document.all["hdnAltaId"].value != '') {
            alert(document.all["hdnAltaId"].value);
            document.all["hdnAltaId"].value = '';
        }

        function mCargarLocalidades(pstrTipo) {
            var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePcia").value
            LoadComboXML("localidades_cargar", sFiltro, "cmb" + pstrTipo + "DireLoca", "S");
        }

        //function mSelecCP(pstrTipo) {
        //    document.all["txt" + pstrTipo + "DireCP"].value = LeerCamposXML("localidades", document.all["cmb" + pstrTipo + "DireLoca"].value, "loca_cpos");
        //}

    </script>
</body>
</html>
