

Namespace SRA

Partial Class CentrosEmisores
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"

   Public mstrCmd As String
   Public mstrTabla As String = "emisores_ctros"
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mConsultar()
            mCargarCombos()
            mSetearMaxLength()
            mSetearEventos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtCodiNum.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "emct_codi")
      txtDescripcion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "emct_desc")
      txtCantDupl.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "emct_dupl")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarComboBool(Me.cmbCasaCentral, "S")
      clsWeb.gCargarComboBool(Me.cmbCobranzaDirecta, "S")
      clsWeb.gCargarComboBool(Me.cmbDatosFact, "S")
   End Sub

   Private Sub mEstablecerPerfil()
      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Usuarios, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If
      btnAlta.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      btnLimp.Visible = (btnAlta.Visible Or btnBaja.Visible Or btnModi.Visible)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
         clsWeb.gCargarDataGrid((mstrConn), mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = E.Item.Cells(1).Text
      mCargarDatos()
      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtDescripcion.Text = ""
      txtCodiNum.Text = ""
      txtCantDupl.Text = ""
      cmbCobranzaDirecta.Limpiar()
      cmbCasaCentral.Limpiar()
      cmbDatosFact.Limpiar()
      mSetearEditor(True)
      txtBajaFecha.Text = ""
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      mEstablecerPerfil()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Alta()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("emct_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("emct_codi") = txtCodiNum.Text
         .Item("emct_desc") = txtDescripcion.Text
         .Item("emct_cobra_dire") = cmbCobranzaDirecta.ValorBool
         .Item("emct_central") = cmbCasaCentral.ValorBool
         .Item("emct_datos_fact") = cmbDatosFact.ValorBool
                .Item("emct_dupl") = txtCantDupl.Valor
                If (txtBajaFecha.Fecha.ToString.Length > 0) Then
                    .Item("emct_baja_fecha") = txtBajaFecha.Fecha
                End If
      End With

      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If txtCantDupl.Valor.ToString = "0" Then
         Throw New AccesoBD.clsErrNeg("La cantidad de duplicados debe ser mayor a 0.")
      End If
   End Sub

   Private Function mCargarDatos() As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         txtCodiNum.Text = .Item("emct_codi")
         txtDescripcion.Text = .Item("emct_desc")
         cmbCobranzaDirecta.ValorBool = .Item("emct_cobra_dire")
         cmbCasaCentral.ValorBool = .Item("emct_central")
         cmbDatosFact.ValorBool = .Item("emct_datos_fact")
                txtCantDupl.Valor = .Item("emct_dupl")
                If (.Item("emct_baja_fecha").ToString.Length > 0) Then
                    txtBajaFecha.Fecha = .Item("emct_baja_fecha")
                End If
      End With
      Return ldsEsta
   End Function
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
End Class
End Namespace
