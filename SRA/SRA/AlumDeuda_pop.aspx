<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AlumDeuda_pop" CodeFile="AlumDeuda_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Consulta de Deuda</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR height="100%">
					<TD vAlign="top" colspan="6"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px"
							PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
							BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
									<ItemTemplate>
										<asp:CheckBox ID="chkSel" Runat="server" onclick="mCalcularTotales();//chkSelCheck(this);"></asp:CheckBox>
										<DIV runat="server" id="divTotal" style="DISPLAY: none"><%#(100*DataBinder.Eval(Container, "DataItem.comp_neto"))%></DIV>
										<DIV runat="server" id="divInte" style="DISPLAY: none"><%#(100*DataBinder.Eval(Container, "DataItem.interes"))%></DIV>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="False" DataField="comp_id"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="selec_ini"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="anio"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="perio"></asp:BoundColumn>
								<asp:BoundColumn DataField="coco_desc_ampl" HeaderText="Concepto"></asp:BoundColumn>
								<asp:BoundColumn DataField="comp_neto" HeaderText="Importe"></asp:BoundColumn>
								<asp:BoundColumn DataField="interes" HeaderText="Interes"></asp:BoundColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="center" width="100%" colspan="6">
						<asp:Label id="lblTotal" runat="server" cssclass="titulo">Total Deuda Alumno:</asp:Label>&nbsp;
						<CC1:NUMBERBOX id="txtTotal" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="65px"></CC1:NUMBERBOX>
						<asp:Label id="Label1" runat="server" cssclass="titulo">Total Seleccionado:</asp:Label>&nbsp;
						<CC1:NUMBERBOX id="txtTotalSel" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="65px"></CC1:NUMBERBOX></TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="right" height="40" colspan="6">
						<asp:button id="btnAceptar" Width="80px" cssclass="boton" runat="server" Text="Aceptar"></asp:button>&nbsp;
						<asp:button id="btnCerrar" runat="server" Text="Cerrar" Width="80px" cssclass="boton"></asp:button></TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
	function mCalcularTotales()
	{
		var dTotal=0;
		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
				{
					dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divTotal").innerText.replace(",",""));
					dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText.replace(",",""));
				}
			}
		}
		dTotal = dTotal.toString();
		if (dTotal==0)
			document.all("txtTotalSel").value = "0.00"
		else
			document.all("txtTotalSel").value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2 , dTotal.length );	
	}
	function chkSelCheck(pChk)
	{
		var dTotal=0;
		var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
		var lbooChecked = pChk.checked;

		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				//if (fila>lintFil)
					//document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked = false;
				
				//if((lbooChecked)&&(fila<=lintFil))
					//document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked = lbooChecked;


				if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
				{
					dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divTotal").innerText.replace(",",""));
					dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText.replace(",",""));
				}
			}
		}

		dTotal = dTotal.toString();

		if (dTotal==0)
			document.all("txtTotalSel").value = "0.00"
		else
			document.all("txtTotalSel").value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2 , dTotal.length );
	}
	mCalcularTotales();
		</SCRIPT>
	</BODY>
</HTML>
