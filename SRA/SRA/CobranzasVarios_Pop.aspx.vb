Namespace SRA

Partial Class CobranzasVarios_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
   Private mstrTablaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep

   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mdsDatos = Session(Request("sess"))
         hdnClieId.Text = Request("clie")

         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mSetearMaxLength()
            mCargarCombos()

            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
			Else
				mCargarCentroCostos(False)
				mSetearControles()
			End If

		Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearControles()
      btnAlta.Attributes.Add("onclick", "return GrabarUnaVez(this);")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaConcep)
      txtImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "coco_impo")
      txtDescAmpliConcep.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "coco_desc_ampl")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConc, "S", "@conc_reci=1")
      clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCcos, "N")
	End Sub


	Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
		Dim lstrCta As String = ""
            If (cmbConc.Text.Trim().Length > 0) Then
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbConc.Valor)
                lstrCta = lstrCta.Substring(0, 1)
            End If
            If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
			If Not Request.Form("cmbCCos") Is Nothing Or pbooEdit Then
				clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCcos, "id", "descrip", "N")
				If Not Request.Form("cmbCCos") Is Nothing Then
					cmbCcos.Valor = Request.Form("cmbCCos").ToString
				End If
			End If
		Else
			cmbCcos.Items.Clear()
		End If
	End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      With mdsDatos.Tables(mstrTablaConcep).Select("coco_id=" & pstrId)(0)
         hdnId.Text = .Item("coco_id").ToString()

			cmbConc.Valor = .Item("coco_conc_id")
			mCargarCentroCostos(True)
			If .Item("coco_ccos_id") Is DBNull.Value Then
				cmbCcos.Enabled = False
			Else
				cmbCcos.Valor = .Item("coco_ccos_id")
			End If
            txtImpo.Valor = .Item("coco_impo")
            txtDescAmpliConcep.Valor = .Item("coco_desc_ampl")

		End With

      mSetearEditor("", False)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      cmbConc.Limpiar()
      cmbCcos.Limpiar()
      txtImpo.Text = ""
      txtDescAmpliConcep.Text = ""

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mActualizar()
      Try
         mGuardarDatos()

         mConsultar()

         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mBaja()
      Try
         mdsDatos.Tables(mstrTablaConcep).Select("coco_id=" & hdnId.Text)(0).Delete()

         mConsultar()

         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatos()
      mValidarDatos()

      Dim ldrDatos As DataRow

      If hdnId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrTablaConcep).NewRow
         ldrDatos.Item("coco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaConcep), "coco_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrTablaConcep).Select("coco_id=" & hdnId.Text)(0)
      End If

      With ldrDatos
         .Item("coco_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                .Item("coco_conc_id") = cmbConc.Valor
                If (cmbCcos.Text.Trim().Length > 0) Then
                    .Item("coco_ccos_id") = cmbCcos.Valor
                Else
                    .Item("coco_ccos_id") = DBNull.Value
                End If

                .Item("coco_impo") = txtImpo.Valor
         .Item("coco_impo_ivai") = .Item("coco_impo")
         .Item("_desc") = cmbConc.SelectedItem.Text
         .Item("coco_desc_ampl") = txtDescAmpliConcep.Valor

                If (cmbCcos.Text.Trim().Length > 0) Then
                    .Item("_desc") += cmbCcos.SelectedItem.Text
                End If

                If hdnId.Text = "" Then
            .Table.Rows.Add(ldrDatos)
         End If
      End With
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If cmbCcos.Valor Is DBNull.Value Then
         If clsSQLServer.gObtenerEstruc(mstrConn, "conceptosX", cmbConc.Valor.ToString).Tables(0).Rows(0).Item("cta_resu").ToString() = "1" Then
            Throw New AccesoBD.clsErrNeg("La cuenta del concepto es de resultado, debe indicar el centro de costo.")
         End If
      End If
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
		Try
			cmbCcos.Enabled = True
			mCargarDatos(e.Item.Cells(1).Text)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
   End Sub
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mActualizar()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mActualizar()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Public Sub mConsultar()
      Try
         mdsDatos.Tables(mstrTablaConcep).DefaultView.Sort = "coco_id DESC"
         grdDato.DataSource = mdsDatos.Tables(mstrTablaConcep).DefaultView
         grdDato.DataBind()

         grdDato.DataSource = mdsDatos.Tables(mstrTablaConcep)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
