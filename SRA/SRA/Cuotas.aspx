<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Cuotas" CodeFile="Cuotas.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cuotas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px" vAlign="bottom" colSpan="3" height="33"><asp:label id="Label4" runat="server" width="391px" cssclass="opcion">Cuotas</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3"><asp:panel id="Panel1" runat="server" cssclass="titulo" visible="True" BorderWidth="0" BorderStyle="Solid"
											Width="100%">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="Botonimagen1" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		ForeColor="Transparent" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif"
																		OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																		OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="Label1" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:NumberBox id="txtAnoFil" runat="server" cssclass="cuadrotexto" Width="64px"></CC1:NumberBox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR> <!-- Fin Filtro -->
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Height="116px">
												<TABLE id="Table2" style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
													<TR>
														<TD vAlign="top" width="100%" colSpan="3"></TD>
													</TR> <!-- Fin Filtro -->
													<TR>
														<TD style="HEIGHT: 15px" align="left" colSpan="3" height="15">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo">A�os</asp:Label></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCuota" runat="server" cssclass="titulo" Width="100%" Visible="True">
																<TABLE id="TableDetalle" cellPadding="0" width="100%" align="left" border="0">
																	<TR>
																		<TD align="left" width="100%" colSpan="3">
																			<asp:datagrid id="grdCuotas" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				AutoGenerateColumns="False" OnEditCommand="mEditarDatos" OnPageIndexChanged="grdCuotas_PageChanged"
																				CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="cues_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_periodo" HeaderText="Periodo">
																						<HeaderStyle Width="30%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cues_deve_fecha" HeaderText="Devengamiento" DataFormatString="{0:dd/MM/yyyy}">
																						<HeaderStyle Width="19%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cues_venc_fecha" HeaderText="Vencimiento" DataFormatString="{0:dd/MM/yyyy}">
																						<HeaderStyle Width="19%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cues_perio_inic_fecha" HeaderText="Inicio" DataFormatString="{0:dd/MM/yyyy}">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cues_perio_fina_fecha" HeaderText="Fin" DataFormatString="{0:dd/MM/yyyy}">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="cues_anio" HeaderText="anio"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="1">
																			<asp:Button id="btnNuevaCuota" runat="server" cssclass="boton" Width="130px" Text="Nuevo periodo >>"></asp:Button></TD>
																		<TD style="WIDTH: 100%" align="right" colSpan="2">
																			<asp:Button id="btnAuto" runat="server" cssclass="boton" Width="130px" Text="Generar nuevo a�o >>"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="4">
																			<asp:panel id="PanDetalle" runat="server" cssclass="titulo" Width="100%" Visible="false">
																				<TABLE class="FdoFld" id="TableaPnDetalle" style="WIDTH: 100%" cellPadding="0" align="left"
																					border="0">
																					<TR>
																						<TD align="right" colSpan="2">
																							<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
																								ToolTip="Cerrar"></asp:ImageButton></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="Label2" runat="server" cssclass="titulo">A�o:</asp:Label></TD>
																						<TD height="5" rowSpan="1">
																							<CC1:NumberBox id="txtAno" runat="server" cssclass="cuadrotexto" Width="64px"></CC1:NumberBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="Label3" runat="server" cssclass="titulo">Tipo de periodo:</asp:Label></TD>
																						<TD height="5">
																							<cc1:combobox class="combo" id="cmbPeriTipo" runat="server" Width="243px" Obligatorio="False"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblAsam" runat="server" cssclass="titulo">Periodo:</asp:Label></TD>
																						<TD>
																							<CC1:NumberBox id="txtPeri" runat="server" cssclass="cuadrotexto" Width="64px"></CC1:NumberBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="Label5" runat="server" cssclass="titulo">Fecha devengado:</asp:Label></TD>
																						<TD align="left">
																							<cc1:DateBox id="txtDeve" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																								AceptaNull="True"></cc1:DateBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblVenc" runat="server" cssclass="titulo">Fecha vencimiento:</asp:Label></TD>
																						<TD height="5">
																							<cc1:DateBox id="txtVenc" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																								AceptaNull="True"></cc1:DateBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="Label6" runat="server" cssclass="titulo">Fecha inicio periodo:</asp:Label></TD>
																						<TD align="left">
																							<cc1:DateBox id="txtPeriInic" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																								AceptaNull="True"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																							<asp:Label id="Label7" runat="server" cssclass="titulo">Fecha fin periodo:</asp:Label>
																							<cc1:DateBox id="txtPeriFin" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="False"
																								AceptaNull="True"></cc1:DateBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="center" colSpan="4" height="30"><A id="editar" name="editar"></A>
																							<asp:Button id="BtnAltaCuota" runat="server" cssclass="boton" Width="90px" Text="Agregar"></asp:Button>&nbsp;&nbsp;
																							<asp:Button id="btnBajaCuota" runat="server" cssclass="boton" Width="90px" Text="Eliminar"></asp:Button>&nbsp;
																							<asp:Button id="btnModiCuota" runat="server" cssclass="boton" Width="90px" Text="Modificar"></asp:Button>&nbsp;
																							<asp:Button id="btnLimpCuota" runat="server" cssclass="boton" Width="90px" Text="Limpiar"></asp:Button></TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server"></ASP:PANEL></DIV>
									</TD>
								</TR>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Visible="True"></ASP:TEXTBOX><asp:textbox id="hdnAnoId" runat="server" Visible="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">	
		function mCargarCombo()
		{
		  LoadComboXML("asambleas_futuras_cargar","","cmbAsam", "S");
		  document.all("cmbAsam").disabled = false;		 				  
		}
		
		function mDescargarCombo()
		{
		  document.all("cmbAsam").innerText = ""	
		  document.all("cmbAsam").disabled = true;		 				  
		}
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
		<DIV></DIV>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
