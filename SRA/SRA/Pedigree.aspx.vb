Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports Business.Facturacion
Imports Entities


Namespace SRA


Partial Class Pedigree
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Public mstrCmd As String
    Public mstrProd As String
    Public mstrRazaId As String
    Private mstrConn As String
    Private mboolSoloConsulta As Boolean = False

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            hdnSoloConsulta.Text = "false"
            mstrConn = clsWeb.gVerificarConexion(Me)
            If Not Page.IsPostBack Then
                mSetearMaxLength()
                mCargarCombos()
            End If
            mInicializar()
            mValiImpre(mstrProd)
            mValiEditarPedigree(mstrProd)
            mArmarArbolPedigree(mstrProd)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
   End Sub
    Private Sub mCargarCombos()
        ' Dario 2013-04-23 se saca de la carga del combo asociasiones a la SRA se agrega @mostrar_sra = 0
        clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra = 0", cmbAsoc, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra = 0", cmbAsocPadre, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra = 0", cmbAsocMadre, "id", "descrip", "S")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrProdLong As Object
        Dim lintCol As Integer

        lstrProdLong = clsSQLServer.gCargarLongitudes(mstrConn, "productos")

        txtRP.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_rp")
        txtRPNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_nomb")
        txtNume.MaxLength = 17 ' Dario 2014-12-09 SRA_Neg.Constantes.LongitudesTipos.Entero

        txtRPPadre.MaxLength = txtRP.MaxLength
        txtRPNumePadre.MaxLength = txtRPNume.MaxLength
        txtNombPadre.MaxLength = txtNomb.MaxLength
        txtNumePadre.MaxLength = txtNume.MaxLength

        txtRPMadre.MaxLength = txtRP.MaxLength
        txtRPNumeMadre.MaxLength = txtRPNume.MaxLength
        txtNombMadre.MaxLength = txtNomb.MaxLength
        txtNumeMadre.MaxLength = txtNume.MaxLength
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()

        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim dtProducto As DataTable
        hdnmensajeConsulta.Text = ""
        If Not Request.QueryString("SoloConsulta") Is Nothing Then
            If Request.QueryString("SoloConsulta").ToLower() = "true" Then
                hdnSoloConsulta.Text = Request.QueryString("SoloConsulta")
                mboolSoloConsulta = Convert.ToBoolean(Request.QueryString("SoloConsulta"))
            Else

                hdnSoloConsulta.Text = "false"
                mboolSoloConsulta = False
            End If

            If Not Request.QueryString("mensajeConsulta") Is Nothing And Request.QueryString("mensajeConsulta").ToString() <> "" Then
                hdnmensajeConsulta.Text = Request.QueryString("mensajeConsulta").ToString()
            End If
        End If

        mstrProd = Request.QueryString("prod")
        hdnEstado.Text = mValiEditarPedigree(mstrProd)

        dtProducto = oProducto.GetProductoById(mstrProd, "Producto")
        If dtProducto.Rows.Count > 0 Then
            mstrRazaId = dtProducto.Rows(0).Item("prdt_raza_id")
        End If
        If Not mstrRazaId Is Nothing Then
            hdnInscribeSRA.Text = mValiInscribeSRA(mstrRazaId)
        Else
            hdnInscribeSRA.Text = "0"
        End If

        btnCance.Attributes.Add("onclick", "mCancelar();return true;")
        btnCancePadres.Attributes.Add("onclick", "mCancelar();return true;")

        btnImpri.Attributes.Add("onclick", "return(mValiImpre());")
        mstrRazaId = Request.QueryString("raza")

        usrProd.Sexo = hdnSexo.Text
    End Sub
#End Region

#Region "Operaciones de controles"
    Private Sub mLimpiar()
        cmbAsoc.Limpiar()
        txtNume.Text = ""
        txtRPNume.Text = ""
        txtRP.Text = ""
        txtNomb.Text = ""
        txtNaciFecha.Text = ""
        usrProd.Limpiar()
        optExis.Checked = True
        optNuev.Checked = False
    End Sub
    Private Sub mLimpiarDoble()
        cmbAsocPadre.Limpiar()
        cmbAsocMadre.Limpiar()
        txtNumePadre.Text = ""
        txtNumeMadre.Text = ""
        txtRPNumePadre.Text = ""
        txtRPNumeMadre.Text = ""
        txtRPPadre.Text = ""
        txtRPMadre.Text = ""
        txtNombPadre.Text = ""
        txtNombMadre.Text = ""
        txtNaciFechaPadre.Text = ""
        txtNaciFechaMadre.Text = ""
        usrProdPadre.Limpiar()
        usrProdMadre.Limpiar()
        optExisPadre.Checked = True
        optNuevPadre.Checked = False
        optExisMadre.Checked = True
        optNuevMadre.Checked = False
    End Sub
    Private Sub mCerrarPanel(Optional ByVal pstrPanelSuf As String = "")
        DirectCast(FindControl("panCarga" & pstrPanelSuf), System.Web.UI.WebControls.Panel).Visible = False
        DirectCast(FindControl("panCarga" & pstrPanelSuf), System.Web.UI.WebControls.Panel).Style.Add("VISIBILITY", "hidden")
        DirectCast(FindControl("panCarga" & pstrPanelSuf), System.Web.UI.WebControls.Panel).Style.Add("POSITION", "absolute")
        If pstrPanelSuf <> "" Then
            mLimpiarDoble()
        Else
            mLimpiar()
        End If
    End Sub
    Private Sub mSetearControlProductos(ByVal pstrPrdtId As String, ByVal pbooDoble As Boolean)
        If pbooDoble Then
                usrProdPadre.Sexo = "1"
                usrProdPadre.usrRazaCriadorExt.cmbCriaRazaExt.Limpiar()
                clsWeb.gCargarRefeCmb(mstrConn, "razas_padres", usrProdPadre.usrRazaCriadorExt.cmbCriaRazaExt, "S", "@prdt_id = " & pstrPrdtId & ",@sexo = 1")
                cmbRazaPadre.Limpiar()
                clsWeb.gCargarRefeCmb(mstrConn, "razas_padres", cmbRazaPadre, "S", "@prdt_id = " & pstrPrdtId & ",@sexo = 1")
            usrProdPadre.usrRazaCriadorExt.cmbCriaRazaExt.Valor = mstrRazaId
                usrProdMadre.Sexo = "0"
                usrProdMadre.usrRazaCriadorExt.cmbCriaRazaExt.Limpiar()
                clsWeb.gCargarRefeCmb(mstrConn, "razas_padres", usrProdMadre.usrRazaCriadorExt.cmbCriaRazaExt, "S", "@prdt_id = " & pstrPrdtId & ",@sexo = 0")
                cmbRazaMadre.Limpiar()
                clsWeb.gCargarRefeCmb(mstrConn, "razas_padres", cmbRazaMadre, "S", "@prdt_id = " & pstrPrdtId & ",@sexo = 0")
            usrProdMadre.usrRazaCriadorExt.cmbCriaRazaExt.Valor = mstrRazaId
        Else
            ' Dario 2013-04-24
            ' se agrega se seteo del sexo al control sino camina mal 
            usrProd.Sexo = hdnSexo.Text
            usrProd.Limpiar()
            clsWeb.gCargarRefeCmb(mstrConn, "razas_padres", usrProd.usrRazaCriadorExt.cmbCriaRazaExt, "S", "@prdt_id = " & pstrPrdtId & ",@sexo = " & hdnSexo.Text)
            clsWeb.gCargarRefeCmb(mstrConn, "razas_padres", cmbRaza, "S", "@prdt_id = " & pstrPrdtId & ",@sexo = " & hdnSexo.Text)
            usrProd.usrRazaCriadorExt.cmbCriaRazaExt.Valor = mstrRazaId
        End If

    End Sub
    Public Sub mArmarArbolPedigree(ByVal pstrProdId As String)
        Try
            Dim mobj As SRA_Neg.Pedigree
            mobj = New SRA_Neg.Pedigree(mstrConn)
            mobj.ArmarArbolPedigree(mstrConn, pstrProdId)

            '************* NIVEL 0 ******************************
            txtProd.Text = mobj.pProd
            txtProd.Tag = mobj.pProdId
            '************* NIVEL 1 ******************************
            txtPadreA.Text = mobj.pPadreA
            txtPadreA.Tag = mobj.pPadreAId

            txtMadreB.Text = mobj.pMadreB
            txtMadreB.Tag = mobj.pMadreBId

            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim dtProducto As DataTable

            dtProducto = oProducto.GetProductoById(mobj.pProdId, "")

            If txtPadreA.Text = "" And txtPadreA.Tag = "" Then
                If ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False) <> "0" Then
                    txtPadreA.Text = "Raza:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False) + "-" + _
                    oRaza.GetRazaByRazaCodigo(ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False)).Trim + _
                                                   " | HBA:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbap"), False) + _
                                                   " | RP:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpPd"), False)
                End If



            End If


            If txtMadreB.Text = "" And txtMadreB.Tag = "" Then
                If ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False) <> "0" Then
                    txtMadreB.Text = "Raza:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False) + "-" + _
                                   oRaza.GetRazaByRazaCodigo(ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False)).Trim() + _
                                    " | HBA:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbam"), False) + _
                                    " | RP:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpMd"), False)
                End If
            End If




            '************* NIVEL 2 ******************************
            txtPadreA1.Text = mobj.pPadreA1
            txtPadreA1.Tag = mobj.pPadreA1Id

            txtMadreA1.Text = mobj.pMadreA1
            txtMadreA1.Tag = mobj.pMadreA1Id

            txtPadreB1.Text = mobj.pPadreB1
            txtPadreB1.Tag = mobj.pPadreB1Id

            txtMadreB1.Text = mobj.pMadreB1
            txtMadreB1.Tag = mobj.pMadreB1Id
            '************* NIVEL 3 ******************************
            txtPadreA21.Text = mobj.pPadreA21
            txtPadreA21.Tag = mobj.pPadreA21Id

            txtMadreA21.Text = mobj.pMadreA21
            txtMadreA21.Tag = mobj.pMadreA21Id

            txtPadreA22.Text = mobj.pPadreA22
            txtPadreA22.Tag = mobj.pPadreA22Id

            txtMadreA22.Text = mobj.pMadreA22
            txtMadreA22.Tag = mobj.pMadreA22Id

            txtPadreB11.Text = mobj.pPadreB11
            txtPadreB11.Tag = mobj.pPadreB11Id

            txtMadreB11.Text = mobj.pMadreB11
            txtMadreB11.Tag = mobj.pMadreB11Id

            txtPadreB12.Text = mobj.pPadreB12
            txtPadreB12.Tag = mobj.pPadreB12Id

            txtMadreB12.Text = mobj.pMadreB12
            txtMadreB12.Tag = mobj.pMadreB12Id
            '************** NIVEL 4 *****************************
            txtPadreA211.Text = mobj.pPadreA211
            txtPadreA211.Tag = mobj.pPadreA211Id

            txtMadreA211.Text = mobj.pMadreA211
            txtMadreA211.Tag = mobj.pMadreA211Id

            txtPadreA212.Text = mobj.pPadreA212
            txtPadreA212.Tag = mobj.pPadreA212Id

            txtMadreA212.Text = mobj.pMadreA212
            txtMadreA212.Tag = mobj.pMadreA212Id

            txtPadreA221.Text = mobj.pPadreA221
            txtPadreA221.Tag = mobj.pPadreA221Id

            txtMadreA221.Text = mobj.pMadreA221
            txtMadreA221.Tag = mobj.pMadreA221Id

            txtPadreA222.Text = mobj.pPadreA222
            txtPadreA222.Tag = mobj.pPadreA222Id

            txtMadreA222.Text = mobj.pMadreA222
            txtMadreA222.Tag = mobj.pMadreA222Id
            '************** NIVEL 5 *****************************
            txtPadreB111.Text = mobj.pPadreB111
            txtPadreB111.Tag = mobj.pPadreB111Id

            txtMadreB111.Text = mobj.pMadreB111
            txtMadreB111.Tag = mobj.pMadreB111Id

            txtPadreB112.Text = mobj.pPadreB112
            txtPadreB112.Tag = mobj.pPadreB112Id

            txtMadreB112.Text = mobj.pMadreB112
            txtMadreB112.Tag = mobj.pMadreB112Id

            txtPadreB121.Text = mobj.pPadreB121
            txtPadreB121.Tag = mobj.pPadreB121Id

            txtMadreB121.Text = mobj.pMadreB121
            txtMadreB121.Tag = mobj.pMadreB121Id

            txtPadreB122.Text = mobj.pPadreB122
            txtPadreB122.Tag = mobj.pPadreB122Id

            txtMadreB122.Text = mobj.pMadreB122
            txtMadreB122.Tag = mobj.pMadreB122Id

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub mCargarPadres(ByVal pstrPrdtId As String)
        mstrCmd = "exec PadreyMadre_consul "
        mstrCmd += "@prdt_id = " & pstrPrdtId

        Dim lDsDatos As DataSet
        lDsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        If lDsDatos.Tables(0).Select("Padre<>0").GetUpperBound(0) > -1 Then
            usrProdPadre.Valor = lDsDatos.Tables(0).Select("Padre<>0")(0).Item("Padre")
        End If

        If lDsDatos.Tables(0).Select("Madre<>0").GetUpperBound(0) > -1 Then
            usrProdMadre.Valor = lDsDatos.Tables(0).Select("Madre<>0")(0).Item("Madre")
        End If
    End Sub
#End Region

#Region "Opciones ABM"
    Private Sub mGrabar(Optional ByVal pstrSufijo As String = "")
        mstrCmd = "exec productos_pedigree_actu "
        mstrCmd = mstrCmd & " @prdt_sexo=" + clsSQLServer.gFormatArg(DirectCast(FindControl("hdnSexo" & pstrSufijo), System.Web.UI.WebControls.TextBox).Text, SqlDbType.Int)

        If DirectCast(FindControl("optNuev" & pstrSufijo), System.Web.UI.WebControls.RadioButton).Checked Then
            If Not DirectCast(FindControl("txtRP" & pstrSufijo), NixorControls.TextBoxTab).Valor Is System.DBNull.Value Then
                mstrCmd = mstrCmd & ",@prdt_rp=" + clsSQLServer.gFormatArg(DirectCast(FindControl("txtRP" & pstrSufijo), NixorControls.TextBoxTab).Valor, SqlDbType.VarChar)
            End If
            If Not DirectCast(FindControl("txtRPNume" & pstrSufijo), NixorControls.NumberBox).Valor Is System.DBNull.Value Then
                mstrCmd = mstrCmd & ",@prdt_rp_nume=" + clsSQLServer.gFormatArg(DirectCast(FindControl("txtRPNume" & pstrSufijo), NixorControls.NumberBox).Valor, SqlDbType.Int)
            End If
            mstrCmd = mstrCmd & ",@prdt_nomb=" + clsSQLServer.gFormatArg(DirectCast(FindControl("txtNomb" & pstrSufijo), NixorControls.TextBoxTab).Valor, SqlDbType.VarChar)
            mstrCmd = mstrCmd & ",@prdt_naci_fecha=" + clsSQLServer.gFormatArg(DirectCast(FindControl("txtNaciFecha" & pstrSufijo), NixorControls.DateBox).Text, SqlDbType.SmallDateTime)
            mstrCmd = mstrCmd & ",@asoc_id=" + clsSQLServer.gFormatArg(DirectCast(FindControl("cmbAsoc" & pstrSufijo), NixorControls.ComboBox).Valor.ToString, SqlDbType.Int)
            ' Dario 2013-04-23 se corrige tipor de dato entra NumberBox sale TextBoxTab en txtNume
            If Not DirectCast(FindControl("txtNume" & pstrSufijo), NixorControls.NumberBox).Valor Is System.DBNull.Value Then
                mstrCmd = mstrCmd & ",@asoc_nume=" + clsSQLServer.gFormatArg(DirectCast(FindControl("txtNume" & pstrSufijo), NixorControls.NumberBox).Valor.ToString, SqlDbType.VarChar)
            End If
            mstrCmd = mstrCmd & ",@prdt_raza_id=" + clsSQLServer.gFormatArg(DirectCast(FindControl("cmbRaza" & pstrSufijo), NixorControls.ComboBox).Valor.ToString, SqlDbType.Int)
        Else
            mstrCmd = mstrCmd & ",@prdt_id=" + clsSQLServer.gFormatArg(DirectCast(FindControl("usrProd" & pstrSufijo), usrProducto).Valor.ToString, SqlDbType.Int)
        End If
        mstrCmd = mstrCmd & ",@prdt_audi_user=" + clsSQLServer.gFormatArg(Session("sUserId").ToString(), SqlDbType.Int)
        mstrCmd = mstrCmd & ",@ante_prod_id=" + clsSQLServer.gFormatArg(DirectCast(FindControl(hdnAnte.Text), NixorControls.TextBoxTab).Tag, SqlDbType.Int)

        clsSQLServer.gExecuteScalar(mstrConn, mstrCmd)

        mArmarArbolPedigree(mstrProd)
    End Sub
    Private Function mValidadDatosPadre() As Boolean
        If optNuevPadre.Checked Then
            ' Dario 2013-06-11 se elimin la obligatoriedad de algunos datos para el nuevo
            'If Not (cmbRazaPadre.Valor.ToString <> "" AndAlso cmbAsocPadre.Valor.ToString <> "" AndAlso txtNumePadre.Valor.ToString <> "" AndAlso txtRPPadre.Valor.ToString <> "" AndAlso txtRPNumePadre.Valor.ToString <> "" AndAlso txtNombPadre.Valor.ToString <> "") Then
            If Not (cmbRazaPadre.Valor.ToString <> "" AndAlso cmbAsocPadre.Valor.ToString <> "" AndAlso txtNombPadre.Valor.ToString <> "") Then
                Throw New AccesoBD.clsErrNeg("Todos los datos del Padre nuevo deben estar completos.")
            End If
        Else
            If usrProdPadre.Valor.ToString = "" Then
                Return False
            End If
        End If
        Return True
    End Function
    Private Function mValidadDatosMadre() As Boolean
        If optNuevMadre.Checked Then
            ' Dario 2013-06-11 se elimin la obligatoriedad de algunos datos para el nuevo
            'If Not (cmbRazaMadre.Valor.ToString <> "" AndAlso cmbAsocMadre.Valor.ToString <> "" AndAlso txtNumeMadre.Valor.ToString <> "" AndAlso txtRPMadre.Valor.ToString <> "" AndAlso txtRPNumeMadre.Valor.ToString <> "" AndAlso txtNombMadre.Valor.ToString <> "") Then
            If Not (cmbRazaMadre.Valor.ToString <> "" AndAlso cmbAsocMadre.Valor.ToString <> "" AndAlso txtNombMadre.Valor.ToString <> "") Then
                Throw New AccesoBD.clsErrNeg("Todos los datos de la Madre nueva deben estar completos.")
            End If
        Else
            If usrProdMadre.Valor.ToString = "" Then
                Return False
            End If
        End If
        Return True
    End Function
#End Region

#Region "Eventos de controles"
    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            mGrabar()
            mCerrarPanel()
            mArmarArbolPedigree(mstrProd)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnGrabarPadres_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarPadres.Click
        Try
            Dim lbooGrabaPadre As Boolean = mValidadDatosPadre()
            Dim lbooGrabaMadre As Boolean = mValidadDatosMadre()

            If lbooGrabaPadre Or lbooGrabaMadre Then
                If lbooGrabaPadre Then mGrabar("Padre")
                If lbooGrabaMadre Then mGrabar("Madre")
                mCerrarPanel("Doble")
                mArmarArbolPedigree(mstrProd)
            Else
                Throw New AccesoBD.clsErrNeg("Debe ingresar un Padre y/o una Madre para el Producto.")
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnCance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCance.Click
        mCerrarPanel()
    End Sub
    Private Sub btnCancePadres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancePadres.Click
        mCerrarPanel("Doble")
    End Sub
    Private Sub hdnProd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnProd.TextChanged
        Try
            'hdnEstado.Text.ToLower() <> "vigente" And
            'If (hdnEstado.Text.ToLower() <> "es baja" And hdnInscribeSRA.Text = "1" And hdnSoloConsulta.Text.ToLower <> "true") Then
            ' 2015-06-15 Dario se saca el hdnInscribeSRA para manejarlo diferente no se lo bloquea
            ' sino que lo deja solo modificar por animales ya existentes pero no dar alta
            If (hdnInscribeSRA.Text = "0") Then
                Dim lctrControl As NixorControls.TextBoxTab = (FindControl(hdnProd.Text))
                mCargarPadres(lctrControl.Tag)
                panCargaDoble.Visible = True
                panCargaDoble.Style.Add("VISIBILITY", "")
                panCargaDoble.Style.Add("POSITION", "absolute")
                mSetearControlProductos(lctrControl.Tag, True)
                txtProdAntePadre.Text = "Padre del Producto: " & lctrControl.Valor.ToString
                txtProdAnteMadre.Text = "Madre del Producto: " & lctrControl.Valor.ToString

                usrProdPadre.Activo = True
                optNuevPadre.Enabled = False
                Me.lblMensajePadre.Text = ""
                Me.lblMensajePadre.Visible = False
                usrProdMadre.Activo = True
                optNuevMadre.Enabled = False
                Me.lblMensajeMadre.Text = ""
                Me.lblMensajeMadre.Visible = True
                btnGrabarPadres.Enabled = True
            Else
                If (hdnEstado.Text.ToLower() <> "es baja" And hdnSoloConsulta.Text.ToLower <> "true") Then
                    Dim lctrControl As NixorControls.TextBoxTab = (FindControl(hdnProd.Text))

                    If lctrControl.Tag = "" Then
                        lctrControl = (FindControl(hdnAnte.Text))
                        panCarga.Visible = True
                        panCarga.Style.Add("VISIBILITY", "")
                        panCarga.Style.Add("POSITION", "absolute")
                        mSetearControlProductos(lctrControl.Tag, False)
                        txtProdAnte.Text = IIf(hdnSexo.Text = "1", "Padre", "Madre") & " del Producto: " & lctrControl.Valor.ToString
                    Else
                        Dim objProductosBusiness As New Business.Productos.ProductosBusiness
                        'ejecuto meto de reversion de la baja 
                        Dim resu As RespValidaModificarPedregeeEntity = objProductosBusiness.ValidaModificarPedregeeProducto(lctrControl.Tag, Session("sUserId").ToString())

                        mCargarPadres(lctrControl.Tag)
                        panCargaDoble.Visible = True
                        panCargaDoble.Style.Add("VISIBILITY", "")
                        panCargaDoble.Style.Add("POSITION", "absolute")
                        mSetearControlProductos(lctrControl.Tag, True)
                        txtProdAntePadre.Text = "Padre del Producto: " & lctrControl.Valor.ToString
                        txtProdAnteMadre.Text = "Madre del Producto: " & lctrControl.Valor.ToString

                        ' 2015-05-29 Dario
                        '-1 = no se puede modificar ni padre ni madre
                        ' 0 = deja modificar padre y madre
                        ' 1 = solo deja modificar padre
                        ' 2 = solo deja modificar madre
                        Select Case resu.respuesta_ID
                            Case -1
                                usrProdPadre.Activo = False
                                optNuevPadre.Enabled = False
                                Me.lblMensajePadre.Text = resu.respuesta_Desc
                                Me.lblMensajePadre.Visible = True
                                usrProdMadre.Activo = False
                                optNuevMadre.Enabled = False
                                Me.lblMensajeMadre.Text = resu.respuesta_Desc
                                Me.lblMensajeMadre.Visible = True
                                btnGrabarPadres.Enabled = False
                            Case 0
                                usrProdPadre.Activo = True
                                optNuevPadre.Enabled = True
                                Me.lblMensajePadre.Text = ""
                                Me.lblMensajePadre.Visible = False
                                usrProdMadre.Activo = True
                                optNuevMadre.Enabled = True
                                Me.lblMensajeMadre.Text = ""
                                Me.lblMensajeMadre.Visible = False
                                btnGrabarPadres.Enabled = True
                            Case 1
                                usrProdPadre.Activo = True
                                optNuevPadre.Enabled = True
                                Me.lblMensajePadre.Text = ""
                                Me.lblMensajePadre.Visible = False
                                usrProdMadre.Activo = False
                                optNuevMadre.Enabled = False
                                Me.lblMensajeMadre.Text = resu.respuesta_Desc
                                Me.lblMensajeMadre.Visible = True
                                btnGrabarPadres.Enabled = True
                            Case 2
                                usrProdPadre.Activo = False
                                optNuevPadre.Enabled = False
                                Me.lblMensajePadre.Text = resu.respuesta_Desc
                                Me.lblMensajePadre.Visible = True
                                usrProdMadre.Activo = True
                                optNuevMadre.Enabled = True
                                Me.lblMensajeMadre.Text = ""
                                Me.lblMensajeMadre.Visible = False
                                btnGrabarPadres.Enabled = True
                            Case Else
                                usrProdPadre.Activo = False
                                optNuevPadre.Enabled = False
                                Me.lblMensajePadre.Text = "El resultado del analisis no permite modificar el Padre"
                                Me.lblMensajePadre.Visible = True
                                usrProdMadre.Activo = False
                                optNuevMadre.Enabled = False
                                Me.lblMensajeMadre.Text = "El resultado del analisis no permite modificar el Madre"
                                btnGrabarPadres.Enabled = False
                        End Select
                    End If
                End If
            End If
            ' vemos como bloqueamos el control
            ' Session("sUserId").ToString()
            'If lDsDatos.Tables(0).Select("Padre<>0").GetUpperBound(0) > -1 Then
            '    usrProdPadre.Valor = lDsDatos.Tables(0).Select("Padre<>0")(0).Item("Padre")
            'End If

            'If lDsDatos.Tables(0).Select("Madre<>0").GetUpperBound(0) > -1 Then
            '    usrProdMadre.Valor = lDsDatos.Tables(0).Select("Madre<>0")(0).Item("Madre")
            'End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            hdnProd.Text = ""
        End Try
    End Sub
#End Region

    Public Function mValiImpre(ByVal pstrProdId As String)
        Dim dtProducto As DataTable
        Dim dtEstado As DataTable
        Dim ProductoId As String
        Dim intEstadoId As Int16
        Dim strEstado As String
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim oEstado As New SRA_Neg.Estado(mstrConn, Session("sUserId").ToString())
        ' Modificado : GSZ 23/04/2015
        dtProducto = oProducto.GetProductoById(pstrProdId, "Producto")
        If dtProducto.Rows.Count > 0 Then
            intEstadoId = dtProducto.Rows(0).Item("prdt_esta_id")
        End If

        dtEstado = oEstado.GetEstadoById(intEstadoId, "Estado")
        If dtEstado.Rows.Count > 0 Then
            strEstado = dtEstado.Rows(0).Item("esta_desc")
        End If

        If (strEstado.ToLower() = "pendiente" Or strEstado.ToLower() = "retenida") Then
            btnImpri.Enabled = False
        Else
            btnImpri.Enabled = True
        End If
        Return True

    End Function

    Private Function mValiInscribeSRA(ByVal mstrRazaId As String) As String

        Dim FacturacionBusiness As New Business.Facturacion.FacturacionBusiness
        Dim pstrArgs As String
        Dim boolInscribeSRA As Boolean
        Dim strInscribeSRA As String

        pstrArgs = "@raza_id=" + mstrRazaId

        boolInscribeSRA = FacturacionBusiness.GetRazaInscribeEnSRA(pstrArgs)
        If boolInscribeSRA Then
            strInscribeSRA = "1"
        Else
            strInscribeSRA = "0"
        End If
        Return strInscribeSRA
    End Function

    Public Function mValiEditarPedigree(ByVal pstrProdId As String) As String
        Dim dtProducto As DataTable
        Dim dtEstado As DataTable
        Dim ProductoId As String
        Dim intEstadoId As Int16
        Dim strEstado As String
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim oEstado As New SRA_Neg.Estado(mstrConn, Session("sUserId").ToString())


        dtProducto = oProducto.GetProductoById(pstrProdId, "Producto")
        If dtProducto.Rows.Count > 0 Then
            intEstadoId = dtProducto.Rows(0).Item("prdt_esta_id")
        End If

        dtEstado = oEstado.GetEstadoById(intEstadoId, "Estado")
        If dtEstado.Rows.Count > 0 Then
            strEstado = dtEstado.Rows(0).Item("esta_desc")
        End If

        Return strEstado
    End Function

    Private Sub mImprimir()
        Try
            'Dim mobj As SRA_Neg.Pedigree

            'mobj = New SRA_Neg.Pedigree(mstrConn)
            'mobj.ArmarArbolPedigree(mstrConn, mstrProd)
            Dim params As String
            ' se modifica para que use el nuevo reporte sino no camina
            'Dim lstrRptName As String = "Certificado_Pedigree"
            Dim lstrRptName As String = "Certificado_pedigree_Masivo"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            params += "&prdt_id=" + mstrProd
            ''PADRE
            'params += "&P=" + mobj.pPadreA()
            ''RAMA DEL ABUELO PATERNO
            'params += "&P1=" + mobj.pPadreA1()
            'params += "&P11=" + mobj.pPadreA21()
            'params += "&P111=" + mobj.pPadreA211()
            'params += "&P110=" + mobj.pMadreA211()
            'params += "&P10=" + mobj.pMadreA21()
            'params += "&P101=" + mobj.pPadreA212()
            'params += "&P100=" + mobj.pMadreA212()
            ''RAMA DE LA ABUELA PATERNA
            'params += "&P0=" + mobj.pMadreA1()
            'params += "&P01=" + mobj.pPadreA22()
            'params += "&P011=" + mobj.pPadreA221()
            'params += "&P010=" + mobj.pMadreA221()
            'params += "&P00=" + mobj.pMadreA22()
            'params += "&P001=" + mobj.pPadreA222()
            'params += "&P000=" + mobj.pMadreA222()

            ''MADRE
            'params += "&M=" + mobj.pMadreB()
            ''RAMA DEL ABUELO MATERNO 
            'params += "&M1=" + mobj.pPadreB1()
            'params += "&M11=" + mobj.pPadreB11()
            'params += "&M111=" + mobj.pPadreB111()
            'params += "&M110=" + mobj.pMadreB111()
            'params += "&M10=" + mobj.pMadreB11()
            'params += "&M101=" + mobj.pPadreB112()
            'params += "&M100=" + mobj.pMadreB112()
            ''RAMA DE LA ABUELA MATERNA
            'params += "&M0=" + mobj.pMadreB1()
            'params += "&M01=" + mobj.pPadreB12()
            'params += "&M011=" + mobj.pPadreB121()
            'params += "&M010=" + mobj.pMadreB121()
            'params += "&M00=" + mobj.pMadreB12()
            'params += "&M001=" + mobj.pPadreB122()
            'params += "&M000=" + mobj.pMadreB122()
            params += "&raza_id=" + mstrRazaId

            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnImpri_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImpri.Click
        mImprimir()
    End Sub
    Function MessageBox(ByRef oPage As Page, ByVal sAviso As String)
        If sAviso.Trim.Length > 0 Then
            Dim s As String = "alert('" & sAviso & "') "
            oPage.RegisterStartupScript("OnLoad", s)
        End If
    End Function
End Class

End Namespace
