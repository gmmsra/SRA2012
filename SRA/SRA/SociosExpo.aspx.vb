Imports System.IO
Imports System.Text


Namespace SRA


Partial Class SociosExpo
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

            mInicializar()
            mEstablecerPerfil()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdCons.PageSize = Convert.ToInt32(mstrParaPageSize)
        txtArch.Text = System.Configuration.ConfigurationSettings.AppSettings("conArchExpoSocios")
        clsWeb.gCargarRefeCmb(mstrConn, "socios_campos", lisCamp, "", "")
        lisCamp.TodosSelec = True
        btnGrab.Attributes.Add("onclick", "if(!confirm('Confirma la exportación del padrón con los socios seleccionados?')) return false;")
        usrSocFil.MostrarOrden = False
        usrSocFil.MostrarDatosOpcionales = False
   End Sub

   Private Sub mLimpiarFiltros()
      usrSocFil.Limpiar()
      panGrab.Visible = False
      grdCons.Visible = False
      lblPrev.Visible = False
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Sub mExportarDBF(ByVal pstrArch As String, ByVal ds As DataSet)
        Try
            clsExporta.clsGrabarArchivo.FormatoDBF(ds.Tables(0), pstrArch)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mExportarXLS(ByVal pstrArch As String, ByVal pbooTitu As Boolean, ByVal ds As DataSet)
        Try
            clsExporta.clsGrabarArchivo.FormatoTexto(ds, pstrArch, vbTab, pbooTitu)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mExportarCSV(ByVal pstrArch As String, ByVal pbooTitu As Boolean, ByVal ds As DataSet)
        Try
            clsExporta.clsGrabarArchivo.FormatoTexto(ds, pstrArch, ",", pbooTitu)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mConsultar()
      Try

         hdnRptId.Text = usrSocFil.ObtenerIdFiltro.ToString()

         If lisCamp.ObtenerIDs() = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar los campos a incluir en la exportación.")
         End If

         mstrCmd = "exec socios_general_exporta_consul "
         mstrCmd += " @rptf_id=" + hdnRptId.Text
         mstrCmd += ",@rptf_campos=" + clsSQLServer.gFormatArg(lisCamp.ObtenerIDs(), SqlDbType.VarChar)
         mstrCmd += ",@rptf_orden=" + clsSQLServer.gFormatArg(cmbOrde.Valor, SqlDbType.VarChar)
         mstrCmd += ",@rptf_top=1"

         Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd, 3000)

         For Each dc As DataColumn In ds.Tables(0).Columns
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            dgCol.DataField = dc.ColumnName
            dgCol.HeaderText = dc.ColumnName
            'If dc.Ordinal = 0 Then
            'dgCol.Visible = False
            'End If

            grdCons.Columns.Add(dgCol)
         Next
         grdCons.DataSource = ds
         grdCons.DataBind()
         grdCons.Visible = True
         If ds.Tables(0).Rows.Count = 30 Then
            lblPrev.Visible = True
         End If
         panGrab.Visible = True
         btnGrab.Enabled = (grdCons.Items.Count > 0)
         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub

    Private Sub btnCons_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCons.Click
        grdCons.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub btnGrab_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGrab.Click

      Try

        If txtArch.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el nombre del archivo.")
        End If

        mstrCmd = "exec socios_general_exporta_consul "
        mstrCmd += " @rptf_id=" + hdnRptId.Text
        mstrCmd += ",@rptf_campos=" + clsSQLServer.gFormatArg(lisCamp.ObtenerIDs(), SqlDbType.VarChar)
        mstrCmd += ",@rptf_orden=" + clsSQLServer.gFormatArg(cmbOrde.Valor, SqlDbType.VarChar)

        Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd, 3000)

        Dim lstrArch As String = System.Configuration.ConfigurationSettings.AppSettings("conPathExpoSocios")
        lstrArch = HttpContext.Current.Server.MapPath(lstrArch)
        lstrArch = lstrArch.Replace("archivo.ext", txtArch.Text.ToLower & ".ext")
        lstrArch = lstrArch.Replace(".ext", "." & cmbForm.Valor.ToString.ToLower)

        Select Case cmbForm.Valor
            Case "CSV"
                mExportarCSV(lstrArch, chkTitu.Checked, ds)
            Case "DBF"
                mExportarDBF(lstrArch, ds)
            Case "XLS"
                mExportarXLS(lstrArch, chkTitu.Checked, ds)
        End Select

        hdnExpo.Text = txtArch.Text.ToLower & "." & cmbForm.Valor.ToString.ToLower
        'Throw New AccesoBD.clsErrNeg("La exportación ha finalizado correctamente.")

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      Finally
         mConsultar()
      End Try

    End Sub

End Class
End Namespace
