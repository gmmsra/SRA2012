<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Enfermedades" CodeFile="Enfermedades.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Enfermedades</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('')" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Enfermedades</asp:label></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
												Width="100%" Visible="True">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblCodigoFil" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtCodigoFil" runat="server" cssclass="cuadrotexto" Width="72px"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblDenominacionFil" runat="server" cssclass="titulo">Denominaci�n:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:TextBoxtab id="txtDenominacionFil" runat="server" cssclass="cuadrotexto" Width="80%"></cc1:TextBoxtab></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3" align="center"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="enfe_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="enfe_codi" ReadOnly="True" HeaderText="C&#243;digo" ItemStyle-HorizontalAlign="left"></asp:BoundColumn>
													<asp:BoundColumn DataField="enfe_deno" ReadOnly="True" HeaderText="Denominaci&#243;n">
														<HeaderStyle Width="50%"></HeaderStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<tr>
										<TD colSpan="3"></TD>
									</tr>
									<TR>
										<TD></TD>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
												IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
												ImageOver="btnNuev2.gif" ForeColor="Transparent" ToolTip="Argerar un Nuevo Registro" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
										<TD align="right"><CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
												BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
												BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent" ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
									</TR>
									<tr>
										<TD colSpan="3"></TD>
									</tr>
									<TR>
										<TD colSpan="3" align="center"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> Enfermedades</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkRazas" runat="server" cssclass="solapa"
																Width="80px" Height="21px" CausesValidation="False">Razas</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkResultados" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False">Resultados</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																	<TBODY>
																		<TR>
																			<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																				<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 16px">
																				<CC1:numberbox id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="72px"></CC1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																				align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																				<asp:Label id="lblDenominacion" runat="server" cssclass="titulo">Denominaci�n:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 32px">
																				<cc1:textboxtab id="txtDenominacion" runat="server" cssclass="textolibre" Width="300px" Obligatorio="True"
																					height="46px" Rows="2" TextMode="MultiLine"></cc1:textboxtab></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 220px; HEIGHT: 16px" noWrap align="right">
																				<asp:Label id="lblDiasVigencia" runat="server" cssclass="titulo">D�as de Vigencia de los Certificados</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 32px">
																				<CC1:numberbox id="txtDiasVigencia" runat="server" cssclass="cuadrotexto" Width="72px"></CC1:numberbox></TD></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3"><asp:panel id="panRaza" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="TablaRazaAsoc" border="0" cellPadding="0" align="left">
													<TR>
														<TD vAlign="top" colSpan="2" align="center">
															<asp:datagrid id="grdRaza" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
																HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGridRaza_Page"
																OnEditCommand="mEditarDatosEnfeRaza" AutoGenerateColumns="False">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="enra_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_razas_desc" ReadOnly="True" HeaderText="Raza">
																		<HeaderStyle Width="40%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_espe_desc" ReadOnly="True" HeaderText="Especie">
																		<HeaderStyle Width="20%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_enfe_deno" ReadOnly="True" HeaderText="Enfermedad">
																		<HeaderStyle Width="20%"></HeaderStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" vAlign="top" align="right">
															<asp:Label id="lblEspecie" runat="server" cssclass="titulo">Especie:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbEspecie" class="combo" runat="server" Width="176px" onchange="mCargarRaza('cmbEspecie','cmbRaza')"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 22px" vAlign="top" align="right">
															<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 22px">
															<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="300px" onchange="setCmbEspecie('cmbEspecie','cmbRaza')"
																filtra="true" NomOper="razas_cargar" MostrarBotones="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center">
															<asp:Label id="lblBajaRaza" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD colSpan="2" align="center">
															<asp:Button id="btnAltaRazas" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBajaRazas" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModiRazas" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimpRazas" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<tr>
										<TD style="WIDTH: 100%" colSpan="3">
										<asp:panel id="panResultados" runat="server" cssclass="titulo" Width="100%" Visible="False">
												<TABLE style="WIDTH: 100%" id="TablaResultados" border="0" cellPadding="0" align="left">
													<TR>
														<TD vAlign="top" colSpan="2" align="center">
															<asp:datagrid id="grdResultados" runat="server"  BorderStyle="None" BorderWidth="1px"
																width="100%" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None"
																CellPadding="1" OnPageIndexChanged="DataGridResultados_Page" OnEditCommand="mEditarDatosEnfeResultados"
																AutoGenerateColumns="False">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="enre_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_resa_deno" ReadOnly="True" HeaderText="Resultado">
																		<HeaderStyle Width="20%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_enfe_deno" ReadOnly="True" HeaderText="Enfermedad">
																		<HeaderStyle Width="20%"></HeaderStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
															<asp:Label id="lblEnfermedadResul" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:numberbox id="txtEnfermedadResul" runat="server" cssclass="cuadrotexto" Width="72px"></CC1:numberbox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" vAlign="top" align="right">
															<asp:Label id="lblResultados" runat="server" cssclass="titulo">Resultados:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbResultados" class="combo" runat="server" Width="176px"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center">
															<asp:Label id="lblBajaResultados" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD colSpan="2" align="center">
															<asp:Button id="btnAltaResultados" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBajaResultados" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModiResultados" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimpResultados" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
									</asp:panel></TD>
									</tr>
					</tr>
				</TBODY>
			</table>
			</asp:panel><ASP:PANEL id="panBotones" Runat="server">
				<TABLE width="100%">
					<TR>
						<TD align="center">
							<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
					</TR>
					<TR height="30">
						<TD align="center"><A id="editar" name="editar"></A>
							<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
							<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
								Text="Baja"></asp:Button>&nbsp;
							<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
								Text="Modificar"></asp:Button>&nbsp;
							<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
								Text="Limpiar"></asp:Button></TD>
					</TR>
				</TABLE>
			</ASP:PANEL>
			<DIV></DIV>
			</TD></TR></TBODY></TABLE> 
			<!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnRazaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnResultadoId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function mCargarRaza(pEspe,pRaza)
		{
		    var sFiltro = "";
		    var sOpcion="S";
		    
		    if (document.all(pEspe).value != '')
				sFiltro = document.all(pEspe).value;
			LoadComboXML("razas_cargar", sFiltro, pRaza, sOpcion);
	    }
	    
	    function setCmbEspecie(pEspe,pRaza)
	    {
			if (document.all(pRaza).value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all(pRaza).value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all(pEspe).value = vstrRet[0];
			}		
	    }
	    if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
