<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.InscripcionHembraDonante" CodeFile="InscripcionHembraDonante.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROR" Src="controles/usrProductoExtranjero.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Inscripción Hembra Donante</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">

		</script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="2"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Inscripción Hembra Donante</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" BorderStyle="Solid"
										Width="100%">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																	ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																	ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																	IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																align="right">
																<asp:Label id="Label3" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
															<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																<UC1:PROH id="usrProductoFil" runat="server" MuestraDesc="True" EsPropietario="True" MuestraBotonAgregaImportado="False"
																	AutoPostBack="False" FilTipo="S" Ancho="800" Saltos="1,2" Tabla="productos" AceptaNull="false"></UC1:PROH></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="10" colSpan="2"></TD>
							</TR> <!---fin filtro --->
							<TR>
								<TD vAlign="top" colSpan="2" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="prdt_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="prdt_sra_nume" HeaderText="Nro.Producto"></asp:BoundColumn>
											<asp:BoundColumn DataField="prdt_nomb" HeaderText="Producto"></asp:BoundColumn>
											<asp:BoundColumn DataField="prdt_dona_nume" HeaderText="Hembra Donante"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_raza_codi" HeaderText="Raza"></asp:BoundColumn>
											<asp:BoundColumn DataField="_criador" HeaderText="Criador"></asp:BoundColumn>
											
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="2">
									<asp:panel id="panBotones" runat="server" cssclass="titulo" Visible="True" BorderWidth="0"
										BorderStyle="None" Width="100%">
										<TABLE style="WIDTH: 100%" id="Table3" border="0" cellPadding="0" align="left">
											<TR>
												<TD align="left">
													<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
														ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
														IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Inscripción"></CC1:BOTONIMAGEN></TD>
												<TD></TD>
												<TD align="right">
													<CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
														ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
														IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar"
														ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TBODY>
														<TR>
															<TD height="5"><asp:label id="lblTitu" runat="server" cssclass="titulo"></asp:label></TD>
															<TD vAlign="top" align="right">&nbsp;
																<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
														</TR>
														<TR>
														</TR>
														<TR>
															<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																align="right">
																<asp:Label id="Label1" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
															<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																<UC1:PROH id="usrProducto" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																	MuestraDesc="True" Ancho="800" EsPropietario="True" FilTipo="S" AutoPostBack="False" Obligatorio="True"></UC1:PROH></TD>
														</TR>
							</TR>
							<TR>
								<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
									align="right"><asp:label id="lblNroHembraDonante" runat="server" cssclass="titulo">Nro. Donante:</asp:label>&nbsp;</TD>
								<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"><CC1:NUMBERBOX id="txtNroDonante" runat="server" cssclass="cuadrotexto" Width="160px" Obligatorio="True"
										Enabled="False"></CC1:NUMBERBOX></TD>
							</TR>
							<TR>
								<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
									align="right"><asp:label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:label>&nbsp;</TD>
								<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"><cc1:datebox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="True"></cc1:datebox></TD>
							<TR>
								<TD background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
							</TR>
							<TR>
								<TD colSpan="3" align="center"><A id="editar" name="editar"></A><asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
										Text="Modificar"></asp:button>&nbsp;
									<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
										Text="Limpiar"></asp:button></TD>
							</TR>
						</TABLE>
						</P></asp:panel></DIV></td>
				</tr>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
			
		</SCRIPT>
	</BODY>
</HTML>
