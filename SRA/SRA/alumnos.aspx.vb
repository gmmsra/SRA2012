'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_alumnos_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
' la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'alumnos.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
' Dario 2013-10-02 cambio por Procampo CBU
Namespace SRA

'Partial Class alumnos
Partial Class Migrated_alumnos

Inherits alumnos

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents hdnPosic As System.Web.UI.WebControls.TextBox




    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Alumnos
    Private mstrClieEstu As String = SRA_Neg.Constantes.gTab_ClientesEstudios
    Private mstrClieOcup As String = SRA_Neg.Constantes.gTab_ClientesOcupaciones
    Private mstrClieFunc As String = SRA_Neg.Constantes.gTab_ClientesFunciones
    Private mstrClieExpl As String = SRA_Neg.Constantes.gTab_ClientesExplota

    Private mbooSoloBusq As Boolean

    Private mstrConn As String
    Private mstrClieDerivCtrl As String
    Private mstrValorId As String
    Private mstrParaPageSize As Integer

    Private mdsDatos As DataSet
    Private mintInse As Integer

    Private mbooPermiAlta As Boolean
    Private mbooPermiBaja As Boolean
    Private mbooPermiModi As Boolean
    Private mbooPermiDeta As Boolean

    Private Enum Columnas As Integer
        Sele = 0
        Edit = 1
        Id = 2
        Lega = 3
        IdTarj = 7
    End Enum
#End Region

#Region "Inicializaci�n de Variables"
    Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        If Not Request.QueryString("fkvalor") Is Nothing Then
            mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
        End If
        mEstablecerPerfil()
        If mintInse = 0 Then
            mintInse = Convert.ToInt32(clsWeb.gValorParametro(Request.QueryString("FilInseId")))
        End If
        usrClieFil.FilInseId = mintInse
    End Sub

    Private Sub mEstablecerPerfil()

        Select Case mintInse
            Case 1  'ISEA
                mbooPermiAlta = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Alta_ISEA, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiBaja = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Baja_ISEA, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Modificacion_ISEA, String), (mstrConn), (Session("sUserId").ToString()))
            Case 2  'CEIDA
                mbooPermiAlta = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Alta_CEIDA, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiBaja = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Baja_CEIDA, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Modificacion_CEIDA, String), (mstrConn), (Session("sUserId").ToString()))
            Case 3  'EGEA
                mbooPermiAlta = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Alta_EGEA, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiBaja = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Baja_EGEA, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Alumnos_Modificacion_EGEA, String), (mstrConn), (Session("sUserId").ToString()))
        End Select

        btnAlta.Visible = mbooPermiAlta
        btnBaja.Visible = mbooPermiBaja
        btnModi.Visible = mbooPermiModi
        btnLimp.Visible = (mbooPermiAlta Or mbooPermiModi)
        btnAgre.Enabled = (mbooPermiAlta Or mbooPermiModi)

        'permiso sobre los detalles.
        mbooPermiDeta = (mbooPermiAlta Or mbooPermiModi)

        'estudios
        btnAltaEstu.Visible = mbooPermiDeta
        btnBajaEstu.Visible = mbooPermiDeta
        btnModiEstu.Visible = mbooPermiDeta
        btnLimpEstu.Visible = mbooPermiDeta

        'ocupaciones
        btnAltaOcup.Visible = mbooPermiDeta
        btnBajaOcup.Visible = mbooPermiDeta
        btnModiOcup.Visible = mbooPermiDeta
        btnLimpOcup.Visible = mbooPermiDeta

        'cargos
        btnAltaFunc.Visible = mbooPermiDeta
        btnBajaFunc.Visible = mbooPermiDeta
        btnModiFunc.Visible = mbooPermiDeta
        btnLimpFunc.Visible = mbooPermiDeta

        'explotaciones
        btnAltaExpl.Visible = mbooPermiDeta
        btnBajaExpl.Visible = mbooPermiDeta
        btnModiExpl.Visible = mbooPermiDeta
        btnLimpExpl.Visible = mbooPermiDeta

    End Sub

    Public Overrides Sub mInicializarControl()
        Try
            mstrClieDerivCtrl = clsWeb.gValorParametro(Request.QueryString("ctrlId"))
            mbooSoloBusq = clsWeb.gValorParametro(Request.QueryString("SoloBusq")) = "1"
            mstrValorId = clsWeb.gValorParametro(Request.QueryString("ValorId"))

            If hdnValorId.Text = "" Then
                hdnValorId.Text = mstrValorId 'pone el id seleccionado en el control de b�squeda
            ElseIf hdnValorId.Text = "-1" Then  'si ya se limpiaron los filtros, ignora el id con el que vino
                mstrValorId = ""
            End If

            'valores de los filtros
            If Not Page.IsPostBack Then
                txtApelFil.Text = clsWeb.gValorParametro(Request.QueryString("Apel"))
                txtCuitFil.Text = clsWeb.gValorParametro(Request.QueryString("Cuit"))
                txtDocuNumeFil.Text = clsWeb.gValorParametro(Request.QueryString("DocuNume"))
                txtLegaFil.Text = clsWeb.gValorParametro(Request.QueryString("CodiNume"))
                If clsWeb.gValorParametro(Request.QueryString("LegaNume")) <> "" Then
                    txtLegaFil.Text = clsWeb.gValorParametro(Request.QueryString("LegaNume"))
                End If
                If clsWeb.gValorParametro(Request.QueryString("ClieNume")) <> "" Then
                    usrClieFil.Valor = clsWeb.gValorParametro(Request.QueryString("ClieNume"))
                End If
                If mbooSoloBusq Then
                    grdDato.Columns(1).Visible = False
                    btnAgre.Enabled = False
                End If
            End If

            'seteo de controles segun filtros
            grdDato.Columns(0).Visible = (mstrClieDerivCtrl <> "")
            btnLimpiarFil.Visible = (mstrClieDerivCtrl <> "")
            hdnInseId.Text = mintInse

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then
                mInicializarControl()

                Session(mstrTabla) = Nothing
                mdsDatos = Nothing

                mSetearMaxLength()
                mSetearEventos()
                    txtIngrFecha.Fecha = Today
                    mCargarCombos()

                If mstrClieDerivCtrl <> "" Then
                    mConsultar()
                Else
                    mMostrarPanel(False)
                End If

                If Not Page.IsPostBack And grdDato.Items.Count = 1 And mstrClieDerivCtrl <> "" Then
                    If mstrValorId = "" Then
                        mRetorno(grdDato.Items(0).Cells(Columnas.Lega).Text)
                    Else
                        mCargarDatos(mstrValorId)
                        Return
                    End If
                End If

                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                mdsDatos = Session(mstrTabla)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mCargarProvincias()
        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbExclPcia, "S", IIf(cmbExclPais.Valor Is DBNull.Value, "-1", cmbExclPais.Valor))
        mCargarLocalidades()
    End Sub

    Private Sub mCargarLocalidades()
        clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbExclLoca, "N", IIf(cmbExclPcia.Valor Is DBNull.Value, "-1", cmbExclPcia.Valor))
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "estudios", cmbEstu, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "explota_tipos", cmbExti, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "funciones", cmbFunc, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "ocupaciones", cmbOcup, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbExclPais, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "becas", cmbBeca, "N", "@beca_inse_id=" + mintInse.ToString)

        If clsWeb.gValorParametro(Request.QueryString("DocuTipo")) <> "" Then
            cmbDocuTipoFil.Valor = clsWeb.gValorParametro(Request.QueryString("DocuTipo"))
        End If

        mCargarProvincias()
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtLega.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "alum_lega")
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "alum_obse")
    End Sub
#End Region

#Region "Seteo de Controles"


    Private Sub mSetearDA()
        With mdsDatos.Tables(mstrTabla).Rows(0)
            If .Item("_tacl_id").ToString = "" Then
                mMostrarDA(False)
            Else
                mMostrarDA(True)
                Dim boolCargaCBU = False
                ' Dario 2013-10-02 controlo que fue seleccionad una tarjeta, para realizar la consulta para
                ' ver si carga CBU o nro de cuenta
                If (Not .Item("_tacl_tarj_id") Is DBNull.Value) Then
                    boolCargaCBU = clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_consul @tarj_id=" & .Item("_tacl_tarj_id").ToString, "tarj_PermiteDebitoEnCuenta")
                End If
                lblDebAuto.Text = "Tarjeta: " & .Item("_tacl_tarj") + IIf(boolCargaCBU, "  CBU.: ", "  Nro.: ") & .Item("_tacl_nume") + " Fecha Vto.: " & .Item("_tacl_vcto_fecha")
            End If
        End With


    End Sub
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrClieEstu
                btnBajaEstu.Enabled = Not (pbooAlta)
                btnModiEstu.Enabled = Not (pbooAlta)
                btnAltaEstu.Enabled = pbooAlta
            Case mstrClieOcup
                btnBajaOcup.Enabled = Not (pbooAlta)
                btnModiOcup.Enabled = Not (pbooAlta)
                btnAltaOcup.Enabled = pbooAlta
            Case mstrClieFunc
                btnBajaFunc.Enabled = Not (pbooAlta)
                btnModiFunc.Enabled = Not (pbooAlta)
                btnAltaFunc.Enabled = pbooAlta
            Case mstrClieExpl
                btnBajaExpl.Enabled = Not (pbooAlta)
                btnModiExpl.Enabled = Not (pbooAlta)
                btnAltaExpl.Enabled = pbooAlta
            Case Else
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnAlta.Enabled = pbooAlta
                btnDA.Disabled = pbooAlta
        End Select
    End Sub

    Public Overrides Sub mCargarDatos(ByVal pstrId As String)
        mCrearDataSet(pstrId)

        With mdsDatos.Tables(mstrTabla).Rows(0)
            hdnId.Text = .Item("alum_id").ToString()
                usrClie.Valor = IIf(.Item("alum_clie_id").ToString.Trim.Length > 0, .Item("alum_clie_id"), String.Empty)
                txtLega.Valor = IIf(.Item("alum_lega").ToString.Trim.Length > 0, .Item("alum_lega"), String.Empty)
                usrFactClie.Valor = IIf(.Item("alum_fact_clie_id").ToString.Length > 0, .Item("alum_fact_clie_id"), String.Empty)
                txtIngrFecha.Fecha = IIf(.Item("alum_ingr_fecha").ToString.Length > 0, .Item("alum_ingr_fecha"), String.Empty)

                txtObse.Valor = IIf(.Item("alum_obse").ToString.Length > 0, .Item("alum_obse"), String.Empty)

                cmbBeca.Valor = IIf(.Item("alum_beca_id").ToString.Length > 0, .Item("alum_beca_id"), String.Empty)

            If Not .IsNull("alum_baja_fecha") Then
                lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("alum_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
                lblBaja.Text = ""
            End If
        End With

        lblTitu.Text = "Registro Seleccionado: " + usrClie.Apel + " - " + txtLega.Text
        mSetearDA()
        mSetearEditor("", False)
        mMostrarPanel(True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnTarjId.Text = ""
        hdnAlumId.Text = ""
        lblTitu.Text = ""
        txtLega.Text = ""
        usrFactClie.Limpiar()
            txtIngrFecha.Fecha = Today
            txtObse.Text = ""
        lblBaja.Text = ""
        lblDebAuto.Text = ""
        cmbBeca.Limpiar()


        usrClie.Limpiar()



        grdEstu.CurrentPageIndex = 0
        grdExpl.CurrentPageIndex = 0
        grdFunc.CurrentPageIndex = 0
        grdOcup.CurrentPageIndex = 0

        mCrearDataSet("")


        mLimpiarEstudio()
        mLimpiarExplotacion()
        mLimpiarFunc()
        mLimpiarOcupacion()

        mSetearEditor("", True)
        mShowTabs(1)
    End Sub

    Private Sub mCerrar()
        mLimpiar()
        mConsultar()
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        Dim lbooVisiOri As Boolean = panDato.Visible

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        btnAgre.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
            grdDato.DataBind()

            mShowTabs(1)
        Else
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing
        End If

        tabLinks.Visible = pbooVisi

        If lbooVisiOri And Not pbooVisi Then
            mLimpiarFiltros()
        End If
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panEstu.Visible = False
        panExpl.Visible = False
        panFunc.Visible = False
        panOcup.Visible = False
        panCabecera.Visible = False

        lnkCabecera.Font.Bold = False
        lnkEstu.Font.Bold = False
        lnkExplo.Font.Bold = False
        lnkFunc.Font.Bold = False
        lnkOcup.Font.Bold = False

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Text = "Datos del Alumno "
            Case 2
                panEstu.Visible = True
                lnkEstu.Font.Bold = True
                lblTitu.Text = "Estudios del Alumno: " & usrClie.Apel
                grdEstu.Visible = True
            Case 3
                panOcup.Visible = True
                lnkOcup.Font.Bold = True
                lblTitu.Text = "Ocupaciones del Alumno: " & usrClie.Apel
                grdOcup.Visible = True
            Case 4
                panFunc.Visible = True
                lnkFunc.Font.Bold = True
                lblTitu.Text = "Cargos del Alumno: " & usrClie.Apel
                grdFunc.Visible = True
            Case 5
                panExpl.Visible = True
                lnkExplo.Font.Bold = True
                lblTitu.Text = "Explotaciones del Alumno: " & usrClie.Apel
                grdFunc.Visible = True
        End Select
    End Sub

    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub

    Private Sub lnkEstu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEstu.Click
        mShowTabs(2)
    End Sub

    Private Sub lnkOcup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOcup.Click
        mShowTabs(3)
    End Sub

    Private Sub lnkFunc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFunc.Click
        mShowTabs(4)
    End Sub

    Private Sub lnkExplo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExplo.Click
        mShowTabs(5)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim lstrId, lstrLega As String
            mGuardarDatos()

            Dim lobjAlumnos As New SRA_Neg.Alumnos(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            lstrId = lobjAlumnos.Alta()

            If mstrClieDerivCtrl <> "" AndAlso Request.QueryString("ctrlId").ToString <> "" Then
                lstrLega = clsSQLServer.gObtenerValorCampo(mstrConn, mstrTabla, lstrId, "alum_lega").ToString
                mRetorno(lstrLega)
            Else
                mConsultar()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            mGuardarDatos()

            Dim lobjAlumnos As New SRA_Neg.Alumnos(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            lobjAlumnos.Modi()

            If mstrClieDerivCtrl <> "" AndAlso Request.QueryString("ctrlId").ToString <> "" Then
                mRetorno(txtLega.Text)
                Return
            End If

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjAlumnos As New SRA_Neg.Alumnos(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjAlumnos.Baja(hdnId.Text)

            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

        Private Sub mGuardarDatos()
            mValidarDatos()
            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("alum_inse_id") = IIf(mintInse.ToString.Length > 0, mintInse, DBNull.Value)
                .Item("alum_clie_id") = IIf(usrClie.Valor.ToString.Trim.Length > 0, usrClie.Valor, DBNull.Value)
                .Item("alum_lega") = IIf(txtLega.Valor.Trim.Length > 0, txtLega.Valor, DBNull.Value)
                .Item("alum_fact_clie_id") = IIf(usrFactClie.Valor.ToString.Length > 0, usrFactClie.Valor, DBNull.Value)
                .Item("alum_ingr_fecha") = IIf(txtIngrFecha.Fecha.Trim.Length > 0, txtIngrFecha.Fecha, DBNull.Value)
                .Item("alum_obse") = txtObse.Valor
                .Item("alum_baja_fecha") = DBNull.Value
                .Item("alum_beca_id") = IIf(cmbBeca.Valor.ToString.Length > 0, cmbBeca.Valor, DBNull.Value)
            End With
        End Sub

    Private Sub mMostrarDA(ByVal lboolMostrar As Boolean)
        lblDebAuto.Text = ""

        If lboolMostrar Then
            lblDA.Text = ""
            btnDA.InnerHtml = "Ver. D.Auto."
        Else
            lblDA.Text = "NO"
            btnDA.InnerHtml = "Ver D.Auto."
        End If
    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Public Overrides Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrClieEstu
        mdsDatos.Tables(2).TableName = mstrClieOcup
        mdsDatos.Tables(3).TableName = mstrClieFunc
        mdsDatos.Tables(4).TableName = mstrClieExpl


        With mdsDatos.Tables(mstrTabla).Rows(0)
            If .IsNull("alum_id") Then
                .Item("alum_id") = -1
            End If
        End With

        mConsultarEstu()

        mConsultarOcup()

        mConsultarFunc()

        mConsultarExpl()

        Session(mstrTabla) = mdsDatos
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
    Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(Columnas.Id).Text)



        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mSeleccionar(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mRetorno(E.Item.Cells(Columnas.Lega).Text)
    End Sub

    Public Overrides Sub mRetorno(ByVal pstrLega As String)
        Dim lsbMsg As New StringBuilder
        lsbMsg.Append("<SCRIPT language='javascript'>")
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", clsWeb.gValorParametro(Request.QueryString("ctrlId")), pstrLega))
        lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].onchange();", clsWeb.gValorParametro(Request.QueryString("ctrlId"))))
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)

        Session(mstrTabla) = Nothing
        mdsDatos = Nothing
    End Sub
#End Region

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub txtLegaFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLegaFil.TextChanged
        Try
            mConsultar()

            With DirectCast(grdDato.DataSource, DataSet).Tables(0)
                If .Rows.Count = 1 Then
                    mCargarDatos(.Rows(0).Item("alum_id"))
                ElseIf .Rows.Count = 0 Then
                    Throw New AccesoBD.clsErrNeg("Nro. de Legago Inexistente")
                End If
            End With

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mConsultar()
        Try
            Dim lstrCmd As String

            lstrCmd = ObtenerSql(mstrValorId, txtApelFil.Text, usrClieFil.Valor.ToString, txtLegaFil.Valor.ToString, _
                                 txtCuitFil.TextoPlano.ToString, cmbDocuTipoFil.Valor.ToString, _
                                 txtDocuNumeFil.Valor.ToString, mintInse.ToString, mstrClieDerivCtrl)

            'clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

'    Public Shared Function ObtenerSql(ByVal pstrValorId As String, ByVal pstrApel As String, ByVal pstrClieId As String, ByVal pstrLega As String, ByVal pstrCuit As String, ByVal pstrDocuTipo As String, ByVal pstrDocuNume As String, ByVal pstrInse As String, ByVal pstrClieDerivCtrl As String) As String
'        Dim lstrCmd As New StringBuilder
'
'        lstrCmd.Append("exec alumnos_consul")
'
'        If pstrValorId <> "" Then
'            lstrCmd.Append(" @alum_id=" + pstrValorId)
'        Else
'
'            lstrCmd.Append(" @alum_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
'            lstrCmd.Append(", @alum_clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
'            lstrCmd.Append(", @alum_lega=" + clsSQLServer.gFormatArg(pstrLega, SqlDbType.Int))
'            lstrCmd.Append(", @alum_inse_id=" + clsSQLServer.gFormatArg(pstrInse, SqlDbType.Int))
'            lstrCmd.Append(", @clie_cuit=" + clsSQLServer.gFormatArg(pstrCuit, SqlDbType.Int))
'            lstrCmd.Append(", @clie_doti_id=" + clsSQLServer.gFormatArg(pstrDocuTipo, SqlDbType.Int))
'            lstrCmd.Append(", @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNume, SqlDbType.Int))
'
'            If pstrClieDerivCtrl <> "" Then
'                lstrCmd.Append(", @alum_esta_id=" + CStr(SRA_Neg.Constantes.Estados.Alumnos_Vigente))
'            End If
'        End If
'
'        Return (lstrCmd.ToString)
'    End Function

    Private Sub mLimpiarFiltros()
        hdnValorId.Text = "-1"  'para que ignore el id que vino del control
        mstrValorId = ""
        txtApelFil.Text = ""
        txtCuitFil.Text = ""
        cmbDocuTipoFil.Limpiar()
        txtDocuNumeFil.Text = ""
        txtLegaFil.Text = ""
        usrClieFil.Limpiar()
    End Sub

    Private Sub usrClieFil_Cambio(ByVal sender As Object) Handles usrClieFil.Cambio
        mConsultar()
    End Sub

#Region "Detalle"
    Public Overrides Sub mEditarDatosEstu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrEscl As DataRow

            hdnEsclId.Text = E.Item.Cells(1).Text
            ldrEscl = mdsDatos.Tables(mstrClieEstu).Select("escl_id=" & hdnEsclId.Text)(0)

            With ldrEscl
                cmbEstu.Valor = .Item("escl_estu_id")
            End With

            mSetearEditor(mstrClieEstu, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mEditarDatosOcup(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrOcup As DataRow

            hdnOcclId.Text = E.Item.Cells(1).Text
            ldrOcup = mdsDatos.Tables(mstrClieOcup).Select("occl_id=" & hdnOcclId.Text)(0)

            With ldrOcup
                cmbOcup.Valor = .Item("occl_Ocup_id")
            End With

            mSetearEditor(mstrClieOcup, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mEditarDatosExpl(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrExpl As DataRow

            hdnExclId.Text = E.Item.Cells(1).Text
            ldrExpl = mdsDatos.Tables(mstrClieExpl).Select("excl_id=" & hdnExclId.Text)(0)

            With ldrExpl
                cmbExti.Valor = .Item("excl_exti_id")
                cmbExclPais.Valor = .Item("_pcia_pais_id")
                mCargarProvincias()
                cmbExclPcia.Valor = .Item("excl_pcia_id")
                mCargarLocalidades()
                    cmbExclLoca.Valor = IIf(.Item("excl_loca_id").ToString.Length > 0, .Item("excl_loca_id"), DBNull.Value)
            End With

            mSetearEditor(mstrClieExpl, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mEditarDatosFunc(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrFunc As DataRow

            hdnFuclId.Text = E.Item.Cells(1).Text
            ldrFunc = mdsDatos.Tables(mstrClieFunc).Select("fucl_id=" & hdnFuclId.Text)(0)

            With ldrFunc
                cmbFunc.Valor = .Item("fucl_Func_id")
            End With

            mSetearEditor(mstrClieFunc, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub grdEstu_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdEstu.EditItemIndex = -1
            If (grdEstu.CurrentPageIndex < 0 Or grdEstu.CurrentPageIndex >= grdEstu.PageCount) Then
                grdEstu.CurrentPageIndex = 0
            Else
                grdEstu.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultarEstu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub grdOcup_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdOcup.EditItemIndex = -1
            If (grdOcup.CurrentPageIndex < 0 Or grdOcup.CurrentPageIndex >= grdOcup.PageCount) Then
                grdOcup.CurrentPageIndex = 0
            Else
                grdOcup.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultarOcup()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub grdFunc_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdFunc.EditItemIndex = -1
            If (grdFunc.CurrentPageIndex < 0 Or grdFunc.CurrentPageIndex >= grdFunc.PageCount) Then
                grdFunc.CurrentPageIndex = 0
            Else
                grdFunc.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultarFunc()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub grdExpl_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdExpl.EditItemIndex = -1
            If (grdExpl.CurrentPageIndex < 0 Or grdExpl.CurrentPageIndex >= grdExpl.PageCount) Then
                grdExpl.CurrentPageIndex = 0
            Else
                grdExpl.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultarExpl()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarEstudio()
        Try
            mGuardarDatosEscl()

            mLimpiarEstudio()

            mConsultarEstu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarFunc()
        Try
            mGuardarDatosFunc()

            mLimpiarFunc()

            mConsultarFunc()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mActualizarOcupacion()
        Try
            mGuardarDatosOcup()

            mLimpiarOcupacion()

            mConsultarOcup()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarExplotacion()
        Try
            mGuardarDatosExpl()

            mLimpiarExplotacion()

            mConsultarExpl()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mGuardarDatosEscl()
        Dim ldrDatos As DataRow

            If cmbEstu.Valor Is DBNull.Value Or cmbEstu.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Estudio.")
            End If

        If mdsDatos.Tables(mstrClieEstu).Select("escl_estu_id=" & cmbEstu.Valor & " AND escl_id <> " & clsSQLServer.gFormatArg(hdnEsclId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
            Throw New AccesoBD.clsErrNeg("Estudio existente.")
        End If

        If hdnEsclId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrClieEstu).NewRow
            ldrDatos.Item("escl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieEstu), "escl_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrClieEstu).Select("escl_id=" & hdnEsclId.Text)(0)
        End If

        With ldrDatos
            .Item("escl_Estu_id") = cmbEstu.Valor
            .Item("_Estu_desc") = cmbEstu.SelectedItem.Text
        End With

        If hdnEsclId.Text = "" Then
            mdsDatos.Tables(mstrClieEstu).Rows.Add(ldrDatos)
        End If
    End Sub


    Private Sub mGuardarDatosFunc()
        Dim ldrDatos As DataRow

            If cmbFunc.Valor Is DBNull.Value Or cmbFunc.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la Funci�n")
            End If

        If mdsDatos.Tables(mstrClieFunc).Select("fucl_Func_id=" & cmbFunc.Valor & " AND fucl_id <> " & clsSQLServer.gFormatArg(hdnFuclId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
            Throw New AccesoBD.clsErrNeg("Funci�n existente.")
        End If

        If hdnFuclId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrClieFunc).NewRow
            ldrDatos.Item("fucl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieFunc), "fucl_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrClieFunc).Select("fucl_id=" & hdnFuclId.Text)(0)
        End If

        With ldrDatos
            .Item("fucl_Func_id") = cmbFunc.Valor
            .Item("_Func_desc") = cmbFunc.SelectedItem.Text
        End With

        If hdnFuclId.Text = "" Then
            mdsDatos.Tables(mstrClieFunc).Rows.Add(ldrDatos)
        End If
    End Sub

    Private Sub mGuardarDatosOcup()
        Dim ldrDatos As DataRow

            If cmbOcup.Valor Is DBNull.Value Or cmbOcup.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la Ocupaci�n.")
            End If

        If mdsDatos.Tables(mstrClieOcup).Select("occl_ocup_id=" & cmbOcup.Valor & " AND occl_id <> " & clsSQLServer.gFormatArg(hdnOcclId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
            Throw New AccesoBD.clsErrNeg("Ocupaci�n existente.")
        End If

        If hdnOcclId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrClieOcup).NewRow
            ldrDatos.Item("occl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieOcup), "occl_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrClieOcup).Select("occl_id=" & hdnOcclId.Text)(0)
        End If

        With ldrDatos
            .Item("occl_ocup_id") = cmbOcup.Valor
            .Item("_ocup_desc") = cmbOcup.SelectedItem.Text
        End With

        If hdnOcclId.Text = "" Then
            mdsDatos.Tables(mstrClieOcup).Rows.Add(ldrDatos)
        End If
    End Sub

    Private Sub mGuardarDatosExpl()
        Dim ldrDatos As DataRow

            If cmbExti.Valor Is DBNull.Value Or cmbExti.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Tipo.")
            End If

            If cmbExclPcia.Valor Is DBNull.Value Or cmbExclPcia.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar una Provincia.")
            End If

        If hdnExclId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrClieExpl).NewRow
            ldrDatos.Item("excl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieExpl), "excl_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrClieExpl).Select("excl_id=" & hdnExclId.Text)(0)
        End If

        With ldrDatos
                .Item("excl_exti_id") = IIf(cmbExti.Valor.Length > 0, cmbExti.Valor, DBNull.Value)
                .Item("excl_loca_id") = IIf(cmbExclLoca.Valor.Length > 0, cmbExclLoca.Valor, DBNull.Value)
                .Item("excl_pcia_id") = IIf(cmbExclPcia.Valor.Length > 0, cmbExclPcia.Valor, DBNull.Value)
                .Item("_pcia_pais_id") = IIf(cmbExclPais.Valor.Length > 0, cmbExclPais.Valor, DBNull.Value)
            .Item("_exti_desc") = cmbExti.SelectedItem.Text
            .Item("_pcia_desc") = cmbExclPcia.SelectedItem.Text

            If cmbExclLoca.Valor Is DBNull.Value Then
                .Item("_loca_desc") = DBNull.Value
            Else
                .Item("_loca_desc") = cmbExclLoca.SelectedItem.Text
            End If
        End With

        If hdnExclId.Text = "" Then
            mdsDatos.Tables(mstrClieExpl).Rows.Add(ldrDatos)
        End If

    End Sub


    Private Sub mLimpiarEstudio()
        hdnEsclId.Text = ""
        cmbEstu.Limpiar()

        mSetearEditor(mstrClieEstu, True)
    End Sub

    Private Sub mLimpiarOcupacion()
        hdnOcclId.Text = ""
        cmbOcup.Limpiar()
        mSetearEditor(mstrClieOcup, True)
    End Sub

    Private Sub mLimpiarExplotacion()
        hdnExclId.Text = ""
        cmbExti.Limpiar()
        cmbExclPais.Limpiar()

        mCargarProvincias()

        mSetearEditor(mstrClieExpl, True)
    End Sub

    Private Sub mLimpiarFunc()
        hdnFuclId.Text = ""
        cmbFunc.Limpiar()
        mSetearEditor(mstrClieFunc, True)
    End Sub

#End Region



    Private Sub btnLimpOcup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpOcup.Click
        mLimpiarOcupacion()
    End Sub

    Private Sub btnLimpFunc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpFunc.Click
        mLimpiarFunc()
    End Sub

    Private Sub btnLimpEstu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpEstu.Click
        mLimpiarEstudio()
    End Sub

    Private Sub btnLimpExpl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpExpl.Click
        mLimpiarExplotacion()
    End Sub

    Private Sub btnBajaEstu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaEstu.Click
        Try
            mdsDatos.Tables(mstrClieEstu).Select("escl_id=" & hdnEsclId.Text)(0).Delete()

            mConsultarEstu()

            mLimpiarEstudio()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBajaOcup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaOcup.Click
        Try
            mdsDatos.Tables(mstrClieOcup).Select("occl_id=" & hdnOcclId.Text)(0).Delete()

            mConsultarOcup()

            mLimpiarOcupacion()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBajaFunc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaFunc.Click
        Try
            mdsDatos.Tables(mstrClieFunc).Select("fucl_id=" & hdnFuclId.Text)(0).Delete()

            mConsultarFunc()

            mLimpiarFunc()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBajaExpl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaExpl.Click
        Try
            mdsDatos.Tables(mstrClieExpl).Select("excl_id=" & hdnExclId.Text)(0).Delete()

            mConsultarExpl()

            mLimpiarExplotacion()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAltaEstu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaEstu.Click
        mActualizarEstudio()
    End Sub

    Private Sub btnAltaFunc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaFunc.Click
        mActualizarFunc()
    End Sub

    Private Sub btnAltaOcup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaOcup.Click
        mActualizarOcupacion()
    End Sub

    Private Sub btnAltaExpl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaExpl.Click
        mActualizarExplotacion()
    End Sub

    Private Sub btnModiEstu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiEstu.Click
        mActualizarEstudio()
    End Sub

    Private Sub btnModiOcup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiOcup.Click
        mActualizarOcupacion()
    End Sub

    Private Sub btnModiFunc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiFunc.Click
        mActualizarFunc()
    End Sub

    Private Sub btnModiExpl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiExpl.Click
        mActualizarExplotacion()
    End Sub


    Private Sub mConsultarEstu()
        grdEstu.DataSource = mdsDatos.Tables(mstrClieEstu)
        grdEstu.DataBind()
    End Sub

    Private Sub mConsultarOcup()
        grdOcup.DataSource = mdsDatos.Tables(mstrClieOcup)
        grdOcup.DataBind()
    End Sub

    Private Sub mConsultarFunc()
        grdFunc.DataSource = mdsDatos.Tables(mstrClieFunc)
        grdFunc.DataBind()
    End Sub

    Private Sub mConsultarExpl()
        grdExpl.DataSource = mdsDatos.Tables(mstrClieExpl)
        grdExpl.DataBind()
    End Sub

    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub hdnAlumId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnAlumId.TextChanged
        mCargarDatos(hdnAlumId.Text)
    End Sub

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        Try
            mAgregar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub

    'Public Sub New()

    'End Sub

    'Protected Overrides Sub Finalize()
    '   MyBase.Finalize()
    'End Sub



End Class
End Namespace
