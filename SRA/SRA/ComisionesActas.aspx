<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ComisionesActas" CodeFile="ComisionesActas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Actas de Comisiones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 100%; HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Actas de Comisiones</asp:label></TD>
								<TD vAlign="top" align="right">
									<asp:ImageButton id="imgClosePrincipal" runat="server" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"
										CausesValidation="False"></asp:ImageButton></TD>
							</TR>
							<TR>
								<TD vAlign="top" colspan="3" align="center">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="crac_id" ReadOnly="True" HeaderText="C�digo"></asp:BoundColumn>
											<asp:BoundColumn DataField="crac_tema" ReadOnly="True" HeaderText="Tema">
												<HeaderStyle Width="60%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_criador" ReadOnly="True" HeaderText="Criador">
												<HeaderStyle Width="40%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid>
								</TD>
							</TR>
							<TR>
								<TD vAlign="middle" colspan="3">
									<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
										IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
										ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ForeColor="Transparent" ImageDisable="btnNuev0.gif"
										ToolTip="Agregar un Nuevo Acta"></CC1:BOTONIMAGEN></TD>
							<TR>
								<TD align="center" colspan="3">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" Height="116px">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD vAlign="top" align="right" colSpan="2">
														<asp:ImageButton id="imgClose" runat="server" CausesValidation="False" ImageUrl="images\Close.bmp"
															ToolTip="Cerrar"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 10%; HEIGHT: 16px" vAlign="top" align="right">
														<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 90%; HEIGHT: 16px" vAlign="top">
														<UC1:CLIE id="usrCria" runat="server" Alto="560" Criador="True" Ancho="780" Saltos="1,1,1"
															Tabla="Criadores" MuestraDesc="True"></UC1:CLIE></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 10%; HEIGHT: 16px" vAlign="top" align="right">
														<asp:Label id="lblTema" runat="server" cssclass="titulo">Tema:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 90%; HEIGHT: 16px" vAlign="top">
														<cc1:textboxtab id="txtTema" runat="server" cssclass="textolibre" height="100px" Rows="10" TextMode="MultiLine"
															Width="90%"></cc1:textboxtab></TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
												</TR>
												<TR>
													<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCdocId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		</SCRIPT>
	</BODY>
</HTML>
