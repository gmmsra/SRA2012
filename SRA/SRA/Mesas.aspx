<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Mesas" CodeFile="Mesas.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Mesas de Ex�men</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');Imprimir();" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Mesas de Examen</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 645px">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 82%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="80px" MaxValor="2099"
																				AceptaNull="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblMateFil" runat="server" cssclass="titulo">Materia:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 82%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbMateFil" runat="server" Width="350px" AceptaNull="false"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">&nbsp;</TD>
																		<TD style="WIDTH: 82%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
										CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page"
										AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="meex_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="meex_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="meex_desc" HeaderText="Descripci�n"></asp:BoundColumn>
											<asp:BoundColumn DataField="_mate_desc" HeaderText="Materia"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
										BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
										ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Mesa de Ex�men"></CC1:BOTONIMAGEN></TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Mesa</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkProf" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" CausesValidation="False" Height="21px"> Profesores</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkAlum" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" CausesValidation="False" Height="21px"> Alumnos</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<cc1:textboxtab id="txtDesc" runat="server" cssclass="cuadrotexto" Width="460px" Obligatorio="true"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right"></TD>
																		<TD>
																			<asp:RadioButton id="rbtnCarr" onclick="mSetearMaterias();" runat="server" cssclass="titulo" Checked="True"
																				GroupName="RadioGroup1" Text="Carrera"></asp:RadioButton>
																			<asp:RadioButton id="rbtnMate" onclick="mSetearMaterias();" runat="server" cssclass="titulo" GroupName="RadioGroup1"
																				Text="Materia"></asp:RadioButton></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:label id="lblAnio" runat="server" cssclass="titulo">A�o:</asp:label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtAnio" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="2099"
																				AceptaNull="False" MaxLength="4" onchange="javascript:txtAnio_Change(this);"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 17px" align="right">
																			<asp:label id="lblCicl" runat="server" cssclass="titulo">Ciclo:</asp:label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px">
																			<cc1:combobox class="combo" id="cmbCicl" runat="server" Width="480px" AceptaNull="false" onchange="javascript:cmbCicl_Change(this);"
																				NomOper="ciclos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:label id="lblComi" runat="server" cssclass="titulo">Comisi�n:</asp:label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbComi" runat="server" Width="480px" onchange="javascript:cmbComi_Change(this);"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 11px" align="right">
																			<asp:Label id="lblMate" runat="server" cssclass="titulo">Materia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 11px">
																			<asp:Label id="txtMate" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblMateSola" runat="server" cssclass="titulo">Materia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbMate" runat="server" Width="480px" AceptaNull="False" Obligatorio="false"
																				onchange="javascript:cmbMate_Change(this);"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:imagebutton id="btnList" runat="server" ImageUrl="./imagenes/prints1.GIF" ToolTip="Imprimir"></asp:imagebutton></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panProf" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TableProf" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdProf" runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				OnPageIndexChanged="grdProf_PageChanged" OnEditCommand="mEditarDatosProf" HorizontalAlign="Center"
																				CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="mepr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_prof_nyap" HeaderText="Profesor"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblProf" runat="server" cssclass="titulo">Profesor:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbProf" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaProf" runat="server" cssclass="boton" Width="100px" Text="Agregar Prof."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaProf" runat="server" cssclass="boton" Width="100px" Text="Eliminar Prof."></asp:Button>&nbsp;
																			<asp:Button id="btnModiProf" runat="server" cssclass="boton" Width="100px" Text="Modificar Prof."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpProf" runat="server" cssclass="boton" Width="100px" Text="Limpiar Prof."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panAlum" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="Table4" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdAlum" runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				OnPageIndexChanged="grdAlum_PageChanged" OnEditCommand="mEditarDatosAlum" HorizontalAlign="Center"
																				CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="meal_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_alum_lega" HeaderText="Legajo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_alum_desc" HeaderText="Alumno"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="gene_fact" HeaderText="Generar Factura"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" vAlign="top" align="right" width="15%">
																			<asp:Label id="lblAlum" runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 27px">
																			<UC1:CLIE id="usrAlum" runat="server" SoloBusq="True" FilCUIT="True" FilDocuNume="True" CampoVal="Alumno"
																				Ancho="800" FilLegaNume="True" Saltos="1,1,1" Tabla="Alumnos"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="60%" border="0">
																				<TR>
																					<TD style="HEIGHT: 20px" align="center">
																						<asp:CheckBox id="chkRecu" onclick="javascript:mSetearFC();" Text="Recuperatorio" Runat="server"
																							CssClass="titulo"></asp:CheckBox></TD>
																					<TD style="HEIGHT: 20px">
																						<asp:CheckBox id="chkGeneFact" Text="Generar Factura" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																				</TR>
																				<TR>
																					<TD></TD>
																					<TD>
																						<asp:CheckBox id="chkExceCC" Text="Excepci�n Cta. Cte." Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																				</TR>
																				<TR>
																					<TD height="30"></TD>
																					<TD height="30"><BUTTON class="boton" id="btnGeneLista" style="WIDTH: 100px" onclick="btnGeneLista_click();"
																							type="button" runat="server" value="Detalles">Generar Lista</BUTTON></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaAlum" runat="server" cssclass="boton" Width="100px" Text="Agregar Alum."></asp:Button>&nbsp;
																			<asp:Button id="btnBajaAlum" runat="server" cssclass="boton" Width="100px" Text="Eliminar Alum."></asp:Button>&nbsp;
																			<asp:Button id="btnModiAlum" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Modificar Alum."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpAlum" runat="server" cssclass="boton" Width="100px" Text="Limpiar Alum."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											<P></P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										<DIV></DIV>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
						<P></P>
						<DIV></DIV>
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
			<asp:textbox id="hdnId" runat="server"></asp:textbox>
			<asp:textbox id="hdnMeprId" runat="server"></asp:textbox>
			<asp:textbox id="hdnMealId" runat="server"></asp:textbox>
			<asp:textbox id="lblMens" runat="server"></asp:textbox>
			<asp:textbox id="hdnSeleMate" runat="server"></asp:textbox>
			<asp:textbox id="hdnSeleComi" runat="server"></asp:textbox>
			<asp:textbox id="hdnSeleCicl" runat="server"></asp:textbox>
			<asp:textbox id="hdnInse" runat="server"></asp:textbox>
			<asp:textbox id="hdnDatosPop" runat="server"></asp:textbox>
			<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
			<asp:textbox id="hdnImprimir" runat="server"></asp:textbox>
			<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		function mRecargarCombos()
		{
			if (document.all('txtAnio').value !='')
			{				
				var lstrAnio = document.all('txtAnio').value;
				var lstrInse = document.all('hdnInse').value;
				LoadComboXML("ciclos_cargar", lstrInse+','+lstrAnio, "cmbCicl", "T");
				var lstrCiclo = document.all('hdnSeleCicl').value;
				if (lstrCiclo != '')
				{
					document.all('cmbCicl').value = lstrCiclo;
					LoadComboXML("comisiones_cargar","@cicl_id="+lstrCiclo+",@mate_cali=1", "cmbComi", "T");
					document.all('cmbComi').value = document.all('hdnSeleComi').value;
					document.all('txtMate').innerHTML = LeerCamposXML("comisiones", document.all('hdnSeleComi').value, "_mate_desc");
				}
		    }			
		}
		function mSetearFC()
		{
			document.all["chkRecu"].disabled = (document.all('hdnMealId').value != '');
			if (document.all["chkRecu"].disabled)
				document.all["chkRecu"].checked = false;
			document.all["chkGeneFact"].disabled = (!document.all["chkRecu"].checked);
			document.all["chkExceCC"].disabled = (!document.all["chkRecu"].checked);
			if (document.all["chkGeneFact"].disabled)
				document.all["chkGeneFact"].checked = false;
			if (document.all["chkRecu"].checked)
				document.all["chkGeneFact"].checked = true;
			if (document.all["chkExceCC"].disabled)
				document.all["chkExceCC"].checked = false;
		}
		function mSetearMaterias()
		{
			var lstrTipo = '';
			var lbooHabi = true;
			if (document.all('rbtnMate').disabled && document.all('rbtnCarr').disabled)
				lbooHabi = false;
			if (lbooHabi)
			{
				if (document.all('rbtnCarr').checked)
					lstrTipo = 'C';
				else
					lstrTipo = 'M';
				document.all["cmbMate"].disabled=(lstrTipo=="C");
				if(document.all["cmbMate"].disabled)
					document.all["cmbMate"].selectedIndex=-1;
				document.all["txtAnio"].disabled=(lstrTipo=="M");
				document.all["cmbCicl"].disabled=(lstrTipo=="M");
				document.all["cmbComi"].disabled=(lstrTipo=="M");
				if (lstrTipo == "M")
				{
					document.all["txtAnio"].value='';
					document.all["cmbCicl"].selectedIndex=-1;
					document.all["cmbComi"].selectedIndex=-1;
					document.all["txtMate"].innerHTML='';
				}
			}
			else
			{
				document.all["txtAnio"].disabled=true;
				document.all["cmbCicl"].disabled=true;
				document.all["cmbComi"].disabled=true;
				document.all["cmbMate"].disabled=true;
			}
		}
		function txtAnio_Change(pAnio, pInse)
		{
		    var sFiltro = document.all('hdnInse').value + "," ;		    
		    if (pAnio.value=="")
		       sFiltro =  sFiltro + "-1";
		    else 
		       sFiltro = sFiltro + pAnio.value ;		       
		    LoadComboXML("ciclos_cargar", sFiltro, "cmbCicl", "T");
		    LoadComboXML("comisiones_cargar","@cicl_id=0,@mate_cali=1", "cmbComi", "T");		    
		    document.all('hdnSeleCicl').value = '';
		    document.all('hdnSeleMate').value = '';
		    document.all('hdnSeleComi').value = '';		    
		}		
		function cmbMate_Change(pValor)
		{
			document.all('hdnSeleMate').value = pValor.value;
		}		
		function cmbComi_Change(pValor)
		{
			document.all('hdnSeleComi').value = pValor.value;
			var sRet = LeerCamposXML("comisiones", pValor.value, "_mate_desc");
			document.all('txtMate').innerHTML = sRet;
		}		
		function cmbCicl_Change(pCiclo)
		{
		    var sFiltro = "@mate_inse_id=" + document.all('hdnInse').value;
		    if(pCiclo.value!="")
				sFiltro = sFiltro + ",@cicl_id=" + pCiclo.value;
		    document.all('hdnSeleCicl').value = pCiclo.value;
		    LoadComboXML("comisiones_cargar","@cicl_id=" + pCiclo.value, "cmbComi", "T");		   
		}		
		var vVentanas = new Array(null);	
		function btnGeneLista_click() 
		{
			var lstrMate = document.all('hdnSeleMate').value;
			var lstrComi = document.all('hdnSeleComi').value;
			if (lstrMate != '' || lstrComi != '')
			{
				if (lstrComi == '') 
					lstrComi = '0';
  				gAbrirVentanas("Alumnos_Pop.aspx?inse=" + document.all("hdnInse").value + "&mate=" + lstrMate + "&comi=" + lstrComi, 0, "580","330","50","50");
			}
			else
			{
				alert('Debe seleccionar una materia.');
			}
		}		
		if (document.all["chkRecu"]!= null)
			mSetearFC();
		if (document.all('rbtnCarr')!=null)
			mSetearMaterias();
		if (document.all('cmbCicl')!=null)
			mRecargarCombos();
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtFecha"]!= null)
			document.all["txtFecha"].focus();
		function Imprimir()
		{
			var strNume;
			if(document.all("hdnImprimir").value!="")
			{
				var vFCs = document.all("hdnImprimir").value.split(",");
				document.all("hdnImprimir").value = "";

				if (window.confirm("�Desea imprimir las facturas generadas?"))
				{
					for(iFc=0; iFc<vFCs.length; iFc++)
					{
						strNume=LeerCamposXML("comprobantesX", vFCs[iFc], "numero");
						if (strNume == '')
							return;
						if (window.confirm("�Desea imprimir la factura " + strNume + "?"))
						{
							try{
								var sRet = mImprimirCopias(2,'O',vFCs[iFc],"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original

								if(sRet=="0") return(sRet);
								
								if (window.confirm("�Se imprimio la factura correctamente?"))
								{
									var lstrCopias = LeerCamposXML("comprobantes_imprimir", vFCs[iFc], "copias");

									if (lstrCopias != "0" && lstrCopias != "1")
										sRet = mImprimirCopias(lstrCopias,'D',vFCs[iFc],"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias

									EjecutarMetodoXML("Utiles.ModiImpreso", vFCs[iFc] + ";1");
								}
								else
									document.all("hdnImprId").value = '';
							}
							catch(e)
							{
								document.all("hdnImprId").value = '';
								alert("Error al intentar efectuar la impresi�n");
							}
						}
					}
				}
			}
		}			
		</SCRIPT>
		</TD></TR></TBODY></TABLE>
	</BODY>
</HTML>
