<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ArancelesRangos" CodeFile="ArancelesRangos.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Rangos para aranceles de RRGG</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Rangos para aranceles de RRGG</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" BorderStyle="Solid"
										Width="100%">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																	IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblDescFil" runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtDescFil" runat="server" cssclass="cuadrotexto" Width="250px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 13.76%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="2"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="raac_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="raac_desc" ReadOnly="True" HeaderText="Descripción">
												<HeaderStyle Width="90%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="2">
									<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
													ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif"
													ToolTip="Agregar un Nuevo Rango"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<tr>
								<TD colSpan="2" height="10"></TD>
							</tr>
							<TR>
								<TD align="center" colSpan="2"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Visible="False" Height="124%">
										<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<TR>
												<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
												<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
												<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> General</asp:linkbutton></TD>
												<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkRangos" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Rangos</asp:linkbutton></TD>
												<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" Height="116px">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 100%" vAlign="top" align="right">
													<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%">
													<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD align="right">
																	<asp:label id="lblDesc" runat="server" cssclass="titulo">Descripción:</asp:label></TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="328px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="2">
													<asp:panel id="panRangos" runat="server" cssclass="titulo" Width="100%" Visible="False">
														<TABLE id="TablaRangos" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD vAlign="top" align="center" colSpan="2">
																	<asp:datagrid id="grdRangos" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																		AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																		OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatosRangos" AutoGenerateColumns="False"
																		PageSize="20">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																		<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="2%"></HeaderStyle>
																				<ItemTemplate>
																					<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																						<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																					</asp:LinkButton>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="raad_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																			<asp:BoundColumn DataField="raad_desde" ReadOnly="True" HeaderText="Desde mes">
																				<HeaderStyle Width="25%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="raad_hasta" ReadOnly="True" HeaderText="Hasta mes">
																				<HeaderStyle Width="25%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="raad_tope_fecha" ReadOnly="True" HeaderText="Fecha tope" DataFormatString="{0:dd/MM/yyyy}">
																				<HeaderStyle Width="25%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="raad_sobre_fecha" ReadOnly="True" HeaderText="Fecha sobretasa" DataFormatString="{0:dd/MM/yyyy}">
																				<HeaderStyle Width="25%"></HeaderStyle>
																			</asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%" colSpan="2">
																	<TABLE id="TablaDeta" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD align="right" width="20%">
																				<asp:Label id="lblMesDesde" runat="server" cssclass="titulo">Mes desde:</asp:Label></TD>
																			<TD width="30%">
																				<CC1:NUMBERBOX id="txtMesDesde" runat="server" width="20px" cssclass="cuadrotexto" AceptaNull="False"></CC1:NUMBERBOX></TD>
																			<TD align="right" width="30%">
																				<asp:Label id="lblMesHasta" runat="server" cssclass="titulo">Mes hasta:</asp:Label></TD>
																			<TD width="20%">
																				<CC1:NUMBERBOX id="txtMesHasta" runat="server" width="20px" cssclass="cuadrotexto" AceptaNull="False"></CC1:NUMBERBOX></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblFechaTope" runat="server" cssclass="titulo" obligatorio=true>Fecha tope:</asp:Label></TD>
																			<TD>
																				<cc1:DateBox id="txtFechaTope" runat="server" width="68px" cssclass="cuadrotexto" obligatorio= true></cc1:DateBox></TD>
																			<TD align="right">
																				<asp:Label id="lblFechaSobre" runat="server" cssclass="titulo" obligatorio= true>Fecha sobretasa:</asp:Label></TD>
																			<TD>
																				<cc1:DateBox id="txtFechaSobre" runat="server" width="68px" cssclass="cuadrotexto" obligatorio=false></cc1:DateBox></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR height="30">
																<TD align="center" colSpan="2">
																	<asp:Button id="btnAltaRango" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																	<asp:Button id="btnBajaRango" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Baja"></asp:Button>
																	<asp:Button id="btnModiRango" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Modificar"></asp:Button>
																	<asp:Button id="btnLimpRango" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Limpiar"></asp:Button></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																	height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
										</TABLE>
									</asp:panel><ASP:PANEL id="panBotones" Runat="server">
										<TABLE width="100%">
											<TR>
												<TD align="center" colSpan="2">
													<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Baja"></asp:Button>&nbsp;
													<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Modificar"></asp:Button>&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Limpiar"></asp:Button>&nbsp;
												</TD>
											</TR>
										</TABLE>
									</ASP:PANEL>
									<DIV></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnHabComboIVA" runat="server">1</asp:textbox><asp:textbox id="hdnRangoId" runat="server"></asp:textbox><cc1:combobox class="combo" id="cmbFormAux" runat="server" Width="232px"></cc1:combobox></DIV>
		</form>
		<SCRIPT language="javascript">
	if (document.all["editar"]!= null)
		document.location='#editar';
	if (document.all["txtCodi"]!= null)
		document.all["txtCodi"].focus();
		
	function ValorFijoChange()
	{
		if(window.event.srcElement.value!="")
			document.all["cmbTari"].selectedIndex=2;
		else
			document.all["cmbTari"].selectedIndex=1;
	}
	//function mHabilitarCombo(pDesa)
	//{
	//  document.all("cmbGravaTasa").disabled = pDesa;
	// 	 
    //  if(document.all("cmbGravaTasa").disabled)
	//		document.all("cmbGravaTasa").value="";
	// 		  	     	 				         	  
	//}
	
	function mHabilitarCombo(pDesa)
	{
		document.all("cmbGravaTasa").disabled = pDesa;
        if(document.all("cmbGravaTasa").disabled)
		{
			document.all("cmbGravaTasa").value="";
			document.all("hdnHabComboIVA").value="1";
		}
		else
			document.all("hdnHabComboIVA").value="0";
	}
	
	function mMostrarDescripFormula()
	{
		document.all["cmbFormAux"].value = document.all["cmbForm"].value;
		if(document.all["cmbFormAux"].selectedIndex!=-1)
		{
		    if (document.all["cmbFormAux"].selectedIndex!=0)
		    {
			document.all["txtFormDesc"].value = document.all["cmbFormAux"].item(document.all["cmbFormAux"].selectedIndex).text;
			}else
			{
			document.all["txtFormDesc"].value = '';
			}
		} 
		}	 
		</SCRIPT>
	</BODY>
</HTML>
