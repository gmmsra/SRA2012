Namespace SRA

Partial Class EstaCambio
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub
#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_SoliCambios
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Edit = 0
      Id = 1
      Tipo = 2
      Nume = 3
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      If Not usrSoci.Valor Is DBNull.Value Then
         mCargarDatosSocio(usrSoci.Valor)
        End If
        'usrSociFil.mstrConn = mstrConn
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then

            mSetearEventos()
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mCargarCombos()
            mConsultar()

         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnAlta.Attributes.Add("onclick", "if(!mConfirmarSociosRela()) return false;")
      btnModi.Attributes.Add("onclick", "if(!mConfirmarSociosRela()) return false;")
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "estados_socios_cargar ", cmbEsta, "id", "descrip", "S")
   End Sub

#End Region

#Region "Seteo de Controles"

   Public Sub mCargarDatos(ByVal pstrId As String)

      mCrearDataSet(pstrId)
      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("socm_id").ToString()
                usrSoci.Valor = .Item("socm_soci_id").ToString
                txtMoti.Valor = .Item("socm_obse").ToString
            lblFechaAnterior.Text = .Item("_ante_fecha").ToString
            'lblEstadoAnterior.Text = .Item("_esta_actual").ToString        16/02/2011...se reemplazo por sentencia siguiente
            lblEstadoAnterior.Text = .Item("_estado_categoria").ToString
                txtFecha.Fecha = .Item("socm_fecha").ToString
                cmbEsta.Valor = .Item("socm_esta_id").ToString
         txtEsta.Text = .Item("_solicitud").ToString
      End With

      mSetearEditor(False)
      mMostrarPanel(True)

   End Sub

   Private Sub mCerrar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
   End Sub

#End Region

#Region "Opciones de ABM"

   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Alta()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, hdnId.Text, "@tipo='E'")

      If (usrSoci.Valor Is System.DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el socio.")
      End If

      If (txtFecha.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de la solicitud.")
      End If

      If (cmbEsta.Valor Is System.DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el nuevo estado.")
      End If

      If (cmbEsta.SelectedItem.Text = lblEstadoAnterior.Text) Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar un estado diferente al actual.")
      End If

      With ldsEsta.Tables(0).Rows(0)
                .Item("socm_esta_id") = IIf(cmbEsta.Valor.Trim.Length > 0, cmbEsta.Valor, DBNull.Value)
                .Item("socm_soci_id") = IIf(usrSoci.Valor.ToString.Trim.Length > 0, usrSoci.Valor, DBNull.Value)
                .Item("socm_fecha") = IIf(txtFecha.Fecha.Trim.Length > 0, txtFecha.Fecha, DBNull.Value)
         .Item("socm_obse") = txtMoti.Text
      End With

      Return ldsEsta
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, "@tipo='E'")
      mdsDatos.Tables(0).TableName = mstrTabla
      Session(mstrTabla) = mdsDatos
   End Sub

#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(Columnas.Id).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub mCargarDatosSocio(ByVal pintSoci As Integer)
      Dim ldsDatos As DataSet
      Try
         mstrCmd = "exec socio_cate_esta_consul " & pintSoci & ",'E'"
         ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
         If ldsDatos.Tables(0).Rows.Count <> 0 Then
            With ldsDatos.Tables(0).Rows(0)
                        lblEstadoAnterior.Text = .Item("estado").ToString
                        lblFechaAnterior.Text = .Item("esta_fecha").ToString
               If (.Item("esta_reque_apro") = 1) Then
                            lblReqApro.Text = "(Requiere Aprobación)"
               Else
                  lblReqApro.Text = "(Cambio Automático)"
               End If
            End With
        End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul ")
         lstrCmd.Append("@esta = " + cmbEstaFil.Valor)
         lstrCmd.Append(",@tipo = 'E'")
         If Not usrSociFil.Valor Is System.DBNull.Value Then
            lstrCmd.Append(", @socm_soci_id=" + clsSQLServer.gFormatArg(usrSociFil.Valor, SqlDbType.Int))
         End If

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtEsta.Text = ""
      txtFecha.Fecha = Now
      txtMoti.Text = ""
      lblFechaAnterior.Text = ""
      lblEstadoAnterior.Text = ""
      lblReqApro.Text = ""
      cmbEsta.Limpiar()
      usrSoci.Valor = 0
      mSetearEditor(True)
   End Sub

   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      usrSoci.Valor = usrSociFil.Valor
      If Not usrSoci.Valor Is DBNull.Value Then
        mCargarDatosSocio(usrSoci.Valor)
      End If
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()
      usrSociFil.Valor = 0
      cmbEstaFil.SelectedIndex = 0
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
       mLimpiarFiltros()
   End Sub

   Private Sub btnAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

End Class
End Namespace
