<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CuponesRechazo" CodeFile="CuponesRechazo.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Rechazo de Cupones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"> 
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	
	</HEAD>
	<BODY onload="gSetearTituloFrame('');Imprimir();" class="pagina" leftMargin="5" topMargin="5"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Rechazo de Cupones de Tarjeta</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="97%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblBancFil" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbTarjFil" runat="server" Width="240px" AceptaNull="false"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 13.43%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" Tabla="Clientes" Saltos="1,1"
																				FilSociNume="True" MuestraDesc="true" FilDocuNume="True" Ancho="800"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblNumeFil" runat="server" cssclass="titulo">Nro.de Cup�n:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" Width="270"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg"></TD>
																		<TD noWrap align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<asp:checkbox id="chkRech" CssClass="titulo" Runat="server" Text="Traer Rechazados"></asp:checkbox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										OnItemDataBound="mItemDataBound" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="paco_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_nume" HeaderText="Nro.Cupon"></asp:BoundColumn>
											<asp:BoundColumn DataField="clie_apel" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="_moneda" HeaderText="Moneda"></asp:BoundColumn>
											<asp:BoundColumn DataField="_paco_orig_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
										BorderStyle="Solid">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
												<TD vAlign="top" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="2">
													<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
														<TR>
															<TD align="right">
																<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:combobox class="combo" id="cmbTarj" runat="server" Width="100%" enabled="false" onchange="mCargarCuentas();"></cc1:combobox></TD>
															<TD align="right">
																<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro.Cupon:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:textboxtab id="txtNume" runat="server" cssclass="cuadrotextodeshab" Width="100px" enabled="false"></cc1:textboxtab></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblMone" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:combobox class="combo" id="cmbMone" runat="server" cssclass="cuadrotextodeshab" Width="90px"
																	enabled="false" Obligatorio="True"></cc1:combobox></TD>
															<TD align="right">
																<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="True" EsDecimal="true"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
															<TD >
																<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="true"></cc1:DateBox></TD>
														    <TD align="right">
																<asp:Label id="lblNroTarjeta" runat="server" cssclass="titulo">Nro.Tarjeta:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtNroTarjeta" runat="server" cssclass="cuadrotextodeshab" Width="100px" enabled="false"
																	Obligatorio="True" EsDecimal="true"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right" width=120>
																<asp:Label id="lblCuot" runat="server" cssclass="titulo">Cantidad Cuotas:</asp:Label>&nbsp;
															</TD>
															<TD>
																<cc1:textboxtab id="txtCuot" runat="server" cssclass="cuadrotextodeshab" Width="100px" enabled="false"></cc1:textboxtab>
															</TD>
															<TD align=right>
																<asp:Label id="lblAuto" runat="server" cssclass="titulo">C�digo Autorizaci�n:</asp:Label>&nbsp;
															</TD>
															<TD>
																<cc1:textboxtab id="txtAuto" runat="server" cssclass="cuadrotextodeshab" Width="120px" enabled="false"></cc1:textboxtab>
															</TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 9px" vAlign="top" align="right">
																<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
															<TD style="HEIGHT: 9px" colSpan="3">
																<UC1:CLIE id="usrClie" runat="server" AceptaNull="false" Tabla="Clientes" Saltos="1,1" FilSociNume="True"
																	MuestraDesc="true" FilDocuNume="True" Ancho="800" Activo="False"></UC1:CLIE></TD>
														</TR>
														<TR>
															<TD background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD colSpan="4">
																<asp:Label id="Label3" runat="server" cssclass="titulo">&nbsp;&nbsp;Datos del Rechazo</asp:Label>&nbsp;</TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblCompFecha" runat="server" cssclass="titulo">Fecha Valor:</asp:Label>&nbsp;</TD>
															<TD colSpan="3">
																<cc1:DateBox id="txtCompFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
														</TR>
														<TR>
															<TD vAlign="middle" align="center" colSpan="4" height="5">
																<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
									<ASP:PANEL id="panBotones" Runat="server">
										<TABLE width="100%">
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Rechazar" Visible=False CausesValidation="False"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="100px" Text="Anular Rechazo" Visible=False
														CausesValidation="False"></asp:Button>&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Visible=False Text="Cerrar" CausesValidation="False"></asp:Button></TD>
											</TR>
										</TABLE>
									</ASP:PANEL>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImpriTipo" runat="server"></asp:textbox>
				<asp:textbox id="hdnEmctId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimir" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimio" runat="server" AutoPostBack="True"></asp:textbox>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT>
				</OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		 function Imprimir()
		 {

			if(document.all("hdnImprimir").value!="")
			{
			    var sNro = '';
			    sNro = LeerCamposXML("comprobantes", document.all("hdnImprimir").value, "_comp_desc").split("|");
	          //  sNro = sNro[0]; 	

			    var lstrTipo = '';
			    if (document.all("hdnImpriTipo").value == 'NC')
					lstrTipo = 'Nota de Cr�dito';
			    else
					lstrTipo = 'Nota de D�bito';
					           
				if (window.confirm("�Desea imprimir la " + lstrTipo + " " + sNro + " ?"))
			
				{
				       
				 try{   
				      	var sRet = mImprimirCopias(2,'O',document.all("hdnImprimir").value,"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original
				        
					 	if (window.confirm("�Se imprimi� la " + lstrTipo + " correctamente?"))
						{
							var lstrCopias = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimir").value, "copias");

						    if (lstrCopias != "0" && lstrCopias != "1")
								sRet = mImprimirCopias(lstrCopias,'D',document.all("hdnImprimir").value,"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias

							document.all("hdnImprId").value = '';
							document.all("hdnImprimio").value="1";
							__doPostBack('hdnImprimio','');	
							document.all("hdnImprimir").value="";	
							document.all("hdnImpriTipo").value="";		
						}
						else
						{
							document.all("hdnImprId").value = '';
							Imprimir();
						}
					}
					catch(e)
					{
						alert("Error al intentar efectuar la impresi�n");
					}
				}
				else
				{ 	document.all("hdnImprimio").value="0";
					document.all("hdnImprimir").value=""
					document.all("hdnImpriTipo").value=""
					__doPostBack('hdnImprimio','');		
				}
			}
		}
		
		
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["cmbConc"]!= null&&!document.all["cmbConc"].disabled)
			document.all["cmbConc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
