'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_Clientes_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
'a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'Clientes.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Imports System.Data
Imports System.Data.SqlClient
Imports AccesoBD
Imports Business.Facturacion
Imports ReglasValida.Validaciones
Imports SRA_Neg.Utiles


Namespace SRA


    'Partial Class Clientes
    Partial Class Migrated_Clientes

        Inherits Clientes

#Region " C�digo generado por el Dise�ador de Web Forms "

        'El Dise�ador de Web Forms requiere esta llamada.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Protected WithEvents btnTele As System.Web.UI.WebControls.Button
        Protected WithEvents hdnFotoPath As System.Web.UI.WebControls.TextBox
        'separadores de filas



        'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
        'No se debe eliminar o mover.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
            'No la modifique con el editor de c�digo.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_Clientes
        Private mstrClieDire As String = SRA_Neg.Constantes.gTab_ClientesDire
        Private mstrClieTele As String = SRA_Neg.Constantes.gTab_ClientesTele
        Private mstrClieMail As String = SRA_Neg.Constantes.gTab_ClientesMails
        Private mstrClieInte As String = SRA_Neg.Constantes.gTab_ClientesAgrup
        'Private mstrClieIntegr As String = SRA_Neg.Constantes.gTab_ClientesAgrup
        Private mstrActiTele As String = SRA_Neg.Constantes.gTab_ActiTele
        Private mstrActiDire As String = SRA_Neg.Constantes.gTab_ActiDire
        Private mstrActiMail As String = SRA_Neg.Constantes.gTab_ActiMail
        Private mstrParaPageSize As Integer
        Private mstrCmd As String
        Private mdsDatos As DataSet
        Private mstrds As String
        Private mdsDatosAux As DataSet
        Private mstrConn As String
        '    Public mstrClieCtrl As String
        Private mstrClieId As String
        Private mstrValorId As String
        '    Public mbooAgru As Boolean
        '    Public mbooActi As Boolean
        '    Public mintActiDefa As Integer
        Private mbooSoloBusq As Boolean
        Private mbooPermiAlta As Boolean
        Private mbooPermiBaja As Boolean
        Private mbooPermiModi As Boolean
        Private mbooPermiDeta As Boolean
        Private mbooInclDeshab As Boolean
        'campos a filtrar
        Private mstrFilNomb As String
        Private mstrFilFanta As String
        Private mstrFilSociNume As String
        Private mstrFilExpo As String
        Private mstrFilCriaNume As String
        Private mstrFilNoCriaNume As String
        Private mstrFilCuit As String
        Private mstrFilDocu As String
        Private mstrFilTarj As String
        Private mstrFilEnti As String
        Private mstrFilMedioPago As String
        Private mstrFilAgru As String
        Private mstrFilEmpr As String
        Private mstrFilClaveUnica As String
        Private mstrFilLegaNume As String
        Private mstrFilClieTipo As String = "T"
        'valores de los filtros 
        Private mstrClieNume As String
        Private mstrApel As String
        Private mstrNomb As String
        Private mstrFanta As String
        Private mstrSociNume As String
        Private mstrRaza As String
        Private mstrCriaNume As String
        Private mstrNoCriaNume As String
        Private mstrCuit As String
        Private mstrDocuTipo As String
        Private mstrDocuNume As String
        Private mstrTarjTipo As String
        Private mstrTarjNume As String
        Private mstrEnti As String
        Private mstrMedioPago As String
        Private mstrEmpr As String
        Private mstrClaveUnica As String
        Private mstrLegaNume As String
        'columnas a mostrar 
        Private mstrColNomb As String
        Private mstrColFanta As String
        Private mstrColSociNume As String
        Private mstrColRaza As String
        Private mstrColCriaNume As String
        Private mstrColNoCriaNume As String
        Private mstrColCuit As String
        Private mstrColDocuNume As String
        Private mstrColEnti As String
        Private mstrColMedioPago As String
        Private mstrColEmpr As String
        Private mstrColClaveUnica As String
        Private mstrColLegaNume As String
        'columnas de la grilla de resultados
        Private mintGrdClieEdit As Integer = 0
        Private mintGrdClieSele As Integer = 1
        Private mintGrdClieNume As Integer = 2
        Private mintGrdDesc As Integer = 3
        Private mintGrdApel As Integer = 4
        Private mintGrdNomb As Integer = 5
        Private mintGrdFanta As Integer = 6
        Private mintGrdSociNume As Integer = 6
        Private mintGrdRaza As Integer = 7
        Private mintGrdCriaNume As Integer = 8
        Private mintGrdNoCriaNume As Integer = 9
        Private mintGrdCuit As Integer = 10
        Private mintGrdDocuNume As Integer = 11
        Private mintGrdEnti As Integer = 12
        Private mintGrdMedioPago As Integer = 13
        Private mintGrdEmpr As Integer = 14
        Private mintGrdClaveUnica As Integer = 15
        Private mintGrdInst As Integer = 16
        Private mintGrdLegaNume As Integer = 17
        Private mintGrdEsta As Integer = 18

        Private Enum Columnas As Integer
            ClieSele = 0
            ClieEdit = 1
            ClieId = 2
            ClieApel = 3
            ClieFant = 4
            ClieCuit = 5
            ActiProp = 6
        End Enum
#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim lstrFil As String
            Try

                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializarControl()
                mInicializar()

                If (Not Page.IsPostBack) Then
                    If mstrClieCtrl = "" Then
                        Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                    End If

                    ' se setea N para primer ingreso
                    hdnModiMain.Text = "N"
                    hdnModiDireAdic.Text = "N"

                    Session(mSess(mstrTabla)) = Nothing

                    mSetearMaxLength()
                    mSetearEventos()
                    mCargarCombos()

                    If mstrClieId <> "" Then
                        mCargarDatos(mstrClieId)
                    Else
                        If mstrClieCtrl <> "" Then
                            mMostrarPanel(False)
                            lstrFil = txtClieNumeFil.Text & txtApelFil.Text & txtNombFil.Text & txtSociNumeFil.Text & txtExpoFil.Text & txtCriaNumeFil.Text & txtNoCriaNumeFil.Text & txtCuitFil.TextoPlano & txtDocuNumeFil.Text & txtCunicaFil.Text & cmbInseFil.SelectedValue.ToString & txtLegaNumeFil.Text & txtTarjNumeFil.Text & cmbTarjTipoFil.SelectedValue.ToString
                            If lstrFil <> "" Then
                                mConsultar()
                            End If
                        Else
                            'mConsultar()
                            mMostrarPanel(False)
                        End If
                    End If
                Else
                    If panDato.Visible Then
                        mdsDatos = Session(mSess(mstrTabla))
                    End If
                End If

                If hdnOrdenAnte.Text <> "" Then
                    mOrdenar()
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mOrdenar()
            mReordenarClientes(hdnOrdenAnte.Text, hdnOrdenProx.Text)
            hdnOrdenAnte.Text = ""
            hdnOrdenProx.Text = ""
        End Sub
        Private Sub mLimpiarSessionVar()
            Me.Session.Add("FiltroIdCliente", "")
            Me.Session.Add("FiltroApel", "")
            Me.Session.Add("FiltroCriaNume", "")
            Me.Session.Add("FiltroCIUT", "")
            Me.Session.Add("FiltroCUNICA", "")
            Me.Session.Add("FiltroDocuNume", "")
            Me.Session.Add("FiltroExpo", "")
            Me.Session.Add("FiltroLegaNume", "")
            Me.Session.Add("FiltroNoCriaNume", "")
            Me.Session.Add("FiltroNomb", "")
            Me.Session.Add("FiltroSociNume", "")
            Me.Session.Add("FiltroTarjNume", "")
        End Sub

        Private Sub mCargarCombos()

            clsWeb.gCargarRefeCmb(mstrConn, "personas_tipos", cmbTipo, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbTipoDocu, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTeleTipo, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbDefaTeleTipo, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDirePais, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDefaDirePais, "S")
            If (cmbDirePais.Valor.Trim().ToString().Length > 0) Then
                clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
            End If
            clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjTipoFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "institutos", cmbInseFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "entidades", cmbEntiFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "empresas", cmbEmprFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazaFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "pago_medios", cmbPagoFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "nombre_tipos", cmbTipoSuge, "S")

            If mbooAgru Then
                clsWeb.gCargarCombo(mstrConn, "iva_posic_cargar @ConsFina='S'", cmbIVA, "id", "descrip", "S")
            Else
                clsWeb.gCargarRefeCmb(mstrConn, "iva_posic", cmbIVA, "S")
            End If

            'inicializar valores
            If mValorParametro(Request.QueryString("Enti")) <> "" Then
                cmbEntiFil.Valor = mValorParametro(Request.QueryString("Enti"))
            End If
            If mValorParametro(Request.QueryString("TarjTipo")) <> "" Then
                cmbTarjTipoFil.Valor = mValorParametro(Request.QueryString("TarjTipo"))
            End If
            If mValorParametro(Request.QueryString("DocuTipo")) <> "" Then
                cmbDocuTipoFil.Valor = mValorParametro(Request.QueryString("DocuTipo"))
            End If
            If mValorParametro(Request.QueryString("MedioPago")) <> "" Then
                cmbPagoFil.Valor = mValorParametro(Request.QueryString("MedioPago"))
            End If
            If mValorParametro(Request.QueryString("Empr")) <> "" Then
                cmbEmprFil.Valor = mValorParametro(Request.QueryString("Empr"))
            End If
            If mValorParametro(Request.QueryString("Raza")) <> "" Then
                cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
            End If
            If mValorParametro(Request.QueryString("FilInseId")) <> "" Then
                cmbInseFil.Valor = mValorParametro(Request.QueryString("FilInseId"))
                cmbInseFil.Enabled = False
            Else
                cmbInseFil.Enabled = True
            End If
            If mValorParametro(Request.QueryString("Inse")) <> "" Then
                cmbInseFil.Valor = mValorParametro(Request.QueryString("Inse"))
            End If

            ' Dario cambio para que setee por defecto el pais por defecto, cuac
            Dim ds As DataSet = cmbDirePais.DataSource

            For Each itemrow As DataRow In ds.Tables(0).Rows
                If (Not IsDBNull(itemrow(2))) Then
                    If (itemrow(2)) Then
                        cmbDirePais.Valor() = itemrow("id")
                        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
                        cmbDefaDirePais.Valor() = itemrow("id")
                        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
                        Exit For
                    End If
                End If
            Next
            ' fin cambio Dario
        End Sub
        Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
            btnBajaTele.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
            btnBajaDire.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
            btnBajaMail.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
            cmbTipoSuge.Attributes.Add("onchange", "mSugerirTipo(this.value);return(false);")

            'If Not mbooAgru Then
            'btnAlta.Attributes.Add("onclick", "if(document.all('txtFalleFecha')!=null && document.all('txtFalleFecha').value!='')return(confirm('Confirma la baja del registro mediante la fecha de fallecimiento?'));")
            'btnModi.Attributes.Add("onclick", "if(document.all('txtFalleFecha')!=null && document.all('txtFalleFecha').value!='')return(confirm('Confirma la baja del registro mediante la fecha de fallecimiento?'));")
            'End If
        End Sub
        Private Sub mEstablecerPerfil()

            If (Not clsSQLServer.gMenuPermi(CType(Opciones.Clientes, String), (mstrConn), (Session("sUserId").ToString()))) Then
                Response.Redirect("noaccess.aspx")
            End If

            If mbooAgru Then
                'Agrupaciones
                mbooPermiAlta = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Agrupaciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiBaja = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Agrupaciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))
                mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Agrupaciones_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
            Else
                If mbooActi Then
                    'Datos por Actividad
                    mbooPermiAlta = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.DatosActividad_Alta, String), (mstrConn), (Session("sUserId").ToString()))
                    mbooPermiBaja = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.DatosActividad_Baja, String), (mstrConn), (Session("sUserId").ToString()))
                    mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.DatosActividad_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
                Else
                    'Clientes
                    mbooPermiAlta = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Clientes_Alta, String), (mstrConn), (Session("sUserId").ToString()))
                    mbooPermiBaja = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Clientes_Baja, String), (mstrConn), (Session("sUserId").ToString()))
                    mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Clientes_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
                End If
            End If

            btnAlta.Visible = mbooPermiAlta And Not mbooActi
            btnBaja.Visible = mbooPermiBaja And Not mbooActi
            btnModi.Visible = mbooPermiModi
            btnLimp.Visible = ((mbooPermiAlta Or mbooPermiModi) And Not mbooActi)
            btnAgre.Enabled = ((mbooPermiAlta Or mbooPermiModi) And Not mbooActi)

            'permiso sobre los detalles.
            mbooPermiDeta = (mbooPermiAlta Or mbooPermiModi)

            'telefonos de clientes 
            btnAltaTele.Visible = mbooPermiDeta
            btnBajaTele.Visible = mbooPermiDeta
            btnModiTele.Visible = mbooPermiDeta
            btnLimpTele.Visible = mbooPermiDeta

            'direcciones de clientes
            btnAltaDire.Visible = mbooPermiDeta
            btnBajaDire.Visible = mbooPermiDeta
            btnModiDire.Visible = mbooPermiDeta
            btnLimpDire.Visible = mbooPermiDeta

            'mails de clientes
            btnAltaMail.Visible = mbooPermiDeta
            btnBajaMail.Visible = mbooPermiDeta
            btnModiMail.Visible = mbooPermiDeta
            btnLimpMail.Visible = mbooPermiDeta

        End Sub
        Private Sub mSetearMaxLength()

            Dim lstrClieLong As Object
            Dim lstrClieTeleLong As Object
            Dim lstrClieDireLong As Object
            Dim lstrClieMailLong As Object
            Dim lintCol As Integer

            lstrClieLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
            txtApel.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_apel")
            txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_nomb")
            txtFant.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_fanta")
            txtDocuNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_docu_nume")
            txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_obse")

            lstrClieTeleLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieTele)
            txtTeleNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_tele")
            txtTeleArea.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_area_code")
            txtTeleRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_refe")

            txtDefaTeleNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_tele")
            txtDefaTeleArea.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_area_code")
            txtDefaTeleRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_refe")

            lstrClieDireLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieDire)
            txtDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_dire")
            txtDireCP.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_cpos")
            txtDireRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_refe")

            txtDefaDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_dire")
            txtDefaDireCP.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_cpos")
            txtDefaDireRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_refe")

            lstrClieMailLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieMail)
            txtMail.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_mail")
            txtMailRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_refe")

            txtDefaMail.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_mail")
            txtDefaMailRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_refe")

            txtApelFil.MaxLength = txtApel.MaxLength
            txtNombFil.MaxLength = txtNomb.MaxLength
            txtCuitFil.MaxLength = txtCUIT.MaxLength
            txtDocuNumeFil.MaxLength = txtDocuNume.MaxLength

        End Sub
#End Region

#Region "Inicializacion de Variables"
        Public Overrides Sub mInicializar()
            clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            mintActiDefa = clsSQLServer.gConsultarValor("exec usuarios_actividades_default " & Session("sUserId").ToString, mstrConn, 0)
            mEstablecerPerfil()

            usrClieFil.Tabla = mstrTabla
            usrClieFil.AutoPostback = True
            usrClieFil.FilFanta = True
            usrClieFil.FilClaveUnica = True
            usrClieFil.ColClaveUnica = True
            usrClieFil.ColFanta = True
            usrClieFil.Ancho = 790
            usrClieFil.Alto = 510
            usrClieFil.ColDocuNume = True
            usrClieFil.ColCUIT = True
            usrClieFil.ColClaveUnica = False
            usrClieFil.FilClaveUnica = True
            usrClieFil.FilCUIT = True
            usrClieFil.FilDocuNume = True
            usrClieFil.FilTarjNume = True
            usrClieFil.FilFanta = True

            ' mstrds = CType(DateTime.Now, String)

            txtCunicaFil.Enabled = True '29/12/2010'

        End Sub
        Public Overrides Sub mSetearCliente(ByVal pbooAgru As Boolean)
            lblTituAbm.Text = IIf(mbooAgru, "Agrupaciones", IIf(mbooActi, "Clientes - Datos por Actividad", "Clientes"))
            btnAgre.ToolTip = IIf(mbooAgru, "Agregar una Nueva Agrupaci�n", "Agregar un Nuevo Cliente")
            btnAgre.Visible = mbooPermiAlta And (Not panDato.Visible And Not mbooActi)
            btnAlta.Visible = mbooPermiAlta And Not mbooActi
            btnBaja.Visible = mbooPermiBaja And Not mbooActi
            panInte.Visible = pbooAgru

            chkSinCorreo.Visible = (Not pbooAgru)
            lblNaciFecha.Visible = (Not pbooAgru)
            txtNaciFecha.Visible = (Not pbooAgru)
            lblFalleFecha.Visible = (Not pbooAgru)
            txtFalleFecha.Visible = (Not pbooAgru)
            lblCUIT.Visible = (Not pbooAgru)
            txtCUIT.Visible = (Not pbooAgru)
            lblFoto.Visible = (Not pbooAgru)
            txtFoto.Visible = (Not pbooAgru)
            filFoto.Visible = (Not pbooAgru)
            lblFirma.Visible = (Not pbooAgru)
            txtFirma.Visible = (Not pbooAgru)
            filFirma.Visible = (Not pbooAgru)
            lblTipoDocu.Visible = Not pbooAgru
            cmbTipoDocu.Visible = (Not pbooAgru)
            txtDocuNume.Visible = (Not pbooAgru)

            lblTarjFil.Visible = (Not pbooAgru)
            trTarjFil.Style.Add("display", IIf(pbooAgru, "none", "inline"))
            cmbTarjTipoFil.Visible = (Not pbooAgru)
            txtTarjNumeFil.Visible = (Not pbooAgru)

            lblDocuNume.Visible = (Not pbooAgru)
            lblNomb.Visible = (Not pbooAgru)
            txtNomb.Visible = (Not pbooAgru)
            lblFant.Visible = True '02/12/2010 AG
            txtFant.Visible = True ''(Not pbooAgru)
            lblApel.Visible = (Not pbooAgru)
            txtApel.Visible = (Not pbooAgru)
            lblTipo.Visible = (Not pbooAgru)
            cmbTipo.Visible = (Not pbooAgru)
            cmbTipoSuge.Visible = (Not pbooAgru)
            lblDesc.Visible = pbooAgru
            txtDesc.Visible = pbooAgru

            hdnAgru.Text = IIf(pbooAgru, "S", "N")
            grdDato.Columns(mintGrdApel).Visible = (Not pbooAgru)
            grdDato.Columns(mintGrdDesc).Visible = pbooAgru
            chkAgrupa.Visible = Not pbooAgru
            If pbooAgru Then
                chkAgrupa.Checked = pbooAgru
            End If

            lblEsta.Visible = Not pbooAgru
            txtEsta.Visible = Not pbooAgru

            If pbooAgru Then
                lnkDire.Text = "Direcciones"
                lnkMail.Text = "Mails"
                lnkTele.Text = "Tel�fonos"
            End If

            'controles de cliente gen�rico
            pbooAgru = True
            Dim lintConsFibalID = clsSQLServer.gParametroValorConsul((mstrConn), "para_iva_cons_final")
            Dim strRet As String = clsSQLServer.gCampoValorConsul(mstrConn, "personas_tipos_consul " + IIf(cmbTipo.Valor Is DBNull.Value, "0", cmbTipo.Valor), "peti_gene")
            If (strRet) <> "" Then
                txtCUIT.Text = ""
                txtNomb.Valor = ""
                cmbIVA.Valor = lintConsFibalID
                pbooAgru = False
            End If
            clsWeb.gActivarControl(txtCUIT, pbooAgru)
            clsWeb.gActivarControl(txtNomb, pbooAgru)
            cmbIVA.Enabled = pbooAgru
            cmbIVA.CssClass = "cuadrotextolibre"

        End Sub
        Public Overrides Sub mInicializarControl()

            Try

                Dim lstrInclDeshab As String = mValorParametro(Request.QueryString("InclDeshab"))

                mstrClieCtrl = mValorParametro(Request.QueryString("ctrlId"))
                mbooSoloBusq = mValorParametro(Request.QueryString("SoloBusq")) = "1"
                mstrValorId = mValorParametro(Request.QueryString("ValorId"))
                mstrClieId = mValorParametro(Request.QueryString("ClieId"))
                mbooInclDeshab = IIf(lstrInclDeshab = "1", True, False)
                If Not Page.IsPostBack Then chkBaja.Checked = mbooInclDeshab 'Pantanettig 19/11/2007
                'chkBaja.Checked = lstrInclDeshab

                If hdnValorId.Text = "" Then
                    hdnValorId.Text = mstrValorId     'pone el id seleccionado en el control de b�squeda
                ElseIf hdnValorId.Text = "-1" Then    'si ya se limpiaron los filtros, ignora el id con el que vino
                    mstrValorId = ""
                End If

                mbooAgru = mValorParametro(Request.QueryString("tipo") = "agru")
                mbooActi = mValorParametro(Request.QueryString("a") = "s")

                mSetearCliente(mbooAgru)

                'campos a filtrar
                If mstrClieCtrl <> "" Then
                    imgCloseFil.Visible = True
                    mstrFilNomb = mValorParametro(Request.QueryString("FilNomb"))
                    mstrFilFanta = mValorParametro(Request.QueryString("FilFanta"))
                    mstrFilSociNume = mValorParametro(Request.QueryString("FilSociNume"))
                    mstrFilExpo = mValorParametro(Request.QueryString("FilExpo"))
                    mstrFilCriaNume = mValorParametro(Request.QueryString("FilCriaNume"))
                    mstrFilNoCriaNume = mValorParametro(Request.QueryString("FilNoCriaNume"))
                    mstrFilCuit = mValorParametro(Request.QueryString("FilCuit"))
                    mstrFilDocu = mValorParametro(Request.QueryString("FilDocuNume"))
                    mstrFilTarj = "1"     'mValorParametro(Request.QueryString("FilTarjNume"))
                    mstrFilEnti = mValorParametro(Request.QueryString("FilEnti"))
                    mstrFilMedioPago = mValorParametro(Request.QueryString("FilMedioPago"))
                    mstrFilAgru = mValorParametro(Request.QueryString("FilAgru"))
                    mstrFilEmpr = mValorParametro(Request.QueryString("FilEmpr"))
                    mstrFilClaveUnica = mValorParametro(Request.QueryString("FilClaveUnica"))
                    mstrFilLegaNume = mValorParametro(Request.QueryString("FilLegaNume"))
                    mstrFilClieTipo = mValorParametro(Request.QueryString("FilTipo"))
                Else
                    imgCloseFil.Visible = False
                    mstrFilNomb = "1"
                    mstrFilFanta = mValorParametro(Request.QueryString("FilFanta"))
                    mstrFilSociNume = "1"
                    mstrFilExpo = "1"
                    mstrFilCriaNume = "1"
                    mstrFilNoCriaNume = "1"
                    mstrFilCuit = "1"
                    mstrFilDocu = "1"
                    mstrFilTarj = "1"
                    mstrFilEnti = "1"
                    mstrFilMedioPago = "1"
                    mstrFilAgru = "1"
                    mstrFilEmpr = "1"
                    mstrFilClaveUnica = "1"
                    mstrFilLegaNume = "1"
                    mstrFilClieTipo = "T"
                End If

                'valores de los filtros
                If Not Page.IsPostBack Then
                    cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
                    txtClieNumeFil.Valor = mValorParametro(Request.QueryString("CodiNume"))
                    txtApelFil.Valor = mValorParametro(Request.QueryString("Apel"))
                    txtNombFil.Valor = mValorParametro(Request.QueryString("Nomb"))
                    txtSociNumeFil.Valor = mValorParametro(Request.QueryString("SociNume"))
                    txtExpoFil.Valor = mValorParametro(Request.QueryString("Expo"))
                    txtCriaNumeFil.Valor = mValorParametro(Request.QueryString("CriaNume"))
                    txtNoCriaNumeFil.Valor = mValorParametro(Request.QueryString("NoCriaNume"))
                    txtCuitFil.Text = mValorParametro(Request.QueryString("Cuit"))
                    txtCunicaFil.Valor = mValorParametro(Request.QueryString("ClaveUnica"))
                    txtDocuNumeFil.Valor = mValorParametro(Request.QueryString("DocuNume"))
                    txtTarjNumeFil.Valor = mValorParametro(Request.QueryString("TarjNume"))
                    cmbInseFil.Valor = mValorParametro(Request.QueryString("Inse"))
                    txtLegaNumeFil.Valor = mValorParametro(Request.QueryString("LegaNume"))
                    'chkFanta.Checked = IIf(mValorParametro(Request.QueryString("Fanta")) = "1", True, False)
                    'chkAgrupa.Checked = IIf(mValorParametro(Request.QueryString("Agru")) = "1", True, False)
                End If

                'columnas a mostrar
                mstrColNomb = mValorParametro(Request.QueryString("ColNomb"))
                mstrColFanta = mValorParametro(Request.QueryString("ColFanta"))
                mstrColSociNume = mValorParametro(Request.QueryString("ColSociNume"))
                mstrColRaza = mValorParametro(Request.QueryString("ColCriaNume"))
                mstrColCriaNume = mValorParametro(Request.QueryString("ColCriaNume"))
                mstrColNoCriaNume = mValorParametro(Request.QueryString("ColNoCriaNume"))
                mstrColCuit = mValorParametro(Request.QueryString("ColCuit"))
                mstrColDocuNume = mValorParametro(Request.QueryString("ColDocuNume"))
                mstrColEnti = mValorParametro(Request.QueryString("ColEnti"))
                mstrColMedioPago = mValorParametro(Request.QueryString("ColMedioPago"))
                mstrColEmpr = mValorParametro(Request.QueryString("ColEmpr"))
                mstrColClaveUnica = mValorParametro(Request.QueryString("ColClaveUnica"))
                mstrColLegaNume = mValorParametro(Request.QueryString("ColLegaNume"))

                If mbooSoloBusq Then
                    grdDato.Columns(1).Visible = False
                    btnAgre.Enabled = False
                End If

                'seteo de controles segun filtros
                grdDato.Columns(0).Visible = (mstrClieCtrl <> "")

                If mstrClieCtrl <> "" Then
                    grdDato.Columns(mintGrdNomb).Visible = False     '(mstrColNomb = 1)
                    grdDato.Columns(mintGrdFanta).Visible = (mstrColFanta = "1")
                    grdDato.Columns(mintGrdSociNume).Visible = (mstrColSociNume = "1") And Not mbooAgru
                    grdDato.Columns(mintGrdRaza).Visible = (mstrColRaza = "1")
                    grdDato.Columns(mintGrdCriaNume).Visible = (mstrColCriaNume = "1")
                    grdDato.Columns(mintGrdNoCriaNume).Visible = (mstrColNoCriaNume = "1")
                    grdDato.Columns(mintGrdCuit).Visible = (mstrColCuit = "1")
                    grdDato.Columns(mintGrdDocuNume).Visible = (mstrColDocuNume = "1")
                    grdDato.Columns(mintGrdEnti).Visible = (mstrColEnti = "1")
                    grdDato.Columns(mintGrdMedioPago).Visible = (mstrColMedioPago = "1")
                    grdDato.Columns(mintGrdEmpr).Visible = (mstrColEmpr = "1")
                    grdDato.Columns(mintGrdClaveUnica).Visible = (mstrColClaveUnica = "1")
                    grdDato.Columns(mintGrdInst).Visible = False
                    grdDato.Columns(mintGrdLegaNume).Visible = (mstrColLegaNume = "1")
                    'grdDato.Columns(mintGrdEsta).Visible = (mstrClieCtrl = "")
                Else
                    grdDato.Columns(mintGrdNomb).Visible = False     '(Not mbooAgru)
                End If

                If mstrClieCtrl <> "" Then
                    lblSociNumeFil.Visible = True     '(mstrFilSociNume = "1")
                    lblExpoFil.Visible = (mstrFilExpo = "1")
                    lblCriaNumeFil.Visible = (mstrFilCriaNume = "1")
                    cmbRazaFil.Visible = (mstrFilCriaNume = "1")
                    lblNoCriaNumeFil.Visible = (mstrFilNoCriaNume = "1")
                    lblCuitFil.Visible = (mstrFilCuit = "1")
                    lblDocuFil.Visible = (mstrFilDocu = "1")
                    lblTarjFil.Visible = (mstrFilTarj = "1")
                    lblEntiFil.Visible = (mstrFilEnti = "1")
                    lblPagoFil.Visible = (mstrFilMedioPago = "1")
                    lblEmprFil.Visible = (mstrFilEmpr = "1")
                    lblCunicaFil.Visible = (mstrFilClaveUnica = "1")
                    lblLegaNumeFil.Visible = (mstrFilLegaNume = "1")
                    txtSociNumeFil.Visible = True     '(mstrFilSociNume = "1")
                    txtExpoFil.Visible = (mstrFilExpo = "1")
                    txtCriaNumeFil.Visible = (mstrFilCriaNume = "1")
                    txtNoCriaNumeFil.Visible = (mstrFilNoCriaNume = "1")
                    txtCuitFil.Visible = (mstrFilCuit = "1")
                    cmbDocuTipoFil.Visible = (mstrFilDocu = "1")
                    txtDocuNumeFil.Visible = (mstrFilDocu = "1")
                    cmbTarjTipoFil.Visible = (mstrFilTarj = "1")
                    txtTarjNumeFil.Visible = (mstrFilTarj = "1")
                    cmbEntiFil.Visible = (mstrFilEnti = "1")
                    cmbPagoFil.Visible = (mstrFilMedioPago = "1")
                    cmbEmprFil.Visible = (mstrFilEmpr = "1")
                    txtCunicaFil.Visible = (mstrFilClaveUnica = "1")
                    cmbInseFil.Visible = (mstrFilLegaNume = "1")
                    txtLegaNumeFil.Visible = (mstrFilLegaNume = "1")
                    lblFant.Visible = True  '02/12/2010
                    txtFant.Visible = True ''(mstrFilFanta = "1")
                    chkAgrupa.Visible = (mstrFilAgru = "1")
                Else
                    If Not mbooAgru Then
                        mstrFilCriaNume = "1"
                        mstrFilCriaNume = "1"
                        cmbRazaFil.Visible = True
                        txtCriaNumeFil.Visible = True
                        lblCriaNumeFil.Visible = True
                        tdIntegrSep.Style.Add("display", "inline")
                        tdIntegr.Style.Add("display", "inline")
                    Else
                        mstrFilCriaNume = "0"
                        mstrFilCriaNume = "0"
                        cmbRazaFil.Visible = False
                        txtCriaNumeFil.Visible = False
                        lblCriaNumeFil.Visible = False
                        tdIntegrSep.Style.Add("display", "none")
                        tdIntegr.Style.Add("display", "none")
                    End If
                End If

                trCriaNumeFil.Style.Add("display", IIf(lblCriaNumeFil.Visible, "inline", "none"))
                trNoCriaNumeFil.Style.Add("display", IIf(lblNoCriaNumeFil.Visible, "inline", "none"))
                trCuitFil.Style.Add("display", IIf(lblCuitFil.Visible, "inline", "none"))
                trDocuFil.Style.Add("display", IIf(lblDocuFil.Visible, "inline", "none"))
                trTarjFil.Style.Add("display", IIf(lblTarjFil.Visible, "inline", "none"))
                trEntiFil.Style.Add("display", IIf(lblEntiFil.Visible, "inline", "none"))
                trPagoFil.Style.Add("display", IIf(lblPagoFil.Visible, "inline", "none"))
                trEmprFil.Style.Add("display", IIf(lblEmprFil.Visible, "inline", "none"))
                trCunicaFil.Style.Add("display", IIf(lblCunicaFil.Visible, "inline", "none"))
                trLegaNumeFil.Style.Add("display", IIf(lblLegaNumeFil.Visible, "inline", "none"))

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Function mValorParametro(ByVal pstrPara As String) As String
            Dim lstrPara As String
            If pstrPara Is Nothing Then
                lstrPara = ""
            Else
                If pstrPara Is System.DBNull.Value Then
                    lstrPara = ""
                Else
                    lstrPara = pstrPara
                End If
            End If
            Return (lstrPara)
        End Function
#End Region

#Region "Operaciones sobre el DataGrid"
        Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.EditItemIndex = -1
                If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = 0
                Else
                    grdDato.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Overrides Sub mValidarConsulta()

            Try
                If (cmbRazaFil.Valor = 0 And txtCriaNumeFil.Text <> "") _
                Or (cmbRazaFil.Valor <> 0 And txtCriaNumeFil.Text = "") Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la raza y el numero de criador.")
                End If

                If (cmbDocuTipoFil.Valor = 0 And txtDocuNumeFil.Text <> "") _
                Or (cmbDocuTipoFil.Valor <> 0 And txtDocuNumeFil.Text = "") Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el tipo y n�mero de documento.")
                End If

                If (cmbInseFil.Valor = 0 And txtLegaNumeFil.Text <> "") _
                Or (cmbInseFil.Valor <> 0 And txtLegaNumeFil.Text = "") Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el instituto y el n�mero de legajo.")
                End If

                If (cmbTarjTipoFil.Valor = 0 And txtTarjNumeFil.Text <> "") _
                Or (cmbTarjTipoFil.Valor <> 0 And txtTarjNumeFil.Text = "") Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la tarjeta y el n�mero de la misma.")
                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Overrides Sub mConsultar()
            Try
                Dim lstrCmd As String

                Dim FiltroIdCliente As String = Me.Session("FiltroIdCliente")

                If Not IsNothing(FiltroIdCliente) Then
                    If (FiltroIdCliente.Length > 0 And txtClieNumeFil.Valor.ToString.Length = 0) Then
                        txtClieNumeFil.Valor = FiltroIdCliente
                    End If
                End If

                Dim FiltroApel As String = Me.Session("FiltroApel")
                If Not IsNothing(FiltroApel) Then
                    If (FiltroApel.Length > 0 And txtApelFil.Valor.ToString.Length = 0) Then
                        txtApelFil.Valor = FiltroApel
                    End If
                End If

                Dim FiltroCriaNume As String = Me.Session("FiltroCriaNume")
                If Not IsNothing(FiltroCriaNume) Then
                    If (FiltroCriaNume.Length > 0 And txtCriaNumeFil.Valor.ToString.Length = 0) Then
                        txtCriaNumeFil.Valor = FiltroCriaNume
                    End If
                End If

                Dim FiltroCIUT As String = Me.Session("FiltroCIUT")
                If Not IsNothing(FiltroCIUT) Then
                    If (FiltroCIUT.Length > 0 And txtCuitFil.Text.ToString.Length = 0) Then
                        txtCuitFil.Text = FiltroCIUT
                    End If
                End If

                Dim FiltroCUNICA As String = Me.Session("FiltroCUNICA")
                If Not IsNothing(FiltroCUNICA) Then
                    If (FiltroCUNICA.Length > 0 And txtCunicaFil.Valor.ToString.Length = 0) Then
                        txtCunicaFil.Valor = FiltroCUNICA
                    End If
                End If

                Dim FiltroDocuNume As String = Me.Session("FiltroDocuNume")
                If Not IsNothing(FiltroDocuNume) Then
                    If (FiltroDocuNume.Length > 0 And txtDocuNumeFil.Valor.ToString.Length = 0) Then
                        txtDocuNumeFil.Valor = FiltroDocuNume
                    End If
                End If

                Dim FiltroExpo As String = Me.Session("FiltroExpo")
                If Not IsNothing(FiltroExpo) Then
                    If (FiltroExpo.Length > 0 And txtExpoFil.Valor.ToString.Length = 0) Then
                        txtExpoFil.Valor = FiltroExpo
                    End If
                End If

                Dim FiltroLegaNume As String = Me.Session("FiltroLegaNume")
                If Not IsNothing(FiltroLegaNume) Then
                    If (FiltroLegaNume.Length > 0 And txtLegaNumeFil.Valor.ToString.Length = 0) Then
                        txtLegaNumeFil.Valor = FiltroLegaNume
                    End If
                End If

                Dim FiltroNoCriaNume As String = Me.Session("FiltroNoCriaNume")
                If Not IsNothing(FiltroNoCriaNume) Then
                    If (FiltroNoCriaNume.Length > 0 And txtNoCriaNumeFil.Valor.ToString.Length = 0) Then
                        txtNoCriaNumeFil.Valor = FiltroNoCriaNume
                    End If
                End If

                Dim FiltroNomb As String = Me.Session("FiltroNomb")
                If Not IsNothing(FiltroNomb) Then
                    If (FiltroNomb.Length > 0 And txtNombFil.Valor.ToString.Length = 0) Then
                        txtNombFil.Valor = FiltroNomb
                    End If
                End If

                Dim FiltroSociNume As String = Me.Session("FiltroSociNume")
                If Not IsNothing(FiltroSociNume) Then
                    If (FiltroSociNume.Length > 0 And txtSociNumeFil.Valor.ToString.Length = 0) Then
                        txtSociNumeFil.Valor = FiltroSociNume
                    End If
                End If

                Dim FiltroTarjNume As String = Me.Session("FiltroTarjNume")
                If Not IsNothing(FiltroTarjNume) Then
                    If (FiltroTarjNume.Length > 0 And txtTarjNumeFil.Valor.ToString.Length = 0) Then
                        txtTarjNumeFil.Valor = FiltroTarjNume
                    End If
                End If

                lstrCmd = ObtenerSql(Session("sUserId").ToString(), IIf(chkBusc.Checked, 1, 0), txtClieNumeFil.Valor.ToString,
                       IIf(chkBaja.Checked, 1, 0), mstrClieCtrl, mstrFilAgru, mbooAgru, IIf(chkAgrupa.Checked, 1, 0),
                       mstrValorId, txtApelFil.Text, txtNombFil.Text, txtSociNumeFil.Valor.ToString, mstrFilCriaNume,
                       cmbRazaFil.Valor.ToString, txtCriaNumeFil.Valor.ToString, mstrFilNoCriaNume, txtNoCriaNumeFil.Valor.ToString,
                       mstrFilCuit, txtCuitFil.TextoPlano.ToString, mstrFilDocu, cmbDocuTipoFil.Valor.ToString,
                       txtDocuNumeFil.Valor.ToString, mstrFilTarj, cmbTarjTipoFil.Valor.ToString, txtTarjNumeFil.Valor.ToString,
                       mstrFilEnti, cmbEntiFil.Valor.ToString, mstrFilMedioPago, cmbPagoFil.Valor.ToString,
                       mstrFilEmpr, cmbEmprFil.Valor.ToString, mstrFilClaveUnica, txtCunicaFil.Valor.ToString,
                       mstrFilLegaNume, cmbInseFil.Valor.ToString, txtExpoFil.Valor.ToString, txtLegaNumeFil.Valor.ToString,
                       mstrFilClieTipo, mbooInclDeshab)

                clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

                If (txtClieNumeFil.Text <> "") Then
                    If (Convert.ToInt32(txtClieNumeFil.Text) = 0) Then
                        txtClieNumeFil.Text = ""
                    End If
                End If

                If Not Page.IsPostBack And grdDato.Items.Count = 1 And mstrClieCtrl <> "" Then
                    If mstrValorId = "" Then
                        mCliente(grdDato.Items(0).Cells(Columnas.ClieId).Text)
                    Else
                        mCargarDatos(mstrValorId)
                        Return
                    End If
                End If
                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try

        End Sub
        '    Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pstrClieId As String, _
        '                ByVal pintBaja As Integer, ByVal pstrClieCtrl As String, ByVal pbooFilAgru As Boolean, _
        '                ByVal pbooSoloAgrupa As Boolean, ByVal pintInclAgru As Integer, ByVal pstrValorId As String, _
        '                ByVal pstrApel As String, ByVal pstrNomb As String, ByVal pstrSociNume As String, ByVal pstrFilCriaNume As String, _
        '                ByVal pstrRazaFil As String, ByVal pstrCriaNumeFil As String, ByVal pstrFilNoCriaNume As String, _
        '                ByVal pstrNoCriaNumeFil As String, ByVal pstrFilCuit As String, ByVal pstrCuitFil As String, _
        '                ByVal pstrFilDocu As String, ByVal pstrDocuTipoFil As String, ByVal pstrDocuNumeFil As String, _
        '                ByVal pstrFilTarj As String, ByVal pstrTarjTipoFil As String, ByVal pstrTarjNumeFil As String, _
        '                ByVal pstrFilEnti As String, ByVal pstrEntiFil As String, ByVal pstrFilMedioPago As String, _
        '                ByVal pstrPagoFil As String, ByVal pstrFilEmpr As String, ByVal pstrEmprFil As String, _
        '                ByVal pstrFilClaveUnica As String, ByVal pstrCunicaFil As String, ByVal pstrFilLegaNume As String, _
        '                ByVal pstrInseFil As String, ByVal pstrExpo As String, ByVal pstrLegaNumeFil As String, ByVal pstrFilClieTipo As String, _
        '                Optional ByVal mbooInclDeshab As Boolean = False) As String
        '        Dim lstrCmd As New StringBuilder
        '
        '        'esta funci�n se llama tambi�n desde clsXMLHTTP
        '        lstrCmd.Append("exec clientes_busq")
        '        lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
        '        lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
        '        lstrCmd.Append(" , @clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
        '        If mbooInclDeshab Then
        '            lstrCmd.Append(" , @incluir_bajas = 1")
        '        Else
        '            lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)
        '        End If
        '        lstrCmd.Append(" , @incluir_agru = " + pintInclAgru.ToString)
        '
        '        If pstrClieCtrl = "" Then
        '            lstrCmd.Append(" , @clie_agru = " + IIf(pbooSoloAgrupa, "1", "0"))
        '        Else
        '            If pbooFilAgru = "1" Then
        '                lstrCmd.Append(" , @clie_agru = " + IIf(pbooSoloAgrupa, "null", "0"))
        '            Else
        '                lstrCmd.Append(" , @clie_agru = 0")
        '            End If
        '        End If
        '
        '        If pstrValorId = "" Then
        '            lstrCmd.Append(" , @clie_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
        '            lstrCmd.Append(" , @clie_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
        '            lstrCmd.Append(" , @clie_soci_nume=" + clsSQLServer.gFormatArg(pstrSociNume, SqlDbType.Int))
        '            lstrCmd.Append(" , @exps_nume=" + clsSQLServer.gFormatArg(pstrExpo, SqlDbType.Int))
        '
        '            If pstrFilCriaNume = "1" Then
        '                lstrCmd.Append(" , @clie_raza=" + clsSQLServer.gFormatArg(pstrRazaFil, SqlDbType.Int))
        '                lstrCmd.Append(" , @clie_cria_nume=" + clsSQLServer.gFormatArg(pstrCriaNumeFil, SqlDbType.Int))
        '            End If
        '            If pstrFilNoCriaNume = "1" Then
        '                lstrCmd.Append(" , @clie_nocria_nume=" + clsSQLServer.gFormatArg(pstrNoCriaNumeFil, SqlDbType.Int))
        '            End If
        '            If pstrFilCuit = "1" And pstrCuitFil <> "" Then
        '                lstrCmd.Append(" , @clie_cuit=" + clsSQLServer.gFormatArg(Replace(pstrCuitFil, "-", ""), SqlDbType.Int))
        '            End If
        '            If pstrFilDocu = "1" Then
        '                lstrCmd.Append(" , @clie_docu_tipo=" + clsSQLServer.gFormatArg(pstrDocuTipoFil, SqlDbType.Int))
        '                lstrCmd.Append(" , @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNumeFil, SqlDbType.Int))
        '            End If
        '            If pstrFilTarj = "1" Then
        '                lstrCmd.Append(" , @tarj_id=" + clsSQLServer.gFormatArg(pstrTarjTipoFil, SqlDbType.Int))
        '                lstrCmd.Append(" , @tarj_nume=" + clsSQLServer.gFormatArg(pstrTarjNumeFil, SqlDbType.VarChar))
        '            End If
        '            If pstrFilEnti = "1" Then
        '                lstrCmd.Append(" , @clie_enti=" + clsSQLServer.gFormatArg(pstrEntiFil, SqlDbType.Int))
        '            End If
        '            If pstrFilMedioPago = "1" Then
        '                lstrCmd.Append(" , @clie_medio_pago=" + clsSQLServer.gFormatArg(pstrPagoFil, SqlDbType.Int))
        '            End If
        '            If pstrFilEmpr = "1" Then
        '                lstrCmd.Append(" , @clie_empr=" + clsSQLServer.gFormatArg(pstrEmprFil, SqlDbType.Int))
        '            End If
        '            ' Dario 2013-05-02 Cambio por error en la recarga de la grilla despues de guardar
        '            If pstrFilClaveUnica = "1" Then
        '                If (pstrCunicaFil = "0") Then
        '                    pstrCunicaFil = ""
        '                End If
        '                lstrCmd.Append(" , @clie_cunica=" + clsSQLServer.gFormatArg(Replace(pstrCunicaFil, "-", ""), SqlDbType.VarChar))
        '            End If
        '            'If pstrFilClaveUnica = "1" Then
        '            '    lstrCmd.Append(" , @clie_cunica=" + clsSQLServer.gFormatArg(Replace(pstrCunicaFil, "-", ""), SqlDbType.VarChar))
        '            'End If
        '            ' fin cambio
        '            If pstrFilLegaNume = "1" Then
        '                lstrCmd.Append(" , @clie_inse_id=" + clsSQLServer.gFormatArg(pstrInseFil, SqlDbType.Int))
        '                lstrCmd.Append(" , @clie_lega_nume=" + clsSQLServer.gFormatArg(pstrLegaNumeFil, SqlDbType.Int))
        '            End If
        '        End If
        '        If pstrFilClieTipo <> "" Then
        '            lstrCmd.Append(" , @clie_tipo=" + clsSQLServer.gFormatArg(pstrFilClieTipo, SqlDbType.VarChar.VarChar))
        '        End If
        '
        '        Return (lstrCmd.ToString)
        '    End Function
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            Select Case pstrTabla
                Case mstrClieTele
                    btnBajaTele.Enabled = Not (pbooAlta)
                    btnModiTele.Enabled = Not (pbooAlta)
                    btnAltaTele.Enabled = pbooAlta
                Case mstrClieDire
                    btnBajaDire.Enabled = (Not pbooAlta And (hdnEsSocio.Text <> "S" Or mbooActi))
                    btnModiDire.Enabled = (Not pbooAlta And (hdnEsSocio.Text <> "S" Or mbooActi))
                    btnAltaDire.Enabled = (pbooAlta And (hdnEsSocio.Text <> "S" Or mbooActi))
                Case mstrClieMail
                    btnBajaMail.Enabled = Not (pbooAlta)
                    btnModiMail.Enabled = Not (pbooAlta)
                    btnAltaMail.Enabled = pbooAlta
                Case mstrClieInte
                    btnBajaInte.Enabled = Not (pbooAlta)
                    btnModiInte.Enabled = Not (pbooAlta)
                    btnAltaInte.Enabled = pbooAlta
                Case mstrClieInte & "_juridicos"
                    btnBajaIntegr.Enabled = Not (pbooAlta)
                    btnModiIntegr.Enabled = Not (pbooAlta)
                    btnAltaIntegr.Enabled = pbooAlta
                Case Else
                    btnBaja.Enabled = Not (pbooAlta)
                    btnModi.Enabled = Not (pbooAlta)
                    btnAlta.Enabled = pbooAlta
                    btnOtrosDatos.Visible = Not pbooAlta And Not mbooAgru
                    'txtClaveUnica.Enabled = pbooAlta                '29/12/2010
                    txtClaveUnica.Enabled = False
                    If pbooAlta Then txtClaveUnica.Text = "  "
            End Select
        End Sub
        Public Overrides Sub mSeleccionarCliente(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                Dim ldtsData As New DataSet
                ldtsData = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", E.Item.Cells(Columnas.ClieId).Text)
                If Not mbooInclDeshab And Not ldtsData.Tables(0).Rows(0).IsNull("clie_baja_fecha") Then
                    Throw New AccesoBD.clsErrNeg("No se puede seleccionar el registro dado que el mismo est� dado de baja.")
                End If

                mCliente(E.Item.Cells(Columnas.ClieId).Text)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCliente(ByVal mstrClie As String)
            Dim lsbMsg As New StringBuilder

            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", Request.QueryString("ctrlId"), mstrClie))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].onchange();", Request.QueryString("ctrlId")))
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)
        End Sub

        Public Overrides Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                mCargarDatos(E.Item.Cells(Columnas.ClieId).Text)
                mHabilitarDatosPrincipales(False)
                mSetearDatosSocio(hdnEsSocio.Text = "S")
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mHabilitarDatosPrincipales(ByVal pOk As Boolean)
            Dim mOk As Boolean
            mOk = pOk Or Not mbooActi
            cmbTipo.Enabled = mOk
            txtApel.Enabled = mOk
            txtNomb.Enabled = mOk
            cmbTipoSuge.Enabled = mOk
            txtFant.Enabled = mOk
            txtCUIT.Enabled = mOk
            cmbIVA.Enabled = mOk
            cmbTipoDocu.Enabled = mOk
            txtDocuNume.Enabled = mOk
            chkSinCorreo.Enabled = mOk
            txtNaciFecha.Enabled = mOk
            txtFalleFecha.Enabled = mOk
            txtFoto.Enabled = mOk
            filFoto.Visible = mOk
            txtFirma.Enabled = mOk
            filFirma.Visible = mOk
            imgDelFoto.Visible = mOk
            imgDelFirm.Visible = mOk
            txtObse.Enabled = mOk
            grdInte.Enabled = mOk
            usrClieFil.Activo = mOk
        End Sub

        Public Overrides Sub mCargarDatos(ByVal pstrId As String)
            Dim lstrCarpeta As String
            Dim lbooEsSocio As Boolean = False

            mLimpiar()

            If clsSQLServer.gCampoValorConsul(mstrConn, "socios_busq @soci_clie_id=" + pstrId, "soci_id") = "" Then
                lbooEsSocio = False
                hdnEsSocio.Text = "N"
            Else
                lbooEsSocio = True
                hdnEsSocio.Text = "S"
            End If

            hdnId.Text = clsFormatear.gFormatCadena(pstrId)

            mCrearDataSet(hdnId.Text)

            grdTele.CurrentPageIndex = 0
            grdDire.CurrentPageIndex = 0
            grdMail.CurrentPageIndex = 0
            grdIntegr.CurrentPageIndex = 0

            cmbTipoSuge.Visible = False
            Dim lstrURL As New StringBuilder


            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    If Not .IsNull("clie_agru") AndAlso .Item("clie_agru") And Not mbooAgru Then


                        lstrURL.Append("clientes.aspx?")
                        lstrURL.Append("tipo=agru")
                        lstrURL.Append("&ctrlId=" & mstrClieCtrl)
                        lstrURL.Append("&FilAgru=0")
                        lstrURL.Append("&ClieId=" & .Item("clie_id").ToString)
                        lstrURL.Append("&SoloBusq=" & IIf(mbooSoloBusq, "1", "0"))
                        lstrURL.Append("&ValorId=" & mstrValorId)
                        lstrURL.Append("&a=" & IIf(mbooActi, "1", "0"))
                        lstrURL.Append("&FilNomb=" & mstrFilNomb)
                        lstrURL.Append("&FilSociNume=" & mstrFilSociNume)
                        lstrURL.Append("&FilExpo=" & mstrFilExpo)
                        lstrURL.Append("&FilCriaNume=" & mstrFilCriaNume)
                        lstrURL.Append("&FilNoCriaNume=" & mstrFilNoCriaNume)
                        lstrURL.Append("&FilCuit=" & mstrFilCuit)
                        lstrURL.Append("&FilDocuNume=" & mstrFilDocu)
                        lstrURL.Append("&FilEnti=1")
                        lstrURL.Append("&FilMedioPago=" & mstrFilMedioPago)
                        lstrURL.Append("&FilEmpr=" & mstrFilEmpr)
                        lstrURL.Append("&FilClaveUnica=" & mstrFilClaveUnica)
                        lstrURL.Append("&FilLegaNume=" & mstrFilLegaNume)
                        lstrURL.Append("&FilTipo=" & mstrFilClieTipo)
                        lstrURL.Append("&CodiNume=" & mValorParametro(Request.QueryString("CodiNume")))
                        lstrURL.Append("&Apel=" & mValorParametro(Request.QueryString("Apel")))
                        lstrURL.Append("&Nomb=" & mValorParametro(Request.QueryString("Nomb")))
                        lstrURL.Append("&SociNume=" &
                        IIf(mValorParametro(Request.QueryString("SociNume")) = "", "0",
                        mValorParametro(Request.QueryString("SociNume"))))

                        lstrURL.Append("&Expo=" & EmptyToCero(mValorParametro(Request.QueryString("Expo"))))
                        lstrURL.Append("&CriaNume=" & EmptyToCero(mValorParametro(Request.QueryString("CriaNume"))))
                        lstrURL.Append("&NoCriaNume=" & EmptyToCero(mValorParametro(Request.QueryString("NoCriaNume"))))
                        lstrURL.Append("&Cuit=" & EmptyToCero(mValorParametro(Request.QueryString("Cuit"))))
                        lstrURL.Append("&ClaveUnica=" & EmptyToCero(mValorParametro(Request.QueryString("ClaveUnica"))))
                        lstrURL.Append("&DocuNume=" & EmptyToCero(mValorParametro(Request.QueryString("DocuNume"))))
                        lstrURL.Append("&TarjNume=" & EmptyToCero(mValorParametro(Request.QueryString("TarjNume"))))
                        lstrURL.Append("&LegaNume=" & EmptyToCero(mValorParametro(Request.QueryString("LegaNume"))))
                        lstrURL.Append("&Agru=1")
                        lstrURL.Append("&ColNomb=" & mstrColNomb)
                        lstrURL.Append("&ColSociNume=" & mstrColSociNume)
                        lstrURL.Append("&ColCriaNume=" & mstrColCriaNume)
                        lstrURL.Append("&ColNoCriaNume=" & mstrColNoCriaNume)
                        lstrURL.Append("&ColCuit=" & mstrColCuit)
                        lstrURL.Append("&ColDocuNume=" & mstrColDocuNume)
                        lstrURL.Append("&ColEnti=" & mstrColEnti)
                        lstrURL.Append("&ColMedioPago=" & mstrColMedioPago)
                        lstrURL.Append("&ColEmpr=" & mstrColEmpr)
                        lstrURL.Append("&ColClaveUnica=" & mstrColClaveUnica)
                        lstrURL.Append("&ColLegaNume=" & mstrColLegaNume)

                        Response.Redirect(lstrURL.ToString)

                    End If
                    txtApel.Valor = IIf(.Item("clie_apel").ToString.Trim.Length > 0, .Item("clie_apel"), String.Empty)
                    txtNomb.Valor = IIf(.Item("clie_nomb").ToString.Trim.Length > 0, .Item("clie_nomb"), String.Empty)
                    txtFant.Valor = IIf(.Item("clie_fanta").ToString.Trim.Length > 0, .Item("clie_fanta"), String.Empty)
                    txtObse.Valor = IIf(.Item("clie_obse").ToString.Trim.Length > 0, .Item("clie_obse"), String.Empty)
                    txtDocuNume.Valor = IIf(.Item("clie_docu_nume").ToString.Trim.Length > 0, .Item("clie_docu_nume"), String.Empty)

                    txtDesc.Text = IIf(.Item("_descrip").ToString.Trim.Length > 0, .Item("_descrip"), String.Empty)
                    If .Item("clie_cuit") Is DBNull.Value OrElse .Item("clie_cuit") = "0" Then
                        txtCUIT.Text = ""
                    Else
                        txtCUIT.Text = Trim(.Item("_cuit"))
                    End If
                    If .Item("clie_peti_id").ToString.Length > 0 Then
                        cmbTipo.Valor = .Item("clie_peti_id")
                    End If
                    If .Item("clie_peti_id").ToString = "2" Then
                        lnkIntegr.Enabled = True
                    End If
                    cmbTipoDocu.Valor = IIf(.Item("clie_doti_id").ToString.Trim.Length > 0, .Item("clie_doti_id"), String.Empty)

                    If Not .IsNull("clie_sexo") Then
                        cmbSexo.Valor = IIf(.Item("clie_sexo").ToString, "1", "0")
                    Else
                        cmbSexo.Limpiar()
                    End If
                    'cmbSexo.Valor = IIf(.Item("clie_sexo").ToString.Trim.Length > 0, .Item("clie_sexo"), String.Empty)

                    txtEsta.Text = IIf(.Item("_clie_estado").ToString.Trim.Length > 0, .Item("_clie_estado"), String.Empty)

                    If Not .IsNull("clie_baja_fecha") Then
                        txtEsta.Text += "(" & CDate(.Item("clie_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    End If

                    lblAgruEsta.Text = txtEsta.Text
                    ' 26/02/07 (por el paralelo se genera manualmente la clave unica)
                    ' lblAgruCunica.Text = clsSQLServer.gObtenerValorCampo(mstrConn, "clientes_cunica", .Item("clie_id"), "clcu_cunica").ToString.Trim
                    txtClaveUnica.Text = IIf(.Item("clcu_cunica").ToString.Trim.Length > 0, .Item("clcu_cunica"), String.Empty)


                    cmbIVA.Valor = IIf(.Item("clie_ivap_id").ToString.Trim.Length > 0, .Item("clie_ivap_id"), String.Empty)
                    chkSinCorreo.Checked = clsWeb.gFormatCheck(.Item("clie_sin_correo"), "True")
                    txtNaciFecha.Fecha = IIf(.Item("clie_naci_fecha").ToString.Trim.Length > 0, .Item("clie_naci_fecha"), String.Empty)
                    txtFalleFecha.Fecha = IIf(.Item("clie_falle_fecha").ToString.Trim.Length > 0, .Item("clie_falle_fecha"), String.Empty)

                    lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString()
                    lstrCarpeta += "/" + clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_foto_path") + "/"

                    If .IsNull("clie_foto") Then
                        txtFoto.Text = ""
                        btnFotoVer.Visible = False
                    Else
                        btnFotoVer.Visible = True
                        txtFoto.Text = .Item("clie_foto")
                        btnFotoVer.Attributes.Add("onclick", String.Format("gAbrirVentanas('{0}', 14, '200','200','100','100');", lstrCarpeta + txtFoto.Text.Substring(txtFoto.Text.LastIndexOf("\") + 1)))
                    End If

                    If .IsNull("clie_firma") Then
                        txtFirma.Text = ""
                        btnFirmaVer.Visible = False
                    Else
                        btnFirmaVer.Visible = True
                        txtFirma.Text = .Item("clie_firma")
                        btnFirmaVer.Attributes.Add("onclick", String.Format("gAbrirVentanas('{0}', 15, '200','200','320','100');", lstrCarpeta + txtFirma.Text.Substring(txtFirma.Text.LastIndexOf("\") + 1)))
                    End If

                    If Not .IsNull("clie_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("clie_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If

                    lblTitu.Text = "Datos del Cliente: " & txtApel.Text & IIf(txtNomb.Text <> "", ", " & txtNomb.Text, "") & " (Nro: " & .Item("clie_id") & IIf(.IsNull("_soci_id"), "", " - Socio: " & .Item("_soci_nume")) & ")"
                    lblTitu.Visible = False
                End With

                mCargarDatosDire("", True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais,
                cmbDefaDirePcia, cmbDefaDireLoca)

                mCargarDatosTele("", True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)
                mCargarDatosMail("", True, txtDefaMail, txtDefaMailRefe)

                mSetearEditor("", False)
                mSetearEditor(mstrClieTele, True)
                mSetearEditor(mstrClieDire, True)
                mSetearEditor(mstrClieMail, True)
                mMostrarPanel(True)
                mSetearDatosSocio(lbooEsSocio)
                mShowTabs(1)
            End If
        End Sub

        Private Sub mSetearDatosSocio(ByVal pbooEsSocio As Boolean)
            cmbTipo.Enabled = Not pbooEsSocio
            txtApel.Enabled = Not pbooEsSocio
            txtNomb.Enabled = Not pbooEsSocio
            txtFant.Enabled = True ''Not pbooEsSocio
            txtNaciFecha.Enabled = Not pbooEsSocio
            txtFalleFecha.Enabled = Not pbooEsSocio
            chkSinCorreo.Enabled = Not pbooEsSocio
            txtDefaDire.Enabled = Not pbooEsSocio
            txtDefaDireCP.Enabled = Not pbooEsSocio
            txtDefaDireRefe.Enabled = Not pbooEsSocio
            cmbDefaDirePais.Enabled = Not pbooEsSocio
            cmbDefaDirePcia.Enabled = Not pbooEsSocio
            cmbDefaDireLoca.Enabled = Not pbooEsSocio
            btnDefaLoca.Disabled = pbooEsSocio
            btnLimpDire.Enabled = Not pbooEsSocio Or mbooActi
            btnSelecDire.Disabled = pbooEsSocio Or mbooActi
        End Sub

        Private Sub mAgregar()
            mLimpiar()

            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDefaDirePais, "S")

            ' Dario 2013-05-08 cambio para que setee por defecto el pais por defecto en alta nueva
            Dim ds As DataSet = cmbDirePais.DataSource

            For Each itemrow As DataRow In ds.Tables(0).Rows
                If (Not IsDBNull(itemrow(2))) Then
                    If (itemrow(2)) Then
                        cmbDefaDirePais.Valor() = itemrow("id")
                        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
                        Exit For
                    End If
                End If
            Next

            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
        End Sub

        Private Sub mLimpiarFiltros()
            hdnValorId.Text = "-1"  'para que ignore el id que vino del control
            mstrValorId = ""
            txtClieNumeFil.Text = ""
            txtApelFil.Text = ""
            txtExpoFil.Text = ""
            txtNombFil.Text = ""
            txtSociNumeFil.Text = ""
            cmbRazaFil.Limpiar()
            txtCriaNumeFil.Text = ""
            txtNoCriaNumeFil.Text = ""
            txtCuitFil.Text = ""
            cmbDocuTipoFil.Limpiar()
            txtDocuNumeFil.Text = ""
            cmbEntiFil.Limpiar()
            cmbPagoFil.Limpiar()
            cmbEmprFil.Limpiar()
            txtCunicaFil.Text = ""
            If cmbInseFil.Enabled Then cmbInseFil.Limpiar()
            cmbTarjTipoFil.Limpiar()
            txtTarjNumeFil.Text = ""
            txtLegaNumeFil.Text = ""
            chkAgrupa.Checked = False
            chkBusc.Checked = False
            chkBaja.Checked = False
            btnAgre.Visible = True
            Me.mLimpiarSessionVar()
        End Sub

        Private Sub mLimpiar()
            hdnId.Text = ""
            hdnEsSocio.Text = ""
            lblBaja.Text = ""
            txtDesc.Text = ""
            txtApel.Text = ""
            txtNomb.Text = ""
            txtFant.Text = ""
            txtCUIT.Text = ""
            txtObse.Text = ""
            txtDocuNume.Text = ""
            txtNaciFecha.Text = ""
            txtFalleFecha.Text = ""
            txtFoto.Text = ""
            txtFirma.Text = ""
            btnFotoVer.Visible = False
            btnFirmaVer.Visible = False
            txtEsta.Text = ""
            cmbTipoSuge.Visible = True
            txtClaveUnica.Valor = ""

            cmbTipo.Limpiar()
            cmbIVA.Limpiar()
            cmbTipoDocu.Limpiar()
            cmbTipoSuge.Limpiar()

            hdnTipoId.Text = ""
            hdnLocaPop.Text = ""

            chkSinCorreo.Checked = False

            mLimpiarTelefono(False, txtTeleArea, txtTeleNume, txtTeleRefe, cmbTeleTipo)
            mLimpiarTelefono(True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)

            mLimpiarDireccion(False, txtDire, txtDireCP, txtDireRefe, cmbDirePais, cmbDirePcia, cmbDireLoca)

            mLimpiarDireccion(True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais, cmbDefaDirePcia,
            cmbDefaDireLoca)

            mLimpiarMail(False, txtMail, txtMailRefe)
            mLimpiarMail(True, txtDefaMail, txtDefaMailRefe)

            mLimpiarIntegr()

            grdTele.CurrentPageIndex = 0
            grdDire.CurrentPageIndex = 0
            grdMail.CurrentPageIndex = 0
            grdIntegr.CurrentPageIndex = 0

            mCrearDataSet("")

            lblTitu.Text = ""

            mSetearEditor("", True)
            mSetearDatosSocio(False)
            mShowTabs(1)
        End Sub

        Private Sub mCerrar()
            If mstrClieCtrl <> "" Then
                If hdnId.Text <> "" Then
                    Dim ldtsData As New DataSet
                    ldtsData = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", hdnId.Text)

                    If panDato.Visible And ldtsData.Tables(0).Rows(0).IsNull("clie_baja_fecha") Then
                        mCliente(hdnId.Text)
                    Else
                        mMostrarPanel(False)
                    End If
                Else
                    If panDato.Visible Then
                        mMostrarPanel(False)
                    Else
                        mCliente("")
                    End If
                End If
            Else
                mMostrarPanel(False)
            End If
        End Sub
        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            panDato.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            tabLinks.Visible = pbooVisi
            panFiltros.Visible = Not pbooVisi
            grdDato.Visible = Not pbooVisi

            btnAgre.Visible = (Not panDato.Visible And Not mbooActi)

            lnkCabecera.Font.Bold = True
            lnkTele.Font.Bold = False
            lnkDire.Font.Bold = False
            lnkMail.Font.Bold = False
            lnkDocu.Font.Bold = False
            lnkIntegr.Font.Bold = False
        End Sub

        Private Sub mPasarIdsClie()
            Dim lstrIds As String
            mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = "baja_fecha is null"
            For Each ldr As DataRowView In mdsDatos.Tables(mstrClieInte).DefaultView
                If lstrIds = "" Then
                    lstrIds = ldr.Item("clag_clie_clie_id").ToString
                Else
                    lstrIds = lstrIds.ToString + "," + ldr.Item("clag_clie_clie_id").ToString
                End If
            Next
            hdnClieIds.Text = lstrIds
        End Sub
        'Public Function EmptyToCero(ByVal stringToCheck As String) As String
        '    If stringToCheck Is Nothing OrElse stringToCheck.Length = 0 Then Return "0"
        '    Return stringToCheck
        'End Function

        'Public Function IsEmptyNullOrWhitespaceOnly(ByVal stringToCheck As String, ByVal ParamArray whitespaceChars() As Char) As Boolean
        '    If stringToCheck Is Nothing OrElse stringToCheck.Length = 0 Then Return True
        '    Return IsWhitespaceOnly(stringToCheck, whitespaceChars)
        'End Function
        'Public Function IsWhitespaceOnly(ByVal stringToCheck As String, ByVal ParamArray whitespaceChars() As Char) As Boolean
        '    Dim length As Int32 = stringToCheck.Length
        '    Dim middle As Int32 = length \ 2
        '    Dim first As Int32 = middle
        '    Dim second As Int32 = middle + 1
        '    Dim chr As Char

        '    If whitespaceChars Is Nothing OrElse whitespaceChars.Length = 0 Then
        '        whitespaceChars = New Char() {" "c, ChrW(&H3000)}
        '    End If

        '    Dim whitespaceUbound As Int32 = whitespaceChars.Length - 1
        '    Dim hasNonWhitespace As Boolean = False

        '    For i As Int32 = 0 To middle

        '        If first >= 0 Then
        '            hasNonWhitespace = True
        '            chr = stringToCheck.Chars(first)
        '            For j As Int32 = 0 To whitespaceUbound
        '                If chr = whitespaceChars(j) Then
        '                    hasNonWhitespace = False
        '                    Exit For
        '                End If
        '            Next
        '            first -= 1
        '        End If

        '        If hasNonWhitespace Then Return False


        '        If second < length Then
        '            hasNonWhitespace = True
        '            chr = stringToCheck.Chars(second)
        '            For j As Int32 = 0 To whitespaceUbound
        '                If chr = whitespaceChars(j) Then
        '                    hasNonWhitespace = False
        '                    Exit For
        '                End If
        '            Next
        '            second += 1
        '        End If

        '        If hasNonWhitespace Then Return False

        '    Next

        '    Return Not hasNonWhitespace

        'End Function




#End Region

#Region "Opciones de ABM"
        Private Sub mAlta()
            Try
                clsError.gLoggear("entra al alta de clientes")
                Dim lstrClieId As String
                Dim intCantidad As Integer = 0
                mGuardarDatos()
                Dim lobjCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString(), mstrTabla,
                mstrClieTele, mstrClieDire, mstrClieMail, mstrClieInte, mdsDatos.Copy, filFoto, filFirma,
                Server, txtFoto, txtFirma)

                If mbooAgru Then
                    'Integrantes de la agrupacion
                    For Each odrDetaInte As DataRow In mdsDatos.Tables(mstrClieInte).Select("_peti_id <> 2")
                        intCantidad = intCantidad + 1
                    Next

                    If (intCantidad <= 1) Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar mas de un integrate por agrupaci�n.")
                    End If
                End If

                lstrClieId = lobjCliente.Alta(mbooAgru)
                ' Dario 2013-05-15 1111 se genera la fila en novnuev en el alta del cliente 
                SRA_Neg.Utiles.NovNuevClientesNovedades(mstrConn, "INGR_CTE", lstrClieId)
                ' fin 2013-05-15 1111
                clsError.gLoggear("ClienteBusiness.IntegrarClienteAFinneg POST clien_id:- " + lstrClieId)
                ClienteBusiness.IntegrarClienteAFinneg(String.Empty, lstrClieId, "post")

                Me.Session.Add("FiltroIdCliente", lstrClieId)

                If mstrClieCtrl <> "" Then
                    mCliente(lstrClieId)
                    Return
                End If

                mConsultar()

                mMostrarPanel(False)

                hdnModi.Text = "S"

                hdnAltaId.Text = "El cliente ha sido dado de alta con el n�mero " & lstrClieId.ToString

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mModi()
            Try
                mGuardarDatos()
                Dim lobjCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString(), mstrTabla,
                mstrClieTele, mstrClieDire, mstrClieMail, mstrClieInte, mdsDatos.Copy, filFoto, filFirma,
                Server, txtFoto, txtFirma)

                lobjCliente.Modi(mbooAgru)

                If mstrClieCtrl <> "" Then   'mstrValorId
                    mCliente(hdnId.Text)
                    Return
                End If

                mConsultar()

                mMostrarPanel(False)

                hdnModi.Text = "S"

                clsError.gLoggear("ClienteBusiness.IntegrarClienteAFinneg PUT clien_id:- " + hdnId.Text)
                ClienteBusiness.IntegrarClienteAFinneg(String.Empty, hdnId.Text, "put")

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mBaja()
            Try
                Dim lintPage As Integer = grdDato.CurrentPageIndex

                Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
                lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

                If mstrClieCtrl <> "" Then   'mstrValorId
                    mCliente("")
                    Return
                End If

                grdDato.CurrentPageIndex = 0

                mConsultar()

                If (lintPage < grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = lintPage
                    mConsultar()
                End If

                mMostrarPanel(False)

                hdnModi.Text = "S"

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mValidarDatos()
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)

        End Sub
        Private Function mGuardarDatos() As DataSet

            Dim lintCpo As Integer
            mValidarDatos()


            mGuardarDatosDire(True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais, cmbDefaDirePcia,
            cmbDefaDireLoca)

            mGuardarDatosTele(True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)
            mGuardarDatosMail(True, txtDefaMail, txtDefaMailRefe)

            'eliminar esta validacion cuando la clave unica vuelva a ser automatica
            'If Me.txtClaveUnica.Valor Is DBNull.Value Then
            '	Throw New AccesoBD.clsErrNeg("Debe ingresar la clave �nica.")
            'End If
            If mbooAgru Then

                If (mdsDatos.Tables(mstrClieInte).Select("baja_fecha is null")).GetUpperBound(0) <= -1 Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar al menos un integrante activo para la agrupaci�n.")
                End If
            End If
            'la direccion no es obligatoria en un cliente generico
            Dim strRet As String = clsSQLServer.gCampoValorConsul(mstrConn, "personas_tipos_consul " + IIf(cmbTipo.Valor Is DBNull.Value, "0", cmbTipo.Valor), "peti_gene")
            If (strRet) = "" Then
                If mdsDatos.Tables(mstrClieDire).Select("_estado = 'Activo'").GetUpperBound(0) = -1 Then
                    Throw New AccesoBD.clsErrNeg("Esta ingresando un Cliente sin una Direcci�n Activa.")
                End If
            End If
            'If mdsDatos.Tables(mstrClieDire).Select().GetUpperBound(0) <> -1 Then
            'If mdsDatos.Tables(mstrClieDire).Select("dicl_defa=True").GetUpperBound(0) <> 0 Then
            'Throw New AccesoBD.clsErrNeg("Debe indicar una, y solo una, direcci�n default.")
            'End If
            'End If

            If mbooAgru Then
                If Not mValidarAgrupacion(hdnId.Text) Then
                    Throw New AccesoBD.clsErrNeg("Ya existe otra agrupacion con los mismos clientes ingresados.")
                End If
            End If

            With mdsDatos.Tables(0).Rows(0)
                .Item("clie_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("clie_apel") = txtApel.Valor
                If cmbTipo.Valor.ToString() = "2" Or cmbTipo.Valor.ToString() = "4" Or
                cmbTipo.Valor.ToString() = "3" Or cmbTipo.Valor.ToString() = "" Then
                    txtNomb.Valor = ""
                    txtDocuNume.Valor = ""
                    cmbTipoDocu.Valor = 0
                End If
                .Item("clie_nomb") = txtNomb.Valor
                .Item("clie_fanta") = txtFant.Valor
                .Item("clie_cuit") = txtCUIT.TextoPlano
                .Item("clie_ivap_id") = IIf(cmbIVA.Valor.Trim.Length > 0, cmbIVA.Valor, DBNull.Value)
                .Item("clie_peti_id") = IIf(cmbTipo.Valor.Trim.Length > 0, cmbTipo.Valor, DBNull.Value)
                .Item("clie_doti_id") = IIf(cmbTipoDocu.Valor.Trim.Length > 0, cmbTipoDocu.Valor, DBNull.Value)
                .Item("clie_docu_nume") = IIf(txtDocuNume.Valor.Trim.Length > 0, txtDocuNume.Valor, DBNull.Value)
                .Item("clie_obse") = txtObse.Valor
                .Item("clie_agru") = IIf(mbooAgru, 1, 0)
                .Item("clie_sin_correo") = chkSinCorreo.Checked
                .Item("clie_naci_fecha") = IIf(txtNaciFecha.Fecha.Trim.Length > 0, txtNaciFecha.Fecha, DBNull.Value)
                .Item("clie_falle_fecha") = IIf(txtFalleFecha.Fecha.Trim.Length > 0, txtFalleFecha.Fecha, DBNull.Value)

                If txtFoto.Text <> "" Then
                    .Item("clie_foto") = txtFoto.Text
                    .Item("clie_foto") = .Item("clie_foto").Substring(.Item("clie_foto").LastIndexOf("\") + 1)
                Else
                    .Item("clie_foto") = filFoto.Value
                    .Item("clie_foto") = .Item("clie_foto").Substring(.Item("clie_foto").LastIndexOf("\") + 1)
                    If filFoto.Value <> "" And hdnId.Text <> "" Then
                        .Item("clie_foto") = hdnId.Text & "_" & .Item("clie_foto")
                    End If
                End If

                If txtFirma.Text <> "" Then
                    .Item("clie_firma") = txtFirma.Text
                    .Item("clie_firma") = .Item("clie_firma").Substring(.Item("clie_firma").LastIndexOf("\") + 1)
                Else
                    .Item("clie_firma") = filFirma.Value
                    .Item("clie_firma") = .Item("clie_firma").Substring(.Item("clie_firma").LastIndexOf("\") + 1)
                    If filFirma.Value <> "" And hdnId.Text <> "" Then
                        .Item("clie_firma") = hdnId.Text & "_" & .Item("clie_firma")
                    End If
                End If
                ' 26/02/07 (por el paralelo se genera manualmente la clave unica)
                Dim lstrClave As String = txtClaveUnica.Valor
                lstrClave = lstrClave.Replace("-", "")
                lstrClave = lstrClave.TrimStart("")
                Dim lintLargo, x As Integer
                lintLargo = 11 - Len(lstrClave)
                For x = 1 To lintLargo
                    lstrClave = lstrClave.Insert(lstrClave.Length, " ")
                Next
                lstrClave = lstrClave.Insert(2, "-")
                lstrClave = lstrClave.Insert(11, "-")

                .Item("clcu_cunica") = lstrClave
                .Item("clie_sexo") = IIf(cmbSexo.Valor.ToString().Length = 0, DBNull.Value, cmbSexo.Valor)

            End With

            Return mdsDatos
        End Function

        Public Overrides Sub mCrearDataSet(ByVal pstrId As String)

            Dim lstrOpci As String

            lstrOpci = "@acti=" & IIf(mbooActi, "1", "0") & ", @audi_user=" & Session("sUserId").ToString
            mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, lstrOpci)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrClieTele
            mdsDatos.Tables(2).TableName = mstrClieDire
            mdsDatos.Tables(3).TableName = mstrClieMail
            mdsDatos.Tables(4).TableName = mstrClieInte

            If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
                mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
            End If

            grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
            grdTele.DataBind()

            grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
            grdDire.DataBind()

            grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
            grdMail.DataBind()

            grdTele.Columns(Columnas.ActiProp).Visible = mbooActi
            grdDire.Columns(Columnas.ActiProp).Visible = mbooActi
            grdMail.Columns(Columnas.ActiProp).Visible = mbooActi

            mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = "_peti_id <> 2"
            grdInte.DataSource = mdsDatos.Tables(mstrClieInte)
            grdInte.DataBind()

            mFormatearIntegrantes()

            mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = "_peti_id = 2 and _clie_agru = 0"
            grdIntegr.DataSource = mdsDatos.Tables(mstrClieInte)
            grdIntegr.DataBind()
            mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = ""

            Session(mSess(mstrTabla)) = mdsDatos

        End Sub
        Private Sub mFormatearIntegrantes()
            Dim lintI As Integer
            For lintI = 0 To grdInte.Items.Count - 1
                grdInte.Items(lintI).Cells(1).Text = "<img alt='Cambiar Orden' style='cursor:hand;' border=0 src='images/orden.gif' ondragstart='javascript:mDrag(" + grdInte.Items(lintI).Cells(3).Text + ");' ondragenter='javascript:mDropCancel(this);' ondragover='javascript:mDropCancel(this);' ondrop='javascript:mDrop(" + grdInte.Items(lintI).Cells(3).Text + ");'>"
            Next lintI
        End Sub
        Private Function mValidarAgrupacion(ByVal pstrID As String) As Boolean
            Try
                Dim lstrInte As String = ""

                mdsDatos.Tables(mstrClieInte).DefaultView.Sort() = "clag_clie_clie_id"
                For Each odrDetaInte As DataRow In mdsDatos.Tables(mstrClieInte).Select()
                    If odrDetaInte.Item("baja_fecha") Is DBNull.Value Then
                        If lstrInte <> "" Then
                            lstrInte = lstrInte & ";"
                        End If
                        lstrInte = lstrInte & odrDetaInte.Item("clag_clie_clie_id")
                    End If
                Next

                Dim lstrCmd As String = "exec agrupaciones_verificar"
                lstrCmd = lstrCmd + " @clie_agru_id=" & clsSQLServer.gFormatArg(pstrID, SqlDbType.Int)
                lstrCmd = lstrCmd + ",@integrantes='" & lstrInte & "'"

                Dim myConnection As New SqlConnection(Session("sConn").ToString())
                Dim cmdExec = New SqlCommand
                myConnection.Open()
                cmdExec.Connection = myConnection
                cmdExec.CommandType = CommandType.Text
                cmdExec.CommandText = lstrCmd
                Dim dr As SqlDataReader = cmdExec.ExecuteReader()
                While (dr.Read())
                    Return (dr.GetValue(0).ToString().Trim())
                End While
                dr.Close()
                myConnection.Close()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                Return (False)
            End Try

        End Function
#End Region

#Region "Eventos de Controles"
        Private Sub mShowTabs(ByVal origen As Byte)
            Dim lstrTitu As String
            If lblTitu.Text <> "" Then
                lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
            End If

            panDato.Visible = True
            panBotones.Visible = True
            btnAgre.Visible = False
            panTele.Visible = False
            panDire.Visible = False
            panMail.Visible = False
            panDocu.Visible = False
            panIntegr.Visible = False
            panCabecera.Visible = False

            lnkCabecera.Font.Bold = False
            lnkTele.Font.Bold = False
            lnkDire.Font.Bold = False
            lnkMail.Font.Bold = False
            lnkDocu.Font.Bold = False
            lnkIntegr.Font.Bold = False

            Select Case origen
                Case 1
                    panCabecera.Visible = True
                    lnkCabecera.Font.Bold = True
                    lblTitu.Visible = False

                    mCargarDatosTele("", True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)
                    mCargarDatosDire("", True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais,
                      cmbDefaDirePcia, cmbDefaDireLoca)
                    mCargarDatosMail("", True, txtDefaMail, txtDefaMailRefe)
                Case 2
                    mGuardarDatosTele(True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)
                    mCargarDatosTele("", True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)

                    panTele.Visible = True
                    lnkTele.Font.Bold = True
                    lblTitu.Text = "Tel�fonos del Cliente: " & lstrTitu
                    lblTitu.Visible = True
                    grdTele.Visible = True
                Case 3
                    mGuardarDatosDire(True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais,
                    cmbDefaDirePcia, cmbDefaDireLoca)

                    panDire.Visible = True
                    lnkDire.Font.Bold = True
                    lblTitu.Text = "Direcciones del Cliente: " & lstrTitu
                    lblTitu.Visible = True
                    grdDire.Visible = True
                Case 4
                    mGuardarDatosMail(True, txtDefaMail, txtDefaMailRefe)

                    panMail.Visible = True
                    lnkMail.Font.Bold = True
                    lblTitu.Text = "Mails del Cliente: " & lstrTitu
                    lblTitu.Visible = True
                    grdMail.Visible = True
                Case 5
                    panDocu.Visible = True
                    lnkDocu.Font.Bold = True
                    lblTitu.Text = "Documentaci�n del Cliente: " & lstrTitu
                    lblTitu.Visible = True
                Case 6
                    panIntegr.Visible = True
                    lnkIntegr.Font.Bold = True
                    lblTitu.Text = "Integrantes: " & lstrTitu
                    lblTitu.Visible = True
            End Select
        End Sub
        Private Sub lnkTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTele.Click
            Try
                mShowTabs(2)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub lnkDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDire.Click
            Try
                mPasarIdsClie()
                mShowTabs(3)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub lnkMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMail.Click
            Try
                mShowTabs(4)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub lnkDocu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
            Try
                mShowTabs(5)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub lnkIntegr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkIntegr.Click
            Try
                mShowTabs(6)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
            Try
                mShowTabs(1)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub
        Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub
        Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            Try
                Dim intCantDefa As Integer
                Dim intCantDefaMail As Integer
                Dim intCantDefaDire As Integer

                For Each dr As DataRow In mdsDatos.Tables(mstrClieTele).Select()
                    If dr.Item("tecl_defa") = True Then

                        intCantDefa = intCantDefa + 1
                    End If
                Next

                If intCantDefa > 1 Then
                    Throw New AccesoBD.clsErrNeg("Solo se permite un telefono default.")
                End If


                hdnModiMain.Text = "S"
                mModi()

                hdnModiMain.Text = "N"
                hdnModiTeleAdic.Text = "N"
                hdnModiMailAdic.Text = "N"
                hdnModiDireAdic.Text = "N"
                hdnAltaDireAdic.Text = "N"
                Dim intClienteId As Integer

                Dim intTeleAdicionalId As Integer
                Dim intMailAdicionalId As Integer

                Dim oClientes As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
                ' Dario 2013-10-08 arregla problema de integracion
                intClienteId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("clie_id"), False)

                If txtDefaTeleArea.Text = "" And txtDefaTeleNume.Text = "" Then

                    Dim ldrTeleAdicional As DataRow

                    Dim cantTele As Integer
                    cantTele = mdsDatos.Tables(mstrClieTele).Rows.Count
                    If cantTele > 0 Then

                        With mdsDatos.Tables(mstrClieTele).Select()(0)
                            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("tecl_gene")) Then
                                Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
                            End If

                        End With
                        Dim boolDefaTele As Boolean = False


                        For Each dr As DataRow In mdsDatos.Tables(mstrClieTele).Select()
                            If dr.Item("tecl_defa") = True Then

                                boolDefaTele = True
                            End If
                        Next
                        If intCantDefa > 1 Then
                            Throw New AccesoBD.clsErrNeg("Solo se permite un telefono default.")
                        End If

                        If Not boolDefaTele Then
                            ldrTeleAdicional = oClientes.GetTeleAdicionalByClienteId(intClienteId)
                            If Not ldrTeleAdicional Is Nothing Then
                                intTeleAdicionalId = ldrTeleAdicional.Item("tecl_id")
                                Dim oSocios As New SRA_Neg.Socios(mstrConn, Session("sUserId").ToString())
                                oSocios.SetTeleDefaultByMaclId(intTeleAdicionalId)
                            End If
                        End If
                    End If
                End If

                If txtDefaMail.Text = "" And txtDefaMailRefe.Text = "" Then

                    Dim ldrMailAdicional As DataRow

                    Dim cantMail As Integer
                    cantMail = mdsDatos.Tables(mstrClieMail).Rows.Count
                    If cantMail > 0 Then

                        With mdsDatos.Tables(mstrClieMail).Select()(0)
                            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("macl_gene")) Then
                                Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
                            End If

                        End With
                        Dim boolDefaMail As Boolean = False

                        For Each dr As DataRow In mdsDatos.Tables(mstrClieMail).Select()
                            If dr.Item("macl_defa") = True Then

                                boolDefaMail = True
                            End If
                        Next
                        If intCantDefaMail > 1 Then
                            Throw New AccesoBD.clsErrNeg("Solo se permite un mail default.")
                        End If

                        If Not boolDefaMail Then
                            ldrMailAdicional = oClientes.GetMailAdicionalByClienteId(intClienteId)
                            If Not ldrMailAdicional Is Nothing Then
                                intMailAdicionalId = ldrMailAdicional.Item("macl_id")
                                Dim oSocios As New SRA_Neg.Socios(mstrConn, Session("sUserId").ToString())
                                oSocios.SetMailDefaultByMaclId(intMailAdicionalId)
                            End If
                        End If
                    End If
                End If

                If txtDefaDire.Text = "" And txtDefaDireRefe.Text = "" Then

                    Dim ldrDireAdicional As DataRow

                    Dim cantDire As Integer
                    cantDire = mdsDatos.Tables(mstrClieDire).Rows.Count
                    If cantDire > 0 Then

                        With mdsDatos.Tables(mstrClieDire).Select()(0)
                            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("dicl_gene")) Then
                                Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
                            End If

                        End With
                        Dim boolDefaDire As Boolean = False
                        intCantDefaDire = 0

                        For Each dr As DataRow In mdsDatos.Tables(mstrClieDire).Select()
                            If dr.Item("dicl_defa") = True Then
                                intCantDefaDire = intCantDefaDire + 1
                                boolDefaDire = True
                            End If
                        Next
                        If intCantDefaDire > 1 Then
                            Throw New AccesoBD.clsErrNeg("Solo se permite una direccion default.")
                        End If

                        If Not boolDefaDire Then
                            ldrDireAdicional = oClientes.GetDireAdicionalByClienteId(intClienteId)
                            If Not ldrDireAdicional Is Nothing Then
                                Dim intDireAdicionalId As Integer = ldrDireAdicional.Item("dicl_id")
                                Dim oSocios As New SRA_Neg.Socios(mstrConn, Session("sUserId").ToString())
                                oSocios.SetDireDefaultByDiclId(intDireAdicionalId)
                            End If
                        End If
                    End If
                End If

                ' Dario 2013-05-15 1111 se genera la fila en novnuev en el alta del cliente 
                SRA_Neg.Utiles.NovNuevClientesNovedades(mstrConn, "MODI_CTE", intClienteId)
                ' fin 2013-05-15 1111
                'clsError.gLoggear("ClienteBusiness.IntegrarClienteAFinneg POST(modi_click) clien_id:- " + hdnId.Text)
                'ClienteBusiness.IntegrarClienteAFinneg(String.Empty, hdnId.Text, "post")
                'clsError.gLoggear("ClienteBusiness.IntegrarClienteAFinneg PUT(modi_click) clien_id:- " + hdnId.Text)
                'ClienteBusiness.IntegrarClienteAFinneg(String.Empty, hdnId.Text, "put")

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()

            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDefaDirePais, "S")

            ' Dario 2013-05-08 cambio para que setee por defecto el pais por defecto en alta nueva
            Dim ds As DataSet = cmbDirePais.DataSource

            For Each itemrow As DataRow In ds.Tables(0).Rows
                If (Not IsDBNull(itemrow(2))) Then
                    If (itemrow(2)) Then
                        cmbDefaDirePais.Valor() = itemrow("id")
                        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
                        Exit For
                    End If
                End If
            Next
        End Sub

        Private Sub btnAltaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaTele.Click
            Try
                hdnModiTeleAdic.Text = "S"
                hdnAltaDireAdic.Text = "S"

                If txtTeleNume.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el N�mero de Tel�fono.")
                End If

                If txtTeleArea.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el C�digo de Area.")
                End If

                If txtTeleRefe.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el Tipo de Tel�fono.")
                End If


                mActualizarTelefono()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnAltaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaMail.Click
            hdnModiMailAdic.Text = "S"
            hdnAltaDireAdic.Text = "S"
            mActualizarMail()
        End Sub
        Private Sub btnAltaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDire.Click
            hdnModiDireAdic.Text = "S"
            hdnAltaDireAdic.Text = "S"
            mActualizarDireccion()
        End Sub
        Private Sub btnAltaInte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaInte.Click
            mActualizarIntegrantes()
        End Sub
        Private Sub btnAltaIntegr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaIntegr.Click
            mActualizarIntegr()
        End Sub

        Private Sub btnBajaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaTele.Click
            Try
                If (System.Convert.ToInt64(hdnTeleId.Text) > 0) Then
                    Dim dr As DataRow = mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)
                    Dim setDefault As Boolean = False
                    If (dr.Item("tecl_defa")) Then
                        setDefault = True
                    End If
                    With mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)
                        'If .Item("tecl_defa") Then
                        '    Throw New AccesoBD.clsErrNeg("No se puede eliminar el telefono default, debe reasignar el default a otro telefono antes de borrarlo.")
                        'End If
                        If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("tecl_gene")) Then
                            Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
                        End If
                        ' .Delete()
                        ' GSZ 30/04/2013
                        .Item("tecl_baja_fecha") = System.DateTime.Now.ToString
                        .Item("tecl_defa") = False
                        .Item("_estado") = "Inactivo"

                    End With
                    If (setDefault) Then
                        mdsDatos.Tables(mstrClieTele).DefaultView.RowFilter = "tecl_baja_fecha is null"
                        mdsDatos.AcceptChanges()


                        If (mdsDatos.Tables(mstrClieTele).Rows.Count > 0 And
                            mdsDatos.Tables(mstrClieTele).Select("tecl_baja_fecha is null and tecl_id<>" & hdnTeleId.Text).Length > 0) Then
                            With mdsDatos.Tables(mstrClieTele).Select("tecl_baja_fecha is null and tecl_id<>" & hdnTeleId.Text)(0)
                                .Item("tecl_defa") = True
                                .Item("_defa") = "X"
                            End With
                            mdsDatos.AcceptChanges()
                        End If
                    End If
                Else
                    Dim dr As DataRow = mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)
                    If (dr.Item("tecl_defa")) Then
                        mdsDatos.Tables(mstrClieTele).Rows.Remove(dr)
                        mdsDatos.AcceptChanges()
                        If (mdsDatos.Tables(mstrClieTele).Rows.Count > 0) Then
                            With mdsDatos.Tables(mstrClieTele).Select()(0)
                                .Item("tecl_defa") = True
                                .Item("_defa") = "X"
                            End With
                            mdsDatos.AcceptChanges()
                        End If
                    Else
                        mdsDatos.Tables(mstrClieTele).Rows.Remove(dr)
                        mdsDatos.AcceptChanges()
                    End If
                End If

                hdnModiTeleAdic.Text = "S"
                mdsDatos.AcceptChanges()

                'Dim dsTeleActivo As DataSet

                mdsDatos.Tables(mstrClieTele).DefaultView.RowFilter = "tecl_baja_fecha is null"
                grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
                grdTele.DataBind()


                mLimpiarTelefono(False, txtTeleArea, txtTeleNume, txtTeleRefe, cmbTeleTipo)
                mLimpiarTelefono(True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnBajaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDire.Click
            Try
                If (System.Convert.ToInt64(hdnDireId.Text) > 0) Then
                    Dim dr As DataRow = mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)
                    Dim setDefault As Boolean = False
                    If (dr.Item("dicl_defa")) Then
                        setDefault = True
                    End If
                    With mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)
                        If .Item("dicl_defa") Then
                            Throw New AccesoBD.clsErrNeg("No se puede eliminar el domicilio default, debe reasignar el default a otro domicilio antes de borrarlo.")
                        End If
                        If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("dicl_gene")) Then
                            Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
                        End If
                        .Item("dicl_baja_fecha") = System.DateTime.Now.ToString
                        .Item("dicl_defa") = False
                        .Item("_estado") = "Inactivo"
                    End With

                    If (setDefault) Then
                        mdsDatos.Tables(mstrClieDire).DefaultView.RowFilter = "dicl_baja_fecha is null"
                        mdsDatos.AcceptChanges()
                        If (mdsDatos.Tables(mstrClieDire).Rows.Count > 0 And
                            mdsDatos.Tables(mstrClieDire).Select("dicl_baja_fecha is null and dicl_id<>" & hdnDireId.Text).Length > 0) Then
                            With mdsDatos.Tables(mstrClieDire).Select("dicl_baja_fecha is null and dicl_id<>" & hdnDireId.Text)(0)
                                .Item("dicl_defa") = True
                                .Item("_defa") = "X"
                            End With
                            mdsDatos.AcceptChanges()
                        End If
                    End If
                Else
                    Dim dr As DataRow = mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)
                    If (dr.Item("dicl_defa")) Then
                        mdsDatos.Tables(mstrClieDire).Rows.Remove(dr)
                        mdsDatos.AcceptChanges()
                        If (mdsDatos.Tables(mstrClieDire).Rows.Count > 0) Then
                            With mdsDatos.Tables(mstrClieDire).Select()(0)
                                .Item("dicl_defa") = True
                                .Item("_defa") = "X"
                            End With
                            mdsDatos.AcceptChanges()
                        End If
                    Else
                        mdsDatos.Tables(mstrClieDire).Rows.Remove(dr)
                        mdsDatos.AcceptChanges()
                    End If
                End If

                hdnModiDireAdic.Text = "S"
                mdsDatos.AcceptChanges()

                mdsDatos.Tables(mstrClieDire).DefaultView.RowFilter = "dicl_baja_fecha is null"
                grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
                grdDire.DataBind()

                mLimpiarDireccion(False, txtDire, txtDireCP, txtDireRefe, cmbDirePais, cmbDirePcia, cmbDireLoca)
                mLimpiarDireccion(True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais, cmbDefaDirePcia, cmbDefaDireLoca)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnBajaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaMail.Click
            Try
                If (System.Convert.ToInt64(hdnMailId.Text) > 0) Then
                    Dim dr As DataRow = mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)
                    Dim setDefault As Boolean = False
                    If (dr.Item("macl_defa")) Then
                        setDefault = True
                    End If
                    With mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)
                        'If .Item("macl_defa") Then
                        '    Throw New AccesoBD.clsErrNeg("No se puede eliminar el email default, debe reasignar el default a otro email antes de borrarlo.")
                        'End If
                        If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("macl_gene")) Then
                            Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
                        End If
                        ' .Delete()
                        ' GSZ 02/05/2013
                        .Item("macl_baja_fecha") = System.DateTime.Now.ToString
                        .Item("macl_defa") = False
                        .Item("_estado") = "Inactivo"
                    End With
                    If (setDefault) Then
                        mdsDatos.Tables(mstrClieMail).DefaultView.RowFilter = "macl_baja_fecha is null"
                        mdsDatos.AcceptChanges()
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count > 0 And
                            mdsDatos.Tables(mstrClieMail).Select("macl_baja_fecha is null and macl_id<>" & hdnMailId.Text).Length > 0) Then
                            With mdsDatos.Tables(mstrClieMail).Select("macl_baja_fecha is null and macl_id<>" & hdnMailId.Text)(0)
                                .Item("macl_defa") = True
                                .Item("_defa") = "X"
                            End With
                            mdsDatos.AcceptChanges()
                        End If
                    End If
                Else
                    Dim dr As DataRow = mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)
                    If (dr.Item("macl_defa")) Then
                        mdsDatos.Tables(mstrClieMail).Rows.Remove(dr)
                        mdsDatos.AcceptChanges()
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count > 0) Then
                            With mdsDatos.Tables(mstrClieMail).Select()(0)
                                .Item("macl_defa") = True
                                .Item("_defa") = "X"
                            End With
                            mdsDatos.AcceptChanges()
                        End If
                    Else
                        mdsDatos.Tables(mstrClieMail).Rows.Remove(dr)
                        mdsDatos.AcceptChanges()
                    End If
                End If

                hdnModiMailAdic.Text = "S"
                mdsDatos.AcceptChanges()

                mdsDatos.Tables(mstrClieMail).DefaultView.RowFilter = "macl_baja_fecha is null"
                grdMail.DataSource = mdsDatos.Tables(mstrClieMail)

                grdMail.DataBind()
                mLimpiarMail(False, txtMail, txtMailRefe)
                mLimpiarMail(True, txtDefaMail, txtDefaMailRefe)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnBajaIntegr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaIntegr.Click
            Try
                mdsDatos.Tables(mstrClieInte).Select("clag_id=" & hdnIntegrId.Text)(0).Delete()
                mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = "_peti_id = 2 and _clie_agru = 0"
                grdIntegr.DataSource = mdsDatos.Tables(mstrClieInte)
                mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = ""
                grdIntegr.DataBind()
                mLimpiarIntegr()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnModiTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiTele.Click
            hdnModiTeleAdic.Text = "S"
            mActualizarTelefono()
        End Sub
        Private Sub btnModiDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDire.Click
            hdnModiDireAdic.Text = "S"
            mActualizarDireccion()
        End Sub
        Private Sub btnModiMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiMail.Click
            hdnModiMailAdic.Text = "S"
            mActualizarMail()
        End Sub
        Private Sub btnModiIntegr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiIntegr.Click
            mActualizarIntegr()
        End Sub

        Private Sub btnLimpDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDire.Click
            mLimpiarDireccion(False, txtDire, txtDireCP, txtDireRefe, cmbDirePais, cmbDirePcia, cmbDireLoca)
        End Sub
        Private Sub btnLimpMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpMail.Click
            mLimpiarMail(False, txtMail, txtMailRefe)
        End Sub
        Private Sub btnLimpTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpTele.Click
            mLimpiarTelefono(False, txtTeleArea, txtTeleNume, txtTeleRefe, cmbTeleTipo)
        End Sub
        Private Sub btnLimpIntegr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpIntegr.Click
            mLimpiarIntegr()
        End Sub

        Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
            mLimpiarFiltros()
        End Sub
        Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
            grdDato.CurrentPageIndex = 0
            'mValidarConsulta()
            ' Dario 2013-05-02 Cambio por perdida de valor de los filtros a la vuelta de una operacion
            Me.Session.Add("FiltroIdCliente", Me.txtClieNumeFil.Valor.ToString)
            Me.Session.Add("FiltroApel", Me.txtApelFil.Valor.ToString)
            Me.Session.Add("FiltroCriaNume", Me.txtCriaNumeFil.Valor.ToString)
            Me.Session.Add("FiltroCIUT", Me.txtCuitFil.Text.ToString)
            Me.Session.Add("FiltroCUNICA", Me.txtCunicaFil.Valor.ToString)
            Me.Session.Add("FiltroDocuNume", Me.txtDocuNumeFil.Valor.ToString)
            Me.Session.Add("FiltroExpo", Me.txtExpoFil.Valor.ToString)
            Me.Session.Add("FiltroLegaNume", Me.txtLegaNumeFil.Valor.ToString)
            Me.Session.Add("FiltroNoCriaNume", Me.txtNoCriaNumeFil.Valor.ToString)
            Me.Session.Add("FiltroNomb", Me.txtNombFil.Valor.ToString)
            Me.Session.Add("FiltroSociNume", Me.txtSociNumeFil.Valor.ToString)
            Me.Session.Add("FiltroTarjNume", Me.txtTarjNumeFil.Valor.ToString)
            ' fin cambio

            mConsultar()
        End Sub
        Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub
        Private Sub imgCloseFil_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCloseFil.Click
            mCerrar()
        End Sub
        Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            mAgregar()
        End Sub

        Public Overrides Sub grdTele_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdTele.EditItemIndex = -1
                If (grdTele.CurrentPageIndex < 0 Or grdTele.CurrentPageIndex >= grdTele.PageCount) Then
                    grdTele.CurrentPageIndex = 0
                Else
                    grdTele.CurrentPageIndex = E.NewPageIndex
                End If
                grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
                grdTele.DataBind()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Overrides Sub grdDire_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDire.EditItemIndex = -1
                If (grdDire.CurrentPageIndex < 0 Or grdDire.CurrentPageIndex >= grdDire.PageCount) Then
                    grdDire.CurrentPageIndex = 0
                Else
                    grdDire.CurrentPageIndex = E.NewPageIndex
                End If
                grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
                grdDire.DataBind()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Overrides Sub grdMail_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdMail.EditItemIndex = -1
                If (grdMail.CurrentPageIndex < 0 Or grdMail.CurrentPageIndex >= grdMail.PageCount) Then
                    grdMail.CurrentPageIndex = 0
                Else
                    grdMail.CurrentPageIndex = E.NewPageIndex
                End If
                grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
                grdMail.DataBind()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Overrides Sub grdIntegr_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdIntegr.EditItemIndex = -1
                If (grdIntegr.CurrentPageIndex < 0 Or grdIntegr.CurrentPageIndex >= grdIntegr.PageCount) Then
                    grdIntegr.CurrentPageIndex = 0
                Else
                    grdIntegr.CurrentPageIndex = E.NewPageIndex
                End If
                grdIntegr.DataSource = mdsDatos.Tables(mstrClieInte).Select("_peti_id = 2 and _clie_agru = 0")
                grdIntegr.DataBind()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Function mSess(ByVal pstrTabla As String) As String
            If hdnSess.Text = "" Then
                hdnSess.Text = pstrTabla & Now.Millisecond.ToString
            End If
            Return (hdnSess.Text)
        End Function
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
        Private Sub hdnLocaPop_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdnLocaPop.TextChanged
            Try
                If (hdnLocaPop.Text <> "") Then
                    If panCabecera.Visible Then
                        cmbDefaDireLoca.Valor = hdnLocaPop.Text
                        txtDefaDireCP.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "localidades", cmbDefaDireLoca.Valor.ToString, "loca_cpos").ToString
                    Else
                        cmbDireLoca.Valor = hdnLocaPop.Text
                        txtDireCP.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "localidades", cmbDefaDireLoca.Valor.ToString, "loca_cpos").ToString
                    End If
                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try

        End Sub
        Private Sub hdnDatosPop_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
            Try
                If (hdnDatosPop.Text <> "") Then

                    mstrCmd = "exec dbo.dire_clientes_consul "
                    mstrCmd += hdnDatosPop.Text

                    Dim ds As New DataSet
                    ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

                    If ds.Tables(0).Rows.Count > 0 Then
                        With ds.Tables(0).Rows(0)
                            txtDire.Valor = .Item("dicl_dire")
                            txtDireCP.Valor = .Item("dicl_cpos")
                            txtDireRefe.Valor = .Item("dicl_refe")
                            cmbDirePais.Valor = .Item("_dicl_pais_id")
                            clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
                            cmbDirePcia.Valor = .Item("_dicl_pcia_id")
                            clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
                            clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbDirePcia.Valor, cmbLocaAux, "id", "descrip", "S", True)
                            cmbDireLoca.Valor = .Item("dicl_loca_id")
                        End With
                    End If
                    hdnDatosPop.Text = ""

                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try

        End Sub
#End Region

#Region "Detalle"
        Public Overrides Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnTeleId.Text = E.Item.Cells(1).Text

                mCargarDatosTele(hdnTeleId.Text, False, txtTeleArea, txtTeleNume, txtTeleRefe, cmbTeleTipo)

                mSetearEditor(mstrClieTele, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCargarDatosTele(ByVal pstrId As String, ByVal pbooDefa As Boolean,
        ByVal ptxtArea As NixorControls.TextBoxTab, ByVal ptxtNume As NixorControls.TextBoxTab,
        ByVal ptxtRefe As NixorControls.TextBoxTab, ByVal pcmbTipo As NixorControls.ComboBox)

            Dim ldrTele As DataRow
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lbooGene As Boolean


            With mdsDatos.Tables(mstrClieTele).Select(IIf(pbooDefa, "tecl_defa=1", "tecl_id=" + pstrId))
                If .GetUpperBound(0) > -1 Then
                    ldrTele = .GetValue(0)
                Else
                    If pbooDefa Then
                        'mLimpiarTelefono(True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)
                    End If
                    Exit Sub
                End If
            End With



            With ldrTele
                ptxtArea.Valor = .Item("tecl_area_code")
                ptxtNume.Valor = .Item("tecl_tele")

                ptxtRefe.Valor = IIf(.IsNull("tecl_refe"), "", .Item("tecl_refe"))
                pcmbTipo.Valor = .Item("tecl_teti_id")
                lstrActiProp = .Item("acti_prop").ToString
                lstrActiNoProp = .Item("acti_noprop").ToString
                lbooGene = .Item("tecl_gene")

                If Not pbooDefa Then
                    chkTeleDefa.Checked = IIf(.IsNull("tecl_defa"), False, .Item("tecl_defa"))
                End If
            End With

            If Not pbooDefa Then
                If mbooActi Then
                    mHabilitarTele(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
                End If

                mCargarActividades(grdActiTele, lstrActiProp, lstrActiNoProp)
                grdActiTele.Visible = True
            End If
        End Sub

        Public Overrides Sub mEditarDatosDire(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnDireId.Text = E.Item.Cells(1).Text

                mCargarDatosDire(hdnDireId.Text, False, txtDire, txtDireCP, txtDireRefe, cmbDirePais, cmbDirePcia, cmbDireLoca)

                mSetearEditor(mstrClieDire, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCargarDatosDire(ByVal pstrId As String, ByVal pbooDefa As Boolean,
        ByVal ptxtDire As NixorControls.TextBoxTab, ByVal ptxtCP As NixorControls.CodPostalBox,
        ByVal ptxtRefe As NixorControls.TextBoxTab, ByVal pcmbPais As NixorControls.ComboBox,
        ByVal pcmbPcia As NixorControls.ComboBox, ByVal pcmbLoca As NixorControls.ComboBox)

            Dim ldrDire As DataRow
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lbooGene As Boolean

            With mdsDatos.Tables(mstrClieDire).Select(IIf(pbooDefa, "dicl_defa=1", "dicl_id=" + pstrId))
                If .GetUpperBound(0) > -1 Then
                    ldrDire = .GetValue(0)
                Else
                    If pbooDefa Then
                        'mLimpiarDireccion(True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais, cmbDefaDirePcia, cmbDefaDireLoca)
                    End If
                    Exit Sub
                End If
            End With

            With ldrDire
                pcmbPais.Valor = .Item("_dicl_pais_id").ToString
                ptxtDire.Valor = .Item("dicl_dire").ToString
                ptxtCP.Valor = .Item("dicl_cpos").ToString
                ptxtCP.ValidarPaisDefault = IIf(pcmbPais.Valor = Session("sPaisDefaId"), "S", "N")
                ptxtRefe.Valor = .Item("dicl_refe").ToString
                pcmbPcia.Limpiar()
                'If (pcmbPcia.Items.Count = 0) Then
                clsWeb.gCargarRefeCmb(mstrConn, "provincias", pcmbPcia, "S", pcmbPais.Valor.ToString)
                'End If
                pcmbPcia.Valor = IIf(IsDBNull(.Item("_dicl_pcia_id")), String.Empty, .Item("_dicl_pcia_id"))
                pcmbLoca.Limpiar()
                clsWeb.gCargarRefeCmb(mstrConn, "localidades", pcmbLoca, "S", "0" + pcmbPcia.Valor.ToString)
                pcmbLoca.Valor = .Item("dicl_loca_id").ToString
                lstrActiProp = .Item("acti_prop").ToString
                lstrActiNoProp = .Item("acti_noprop").ToString
                lbooGene = .Item("dicl_gene").ToString
                .Item("_defa") = IIf(.Item("dicl_defa"), "X", "")
                If Not pbooDefa Then
                    chkDireDefa.Checked = IIf(.IsNull("dicl_defa"), False, .Item("dicl_defa"))

                End If
            End With

            If Not pbooDefa Then
                If mbooActi Then
                    mHabilitarDire(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
                End If

                mCargarActividades(grdActiDire, lstrActiProp, lstrActiNoProp)
                grdActiDire.Visible = True
            End If
        End Sub

        Public Overrides Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnMailId.Text = E.Item.Cells(1).Text

                mCargarDatosMail(hdnMailId.Text, False, txtMail, txtMailRefe)

                mSetearEditor(mstrClieMail, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCargarDatosMail(ByVal pstrId As String, ByVal pbooDefa As Boolean,
        ByVal ptxtMail As NixorControls.TextBoxTab, ByVal ptxtRefe As NixorControls.TextBoxTab)
            Dim ldrMail As DataRow
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lbooGene As Boolean

            With mdsDatos.Tables(mstrClieMail).Select(IIf(pbooDefa, "macl_defa=1", "macl_id=" + pstrId))
                If .GetUpperBound(0) > -1 Then
                    ldrMail = .GetValue(0)
                Else
                    If pbooDefa Then
                        'mLimpiarMail(True, ptxtMail, ptxtRefe)
                    End If
                    Exit Sub
                End If
            End With

            With ldrMail
                ptxtMail.Valor = .Item("macl_mail")
                ptxtRefe.Valor = .Item("macl_refe")
                lstrActiProp = .Item("acti_prop").ToString
                lstrActiNoProp = .Item("acti_noprop").ToString
                lbooGene = .Item("macl_gene")

                If Not pbooDefa Then
                    chkMailDefa.Checked = IIf(.IsNull("macl_defa"), False, .Item("macl_defa"))
                End If
            End With

            If Not pbooDefa Then
                If mbooActi Then
                    mHabilitarMail(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
                End If

                mCargarActividades(grdActiMail, lstrActiProp, lstrActiNoProp)
                grdActiMail.Visible = True
            End If
        End Sub
        Public Overrides Sub mEditarDatosInte(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnIntegrId.Text = E.Item.Cells(3).Text

                mCargarDatosInte()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Overrides Sub mEditarDatosIntegr(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                hdnIntegrId.Text = E.Item.Cells(1).Text

                mCargarDatosIntegr()

                mSetearEditor(mstrClieInte & "_juridicos", False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCargarDatosInte()
            Dim ldrDeta As DataRow
            ldrDeta = mdsDatos.Tables(mstrClieInte).Select("clag_id=" & hdnIntegrId.Text)(0)
            With ldrDeta
                usrClieFil.Valor = .Item("clag_clie_clie_id")
                chkResp.Checked = IIf(.IsNull("clag_resp"), False, .Item("clag_resp"))
            End With
        End Sub
        Private Sub mCargarDatosIntegr()
            Dim ldrDeta As DataRow

            ldrDeta = mdsDatos.Tables(mstrClieInte).Select("clag_id=" & hdnIntegrId.Text)(0)

            With ldrDeta
                usrIntegr.Valor = .Item("clag_clie_clie_id")
                chkInteResp.Checked = IIf(.IsNull("clag_resp"), False, .Item("clag_resp"))
                If Not .IsNull("baja_fecha") Then
                    lblBajaIntegr.Text = "Registro dado de baja en fecha: " & CDate(.Item("baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaIntegr.Text = ""
                End If
            End With

            mSetearEditor(mstrClieInte & "_juridicos", False)
        End Sub

        Private Sub mActualizarTelefono()
            Try
                mGuardarDatosTele(False, txtTeleArea, txtTeleNume, txtTeleRefe, cmbTeleTipo)
                mLimpiarTelefono(False, txtTeleArea, txtTeleNume, txtTeleRefe, cmbTeleTipo)

                grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
                grdTele.DataBind()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mGuardarDatosTele(ByVal pbooDefa As Boolean, ByVal ptxtArea As NixorControls.TextBoxTab,
        ByVal ptxtNume As NixorControls.TextBoxTab, ByVal ptxtRefe As NixorControls.TextBoxTab,
        ByVal pcmbTipo As NixorControls.ComboBox)
            Dim intClienteId As Integer
            Dim ldrDatos As DataRow
            Dim lstrCheck As System.Web.UI.WebControls.CheckBox
            Dim lstrProp As NixorControls.ComboBox
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lstrActiPropDesc As String
            Dim lbooDefault As Boolean
            Dim lstrTeleId As String

            intClienteId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("clie_id"), False)

            If pbooDefa And ptxtArea.Text = "" And ptxtNume.Text = "" And ptxtRefe.Text = "" Then 'graba el tel�fono en la default

                Dim cantTele As Integer
                cantTele = mdsDatos.Tables(mstrClieTele).Rows.Count
                If cantTele <= 0 Then
                    Exit Sub

                End If
                If hdnModiTeleAdic.Text <> "S" Then
                    For Each dr As DataRow In mdsDatos.Tables(mstrClieTele).Select("tecl_defa=1")
                        lstrTeleId = dr.Item("tecl_id")

                        dr.Item("tecl_baja_fecha") = System.DateTime.Now.ToString
                        dr.Item("tecl_defa") = False
                        dr.Item("_estado") = "Inactivo"
                        mdsDatos.AcceptChanges()
                        Exit For
                    Next

                    Dim intTeleAdicionalId As Integer
                    Dim ldrTeleAdicional As DataRow
                    Dim oClientes As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
                    ldrTeleAdicional = oClientes.GetTeleAdicionalByClienteId(intClienteId)
                    If Not ldrTeleAdicional Is Nothing Then
                        intTeleAdicionalId = ldrTeleAdicional.Item("tecl_id")

                        If intTeleAdicionalId <> 0 Then

                            With mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & intTeleAdicionalId)(0)
                                If mdsDatos.Tables(mstrClieTele).Rows.Count > 0 Then
                                    mdsDatos.Tables(mstrClieTele).Rows(1).Item("tecl_defa") = True
                                    mdsDatos.Tables(mstrClieTele).Rows(1).Item("_defa") = "X"

                                    mdsDatos.AcceptChanges()
                                End If
                            End With
                        End If
                    End If
                End If
                Exit Sub
            Else
                If ptxtNume.Text <> "" Then
                    If ptxtArea.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar el C�digo de Area.")
                    End If
                End If
                lstrTeleId = hdnTeleId.Text
            End If

            If ptxtNume.Text <> "" Then
                If ptxtNume.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el N�mero de Tel�fono.")
                End If

                If ptxtArea.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el C�digo de Area.")
                End If

                If pcmbTipo.SelectedValue = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el Tipo de Tel�fono.")
                End If
            End If
            ' Dario 2013-05-09 cambio para que de de alta 
            If (lstrTeleId = "") Then
                lstrTeleId = "-1"
            End If
            If (System.Convert.ToInt64(lstrTeleId) = -1) Then
                If ptxtNume.Text <> "" Then
                    If ptxtArea.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar el C�digo de Area.")
                    End If
                End If

                If ldrDatos Is Nothing Then
                    ldrDatos = mdsDatos.Tables(mstrClieTele).NewRow
                End If

                Dim index As Integer = mdsDatos.Tables(mstrClieTele).Rows.Count - 100

                ' Dario 2013-05-08 Cambio no registra alta de tel
                'If mbooActi Then
                '    ldrDatos.Item("acti_prop") = mintActiDefa
                'End If
                If (hdnAltaDireAdic.Text = "S") Then ' alta nuevo
                    With ldrDatos
                        If ValidarNulos(ptxtNume.Valor, False) = "0" Then
                            .Item("tecl_baja_fecha") = DateTime.Now
                            .Item("tecl_defa") = 0
                        Else
                            .Item("tecl_id") = index
                            .Item("tecl_tele") = ptxtNume.Valor
                            .Item("tecl_area_code") = ptxtArea.Valor
                            .Item("_tele_desc") = ptxtArea.Valor & " " & ptxtNume.Valor
                            .Item("tecl_refe") = ptxtRefe.Valor
                            .Item("tecl_teti_id") = pcmbTipo.Valor
                            If (mdsDatos.Tables(mstrClieTele).Rows.Count = 0) Then
                                .Item("tecl_defa") = True
                                .Item("_defa") = "X"
                            Else
                                If (Not lbooDefault And chkTeleDefa.Checked) Then
                                    Me.mBlanquearDefaults(mstrClieTele, "tecl")
                                End If
                                .Item("tecl_defa") = lbooDefault Or (Not lbooDefault And chkTeleDefa.Checked)
                                .Item("_defa") = IIf(.Item("tecl_defa"), "X", "")
                            End If
                        End If

                        If System.Convert.ToInt64(lstrTeleId) < 1 Then
                            .Item("tecl_gene") = Not mbooActi
                        End If

                        If Not pbooDefa Then
                            If .Item("tecl_gene") Then
                                If lstrActiNoProp = "" Then
                                    lstrActiNoProp = lstrActiProp
                                Else
                                    lstrActiNoProp += "," & lstrActiProp
                                End If
                            End If
                            .Item("acti_noprop") = lstrActiNoProp
                            If Not .Item("tecl_gene") Then
                                .Item("_prop") = lstrActiPropDesc
                            Else
                                .Item("acti_prop") = ""
                            End If
                        End If
                    End With
                ElseIf (mdsDatos.Tables(mstrClieTele).Rows.Count = 0) Then ' alta en la grilla cuando se carga desde la ppal
                    If (ptxtArea.Valor.ToString.Length > 0 And ptxtNume.Valor.ToString.Length > 0 _
                        And pcmbTipo.SelectedIndex > 0) Then
                        With ldrDatos
                            If ValidarNulos(ptxtNume.Valor, False) = "0" Then
                                .Item("tecl_baja_fecha") = DateTime.Now
                                .Item("tecl_defa") = 0
                            Else
                                .Item("tecl_id") = index
                                .Item("tecl_tele") = ptxtNume.Valor
                                .Item("tecl_area_code") = ptxtArea.Valor
                                .Item("_tele_desc") = ptxtArea.Valor & " " & ptxtNume.Valor
                                .Item("tecl_refe") = ptxtRefe.Valor
                                .Item("tecl_teti_id") = pcmbTipo.Valor
                                .Item("tecl_defa") = True
                                .Item("_defa") = "X"
                            End If

                            If System.Convert.ToInt64(lstrTeleId) < 1 Then
                                .Item("tecl_gene") = Not mbooActi
                            End If

                            If Not pbooDefa Then
                                If .Item("tecl_gene") Then
                                    If lstrActiNoProp = "" Then
                                        lstrActiNoProp = lstrActiProp
                                    Else
                                        lstrActiNoProp += "," & lstrActiProp
                                    End If
                                End If
                                .Item("acti_noprop") = lstrActiNoProp
                                If Not .Item("tecl_gene") Then
                                    .Item("_prop") = lstrActiPropDesc
                                Else
                                    .Item("acti_prop") = ""
                                End If
                            End If
                        End With
                        hdnAltaDireAdic.Text = "S"
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieTele).Select()(0)
                        End If
                    End If
                ElseIf (mdsDatos.Tables(mstrClieTele).Rows.Count = 1) Then ' modificacion del tele en el ds desde la ppal
                    If (ptxtArea.Valor.ToString.Length > 0 And ptxtNume.Valor.ToString.Length > 0 _
                        And pcmbTipo.SelectedIndex > 0) Then
                        ldrDatos = mdsDatos.Tables(mstrClieTele).Select("tecl_defa=1")(0)
                        With ldrDatos
                            If ValidarNulos(ptxtNume.Valor, False) = "0" Then
                                .Item("tecl_baja_fecha") = DateTime.Now
                                .Item("tecl_defa") = 0
                            Else
                                .Item("tecl_tele") = ptxtNume.Valor
                                .Item("tecl_area_code") = ptxtArea.Valor
                                .Item("_tele_desc") = ptxtArea.Valor & " " & ptxtNume.Valor
                                .Item("tecl_refe") = ptxtRefe.Valor
                                .Item("tecl_teti_id") = pcmbTipo.Valor
                                .Item("tecl_defa") = True
                                .Item("_defa") = "X"
                            End If
                            If System.Convert.ToInt64(lstrTeleId) < 1 Then
                                .Item("tecl_gene") = Not mbooActi
                            End If

                            If Not pbooDefa Then
                                If .Item("tecl_gene") Then
                                    If lstrActiNoProp = "" Then
                                        lstrActiNoProp = lstrActiProp
                                    Else
                                        lstrActiNoProp += "," & lstrActiProp
                                    End If
                                End If
                                .Item("acti_noprop") = lstrActiNoProp
                                If Not .Item("tecl_gene") Then
                                    .Item("_prop") = lstrActiPropDesc
                                Else
                                    .Item("acti_prop") = ""
                                End If
                            End If
                        End With
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieTele).Select()(0)
                        End If
                    End If
                ElseIf (mdsDatos.Tables(mstrClieTele).Rows.Count > 1) Then ' modificacion del tele en el ds desde la ppal
                    If (ptxtArea.Valor.ToString.Length > 0 And ptxtNume.Valor.ToString.Length > 0 _
                        And pcmbTipo.SelectedIndex > 0) Then
                        ldrDatos = mdsDatos.Tables(mstrClieTele).Select("tecl_defa=1")(0)
                        With ldrDatos
                            If ValidarNulos(ptxtNume.Valor, False) = "0" Then
                                .Item("tecl_baja_fecha") = DateTime.Now
                                .Item("tecl_defa") = 0
                            Else
                                .Item("tecl_tele") = ptxtNume.Valor
                                .Item("tecl_area_code") = ptxtArea.Valor
                                .Item("_tele_desc") = ptxtArea.Valor & " " & ptxtNume.Valor
                                .Item("tecl_refe") = ptxtRefe.Valor
                                .Item("tecl_teti_id") = pcmbTipo.Valor
                                .Item("tecl_defa") = True
                                .Item("_defa") = "X"
                            End If
                            If System.Convert.ToInt64(lstrTeleId) < 1 Then
                                .Item("tecl_gene") = Not mbooActi
                            End If

                            If Not pbooDefa Then
                                If .Item("tecl_gene") Then
                                    If lstrActiNoProp = "" Then
                                        lstrActiNoProp = lstrActiProp
                                    Else
                                        lstrActiNoProp += "," & lstrActiProp
                                    End If
                                End If
                                .Item("acti_noprop") = lstrActiNoProp
                                If Not .Item("tecl_gene") Then
                                    .Item("_prop") = lstrActiPropDesc
                                Else
                                    .Item("acti_prop") = ""
                                End If
                            End If
                        End With
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieTele).Select()(0)
                        End If
                    End If
                Else
                    If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                        ldrDatos = Nothing
                    Else
                        ldrDatos = mdsDatos.Tables(mstrClieTele).Select()(0)
                    End If
                End If
            Else ' modificacion
                ldrDatos = mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & lstrTeleId)(0)
                With ldrDatos
                    If ValidarNulos(ptxtNume.Valor, False) = "0" Then
                        .Item("tecl_baja_fecha") = DateTime.Now
                        .Item("tecl_defa") = 0
                    Else
                        .Item("tecl_tele") = ptxtNume.Valor
                        .Item("tecl_area_code") = ptxtArea.Valor
                        .Item("_tele_desc") = ptxtArea.Valor & " " & ptxtNume.Valor
                        .Item("tecl_refe") = ptxtRefe.Valor
                        .Item("tecl_teti_id") = pcmbTipo.Valor
                        If (mdsDatos.Tables(mstrClieTele).Rows.Count = 1) Then
                            .Item("tecl_defa") = True
                            .Item("_defa") = "X"
                        Else
                            If (Not lbooDefault And chkTeleDefa.Checked) Then
                                Me.mBlanquearDefaults(mstrClieTele, "tecl")
                            End If
                            .Item("tecl_defa") = lbooDefault Or (Not lbooDefault And chkTeleDefa.Checked)
                            .Item("_defa") = IIf(.Item("tecl_defa"), "X", "")
                        End If
                    End If
                    If lstrTeleId = "" Then
                        .Item("tecl_gene") = Not mbooActi
                    End If

                    If Not pbooDefa Then
                        If .Item("tecl_gene") Then
                            If lstrActiNoProp = "" Then
                                lstrActiNoProp = lstrActiProp
                            Else
                                lstrActiNoProp += "," & lstrActiProp
                            End If
                        End If
                        .Item("acti_noprop") = lstrActiNoProp
                        If Not .Item("tecl_gene") Then
                            .Item("_prop") = lstrActiPropDesc
                        Else
                            .Item("acti_prop") = ""
                        End If
                    End If
                End With
            End If ' fin modificacion

            If Not pbooDefa Then
                lstrActiProp = ""
                lstrActiNoProp = ""
                lstrActiPropDesc = ""
                For Each oDataItem As DataGridItem In grdActiTele.Items
                    lstrCheck = oDataItem.FindControl("chkActi")
                    lstrProp = oDataItem.FindControl("cmbActiProp")
                    If lstrCheck.Checked Then
                        'If lstrProp.Valor = "S" Then
                        If mintActiDefa.ToString = oDataItem.Cells(1).Text Then
                            If lstrActiProp <> "" Then
                                lstrActiProp += ","
                                lstrActiPropDesc += " / "
                            End If
                            lstrActiProp += oDataItem.Cells(1).Text
                            lstrActiPropDesc += oDataItem.Cells(2).Text
                        Else
                            If lstrActiNoProp <> "" Then
                                lstrActiNoProp += ","
                            End If
                            lstrActiNoProp += oDataItem.Cells(1).Text
                        End If
                    End If
                Next

                If lstrTeleId = "" And mbooActi And (lstrActiProp & lstrActiNoProp) = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar a que actividad/es pertenece el tel�fono.")
                End If
            End If

            mLimpiarDefas(mstrClieTele, "tecl", ldrDatos)

            If hdnAltaDireAdic.Text = "S" Then
                mdsDatos.Tables(mstrClieTele).Rows.Add(ldrDatos)
                hdnAltaDireAdic.Text = "N"
            End If

            grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
            grdTele.DataBind()

            'refresco los defaults
            If Not pbooDefa Then
                mCargarDatosTele("", True, txtDefaTeleArea, txtDefaTeleNume, txtDefaTeleRefe, cmbDefaTeleTipo)
            End If
        End Sub

        Private Sub mActualizarIntegrantes()
            Try
                mGuardarDatosInte()

                mLimpiarInte()
                grdInte.DataSource = mdsDatos.Tables(mstrClieInte)
                grdInte.DataBind()
                mFormatearIntegrantes()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mGuardarDatosInte()
            Dim ldrDatos As DataRow
            Dim lbooAlta As Boolean
            Dim lstrSociNume As String
            Dim lstrSociEsta As String

            If usrClieFil.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar un Cliente.")
            End If

            lbooAlta = False
            If hdnClagId.Text = "" Then
                If (mdsDatos.Tables(mstrClieInte).Select("clag_clie_clie_id=" & usrClieFil.Valor)).GetUpperBound(0) > -1 Then
                    ldrDatos = mdsDatos.Tables(mstrClieInte).Select("clag_clie_clie_id=" & usrClieFil.Valor)(0)
                Else
                    ldrDatos = mdsDatos.Tables(mstrClieInte).NewRow
                    ldrDatos.Item("clag_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieInte), "clag_id")
                    lbooAlta = True
                End If
            Else
                ldrDatos = mdsDatos.Tables(mstrClieInte).Select("clagl_id=" & hdnClagId.Text)(0)
            End If

            With ldrDatos
                .Item("baja_fecha") = System.DBNull.Value
                .Item("_estado") = "Activo"
                .Item("_peti_id") = "-1"
                .Item("clag_resp") = chkResp.Checked
                .Item("_resp") = IIf(chkResp.Checked, "S", "")
                lstrSociNume = clsSQLServer.gCampoValorConsul(mstrConn, "socios_busq @soci_clie_id=" & usrClieFil.Valor, "soci_nume")
                lstrSociEsta = clsSQLServer.gCampoValorConsul(mstrConn, "socios_busq @soci_clie_id=" & usrClieFil.Valor, "_estado")
                If lstrSociNume <> "" Then
                    .Item("_soci_nume") = lstrSociNume
                End If
                If lstrSociEsta <> "" Then
                    .Item("_soci_esta") = lstrSociEsta
                End If
                .Item("_cliente") = usrClieFil.Valor & "-" & usrClieFil.Apel
                If lbooAlta Then
                    .Item("clag_orden") = grdInte.Items.Count + 1
                    .Item("clag_clie_id") = System.DBNull.Value
                End If
                .Item("clag_clie_clie_id") = usrClieFil.Valor
            End With

            If lbooAlta Then
                mdsDatos.Tables(mstrClieInte).Rows.Add(ldrDatos)
                mdsDatos.Tables(mstrClieInte).DefaultView.Sort() = "clag_orden"
            End If
        End Sub

        Private Sub mActualizarIntegr()
            Try
                mGuardarDatosIntegr()

                mLimpiarIntegr()
                mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = "_peti_id = 2 and _clie_agru = 0"
                grdIntegr.DataSource = mdsDatos.Tables(mstrClieInte)
                grdIntegr.DataBind()
                mdsDatos.Tables(mstrClieInte).DefaultView.RowFilter = ""
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mGuardarDatosIntegr()
            Dim ldrDatos As DataRow
            Dim lbooAlta As Boolean
            Dim lstrSociNume As String
            Dim lstrSociEsta As String

            If usrIntegr.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar un Integrante.")
            End If

            If clsSQLServer.gObtenerValorCampo(mstrConn, mstrTabla, "@clie_id = " & usrIntegr.Valor.ToString, "clie_peti_id").ToString = "3" Then
                Throw New AccesoBD.clsErrNeg("El Integrante no puede ser de tipo Gen�rico.")
            End If

            If clsSQLServer.gObtenerValorCampo(mstrConn, mstrTabla, "@clie_id = " & usrIntegr.Valor.ToString, "clie_agru").ToString = "1" Then
                Throw New AccesoBD.clsErrNeg("El Integrante no puede ser una Agrupaci�n.")
            End If

            lbooAlta = False
            If hdnIntegrId.Text <> "" Then
                ldrDatos = mdsDatos.Tables(mstrClieInte).Select("clag_id=" & hdnIntegrId.Text)(0)
            Else
                If (mdsDatos.Tables(mstrClieInte).Select("clag_clie_clie_id=" & usrIntegr.Valor)).GetUpperBound(0) > -1 Then
                    ldrDatos = mdsDatos.Tables(mstrClieInte).Select("clag_clie_clie_id=" & usrIntegr.Valor)(0)
                Else
                    lbooAlta = True
                    ldrDatos = mdsDatos.Tables(mstrClieInte).NewRow
                    ldrDatos.Item("clag_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieInte), "clag_id")
                End If
            End If

            With ldrDatos
                .Item("baja_fecha") = System.DBNull.Value
                .Item("_estado") = "Activo"
                .Item("_peti_id") = "2"
                .Item("_clie_agru") = False
                .Item("clag_orden") = 1
                .Item("clag_resp") = chkInteResp.Checked
                .Item("_resp") = IIf(chkInteResp.Checked, "S", "")
                .Item("_cliente") = usrIntegr.Valor & "-" & usrIntegr.Apel
                .Item("clag_clie_clie_id") = usrIntegr.Valor
                lstrSociNume = clsSQLServer.gCampoValorConsul(mstrConn, "socios_busq @soci_clie_id=" & usrIntegr.Valor, "soci_nume")
                lstrSociEsta = clsSQLServer.gCampoValorConsul(mstrConn, "socios_busq @soci_clie_id=" & usrIntegr.Valor, "_estado")
                If lstrSociNume <> "" Then
                    .Item("_soci_nume") = lstrSociNume
                End If
                If lstrSociEsta <> "" Then
                    .Item("_soci_esta") = lstrSociEsta
                End If

                If hdnIntegrId.Text = "" Then
                    If lbooAlta Then .Table.Rows.Add(ldrDatos)
                End If
            End With

            mdsDatos.Tables(mstrClieInte).DefaultView.Sort() = "clag_orden"
        End Sub

        Private Sub mActualizarMail()
            Try
                mGuardarDatosMail(False, txtMail, txtMailRefe)
                mLimpiarMail(False, txtMail, txtMailRefe)

                grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
                grdMail.DataBind()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mGuardarDatosMail(ByVal pbooDefa As Boolean, ByVal ptxtMail As NixorControls.TextBoxTab,
        ByVal ptxtRefe As NixorControls.TextBoxTab)
            Dim ldrDatos As DataRow
            Dim lstrCheck As System.Web.UI.WebControls.CheckBox
            Dim lstrProp As NixorControls.ComboBox
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lstrActiPropDesc As String
            Dim lbooDefault As Boolean
            Dim lstrMailId As String
            Dim intClienteId As Integer

            Dim intMailAdicionalId As Integer
            Dim ldrMailAdicional As DataRow
            Dim mbooMailAsignadoAuto As Boolean

            intClienteId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("clie_id"), False)

            If pbooDefa And ptxtMail.Text = "" And ptxtRefe.Text = "" Then  'si es default y no tiene direcci�n, lo borra
                ''inicio
                If hdnModiMailAdic.Text <> "S" Then
                    For Each dr As DataRow In mdsDatos.Tables(mstrClieMail).Select("macl_defa=1")
                        'mdsDatos.Tables(mstrClieMail).Rows.Remove(dr)
                        dr.Item("macl_baja_fecha") = System.DateTime.Now.ToString
                        dr.Item("macl_defa") = False
                        dr.Item("_estado") = "Inactivo"
                        mdsDatos.AcceptChanges()
                        Exit For
                    Next

                    Dim oClientes As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
                    ldrMailAdicional = oClientes.GetMailAdicionalByClienteId(intClienteId)
                    If Not ldrMailAdicional Is Nothing Then
                        intMailAdicionalId = ldrMailAdicional.Item("macl_id")

                        If intMailAdicionalId <> 0 Then

                            With mdsDatos.Tables(mstrClieMail).Select("macl_id=" & intMailAdicionalId)(0)
                                If mdsDatos.Tables(mstrClieMail).Rows.Count > 0 Then
                                    mdsDatos.Tables(mstrClieMail).Rows(0).Item("macl_defa") = True
                                    mdsDatos.Tables(mstrClieMail).Rows(0).Item("_defa") = "X"
                                    mbooMailAsignadoAuto = True
                                    mdsDatos.AcceptChanges()
                                End If
                            End With
                        End If
                    End If
                    Exit Sub
                End If
                'If pbooDefa Then  'graba la Teleccion en la default
                '    For Each dr As DataRow In mdsDatos.Tables(mstrClieMail).Select("macl_defa=1")
                '        lstrMailId = dr.Item("macl_id")
                '        Exit For
                '    Next
            Else
                If ptxtMail.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar una Direcci�n de Mail.")
                End If
                lstrMailId = hdnMailId.Text
            End If

            'ldrDatos.Item("macl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieMail), "macl_id")
            'If mbooActi Then
            '    ldrDatos.Item("acti_prop") = mintActiDefa
            'End If

            ' Dario 2013-05-09 cambio para que de de alta 
            If (lstrMailId = "") Then
                lstrMailId = "-1"
            End If

            If (System.Convert.ToInt64(lstrMailId) = -1) Then  ' alta nueva
                If ldrDatos Is Nothing Then
                    ldrDatos = mdsDatos.Tables(mstrClieMail).NewRow
                End If

                Dim index As Integer = mdsDatos.Tables(mstrClieMail).Rows.Count - 100

                If (hdnAltaDireAdic.Text = "S") Then ' alta nuevo
                    With ldrDatos
                        .Item("macl_id") = index
                        .Item("macl_baja_fecha") = System.DBNull.Value
                        .Item("macl_mail") = ptxtMail.Valor
                        .Item("macl_refe") = ptxtRefe.Valor
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            .Item("macl_defa") = True
                            .Item("_defa") = "X"
                        Else
                            If (Not lbooDefault And chkMailDefa.Checked) Then
                                Me.mBlanquearDefaults(mstrClieMail, "macl")
                            End If
                            .Item("macl_defa") = lbooDefault Or (Not lbooDefault And chkMailDefa.Checked)
                            .Item("_defa") = IIf(.Item("macl_defa"), "X", "")
                        End If
                        If (System.Convert.ToInt64(lstrMailId) = -1) Then
                            .Item("macl_gene") = Not mbooActi
                        End If
                        If Not pbooDefa Then
                            If .Item("macl_gene") Then
                                If lstrActiNoProp = "" Then
                                    lstrActiNoProp = lstrActiProp
                                Else
                                    lstrActiNoProp += "," & lstrActiProp
                                End If
                            End If
                            .Item("acti_noprop") = lstrActiNoProp
                            If Not .Item("macl_gene") Then
                                .Item("_prop") = lstrActiPropDesc
                            Else
                                .Item("acti_prop") = ""
                            End If
                        End If
                    End With
                ElseIf (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then ' alta en la grilla cuando se carga desde la ppal
                    If (ptxtMail.Valor.ToString.Length > 0 And ptxtRefe.Valor.ToString.Length > 0) Then
                        With ldrDatos
                            .Item("macl_id") = index
                            .Item("macl_baja_fecha") = System.DBNull.Value
                            .Item("macl_mail") = ptxtMail.Valor
                            .Item("macl_refe") = ptxtRefe.Valor
                            If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                                .Item("macl_defa") = True
                                .Item("_defa") = "X"
                            Else
                                .Item("macl_defa") = lbooDefault Or (Not lbooDefault And chkMailDefa.Checked)
                                .Item("_defa") = IIf(.Item("macl_defa"), "X", "")
                            End If
                            If (System.Convert.ToInt64(lstrMailId) = -1) Then
                                .Item("macl_gene") = Not mbooActi
                            End If
                            If Not pbooDefa Then
                                If .Item("macl_gene") Then
                                    If lstrActiNoProp = "" Then
                                        lstrActiNoProp = lstrActiProp
                                    Else
                                        lstrActiNoProp += "," & lstrActiProp
                                    End If
                                End If
                                .Item("acti_noprop") = lstrActiNoProp
                                If Not .Item("macl_gene") Then
                                    .Item("_prop") = lstrActiPropDesc
                                Else
                                    .Item("acti_prop") = ""
                                End If
                            End If
                        End With
                        hdnAltaDireAdic.Text = "S"
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieMail).Select()(0)
                        End If
                    End If
                ElseIf (mdsDatos.Tables(mstrClieMail).Rows.Count = 1) Then ' modificacion del mail en el ds desde la ppal
                    If (ptxtMail.Valor.ToString.Length > 0 And ptxtRefe.Valor.ToString.Length > 0) Then
                        ldrDatos = mdsDatos.Tables(mstrClieMail).Select("macl_defa=1")(0)
                        With ldrDatos
                            .Item("macl_baja_fecha") = System.DBNull.Value
                            .Item("macl_mail") = ptxtMail.Valor
                            .Item("macl_refe") = ptxtRefe.Valor
                            If (mdsDatos.Tables(mstrClieMail).Rows.Count = 1) Then
                                .Item("macl_defa") = True
                                .Item("_defa") = "X"
                            Else
                                .Item("macl_defa") = lbooDefault Or (Not lbooDefault And chkMailDefa.Checked)
                                .Item("_defa") = IIf(.Item("macl_defa"), "X", "")
                            End If
                            If (System.Convert.ToInt64(lstrMailId) = -1) Then
                                .Item("macl_gene") = Not mbooActi
                            End If
                            If Not pbooDefa Then
                                If .Item("macl_gene") Then
                                    If lstrActiNoProp = "" Then
                                        lstrActiNoProp = lstrActiProp
                                    Else
                                        lstrActiNoProp += "," & lstrActiProp
                                    End If
                                End If
                                .Item("acti_noprop") = lstrActiNoProp
                                If Not .Item("macl_gene") Then
                                    .Item("_prop") = lstrActiPropDesc
                                Else
                                    .Item("acti_prop") = ""
                                End If
                            End If
                        End With
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieMail).Select()(0)
                        End If
                    End If
                ElseIf (mdsDatos.Tables(mstrClieMail).Rows.Count > 1) Then ' modificacion del mail en el ds desde la ppal
                    If (ptxtMail.Valor.ToString.Length > 0 And ptxtRefe.Valor.ToString.Length > 0) Then
                        ldrDatos = mdsDatos.Tables(mstrClieMail).Select("macl_defa=1")(0)
                        With ldrDatos
                            .Item("macl_baja_fecha") = System.DBNull.Value
                            .Item("macl_mail") = ptxtMail.Valor
                            .Item("macl_refe") = ptxtRefe.Valor
                            .Item("macl_defa") = True
                            .Item("_defa") = "X"
                            If (System.Convert.ToInt64(lstrMailId) = -1) Then
                                .Item("macl_gene") = Not mbooActi
                            End If
                            If Not pbooDefa Then
                                If .Item("macl_gene") Then
                                    If lstrActiNoProp = "" Then
                                        lstrActiNoProp = lstrActiProp
                                    Else
                                        lstrActiNoProp += "," & lstrActiProp
                                    End If
                                End If
                                .Item("acti_noprop") = lstrActiNoProp
                                If Not .Item("macl_gene") Then
                                    .Item("_prop") = lstrActiPropDesc
                                Else
                                    .Item("acti_prop") = ""
                                End If
                            End If
                        End With
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieMail).Select()(0)
                        End If
                    End If
                Else
                    If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                        ldrDatos = Nothing
                    Else
                        ldrDatos = mdsDatos.Tables(mstrClieMail).Select()(0)
                    End If
                End If
            Else ' modificacion
                ldrDatos = mdsDatos.Tables(mstrClieMail).Select("macl_id=" & lstrMailId)(0)
                With ldrDatos
                    If ValidarNulos(ptxtMail.Valor, False) = "0" Or ValidarNulos(ptxtMail.Valor, False) = "" Then
                        .Item("macl_baja_fecha") = DateTime.Now
                        .Item("macl_defa") = 0
                    Else
                        .Item("macl_baja_fecha") = System.DBNull.Value
                        .Item("macl_mail") = ptxtMail.Valor
                        .Item("macl_refe") = ptxtRefe.Valor
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 1) Then
                            .Item("macl_defa") = True
                            .Item("_defa") = "X"
                        Else
                            If (Not lbooDefault And chkMailDefa.Checked) Then
                                Me.mBlanquearDefaults(mstrClieMail, "macl")
                            End If
                            .Item("macl_defa") = lbooDefault Or (Not lbooDefault And chkMailDefa.Checked)
                            .Item("_defa") = IIf(.Item("macl_defa"), "X", "")
                        End If
                    End If

                    If hdnMailId.Text = "" Then
                        .Item("macl_gene") = Not mbooActi
                    End If

                    If Not pbooDefa Then
                        If .Item("macl_gene") Then
                            If lstrActiNoProp = "" Then
                                lstrActiNoProp = lstrActiProp
                            Else
                                lstrActiNoProp += "," & lstrActiProp
                            End If
                        End If
                        .Item("acti_noprop") = lstrActiNoProp
                        If Not .Item("macl_gene") Then
                            .Item("_prop") = lstrActiPropDesc
                        Else
                            .Item("acti_prop") = ""
                        End If
                    End If
                End With
            End If

            If Not pbooDefa Then
                lstrActiProp = ""
                lstrActiNoProp = ""
                For Each oDataItem As DataGridItem In grdActiMail.Items
                    lstrCheck = oDataItem.FindControl("chkActi")
                    lstrProp = oDataItem.FindControl("cmbActiProp")
                    If lstrCheck.Checked Then
                        If mintActiDefa.ToString = oDataItem.Cells(1).Text Then
                            If lstrActiProp <> "" Then
                                lstrActiProp += ","
                                lstrActiPropDesc += " / "
                            End If
                            lstrActiProp += oDataItem.Cells(1).Text
                            lstrActiPropDesc += oDataItem.Cells(2).Text
                        Else
                            If lstrActiNoProp <> "" Then
                                lstrActiNoProp += ","
                            End If
                            lstrActiNoProp += oDataItem.Cells(1).Text
                        End If
                    End If
                Next

                If lstrMailId = "" And mbooActi And (lstrActiProp & lstrActiNoProp) = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar a que actividad/es pertenece el mail.")
                End If
            End If

            'lbooDefault = mActualizarDefaults(pbooDefa, mstrClieMail, "macl")

            mLimpiarDefas(mstrClieMail, "macl", ldrDatos)

            If hdnAltaDireAdic.Text = "S" Then
                mdsDatos.Tables(mstrClieMail).Rows.Add(ldrDatos)
                hdnAltaDireAdic.Text = "N"
            End If

            grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
            grdMail.DataBind()

            'refresco los defaults
            If Not pbooDefa Then
                mCargarDatosMail("", True, txtDefaMail, txtDefaMailRefe)
            End If
        End Sub

        Private Sub mActualizarDireccion()
            Try
                mGuardarDatosDire(False, txtDire, txtDireCP, txtDireRefe, cmbDirePais, cmbDirePcia, cmbDireLoca)
                mLimpiarDireccion(False, txtDire, txtDireCP, txtDireRefe, cmbDirePais, cmbDirePcia, cmbDireLoca)

                grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
                grdDire.DataBind()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mGuardarDatosDire(ByVal pbooDefa As Boolean, ByVal ptxtDire As NixorControls.TextBoxTab, ByVal ptxtCP As NixorControls.CodPostalBox, ByVal ptxtRefe As NixorControls.TextBoxTab, ByVal pcmbPais As NixorControls.ComboBox, ByVal pcmbPcia As NixorControls.ComboBox, ByVal pcmbLoca As NixorControls.ComboBox)
            Dim ldrDatos As DataRow
            Dim lstrCheck As System.Web.UI.WebControls.CheckBox
            Dim lstrProp As NixorControls.ComboBox
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lstrActiPropDesc As String
            Dim lbooDefault As Boolean
            Dim lstrDireId As String

            If pbooDefa And ptxtDire.Text = "" Then   'si es default y no tiene direcci�n, lo borra
                Exit Sub
            End If

            If pbooDefa And ptxtDire.Text = "" And ptxtRefe.Text = "" Then 'graba la direccion en la default

                Dim cantDire As Integer
                cantDire = mdsDatos.Tables(mstrClieDire).Rows.Count
                If cantDire <= 0 Then
                    Exit Sub

                End If


                For Each dr As DataRow In mdsDatos.Tables(mstrClieDire).Select("dicl_defa=1")
                    lstrDireId = dr.Item("dicl_id")
                    Exit For
                Next
            Else
                If ptxtDire.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar una Direcci�n.")
                End If
                lstrDireId = hdnDireId.Text
            End If

            If Not pbooDefa Or ptxtDire.Text <> "" Then  'si no es default o ingres� la direcci�n, valido los dem�s datos
                If pcmbLoca.SelectedValue = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la Localidad.")
                End If
            End If

            ' Dario 2013-05-09 cambio para que de de alta 
            If (lstrDireId = "") Then
                lstrDireId = "-1"
            End If

            ' Dario 2013-05-07 cambio por error en el alta de cliente domi default    
            If (System.Convert.ToInt64(lstrDireId) = -1) Then
                If ldrDatos Is Nothing Then
                    ldrDatos = mdsDatos.Tables(mstrClieDire).NewRow
                End If
                Dim index As Integer = mdsDatos.Tables(mstrClieDire).Rows.Count - 100

                If (hdnAltaDireAdic.Text = "S") Then ' alta nuevo
                    With ldrDatos
                        .Item("dicl_id") = index
                        .Item("dicl_baja_fecha") = System.DBNull.Value
                        .Item("_estado") = "Activo"
                        .Item("dicl_dire") = ptxtDire.Valor.ToUpper
                        .Item("dicl_refe") = ptxtRefe.Valor
                        .Item("dicl_cpos") = ptxtCP.Valor
                        .Item("_dire_desc") = ptxtDire.Valor
                        .Item("dicl_loca_id") = pcmbLoca.Valor
                        If (pcmbPcia.Valor.Trim().ToString().Length > 0) Then
                            .Item("_dicl_pcia_id") = pcmbPcia.Valor
                        End If
                        .Item("_dicl_pais_id") = pcmbPais.Valor
                        If (mdsDatos.Tables(mstrClieDire).Rows.Count = 0) Then
                            .Item("dicl_defa") = True
                            .Item("_defa") = "X"
                        Else
                            If (Not lbooDefault And chkDireDefa.Checked) Then
                                Me.mBlanquearDefaults(mstrClieDire, "dicl")
                            End If
                            .Item("dicl_defa") = lbooDefault Or (Not lbooDefault And chkDireDefa.Checked)
                            .Item("_defa") = IIf(.Item("dicl_defa"), "X", "")
                        End If

                        If System.Convert.ToInt64(lstrDireId) = -1 Then
                            .Item("dicl_gene") = Not mbooActi
                        End If

                        If Not pbooDefa Then
                            If .Item("dicl_gene") Then
                                If lstrActiNoProp = "" Then
                                    lstrActiNoProp = lstrActiProp
                                Else
                                    lstrActiNoProp += "," & lstrActiProp
                                End If
                            End If
                            .Item("acti_noprop") = lstrActiNoProp
                            If Not .Item("dicl_gene") Then
                                .Item("_prop") = lstrActiPropDesc
                            Else
                                .Item("acti_prop") = ""
                            End If
                        End If
                    End With
                ElseIf (mdsDatos.Tables(mstrClieDire).Rows.Count = 0) Then
                    If (ptxtDire.Valor.ToString.Length > 0 And pcmbPais.SelectedIndex > 0 _
                        And pcmbPcia.SelectedIndex > 0 And pcmbLoca.SelectedIndex > 0) Then
                        With ldrDatos
                            .Item("dicl_id") = index
                            .Item("dicl_baja_fecha") = System.DBNull.Value
                            .Item("_estado") = "Activo"
                            .Item("dicl_dire") = ptxtDire.Valor.ToUpper
                            .Item("dicl_refe") = ptxtRefe.Valor
                            .Item("dicl_cpos") = ptxtCP.Valor
                            .Item("_dire_desc") = ptxtDire.Valor
                            .Item("dicl_loca_id") = pcmbLoca.Valor
                            .Item("_dicl_pcia_id") = pcmbPcia.Valor
                            .Item("_dicl_pais_id") = pcmbPais.Valor
                            .Item("dicl_defa") = True
                            ' si es A inhgreso como alta de cliente se setea como default
                            .Item("_defa") = "X"
                            If System.Convert.ToInt64(lstrDireId) = -1 Then
                                .Item("dicl_gene") = Not mbooActi
                            End If

                            If Not pbooDefa Then
                                If .Item("dicl_gene") Then
                                    If lstrActiNoProp = "" Then
                                        lstrActiNoProp = lstrActiProp
                                    Else
                                        lstrActiNoProp += "," & lstrActiProp
                                    End If
                                End If
                                .Item("acti_noprop") = lstrActiNoProp
                                If Not .Item("dicl_gene") Then
                                    .Item("_prop") = lstrActiPropDesc
                                Else
                                    .Item("acti_prop") = ""
                                End If
                            End If
                        End With
                        hdnAltaDireAdic.Text = "S"
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieDire).Select()(0)
                        End If
                    End If
                ElseIf (mdsDatos.Tables(mstrClieDire).Rows.Count = 1) Then ' modificacion del ppal en la ds
                    If (ptxtDire.Valor.ToString.Length > 0 And pcmbPais.SelectedIndex > 0 _
                        And pcmbPcia.SelectedIndex > 0 And pcmbLoca.SelectedIndex > 0) Then
                        ldrDatos = mdsDatos.Tables(mstrClieDire).Select("dicl_defa=1")(0)
                        ldrDatos.Item("dicl_baja_fecha") = System.DBNull.Value
                        ldrDatos.Item("_estado") = "Activo"
                        ldrDatos.Item("dicl_dire") = ptxtDire.Valor.ToUpper
                        ldrDatos.Item("dicl_refe") = ptxtRefe.Valor
                        ldrDatos.Item("dicl_cpos") = ptxtCP.Valor
                        ldrDatos.Item("_dire_desc") = ptxtDire.Valor
                        ldrDatos.Item("dicl_loca_id") = pcmbLoca.Valor
                        ldrDatos.Item("_dicl_pcia_id") = pcmbPcia.Valor
                        ldrDatos.Item("_dicl_pais_id") = pcmbPais.Valor
                        If (mdsDatos.Tables(mstrClieDire).Rows.Count = 1) Then
                            ldrDatos.Item("dicl_defa") = True
                            ldrDatos.Item("_defa") = "X"
                        Else
                            ldrDatos.Item("dicl_defa") = lbooDefault Or (Not lbooDefault And chkDireDefa.Checked)
                            ldrDatos.Item("_defa") = IIf(ldrDatos.Item("dicl_defa"), "X", "")
                        End If


                        If System.Convert.ToInt64(lstrDireId) = -1 Then
                            ldrDatos.Item("dicl_gene") = Not mbooActi
                        End If

                        If Not pbooDefa Then
                            If ldrDatos.Item("dicl_gene") Then
                                If lstrActiNoProp = "" Then
                                    lstrActiNoProp = lstrActiProp
                                Else
                                    lstrActiNoProp += "," & lstrActiProp
                                End If
                            End If
                            ldrDatos.Item("acti_noprop") = lstrActiNoProp
                            If Not ldrDatos.Item("dicl_gene") Then
                                ldrDatos.Item("_prop") = lstrActiPropDesc
                            Else
                                ldrDatos.Item("acti_prop") = ""
                            End If
                        End If
                    Else
                        If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieDire).Select()(0)
                        End If
                    End If
                ElseIf (mdsDatos.Tables(mstrClieDire).Rows.Count > 1) Then
                    'If (hdnModiDireAdic.Text = "S" And hdnModiMain.Text = "N" And hdnAltaDireAdic.Text <> "S" And lstrDireId <> "") Then
                    If (ptxtDire.Valor.ToString.Length > 0 And pcmbPais.SelectedIndex > 0 _
                        And pcmbPcia.SelectedIndex > 0 And pcmbLoca.SelectedIndex > 0) Then
                        ldrDatos = mdsDatos.Tables(mstrClieDire).Select("dicl_defa=1")(0)
                        ldrDatos.Item("dicl_baja_fecha") = System.DBNull.Value
                        ldrDatos.Item("_estado") = "Activo"
                        ldrDatos.Item("dicl_dire") = ptxtDire.Valor.ToUpper
                        ldrDatos.Item("dicl_refe") = ptxtRefe.Valor
                        ldrDatos.Item("dicl_cpos") = ptxtCP.Valor
                        ldrDatos.Item("_dire_desc") = ptxtDire.Valor
                        ldrDatos.Item("dicl_loca_id") = pcmbLoca.Valor
                        ldrDatos.Item("_dicl_pcia_id") = pcmbPcia.Valor
                        ldrDatos.Item("_dicl_pais_id") = pcmbPais.Valor
                        'If (mdsDatos.Tables(mstrClieDire).Rows.Count = 1) Then
                        ldrDatos.Item("dicl_defa") = True
                        ldrDatos.Item("_defa") = "X"
                        'Else
                        '    If (Not lbooDefault And chkDireDefa.Checked) Then
                        '        Me.mBlanquearDefaults(mstrClieDire, "dicl")
                        '    End If
                        '    ldrDatos.Item("dicl_defa") = lbooDefault Or (Not lbooDefault And chkDireDefa.Checked)
                        '    ldrDatos.Item("_defa") = IIf(ldrDatos.Item("dicl_defa"), "X", "")
                        'End If

                        If System.Convert.ToInt64(lstrDireId) = -1 Then
                            ldrDatos.Item("dicl_gene") = Not mbooActi
                        End If

                        If Not pbooDefa Then
                            If ldrDatos.Item("dicl_gene") Then
                                If lstrActiNoProp = "" Then
                                    lstrActiNoProp = lstrActiProp
                                Else
                                    lstrActiNoProp += "," & lstrActiProp
                                End If
                            End If
                            ldrDatos.Item("acti_noprop") = lstrActiNoProp
                            If Not ldrDatos.Item("dicl_gene") Then
                                ldrDatos.Item("_prop") = lstrActiPropDesc
                            Else
                                ldrDatos.Item("acti_prop") = ""
                            End If
                        End If
                    Else
                        If (mdsDatos.Tables(mstrClieDire).Rows.Count = 0) Then
                            ldrDatos = Nothing
                        Else
                            ldrDatos = mdsDatos.Tables(mstrClieDire).Select()(0)
                        End If
                    End If
                Else
                    If (mdsDatos.Tables(mstrClieMail).Rows.Count = 0) Then
                        ldrDatos = Nothing
                    Else
                        ldrDatos = mdsDatos.Tables(mstrClieDire).Select()(0)
                    End If
                End If
                clsWeb.ConvertDsToXML(mdsDatos, "antesdire")

                If mbooActi Then
                    ldrDatos.Item("acti_prop") = mintActiDefa
                End If
            Else ' modificacion
                ldrDatos = mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & lstrDireId)(0)
                If (hdnModiDireAdic.Text = "S" And hdnModiMain.Text = "N" And hdnAltaDireAdic.Text <> "S" And lstrDireId <> "") Then
                    'ldrDatos.Item("dicl_id") = lstrDireId 'clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieDire), "dicl_id")
                    ldrDatos.Item("dicl_baja_fecha") = System.DBNull.Value ' revive domi
                    ldrDatos.Item("_estado") = "Activo"                    ' revive domi
                    ldrDatos.Item("dicl_dire") = ptxtDire.Valor.ToUpper
                    ldrDatos.Item("dicl_refe") = ptxtRefe.Valor
                    ldrDatos.Item("dicl_cpos") = ptxtCP.Valor
                    ldrDatos.Item("_dire_desc") = ptxtDire.Valor
                    ldrDatos.Item("dicl_loca_id") = pcmbLoca.Valor
                    ldrDatos.Item("_dicl_pcia_id") = pcmbPcia.Valor
                    ldrDatos.Item("_dicl_pais_id") = pcmbPais.Valor
                    If (mdsDatos.Tables(mstrClieDire).Rows.Count = 1) Then
                        ldrDatos.Item("dicl_defa") = True
                        ldrDatos.Item("_defa") = "X"
                    Else
                        If (Not lbooDefault And chkDireDefa.Checked) Then
                            Me.mBlanquearDefaults(mstrClieDire, "dicl")
                        End If
                        ldrDatos.Item("dicl_defa") = lbooDefault Or (Not lbooDefault And chkDireDefa.Checked)
                        ldrDatos.Item("_defa") = IIf(ldrDatos.Item("dicl_defa"), "X", "")
                    End If

                    If hdnDireId.Text = "" Then
                        ldrDatos.Item("dicl_gene") = Not mbooActi
                    End If

                    If Not pbooDefa Then
                        If ldrDatos.Item("dicl_gene") Then
                            If lstrActiNoProp = "" Then
                                lstrActiNoProp = lstrActiProp
                            Else
                                lstrActiNoProp += "," & lstrActiProp
                            End If
                        End If
                        ldrDatos.Item("acti_noprop") = lstrActiNoProp
                        If Not ldrDatos.Item("dicl_gene") Then
                            ldrDatos.Item("_prop") = lstrActiPropDesc
                        Else
                            ldrDatos.Item("acti_prop") = ""
                        End If
                    End If
                End If
            End If ' fin  modificacion

            If Not pbooDefa Then
                lstrActiProp = ""
                lstrActiNoProp = ""
                For Each oDataItem As DataGridItem In grdActiDire.Items
                    lstrCheck = oDataItem.FindControl("chkActi")
                    lstrProp = oDataItem.FindControl("pcmbActiProp")
                    If lstrCheck.Checked Then
                        If mintActiDefa.ToString = oDataItem.Cells(1).Text Then
                            If lstrActiProp <> "" Then
                                lstrActiProp += ","
                                lstrActiPropDesc += " / "
                            End If
                            lstrActiProp += oDataItem.Cells(1).Text
                            lstrActiPropDesc += oDataItem.Cells(2).Text
                        Else
                            If lstrActiNoProp <> "" Then
                                lstrActiNoProp += ","
                            End If
                            lstrActiNoProp += oDataItem.Cells(1).Text
                        End If
                    End If
                Next

                If lstrDireId = "" And mbooActi And (lstrActiProp & lstrActiNoProp) = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar a que actividad/es pertenece la direcci�n.")
                End If
            End If

            mLimpiarDefas(mstrClieDire, "dicl", ldrDatos)

            If (hdnAltaDireAdic.Text = "S") Then
                mdsDatos.Tables(mstrClieDire).Rows.Add(ldrDatos)
                hdnAltaDireAdic.Text = "N"
            End If

            mdsDatos.AcceptChanges()

            grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
            grdDire.DataBind()

            'refresco los defaults
            If Not pbooDefa Then
                mCargarDatosDire("", True, txtDefaDire, txtDefaDireCP, txtDefaDireRefe, cmbDefaDirePais, cmbDefaDirePcia, cmbDefaDireLoca)
            End If
        End Sub

        Public Overrides Sub mEliminarIntegrante(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                With mdsDatos.Tables(mstrClieInte).Select("clag_id=" & E.Item.Cells(3).Text)(0)
                    .Item("baja_fecha") = System.DateTime.Now.ToString
                    .Item("_estado") = "Inactivo"
                End With
                grdInte.DataSource = mdsDatos.Tables(mstrClieInte)
                grdInte.DataBind()
                mFormatearIntegrantes()
                mLimpiarInte()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCargarActividades(ByVal pgrdActi As System.Web.UI.WebControls.DataGrid, ByVal pstrActiProp As String, ByVal pstrActiNoProp As String)

            Dim lstrCheck As System.Web.UI.WebControls.CheckBox
            Dim lstrProp As NixorControls.ComboBox

            clsWeb.gCargarDataGrid(mstrConn, "actividades_cargar @usua_id=" & Session("sUserId").ToString() & ",@socios='N'", pgrdActi)
            pstrActiProp = "," & pstrActiProp & ","
            pstrActiNoProp = "," & pstrActiNoProp & ","

            For Each oDataItem As DataGridItem In pgrdActi.Items
                lstrCheck = oDataItem.FindControl("chkActi")
                lstrProp = oDataItem.FindControl("cmbActiProp")
                If pstrActiProp.IndexOf("," & oDataItem.Cells(1).Text & ",") <> -1 Or pstrActiNoProp.IndexOf("," & oDataItem.Cells(1).Text & ",") <> -1 Then
                    lstrCheck.Checked = True
                Else
                    lstrCheck.Checked = False
                End If
                If pstrActiProp.IndexOf("," & oDataItem.Cells(1).Text & ",") <> -1 Then
                    lstrProp.Valor = "S"
                    lstrCheck.Enabled = False
                Else
                    If oDataItem.Cells(1).Text = mintActiDefa Then
                        lstrCheck.Checked = True
                        lstrCheck.Enabled = False
                    Else
                        lstrCheck.Enabled = True
                    End If
                End If
            Next
        End Sub

        Private Sub mHabilitarTele(ByVal pbooHabi As Boolean)
            txtTeleNume.Enabled = pbooHabi
            txtTeleArea.Enabled = pbooHabi
            txtTeleRefe.Enabled = pbooHabi
            cmbTeleTipo.Enabled = pbooHabi
            chkTeleDefa.Enabled = pbooHabi
        End Sub
        Private Sub mHabilitarDire(ByVal pbooHabi As Boolean)
            txtDire.Enabled = pbooHabi
            txtDireCP.Enabled = pbooHabi
            txtDireRefe.Enabled = pbooHabi
            cmbDirePais.Enabled = pbooHabi
            cmbDirePcia.Enabled = pbooHabi
            cmbDireLoca.Enabled = pbooHabi
            chkDireDefa.Enabled = pbooHabi

            btnLoca.Disabled = Not pbooHabi
        End Sub
        Private Sub mHabilitarMail(ByVal pbooHabi As Boolean)
            txtMail.Enabled = pbooHabi
            txtMailRefe.Enabled = pbooHabi
            chkMailDefa.Enabled = pbooHabi
        End Sub

        Private Sub mLimpiarTelefono(ByVal pbooDefa As Boolean, ByVal ptxtArea As NixorControls.TextBoxTab, ByVal ptxtNume As NixorControls.TextBoxTab, ByVal ptxtRefe As NixorControls.TextBoxTab, ByVal pcmbTipo As NixorControls.ComboBox)
            hdnTeleId.Text = ""
            ptxtArea.Text = ""
            ptxtNume.Text = ""
            ptxtRefe.Text = ""
            pcmbTipo.Limpiar()

            If Not pbooDefa Then
                mCargarActividades(grdActiTele, "", "")
                grdActiTele.Visible = True

                mSetearEditor(mstrClieTele, True)
                mHabilitarTele(True)
            End If
        End Sub
        Private Sub mLimpiarDireccion(ByVal pbooDefa As Boolean, ByVal ptxtDire As NixorControls.TextBoxTab, ByVal ptxtCP As NixorControls.CodPostalBox, ByVal ptxtRefe As NixorControls.TextBoxTab, ByVal pcmbPais As NixorControls.ComboBox, ByVal pcmbPcia As NixorControls.ComboBox, ByVal pcmbLoca As NixorControls.ComboBox)
            hdnDireId.Text = ""
            ptxtDire.Text = ""
            ptxtCP.Text = ""
            ptxtRefe.Text = ""

            pcmbPais.Limpiar()

            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDirePais, "S")
            ' Dario cambio para que setee por defecto el pais por defecto, cuac
            Dim ds As DataSet = cmbDirePais.DataSource

            For Each itemrow As DataRow In ds.Tables(0).Rows
                If (Not IsDBNull(itemrow(2))) Then
                    If (itemrow(2)) Then
                        cmbDirePais.Valor() = itemrow("id")
                        Exit For
                    End If
                End If
            Next
            ' fin cambio Dario
            clsWeb.gCargarRefeCmb(mstrConn, "provincias", pcmbPcia, "S", "0" + pcmbPais.Valor.ToString)
            pcmbLoca.Valor = String.Empty
            clsWeb.gCargarRefeCmb(mstrConn, "localidades", pcmbLoca, "S", "0" + pcmbLoca.Valor.ToString)

            If Not pbooDefa Then
                mCargarActividades(grdActiDire, "", "")
                grdActiDire.Visible = True

                mSetearEditor(mstrClieDire, True)
                mHabilitarDire(True)
            End If



        End Sub
        Private Sub mLimpiarInte()
            hdnClagId.Text = ""
            usrClieFil.Valor = ""
            chkResp.Checked = False
            mSetearEditor(mstrClieInte, True)
        End Sub
        Private Sub mLimpiarIntegr()
            hdnIntegrId.Text = ""
            usrIntegr.Limpiar()
            chkInteResp.Checked = False
            lblBajaIntegr.Text = ""
            mSetearEditor(mstrClieInte & "_juridicos", True)
        End Sub
        Private Sub mLimpiarMail(ByVal pbooDefa As Boolean, ByVal ptxtMail As NixorControls.TextBoxTab, ByVal ptxtRefe As NixorControls.TextBoxTab)
            hdnMailId.Text = ""
            ptxtMail.Text = ""
            ptxtRefe.Text = ""

            If Not pbooDefa Then
                mCargarActividades(grdActiMail, "", "")
                grdActiMail.Visible = True
                mSetearEditor(mstrClieMail, True)
                mHabilitarMail(True)
            End If
        End Sub
        Private Sub mLimpiarDefas(ByVal pstrTabla As String, ByVal pstrPref As String, ByVal pDr As DataRow)
            If Not IsNothing(pDr) Then
                With pDr
                    If .Item(pstrPref + "_defa") Then
                        For Each dr As DataRow In mdsDatos.Tables(pstrTabla).Select(pstrPref + "_defa=1 AND " + pstrPref + "_id <>" + .Item(pstrPref + "_id").ToString)
                            dr.Item(pstrPref + "_defa") = False
                            dr.Item("_defa") = ""
                        Next
                    End If
                End With
            End If
        End Sub

        Private Sub mReordenarClientes(ByVal pClieAnte As String, ByVal pClieProx As String)
            Dim lintNuevoOrdeIni As Integer
            Dim lintNuevoOrdeFin As Integer
            Dim lintOrdeAux As Integer
            Dim lstrClieAux As String
            Dim ldrDatos As DataRow

            ldrDatos = mdsDatos.Tables(mstrClieInte).Select("clag_id=" & pClieProx)(0)
            lintNuevoOrdeIni = ldrDatos.Item("clag_orden")

            ldrDatos = mdsDatos.Tables(mstrClieInte).Select("clag_id=" & pClieAnte)(0)
            lintNuevoOrdeFin = ldrDatos.Item("clag_orden")

            ldrDatos = mdsDatos.Tables(mstrClieInte).Select("clag_id=" & pClieAnte)(0)
            ldrDatos.Item("clag_orden") = lintNuevoOrdeIni

            For Each odrClag As DataRow In mdsDatos.Tables(mstrClieInte).Select()
                If odrClag.Item("clag_orden") >= lintNuevoOrdeIni And odrClag.Item("clag_orden") <= lintNuevoOrdeFin And odrClag.Item("clag_id") <> pClieAnte Then
                    odrClag.Item("clag_orden") = odrClag.Item("clag_orden") + 1
                End If
            Next

            mdsDatos.Tables(mstrClieInte).DefaultView.Sort = "clag_orden"

            lintOrdeAux = 1
            For Each odrClag As DataRow In mdsDatos.Tables(mstrClieInte).Select()
                odrClag.Item("clag_orden") = lintOrdeAux
                lintOrdeAux = lintOrdeAux + 1
            Next

            mdsDatos.Tables(mstrClieInte).DefaultView.Sort = "clag_orden"
            grdInte.DataSource = mdsDatos.Tables(mstrClieInte)
            grdInte.DataBind()
            mFormatearIntegrantes()
        End Sub
        '   Dario 2013-05-09 permite blanquer los default en el caso del alta nuevo si hay otro defualt
        Private Function mBlanquearDefaults(ByVal pstrTabla As String, ByVal pstrPrefijo As String)
            For Each dr As DataRow In mdsDatos.Tables(pstrTabla).Select(pstrPrefijo + "_defa=1")
                With dr
                    .Item(pstrPrefijo + "_defa") = False
                    .Item("_defa") = ""
                End With
            Next
        End Function

        Private Function mActualizarDefaults(ByVal pbooDefa As Boolean, ByVal pstrTabla As String, ByVal pstrPrefijo As String) As Boolean
            Dim lbooDefault As Boolean
            If Not mbooActi Then
                If pbooDefa Then
                    For Each dr As DataRow In mdsDatos.Tables(pstrTabla).Select(pstrPrefijo + "_defa=1")
                        With dr
                            .Item(pstrPrefijo + "_defa") = False
                            .Item("_defa") = ""
                        End With
                    Next
                End If
                lbooDefault = (mdsDatos.Tables(pstrTabla).Select(pstrPrefijo + "_defa=1").GetLength(0) = 0)
            Else
                lbooDefault = pbooDefa
            End If

            Return lbooDefault
        End Function
#End Region



        Private Sub cmbTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipo.SelectedIndexChanged



        End Sub
    End Class
End Namespace
