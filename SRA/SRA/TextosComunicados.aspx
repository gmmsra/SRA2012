<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TextosComunicados" CodeFile="TextosComunicados.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Textos de Comunicados</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Textos de Comunicados</asp:label></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<FooterStyle CssClass="footer"></FooterStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="teco_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="teco_desc" HeaderText="Descripción"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="middle" width="100" colSpan="3">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ForeColor="Transparent"
											ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Registro" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD colSpan="3"></TD>
								</tr>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Height="124%" Visible="False">
											<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Height="21px" CausesValidation="False" Width="80px"> Texto </asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
															Height="21px" CausesValidation="False" Width="70px"> Categorías </asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Height="116px" Visible="False">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD vAlign="middle" align="right">
														<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD>
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD align="right" width="25%">
																		<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;</TD>
																	<TD width="75%">
																		<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="360px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD>
														<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD align="center" colSpan="2">
																		<asp:datagrid id="grdDeta" runat="server" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1"
																			GridLines="None" CellPadding="1" OnPageIndexChanged="DataGridDeta_Page" AutoGenerateColumns="False"
																			width="100%" BorderStyle="None" BorderWidth="1px" OnEditCommand="mEditarDeta">
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="tcca_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_cate_desc" HeaderText="Categoria"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_inst_desc" HeaderText="Institución"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right" width="25%">
																		<asp:Label id="lblCate" runat="server" cssclass="titulo">Categoría:</asp:Label>&nbsp;</TD>
																	<TD width="75%">
																		<cc1:combobox class="combo" id="cmbCate" runat="server" Width="248px" AceptaNull="True"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblInst" runat="server" cssclass="titulo" Width="24px">Institución:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 19px">
																		<cc1:combobox class="combo" id="cmbInst" runat="server" Width="248px" AceptaNull="True"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblCateMens" runat="server" cssclass="titulo" Width="24px">Mensaje:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 19px">
																		<CC1:TEXTBOXTAB id="txtMsg1" runat="server" Width="360px" Obligatorio="false" CssClass="cuadrotexto"></CC1:TEXTBOXTAB>
																		<CC1:TEXTBOXTAB id="txtMsg2" runat="server" Width="360px" Obligatorio="false" CssClass="cuadrotexto"></CC1:TEXTBOXTAB>
																		<CC1:TEXTBOXTAB id="txtMsg3" runat="server" Width="360px" Obligatorio="false" CssClass="cuadrotexto"></CC1:TEXTBOXTAB>
																		<CC1:TEXTBOXTAB id="txtMsg4" runat="server" Width="360px" Obligatorio="false" CssClass="cuadrotexto"></CC1:TEXTBOXTAB>
																		<CC1:TEXTBOXTAB id="txtMsg5" runat="server" Width="360px" Obligatorio="false" CssClass="cuadrotexto"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" height="4"></TD>
																	<TD height="4"></TD>
																</TR>
																<TR>
																	<TD vAlign="middle" align="center" colSpan="2" height="30">
																		<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
																		<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Visible="true" Width="80px" Text="Baja"></asp:Button>&nbsp;
																		<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Visible="true" Width="80px" Text="Modificar"></asp:Button>&nbsp;
																		<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" Text="Limpiar "></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD height="1"></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
