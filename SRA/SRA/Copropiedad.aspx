
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Copropiedad" CodeFile="Copropiedad.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Copropiedad</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<SCRIPT language="javascript">
		function mSetearTipoPropietario()
		{
			if (document.all('panPropCria')!=null) 
			{
				if (document.all('optCria').checked)
				{
					imgLimpClieDeriv_click('usrPropClie');
					document.all('panPropCria').style.display='';
					document.all('panPropClie').style.display='none';					
				}
				else
				{
					imgLimpClieDeriv_click('usrPropCria');
					document.all('panPropCria').style.display='none';
					document.all('panPropClie').style.display='';					
				}
			}
		}
		
		function usrPropClie_onchange()
		{
			if (document.all('usrPropClie:txtId').value != '')
				__doPostBack('btnDeta','');
	    }
	    
		function usrPropCria_onchange()
		{
			if (document.all('usrPropCria:txtId').value != '')
				__doPostBack('btnDeta','');
	    }

	    function usrProducto_onchange()
		{
			//__doPostBack('usrProducto','');
			//alert(document.all('usrProducto:txtId').value);
		}
	    function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Copropiedad</asp:label></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
										Width="100%" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" align="right">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																	CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="10"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="10"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD height="6" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 17px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblSraNumeDesde" runat="server" cssclass="titulo">SRA Num. Desde:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2">
																			<cc1:numberbox id="txtSraNumeDesde" runat="server" cssclass="cuadrotexto" Visible="true" Width="100px"
																				esdecimal="False" MaxValor="9999999999999" maxLength="12"></cc1:numberbox>&nbsp;&nbsp;
																			<asp:Label id="lblSraNumeHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtSraNumeHasta" runat="server" cssclass="cuadrotexto" Visible="true" Width="100px"
																				esdecimal="False" MaxValor="9999999999999" maxLength="12"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 160px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblPropFil" runat="server" cssclass="titulo">Propietario:&nbsp;</asp:label></TD>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2">
																			<UC1:CLIE id="usrPropFil" runat="server" Tabla="Criadores" Saltos="1,2" FilSociNume="True"
																				FilTipo="S" MuestraDesc="False" FilDocuNume="True" Ancho="800" AutoPostBack="True" AceptaNull="false"
																				CampoVal="Criador"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 160px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblProductoFil" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2">
																			<UC1:PROD id="usrProductoFil" runat="server" Tabla="productos" Saltos="1,2" FilSociNume="True"
																				FilTipo="S" MuestraDesc="True" FilDocuNume="True" Ancho="800" AutoPostBack="False" AceptaNull="false"></UC1:PROD></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblTipoReporte" runat="server" cssclass="titulo">Tipo Reporte:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg" colSpan="2">
																			<cc1:combobox id="cmbTipoReporte" class="combo" runat="server" Width="200px" AceptaNull="False">
																				<asp:ListItem Value="">(Seleccione)</asp:ListItem>
																				<asp:ListItem Value="C">Completo</asp:ListItem>
																				<asp:ListItem Value="R">Resumido</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD height="6" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivfin.jpg" colSpan="3"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="3" height="16"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="prdt_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="_prdt_nomb" HeaderText="Producto"></asp:BoundColumn>
											<asp:BoundColumn DataField="_clie_nomb" HeaderText="Propietario"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="1">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="bottom">
									<TABLE style="WIDTH: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left" width="99%"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
													BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
													ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Relaci�n" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="center" width="100"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
													BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
													ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
										</TR>
										<TR>
											<TD align="left" width="99%" colSpan="3"><asp:panel id="panLinks" runat="server" cssclass="titulo" Visible="False">
													<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
														<TR>
															<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
															<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
															<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
															<TD background="imagenes/tab_fondo.bmp" width="1">
																<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
																	cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Propiedad </asp:linkbutton></TD>
															<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
															<TD background="imagenes/tab_fondo.bmp" width="1">
																<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDeta" runat="server" cssclass="solapa"
																	Width="70px" Height="21px" CausesValidation="False"> Porcentajes </asp:linkbutton></TD>
															<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
											Visible="False" width="100%" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD style="WIDTH: 555px" height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 18px"></TD>
														<TD style="HEIGHT: 18px" height="18" vAlign="top" colSpan="2" align="center">
															<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD height="10" colSpan="2" noWrap align="left">
																		<TABLE id="Table7" border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD colSpan="2" align="left">
																					<asp:panel id="panCabecera" runat="server" cssclass="titulo" Visible="False" Width="100%">
																						<TABLE id="Table9" border="0" cellSpacing="0" cellPadding="0" width="100%">
																							<TR>
																								<TD align="right">
																									<asp:Label id="lblProd" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																								<TD>
																									<UC1:PROD id="usrProducto" runat="server" Tabla="productos" Saltos="1,2" FilSociNume="True"
																										FilTipo="S" MuestraDesc="True" FilDocuNume="True" Ancho="800" AutoPostBack="False" AceptaNull="false"></UC1:PROD></TD>
																							</TR>
																							<TR>
																								<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																							</TR>
																							<TR>
																								<TD style="HEIGHT: 19px" noWrap align="right">
																									<asp:Label id="lblFechaNaci" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																								<TD style="HEIGHT: 19px">
																									<cc1:DateBox id="txtFechaNaci" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="False"
																										Obligatorio="True"></cc1:DateBox>&nbsp;
																									<asp:Label id="lblFechaVenta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;
																									<cc1:DateBox id="txtFechaVenta" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="False"
																										Obligatorio="False"></cc1:DateBox></TD>
																							</TR>
																							<TR>
																								<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																							</TR>
																							<TR>
																								<TD align="right">
																									<asp:label id="lblCopro" runat="server" cssclass="titulo">Propietario:</asp:label>&nbsp;</TD>
																								<TD>
																									<asp:RadioButton id="optClie" onclick="javascript:mSetearTipoPropietario();" runat="server" CssClass="titulo"
																										GroupName="grpTipo" Checked="True" Text="Cliente"></asp:RadioButton>
																									<asp:RadioButton id="optCria" onclick="javascript:mSetearTipoPropietario();" runat="server" CssClass="titulo"
																										GroupName="grpTipo" Text="Criador"></asp:RadioButton>
																									<DIV id="panPropClie">
																										<UC1:CLIE id="usrPropClie" runat="server" Tabla="Clientes" Saltos="1,2" FilSociNume="True"
																											FilTipo="S" MuestraDesc="False" FilDocuNume="True" Ancho="800" AutoPostBack="False" AceptaNull="false"></UC1:CLIE></DIV>
																									<DIV id="panPropCria">
																										<UC1:CLIE id="usrPropCria" runat="server" Tabla="Criadores" Saltos="1,2" FilSociNume="True"
																											FilTipo="S" MuestraDesc="False" FilDocuNume="True" Ancho="800" AutoPostBack="False" AceptaNull="false"></UC1:CLIE></DIV>
																								</TD>
																							</TR>
																						</TABLE>
																					</asp:panel></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 18px"></TD>
														<TD style="HEIGHT: 18px" height="18" vAlign="top" colSpan="2" align="center">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE style="WIDTH: 100%" id="Table8" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="3">
																			<asp:datagrid id="grdCopro" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosCopro" OnPageIndexChanged="grdCopro_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="false" DataField="prdp_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prdp_clie_id" HeaderText="Cliente"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="prdp_cria_id" HeaderText="Criador"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="prdp_desde" HeaderText="Desde" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn Visible="false" DataField="prdp_hasta" HeaderText="Hasta" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_clie_nomb" HeaderText="Nombre y Apellido/R.Social"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prdp_parti" HeaderText="Porcentaje"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD height="8" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%" height="1" align="right">
																			<asp:Label id="lblNombClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%">
																			<asp:Label id="txtNumeClie" runat="server" cssclass="titulo"></asp:Label>
																			<asp:Label id="txtNombClie" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%" height="1" align="right">
																			<asp:Label id="lblPorcComp" runat="server" cssclass="titulo">Porcentaje:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%">
																			<cc1:numberbox id="txtPorc" runat="server" cssclass="cuadrotexto" Width="60px" esdecimal="False"
																				MaxValor="100" MaxLength="3"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="3" align="center">
																			<asp:Button id="btnModiCopro" runat="server" cssclass="boton" Width="100px" Text="Modificar"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpComp" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Limpiar"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle" align="center" colSpan="2" height="34"><asp:panel id="panBotones" runat="server" cssclass="titulo" Width="100%" Visible="False"><asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp; 
<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" Visible="False" CausesValidation="False"
											Text="Baja"></asp:button>&nbsp; 
<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
											Text="Modificar"></asp:button>&nbsp; 
<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
											Text="Limpiar"></asp:button>
<asp:button id="btnDeta" runat="server" cssclass="boton" Width="80px" Visible="False" CausesValidation="False"
											Text="Detalle"></asp:button></TD>
								</asp:panel></TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnPropOrig" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		//if (document.all["txtDesc"]!= null)
			//document.all["txtDesc"].focus();
	    mSetearTipoPropietario();
		</SCRIPT>
	</BODY>
</HTML>
