'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_Criadores_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
' a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'Criadores.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Namespace SRA

'Partial Class Criadores
Partial Class Migrated_Criadores

Inherits Criadores

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mdsDatos As DataSet
    Private mstrds As String
    Private mdsDatosAux As DataSet
    Private mstrConn As String
'    Public mstrCriaCtrl As String
    Private mstrCriaId As String
    Private mstrValorId As String
'    Public mbooActi As Boolean
'    Public mintActiDefa As Integer
    Private mbooSoloBusq As Boolean
    Private mbooNewClieDeriv As Boolean
    'campos a filtrar
    Private mstrFilNomb As String
    'Private mstrFilFanta As String
    Private mstrFilSociNume As String
    Private mstrFilCriaNume As String
    Private mstrFilRepreCriaNume As String
    Private mstrFilNoCriaNume As String
    Private mstrFilCuit As String
    Private mstrFilDocu As String
    Private mstrFilTarj As String
    Private mstrFilEnti As String
    Private mstrFilMedioPago As String
    Private mstrFilAgru As String
    Private mstrFilEmpr As String
    Private mstrFilClaveUnica As String
    Private mstrFilRespa As String
    Private mstrFilProd As String
    Private mstrFilLegaNume As String
    Private mstrFilClieTipo As String
    Private mstrFilHabiRaza As String
    'valores de los filtros 
    Private mstrClieNume As String
    Private mstrNume As String
    Private mstrNomb As String
    'Private mstrFanta As String
    Private mstrSociNume As String
    Private mstrRaza As String
    Private mstrCriaNume As String
    Private mstrNoCriaNume As String
    Private mstrCuit As String
    Private mstrDocuTipo As String
    Private mstrDocuNume As String
    Private mstrTarjTipo As String
    Private mstrTarjNume As String
    Private mstrEnti As String
    Private mstrMedioPago As String
    Private mstrEmpr As String
    Private mstrClaveUnica As String
    Private mstrLegaNume As String
    'columnas a mostrar 
    Private mstrColNomb As String
    'Private mstrColFanta As String
    Private mstrColSociNume As String
    Private mstrColRaza As String
    Private mstrColCriaNume As String
    Private mstrColNoCriaNume As String
    Private mstrColCuit As String
    Private mstrColDocuNume As String
    Private mstrColEnti As String
    Private mstrColMedioPago As String
    Private mstrColEmpr As String
    Private mstrColClaveUnica As String
    Private mstrColLegaNume As String
    'columnas de la grilla de resultados
    Private mintGrdCriaEdit As Integer = 0
    Private mintGrdClieSele As Integer = 1
    Private mintGrdCriaId As Integer = 2
    Private mintGrdCriaRaza As Integer = 3
    Private mintGrdCriaNume As Integer = 4
    Private mintGrdClieNume As Integer = 5
    Private mintGrdApel As Integer = 6
    Private mintGrdNomb As Integer = 7
    Private mintGrdSociNume As Integer = 8
    Private mintGrdCuit As Integer = 9
    Private mintGrdDocuNume As Integer = 10
    Private mintGrdClaveUnica As Integer = 11
    Private mintGrdEsta As Integer = 12
    Private mintGrdPrefijo As Integer = 15

    Private Enum Columnas As Integer
        ClieSele = 0
        ClieEdit = 1
        CriaId = 2
        CriaRazaDesc = 3
        CriaNume = 4
        ClieId = 5
        ClieApel = 6
        CriaRaza = 13
        CriaRazaCodi = 14
        CriaPrefijo = 15
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lstrFil As String
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializarControl()
            mInicializar()
            If (Not Page.IsPostBack) Then
                If mstrCriaCtrl = "" Then
                    'Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                End If

                clsWeb.gInicializarControles(Me, mstrConn)

                Session(mstrTabla) = Nothing

                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()

                If mstrCriaId <> "" Then
                    mCargarDatos(mstrCriaId)
                Else
                    If mstrCriaCtrl <> "" Then
                        mMostrarPanel(False)
                        lstrFil = usrClieFil.Valor & txtNombFil.Text & txtSociNumeFil.Text & txtCriaNumeFil.Text & txtCuitFil.TextoPlano & txtDocuNumeFil.Text & txtCunicaFil.Text
                        If lstrFil <> "" Then
                            mConsultar()
                        End If
                    Else
                        mMostrarPanel(False)
                    End If
                End If
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                    Dim x As String = Session.SessionID
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPaisFil, "S")
            'cmbPaisFil.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id")
            clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & IIf(cmbPaisFil.Valor = "", DBNull.Value, cmbPaisFil.Valor), cmbProvFil, "id", "descrip", "S", True)
            clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id = 50, @defa = 1")
        clsWeb.gCargarRefeCmb(mstrConn, "RG_CONVENIOS_RRGG", cmbConvenioRRGG, "", "")

        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "T")
        'clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRepreRazaFil, "id", "descrip", "T")
        SRA_Neg.Utiles.gSetearRaza(cmbRaza)
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
        'SRA_Neg.Utiles.gSetearRaza(cmbRepreRazaFil)

        clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "T")

        'inicializar valores
        If mValorParametro(Request.QueryString("DocuTipo")) <> "" Then
            cmbDocuTipoFil.Valor = mValorParametro(Request.QueryString("DocuTipo"))
        End If
        If mValorParametro(Request.QueryString("Raza")) <> "" Then
            cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
        End If
    End Sub

    Private Sub mSetearEventos()
        btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
        btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')){return false;} else {mPorcPeti();}")
        btnTramites.Attributes.Add("onclick", "btnTramites_click(); return false;")
        btnSemen.Attributes.Add("onclick", "mVerStockSemen(); return false;")
        btnEmbriones.Attributes.Add("onclick", "mVerStockEmbriones(); return false;")
        btnServi.Attributes.Add("onclick", "btnServi_click(); return false;")
        btnNaci.Attributes.Add("onclick", "btnNaci_click(); return false;")
    End Sub

    Private Sub mSetearMaxLength()

        Dim lstrCriaLong As Object
        Dim lstrClieLong As Object
        Dim lstrEstabLong As Object
        Dim lintCol As Integer

        lstrCriaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrCriaLong, "cria_nume")
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrCriaLong, "cria_obse")

        lstrClieLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClientes)
        txtDocuNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_docu_nume")
        txtApelFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_apel")
        txtNombFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_nomb")
        txtRespaFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_respa")

        txtRespa.MaxLength = txtRespaFil.MaxLength
        txtCriaNumeFil.MaxLength = txtNume.MaxLength
        'txtRepreCriaNumeFil.MaxLength = txtNume.MaxLength
        txtSociNumeFil.MaxLength = txtNume.MaxLength

        lstrEstabLong = clsSQLServer.gCargarLongitudes(mstrConn, "establecimientos")
        txtDescFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrEstabLong, "estb_desc")

    End Sub

#End Region

#Region "Inicializacion de Variables"

    Public Overrides Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        mintActiDefa = clsSQLServer.gConsultarValor("exec usuarios_actividades_default " & Session("sUserId").ToString, mstrConn, 0)

        usrClieFil.Tabla = mstrClientes
        usrClieFil.AutoPostback = False
        usrClieFil.FilClaveUnica = True
        usrClieFil.ColClaveUnica = True
        usrClieFil.Ancho = 790
        usrClieFil.Alto = 510
        usrClieFil.ColDocuNume = True
        usrClieFil.ColCUIT = True
        usrClieFil.ColClaveUnica = False
        usrClieFil.FilClaveUnica = True
        usrClieFil.FilCUIT = True
        usrClieFil.FilDocuNume = True
        usrClieFil.FilTarjNume = True
        usrClieFil.FilTipo = "T"

        usrClie.Tabla = mstrClientes
        usrClie.AutoPostback = False
        usrClie.FilClaveUnica = True
        usrClie.ColClaveUnica = True
        usrClie.Ancho = 790
        usrClie.Alto = 510
        usrClie.ColDocuNume = True
        usrClie.ColCUIT = True
        usrClie.ColClaveUnica = False
        usrClie.FilClaveUnica = True
        usrClie.FilCUIT = True
        usrClie.FilDocuNume = True
        usrClie.FilTarjNume = True
        usrClie.FilTipo = "T"

        btnNroCriador.Disabled = True
        btnNroNoCriador.Disabled = True

    End Sub

    Public Overrides Sub mInicializarControl()

        Try
            mstrCriaCtrl = mValorParametro(Request.QueryString("ctrlId"))
            mbooSoloBusq = mValorParametro(Request.QueryString("SoloBusq")) = "1"
            mstrValorId = mValorParametro(Request.QueryString("ValorId"))
            mstrCriaId = mValorParametro(Request.QueryString("ClieId"))

            mbooNewClieDeriv = mValorParametro(Request.QueryString("pstNewClieDeriv")) = "1"

            If hdnValorId.Text = "" Then
                hdnValorId.Text = mstrValorId 'pone el id seleccionado en el control de b�squeda
            ElseIf hdnValorId.Text = "-1" Then  'si ya se limpiaron los filtros, ignora el id con el que vino
                mstrValorId = ""
            End If

            mbooActi = mValorParametro(Request.QueryString("a") = "s")

            'campos a filtrar
            If mstrCriaCtrl <> "" Then
                mstrFilNomb = mValorParametro(Request.QueryString("FilNomb"))
                mstrFilSociNume = mValorParametro(Request.QueryString("FilSociNume"))
                mstrFilCriaNume = mValorParametro(Request.QueryString("FilCriaNume"))
                mstrFilRepreCriaNume = mValorParametro(Request.QueryString("FilRepreCriaNume"))
                mstrFilCuit = mValorParametro(Request.QueryString("FilCuit"))
                mstrFilDocu = mValorParametro(Request.QueryString("FilDocuNume"))
                mstrFilClaveUnica = mValorParametro(Request.QueryString("FilClaveUnica"))
                mstrFilRespa = mValorParametro(Request.QueryString("FilRespa"))
                mstrFilProd = mValorParametro(Request.QueryString("FilProd"))
                mstrFilHabiRaza = mValorParametro(Request.QueryString("FilHabiRaza"))
            Else
                mstrFilNomb = 1
                mstrFilSociNume = 1
                mstrFilCriaNume = 1
                mstrFilRepreCriaNume = 1
                mstrFilCuit = 1
                mstrFilDocu = 1
                mstrFilClaveUnica = 1
                mstrFilRespa = 1
                mstrFilProd = 1
                mstrFilHabiRaza = 1
            End If

            'valores de los filtros
            If Not Page.IsPostBack Then
                txtApelFil.Text = mValorParametro(Request.QueryString("Apel"))
                txtNombFil.Text = mValorParametro(Request.QueryString("Nomb"))
                txtSociNumeFil.Text = mValorParametro(Request.QueryString("SociNume"))
                txtCriaNumeFil.Text = mValorParametro(Request.QueryString("CriaNume"))
                txtCuitFil.Text = mValorParametro(Request.QueryString("Cuit"))
                txtCunicaFil.Text = mValorParametro(Request.QueryString("ClaveUnica"))
                txtDocuNumeFil.Text = mValorParametro(Request.QueryString("DocuNume"))
            End If

            'columnas a mostrar
            mstrColNomb = mValorParametro(Request.QueryString("ColNomb"))
            mstrColSociNume = mValorParametro(Request.QueryString("ColSociNume"))
            mstrColRaza = "1"  ' mValorParametro(Request.QueryString("ColCriaNume"))
            mstrColCriaNume = mValorParametro(Request.QueryString("ColCriaNume"))
            mstrColCuit = mValorParametro(Request.QueryString("ColCuit"))
            mstrColDocuNume = mValorParametro(Request.QueryString("ColDocuNume"))
            mstrColClaveUnica = mValorParametro(Request.QueryString("ColClaveUnica"))

            If mbooSoloBusq Then
                grdDato.Columns(1).Visible = False
                btnAgre.Enabled = False
            End If

            'seteo de controles segun filtros
            grdDato.Columns(0).Visible = (mstrCriaCtrl <> "")

            If mstrCriaCtrl <> "" Then
                grdDato.Columns(mintGrdNomb).Visible = False '(mstrColNomb = 1)
                grdDato.Columns(mintGrdSociNume).Visible = (mstrColSociNume = 1)
                grdDato.Columns(mintGrdCriaRaza).Visible = (mstrColRaza = 1)
                grdDato.Columns(mintGrdCriaNume).Visible = (mstrColCriaNume = 1)
                grdDato.Columns(mintGrdCuit).Visible = (mstrColCuit = 1)
                grdDato.Columns(mintGrdDocuNume).Visible = (mstrColDocuNume = 1)
                grdDato.Columns(mintGrdClaveUnica).Visible = (mstrColClaveUnica = 1)
                'grdDato.Columns(mintGrdEsta).Visible = (mstrCriaCtrl = "")
                grdDato.Columns(mintGrdPrefijo).Visible = True
            Else
                grdDato.Columns(mintGrdNomb).Visible = False
            End If

            If mstrCriaCtrl <> "" Then
                lblSociNumeFil.Visible = True '(mstrFilSociNume = "1")
                lblCuitFil.Visible = (mstrFilCuit = "1")
                lblDocuFil.Visible = (mstrFilDocu = "1")
                lblCunicaFil.Visible = (mstrFilClaveUnica = "1")
                lblRespaFil.Visible = (mstrFilRespa = "1")
                lblProdFil.Visible = (mstrFilProd = "1")
                txtSociNumeFil.Visible = True '(mstrFilSociNume = "1")
                cmbRazaFil.Visible = (mstrFilCriaNume = "1")
                txtCriaNumeFil.Visible = (mstrFilCriaNume = "1")
                'cmbRepreRazaFil.Visible = (mstrFilRepreCriaNume = "1")
                'txtRepreCriaNumeFil.Visible = (mstrFilRepreCriaNume = "1")
                txtCuitFil.Visible = (mstrFilCuit = "1")
                cmbDocuTipoFil.Visible = (mstrFilDocu = "1")
                txtDocuNumeFil.Visible = (mstrFilDocu = "1")
                txtCunicaFil.Visible = (mstrFilClaveUnica = "1")
                txtRespaFil.Visible = (mstrFilRespa = "1")
                'cmbProdFil.Visible = (mstrFilProd = "1")
                cmbRazaFil.Enabled = (mstrFilHabiRaza = "1")
            Else
                mstrFilCriaNume = "1"
                mstrFilRepreCriaNume = "1"
                cmbRazaFil.Visible = True
                cmbRazaFil.Enabled = True
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mValorParametro(ByVal pstrPara As String) As String
        Dim lstrPara As String
        If pstrPara Is Nothing Then
            lstrPara = ""
        Else
            If pstrPara Is System.DBNull.Value Then
                lstrPara = ""
            Else
                lstrPara = pstrPara
            End If
        End If
        Return (lstrPara)
    End Function

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mValidarConsulta()
        Try
            If (cmbDocuTipoFil.Valor = 0 And txtDocuNumeFil.Text <> "") _
            Or (cmbDocuTipoFil.Valor <> 0 And txtDocuNumeFil.Text = "") Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el tipo y n�mero de documento.")
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mConsultar()
        Try
            Dim lstrCmd As String

                lstrCmd = ObtenerSql(Session("sUserId").ToString(), IIf(chkBusc.Checked, 1, 0), usrClieFil.Valor,
                                 IIf(chkBaja.Checked, 1, 0), mstrCriaCtrl, txtApelFil.Text, txtNombFil.Text,
                                 txtSociNumeFil.Text, mstrFilCriaNume, cmbRazaFil.SelectedValue, txtCriaNumeFil.Text,
                                 mstrFilRepreCriaNume, 0, 0,
                                 mstrFilCuit, txtCuitFil.TextoPlano.ToString, mstrFilDocu, cmbDocuTipoFil.SelectedValue,
                                 txtDocuNumeFil.Text, mstrFilClaveUnica, txtCunicaFil.Text, mstrFilRespa,
                                 txtRespaFil.Text, IIf(cmbPaisFil.Valor.ToString = "", "null", cmbPaisFil.Valor.ToString), cmbProvFil.Valor.ToString,
                                 IIf(chkBuscFil.Checked, 1, 0), txtDescFil.Text)  'cmbRepreRazaFil.SelectedValue, txtRepreCriaNumeFil.Text, _

                clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

            grdDato.Visible = True

            If Not Page.IsPostBack And grdDato.Items.Count = 1 And mstrCriaCtrl <> "" Then
                If mstrValorId = "" Then
                    mCriador(grdDato.Items(0).Cells(Columnas.CriaId).Text, grdDato.Items(0).Cells(Columnas.CriaNume).Text, grdDato.Items(0).Cells(Columnas.ClieApel).Text, grdDato.Items(0).Cells(Columnas.CriaRaza).Text, grdDato.Items(0).Cells(Columnas.CriaRazaCodi).Text)
                Else
                    mCargarDatos(mstrValorId)
                    Return
                End If
            End If
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

'    Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pstrClieId As String, _
'                                      ByVal pintBaja As Integer, ByVal pstrCriaCtrl As String, ByVal pstrApel As String, _
'                                      ByVal pstrNomb As String, _
'                                      ByVal pstrSociNume As String, ByVal pstrFilCriaNume As String, ByVal pstrRazaFil As String, _
'                                      ByVal pstrCriaNumeFil As String, ByVal pstrFilRepreCriaNume As String, ByVal pstrRepreRazaFil As String, _
'                                      ByVal pstrRepreCriaNumeFil As String, ByVal pstrFilCuit As String, ByVal pstrCuitFil As String, _
'                                      ByVal pstrFilDocu As String, ByVal pstrDocuTipoFil As String, ByVal pstrDocuNumeFil As String, _
'                                      ByVal pstrFilClaveUnica As String, ByVal pstrCunicaFil As String, _
'                                      ByVal pstrFilRespa As String, ByVal pstrRespa As String, _
'                                      ByVal pstrFilPais As String, ByVal pstrFilprov As String, _
'                                      ByVal pintBuscFil As Integer, ByVal pstrEstab As String) As String
'
'
'        Dim lstrCmd As New StringBuilder
'
'        'esta funci�n se llama tambi�n desde clsXMLHTTP
'        lstrCmd.Append("exec criadores_busq")
'        lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
'        lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
'        lstrCmd.Append(" , @clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
'        lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)
'
'        lstrCmd.Append(" , @clie_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
'        lstrCmd.Append(" , @clie_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
'        lstrCmd.Append(" , @clie_soci_nume=" + clsSQLServer.gFormatArg(pstrSociNume, SqlDbType.Int))
'
'        If pstrFilCriaNume = "1" Then
'            lstrCmd.Append(" , @clie_raza=" + clsSQLServer.gFormatArg(pstrRazaFil, SqlDbType.Int))
'            lstrCmd.Append(" , @clie_cria_nume=" + clsSQLServer.gFormatArg(pstrCriaNumeFil, SqlDbType.Int))
'        End If
'        If pstrFilRepreCriaNume = "1" Then
'            lstrCmd.Append(" , @repre_clie_raza=" + clsSQLServer.gFormatArg(pstrRepreRazaFil, SqlDbType.Int))
'            lstrCmd.Append(" , @repre_clie_cria_nume=" + clsSQLServer.gFormatArg(pstrRepreCriaNumeFil, SqlDbType.Int))
'        End If
'        If pstrFilCuit = "1" And pstrCuitFil <> "" Then
'            lstrCmd.Append(" , @clie_cuit=" + clsSQLServer.gFormatArg(pstrCuitFil, SqlDbType.Int))
'        End If
'        If pstrFilRespa = "1" And pstrRespa <> "" Then
'            lstrCmd.Append(" , @clie_respa=" + clsSQLServer.gFormatArg(pstrRespa, SqlDbType.VarChar))
'        End If
'        If pstrFilDocu = "1" Then
'            lstrCmd.Append(" , @clie_docu_tipo=" + clsSQLServer.gFormatArg(pstrDocuTipoFil, SqlDbType.Int))
'            lstrCmd.Append(" , @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNumeFil, SqlDbType.Int))
'        End If
'        If pstrFilClaveUnica = "1" Then
'            lstrCmd.Append(" , @clie_cunica=" + clsSQLServer.gFormatArg(pstrCunicaFil, SqlDbType.VarChar))
'        End If
'
'        lstrCmd.Append(" , @pais_id=" + clsSQLServer.gFormatArg(pstrFilPais, SqlDbType.Int))
'        lstrCmd.Append(" , @prov_id=" + clsSQLServer.gFormatArg(pstrFilprov, SqlDbType.Int))
'        lstrCmd.Append(" , @buscar_en_estab=" + pintBuscFil.ToString)
'        lstrCmd.Append(" , @estab=" + clsSQLServer.gFormatArg(pstrEstab, SqlDbType.VarChar))
'
'        Return (lstrCmd.ToString)
'    End Function
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnAlta.Enabled = pbooAlta
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        'txtNume.Enabled = Not (pbooAlta)
        txtAltaFecha.ReadOnly = Not pbooAlta
        txtAltaFecha.CssClass = IIf(pbooAlta, "cuadrotexto", "textolibredeshab")
        txtAltaFecha.MostrarBoton = IIf(pbooAlta, "True", "False")
    End Sub
    Public Overrides Sub mSeleccionarCriador(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lstrCriaId As String, lstrCriaNume As String, lstrCriaNomb As String, lstrCriaRaza As String, lstrRazaCodi As String
        lstrCriaId = E.Item.Cells(Columnas.CriaId).Text
        lstrCriaNume = Replace(E.Item.Cells(Columnas.CriaNume).Text, "&nbsp;", "")
        lstrCriaNomb = Replace(E.Item.Cells(Columnas.ClieApel).Text, "&nbsp;", "")
        lstrCriaRaza = Replace(E.Item.Cells(Columnas.CriaRaza).Text, "&nbsp;", "")
        lstrRazaCodi = Replace(E.Item.Cells(Columnas.CriaRazaCodi).Text, "&nbsp;", "")
        mCriador(lstrCriaId, lstrCriaNume, lstrCriaNomb, lstrCriaRaza, lstrRazaCodi)
    End Sub
    Private Sub mCriador(ByVal mstrCriaId As String, ByVal mstrCriaNume As String, ByVal mstrCriaNomb As String, ByVal mstrCriaRaza As String, ByVal mstrRazaCodi As String)

        Dim lsbMsg As New StringBuilder
        If (mbooNewClieDeriv = False) Then
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", Request.QueryString("ctrlId"), mstrCriaNume))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtId'].value='{1}';", Request.QueryString("ctrlId"), mstrCriaId))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtApel'].value=""{1}"";", Request.QueryString("ctrlId"), mstrCriaNomb))
            lsbMsg.Append(String.Format("var pCombo = window.opener.document.all['{0}:cmbRazaCria']; ", Request.QueryString("ctrlId")))
            lsbMsg.Append("for (var lintIndi = 0; lintIndi < pCombo.length; lintIndi++) { ")
            lsbMsg.Append("  if (pCombo.options[lintIndi].value == '" & mstrCriaRaza.Trim & "') {")
            lsbMsg.Append("    pCombo.selectedIndex = lintIndi; } } ")
            lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbRazaCria'].value='{1}';", Request.QueryString("ctrlId"), mstrRazaCodi))
            'lsbMsg.Append("try { window.opener." & Request.QueryString("ctrlId") & "Id_onchange(); } ;")
            'lsbMsg.Append("catch(e){};")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
        Else
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", Request.QueryString("ctrlId"), mstrCriaNume))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtId'].value='{1}';", Request.QueryString("ctrlId"), mstrCriaId))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtApel'].value=""{1}"";", Request.QueryString("ctrlId"), mstrCriaNomb))
            lsbMsg.Append(String.Format("var pCombo = window.opener.document.all['{0}:cmbRazaCria']; ", Request.QueryString("ctrlId")))
            lsbMsg.Append("for (var lintIndi = 0; lintIndi < pCombo.length; lintIndi++) { ")
            lsbMsg.Append("  if (pCombo.options[lintIndi].value == '" & mstrCriaRaza.Trim & "') {")
            lsbMsg.Append("    pCombo.selectedIndex = lintIndi; } } ")
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtRazaCodi'].value='{1}';", Request.QueryString("ctrlId"), mstrRazaCodi))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtVieneDeCriadores'].value='si';", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.__doPostBack(window.opener.document.all['{0}:txtCodi'].name,'');", Request.QueryString("ctrlId")))
            'lsbMsg.Append("try { window.opener." & Request.QueryString("ctrlId") & "Id_onchange(); } ;")
            'lsbMsg.Append("catch(e){};")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
        End If
        Response.Write(lsbMsg.ToString)
    End Sub
    Public Overrides Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(Columnas.CriaId).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Overrides Sub mCargarDatos(ByVal pstrId As String)
        Try
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(pstrId)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    hdnClieId.Text = clsFormatear.gFormatCadena(.Item("cria_clie_id"))
                    usrClie.Valor = .Item("cria_clie_id")
                    txtNume.Valor = .Item("cria_nume")
                    hdnNumeCria.Text = .Item("cria_nume")
                    cmbRaza.Valor = .Item("cria_raza_id")
                    cmbRaza.Enabled = False
                        txtRespa.Valor = IIf(.Item("clie_respa").ToString.Length > 0, .Item("clie_respa"), String.Empty)
                        txtBajaMoti.Valor = IIf(.Item("cria_baja_moti").ToString.Length > 0, .Item("cria_baja_moti"), String.Empty)

                        txtObse.Valor = IIf(.Item("cria_obse").ToString.Length > 0, .Item("cria_obse"), String.Empty)

                    txtDPDireccion.Valor = .Item("_direPostal")
                    txtDPCP.Valor = .Item("_direCPPostal")

                        txtTParea.Valor = IIf(.Item("_teleAreaPostal").ToString.Length > 0, .Item("_teleAreaPostal"), String.Empty)
                        txtTPTelefono.Valor = IIf(.Item("_telePostal").ToString.Length > 0, .Item("_telePostal"), String.Empty)

                        txtMPMail.Valor = IIf(.Item("_mailPostal").ToString.Length > 0, .Item("_mailPostal"), String.Empty)

                        ' GM 17/09/2022 revisado por error en log. DVN 2022-10-11 0857 se reemplaza el >0 por el = 0
                        txtEstablecimiento.Valor = IIf(.Item("_estable").ToString.Length = 0, "", .Item("_estable").ToString())
                        txtDire.Valor = .Item("_dire").ToString()
                        txtCP.Valor = .Item("_dicl_cpos").ToString()


                        txtTeleArea.Valor = IIf(.Item("_tecl_area_code").ToString.Length > 0, .Item("_tecl_area_code"), String.Empty)
                        txtTeleNume.Valor = IIf(.Item("_tecl_tele").ToString.Length > 0, .Item("_tecl_tele"), String.Empty)
                        txtMail.Valor = IIf(.Item("_macl_mail").ToString.Length > 0, .Item("_macl_mail"), String.Empty)
                        cmbEsta.Valor = .Item("cria_esta_id")
                        cmbConvenioRRGG.Valor = IIf(.Item("Cria_IntIdConvRRGG").ToString.Length > 0, .Item("Cria_IntIdConvRRGG"), String.Empty)
                    chkIntegraComi.Checked = IIf(.Item("_integraComision") = 1, True, False)

                    If Not .IsNull("cria_conf_aviso") Then
                        chkConfAviso.Checked = clsWeb.gFormatCheck(.Item("cria_conf_aviso"), "True")
                    Else
                        chkConfAviso.Checked = False
                    End If

                        txtAltaFecha.Fecha = .Item("cria_alta_fecha").ToString()

                        If Not .IsNull("cria_baja_fecha") Then
                            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("cria_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                        Else
                            lblBaja.Text = ""
                    End If
                    lblTitu.Text = "Datos del Criador: " & txtNume.Text & "  Raza: " & .Item("_raza_desc") & " (Cliente Nro: " & .Item("cria_clie_id") & ")" ''& IIf(.IsNull("_soci_id"), "", " - Socio: " & .Item("_soci_nume")) & ")"

                    If .Item("_esta_baja") Then
                        txtFechaBaja.Enabled = True
                        txtFechaBaja.Fecha = .Item("cria_baja_fecha")
                    End If

                    If Not .IsNull("cria_Docu_Retira_Personal") Then
                        chkRetDocuPersonal.Checked = clsWeb.gFormatCheck(.Item("cria_Docu_Retira_Personal"), "True")
                    Else
                        chkRetDocuPersonal.Checked = False
                    End If

                End With

                hdnModi.Text = "S"
                btnNroCriador.Disabled = True
                btnNroNoCriador.Disabled = True
                txtNume.Enabled = False

                mSetearEditor("", False)
                mMostrarPanel(True)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiarFiltros()
        hdnValorId.Text = "-1"  'para que ignore el id que vino del control
        mstrValorId = ""
        txtNombFil.Text = ""
        txtApelFil.Text = ""
        txtRespaFil.Text = ""
        txtSociNumeFil.Text = ""
        If cmbRazaFil.Enabled Then
            cmbRazaFil.Limpiar()
        End If
        txtCriaNumeFil.Text = ""
        txtCuitFil.Text = ""
        cmbDocuTipoFil.Limpiar()
        txtDocuNumeFil.Text = ""
        txtCunicaFil.Text = ""
        chkBusc.Checked = False
        chkBaja.Checked = False
        usrClieFil.Valor = ""
        'cmbRepreRazaFil.Limpiar()
        'txtRepreCriaNumeFil.Text = ""

        cmbPaisFil.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id")
            clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & IIf(cmbPaisFil.Valor = "", DBNull.Value, cmbPaisFil.Valor), cmbProvFil, "id", "descrip", "S", True)

            grdDato.Visible = False

        End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnBaja.Text = ""
        hdnClieId.Text = ""
        lblBaja.Text = ""
        txtBajaMoti.Text = ""
        txtObse.Text = ""
        txtNume.Text = ""
        txtAltaFecha.Text = DateTime.Now.ToString("dd/MM/yyyy")
        cmbEsta.Limpiar()
        cmbConvenioRRGG.Limpiar()
        txtRespa.Text = ""
        cmbRaza.Limpiar()
        cmbRaza.Enabled = True
        chkConfAviso.Checked = False
        chkIntegraComi.Checked = False

        txtEstablecimiento.Text = ""
        txtDire.Text = ""
        txtDPDireccion.Text = ""
        txtDPCP.Text = ""
        txtTParea.Text = ""
        txtTPTelefono.Text = ""
        txtMPMail.Text = ""
        txtTeleArea.Text = ""
        txtTeleNume.Text = ""
        txtMail.Text = ""
        chkRetDocuPersonal.Checked = False
        hdnNumeCria.Text = ""

        usrClie.Limpiar()

        mCrearDataSet("")

        lblTitu.Text = ""
        hdnModi.Text = ""
        mSetearEditor("", True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        'panBotonesConsul.Visible = (pbooVisi And hdnId.Text <> "")
        btnAgre.Visible = (Not panDato.Visible And Not mbooActi)

        panDato.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim lstrClieId As String

            mGuardarDatos()
            Dim lobjCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Server)

            lstrClieId = lobjCriador.Alta()

                Dim strCriaNume As String

                If (Not txtNume.Valor = "") Then
                    strCriaNume = txtNume.Valor
                Else
                    strCriaNume = clsSQLServer.gFormatArg(hdnNumeCria.Text, SqlDbType.VarChar)
                End If

                Dim lsbMsg As String = "Se registro con exito el Criador nro:" & (cmbRaza.SelectedItem.Text.Split("-")(0)) & " - " & strCriaNume
            AccesoBD.clsError.gGenerarMensajes(Me, lsbMsg)

                If mstrCriaCtrl <> "" Then
                    'mCriador(lstrClieId, txtNume.Text, "", cmbRaza.Valor.ToString, cmbRaza.Valor.ToString)
                    mCriador(lstrClieId, strCriaNume, "", cmbRaza.Valor.ToString, cmbRaza.Valor.ToString)
                    Return
                End If

                mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Server)

            lobjCriador.Modi()

            Dim lsbMsg As String = "Se modifico con exito el Criador nro:" & (cmbRaza.SelectedItem.Text.Split("-")(0)) & " - " & hdnNumeCria.Text
            AccesoBD.clsError.gGenerarMensajes(Me, lsbMsg)

            If mstrValorId <> "" Then
                mCriador(hdnId.Text, txtNume.Text, "", cmbRaza.Valor.ToString, cmbRaza.Valor.ToString)
                Return
            End If

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjCriador.BajaMoti(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int), txtBajaMoti.Text, Session("sUserId").ToString())

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
        If hdnBaja.Text = "1" And txtFechaBaja.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe completar la Fecha de Baja.")
        End If
        If hdnBaja.Text = "1" And txtBajaMoti.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe completar el Motivo de Baja.")
        End If
        Dim crianume As Int32
            If (Not txtNume.Valor = "") Then
                crianume = clsSQLServer.gFormatArg(txtNume.Valor, SqlDbType.Int)
            Else
                crianume = clsSQLServer.gFormatArg(hdnNumeCria.Text, SqlDbType.Int)
        End If
        If (crianume = 0) Then
            Throw New AccesoBD.clsErrNeg("Debe completar el numero de criador/no criador.")
        End If

            If (Not (cmbConvenioRRGG.Valor Is DBNull.Value) And cmbConvenioRRGG.Valor.Length > 0) Then
                Dim idrazaConvenio As Int32 = clsSQLServer.gConsultarValor("select intRazaId from RG_CONVENIOS_RRGG where IntIdConvRRGG =" + cmbConvenioRRGG.Valor, mstrConn, 0)
                If (idrazaConvenio > 0) Then
                    If (cmbRaza.Valor <> idrazaConvenio) Then
                        Throw New AccesoBD.clsErrNeg("Raza no permitida para ese convenio")
                    End If
                End If

            End If
        


    End Sub
    Private Function mGuardarDatos() As DataSet
        Dim lintCpo As Integer
        mValidarDatos()
        With mdsDatos.Tables(0).Rows(0)
            .Item("cria_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("cria_clie_id") = usrClie.Valor
                ' Dario 2015-04-20 no tocar esto
                ' si se carga automaticamente el nro de criador o no criador lo tomamos del hidden
                ' sino se toma del txtnume, caso contrario uno u otro vienen vacios y se rompe
                If (Not txtNume.Valor = "") Then
                    .Item("cria_nume") = txtNume.Valor
                Else
                    .Item("cria_nume") = clsSQLServer.gFormatArg(hdnNumeCria.Text, SqlDbType.Int) ' Dario 2015-04-16 txtNume.Valor  'MF 03/02/2015 Se reemplaza hidden para que tome el dato ingresado por el usuario
            End If

            .Item("clie_respa") = txtRespa.Valor
            .Item("cria_raza_id") = cmbRaza.Valor
            .Item("cria_conf_aviso") = chkConfAviso.Checked
            .Item("cria_alta_fecha") = txtAltaFecha.Fecha
                .Item("cria_obse") = txtObse.Valor
                If txtFechaBaja.Fecha.Length > 0 Then
                    .Item("cria_baja_fecha") = txtFechaBaja.Fecha
                End If

                .Item("cria_esta_id") = cmbEsta.Valor
                .Item("cria_baja_moti") = IIf(hdnBaja.Text = "", DBNull.Value, txtBajaMoti.Valor)
                .Item("cria_Docu_Retira_Personal") = chkRetDocuPersonal.Checked ' Dario 2015-04-16
                If cmbConvenioRRGG.Valor.Length > 0 Then
                    .Item("Cria_IntIdConvRRGG") = cmbConvenioRRGG.Valor
                End If
            End With

        Return mdsDatos
    End Function
    Public Overrides Sub mCrearDataSet(ByVal pstrId As String)
        Dim lstrOpci As String

        lstrOpci = "@audi_user=" & Session("sUserId").ToString
        mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, lstrOpci)

        mdsDatos.Tables(0).TableName = mstrTabla

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        Session(mstrTabla) = mdsDatos
    End Sub
#End Region

#Region "Eventos de Controles"
        Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub
        Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mValidarConsulta()
        mConsultar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
#End Region

    'Private Function mSess(ByVal pstrTabla As String) As String
    '    If hdnSess.Text = "" Then
    '        hdnSess.Text = pstrTabla & Now.Millisecond.ToString
    '    End If
    '    Return (hdnSess.Text)
    'End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
End Namespace
