<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.OrdenVota" CodeFile="OrdenVota.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Orden de Votaci�n</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3" style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Orden de Votaci�n</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD vAlign="top"><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblAsam" runat="server" cssclass="titulo">Asamblea:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbAsam" runat="server" Width="350px" AutoPostback="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 24px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblSociFil" runat="server" cssclass="titulo">Socio:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 75%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrSociFil" runat="server" width="100%" AutoPostback="True" FilCUIT="True" Tabla="Socios" CampoVal="Socio"
																				Saltos="1,1,1" FilSociNume="True" PermiModi="True" MuestraDesc="False" FilDocuNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD vAlign="middle" width="100%">
									<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD vAlign="middle" align="left" width="50%">
												<DIV id=divgraba style="DISPLAY: inline">
													<asp:button cssclass="boton" id="btnAlta" runat="server" Width="170px" CausesValidation="False"
														Text="Generar Orden"></asp:button></DIV>
												<DIV id=divproce style="DISPLAY: none">
													<asp:Image id=Image1 runat="server" ImageUrl="imagenes/btnProce.gif"></asp:Image></DIV>
											</TD>
											<TD align="right">
												<asp:button cssclass="boton" id="btnBaja" runat="server" Width="170" CausesValidation="False"
													Text="Borrar Orden"></asp:button></TD>
											<TD vAlign="middle" width="50%" align="right">
												<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
													IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
													ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
										</TR>
									</TABLE>
								</TD>
								<TD width="50" align="right"></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" width="99%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Height="116px" Visible="False">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD></TD>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
													<TD vAlign="top" align="right">&nbsp;
														<asp:ImageButton id="imgClose" runat="server" CausesValidation="False" ImageUrl="images\Close.bmp"
															ToolTip="Cerrar"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" align="right" colSpan="3">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD vAlign="top" align="right" width="70">
																		<asp:Label id="lblDatos" runat="server" cssclass="titulo">Datos:</asp:Label>&nbsp;
																	</TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtDatos" style="FONT-WEIGHT: bold" runat="server" cssclass="cuadrotexto" Width="100%"
																			Height="150px" EnterPorTab="False" TextMode="MultiLine" Enabled="False"></CC1:TEXTBOXTAB></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';

		function mProcesar()
		{
			var bRet;
			bRet=confirm('�Confirma la generaci�n del Orden de Votaci�n?');
			
			if(bRet)
			{	
				document.all("divgraba").style.display ="none";
				document.all("divproce").style.display ="inline";
			}
			return(bRet); 
		}
		</SCRIPT>
	</BODY>
</HTML>
