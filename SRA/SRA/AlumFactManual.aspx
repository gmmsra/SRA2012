<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AlumFactManual" enableViewStateMac="False" CodeFile="AlumFactManual.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Facturaci�n Manual de Cuotas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript">
		function mSetearConcepto(pConc)
		{
			var strRet = LeerCamposXML("conceptos", "@conc_id="+pConc.value, "_cuenta");
			if (strRet!='')
			{  
				var lstrCta = strRet.substring(0,1);
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCCos').disabled = true;
					document.all('txtcmbCCos').disabled = true;
					document.all('cmbCCos').innerHTML = '';
					document.all('txtcmbCCos').value = '';
				}
				else
				{
					document.all('cmbCCos').disabled = false;
					document.all('txtcmbCCos').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCCos", "N");					
				}
			}			
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');Imprimir();" class="pagina" leftMargin="5" topMargin="5"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Facturaci�n Manual de Cuotas</asp:label>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD></TD>
																<TD vAlign="top" align="right" height="5"></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right" width="150">
																	<asp:label id="lblAlum" runat="server" cssclass="titulo">Alumno:</asp:label>&nbsp;</TD>
																<TD>
																	<UC1:CLIE id="usrAlum" runat="server" Saltos="1,1,1,1" Ancho="800" autopostback="true" FilSociNume="True"
																		Tabla="Alumnos" FilFanta="true"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblFactClie" runat="server" cssclass="titulo" Width="108px">Cliente Fact.:</asp:Label>&nbsp;
																</TD>
																<TD>
																	<UC1:CLIE id="usrFactClie" runat="server" Saltos="1,1,1" autopostback="true" Tabla="Clientes"
																		FilLegaNume="True" FilDocuNume="True" FilCUIT="True"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblClieAlum" runat="server" cssclass="titulo">Alumnos:</asp:Label>&nbsp;</TD>
																<TD width="100%">
																	<cc1:combobox class="combo" id="cmbClieAlum" runat="server" Width="100%" autopostback="true"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:label id="lblFecha" runat="server" cssclass="titulo">Fecha Comp.:</asp:label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="80px" Enabled="False"
																		Obligatorio="true"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panFact" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TableFact" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD height="1" style="WIDTH: 100%" bgcolor=DarkBlue colspan="2"></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right" colspan="2">
																	<asp:ImageButton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="images\Close.bmp"></asp:ImageButton></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblCuota" runat="server" cssclass="titulo">Cuota:</asp:Label>&nbsp;</TD>
																<TD width="100%">
																	<cc1:combobox class="combo" id="cmbCuota" runat="server" Width="100%"></cc1:combobox></TD>
															</TR>
															<TR height="30">
																<TD align="center" colSpan="2"><A id="editarCabe" name="editarCabe"></A>
																	<asp:Button id="btnAltaDet" runat="server" cssclass="boton" Width="120px" CausesValidation="False"
																		Text="Agregar Cuota"></asp:Button></TD>
															</TR>
															<TR>
																<TD>
																	<asp:Label id="lblCopcTit" runat="server" cssclass="titulo">Cuotas:</asp:Label>&nbsp;</TD>
															</TR>
															<TR>
																<TD vAlign="top" align="center" colSpan="2">
																	<asp:datagrid id="grdDeta" runat="server" width="80%" BorderWidth="1px" BorderStyle="None" ShowFooter="True"
																		AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																		OnItemDataBound="mItemDataBound" OnEditCommand="mEditarDatos" AllowPaging="False">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="2%"></HeaderStyle>
																				<ItemTemplate>
																					<asp:LinkButton id="lnkDel" runat="server" Text="Excluir" CausesValidation="false" CommandName="Edit">
																						<img src='imagenes/del.gif' onclick="javascript:return(confirm('�Desea excluir la cuota?'))"
																							border="0" alt="Eliminar Cuota" style="cursor:hand;" />
																					</asp:LinkButton>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="copc_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_alum_desc" HeaderText="Alumno"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_carr_desc" HeaderText="Carrera"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_peri_desc" HeaderText="Cuota"></asp:BoundColumn>
																			<asp:BoundColumn DataField="copc_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid><A id="editarFact" name="editarFact"></A></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD>
																	<asp:Label id="Label1" runat="server" cssclass="titulo">Factura:</asp:Label>&nbsp;</TD>
															</TR>
															<TR>
																<TD vAlign="top" align="center" colSpan="2">
																	<asp:datagrid id="grdFact" runat="server" width="80%" BorderWidth="1px" BorderStyle="None" ShowFooter="True"
																		AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																		OnItemDataBound="mItemDataBoundFact" OnEditCommand="mEditarDatosConc" AllowPaging="False">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:TemplateColumn HeaderStyle-Width="2%">
																				<ItemTemplate>
																					<asp:LinkButton id="lnkEdit" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																						<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																					</asp:LinkButton>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="coco_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_desc" HeaderText="Concepto"></asp:BoundColumn>
																			<asp:BoundColumn DataField="coco_desc_ampl" HeaderText="Descripci�n"></asp:BoundColumn>
																			<asp:BoundColumn DataField="coco_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD align="center" colSpan="2">
																	<DIV>
																		<asp:panel id="panConc" runat="server" width="80%" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid">
																			<TABLE class="FdoFld" id="tabConc" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																				<TR>
																					<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																						<asp:Label id="lblConc" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
																					<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																						<cc1:combobox class="combo" id="cmbConc" runat="server" cssclass="cuadrotexto" Width="300px" onchange="javascript:mSetearConcepto(this);"
																						Obligatorio="True" NomOper="cuentas_cargar" filtra="true" MostrarBotones="False" TextMaxLength=5></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																						<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
																					<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																						<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="cuentas_cargar"
																							filtra="true" MostrarBotones="False" TextMaxLength=6></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD align="right" background="imagenes/formfdofields.jpg">
																						<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																					<TD background="imagenes/formfdofields.jpg">
																						<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"
																							EsDecimal="true"></cc1:numberbox></TD>
																				</TR>
																				<TR>
																					<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				</TR>
																				<TR>
																					<TD onclick="Variable=1;" align="center" colSpan="3"><A id="editar" name="editar"></A>
																						<asp:Button id="btnAltaConc" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																						<asp:Button id="btnBajaConc" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																							Text="Baja"></asp:Button>&nbsp;&nbsp;
																						<asp:Button id="btnModiConc" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																							Text="Modificar"></asp:Button>&nbsp;&nbsp;
																						<asp:Button id="btnLimpConc" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																							Text="Limpiar"></asp:Button></TD>
																				</TR>
																			</TABLE>
																		</asp:panel></DIV>
																</TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
						</TABLE>
						<ASP:PANEL id="panBotones" Runat="server" Visible="False">
							<TABLE width="100%">
								<TR height="30">
									<TD align="center">
										<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
											Text="Grabar"></asp:Button></TD>
								</TR>
							</TABLE>
						</ASP:PANEL>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnFactClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
				<asp:textbox id="hdnConcId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimir" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimio" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnFactNume" runat="server"></asp:textbox>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editarCabe"]!= null)
			document.location='#editarCabe';
		if (document.all["editarFact"]!= null)
			document.location='#editarFact';
		if (document.all["editarConc"]!= null)
			document.location='#editarConc';
			
		function Imprimir()
		{
			if(document.all("hdnImprimir").value!="")
			{
				if (window.confirm("�Desea imprimir la factura Nro " + document.all("hdnFactNume").value + "?"))
				{
					try{
						var sRet = mImprimirCopias(2,'O',document.all("hdnImprimir").value,"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original

						if(sRet=="0") return(sRet);

						if (window.confirm("�Se imprimio la factura correctamente?"))
						{
							var lstrCopias = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimir").value, "copias");

						    if (lstrCopias != "0" && lstrCopias != "1")
								sRet = mImprimirCopias(lstrCopias,'D',document.all("hdnImprimir").value,"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias

							document.all("hdnImprimio").value=1;
							document.all("hdnImprId").value = '';
						}
						else
						{
							document.all("hdnImprId").value = '';
							Imprimir();
						}
					}
					catch(e)
					{
						document.all("hdnImprId").value = '';
						alert("Error al intentar efectuar la impresi�n");
					}
				}
				else
				{
					document.all("hdnImprimio").value=0;
				}
				__doPostBack('hdnImprimio','');
			}
		}
		if (document.all["cmbCCosConcep"]!= null)
		{
			document.all('cmbCCos').disabled = (document.all('cmbCCos').options.length <= 1);
			document.all('txtcmbCCos').disabled = (document.all('cmbCCos').options.length <= 1);
		}
		</SCRIPT>
	</BODY>
</HTML>
