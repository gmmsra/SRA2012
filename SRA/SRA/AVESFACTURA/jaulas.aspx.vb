Namespace SRA

Partial Class Jaulas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents btnEstab As System.Web.UI.HtmlControls.HtmlButton

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Jaulas
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Dim lstrFil As String
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mSess(mstrTabla)) = Nothing
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mMostrarPanel(False)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mSess(mstrTabla))
               Dim x As String = Session.SessionID
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @espe_venta=1", cmbRazaFil, "id", "descrip_codi", "T")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @espe_venta=1", cmbRaza, "id", "descrip_codi", "S")
        clsWeb.gCargarCombo(mstrConn, "estados_cargar @esti_id=120", cmbEstaFil, "id", "descrip", "T")
        clsWeb.gCargarRefeCmb(mstrConn, "expositores", cmbExpoFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "expositores", cmbExpo, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "exposiciones", cmbExposiFil, "T")
        SRA_Neg.Utiles.gSetearRaza(cmbRaza)
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()

      Dim lstrJaulLong As Object
      
      lstrJaulLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtAni.MaxLength = clsSQLServer.gObtenerLongitud(lstrJaulLong, "jaul_anil_nume")
      txtPeso.MaxLength = clsSQLServer.gObtenerLongitud(lstrJaulLong, "jaul_peso")
      txtPrecPart.MaxLength = clsSQLServer.gObtenerLongitud(lstrJaulLong, "jaul_part_precio")
      txtPrecVent.MaxLength = clsSQLServer.gObtenerLongitud(lstrJaulLong, "jaul_vent_precio")
      txtUbic.MaxLength = clsSQLServer.gObtenerLongitud(lstrJaulLong, "jaul_ubic")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrJaulLong, "jaul_obse")

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      hdnExpo.Text = clsSQLServer.gParametroValorConsul(mstrConn, "para_expo")
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()

      Try

         mstrCmd = "exec jaulas_busq "
         mstrCmd = mstrCmd & " @jaul_nume=" & clsSQLServer.gFormatArg(txtNumeFil.Valor.ToString, SqlDbType.Int)
         mstrCmd = mstrCmd & ",@jaul_raza_id=" & clsSQLServer.gFormatArg(cmbRazaFil.Valor.ToString, SqlDbType.Int)
         mstrCmd = mstrCmd & ",@jaul_exps_id=" & clsSQLServer.gFormatArg(cmbExpoFil.Valor.ToString, SqlDbType.Int)
         mstrCmd = mstrCmd & ",@jaul_expo_id=" & clsSQLServer.gFormatArg(cmbExposiFil.Valor.ToString, SqlDbType.Int)
         mstrCmd = mstrCmd & ",@jaul_anil_nume=" & clsSQLServer.gFormatArg(txtAniFil.Valor.ToString, SqlDbType.Int)
         mstrCmd = mstrCmd & ",@fecha_desde=" & clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
         mstrCmd = mstrCmd & ",@fecha_hasta=" & clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
         If cmbPresFil.Valor.ToString <> "T" Then
            mstrCmd = mstrCmd & ",@jaul_pres=" & clsSQLServer.gFormatArg(cmbPresFil.Valor.ToString, SqlDbType.Int)
         End If
         mstrCmd = mstrCmd & ",@jaul_esta_id=" & clsSQLServer.gFormatArg(cmbEstaFil.Valor.ToString, SqlDbType.Int)

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         grdDato.Visible = True

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnAlta.Enabled = pbooAlta
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      txtNume.Enabled = Not (pbooAlta)
      cmbRaza.Enabled = pbooAlta
      cmbExpo.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(1).Text)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Try
         mLimpiar()

         hdnId.Text = clsFormatear.gFormatCadena(pstrId)

         mCrearDataSet(hdnId.Text)

         If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
               hdnId.Text = clsFormatear.gFormatCadena(.Item("jaul_id"))
               txtNume.Text = .Item("jaul_nume")
               txtEsta.Text = .Item("_estado")
               cmbRaza.Valor = .Item("jaul_raza_id")
               cmbExpo.Valor = .Item("jaul_exps_id")
               txtUbic.Valor = .Item("jaul_ubic")
               txtObse.Valor = .Item("jaul_obse")
               txtPeso.Valor = .Item("jaul_peso")
               txtPrecPart.Valor = .Item("jaul_part_precio")
               txtPrecVent.Valor = .Item("jaul_vent_precio")
               txtAni.Valor = .Item("jaul_anil_nume")
               If Not .IsNull("jaul_pres") Then
                  chkPres.Checked = clsWeb.gFormatCheck(.Item("jaul_pres"), "True")
               Else
                  chkPres.Checked = False
               End If
               If Not .IsNull("jaul_prem") Then
                  chkPrem.Checked = clsWeb.gFormatCheck(.Item("jaul_prem"), "True")
               Else
                  chkPrem.Checked = False
               End If
               txtIngreFecha.Fecha = .Item("jaul_ingr_fecha")
               txtRetiFecha.Fecha = .Item("jaul_reti_fecha")
               If Not .IsNull("jaul_baja_fecha") Then
                  lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("jaul_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
               Else
                  lblBaja.Text = ""
               End If
            End With

            mSetearEditor("", False)
            mMostrarPanel(True)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()
      txtNumeFil.Text = ""
      txtAniFil.Text = ""
      txtFechaDesdeFil.Text = ""
      txtFechaHastaFil.Text = ""
      cmbRazaFil.Limpiar()
      cmbExpoFil.Limpiar()
      cmbPresFil.Limpiar()
      cmbEstaFil.Limpiar()
      grdDato.Visible = False
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      lblBaja.Text = ""
      txtObse.Text = ""
      txtNume.Text = ""
      txtIngreFecha.Text = ""
      txtRetiFecha.Text = ""
      txtEsta.Text = ""
      txtAni.Text = ""
      txtUbic.Text = ""
      txtPeso.Text = ""
      txtPrecPart.Text = ""
      txtPrecVent.Text = ""
      cmbRaza.Limpiar()
      cmbExpo.Limpiar()
      chkPres.Checked = False
      chkPrem.Checked = False
      mCrearDataSet("")
      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = (Not panDato.Visible)
      panDato.Visible = pbooVisi
      panFiltros.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible
   End Sub

#End Region

#Region "Opciones de ABM"

   Private Sub mAlta()
      Try
         Dim lstrJaulId As String

         mGuardarDatos()
         Dim lobjJaula As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lstrJaulId = lobjJaula.Alta()

         mConsultar()
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjJaula As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjJaula.Modi()

         mConsultar()
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjJaula As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
         lobjJaula.Baja(hdnId.Text)

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet

      If cmbRaza.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la raza.")
      End If

      If cmbExpo.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el expositor.")
      End If

      If txtIngreFecha.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de ingreso.")
      End If

      With mdsDatos.Tables(0).Rows(0)
        .Item("jaul_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
        .Item("jaul_raza_id") = cmbRaza.Valor
        .Item("jaul_expo_id") = hdnExpo.Text
        .Item("jaul_exps_id") = cmbExpo.Valor
        .Item("jaul_ingr_tipo") = "M"   'abm manual
        .Item("jaul_ubic") = txtUbic.Valor
        .Item("jaul_obse") = txtObse.Valor
        .Item("jaul_peso") = txtPeso.Valor
        .Item("jaul_part_precio") = txtPrecPart.Valor
        .Item("jaul_vent_precio") = txtPrecVent.Valor
        .Item("jaul_anil_nume") = txtAni.Valor
        .Item("jaul_pres") = IIf(chkPres.Checked, True, False)
        .Item("jaul_prem") = IIf(chkPrem.Checked, True, False)
        .Item("jaul_ingr_fecha") = txtIngreFecha.Fecha
        .Item("jaul_reti_fecha") = txtRetiFecha.Fecha
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)
      Dim lstrOpci As String

      lstrOpci = "@audi_user=" & Session("sUserId").ToString
      mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, lstrOpci)

      mdsDatos.Tables(0).TableName = mstrTabla

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      Session(mSess(mstrTabla)) = mdsDatos
   End Sub

#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
#End Region

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

   Protected Overrides Sub Finalize()
      MyBase.Finalize()
   End Sub
End Class
End Namespace
