<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AvesMenu" EnableViewState="false" CodeFile="menu.aspx.vb" %>
<HTML>
	<HEAD>
		<TITLE>SRA</TITLE>
		<script language="JavaScript" type="text/javascript">
		var oMenuAnt=null;
		var oClaseAnt=null;

		function menuonmouseover()
		{
		if(oMenuAnt!=null)
		{
			oMenuAnt.className = oClaseAnt;
		}
		oMenuAnt=window.event.srcElement;
		oClaseAnt=oMenuAnt.className;
		window.event.srcElement.className = window.event.srcElement.className + "_sel";
		}
		
		function Menumenu(ind)
		{
			if (document.all["Tabla" + ind].style.display=="inline")
				document.all["Tabla" + ind].style.display="none";
			else
			{
				var arrDivs = document.getElementsByTagName('table'); 
				for (var i = 0; i < arrDivs.length; i++) {
					if(arrDivs[i].id.indexOf("Tabla") != -1)
					{
						arrDivs[i].style.display = "none";
					}
				}				
				document.all["Tabla" + ind].style.display="inline";
			}
		}
		
		function subMenumenu(ind,sub)
		{
		if (document.all["Tabla" + ind+'_'+sub].style.display=="inline")
			document.all["Tabla" + ind+'_'+sub].style.display="none";
		else
			document.all["Tabla" + ind+'_'+sub].style.display="inline";
		}
		function subMenumenumenu(ind,sub,sub2)
		{
		if (document.all["Tabla" + ind+'_'+sub+'_'+sub2].style.display=="inline")
			document.all["Tabla" + ind+'_'+sub+'_'+sub2].style.display="none";
		else
			document.all["Tabla" + ind+'_'+sub+'_'+sub2].style.display="inline";
		}
		</script>
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body leftMargin="0" background="../imagenes/fdomenu.jpg" topMargin="0" rightMargin="0">
		<form id="Form1" runat="server">
			<table class="cssMenu" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" align="right">
						<table cellSpacing="0" cellPadding="0" width="15" border="1" border-color="white">
							<tr>
								<td align="center"><span id="lblExp" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; CURSOR: hand; COLOR: white; FONT-FAMILY: Verdana; HEIGHT: 4px"
										onclick="CambiarExp(this)">-</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="100%">
					<td vAlign="top" align="left" width="100%">
						<table id="TabMenu" style="WIDTH: 100%" cellSpacing="1" cellPadding="0" runat="server">
						</table> <!--Cierre del TabMenu-->
					</td>
				</tr>
			</table> <!--Cierre tabla principal-->
			<div style="DISPLAY:none"><IFRAME id="Defib" src="refses.aspx" frameBorder="no" width="0" height="0" runat="server"></IFRAME></div>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		<script>
function CambiarExp()
{
	if(document.all("lblExp").innerText=="-")
	{
		parent.document.all(6).cols="15,*";
		document.all("lblExp").innerText="+";
	}
	
	else
	{
		parent.document.all(6).cols="160,*";
		document.all("lblExp").innerText="-";
	}
	
	/*
	var arrDivs = document.getElementsByTagName('table'); 
	for (var i = 0; i < arrDivs.length; i++) {
		if(arrDivs[i].id.indexOf("Tabla") != -1)
		{
		if(document.all("lblExp").innerText=="+")
			arrDivs[i].style.display = "none";
		else
			arrDivs[i].style.display = "inline";
		}
	}
	*/
	if(document.all("lblExp").innerText=="+")
		document.all("TabMenu").style.display = "none";
	else
		document.all("TabMenu").style.display = "inline";
		
	document.all("lblExp").focus();
}

		</script>
		</TD></TR></TABLE>
	</body>
</HTML>
