<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Jaulas" CodeFile="jaulas.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Jaulas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../includes/utiles.js"></script>
		<script language="JavaScript" src="../includes/paneles.js"></script>
		<script language="JavaScript">
	    function expandir()
		{
			if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0"
		onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="../imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recsup.jpg"><IMG height="10" src="../imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="../imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="../imagenes/reciz.jpg"><IMG height="10" src="../imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Jaulas</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderStyle="none" BorderWidth="1px"
											width="100%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="4">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/"
																		IncludesUrl="../includes/" BackColor="Transparent" ImageUrl="../imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/"
																		IncludesUrl="../includes/" BackColor="Transparent" ImageUrl="../imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="../imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="../imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="../imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="../imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="../imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="4">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD background="../imagenes/formfdofields.jpg" height="10"></TD>
																<TD noWrap align="right" background="../imagenes/formfdofields.jpg"></TD>
																<TD background="../imagenes/formfdofields.jpg"></TD>
															</TR>
															<TR>
																<TD background="../imagenes/formfdofields.jpg"></TD>
																<TD noWrap align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblNumeFil" runat="server" cssclass="titulo">Nro. Jaula:</asp:label>&nbsp;</TD>
																<TD background="../imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtNumeFil" runat="server" cssclass="cuadrotexto" IncludesUrl="../Includes/"
																		Width="90px" Visible="true" Obligatorio="True" esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD background="../imagenes/formfdofields.jpg"></TD>
																<TD noWrap align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:label>&nbsp;</TD>
																<TD background="../imagenes/formfdofields.jpg">
																	<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="../Images/"
																		IncludesUrl="../Includes/" Width="277px" AceptaNull="False" Height="20px" MostrarBotones="False"
																		NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD background="../imagenes/formfdofields.jpg"></TD>
																<TD noWrap align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblAniFil" runat="server" cssclass="titulo">Nro. Anillo:</asp:label>&nbsp;</TD>
																<TD background="../imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtAniFil" runat="server" cssclass="cuadrotexto" IncludesUrl="../Includes/"
																		Width="90px" Visible="true" Obligatorio="True" esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD background="../imagenes/formfdofields.jpg"></TD>
																<TD noWrap align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblExpoFil" runat="server" cssclass="titulo">Expositor:</asp:label>&nbsp;</TD>
																<TD background="../imagenes/formfdofields.jpg">
																	<cc1:combobox class="combo" id="cmbExpoFil" runat="server" ImagesUrl="../Images/" IncludesUrl="../Includes/"
																		Width="370px" AceptaNull="False" MostrarBotones="False" NomOper="expositores_cargar" CssClass="cuadrotexto"
																		Filtra="True"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD background="../imagenes/formfdofields.jpg"></TD>
																<TD noWrap align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblFechaFil" runat="server" cssclass="titulo">Fecha Desde:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 75%; HEIGHT: 17px" background="../imagenes/formfdofields.jpg">
																	<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" ImagesUrl="../Images/"
																		IncludesUrl="../Includes/" Width="68px" Obligatorio="False"></cc1:DateBox>&nbsp;
																	<asp:label id="lblHastaFil" runat="server" cssclass="titulo">Hasta:</asp:label>&nbsp;
																	<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="../Images/"
																		IncludesUrl="../Includes/" Width="68px" Obligatorio="False"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg"></TD>
																<TD align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblPresFil" runat="server" cssclass="titulo">Presente:</asp:label>
																&nbsp;
																<TD style="WIDTH: 75%; HEIGHT: 10px" background="../imagenes/formfdofields.jpg">
																	<cc1:combobox class="combo" id="cmbPresFil" runat="server" ImagesUrl="../Images/" IncludesUrl="../Includes/"
																		Width="116px" AceptaNull="False">
																		<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
																		<asp:ListItem Value="1">Si</asp:ListItem>
																		<asp:ListItem Value="0">No</asp:ListItem>
																	</cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formfdofields.jpg"></TD>
																<TD noWrap align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblExposi" runat="server" cssclass="titulo">Exposición:</asp:label>&nbsp;</TD>
																<TD background="../imagenes/formfdofields.jpg">
																	<cc1:combobox class="combo" id="cmbExposiFil" runat="server" ImagesUrl="../Images/" IncludesUrl="../Includes/"
																		Width="230px" AceptaNull="False"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg"></TD>
																<TD style="HEIGHT: 10px" noWrap align="right" background="../imagenes/formfdofields.jpg">
																	<asp:label id="lblEstaFil" runat="server" cssclass="titulo">Estado:</asp:label>&nbsp;</TD>
																<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg">
																	<cc1:combobox class="combo" id="cmbEstaFil" runat="server" ImagesUrl="../Images/" IncludesUrl="../Includes/"
																		Width="230px" AceptaNull="False"></cc1:combobox></TD>
															</TR>
															<TR>
																<TD background="../imagenes/formfdofields.jpg" height="6"></TD>
																<TD style="WIDTH: 105px" noWrap align="right" background="../imagenes/formfdofields.jpg"
																	height="6"></TD>
																<TD background="../imagenes/formfdofields.jpg" height="6"></TD>
															</TR>
														</TABLE>
													</TD>
												<TR>
													<TD style="WIDTH: 2%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
													<TD style="WIDTH: 18.15%" align="right" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
													<TD style="WIDTH: 88%" width="8" background="../imagenes/formdivfin.jpg" height="2"><IMG height="2" src="../imagenes/formdivfin.jpg" width="1"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
									<!--TD width="2" background="../imagenes/formde.jpg"><IMG height="2" src="../imagenes/formde.jpg" width="2" border="0"></TD--></TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
											OnPageIndexChanged="DataGrid_Page">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='../images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="jaul_id" ReadOnly="True" HeaderText="Id Jaula"></asp:BoundColumn>
												<asp:BoundColumn DataField="jaul_nume" ReadOnly="True" HeaderText="Nro. Jaula"></asp:BoundColumn>
												<asp:BoundColumn DataField="raza" ReadOnly="True" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn DataField="expositor" ReadOnly="True" HeaderText="Expositor"></asp:BoundColumn>
												<asp:BoundColumn DataField="jaul_ingr_fecha" ReadOnly="True" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="jaul_anil_nume" ReadOnly="True" HeaderText="Anillo"></asp:BoundColumn>
												<asp:BoundColumn DataField="estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD vAlign="middle" width="100" colSpan="2">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
											ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="../imagenes/" IncludesUrl="../includes/" BackColor="Transparent"
											ImageUrl="../imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Jaula"></CC1:BOTONIMAGEN></TD>
									<TD align="right"></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												width="100%" Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD colSpan="3">
															<TABLE class="FdoFld" id="Table4" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
																border="0">
																<TR>
																	<TD style="WIDTH: 133px" align="right">
																		<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro. Jaula:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD>
																					<asp:Label id="txtNume" runat="server" cssclass="titulo"></asp:Label></TD>
																				<TD align="right">
																					<asp:ImageButton id="imgClose" runat="server" ImageUrl="../images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" vAlign="middle" align="right">
																		<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" ImagesUrl="../Images/"
																			IncludesUrl="../Includes/" Width="300px" Obligatorio="True" AceptaNull="True" Height="20px"
																			MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" align="right">
																		<asp:Label id="lblExpo" runat="server" cssclass="titulo">Expositor:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:combobox class="combo" id="cmbExpo" runat="server" ImagesUrl="../Images/" IncludesUrl="../Includes/"
																			Width="340px" Obligatorio="True" AceptaNull="True" MostrarBotones="False" NomOper="expositores_cargar"
																			CssClass="cuadrotexto" Filtra="True"></cc1:combobox></TD>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" align="right"></TD>
																	<TD colSpan="2">
																		<asp:CheckBox id="chkPres" CssClass="titulo" Text="Presente" Runat="server"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;
																		<asp:CheckBox id="chkPrem" CssClass="titulo" Text="Premio" Runat="server"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px; HEIGHT: 7px" align="right">
																		<asp:Label id="lblIngreFecha" runat="server" cssclass="titulo">Fecha Ingreso:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 7px" colSpan="2">
																		<cc1:DateBox id="txtIngreFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="../Images/"
																			IncludesUrl="../Includes/" Width="68px" Obligatorio="True"></cc1:DateBox>&nbsp;&nbsp;
																		<asp:Label id="lblRetiFecha" runat="server" cssclass="titulo">Fecha Retiro:</asp:Label>&nbsp;
																		<cc1:DateBox id="txtRetiFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="../Images/" IncludesUrl="../Includes/"
																			Width="68px" Obligatorio="True" Enabled="False"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" align="right">
																		<asp:Label id="lblAni" runat="server" cssclass="titulo">Nro. Anillo:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:numberbox id="txtAni" runat="server" IncludesUrl="../Includes/" Width="98px" CssClass="cuadrotexto"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" align="right">
																		<asp:Label id="lblPeso" runat="server" cssclass="titulo">Peso:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:numberbox id="txtPeso" runat="server" IncludesUrl="../Includes/" Width="98px" CssClass="cuadrotexto"
																			EsDecimal="True"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" noWrap align="right">
																		<asp:Label id="lblPrecPart" runat="server" cssclass="titulo">Precio Particular:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:numberbox id="txtPrecPart" runat="server" IncludesUrl="../Includes/" Width="98px" CssClass="cuadrotexto"
																			Enabled="False" EsDecimal="True"></cc1:numberbox>&nbsp;
																		<asp:Label id="lblPrecVent" runat="server" cssclass="titulo">Precio Venta:</asp:Label>&nbsp;
																		<cc1:numberbox id="txtPrecVent" runat="server" IncludesUrl="../Includes/" Width="98px" CssClass="cuadrotexto"
																			Enabled="False" EsDecimal="True" EsPorcen="False"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" vAlign="top" align="right">
																		<asp:Label id="lblUbic" runat="server" cssclass="titulo">Ubicación:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtUbic" runat="server" cssclass="cuadrotexto" IncludesUrl="../Includes/" Width="380px"
																			Obligatorio="false"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" vAlign="top" align="right">
																		<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" IncludesUrl="../Includes/" Width="380px"
																			Obligatorio="false" Height="41px" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="../imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px; HEIGHT: 18px" vAlign="top" align="right">
																		<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 18px" colSpan="2">
																		<asp:Label id="txtEsta" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 133px" vAlign="top" align="right"></TD>
																	<TD colSpan="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<ASP:PANEL id="panBotones" Runat="server">
							<TABLE width="100%">
								<TR>
									<TD align="center">
										<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
								</TR>
								<TR height="30">
									<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
										<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
										<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
											Text="Baja"></asp:Button>&nbsp;&nbsp;
										<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
											Text="Modificar"></asp:Button>&nbsp;&nbsp;
										<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
											Text="Limpiar"></asp:Button></TD>
								</TR>
							</TABLE>
						</ASP:PANEL></td>
					<td width="13" background="../imagenes/recde.jpg"><IMG height="10" src="../imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<!--- FIN CONTENIDO --->
				<tr>
					<td width="9"><IMG height="15" src="../imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="../imagenes/recinf.jpg"><IMG height="15" src="../imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="../imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnExpo" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["txtNume"]!= null && !document.all["txtNume"].disabled)
			document.all["txtNume"].focus();
		gSetearTituloFrame('Jaulas');			
		</SCRIPT>
	</BODY>
</HTML>
