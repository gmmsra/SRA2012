using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ReglasValida
{
	public class Validaciones
	{
		static string mstrCmd = "";
		// Nombres de tablas.
		public static string gstrTabProd = "productos";
		public static string gstrTabTram = "tramites";
		public static string gstrTabTramProd = "tramites_productos";
		public static string gstrTabProdInscPlan = "rg_productos_inscrip_planilla";
		public static string gstrTabProdInsc = "rg_productos_inscrip";
		public static string gstrTabServDenu = "rg_servi_denuncias";
		public static string gstrTabServDenuDeta = "rg_servi_denuncias_deta";

		public class Procesos
		{
			public static int Nacimientos = 1;
			public static int Servicios = 2;
			public static int Transferencias = 5;
		}

		public static DataSet gDeterminarReglas(string pstrConn, string pstrProce)
		{
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlDataAdapter cmdExecCommand = new SqlDataAdapter("exec cs_validaciones_consul " + pstrProce, pstrConn);
			DataSet ds = new DataSet();
			myConnection.Open();
			cmdExecCommand.Fill(ds);
			myConnection.Close();
			return (ds); 
		}

		public static bool mValidar(string pstrConn, string pstrSql)
		{
			bool lbooVali = true;
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = pstrSql;
			SqlDataReader dr = cmdExec.ExecuteReader();
			while (dr.Read())
			{
				lbooVali = false;
			}
			dr.Close();
			myConnection.Close();
			return (lbooVali);
		}

		public static string gValidarReglas(string pstrConn, DataSet dsVali, string pstrTabla, string pstrId, string pstrRaza, string pstrSexo, string pstrUsua, bool pbooGrabarError)
		{
			string lstrErrorId = "";
			string lstrErrorDesc = "";
			lstrErrorId = mArmarReglaSQL(pstrConn, dsVali, pstrTabla, pstrId, pstrRaza, pstrSexo, ref lstrErrorDesc, pbooGrabarError, pstrUsua);
			if (pbooGrabarError && lstrErrorId == "")
			{
				mGrabarAprobacion(pstrConn, pstrTabla, pstrId, pstrUsua);
			}
			return (lstrErrorDesc);
		}

		private static void mGrabarAprobacion(string pstrConn, string pstrTabla, string pstrId, string pstrUsua)
		{
			mstrCmd = "exec cs_validaciones_aprobacion_grabar ";
			mstrCmd = mstrCmd + " '" + pstrTabla + "'";
			mstrCmd = mstrCmd + "," + pstrId;
			mstrCmd = mstrCmd + "," + pstrUsua;
			mExecute(pstrConn, mstrCmd);
		}

		public static string mArmarReglaSQL(string pstrConn, DataSet dsVali, string pstrTabla, string pstrId, string pstrRaza, string pstrSexo, ref string pstrErrorDesc, bool pbooGrabarError, string pstrUsua)
		{
			string lstrPK = "";
			string lstrForm = "";
			string lstrRaza = "";
			string lstrSexo = "";
			string lstrMiniValor = "";
			string lstrMiniTabla = "";
			string lstrMiniFK = "";
			string lstrMiniTablaFK = "";
			string lstrMaxiValor = "";
			string lstrMaxiTabla = "";
			string lstrMaxiFK = "";
			string lstrMaxiTablaFK = "";
			string lstrSql = "";
			string lstrErrorId = "";
			string lstrErrorDesc = "";
			string lstrCampo = "";
			string lstrFrom = "";
			string lstrWhere = "";
			bool lbooObli = false;
			bool lbooManual = false;
			string lstrResuErrorId = "";
			string lstrAgru = "";
			string lstrOrde = "";
			string lstrUltiAgruError = "";
			string lstrRegla = "";
			string lstrAlias = "";

			lstrPK = "prdt_id";
			lstrFrom = pstrTabla;
			lstrWhere = "";

			pstrErrorDesc = "";
            
			if (pstrTabla == gstrTabServDenu)
			{
				lstrAlias = "c.";
				lstrPK = "sede_id";
				lstrFrom = gstrTabServDenu + " c," + gstrTabServDenuDeta + " d";
				lstrWhere = "c.sede_id = d.sdde_sede_id";
			}
			if (pstrTabla == gstrTabServDenuDeta)
			{
				lstrAlias = "d."; 
				lstrPK = "sdde_id";
				lstrFrom = gstrTabServDenu + " c," + gstrTabServDenuDeta + " d";
				lstrWhere = "c.sede_id = d.sdde_sede_id";
			}
			if (pstrTabla == gstrTabTramProd)
			{
				lstrAlias = "p."; 
				lstrPK = "trpr_id";
				lstrFrom = gstrTabTramProd + " p," + gstrTabTram + " t";
				lstrWhere = "p.trpr_tram_id = t.tram_id";
			}
			if (pstrTabla == gstrTabProdInsc)
			{
				lstrAlias = "p."; 
				lstrPK = "pdin_id";
				lstrFrom = gstrTabProdInsc + " i," + gstrTabProd + " p";
				lstrWhere = "i.pdin_id = p.prdt_pdin_id";
			}

			foreach (DataRow odrDeta in dsVali.Tables[0].Rows)
			{
				lstrForm = mFormatCadena(odrDeta.ItemArray[0].ToString());
				lstrSexo = mFormatCadena(odrDeta.ItemArray[1].ToString());
				lstrCampo = mFormatCadena(odrDeta.ItemArray[2].ToString());
				lbooObli = mFormatBolean(odrDeta.ItemArray[3].ToString());
				lstrMiniValor = mFormatCadena(odrDeta.ItemArray[4].ToString());
				lstrMiniTabla = mFormatCadena(odrDeta.ItemArray[5].ToString());
				lstrMiniFK = mFormatCadena(odrDeta.ItemArray[6].ToString());
				lstrMiniTablaFK = mFormatCadena(odrDeta.ItemArray[7].ToString());
				lstrMaxiValor = mFormatCadena(odrDeta.ItemArray[8].ToString());
				lstrMaxiTabla = mFormatCadena(odrDeta.ItemArray[9].ToString());
				lstrMaxiFK = mFormatCadena(odrDeta.ItemArray[10].ToString());
				lstrMaxiTablaFK = mFormatCadena(odrDeta.ItemArray[11].ToString());
				lstrRaza = ";" + mFormatCadena(odrDeta.ItemArray[12].ToString()) + ";";
				lstrErrorId = mFormatCadena(odrDeta.ItemArray[13].ToString());
				lstrErrorDesc = mFormatCadena(odrDeta.ItemArray[14].ToString());
				lbooManual = mFormatBolean(odrDeta.ItemArray[15].ToString());
				lstrAgru = mFormatCadena(odrDeta.ItemArray[16].ToString());
				lstrOrde = mFormatCadena(odrDeta.ItemArray[17].ToString());
                
				// verificar si la validacion corresponde a la raza.
				if (lstrRaza == ";;" || lstrRaza.IndexOf(";" + pstrRaza + ";") != -1)
				{
					// verificar si la validacion corresponde al sexo.
					if (lstrSexo == "" || pstrSexo == "" || lstrSexo == pstrSexo)
					{
						// no seguir validando si ya se produjo un error del mismo grupo de errores.
						if (lstrUltiAgruError != lstrAgru || lstrAgru == "0")
						{
							lstrSql = "select * from " + lstrFrom;
							lstrRegla = "";
							// tablas
							if (lstrMiniTabla != "")
								lstrSql = lstrSql + "," + lstrMiniTabla + " mi";
							if (lstrMaxiTabla != "")
								lstrSql = lstrSql + "," + lstrMaxiTabla + " ma";
							// condiciones
							lstrSql = lstrSql + " where " + lstrPK + " = " + pstrId;
							if (lstrWhere != "")
								lstrSql = lstrSql  + " and " + lstrWhere;
							if (lstrMiniTabla != "")
								lstrSql = lstrSql + " and " + lstrMiniFK + " = mi." + lstrMiniTablaFK;
							if (lstrMaxiTabla != "")
								lstrSql = lstrSql + " and " + lstrMaxiFK + " = ma." + lstrMaxiTablaFK;
							// campo obligatorio.
							if (lbooObli)
							{
								lstrRegla = lstrRegla + lstrCampo + " is not null";
							}
							// valores minimo y maximo.
							if (lstrMiniValor != "")
							{
								if (lstrRegla!= "")
									lstrRegla = lstrRegla + " and ";
								lstrRegla = lstrRegla + lstrAlias + lstrCampo + " >= ";
								if (lstrMiniTabla != "")
									lstrRegla = lstrRegla + "mi.";
								lstrRegla = lstrRegla + lstrMiniValor;
							}
							if (lstrMaxiValor != "")
							{
								if (lstrRegla != "")
									lstrRegla = lstrRegla + " and ";
								lstrRegla = lstrRegla + lstrAlias + lstrCampo + " <= ";
								if (lstrMaxiTabla != "")
									lstrRegla = lstrRegla + "ma.";
								lstrRegla = lstrRegla + lstrMaxiValor;
							}
							// reglas y formulas
							if (lstrRegla != "" && lstrForm != "")
							{
								lstrSql = lstrSql + " and ";
								lstrSql = lstrSql + "(";
								lstrSql = lstrSql + " not (" + lstrRegla + ")";
								lstrSql = lstrSql + " or (" + lstrForm + ")";
								lstrSql = lstrSql + ")";
							}
							else
							{
								// solo reglas
								if (lstrRegla != "")
									lstrSql = lstrSql + " and not (" + lstrRegla + ")";
								// solo formulas
								if (lstrForm != "")
									lstrSql = lstrSql + " and " + lstrForm;
							}
							// realizar la validacion
							if (lstrForm != "" || lstrRegla != "")
							{
								// validacion manual, siempre rechazar.
								if (lbooManual | !mValidar(pstrConn, lstrSql))
								{
									pstrErrorDesc = lstrErrorDesc;
									lstrResuErrorId = lstrErrorId;
									lstrUltiAgruError = lstrAgru;
									if (pbooGrabarError)
										mGrabarError(pstrConn, pstrTabla, pstrId, lstrErrorId, pstrUsua);
								}
							}
						}
					}
				}
			}
			return (lstrResuErrorId); 
		}

		private static void mGrabarError(string pstrConn, string pstrTabla, string pstrId, string lstrErrorId, string pstrUsua)
		{
			mstrCmd = "exec cs_validaciones_errores_grabar ";
			mstrCmd = mstrCmd + " '" + pstrTabla + "'";
			mstrCmd = mstrCmd + "," + pstrId;
			mstrCmd = mstrCmd + "," + lstrErrorId;
			mstrCmd = mstrCmd + "," + pstrUsua;
			mExecute(pstrConn, mstrCmd);
		}

		public static string mFormatCadena(string pstrCade)
		{
			if (pstrCade == null)
			{
				pstrCade = "";
				return (pstrCade);
			}
			pstrCade = pstrCade.Trim();
			pstrCade = pstrCade.Replace("&nbsp;", "");
			return (pstrCade);
		}

		public static bool mFormatBolean(string pstrCade)
		{
			if (pstrCade == null || pstrCade == "")
			{
				return (false);
			}
			return (Convert.ToBoolean(pstrCade));
		}

		public static int mExecute(string pstrConn, string pstrProc)
		{
			if (pstrConn.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrConn");
			}
			if (pstrProc.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrProc");
			}

			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = pstrProc;

			myConnection.Open();
			int lintExec = cmdExecCommand.ExecuteNonQuery();
			myConnection.Close();
			return (lintExec);

		}
	}
}
