Namespace SRA

Partial Class ClientesIncobNoti
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ClientesIncob
   Private mstrIncobDeta As String = SRA_Neg.Constantes.gTab_ClientesIncobDeta

   Private mstrConn As String
   Private mstrTipo As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet

   Private Enum ColumnasDeta As Integer
      Id = 1
      Mail = 7
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      grdDeta.PageSize = Convert.ToInt32(mstrParaPageSize)
      mstrTipo = Request("tipo")
      If mstrTipo = "S" Then
         lblTituAbm.Text = "Notificación a Socios Incobrables"
      Else
         lblTituAbm.Text = "Notificación a Clientes Incobrables"
      End If
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mSetearMaxLength()
            mSetearEventos()
                    txtFecha.Fecha = Today
                    txtFecha.Enabled = False
            txtCierreFecha.Enabled = False
            txtAnioFil.Valor = Today.Year
            mCargarCombos()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnEnvio.Attributes.Add("onclick", "return(mConfirmar());")
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "lsin_obs")
   End Sub

   Private Sub mCargarCombos()
      With cmbTipo
         '.Items.Clear()
         '.Items.Add("(Todos)")
         '.Items(.Items.Count - 1).Value = ""
         If mstrTipo = "S" Then
            .Items.Add("Deuda Social")
            .Items(.Items.Count - 1).Value = "S"
         Else
            .Items.Add("Deuda Cta.Cte.")
            .Items(.Items.Count - 1).Value = "C"
         End If
      End With
      If mstrTipo = "S" Then
         clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar @acti_id=2", cmbMail, "id", "descrip", "S")
      else
         clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar @nosocio='S'", cmbMail, "id", "descrip", "S")
      End If
   End Sub
#End Region

#Region "Seteo de Controles"
   
   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lintComiCant As Integer
      Dim lbooCerrada As Boolean

      mCrearDataSet(pstrId)

      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("lsin_id").ToString()
         txtFecha.Fecha = .Item("lsin_fecha")
         txtObse.Valor = .Item("lsin_obse")

         txtCierreFecha.Fecha = .Item("lsin_cierre_fecha")

         lbooCerrada = Not .IsNull("lsin_cierre_fecha")
      End With

      lblTitu.Text = "Registro Seleccionado: " + CDate(txtFecha.Fecha).ToString("dd/MM/yyyy") + " - " + Left(txtObse.Text, 30)

      mMostrarPanel(True)
   End Sub


   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""
            txtFecha.Fecha = Today.ToString("dd/MM/yyyy")
            txtObse.Text = ""

      txtCierreFecha.Text = ""

      grdDeta.CurrentPageIndex = 0

      txtObse.Enabled = False

      mCrearDataSet("")
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      Dim lbooVisiOri As Boolean = panDato.Visible

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible

      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()

      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      If lbooVisiOri And Not pbooVisi Then
         txtAnioFil.Valor = Today.Year
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Public Sub mCrearDataSet(ByVal pstrId As String)

      'mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, "@etapa = 'N'")
      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrIncobDeta

      With mdsDatos.Tables(mstrTabla).Rows(0)
         If .IsNull("lsin_id") Then
            .Item("lsin_id") = -1
         End If
      End With

      mConsultarDeta()
      Session(mstrTabla) = mdsDatos
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
      Try
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @anio=" + clsSQLServer.gFormatArg(txtAnioFil.Text, SqlDbType.Int))
         lstrCmd.Append(",@etapa=" + clsSQLServer.gFormatArg("N", SqlDbType.VarChar))
         lstrCmd.Append(",@tipo=" + clsSQLServer.gFormatArg(IIf(mstrTipo = "S", 1, 0), SqlDbType.Int))

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Detalle"
   Public Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.EditItemIndex = -1
         If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
            grdDeta.CurrentPageIndex = 0
         Else
            grdDeta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Public Sub mConsultarDeta()
      With mdsDatos.Tables(mstrIncobDeta)
         .DefaultView.RowFilter = ""
         .DefaultView.Sort = "_clie_apel"
         grdDeta.DataSource = .DefaultView
         grdDeta.DataBind()
      End With
   End Sub
#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mListar(ByVal pstrSele As String)
      Try
         Dim lstrRptName As String
         Dim lstrSele As String = pstrSele

         If cmbRepo.Valor.ToString = "E" Then
             lstrRptName = "Etiquetas"
         Else
             lstrRptName = "IncobDeta_Noti"
         End If

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         If cmbRepo.Valor.ToString = "E" Then
            lstrRpt += SRA_Neg.Utiles.ParametrosEtiquetas("NI", , IIf(chkDire.Checked, "True", "False"), , , , , , lstrSele, IIf(cmbRepoMail.Valor.ToString <> "T", cmbRepoMail.Valor.ToString, ""), IIf(cmbRepoCorreo.Valor.ToString <> "T", cmbRepoCorreo.Valor.ToString, ""))
         Else
            lstrRpt += "&inco_id=0"
            lstrRpt += "&inco_ids=" + lstrSele
            If cmbRepoMail.Valor.ToString <> "T" Then
                lstrRpt += "&Mail=" & cmbRepoMail.Valor.ToString
            End If
            If cmbRepoCorreo.Valor.ToString <> "T" Then
                lstrRpt += "&Correo=" & cmbRepoCorreo.Valor.ToString
            End If
         End If

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mObtenerSeleccion(ByVal pTipo As String) As String
      Dim lstrSele As String = ""
      For Each oDataItem As DataGridItem In grdDeta.Items
        If (DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked) Then
            If pTipo <> "M" Or (pTipo = "M" And clsFormatear.gFormatCadena(oDataItem.Cells(ColumnasDeta.Mail).Text) <> "") Then
                If lstrSele <> "" Then
                    lstrSele = lstrSele & ","
                End If
                lstrSele = lstrSele & oDataItem.Cells(ColumnasDeta.Id).Text
            End If
        End If
      Next
      Return (lstrSele)
   End Function

   Private Sub mEnviar()
      Try
         Dim rs As ReportService.ReportingService
         Dim result As Byte() = Nothing
         Dim reportPath As String
            Dim format As String = "HTML4.0"
         Dim historyID As String = Nothing
         Dim devInfo As String = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>"
         Dim eMail As System.Web.Mail.MailMessage
         Dim credentials As ReportService.DataSourceCredentials() = Nothing
         Dim showHideToggle As String = Nothing
         Dim encoding As String
         Dim mimeType As String
         Dim warnings As ReportService.Warning() = Nothing
         Dim reportHistoryParameters As ReportService.ParameterValue() = Nothing
         Dim streamIDs As String() = Nothing
         Dim sh As New ReportService.SessionHeader
         Dim parameters(1) As ReportService.ParameterValue
         Dim lstrAsunto As String

         Dim lstrAsun As String = clsSQLServer.gCampoValorConsul(mstrConn, "mails_modelos_consul @mamo_id=" & cmbMail.Valor.ToString, "mamo_asun")

         rs = New ReportService.ReportingService
         rs.Credentials = System.Net.CredentialCache.DefaultCredentials

         parameters(0) = New ReportService.ParameterValue
         parameters(0).Name = "inco_id"

         parameters(1) = New ReportService.ParameterValue
         parameters(1).Name = "mmca_mamo_id"

         reportPath = "/" & System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString
         reportPath += "/IncobDeta_Noti"

         rs.SessionHeaderValue = sh

         For Each oDataItem As DataGridItem In grdDeta.Items
            If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
               parameters(0).Value = oDataItem.Cells(ColumnasDeta.Id).Text
               parameters(1).Value = cmbMail.Valor.ToString

               result = rs.Render(reportPath, format, historyID, devInfo, parameters, credentials, showHideToggle, encoding, mimeType, reportHistoryParameters, warnings, streamIDs)

               SRA_Neg.clsMail.gEnviarMail(oDataItem.Cells(ColumnasDeta.Mail).Text, lstrAsun, System.Text.Encoding.ASCII.GetString(result))
            End If
         Next

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub grdDeta_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDeta.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 And Not (mdsDatos Is Nothing) Then
            With CType(e.Item.DataItem, DataRowView).Row
               'If .IsNull("_mail") Then
                  'e.Item.FindControl("chkSel").Visible = False
               'Else
                  DirectCast(e.Item.FindControl("chkSel"), CheckBox).Checked = True
               'End If
            End With
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnEnvio_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvio.Click
      Try
        Dim lstrSele As String = mObtenerSeleccion(cmbRepo.Valor.ToString)
        If lstrSele = "" Then
            If cmbRepo.Valor.ToString = "M" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar al menos un cliente que posea dirección de email.")
            Else
                Throw New AccesoBD.clsErrNeg("Debe indicar al menos un cliente.")
            End If
        Else
            If cmbRepo.Valor.ToString = "M" Then
                mEnviar()
            Else
                mListar(lstrSele)
            End If
        End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

Private Sub grdDeta_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdDeta.Init

End Sub
End Class
End Namespace
