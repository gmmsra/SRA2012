<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.FacExterna_detalle_pop" CodeFile="FacExterna_detalle_pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Detalle del comprobante de facturación externa</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		
		
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right"><asp:imagebutton id="btnCerrar" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes/close3.bmp"></asp:imagebutton>&nbsp;
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panOtrosDatos" runat="server" Visible="true" cssclass="titulo" Width="100%"
										BorderWidth="1px" BorderStyle="Solid" height="100%">
										<TABLE class="FdoFld" id="tblOtrosDatos" style="WIDTH: 100%" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 100%" align="left" colSpan="4">
													<asp:label id="lblActividad" runat="server" cssclass="titulo"></asp:label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" align="left" colSpan="4">
													<asp:label id="lblCliente" runat="server" cssclass="titulo"></asp:label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" align="left" colSpan="4">
													<asp:label id="lblFechaImporte" runat="server" cssclass="titulo"></asp:label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" align="left" colSpan="4"></TD>
											</TR>
											<TR>
											<TR>
												<TD style="WIDTH: 70%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<tr>
												<TD style="WIDTH: 100%" align="left" colSpan="4">
													<asp:label id="Label1" runat="server" cssclass="titulo">Detalle de conceptos:</asp:label></TD>
											</tr>
											<TR>
												<TD style="WIDTH: 100%" align="center" colSpan="4">
													<asp:datagrid id="grdConceptos" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
														ItemStyle-Height="5px" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
														CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
														<FooterStyle CssClass="footer"></FooterStyle>
														<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
														<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
														<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
														<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
														<HeaderStyle Font-Size="X-Small" CssClass="header"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="f802_conc" HeaderText="C&#243;d.">
																<HeaderStyle Width="10%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="conc_desc" HeaderText="Descrip.">
																<HeaderStyle Width="40%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="f802_detalle" HeaderText="Descrip.Ampli.">
																<HeaderStyle Width="30%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="f802_impitem" HeaderText="Importe($)">
																<HeaderStyle HorizontalAlign="Right" Width="10%"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="f802_ctrocosto" HeaderText="C.Costo">
																<HeaderStyle Width="10%"></HeaderStyle>
															</asp:BoundColumn>
														</Columns>
														<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 70%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR align="left">
												<TD style="WIDTH: 100%" align="left" colSpan="4">
													<asp:label id="Label2" runat="server" cssclass="titulo">Detalle de vencimientos:</asp:label></TD>
											</TR>
											<TR align="left">
												<TD style="WIDTH: 100%" align="left" colSpan="4">
													<asp:datagrid id="grdVtos" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
														ItemStyle-Height="5px" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
														CellSpacing="1" AllowPaging="True" width="50%">
														<FooterStyle CssClass="footer"></FooterStyle>
														<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
														<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
														<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
														<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
														<HeaderStyle Font-Size="X-Small" CssClass="header"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="f803_fecvto" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
																<HeaderStyle Width="50%"></HeaderStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="f803_impvto" HeaderText="Importe($)" DataFormatString="{0:F2}">
																<HeaderStyle HorizontalAlign="Right" Width="50%"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
														<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
