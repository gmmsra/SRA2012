Namespace SRA

Partial Class Alertas_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblExenta As System.Web.UI.WebControls.Label
    Protected WithEvents lblPercep As System.Web.UI.WebControls.Label
    Protected WithEvents lblPrecSoci As System.Web.UI.WebControls.Label
    Protected WithEvents lblMiemCD As System.Web.UI.WebControls.Label

    Protected WithEvents btnCtaCte As System.Web.UI.WebControls.Button
    Protected WithEvents btnGenerarCuot As System.Web.UI.WebControls.ImageButton
    Protected WithEvents panDato As System.Web.UI.WebControls.Panel

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Public mstrTitulo As String
    Private mstrClieId As String
    Private mstrSociId As String
    Public mstrColor As String
    Private mstrOrigen As String
    Private mstrFechaValor As String
    Private mbooEsConsul As Boolean
    Private mstrConn As String
    Dim mdsDatos As DataSet
    Dim mstrCmd As String

    Private Enum CamposEstado As Integer
        saldo_cta_cte = 0
        saldo_social = 1
        saldo_total = 2
        imp_a_cta_cta_cte = 3
        imp_a_cta_cta_social = 4
        lim_saldo = 5
        color = 6
        Saldo_excedido = 7
        saldo_cta_cte_venc = 8
        saldo_social_venc = 9
        TotalProformas = 10
        OtrasCtas = 11
        OtrasCtasVenc = 12
    End Enum

    Private Enum OtrosDatos As Integer
        cuit = 0
        condIVA = 1
        categoria = 2
        legIsea = 3
        LegCeida = 4
        estado = 5
        NroSocio = 6
        legEgea = 7
    End Enum

    Public Enum CamposEspeciales As Integer
        exento = 0
        iva = 1
        soci = 2
        precio_socio = 3
        miem = 4
        color = 5
        desc = 6
        socio_falle_precio_socio = 7
        soli_tram_precio_socio = 8
        obser = 9
        precio_no_socio = 10 ' fallec, renun en tram, baja, susp
        socio_adhe = 11
        categoIIBB = 15
        nroInsc = 16
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If Not Page.IsPostBack Then
                Dim lobj As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())

                Select Case mstrOrigen
                    Case "S"
                        ' estados de saldos

                        mMostrarPanel(1)
                        If mstrSociId = "" Then
                            btnDeudaSocial.Disabled = True
                            btnDeudaSocialVda.Disabled = True
                        End If
                        Dim lstrCmd As String = "exec proformas_busq @prfr_clie_id=" + mstrClieId
                        Dim ds As New DataSet
                        ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)


                        lobj.EstadoSaldos(mstrConn, mstrClieId, clsFormatear.gFormatFechaDateTime(mstrFechaValor))
                        txtSaldoCaCte.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.saldo_cta_cte)
                        txtSaldoCaCteVenc.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.saldo_cta_cte_venc)
                        txtSaldoCtaSoci.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.saldo_social)
                        txtSaldoCtaSociVenc.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.saldo_social_venc)
                        txtTotalSaldo.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.saldo_total)
                        txtImpCuentaCtaCte.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.imp_a_cta_cta_cte)
                        txtImpCuentaCtaSocial.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.imp_a_cta_cta_social)
                        txtProformas.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.TotalProformas)

                        txtSaldoOtrasCtas.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.OtrasCtas)
                        txtSaldoOtrasCtasVenc.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.OtrasCtasVenc)

                        mCargarProformas(False)
                        If lobj.pEstadoSaldosTodo.Item(CamposEstado.lim_saldo) = "" Then
                            txtLimiteSaldo.Valor = "No Opera"
                        Else
                            If lobj.pEstadoSaldosTodo.Item(CamposEstado.lim_saldo) = 0 Then
                                txtLimiteSaldo.Valor = "Sin L�mite"
                            Else
                                txtLimiteSaldo.Valor = lobj.pEstadoSaldosTodo.Item(CamposEstado.lim_saldo)
                            End If
                        End If
                        txtImpAcuses.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "acuses_total_consul " & mstrClieId, "total_acuses")

                        ' Dario 2013-11-20 se ocultan los botones de los detalles si estan en 
                        ' 0 los txt correspondientes
                        Me.OcultarPulas()
                    Case "O"
                        'otros datos
                        mMostrarPanel(2)
                        Dim lstrCampos As New System.Collections.ArrayList
                        lstrCampos = lobj.OtrosDatosDelCliente(mstrConn, mstrClieId)
                        txtcuit.Text = lstrCampos(OtrosDatos.cuit)
                        txtCondIVA.Text = lstrCampos(OtrosDatos.condIVA)
                        txtCatego.Text = lstrCampos(OtrosDatos.categoria)
                        txtISEA.Text = lstrCampos(OtrosDatos.legIsea)
                        txtCEIDA.Text = lstrCampos(OtrosDatos.LegCeida)
                        txtNroSocio.Text = lstrCampos(OtrosDatos.NroSocio)
                        txtestado.Text = lstrCampos(OtrosDatos.estado)
                        txtEGEA.Text = lstrCampos(OtrosDatos.legEgea)

                        txtCunica.Text = clsSQLServer.gObtenerValorCampo(mstrConn, "clientes_cunica", mstrClieId, "clcu_cunica").ToString.Trim

                        mConsultar(True)
                        mConsultarExpo()

                    Case Else
                        ' especiales
                        mMostrarPanel(3)
                        lobj.Especiales(mstrConn, mstrClieId)

                        'Cargo el combo de Categor�a.
                        clsWeb.gCargarCombo(mstrConn, "iibb_catego_cargar", cmbIIBB, "id", "descrip")

                        txtExenta.Text = lobj.pEspecialesTodo(CamposEspeciales.exento)
                        txtPercep.Text = lobj.pEspecialesTodo(CamposEspeciales.iva)
                        cmbIIBB.Valor = lobj.pEspecialesTodo(CamposEspeciales.categoIIBB)
                        txtNroInsc.Text = lobj.pEspecialesTodo(CamposEspeciales.nroInsc)
                        txtPrecSoci.Text = lobj.pEspecialesTodo(CamposEspeciales.precio_socio)
                        txtMiemCD.Text = lobj.pEspecialesTodo(CamposEspeciales.miem)
                        txtObser.Valor = lobj.pEspecialesTodo(CamposEspeciales.obser)
                End Select
            End If
        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Public Sub mProformasDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lstrMsg As String = ""
        lstrMsg += "<SCRIPT language='javascript'>"
        lstrMsg += "var win=window.open('Proforma_detalle_pop.aspx?id=" & E.Item.Cells(1).Text & "&nro=" & E.Item.Cells(3).Text & "','','location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=780px,height=500px,left=100px,top=100px');"
        lstrMsg += "</SCRIPT>"
        Response.Write(lstrMsg)
    End Sub

    Public Sub grdproforma_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdproforma.EditItemIndex = -1
            If (grdproforma.CurrentPageIndex < 0 Or grdproforma.CurrentPageIndex >= grdproforma.PageCount) Then
                grdproforma.CurrentPageIndex = 0
            Else
                grdproforma.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarProformas(ByVal pbooPage As Boolean)

        Try
            mstrCmd = "exec proformas_totales_busq @prfr_clie_id=" + mstrClieId + ", @fecha= " + clsSQLServer.gFormatArg(mstrFechaValor, SqlDbType.SmallDateTime)

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            grdproforma.DataSource = ds
            grdproforma.DataBind()
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    ' Dario 2013-11-20 Funcion que oculta las lupas si el valor del txt que atiende esta en 0
    Private Sub OcultarPulas()
        Try

            If (txtSaldoCaCte.Valor = 0) Then
                btnDeudaCte.Visible = False
                btnDeudaCte2.Visible = True
            Else
                btnDeudaCte2.Visible = False
            End If

            If (txtSaldoCaCteVenc.Valor = 0) Then
                btnDeudaCteVen.Visible = False
                btnDeudaCteVen2.Visible = True
            Else
                btnDeudaCteVen2.Visible = False
            End If

            If (txtSaldoCtaSoci.Valor = 0) Then
                btnDeudaSocial.Visible = False
                btnDeudaSocial2.Visible = True
            Else
                btnDeudaSocial2.Visible = False
            End If

            If (txtSaldoCtaSociVenc.Valor = 0) Then
                btnDeudaSocialVda.Visible = False
                btnDeudaSocialVda2.Visible = True
            Else
                btnDeudaSocialVda2.Visible = False
            End If

            If (txtSaldoOtrasCtas.Valor = 0) Then
                btnOtrasCtas.Visible = False
                btnOtrasCtas2.Visible = True
            Else
                btnOtrasCtas2.Visible = False
            End If

            If (txtImpCuentaCtaCte.Valor = 0) Then
                btnACtaCtaCte.Visible = False
            End If

            If (txtImpCuentaCtaSocial.Valor = 0) Then
                btnImpCuentaCtaSocial.Visible = False
            End If

            If (txtImpAcuses.Valor = 0) Then
                btnImpAcuses.Visible = False
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        mstrTitulo = Request.QueryString("titulo")
        mstrClieId = Request.QueryString("clieId")
        mstrSociId = Request.QueryString("sociId")
        mstrOrigen = Request.QueryString("origen")
        mstrFechaValor = Request.QueryString("FechaValor")

        hdnSociId.Text = Request.QueryString("sociId")
        hdnFechaValor.Text = Request.QueryString("FechaValor")
        hdnClieId.Text = Request.QueryString("clieId")
        hdnFecha.Text = Today.ToString("dd/MM/yyyy")
        hdnSociId.Text = mstrSociId
    End Sub
#End Region

    Private Sub mMostrarPanel(ByVal pboolmostrar As Integer)
        Select Case pboolmostrar
            Case 1
                panEstadoSaldos.Visible = True
            Case 2
                panOtrosDatos.Visible = True
            Case 3
                panEspeciales.Visible = True
        End Select
    End Sub

    Private Sub btnCtaCte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCtaCte.Click
        Try
            Dim lsbPagina As New System.Text.StringBuilder
            Dim pstrTitulo As String = "Cuenta Corriente"
            ' grilla con la actividad y el importe a cuenta --falta hacerlo  
            'lsbPagina.Append("consulta_pop.aspx?EsConsul=0&titulo=" & pstrTitulo & "&tabla=leyendas_factu")

            clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 500, 400, 9, 250)

        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar(ByVal pbooPage As Boolean)
        Try
            mstrCmd = "exec criadores_consul_busq "
            mstrCmd += mstrClieId


            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            If pbooPage Then
                Dim i As Integer = grdConsulta.Columns.Count - 1
                While i > 0
                    grdConsulta.Columns.Remove(grdConsulta.Columns(i))
                    i -= 1
                End While
            End If

            For Each dc As DataColumn In ds.Tables(0).Columns
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                dgCol.DataField = dc.ColumnName
                dgCol.HeaderText = dc.ColumnName
                If dc.Ordinal = 0 Then
                    dgCol.Visible = False
                End If

                grdConsulta.Columns.Add(dgCol)
            Next
            grdConsulta.DataSource = ds
            grdConsulta.DataBind()
            ds.Dispose()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultarExpo()
        Try
            Dim ds As New DataSet

            mstrCmd = "exec expositores_consul_busq "
            mstrCmd += mstrClieId

            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            grdExpositores.DataSource = ds
            grdExpositores.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConsulta.EditItemIndex = -1
            If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
                grdConsulta.CurrentPageIndex = 0
            Else
                grdConsulta.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub grdExpositores_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdExpositores.EditItemIndex = -1
            If (grdExpositores.CurrentPageIndex < 0 Or grdExpositores.CurrentPageIndex >= grdExpositores.PageCount) Then
                grdExpositores.CurrentPageIndex = 0
            Else
                grdExpositores.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarExpo()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrar.Click
        Response.Write("<Script>window.close();</script>")
    End Sub

    Private Sub btnDeudaSocial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            Dim lsbPagina As New System.Text.StringBuilder

            lsbPagina.Append("SociDeuda_Pop.aspx?soci_id=" & mstrSociId)

            clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 500, 400, 9, 250)

        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub


End Class
End Namespace
