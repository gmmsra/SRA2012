<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CuotasCatego" CodeFile="CuotasCatego.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cuotas por Categor�a</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Cuotas por Categor�a</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
											BorderStyle="Solid">
											<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																		ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblEstaFil" runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbCateFil" class="combo" runat="server" Width="160px" AceptaNull="False"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblInstFil" runat="server" cssclass="titulo">Instituci�n:&nbsp;</asp:Label></TD>
																			<TD style="WIDTH: 75%" background="imagenes/formfdofields.jpg">
																				<cc1:combobox id="cmbInstFil" class="combo" runat="server" Width="200px" AceptaNull="false"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnUpdateCommand="mEditarDatos"
											AutoGenerateColumns="False" width="100%">
											<FooterStyle CssClass="footer"></FooterStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="cuca_id" ReadOnly="True" HeaderText="id"></asp:BoundColumn>
												<asp:BoundColumn DataField="_categoria" ReadOnly="True" HeaderText="Categor&#237;a"></asp:BoundColumn>
												<asp:BoundColumn DataField="_Institucion" HeaderText="Instituci&#243;n"></asp:BoundColumn>
												<asp:BoundColumn DataField="cuca_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="cuca_valor" HeaderText="Valor"></asp:BoundColumn>
												<asp:BoundColumn DataField="cuca_deau_valor" HeaderText="Valor D.A."></asp:BoundColumn>
												<asp:BoundColumn DataField="cuca_valor_joven" HeaderText="Valor Joven"></asp:BoundColumn>
												<asp:BoundColumn DataField="cuca_deau_valor_joven" HeaderText="Valor D.A. Joven"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="cuca_cate_id" HeaderText="ID_cate"></asp:BoundColumn>
												<asp:BoundColumn DataField="_DeveTipoPerio" HeaderText="Perio. Deveng."></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD colSpan="3" vAlign="middle">
										<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD align="left" width="99%"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
														BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
														ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Relaci�n"></CC1:BOTONIMAGEN></TD>
												<TD></TD>
												<TD align="center" width="100"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
														BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
														ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BOTONIMAGEN></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
												BorderStyle="Solid" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="HEIGHT: 16px" width="30%" align="right">
																			<asp:Label id="lblCate" runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px" height="16" colSpan="2">
																			<cc1:combobox id="cmbCate" class="combo" runat="server" Width="248px" onselectedItem=" mSelecCate();"
																				onchange=" mSelecCate();"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" width="30%" align="right">
																			<asp:Label id="lblInst" runat="server" cssclass="titulo">Instituci�n:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" height="16" colSpan="2">
																			<cc1:combobox id="cmbInst" class="combo" runat="server" Width="248px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" width="30%" align="right">
																			<asp:Label id="lblFechaIni" runat="server" cssclass="titulo">Fecha Inicio:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" height="16" colSpan="2">
																			<cc1:DateBox id="txtFechaIni" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" width="30%" align="right">
																			<asp:Label id="lblValor" runat="server" cssclass="titulo">Valor:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" height="16" colSpan="2">
																			<cc1:numberbox style="FONT-SIZE: 8pt" id="txtValor" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="True" height="18px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" width="30%" align="right">
																			<asp:Label id="lblValorDA" runat="server" cssclass="titulo" Width="201px">Valor por debito autom�tico:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" height="16" colSpan="2">
																			<cc1:numberbox style="FONT-SIZE: 8pt" id="txtValorDA" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="True" height="18px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" width="30%" align="right">
																			<asp:Label id="lblValorJ" runat="server" cssclass="titulo">Valor Joven:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" height="16" colSpan="2">
																			<cc1:numberbox style="FONT-SIZE: 8pt" id="txtValorJ" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="True" height="18px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" width="30%" align="right">
																			<asp:Label id="lblValorDAJ" runat="server" cssclass="titulo" Width="201px">Valor por debito autom�tico Joven:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" height="16" colSpan="2">
																			<cc1:numberbox style="FONT-SIZE: 8pt" id="txtValorDAJ" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="True" height="18px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" vAlign="top" width="30%" align="right">
																			<asp:Label id="lblDC" runat="server" cssclass="titulo">A�o ult. cta. devengada:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px" height="14" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtDC" runat="server" cssclass="cuadrotexto" Width="80px" Enabled="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" vAlign="top" width="30%" align="right">
																			<asp:Label id="lblTP" runat="server" cssclass="titulo">Per�odo ult. cta. devengada:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px" height="14" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtUC" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></CC1:TEXTBOXTAB>
																			<CC1:TEXTBOXTAB id="txtTP" runat="server" cssclass="cuadrotexto" Width="140px" Enabled="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 22px" vAlign="top" width="30%" align="right">
																			<asp:Label id="lblPeri" runat="server" cssclass="titulo">Per�odo de Devengamiento:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 22px" height="22" colSpan="2">
																			<cc1:combobox id="cmbPeri" class="combo" runat="server" Width="248px"></cc1:combobox></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center"></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnHisto" runat="server" cssclass="boton" Width="80px" ToolTip="Consultar hist�rico de movimientos"
																CausesValidation="False" Text="Hist�rico"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox>
				<cc1:combobox class="combo" id="cmbCateAux" runat="server" Width="100%" AceptaNull="false"></cc1:combobox>
				<asp:textbox id="hdnClieId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["cmbCate"]!= null)
			document.all["cmbCate"].focus();
			
			function mSelecCate()
		  {
			document.all["cmbCateAux"].value = document.all["cmbCate"].value;
						
			if (document.all["cmbCateAux"].item(document.all["cmbCateAux"].selectedIndex).text == "S")
			  {	
			  	document.all["cmbInst"].disabled = false;
				
               }   
                else
               {
               
                document.all["cmbInst"].disabled = true;
                document.all["cmbInst"].selectedIndex=0;
                
			
               }   
		   }
		</SCRIPT>
	</BODY>
</HTML>
