<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ValidacionGrupos" CodeFile="ValidacionGrupos.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Grupos de Validaci�n</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Grupos de Validaci�n</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="100%">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD colSpan="3">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD>
																	<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:label id="lblCodigoFil" runat="server" cssclass="titulo">C�digo de Grupo:</asp:label></TD>
																			<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtCodigoFil" runat="server" cssclass="cuadrotexto" Width="88px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:label id="lblAbreFil" runat="server" cssclass="titulo">Abreviatura:</asp:label></TD>
																			<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtAbreFil" runat="server" cssclass="cuadrotexto" Width="312px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<FooterStyle CssClass="footer"></FooterStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="grva_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="grva_codi" HeaderText="C&#243;d.Grupo"></asp:BoundColumn>
												<asp:BoundColumn DataField="grva_abre" HeaderText="Abreviatura">
													<HeaderStyle Width="45%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="grva_obse" HeaderText="Observaci&#243;n">
													<HeaderStyle Width="45%"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Grupo de Validaci�n" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Imprimir Listado" ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD colSpan="3"></TD>
								</tr>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Height="124%" Visible="False">
											<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> General</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDeta" runat="server" cssclass="solapa"
															Width="80px" Height="21px" CausesValidation="False"> Razas</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkReglas" runat="server"
															cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Reglas</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Height="116px" Visible="False">
											<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
												align="left">
												<TR>
													<TD vAlign="middle" align="right">
														<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD>
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																<TR>
																	<TD width="25%" align="right">
																		<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo de Grupo:</asp:Label>&nbsp;</TD>
																	<TD width="75%">
																		<CC1:numberbox id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="88px" Obligatorio="True"></CC1:numberbox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD width="25%" align="right">
																		<asp:Label id="lblAbre" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;</TD>
																	<TD width="75%">
																		<CC1:TEXTBOXTAB id="txtAbre" runat="server" cssclass="cuadrotexto" Width="360px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD width="25%" align="right">
																		<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaci�n:</asp:Label>&nbsp;</TD>
																	<TD width="75%">
																		<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="360px"></CC1:TEXTBOXTAB></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD>
														<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
															<TABLE style="WIDTH: 100%" id="TableDetalle" border="0" cellPadding="0" align="left">
																<TR>
																	<TD colSpan="2" align="center">
																		<asp:datagrid id="grdDeta" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																			HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGridDeta_Page"
																			AutoGenerateColumns="False" OnEditCommand="mEditarDeta">
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="grra_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_espe_desc" HeaderText="Especie"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_raza_desc" HeaderText="Raza"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD width="25%" align="right">
																		<asp:Label id="lblEspecie" runat="server" cssclass="titulo">Especie:</asp:Label></TD>
																	<TD width="75%">
																		<cc1:combobox id="cmbEspecie" class="combo" runat="server" Width="248px" onchange="mCargarRaza()"
																			AceptaNull="True"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblRaza" runat="server" cssclass="titulo" Width="24px">Raza:</asp:Label></TD>
																	<TD style="HEIGHT: 19px">
																		<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="300px" onchange="setCmbEspecie()"
																			MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="30" vAlign="middle" colSpan="2" align="center">
																		<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="100px" Text="Alta"></asp:Button>&nbsp;&nbsp;
																		<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Baja"></asp:Button>&nbsp;
																		<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Modificar"></asp:Button>&nbsp;
																		<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="100px" Text="Limpiar "></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD>
														<asp:panel id="panReglas" runat="server" cssclass="titulo" Width="100%">
															<TABLE style="WIDTH: 100%" id="TableReglas" border="0" cellPadding="0" align="left">
																<TR>
																	<TD colSpan="2" align="center">
																		<asp:datagrid id="grdRegla" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																			HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGridRegla_Page"
																			AutoGenerateColumns="False" OnEditCommand="mEditarRegla">
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="grre_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_regla" HeaderText="Regla">
																					<HeaderStyle Width="50%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_mensaje" HeaderText="Mensaje">
																					<HeaderStyle Width="50%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 157px" width="157" align="right">
																		<asp:Label id="lblRegRegla" runat="server" cssclass="titulo">Regla:</asp:Label></TD>
																	<TD width="75%" noWrap align="left">
																		<cc1:combobox id="cmbRegRegla" class="combo" runat="server" Width="253px" AceptaNull="True" nomoper="rg_reglas_vali_cargar"></cc1:combobox>
																		<CC1:TEXTBOXTAB id="txtRegla" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="false"></CC1:TEXTBOXTAB>
																		<asp:Button id="btnVer" runat="server" cssclass="boton" Width="40px" Visible="true" Text="Ver"></asp:Button></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 157px" align="right">
																		<asp:Label id="lblRegMensaje" runat="server" cssclass="titulo">Mensaje:</asp:Label></TD>
																	<TD style="HEIGHT: 19px">
																		<cc1:combobox id="cmbRegMensaje" class="combo" runat="server" Width="253px" AceptaNull="True"
																			nomoper="rg_mensajes_cargar"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD colSpan="2" align="center">
																		<asp:Label id="lblBajaReg" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																</TR>
																<TR>
																	<TD height="30" vAlign="middle" colSpan="2" align="center">
																		<asp:Button id="btnAltaReg" runat="server" cssclass="boton" Width="100px" Text="Alta"></asp:Button>&nbsp;&nbsp;
																		<asp:Button id="btnBajaReg" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Baja"></asp:Button>&nbsp;
																		<asp:Button id="btnModiReg" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Modificar"></asp:Button>&nbsp;
																		<asp:Button id="btnLimpReg" runat="server" cssclass="boton" Width="100px" Text="Limpiar "></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD height="1"></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox><asp:textbox id="hdnReglId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		function mCargarRaza()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecie").value != '')
				sFiltro = document.all("cmbEspecie").value;
				LoadComboXML("razas_cargar", sFiltro, "cmbRaza", "S");
	    }
	    
	    function setCmbEspecie()
	    {
			if (document.all("cmbRaza").value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all("cmbRaza").value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all("cmbEspecie").value = vstrRet[0];
			}		
	    }
	    function mCargarReglas()
	    {
		    LoadComboXML("rg_reglas_vali_cargar", "'"+document.all("txtRegla").value+"'", "cmbRegRegla", "S");
		    return(false);
	    }
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
