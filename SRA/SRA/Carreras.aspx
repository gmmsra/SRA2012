<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.carreras" CodeFile="carreras.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Carreras</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px" height="33"></TD>
									<TD style="HEIGHT: 33px" vAlign="bottom" colSpan="2" height="33"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Carreras</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 5px" width="100%" colSpan="3"></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="97%" BorderWidth="0"
											BorderStyle="Solid">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblDescFil" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtDescFil" runat="server" cssclass="cuadrotexto" Width="406px" MaxLength="80"
																					AceptaNull="False" Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblAbreFil" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtAbreFil" runat="server" cssclass="cuadrotexto" Width="144px" AceptaNull="False"
																					Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR> <!-- Fin Filtro -->
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
											OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="carr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="carr_desc" ReadOnly="True" HeaderText="Carrera">
													<HeaderStyle Width="60%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="carr_abre" ReadOnly="True" HeaderText="Abreviatura"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Carrera"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Carrera</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCorre" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" Height="21px" CausesValidation="False">Materias</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkEqui" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" Height="21px" CausesValidation="False"> Equivalentes</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCarre" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" Height="21px" CausesValidation="False">Correlativas</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD><a name="editar"></a></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" Visible="False" BorderWidth="1px"
												BorderStyle="Solid" Height="116px">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" align="right" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripcion:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="406px" MaxLength="80"
																				AceptaNull="False" Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblAbre" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtAbre" runat="server" cssclass="cuadrotexto" Width="144px" AceptaNull="False"
																				Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 18px" align="right">
																			<asp:Label id="lblPerTipo" runat="server" cssclass="titulo">Tipo Periodo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px" colSpan="2" height="18">
																			<cc1:combobox class="combo" id="cmbPerTipo" runat="server" Width="200px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblDura" runat="server" cssclass="titulo">Duracion:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:NumberBox id="txtDura" runat="server" cssclass="cuadrotexto" Width="64px"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" align="right">
																			<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 21px" colSpan="2" height="21">
																			<cc1:combobox class="combo" id="cmbEsta" runat="server" Width="200px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 3px" align="right">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo Carrera:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 3px" colSpan="2" height="3">
																			<P>
																				<cc1:combobox class="combo" id="cmbTipo" runat="server" Width="200px"></cc1:combobox></P>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 6px" align="right">&nbsp;</TD>
																		<TD style="HEIGHT: 6px" colSpan="2" height="6">
																			<asp:CheckBox id="ChkPresencial" runat="server" Text="Presencial" CssClass="titulo"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 19px" align="right">
																			<asp:Label id="lblAsisCer" runat="server" cssclass="titulo">Certificado de Asistencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 19px" colSpan="2" height="19">
																			<cc1:combobox class="combo" id="cmbAsisCer" runat="server" Width="200px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblAprCert" runat="server" cssclass="titulo">Certificado de aprobado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<cc1:combobox class="combo" id="cmbAprCer" runat="server" Width="200px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 43px" align="right">
																			<asp:Label id="lblObs" runat="server" cssclass="titulo">Observaci�n:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 43px" colSpan="2" height="43">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="296px" MaxLength="120"
																				AceptaNull="False" Obligatorio="False" Height="56px" Columns="9" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																			height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="3">
																			<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																				BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																				BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BotonImagen></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCorre" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdCorre" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" Visible="False"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="grdCorre_PageChanged" OnEditCommand="mEditarDatosCorre" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="maco_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_mate_descComp" HeaderText="Materia">
																						<HeaderStyle Width="44%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_corr_descComp" ReadOnly="True" HeaderText="Correlativa">
																						<HeaderStyle Width="44%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado">
																						<HeaderStyle Width="10%"></HeaderStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 24px" align="right">
																			<asp:Label id="lblmate" runat="server" cssclass="titulo">Materia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 24px">
																			<cc1:combobox class="combo" id="cmbmate" runat="server" Width="400px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 5px" align="right">
																			<asp:Label id="lblMateCorr" runat="server" cssclass="titulo">Correlativa:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 5px">
																			<cc1:combobox class="combo" id="cmbMateCorr" runat="server" Width="400px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha de inicio:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha de fin:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaCorre" runat="server" cssclass="boton" Width="123px" Text="Agregar Correlativa"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaCorre" runat="server" cssclass="boton" Width="123px" Text="Eliminar Correlativa"></asp:Button>&nbsp;
																			<asp:Button id="btnModiCorre" runat="server" cssclass="boton" Width="129px" Text="Modificar Correlativa"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpCorre" runat="server" cssclass="boton" Width="119px" Text="Limpiar Correlativa"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panEqui" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table4" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdEqui" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="False"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="grdEqui_PageChanged" OnEditCommand="mEditarDatosEqui" AutoGenerateColumns="False">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="maeq_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_mate_descComp" HeaderText="Materia">
																						<HeaderStyle Width="40%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_equi_descComp" HeaderText="Equivalente">
																						<HeaderStyle Width="45%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_Estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 25px" align="right">
																			<asp:Label id="lblmateE" runat="server" cssclass="titulo">Materia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 25px">
																			<cc1:combobox class="combo" id="cmbmateE" runat="server" Width="400px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 9px" align="right">
																			<asp:Label id="lblMateEqui" runat="server" cssclass="titulo">Equivalente:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 9px">
																			<cc1:combobox class="combo" id="cmbMateEqui" runat="server" Width="400px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaDesdeEqui" runat="server" cssclass="titulo">Fecha de inicio:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaDesdeEqui" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaHastaEqui" runat="server" cssclass="titulo">Fecha de fin:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaHastaEqui" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaEqui" runat="server" cssclass="boton" Width="133px" Text="Agregar Equivalencia"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaEqui" runat="server" cssclass="boton" Width="131px" Text="Eliminar Equivalencia"></asp:Button>&nbsp;
																			<asp:Button id="btnModiEqui" runat="server" cssclass="boton" Width="134px" Text="Modificar Equivalencia"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpEqui" runat="server" cssclass="boton" Width="123px" Text="Limpiar Equivalencia"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD align="center" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCarre" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdCarre" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" Visible="False"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				OnPageIndexChanged="grdCarre_PageChanged" OnEditCommand="mEditarDatosCarre" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="cama_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_mate_descComp" HeaderText="Materia">
																						<HeaderStyle Width="58%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_peti_desc" HeaderText="Tipo de Per&#237;odo">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cama_perio" HeaderText="Per&#237;odo">
																						<HeaderStyle Width="10%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="cama_carr_id" HeaderText="cama_carr_id"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="cama_peti_id" HeaderText="cama_peti_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_obli" HeaderText="Obligatoria">
																						<HeaderStyle Width="5%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado">
																						<HeaderStyle Width="10%"></HeaderStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 25px" align="right">
																			<asp:Label id="lblmateC" runat="server" cssclass="titulo">Materia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 25px">
																			<cc1:combobox class="combo" id="cmbmateC" runat="server" Width="400px" Obligatorio="True" onchange="cargarComboConTiposDePeriodo(this)"
																				NomOper="materias"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 26px" align="right">
																			<asp:Label id="lblPeriCarre" runat="server" cssclass="titulo">Tipo de Per�odo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 26px">
																			<cc1:combobox class="combo" id="cmbPeriCarre" runat="server" Width="260px" Obligatorio="True"
																				NomOper="periodo_tipos_lectu_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblPerioCarre" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<cc1:numberbox id="txtPerioCarre" runat="server" cssclass="cuadrotexto" Width="112px" Obligatorio="True"
																				MaxValor="9999999999999" esdecimal="False" Wrap="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
																		<TD style="HEIGHT: 32px" colSpan="2" height="32">
																			<asp:CheckBox id="chkObligatorio" Text="Obligatoria" CssClass="titulo" Runat="server"></asp:CheckBox>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaCarre" runat="server" cssclass="boton" Width="100px" Text="Agregar Materia"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaCarre" runat="server" cssclass="boton" Width="100px" Text="Eliminar Materia"></asp:Button>&nbsp;
																			<asp:Button id="btnModiCarre" runat="server" cssclass="boton" Width="108px" Text="Modificar Materia"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpCarre" runat="server" cssclass="boton" Width="100px" Text="Limpiar Materia"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnEquiId" runat="server"></asp:textbox><asp:textbox id="hdnCorreId" runat="server"></asp:textbox><asp:textbox id="hdnCarreId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		
	
		
		function cargarComboConTiposDePeriodo(pCmb)
		{					
		    cargarCombo("cmbPeriCarre",pCmb) 	
		}		
		
		function cargarCombo(pCmbPeri,pCmb)
		{	
		    var sFiltro = obtenerFiltro(pCmb)			    	    	
		    LoadComboXML("periodo_tipos_lectu_cargar", sFiltro, pCmbPeri, "S");		   			    
		}

		function obtenerFiltro(pCmb)
		{	
		    var sFiltro = "@mate_id = " + pCmb.value;
		    return sFiltro;
		}
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
		</SCRIPT>
		</TD></TR></TBODY></TABLE>
	</BODY>
</HTML>
