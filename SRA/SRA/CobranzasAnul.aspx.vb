' Dario 2013-09-02 se habilita el boton solo en los casos que corresponda
Namespace SRA

Partial Class CobranzasAnul
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaIng As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaIngreso As System.Web.UI.WebControls.Label
    Protected WithEvents btnOtrosDatos As NixorControls.BotonImagen
    Protected WithEvents btnSigCabe As System.Web.UI.WebControls.ImageButton


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta

    Private mstrTipo As String
    Private mstrCompId As String
    Public mstrTitu As String

    Private Enum ColumnasFil As Integer
        lnkEdit = 0
        comp_id = 1
        fecha = 2
        tico_desc = 3
        comp_nro = 4
        cliente = 5
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mstrTipo = Request.QueryString("tipo")
            mstrCompId = Request.QueryString("comp_id")

            If mstrCompId <> "" Then
                mstrTitu = "Consulta de " + clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", mstrCompId, "coti_desc").ToString
            Else
                If mstrTipo = "AC" Or mstrTipo = "AS" Then
                    mstrTitu = "Anulación de Aplicaciones y Transferencias"
                Else
                    mstrTitu = "Anulación de Cobranzas"
                End If
            End If

            If Not Page.IsPostBack Then
                mEstablecerPerfil()
                mInicializar()
                mSetearEventos()
                mCargarCombos()

                    txtFechaFil.Fecha = Today

                    If mstrCompId = "" Then
                    mConsultarBusc()
                Else
                    mCargarDatos(mstrCompId)
                End If
                clsWeb.gInicializarControles(sender, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEstablecerPerfil()
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mCargarCombos()
        If mstrTipo = "AC" Or mstrTipo = "AS" Then
            clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbCoti, "", "@coti_ids='" & SRA_Neg.Constantes.ComprobTipos.AplicacionCreditos & "," & SRA_Neg.Constantes.ComprobTipos.Transferencias & "'")
        Else
            clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbCoti, "", "@coti_ids='" & SRA_Neg.Constantes.ComprobTipos.Recibo & "," & SRA_Neg.Constantes.ComprobTipos.Recibo_Acuse & "'")
            cmbCoti.Valor = CInt(SRA_Neg.Constantes.ComprobTipos.Recibo)
        End If
    End Sub

    Public Sub mInicializar()
        Dim lstrParaPageSize As String
        clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(lstrParaPageSize)

        If mstrCompId <> "" Then
            lblTituAbm.Visible = False
            btnBaja.Visible = False
        Else
            lblTituAbm.Text = mstrTitu
            If mstrTipo = "AC" Or mstrTipo = "AS" Then
                    txtFechaFil.Fecha = Today
                    'txtFechaFil.Enabled = False
                End If
            btnVista.Visible = False
        End If
    End Sub

    Private Sub mLimpiarFil()
        cmbCoti.Limpiar()
        usrClieFil.Limpiar()
            txtFechaFil.Fecha = Today
            txtNroCompFil.Text = ""
        grdDato.CurrentPageIndex = 0
        mConsultarBusc()
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultarBusc()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Public Sub grdPagos_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdPagos.CurrentPageIndex = E.NewPageIndex
            mConsultarDeta()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Public Sub grdAplic_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAplic.CurrentPageIndex = E.NewPageIndex
            mConsultarDeta()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Public Sub mConsultarBusc()
        Dim lstrFiltros As New System.text.StringBuilder

        With lstrFiltros
            .Append("exec " + mstrTabla + "_busq")
            .Append(" @coti_id=")
            .Append(cmbCoti.Valor)
            .Append(", @clie_id=")
            .Append(usrClieFil.Valor.ToString())
            .Append(", @comp_ingr_fecha=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
            .Append(", @nro=")
            .Append(txtNroCompFil.Valor)

            Select Case mstrTipo
                Case "AC"
                    .Append(", @modu_id=")
                    .Append(SRA_Neg.Constantes.Modulos.AplicacionCreditos)
                Case "AS"
                    .Append(", @modu_id=")
                    .Append(SRA_Neg.Constantes.Modulos.AplicacionCreditosSocios)
            End Select

            If Session("sCentroEmisorId") Is Nothing Then
                .Append(", @emct_id=-1")
            Else
                .Append(", @emct_id=")
                .Append(Session("sCentroEmisorId"))
            End If

            clsWeb.gCargarDataGrid(mstrConn, .ToString, grdDato)
        End With
    End Sub

    Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            mCargarDatos(e.Item.Cells(1).Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub mCargarDatos(ByVal pstrCompId As String)
        hdnId.Text = pstrCompId
        Dim ldsDatos As DataSet = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, hdnId.Text)

        With ldsDatos.Tables(mstrTabla).Rows(0)
            txtFechaComp.Fecha = .Item("comp_ingr_fecha")
            txtFechaValor.Fecha = .Item("comp_fecha")
            usrClie.Valor = .Item("comp_clie_id")
            txtNroComp.Valor = .Item("comp_nume")
            txtCemi.Valor = .Item("comp_cemi_nume")
            ' Dario 2013-09-02 se habilita el boton solo en los casos que corresponda
            Dim tipoComprob As Int32 = .Item("comp_coti_id")
            Select Case tipoComprob
                Case 13, 28, 29, 30, 31, 32, 33, 34
                    mCargarListar(.Item("comp_coti_id"), pstrCompId)
                    Me.btnVista.Visible = True
                Case Else
                    Me.btnVista.Visible = False
            End Select
            ' Dario fin cambio
        End With

        If ldsDatos.Tables(mstrTablaDeta).Rows.Count > 0 Then
            With ldsDatos.Tables(mstrTablaDeta).Rows(0)
                txtCotDolar.Valor = .Item("code_coti")
                    txtObser.Valor = .Item("code_obse").ToString
                    txtNyap.Valor = .Item("code_reci_nyap").ToString

                chkTPers.Checked = .Item("code_pers")
            End With
        End If

        mConsultarDeta()

        mMostrarPanel(True)
    End Sub

    Private Sub mConsultarDeta()
        Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla & "_anula", hdnId.Text)

        'uno las descripcioneas para el desde/hasta de las cuotas sociales
        For Each lDr As DataRow In ldsDatos.Tables(0).Rows
            With lDr
                If Not .IsNull("descrip2") AndAlso .Item("descrip").ToString.IndexOf(.Item("descrip2").ToString) = -1 Then
                    .Item("descrip") = .Item("descrip").ToString + "-" + .Item("descrip2").ToString
                End If
            End With
        Next

        grdAplic.DataSource = ldsDatos.Tables(0)
        grdAplic.DataBind()

        grdPagos.DataSource = ldsDatos.Tables(1)
        grdPagos.DataBind()
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        Dim lbooVisiOri As Boolean = panDato.Visible

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
        End If
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub mCerrar()
        Try
            If mstrCompId = "" Then
                mMostrarPanel(False)
            Else
                Response.Write("<Script>window.close();</script>")
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjNeg As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjNeg.Baja(hdnId.Text)
            mCerrar()
            mConsultarBusc()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarListar(ByVal pintCotiId As Integer, ByVal pstrId As String)
        Dim lstrRptName As String

        Select Case pintCotiId
            Case SRA_Neg.Constantes.ComprobTipos.Recibo
                lstrRptName = "Recibo"
            Case SRA_Neg.Constantes.ComprobTipos.Recibo_Acuse
                lstrRptName = "Acuse"
        End Select

        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        lstrRpt += "&comp_id=" + pstrId

        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt, False)

        hdnPagina.Text = lstrRpt
    End Sub

    Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultarBusc()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        Try
            mLimpiarFil()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
