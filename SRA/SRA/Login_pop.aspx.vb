Namespace SRA

Partial Class Login_pop
   Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Try
         Dim lstrCmd As String
         Dim lstrUsuaId As String

         lstrCmd = "exec login_consul"
         lstrCmd += " " + clsSQLServer.gFormatArg(UserName.Text, SqlDbType.VarChar)
         lstrCmd += "," + clsSQLServer.gFormatArg(SRA_Neg.Encript.gEncriptar(UserPass.Text), SqlDbType.VarChar)
         lstrCmd += "," + clsSQLServer.gFormatArg("N", SqlDbType.VarChar)

         Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(mstrConn, lstrCmd)
         While (dr.Read())
            lstrUsuaId = dr.GetValue(0).ToString().Trim()
         End While
         dr.Close()

         Dim lsbMsg As New StringBuilder
         lsbMsg.Append("<SCRIPT language='javascript'>")
         lsbMsg.Append(String.Format("window.opener.document.all['hdnLoginPop'].value='{0}';", lstrUsuaId))
         lsbMsg.Append("window.opener.document.all['hdnLoginPop'].onchange();")
         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub
End Class

End Namespace
