Imports System.Data.SqlClient
Imports ReglasValida.Validaciones


Namespace SRA



Partial Class BajaVentas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents cmbProdSexo As NixorControls.ComboBox


    Protected WithEvents lblProv As System.Web.UI.WebControls.Label

    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents lblCria As System.Web.UI.WebControls.Label
    Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblProp As System.Web.UI.WebControls.Label
    Protected WithEvents lblSexo As System.Web.UI.WebControls.Label
    Protected WithEvents lblRP As System.Web.UI.WebControls.Label
    Protected WithEvents lblNomb As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_BajaDenun
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_BajaDenunDeta
    Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Public strConn As String
    Private mdsDatos As DataSet
    Private mstrConn As String
    Private strRazaId As String
    Private strCriadorId As String
    Private strRazaCodi As String
    Private strRazaDescripcion As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                Session(mstrTabla) = Nothing
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mMostrarPanel(False)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnBajaDeta.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
    End Sub

    Private Sub mSetearMaxLength()

        Dim lstrDenuLong As Object
        Dim lstrDetaLong As Object

        lstrDetaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)
        txtNroInscripcion.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "bjdd_nroinscripcion")
        txtDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "bjdd_dire")

        txtDocu.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "bjdd_docu")
        txtObservacion.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "bjdd_observacion")

    End Sub

#End Region

#Region "Inicializacion de Variables"

    Public Sub mInicializar()

        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)


        usrPropFil.Tabla = mstrCriadores
        usrPropFil.Criador = True
        usrPropFil.AutoPostback = False
        usrPropFil.FilClaveUnica = True
        usrPropFil.ColClaveUnica = False
        usrPropFil.Ancho = 790
        usrPropFil.Alto = 510
        usrPropFil.ColDocuNume = False
        usrPropFil.ColCriaNume = True
        usrPropFil.ColCUIT = True
        usrPropFil.FilCUIT = True
        usrPropFil.FilDocuNume = True
        usrPropFil.FilAgru = False
        usrPropFil.FilTarjNume = False

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet


            lstrCmd = "exec " & mstrTabla & "_busq"
            lstrCmd = lstrCmd & " @prop_id=" & clsSQLServer.gFormatArg(usrPropFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ", @raza_id=" & clsSQLServer.gFormatArg(usrPropFil.RazaId, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@fecha_desde=" & clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@fecha_hasta=" & clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)


            'dsDatos = clsWeb.gCargarDataSet(mstrConn, lstrCmd)
            'grdDato.DataSource = dsDatos
            'grdDato.DataBind()
            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

            grdDato.Visible = True







            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Public Function MuestraMotivoBaja(ByVal objText1 As Object) As String



        Dim dtMotivoBaja As DataTable
        Dim sReturn As String = ""



        Dim oMotivoBaja As New SRA_Neg.MotivoBaja(mstrConn, 1)

        dtMotivoBaja = oMotivoBaja.GetMotivoBajaById(objText1.ToString(), "")
        If dtMotivoBaja.Rows.Count > 0 Then
            sReturn = dtMotivoBaja.Rows(0).Item("bamo_DESC").ToString()
        End If

        Return sReturn
    End Function

#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTablaDeta
                btnBajaDeta.Enabled = Not (pbooAlta)
                btnModiDeta.Enabled = Not (pbooAlta)
                btnAltaDeta.Enabled = pbooAlta
            Case Else
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnAlta.Enabled = pbooAlta
        End Select
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(1).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "baja_motivos_cargar @tipoSeleccion=B", cmbBaja, "id", "descrip_codi", "S")
    End Sub

    Public Sub mCargarDatos(ByVal pstrId As String)

        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(pstrId)

        mCrearDataSet(hdnId.Text)

        grdDeta.CurrentPageIndex = 0

        Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())


        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                hdnId.Text = .Item("bjde_id")

                hdnRaza.Text = ValidarNulos(.Item("bjde_raza_id"), False)

                If Not .IsNull("bjde_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("bjde_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
                If hdnRaza.Text <> "0" Then
                    strRazaDescripcion = oRaza.GetRazaDescripcionById(.Item("bjde_raza_id"))
                    strRazaCodi = oRaza.GetRazaCodiById(.Item("bjde_raza_id"))
                End If

                lblTitu.Text = "Denuncia: Raza: " & strRazaDescripcion & " - Criador: " & .Item("_criador") & "- Folio: " & .Item("bjde_folio_nume")
                lblTitu.Visible = False
            End With

            hdnDetaCarga.Text = "S"

            mSetearEditor("", False)
            mSetearEditor(mstrTablaDeta, True)
            mMostrarPanel(True)
            mShowTabs(1)

        End If
    End Sub

    Private Sub mAgregar()
        mLimpiar()

        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiarFiltros()
        hdnValorId.Text = "-1"  'para que ignore el id que vino del control
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        usrPropFil.Limpiar()
        ' usrCriaFil.Limpiar()
        grdDato.Visible = False
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        usrProductoDenBaja.Limpiar()

        mLimpiarDetalle()
        cmbBaja.Limpiar()
        mCrearDataSet("")

        lblTitu.Text = ""

        grdDeta.CurrentPageIndex = 0
        hdnCriadorId.Text = ""
        hdnRaza.Text = ""

        mSetearEditor("", True)
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible

        lnkDeta.Font.Bold = True

        panDato.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panDeta.Visible = True

        tabLinks.Visible = pbooVisi

        lnkDeta.Font.Bold = False
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)

        Try
            Dim lstrTitu As String

            panDato.Visible = True
            panBotones.Visible = True
            btnAgre.Visible = False
            panDeta.Visible = False

            origen = 2
            lnkDeta.Font.Bold = False

            Select Case origen
                Case 2
                    panDeta.Visible = True
                    lnkDeta.Font.Bold = True
                    lblTitu.Text = "Productos de la Denuncia     " & lstrTitu & " Raza: " & _
                    strRazaCodi + "-" + strRazaDescripcion

                    lblTitu.Visible = True
                    grdDeta.Visible = True
            End Select

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim lstrDenuId As String

            mGuardarDatos()
            Dim lobjDenu As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)

            lstrDenuId = lobjDenu.Alta()

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try

            mGuardarDatos()
            Dim lobjDenu As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)

            'If txtFalleFecha.Text <> "" And cmbBaja.Valor Is DBNull.Value Then
            '    Throw New AccesoBD.clsErrNeg("Debe indicar el motivo de la baja.")
            'End If

            lobjDenu.Modi()

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjDenu As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjDenu.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet

        Dim lintDetaCanti As Integer

        mValidarDatos()

        If usrProductoDenBaja.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el propietario.")
        End If


        lintDetaCanti = mdsDatos.Tables(mstrTablaDeta).Select("bjdd_baja_fecha is null").GetUpperBound(0) + 1

        If lintDetaCanti = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar al menos un producto para la denuncia.")
        End If


        With mdsDatos.Tables(0).Rows(0)
            .Item("bjde_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("bjde_prop_cria_id") = hdnCriadorId.Text
            .Item("bjde_cria_id") = hdnCriadorId.Text
            .Item("bjde_raza_id") = hdnRaza.Text
            .Item("bjde_fecha") = DateTime.Now.ToString("dd/MM/yyyy")

        End With

        Return mdsDatos

    End Function

    Public Sub mCrearDataSet(ByVal pstrId As String)

        Dim lstrOpci As String

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDeta

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
        grdDeta.DataBind()

        Session(mstrTabla) = mdsDatos

    End Sub



#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

#End Region

#Region "Detalle"
    Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim intCriaId As Int32
            Dim intRazaId As Int32
            Dim ldrDeta As DataRow

            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim dtRaza As DataTable

            dtRaza = oRaza.GetRazaByCriaId(usrProductoDenBaja.Valor)

            If hdnRaza.Text <> "" Then
                intRazaId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("bjde_raza_id"), False)
            End If

            intCriaId = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("bjde_cria_id"), False)
            If intCriaId = 0 Then
                If hdnCriadorId.Text <> "" Then
                    intCriaId = hdnCriadorId.Text
                End If
            End If

            hdnDetaId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaDeta).Select("bjdd_id=" & hdnDetaId.Text)(0)

            With ldrDeta
                txtDire.Valor = .Item("bjdd_dire")
                txtNroInscripcion.Valor = ValidarNulos(.Item("bjdd_nroinscripcion"), False)
                txtDocu.Valor = ValidarNulos(.Item("bjdd_docu"), False)
                usrProductoDenBaja.txtProdNombExt.Valor = ValidarNulos(.Item("bjdd_nyap"), False)
                txtFecha.Fecha = ValidarNulos(.Item("bjdd_denu_fecha"), False)
                usrProductoDenBaja.Valor = ValidarNulos(.Item("bjdd_prdt_id"), False) ' 2014-11-28 Dario se agrega el id de priducto
                usrProductoDenBaja.txtSraNumeExt.Valor = ValidarNulos(.Item("bjdd_sra_nume"), False)
                usrProductoDenBaja.txtRPExt.Text = ValidarNulos(.Item("bjdd_rp"), False)
                usrProductoDenBaja.txtSexoIdExt.Valor = IIf(.Item("bjdd_sexo"), "1", "0")
                usrProductoDenBaja.CriaOrPropId = intCriaId
                cmbBaja.Valor = ValidarNulos(.Item("bjdd_motivo_baja_id"), False)
                txtObservacion.Text = ValidarNulos(.Item("bjdd_observacion"), False)
                txtNroInscripcion.Text = ValidarNulos(.Item("bjdd_nroinscripcion"), False)

                txtEsta.Text = ValidarNulos(.Item("_estado"), False)
            End With

            btnErr.Visible = (txtEsta.Text.ToUpper = "RECHAZADO")

            mSetearEditor(mstrTablaDeta, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDeta.EditItemIndex = -1
            If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
                grdDeta.CurrentPageIndex = 0
            Else
                grdDeta.CurrentPageIndex = E.NewPageIndex
            End If
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarDetalle(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosDeta(pbooAlta)

            mLimpiarDetalle()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosDeta(ByVal pbooAlta As Boolean)

        Dim ldrDatos As DataRow
        Dim ldrDatosMain As DataRow
        Dim lstrProdRP As String
        Dim lintProdNume As Integer

        mValidarDetalle()

        If hdnDetaId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDeta).NewRow
            ldrDatos.Item("bjdd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "bjdd_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaDeta).Select("bjdd_id=" & hdnDetaId.Text)(0)
        End If

        With ldrDatos
            .Item("bjdd_dire") = ValidarNulos(txtDire.Valor.ToString, False)
            .Item("bjdd_docu") = ValidarNulos(txtDocu.Valor.ToString, False)
            .Item("bjdd_prdt_id") = ValidarNulos(usrProductoDenBaja.Valor, False) ' 2014-11-28 Dario se agrega el id de priducto
            .Item("bjdd_nyap") = ValidarNulos(usrProductoDenBaja.txtProdNombExt.Text, False)
            .Item("bjdd_denu_fecha") = txtFecha.Fecha
            .Item("bjdd_sra_nume") = ValidarNulos(usrProductoDenBaja.txtSraNumeExt.Text, False)
            .Item("bjdd_rp") = ValidarNulos(usrProductoDenBaja.txtRPExt.Text, False)
            .Item("bjdd_sexo") = IIf(usrProductoDenBaja.txtSexoIdExt.Text, True, False)
            .Item("_sexo") = IIf(usrProductoDenBaja.txtSexoIdExt.Text = "1", "M", "H")
            .Item("bjdd_baja_fecha") = System.DBNull.Value
            ' .Item("bjdd_esta_id") = SRA_Neg.Constantes.Estados.Productos_Baja '74
            .Item("bjdd_motivo_baja_id") = cmbBaja.Valor
            .Item("bjdd_observacion") = txtObservacion.Text
            .Item("bjdd_nroinscripcion") = Val(ValidarNulos(txtNroInscripcion.Text, False))
            Dim oEstado As New SRA_Neg.Estado(mstrConn, Session("sUserId").ToString())

            .Item("_estado") = oEstado.GetEstadoDescripById(cmbBaja.Valor)

        End With

        If hdnRaza.Text = "" Then
            hdnRaza.Text = usrProductoDenBaja.RazaId
        End If

        If hdnCriadorId.Text = "" Then
            hdnCriadorId.Text = usrProductoDenBaja.CriaOrPropId
        End If

        If hdnDetaId.Text = "" Then
            mdsDatos.Tables(mstrTablaDeta).Rows.Add(ldrDatos)
        End If

        If hdnId.Text = "" Then
            ldrDatosMain = mdsDatos.Tables(mstrTabla).NewRow
            ldrDatosMain.Item("bjde_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTabla), "bjde_id")
        Else
            ldrDatosMain = mdsDatos.Tables(mstrTabla).Select("bjde_id=" & hdnId.Text)(0)
        End If

        With ldrDatosMain
            .Item("bjde_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("bjde_prop_cria_id") = hdnCriadorId.Text
            .Item("bjde_cria_id") = hdnCriadorId.Text
            .Item("bjde_raza_id") = hdnRaza.Text
            .Item("bjde_fecha") = DateTime.Now.ToString("dd/MM/yyyy")
        End With

        If hdnId.Text = "" Then
            mdsDatos.Tables(mstrTabla).Rows.Add(ldrDatosMain)
        End If

    End Sub

    Sub mValidarDetalle()
        'If usrProductoDenBaja.Valor Is DBNull.Value Then
        '    Throw New AccesoBD.clsErrNeg("Debe de informar el producto")
        'End If

        'If usrProductoDenBaja.Valor.ToString() = "" Then
        '    Throw New AccesoBD.clsErrNeg("Debe indicar el propietario .")
        'End If
        'If usrProductoDenBaja.CriaOrPropId = "" Or usrProductoDenBaja.CriaOrPropId = "0" Then
        '    Throw New AccesoBD.clsErrNeg("Debe indicar el propietario .")
        'End If

        If cmbBaja.Valor.ToString() = "" Then
            Throw New AccesoBD.clsErrNeg("El Estado de baja debe ser distinto de vacio.")
        End If

        If cmbBaja.Valor.ToString() = "3" Then
            If txtDocu.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("El documento debe ser distinto de vacio.")
            End If

            If txtDire.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("La direccion debe ser distinto de vacio.")
            End If

            If txtNroInscripcion.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el nro de inscripcion.")
            End If
        Else
            txtDocu.Valor = ""
            txtDire.Valor = ""
            txtNroInscripcion.Text = ""
        End If

        If usrProductoDenBaja.txtProdNombExt.Text = "" Then
            Throw New AccesoBD.clsErrNeg("El nombre del producto debe ser distinto de vacio.")
        End If

        If txtFecha.Text = "" Then
            txtFecha.Fecha = Now
        End If

        If CDate(txtFecha.Text) > DateTime.Now Then
            Throw New AccesoBD.clsErrNeg("La Fecha de la denuncia de Baja no puede ser superior a la fecha actual.")
        End If

    End Sub

    Private Sub mObtenerDatosProducto(ByVal pstrProdId As String, ByRef pstrProdRP As String, ByRef pintProdNume As Integer)
        Try
            Dim lstrCmd As String
            Dim ldsDatos As DataSet

            lstrCmd = "exec productos_consul " & pstrProdId
            ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                pintProdNume = ""
                pstrProdRP = ""
            Else
                With ldsDatos.Tables(0).Rows(0)
                    pintProdNume = .Item("prdt_sra_nume")
                    pstrProdRP = .Item("prdt_rp").ToString
                End With
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Private Sub mLimpiarDetalle()
        hdnDetaId.Text = ""
        txtFecha.Text = ""
        txtNroInscripcion.Text = ""
        txtDire.Text = ""
        txtDocu.Text = ""
        txtEsta.Text = ""
        txtObservacion.Text = ""

        btnErr.Visible = False
        usrProductoDenBaja.Limpiar()
        cmbBaja.Limpiar()
        usrProductoDenBaja.cmbSexoProdExt.Limpiar()
        usrProductoDenBaja.cmbSexoProdExt.Valor = -1


        If hdnRaza.Text <> "" And hdnCriadorId.Text <> "" Then
            usrProductoDenBaja.RazaId = hdnRaza.Text
            usrProductoDenBaja.CriaOrPropId = hdnCriadorId.Text
        End If
        mSetearEditor(mstrTablaDeta, True)
    End Sub

#End Region

    Private Sub lnkDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
        mShowTabs(2)
    End Sub

    Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        mLimpiarDetalle()
    End Sub

    Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        Try
            With mdsDatos.Tables(mstrTablaDeta).Select("bjdd_id=" & hdnDetaId.Text)(0)
                .Item("bjdd_baja_fecha") = System.DateTime.Now.ToString
                .Item("_estado") = "Baja"
            End With
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()
            mLimpiarDetalle()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
        mActualizarDetalle(True)
    End Sub

    Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        mActualizarDetalle(False)
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub cmbBaja_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBaja.SelectedIndexChanged
        If cmbBaja.Valor = 3 Then
            rowVenta.Style.Add("display", "inline")
        Else
            rowVenta.Style.Add("display", "none")
        End If
    End Sub
End Class
End Namespace
