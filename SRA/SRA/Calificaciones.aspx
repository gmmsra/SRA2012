<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Calificaciones" CodeFile="Calificaciones.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Calificaciones</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function cmbCiclFil_Change(pcmbCiclFil)
		{
			var sFilCicl = pcmbCiclFil.value;
			if(sFilCicl=="") sFilCicl="0";
			var sFiltro = document.all("cmbMateFil").value;
			if(sFiltro=="") sFiltro="0";
			sFiltro = "@cicl_id=" + sFilCicl + ",@mate_id=" + sFiltro;
			LoadComboXML("comisiones_cargar", sFiltro,"cmbComiFil", "N");
		}

		function cmbMateFil_Change(pcmbMateFil)
		{
			var sFiltro = pcmbMateFil.value;
			var anio = new Date();
			if(sFiltro=="") sFiltro="0";
			LoadComboXML("mesas_exam_cargar", "@meex_mate_id=" + sFiltro,"cmbMesaFil","N");
			LoadComboXML("ciclos_cargar", "@cima_mate_id=" + sFiltro + ",@cicl_anio=" + anio.getFullYear(),"cmbCiclFil","N");
			cmbCiclFil_Change(document.all("cmbCiclFil"));
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 27px" height="27"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Calificaciones</asp:label></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="middle" colSpan="2">
											<asp:panel id="panFiltros" runat="server" cssclass="titulo" width="95.36%" BorderStyle="Solid"
												BorderWidth="0">
												<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TBODY>
														<TR>
															<TD style="WIDTH: 100%">
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 8px" width="24"></TD>
																		<TD style="HEIGHT: 8px" width="42"></TD>
																		<TD style="HEIGHT: 8px" width="26"></TD>
																		<TD style="HEIGHT: 8px"></TD>
																		<TD style="HEIGHT: 8px" width="26"><CC1:BOTONIMAGEN id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																				BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
																				ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
																		<TD style="HEIGHT: 8px" width="26"><CC1:BOTONIMAGEN id="btnLimpFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																				BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
																				ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BOTONIMAGEN></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																		<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																		<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																		<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD>
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TBODY>
																		<TR>
																			<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																			<TD><!-- FOMULARIO -->
																				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																					<TBODY>
																						<TR>
																							<TD style="WIDTH: 8%; HEIGHT: 20px" background="imagenes/formfdofields.jpg"></TD>
																							<TD style="WIDTH: 10%; HEIGHT: 20px" align="right" background="imagenes/formfdofields.jpg"><asp:label id="lblMateDil" runat="server" cssclass="titulo">Materia:</asp:label>&nbsp;</TD>
																							<TD style="WIDTH: 82%; HEIGHT: 20px" background="imagenes/formfdofields.jpg"><cc1:combobox class="combo" id="cmbMateFil" runat="server" onchange="cmbMateFil_Change(this)"
																									AceptaNull="False" Width="504px"></cc1:combobox></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 8%; HEIGHT: 2px" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																							<TD style="WIDTH: 10%; HEIGHT: 2px" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																							<TD style="WIDTH: 82%; HEIGHT: 2px" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 8%; HEIGHT: 13px" align="right" background="imagenes/formfdofields.jpg"></TD>
																							<TD style="WIDTH: 10%; HEIGHT: 13px" align="right" background="imagenes/formfdofields.jpg"><asp:label id="lblCiclFil" runat="server" cssclass="titulo">Ciclo:</asp:label>&nbsp;</TD>
																							<TD style="WIDTH: 82%; HEIGHT: 13px" background="imagenes/formfdofields.jpg"><cc1:combobox class="combo" id="cmbCiclFil" runat="server" onchange="cmbCiclFil_Change(this)"
																									AceptaNull="False" Width="504px"></cc1:combobox></TD>
																						</TR>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 8%; HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg"
																				height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg"
																				height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 82%; HEIGHT: 2px" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"><asp:label id="lblComiFil" runat="server" cssclass="titulo">Comisión:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 82%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"><cc1:combobox class="combo" id="cmbComiFil" runat="server" AceptaNull="False" Width="504px" NomOper="comisiones_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 8%; HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg"
																				height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg"
																				height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 82%; HEIGHT: 2px" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 8%; HEIGHT: 21px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 21px" align="right" background="imagenes/formfdofields.jpg"><asp:label id="lblMesaFil" runat="server" cssclass="titulo">Mesa:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 82%; HEIGHT: 21px" background="imagenes/formfdofields.jpg"><cc1:combobox class="combo" id="cmbMesaFil" runat="server" AceptaNull="False" Width="504px" NomOper="mesas_exam_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 8%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 82%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"><asp:label id="lblAlumFil" runat="server" cssclass="titulo">Alumno:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 82%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"><UC1:CLIE id="usrAlumFil" runat="server" AceptaNull="False" Tabla="Alumnos" Saltos="1,1,1"
																					FilLegaNume="True" MuestraDesc="False" Ancho="800" CampoVal="Alumno" FilDocuNume="True" FilCUIT="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">&nbsp;</TD>
																			<TD style="WIDTH: 82%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<td align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></td>
																			<td align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></td>
																			<td width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></td>
																		</TR>
																	</TBODY>
																</TABLE>
															</TD>
															<td width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></td>
														</TR>
													</TBODY>
												</TABLE>
											</asp:panel>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							</td>
					</tr>
					<TR height="10">
						<TD colSpan="3"></TD>
					</TR>
					<TR id="trCati" height="25" runat="server" visible="false">
						<TD colSpan="3"><asp:label id="lblCati" runat="server" cssclass="titulo">Tipo de Calificación:</asp:label>&nbsp;&nbsp;<cc1:combobox class="combo" id="cmbCati" runat="server" Width="180px"></cc1:combobox></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="98%" BorderStyle="None" BorderWidth="1px" Visible="True"
								AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" PageSize="200">
								<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
								<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
								<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
								<Columns>
									<asp:BoundColumn Visible="False" DataField="cali_id"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="cali_alum_id"></asp:BoundColumn>
									<asp:BoundColumn DataField="_alum_lega" ReadOnly="True" HeaderText="Nro.Legajo"></asp:BoundColumn>
									<asp:BoundColumn DataField="_alum_desc" HeaderText="Apellido y Nombre"></asp:BoundColumn>
									<asp:BoundColumn DataField="_desc" HeaderText="Tipo"></asp:BoundColumn>
									<asp:BoundColumn DataField="cali_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="_mate_cali" HeaderText="_mate_cali"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Calif.">
										<HeaderStyle Width="7%"></HeaderStyle>
										<ItemTemplate>
											<cc1:numberbox id="txtCali" runat="server" EsDecimal="True" cssclass="cuadrotexto" Width="55px"
												height="18px" style="font-size: 8pt"></cc1:numberbox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Aprob.">
										<HeaderStyle Width="3%"></HeaderStyle>
										<ItemTemplate>
											<cc1:combobox class="combo" id="cmbApro" runat="server" Width="100px"></cc1:combobox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="_asis_porc" HeaderText="% Asistencia">
										<HeaderStyle Width="80px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="_apro" HeaderText="Aprobado">
										<HeaderStyle Width="60px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD vAlign="bottom" align="left" colSpan="2" height="30"><CC1:BOTONIMAGEN id="btnModi" runat="server" BorderStyle="None" ImageOver="btnGrab2.gif" ImageBoton="btnGrab.gif"
								BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ForeColor="Transparent"
								ImageUrl="imagenes/btnImpr.jpg" Visible="False"></CC1:BOTONIMAGEN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
					</TR>
				</TBODY>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
			</TR>
			<tr>
				<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
				<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
				<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["cmbMateFil"]!= null)
			document.all["cmbMateFil"].focus();
		</SCRIPT>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
