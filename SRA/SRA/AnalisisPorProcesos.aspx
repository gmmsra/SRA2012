<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AnalisisPorProcesos" CodeFile="AnalisisPorProcesos.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>An�lisis Por Procesos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		
		function mCambioEspecie(pEspe)
		{
			mCargarResultados(pEspe);
			
		}
		function mCargarResultados(pEspe)
		{
		    var sFiltro = "@espe_id=" + pEspe.value;
		    if (sFiltro!='')
				LoadComboXML("rg_analisis_resul_cargar", sFiltro, "cmbResultados", "S");
			else
				document.all('cmbResultados').selectedIndex = 0;
		}		
		function mCargarRazas(pEspe)
		{
		    var sFiltro = "@raza_espe_id=" + pEspe.value;
		    if (sFiltro!='')
				LoadComboXML("razas_cargar", sFiltro, "cmbRazas", "S");
			else
				document.all('cmbRazas').selectedIndex = 0;
		}
/*		function mMensajes()
	   	{
			gAbrirVentanas("mensajes.aspx?t=popup",1);
		}*/
/*		function mCondicion()
	   	{
	   		var lstrSp = 'rg_formulas_cargar';
	   		var lstrFil = document.all('cmbFormula').value;
	   		var lstrCamp = 'condicion';
	   		var lstrTitu = 'Formula: '+document.all["cmbFormula"].item(document.all["cmbFormula"].selectedIndex).text;
			if (lstrFil == '')
		   	{
				alert('Debe seleccionar una f�rmula.');	
				document.all('cmbFormula').focus();
				return(false);
			}				
			gAbrirVentanas("ConsultaDatos.aspx?titu="+lstrTitu+"&fil="+lstrFil+"&sp="+lstrSp+"&camp="+lstrCamp,1);
		}
*/		function mCargarMensajes()
	   	{
			LoadComboXML("rg_mensajes_cargar", "", "cmbMensaje", "S");
		}		

		function mSetearValores(ptxtObje,pcmbObje)
		{
			if (pcmbObje.value == '')
			{
				document.all[ptxtObje].disabled = false;
			}
			else
			{
				document.all[ptxtObje].disabled = true;
				document.all[ptxtObje].value = '';
			}		
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">An�lisis por Procesos</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="100%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																		BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																		BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 37.04%; HEIGHT: 25px" vAlign="top" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblEspecie" runat="server" cssclass="titulo" Width="54px">Especie:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbEspeFil" runat="server" cssclass="cuadrotexto" Width="312px"
																					AceptaNull="False" Height="20px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 37.04%; HEIGHT: 25px" vAlign="top" noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblProceso" runat="server" cssclass="titulo" Width="54px">Proceso:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbProceFil" runat="server" cssclass="cuadrotexto" Width="312px"
																					AceptaNull="False" Height="20px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="arrp_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="_espe_desc" HeaderText="Especie"></asp:BoundColumn>
												<asp:BoundColumn DataField="_ares_desc" HeaderText="Resultado"></asp:BoundColumn>
												<asp:BoundColumn DataField="_rpro_desc" HeaderText="Proceso"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="arrp_tipo" HeaderText="Tipo_id"></asp:BoundColumn>
												<asp:BoundColumn DataField="_tipo_desc" HeaderText="Tipo"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Denuncia"></CC1:BOTONIMAGEN></TD>
									<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
											BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ImageDisable="btnImpr0.gif" ToolTip="Imprimir Listado"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD colSpan="3"></TD>
								</tr>
								<TR>
									<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Height="124%" Visible="False">
											<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
												<TR>
													<TD width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
													<TD width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> General </asp:linkbutton></TD>
													<TD width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
															Width="80px" Height="21px" CausesValidation="False"> Razas</asp:linkbutton></TD>
													<TD width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												width="99%" Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 19px" vAlign="middle" align="right">
																			<asp:label id="Label1" runat="server" cssclass="titulo" Width="54px">Especie:</asp:label>&nbsp;</TD>
																		<TD style="HEIGHT: 19px" colSpan="2">
																			<cc1:combobox class="combo" id="cmbEspecie" runat="server" cssclass="cuadrotexto" Width="312px"
																				AceptaNull="False" Height="20px" onchange="javascript:mCambioEspecie(this);"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 19px" vAlign="middle" align="right">
																			<asp:Label id="lblResultado" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 19px" colSpan="2">
																			<cc1:combobox class="combo" id="cmbResultados" runat="server" cssclass="cuadrotexto" Width="300px"
																				AceptaNull="True" Height="20px" NomOper="rg_analisis_resul_cargar" MostrarBotones="False"
																				filtra="true" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Tipo:</asp:Label></TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<cc1:combobox class="combo" id="cmbTipo" runat="server" Width="113px" AceptaNull="False">
																				<asp:ListItem Value="null" Selected="True">(Todos)</asp:ListItem>
																				<asp:ListItem Value="0">Tipificaci&oacute;n Sangu&iacute;nea</asp:ListItem>
																				<asp:ListItem Value="1">ADN</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:Label id="Label3" runat="server" cssclass="titulo">Vigencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<asp:Label id="lblDesdeFecha" runat="server" cssclass="titulo">Desde:</asp:Label>
																			<cc1:DateBox id="txtDesdeFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblHastaFecha" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="txtHastaFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 114px; HEIGHT: 18px" vAlign="middle" align="right">
																			<asp:label id="Label4" runat="server" cssclass="titulo" Width="54px">Proceso:</asp:label>&nbsp;</TD>
																		<TD style="HEIGHT: 18px" colSpan="2">
																			<cc1:combobox class="combo" id="cmbProceso" runat="server" cssclass="cuadrotexto" Width="312px"
																				AceptaNull="False" Height="20px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="center" colSpan="2">
																			<asp:datagrid id="grdDeta" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnPageIndexChanged="DataGridDeta_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																				AllowPaging="True" Visible="True" OnEditCommand="mEditarDeta">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="arrd_id" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_raza_id" ReadOnly="True" HeaderText="id_raza"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_raza_desc" HeaderText="Raza"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="16"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 22px" align="right">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label></TD>
																		<TD style="HEIGHT: 22px">
																			<cc1:combobox class="combo" id="cmbRazas" runat="server" Width="248px" AceptaNull="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="100px" Text="Alta"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Baja"></asp:Button>&nbsp;
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="100px" Visible="true" Text="Modificar"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="100px" Text="Limpiar "></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all[document.all('txtMini')]!= null)
		{
			mSetearValores(document.all('txtMini'),document.all('cmbMini'));
			mSetearValores(document.all('txtMaxi'),document.all('cmbMaxi'));
		}
		</SCRIPT>
	</BODY>
</HTML>
