Namespace SRA

Partial Class CuotasCatego
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lblCateFil As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region


#Region "Definici�n de Variables"
   Private mstrTabla As String = "cuotas_catego"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String

   Private Enum Columnas As Integer
      Id = 1
      
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar()
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCateFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInstFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "categorias_aux", cmbCateAux, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeri, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInst, "S")
      cmbInstFil.Items.Insert(1, "(Ninguna)")
      cmbInstFil.Items(1).Value = "N"
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()

      'Dim lbooPermiAlta As Boolean
      'Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Cuotas_Catego, String), (mstrConn), (Session("sUserId").ToString()))) Then
      '   Response.Redirect("noaccess.aspx")
      'End If


      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Cuotas_Catego_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta
      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Cuotas_Catego_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Cuotas_Catego_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi
      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)

   End Sub



#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)


   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub



   Private Sub mSelecCate()
      cmbCateAux.Valor = cmbCate.Valor
      If cmbCateAux.SelectedItem.Text = "S" Then
         cmbInst.Enabled = True
      Else
         cmbInst.Enabled = False
         cmbInst.SelectedIndex = 0
      End If
   End Sub

   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla & "_consul"
         mstrCmd += " @cuca_cate_id = " + cmbCateFil.Valor.ToString
         mstrCmd += ",@cuca_inst_id = " + IIf(cmbInstFil.Valor.ToString = "N", "0", cmbInstFil.Valor.ToString)
         mstrCmd += ",@cuca_inst_ninguna = " + IIf(cmbInstFil.Valor.ToString = "N", "1", "0")

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    cmbCate.Valor = IIf(.Item("cuca_cate_id").ToString.Trim.Length > 0, .Item("cuca_cate_id"), String.Empty)
                    txtFechaIni.Fecha = IIf(.Item("cuca_fecha").ToString.Trim.Length > 0, .Item("cuca_fecha"), String.Empty)
                    txtValor.Valor = IIf(.Item("cuca_valor").ToString.Length > 0, .Item("cuca_valor"), String.Empty)
                    txtValorDA.Valor = IIf(.Item("cuca_deau_valor").ToString.Length > 0, .Item("cuca_deau_valor"), String.Empty)
                    txtValorJ.Valor = IIf(.Item("cuca_valor_joven").ToString.Length > 0, .Item("cuca_valor_joven"), String.Empty)
                    txtValorDAJ.Valor = IIf(.Item("cuca_deau_valor_joven").ToString.Length > 0, .Item("cuca_deau_valor_joven"), String.Empty)
                    txtDC.Valor = IIf(.Item("_cuca_deve_anio").ToString.Length > 0, .Item("_cuca_deve_anio"), String.Empty)
                    txtTP.Valor = IIf(.Item("_DeveTipoPerio").ToString.Length > 0, .Item("_DeveTipoPerio"), String.Empty)
                    txtUC.Valor = IIf(.Item("_cuca_deve_perio").ToString.Length > 0, .Item("_cuca_deve_perio"), String.Empty)
                    cmbInst.Valor = IIf(.Item("cuca_inst_id").ToString.Length > 0, .Item("cuca_inst_id"), String.Empty)
                    cmbPeri.Valor = IIf(.Item("cuca_peti_id").ToString.Length > 0, .Item("cuca_peti_id"), String.Empty)
                    mSelecCate()
                End With

                mSetearEditor(False)
                mMostrarPanel(True)
            End If
        End Sub

   Private Sub mAgregar()
      Try
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()
        hdnId.Text = ""
        txtFechaIni.Text = ""
        txtValor.Text = ""
        txtValorDA.Text = ""
        txtValorJ.Text = ""
        txtValorDAJ.Text = ""
        txtUC.Valor = ""
        txtTP.Valor = ""
        txtDC.Valor = ""
        cmbCate.Limpiar()
        cmbInst.Limpiar()
        cmbPeri.Limpiar()
        lblTitu.Text = ""

        mCrearDataSet("")
        mSetearEditor(True)

    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)

        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        panDato.Visible = pbooVisi
        'panFiltros.Visible = Not panDato.Visible
        'grdDato.Visible = Not panDato.Visible
    End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjCuotas As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCuotas.Alta()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mModi()
      Try
         If mNo_BajaModi() Then
            Throw New AccesoBD.clsErrNeg("No puede modificar el registro.")
         End If

            mGuardarDatos()

            Dim lobjCuotas As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjCuotas.Modi()

            mConsultar()

            mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Function mNo_BajaModi() As Boolean
      If Not txtUC.Valor Is DBNull.Value Or Not txtDC.Valor Is DBNull.Value Or Not txtTP.Valor Is DBNull.Value Then
         Return True
      Else
         Return False
      End If
   End Function
   Private Sub mBaja()
      Try
         If mNo_BajaModi() Then
            Throw New AccesoBD.clsErrNeg("No puede dar de baja el registro.")
         End If

         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCuotas As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCuotas.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      With mdsDatos.Tables(0).Rows(0)
            .Item("cuca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("cuca_cate_id") = IIf(cmbCate.Valor.Trim.Length > 0, cmbCate.Valor, DBNull.Value)
                .Item("cuca_fecha") = IIf(txtFechaIni.Fecha.Trim.Length > 0, txtFechaIni.Fecha, DBNull.Value)
                .Item("cuca_valor") = IIf(txtValor.Valor.Trim.Length > 0, txtValor.Valor, DBNull.Value)
                .Item("cuca_deau_valor") = IIf(txtValorDA.Valor.Trim.Length > 0, txtValorDA.Valor, DBNull.Value)
                .Item("cuca_valor_joven") = IIf(txtValorJ.Valor.Trim.Length > 0, txtValorJ.Valor, DBNull.Value)
                .Item("cuca_deau_valor_joven") = IIf(txtValorDAJ.Valor.Trim.Length > 0, txtValorDAJ.Valor, DBNull.Value)
                .Item("cuca_inst_id") = IIf(cmbInst.Valor.Trim.Length > 0, cmbInst.Valor, DBNull.Value)
                .Item("cuca_peti_id") = IIf(cmbPeri.Valor.Trim.Length > 0, cmbPeri.Valor, DBNull.Value)
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      Session(mstrTabla) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "CuotasCatego"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&cate_id=" + cmbCateFil.Valor.ToString
            lstrRpt += "&inst_id=" + IIf(cmbInstFil.Valor.ToString = "N", "0", cmbInstFil.Valor.ToString)
            lstrRpt += "&inst_ninguna=" + IIf(cmbInstFil.Valor.ToString = "N", "1", "0")

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        Try
            If cmbCateFil.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar una categor�a.")
            Else
                mConsultar()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

    Private Sub btnHisto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHisto.Click

        Try
            If cmbCate.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la categor�a.")
            End If


            Dim lstrPagina As String = "MovimCuotasCatego_Pop.aspx?mcuc_cate_id=" + cmbCate.Valor.ToString & "&categoria=" & cmbCate.SelectedItem.Text
            clsWeb.gGenerarPopUp(Me, lstrPagina, 650, 400, 10, 75)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region
End Class

End Namespace
