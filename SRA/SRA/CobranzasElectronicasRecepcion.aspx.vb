Imports Business.Facturacion.CobranzasBusiness
Imports System.Collections
Imports Entities


Namespace SRA


Partial Class CobranzasElectronicasRecepcion
    Inherits FormGenerico
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCabeTotalApl As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalAplic As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalPago As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalDif As System.Web.UI.WebControls.Label
    Protected WithEvents lblCabeTotalDifer As System.Web.UI.WebControls.Label


    'Private mstrConn As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private titulo As String = "Recepción de Cobranzas Electrónicas ("

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim mstrConn As String = clsWeb.gVerificarConexion(Me)

        If (Not Page.IsPostBack) Then
            ' Blanqueo las variables de session
            Me.Session.Remove("GetCobranzasARecepcionar")
            Me.Session.Remove("GetEncabezadoCobranzasARecepcionar")
            ' variable de session que controla la doble operacion sobre el 
            ' boton confirmar
            Me.Session.Add("CobranzasElectronicaRecepcion_btnConfirmar", False)
            ' carga combo
            Me.CargarCombo()
            ' se consulta para cargar la grilla
            'mConsultar()
        End If
    End Sub
    ' Dario 2013-05-31 Metodo que se ejecuta en el cambio de pagina(metodo de paginacion de la grilla)
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    ' Dario 2013-05-31 Metodo que carga la grilla 
    Private Sub mConsultar()
        Try
            Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
            ' ejecuto el metodo que carga el encabezado de fecha mas antigua para la empresa
            ' de cobranzas selecionada en el combo
            Dim objEncabezado As CobranzasCabEntity = _CobranzasBusiness.GetEncabezadoCobranzasARecepcionar(Convert.ToInt32(Me.cmbEmpresasCobradoras.SelectedValue))
            '
            Me.Session.Add("GetEncabezadoCobranzasARecepcionar", objEncabezado)
            ' ejecuto el metodo que recupera los datos a mostar en la grilla
            Dim ds As DataSet = _CobranzasBusiness.GetCobranzasARecepcionar(Me.User.Identity.Name _
                                                                           , objEncabezado.intIdInterCobElecCab)
            ' ejecuto metodo que arma el resumen
            Me.getDatosResumen(ds, objEncabezado)
            '
            Me.Session.Add("GetCobranzasARecepcionar", ds)
            '
            ' cargo el resultado de la consulta en la grilla 
            grdDato.DataSource = ds
            grdDato.DataBind()
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    ' Dario 2013-06-17 Metodoq ue carga el combo de empresa cobradora
    Private Sub CargarCombo()
        Try
            Dim bitEjecutaConsulta As Boolean = False
            Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
            ' ejecuto el metodo que recupera los datos a mostar en la grilla
            Dim ds As DataSet = _CobranzasBusiness.GetEmpresasCobranzasElectronicas(Me.User.Identity.Name)
            ' controlo que tenga algo la consulta de empresas
            If (Not ds Is Nothing And ds.Tables.Count > 0) Then
                ' si tiene 0 o mas de uno agrega el seleccione sino deja solo el unico que tiene
                If (ds.Tables(0).Rows.Count = 0 Or ds.Tables(0).Rows.Count > 1) Then
                    cmbEmpresasCobradoras.Items.Add(New System.Web.UI.WebControls.ListItem("(Seleccione)", "-1"))
                Else
                    bitEjecutaConsulta = True
                End If
                ' recorre el resultado de la consulta y lo carga en el combo
                For Each dr As DataRow In ds.Tables(0).Rows
                    ' agrego el item 
                    cmbEmpresasCobradoras.Items.Add( _
                                                    New System.Web.UI.WebControls.ListItem(dr("varDesEmpresaCobElect").ToString() _
                                                                                          , dr("intIdEmprezasCobranzasElectronicas").ToString()))
                Next
            End If
            ' si solo tiene un item el combo llamo a la consulta para cargar la grilla
            If (bitEjecutaConsulta) Then
                Me.mConsultar()
            End If

            ds.Dispose()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    'Dario 2013-05-31 metodo que hace el calculo del resumen de la pagina
    Private Sub getDatosResumen(ByVal oDs As DataSet, ByVal objEncabezado As CobranzasCabEntity)
        ' seteo el titulo de acuerdo a la empresa seleccionada en el combo
        Me.lblTitu.Text = Me.titulo & objEncabezado.varDesEmpresaCobElect & ")"
        ' obtengo por calculo desde el ds el monto total recaudacion bruta
        Dim recaudacionBrutaCalculada As Decimal = Me.getMontoNovedadesRecibidas(oDs)
        ' variable de cotador de novedades
        Dim CantTotalNovedades As Int32 = 0
        ' cargo los controles de la pagina con los datos del objeto encabezado
        Me.lblCantTotalRegistrosDato.Text = objEncabezado.intCantidadDeNovedades.ToString()
        Me.lblCantTotalPago.Text = ("$ " & objEncabezado.decRecaudacion.ToString("#,###,##0.00")) '.Replace(",", "*").Replace(".", ",").Replace("*", ".")
        Me.lblFAcredDato.Text = objEncabezado.dateFechaAcreditacion.ToString("dd/MM/yyyy")
        Me.txtNroComprobante.Text = objEncabezado.varNroComprobante
        Me.txtRecaudacionNeta.Text = objEncabezado.decRecaudacionNeta.ToString("#,###,##0.00") '.Replace(",", "*").Replace(".", ",").Replace("*", ".")
        ' si hay registros seteo mi variable con el valor del count de los rows de la tabla
        If (Not oDs Is Nothing And oDs.Tables.Count > 0) Then
            CantTotalNovedades = oDs.Tables(0).Rows.Count
        End If
        ' variable de mensaje de diferencias
        Dim msg As String = String.Empty
        If (CantTotalNovedades <> objEncabezado.intCantidadDeNovedades) Then
            msg = msg & "La cantidad de novedades informadas en el archivo,\r\nno se corresponde con la cantidad de novedades recuparadas en la consulta.\r\n"
        End If
        If (recaudacionBrutaCalculada <> objEncabezado.decRecaudacion) Then
            msg = msg & "La recaudación informada en el archivo,\r\nno se corresponde con la suma de los importes de las novedades recuparadas en la consulta.\r\n"
        End If
        ' si msg tiene mensajes muestro alerta
        If (msg.Length > 0) Then
            Response.Write("<script language='javascript'>window.alert('" & msg & "');</script>")
        End If
        '
    End Sub

    ' Dario 2013-05-31 Metodo que hace el calculo del importe total 
    '                  de las novedades a ingresar
    Private Function getMontoNovedadesRecibidas(ByVal oDs As DataSet) As Decimal
        ' Seteo variable de respuesta
        Dim respuesta As Decimal = 0
        If (Not oDs Is Nothing And oDs.Tables.Count > 0) Then
            ' recorro la grilla y acumulo el importe de cada item
            For Each dr As DataRow In oDs.Tables(0).Rows
                respuesta = respuesta + System.Convert.ToDecimal(dr("Importe"))
            Next
        End If
        ' retornamos la respuesta del importe totalizado
        Return respuesta
    End Function

    ' Dario 2013-05-31 Evento del boton Confirmar
    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        ' Verifico que la variable de sesion este <> true para proseguir con la operacion 
        ' sino se oprimio mas de una vez el boton confirmar
        If (Me.Session("CobranzasElectronicaRecepcion_btnConfirmar") <> True) Then
            ' verifico que los datos obligatorios esten completos
            If (Me.txtNroComprobante.Text.Trim.Length > 0 And Me.txtRecaudacionNeta.Text.Trim.Length > 0 _
                And Convert.ToDecimal("0" & Me.txtRecaudacionNeta.Text.Trim) > 0) Then
                ' Ingreso ponemos la variable de session en true
                Me.Session.Add("CobranzasElectronicaRecepcion_btnConfirmar", True)
                ' recupero la coleccion de la session de los datos cargados en la grilla
                Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
                Dim listaCoVtIds As New ArrayList
                ' 
                Dim ds As DataSet = Me.Session("GetCobranzasARecepcionar")
                ' obtengo el objeto encabezado de mis cobranza que confirme y completo con los
                ' datos ingresados por el usuario
                Dim objEncabezado As CobranzasCabEntity = Me.Session("GetEncabezadoCobranzasARecepcionar")
                ' 
                objEncabezado.varNroComprobante = Me.txtNroComprobante.Text
                objEncabezado.decRecaudacionNeta = Convert.ToDecimal(Me.txtRecaudacionNeta.Text)
                ' controlo que el neto ingresado sea mejor o igual que el bruto
                If (objEncabezado.decRecaudacion >= objEncabezado.decRecaudacionNeta) Then
                        ' obtengo en nombre de la pc para dejar registro
                        Dim lstrHost As String = Net.Dns.GetHostByAddress(Request.ServerVariables("REMOTE_ADDR")).HostName
                        ' ejecuta el metodo que registra las cobranzas 
                        ' ingresadas de pagos mis cuentas
                        Dim respuesta As String = _CobranzasBusiness.RecepcionarCobranzasElectronicas(ds, objEncabezado, Me.Session("sUserId"), lstrHost)
                    'si hubo errores la cadena de respuesta tiene length mayor a 0
                    If (respuesta.Length > 0) Then
                        Response.Write("<script language='javascript'>window.alert('" & respuesta & "');</script>")
                    Else
                        ' menasje de exito, glorioso.
                        Response.Write("<script language='javascript'>window.alert('La operación de recepción termino correctamente!!');</script>")
                    End If
                    ' Cuando termina se vuelve a dejar la variable de session en false
                    Me.Session.Add("CobranzasElectronicaRecepcion_btnConfirmar", False)
                    ' elimino la coleccion de pagos luego de completar la operacion
                    ' de recepciond e cobranzas
                    Me.Session.Remove("GetCobranzasARecepcionar")
                    Me.Session.Remove("GetEncabezadoCobranzasARecepcionar")
                    'seteo la pagina en la primera y recargo la grilla por si hay errores en la operacion asi los muestra
                    grdDato.CurrentPageIndex = 0
                    Me.mConsultar()
                Else
                    ' error se vuelve a dejar la variable de session en false
                    Me.Session.Add("CobranzasElectronicaRecepcion_btnConfirmar", False)
                    Response.Write("<script language='javascript'>window.alert('ERROR: El importe recaudado neto es mayor que el bruto!!');</script>")
                End If
            Else
                Response.Write("<script language='javascript'>window.alert('El nro de comprobante y la recaudación neta son obligatorios!!');</script>")
            End If

        End If
    End Sub

    ' Metodo que se dispara en el cambio de item del combo empresas
    Public Sub cmbEmpresasCobradoras_OnSelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEmpresasCobradoras.SelectedIndexChanged
        ' ejecuto la consulta de novedades para cargar la grilla
        Me.mConsultar()
    End Sub

End Class

End Namespace
