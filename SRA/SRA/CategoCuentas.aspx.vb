Namespace SRA

Partial Class CategoCuentas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

 End Sub
   Protected WithEvents PanDetalle As System.Web.UI.WebControls.Panel

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "catego_cuentas"

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private ds As New DataSet
   Private mstrConn As String
   Private mstrItemCons As String



   Private Enum Columnas As Integer
      Id = 1
   End Enum


#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()


         If (Not Page.IsPostBack) Then

            mSetearEventos()
            mEstablecerPerfil()
            mCargarDSCtaCatego("cuentas_ctables")
            mCargarCombos()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)

         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
               mSetearCCos()
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.catego_cuentas, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.catego_cuentas_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.catego_cuentas_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.catego_cuentas_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

        Private Sub mSetearCCos()

            Dim lbooHabi As Boolean
            Dim lstrRet As String

            If Not cmbCta07.Valor Is DBNull.Value Then
                'lstrRet = clsSQLServer.gObtenerEstruc(mstrConn, "ctas_ctables", cmbCta07.Valor).Tables(0).Rows(0).Item("cuct_codi").ToString.Substring(0, 1)
                'lbooHabi = lstrRet.Substring(0, 1) = "4" Or lstrRet.Substring(0, 1) = "5"

                'GM 21/05/2021 
                lstrRet = clsSQLServer.gObtenerEstruc(mstrConn, "ctas_ctables", cmbCta07.Valor).Tables(0).Rows(0).Item("cuct_codi").ToString
                If lstrRet.Length > 0 Then
                    lstrRet = lstrRet.Substring(0, 1)
                    lbooHabi = lstrRet.Substring(0, 1) = "4" Or lstrRet.Substring(0, 1) = "5"
                End If

            End If

            If lbooHabi Then
                cmbCcos07.Enabled = True
                If Not Request.Form(cmbCcos07.ClientID) Is Nothing Then
                    cmbCcos07.Valor = Request.Form(cmbCcos07.ClientID).ToString
                End If
            Else
                cmbCcos07.Enabled = False
                cmbCcos07.Limpiar()
            End If
        End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


#End Region

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub



   Private Sub mVerInstituciones()
      With mdsDatos.Tables(0).Rows(0)
         If (cmbCateAux.Items(cmbCate.SelectedIndex()).Text = "S") Then
            If Not .IsNull("cacu_inst_id") Then
               cmbInst.Valor = .Item("cacu_inst_id")
            End If
         Else
            cmbInst.Enabled = False
            cmbInst.SelectedIndex = 0
         End If
      End With
   End Sub

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

            Try
                mLimpiar()

                hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

                mCrearDataSet(hdnId.Text)

                If mdsDatos.Tables(0).Rows.Count > 0 Then
                    With mdsDatos.Tables(0).Rows(0)
                        cmbCate.Valor = .Item("cacu_cate_id")
                        mVerInstituciones()
                        cmbCta01.Valor = IIf(.Item("cacu_soli_cuct_id").ToString.Trim.Length > 0, .Item("cacu_soli_cuct_id"), String.Empty)
                        cmbcta02.Valor = IIf(.Item("cacu_acta_cuct_id").ToString.Trim > 0, .Item("cacu_acta_cuct_id"), String.Empty)
                        cmbCta03.Valor = IIf(.Item("cacu_bime_cuct_id").ToString.Trim > 0, .Item("cacu_bime_cuct_id"), String.Empty)
                        cmbCta04.Valor = IIf(.Item("cacu_venc_cuct_id").ToString.Trim.Length > 0, .Item("cacu_venc_cuct_id"), String.Empty)
                        cmbCta05.Valor = IIf(.Item("cacu_adel_cuct_id").ToString.Trim.Length > 0, .Item("cacu_adel_cuct_id"), String.Empty)
                        cmbCta07.Valor = IIf(.Item("cacu_cuot_cuct_id").ToString.Trim.Length > 0, .Item("cacu_cuot_cuct_id"), String.Empty)
                        cmbCcos07.Valor = IIf(.Item("cacu_cuot_ccos_id").ToString.Trim.Length > 0, .Item("cacu_cuot_ccos_id"), String.Empty)
                        cmbRCta.Valor = IIf(.Item("cacu_reva_cuct_id").ToString.Trim.Length > 0, .Item("cacu_reva_cuct_id"), String.Empty)
                        cmbCategoria.Valor = IIf(.Item("cacu_camb_cuct_id").ToString.Trim.Length > 0, .Item("cacu_camb_cuct_id"), String.Empty)
                        mSetearCCos()
                        mSelecCate()
                    End With

                    mSetearEditor("", False)
                    mMostrarPanel(True)
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub



   Private Sub mAgregar()
      Try
         mLimpiar()
         btnBaja.Enabled = False
         btnModi.Enabled = False
         btnAlta.Enabled = True
         mMostrarPanel(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""

      mCrearDataSet("")


      cmbCate.Limpiar()
      cmbInst.Limpiar()
      cmbCta01.Limpiar()
      cmbCta02.Limpiar()
      cmbCta03.Limpiar()
      cmbCta04.Limpiar()
      cmbCta05.Limpiar()
        cmbCta07.Limpiar()
        cmbRCta.Limpiar()
        cmbCategoria.Limpiar()
      mSetearCCos()

      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      PanDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = Not (PanDato.Visible)
      PanDato.Visible = pbooVisi
      grdDato.Visible = Not PanDato.Visible
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos(True)
         Dim lobjDistritos As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjDistritos.Alta()
         mConsultar()


         mMostrarPanel(False)


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mGuardarDatos(False)
         Dim lobjIncripciones As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjIncripciones.Modi()

         mConsultar()

         mMostrarPanel(False)



      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjIncripciones As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjIncripciones.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0


         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidaDatos(ByVal pboolAlta As Boolean)


      With mdsDatos.Tables(0).Rows(0)
         If cmbCate.SelectedItem.Text = "(Seleccione)" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una categor�a.")
         End If
         If cmbCta01.SelectedItem.Text = "(Seleccione)" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una cuenta para solicitud de ingreso.")
         End If

         If cmbCta02.SelectedItem.Text = "(Seleccione)" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una cuenta para pagos a cuenta.")
         End If
         If cmbCta03.SelectedItem.Text = "(Seleccione)" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una cuenta para bimestre en curso.")
         End If
         If cmbCta04.SelectedItem.Text = "(Seleccione)" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una cuenta para cuota vencida.")
         End If
         If cmbCta05.SelectedItem.Text = "(Seleccione)" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar una cuenta para cuota adelantada.")
         End If
         If cmbCta07.SelectedItem.Text = "(Seleccione)" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una cuenta para cuotas sociales.")
         End If
         If cmbCCos07.Enabled And cmbCCos07.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un centro de costo para cuotas sociales.")
            End If
            'If cmbRCta.Enabled And cmbRCta.Valor Is DBNull.Value Then
            '    Throw New AccesoBD.clsErrNeg("Debe seleccionar una opci�n para revaluo coutas.")
            'End If
        End With


   End Sub

        Private Function mGuardarDatos(ByVal pboolAlta As Boolean) As DataSet
            mValidaDatos(pboolAlta)
            With mdsDatos.Tables(0).Rows(0)
                .Item("cacu_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("cacu_cate_id") = IIf(cmbCate.Valor.Trim.Length > 0, cmbCate.Valor, DBNull.Value)
                .Item("cacu_inst_id") = IIf(cmbInst.Valor.Trim.Length > 0, cmbInst.Valor, DBNull.Value)
                .Item("cacu_soli_cuct_id") = IIf(cmbCta01.Valor.Trim.Length > 0, cmbCta01.Valor, DBNull.Value)
                .Item("cacu_acta_cuct_id") = IIf(cmbcta02.Valor.Trim.Length > 0, cmbcta02.Valor, DBNull.Value)
                .Item("cacu_bime_cuct_id") = IIf(cmbCta03.Valor.Trim.Length > 0, cmbCta03.Valor, DBNull.Value)
                .Item("cacu_venc_cuct_id") = IIf(cmbCta04.Valor.Trim.Length > 0, cmbCta04.Valor, DBNull.Value)
                .Item("cacu_reva_cuct_id") = IIf(cmbRCta.Valor.Trim.Length > 0, cmbRCta.Valor, DBNull.Value)
                .Item("cacu_camb_cuct_id") = IIf(cmbCategoria.Valor.Trim.Length > 0, cmbCategoria.Valor, DBNull.Value)
                .Item("cacu_adel_cuct_id") = IIf(cmbCta05.Valor.Trim.Length > 0, cmbCta05.Valor, DBNull.Value)
                .Item("cacu_cuot_cuct_id") = IIf(cmbCta07.Valor.Trim.Length > 0, cmbCta07.Valor, DBNull.Value)
                .Item("cacu_cuot_ccos_id") = IIf(cmbCcos07.Valor.Trim.Length > 0, cmbCcos07.Valor, DBNull.Value)
            End With
            Return mdsDatos
        End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla



      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      Session(mstrTabla) = mdsDatos

   End Sub



#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
#End Region

#Region "Detalle"

   Private Function mCargarDSCtaCatego(ByVal pstrTable As String) As DataSet
      Try

         Dim mstrCmd As String = "exec " + pstrTable + "_cargar"

         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
         Return ds

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Function

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S", "@cate_paga=1")
      clsWeb.gCargarRefeCmb(mstrConn, "categorias_aux", cmbCateAux, "S", "@cate_paga=1")
      clsWeb.gCargarRefeCmb(mstrConn, "instituciones", cmbInst, "S")
      Dim combos As New System.Collections.ArrayList
      With combos
         .Add(cmbCta01)
         .Add(cmbCta02)
         .Add(cmbCta03)
         .Add(cmbCta04)
         .Add(cmbCta05)
            .Add(cmbCta07)
            .Add(cmbRCta)
            .Add(cmbCategoria)
      End With
      'clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", combos, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", combos, "S", "null,null,'1,2,4,5'")
      clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCCos07, "S")
   End Sub

   Private Sub mSelecCate()
      cmbCateAux.Valor = cmbCate.Valor
      If cmbCateAux.SelectedItem.Text = "S" Then
         cmbInst.Enabled = True
      Else
         cmbInst.Enabled = False
         cmbInst.SelectedIndex = 0
      End If
   End Sub
   Public Sub mConsultar()
      Try

         mstrCmd = "exec " + mstrTabla + "_consul"
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "CuentaCatego"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region
End Class
End Namespace
