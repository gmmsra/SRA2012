Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Configuration


Namespace SRA


Partial Class perfiles

    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Public mstrTabla As String
   Public mstrParaPageSize As String
   Public mstrPKey As String
   Public mstrCmd As String
   Public mintId As Integer
   Public mintDesc As Integer
   Public mintCols As Integer
   Private mstrConn As String


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()

      mintId = 1
      mintDesc = 2
      mintCols = 3
      mstrTabla = "perfiles"
      mstrPKey = "perf_id"
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mConsultar()
            mSetearMaxLength()
            clsWeb.gCargarRefeCmb(mstrConn, "perfiles", cmbPerf, "perf_id")
            mSetearEventos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      chkCopy.Attributes.Add("onkeydown", "mEnterPorTab();")
   End Sub

   Private Sub mEstablecerPerfil()
      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Perfiles, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If
      btnAlta.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      btnLimp.Visible = (btnAlta.Visible Or btnBaja.Visible Or btnModi.Visible)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pref_desc")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal e As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = e.NewPageIndex
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()

      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub


#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)

      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)

      btnAlta.Enabled = pbooAlta
      chkCopy.Visible = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal e As DataGridCommandEventArgs)

      txtId.Text = clsFormatear.gFormatCadena(e.Item.Cells(mintId).Text)
      txtDesc.Text = clsFormatear.gFormatCadena(e.Item.Cells(mintDesc).Text)
      chkCopy.Checked = False
      cmbPerf.Visible = False
      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub
   Private Sub mAgregar()

      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()

      txtDesc.Text = ""
      chkCopy.Checked = False
      cmbPerf.Visible = True
      mSetearEditor(True)
   End Sub
   Private Sub mCerrar()

      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If pbooVisi Then
         hdnPage.Text = " "

      Else

         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      mEstablecerPerfil()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()

      Try

         mstrCmd = "exec " + mstrTabla + "_alta "
         mstrCmd = mstrCmd + " " + clsSQLServer.gFormatArg(txtDesc.Text, SqlDbType.VarChar)
         If (chkCopy.Checked) Then

            mstrCmd = mstrCmd + "," + clsSQLServer.gFormatArg("S", SqlDbType.VarChar)

         Else

            mstrCmd = mstrCmd + "," + clsSQLServer.gFormatArg("N", SqlDbType.VarChar)
         End If
         mstrCmd = mstrCmd + "," + clsSQLServer.gFormatArg(clsWeb.gValorCombo(cmbPerf), SqlDbType.Int)

         Dim lintExec As Integer = clsSQLServer.gExecute(mstrConn, mstrCmd)

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()

      Try

         mstrCmd = "exec " + mstrTabla + "_modi "
         mstrCmd = mstrCmd + " " + clsSQLServer.gFormatArg(txtId.Text, SqlDbType.Int)
         mstrCmd = mstrCmd + "," + clsSQLServer.gFormatArg(txtDesc.Text, SqlDbType.VarChar)

         Dim lintExec As Integer = clsSQLServer.gExecute(mstrConn, mstrCmd)

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()

      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         mstrCmd = "exec " + mstrTabla + "_baja "
         mstrCmd = mstrCmd + " " + clsSQLServer.gFormatArg(txtId.Text, SqlDbType.Int)

         Dim lintExec As Integer = clsSQLServer.gExecute(mstrConn, mstrCmd)

         grdDato.EditItemIndex = -1
         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then

            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

  
   Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
End Class
End Namespace
