Imports System.Data.SqlClient
Imports System.Text
Imports System.Globalization


Namespace SRA


Partial Class PlanDePagos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

 End Sub
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton
   Protected WithEvents hdnMailId As System.Web.UI.WebControls.TextBox
   Protected WithEvents hdnDireId As System.Web.UI.WebControls.TextBox
   Protected WithEvents Label2 As System.Web.UI.WebControls.Label
   Protected WithEvents Label3 As System.Web.UI.WebControls.Label
   Protected WithEvents panBotones As System.Web.UI.WebControls.Panel

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_PagosPlan
   Private mstrTablaInstitutos As String = SRA_Neg.Constantes.gTab_Institutos
   Private mstrPagosCuotas As String = SRA_Neg.Constantes.gTab_PagosCuotas
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String
   Private mintinsti As Int32

   Private mintDeauImpo As Double
   Private mintSociImpo As Double
   Private mint1SociImpo As Double
   Private mint2SociImpo As Double

   Private mintMatrConc As String
   Private mintCuotConc As String
   Private mintCcos As String

   Private Enum Columnas As Integer
      PlanId = 1
      PlanDesc = 2
   End Enum
   Private Enum ColumnasDeta As Integer
      CuotaId = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mConsultar(grdDato, False)
            tabLinks.Visible = False
            clsWeb.gInicializarControles(Me, mstrConn)
            mCargarCombos()
         Else
            mCargarCentroCostos(False)
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbMatrConc, "S", "@conc_grava=0")
      cmbMatrConc.Valor = mintMatrConc
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbCuotConc, "S", "@conc_grava=0")
      cmbCuotConc.Valor = mintCuotConc
        '24/02/2011 - AG: Se descomentaron las 2 lineas de codigo siguientes, para cargar combo de ccosto.
        clsWeb.gCargarRefeCmb(mstrConn, "centrosc_cuenta", cmbCcos, "S", "@cuenta=4")
        cmbCcos.Valor = mintCcos
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaCuota.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Profesores, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrPagosPlanLong As Object
      Dim lstrPagosCuotasLong As Object

      lstrPagosPlanLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrPagosPlanLong, "papl_desc")
      txtMonto.MaxLength = clsSQLServer.gObtenerLongitud(lstrPagosPlanLong, "papl_matr_monto")


      lstrPagosCuotasLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrPagosCuotas)
      txtImp.MaxLength = clsSQLServer.gObtenerLongitud(lstrPagosCuotasLong, "plco_impo")
      txtDeauImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrPagosCuotasLong, "plco_deau_impo")
      txtSociImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrPagosCuotasLong, "plco_soci_impo")
      txt1SociImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrPagosCuotasLong, "plco_1soci_impo")
      txt2SociImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrPagosCuotasLong, "plco_2soci_impo")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintinsti = Convert.ToInt32(Request.QueryString("fkvalor"))

      Dim lstrCmd As New StringBuilder
      Dim ldsEsta As New DataSet

      lstrCmd.Append("exec " + mstrTablaInstitutos + "_consul ")
      lstrCmd.Append(mintinsti.ToString)
      ldsEsta = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      With ldsEsta.Tables(0).Rows(0)
         mintMatrConc = .Item("inse_matr_conc_id").ToString
         mintCuotConc = .Item("inse_cuot_conc_id").ToString
         mintCcos = .Item("inse_ccos_id").ToString
         mintDeauImpo = .Item("inse_deau_porc")
         mintSociImpo = .Item("inse_soci_porc")
         mint1SociImpo = .Item("inse_1soci_porc")
         mint2SociImpo = .Item("inse_2soci_porc")
      End With
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(grdDato, True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrPagosCuotas
            btnBajaCuota.Enabled = Not (pbooAlta)
            btnModiCuota.Enabled = Not (pbooAlta)
            btnAltaCuota.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
      Dim lstrCta1 As String = ""
      Dim lstrCta2 As String = ""
      If Not cmbMatrConc.Valor Is DBNull.Value and Not cmbCuotConc.Valor Is DBNull.Value  Then
        lstrCta1 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbMatrConc.Valor)
        lstrCta1 = lstrCta1.Substring(0, 1)
        lstrCta2 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbCuotConc.Valor)
        lstrCta2 = lstrCta2.Substring(0, 1)
      End If
      If lstrCta1 <> "" And lstrCta1 <> "1" And lstrCta1 <> "2" And lstrCta1 <> "3" And lstrCta2 <> "" And lstrCta2 <> "1" And lstrCta2 <> "2" And lstrCta2 <> "3" And lstrCta1 = lstrCta2 Then
         If Not Request.Form("cmbCCos") Is Nothing Or pbooEdit Then
            clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta1, cmbCcos, "id", "descrip", "N")
            If Not Request.Form("cmbCCos") Is Nothing Then
                cmbCcos.Valor = Request.Form("cmbCCos").ToString
            'Else
                'cmbCcos.Valor = mintCcos
            End If
         End If
      Else
         cmbCcos.Items.Clear()
      End If
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mLimpiar()

         hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.PlanId).Text)

         mCrearDataSet(hdnId.Text)

         grdCuota.CurrentPageIndex = 0

         If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
               txtDesc.Valor = .Item("papl_desc")
                        txtMonto.Valor = IIf(.Item("papl_matr_monto").ToString.Length > 0, .Item("papl_matr_monto"), "")
                        lblCtasAuto.Text = .Item("papl_cuot_cant")
               cmbMatrConc.Valor = .Item("papl_matr_conc_id")
               cmbCuotConc.Valor = .Item("papl_cuot_conc_id")
               mCargarCentroCostos(True)
               cmbCcos.Valor = .Item("papl_ccos_id")

                        'If txtMonto.Valor Is DBNull.Value Then
                        If txtMonto.Text.Trim().Length = 0 Then
                            chkMatri.Checked = False
                            txtMonto.Enabled = False
                        Else
                            chkMatri.Checked = True
                        End If

                        If Not .IsNull("papl_baja_fecha") Then
                            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("papl_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                        Else
                            lblBaja.Text = ""
                        End If

                    End With

            mSetearEditor("", False)
            mSetearEditor(mstrPagosCuotas, True)
            mMostrarPanel(True)
            mShowTabs(1)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosCuota(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Dim ldrCuota As DataRow

      hdnCuotaId.Text = E.Item.Cells(1).Text
      ldrCuota = mdsDatos.Tables(mstrPagosCuotas).Select("plco_id=" & hdnCuotaId.Text)(0)

      With ldrCuota
         txtImp.Valor = .Item("plco_impo")
         txtDeauImpo.Valor = .Item("plco_deau_impo")
         txtSociImpo.Valor = .Item("plco_soci_impo")
         txt1SociImpo.Valor = .Item("plco_1soci_impo")
         txt2SociImpo.Valor = .Item("plco_2soci_impo")
         txtFCta.Fecha = .Item("plco_fecha")

      End With
      mSetearEditor(mstrPagosCuotas, False)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      txtDesc.Text = ""
      txtMonto.Text = ""
      lblCtasAuto.Text = ""
      lblBaja.Text = ""
      chkMatri.Checked = True

      cmbMatrConc.Valor = mintMatrConc
      cmbCuotConc.Valor = mintCuotConc
      mCargarCentroCostos(False)
      cmbCcos.Valor = mintCcos

      lblDeauImpo.Text = "Dto.D.A.( " & mintDeauImpo.ToString & "% ) $:"
      lblSociImpo.Text = "Dto.Activo( " & mintSociImpo.ToString & "% ) $:"
      lbl1SociImpo.Text = "Dto.Adherente( " & mint1SociImpo.ToString & "% ) $:"
      lbl2SociImpo.Text = "Dto.Menor( " & mint2SociImpo.ToString & "% ) $:"
      txtDeauImpo.Text = ""
      txtSociImpo.Text = ""
      txt1SociImpo.Text = ""
      txt2SociImpo.Text = ""
      hdnDeauImpo.Text = mintDeauImpo
      hdnSociImpo.Text = mintSociImpo
      hdn1SociImpo.Text = mint1SociImpo
      hdn2SociImpo.Text = mint2SociImpo

      mLimpiarCuotas(False)

      mCrearDataSet("")

      lblTitu.Text = ""

      grdCuota.CurrentPageIndex = 0

      mSetearEditor("", True)

      mShowTabs(1)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panBoto.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)

      lnkCabecera.Font.Bold = True


      panDato.Visible = pbooVisi
      'grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True


      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True

   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      PanCuota.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkCuota.Font.Bold = False


      Select Case origen
         Case 1
            'Datos plan
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
         Case 2
            'cuotas
            PanCuota.Visible = True
            lnkCuota.Font.Bold = True

      End Select
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Alta()
         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Modi()

         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar(grdDato, True)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Private Function mGuardarDatos() As DataSet

            Dim lintCpo As Integer

            If txtDesc.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la descripci�n del plan de pago.")
            End If

            If chkMatri.Checked Then
                If Me.txtMonto.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el monto de la matr�cula.")
                End If
                If cmbMatrConc.Valor Is DBNull.Value Or cmbMatrConc.Valor.ToString.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el concepto de la matr�cula.")
                End If

                If cmbCcos.Valor Is DBNull.Value Or cmbCcos.Valor.ToString.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el centro de costo.")
                End If
            Else
                Me.txtMonto.Text = ""
                Me.txtMonto.Enabled = False
            End If


            With mdsDatos.Tables(0).Rows(0)
                .Item("papl_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("papl_desc") = txtDesc.Valor
                .Item("papl_baja_fecha") = DBNull.Value
                .Item("papl_matr_monto") = If(txtMonto.Text.Trim().Length = 0, DBNull.Value, txtMonto.Valor)
                .Item("papl_matr_conc_id") = cmbMatrConc.Valor
                .Item("papl_cuot_conc_id") = cmbCuotConc.Valor
                .Item("papl_ccos_id") = cmbCcos.Valor
                .Item("papl_inse_id") = mintinsti
                If lblCtasAuto.Text <> "" Then .Item("papl_cuot_cant") = clsSQLServer.gFormatArg(lblCtasAuto.Text, SqlDbType.Int)
            End With

            Return mdsDatos
        End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrPagosCuotas


      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      grdCuota.DataSource = mdsDatos.Tables(mstrPagosCuotas)
      grdCuota.DataBind()



      Session(mstrTabla) = mdsDatos

   End Sub
#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

#Region "Detalle"
   Public Sub grdCuota_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCuota.EditItemIndex = -1
         If (grdCuota.CurrentPageIndex < 0 Or grdCuota.CurrentPageIndex >= grdCuota.PageCount) Then
            grdCuota.CurrentPageIndex = 0
         Else
            grdCuota.CurrentPageIndex = E.NewPageIndex
         End If
         grdCuota.DataSource = mdsDatos.Tables(mstrPagosCuotas)
         grdCuota.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal pOk As Boolean)
      Try
         Dim strFin As String = ""
         If Not pOk Then
            strFin = ",@ejecuta = N "
         End If
         mstrCmd = "exec " + mstrTabla + "_consul " + "@papl_inse_id = " + mintinsti.ToString() + strFin
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarCuotas(ByVal pboolAuto As Boolean, ByVal pboolLimp As Boolean)
      Try
         If Not pboolAuto Then
            mGuardarDatosCuotas()
         End If
         mLimpiarCuotas(pboolLimp)
         With mdsDatos.Tables(mstrPagosCuotas)
            .DefaultView.Sort = "plco_fecha"
            grdCuota.DataSource = .DefaultView
         End With

         grdCuota.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Private Sub mValidarDatos()

            If txtImp.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el importe de la cuota.")
            End If

            If txtFCta.Fecha Is DBNull.Value Or txtFCta.Fecha.Trim.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de emisi�n de la cuota.")
            End If

        End Sub

   Private Sub mGuardarDatosCuotas()

      Dim ldrDatos As DataRow
      Dim lDateFecha As DateTime
      Try
         mValidarDatos()

         If hdnCuotaId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrPagosCuotas).NewRow
            ldrDatos.Item("plco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrPagosCuotas), "plco_id")
         Else
            ldrDatos = mdsDatos.Tables(mstrPagosCuotas).Select("plco_id=" & hdnCuotaId.Text)(0)
         End If

         With ldrDatos
                    .Item("plco_fecha") = txtFCta.Fecha
            lDateFecha = txtFCta.Fecha
            .Item("plco_impo") = txtImp.Valor

            .Item("plco_deau_impo") = txtImp.Valor * (mintDeauImpo / 100)
            .Item("plco_soci_impo") = txtImp.Valor * (mintSociImpo / 100)
            .Item("plco_1soci_impo") = txtImp.Valor * (mint1SociImpo / 100)
            .Item("plco_2soci_impo") = txtImp.Valor * (mint2SociImpo / 100)
         End With

         If hdnCuotaId.Text = "" Then
            mdsDatos.Tables(mstrPagosCuotas).Rows.Add(ldrDatos)
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub mLimpiarCuotas(ByVal pboolMant As Boolean)
      If Not pboolMant Then
         txtImp.Text = ""
         txtDeauImpo.Text = ""
         txtSociImpo.Text = ""
         txt1SociImpo.Text = ""
         txt2SociImpo.Text = ""
      End If
      txtFCta.Text = ""
      hdnCuotaId.Text = ""
      mSetearEditor(mstrPagosCuotas, True)
   End Sub
#End Region

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnBajaCuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCuota.Click
      Try
         mdsDatos.Tables(mstrPagosCuotas).Select("plco_id=" & hdnCuotaId.Text)(0).Delete()
         grdCuota.DataSource = mdsDatos.Tables(mstrPagosCuotas)
         grdCuota.DataBind()
         mLimpiarCuotas(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpCuota.Click
      mLimpiarCuotas(False)
   End Sub

   Private Sub lnkCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCuota.Click
      mShowTabs(2)
   End Sub

   Private Sub btnGeneCuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneCuota.Click
      Try
         Dim dr As Data.DataRow
         Dim ok As Boolean = False
         Dim ie As System.Collections.IEnumerator = mdsDatos.Tables(mstrPagosCuotas).Rows.GetEnumerator()
         ie.Reset()

         While (ie.MoveNext() And Not ok)
            dr = ie.Current
            ok = Not dr.RowState = DataRowState.Deleted
         End While

         If ok Then
            Throw New AccesoBD.clsErrNeg("Ya existe un plan de cuotas.")
         End If

         Dim lstrPagina As String = "Cuotas_Pop.aspx?"
         clsWeb.gGenerarPopUp(Me, lstrPagina, 300, 180, 10, 75)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
      Try
         Dim ldoubCuota As Double
         Dim lDateFecha As DateTime
         Dim lDateFechaVto As DateTime
         Dim lstrImp As String
         Dim lstrDias As String
         Dim x As Int32

         If (hdnDatosPop.Text <> "") Then
            Dim lvrDatos() As String = hdnDatosPop.Text.Split(",")

            ldoubCuota = clsSQLServer.gFormatArg(lvrDatos(1), SqlDbType.Int)
            lstrImp = clsSQLServer.gFormatArg(lvrDatos(0), SqlDbType.Float)

            lDateFecha = CDate(lvrDatos(2))


            hdnDatosPop.Text = ""
            'mConsultarInsc()

            For x = 1 To ldoubCuota
               If x <> 1 Then
                  lDateFecha = lDateFecha.AddMonths(1)
               End If

               mGuardarDatosCuotasAuto(lstrImp, lDateFecha)
            Next
            lblCtasAuto.Text = ldoubCuota
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosCuotasAuto(ByVal pdoubImporte As Double, ByVal pstrFecha As Date)
      Dim ldrDatos As DataRow
      Try
         If hdnCuotaId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrPagosCuotas).NewRow
            ldrDatos.Item("plco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrPagosCuotas), "plco_id")
         Else
            ldrDatos = mdsDatos.Tables(mstrPagosCuotas).Select("plco_id=" & hdnCuotaId.Text)(0)
         End If

         With ldrDatos
            If hdnId.Text <> "" Then
               '.Item("mapr_clie_id") = hdnId.Text
            End If
            .Item("plco_impo") = pdoubImporte
            .Item("plco_fecha") = pstrFecha
            .Item("plco_deau_impo") = pdoubImporte * (mintDeauImpo / 100)
            .Item("plco_soci_impo") = pdoubImporte * (mintSociImpo / 100)
            .Item("plco_1soci_impo") = pdoubImporte * (mint1SociImpo / 100)
            .Item("plco_2soci_impo") = pdoubImporte * (mint2SociImpo / 100)
         End With

         If hdnCuotaId.Text = "" Then
            mdsDatos.Tables(mstrPagosCuotas).Rows.Add(ldrDatos)
         End If
         mActualizarCuotas(True, False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try


   End Sub

   Private Sub btnAltaCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaCuota.Click
      mActualizarCuotas(False, False)
   End Sub

   Private Sub btnModiCuota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiCuota.Click
      mActualizarCuotas(False, False)
   End Sub
End Class
End Namespace
