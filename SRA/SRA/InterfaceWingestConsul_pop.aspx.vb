Namespace SRA

Partial Class InterfaceWingestConsul_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mdsDatos As DataSet

    Private Enum Columnas As Integer
        Chk = 0
        ImpoID = 1
        NombreArch = 2
        FechaRecep = 3
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If Not Page.IsPostBack Then
                mConsultar()
            Else

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConsulta.EditItemIndex = -1
            If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
                grdConsulta.CurrentPageIndex = 0
            Else
                grdConsulta.CurrentPageIndex = E.NewPageIndex
            End If

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


#End Region

#Region "Funciones"
    Private Function mFecha() As String
        Dim lstrFecha As String

        lstrFecha = lstrFecha.Substring(0, 10)
        Return (clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(lstrFecha)))
    End Function

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            Dim ds As DataSet
            Dim ldsDatos As DataSet

            lstrCmd.Append("exec rg_importaciones_consul ")

            'Nombre Archivo.
            If txtArch.Text <> "" Then
                lstrCmd.AppendFormat("'{0}'", txtArch.Text.Trim())
            Else
                lstrCmd.Append(" null")
            End If

            'Fecha Desde.
            If txtFechaDesde.Text <> "" Then
                lstrCmd.AppendFormat(",{0}", clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaDesde.Text)))
            Else
                lstrCmd.Append(",null")
            End If

            'Fecha Hasta.
            If txtFechaHasta.Text <> "" Then
                lstrCmd.AppendFormat(",{0}", clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtFechaHasta.Text)))
            Else
                lstrCmd.Append(",null")
            End If

            'Ejecuto el Query.
            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            grdConsulta.DataSource = ds
            grdConsulta.DataBind()
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mCerrar()
        Try
            Dim lsbMsg As New StringBuilder

            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
            Response.Write(lsbMsg.ToString)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

#End Region

#Region "Eventos"
    Private Sub btnConsul_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsul.Click
        Try
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Try
            'Verifico que haya algo seleccionado para listar.
            If hdnImpoID.Text <> "" Then
                'Defino variables.
                Dim lstrRptName As String
                Dim lintRF As Integer
                Dim lstrRepoPara As String = ""
                Dim lstrRepo As String = ""
                Dim strNombreArchivo As String
                Dim lstrRpt As String
                Dim strImpoID As String = hdnImpoID.Text
                Dim strNombArch As String = hdnNombArch.Text.Trim()
                Dim lsbMsg As New StringBuilder

                'Verifico que letra contiene el nombre del archivo.
                If (strNombArch.IndexOf("N") <> -1) Then
                    'Nacimiento.
                    lstrRepo = "ImportacionWingestNacimiento"
                ElseIf (strNombArch.IndexOf("S") <> -1) Then
                    'Servicios.
                    lstrRepo = "ImportacionWingestServicios"
                ElseIf (strNombArch.IndexOf("R") <> -1) Then
                    'Recuperación.
                    lstrRepo = "ImportacionWingestRecuperacion"
                ElseIf (strNombArch.IndexOf("T") <> -1) Then
                    'Trasplante.
                    lstrRepo = "ImportacionWingestTrasplante"
                ElseIf (strNombArch.IndexOf("I") <> -1) Then
                    'Implante.
                    lstrRepo = "ImportacionWingestImplantacion"
                ElseIf (strNombArch.IndexOf("B") <> -1) Then
                    'Baja.
                    lstrRepo = "ImportacionWingestBaja"
                Else
                    lstrRepo = "ImportacionWingest"
                End If

                'Armo la url del reporte.
                lstrRepo += "&impo_id=" & strImpoID
                lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRepo)
                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

                'Muestro el Reporte en una nueva ventana.
                lsbMsg.Append("<SCRIPT language='javascript'>")
                lsbMsg.Append("gAbrirVentanas('" + lstrRpt + "', 0,'1024','600','50','50');")
                lsbMsg.Append("</SCRIPT>")
                RegisterClientScriptBlock("subscribescript", lsbMsg.ToString())

            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        Try
            mCerrar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region


End Class

End Namespace
