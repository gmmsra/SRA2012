' Dario 2014-05-28 Cambios de nombres asignacion de campos y volar planilla 1
Imports ReglasValida.Validaciones
Imports SRA_Entidad


Namespace SRA

    Partial Class ExportacionSemen
        Inherits FormGenerico


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents usrCriaProp As usrClieDeriv
        Protected WithEvents rowDivExpo As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents rowDivProp As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents rowDivPaisFil As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents rowDosisCriaSemen As System.Web.UI.HtmlControls.HtmlTableRow


        Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
        Protected WithEvents cmbRazaFil As NixorControls.ComboBox
        Protected WithEvents cmbRazaCria As NixorControls.ComboBox


        Protected WithEvents lblSraNumeFil As System.Web.UI.WebControls.Label
        Protected WithEvents txtSraNumeFil As NixorControls.NumberBox
        'Protected WithEvents lblProdFil As System.Web.UI.WebControls.Label
        Protected WithEvents lblDosiCantSemen As System.Web.UI.WebControls.Label
        Protected WithEvents lblCantEmbrSemen As System.Web.UI.WebControls.Label
        Protected WithEvents txtCantEmbrSemen As NixorControls.NumberBox
        Protected WithEvents lblTranPlan As System.Web.UI.WebControls.Label




        Protected WithEvents lblRecuFecha As System.Web.UI.WebControls.Label
        Protected WithEvents txtRecuFecha As NixorControls.DateBox
        Protected WithEvents btnTeDenu As System.Web.UI.WebControls.Button
        Protected WithEvents txtTeDesc As NixorControls.TextBoxTab

        Protected WithEvents cmbVari As NixorControls.ComboBox
        Protected WithEvents lblNombComp As System.Web.UI.WebControls.Label

        Protected WithEvents lblTipoRegistro As System.Web.UI.WebControls.Label
        Protected WithEvents cmbRaza As NixorControls.ComboBox
        Protected WithEvents lblRaza As System.Web.UI.WebControls.Label
        Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label

        Protected WithEvents lblCantEmbr As System.Web.UI.WebControls.Label
        Protected WithEvents txtCantEmbr As NixorControls.NumberBox
        Protected WithEvents rowCantEmbr As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents lblCriaCant As System.Web.UI.WebControls.Label
        Protected WithEvents rowDosisCria As System.Web.UI.HtmlControls.HtmlTableRow


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_Tramites
        Private mstrTablaRequisitos As String = SRA_Neg.Constantes.gTab_Tramites_Deta
        Private mstrTablaDocumentos As String = SRA_Neg.Constantes.gTab_Tramites_Docum
        Private mstrTablaObservaciones As String = SRA_Neg.Constantes.gTab_Tramites_Obse
        Private mstrTablaProductos As String = SRA_Neg.Constantes.gTab_Productos
        Private mstrTablaTramitesProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
        Private mstrTablaDenuncia As String = SRA_Neg.Constantes.gTab_TeDenun
        Private mstrTablaSemenStock As String = SRA_Neg.Constantes.gTab_SemenStock
        Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis

        Private mstrTramiteId As String = ""

        Private mstrParaPageSize As Integer
        Private mdsDatosPadre As DataSet
        Private mdsDatosMadre As DataSet
        Private dsVali As DataSet
        Private mstrCmd As String
        Public mintProce As Integer
        Private mdsDatos As DataSet
        Private mstrConn As String
        Private mstrTrapId As String
        Public mstrTitulo As String
        Public mintTtraId As Integer

#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()
                If (Not Page.IsPostBack) Then
                    Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                    clsWeb.gInicializarControles(Me, mstrConn)

                    Session(mstrTabla) = Nothing

                    mstrTramiteId = Request.QueryString("Tram_Id")

                    If mstrTramiteId <> "" Then
                        hdnId.Text = mstrTramiteId
                        Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
                        cmbEsta.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                        cmbEstadoFil.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                        mEditarTramite(mstrTramiteId)
                        mMostrarPanel(True)
                        lnkRequ.Font.Bold = False
                        lnkDocu.Font.Bold = False
                        lnkObse.Font.Bold = False

                        lnkRequ.Enabled = False
                        lnkDocu.Enabled = False
                        lnkObse.Enabled = False

                        panRequ.Visible = False
                        panDocu.Visible = False
                        panObse.Visible = False
                    End If

                    If mstrTramiteId = "" Then
                        mSetearMaxLength()
                        mSetearEventos()
                        mCargarCombos()
                        mMostrarPanel(False)
                    End If

                Else
                    mstrTramiteId = Request.QueryString("Tram_Id")
                    If mstrTramiteId <> "" Then
                        Response.Write("<Script>window.close();</script>")
                    End If

                End If
                mdsDatos = Session(mstrTabla)
                mstrTrapId = Session("mstrTrapId")


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub mCargarCombos()
            Dim lstrSRA As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
            clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")
            clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")


            clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazaFil, "T")
            SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)

            clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
            SRA_Neg.Utiles.gSetearRaza(cmbRaza)


            clsWeb.gCargarCombo(mstrConn, "rg_requisitos_cargar", cmbRequRequ, "id", "descrip_codi", "S")
            clsWeb.gCargarCombo(mstrConn, "importadores_cargar", cmbExpo, "id", "descrip_codi", "S")
            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "S", "@inc_defa = null", True)
            clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPaisFil, "S", "", True)



        End Sub
        Private Sub mSetearEventos()
            btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
            btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
            btnBaja.Attributes.Add("onclick", "if(!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?')){return false;} else {mPorcPeti();}")
            ' btnNuevoImpEmb.Attributes.Add("onclick", "return btnNuevoImpEmb_click();") Dario 2014-05-28

            'btnTeDenu.Visible = True
            'btnTeDenu.Text = "Denuncia"
            'btnTeDenu.Attributes.Add("onclick", "mCargarTE();return false;")

            btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
            btnErr.Visible = True


            btnSemen.Visible = True
            btnSemen.Text = "Stock Semen"
            btnSemen.Attributes.Add("onclick", "mVerStockSemen(); return false;")


        End Sub
        Private Sub mSetearMaxLength()
            Dim lstrLong As Object
            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaSemenStock)
            txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nume")
            txtDosiCantExpoSemen.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_dosi_cant")

            txtNroControl.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nro_control")


            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, SRA_Neg.Constantes.gTab_Productos)
            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocumentos)
            txtDocuObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trdo_refe")
            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaObservaciones)
            txtObseObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trao_obse")

        End Sub
#End Region

#Region "Inicializacion de Variables"
        Public Sub mHabilitarDatosTE(ByVal pbooHabi As Boolean)
            'cmbCriaVend.Enabled = pbooHabi
            'txtRecuFecha.Enabled = pbooHabi
            'usrProd.Habilitar = False
            'usrProd.usrMadreExt.Activo = pbooHabi
            'usrProd.usrMadreExt.cmbProdRazaExt.Enabled = False
            'usrProd.usrPadreExt.Activo = pbooHabi
            'usrProd.usrCriaExt.Activo = False

        End Sub
        Public Sub mInicializar()
            clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdRequ.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdDocu.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdObse.PageSize = Convert.ToInt32(mstrParaPageSize)


            mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
            mintProce = ReglasValida.Validaciones.Procesos.ExportacionSemen



            mstrTitulo = "Exportaci�n de Semen"
            rowProp.Style.Add("display", "none")
            rowCriaFil.Style.Add("display", "none")


            usrProductoPadre.FilRpNume = True
            usrProductoPadre.IgnogaInexistente = True


            usrProductoPadre.FilSexo = False
            usrProductoPadre.Sexo = 1


            btnNuevoImpEmb.AlternateText = "Nueva " & mstrTitulo

            'usrProductoPadre.MuestraAsocExtr = False
            'usrProductoPadre.MuestraNroAsocExtr = False

            'usrProdFil.MuestraAsocExtr = False
            'usrProdFil.MuestraNroAsocExtr = False
            'usrProdFil.cmbProdAsocExt.Enabled = False
            'usrProdFil.txtCodiExt.Enabled = False


            lblTituAbm.Text = mstrTitulo
        End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
        Public Sub grdDato_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.EditItemIndex = -1
                If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = 0
                Else
                    grdDato.CurrentPageIndex = E.NewPageIndex
                End If
                If Page.IsPostBack Then
                    mConsultar(True)
                Else
                    mConsultar(False)
                End If
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Sub grdRequ_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdRequ.EditItemIndex = -1
                If (grdRequ.CurrentPageIndex < 0 Or grdRequ.CurrentPageIndex >= grdRequ.PageCount) Then
                    grdRequ.CurrentPageIndex = 0
                Else
                    grdRequ.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarRequ()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Sub grdDocu_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDocu.EditItemIndex = -1
                If (grdDocu.CurrentPageIndex < 0 Or grdDocu.CurrentPageIndex >= grdDocu.PageCount) Then
                    grdDocu.CurrentPageIndex = 0
                Else
                    grdDocu.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarDocu()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Sub grdObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdObse.EditItemIndex = -1
                If (grdObse.CurrentPageIndex < 0 Or grdObse.CurrentPageIndex >= grdObse.PageCount) Then
                    grdObse.CurrentPageIndex = 0
                Else
                    grdObse.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarObse()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mConsultar(ByVal boolMostrarTodos As Boolean)
            Try
                Dim lstrCmd As String
                Dim dsDatos As New DataSet

                lstrCmd = "exec semen_busq "
                lstrCmd += " @tram_nume=" + clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int)
                lstrCmd += ",@tram_raza_id=" + clsSQLServer.gFormatArg(ValidarNulos(usrProdFil.RazaId, True), SqlDbType.Int)


                If Not usrProdFil Is Nothing Then
                    lstrCmd += ",@sra_nume=" + clsSQLServer.gFormatArg(ValidarNulos(usrProdFil.txtSraNumeExt.Valor, True), SqlDbType.Int)
                End If

                lstrCmd += ",@cria_id=" + clsSQLServer.gFormatArg(usrCriaFil.Valor, SqlDbType.Int) ' Dario 2014-10-15
                If Not usrProdFil Is Nothing Then
                    lstrCmd += ",@prod_id=" + clsSQLServer.gFormatArg(usrProdFil.Valor, SqlDbType.Int)
                End If
                lstrCmd += ",@prod_nomb=" + IIf(usrProdFil.txtProdNombExt.Valor.ToString().Trim() = "", "NULL", "'" + usrProdFil.txtProdNombExt.Valor + "'")
                lstrCmd += ",@fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
                lstrCmd += ",@fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)
                lstrCmd += ",@tram_ttra_id=" + mintTtraId.ToString
                lstrCmd += ",@tram_esta_id=" + cmbEstadoFil.Valor.ToString
                lstrCmd += ",@tram_pais_id=" + cmbPaisFil.Valor.ToString
                lstrCmd += ",@mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")


                dsDatos = clsWeb.gCargarDataSet(mstrConn, lstrCmd)
                grdDato.DataSource = dsDatos
                grdDato.DataBind()
                grdDato.Visible = True



                mMostrarPanel(False)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try

        End Sub
        Private Sub mConsultarRequ()
            grdRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
            grdRequ.DataBind()
        End Sub
        Private Sub mConsultarDocu()
            grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocumentos)
            grdDocu.DataBind()
        End Sub
        Private Sub mConsultarObse()
            mdsDatos.Tables(mstrTablaObservaciones).DefaultView.Sort = "trao_fecha desc,_requ_desc"
            grdObse.DataSource = mdsDatos.Tables(mstrTablaObservaciones)
            grdObse.DataBind()
        End Sub
        Private Function mCrearDataSet(ByVal pstrId As String, ByVal pSpEstructura As String, _
                     ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                     ByVal pOpcionAdd As Boolean) As Boolean


            Dim ldsDatosTransf As New DataSet
            Dim tblDatos As New DataTable(pTableName)

            ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, pstrId)
            tblDatos = ldsDatosTransf.Tables(0)
            tblDatos.TableName = pTableName

            dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
            Session(mstrTabla) = dsDatosNewDataSet

            Return True
        End Function
        Private Function mCrearDataSet(ByVal pstrId As String, ByVal pSpEstructura As String, _
                        ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                        ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


            Dim ldsDatosTransf As New DataSet
            Dim tblDatos As New DataTable(pTableName)

            ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
            tblDatos = ldsDatosTransf.Tables(0)
            tblDatos.TableName = pTableName

            dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
            Session(mstrTabla) = dsDatosNewDataSet

            Return True
        End Function

        Public Sub mCrearDataSet(ByVal pstrId As String)
            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrTablaRequisitos
            mdsDatos.Tables(2).TableName = mstrTablaDocumentos
            mdsDatos.Tables(3).TableName = mstrTablaObservaciones
            'mdsDatos.Tables(4).TableName = mstrTablaProductos


            grdRequ.CurrentPageIndex = 0
            grdDato.CurrentPageIndex = 0
            grdObse.CurrentPageIndex = 0

            mConsultarRequ()
            mConsultarDocu()
            mConsultarObse()


            Session(mstrTabla) = mdsDatos
        End Sub
#End Region

#Region "Seteo de Controles"
        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mEditarTramite(E.Item.Cells(2).Text)
        End Sub

        Private Sub mCargarCriadores(ByVal pobjCombo As NixorControls.ComboBox, ByVal pstrClieId As String, ByVal pstrRaza As String)
            If pobjCombo.Visible Then
                mstrCmd = "criadores_cliente_cargar "
                If pstrClieId <> "" Then
                    mstrCmd = mstrCmd & " @clie_id=" & pstrClieId
                Else
                    mstrCmd = mstrCmd & " @clie_id=0"
                End If
                If pstrRaza <> "" Then
                    mstrCmd = mstrCmd & ",@raza_id=" & pstrRaza
                End If
                clsWeb.gCargarCombo(mstrConn, mstrCmd, pobjCombo, "id", "descrip", "S")
            End If
        End Sub


        Public Sub mEditarTramite(ByVal pstrTram As String)
            Try
                Dim intClienteId As Integer
                Dim intVendClienteId As Integer

                Dim ProductoId As Int32
                Dim SemenId As Int32
                Dim DenunciaId As Int32
                Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
                Dim dtDenuncia As DataTable
                Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, Session("sUserId").ToString())
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                Dim vprdtPadre_id As String

                Dim dtProductoPadre As DataTable

                mLimpiar()

                hdnId.Text = clsFormatear.gFormatCadena(pstrTram)

                intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
                intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrProductoPadre.RazaId, usrProductoPadre.CriaOrPropId)

                ProductoId = -1
                SemenId = -1
                DenunciaId = -1





                dtDenuncia = oDenuncia.GetDenunciaSemenByTramiteId(hdnId.Text, "")
                If dtDenuncia.Rows.Count > 0 Then
                    DenunciaId = dtDenuncia.Rows(0).Item("tede_id")
                End If




                Dim dtSemenStock As DataTable
                Dim oSemen As New SRA_Neg.SemenStock(mstrConn, Session("sUserId").ToString())



                dtSemenStock = oSemen.GetSemenStockByTramiteId(hdnId.Text, "")
                If dtSemenStock.Rows.Count > 0 Then
                    SemenId = dtSemenStock.Rows(0).Item("sest_id")
                    If ValidarNulos(dtSemenStock.Rows(0).Item("sest_fecha_tran"), False) <> "0" Then
                        txtFechaExportacion.Fecha = ValidarNulos(dtSemenStock.Rows(0).Item("sest_fecha_tran"), False)
                    End If
                End If

                mCrearDataSet(hdnId.Text)
                mCrearDataSet("", mstrTablaAnalisis, mstrTablaAnalisis, mdsDatos, True, ProductoId.ToString)
                mCrearDataSet("", mstrTablaSemenStock, mstrTablaSemenStock, mdsDatos, True, SemenId)
                mCrearDataSet("", mstrTablaDenuncia, mstrTablaDenuncia, mdsDatos, True, DenunciaId)



                With mdsDatos.Tables(mstrTabla).Rows(0)
                    hdnTramNro.Text = "Tr�mite Nro: " & .Item("tram_nume")
                    lblTitu.Text = hdnTramNro.Text
                    hdnRazaId.Text = .Item("tram_raza_id")
                    txtInicFecha.Fecha = IIf(.Item("tram_inic_fecha").ToString.Length > 0, .Item("tram_inic_fecha"), String.Empty)
                    txtFinaFecha.Fecha = IIf(.Item("tram_fina_fecha").ToString.Length > 0, .Item("tram_fina_fecha"), String.Empty)
                    txtFechaTram.Fecha = IIf(.Item("tram_pres_fecha").ToString.Length > 0, .Item("tram_pres_fecha"), String.Empty)
                    txtFechaExportacion.Fecha = IIf(.Item("tram_oper_fecha").ToString.Length > 0, .Item("tram_oper_fecha"), String.Empty)

                    txtNroControl.Text = ValidarNulos(.Item("tram_nro_control"), False)
                    If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then
                        txtDosiCantExpoSemen.Valor = mdsDatos.Tables(mstrTabla).Rows(0).Item("tram_dosi_cant")

                    Else
                        txtDosiCantExpoSemen.Valor = 0

                    End If


                    If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then
                        vprdtPadre_id = ValidarNulos(mdsDatos.Tables(mstrTabla).Rows(0).Item("tram_embr_padre_id"), False)
                    End If

                    usrProductoPadre.Valor = .Item("tram_embr_padre_id")

                    dtProductoPadre = oProducto.GetProductoById(vprdtPadre_id, "")
                    If dtProductoPadre.Rows.Count > 0 Then
                        usrProductoPadre.RazaId = ValidarNulos(dtProductoPadre.Rows(0).Item("prdt_raza_id"), False)
                        usrProductoPadre.CriaOrPropId = _
                        IIf(ValidarNulos(dtProductoPadre.Rows(0).Item("prdt_prop_cria_id"), False) = 0, _
                        ValidarNulos(dtProductoPadre.Rows(0).Item("prdt_cria_id"), False), _
                        ValidarNulos(dtProductoPadre.Rows(0).Item("prdt_prop_cria_id"), False))
                    End If

                    dtSemenStock = oSemen.GetSemenStockByTramiteId(hdnId.Text, "")


                    If dtSemenStock.Rows.Count > 0 Then
                        If ValidarNulos(dtSemenStock.Rows(0).Item("sest_fecha_tran"), False) <> "0" Then
                            txtFechaExportacion.Fecha = ValidarNulos(dtSemenStock.Rows(0).Item("sest_fecha_tran"), False)
                        End If
                    End If


                    cmbEsta.Valor = .Item("tram_esta_id")
                    cmbExpo.Valor = .Item("tram_impr_id")
                    cmbPais.Valor = .Item("tram_pais_id")
                    usrCriaComp.Valor = .Item("tram_comp_cria_id")
                    hdnCompOrig.Text = .Item("tram_comp_clie_id")



                    hdnVendOrig.Text = .Item("tram_vend_clie_id")


                    If Not .IsNull("tram_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("tram_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If

                    'If .IsNull("_te_denun_desc") Then
                    '    txtTeDesc.Text = ""
                    'Else
                    '    txtTeDesc.Text = .Item("_te_denun_desc")
                    'End If

                    'datos del �ltimo analisis
                    'If mdsDatos.Tables(mstrTablaAnalisis).Rows.Count > 0 Then
                    '    mdsDatos.Tables(mstrTablaAnalisis).DefaultView.Sort() = "prta_fecha DESC"
                    '    With mdsDatos.Tables(mstrTablaAnalisis).Select()(0)
                    '        txtNroAnal.Valor = .Item("prta_nume")
                    '        'txtTipoAnal.Valor = .Item("_tipo")
                    '        txtResulAnal.Valor = .Item("_resul_codi") & "-" & .Item("_resul")
                    '    End With
                    '    mdsDatos.Tables(mstrTablaAnalisis).DefaultView.RowFilter = ""
                    'End If



                    'If usrProd.usrProductoExt.cmbSexoProdExt.Valor.ToString = "1" And _
                    '    mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Then

                    '    rowDosisCria.Style.Add("display", "inline")

                    'End If

                    Dim dtSemenSaldoCompStock As DataTable
                    Dim dtSemenSaldoVendStock As DataTable



                    dtSemenSaldoCompStock = oSemen.GetSaldoSemenStockByCriaIdPadres( _
                    usrCriaComp.Valor, usrProductoPadre.Valor)

                    dtSemenSaldoVendStock = oSemen.GetSaldoSemenStockByCriaIdPadres( _
    usrProductoPadre.CriaOrPropId, usrProductoPadre.Valor)

                    If (dtSemenSaldoCompStock.Rows.Count > 0) Then
                        txtstockCantComprador.Text = ValidarNulos(dtSemenSaldoCompStock.Rows(0).Item("cant"), False)

                    End If

                    If (dtSemenSaldoVendStock.Rows.Count > 0) Then
                        txtstockCantVendedor.Text = ValidarNulos(dtSemenSaldoVendStock.Rows(0).Item("cant"), False)

                    End If


                    'btnTeDenu.Visible = False
                    'txtTeDesc.Visible = False
                    mSetearEditor(mstrTabla, False)
                    mMostrarPanel(True)
                End With
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosRequ(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                Dim ldrDeta As DataRow

                hdnRequId.Text = E.Item.Cells(1).Text
                ldrDeta = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)

                With ldrDeta
                    cmbRequRequ.Valor = .Item("trad_requ_id")
                    chkRequPend.Checked = .Item("trad_pend")
                    lblRequManu.Text = .Item("_manu")
                End With

                mSetearEditor(mstrTablaRequisitos, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Sub mEditarDatosDocu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                Dim ldrDoc As DataRow

                hdnDocuId.Text = E.Item.Cells(1).Text
                ldrDoc = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)

                With ldrDoc
                    If .IsNull("trdo_path") Then
                        txtDocuDocu.Valor = ""
                        imgDelDocuDoc.Visible = False
                    Else
                        imgDelDocuDoc.Visible = True
                        txtDocuDocu.Valor = .Item("trdo_path")
                    End If
                    txtDocuObse.Valor = .Item("trdo_refe")
                End With

                mSetearEditor(mstrTablaDocumentos, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                Dim ldrObse As DataRow

                hdnObseId.Text = E.Item.Cells(1).Text
                ldrObse = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)

                With ldrObse
                    cmbObseRequ.Valor = .Item("_trad_requ_id")
                    txtObseObse.Valor = .Item("trao_obse")
                    lblObseFecha.Text = CDate(.Item("trao_fecha")).ToString("dd/MM/yyyy")
                End With

                mSetearEditor(mstrTablaObservaciones, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub mLimpiarFiltros()
            txtNumeFil.Text = ""
            txtFechaDesdeFil.Text = ""
            txtFechaHastaFil.Text = ""


            SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
            cmbEstadoFil.Limpiar()
            cmbPaisFil.Limpiar()
            txtNumeFil.Text = ""
            usrCriaFil.cmbCriaRazaExt.Enabled = True
            usrCriaFil.Limpiar()




            grdDato.Visible = False
        End Sub
        Private Sub mLimpiar()
            hdnId.Text = ""
            hdnRazaId.Text = ""
            lblBaja.Text = ""
            hdnMultiProd.Text = ""

            hdnTEId.Text = ""
            usrProductoPadre.Limpiar()

            txtInicFecha.Fecha = Now
            txtFechaTram.Fecha = Now
            txtFechaExportacion.Fecha = "" ' System.DBNull.Value
            txtNroControl.Text = ""
            txtFinaFecha.Text = ""
            txtDosiCantExpoSemen.Text = ""
            txtDosiCantExpoSemen.Text = ""

            cmbEsta.Limpiar()

            usrCriaComp.Limpiar()


            usrClieProp.Limpiar()

            cmbPais.Limpiar()
            cmbExpo.Limpiar()

            hdnCompOrig.Text = ""
            hdnVendOrig.Text = ""
            hdnTramNro.Text = ""

            txtstockCantComprador.Text = ""
            txtstockCantVendedor.Text = ""

            lblTitu.Text = ""
            hdnTramNro.Text = ""

            grdRequ.CurrentPageIndex = 0
            grdDocu.CurrentPageIndex = 0
            grdObse.CurrentPageIndex = 0

            mLimpiarRequ()
            mLimpiarDocu()
            mLimpiarObse()


            mCrearDataSet("")
            mCrearDataSet("", mstrTablaSemenStock, mstrTablaSemenStock, mdsDatos, True, "")
            mCrearDataSet("", mstrTablaTramitesProductos, mstrTablaTramitesProductos, mdsDatos, True, "")
            mCrearDataSet("", mstrTablaDenuncia, mstrTablaDenuncia, mdsDatos, True, "")


            Session(mstrTabla) = mdsDatos

            mSetearEditor(mstrTabla, True)
            mShowTabs(1)
            mCargarPlantilla()
        End Sub
        Private Sub mLimpiarComp()
            hdnCompId.Text = ""
            '   txtObseComp.Text = ""
            '  txtPorcComp.Text = ""
            lblNombComp.Text = ""

            '   mSetearEditor(mstrTablaCompradores, True)
        End Sub
        Private Sub mLimpiarRequ()
            hdnRequId.Text = ""
            cmbRequRequ.Limpiar()
            chkRequPend.Checked = False
            lblRequManu.Text = "S�"

            mSetearEditor(mstrTablaRequisitos, True)
        End Sub
        Private Sub mLimpiarDocu()
            hdnDocuId.Text = ""
            txtDocuObse.Valor = ""
            txtDocuDocu.Valor = ""
            imgDelDocuDoc.Visible = False

            mSetearEditor(mstrTablaDocumentos, True)
        End Sub
        Private Sub mLimpiarObse()
            hdnObseId.Text = ""
            cmbObseRequ.Limpiar()
            txtObseObse.Text = ""
            txtObseFecha.Fecha = Now

            mSetearEditor(mstrTablaObservaciones, True)
        End Sub


        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
            Select Case pstrTabla
                Case mstrTabla
                    btnAlta.Enabled = pbooAlta
                    btnBaja.Enabled = Not (pbooAlta)
                    btnModi.Enabled = Not (pbooAlta)
                    'btnPedigree.Disabled = pbooAlta
                    btnErr.Visible = Not (pbooAlta)
                Case mstrTablaRequisitos
                    btnAltaRequ.Enabled = pbooAlta
                    btnBajaRequ.Enabled = Not (pbooAlta)
                    btnModiRequ.Enabled = Not (pbooAlta)
                Case mstrTablaDocumentos
                    btnAltaDocu.Enabled = pbooAlta
                    btnBajaDocu.Enabled = Not (pbooAlta)
                    btnModiDocu.Enabled = Not (pbooAlta)
                Case mstrTablaObservaciones
                    btnAltaObse.Enabled = pbooAlta
                    btnBajaObse.Enabled = Not (pbooAlta)
                    btnModiObse.Enabled = Not (pbooAlta)

            End Select
        End Sub
        Private Sub mAgregar()
            Try
                mstrTrapId = 14 ' hdnDatosPop.Text Dario 2014-05-28
                mLimpiar()
                btnBaja.Enabled = False
                btnModi.Enabled = False
                btnAlta.Enabled = True
                mMostrarPanel(True)
                hdnDatosPop.Text = ""
                Session("mstrTrapId") = mstrTrapId

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mCerrar()
            mMostrarPanel(False)
        End Sub
        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            lnkCabecera.Font.Bold = True
            lnkRequ.Font.Bold = False
            lnkDocu.Font.Bold = False
            lnkObse.Font.Bold = False


            panDato.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            panFiltros.Visible = Not panDato.Visible
            grdDato.Visible = Not panDato.Visible

            panCabecera.Visible = True
            panRequ.Visible = False
            panDocu.Visible = False
            panObse.Visible = False
            '  panComp.Visible = False

            panLinks.Visible = pbooVisi
            btnNuevoImpEmb.Visible = Not panDato.Visible
        End Sub
#End Region

#Region "Opciones de ABM"
        Private Sub mAlta()
            Try
                Dim ldsDatos As DataSet
                Dim ldsDatosProd As DataSet
                Dim lstrId As String
                Dim eError As New ErrorEntity
                Dim strRaza As String
                Dim strSexo As String
                Dim strTramiteId As String

                strRaza = usrProductoPadre.RazaId

                hdnOperacion.Text = "A"
                ldsDatos = Nothing
                ldsDatosProd = Nothing

                ' mValidarDatos(False, False)

                ldsDatos = mGuardarDatos()

                If ldsDatosProd Is Nothing Then
                    ldsDatosProd = mGuardarDatosProd()
                End If

                'ldsDatosProd.Tables("productos").Rows(0).Item("prdt_id") = usrProd.usrProductoExt.Valor
                'strSexo = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_sexo"), True)
                ' mdsDatosPadre = mGuardarDatosPadre(usrProd.usrPadreExt, usrProd.usrPadreExt.Valor.ToString, False)

                Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), _
                   mstrTabla, ldsDatos, ldsDatosProd, Context, True, _
                     SRA_Neg.Constantes.TramitesTipos.ExportacionSemen, Nothing, mdsDatosPadre)

                lstrId = lTramites.Alta()
                lblAltaId.Text = mObtenerTramite(lstrId)

                strTramiteId = lstrId

                mdsDatos.Tables("tramites").Rows(0).Item("tram_id") = strTramiteId
                ldsDatos.Tables("tramites").Rows(0).Item("tram_id") = strTramiteId

                mdsDatos.Tables("tramites").Rows(0).Item("tram_nume") = lblAltaId.Text
                ldsDatos.Tables("tramites").Rows(0).Item("tram_nume") = lblAltaId.Text

                lTramites.Tramites_updateByTramiteId(lstrId, lblAltaId.Text)
                lTramites.AplicarReglasTramite(lstrId, mintProce, mstrTablaSemenStock, "", strRaza, eError)

                If eError.errorDescripcion <> "" Then
                    lTramites.GrabarEstadoRetenidaEnTramites(strTramiteId)
                    Throw New AccesoBD.clsErrNeg("Se detectaron errores validatorios, por favor verifiquelo.")
                End If

                mConsultar(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                    mConsultar(False)
                End If
            Finally
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
            End Try
        End Sub
        Private Sub mModi()
            Try
                Dim ldsDatos As DataSet
                Dim ldsDatosProd As DataSet
                Dim strRaza As String
                Dim eError As New ErrorEntity

                Dim strTramiteId As String

                ldsDatos = mGuardarDatos()

                If ldsDatosProd Is Nothing Then
                    ldsDatosProd = mGuardarDatosProd()
                End If
                'mdsDatosPadre = mGuardarDatosPadre(usrProductoPadre.Valor, usrProductoPadre.Valor.ToString, False)

                Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), _
                mstrTablaSemenStock, ldsDatos, ldsDatosProd, Context, _
                mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen, _
                mintTtraId, Nothing, mdsDatosPadre)

                lTramites.Modi()

                strTramiteId = ValidarNulos(ldsDatos.Tables("tramites").Rows(0).Item("tram_id"), True)
                strRaza = ValidarNulos(ldsDatos.Tables("tramites").Rows(0).Item("tram_raza_id"), True)

                lTramites.AplicarReglasTramite(strTramiteId, mintProce, mstrTablaSemenStock, "", strRaza, eError)

                If eError.errorDescripcion <> "" Then
                    lTramites.GrabarEstadoRetenidaEnTramites(hdnId.Text)
                    Throw New AccesoBD.clsErrNeg("Se detectaron errores validatorios, por favor verifiquelo.")
                End If

                mLimpiar()
                mConsultar(False)
                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                    mConsultar(False)
                End If
            Finally
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
            End Try
        End Sub

        Public Function mGuardarDatosPadre(ByVal pusrProd As SRA.usrProdDeriv, _
                                                   ByVal pstrId As String, ByVal pbooCierre As Boolean) As DataSet
            Dim ldsDatos As DataSet
            Dim lstrId As String
            Dim ldrProd As DataRow
            Dim ldrNume As DataRow

            ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, SRA_Neg.Constantes.gTab_Productos, "", "")

            With ldsDatos
                .Tables(0).TableName = SRA_Neg.Constantes.gTab_Productos
                .Tables(1).TableName = SRA_Neg.Constantes.gTab_ProductosNumeros

            End With

            If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select.GetUpperBound(0) = -1 Then
                ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).NewRow
                ldrProd.Table.Rows.Add(ldrProd)
            Else
                ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select()(0)
            End If

            'productos
            With ldrProd
                lstrId = clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
                .Item("prdt_id") = lstrId
                .Item("prdt_raza_id") = pusrProd.cmbProdRazaExt.Valor
                .Item("prdt_sexo") = pusrProd.cmbSexoProdExt.ValorBool
                .Item("prdt_nomb") = pusrProd.txtProdNombExt.Valor
                .Item("prdt_tran_fecha") = IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)
                .Item("prdt_rp") = IIf(Trim(pusrProd.txtRPExt.Text) = "", DBNull.Value, Trim(pusrProd.txtRPExt.Text))
                .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", "E", .Item("prdt_ndad"))
                .Item("prdt_ori_asoc_id") = pusrProd.cmbProdAsocExt.Valor
                .Item("prdt_ori_asoc_nume") = pusrProd.txtCodiExt.Valor
                '.Item("prdt_ori_asoc_nume_lab") = pusrProd.txtCodiExtLab.Valor
                .Item("generar_numero") = pbooCierre

            End With
            Return ldsDatos
        End Function

        Private Sub mBaja()
            Try
                Dim ldsDatos As DataSet
                Dim ldsDatosProd As DataSet
                Dim lstrId As String
                Dim eError As New ErrorEntity
                Dim strRaza As String

                cmbEsta.SelectedValue = SRA_Neg.Constantes.Estados.Tramites_Baja

                ldsDatos = mGuardarDatos()

                If ldsDatosProd Is Nothing Then
                    ldsDatosProd = mGuardarDatosProd()
                End If

                Dim lintPage As Integer = grdDato.CurrentPageIndex

                Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), _
                           mstrTablaSemenStock, ldsDatos, ldsDatosProd, Context, _
                           mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen, _
                           mintTtraId, Nothing, mdsDatosPadre)

                lTramites.BajaLogica(hdnId.Text)

                'mModi()

                grdDato.CurrentPageIndex = 0

                mConsultar(True)

                mMostrarPanel(False)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            Finally
                Me.panBotones.Style.Add("display", "inline")
                Me.divproce.Style.Add("display", "none")
            End Try
        End Sub
        Private Sub mValidarDatos(ByVal pbooCierre As Boolean, ByVal pbooCierreBaja As Boolean)
            Dim drTramites As DataRow

            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)
            Dim dtSemenSaldoCompStock As DataTable
            Dim dtSemenSaldoVendStock As DataTable


            '   Dim dtSemenStock As DataTable
            '   Dim oSemen As New SRA_Neg.SemenStock(mstrConn, Session("sUserId").ToString())
            '           dtSemenSaldoVendStock = oSemen.GetSaldoSemenStockByCriaIdPadres( _
            '   usrCriaVend.Valor, usrProductoPadre.Valor)

            '        Dim CantStockActual As Int32
            '        CantStockActual = 0

            '            If (dtSemenSaldoVendStock.Rows.Count > 0) Then

            '            CantStockActual = ValidarNulos(dtSemenSaldoVendStock.Rows(0).Item("cant"), False)
            '        End If

            '        If (Val(txtDosiCantSemen.Text) > CantStockActual) Then
            '            Throw New AccesoBD.clsErrNeg("Saldo insuficiente - Saldo Actual " & CantStockActual)
            '        End If

            If usrProductoPadre.Valor Is DBNull.Value Or usrProductoPadre.Valor.ToString.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("El Padre debe tener HBA o SBA ")
            End If

            If usrProductoPadre.txtProdNombExt.Valor Is DBNull.Value Or usrProductoPadre.txtProdNombExt.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe cargar el  Macho Dador")
            End If

            If ValidarNulos(txtNroControl.Valor, False) = 0 Then
                Throw New AccesoBD.clsErrNeg("El numero de Control debe ser distinto de cero.")
            End If

            'If CDate(txtFechaExportacion.Text) > DateTime.Now Then
            '    Throw New AccesoBD.clsErrNeg("La Fecha de Exportaci�n no puede ser superior a la fecha actual.")
            'End If
            If CDate(txtFechaTram.Text) > DateTime.Now Then
                Throw New AccesoBD.clsErrNeg("La Fecha del Tr�mite no puede ser superior a la fecha actual.")
            End If

            If Not pbooCierreBaja Then
                ' Dario 2014-10-28 no obligatorio
                'If usrCriaComp.Valor.ToString = "" Then
                '    Throw New AccesoBD.clsErrNeg("Debe indicar el comprador.")
                'Else
                '    If usrCriaComp.Valor = "0" Then
                '        Throw New AccesoBD.clsErrNeg("El Comprador no tiene datos de Criador.")
                '    End If

                'End If

                If usrProductoPadre.CriaOrPropId.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el vendedor.")
                Else
                    If usrProductoPadre.Valor = "0" Then
                        Throw New AccesoBD.clsErrNeg("El Vendedor no tiene datos de Criador.")
                    End If
                End If


                'If ValidarNulos(txtNroControl.Valor, False) = 0 Then
                '    MessageBox(Me, "El Nro de Control debe ser distinto de 0.")
                'End If
            End If
            'If usrProd.usrPadreExt.cmbProdAsocExt.Valor Is DBNull.Value Then
            '    Throw New AccesoBD.clsErrNeg("Debe ingresar la Asociaci�n del Padre del Semen a Exportar")
            'End If


            If usrProductoPadre.Valor.ToString = "" And hdnMultiProd.Text <> "S" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Padre para Exportar Semen.")
            End If

            If usrProductoPadre.RazaId.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la raza del Padre.")
            End If

            If txtDosiCantExpoSemen.Valor.ToString <= "0" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Cantidad de Dosis.")
            End If

            'If cmbPais.Valor Is DBNull.Value Then
            '    Throw New AccesoBD.clsErrNeg("Debe indicar el Pais de Origen del Padre.")

            'End If

            Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
            'If Not oTramite.ValidarPaisesExportacionByRazaCriador(usrCriaVend.cmbCriaRazaExt.Valor, _
            '                           usrCriaVend.Valor, _
            '                                usrCriaComp.cmbCriaRazaExt.Valor, _
            '                                usrCriaComp.Valor) Then
            '    Throw New AccesoBD.clsErrNeg("El Vendedor debe ser Argentino y el comprador debe ser Extranjero.")
            'End If

            'drTramites = oTramite.GetCantFacturadoByNroControlRazaCriador(txtNroControl.Valor, _
            '                  usrProductoPadre.RazaId, _
            '                 usrCriaComp.txtCodiExt.Valor)

            'If IsNothing(drTramites) Then
            '    Throw New AccesoBD.clsErrNeg("No se encontraron comprobantes ,ni proforma con el Nro.Control " & txtNroControl.Valor)
            'End If
            '' Dario 2014-10-31 comentado ahora no lo quierenal control de nro de control
            '' creo el objeto de productos 
            'Dim objProductosBusiness As New Business.Productos.ProductosBusiness
            '' ejecuto la validacion que me retorna un string con el mensaje de error si existe
            'Dim msg As String = objProductosBusiness.ValidaNumeroControlTramitesPropiedad(txtNroControl.Valor, usrProductoPadre.CriaOrPropId, usrCriaComp.Valor, usrProductoPadre.RazaId, txtDosiCantExpoSemen.Valor, 0, Convert.ToInt16(Common.CodigosServiciosTiposRRGG.RecuperacionSemen))
            'If (Len(msg.Trim) > 0) Then
            '    Throw New AccesoBD.clsErrNeg(msg)
            'End If

        End Sub
        Private Function mObtenerTramite(ByVal pstrId) As String
            Dim lstrId As String = ""
            Try
                mstrCmd = "exec tramites_consul " & pstrId
                Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), mstrCmd)
                While (dr.Read())
                    lstrId = dr.GetValue(dr.GetOrdinal("tram_nume")).ToString().Trim()
                End While
                dr.Close()
                Return (lstrId)
            Catch ex As Exception
                Return (lstrId)
            End Try
        End Function

        Private Function mGuardarDatos() As DataSet
            Dim ldsDatosProd As DataSet
            Dim lbooCierre As Boolean = False
            Dim lbooCierreBaja As Boolean = False
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
            Dim intClienteId As Integer
            Dim intVendClienteId As Integer

            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_pend = 1").GetUpperBound(0) < 0 And _
              grdRequ.Items.Count > 0 Then
                lbooCierre = True
            End If

            If cmbEsta.Valor.ToString = SRA_Neg.Constantes.Estados.Tramites_Baja Then
                lbooCierreBaja = True
            End If

            mValidarDatos(lbooCierre, lbooCierreBaja)


            If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then
                With mdsDatos.Tables(mstrTabla).Rows(0)
                    .Item("tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    .Item("tram_ttra_id") = mintTtraId
                    .Item("tram_dosi_cant") = txtDosiCantExpoSemen.Valor
                    .Item("tram_raza_id") = usrProductoPadre.RazaId
                    .Item("tram_pres_fecha") = IIf(txtFechaTram.Fecha.Length > 0, txtFechaTram.Fecha, DBNull.Value)
                    .Item("tram_oper_fecha") = IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)
                    .Item("tram_esta_id") = IIf(cmbEsta.Valor.Length > 0, cmbEsta.Valor, DBNull.Value)
                    .Item("tram_impr_id") = IIf(cmbExpo.Valor.Length > 0, cmbExpo.Valor, DBNull.Value)
                    .Item("tram_pais_id") = IIf(cmbPais.Valor.Length > 0, cmbPais.Valor, DBNull.Value)
                    .Item("tram_nro_control") = ValidarNulos(txtNroControl.Text, False)

                    intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
                    intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrProductoPadre.RazaId, usrProductoPadre.CriaOrPropId)
                    .Item("tram_pais_id") = IIf(cmbPais.Valor.Length > 0, cmbPais.Valor, DBNull.Value)
                    '.Item("tram_pais_id") = oCliente.GetPaisByClienteId(intClienteId)

                    .Item("tram_prop_clie_id") = intClienteId
                    .Item("tram_vend_clie_id") = intVendClienteId
                    .Item("tram_comp_clie_id") = intClienteId
                    .Item("tram_vend_cria_id") = usrProductoPadre.CriaOrPropId
                    .Item("tram_comp_cria_id") = IIf(usrCriaComp.Valor.ToString.Length > 0, usrCriaComp.Valor, DBNull.Value)
                    .Item("tram_baja_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                    .Item("tram_fina_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                    .Item("tram_dosi_cant") = txtDosiCantExpoSemen.Valor


                    'If hdnTEId.Text <> "" Then
                    '    '   .Item("tram_embr_tede_comp_id") = hdnTEId.Text
                    'End If

                    .Item("tram_embr_madre_id") = DBNull.Value
                    .Item("tram_embr_padre_id") = usrProductoPadre.Valor
                    .Item("tram_nro_control") = ValidarNulos(txtNroControl.Text, False)


                    If .IsNull("tram_inic_fecha") Then
                        .Item("tram_inic_fecha") = Today
                    End If

                    .Item("tram_baja_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                    .Item("tram_fina_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)




                End With
            End If

            Dim ldrDatos As DataRow
            Dim ldrDatosDenuncia As DataRow





            If mdsDatos.Tables(mstrTablaDenuncia).Rows.Count = 0 Then
                ldrDatosDenuncia = mdsDatos.Tables(mstrTablaDenuncia).NewRow

                If Val(hdnId.Text) > 1 Then
                    ldrDatosDenuncia.Item("tede_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                Else

                    ldrDatosDenuncia.Item("tede_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDenuncia), "tede_id")

                End If

            Else
                ldrDatosDenuncia = mdsDatos.Tables(mstrTablaDenuncia).Select()(0)
            End If




            ' Por inconsistencia de datos puede ser que existan trates sin denuncia, en el caso de no exisir agrega la denuncia
            ' EN LA exportacion INTERESA EL vendedor UNA DENUNCIA DE VENTA PORQUE VIENE DEL EXTERIOR

            If mdsDatos.Tables(mstrTablaDenuncia).Rows.Count = 0 Then
                'ldrDatosDenuncia.Item("tede_recu_fecha") = txtRecuFecha.Fecha
                ldrDatosDenuncia.Item("tede_gest_dias") = DBNull.Value
                ldrDatosDenuncia.Item("tede_embr_cong") = DBNull.Value
                ldrDatosDenuncia.Item("tede_embr_fres") = DBNull.Value
                ldrDatosDenuncia.Item("tede_impl_embr_cong") = DBNull.Value
                ldrDatosDenuncia.Item("tede_impl_embr_fres") = DBNull.Value
                ldrDatosDenuncia.Item("tede_serv_fecha") = DBNull.Value

                ldrDatosDenuncia.Item("tede_pad1_prdt_id") = usrProductoPadre.Valor

                ldrDatosDenuncia.Item("tede_cria_id") = ValidarNulos(usrProductoPadre.CriaOrPropId, False)
                ldrDatosDenuncia.Item("tede_clie_id") = intVendClienteId
                ldrDatosDenuncia.Item("tede_imcr_id") = DBNull.Value
                ldrDatosDenuncia.Item("tede_auim_id") = DBNull.Value
                ldrDatosDenuncia.Item("tede_raza_id") = usrProductoPadre.RazaId

                If mdsDatos.Tables(mstrTablaDenuncia).Rows.Count = 0 Then
                    mdsDatos.Tables(mstrTablaDenuncia).Rows.Add(ldrDatosDenuncia)

                End If

            Else
                With mdsDatos.Tables(mstrTablaDenuncia).Rows(0)

                    '  .Item("tede_recu_fecha") = txtRecuFecha.Fecha
                    .Item("tede_gest_dias") = DBNull.Value
                    .Item("tede_impl_embr_cong") = DBNull.Value
                    .Item("tede_impl_embr_fres") = DBNull.Value
                    .Item("tede_serv_fecha") = DBNull.Value

                    .Item("tede_pad1_prdt_id") = usrProductoPadre.Valor
                    .Item("tede_cria_id") = ValidarNulos(usrProductoPadre.CriaOrPropId, False)
                    .Item("tede_clie_id") = intVendClienteId
                    .Item("tede_imcr_id") = DBNull.Value
                    .Item("tede_auim_id") = DBNull.Value
                    .Item("tede_raza_id") = usrProductoPadre.RazaId

                    If mdsDatos.Tables(mstrTablaDenuncia).Rows.Count = 0 Then
                        .Table.Rows.Add(ldrDatosDenuncia)

                    End If
                End With

            End If


            mdsDatos.AcceptChanges()


            Return mdsDatos
        End Function


        Public Function mGuardarDatosPadre(ByVal pusrProd As SRA.usrProdDeriv, _
            ByVal pstrId As String, ByVal pbooCierre As Boolean, ByVal pSexo As Integer) As DataSet

            Dim ldsDatos As DataSet
            Dim lstrId As String
            Dim ldrProd As DataRow
            Dim ldrNume As DataRow

            ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, SRA_Neg.Constantes.gTab_Productos, "", "")
            With ldsDatos
                .Tables(0).TableName = SRA_Neg.Constantes.gTab_Productos
                .Tables(1).TableName = SRA_Neg.Constantes.gTab_ProductosAnalisis
                .Tables(2).TableName = SRA_Neg.Constantes.gTab_ProductosNumeros
            End With

            If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select.GetUpperBound(0) = -1 Then
                ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).NewRow
                ldrProd.Table.Rows.Add(ldrProd)
            Else
                ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select()(0)
            End If

            'productos
            With ldrProd
                lstrId = clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
                .Item("prdt_id") = lstrId
                .Item("prdt_raza_id") = pusrProd.cmbProdRazaExt.Valor
                'usrPrdLocal.usrProducto.cmbProdRaza.Valor

                .Item("prdt_sexo") = pSexo

                .Item("prdt_nomb") = pusrProd.txtProdNombExt.Valor
                '.Item("prdt_px") = txtPX.Text
                '.Item("prdt_naci_fecha") = txtFechaNac.Fecha
                .Item("prdt_tran_fecha") = IIf(txtFechaExportacion.Fecha.Length > 0, txtFechaExportacion.Fecha, DBNull.Value)
                .Item("prdt_rp") = IIf(Trim(pusrProd.txtRPExt.Text) = "", DBNull.Value, Trim(pusrProd.txtRPExt.Text))
                .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", "E", .Item("prdt_ndad"))
                .Item("prdt_ori_asoc_id") = pusrProd.cmbProdAsocExt.Valor
                .Item("prdt_ori_asoc_nume") = pusrProd.txtCodiExt.Valor
                '.Item("prdt_ori_asoc_nume_lab") = pusrProd.txtCodiExtLab.Valor
                .Item("generar_numero") = pbooCierre

            End With



            Return ldsDatos
        End Function


        'REQUISISTOS
        Private Sub mActualizarRequ(ByVal pbooAlta As Boolean)
            Try
                mGuardarDatosRequ(pbooAlta)
                mLimpiarRequ()
                mConsultarRequ()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mValidarRequ(ByVal pbooAlta As Boolean)
            If cmbRequRequ.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
            End If
            If pbooAlta Then
                If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                    Throw New AccesoBD.clsErrNeg("Requisito existente.")
                End If
            End If
        End Sub
        Private Sub mValidarModiRequ(ByVal pbooAlta As Boolean)
            If grdRequ.Items.Count = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
            End If
            If pbooAlta Then
                If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                    Throw New AccesoBD.clsErrNeg("Requisito existente.")
                End If
            End If
        End Sub
        Private Sub mGuardarDatosRequ(ByVal pbooAlta As Boolean)
            Dim ldrDatos As DataRow
            If hdnOperacion.Text = "M" Then
                mValidarModiRequ(pbooAlta)
            Else
                mValidarRequ(pbooAlta)
            End If

            If hdnRequId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                ldrDatos.Item("trad_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRequisitos), "trad_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)
            End If

            With ldrDatos
                .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("trad_requ_id") = cmbRequRequ.Valor
                .Item("trad_pend") = chkRequPend.Checked
                .Item("trad_obli") = True
                .Item("trad_manu") = IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
                .Item("trad_baja_fecha") = DBNull.Value
                .Item("trad_audi_user") = Session("sUserId").ToString()
                .Item("_requ_desc") = cmbRequRequ.SelectedItem.Text
                .Item("_pend") = IIf(chkRequPend.Checked, "S�", "No")
                .Item("_manu") = "S�"
                .Item("_estado") = "Activo"

                If hdnRequId.Text = "" Then
                    .Table.Rows.Add(ldrDatos)
                End If
            End With

        End Sub
        'DOCUMENTOS
        Private Sub mActualizarDocu()
            Try
                mGuardarDatosDocu()
                mLimpiarDocu()
                mConsultarDocu()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mValidarDocu()
            If txtDocuDocu.Valor.ToString = "" And filDocuDocu.Value = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
            End If

            If txtDocuObse.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
            End If
        End Sub
        Private Sub mGuardarDatosDocu()
            Dim ldrDatos As DataRow
            Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_tram_docu_path")

            mValidarDocu()

            If hdnDocuId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).NewRow
                ldrDatos.Item("trdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocumentos), "trdo_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)
            End If

            With ldrDatos
                .Item("trdo_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

                If filDocuDocu.Value <> "" Then
                    .Item("trdo_path") = filDocuDocu.Value
                Else
                    .Item("trdo_path") = txtDocuDocu.Valor
                End If
                If Not .IsNull("trdo_path") Then
                    .Item("trdo_path") = .Item("trdo_path").Substring(.Item("trdo_path").LastIndexOf("\") + 1)
                End If
                .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(.Item("trdo_id"), "-", "m") + "__" + .Item("trdo_path")
                .Item("trdo_refe") = txtDocuObse.Valor
                .Item("trdo_baja_fecha") = DBNull.Value
                .Item("trdo_audi_user") = Session("sUserId").ToString()
                .Item("_estado") = "Activo"

                SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocuDocu)

                If hdnDocuId.Text = "" Then
                    .Table.Rows.Add(ldrDatos)
                End If
            End With
        End Sub
        'OBSERVACIONES
        Private Sub mActualizarObse()
            Try
                mGuardarDatosObse()
                mLimpiarObse()
                mConsultarObse()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub mValidarObse()
            If txtObseObse.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Observaci�n.")
            End If
            If txtObseFecha.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de la Observaci�n.")
            End If
        End Sub
        Private Sub mGuardarDatosObse()
            Dim ldrDatos As DataRow
            mValidarObse()

            If hdnObseId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).NewRow
                ldrDatos.Item("trao_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaObservaciones), "trao_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)
            End If

            With ldrDatos
                .Item("trao_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                If cmbObseRequ.Valor.ToString = "" Then
                    .Item("trao_trad_id") = DBNull.Value
                Else
                    .Item("trao_trad_id") = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_requ_id=" + cmbObseRequ.Valor.ToString)(0).Item("trad_id")
                End If
                .Item("_requ_desc") = IIf(cmbObseRequ.Valor.ToString = "", "COMENTARIO GENERAL", cmbObseRequ.SelectedItem.Text)
                .Item("_trad_requ_id") = IIf(cmbObseRequ.Valor.ToString = "", "0", cmbObseRequ.Valor)
                .Item("trao_obse") = txtObseObse.Valor
                .Item("trao_fecha") = txtObseFecha.Fecha
                .Item("trao_audi_user") = Session("sUserId").ToString()

                If hdnObseId.Text = "" Then
                    .Table.Rows.Add(ldrDatos)
                End If
            End With
        End Sub
        'PRODUCTOS
        Private Function mGuardarDatosProd() As DataSet
            Dim ldsDatosProd As DataSet
            Dim ldrDatos As DataRow

            'usrProd.NdadProd = "E"
            'usrProd.NdadPadres = "E"

            'If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen And _
            'usrProd.usrProductoExt.txtRPExt.Text <> "" Then

            '    If usrProd.usrProductoExt.txtRPExt.Text.Substring(0, 3).ToUpper() <> "SI " Then
            '        usrProd.usrProductoExt.txtRPExt.Text = "SI " + usrProd.usrProductoExt.txtRPExt.Text
            '    End If
            'End If

            'ldsDatosProd = usrProd.GuardarDatos(mintTtraId)

            Return (ldsDatosProd)
        End Function
#End Region

#Region "Eventos de Controles"
        Private Sub mShowTabs(ByVal origen As Byte)
            panDato.Visible = True
            panBotones.Visible = True
            panCabecera.Visible = False
            panRequ.Visible = False
            panDocu.Visible = False
            panObse.Visible = False

            lnkCabecera.Font.Bold = False
            lnkRequ.Font.Bold = False
            lnkDocu.Font.Bold = False
            lnkObse.Font.Bold = False

            lblTitu.Text = ""
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())

            If origen <> 1 And usrProductoPadre.Valor <> 0 Then
                lblTitu.Text = "Padre de Embrion: " & _
             oRaza.GetRazaDescripcionById(usrProductoPadre.RazaId) & _
             " - " & _
             usrProductoPadre.txtProdNombExt.Valor.ToString
            End If

            Select Case origen
                Case 1
                    panCabecera.Visible = True
                    lnkCabecera.Font.Bold = True
                    lblTitu.Text = hdnTramNro.Text
                Case 2
                    panRequ.Visible = True
                    lnkRequ.Font.Bold = True
                Case 3
                    panDocu.Visible = True
                    lnkDocu.Font.Bold = True
                Case 4
                    mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = "trad_baja_fecha is null"

                    cmbObseRequ.DataTextField = "_requ_desc"
                    cmbObseRequ.DataValueField = "trad_requ_id"
                    cmbObseRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
                    cmbObseRequ.DataBind()
                    cmbObseRequ.Items.Insert(0, "(Seleccione)")
                    cmbObseRequ.Items(0).Value = ""
                    cmbObseRequ.Valor = ""

                    mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = ""

                    panObse.Visible = True
                    lnkObse.Font.Bold = True

            End Select
        End Sub
        Private Sub mLimpiarPersonas(ByVal pstrTabla As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
            For Each lDr As DataRow In mdsDatos.Tables(pstrTabla).Select()
                If lDr.Item("trpe_id") > 0 Then
                    lDr.Delete()
                Else
                    mdsDatos.Tables(pstrTabla).Rows.Remove(lDr)
                End If
            Next

            If mdsDatos.Tables(pstrTabla).Select.GetUpperBound(0) = -1 Then pctrHdnOrig.Text = ""

            pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
            pgrdGrilla.DataBind()
        End Sub
        'Private Sub mCargarPersonas(ByVal pstrTabla As String, ByVal pstrCliente As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
        '   Dim ldsDatos As DataSet

        '   mstrCmd = "@clie_id=" & pstrCliente
        '   If usrProd.usrProductoExt.Valor.ToString <> "" Then
        '      mstrCmd += ",@prdt_id=" & IIf(pstrTabla = mstrTablaCompradores, "null", usrProd.usrProductoExt.Valor.ToString)
        '   End If

        '   ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "tramites_personas", mstrCmd)

        '   mLimpiarPersonas(pstrTabla, pgrdGrilla, pctrHdnOrig)

        '   For Each lDr As DataRow In ldsDatos.Tables(0).Select()
        '      lDr.Item("trpe_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(pstrTabla), "trpe_id")
        '      mdsDatos.Tables(pstrTabla).ImportRow(lDr)
        '   Next

        '   If pstrTabla = mstrTablaCompradores Then
        '      If mdsDatos.Tables(pstrTabla).Select().GetUpperBound(0) = 0 Then
        '         Dim ldr As DataRow = mdsDatos.Tables(pstrTabla).Select().GetValue(0)
        '         ldr.Item("trpe_porc") = 100
        '      End If
        '   End If

        '   pctrHdnOrig.Text = pstrCliente

        '   pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
        '   pgrdGrilla.DataBind()
        '   Session(mSess(mstrTablaEmbrionesStock)) = mdsDatos
        '   mLimpiarComp()
        'End Sub


        Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub
        Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
        End Sub
        Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub
        Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub
        'REQUISISTOS
        Private Sub btnAltaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRequ.Click
            mActualizarRequ(True)
        End Sub
        Private Sub btnLimpRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRequ.Click
            mLimpiarRequ()
        End Sub
        Private Sub btnBajaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRequ.Click
            Try
                For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaObservaciones).Select("trao_trad_id=" + hdnRequId.Text)
                    odrDeta.Delete()
                Next

                mConsultarObse()
                mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0).Delete()
                grdRequ.CurrentPageIndex = 0
                mConsultarRequ()
                mLimpiarRequ()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnModiRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRequ.Click
            mActualizarRequ(False)
        End Sub
        'DOCUMENTOS
        Private Sub btnAltaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocu.Click
            mActualizarDocu()
        End Sub
        Private Sub btnLimpDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocu.Click
            mLimpiarDocu()
        End Sub
        Private Sub btnBajaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocu.Click
            Try
                mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0).Delete()
                grdDocu.CurrentPageIndex = 0
                mConsultarDocu()
                mLimpiarDocu()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnModiDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocu.Click
            mActualizarDocu()
        End Sub
        'OBSERVACIONES
        Private Sub btnAltaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaObse.Click
            mActualizarObse()
        End Sub
        Private Sub btnLimpObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpObse.Click
            mLimpiarObse()
        End Sub
        Private Sub btnBajaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaObse.Click
            Try
                mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0).Delete()
                grdObse.CurrentPageIndex = 0
                mConsultarObse()
                mLimpiarObse()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Private Sub btnModiObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiObse.Click
            mActualizarObse()
        End Sub

        Private Sub btnNuevoImpEmb_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevoImpEmb.Click
            mAgregar()
        End Sub
        Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub
        Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
            mLimpiarFiltros()
        End Sub
        Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
            grdDato.CurrentPageIndex = 0
            mConsultar(False)
        End Sub
        Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
            mShowTabs(1)
        End Sub
        Private Sub lnkRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRequ.Click
            mShowTabs(2)
        End Sub
        Private Sub lnkDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
            mShowTabs(3)
        End Sub
        Private Sub lnkObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkObse.Click
            mShowTabs(4)
        End Sub

#End Region

#Region "Opciones de POP"
        Private Sub mCargarPlantilla()
            If mstrTrapId <> "" Then

                Dim lDs As New DataSet
                lDs = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Tramites_Plantilla, "@SinBaja=1,@trap_id=" + mstrTrapId)
                Dim ldrDatos As DataRow

                mdsDatos.Tables(mstrTablaRequisitos).Clear()

                For Each ldrOri As DataRow In lDs.Tables(1).Select
                    ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                    With ldrDatos
                        .Item("trad_id") = clsSQLServer.gObtenerId(.Table, "trad_id")
                        .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                        .Item("trad_baja_fecha") = DBNull.Value
                        .Item("trad_requ_id") = ldrOri.Item("trpd_requ_id")
                        .Item("trad_obli") = ldrOri.Item("trpd_obli")
                        .Item("trad_pend") = True
                        .Item("trad_manu") = False
                        .Item("_requ_desc") = ldrOri.Item("_requisito")
                        .Item("_obli") = ldrOri.Item("_obligatorio")
                        .Item("_pend") = "S�"
                        .Item("_manu") = "No"
                        .Item("_estado") = ldrOri.Item("_estado")

                        .Table.Rows.Add(ldrDatos)
                    End With
                Next

                Dim lstrRazas As String = ""
                Dim lstrEspecies As String = ""

                For Each ldrOri As DataRow In lDs.Tables(2).Select
                    With ldrOri
                        If .IsNull("trdr_raza_id") Then
                            If lstrEspecies.Length > 0 Then lstrEspecies += ","
                            lstrEspecies += .Item("trdr_espe_id").ToString
                        Else
                            If lstrRazas.Length > 0 Then lstrRazas += ","
                            lstrRazas += .Item("trdr_raza_id").ToString
                        End If
                    End With
                Next

                'usrProd.usrProductoExt.FiltroRazas = "@raza_ids = " & IIf(lstrRazas = "", "null", "'" & lstrRazas & "'") & ",@raza_espe_ids = " & IIf(lstrEspecies = "", "null", "'" & lstrEspecies & "'")
                'usrProd.usrProductoExt.mCargarRazas()

                mConsultarRequ()
            End If
        End Sub
        Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
            Try
                mstrTrapId = hdnDatosPop.Text
                mAgregar()
                hdnDatosPop.Text = ""
                Session("mstrTrapId") = mstrTrapId

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

        Private Sub hdnDatosTEPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosTEPop.TextChanged
            Try
                Dim dtEmbrionStock As DataTable
                Dim oEmbrion As New SRA_Neg.EmbrionStock(mstrConn, Session("sUserId").ToString())
                hdnTEId.Text = ""
                txtTeDesc.Text = ""
                txtCantEmbrSemen.Text = ""
                usrProductoPadre.Valor = ""

                If (hdnDatosTEPop.Text <> "") Then
                    hdnTEId.Text = hdnDatosTEPop.Text
                    dtEmbrionStock = oEmbrion.GetDescripcionTE(hdnDatosTEPop.Text)
                    If dtEmbrionStock.Rows.Count > 0 Then
                        txtTeDesc.Text = "Denuncia TE Nro.: " + Convert.ToString(dtEmbrionStock.Rows(0).Item("tede_nume"))
                        txtCantEmbrSemen.Text = dtEmbrionStock.Rows(0).Item("tede_embr")
                        usrProductoPadre.Valor = dtEmbrionStock.Rows(0).Item("tede_pad1_prdt_id")
                    Else
                        txtTeDesc.Text = "Denuncia TE Nro.: " + "0"
                        txtCantEmbrSemen.Text = "0"
                    End If


                    hdnDatosTEPop.Text = ""
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Function MessageBox(ByRef oPage As Page, ByVal sAviso As String)
            If sAviso.Trim.Length > 0 Then
                Dim s As String = "alert('" & sAviso & "') "
                oPage.RegisterStartupScript("OnLoad", s)
            End If
        End Function

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Private Sub btnSemen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSemen.Click

        End Sub

    End Class
End Namespace
