Namespace SRA

Partial Class EstadosSocios
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"

   Public mstrCmd As String
   Public mstrTabla As String = "Estados"
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mConsultar()
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mEstablecerPerfil()
   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then

            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      mstrCmd = "exec " + mstrTabla + "_consul"
      mstrCmd += " @esta_esti_id=" + CType(SRA_Neg.Constantes.EstaTipos.EstaTipo_Socios, String)

      clsWeb.gCargarDataGrid((mstrConn), mstrCmd, grdDato)
      mMostrarPanel(False)
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnModi.Enabled = Not (pbooAlta)
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbRelaEsta, "N", SRA_Neg.Constantes.EstaTipos.EstaTipo_Socios)
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = E.Item.Cells(1).Text
      mCargarDatos()
      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtDesc.Text = ""
      chkDeve.Checked = True
      chkRequ.Checked = True
      chkInte.Checked = True
      cmbRelaEsta.Limpiar()
      mSetearEditor(True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      mEstablecerPerfil()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Alta()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("esta_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("esta_desc") = txtDesc.Text
         .Item("esta_soci_deve") = chkDeve.Checked
         .Item("esta_reque_apro") = chkRequ.Checked
         .Item("esta_soci_inte") = chkInte.Checked
                .Item("esta_rela_esta_id") = IIf(cmbRelaEsta.Valor.Trim.Length > 0, cmbRelaEsta.Valor, DBNull.Value)

         .Item("_deve") = mTranslate(chkDeve.Checked)
         .Item("_requ") = mTranslate(chkRequ.Checked)
         .Item("_inte") = mTranslate(chkInte.Checked)
      End With

      Return ldsEsta
   End Function

   Function mTranslate(ByVal pOk As Boolean) As String
      Dim str As String = "No"
      If (pOk) Then
         str = "Si"
      End If
      Return str
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

        Private Function mCargarDatos() As DataSet

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

            With ldsEsta.Tables(0).Rows(0)
                txtDesc.Text = .Item("esta_desc")
                chkDeve.Checked = .Item("esta_soci_deve")
                chkRequ.Checked = .Item("esta_reque_apro")
                chkInte.Checked = .Item("esta_soci_inte")
                cmbRelaEsta.Valor = IIf(.Item("esta_rela_esta_id").ToString.Length > 0, .Item("esta_rela_esta_id"), String.Empty)
            End With

            Return ldsEsta
        End Function
#End Region

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
End Class
End Namespace
