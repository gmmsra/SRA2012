<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PlanDePagos" CodeFile="PlanDePagos.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Plan de pagos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function mSetearConcepto()
		{
			var strRet1 = '';
			var strRet2 = '';
			var lstrCta1 = '';
			var lstrCta2 = '';
			if (document.all('cmbCuotConc').value!='')
				strRet1 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbMatrConc').value, "_cuenta");
			if (document.all('cmbCuotConc').value!='')
				strRet2 = LeerCamposXML("conceptos", "@conc_id="+document.all('cmbCuotConc').value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet1!='')
				lstrCta1 = strRet1.substring(0,1);
			if (strRet2!='')
				lstrCta2 = strRet2.substring(0,1);
			document.all('hdnCta').value = lstrCta1;
			if (lstrCta1=='' || lstrCta1=='1' || lstrCta1=='2' || lstrCta1=='3'
				|| lstrCta2=='' || lstrCta2=='1' || lstrCta2=='2' || lstrCta2=='3'
				|| lstrCta1 != lstrCta2)
			{
				document.all('cmbCcos').disabled = true;
				document.all('txtcmbCcos').disabled = true;
				document.all('cmbCcos').innerHTML = '';
				document.all('txtcmbCcos').value = '';
			}
			else
			{
				document.all('cmbCcos').disabled = false;
				document.all('txtcmbCcos').disabled = false;
				LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta1, "cmbCcos", "N");
			}
		}
		function mSetearText()
		{
		 var chek;
		 	if (document.all["chkMatri"].checked == true) 
			 {
		      chek = 0;
		     }else
		     {
		      chek = 1;
		     } 	  
			
              document.all["txtMonto"].disabled = chek;
	   }

		function mCalcularDescuentos()
		{
			if(NumDecimal(document.all["txtImp"].value * (document.all["hdnDeauImpo"].value / 100),2)==0)
				{
				document.all["txtDeauImpo"].value="";
				document.all["txtSociImpo"].value="";
				document.all["txt1SociImpo"].value="";
				document.all["txt2SociImpo"].value="";
				}
			else
				{
				document.all["txtDeauImpo"].value=NumDecimal(document.all["txtImp"].value * (document.all["hdnDeauImpo"].value / 100),2);
				document.all["txtSociImpo"].value=NumDecimal(document.all["txtImp"].value * (document.all["hdnSociImpo"].value / 100),2);
				document.all["txt1SociImpo"].value=NumDecimal(document.all["txtImp"].value * (document.all["hdn1SociImpo"].value / 100),2);
				document.all["txt2SociImpo"].value=NumDecimal(document.all["txtImp"].value * (document.all["hdn2SociImpo"].value / 100),2);
				}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Plan de pagos</asp:label></TD>
								</TR>
								<TR>
									<TD height="10"></TD>
									<TD vAlign="bottom" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<FooterStyle CssClass="footer"></FooterStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="9px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="papl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="papl_desc" ReadOnly="True" HeaderText="Descripci&#243;n">
													<HeaderStyle Width="50%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_matricula" HeaderText="Matr&#237;cula"></asp:BoundColumn>
												<asp:BoundColumn DataField="papl_matr_monto" HeaderText="Monto"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cuotas" HeaderText="Cuotas"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="papl_cuot_cant" HeaderText="papl_cuot_cant"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2">
										<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ToolTip="Agregar un Nuevo Plan de Pago"
														ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
														CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
														ForeColor="Transparent" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
												<TD align="right"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR height="10">
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="middle">
										<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD align="left"></TD>
												<TD></TD>
												<TD align="center" width="50"></TD>
											</TR>
										</TABLE>
									</TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="120px" CausesValidation="False" Height="21px"> Plan de pago</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCuota" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" CausesValidation="False" Height="21px"> Cuotas</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderWidth="1px" BorderStyle="Solid"
												Height="116px" Visible="False">
												<P align="right">
													<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
														border="0">
														<TR>
															<TD>
																<P></P>
															</TD>
															<TD height="5">
																<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
															<TD vAlign="top" align="right">&nbsp;
																<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="3">
																<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																	<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="HEIGHT: 16px" align="right" width="30%">
																				<asp:Label id="lblDesc" runat="server" cssclass="titulo" Width="79px">Descripci�n:</asp:Label>&nbsp;
																			</TD>
																			<TD style="HEIGHT: 16px" colSpan="2" height="16">
																				<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="299px" Obligatorio="True"
																					apeDesc></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 16px" align="right" width="30%">
																				<asp:checkbox id="chkMatri" onclick=" mSetearText();" Runat="server" CssClass="titulo" Text="Matr�cula"></asp:checkbox></TD>
																			<TD align="left" background="imagenes/formfdofields.jpg" colSpan="2" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 16px" align="right" width="30%">
																				<asp:Label id="lblMonto" runat="server" cssclass="titulo" Width="79px">Monto:</asp:Label>&nbsp;
																			</TD>
																			<TD style="HEIGHT: 16px" colSpan="2" height="16">
																				<cc1:numberbox id="txtMonto" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																					MaxValor="1000" EsDecimal="True" height="18px"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="3" height="16"></TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblMatrConc" runat="server" cssclass="titulo">Concepto de Matr�cula:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<TABLE cellSpacing="0" cellPadding="0" border="0">
																					<TR>
																						<TD>
																							<cc1:combobox class="combo" id="cmbMatrConc" runat="server" cssclass="cuadrotexto" Width="300px"
																								Obligatorio="True" MostrarBotones="False" filtra="true" NomOper="cuentas_cargar" TextMaxLength="5"
																								onchange="javascript:mSetearConcepto();"></cc1:combobox>
																						<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																								onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbMatrConc','Conceptos','@conc_grava=0');"
																								alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblCuotConc" runat="server" cssclass="titulo">Concepto de Cuotas:</asp:Label>&nbsp;</TD>
																			<TD colSpan="2">
																				<TABLE cellSpacing="0" cellPadding="0" border="0">
																					<TR>
																						<TD>
																							<cc1:combobox class="combo" id="cmbCuotConc" runat="server" cssclass="cuadrotexto" Width="300px"
																								Obligatorio="True" MostrarBotones="False" filtra="true" NomOper="cuentas_cargar" TextMaxLength="5"
																								onchange="javascript:mSetearConcepto();"></cc1:combobox></TD>
																						<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																								onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbCuotConc','Conceptos','@conc_grava=0');"
																								alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right">
																				<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
																			<TD colSpan="3">
																				<TABLE cellSpacing="0" cellPadding="0" border="0">
																					<TR>
																						<TD>
																							<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"
																								MostrarBotones="False" filtra="true" NomOper="ccentros_cargar" TextMaxLength="6"></cc1:combobox></TD>
																						<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																								onclick="mSetearConcepto(); var lstrFil='-1'; if (document.all('hdnCta').value != '') lstrFil = document.all('hdnCta').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos','Centros de Costos','@cuenta='+lstrFil);"
																								alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 100%" colSpan="3">
																<asp:panel id="PanCuota" runat="server" cssclass="titulo" Width="100%" Visible="False">
																	<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2">
																				<asp:datagrid id="grdCuota" runat="server" width="100%" AutoGenerateColumns="False" OnEditCommand="mEditarDatosCuota"
																					OnPageIndexChanged="grdCuota_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Left" AllowPaging="True" BorderWidth="1px" BorderStyle="None">
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="plco_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="plco_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																						<asp:BoundColumn DataField="plco_impo" ReadOnly="True" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																							ItemStyle-HorizontalAlign="Right" HeaderText="Importe $" HeaderStyle-Width="30%"></asp:BoundColumn>
																						<asp:BoundColumn DataField="plco_deau_impo" ReadOnly="True" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																							ItemStyle-HorizontalAlign="Right" HeaderText="Descuento D�bito Autom�tico $"></asp:BoundColumn>
																						<asp:BoundColumn DataField="plco_soci_impo" ReadOnly="True" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																							ItemStyle-HorizontalAlign="Right" HeaderText="Descuento Activo $"></asp:BoundColumn>
																						<asp:BoundColumn DataField="plco_1soci_impo" ReadOnly="True" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																							ItemStyle-HorizontalAlign="Right" HeaderText="Descuento Adherente $"></asp:BoundColumn>
																						<asp:BoundColumn DataField="plco_2soci_impo" ReadOnly="True" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																							ItemStyle-HorizontalAlign="Right" HeaderText="Descuento Menor $"></asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="16"></TD>
																		</TR>
																		<TR>
																			<TD align="right" width="30%">
																				<asp:Label id="lblFCta" runat="server" cssclass="titulo">Fecha cuota:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:DateBox id="txtFCta" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="False"
																					AceptaNull="True"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" width="30%">
																				<asp:Label id="lblImp" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:numberbox id="txtImp" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																					MaxValor="1000" EsDecimal="True" height="18px" onchange="mCalcularDescuentos()"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" width="30%">
																				<asp:Label id="lblDeauImpo" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:numberbox id="txtDeauImpo" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																					MaxValor="1000" EsDecimal="True" height="18px" AceptaNull="false" Enabled="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" width="30%">
																				<asp:Label id="lblSociImpo" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:numberbox id="txtSociImpo" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																					MaxValor="1000" EsDecimal="True" height="18px" AceptaNull="false" Enabled="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" width="30%">
																				<asp:Label id="lbl1SociImpo" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:numberbox id="txt1SociImpo" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																					MaxValor="1000" EsDecimal="True" height="18px" AceptaNull="false" Enabled="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" width="30%">
																				<asp:Label id="lbl2SociImpo" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:numberbox id="txt2SociImpo" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																					MaxValor="1000" EsDecimal="True" height="18px" AceptaNull="false" Enabled="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="center" colSpan="2" height="30">
																				<asp:Button id="btnAltaCuota" runat="server" cssclass="boton" Width="100px" Text="Agregar Cuota"></asp:Button>&nbsp;
																				<asp:Button id="btnBajaCuota" runat="server" cssclass="boton" Width="100px" Text="Eliminar Cuota"></asp:Button>&nbsp;
																				<asp:Button id="btnModiCuota" runat="server" cssclass="boton" Width="100px" Text="Modificar Cuota"></asp:Button>&nbsp;
																				<asp:Button id="btnLimpCuota" runat="server" cssclass="boton" Width="100px" Text="Limpiar Cuota"></asp:Button>&nbsp;
																				<asp:Button id="btnGeneCuota" runat="server" cssclass="boton" Width="100px" Text="Generar Cuotas"></asp:Button></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
													</TABLE>
												</P>
											</asp:panel>
											<ASP:PANEL id="panBoto" Runat="server" Visible="False">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
										</DIV>
									</TD>
								</TR>
								<TR>
									<TD align="center" height="15"></TD>
								</TR>
							</TBODY>
						</TABLE>
					</td> <!--- FIN CONTENIDO --->
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:Label id="lblCtasAuto" runat="server" cssclass="titulo"></asp:Label>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCuotaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDeauImpo" runat="server"></asp:textbox>
				<asp:textbox id="hdnSociImpo" runat="server"></asp:textbox>
				<asp:textbox id="hdn1SociImpo" runat="server"></asp:textbox>
				<asp:textbox id="hdn2SociImpo" runat="server"></asp:textbox>
				<asp:textbox id="hdnCta" runat="server"></asp:textbox>
				<asp:textbox id="hdnPosic" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
			if (document.all["editar"]!= null)
				document.location='#editar';
			if (document.all["txtDesc"]!= null)
				document.all["txtDesc"].focus();
		
		</SCRIPT>
	</BODY>
</HTML>
