<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DeudaAgro" CodeFile="DeudaAgro.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Consulta de Deuda</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
							<TR>
								<TD vAlign="top" align="right" width="100%" colSpan="6">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"></asp:imagebutton></TD>
							</TR>
							<tr>
								<td colSpan="6">
									<table width="100%">
										<TR>
											<TD align="right" style="WIDTH: 107px"><asp:label id="lblSoci" runat="server" cssclass="titulo">Socio:</asp:label></TD>
											<TD align="left"><UC1:CLIE id="usrSoci" runat="server" autopostback="true" FilDocuNume="True" MuestraDesc="False"
													FilSociNume="True" FilClieNume="True" Saltos="1,1,1" Tabla="Socios" FilCUIT="True" width="250"></UC1:CLIE></TD>
										</TR>
										<tr>
											<TD align="right" style="WIDTH: 107px"><asp:label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:label></TD>
											<TD><cc1:datebox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" autopostback="true" Width="68px"
													Enabled="False"></cc1:datebox>&nbsp;&nbsp;<asp:label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:label>&nbsp;
												<cc1:datebox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" autopostback="true" Width="68px"
													Enabled="False"></cc1:datebox></TD>
										</tr>
										<tr>
											<td align="left" colspan="2">
												<asp:Label Runat="server" ID="lblDeudaAnterior" CssClass="titulo">Deuda Anterior:</asp:Label>&nbsp;
												<asp:TextBox Runat="server" ID="txtDeudaAnterior" Width="72px" Height="20px" CssClass="cuadrotexto"
													ReadOnly="True" ForeColor="Red" Font-Bold="True"></asp:TextBox>&nbsp;
												<asp:Label id="lblBimAnteDesc" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<TR height="100%">
								<TD vAlign="top" colSpan="6"><asp:datagrid id="grdConsulta" runat="server" width="100%" BorderStyle="None" AllowPaging="True"
										BorderWidth="1px" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
										PageSize="100" ItemStyle-Height="5px" AutoGenerateColumns="False">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server" onclick="chkSelCheck(this);"></asp:CheckBox>
													<DIV runat="server" id="divCuot" style="DISPLAY: none"><%#(100*DataBinder.Eval(Container, "DataItem.cuota"))%></DIV>
													<DIV runat="server" id="divPeti" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.peti_id")%></DIV>
													<DIV runat="server" id="divPerio" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.perio")%></DIV>
													<DIV runat="server" id="divBimDesc" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem._concepto")%></DIV>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="comp_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="anio"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="perio"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="peti_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="_concepto" HeaderText="Concepto"></asp:BoundColumn>
											<asp:BoundColumn DataField="cues_perio_inic_fecha" HeaderText="Inicia" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="cues_perio_fina_fecha" HeaderText="Finaliza" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="cuota" HeaderText="Cuota" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_chk" HeaderText="Seleccion" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 390px; HEIGHT: 24px" align="left" width="390" colSpan="4" rowSpan="1">
									<asp:label id="lblTotalTotal" runat="server" cssclass="titulo">Total a Ajustar:</asp:label>
									<CC1:NUMBERBOX id="txtTotalTotal" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="50px"></CC1:NUMBERBOX>&nbsp;&nbsp;
									<asp:Label id="lblBimDesc" runat="server" cssclass="titulo"></asp:Label></TD>
								<TD style="HEIGHT: 24px" align="left" width="25%" colSpan="2">&nbsp;</TD>
							</TR>
							<TR>
								<TD style="WIDTH: 390px" vAlign="middle" align="left" colSpan="2" height="40"><asp:imagebutton id="btnList" runat="server" ToolTip="Imprimir" ImageUrl="./imagenes/prints1.GIF"
										Visible="False"></asp:imagebutton></TD>
								<TD vAlign="middle" align="right" colSpan="4" height="40">
									<asp:button id="btnAceptar" runat="server" cssclass="boton" Width="80px" Text="Aceptar"></asp:button></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="hdnBimDesc" runat="server"></ASP:TEXTBOX></DIV>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="hdnSele" runat="server"></ASP:TEXTBOX></DIV>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="hdnBimAnteDesc" runat="server"></ASP:TEXTBOX></DIV>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="hdnSociAnte" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
	function chkSelCheck(pChk)
	{
		var lintPerio;
		var lintPeti;
		var dCuot=0;
		var dAnteCuot=0;
		var dTotal=0;
		var dAnteTotal=0;
		var bAnte=true;
		
		var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
		var lbooChecked = pChk.checked;

		lintPerio = eval(document.all(pChk.id.replace("chkSel","divPerio")).innerText);
		lintPeti = eval(document.all(pChk.id.replace("chkSel","divPeti")).innerText);

		if(lintPerio==1&&lintPeti==8)
		{
			lintFil+=1;
			if (document.all("grdConsulta__ctl" + lintFil.toString() + "_chkSel")!=null)
				document.all("grdConsulta__ctl" + lintFil.toString() + "_chkSel").checked = lbooChecked;
		}

		if(lintPerio==2&&lintPeti==8) //&&!lbooChecked
		{
			lintFil-=1;
			document.all("grdConsulta__ctl" + lintFil.toString() + "_chkSel").checked = lbooChecked;
		}
	
		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				//if (fila>lintFil)
					//document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked = false;
				
				//if((lbooChecked)&&(fila<=lintFil))
					//document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked = lbooChecked;


				if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
				{
					dCuot += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText.replace(",",""));
					bAnte = false;
				}
				else
				{
					if (bAnte)
						dAnteCuot += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText.replace(",",""));
				}				
			}
		}

		dTotal = dCuot;
		dTotal = dTotal.toString();
		dAnteTotal = dAnteCuot;
		dAnteTotal = dAnteTotal.toString();
		
		if (dTotal==0)
			document.all("txtTotalTotal").value = "0.00"
		else
			document.all("txtTotalTotal").value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2 , dTotal.length );			
			
		if (dAnteTotal==0)
			document.all("txtDeudaAnterior").value = "0.00"
		else
			document.all("txtDeudaAnterior").value = dAnteTotal.substring(0, dAnteTotal.length - 2) + "." + dAnteTotal.substring(dAnteTotal.length - 2 , dAnteTotal.length );			
			
		mDeterminarPeriodo();
	}
		function mDeterminarPeriodo()
		{
			var lstrBimDesc = '';
			var lstrBimDescPrim = '';
			var lstrBimDescUlti = '';
			
			//var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
			//var lbooChecked = pChk.checked;

			if (document.all("grdConsulta").children(0).children.length > 1)
			{
				for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
				{
					if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
					{
						if (lstrBimDesc == '')
						{
							lstrBimDesc = document.all("grdConsulta__ctl" + fila.toString() + "_divBimDesc").innerText.toUpperCase();
						}
						lstrBimDescUlti = document.all("grdConsulta__ctl" + fila.toString() + "_divBimDesc").innerText.toUpperCase();
					}
					if (lstrBimDescPrim == '')
					{
						lstrBimDescPrim = document.all("grdConsulta__ctl" + fila.toString() + "_divBimDesc").innerText.toUpperCase();
					}
				}
			}
			if (lstrBimDesc != '')
				document.all("lblBimDesc").innerHTML = lstrBimDesc + ' al ' + lstrBimDescUlti;
			else
				document.all("lblBimDesc").innerHTML = '';
			document.all("hdnBimDesc").value = document.all("lblBimDesc").innerHTML;
			if (lstrBimDesc != '')
			{
				var lstrPerio = "BIM";
				if (document.all('hdnBimAnteDesc').value.substring(0,3).toUpperCase() == "SEM")
					lstrPerio = "SEM";					
				var lstrDeudaDesde = document.all('hdnBimAnteDesc').value.substring(3, document.all('hdnBimAnteDesc').value.toUpperCase().lastIndexOf(lstrPerio) - 3).replace("al", "");
				
				var lstrSeleDesde = document.all('lblBimDesc').innerHTML.substring(3, document.all('lblBimDesc').innerHTML.toUpperCase().lastIndexOf(lstrPerio) - 3).replace("al", "");
				lstrSeleDesde = lstrSeleDesde.replace(".", "");				
				var lintAnio = lstrSeleDesde.substring(0, 2);
				var lintBim = lstrSeleDesde.substring(3, 4);				
				lintBim = lintBim*1 - 1;
				if (lintBim == '0')
				{
					if (lstrPerio == "BIM")
						lintBim = '6';
					else
						lintBim = '2';
					lintAnio = lintAnio*1 - 1;					
					if (lintAnio < 0)
						lintAnio = 99;
					if (lintAnio < 10)
						lintAnio = '0'+ lintAnio;
					if (lintAnio == 0)
						lintAnio = '00';
				}
				if (lstrDeudaDesde == "" || lstrDeudaDesde == "SEM")
					lstrDeudaDesde = lstrBimDescPrim;
				else
					lstrDeudaDesde = lstrPerio + "" + lstrDeudaDesde;
				document.all('hdnBimAnteDesc').value = lstrDeudaDesde + " al " + lstrPerio + " " + lintAnio + "/" + lintBim;
				if(document.all('txtDeudaAnterior').value != '0.00')
					document.all('lblBimAnteDesc').innerHTML = document.all('hdnBimAnteDesc').value;
				else
					document.all('lblBimAnteDesc').innerHTML = '';
        	}
        	if (document.all('lblBimDesc').innerHTML.substring(0,3) == 'SEM')	
			{
				document.all('lblBimDesc').innerHTML = mAnualidad(document.all('lblBimDesc').innerHTML);
			}
			if (document.all('lblBimAnteDesc').innerHTML.substring(0,3) == 'SEM')	
			{
				document.all('lblBimAnteDesc').innerHTML = mAnualidad(document.all('lblBimAnteDesc').innerHTML);
			}			
		}	
		function mAnualidad(pDesc)
		{
			if (pDesc == '')
				return('');
			var lstrPrefIni = '20';
			var lstrPrefFin = '20';
			var lstrAnioDesde = pDesc.replace('SEM','');
			var lstrAnioHasta = lstrAnioDesde.substring(lstrAnioDesde.indexOf('/')+3);
			lstrAnioDesde = lstrAnioDesde.substring(0,lstrAnioDesde.indexOf('/'));
			lstrAnioHasta = lstrAnioHasta.replace('SEM','').replace('al ','');
			lstrAnioHasta = lstrAnioHasta.substring(0,lstrAnioHasta.indexOf('/'));
			lstrAnioDesde = lstrAnioDesde.replace(' ','').replace('.','');
			lstrAnioHasta = lstrAnioHasta.replace(' ','').replace('.','');
			if (lstrAnioDesde > '50')
				lstrPrefIni = '19';
			if (lstrAnioHasta > '50')
				lstrPrefFin = '19';
			var lstrDescAnual = lstrPrefIni + pDesc;
			lstrDescAnual = lstrDescAnual.replace('SEM','');
			lstrDescAnual = lstrDescAnual.replace('/1','');
			lstrDescAnual = lstrDescAnual.replace('/2','');
			lstrDescAnual = lstrDescAnual.replace('SEM','');
			lstrDescAnual = lstrDescAnual.replace('/1','');
			lstrDescAnual = lstrDescAnual.replace('/2','');
			lstrDescAnual = lstrDescAnual.replace('al ','al '+lstrPrefFin);
			lstrDescAnual = lstrDescAnual.replace(lstrPrefIni+' ',lstrPrefIni);
			lstrDescAnual = lstrDescAnual.replace(lstrPrefFin+' ',lstrPrefFin);
			lstrDescAnual = lstrDescAnual.replace('.','');
			lstrDescAnual = lstrDescAnual.replace('.','');
			var lstrAnio = lstrDescAnual.replace('al','');
			if (lstrAnio.indexOf(' ') != -1)
			{
				var lstrDesde = lstrAnio.substring(0,lstrAnio.indexOf(' ')).replace(' ','');
				var lstrHasta = lstrAnio.substring(lstrAnio.indexOf(' ')).replace(' ','');
				if (lstrDesde.replace(' ','') == lstrHasta.replace(' ',''))
					lstrDescAnual = lstrDesde.replace(' ','');
			}
			return('Anualidad ' + lstrDescAnual);
		}
		if (document.all("lblBimDesc") != null)
			mDeterminarPeriodo();
		</SCRIPT>
	</BODY>
</HTML>
