<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EventosAdministracion" CodeFile="EventosAdministracion.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Administraci�n de Eventos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Administraci�n de Eventos</asp:label></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" AutoGenerateColumns="False" CellPadding="1"
										GridLines="None" CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page"
										AllowPaging="True" width="100%">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="even_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="even_inic_fecha" HeaderText="Fecha Inicio" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="even_fina_fecha" HeaderText="Fecha Fin" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="even_desc" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="60%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colSpan="2"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
										BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
										ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Mesa de Ex�men"></CC1:BOTONIMAGEN></TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Height="21px" CausesValidation="False" Width="80px"> Evento</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Height="21px" CausesValidation="False" Width="70px"> Categor�as</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											width="100%" Height="116px" Visible="False">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblIniFecha" runat="server" cssclass="titulo">Fecha Inicio:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<cc1:DateBox id="txtIniFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblFechFin" runat="server" cssclass="titulo">Fecha Fin:</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtFinFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblCod" runat="server" cssclass="titulo" Width="62px">Descripci�n:</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="textolibredeshab" Width="360px" Height="39px"
																				Obligatorio="True" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" vAlign="top" align="right">
																			<asp:Label id="lblLugarUno" cssclass="titulo" Runat="server">Lugar 1:</asp:Label></TD>
																		<TD style="HEIGHT: 20px">
																			<CC1:combobox id="cmbLugarUno" runat="server" cssclass="combo" Width="360px"></CC1:combobox>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" vAlign="top" align="right">
																			<asp:Label id="lblLugarDos" cssclass="titulo" Runat="server">Lugar 2:</asp:Label></TD>
																		<TD style="HEIGHT: 14px">
																			<CC1:combobox id="cmbLugarDos" runat="server" cssclass="combo" Width="360px"></CC1:combobox>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblObser" runat="server" cssclass="titulo">Observaciones:</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtObser" runat="server" cssclass="textolibredeshab" Width="360px" Height="54px"
																				Obligatorio="True" EnterPorTab="False" TextMode="MultiLine" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblEnt1" cssclass="titulo" Runat="server">Nombre Entrada:</asp:Label></TD>
																		<TD>
																			<CC1:combobox id="cmbEntradaUno" runat="server" cssclass="combo" Width="240px"></CC1:combobox>&nbsp;
																			<asp:CheckBox id="ChkNume1" Runat="server" Text="Controla Numeraci�n" Checked="True" CssClass="titulo"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD noWrap align="right">
																			<asp:Label id="lblEnt2" cssclass="titulo" Runat="server">Nombre Entrada:</asp:Label></TD>
																		<TD>
																			<CC1:combobox id="cmbEntradaDos" runat="server" cssclass="combo" Width="240px"></CC1:combobox>&nbsp;
																			<asp:CheckBox id="ChkNume2" Runat="server" Text="Controla Numeraci�n" Checked="True" CssClass="titulo"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD noWrap align="right">
																			<asp:Label id="lblEnt3" cssclass="titulo" Runat="server">Nombre Entrada:</asp:Label></TD>
																		<TD>
																			<CC1:combobox id="cmbEntradaTres" runat="server" cssclass="combo" Width="240px"></CC1:combobox>&nbsp;
																			<asp:CheckBox id="ChkNume3" Runat="server" Text="Controla Numeraci�n" Checked="True" CssClass="titulo"></asp:CheckBox></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TableProf" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdCate" runat="server" width="100%" AllowPaging="True" OnPageIndexChanged="grdCate_PageChanged"
																				OnEditCommand="mEditarDatosCate" BorderWidth="1px" HorizontalAlign="Center" CellSpacing="1"
																				GridLines="None" CellPadding="1" AutoGenerateColumns="False" BorderStyle="None" Visible="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="evca_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_categoria" HeaderText="Categor�a">
																						<HeaderStyle Width="40%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="evca_ent1_cant" HeaderText="Cantidad">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="evca_ent2_cant" HeaderText="Cantidad">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="evca_ent3_cant" HeaderText="Cantidad">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" align="right">
																			<asp:Label id="lblCate" runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 20px">
																			<cc1:combobox id="cmbCate" class="combo" runat="server" Width="260px"></cc1:combobox>&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" align="right">
																			<asp:Label id="lblCant1" runat="server" cssclass="titulo">Cantidad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 20px">
																			<cc1:numberbox id="txtCant1" runat="server" cssclass="cuadrotexto" Width="93px" Obligatorio="True"
																				MaxValor="9999999999999" esdecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEntra1" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" align="right">
																			<asp:Label id="lblCant2" runat="server" cssclass="titulo">Cantidad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 20px">
																			<cc1:numberbox id="txtCant2" runat="server" cssclass="cuadrotexto" Width="93px" Obligatorio="True"
																				MaxValor="9999999999999" esdecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEntra2" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" align="right">
																			<asp:Label id="lblCant3" runat="server" cssclass="titulo">Cantidad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 20px">
																			<cc1:numberbox id="txtCant3" runat="server" cssclass="cuadrotexto" Width="93px" Obligatorio="True"
																				MaxValor="9999999999999" esdecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblEntra3" runat="server" cssclass="titulo"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="100px" Text="Agregar Categ."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="100px" Text="Eliminar Categ."></asp:Button>&nbsp;
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="105px" Text="Modificar Categ."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="100px" Text="Limpiar Categ."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnCateId" runat="server"></asp:textbox><asp:textbox id="hdnPage" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtIniFecha"]!= null)
			document.all["txtIniFecha"].focus();
		</SCRIPT>
	</BODY>
</HTML>
