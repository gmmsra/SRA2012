<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ClientesIncob" CodeFile="ClientesIncob.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var vVentanas = new Array(null);
	
		function btnGeneLista_click() 
		{
			var chek;
		 	//if (document.all["chkIncl"].checked == true) 
		      //chek = "0";
			//else
		      chek = "1";

  			gAbrirVentanas("ClientesIncob_FilPop.aspx?tipo=" + document.all("cmbTipo").value + "&incl=" + chek + "&lsin_id=" + document.all("hdnId").value + "&etapa=" + document.all("hdnEtapa").value, 0, "580","400","50","50");
		}

		function usrClie_onchange()
		{
			var sFiltro=""
			document.all("txtImpoClie").value = "";
			document.all("hdnSociId").value = "";
			document.all("txtSociNume").value = "";
			document.all("hdnSociNume").value = "";
			document.all("hdnDeuda").value = "";

			if(document.all("usrClie:txtId").value!="")
			{
				sFiltro = document.all("usrClie:txtId").value + ",0";
				var vsRet = EjecutarMetodoXML("Utiles.ObtenerDatosIncobCliente", sFiltro).split("|");
				if (document.all("cmbTipo").value == "S")
					document.all("txtImpoClie").value = vsRet[3]; // deuda social
				else
					document.all("txtImpoClie").value = vsRet[4]; // deuda ctac	
				
				document.all("hdnSociId").value = vsRet[1];
				document.all("txtSociNume").value = vsRet[2];
				if (document.all("txtSociNume").value == "0")
				{
					var vsRetSoci = LeerCamposXML("clientesX", document.all("usrClie:txtId").value, "soci_id,soci_nume").split("|");
					document.all("hdnSociId").value = vsRetSoci[0];
					document.all("txtSociNume").value = vsRetSoci[1];
				}
				document.all("hdnSociNume").value = document.all("txtSociNume").value;
				document.all("hdnDeuda").value = document.all("txtImpoClie").value;
			}
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD colSpan="3" style="HEIGHT: 25px" vAlign="bottom" height="25" width="100%"><asp:label cssclass="opcion" id="lblTituAbm" runat="server">Clientes Incobrables</asp:label></TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD vAlign="top" colSpan="3" width="100%">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD width="100%"><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False"
																				MaxValor="2099"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD colSpan="3" vAlign="top" align="center" width="100%">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="lsin_id" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="lsin_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="lsin_obse" HeaderText="Observaciones"></asp:BoundColumn>
											<asp:BoundColumn DataField="_esta_desc" HeaderText="Estado"></asp:BoundColumn>
											<asp:BoundColumn DataField="lsin_cierre_fecha" HeaderText="Fecha Cierre" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" width="100%" colspan="3">
									<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar una Nueva Lista de Incobrables"></CC1:BotonImagen>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
											BorderWidth="1px" Visible="False">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD width="15%" align="right">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label></TD>
																		<TD>
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																		<TD align="right">
																			<asp:Label id="lblCierreFecha" runat="server" cssclass="titulo">Fecha de cierre:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtCierreFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
																		<TD style="HEIGHT: 32px" height="32">&nbsp;</TD>
																		<TD style="HEIGHT: 32px" align="right">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 32px" height="32">
																			<cc1:combobox id="cmbTipo" class="combo" runat="server" Width="180px" Enabled="False" Obligatorio="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD colSpan="3">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" EnterPorTab="False"
																				TextMode="MultiLine" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="4" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="4">
															<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="Table4" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2" align="left">
																			<asp:datagrid id="grdDeta" runat="server" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosDeta"
																				OnPageIndexChanged="grdDeta_PageChanged" AutoGenerateColumns="False" width="95%">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="inco_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_clie_apel" HeaderText="Cliente"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
																					<asp:BoundColumn DataField="inco_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="inco_impo" HeaderText="Importe"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_actu_inco_impo" HeaderText="Importe"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_esta_desc" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="30" vAlign="middle" colSpan="2" align="center"><BUTTON style="WIDTH: 90px" id="btnGeneLista" class="boton" onclick="btnGeneLista_click();"
																				runat="server" value="Detalles">Generar Lista</BUTTON>&nbsp;
																			<asp:Button id="btnBajaListaTodos" runat="server" cssclass="boton" Width="105px" Text="Borrar Todos"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="3" align="center">
																			<asp:panel id="panClie" runat="server" cssclass="titulo" Width="95%" BorderWidth="1px">
																				<TABLE style="WIDTH: 95%" id="TabClie" border="0" cellPadding="0" align="left">
																					<TR>
																						<TD vAlign="top" align="right">
																							<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;
																						</TD>
																						<TD width="80%">
																							<UC1:CLIE id="usrClie" runat="server" FilCUIT="True" Tabla="Clientes" Saltos="1,1,1" Ancho="780"
																								CampoVal="Cliente" Alto="560" FilSociNume="True" FilClie="True" FilDocuNume="True"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"></TD>
																						<TD>
																							<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																								<TR>
																									<TD vAlign="top" noWrap align="right">
																										<asp:Label id="lblImpoClieDeuda" runat="server" cssclass="titulo">Deuda:</asp:Label>&nbsp;</TD>
																									<TD width="80%">
																										<cc1:numberbox id="txtImpoClie" runat="server" cssclass="cuadrotextodeshab" Width="100px" Enabled="false"
																											EsDecimal="True"></cc1:numberbox></TD>
																									<TD vAlign="top" noWrap align="right">
																										<asp:Label id="lblSociNumero" runat="server" cssclass="titulo">Nro.Socio:</asp:Label>&nbsp;
																									</TD>
																									<TD width="80%">
																										<cc1:numberbox id="txtSociNume" runat="server" cssclass="cuadrotextodeshab" Width="100px" Enabled="false"
																											EsDecimal="True"></cc1:numberbox></TD>
																								</TR>
																							</TABLE>
																						</TD>
																					</TR>
																					<TR>
																						<TD height="30" vAlign="middle" colSpan="2" align="center">
																							<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																							<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="85px" Text="Eliminar"></asp:Button>&nbsp;
																							<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Visible="False" Width="90px" Text="Modificar"></asp:Button>
																							<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="85px" Text="Limpiar"></asp:Button></TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel>
										<ASP:PANEL Runat="server" ID="panBotones">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button>&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnCerrar" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Cerrar" Font-Bold="True"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</TD>
												</TR>
												<TR>
													<TD height="30" align="center">
														<asp:Button id="btnList" runat="server" cssclass="boton" Width="115px" CausesValidation="False"
															Text="Padr�n Detallado"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnListResu" runat="server" cssclass="boton" Width="115px" CausesValidation="False"
															Text="Padr�n Resumido"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
				<asp:textbox id="hdnIncoId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSociId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSociNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnDeuda" runat="server"></asp:textbox>
				<asp:textbox id="hdnSociLsinId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnEtapa" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
