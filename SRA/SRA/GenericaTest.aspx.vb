Imports SRA
Public Class GenericaTest
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    'Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnAgre As NixorControls.BotonImagen
    'Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents lblCodigo As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDesc As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFox As System.Web.UI.WebControls.Label
    'Protected WithEvents lblWald As System.Web.UI.WebControls.Label
    'Protected WithEvents btnAlta As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
    'Protected WithEvents btnModi As System.Web.UI.WebControls.Button
    'Protected WithEvents btnLimp As System.Web.UI.WebControls.Button
    'Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As NixorControls.BotonImagen
    'Protected WithEvents lblEstaFil As System.Web.UI.WebControls.Label
    'Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    'Private designerPlaceholderDeclaration As System.Object


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearEventos()
                mSetearMaxLength()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        'clsWeb.gCargarCombo(mstrConn, "wald_asientos_tipo_cargar", cmbWald, "id", "descrip_codi", "N")
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        'lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        'txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tias_id")
        'txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tias_desc")
        'txtFox.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "tias_compati_a")
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul"
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        Dim ldsDatos As DataSet
        ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        If ldsDatos.Tables(0).Rows.Count > 0 Then
            With ldsDatos.Tables(0).Rows(0)
                'txtCodigo.Valor = .Item("tias_id")
                'txtCodigo.Enabled = False
                'txtDesc.Valor = .Item("tias_desc")
                'txtFox.Valor = .Item("tias_compati_a")
                'cmbWald.Valor = .Item("tias_wald")
            End With

            mSetearEditor(False)
            mMostrarPanel(True)
        End If
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        'txtCodigo.Text = ""
        'txtCodigo.Enabled = True
        'txtDesc.Text = ""
        'txtFox.Text = ""
        'cmbWald.Limpiar()
        mSetearEditor(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        With ldsEsta.Tables(0).Rows(0)
            .Item("tias_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            '.Item("tias_desc") = txtDesc.Valor
            '.Item("tias_compati_a") = txtFox.Valor
            '.Item("tias_wald") = cmbWald.Valor
            .Item("tias_audi_user") = Session("sUserId").ToString()
        End With

        Return ldsEsta
    End Function
    Private Sub mAlta()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc, Context)

            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc, Context)
            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
#End Region

    Protected Sub btnBuscar_Click(sender As Object, e As ImageClickEventArgs) Handles btnBuscar.Click

    End Sub
End Class
