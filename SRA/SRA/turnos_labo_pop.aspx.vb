Namespace SRA

Partial Class turnos_labo_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()

      If Page.IsPostBack Then
         For i As Integer = 0 To 2
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            grdTurnos.Columns.Add(dgCol)
         Next
      End If
   End Sub

#End Region

   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label


#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String = SRA_Neg.Constantes.gTab_Proformas
   Private mstrConn As String
    Private mstrActivId, mstrClieId, mstrOrigen, mstrProfID As String
   Public mstrTitulo As String
   Private mobj As SRA_Neg.Facturacion

   Private Enum Columnas As Integer
      turn_rec = 0
      fecha_rec = 1
      sob_pff = 2
      rec_eff = 3
      dto_opp = 4
      dto_opc = 5
      rec_ptu = 6
      regicont = 7
   End Enum
   
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrClieId = Request.QueryString("cliente")
      mstrTitulo = Request.QueryString("titulo")
        mstrOrigen = Request.QueryString("origen")
        mstrProfID = Request.QueryString("proforma")
      mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())

   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultar()
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdTurnos.EditItemIndex = -1
         If (grdTurnos.CurrentPageIndex < 0 Or grdTurnos.CurrentPageIndex >= grdTurnos.PageCount) Then
            grdTurnos.CurrentPageIndex = 0
         Else
            grdTurnos.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      If mstrOrigen = "B" Then
         lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPopTurnoBov'].value='{0}';", E.Item.Cells(1).Text))
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPopTurnoBov','');")
      Else
         lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPopTurnoEqu'].value='{0}';", E.Item.Cells(1).Text))
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPopTurnoEqu','');")
      End If
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub

   Public Sub mConsultar()
      Try

            grdTurnos.DataSource = mobj.TurnosGrillaDS(mstrConn, mstrClieId, mstrProfID, mstrOrigen)
         grdTurnos.DataBind()

         For Each lItem As WebControls.DataGridItem In grdTurnos.Items
            If lItem.Cells(4).Text = 0 Then
               'poner en rojo la fila
               lItem.Cells(4).BackColor = System.Drawing.Color.Red

            End If
         Next

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region


   Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub
End Class

End Namespace
