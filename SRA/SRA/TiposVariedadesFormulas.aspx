<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TiposVariedadesFormulas" CodeFile="TiposVariedadesFormulas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Formulas Registros Razas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Formulas Variedades Razas</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" BorderStyle="Solid"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																	IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																align="right">
																<asp:Label id="lblEspeFil" runat="server" cssclass="titulo">Especie:</asp:Label>&nbsp;</TD>
															<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																<cc1:combobox id="cmbEspecieFil" class="combo" runat="server" Width="176px" onchange="mCargarRazaFil()"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																align="right">
																<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
															<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																<cc1:combobox id="cmbRazaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="300px"
																	onchange="setCmbEspecieFil()" MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
														</TR>
														<TR>
															<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2" height="10"></TD>
							</TR> <!---fin filtro --->
							<TR>
								<TD vAlign="top" colspan="2" align="center"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Variedad" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="varz_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="_espe_desc" HeaderText="Especie">
												<HeaderStyle Width="20%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="_raza_desc" HeaderText="Raza">
												<HeaderStyle Width="25%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="True" DataField="_vari_padre" ReadOnly="True" HeaderText="Vari. Padre">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="True" DataField="_vari_madre" ReadOnly="True" HeaderText="Vari. Madre">
												<HeaderStyle Width="13%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="True" DataField="_vari_cria" ReadOnly="True" HeaderText="Vari. Resul">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="True" DataField="_estado" ReadOnly="True" HeaderText="Estado">
												<HeaderStyle Width="12%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="middle" colspan="2">
									<TABLE id="Table3" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
													IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
													ImageOver="btnNuev2.gif" ForeColor="Transparent" ToolTip="Agregar un Nuevo Px" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent" ToolTip="Imprimir Listado"
													ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD align="center" colspan="2">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
															<asp:Label id="lblespe" runat="server" cssclass="titulo">Especie:</asp:Label>&nbsp;</TD>
														<TD background="imagenes/formfdofields.jpg" align="left">
															<cc1:combobox id="cmbEspecie" class="combo" runat="server" Width="176px" onchange="mCargarRaza()"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 20%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
															<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
														<TD background="imagenes/formfdofields.jpg" align="left">
															<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="280px" onchange="setCmbEspecie()"
																MostrarBotones="False" NomOper="razas_cargar" filtra="true" Height="20px" obligatorio="True"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 22px" align="right">
															<asp:Label id="lblRegiPadre" runat="server" cssclass="titulo">Variedad  Padre:</asp:Label></TD>
														<TD style="HEIGHT: 22px" colSpan="2">
															<cc1:combobox id="cmbRegiPadre" class="combo" runat="server" Width="200px"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 4px" height="4" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 14px" align="right">
															<asp:Label id="lblRegiMadre" runat="server" cssclass="titulo">Variedad Madre:</asp:Label></TD>
														<TD colSpan="2">
															<cc1:combobox id="cmbRegiMadre" class="combo" runat="server" Width="200px"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblRegiCria" runat="server" cssclass="titulo">Variedad  Resultado:</asp:Label></TD>
														<TD colSpan="2">
															<cc1:combobox id="cmbRegiResu" class="combo" runat="server" Width="200px"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD background="imagenes/formfdofields.jpg"></TD>
														<TD background="imagenes/formfdofields.jpg" align="right"></TD>
													</TR>
													<TR>
														<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		function mCargarRazaFil()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecieFil").value != '')
				sFiltro = document.all("cmbEspecieFil").value;
				LoadComboXML("razas_cargar", sFiltro, "cmbRazaFil", "S");
				
	    }
	    function setCmbEspecieFil()
	    {
			if (document.all("cmbRazaFil").value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all("cmbRazaFil").value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all("cmbEspecieFil").value = vstrRet[0];			
			}		
	    }
	    
	     function mCargarRaza(pEspe,pRaza)
		{
		    var sFiltro = "";
		    var sOpcion="S";
		    
		    if (document.all(pEspe).value != '')
				sFiltro = document.all(pEspe).value;
			if (pRaza=="cmbRazaFil")	
				sOpcion="T";
 	       LoadComboXML("razas_cargar", sFiltro, pRaza, sOpcion);
	    }
	    
	    function setCmbEspecie(pEspe,pRaza)
	    {
			if (document.all(pRaza).value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all(pRaza).value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all(pEspe).value = vstrRet[0];
			}		
	    }
	    
	    
	    /*
	    function mCargarRaza()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecie").value != '')
				sFiltro = document.all("cmbEspecie").value;
				LoadComboXML("razas_cargar", sFiltro, "cmbRaza", "S");
	    }
	    function setCmbEspecie()
	    {
			if (document.all("cmbRaza").value != "")
			{
				mCargarTipoVariedadPadre(document.all("cmbRaza").value);
				mCargarTipoVariedadMadre(document.all("cmbRaza").value);
				mCargarTipoVariedadResultado(document.all("cmbRaza").value);
				
				var vstrRet = LeerCamposXML("razas", document.all("cmbRaza").value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all("cmbEspecie").value = vstrRet[0];
				
				
			
			}		
	    }*/
	    function mCargarTipoVariedadPadre(pRaza)
		{
		    if (pRaza!='')
			{
			    LoadComboXML("rg_variedades_cargar", pRaza, "cmbRegiPadre", "S");			    
			}
			else
			{
				LoadComboXML("rg_variedades_cargar", pRaza, "cmbRegiPadre", "S");			
			}
		}
		function mCargarTipoVariedadMadre(pRaza)
		{
		    if (pRaza!='')
			{			    
			    LoadComboXML("rg_variedades_cargar", pRaza, "cmbRegiMadre", "S");			    
			}
			else
			{				
				LoadComboXML("rg_variedades_cargar", pRaza, "cmbRegiMadre", "S");			
			}
		}
		function mCargarTipoVariedadResultado(pRaza)
		{
		    if (pRaza!='')
			{			    
			    LoadComboXML("rg_variedades_cargar", pRaza, "cmbRegiResu", "S");
			}
			else
			{				
			    LoadComboXML("rg_variedades_cargar", pRaza, "cmbRegiResu", "S");
				
			}
		}
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		</SCRIPT>
	</BODY>
</HTML>
