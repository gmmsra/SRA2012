Namespace SRA

Partial Class Calificaciones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub






    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Calificaciones
   Private mstrCmd As String
   Private mstrConn As String
   Private mintInse As Integer

   Private Enum Columnas As Integer
      Id = 0
      AlumId = 1
      AlumLega = 2
      AlumDesc = 3
      Desc = 4
      Fech = 5
      MateCali = 6
      Cali = 7
      NoCali = 8
      AproCmb = 9
      Asis = 10
      Apro = 11
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
      usrAlumFil.FilInseId = mintInse
      btnModi.Attributes.Add("onclick", "if(!confirm('Confirma la actualización de las calificaciones?')) return false;")
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub mConsultar()
      Try
         Dim ds As New DataSet

         If cmbComiFil.Valor = 0 And cmbMesaFil.Valor = 0 And usrAlumFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la comisión, la mesa o el alumno.")
         End If

         If cmbComiFil.Valor <> 0 And cmbMesaFil.Valor <> 0 Then
            Throw New AccesoBD.clsErrNeg("No puede indicar la comisión y la mesa.")
         End If

         mstrCmd = "exec " + mstrTabla & "_busq"
         mstrCmd += " @comi_id=" & cmbComiFil.Valor
         mstrCmd += ", @meex_id=" & cmbMesaFil.Valor
         mstrCmd += ", @alum_id=" & usrAlumFil.Valor
         mstrCmd += ", @mate_id=" & cmbMateFil.Valor

         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)



         grdDato.DataSource = ds
         grdDato.DataBind()

         grdDato.Visible = True

         If grdDato.Items.Count > 0 Then
            grdDato.Columns(Columnas.Cali).Visible = ds.Tables(0).Rows(0).Item("_mate_cali").ToString
            grdDato.Columns(Columnas.NoCali).Visible = Not grdDato.Columns(Columnas.Cali).Visible
         End If

         btnModi.Visible = (grdDato.Items.Count > 0)
         FindControl("trCati").Visible = (grdDato.Items.Count > 0)

         grdDato.Columns(Columnas.Desc).Visible = cmbMesaFil.Valor = 0 And cmbComiFil.Valor = 0

         Session(mstrTabla) = ds

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub grdDato_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDato.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 Then
                Dim oCalif As NixorControls.NumberBox = e.Item.FindControl("txtCali")
                Dim ocmbApro As NixorControls.ComboBox = e.Item.FindControl("cmbApro")
            clsWeb.gCargarComboBool(ocmbApro, "S")


                    With CType(e.Item.DataItem, DataRowView).Row

                        ocmbApro.Valor = IIf(.Item("cali_apro").ToString.Length > 0, .Item("cali_apro"), "")
                        oCalif.Valor = IIf(.Item("cali_cali").ToString.Length > 0, .Item("cali_cali"), "")

                        If .IsNull("cali_id") Then
                            .Item("cali_id") = -1 * e.Item.ItemIndex
                            e.Item.Cells(Columnas.Id).Text = .Item("cali_id")
                        End If



                        If Not .IsNull("cali_cati_id") Then
                            cmbCati.Valor = .Item("cali_cati_id")
                        End If
                    End With
                End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"

   Private Sub mLimpiarFiltros()
      cmbMateFil.Limpiar()
      usrAlumFil.Limpiar()
      cmbCiclFil.Items.Clear()

      mCargarComisiones()
      mCargarMesas()

      grdDato.Visible = False
      btnModi.Visible = False
      FindControl("trCati").Visible = False

      Session(mstrTabla) = Nothing
   End Sub
#End Region

   

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMateFil, "S", "@mate_inse_id=" & mintInse.ToString)
      clsWeb.gCargarRefeCmb(mstrConn, "califica_tipos", cmbCati, "S")
      mCargarCiclos()
      mCargarComisiones()
      mCargarMesas()
   End Sub

   Private Sub mCargarComisiones()
      clsWeb.gCargarRefeCmb(mstrConn, "comisiones", cmbComiFil, "N", "@cicl_id=" & cmbCiclFil.Valor & ", @mate_id=" & cmbMateFil.Valor)
   End Sub

   Private Sub mCargarMesas()
      clsWeb.gCargarRefeCmb(mstrConn, "mesas_exam", cmbMesaFil, "N", "@meex_mate_id=" & cmbMateFil.Valor)
   End Sub

   Private Sub mCargarCiclos()
      Dim lstrMate As String = ""
      If cmbMateFil.Valor.ToString <> "" Then
         lstrMate = ",@cima_mate_id=" & cmbMateFil.Valor.ToString
      End If
      clsWeb.gCargarRefeCmb(mstrConn, "ciclos", cmbCiclFil, "N", "@cicl_inse_id=" & mintInse.ToString & lstrMate)
   End Sub

   Private Sub mModi()
      Try
         Dim ldsDatosTmp As DataSet
         ldsDatosTmp = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatosTmp)
         lobjGenerico.ActualizaMul()

         mLimpiarFiltros()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Private Function mGuardarDatos() As DataSet
            Dim ldrDatos As DataRow
            Dim ldsDatosTmp As DataSet
            Dim lcmbApro As NixorControls.ComboBox

            'TODO::MARIO::CAMBIAR
            'Dim lintAproCalif As Integer = clsSQLServer.gParaConsul(mstrConn).Tables(0).Rows(0).Item("para_aprob_calif")
            Dim lintAproCalif As Integer = 0
            Dim ldsInstit As DataSet = clsSQLServer.gExecuteQuery(mstrConn, "institutos_consul " + mintInse.ToString)
            If ldsInstit.Tables.Count > 0 AndAlso ldsInstit.Tables(0).Rows.Count > 0 Then
                If ldsInstit.Tables(0).Rows(0).IsNull("inse_aprob_calif") Then
                    Throw New AccesoBD.clsErrNeg("Para ingresar calificaciones debe indicar el parámetro nota aprobación para el instituto.")
                Else
                    lintAproCalif = Convert.ToInt32(ldsInstit.Tables(0).Rows(0).Item("inse_aprob_calif"))
                End If
            End If

            'uso una copia para no perder los registros que borro
            If Not Session(mstrTabla) Is Nothing Then
                ldsDatosTmp = DirectCast(Session(mstrTabla), DataSet).Copy
            Else
                Throw New AccesoBD.clsErrNeg("Debe realizar una consulta para poder ingresar las calificaciones, no utilice el boton back del navegador.")
            End If

            If cmbCati.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Tipo de Calificación.")
            End If

            For Each oItem As DataGridItem In grdDato.Items
                lcmbApro = oItem.FindControl("cmbApro")

                If (usrAlumFil.Valor > 0) Then
                    ldrDatos = ldsDatosTmp.Tables(0).Select("cali_id=" & oItem.Cells(Columnas.Id).Text)(0)
                Else
                    ldrDatos = ldsDatosTmp.Tables(0).Select("cali_alum_id=" & oItem.Cells(Columnas.AlumId).Text)(0)
                End If

                With ldrDatos
                    .Item("cali_cati_id") = IIf(cmbCati.Valor.Length > 0, cmbCati.Valor, DBNull.Value)
                    'si califica con nota (1 -> calif con nota)
                    If .Item("_mate_cali") Then
                        If (DirectCast(oItem.FindControl("txtcali"), NixorControls.NumberBox).Valor.Length > 0) Then
                            .Item("cali_cali") = DirectCast(oItem.FindControl("txtcali"), NixorControls.NumberBox).Valor
                        End If
                        If Not .IsNull("cali_cali") Then
                            .Item("cali_apro") = .Item("cali_cali") >= lintAproCalif
                        Else
                            If .IsNull("cali_id") Then
                                ldrDatos.Table.Rows.Remove(ldrDatos)
                            Else
                                .Delete()
                            End If

                        End If
                    Else
                        If IsDBNull(lcmbApro.ValorBool) Then  ' If Not lcmbApro.ValorBool Is DBNull.Value Then
                            .Item("cali_cali") = 0
                            .Item("cali_apro") = lcmbApro.ValorBool
                        Else
                            If .IsNull("cali_id") Then
                                ldrDatos.Table.Rows.Remove(ldrDatos)
                            Else
                                .Delete()
                            End If
                        End If

                    End If

                End With
            Next

            Return (ldsDatosTmp)
        End Function

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      mConsultar()
   End Sub

   Private Overloads Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnModi.Click
      mModi()
   End Sub

End Class
End Namespace
