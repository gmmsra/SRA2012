Namespace SRA

Partial Class CondicionesEspeciales
    Inherits FormGenerico
    ' Dario cambio 2013-07-02 (se arregla ya que no esta registra el clie deud saldo)

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "clientes"
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mEstablecerPerfil()
                mCargarCombos()
                mSetearMaxLength()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtObser.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "clie_espe_deta")
        txtNroInsc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "clie_iibb_nume")
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarComboBool(Me.cmbFactExen, "S")
        clsWeb.gCargarComboBool(Me.cmbAgPerIVA, "S")
        clsWeb.gCargarComboBool(Me.cmbFacPreSocio, "S")
        clsWeb.gCargarComboBool(Me.cmbMieComDirectiva, "S")
        clsWeb.gCargarComboBool(Me.cmbNDInte, "S")
        clsWeb.gCargarCombo(mstrConn, "iibb_catego_cargar", cmbIIBB, "id", "descrip")
    End Sub
    Private Sub mEstablecerPerfil()
        Dim lbooPermiModi As Boolean

        If (Not clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu, String), (mstrConn), (Session("sUserId").ToString()))) Then
            Response.Redirect("noaccess.aspx")
        End If

        'clientes
        lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
        btnLimp.Visible = (lbooPermiModi)
        mMostrarPanel(False)
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()

        usrClieFil.Tabla = "clientes"
        usrClieFil.AutoPostback = True
        usrClieFil.Ancho = 790
        usrClieFil.Alto = 510

        'Clave unica
        usrClieFil.ColClaveUnica = True
        usrClieFil.FilClaveUnica = True
        'Numero de cliente
        usrClieFil.ColClieNume = True

        'Fantasia
        'usrClieFil.ColFanta = False
        'usrClieFil.FilFanta = False
        'Numero de CUIT
        usrClieFil.ColCUIT = True
        usrClieFil.FilCUIT = True
        'Numero de documento
        usrClieFil.ColDocuNume = True
        usrClieFil.FilDocuNume = True
        'Numero de socio
        usrClieFil.ColSociNume = True
        usrClieFil.FilSociNume = True

    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mLimpiar()
        Me.hdnClieId.Text = ""
        Me.cmbFactExen.Limpiar()
        Me.cmbAgPerIVA.Limpiar()
        Me.cmbFacPreSocio.Limpiar()
        Me.cmbMieComDirectiva.Limpiar()
        Me.cmbNDInte.Limpiar()
        Me.txtLimite.Text = ""
        Me.cmbIIBB.Limpiar()
        Me.txtNroInsc.Text = ""
        txtObser.Valor = ""
        lblTitu.Text = ""
        mstrCmd = ""
        usrClieFil.Limpiar()
        cmbFacPreSocio.Enabled = True
        mMostrarPanel(False)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panDato.Visible = pbooVisi
    End Sub
#End Region

#Region "Opciones de ABM"
    Public Sub mConsultar()
        Try
            Dim mdsDatos As DataSet
            mstrCmd = "exec clientes_condiciones_especiales_consul @ClienteId = " & usrClieFil.Valor
            mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            hdnClieId.Text = usrClieFil.Valor
            lblTitu.Text = "Cliente: " & usrClieFil.Valor & " - " & usrClieFil.Apel
            With mdsDatos.Tables(0).Rows(0)
                Me.cmbFactExen.ValorBool = .Item("clie_fact_exen")
                    Me.cmbAgPerIVA.ValorBool = .Item("clie_perc_iva")
                    If (.Item("clie_prec_soci").ToString.Length > 0) Then
                        Me.cmbFacPreSocio.ValorBool = .Item("clie_prec_soci")
                    Else
                        Me.cmbFacPreSocio.ValorBool = False
                    End If
                    If (.Item("clie_miem_cd").ToString.Length > 0) Then
                        Me.cmbMieComDirectiva.ValorBool = .Item("clie_miem_cd")
                    Else
                        Me.cmbMieComDirectiva.ValorBool = False
                    End If
                    If (.Item("clie_exen_nd").ToString.Length > 0) Then
                        Me.cmbNDInte().ValorBool = .Item("clie_exen_nd")
                    Else
                        Me.cmbNDInte().ValorBool = False
                    End If

                    Me.cmbIIBB.Valor = .Item("clie_ibbc_id").ToString
                    Me.txtNroInsc.Valor = .Item("clie_iibb_nume").ToString
                    If RequiereNroInsc() Then txtNroInsc.Enabled = True Else txtNroInsc.Enabled = False

                    If .Item("clie_deud_monto") Is DBNull.Value Then
                        Me.rbtnNoOpera.Checked = True
                        Me.rbtnSinLimite.Checked = False
                        Me.rbtnConLimite.Checked = False
                        Me.txtLimite.Enabled = False
                    ElseIf .Item("clie_deud_monto") = 0 Then
                        Me.rbtnNoOpera.Checked = False
                        Me.rbtnSinLimite.Checked = True
                        Me.rbtnConLimite.Checked = False
                        Me.txtLimite.Enabled = False
                    ElseIf .Item("clie_deud_monto") > 0 Then
                        Me.rbtnNoOpera.Checked = False
                        Me.rbtnSinLimite.Checked = False
                        Me.rbtnConLimite.Checked = True
                        Me.txtLimite.Enabled = True
                        Me.txtLimite.Valor = .Item("clie_deud_monto")
                    End If
                    txtObser.Valor = .Item("clie_espe_deta").ToString
                End With
            mSetearComboPSocio()

            mMostrarPanel(True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearComboPSocio()
        If clsSQLServer.gCampoValorConsul(mstrConn, "clientes_alertas_especiales_consul @clie_id=" + usrClieFil.Valor, "desc") = "No" Then
            cmbFacPreSocio.Enabled = True
        Else
            cmbFacPreSocio.Valor = 1 'si
            cmbFacPreSocio.Enabled = False
        End If
    End Sub

    Private Function RequiereNroInsc() As Boolean
        Try
            Dim valor As Integer = cmbIIBB.Valor
            Dim vsRet As String() = SRA_Neg.Utiles.DatosIIBB(mstrConn, valor.ToString).Split(";")

            If vsRet(2) = False Then
                txtNroInsc.Valor = ""
            End If

            Return vsRet(2)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function
    Private Sub mAlta()
        Dim Monto As Decimal
        Try
            If (Trim(Me.txtLimite.Text) = "") And Me.rbtnConLimite.Checked Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Monto L�mite.")
            End If
            If (Trim(Me.txtNroInsc.Text) = "") And RequiereNroInsc() Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. Inscripci�n IIBB.")
            End If
            mstrCmd = "exec clientes_condiciones_especiales_modif @ClienteId = " & usrClieFil.Valor
            mstrCmd = mstrCmd + ",@FactEx = " & IIf(Me.cmbFactExen.SelectedItem.Text = "Si", 1, 0)
            mstrCmd = mstrCmd + ",@PercIVA = " & IIf(Me.cmbAgPerIVA.SelectedItem.Text = "Si", 1, 0)
            mstrCmd = mstrCmd + ",@PrecSoc = " & IIf(Me.cmbFacPreSocio.SelectedItem.Text = "Si", 1, 0)
            mstrCmd = mstrCmd + ",@MiemCD = " & IIf(Me.cmbMieComDirectiva.SelectedItem.Text = "Si", 1, 0)
            mstrCmd = mstrCmd + ",@clie_espe_deta = " & IIf(txtObser.Valor.ToString() = "", "NULL", "'" + txtObser.Valor + "'")
            If rbtnSinLimite.Checked Then
                Monto = 0
            ElseIf Me.rbtnConLimite.Checked Then
                Monto = Replace(Me.txtLimite.Valor, ",", ".")
            End If
            mstrCmd = mstrCmd + ",@Monto = " & IIf(Me.rbtnNoOpera.Checked = True, "NULL", Monto)
            mstrCmd = mstrCmd + ",@clie_exen_nd = " & IIf(Me.cmbNDInte.SelectedItem.Text = "Si", 1, 0)
            mstrCmd = mstrCmd + ",@clie_ibbc_id = " & cmbIIBB.Valor
            If (Trim(Me.txtNroInsc.Text) <> "") Then mstrCmd = mstrCmd + ",@clie_iibb_nume = '" & txtNroInsc.Valor & "'"
            ' Dario cambio 2013-07-02 
            If (Me.rbtnNoOpera.Checked = True) Then
                mstrCmd = mstrCmd + ",@clie_deud_saldo = 0"
            Else
                mstrCmd = mstrCmd + ",@clie_deud_saldo = 1"
            End If
            ' fin 

            clsSQLServer.gExecuteScalar(mstrConn, mstrCmd)
            mLimpiar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub usrClieFil_Cambio(ByVal sender As Object) Handles usrClieFil.Cambio
        If Me.usrClieFil.Valor = 0 Then
            mLimpiar()
        Else
            mConsultar()
        End If
    End Sub
    Private Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        mAlta()
        If Me.rbtnConLimite.Checked Then Me.txtLimite.Enabled = True
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        Try
            If Me.usrClieFil.Valor = 0 Then
                mLimpiar()
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
            Else
                mConsultar()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            mLimpiar()
        End Try
    End Sub
#End Region

End Class
End Namespace
