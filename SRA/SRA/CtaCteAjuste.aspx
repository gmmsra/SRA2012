<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CtaCteAjuste" CodeFile="CtaCteAjuste.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Ajuste Cuenta Corriente</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mSetearConcepto(pConc)
		{
			var strRet = LeerCamposXML("conceptos", "@conc_id="+pConc.value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet!='')
			{
				var lstrCta = strRet.substring(0,1);
				document.all('hdnCta').value = lstrCta;
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCcos').disabled = true;
					document.all('txtcmbCcos').disabled = true;
					document.all('cmbCcos').innerHTML = '';
					document.all('txtcmbCcos').value = '';
				}
				else
				{
					document.all('cmbCcos').disabled = false;
					document.all('txtcmbCcos').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCcos", "N");					
				}
			}			
		}
		function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" width="100%" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Ajuste de Cuenta Corriente</asp:label></TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" width="100%" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" vAlign="top" noWrap width="26" rowSpan="2">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" vAlign="top" noWrap width="26" rowSpan="2">
																<CC1:BotonImagen id="btnList" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
																	ImageOver="btnImpr2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" vAlign="bottom" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" vAlign="bottom" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" vAlign="bottom" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" vAlign="bottom"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD width="100%"><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" background="imagenes/formfdofields.jpg"
																	border="0">
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" Autopostback="false" Tabla="Clientes" AceptaNull="False"
																				FilSociNume="True" MuestraDesc="False" Ancho="800" Saltos="1,1,1,1"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" noWrap align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblNroDesdeFil" runat="server" cssclass="titulo">Nro. Ajuste:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtNumeDesdeFil" runat="server" cssclass="cuadrotexto" Width="100px" EsDecimal="False"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblNroHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:numberbox id="txtNumeHastaFil" runat="server" cssclass="cuadrotexto" Width="100px" EsDecimal="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" noWrap align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 19px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="HEIGHT: 19px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblEmctFil" runat="server" cssclass="titulo" Width="49px">Centro Emisor:</asp:Label></TD>
																		<TD style="HEIGHT: 19px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbEmctFil" runat="server" Width="200px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<asp:CheckBox id="chkApro" Runat="server" CssClass="titulo" Checked="False" Text="Incluir Aprobados"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<asp:CheckBox id="ChkAnu" Runat="server" CssClass="titulo" Checked="False" Text="Incluir Anulados"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" width="100%" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatos" OnItemDataBound="mItemDataBound" OnPageIndexChanged="DataGrid_Page"
										AutoGenerateColumns="False" width="100%">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" Causessra Validation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="ajus_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="True" DataField="_comp_nume" HeaderText="Nro.Ajuste"></asp:BoundColumn>
											<asp:BoundColumn DataField="ajus_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="5%"></asp:BoundColumn>
											<asp:BoundColumn DataField="ajus_clie_id" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="_soci_nume" HeaderText="Socio"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cliente" HeaderText="Apellido / Razón Social"></asp:BoundColumn>
											<asp:BoundColumn DataField="_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="_dh" HeaderText="Signo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_conf" HeaderText="Apro."></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="1"></TD>
								<TD vAlign="middle" width="100%"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
										IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar un nuevo Ajuste De Cta.Cte"></CC1:BOTONIMAGEN></TD>
								<TD align="right">
									<asp:Button id="btnConf" runat="server" cssclass="boton" Width="90px" Text="Aprobar"></asp:Button></TD>
							</TR>
							<TR>
								<TD width="1"><A id="editar" name="editar"></A></TD>
								<TD vAlign="middle" width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span onclick="return(confirm('Confirma la confirmación de los ajustes?'));"></span></TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Cabecera</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" Height="21px" CausesValidation="False"> Conceptos</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" width="100%" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro.Ajuste:</asp:Label>&nbsp;</TD>
																		<TD colSpan="3">
																			<cc1:numberbox id="txtNume" runat="server" cssclass="cuadrotexto" Width="100px" EsDecimal="False"
																				Enabled="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha Valor:</asp:Label>&nbsp;
																		</TD>
																		<TD colSpan="3">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%" noWrap align="right">
																			<asp:Label id="lblSigno" runat="server" cssclass="titulo">Signo:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 10%" align="left">
																			<asp:RadioButton id="rbtnHaber" runat="server" cssclass="titulo" Text="Haber" GroupName="RadioGroup1"></asp:RadioButton>
																			<asp:RadioButton id="rbtnDebe" runat="server" cssclass="titulo" Text="Debe" GroupName="RadioGroup1"></asp:RadioButton></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;
																		</TD>
																		<TD width="80%">
																			<UC1:CLIE id="usrClie" runat="server" Tabla="Clientes" FilSociNume="True" Ancho="780" Saltos="1,1,1"
																				Obligatorio="true" FilCUIT="True" CampoVal="Cliente" Alto="560" FilClie="True" FilDocuNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD colSpan="3">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" EnterPorTab="False"
																				TextMode="MultiLine" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDetalle" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TablaDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD vAlign="top" align="center" colSpan="2">
																			<asp:datagrid id="grdDetalle" runat="server" BorderStyle="None" BorderWidth="1px" width="100%"
																				AutoGenerateColumns="False" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatosDeta"
																				CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True"
																				PageSize="5">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="ajco_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_conc_desc" HeaderText="Descripci&#243;n"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_ccos_desc" ReadOnly="True" HeaderText="Centro de Costo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																						ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 20px" noWrap align="right">
																			<asp:Label id="lblDetaSigno" runat="server" cssclass="titulo" Visible="False">Signo:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 10%; HEIGHT: 20px" align="left">
																			<asp:RadioButton id="rbtnDetaHaber" runat="server" cssclass="titulo" Visible="False" Text="Haber"
																				GroupName="RadioGroup1"></asp:RadioButton>
																			<asp:RadioButton id="rbtnDetaDebe" runat="server" cssclass="titulo" Visible="False" Text="Debe" GroupName="RadioGroup1"></asp:RadioButton></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblConc" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																			<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																			<td>															
																			<cc1:combobox class="combo" id="cmbConc" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="cuentas_cargar"
																						filtra="true" MostrarBotones="False" onchange="javascript:mSetearConcepto(this);" TextMaxLength="5"></cc1:combobox>
																			</td>
																			<td>
																				<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																												onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbConc','Conceptos','@conc_acti_id=1');"
																												alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																			</td>
																			</tr>
																			</table>	
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																			<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																			<td>															
																			<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="centrosc_cuenta_cargar"
																						filtra="true" MostrarBotones="False" TextMaxLength="6"></cc1:combobox>
																			</td>
																			<td>
																				<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																												onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos','Centros de Costo','@cuenta='+document.all('hdnCta').value);"
																												alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																			</td>
																			</tr>
																			</table>
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="100px" EsDecimal="true"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 30%; HEIGHT: 22px" vAlign="top" align="right">
																			<asp:Label id="lblDetaObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:textboxtab id="txtDetaObse" runat="server" cssclass="textolibre" Width="100%" TextMode="MultiLine"
																				height="46px" Rows="2"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR height="30">
																		<TD align="center" colSpan="2">
																			<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>
																			<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>
																			<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblAprob" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center">
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;&nbsp;
														<asp:Button id="btnCerrar" runat="server" cssclass="boton" Width="90px" Text="Aprobar" CausesValidation="False"
															Font-Bold="True"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
			<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="hdnCta" runat="server"></ASP:TEXTBOX>
			<asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		if (document.all["cmbCcos"]!= null)
		{
			document.all('cmbCcos').disabled = (document.all('cmbCcos').options.length <= 1);
			document.all('txtcmbCcos').disabled = (document.all('cmbCcos').options.length <= 1);
		}
		</SCRIPT>
	</BODY>
</HTML>
