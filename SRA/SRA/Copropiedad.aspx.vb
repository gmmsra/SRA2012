Namespace SRA

Partial Class Copropiedad
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = "productos_propietario"
   Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ProductosProp
   Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
   Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
   Private mstrConn As String
   Private mdsDatos As DataSet
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private Enum Columnas As Integer
      Edit = 0
      Id = 1
      ClieId = 2
      CriaId = 3
      Desde = 4
      Hasta = 5
      ClieNomb = 6
      Parti = 7
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mCrearDataSet("")
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            mSetearEventos()
            clsWeb.gInicializarControles(Me, mstrConn)
                Session(mstrTabla) = mdsDatos
         Else
                mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
  
   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaDeta

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      grdCopro.CurrentPageIndex = 0

        Session(mstrTabla) = mdsDatos
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul(mstrConn, mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      usrProductoFil.AutoPostback = False
      usrProductoFil.Ancho = 790
      usrProductoFil.Alto = 510

      usrPropClie.Tabla = mstrClientes
      usrPropClie.Criador = False
      usrPropClie.AutoPostback = False
      usrPropClie.FilClaveUnica = False
      usrPropClie.ColClaveUnica = True
      usrPropClie.Ancho = 790
      usrPropClie.Alto = 510
      usrPropClie.ColDocuNume = False
      usrPropClie.ColCUIT = True
      usrPropClie.ColCriaNume = True
      usrPropClie.FilCUIT = True
      usrPropClie.FilDocuNume = True
      usrPropClie.FilAgru = False
      usrPropClie.FilTarjNume = False

      usrPropCria.Tabla = mstrCriadores
      usrPropCria.Criador = True
      usrPropCria.AutoPostback = False
      usrPropCria.FilClaveUnica = False
      usrPropCria.ColClaveUnica = True
      usrPropCria.Ancho = 790
      usrPropCria.Alto = 510
      usrPropCria.ColDocuNume = False
      usrPropCria.ColCUIT = True
      usrPropCria.ColCriaNume = True
      usrPropCria.FilCUIT = True
      usrPropCria.FilDocuNume = True
      usrPropCria.FilAgru = False
      usrPropCria.FilTarjNume = False

      usrPropFil.Tabla = mstrClientes
      usrPropFil.Criador = False
      usrPropFil.AutoPostback = False
      usrPropFil.FilClaveUnica = True
      usrPropFil.ColClaveUnica = False
      usrPropFil.Ancho = 790
      usrPropFil.Alto = 510
      usrPropFil.ColDocuNume = False
      usrPropFil.ColCriaNume = True
      usrPropFil.ColCUIT = True
      usrPropFil.FilCUIT = True
      usrPropFil.FilDocuNume = True
      usrPropFil.FilAgru = False
      usrPropFil.FilTarjNume = False

      usrProducto.AutoPostback = False
      usrProducto.Ancho = 790
      usrProducto.Alto = 510

   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdCopro_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCopro.EditItemIndex = -1
         If (grdCopro.CurrentPageIndex < 0 Or grdCopro.CurrentPageIndex >= grdCopro.PageCount) Then
            grdCopro.CurrentPageIndex = 0
         Else
            grdCopro.CurrentPageIndex = E.NewPageIndex
         End If
         mCargarCopropietarios()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
      usrProducto.cmbProdAsocExt.Enabled = pbooAlta
      usrProducto.cmbProdRazaExt.Enabled = pbooAlta
      usrProducto.cmbSexoProdExt.Enabled = pbooAlta
      usrProducto.txtProdNombExt.Enabled = pbooAlta
      usrProducto.txtCodiExt.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lDsDatos As DataSet
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
      mCrearDataSet(hdnId.Text)

      With mdsDatos.Tables(0).Rows(0)
         usrProducto.Valor = .Item("prdt_id")
         usrPropClie.Valor = .Item("prdt_prop_clie_id")
         hdnPropOrig.Text = .Item("prdt_prop_clie_id")
      End With
      mCargarCopropietarios()
      mSetearEditor("", False)
      mMostrarPanel(True)
      mShowTabs(1)
   End Sub
   Public Sub mEditarDatosCopro(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnDetaId.Text = E.Item.Cells(Columnas.Id).Text
      txtNumeClie.Text = E.Item.Cells(Columnas.ClieId).Text & " - "
      txtNombClie.Text = E.Item.Cells(Columnas.ClieNomb).Text
      txtPorc.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Parti).Text)
   End Sub
   Private Sub mLimpiarFiltros()
      usrPropFil.Limpiar()
		usrProductoFil.Limpiar()
		txtFechaDesdeFil.Text = ""
		txtFechaHastaFil.Text = ""
		txtSraNumeDesde.Text = ""
		txtSraNumeHasta.Text = ""
		cmbTipoReporte.Limpiar()
      grdDato.Visible = False
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
      grdCopro.DataSource = Nothing
      grdCopro.DataBind()
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      usrProducto.Limpiar()
      usrPropCria.Limpiar()
      usrPropClie.Limpiar()
      txtFechaNaci.Text = ""
      txtFechaVenta.Text = ""
      mShowTabs(1)
      mSetearEditor("", True)
   End Sub
   Private Sub mCerrar()
      mConsultarGrilla()
   End Sub
   Private Sub mConsultarGrilla()
      Try
         mConsultar()
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panLinks.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      btnAgre.Visible = Not (panDato.Visible)
      grdDato.Visible = Not (panDato.Visible)
      panFiltro.Visible = Not (panDato.Visible)
      btnList.Visible = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mModificarCopropietario()
      Try
         If txtFechaNaci.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha Desde.")
         End If
         If usrProducto.Valor.ToString = "0" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el producto.")
         End If

         mGuardarDatosCopro()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mAlta()
      Try
         mGuardarDatos()

         Dim lobjProp As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)

         lobjProp.Alta()

         mConsultarGrilla()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()

         Dim lobjProp As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)

         lobjProp.Modi()

         mConsultarGrilla()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjProp As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjProp.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0
         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mLimpiarParticipaciones()

      For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
         If lDr.Item("prdp_clie_id") > 0 Then
            lDr.Delete()
         Else
            mdsDatos.Tables(mstrTablaDeta).Rows.Remove(lDr)
         End If
      Next

      'If mdsDatos.Tables(mstrTablaDeta).Select.GetUpperBound(0) = -1 Then pctrHdnOrig.Text = ""

      grdCopro.DataSource = mdsDatos.Tables(mstrTablaDeta)
      grdCopro.DataBind()

   End Sub
   Private Sub mCargarCopropietarios()
      Try

        Dim ldsDatos As DataSet

        mstrCmd = "exec " & mstrTablaDeta & "_consul "
        mstrCmd += " @clie_id = " & usrPropClie.Valor.ToString
        mstrCmd += ",@cria_id = " & usrPropCria.Valor.ToString
        mstrCmd += ",@prod_id = " & usrProducto.Valor.ToString

        ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        mLimpiarParticipaciones()

        For Each lDr As DataRow In ldsDatos.Tables(0).Select()
           If lDr.Item("prdp_id") Is DBNull.Value Then
                lDr.Item("prdp_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "prdp_id")
           End If
           mdsDatos.Tables(mstrTablaDeta).ImportRow(lDr)
        Next

        grdCopro.DataSource = mdsDatos.Tables(mstrTablaDeta)
        grdCopro.DataBind()

        usrPropCria.cmbCriaRazaExt.Valor = usrProducto.cmbProdRazaExt.Valor
        mLimpiarCopropietarios()
        If grdCopro.Items.Count > 0 Then
            If clsFormatear.gFormatCadena(grdCopro.Items(0).Cells(Columnas.Desde).Text) <> "" Then
                txtFechaNaci.Text = clsFormatear.gFormatFecha2String(grdCopro.Items(0).Cells(Columnas.Desde).Text)
            'Else
                'txtFechaNaci.Text = ""
            End If
            If clsFormatear.gFormatCadena(grdCopro.Items(0).Cells(Columnas.Hasta).Text) <> "" Then
                txtFechaVenta.Text = clsFormatear.gFormatFecha2String(grdCopro.Items(0).Cells(Columnas.Hasta).Text)
            'Else
                'txtFechaVenta.Text = ""
            End If
            If clsFormatear.gFormatCadena(grdCopro.Items(0).Cells(Columnas.CriaId).Text) <> "" And clsFormatear.gFormatCadena(grdCopro.Items(0).Cells(Columnas.CriaId).Text) <> "0" Then
                usrPropCria.Valor = grdCopro.Items(0).Cells(Columnas.CriaId).Text
                optCria.Checked = True
            Else
                'usrPropCria.Limpiar()
                optClie.Checked = True
            End If
        End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
         mstrCmd += " @prdp_prdt_id = " + usrProductoFil.Valor.ToString
			mstrCmd += ", @prdp_clie_id = " + usrPropFil.Valor.ToString
			mstrCmd += ", @fecha_desde = " + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
			mstrCmd += ", @fecha_hasta = " + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)
			mstrCmd += ", @sra_nume_desde = " + IIf(txtSraNumeDesde.Text = "", "0", txtSraNumeDesde.Valor.ToString())
			mstrCmd += ", @sra_nume_hasta = " + IIf(txtSraNumeHasta.Text = "", "0", txtSraNumeHasta.Valor.ToString())
			
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      Dim lintPorc As Integer = 0
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
      If usrProducto.Valor = "0" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar un producto.")
      End If
      If usrPropClie.Valor = "0" And usrPropCria.Valor = "0" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar un cliente o criador.")
      End If
      For Each ldr As DataRow In mdsDatos.Tables(mstrTablaDeta).Select
         With ldr
            lintPorc += IIf(.IsNull("prdp_parti"), 0, .Item("prdp_parti"))
         End With
      Next
      If lintPorc <> 100 And grdCopro.Items.Count > 1 Then
         Throw New AccesoBD.clsErrNeg("La suma de los porcentajes de las participaciones debe sumar 100.")
      End If
   End Sub
   Private Sub mGuardarDatos()

      mValidarDatos()

      With mdsDatos.Tables(mstrTabla).Rows(0)
         .Item("prdt_id") = usrProducto.Valor
         .Item("prdt_prop_clie_id") = usrPropClie.Valor
         .Item("prdt_prop_cria_id") = usrPropCria.Valor
      End With

      For Each ldr As DataRow In mdsDatos.Tables(mstrTablaDeta).Select
         With ldr
            If grdCopro.Items.Count = 1 Then
                .Item("prdp_parti") = 100
            End If
            .Item("prdp_desde") = txtFechaNaci.Fecha
            .Item("prdp_hasta") = txtFechaVenta.Fecha
            If usrPropCria.Valor.ToString <> "0" Then
                .Item("prdp_cria_id") = usrPropCria.Valor
            Else
                .Item("prdp_cria_id") = DBNull.Value
            End If
         End With
      Next

   End Sub

   Private Sub mGuardarDatosCopro()

        Dim ldrDoc As DataRow

        ldrDoc = mdsDatos.Tables(mstrTablaDeta).Select("prdp_id=" & hdnDetaId.Text)(0)

        With ldrDoc
            .Item("prdp_parti") = txtPorc.Text
            .Item("prdp_desde") = txtFechaNaci.Fecha
            .Item("prdp_hasta") = txtFechaVenta.Fecha
            If usrPropCria.Valor.ToString <> "0" Then
                .Item("prdp_cria_id") = usrPropCria.Valor
            Else
                .Item("prdp_cria_id") = DBNull.Value
            End If
        End With

        grdCopro.DataSource = mdsDatos.Tables(mstrTablaDeta)
        grdCopro.DataBind()

        mLimpiarCopropietarios()

   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panCabecera.Visible = False
      panDeta.Visible = False

      lnkCabecera.Font.Bold = False
      lnkDeta.Font.Bold = False

      lblTitu.Text = ""

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
         Case 2
            panDeta.Visible = True
            lnkDeta.Font.Bold = True
            mCargarCopropietarios()
      End Select
   End Sub

   Private Sub mLimpiarCopropietarios()
        hdnDetaId.Text = ""
        txtNumeClie.Text = ""
        txtNombClie.Text = ""
        txtPorc.Text = ""
   End Sub
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         mConsultar()
         mMostrarPanel(False)
         btnAgre.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
			Dim lstrRptName As String
			Dim lstrRpt As String
			Dim lintFechaD As Integer
			Dim lintFechaH As Integer

			If cmbTipoReporte.SelectedValue = "" Then
				Throw New AccesoBD.clsErrNeg("Debe seleccionar el Tipo de Reportes.")
			End If

			If cmbTipoReporte.SelectedValue = "R" Then
				lstrRptName = "CopropiedadResumido"
			Else
				lstrRptName = "CopropiedadCompleto"
			End If

			lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			If txtFechaDesdeFil.Text = "" Then
				lintFechaD = 0
			Else
				lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
			End If

			If txtFechaHastaFil.Text = "" Then
				lintFechaH = 0
			Else
				lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
			End If

			lstrRpt += "&fecha_desde=" + lintFechaD.ToString()
			lstrRpt += "&fecha_hasta=" + lintFechaH.ToString()
			lstrRpt += "&sra_nume_desde=" + IIf(txtSraNumeDesde.Text = "", "0", txtSraNumeDesde.Valor.ToString())
			lstrRpt += "&sra_nume_hasta=" + IIf(txtSraNumeHasta.Text = "", "0", txtSraNumeHasta.Valor.ToString())
			lstrRpt += "&prdp_prdt_id=" + usrProductoFil.Valor.ToString
			lstrRpt += "&prdp_clie_id=" + usrPropFil.Valor.ToString
			lstrRpt += "&producto=" + IIf(usrProductoFil.Valor.ToString = "0", "Todos", usrProductoFil.txtProdNombExt.Text)
			lstrRpt += "&cliente=" + IIf(usrPropFil.Valor.ToString = "0", "Todos", usrPropFil.txtApelExt.Text)


			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)



		Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

#End Region

Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
    mShowTabs(1)
End Sub

Private Sub lnkDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
    mShowTabs(2)
End Sub

Private Sub btnModiCopro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCopro.Click
   mModificarCopropietario()
End Sub

Private Sub btnDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeta.Click
  mCargarCopropietarios()
End Sub

    Private Sub txtFechaVenta_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFechaVenta.TextChanged

    End Sub
End Class

End Namespace

