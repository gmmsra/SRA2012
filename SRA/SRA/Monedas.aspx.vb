Imports System.Data.SqlClient
Imports Common.MailManager


Namespace SRA


Partial Class Monedas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Monedas
   Private mstrConn As String
   Private mstrCmd As String
   Private mintId As Integer
   Private mintCodi As Integer
   Private mintDesc As Integer
   Private mintCuen As Integer
   Private mintEstado As Integer
   Private mintGriFechaBaja As Integer
   Private mintFechaBaja As Integer
   Private mstrParaPageSize As Integer
   Private mstrItemCons As String
   Private mstrFiltro As String
   Private mstrFiltroDeno As String
   Private mstrFiltroConsul As String = ""
   Private mstrFiltroConsulRPT As String = ""
   Private mstrFiltroRpt As String = ""
   Private mstrCmbOpc As String
#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuen, "S")
            mConsultar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      If (Not clsSQLServer.gMenuPermi(1019, (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtAbre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "mone_abre")
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "mone_desc")
   End Sub

#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mintId = 1
      mintCodi = 2
      mintDesc = 3
      mintCuen = 4
      mintEstado = 5
      mintGriFechaBaja = 6
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

 Public Sub mConsultar()
  Try
   Dim pintCol As Integer

   mstrCmd = "exec " + mstrTabla + "_consul "

   Dim ds As New DataSet
   ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

   grdDato.DataSource = ds
   grdDato.DataBind()
   ds.Dispose()

   mMostrarPanel(False)

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Private Sub grdDato_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDato.ItemDataBound
  Try
   If e.Item.ItemIndex <> -1 Then
    With CType(e.Item.DataItem, DataRowView).Row
     If mintFechaBaja = 0 Then
      For i As Integer = 0 To .Table.Columns.Count - 1
       If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
        mintFechaBaja = i
        Exit For
       End If
      Next
     End If

     If mintFechaBaja <> 0 Then
      If .IsNull(mintFechaBaja) Then
       e.Item.Cells(mintEstado).Text = "ACTIVO"
      Else
       e.Item.Cells(mintEstado).Text = "INACTIVO"
       e.Item.Cells(mintGriFechaBaja).Text = DirectCast(.Item(mintFechaBaja), DateTime).ToString("dd/MM/yyyy HH:mm")
      End If
     End If
    End With
   End If

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lDsDatos As DataSet
      
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(mintId).Text)
      lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With lDsDatos.Tables(0).Rows(0)
         txtAbre.Valor = .Item("mone_abre")
         txtDesc.Valor = .Item("mone_desc")
         cmbCuen.Valor = .Item("mone_cuct_id")
         If Not .IsNull("mone_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("mone_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtAbre.Text = ""
      txtDesc.Text = ""
      cmbCuen.Limpiar()
      lblBaja.Text = ""
      mSetearEditor(True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mValidarDatos()

         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

         lobjGenerica.Alta()

         grdDato.CurrentPageIndex = 0
         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mValidarDatos()

         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

         lobjGenerica.Modi()

         grdDato.CurrentPageIndex = 0
         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      If txtAbre.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la abreviatura.")
      End If
      If txtDesc.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la descripci�n.")
      End If
      If cmbCuen.Valor.ToString = "0" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la cuenta contable.")
      End If
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
      Dim lintCpo As Integer

      With ldsEsta.Tables(0).Rows(0)
         .Item("mone_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("mone_abre") = txtAbre.Valor
         .Item("mone_desc") = txtDesc.Valor
         .Item("mone_cuct_id") = cmbCuen.Valor
      End With

      Return ldsEsta
   End Function
#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "Monedas"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

  Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim pepe As New Common.MailManager

        pepe.VarEmail = "dariovasqueznigro@gmail.com"

        Try
            pepe.EnviaMensaje("test Button1_Click sra", "test Button1_Click")
        Catch ex As System.Exception
            lblTituAbm.Text = "Excepci�n: " & ex.Message
            Do Until (ex.InnerException Is Nothing)
                lblTituAbm.Text = lblTituAbm.Text & " * " & "Excepci�n interna: " & ex.InnerException.ToString()
                ex = ex.InnerException
            Loop

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        AccesoBD.clsMail.gEnviarMail("dariovasqueznigro@gmail.com", "test  AccesoBD sra", "Test de mail clsmail")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        SRA_Neg.clsMail.gEnviarMail("dariovasqueznigro@gmail.com", "test sra_neg sra", "test sra_neg", "", True)
    End Sub
End Class
End Namespace
