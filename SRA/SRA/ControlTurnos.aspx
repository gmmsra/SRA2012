<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ControlTurnos" CodeFile="ControlTurnos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Control de Turnos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
	    function mConsultarCriador(pCria, pRaza, txtId, txtDesc)
	    {
	   	  if (pCria.value != '' && pRaza.value != '')
		  {
			 var vstrRet = LeerCamposXML("CriadoresBuscar", "@raza_id="+pRaza.value+",@cria_nume="+pCria.value, "").split("|");
			 if(vstrRet!='')
			 {
			 	 txtId.value = vstrRet[0];
			 	 txtDesc.value = vstrRet[1];				
			 }
			 else
			 {
				 txtId.value = '';
				 txtDesc.value = '';
			 }
		  }
		  else
		  {
			 txtId.value = '';
			 txtDesc.value = '';
		  }
	    }	
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0" width="100%">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px; HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Control de Turnos</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px" width="2"></TD>
									<TD vAlign="middle" noWrap colSpan="2"><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="none"
											width="99%">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="2" background="imagenes/formiz.jpg" colSpan="3"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg" height="6"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg" height="6"></TD>
																			<TD style="WIDTH: 88%" background="imagenes/formfdofields.jpg" height="6"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" FilDocuNume="True" MuestraDesc="False"
																					FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Clientes" Ancho="800"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblRazaFil" runat="server" cssclass="titulo">Raza:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<table border=0 cellpadding=0 cellspacing=0>
																				<tr>
																				<td>
																				<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" AceptaNull="False"
																							Width="300px" Height="20px" NomOper="razas_cargar" MostrarBotones="False" filtra="true" onchange="javascript:mConsultarCriador(document.all('txtCriaFil'), this, document.all('hdnCriaFilId'), document.all('txtCriaDescFil'));"></cc1:combobox>
																				</td>
																				<td>
																					<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																								onclick="mBotonBusquedaAvanzada('razas','raza_desc','cmbRazaFil','Razas','');"
																								alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																				</td>
																				</tr>
																				</table>			
																			</TD>																							
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 2px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																				height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblCriaFil" runat="server" cssclass="titulo">Criador:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtCriaFil" runat="server" cssclass="cuadrotexto" Width="106px" onchange="javascript:mConsultarCriador(this, document.all('cmbRazaFil'), document.all('hdnCriaFilId'), document.all('txtCriaDescFil'));"
																					MaxLength="12" Obligatorio="false" esdecimal="False" MaxValor="9999999999999"></cc1:numberbox>
																				<CC1:TEXTBOXTAB id="txtCriaDescFil" runat="server" cssclass="cuadrotexto" Width="292px" Obligatorio="false"
																					ReadOnly="True"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblRPFil" runat="server" cssclass="titulo">&nbsp;RP:&nbsp;</asp:label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtRPFil" runat="server" cssclass="cuadrotexto" Width="100px" MaxLength="20"
																					EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:&nbsp;</asp:Label></TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox>&nbsp; &nbsp;
																				<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/"
																					Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" height="20"></TD>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg" height="20"></TD>
																			<TD background="imagenes/formfdofields.jpg" height="20"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 18.15%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD height="10" style="WIDTH: 2px"></TD>
									<TD vAlign="top" align="right" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px"></TD>
									<TD vAlign="top" align="left" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="99%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="ltuc_id" ReadOnly="True" HeaderText="Id.EstablCliente"></asp:BoundColumn>
												<asp:BoundColumn DataField="ltuc_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cliente" HeaderText="Cliente"></asp:BoundColumn>
												<asp:BoundColumn DataField="_criador" HeaderText="Raza/Criador"></asp:BoundColumn>
												<asp:BoundColumn DataField="ltuc_rp" HeaderText="RP"></asp:BoundColumn>
												<asp:BoundColumn DataField="_tipo" HeaderText="Tipo"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10" style="WIDTH: 2px"></td>
								</tr>
								<TR>
									<TD style="WIDTH: 2px"></TD>
									<TD vAlign="middle" width="100%">
										<TABLE id="Table4" cellPadding="0" align="left" border="0" width="100%">
											<TR>
												<TD align="left"></TD>
												<TD><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
														BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
														BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nuevo Turno"
														ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN>&nbsp;</TD>
												<TD align="right">&nbsp;<CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif"
														BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
														ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar" ImageDisable="btnImpr0.gif"></CC1:BOTONIMAGEN></TD>
											</TR>
										</TABLE>
									</TD>
									<TD align="right"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px" width="2"></TD>
									<TD align="center" colSpan="2">
										<DIV style="WIDTH: 99%"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												width="99%" Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 119px" vAlign="middle" align="right">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Raza:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																			<td>
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" AceptaNull="False"
																				Width="239px" Height="20px" NomOper="razas_cargar" MostrarBotones="False" filtra="true" onchange="javascript:mConsultarCriador(document.all('txtCria'), this, document.all('hdnCriaId'), document.all('txtCriaDesc'));"></cc1:combobox>
																			</td>
																			<td>
																				<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																							onclick="mBotonBusquedaAvanzada('razas','raza_desc','cmbRaza','Razas','');"
																							alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																			</td>
																			</tr>
																			</table>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px" vAlign="middle" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<cc1:numberbox id="txtCria" runat="server" cssclass="cuadrotexto" Width="106px" onchange="javascript:mConsultarCriador(this, document.all('cmbRaza'), document.all('hdnCriaId'), document.all('txtCriaDesc'));"
																				MaxLength="12" Obligatorio="false" esdecimal="False" MaxValor="9999999999999"></cc1:numberbox>
																			<CC1:TEXTBOXTAB id="txtCriaDesc" runat="server" cssclass="cuadrotexto" Width="292px" Obligatorio="false"
																				ReadOnly="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px" vAlign="middle" noWrap align="right">
																			<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" ImagesUrl="Images/" Width="68px"
																				Obligatorio="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label id="lblRP" runat="server" cssclass="titulo">RP:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 20px" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtRP" runat="server" cssclass="cuadrotexto" Width="100px" MaxLength="20" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label id="lblTipo" runat="server" cssclass="titulo">Tipo:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 20px" colSpan="2">
																			<cc1:combobox class="combo" id="cmbTipo" runat="server" AceptaNull="True" Width="140px" Obligatorio="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 20px" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="450px" Height="50px" MaxLength="20"
																				Obligatorio="false" EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" colSpan="3">
																			<DIV id="panServ">&nbsp;</DIV>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server" Width="100%">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaFilId" runat="server"></asp:textbox></DIV>
			<DIV style="DISPLAY: none">&nbsp;</DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Control de Turnos');
		</SCRIPT>
	</BODY>
</HTML>
