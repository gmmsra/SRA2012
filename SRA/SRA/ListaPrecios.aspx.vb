Imports System.IO


Namespace SRA


Partial Class ListaPrecios
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

 End Sub



   Protected WithEvents lblAranCopi As System.Web.UI.WebControls.Label



   Protected WithEvents txtRedoDeci As NixorControls.NumberBox
   Protected WithEvents lblRedoDeci As System.Web.UI.WebControls.Label


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub
#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_PreciosLista
   Private mstrAranceles As String = SRA_Neg.Constantes.gTab_PreciosAran

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String
   Private mintinsti As Int32

   Private Enum Columnas As Integer
      Id = 1
      impo_soci = 7
      impo_noso = 8
      impo_adhe = 9
   End Enum


#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()


         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")


            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar(grdDato, False)

            tabLinks.Visible = False
            clsWeb.gInicializarControles(Me, mstrConn)
                    txtFechaVige.Fecha = Today



                Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
               mSetearAdherente()
            End If
         End If



      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mFiltrarGrilla()
      Dim mstrfiltro As String
      Try
         grdArancel.CurrentPageIndex = 0
         With mdsDatos.Tables(mstrAranceles)

            If txtArancelFil.Valor = "" And txtArancelCodiFil.Valor = "" Then
               mstrfiltro = " "
            Else
               If txtArancelFil.Valor <> "" And txtArancelCodiFil.Valor = "" Then
                  mstrfiltro = "_arancel like " + "'%" + txtArancelFil.Valor + "%'"
               Else
                  mstrfiltro = "_codi ='" + txtArancelCodiFil.Valor + "'"
               End If

            End If
            .DefaultView.RowFilter = mstrfiltro

            grdArancel.DataSource = .DefaultView
         End With

         grdArancel.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   'Private Sub mCargarListaCopia()
   '   cmbAranCopi.Items.Clear()
   '   If cmbActiv.SelectedItem.Text <> "(Seleccione)" Then
   '      clsWeb.gCargarRefeCmb(mstrConn, "precios_lista_copi", cmbAranCopi, "S", " @peli_acti_id=" & cmbActiv.Valor)
   '   End If
   'End Sub

   Private Sub mCargarCombos()
      Dim combos As New System.Collections.ArrayList
      With combos
         .Add(cmbActiFil)
         .Add(cmbActiv)
      End With
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActiFil, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActiv, "S")

      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "aranceles", cmbArancel, "S")

   End Sub
   Private Sub mCargarListaPreciosCopiar()
      If Not cmbActiv.Valor Is DBNull.Value Then
         If cmbActiv.Valor <> 0 Then
            clsWeb.gCargarRefeCmb(mstrConn, "precios_lista_copi", cmbAranCopi, "S", cmbActiv.Valor)
         End If
      End If
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Precios_lista, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Precios_lista_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Precios_lista_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Precios_lista_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      grdCoef.Visible = (cmbAranCopi.Valor.ToString <> "" And cmbAranCopi.Valor.ToString <> "0")
      If Not grdCoef.Visible Then
         Session("sCoef") = Nothing
      End If
   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      'Try
      '   grdDato.EditItemIndex = -1
      '   If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
      '      grdDato.CurrentPageIndex = 0
      '   Else
      '      grdDato.CurrentPageIndex = E.NewPageIndex
      '   End If
      '   mConsultar(grdDato, True)

      'Catch ex As Exception
      '   clsError.gManejarError(Me, ex)
      'End Try
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar(grdDato, True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub



#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla

         Case mstrAranceles
            btnBajaAran.Enabled = Not (pbooAlta)
            btnModiAran.Enabled = Not (pbooAlta)

            btnAltaAran.Enabled = pbooAlta


         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)

            btnAlta.Enabled = pbooAlta
      End Select
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

      mCrearDataSet(hdnId.Text)

      btnCopiar.Enabled = False
      cmbRedo.Enabled = False
      cmbAranCopi.Enabled = False
      grdCoef.Visible = False
      Session("sCoef") = Nothing

      grdArancel.CurrentPageIndex = 0

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    lblFechaA.Text = .Item("peli_audi_fecha") '.ToString("dd/MM/yyyy HH:mm")
                    cmbActiv.Valor = .Item("peli_acti_id")
                    txtRecaCtaCte.Valor = .Item("peli_reca_ctac")
                    txtRecaTarj.Valor = .Item("peli_reca_tarj")
                    txtFechaVige.Fecha = .Item("peli_vige_fecha")
                    If (txtFechaAprob.Text.Trim().ToString().Length > 0) Then ' Is DBNull.Value) Then
                        txtFechaAprob.Fecha = .Item("peli_aprob_fecha")
                    End If
                    If (txtFechaAprob.Text.Trim().ToString().Length > 0) Then ' Is DBNull.Value) Then
                        txtFechaAprob.Enabled = False
                    End If

                End With

                mSetearEditor("", False)
                mSetearEditor(mstrAranceles, True)
                mMostrarPanel(True)


            End If
        End Sub
        Private Function mCodArancel() As String
      If Not cmbArancel.Valor Is DBNull.Value Then
         Dim lDsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "aranceles", cmbArancel.Valor.ToString)
         Return lDsDatos.Tables(0).Rows(0).Item("aran_codi")
      End If
   End Function
   Private Sub mSetearAdherente()
            'muestra o no los campos de adherente
            'If Not cmbActiv.Valor Is DBNull.Value Then
            If (cmbActiv.Text.Trim().Length > 0) Then
                Dim lDsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "actividades", cmbActiv.Valor.ToString)
                divAdherente.Style.Add("display", IIf(lDsDatos.Tables(0).Rows(0).Item("acti_adhe"), "inline", "none"))
                sepAdherente.Style.Add("display", IIf(lDsDatos.Tables(0).Rows(0).Item("acti_adhe"), "inline", "none"))
                grdArancel.Columns(Columnas.impo_adhe).Visible = lDsDatos.Tables(0).Rows(0).Item("acti_adhe")
            End If
        End Sub
   Public Sub mEditarDatosArancel(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Dim ldrAran As DataRow

      hdnAranId.Text = E.Item.Cells(1).Text
      ldrAran = mdsDatos.Tables(mstrAranceles).Select("prar_id=" & hdnAranId.Text)(0)
      grdArancel.CurrentPageIndex = 0
      With ldrAran
         cmbArancel.Valor = .Item("prar_aran_id")
         cmbMone.Valor = .Item("prar_mone_id")
         txtImpoSoci.Valor = Format(.Item("prar_impo_soci"), "###0.00")
         txtImpoNoSoci.Valor = Format(.Item("prar_impo_noso"), "###0.00")
         If Not .IsNull("prar_impo_adhe") Then
            txtImpoAdhe.Valor = Format(.Item("prar_impo_adhe"), "###0.00")
         End If
         mRecargos()
         If Not .IsNull("prar_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prar_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With
      mSetearEditor(mstrAranceles, False)
   End Sub
   Private Sub mPasarRecargos()
      hdnRecaTarj.Text = IIf(txtRecaTarj.Valor Is DBNull.Value, "", txtRecaTarj.Valor)
      hdnRecaCtaCte.Text = IIf(txtRecaCtaCte.Valor Is DBNull.Value, "", txtRecaCtaCte.Valor)
   End Sub
   Private Sub mRecargos()
      Dim lstrFiltro As String = txtImpoSoci.Valor.ToString + ";"
      lstrFiltro = lstrFiltro + txtImpoNoSoci.Valor.ToString() + ";"
      lstrFiltro = lstrFiltro + txtImpoAdhe.Valor.ToString() + ";"
      lstrFiltro = lstrFiltro + hdnRecaCtaCte.Text + ";"
      lstrFiltro = lstrFiltro + hdnRecaTarj.Text

      Dim vsRet As String() = SRA_Neg.Utiles.Recargos(lstrFiltro).Split(";")

      txtNoSociRecCtaCte.Valor = vsRet(0)
      txtNoSociRecTarj.Valor = vsRet(1)
      txtsociRecCtaCte.Valor = vsRet(2)
      txtsociRecTarj.Valor = vsRet(3)
      txtAdheRecCtaCte.Valor = vsRet(4)
      txtAdheRecTarj.Valor = vsRet(5)




   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      btnCopiar.Enabled = True
      cmbRedo.Enabled = True
      cmbAranCopi.Enabled = True
      grdCoef.Visible = False
      Session("sCoef") = Nothing
      '  mCargarListaPreciosCopiar()

      mMostrarPanel(True)


   End Sub
   Private Sub mLimpiar()

      hdnId.Text = ""

      cmbActiv.Limpiar()
      txtRecaCtaCte.Text = ""
      txtRecaTarj.Text = ""
            txtFechaVige.Fecha = Today
            txtFechaAprob.Text = ""
      txtFechaAprob.Enabled = True

      cmbAranCopi.Valor = ""

      btnCopiar.Enabled = True
      cmbRedo.Enabled = True
      cmbAranCopi.Enabled = True
      grdCoef.Visible = False
      Session("sCoef") = Nothing

      hdnRecaCtaCte.Text = ""

      hdnRecaTarj.Text = ""


      mLimpiarAranceles()
      grdArancel.CurrentPageIndex = 0
      grdDato.CurrentPageIndex = 0

      mCrearDataSet("")

      lblTitu.Text = ""
      lblFechaA.Text = ""

      txtArancelCodiFil.Valor = ""
      txtArancelFil.Valor = ""

      mSetearEditor("", True)
      mShowTabs(1)

   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         mShowTabs(1)

         grdDato.DataSource = Nothing
         grdDato.DataBind()
      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      tabLinks.Visible = pbooVisi

   End Sub
   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panAran.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkAran.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            ' lblTitu.Text = "Datos de la Lista de Precios "


         Case 2
            panAran.Visible = True
            lnkAran.Font.Bold = True
            ' lblTitu.Text = "Aranceles"
            grdArancel.Visible = True


      End Select
   End Sub
   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Alta()
         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Modi()

         mConsultar(grdDato, True)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar(grdDato, True)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Function mGuardarDatos() As DataSet

      mValidarDatos()

            With mdsDatos.Tables(0).Rows(0)
                .Item("peli_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("peli_acti_id") = cmbActiv.Valor
                .Item("peli_reca_ctac") = txtRecaCtaCte.Valor
                .Item("peli_reca_tarj") = txtRecaTarj.Valor
                .Item("peli_vige_fecha") = txtFechaVige.Fecha
                If (txtFechaAprob.Fecha.Trim().ToString().Length > 0) Then
                    .Item("peli_aprob_fecha") = txtFechaAprob.Fecha
                End If
            End With

            Return mdsDatos
   End Function
   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrAranceles


      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      grdArancel.DataSource = mdsDatos.Tables(mstrAranceles)
      grdArancel.DataBind()


      Session(mstrTabla) = mdsDatos

   End Sub
#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mLimpiar()

      mConsultar(grdDato, False)
   End Sub



#End Region

#Region "Detalle"
   Public Sub grdAran_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdArancel.EditItemIndex = -1
         If (grdArancel.CurrentPageIndex < 0 Or grdArancel.CurrentPageIndex >= grdArancel.PageCount) Then
            grdArancel.CurrentPageIndex = 0
         Else
            grdArancel.CurrentPageIndex = E.NewPageIndex
         End If
         Session("sAranPage") = grdArancel.CurrentPageIndex
         grdArancel.DataSource = mdsDatos.Tables(mstrAranceles)
         grdArancel.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mObtenerCoeficiente(ByVal pintGrupo As Integer, ByVal pintMone As Integer) As Decimal
       Dim ldecCoef As Decimal = 1
       Dim lDr As DataRow
       For Each oItem As DataGridItem In grdCoef.Items
           If oItem.Cells(0).Text = pintGrupo Then
              If pintMone = SRA_Neg.Comprobantes.gMonedaPesos(mstrConn) Then
                 ldecCoef = DirectCast(oItem.FindControl("txtCoefPesos"), NixorControls.NumberBox).Valor
              Else
                 ldecCoef = DirectCast(oItem.FindControl("txtCoefDolar"), NixorControls.NumberBox).Valor
              End If
           End If
       Next
       Return (ldecCoef)
   End Function

   Private Function mRedondear(ByVal pdecValor As Decimal)
      Dim lstrRedo As Decimal = pdecValor
      If cmbRedo.SelectedValue = "S" Then
         If cmbRedoDeci.SelectedValue = "C" Then
            lstrRedo = pdecValor.Round(pdecValor, 1)
         Else
            lstrRedo = pdecValor.Round(pdecValor, 0)
         End If
      End If
      Return (lstrRedo)
   End Function

   Public Sub mCopiarLista()
      Dim ldsDatosTmp = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, cmbAranCopi.Valor)
      Dim ldrDatosNew, ldrDatosPosicNew As DataRow
      Dim ldecCoef As Decimal = 1

      ldsDatosTmp.Tables(0).TableName = mstrTabla
      ldsDatosTmp.Tables(1).TableName = mstrAranceles

      For Each oDataRow As DataRow In mdsDatos.Tables(mstrAranceles).Select
         oDataRow.Delete()
      Next

      For Each oDataRowOri As DataRow In ldsDatosTmp.Tables(mstrAranceles).Select
         ldrDatosNew = mdsDatos.Tables(mstrAranceles).NewRow
         With ldrDatosNew

            .Item("prar_id") = clsSQLServer.gObtenerId(.Table, "prar_id")
            .Item("prar_aran_id") = oDataRowOri.Item("prar_aran_id")
            .Item("_codi") = oDataRowOri.Item("_codi")
            .Item("prar_mone_id") = oDataRowOri.Item("prar_mone_id")
            .Item("_grar_id") = oDataRowOri.Item("_grar_id")
            .Item("_grar_desc") = oDataRowOri.Item("_grar_desc")

            ldecCoef = mObtenerCoeficiente(oDataRowOri.Item("_grar_id"), .Item("prar_mone_id"))

            If oDataRowOri.IsNull("prar_impo_soci") Then
               .Item("prar_impo_soci") = 0
            Else
               .Item("prar_impo_soci") = mRedondear(oDataRowOri.Item("prar_impo_soci") * ldecCoef)
            End If

            If oDataRowOri.IsNull("prar_impo_adhe") Then
               .Item("prar_impo_adhe") = 0
            Else
               .Item("prar_impo_adhe") = mRedondear(oDataRowOri.Item("prar_impo_adhe") * ldecCoef)
            End If

            If oDataRowOri.IsNull("prar_impo_noso") Then
               .Item("prar_impo_noso") = 0
            Else
               .Item("prar_impo_noso") = mRedondear(oDataRowOri.Item("prar_impo_noso") * ldecCoef)
            End If

            .Item("_moneda") = oDataRowOri.Item("_moneda")
            .Item("_arancel") = oDataRowOri.Item("_arancel")
            .Item("_estado") = "Activo"

            .Table.Rows.Add(ldrDatosNew)
         End With

      Next
      mdsDatos.Tables(mstrAranceles).DefaultView.Sort = "_codi"
      grdArancel.DataSource = mdsDatos.Tables(mstrAranceles).DefaultView
      grdArancel.DataBind()

   End Sub
   Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal pOk As Boolean)
      Try
         Dim strFin As String = ""
         If Not pOk Then
            strFin = ", @ejecuta = N "
         End If
         mstrCmd = "exec " + mstrTabla + "_consul @peli_acti_id= " + cmbActiFil.Valor.ToString + strFin

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mActualizarAranceles()
      Try
         mGuardarDatosAranceles()

         mLimpiarAranceles()

         If Session("sAranPage") >= 0 Then
            grdArancel.CurrentPageIndex = Session("sAranPage")
         End If
         grdArancel.DataSource = mdsDatos.Tables(mstrAranceles)
         grdArancel.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

            If (txtFechaAprob.Text.Trim.Length > 0) Then
                If CDate(txtFechaVige.Fecha) < CDate(txtFechaAprob.Fecha) Then
                    Throw New AccesoBD.clsErrNeg("La fecha de vigencia debe ser mayor o igual a la fecha de aprobación.")
                End If
            End If

        End Sub
   Private Sub mGuardarDatosAranceles()
      Dim ldrDatos As DataRow

      If hdnAranId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrAranceles).NewRow
         ldrDatos.Item("prar_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrAranceles), "prar_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrAranceles).Select("prar_id=" & hdnAranId.Text)(0)
      End If

      If cmbArancel.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar un arancel.")
      End If

      If cmbMone.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar un tipo de moneda.")
      End If

      If txtImpoSoci.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Importe (precio) para Socios NO Adherentes.")
      End If

      With ldrDatos

         .Item("prar_aran_id") = cmbArancel.Valor
         .Item("prar_mone_id") = cmbMone.Valor
         .Item("prar_impo_soci") = txtImpoSoci.Valor
         .Item("prar_impo_noso") = txtImpoNoSoci.Valor
                .Item("prar_impo_adhe") = IIf(txtImpoAdhe.Valor = "", DBNull.Value, txtImpoAdhe.Valor)
                .Item("_moneda") = cmbMone.SelectedItem.Text
         .Item("_arancel") = cmbArancel.SelectedItem.Text
         If cmbArancel.SelectedItem.Value <> "" Then
            .Item("_grar_id") = clsSQLServer.gCampoValorConsul(mstrConn, "aranceles_consul @aran_id=" + cmbArancel.SelectedItem.Value, "aran_grar_id")
            If Not .IsNull("_grar_id") Then
               .Item("_grar_desc") = clsSQLServer.gCampoValorConsul(mstrConn, "grupo_aranceles_consul @grar_id=" + .Item("_grar_id").ToString(), "grar_desc")
            End If
         End If
         .Item("_codi") = mCodArancel()

         For i As Integer = 0 To .Table.Columns.Count - 1
            If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
               .Item(i) = DBNull.Value
            End If
         Next

      End With

      If hdnAranId.Text = "" Then
         mdsDatos.Tables(mstrAranceles).Rows.Add(ldrDatos)
      End If

   End Sub
   Private Sub mLimpiarAranceles()
      hdnAranId.Text = ""
      cmbArancel.Limpiar()
      cmbMone.Limpiar()
      txtImpoSoci.Text = ""
      txtImpoNoSoci.Text = ""
      txtNoSociRecCtaCte.Valor = ""
      txtNoSociRecTarj.Valor = ""
      txtsociRecCtaCte.Valor = ""
      txtsociRecTarj.Valor = ""
      txtAdheRecTarj.Valor = ""
      txtAdheRecCtaCte.Valor = ""
      txtImpoAdhe.Valor = ""


      mSetearEditor(mstrAranceles, True)
   End Sub
#End Region

#Region "Importar/Exportar"
   Private Function mValidaDatosXLS(ByVal ds As DataSet) As Boolean
      Try
         With ds.Tables(0)
            For i As Integer = 0 To .Rows.Count - 1
               If .Rows(i).Item("prar_aran_id").ToString = "" Or .Rows(i).Item("prar_mone_id").ToString = "" Or _
                  .Rows(i).Item("prar_impo_soci").ToString = "" Or .Rows(i).Item("prar_impo_noso").ToString = "" Or _
                  (cmbActiv.Valor.ToString = SRA_Neg.Constantes.Actividades.EXPOSICION_GANADERA_PALERMO.ToString And .Rows(i).Item("prar_impo_adhe").ToString = "") Or _
                  Not IsNumeric(.Rows(i).Item("prar_impo_soci")) Or _
                  Not IsNumeric(.Rows(i).Item("prar_impo_noso")) Or _
                  (cmbActiv.Valor.ToString = SRA_Neg.Constantes.Actividades.EXPOSICION_GANADERA_PALERMO.ToString And Not IsNumeric(.Rows(i).Item("prar_impo_adhe"))) Then
                  Return False
               End If
            Next
         End With
         Return True
      Catch
         Return False
      End Try
   End Function
   Private Sub mExportarXLS(ByVal pstrArch As String)
      Try
         If hdnId.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Solo se puede Exportar una lista Existente.")
         End If

         Dim mstrCmd As String = "exec lista_precios_dts_exec @peli_id = " + hdnId.Text
         If clsSQLServer.gExecuteScalar(mstrConn, mstrCmd) <> 1 Then
            Throw New AccesoBD.clsErrNeg("La Exportación a Excel falló.")
         End If
         hdnExpo.Text = pstrArch

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mImportarXLS(ByVal pstrArch As String)
      Try
         Dim ds As New DataSet
         Dim Campos As String
         Dim Condicion As String

         If pstrArch = "" Or Not pstrArch.ToUpper.EndsWith(".XLS") Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar un nombre de Archivo Excel válido.")
         End If

         Campos = "`Lista1$`.`aran_id` as `prar_aran_id`," _
                & "`Lista1$`.`mone_id` as `prar_mone_id`,`Lista1$`.`Moneda` as `mone_desc`,`Lista1$`.`Codigo` as `aran_codi`," _
                & "`Lista1$`.`Arancel` as `aran_desc`,`Lista1$`.`ImporteSocio` as `prar_impo_soci`," _
                & "`Lista1$`.`ImporteAdherente` as `prar_impo_adhe`,`Lista1$`.`ImporteNoSocio` as `prar_impo_noso`"

         Condicion = "WHERE `Lista1$`.`aran_id` <> null"

         ds = clsImporta.clsAbrirArchivo.mImportarExcel(pstrArch, Campos, Condicion)

         If ds.Tables(0).Rows.Count < 1 Then
            Throw New AccesoBD.clsErrNeg("La planilla no contine Datos a Procesar.")
         End If

         If Not mValidaDatosXLS(ds) Then
            Throw New AccesoBD.clsErrNeg("Los datos de la Planilla están Fuera de Formato.")
         End If

         Dim lDrAux As DataRow
         Dim mobj As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())

         mdsDatos.Tables(1).Rows.Clear()

         For Each ldr As DataRow In ds.Tables(0).Select
            lDrAux = mdsDatos.Tables(1).NewRow
            With lDrAux
               .Item("prar_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrAranceles), "prar_id")
               .Item("prar_aran_id") = ldr.Item("prar_aran_id")
               .Item("_codi") = mobj.ObtenerValorCampo(mstrConn, "aranceles", "aran_codi", ldr.Item("prar_aran_id"))
               .Item("_arancel") = mobj.ObtenerValorCampo(mstrConn, "aranceles", "aran_desc", ldr.Item("prar_aran_id"))
               .Item("prar_impo_soci") = ldr.Item("prar_impo_soci")
               .Item("prar_impo_adhe") = ldr.Item("prar_impo_adhe")
               .Item("prar_impo_noso") = ldr.Item("prar_impo_noso")
               .Item("prar_mone_id") = ldr.Item("prar_mone_id")
               .Item("_moneda") = mobj.ObtenerValorCampo(mstrConn, "monedas", "mone_desc", ldr.Item("prar_mone_id"))
               .Item("prar_audi_user") = Session("sUserId").ToString()
               .Item("prar_baja_fecha") = DBNull.Value
               .Item("_estado") = "Activo"
            End With
            mdsDatos.Tables(1).Rows.Add(lDrAux)
         Next

         grdArancel.DataSource = mdsDatos.Tables(1)
         grdArancel.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim lstrArch As String = System.Configuration.ConfigurationSettings.AppSettings("conPathExpoPrecios")
        'lstrArch = HttpContext.Current.Server.MapPath(lstrArch)
        mExportarXLS(lstrArch)
        'mExportarXLS("http://webdev/exportaciones/ListaPreciosSRA.xls")
   End Sub
   Private Sub btnImportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportar.Click
      mImportarXLS(Trim(txtInputFile.Value))
   End Sub
#End Region

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnAltaAran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAran.Click
      mActualizarAranceles()
   End Sub
   Private Sub btnModiAran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAran.Click
      mActualizarAranceles()
   End Sub
   Private Sub btnBajaAran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAran.Click
      Try
         mdsDatos.Tables(mstrAranceles).Select("prar_id=" & hdnAranId.Text)(0).Delete()
         grdArancel.DataSource = mdsDatos.Tables(mstrAranceles)
         grdArancel.DataBind()
         mLimpiarAranceles()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpAran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAran.Click
      mLimpiarAranceles()
   End Sub
   Private Sub lnkAran_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAran.Click
      mShowTabs(2)
      mPasarRecargos()
      mRecargos()
   End Sub
   Private Sub btnCopiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopiar.Click
      Try
         If cmbAranCopi.Valor Is DBNull.Value Or cmbAranCopi.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar una lista de precios.")
         End If

         mCopiarLista()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar(grdDato, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnBuscFil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscFil.Click
      mFiltrarGrilla()
   End Sub
   Private Sub cmbAranCopi_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAranCopi.SelectedIndexChanged
      Dim lstrCmd As String = ""
      If cmbAranCopi.Valor.ToString = "" Or cmbAranCopi.Valor.ToString = "0" Then
         grdCoef.Visible = False
         Session("sCoef") = Nothing
      Else
         lstrCmd = "precios_lista_coeficientes_consul @peli_id=" & cmbAranCopi.Valor.ToString
         clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdCoef)
         grdCoef.Visible = True
         Session("sCoef") = grdCoef.DataSource
      End If
   End Sub
End Class
End Namespace
