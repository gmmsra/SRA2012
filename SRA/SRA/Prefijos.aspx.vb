Imports ReglasValida.Validaciones


Namespace SRA


Partial Class Prefijos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Prefijos_Cabecera
    Private mstrTablaPrefijos As String = SRA_Neg.Constantes.gTab_Prefijos_Detalle
    Private estReserv As String = CType(SRA_Neg.Constantes.Estados.RRGG_Reservado, String)
    Private estNoUtil As String = CType(SRA_Neg.Constantes.Estados.RRGG_NoUtilizado, String)
    Private estVigente As String = CType(SRA_Neg.Constantes.Estados.RRGG_Vigente, String)
    Private estSolicitado As String = CType(SRA_Neg.Constantes.Estados.RRGG_Solicitado, String)
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
    Private mdsDatosPref As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtActa.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "prca_acta")

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaPrefijos)
        txtPrefijoFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pref_desc")
        txtOrdenPref.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pref_orden")
        txtDescPref.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pref_desc")
        txtDescAmplPref.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pref_desc_ampl")
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaPref, "id", "descrip_codi", "T")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoPref, "", "100")
    End Sub
    Public Sub mInicializar()
        'usrCria.Tabla = SRA_Neg.Constantes.gTab_Criadores
        usrCriaFil.Criador = True
        usrCriaFil.AutoPostback = False
        usrCriaFil.FilClaveUnica = False
        usrCriaFil.ColClaveUnica = True
        usrCriaFil.Ancho = 790
        usrCriaFil.Alto = 510
        usrCriaFil.ColDocuNume = False
        usrCriaFil.ColCUIT = True
        usrCriaFil.ColCriaNume = True
        usrCriaFil.FilCUIT = True
        usrCriaFil.FilDocuNume = True
        usrCriaFil.FilAgru = False
        usrCriaFil.FilTarjNume = False


        usrCria.Criador = True
        usrCria.AutoPostback = False
        usrCria.FilClaveUnica = False
        usrCria.ColClaveUnica = True
        usrCria.Ancho = 790
        usrCria.Alto = 510
        usrCria.ColDocuNume = False
        usrCria.ColCUIT = True
        usrCria.ColCriaNume = True
        usrCria.FilCUIT = True
        usrCria.FilDocuNume = True
        usrCria.FilAgru = False
        usrCria.FilTarjNume = False
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridPref_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdPrefijos.EditItemIndex = -1
            If (grdPrefijos.CurrentPageIndex < 0 Or grdPrefijos.CurrentPageIndex >= grdPrefijos.PageCount) Then
                grdPrefijos.CurrentPageIndex = 0
            Else
                grdPrefijos.CurrentPageIndex = E.NewPageIndex
            End If
            grdPrefijos.DataSource = mdsDatos.Tables(mstrTablaPrefijos)
            grdPrefijos.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try

            mstrCmd = "exec " + mstrTabla + "_consul @formato='1'"
            mstrCmd = mstrCmd + ",@Cliente=" + usrClieFil.Valor.ToString
            mstrCmd = mstrCmd + ",@Criador=" + IIf(usrCriaFil.Valor.ToString() = "", "0", usrCriaFil.Valor.ToString())
            mstrCmd = mstrCmd + ",@raza=" + IIf(usrCriaFil.RazaId = "", "0", usrCriaFil.RazaId)
            mstrCmd = mstrCmd + ",@Prefijo='" + txtPrefijoFil.Valor.ToString + "'"
            mstrCmd = mstrCmd + ",@Todos=" + CInt(chkTodos.Checked).ToString
            mstrCmd = mstrCmd + ",@buscar_en=" + CInt(chkBuscFil.Checked).ToString

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorPref(ByVal pbooAlta As Boolean)
        btnBajaPref.Enabled = Not (pbooAlta)
        btnModiPref.Enabled = Not (pbooAlta)
        btnAltaPref.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtFecha.Fecha = .Item("prca_fecha")
                usrClie.Valor = .Item("prca_clie_id")
                usrCria.Valor = .Item("_pref_cria_id")
                    txtFechaAprob.Fecha = IIf(.Item("prca_acep_fecha").ToString.Length > 0, .Item("prca_acep_fecha"), "")
                    txtActa.Valor = IIf(.Item("prca_acta").ToString.Length > 0, .Item("prca_acta"), String.Empty)
                    Me.chkVigTresA.Checked = IIf(.Item("prca_VigTresA") = True, True, False)
                    Me.txtFechaVigenc.Fecha = IIf(.Item("prca_Vigen_fecha").ToString.Length > 0, .Item("prca_Vigen_fecha"), String.Empty)

                If Not .IsNull("prca_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prca_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
            End With

            mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
        Public Sub mEditarDatosPref(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try
                Dim ldrPref As DataRow

                hdnPrefId.Text = E.Item.Cells(1).Text
                ldrPref = mdsDatos.Tables(mstrTablaPrefijos).Select("pref_id=" & hdnPrefId.Text)(0)

                With ldrPref
                    txtOrdenPref.Valor = IIf(.Item("pref_orden") = 0, "", .Item("pref_orden"))
                    If Not (.Item("pref_sexo") Is DBNull.Value) Then
                        cmbSexoPref.Valor = .Item("pref_sexo")
                    Else
                        cmbSexoPref.Valor = ""
                    End If
                    txtDescPref.Valor = IIf(IsDBNull(.Item("pref_desc")), String.Empty, .Item("pref_desc"))
                    txtDescAmplPref.Valor = IIf(IsDBNull(.Item("pref_desc_ampl")), String.Empty, .Item("pref_desc_ampl"))
                    cmbRazaPref.Valor = IIf(IsDBNull(.Item("pref_raza_id")), 0, .Item("pref_raza_id"))

                    cmbEstadoPref.Valor = .Item("pref_esta_id")
                    If Not .IsNull("pref_baja_fecha") Then
                        lblBajaPref.Text = "Prefijo dado de baja en fecha: " & CDate(.Item("pref_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBajaPref.Text = ""
                    End If
                End With
                mSetearEditorPref(False)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mCrearDataSet(ByVal pstrId As String)

            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
            If (LCase(pstrId) = "prefijos") Then
                mdsDatosPref = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
                mdsDatosPref.Tables(0).TableName = mstrTablaPrefijos
            End If


            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrTablaPrefijos


            If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
                mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
            End If
            grdPrefijos.DataSource = mdsDatos.Tables(mstrTablaPrefijos)
            grdPrefijos.DataBind()

            Session(mstrTabla) = mdsDatos
        End Sub
        Private Sub mAgregar()
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
        End Sub
        Private Sub mLimpiar()
        hdnId.Text = ""
        txtFecha.Text = ""
        usrClie.Limpiar()
        usrCria.Limpiar()
        txtFechaAprob.Text = ""
        txtActa.Text = ""
        lblBaja.Text = ""

        Me.chkVigTresA.Checked = False
        Me.txtFechaVigenc.Text = ""

        mLimpiarPref()

        grdDato.CurrentPageIndex = 0
        grdPrefijos.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarPref()
        hdnPrefId.Text = ""
        txtOrdenPref.Text = ""
        cmbSexoPref.Limpiar()
        txtDescPref.Text = ""
        txtDescAmplPref.Text = ""
        cmbRazaPref.Limpiar()
        cmbEstadoPref.Valor = IIf(usrClie.Valor = 0 And usrCria.Valor.ToString = "0", CType(SRA_Neg.Constantes.Estados.RRGG_Reservado, String), CType(SRA_Neg.Constantes.Estados.RRGG_Solicitado, String))
        cmbEstadoPref.Enabled = cmbEstadoPref.Valor = CType(SRA_Neg.Constantes.Estados.RRGG_Solicitado, String)


        lblBajaPref.Text = ""
        mSetearEditorPref(True)
    End Sub
    Private Sub mLimpiarFil()
        usrClieFil.Limpiar()
        usrCriaFil.Limpiar()
        txtPrefijoFil.Text = ""
        chkTodos.Checked = False
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        Dim canVigente As Integer

        If grdPrefijos.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar al menos un Prefijo.")
        End If

        For Each oDataItem As DataGridItem In grdPrefijos.Items
            'Valida el orden nuevamente
            If (CInt(oDataItem.Cells(2).Text) < "1" Or CInt(oDataItem.Cells(2).Text) > "5") And usrClie.Valor.ToString <> "0" Then
                Throw New AccesoBD.clsErrNeg("El Orden de los Prefijos debe estar entre 1 y 5.")
            End If

            If oDataItem.Cells(6).Text = estVigente Then
                canVigente += 1
            End If
        Next

        If usrClie.Valor.ToString = "0" And usrCria.Valor.ToString = "0" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente.")
        End If

        'Valida caso de cliente <> NULL
        If usrClie.Valor.ToString <> "0" Then
                'If txtFechaAprob.Text <> "" And canVigente = 0 Then
                '    Throw New AccesoBD.clsErrNeg("No hay ning�n Prefijo con estado Vigente para aprobar.")
                'End If
                If txtFechaAprob.Text = "" And canVigente > 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de Aprobaci�n.")
            End If
            If txtActa.Valor.ToString = "" And canVigente > 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Acta.")
            End If
            ' Dario 2015-04-23 se elimina esto ya que esta mal
            'If canVigente > 1 Then
            '    Throw New AccesoBD.clsErrNeg("Solo puede haber un estado Vigente por Cabecera.")
            'End If
        End If

    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("prca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("prca_fecha") = txtFecha.Fecha
            If Not usrCria.Valor Is DBNull.Value Then
                .Item("prca_clie_id") = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "criadores", "cria_clie_id", usrCria.Valor.ToString)
            Else
                .Item("prca_clie_id") = IIf(usrClie.Valor = 0, DBNull.Value, usrClie.Valor)
            End If
                '.Item("prca_acep_fecha") = txtFechaAprob.Fecha
                If Me.txtFechaAprob.Fecha.ToString.Length > 0 Then
                    .Item("prca_acep_fecha") = txtFechaAprob.Fecha
                Else
                    .Item("prca_acep_fecha") = DBNull.Value
                End If
                .Item("prca_acta") = txtActa.Valor
            .Item("prca_baja_fecha") = DBNull.Value
            .Item("prca_audi_user") = Session("sUserId").ToString()
                .Item("prca_VigTresA") = IIf(Me.chkVigTresA.Checked, 1, 0)
                If Me.txtFechaVigenc.Fecha.ToString.Length > 0 Then
                    .Item("prca_Vigen_fecha") = Me.txtFechaVigenc.Fecha
                Else
                    .Item("prca_Vigen_fecha") = DBNull.Value
                End If

            End With
    End Sub
    'Seccion de prefijos
    Private Sub mActualizarPref()
        Try
            mGuardarDatosPref()

            mLimpiarPref()
            grdPrefijos.DataSource = mdsDatos.Tables(mstrTablaPrefijos)
            grdPrefijos.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaPref()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaPrefijos).Select("pref_id=" & hdnPrefId.Text)(0)

            'gsz   row.Delete()
            row.Item("pref_esta_id") = SRA_Neg.Constantes.Estados.RRGG_NoVigente

            row.Item("_estado") = "No Vigente"
            row.AcceptChanges()

            'mdsDatos.Tables(0).TableName = mstrTabla Cabecera
            'mdsDatos.Tables(1).TableName = mstrTablaPrefijos Prefijos
            ' Cuidado en el dataset carga 2 tablas y todos lo metodos, siempre toman la 1er tabla

            mdsDatos.Tables(mstrTablaPrefijos).AcceptChanges()

            grdPrefijos.DataSource = mdsDatos.Tables(mstrTablaPrefijos)
            grdPrefijos.DataBind()
            mLimpiarPref()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosPref()
        Dim ldrPref As DataRow

        mValidarDatosPref()

        If hdnPrefId.Text = "" Then
            ldrPref = mdsDatos.Tables(mstrTablaPrefijos).NewRow
            ldrPref.Item("pref_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaPrefijos), "pref_id")
        Else
            ldrPref = mdsDatos.Tables(mstrTablaPrefijos).Select("pref_id=" & hdnPrefId.Text)(0)
        End If

        With ldrPref
            .Item("pref_orden") = IIf(txtOrdenPref.Valor.ToString = "", 0, txtOrdenPref.Valor)
                If cmbSexoPref.Valor.Length > 0 Then
                    .Item("pref_sexo") = cmbSexoPref.Valor
                Else
                    .Item("pref_sexo") = DBNull.Value
                End If
                .Item("pref_desc") = UCase(txtDescPref.Valor)
                .Item("pref_desc_ampl") = txtDescAmplPref.Valor
                If cmbRazaPref.Valor.ToString.Length > 0 Then
                    .Item("pref_raza_id") = cmbRazaPref.Valor
                End If

                .Item("pref_esta_id") = cmbEstadoPref.Valor
                .Item("pref_cria_id") = usrCria.Valor
                .Item("pref_audi_user") = Session("sUserId").ToString()
                .Item("pref_baja_fecha") = DBNull.Value
                .Item("_estado") = cmbEstadoPref.SelectedItem.Text
                .Item("_sexo") = cmbSexoPref.SelectedItem.Text
                .Item("_OrdenPorSistema") = IIf(usrClie.Valor = 0, "1", "0")
            End With
        If (hdnPrefId.Text = "") Then
            mdsDatos.Tables(mstrTablaPrefijos).Rows.Add(ldrPref)
        End If

    End Sub
    Private Sub mValidarDatosPref()
        Dim cantVigen As Integer

        'Falta Descripcion Prefijo
        If txtDescPref.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Descripci�n del Prefijo.")
        End If

        'Orden Incorrecto
        If cmbEstadoPref.Valor.ToString <> estReserv And (txtOrdenPref.Valor.ToString < "1" Or txtOrdenPref.Valor.ToString > "5") Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el n�mero de Orden entre 1 y 5 en la solapa Prefijos.")
        End If

        'Valida la repiticion del orden tanto en numero como en la descripcion
        For Each oDataItem As DataGridItem In grdPrefijos.Items
            If hdnPrefId.Text = "" Then
                If oDataItem.Cells(2).Text = txtOrdenPref.Valor.ToString Then
                    If oDataItem.Cells(4).Text = cmbSexoPref.Valor.ToString Or cmbSexoPref.Valor.ToString = "" Then
                        Throw New AccesoBD.clsErrNeg("El Orden solo se puede repetir cuando los Sexos son Diferentes.")
                    End If
                End If

                If oDataItem.Cells(3).Text = txtDescPref.Valor.ToString And oDataItem.Cells(6).Text = cmbEstadoPref.Valor.ToString Then
                    Throw New AccesoBD.clsErrNeg("No puede haber Prefijos iguales para mismo Estado.")
                End If
            End If

            If oDataItem.Cells(6).Text = estVigente And oDataItem.Cells(1).Text <> hdnPrefId.Text Then
                cantVigen += 1
            End If
        Next

        ' Dario 2015-04-24 
        ''No mas de un vigente por cabecera con cliente <> NULL
        'If cantVigen > 0 And usrClie.Valor.ToString <> "" And cmbEstadoPref.Valor.ToString = estVigente Then
        '    Throw New AccesoBD.clsErrNeg("Solo puede haber un estado Vigente por Cabecera.")
        'End If

        'Un vigente resto No Utilizado
        ' 2015-06-02 Dario se comenta esto para que no me baje los demas activos
        'If cmbEstadoPref.Valor.ToString = estVigente And usrClie.Valor.ToString <> "" Then
        '    For Each dr As DataRow In mdsDatos.Tables(mstrTablaPrefijos).Select
        '        With dr
        '            dr.Item("pref_esta_id") = estNoUtil
        '            dr.Item("_estado") = "No Utilizado"
        '        End With
        '    Next
        'End If

    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panPrefijos.Visible = False
            lnkPrefijos.Font.Bold = False
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    panPrefijos.Visible = True
                    lnkPrefijos.Font.Bold = True
                    If usrClie.Valor = 0 And usrCria.Valor.ToString = "0" Then
                        cmbEstadoPref.Valor = estReserv
                        cmbEstadoPref.Enabled = False
                        txtOrdenPref.Text = ""
                        txtOrdenPref.Enabled = False
                        For Each dr As DataRow In mdsDatos.Tables(mstrTablaPrefijos).Select
                            With dr
                                .Item("pref_esta_id") = estReserv
                                .Item("_estado") = "Reservado"
                                .Item("_OrdenPorSistema") = "1"
                            End With
                        Next
                        grdPrefijos.Columns(2).Visible = False
                    Else
                        If hdnPrefId.Text = "" Then cmbEstadoPref.Valor = estSolicitado
                        For Each dr As DataRow In mdsDatos.Tables(mstrTablaPrefijos).Select
                            With dr
                                If .Item("pref_esta_id") = estReserv Then
                                    .Item("pref_esta_id") = estSolicitado
                                    .Item("_estado") = "Solicitado"
                                End If
                                .Item("_OrdenPorSistema") = "0"
                            End With
                        Next
                        grdPrefijos.Columns(2).Visible = True
                        cmbEstadoPref.Enabled = True
                        txtOrdenPref.Enabled = True
                    End If
                    grdPrefijos.DataSource = mdsDatos.Tables(mstrTablaPrefijos)
                    grdPrefijos.DataBind()
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "Prefijos"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&cliente=" + IIf(usrClieFil.Valor.ToString = "", "0", usrClieFil.Valor.ToString)
            lstrRpt += "&raza=" + IIf(usrCriaFil.RazaId.ToString = "", "0", usrCriaFil.RazaId.ToString)
            lstrRpt += "&criador=" + IIf(usrCriaFil.Valor.ToString = "", "0", usrCriaFil.Valor.ToString)
            lstrRpt += "&prefijo=" + txtPrefijoFil.Valor.ToString
            lstrRpt += "&todos=" + chkTodos.Checked.ToString
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    'Botones Generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones de Prefijos
    Private Sub btnAltaPref_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaPref.Click
        mActualizarPref()
    End Sub
    Private Sub btnBajaPref_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaPref.Click
        mBajaPref()
    End Sub
    Private Sub btnModiPref_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiPref.Click
        mActualizarPref()
    End Sub
    Private Sub btnLimpPref_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpPref.Click
        mLimpiarPref()
    End Sub
    'Solapas
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkPrefijos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrefijos.Click
        mShowTabs(2)
    End Sub
#End Region

End Class

End Namespace
