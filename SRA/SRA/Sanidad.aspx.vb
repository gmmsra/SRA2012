Imports ReglasValida.Validaciones


Namespace SRA


Partial Class Sanidad

    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblResultado As System.Web.UI.WebControls.Label
    Protected WithEvents lblCompradorFil As System.Web.UI.WebControls.Label



    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"

    Private mstrTabla As String = "sanidad"
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mEstablecerPerfil()
        'Dim lbooPermiAlta As Boolean
        'Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
        '   Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtObservaciones.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "sani_obse")
    End Sub

    Public Sub mInicializar()

        usrProductoFil.AutoPostback = False
        usrProductoFil.Ancho = 790
        usrProductoFil.Alto = 510
            'usrProductoFil.usrRazaCriadorExt.FiltroRazas = "@raza_sanidad = 1"

        usrProducto.AutoPostback = True
        usrProducto.Ancho = 790
        usrProducto.Alto = 510
            ' usrProducto.usrRazaCriadorExt.FiltroRazas = "@raza_sanidad = 1"


    End Sub

    Private Sub mCargarCombos()

        clsWeb.gCargarCombo(mstrConn, "enfermedades_cargar", cmbEnfeFil, "Id", "descrip", "T")
        clsWeb.gCargarCombo(mstrConn, "enfermedades_razas_cargar", cmbEnfe, "Id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "enfermedades_resultados_habilitadas_cargar", cmbResu, "Id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "rg_laboratorios_sanidad_cargar", cmbLaboratorio, "Id", "descrip", "S")

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


     Public Sub mCrearDataSet(ByVal pstrId As String)
        Try
            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla


            If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
                mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
            End If
         
            Session(mstrTabla) = mdsDatos
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Public Sub mConsultar()
        Try
            Dim dsDatos As New DataSet

            mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + "@sani_id=" + IIf(hdnId.Text = "", "0", hdnId.Text)
            mstrCmd = mstrCmd + ",@prdt_cria_id=" + IIf(usrProductoFil.CriaOrPropId = "", "0", usrProductoFil.CriaOrPropId)
            mstrCmd = mstrCmd + ",@prdt_raza_id=" + IIf(usrProductoFil.RazaId = "", "0", usrProductoFil.RazaId)
            mstrCmd = mstrCmd + ",@sani_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@sani_enfe_id=" + IIf(cmbEnfeFil.Valor.ToString = "", "0", cmbEnfeFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@sani_venc_fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaVenceDesdeFil.Fecha)
            mstrCmd = mstrCmd + ",@sani_venc_fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaVenceHastaFil.Fecha)
            If usrProductoFil.txtProdNombExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_nomb= " + clsSQLServer.gFormatArg(usrProductoFil.txtProdNombExt.Text, SqlDbType.VarChar)
            End If
            If usrProductoFil.txtRPExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_rp= " + clsSQLServer.gFormatArg(usrProductoFil.txtRPExt.Text, SqlDbType.VarChar)
            End If

            If usrProductoFil.txtSraNumeExt.Text <> "" Then
                mstrCmd = mstrCmd + ", @prdt_sra_nume= " + clsSQLServer.gFormatArg(usrProductoFil.txtSraNumeExt.Text, SqlDbType.Int)
            End If


            dsDatos = clsWeb.gCargarDataSet(mstrConn, mstrCmd)
            grdDato.Visible = True
            grdDato.DataSource = dsDatos
            grdDato.DataBind()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Seteo de Controles"

    Private Sub mSetearEditor(ByVal pbooAlta As Boolean, Optional ByVal pbooBaja As Boolean = False)
        If pbooBaja Then
            btnBaja.Enabled = pbooAlta
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
        Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
        End If
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, E.Item.Cells(1).Text)
        hdnId.Text = E.Item.Cells(1).Text
        ''mCrearDataSet(hdnId.Text)
        ldsEsta.Tables(0).TableName = mstrTabla

        Dim oEnfermedades As New SRA_Neg.Enfermedades(mstrConn, Session("sUserId").ToString())

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("sani_id")
            usrProducto.Valor = .Item("sani_prdt_id")
            usrProducto.CriaOrPropId = .Item("_cria_id")
            txtFecha.Fecha = .Item("sani_fecha")

            txtObservaciones.Valor = .Item("sani_obse")
            clsWeb.gCargarCombo(mstrConn, "enfermedades_razas_cargar", cmbEnfe, "Id", "descrip", "S")

            cmbEnfe.Valor = .Item("sani_enfe_id")
            txtFechaVence.Fecha = DateAdd(DateInterval.Day, oEnfermedades.GetEnfermedadDiasVigenciaById(.Item("sani_enfe_id")), .Item("sani_fecha"))


            clsWeb.gCargarCombo(mstrConn, "enfermedades_resultados_habilitadas_cargar", cmbResu, "Id", "descrip", "S")
            cmbResu.Valor = .Item("sani_resa_id")
            cmbLaboratorio.Valor = .Item("sani_lasa_id")

            If .Item("_sani_baja_fecha") Is DBNull.Value Then
                lblBaja.Text = ""
                mSetearEditor(False)
            Else
                lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("_sani_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                mSetearEditor(False, True)
            End If
        End With

        mMostrarPanel(True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        usrProducto.Limpiar()
        mCrearDataSet("")
        txtFecha.Text = ""
        cmbEnfe.Valor = ""
        cmbResu.Valor = ""
        cmbLaboratorio.Valor = ""
        usrProducto.cmbSexoProdExt.Valor = ""
        txtObservaciones.Text = ""
        lblBaja.Text = ""
        txtFechaVence.Text = ""
        grdDato.CurrentPageIndex = 0

        mSetearEditor(True)
    End Sub

    Private Sub mLimpiarFiltro()
        hdnId.Text = ""

        grdDato.CurrentPageIndex = 0


        usrProductoFil.Limpiar()
        txtFechaVenceDesdeFil.Text = ""
        txtFechaVenceHastaFil.Text = ""

        usrProductoFil.cmbSexoProdExt.Valor = ""

        mConsultar()
    End Sub


    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        mEstablecerPerfil()
    End Sub

#End Region

#Region "Opciones de ABM"

    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()

                Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
            lobjGenerica.Alta()

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
            lobjGenerica.Modi()

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If ValidarNulos(usrProducto.RazaId, False) = 0 Then
            Throw New AccesoBD.clsErrNeg("LaRaza debe ser distinta de Cero.")
        End If


    End Sub

        Private Function mGuardarDatos() As DataSet

            Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
            Dim lintCpo As Integer
            Dim oEnfermedades As New SRA_Neg.Enfermedades(mstrConn, Session("sUserId").ToString())

            mValidarDatos()

            'Modificado el 17/09/2022.
            With ldsDatos.Tables(0).Rows(0)
                .Item("sani_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("sani_fecha") = txtFecha.Fecha
                .Item("sani_enfe_id") = IIf(cmbEnfe.Valor.ToString() = "", "", cmbEnfe.Valor)
                .Item("sani_lasa_id") = IIf(cmbLaboratorio.Valor.ToString() = "", "", cmbLaboratorio.Valor)
                .Item("sani_prdt_id") = usrProducto.Valor.ToString()
                .Item("sani_raza_id") = IIf(usrProducto.RazaId.ToString().Length = 0, "", usrProducto.RazaId)
                .Item("sani_resa_id") = IIf(cmbResu.Valor.ToString().Length = 0, "", cmbResu.Valor)
                .Item("sani_obse") = txtObservaciones.Valor.ToString()
                If (txtFecha.Text.Trim().Length > 0) Then
                    .Item("sani_venc_fecha") = DateAdd("d".ToString, oEnfermedades.GetEnfermedadDiasVigenciaById(.Item("sani_enfe_id")), txtFecha.Fecha)
                End If
            End With
            Return ldsDatos
        End Function

#End Region

#Region "Eventos de Controles"

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub

    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
        Try
            Dim lintFVD As Integer
            Dim lintFVH As Integer
            Dim lstrRptName As String

            lstrRptName = "Sanidad"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&prdt_cria_id=" + IIf(usrProductoFil.CriaOrPropId = "", "0", usrProductoFil.CriaOrPropId)
            lstrRpt += "&prdt_raza_id=" + IIf(usrProductoFil.RazaId = "", "0", usrProductoFil.RazaId)
            lstrRpt += "&sani_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
            lstrRpt += "&sani_enfe_id=" + IIf(cmbEnfe.Valor.ToString = "", "0", cmbEnfe.Valor.ToString)

            If txtFechaVenceDesdeFil.Text = "" Then
                lintFVD = 0
            Else
                lintFVD = CType(clsFormatear.gFormatFechaString(txtFechaVenceDesdeFil.Text, "Int32"), Integer)
            End If

            If txtFechaVenceHastaFil.Text = "" Then
                lintFVH = 0
            Else
                lintFVH = CType(clsFormatear.gFormatFechaString(txtFechaVenceHastaFil.Text, "Int32"), Integer)
            End If


            lstrRpt += "&sani_venc_fecha_desde=" & lintFVD
            lstrRpt += "&sani_venc_fecha_hasta=" & lintFVH

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

End Class
End Namespace
