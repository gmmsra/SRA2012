Namespace SRA

Partial Class InscripcionHembraDonante
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnBuscar As NixorControls.BotonImagen
    Protected WithEvents lblProductoFi As System.Web.UI.WebControls.Label
    Protected WithEvents Combobox1 As NixorControls.ComboBox
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label

    Protected WithEvents usrProd As System.Web.UI.WebControls.Label
    'BORRAR.


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                mdsDatos = Session(mstrTabla)
            End If


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        usrProductoFil.FilSexo = False
        usrProductoFil.Sexo = 0

        usrProducto.FilSexo = False
        usrProducto.Sexo = 0
        usrProducto.ControlCriador_Activo = False

        btnLimp.Visible = False

    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try

            If usrProductoFil.CriaOrPropId.ToString = "0" Then
                Throw New AccesoBD.clsErrNeg("Debe completar Raza y Propietario.")
            Else
                mstrCmd = "exec " + mstrTabla + "_busq "
                mstrCmd += " @prdt_raza_id=" + IIf(usrProductoFil.RazaId.ToString = "", "0", usrProductoFil.RazaId.ToString) 'Raza.
                mstrCmd += ",@prop_cria_id=" + usrProductoFil.CriaOrPropId.ToString 'Propietario.
                mstrCmd += ",@prdt_sexo=0" 'Sexo
                mstrCmd += ",@prdt_id=" + usrProductoFil.Valor.ToString 'Producto.
                mstrCmd += ",@SoloMachoDador=1" 'SoloHembraDonante


                clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
                mMostrarPanel(False)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        Dim cantTablas As Integer = mdsDatos.Tables.Count - 1

        For i As Integer = 1 To cantTablas
            mdsDatos.Tables.Remove(mdsDatos.Tables(1))
        Next

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        usrProducto.Activo = pbooAlta
        usrProducto.FilSexo = False
        usrProducto.Sexo = 0
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        btnModi.Text = "Modificar"

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                If .Item("prdt_dona_fecha").ToString = "" Then
                    txtFecha.Text = ""
                Else
                    txtFecha.Text = .Item("prdt_dona_fecha")
                End If

                txtNroDonante.Valor = .Item("prdt_dona_nume")

                'usrCria.Valor = .Item("prdt_cria_id")
                usrProducto.CriaOrPropId = .Item("prdt_prop_cria_id")
                usrProducto.Valor = .Item("prdt_id")
            End With

            mSetearEditor(False)
            mMostrarPanel(True)
        End If
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        usrProducto.Limpiar()
        usrProducto.FilSexo = False
        usrProducto.Sexo = "0"
        txtFecha.Text = ""

        txtNroDonante.Valor = ""

        mSetearEditor(True)
        mCrearDataSet("")
    End Sub
    Private Sub mLimpiarFiltros()
        usrProductoFil.Limpiar()
        grdDato.DataSource = Nothing
        grdDato.DataBind()
        mMostrarPanel(False)
    End Sub
    Private Sub mCerrar()
        mLimpiar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panFiltro.Visible = Not (pbooVisi)
        grdDato.Visible = Not (pbooVisi)
        panBotones.Visible = Not (pbooVisi)
        panDato.Visible = pbooVisi
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
        If txtFecha.Fecha > Date.Now Then
            Throw New AccesoBD.clsErrNeg("El campo Fecha debe ser menor a la actual")
        End If
        If ExisteHembraDonante() Then
            Throw New AccesoBD.clsErrNeg("El registro que desea guardar ya existe en la base de datos. Si desea modificarlo, b�squelo y luego ed�telo.")
        End If
    End Sub

    Private Function ExisteHembraDonante() As Boolean
        Dim lstrCmd As String
        Dim myConnection As New SqlClient.SqlConnection(mstrConn)
        Dim cmdExecCommand As New SqlClient.SqlCommand
        Dim lintProcutoId As Integer = 0

        Try
            If btnModi.Text <> "Modificar" Then
                'Armo el Query.
                lstrCmd = " exec productos_macho_dador_busq "
                lstrCmd += " @prdt_id=" + clsSQLServer.gFormatArg(usrProducto.Valor, SqlDbType.Int)
                lstrCmd += " ,@prdt_dona_nume=" + txtNroDonante.Valor

                'Ejecuto el Query.
                cmdExecCommand.Connection = myConnection
                cmdExecCommand.CommandText = lstrCmd
                cmdExecCommand.CommandTimeout = 9999

                myConnection.Open()
                lintProcutoId = cmdExecCommand.ExecuteScalar()
                myConnection.Close()

                If lintProcutoId <> 0 Then
                    Return True
                End If
            End If

            Return False

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            Return True
        End Try
    End Function

    Private Function mGuardarDatos() As Integer
        mValidarDatos()

        Dim lstrCmd As String
        Dim myConnection As New SqlClient.SqlConnection(mstrConn)
        Dim cmdExecCommand As New SqlClient.SqlCommand
        Dim lintRecAff As Integer = 0

        'Armo el Query.
        lstrCmd = " exec productos_macho_dador_modi "
        If btnModi.Text = "Modificar" Then
            lstrCmd += " @prdt_id=" + clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
        Else
            lstrCmd += " @prdt_id=" + clsSQLServer.gFormatArg(usrProducto.Valor, SqlDbType.Int)
        End If

        lstrCmd += " ,@prdt_dona_nume=" + txtNroDonante.Valor

        lstrCmd += " ,@prdt_dona_fecha=" + clsFormatear.gFormatFecha2DB(txtFecha.Fecha)

        'Ejecuto el Query.
        cmdExecCommand.Connection = myConnection
        cmdExecCommand.CommandText = lstrCmd
        cmdExecCommand.CommandTimeout = 9999

        myConnection.Open()
        lintRecAff = cmdExecCommand.ExecuteNonQuery()
        myConnection.Close()

        Return lintRecAff

        'With mdsDatos.Tables(0).Rows(0)
        '    If btnModi.Text = "Modificar" Then
        '        .Item("prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
        '    Else
        '        .Item("prdt_id") = clsSQLServer.gFormatArg(usrProducto.Valor, SqlDbType.Int)
        '        .Item("prdt_raza_id") = clsSQLServer.gFormatArg(usrCria.RazaId, SqlDbType.Int)
        '    End If
        '    .Item("prdt_dona_nume") = txtNroDonante.Valor
        '    .Item("prdt_dona_fecha") = txtFecha.Text
        'End With

    End Function

    Private Sub mModi()
        Try
            If mGuardarDatos() Then
                mConsultar()
                mMostrarPanel(False)
            End If

            'mGuardarDatos()
            'Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, context)
            'lobjGenerica.Modi()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mValorParametro(ByVal pstrPara As String) As String
        Dim lstrPara As String
        If pstrPara Is Nothing Then
            lstrPara = ""
        Else
            If pstrPara Is System.DBNull.Value Then
                lstrPara = ""
            Else
                lstrPara = pstrPara
            End If
        End If
        Return (lstrPara)
    End Function

    Private Sub mObtenerUltimoNroDonante()
        Try
            Dim lstrCmd As String
            Dim myConnection As New SqlClient.SqlConnection(mstrConn)
            Dim cmdExecCommand As New SqlClient.SqlCommand
            Dim lintRecAff As Integer = 0

            lstrCmd = " exec productos_hembra_donante_ultimo_numero "
            lstrCmd += " @raza_id=" + usrProductoFil.RazaId.ToString

            'Ejecuto el Query.
            cmdExecCommand.Connection = myConnection
            cmdExecCommand.CommandText = lstrCmd
            cmdExecCommand.CommandTimeout = 9000

            myConnection.Open()
            txtNroDonante.Valor = cmdExecCommand.ExecuteScalar()
            myConnection.Close()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mAgregar()
        Try
            If usrProductoFil.CriaOrPropId.ToString = "0" Then
                Throw New AccesoBD.clsErrNeg("Debe completar Raza y Propietario.")
            Else
                mLimpiar()
                mObtenerUltimoNroDonante()

                usrProducto.CriaOrPropId = usrProductoFil.CriaOrPropId

                If usrProductoFil.Valor <> "0" Then
                    usrProducto.Valor = usrProductoFil.Valor
                End If

                btnModi.Text = "Alta"
                txtFecha.Fecha = Date.Now
                mSetearEditor(True)
                mMostrarPanel(True)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mImprimir()
        Try
            Dim params As String
            Dim lstrRptName As String = "InscripcionHembraDonante"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&prdt_raza_id=" + usrProductoFil.RazaId.ToString
            lstrRpt += "&prdt_cria_id=" + usrProductoFil.CriaOrPropId.ToString
            lstrRpt += "&prdt_id=" + usrProductoFil.Valor.ToString
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        mConsultar()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        mImprimir()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
#End Region


End Class
End Namespace
