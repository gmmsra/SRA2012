<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb"  AutoEventWireup="false" Inherits="SRA.TransferenciaSemen" CodeFile="TransferenciaSemen.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<META content="text/html; charset=windows-1252" http-equiv="Content-Type">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		
		function usrCriaVend_cmbRazaCria_onchange(pRaza)
		{
  		  if (document.all('txtusrCriaVend:cmbRazaCria').value!=null)
		  {		
		  		mSetearRazasPadres(pRaza,'Padre');

		  		document.all('usrCriaComp:cmbRazaCria').value =document.all(pRaza).value;
				document.all('usrCriaComp:cmbRazaCria').onchange();
				
				document.all("usrCriaComp:cmbRazaCria").disabled = true;
				document.all("txtusrCriaComp:cmbRazaCria").disabled = true;
				
				document.all("usrProductoPadre:usrCriadorFil:cmbRazaCria").disabled = true;
				document.all("txtusrProductoPadre:usrCriadorFil:cmbRazaCria").disabled = true;
				
				var strFiltro = document.all(pRaza).value;
				var strRet = LeerCamposXML("razas", strFiltro, "raza_Semen_Control_Crias");
				if(strRet == "2")
				{
					document.all("txtDosiCant").disabled = true;
					document.all("txtCriaCant").disabled = false;
					document.all("txtDosiCant").value = "";
				}
				else if (strRet == "1")
				{
					document.all("txtDosiCant").disabled = false;
					document.all("txtCriaCant").value = "";
					document.all("txtCriaCant").disabled = true;
				}
				else if (strRet == "3")
				{
					document.all("txtDosiCant").value = "";
					document.all("txtDosiCant").disabled = false;
					document.all("txtCriaCant").value = "";
					document.all("txtCriaCant").disabled = false;
				}
				else
				{
					document.all("txtDosiCant").value = "";
					document.all("txtDosiCant").disabled = true;
					document.all("txtCriaCant").value = "";
					document.all("txtCriaCant").disabled = true;
				}
		   }
		}
		
		function mVerStockSemen()
		{
			var lstrCriadorId =document.all('usrCriaComp_txtId').value;
			var lstrCriadorNomb = document.all('usrCriaComp_txtApel').value;
			gAbrirVentanas("Reportes/SaldoSemenStock.aspx?criadorID=" + lstrCriadorId + "&criadorNomb=" + lstrCriadorNomb, 1, "750","550","20","40");
		}
		
		function mVerStockSemenC()
		{
			var lstrCriadorId =document.all('usrCriaComp:txtId').value;
			var lstrCriadorNomb = document.all('usrCriaComp_txtApel').value;
			
			gAbrirVentanas("Reportes/SaldoSemenStock.aspx?criadorID=" + lstrCriadorId + "&criadorNomb=" + lstrCriadorNomb , 1, "750","550","20","40");
		}
		
		function mVerStockSemenV()
		{
			//var lstrCriadorId =document.all('usrProductoPadre_usrCriadorFil_txtId').value;
			//var lstrCriadorNomb = document.all('usrProductoPadre_usrCriadorFil_txtApel').value;
			var lstrCriadorId =document.all('usrCriaVend_txtId').value;
			var lstrCriadorNomb = document.all('usrCriaVend_txtApel').value;
			var lstPId =document.all('usrProductoPadre_txtId').value;
			
			gAbrirVentanas("Reportes/SaldoSemenStock.aspx?criadorID=" + lstrCriadorId + "&criadorNomb=" + lstrCriadorNomb + "&PId=" +lstPId , 1, "750","550","20","40");
		}
		
		function mSetearRazasPadres(pRaza,pControl)
		{
			document.all('usrProducto' + pControl + ':usrCriadorFil:cmbRazaCria').value=	document.all(pRaza).value;
			document.all('usrProducto' + pControl + ':usrCriadorFil:cmbRazaCria').onchange();
		}		
		
		function usrProducto_usrPadre_cmbProdRaza_onchange(pRaza)
		{
			mSetearRazasPadres(pRaza,'Padre');
		}
	
		function mSetearRaza(pRaza,pControl)
		{
			if (document.all('usrProd:usr' + pControl + ':cmbProdRaza').value != document.all(pRaza).value)
			{
				document.all('usrProd:usr' + pControl + ':cmbProdRaza').value =document.all(pRaza).value;
				document.all('usrProd:usr' + pControl + ':cmbProdRaza').onchange();
				document.all('usrCriaVend:cmbRazaCria').value =document.all(pRaza).value;
				document.all('usrCriaVend:cmbRazaCria').onchange();
			}
		}
						
		function mVerErrores()
		{
			var lstrProc = '<%=mintProce%>';
			var lstrId = document.all("hdnDetaId").value;
			if (lstrId == "0" || lstrId == "")
				lstrId = document.all("hdnId").value;
			gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce="+lstrProc+"&id=" + lstrId, 1, "700","400","20","40");
		}
		
		function mCargarTE()
		{
			var lstrClieId =document.all('usrClieVend:txtId').value;
			if (lstrClieId=='')
			{
				lstrClieId='0';
			}
		
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Denuncias de TE&tabla=te_denun_tramite&hdn=hdnDatosTEPop&filtros=" + lstrClieId, 2, "700","400","100","50");
		}
		
		function btnAgre_click()
		{
			document.all('hdnDatosPop').value = "-1";
			//gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=SELECCIONE UNA PLANTILLA&tabla=rg_tramites_planXrg_tramites_tipos&filtros=<%=mintTtraId%>", 8,650,350);
			return(false);
		}
		
		function btnModi_click(pstrEstaBaja)
		{
			esta = document.getElementById('cmbEstado');
			if(! esta)
			{
			return(true);
			}	
			
			if(document.all('cmbEstado').value == pstrEstaBaja)
				{
					if(!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?'))
					return(false);
				}
				return(true);
		}
		
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
					{
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 					document.all('lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					document.all('lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
		}
		
		function cmbRazaFil_change()
		{
		    if(document.all("cmbRazaFil").value!="")
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = true;
				
				document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = true;
				document.all("txtusrProdFil:cmbProdRaza").disabled = true;
			
			}
			else
			{
				document.all("usrCriaFil:cmbRazaCria").value = ""
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = false;
				
				document.all("usrProdFil:cmbProdRaza").value = "";
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = false;
				document.all("txtusrProdFil:cmbProdRaza").disabled = false;
			
			}
		}
		
		function usrProd_usrPadre_cmbProdRaza_onchange(pRaza)
		{
			mSetearRazasPadres(pRaza,'Padre');
		}
		
		function usrProd_usrProducto_cmbProdSexo_onchange()
		{
			if ('<%=mintTtraId%>'=='11' || '<%=mintTtraId%>'=='30' || '<%=mintTtraId%>'=='31' || '<%=mintTtraId%>'=='20' || '<%=mintTtraId%>'=='1')
			{
				if (document.all("usrProd_usrProducto_cmbProdSexo")!=null &&  document.all('usrClieProp:txtCodi')!=null)
				{
					//if(document.all("usrProd_usrProducto_cmbProdSexo").value=="1")
					//	document.all('rowDosisCria').style.display = 'inline';
					//else
					//	document.all('rowDosisCria').style.display = 'none';
				}
			}
		}
		
		function usrClieComp_onchange()
		{
			if (document.all('usrClieComp:txtCodi')!=null  && document.all('usrClieProp:txtCodi')!=null && document.all("cmbPais")!=null)
				mBuscarPaisDefa('usrClieComp');
			var lstrRaza = '';
			if (document.all('usrProd:usrProducto:cmbProdRaza')!=null)
				lstrRaza = document.all('usrProd:usrProducto:cmbProdRaza').value;
			else
				lstrRaza = document.all('hdnRazaId').value;
		    if (lstrRaza != '' && document.all('usrClieComp:txtId').value != '')
			{
				var sFiltro = '@clie_id='+document.all('usrClieComp:txtId').value+',@raza_id='+lstrRaza;
				LoadComboXML("criadores_cliente_cargar", sFiltro, "cmbCriaComp", "S");
			}
			else
				LoadComboXML("criadores_cliente_cargar", "@raza_id=0", "cmbCriaComp", "S");
		}	
		
		function mBuscarPaisDefa(pstrCon)
		{
			if (document.all(pstrCon + ':txtCodi').value != "")
				{
					var strFiltro = "@clie_id = " + document.all(pstrCon + ':txtCodi').value;
					var strRet = LeerCamposXML("dire_clientes", strFiltro, "_dicl_pais_id");
					
					strFiltro = "@pais_defa = 1";
					var strRet2 = LeerCamposXML("paises", strFiltro, "pais_id");
					if (strRet == strRet2)
						document.all("cmbPais").value = "";
					else
						document.all("cmbPais").value = strRet;
				}
			else
				document.all("cmbPais").value = "";
		}
		
		function usrProd_usrProducto_cmbProdRaza_onchange(pRaza)
		{
			var sFiltro = document.all(pRaza).value;
			if(sFiltro != "")
			{
				if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
				{
					document.all("usrProd:usrCria:cmbRazaCria").value = document.all(pRaza).value;
					document.all("usrProd:usrCria:cmbRazaCria").onchange();
					document.all("usrProd:usrCria:cmbRazaCria").disabled = true;
					document.all("txtusrProd:usrCria:cmbRazaCria").disabled = true;
				}
				if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
				{
					document.all("usrProd:usrCria:cmbRazaCria").value = document.all(pRaza).value;
					document.all("usrProd:usrCria:cmbRazaCria").onchange();
					document.all("usrProd:usrCria:cmbRazaCria").disabled = true;
					document.all("txtusrProd:usrCria:cmbRazaCria").disabled = true;
				}
				if (document.all("usrProd:usrPadre:cmbProdRaza")!=null)
				{
					document.all("usrProd:usrPadre:cmbProdRaza").value = document.all(pRaza).value;
					document.all("usrProd:usrPadre:cmbProdRaza").onchange();
					document.all("usrProd:usrPadre:cmbProdRaza").disabled = false;
					document.all("txtusrProd:usrPadre:cmbProdRaza").disabled = false;
				}
			}
			else
			{
				if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
				{
					document.all("usrProd:usrCria:cmbRazaCria").value = "";
					document.all("usrProd:usrCria:cmbRazaCria").onchange();
				}
				if (document.all("usrProd:usrPadre:cmbProdRaza")!=null)
				{	
					document.all("usrProd:usrPadre:cmbProdRaza").value = "";
					document.all("usrProd:usrPadre:cmbProdRaza").onchange();
				
					if (document.all("usrProd:usrPadre:cmbProdAsoc").disabled==false)
					{
						document.all("usrProd:usrCria:cmbRazaCria").disabled = false;
						document.all("txtusrProd:usrCria:cmbRazaCria").disabled = false;
						document.all("usrProd:usrPadre:cmbProdRaza").disabled = false;
						document.all("txtusrProd:usrPadre:cmbProdRaza").disabled = false;
					}
				}
			}
		}
				
		function expandir()
		{
			if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" onunload="gCerrarVentanas();" leftMargin="5"
		rightMargin="0" topMargin="5">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Tr�mites</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" colSpan="3" noWrap><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderStyle="none" BorderWidth="1px"
											width="100%">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																		BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 50px">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE class="FdoFld" border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 16.7%" height="15" align="right">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo"> Nro.Tr�mite:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 85%">
																				<CC1:NUMBERBOX id="txtNumeFil" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="140px"
																					MaxValor="999999999"></CC1:NUMBERBOX></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="15" align="right">
																				<asp:Label id="lblEstadoFil" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox id="cmbEstadoFil" class="combo" runat="server" AceptaNull="False" Width="200px"
																					NomOper="estados_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR style="DISPLAY: inline" id="rowPaisFil" runat="server">
																			<TD align="right">
																				<asp:Label id="lblPaisFil" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox id="cmbPaisFil" class="combo" runat="server" Width="280px" aceptanull="false" nomoper="paises_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR style="DISPLAY: inline" id="rowDivPaisFil" runat="server">
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="10" align="right">
																				<asp:label id="lblFechaDesdeTransSemenFil" runat="server" cssclass="titulo">Fecha Venta Desde:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="15" align="right">
																				<asp:label id="lblProdFil" runat="server" cssclass="titulo">Producto:</asp:label>&nbsp;</TD>
																			<TD>
																				<UC1:PROH id="usrProdFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2" IncluirDeshabilitados="true"
																					Ancho="800" EsPropietario="True" FilTipo="S" AutoPostBack="False" MuestraDesc="false"></UC1:PROH></TD>
																		</TR>
																		<TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right">
																				<asp:Label id="lblCompradorFil" runat="server" cssclass="titulo">Comprador:&nbsp;</asp:Label></TD>
																			<TD>
																				<UC1:CLIE id="usrCompradorFil" runat="server" AceptaNull="false" Tabla="Criadores" Saltos="1,2"
																					FilTipo="S" AutoPostBack="False" MuestraDesc="False" FilDocuNume="True" MostrarBotones="False"
																					FilSociNume="True" Criador="true" CampoVal="Criador"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<asp:checkbox id="chkVisto" Text="Incluir Registros Vistos y/o Aprobados" CssClass="titulo" Runat="server"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<tr>
									<td height="10" colSpan="3"></td>
								</tr>
								<TR>
									<TD vAlign="top" colSpan="3" align="right"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
											OnPageIndexChanged="grdDato_Page">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton5" runat="server" CausesValidation="false" CommandName="Update">
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />
															<!--<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" /></asp:LinkButton>--></asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" HeaderText="id tramite" DataField="tram_id"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="tram_nume" HeaderText="N� Tr�mite" HeaderStyle-HorizontalAlign="Center"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="raza" HeaderText="Raza" HeaderStyle-Width="25%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_sra_nume" HeaderText="HBA Macho" HeaderStyle-Width="8%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="prdt_nomb" HeaderText="Nombre" HeaderStyle-Width="30%"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="tram_comp_cria_id" HeaderText="Comprador"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="tram_vend_cria_id" HeaderText="Vendedor"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="clie_apel" ReadOnly="True" HeaderText="Comprador Apellido/Raz&#243;n Social"
													HeaderStyle-Width="25%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="tram_pres_fecha" HeaderText="Presentacion" DataFormatString="{0:dd/MM/yyyy}"
													HeaderStyle-Width="10%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="tram_oper_fecha" HeaderText="Fecha de Venta" DataFormatString="{0:dd/MM/yyyy}"
													HeaderStyle-Width="10%"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="tram_dosi_cant" HeaderText="Dosis" HeaderStyle-Width="20%"></asp:BoundColumn>
												<asp:BoundColumn DataField="esta_desc" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10" colSpan="3"></td>
								</tr>
								<TR>
									<TD vAlign="top" colSpan="3" align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
											ForeColor="Transparent" ToolTip="Agregar una Transferencia de Semen" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<TR>
									<TD colSpan="3" align="center"><asp:panel id="panLinks" runat="server" cssclass="titulo" width="100%" Visible="False" Height="124%">
											<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Tr�mite </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkRequ" runat="server" cssclass="solapa"
															Width="80px" Height="21px" CausesValidation="False"> Requisitos</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDocu" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Documentaci�n </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkObse" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Comentarios</asp:linkbutton></TD>
													<TD></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkVend" runat="server" cssclass="solapa"
															Width="70px" Height="21px" Visible="False" CausesValidation="False"> Propietarios </asp:linkbutton></TD>
													<TD background="imagenes/tab_fondo.bmp"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkComp" runat="server" cssclass="solapa"
															Width="70px" Height="21px" Visible="False" CausesValidation="False"> Compradores </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											width="100%" Visible="False" Height="116px">
											<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="tabDato" class="FdoFld" border="0" cellPadding="0"
												align="left">
												<TR>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
													<TD vAlign="top" align="right">
														<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="2" align="center">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE style="WIDTH: 100%" id="tabCabecera" border="0" cellPadding="0" align="left">
																<TR>
																	<TD width="10%" noWrap align="right">
																		<asp:Label id="lblInicFecha1" runat="server" cssclass="titulo">Fecha Carga:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtInicFecha" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																		<asp:Label id="lblFinaFecha" runat="server" cssclass="titulo">Fecha Cierre:</asp:Label>
																		<cc1:DateBox id="txtFinaFecha" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD width="10%" noWrap align="right">
																		<asp:Label id="lblFechaTransferencia" runat="server" cssclass="titulo">Fecha Transferencia:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtFechaTransf" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																	</TD>
																<TR>
																	<TD noWrap align="right">
																		<asp:Label id="lblFechaTram1" runat="server" cssclass="titulo">Fecha Recepci�n Tramite:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtFechaTram" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD width="10%" align="right">
																		<asp:Label id="lblNroControl" runat="server" cssclass="titulo">Nro. de Control:</asp:Label></TD>
																	<TD>
																		<cc1:numberbox id="txtNroControl" runat="server" cssclass="cuadrotexto" Width="81px" MaxValor="999999999"
																			Visible="true" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:combobox id="cmbEstado" class="combo" runat="server" cssclass="cuadrotexto" Width="180px"
																			AutoPostBack="True" Enabled="False" name="cmbEstado"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD colSpan="2" noWrap align="center">
																		<asp:Label id="lblTranPlan" runat="server" cssclass="titulo"><br>Se ha detectado m�s de un producto para este tr�mite.<br>Puede modificar los mismos desde la opci�n de Transferencia por Planilla.<br></asp:Label></TD>
																</TR>
																<TR>
																	<TD height="15" vAlign="top" align="right">
																		<asp:Label id="lblCriaVend" runat="server" cssclass="titulo">Vendedor Raza/Criador:</asp:Label>&nbsp;</TD>
																	<TD>
																		<UC1:CLIE id="usrCriaVend" runat="server" AceptaNull="False" Tabla="Criadores" Saltos="1,2"
																			Ancho="800" MuestraDesc="false" FilDocuNume="false" criador="true" ></UC1:CLIE></TD>
																</TR>
																<TR>
																	<TD height="15" vAlign="top" align="right">
																		<asp:Label id="lblCriaComp" runat="server" cssclass="titulo">Comprador Raza/Criador:</asp:Label>&nbsp;</TD>
																	<TD>
																		<UC1:CLIE id="usrCriaComp" runat="server" AceptaNull="False" Tabla="Criadores" Saltos="1,2"
																			Ancho="800" MuestraDesc="false" FilDocuNume="True" criador="true"></UC1:CLIE></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																		align="right">
																		<asp:Label id="lblProductoPadre1" runat="server" cssclass="titulo">Macho:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																		<UC1:PROH id="usrProductoPadre" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																			Ancho="800" EsPropietario="True" FilTipo="S" AutoPostBack="False" MuestraDesc="false"
																			MuestraSoloRazaClieDerivr="true"></UC1:PROH></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR id="rowDosisCria" runat="server">
																	<TD width="10%" align="right">
																		<asp:Label id="lblDosiCant" runat="server" cssclass="titulo">Dosis de Semen:</asp:Label></TD>
																	<TD width="80%" align="left">
																		<cc1:numberbox id="txtDosiCant" runat="server" cssclass="cuadrotexto" Width="81px" MaxValor="999999999"
																			Visible="true" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox>
																		<asp:Label id="lblCriaCant" runat="server" cssclass="titulo">&nbsp;&nbsp;Cantidad Crias:&nbsp;</asp:Label>
																		<cc1:numberbox id="txtCriaCant" runat="server" cssclass="cuadrotexto" Width="81px" MaxValor="999999999"
																			Visible="true" esdecimal="False" EsPorcen="False"></cc1:numberbox></TD>
																</TR>
																<TR style="DISPLAY: none" id="rowVendedor" runat="server">
																	<TD colSpan="2">
																		<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																			<TR>
																				<TD align="right">
																					<asp:Label id="lblTRFstockCantVendedor" runat="server" cssclass="titulo">Cant. Dosis Vendedor:</asp:Label>
																					<cc1:numberbox id="txtstockCantVendedor" runat="server" cssclass="cuadrotexto" Width="81px" MaxValor="999999999"
																						Visible="true" Enabled="False" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																				<TD>
																					<asp:Label id="lblTRFstockCantComprador" runat="server" cssclass="titulo">Cant. Dosis Comprador:</asp:Label>
																					<cc1:numberbox id="txtstockCantComprador" runat="server" cssclass="cuadrotexto" Width="81px" MaxValor="999999999"
																						Visible="true" Enabled="False" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR style="DISPLAY: none">
																	<TD colSpan="2">
																		<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																			<TBODY>
																				<TR>
																					<TD>
																						<asp:Label id="lbldos" runat="server" cssclass="titulo">Cant. Dosis Vendedor:</asp:Label>
																						<cc1:numberbox id="txtuno" runat="server" cssclass="cuadrotexto" Width="81px" MaxValor="999999999"
																							Visible="true" Enabled="False" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																					<asp:Label id="lbltres" runat="server" cssclass="titulo">Cant. Dosis Comprador:</asp:Label>
																					<cc1:numberbox id="txtcua" runat="server" cssclass="cuadrotexto" Width="81px" MaxValor="999999999"
																						Visible="true" Enabled="False" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox>
																	</TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
															</TABLE></TD>
												</TR>
												<TR style="DISPLAY: none">
													<TD colSpan="3" align="center">
														<TABLE style="MARGIN-TOP: 10px" id="Table8" class="marco" cellSpacing="0" cellPadding="0"
															width="100%">
															<TR>
																<TD colSpan="3" align="left"><U>
																		<asp:Label id="lblTituAnal" runat="server" cssclass="titulo">Datos del �ltimo an�lisis</asp:Label></U></TD>
															</TR>
															<TR>
																<TD noWrap align="left">
																	<asp:Label id="lblNroAnal" runat="server" cssclass="titulo">N�mero:&nbsp;</asp:Label>
																	<cc1:numberbox id="txtNroAnal" runat="server" cssclass="cuadrotexto" Width="50px" Enabled="False"></cc1:numberbox>
																	<asp:Label id="lblTipoAnal" runat="server" cssclass="titulo">Tipo de analisis:&nbsp;</asp:Label>
																	<CC1:TEXTBOXTAB id="txtTipoAnal" runat="server" cssclass="cuadrotexto" Width="100px" Enabled="False"
																		MaxLength="4"></CC1:TEXTBOXTAB>
																	<asp:Label id="lblResulAnal" runat="server" cssclass="titulo">Resultado:&nbsp;</asp:Label>
																	<CC1:TEXTBOXTAB id="txtResulAnal" runat="server" cssclass="cuadrotexto" Width="50%" Enabled="False"
																		MaxLength="4"></CC1:TEXTBOXTAB></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR style="DISPLAY: none" id="rowCantEmbr" runat="server">
													<TD colSpan="2">
														<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
															<TR>
																<TD width="18%" align="right">
																	<asp:Label id="lblCantEmbr" runat="server" cssclass="titulo">Cantidad:</asp:Label></TD>
																<TD noWrap>
																	<cc1:numberbox id="txtCantEmbr" runat="server" cssclass="cuadrotexto" AceptaNull="false" Width="81px"
																		MaxValor="999999999" Visible="true" esdecimal="False" EsPorcen="False"></cc1:numberbox>&nbsp;
																	<asp:Label id="lblRecuFecha" runat="server" cssclass="titulo">Fecha Recup.:</asp:Label>&nbsp;
																	<cc1:DateBox id="txtRecuFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																	<asp:Button id="btnTeDenu" runat="server" cssclass="boton" Width="90px" Text="Denuncia TE" Visible="False"></asp:Button>&nbsp;
																	<CC1:TEXTBOXTAB id="txtTeDesc" runat="server" cssclass="cuadrotexto" Width="176px" Visible="False"
																		Enabled="False" ReadOnly="True"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR style="DISPLAY: inline" id="rowImpo" runat="server">
													<TD vAlign="top" align="right">
														<asp:Label id="lblImpo" runat="server" cssclass="titulo">Despachante:&nbsp;</asp:Label></TD>
													<TD>
														<cc1:combobox id="cmbImpo" class="combo" runat="server" Width="280px"></cc1:combobox></TD>
												</TR>
												<TR style="DISPLAY: inline" id="rowDivImpo" runat="server">
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR style="DISPLAY: inline" id="rowPais" runat="server">
													<TD vAlign="top" align="right">
														<asp:Label id="lblPais" runat="server" cssclass="titulo">Pa�s:&nbsp;</asp:Label></TD>
													<TD>
														<cc1:combobox id="cmbPais" class="combo" runat="server" Width="280px" nomoper="paises_cargar"></cc1:combobox></TD>
												</TR>
												<TR style="DISPLAY: inline" id="rowDivPais" runat="server">
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" colSpan="2"><asp:panel id="panRequ" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="tabRequ" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:datagrid id="grdRequ" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdRequ_Page"
															AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
															AllowPaging="True" OnEditCommand="mEditarDatosRequ">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="20px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																			<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="trad_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
																	<HeaderStyle Width="40%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="_pend" HeaderText="Pendiente"></asp:BoundColumn>
																<asp:BoundColumn DataField="_manu" HeaderText="Manual"></asp:BoundColumn>
																<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRequRequ" runat="server" cssclass="titulo">Requisito:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbRequRequ" class="combo" runat="server" cssclass="cuadrotexto" Width="80%"
															NomOper="rg_requisitos_cargar" MostrarBotones="False" filtra="true"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<asp:CheckBox id="chkRequPend" Text="Pendiente" CssClass="titulo" Runat="server" Checked="False"></asp:CheckBox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRequManuTit" runat="server" cssclass="titulo">Manual:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblRequManu" runat="server" cssclass="Desc">Si</asp:Label></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRequFinaFechaTit" runat="server" cssclass="titulo">Fecha Finalizaci�n:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblRequFinaFecha" runat="server" cssclass="Desc"></asp:Label></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="30" vAlign="middle" colSpan="2" align="center">
														<asp:Button id="btnAltaRequ" runat="server" cssclass="boton" Width="100px" Text="Agregar Req."></asp:Button>&nbsp;
														<asp:Button id="btnBajaRequ" runat="server" cssclass="boton" Width="100px" Text="Eliminar Req."></asp:Button>&nbsp;
														<asp:Button id="btnModiRequ" runat="server" cssclass="boton" Width="100px" Text="Modificar Req."></asp:Button>&nbsp;
														<asp:Button id="btnLimpRequ" runat="server" cssclass="boton" Width="100px" Text="Limpiar Req."></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" vAlign="top" colSpan="2" align="center"><asp:panel id="panDocu" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="tabDocu" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="2" align="center">
														<asp:datagrid id="grdDocu" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdDocu_Page"
															OnUpdateCommand="mEditarDatosDocu" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
															CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="15px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																			<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="trdo_id" HeaderText="trdo_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="trdo_path" HeaderText="Documento">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="trdo_refe" HeaderText="Referencia">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblDocuDocu" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtDocuDocu" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG style="CURSOR: hand" id="imgDelDocuDoc" onclick="javascript:mLimpiarPath('txtDocuDocu','imgDelDocuDoc');"
															alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<BR>
														<INPUT style="WIDTH: 340px; HEIGHT: 22px" id="filDocuDocu" name="File1" size="49" type="file"
															runat="server">
													</TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblDocuObse" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtDocuObse" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="80%"
															Height="41px" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="34" vAlign="middle" colSpan="2" align="center">
														<asp:Button id="btnAltaDocu" runat="server" cssclass="boton" Width="110px" Text="Agregar Docum."></asp:Button>&nbsp;
														<asp:Button id="btnBajaDocu" runat="server" cssclass="boton" Width="110px" Text="Eliminar Docum."
															Visible="true"></asp:Button>&nbsp;
														<asp:Button id="btnModiDocu" runat="server" cssclass="boton" Width="110px" Text="Modificar Docum."
															Visible="true"></asp:Button>&nbsp;
														<asp:Button id="btnLimpDocu" runat="server" cssclass="boton" Width="110px" Text="Limpiar Docum."
															Visible="true"></asp:Button>&nbsp;&nbsp;<BUTTON style="WIDTH: 110px; FONT-WEIGHT: bold" id="btnDescDocu" class="boton" onclick="javascript:mDescargarDocumento('hdnDocuId','tramites_docum','txtDocuDocu');"
															runat="server" value="Descargar">Descargar</BUTTON></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" colSpan="2"><asp:panel id="panObse" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="tabObse" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:datagrid id="grdObse" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdObse_Page"
															AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
															AllowPaging="True" OnEditCommand="mEditarDatosObse">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="20px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																			<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="trao_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="trao_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
																	ItemStyle-HorizontalAlign="Center" HeaderText="Fecha"></asp:BoundColumn>
																<asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="trao_obse" HeaderText="Comentarios">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblObseFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:DateBox id="txtObseFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblObseRequ" runat="server" cssclass="titulo">Requisito:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbObseRequ" class="combo" runat="server" cssclass="cuadrotexto" Width="80%"
															NomOper="rg_requisitos_cargar" MostrarBotones="False" filtra="true"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblObseObse" runat="server" cssclass="titulo">Comentario:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtObseObse" runat="server" cssclass="cuadrotexto" Width="80%" Height="41px"
															TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="30" vAlign="middle" colSpan="2" align="center">
														<asp:Button id="btnAltaObse" runat="server" cssclass="boton" Width="100px" Text="Agregar Com."></asp:Button>&nbsp;
														<asp:Button id="btnBajaObse" runat="server" cssclass="boton" Width="100px" Text="Eliminar Com."></asp:Button>&nbsp;
														<asp:Button id="btnModiObse" runat="server" cssclass="boton" Width="100px" Text="Modificar Com."></asp:Button>&nbsp;
														<asp:Button id="btnLimpObse" runat="server" cssclass="boton" Width="100px" Text="Limpiar Com."></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" colSpan="2"><asp:panel id="panVend" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="tabPers" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:datagrid id="grdVend" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdVend_Page"
															AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
															AllowPaging="True">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:BoundColumn Visible="False" DataField="trpe_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="_nombre" HeaderText="Nombre y Apellido/R.Social"></asp:BoundColumn>
																<asp:BoundColumn DataField="trpe_porc" HeaderText="Porcentaje"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" colSpan="2"><asp:panel id="panComp" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="Table2" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:datagrid id="grdComp" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdComp_Page"
															AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
															AllowPaging="True" OnEditCommand="mEditarDatosComp">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="20px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																			<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="trpe_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="_nombre" HeaderText="Nombre y Apellido/R.Social"></asp:BoundColumn>
																<asp:BoundColumn DataField="trpe_porc" HeaderText="Porcentaje"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="1" colSpan="2">
														<asp:Label id="lblNombComp" runat="server" cssclass="titulo"></asp:Label></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 15%" height="1" align="right">
														<asp:Label id="lblPorcComp" runat="server" cssclass="titulo">Porcentaje:</asp:Label></TD>
													<TD style="WIDTH: 85%">
														<cc1:numberbox id="txtPorcComp" runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="100"
															esdecimal="False" MaxLength="3"></cc1:numberbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 15%" align="right">
														<asp:Label id="lblObseComp" runat="server" cssclass="titulo">Observaciones:</asp:Label></TD>
													<TD style="WIDTH: 85%">
														<CC1:TEXTBOXTAB id="txtObseComp" runat="server" cssclass="cuadrotexto" Width="80%" Height="41px"
															TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="30" vAlign="middle" colSpan="3" align="center">
														<asp:Button id="btnModiComp" runat="server" cssclass="boton" Width="100px" Text="Modificar"></asp:Button>&nbsp;
														<asp:Button id="btnLimpComp" runat="server" cssclass="boton" Width="100px" Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
							</TBODY>
						</TABLE>
						</asp:panel>
						<DIV style="DISPLAY: inline" id="divgraba" runat="server">
							<ASP:PANEL id="panBotones" Runat="server">
								<TABLE width="100%">
									<TR>
										<TD align="center">
											<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
									</TR>
									<TR height="30">
										<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
											<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnErr" runat="server" cssclass="boton" Width="80px" Text="Ver Errores" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnStockC" runat="server" cssclass="boton" Width="170px" Text="Stock Semen Comprador"
												Visible="true" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnStockV" runat="server" cssclass="boton" Width="170px" Text="Stock Semen Vendedor"
												Visible="false" CausesValidation="False"></asp:Button></TD>
									</TR>
								</TABLE>
							</ASP:PANEL>
						</DIV>
						<DIV style="DISPLAY: none" id="divproce" runat="server">
							<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						 </asp:panel></DIV>
					</td>
				</tr>
			</table> <!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnRazaId" runat="server"></asp:textbox><asp:textbox id="hdnRequId" runat="server"></asp:textbox><asp:textbox id="hdnDetaId" runat="server"></asp:textbox><asp:textbox id="hdnDocuId" runat="server"></asp:textbox><asp:textbox id="hdnObseId" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnCompId" runat="server"></asp:textbox><asp:textbox id="lblAltaId" runat="server"></asp:textbox><asp:textbox id="hdnMultiProd" runat="server"></asp:textbox><asp:textbox id="hdnTramNro" runat="server"></asp:textbox><asp:textbox id="hdnVendOrig" runat="server"></asp:textbox><asp:textbox id="hdnCompOrig" runat="server"></asp:textbox><asp:textbox id="hdnMsgError" runat="server"></asp:textbox><asp:textbox id="hdnDatosTEPop" runat="server"></asp:textbox><asp:textbox id="hdnTEId" runat="server"></asp:textbox><asp:textbox id="hdnCriaComp" runat="server"></asp:textbox><asp:textbox id="hdnCriaVend" runat="server"></asp:textbox>.
			</DIV>
			<script language="JavaScript">
				function mPorcPeti()
				{
					document.all("divgraba").style.display ="none";
					document.all("divproce").style.display ="inline";
				}
			
				if (document.all('lblAltaId')!=null && document.all('lblAltaId').value!='')
				{
					alert('Se ha asignado el nro de tramite '+document.all('lblAltaId').value);
					document.all('lblAltaId').value = '';
				}
				if ('<%=mintTtraId%>'=='11' || '<%=mintTtraId%>'=='30' || '<%=mintTtraId%>'=='31')
				{
					if (document.all('usrProd:usrProducto:txtSexoId')!=null)
						document.all('usrProd:usrProducto:txtSexoId').value='1';
				}
			</script>
		</form>
	</BODY>
</HTML>
