<%@ Reference Page="~/Actividades.aspx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Facturacion" CodeFile="Facturacion.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Facturaci�n</title>
		<meta content="False" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/facturacion.js"></script>
		<script language="JavaScript">
		function mSetearPrecio(pPrecio)
		{
			if (document.all('hdnListaPrecios')!=null)
				document.all('hdnListaPrecios').value = pPrecio.value;
		}
		function mSetearConcepto(pConc)
		{
			var strRet = LeerCamposXML("conceptos", "@conc_id="+pConc.value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet!='')
			{  
				var lstrCta = strRet.substring(0,1);
				document.all('hdnCta').value = lstrCta;
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCCosConcep').disabled = true;
					document.all('txtcmbCCosConcep').disabled = true;
					document.all('cmbCCosConcep').innerHTML = '';
					document.all('txtcmbCCosConcep').value = '';
				}
				else 
				{
					document.all('cmbCCosConcep').disabled = false;
					document.all('txtcmbCCosConcep').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCCosConcep", "N");					
				}
			}
			mCalcularConceptos();
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');Imprimir();ImprimirNC();NoImprime();CerrarProforma();CerrarProformaCobranza();document.all('hdnImprimir').value='';document.all('hdnImprimirNC').value='';document.all('hdnNoImprimir').value='';document.all('hdnProforma').value='';document.all('hdnProfCobrar').value='';"
		rightMargin="0"> 
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Facturaci�n</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center">
										<!--- encabezado de la factura --->
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" Height="5px" Visible="False" BorderWidth="1px"
												BorderStyle="Solid" width="100%">
												<P align="right">
													<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
														<TR>
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																	<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD vAlign="top" align="right" width="100%" colSpan="3" height="5">
																				<TABLE id="TableAlertas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgCtrlAnalisis" style="DISPLAY: none" runat="server">
																								<A id="imgAlalisis" href="javascript:mControlAcumuladosAnalisis();" runat="server"><IMG alt="Cantidad An�lisis" src="imagenes/laboratory.png" border="0"></A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgCtrlTurn" style="DISPLAY: none" runat="server">
																								<A id="A1" href="javascript:mControlTurnosAbrir();" runat="server"><IMG alt="Control de Turnos" src="imagenes/CtrolTurn.gif" border="0"></A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgTarj" style="DISPLAY: none" runat="server"><A id="imgTarj" href="javascript:mTarjetasAbrir();" runat="server"><IMG alt="Puede operar con tarjetas" src="imagenes/CreditCard.gif" border="0" runat="server" id="imgTarjTele">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgAlertaSaldosAzul" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldos" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosAmarillo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosV" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircAmarillo.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosVerde" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosA" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosRojo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosR" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgEspecAzul" style="DISPLAY: none" runat="server"><A id="imgEspecAzul" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecVerde" style="DISPLAY: none" runat="server"><A id="imgEspecVerde" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecRojo" style="DISPLAY: none" runat="server"><A id="imgEspecRojo" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblFechaIng" runat="server" cssclass="titulo">Fecha Ingreso:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblFechaIngreso" runat="server" cssclass="titulo"></asp:label>&nbsp;
																				<asp:label id="lblFechaIva" runat="server" cssclass="titulo">Fecha Ref. I.V.A.:</asp:label>&nbsp;
																				<cc1:DateBox id="txtFechaIva" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="true"></cc1:DateBox>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" colSpan="3" height="5"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbActi" runat="server" Width="250px" AceptaNull="False" onchange="mCargarCombos()"></cc1:combobox></TD>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClie" runat="server" Saltos="" FilFanta="true" FilCriaNume="False" FilLegaNume="True"
																					FilDocuNume="true" FilSociNume="False" Tabla="Clientes"></UC1:CLIE></TD>
																			<TD width="10%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																				<A style="WIDTH: 1%" id="btnOtrosDatos" href="javascript:mOtrosDatosAbrir();" runat="server">
																					<IMG alt="Otros Datos" src="imagenes/btnOtrosDatos.gif" border="0"> </A>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblSocio" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtSocio" runat="server" cssclass="cuadrotexto" Width="80px" onchange="mCargarClienteSocio()"></cc1:numberbox></TD>
																			<TD style="HEIGHT: 5px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR id="divCriador" runat="server">
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblraza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																			<TD align="right" colSpan="2">
																				<TABLE class="FdoFld" id="tblCria" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																							<table border=0 cellpadding=0 cellspacing=0>
																							<tr>
																							<td>
																							<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="250px" onchange="mCargarCliente()"
																													NomOper="razas_cargar" filtra="true" MostrarBotones="False"></cc1:combobox>
																							</td>
																							<td>
																								<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																											onclick="mBotonBusquedaAvanzada('razas','raza_desc','cmbRaza','Razas','');"
																											alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</td>
																							</tr>
																							</table>
																						</TD>
																						<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																							<asp:label id="lblCriaNume" runat="server" cssclass="titulo"> Nro. Criador:</asp:label>
																							<cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Visible="true" Width="80px"
																								Obligatorio="True" onchange="mCargarCliente()" MaxLength="12" MaxValor="9999999999999" esdecimal="False"></cc1:numberbox></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right" colSpan="3">
																				<DIV id="divClieGene" style="DISPLAY: none" runat="server">
																					<TABLE class="FdoFld" id="tblClieGene" style="WIDTH: 100%" cellPadding="0" align="left"
																						border="0">
																						<TR>
																							<TD style="WIDTH: 18%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblNombApel" runat="server" cssclass="titulo">Nomb.y apel.:</asp:Label>&nbsp;</TD>
																							<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																								<CC1:TEXTBOXTAB id="txtNombApel" runat="server" cssclass="textolibredeshab" Height="28px" Width="400px"
																									AceptaNull="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR id="divExpo" runat="server">
																			<TD align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblExpo" runat="server" cssclass="titulo">Exposiciones:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbExpo" runat="server" Width="250px" NomOper="exposiciones_cargar"></cc1:combobox></TD>
																			<TD style="HEIGHT: 5px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblFormaPago" runat="server" cssclass="titulo">Tipo de pago:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbFormaPago" runat="server" Width="180px" onchange="mTipoPago();"></cc1:combobox></TD>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblTPers" runat="server" cssclass="titulo">Tr�m. personal:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg" colspan="3">
																				<TABLE class="FdoFld" id="tbltabla" style="WIDTH: 100%" cellPadding="0" align="left">
																					<TR>
																						<td>
																							<asp:CheckBox id="chkTPers" onclick=" mSetearCheck();" Checked="True" CssClass="titulo" Runat="server"></asp:CheckBox>&nbsp;
																						</td>
																						<TD align =right >
																							<asp:label id="lblFechaValor" runat="server" cssclass="titulo">Fecha Valor:</asp:label>&nbsp;
																						</TD>
																						<td>
																							<SPAN>
																								<cc1:DateBox id="txtFechaValor" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="true"
																									Enabled="False" onchange="mFechaValor();"></cc1:DateBox>&nbsp;&nbsp;</SPAN>
																						</td>
																						<td  align =right >
																							<asp:label id="lblListaPrecios" runat="server" cssclass="titulo">L. de Precios:</asp:label>&nbsp;
																						</td>
																						<td>
																							<cc1:combobox class="combo" id="cmbListaPrecios" onchange="javascript:mSetearPrecio(this);" runat="server" Width="100px" AceptaNull="False"
																								NomOper="precios_lista_fact_cargar" ></cc1:combobox>
																						</td>
																						<td align =right >
																							<asp:label id="lblCotDolar" runat="server" cssclass="titulo">Cot.Dolar:</asp:label>&nbsp;
																						</td>
																						<td>
																							<cc1:numberbox id="txtCotDolar" runat="server" cssclass="textomonto" Width="40px" Enabled="False"
																								CambiaValor="True" EsDecimal="True" CantMax="4"></cc1:numberbox>
																						</td>
																						<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:label id="Label1" runat="server" cssclass="titulo">Convenio:</asp:label>&nbsp;</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbConv" runat="server" Width="250px" AceptaNull="False" NomOper="convenios_cargar"></cc1:combobox></TD>
																			<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD colSpan="3">
																				<TABLE border=0 style="WIDTH: 80%">
																					<TR>
																						<TD style="HEIGHT: 14px">
																							<DIV id="divEstadoSaldos" style="DISPLAY: none" runat="server">
																								<TABLE class="FdoFld" runat="server" id="tblEstadoSaldos" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 300px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																									cellPadding="0" align="left" border="1">
																									<TR>
																										<TD align="right">
																											<asp:label id="lblSaldoCaCte" runat="server" cssclass="titulo">Saldo de Cta.Cte.:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtSaldoCaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblSaldoCtaSoci" runat="server" cssclass="titulo">Saldo de Cta. Social:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtSaldoCtaSoci" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblImpCuentaCtaCte" runat="server" cssclass="titulo">Importe a cuenta en Cta. Cte.:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtImpCuentaCtaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblImpCuentaCtaSocial" runat="server" cssclass="titulo">Importe a cuenta en Cta. Social:</asp:label>&nbsp;
																										</TD>
																										<TD style="WIDTH: 70px">
																											<CC1:NUMBERBOX id="txtImpCuentaCtaSocial" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblImpAcuses" runat="server" cssclass="titulo">Acuses de Recibo Pend.de Proc.:</asp:label>&nbsp;
																										</TD>
																										<TD style="WIDTH: 70px">
																											<CC1:NUMBERBOX id="txtImpAcuses" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																								</TABLE>
																							</DIV>																						
																							</td>
																							<TD style="HEIGHT: 14px">
																							<DIV id="divProformas" style="DISPLAY: none" runat="server">
																								<TABLE class="FdoFld" runat="server" id="Table3" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 240px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																									cellPadding="0" align="left" border="1">
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfRRGG" runat="server" cssclass="titulo">Total Proformas RRGG:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfRRGG" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfLabo" runat="server" cssclass="titulo">Total Prof.Laboratorio:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfLabo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfExpo" runat="server" cssclass="titulo">Total Prof.Exposiciones:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfExpo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfOtras" runat="server" cssclass="titulo">Total Otras Proformas:</asp:label>&nbsp;
																										</TD>
																										<TD style="WIDTH: 70px">
																											<CC1:NUMBERBOX id="txtTotalProfOtras" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																								</TABLE>
																							</DIV>
																						</TD>
																						<TD style="HEIGHT: 14px" vAlign="top" align="right"><A id="btnProfoPendi" href="javascript:mProformasAbrir();" runat="server"><IMG alt="Proformas Pendientes" src="imagenes/btnProformas.gif" border="0"></A></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" align="right" colspan="3">
																				<asp:ImageButton id="btnSigCabe" ondeactivate="this.style.borderStyle='none';ImageUrl='imagenes/fle.jpg'"
																					runat="server" CausesValidation="False" ToolTip="Posterior" ImageUrl="imagenes/fle.jpg"></asp:ImageButton></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- panel compartido con datos importantes del encabezado --->
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panEnca" runat="server" cssclass="titulo" BorderWidth="2px" Visible="False"
																	Width="100%">
																	<TABLE id="tblEnca" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD width="100%" colSpan="2">
																				<TABLE style="WIDTH: 100%">
																					<TR>
																						<TD align="right" background="imagenes/formfdofields.jpg" colSpan="4"></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 10%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeClien" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;
																						</TD>
																						<TD style="WIDTH: 90%" align="left" background="imagenes/formfdofields.jpg" colSpan="3">
																							<asp:Label id="lblCabeCliente" runat="server" cssclass="titulo" Width="440px"></asp:Label></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 10%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;
																						</TD>
																						<TD style="WIDTH: 40%" align="left" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeActividad" runat="server" cssclass="titulo"></asp:Label></TD>
																						<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeTPag" runat="server" cssclass="titulo">Tipo de Pago:</asp:Label>&nbsp;
																						</TD>
																						<TD style="WIDTH: 40%" align="left" background="imagenes/formfdofields.jpg">
																							<asp:Label id="lblCabeTPago" runat="server" cssclass="titulo"></asp:Label></TD>
																					</TR>
																					<TR>
																						<TD align="right" colSpan="5">
																							<HR width="100%" SIZE="1">
																						</TD>
																					</TR>
																					<TR> <!-- el importe neto pasa a ser el bruto, se cambio de lugar los text y se modifico la leyenda de los label (09/06)-->
																						<TD background="imagenes/formfdofields.jpg" colSpan="5">
																							<TABLE style="WIDTH: 100%" border="0">
																								<TR> 
																									<TD>
																										<asp:Label id="lblCabeTotalNetoT" runat="server" cssclass="titulo">Total neto: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalBruto" runat="server" cssclass="titulo"></asp:Label>
																									</TD>
																									<TD>
																										<asp:Label id="lblCabeTotalIVAT" runat="server" cssclass="titulo">Total I.V.A.: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalIVA" runat="server" cssclass="titulo"></asp:Label>
																									</TD>
																									<TD>
																										<asp:Label id="lblCabeTotalSobreT" runat="server" cssclass="titulo">Total sobretasa: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalSobre" runat="server" cssclass="titulo"></asp:Label>
																									</TD>
																									<TD>
																										<asp:Label id="lblCabeTotalBrutoT" runat="server" cssclass="titulo">Total bruto: $</asp:Label>&nbsp;
																										<asp:Label id="lblCabeTotalNeto" runat="server" cssclass="titulo"></asp:Label>
																									</TD>
																									<TD>
																										<asp:Label id="lblTotDescAplic" ForeColor=#56340C runat="server" cssclass="titulo"></asp:Label>&nbsp;
																										<asp:Label id="lblTotDescAplicMonto" ForeColor=#56340C runat="server" cssclass="titulo"></asp:Label>
																									</TD>
																								</TR>
																							</TABLE>
																						</TD>
																					</TR>
																					<tr id="trSimulacionNCAuto" runat="server">
																						<TD background="imagenes/formfdofields.jpg" colSpan="5">
																							<TABLE style="WIDTH: 100%" border="0">
																								<TR>
																									<TD>
																										<asp:Label id="lblMensajeNC" ForeColor=#ff0000 runat="server" cssclass="titulo"></asp:Label>
																									</td>
																								</TR>
																							</TABLE>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- detalle / solapas aranceles - conceptos-->
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panDeta" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE class="FdoFld" id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 100%" colSpan="2"><!--- grilla compartida -->
																				<asp:datagrid id="grdDetalle" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																					Visible="True" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1"
																					HorizontalAlign="Left" AllowPaging="True" OnPageIndexChanged="grdDetalle_PageChanged" OnEditCommand="mEditarDatosDetalle">
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_id" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_tipo" ReadOnly="True" HeaderText="Tipo"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_codi_aran_concep" HeaderText="C&#243;digo"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_desc_aran_concep" ReadOnly="True" HeaderText="Descripci&#243;n">
																							<ItemStyle HorizontalAlign="Left"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_cant" HeaderText="Cant.">
																							<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
																							<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_cant_sin_cargo" HeaderText="Cant.S/C">
																							<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
																							<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_anim_no_fact" HeaderText="Cant.N/F">
																							<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
																							<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_impo" HeaderText="Importe($)" DataFormatString="{0:F2}">
																							<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
																							<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_inic_rp" HeaderText="Identif. (dde)"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_fina_rp" HeaderText="Identif. (hsta)"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_tram" HeaderText="Tr&#225;mite"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_acta" HeaderText="Acta"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_ccos_codi" HeaderText="C.Cos."></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_exen" HeaderText="Ex"></asp:BoundColumn>
																						<asp:BoundColumn DataField="temp_sin_carg" HeaderText="S/C"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_aran_concep_id" HeaderText="temp_aran_concep_id"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_ccos_id" HeaderText="ccos_id"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_impo_ivai" HeaderText="Importe"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_unit_c_iva" HeaderText="Unit_C_IVA" DataFormatString="{0:F2}"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="temp_unit_s_iva" HeaderText="Unit_S_IVA" DataFormatString="{0:F2}"></asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="2"></TD>
																		</TR>
																		
																		<TR>
																			<TD style="WIDTH: 10%" vAlign="top"><!--- solapas --->
																				<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
																					<TR vAlign="top">
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg"></TD>
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:ImageButton id="btnAran" runat="server" CausesValidation="False" ToolTip="Aranceles" ImageUrl="imagenes/tabArancel1.gif"></asp:ImageButton>
																						</TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg"></TD>
																						<TD style="WIDTH: 50%" align="right" background="imagenes/formfdofields.jpg">
																							<asp:ImageButton id="btnConcep" runat="server" CausesValidation="False" ToolTip="Conceptos" ImageUrl="imagenes/tabConcep1.gif"></asp:ImageButton>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<TD style="WIDTH: 90%"><!--- aranceles -->
																				<asp:panel id="panAran" runat="server" cssclass="titulo" BorderWidth="1px" Visible="False"
																					Width="100%" BorderColor="#A4AFC3">
																					<TABLE class="FdoFld" id="tblaran" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<tr>
																					  <td style="WIDTH: 100%" align="left" colspan="2">
																						<div id="divTotalizador" style="DISPLAY: none; WIDTH: 100%" runat="server" align="center">
																							<TABLE width="100%" class="FdoFld" id="tbTotalizador" cellPadding="0" align="center" runat="server" 
																								style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 100%; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																								border="1">
																							<TR height="10px">
																								<TD style="WIDTH: 50%" align="right" valign="middle" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblTotalizadorTit" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>&nbsp;
																		 						</TD>
																								<TD style="WIDTH: 10%" align="center" valign="middle" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblTotalizadorCantLab" runat="server" cssclass="titulo" ForeColor="Red" Width="100%"></asp:Label>
																								</td>
																								<TD style="WIDTH: 10%" align="right" valign="middle" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblFacturadoTit" runat="server" cssclass="titulo" ForeColor="Red">Factura Actual:</asp:Label>&nbsp;
																								</TD>
																								<TD style="WIDTH: 5%" align="center" valign="middle" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblFacturadoDato" runat="server" cssclass="titulo" ForeColor="Red" Width="100%"></asp:Label>
																								</td>
																								<TD style="WIDTH: 5%" align="right" valign="middle" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblTotalAcumTit" runat="server" cssclass="titulo" ForeColor="Red">Total:</asp:Label>&nbsp;
																								</TD>
																								<TD style="WIDTH: 10%" align="center" valign="middle" background="imagenes/formfdofields.jpg">
																									<asp:Label id="lblTotalAcumDato" runat="server" cssclass="titulo" ForeColor="Red" Width="100%"></asp:Label>
																								</td>
																							</tr>
																							</table>
																						</div>
																					</td>
																				</tr>
																						<TR>
																							<TD colSpan="2">
																								<DIV id="DivAranNormal" style="DISPLAY: none" runat="server">
																									<asp:panel id="panAranNormal" runat="server" cssclass="titulo" Visible="true">
																										<TABLE class="FdoFld" id="AranNormal" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblAran" runat="server" cssclass="titulo">Arancel:</asp:Label>&nbsp;</TD>
																												<TD width="300" background="imagenes/formfdofields.jpg">
																													<cc1:combobox class="combo" id="cmbAran" name="cmbAran" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"
																														onchange="mArancelFac();" NomOper="precios_aran" filtra="true" MostrarBotones="False" TextMaxLength=5></cc1:combobox></TD>
																												<TD><IMG language="javascript" id="btnAranBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																														onclick="btnBuscar_click('precios_aran','Aranceles');" alt="Busqueda avanzada" src="imagenes/Buscar16.gif"
																														border="0">
																												</TD>
																											</TR>
																										</TABLE>
																									</asp:panel>
																								</DIV>
																								</TD>
																						</TR>
																						<TR>
																							<TD colSpan="2">
																								<DIV id="DivAranSobretasa" style="DISPLAY: noner" runat="server">
																									<asp:panel id="panAranSobretasa" runat="server" cssclass="titulo" Visible="true">
																										<TABLE class="FdoFld" id="AranSobretasa" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="Label3" runat="server" cssclass="titulo">Arancel:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<CC1:TEXTBOXTAB id="txtAranSTCodi" runat="server" cssclass="cuadrotexto" Width="50px" AceptaNull="False"
																														Enabled="False"></CC1:TEXTBOXTAB>
																													<CC1:TEXTBOXTAB id="txtAranSTDesc" runat="server" cssclass="cuadrotexto" Width="300px" AceptaNull="False"
																														Enabled="False"></CC1:TEXTBOXTAB>
																												</TD>
																											</TR>
																										</TABLE>
																									</asp:panel>
																								</DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblCantAran" runat="server" cssclass="titulo">Cantidad:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																								<cc1:numberbox id="txtCantAran" runat="server" cssclass="cuadrotexto" Width="48px" AceptaNull="false"
																									onchange="mCalcularImporteAranceles();"></cc1:numberbox>&nbsp;
																								<asp:Label id="lblCantSCAran" runat="server" cssclass="titulo" visible="false">Cantidad S/C :</asp:Label>&nbsp;
																								<cc1:numberbox id="txtCantSCAran" runat="server" cssclass="cuadrotexto" Width="48px" AceptaNull="false"
																									visible="false"></cc1:numberbox>&nbsp;
																								<asp:Label id="lblCantNoFacAranRRGG" runat="server" cssclass="titulo">Cantidad No Fac.:</asp:Label>&nbsp;
																								<cc1:numberbox id="txtCantNoFacAranRRGG" runat="server" cssclass="cuadrotexto" Width="40px" AceptaNull="false"></cc1:numberbox>
																							</TD>

																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblPUnitAran" runat="server" cssclass="titulo">P.Unitario:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 80%" align="left" background="imagenes/formfdofields.jpg">
																								<cc1:numberbox id="txtPUnitAran" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																									Enabled="False" EsDecimal="True"></cc1:numberbox>
																							</TD>

																						</TR>
																						<TR height="5">
																							<TD colSpan="2" height="5">
																								<DIV>
																									<asp:panel id="panIVAAran" runat="server" cssclass="titulo" Visible="False">
																										<TABLE class="FdoFld" id="tblIVAAranRRGG" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblImpoIvaAran" runat="server" cssclass="titulo">Importe I.V.A.:</asp:Label>&nbsp;
																												</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtImpoIvaAran" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox>&nbsp;
																													<asp:Label id="lblTasaIVAAran" runat="server" cssclass="titulo">Tasa I.V.A.:</asp:Label>&nbsp;
																													<cc1:numberbox id="txtTasaIVAAran" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTotalSobreTasa" runat="server" cssclass="titulo">Importe Sobretasa:</asp:Label>&nbsp;
																												</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTotalSobreTasa" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox>&nbsp;
																													<asp:Label id="lblTasaSobreTasa" runat="server" cssclass="titulo">Tasa Sobretasa:</asp:Label>&nbsp;
																													<cc1:numberbox id="txtTasaSobreTasa" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																											</TR>
																										</TABLE>
																									</asp:panel></DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblImpoAran" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																							<TD background="imagenes/formfdofields.jpg">
																								<cc1:numberbox id="txtImpoAran" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																									Enabled="False" EsDecimal="True"></cc1:numberbox>&nbsp;&nbsp;
																								<asp:Label id="lblExentoAran" runat="server" cssclass="titulo">Exento:</asp:Label>&nbsp;
																								<asp:CheckBox id="chkExentoAran" Checked="false" CssClass="titulo" Runat="server" Enabled="False"></asp:CheckBox>&nbsp;
																								<asp:Label id="lblSCargoAran" runat="server" cssclass="titulo">S/cargo:</asp:Label>&nbsp;
																								<asp:CheckBox id="chkSCargoAran" Checked="false" CssClass="titulo" Runat="server"></asp:CheckBox></TD>
																						</TR>
																						<TR height="5"> <!--- datos que le pertenecen solo a arancel RRGG -->
																							<TD colSpan="2" height="5">
																								<DIV>
																									<asp:panel id="panDatosRRGG" runat="server" cssclass="titulo" Visible="False">
																										<TABLE class="FdoFld" id="tblDatosRRGG" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblFechaAranRRGG" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<cc1:DateBox id="txtFechaAranRRGG" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="true"
																														onchange="mFechaRRGG();"></cc1:DateBox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblRPAranDRRGG" runat="server" cssclass="titulo">Identificaci�n (desde):</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<CC1:TEXTBOXTAB id="txtRPAranDdeRRGG" runat="server" cssclass="cuadrotexto" Width="134px" AceptaNull="False"></CC1:TEXTBOXTAB>&nbsp;
																													<asp:Label id="lblRPAranHRRGG" runat="server" cssclass="titulo">Identificaci�n (hasta):</asp:Label>&nbsp;
																													<CC1:TEXTBOXTAB id="txtRPAranHtaRRGG" runat="server" cssclass="cuadrotexto" Width="134px" AceptaNull="False"></CC1:TEXTBOXTAB>&nbsp;
																												</TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTramNume" runat="server" cssclass="titulo">Nro.Tr�mite:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTramNume" runat="server" cssclass="cuadrotexto" Width="88px"></cc1:numberbox>
																											    </TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblActaAranRRGG" runat="server" cssclass="titulo">Acta:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<CC1:TEXTBOXTAB id="txtActaAranRRGG" runat="server" cssclass="cuadrotexto" Width="88px" AceptaNull="False"></CC1:TEXTBOXTAB></TD>
																											</TR>
																										</TABLE>
																									</asp:panel></DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblDescAmpliAran" runat="server" cssclass="titulo">Descrip. Ampliada:</asp:Label>&nbsp;</TD>
																							<TD background="imagenes/formfdofields.jpg">
																								<CC1:TEXTBOXTAB id="txtDescAmpliAran" runat="server" cssclass="textolibredeshab" Height="30px" Width="400px"
																									AceptaNull="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR height="5"> <!--- datos que le pertenecen solo a aranceles de laboratorio (turnos) -->
																							<TD colSpan="2" height="5">
																								<DIV>
																									<asp:panel id="panDatosTurnos" runat="server" cssclass="titulo" Visible="False">
																										<TABLE class="marco" id="tblDatosTurnos" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTurnoSP" runat="server" cssclass="titulo">Sob.Palermo:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 30%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTurnosSP" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True"></cc1:numberbox></TD>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTurnosRT" runat="server" cssclass="titulo">Recar.Turno:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 30%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTurnosRT" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True"></cc1:numberbox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTurnosRTU" runat="server" cssclass="titulo">Recar.Tram.Urg.:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 30%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTurnosRTU" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True"></cc1:numberbox></TD>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTurnosDP" runat="server" cssclass="titulo">Desc.Promoci�n:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 30%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTurnosDP" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True"></cc1:numberbox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTurnosDC" runat="server" cssclass="titulo">Desc.Cantidad:</asp:Label>&nbsp;</TD>
																												<TD colspan="4" style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTurnosDC" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True"></cc1:numberbox></TD>
																											</TR>
																										</TABLE>
																									</asp:panel></DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD vAlign="bottom" align="right" background="imagenes/formfdofields.jpg" colSpan="2"
																								height="5">
																								<DIV id="DivTurnos" style="DISPLAY: none" runat="server"><A id="btnTurnosBov" href="javascript:mTurnosBovinosAbrir();" runat="server"><IMG alt="Turnos de Bovinos" src="imagenes/btnTurnosBovinos.gif" border="0"></A>&nbsp;
																								<A id="btnturnosEqu" href="javascript:mTurnosEquinosAbrir();" runat="server"><IMG alt="Turnos de Equinos" src="imagenes/btnTurnosEquinos.gif" border="0"></A>
																								</DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD vAlign="bottom" align="right" background="imagenes/formfdofields.jpg" colSpan="2"
																								height="5"></TD>
																						</TR>
																						<TR>
																							<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2"
																								height="30">
																								<asp:panel id="PanBotonesDetaAran" runat="server" cssclass="titulo" Width="100%">
																								<asp:Button id="btnAltaAran" runat="server" cssclass="boton" Width="120px" Text="Agregar Arancel"></asp:Button>&nbsp; 
																								<asp:Button id="btnBajaAran" runat="server" cssclass="boton" Width="120px" Text="Eliminar Arancel"></asp:Button>&nbsp; 
																								<asp:Button id="btnModiAran" runat="server" cssclass="boton" Width="120px" Text="Modificar Arancel"></asp:Button>&nbsp; 
																								<asp:Button id="btnLimpAran" runat="server" cssclass="boton" Width="120px" Text="Limpiar Arancel"></asp:Button></asp:panel>
																							</TD>
																						</TR>
																					</TABLE>
																				</asp:panel><!--- detalle  conceptos-->
																				<asp:panel id="panConcep" runat="server" cssclass="titulo" BorderWidth="1px" Visible="False"
																					Width="100%" BorderColor="#A4AFC3">
																					<TABLE class="FdoFld" id="tblConcep" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																						<TR>
																							<TD colSpan="3">
																								<TABLE class="FdoFld" id="tblConcepCombo" style="WIDTH: 100%" cellPadding="0" align="left"
																									border="0">
																									<TR>
																										<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																											<asp:Label id="lblConcep" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
																										<TD width="300" background="imagenes/formfdofields.jpg">
																											<cc1:combobox class="combo" id="cmbConcep" runat="server" cssclass="cuadrotexto" Width="300px" onchange="javascript:mSetearConcepto(this);"
																												Obligatorio="True" NomOper="cuentas_cargar" filtra="true" MostrarBotones="False" TextMaxLength=5></cc1:combobox></TD>
																										<TD><IMG language="javascript" id="btnConcepBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																											onclick="btnBuscar_click('conceptos','Conceptos');" alt="Busqueda avanzada" src="imagenes/Buscar16.gif"
																											border="0">
											                                                           </TD>
																									</TR>
																								</TABLE>
																							</TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblCCosConcep" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																								<table border=0 cellpadding=1 cellspacing=0>
																								<tr>
																								<td>															
																								<cc1:combobox class="combo" id="cmbCCosConcep" runat="server" cssclass="cuadrotexto" Width="300px"																									
																									Obligatorio="True" NomOper="centrosc_cuenta_cargar" filtra="true" MostrarBotones="False" TextMaxLength=6></cc1:combobox>
																								</td>
																								<td>
																									<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																										onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCCosConcep','Centros de Costo','@cuenta='+document.all('hdnCta').value);"
																										alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																								</td>
																								</tr>
																								</table>
																							</TD>
																						</TR>
																						<TR>
																							<TD colSpan="2">
																								<DIV>
																									<asp:panel id="panIVAConcep" runat="server" cssclass="titulo" Visible="False">
																										<TABLE class="FdoFld" id="tblIVAConcep" style="WIDTH: 100%" cellPadding="0" align="left"
																											border="0">
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblImpoIvaConcep" runat="server" cssclass="titulo">Importe I.V.A.:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtImpoIvaConcep" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTasaIVAConcep" runat="server" cssclass="titulo">Tasa I.V.A.:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 40%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTasaIVAConcep" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																											</TR>
																											<TR>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTotalSobreTasaC" runat="server" cssclass="titulo">Sobretasa:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTotalSobreTasaC" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																												<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblTasaSobreTasaC" runat="server" cssclass="titulo">Tasa Sobretasa:</asp:Label>&nbsp;</TD>
																												<TD style="WIDTH: 40%" background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtTasaSobreTasaC" runat="server" cssclass="textomonto" Width="50px" AceptaNull="false"
																														EsDecimal="True" ReadOnly="True"></cc1:numberbox></TD>
																											</TR> <!--- 30/08/06 --ocultado 
																											<TR>
																												<TD align="right" background="imagenes/formfdofields.jpg">
																													<asp:Label id="lblImpoNConcep" runat="server" cssclass="titulo">Importe S/IVA:</asp:Label>&nbsp;</TD>
																												<TD background="imagenes/formfdofields.jpg">
																													<cc1:numberbox id="txtImpoNConcep" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																														ReadOnly="True" EsDecimal="True"></cc1:numberbox></TD>
																											</TR>	!---></TABLE>
																									</asp:panel></DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblImporteConcep" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																							<TD style="WIDTH: 80%" background="imagenes/formfdofields.jpg">
																								<cc1:numberbox id="txtImporteConcep" runat="server" cssclass="cuadrotexto" Width="100px" AceptaNull="false"
																									onchange="mCalcularConceptos();" EsDecimal="True"></cc1:numberbox></TD>
																						</TR>
																						<TR>
																							<TD align="right" background="imagenes/formfdofields.jpg">
																								<asp:Label id="lblDescAmpliConcep" runat="server" cssclass="titulo">Descrip. Ampliada:</asp:Label>&nbsp;</TD>
																							<TD background="imagenes/formfdofields.jpg">
																								<CC1:TEXTBOXTAB id="txtDescAmpliConcep" runat="server" cssclass="textolibredeshab" Height="30px"
																									Width="400px" AceptaNull="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																						</TR>
																						<TR>
																							<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="5"></TD>
																						</TR>
																						<TR>
																							<TD colSpan="2">
																								<DIV style="DISPLAY: none">&nbsp;</DIV>
																							</TD>
																						</TR>
																						<TR>
																							<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2"
																								height="30">
																								<asp:panel id="PanBotonesDetaConcep" runat="server" cssclass="titulo" Width="100%">
																								<asp:Button id="btnAltaConcep" runat="server" cssclass="boton" Width="120px" Text="Agregar Concep."></asp:Button>&nbsp; 
																								<asp:Button id="btnBajaConcep" runat="server" cssclass="boton" Width="120px" Text="Eliminar Concep."></asp:Button>&nbsp; 
																								<asp:Button id="btnModiConcep" runat="server" cssclass="boton" Width="120px" Text="Modificar Concep."></asp:Button>&nbsp; 
																								<asp:Button id="btnLimpiarConcep" runat="server" cssclass="boton" Width="120px" Text="Limpiar Concep."></asp:Button></asp:panel></TD>
																						</TR>
																					</TABLE>
																				</asp:panel>
																			</TD> <!--- detalle - conceptos --->
																																						
																		</TR>
																		<TR> 
																			<TD style="WIDTH: 100%" colSpan="2">
																		<TR>
																			<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2"
																				height="30">
																				<asp:panel id="PanBotonesNavegDeta" runat="server" cssclass="titulo" Width="100%" visible="False">
																					<TABLE style="WIDTH: 100%">
																						<TR>
																							<TD align="right" colSpan="2">
																								<asp:ImageButton id="btnAnteriorDeta" runat="server" CausesValidation="False" ToolTip="Anterior"
																									ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																								<asp:ImageButton id="btnPosteriorDeta" runat="server" CausesValidation="False" ToolTip="Posterior"
																									ImageUrl="imagenes/fle.jpg"></asp:ImageButton>&nbsp;
																							</TD>
																						</TR>
																					</TABLE>
																				</asp:panel></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- cuotas --->
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panCuotas" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE class="FdoFld" id="tblCuotas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="left" background="imagenes/formfdofields.jpg"
																				colSpan="2">
																				<asp:Label id="Label7" runat="server" cssclass="titulo">Datos de las cuotas (cta. cte.)</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 100%" background="imagenes/formfdofields.jpg" colSpan="2">
																				<asp:datagrid id="grdCuotas" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																					CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Left" AllowPaging="True"
																					OnPageIndexChanged="grdCuotas_PageChanged" OnEditCommand="mEditarDatosCuotas">
																					<FooterStyle CssClass="footer"></FooterStyle>
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<Columns>
																						<asp:TemplateColumn>
																							<HeaderStyle Width="20px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																									<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																								</asp:LinkButton>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:BoundColumn Visible="False" DataField="covt_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn DataField="covt_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
																							<HeaderStyle Width="20%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="covt_porc" ReadOnly="True" HeaderText="Porcen." HeaderStyle-HorizontalAlign="Right"
																							ItemStyle-HorizontalAlign="Right">
																							<HeaderStyle Width="40%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:BoundColumn DataField="covt_impo" HeaderText="Importe" DataFormatString="{0:###0.00}" HeaderStyle-HorizontalAlign="Right"
																							ItemStyle-HorizontalAlign="Right">
																							<HeaderStyle Width="40%"></HeaderStyle>
																						</asp:BoundColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="16"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblImpTotalt" runat="server" cssclass="titulo">Importe total $:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblImpTotal" runat="server" cssclass="titulo" Width="128px" ForeColor="#C00000"
																					Font-Bold="True"></asp:Label>&nbsp;</TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCuotFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtCuotFecha" runat="server" cssclass="cuadrotexto" width="68px" Obligatorio="True"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCuotPorc" runat="server" cssclass="titulo">Porcentaje:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtCuotPorc" runat="server" cssclass="textomonto" Width="40px" onchange="mCalcularImporteCuota();"
																					MaxValor="100"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCuotImporte" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtCuotImporte" runat="server" cssclass="textomonto" Width="84px" EsDecimal="True"></cc1:numberbox>&nbsp;
																				<asp:Label id="Label4" runat="server" cssclass="titulo">Saldo $:</asp:Label>&nbsp;
																				<asp:Label id="lblImpoSaldo" runat="server" cssclass="titulo" Width="128px" ForeColor="IndianRed"
																					Font-Bold="True"></asp:Label></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2">
																				<asp:panel id="panBotonesCuotas" runat="server" cssclass="titulo" Width="100%">
																					<asp:Button id="btnAltaCuot" runat="server" cssclass="boton" Width="120px" Text="Agregar Cuota"></asp:Button>&nbsp; 
																					<asp:Button id="btnBajaCuot" runat="server" cssclass="boton" Width="120px" Text="Eliminar Cuota"></asp:Button>&nbsp; 
																					<asp:Button id="btnModiCuot" runat="server" cssclass="boton" Width="120px" Text="Modificar Cuota"></asp:Button>&nbsp; 
																					<asp:Button id="btnLimpiarCuot" runat="server" cssclass="boton" Width="120px" Text="Limpiar Cuota"></asp:Button>
																				</asp:panel>
																			</TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2">
																				<asp:panel id="PanBotonesNavegCuot" runat="server" cssclass="titulo" Visible="False" Width="100%">
																					<TABLE style="WIDTH: 100%">
																						<TR>
																							<TD align="right" colSpan="2">
																								<asp:ImageButton id="btnAnteriorCuot" runat="server" CausesValidation="False" ToolTip="Anterior"
																									ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																								<asp:ImageButton id="btnPosteriorCuot" runat="server" CausesValidation="False" ToolTip="Posterior"
																									ImageUrl="imagenes/fle.jpg"></asp:ImageButton>&nbsp;
																							</TD>
																						</TR>
																					</TABLE>
																				</asp:panel></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- autorizaciones --->
															<TD style="WIDTH: 100%" background="imagenes/formfdofields.jpg" colSpan="2">
																<asp:panel id="panAutoriza" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE class="FdoFld" id="tblAutiza" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="left" background="imagenes/formfdofields.jpg"
																				colSpan="2">
																				<asp:Label id="Label6" runat="server" cssclass="titulo">Autorizaciones</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 100%" background="imagenes/formfdofields.jpg" colSpan="2">
																				<asp:datagrid id="grdAutoriza" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																					AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																					PageSize="200">
																					<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																					<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																					<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																					<Columns>
																						<asp:BoundColumn Visible="False" DataField="coau_id" HeaderText="ID"></asp:BoundColumn>
																						<asp:BoundColumn Visible="False" DataField="coau_auti_id" HeaderText="coau_auti_id"></asp:BoundColumn>
																						<asp:BoundColumn DataField="_auti_desc" ReadOnly="True" HeaderText="Tipo de autorizaci&#243;n">
																							<HeaderStyle Width="35%"></HeaderStyle>
																						</asp:BoundColumn>
																						<asp:TemplateColumn HeaderText="Aprob.">
																							<HeaderStyle Width="5%"></HeaderStyle>
																							<ItemTemplate>
																								<asp:CheckBox ID="chkApro" Runat="server" onclick="mAutoriza(this);"></asp:CheckBox>
																								<DIV runat="server" id="divAutoId" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.coau_auti_id")%></DIV>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:TemplateColumn HeaderText="Comentario">
																							<HeaderStyle Width="60%"></HeaderStyle>
																							<ItemTemplate>
																								<CC1:TEXTBOXTAB id="txtComen" runat="server" onchange="mAutorizaComen(this);" cssclass="cuadrotexto"
																									Width="411px" AceptaNull="False"></CC1:TEXTBOXTAB>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																					</Columns>
																					<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																				</asp:datagrid></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="16"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2"
																				height="30">
																				<asp:panel id="panBotonesNavegAutiza" runat="server" cssclass="titulo" Visible="False" Width="100%">
																					<TABLE style="WIDTH: 100%">
																						<TR>
																							<TD align="center" colSpan="2"><BUTTON class="boton" id="btnAutoUsua" style="WIDTH: 115px" onclick="btnAutoUsua_click();"
																									type="button" runat="server" value="Detalles">Autorizante</BUTTON>
																								<asp:Label id="lblAutoUsuaTit" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																							<TD align="right">
																								<asp:ImageButton id="btnAnteriorAutiza" runat="server" CausesValidation="False" ToolTip="Anterior"
																									ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																								<asp:ImageButton id="btnPosteriorAutoriza" runat="server" CausesValidation="False" ToolTip="Posterior"
																									ImageUrl="imagenes/fle.jpg"></asp:ImageButton>&nbsp;
																							</TD>
																						</TR>
																					</TABLE>
																				</asp:panel></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- Tarjeta --->
															<TD style="WIDTH: 100%" colSpan="2">
																<asp:panel id="panPagoTarjeta" runat="server" cssclass="titulo" Visible="False" Width="100%">
																	<TABLE class="FdoFld" id="tblTarj" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="left" background="imagenes/formfdofields.jpg"
																				colSpan="2">
																				<asp:Label id="Label5" runat="server" cssclass="titulo">Datos de la Tarjeta</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblTarj" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbTarj" runat="server" Width="130px" onchange="mSeleTarj()"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblTarjNro" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtTarjNro" runat="server" cssclass="cuadrotexto" Width="150px" EsTarjeta="true"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblTarjCuo" runat="server" cssclass="titulo">Cant. Cuotas:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtTarjCuo" runat="server" cssclass="cuadrotexto" Width="32px" AceptaNull="false"
																					CantMax="2"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblVtaTel" runat="server" cssclass="titulo">Venta Telef�nica:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<asp:CheckBox id="chkVtaTele" Checked="false" CssClass="titulo" Runat="server" Enabled="False"></asp:CheckBox></TD>
																		</TR>
																		<TR>
																			<TD vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2" height="5"></TD>
																		</TR>
																		<TR>
																			<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2">
																				<asp:panel id="PanBotonesPagoTarjeta" runat="server" cssclass="titulo" Visible="False" Width="100%">
																					<TABLE style="WIDTH: 100%">
																						<TR>
																							<TD align="right" colSpan="2">
																								<asp:ImageButton id="btnAnteriorTarj" runat="server" CausesValidation="False" ToolTip="Anterior"
																									ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																								<asp:ImageButton id="btnPosteriorTarj" runat="server" CausesValidation="False" ToolTip="Posterior"
																									ImageUrl="imagenes/fle.jpg"></asp:ImageButton>&nbsp;
																							</TD>
																						</TR>
																					</TABLE>
																				</asp:panel></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
														<TR> <!--- pie --->
															<TD style="WIDTH: 100%" colSpan="2"><!-- el importe neto pasa a ser el bruto, se cambio de lugar los text y se modifico la leyenda de los label (09/06)-->
																<asp:panel id="panPie" runat="server" cssclass="titulo" Visible="False" Height="230px" Width="100%">
																	<TABLE class="FdoFld" id="tblPie" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblTotNeto" runat="server" cssclass="titulo">TOTALES ----> Neto:</asp:Label>&nbsp;
																			</TD>
																			<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtTotBruto" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																					Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblTotalIVA" runat="server" cssclass="titulo">I.V.A.:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtTotalIVA" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																					Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblTotalST" runat="server" cssclass="titulo">Sobretasa:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtTotalST" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																					Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 80%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblTotBruto" runat="server" cssclass="titulo">Bruto:</asp:Label>&nbsp;
																			</TD>
																			<TD style="WIDTH: 20%" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtTotNeto" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"
																					Enabled="False" EsDecimal="True"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 30%" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="Label2" runat="server" cssclass="titulo">Leyenda:</asp:Label>&nbsp;
																			</TD>
																			<TD background="imagenes/formfdofields.jpg" colSpan="3">
																				<CC1:TEXTBOXTAB id="txtObser" runat="server" cssclass="textolibredeshab" Height="54px" Width="400px"
																					AceptaNull="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB><A id="btnBusLeyenda" href="javascript:mLeyendasAbrir();" runat="server"><IMG style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																						alt="Buscar Leyenda" src="imagenes/Buscar16.gif" border="0"> </A>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="4">
																				<DIV>
																					<asp:panel id="panRRGGoLab" runat="server" cssclass="titulo" Visible="False">
																						<TABLE class="FdoFld" id="RRGGoLab" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																							<TR>
																								<TD style="WIDTH: 22%" align="right" background="imagenes/formfdofields.jpg">
																									<asp:label id="lblComp" runat="server" cssclass="titulo">Comprobante:</asp:label>&nbsp;</TD>
																								<TD background="imagenes/formfdofields.jpg">
																									<cc1:combobox class="combo" id="cmbComp" runat="server" Width="250px" AceptaNull="False" onchange="mSelecTipoComp();"
																										NomOper="comprob_tipos_cargar" AutoPostBack="True"></cc1:combobox></TD>
																							</TR>
																						</TABLE>
																					</asp:panel></DIV>
																			</TD>
																		</TR>
																		<TR>
																			<TD colSpan="4">
																				<asp:Button id="btnAplicCred" runat="server" cssclass="boton" Width="120px" Text="Aplic.cr�ditos"></asp:Button>&nbsp;
																				<asp:Button id="btnCobra" runat="server" cssclass="boton" Width="120px" Text="Cobranzas"></asp:Button>
																		<TR>
																			<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="4"
																				height="100%">
																				<asp:panel id="panBotonesPie" runat="server" cssclass="titulo" Visible="False" Height="33"
																					Width="100%">
																					<TABLE style="WIDTH: 100%">
																						<TR>
																							<TD align="right" colSpan="2">
																								<asp:ImageButton id="btnAnteriorPie" runat="server" CausesValidation="False" ToolTip="Anterior" ImageUrl="imagenes/flea.bmp"></asp:ImageButton>&nbsp;
																								<asp:ImageButton id="btnEncabezadoPie" runat="server" CausesValidation="False" ToolTip="Cabecera"
																									ImageUrl="imagenes/fleup.jpg"></asp:ImageButton>&nbsp;
																							</TD>
																						</TR>
																					</TABLE>
																				</asp:panel></TD>
																		</TR>
																	</TABLE>
																</asp:panel></TD>
														</TR>
													</TABLE>
												</P>
											</asp:panel></DIV>
									</TD>
								</TR>
								<tr>
									<td colSpan="3"><A id="editar" name="editar"></A>
										<span onclick="if(document.all('hdntexto').value=='')return(true);else return(confirm(document.all('hdntexto').value));">
											<DIV style="DISPLAY: inline" id="divgraba" runat="server">
											<CC1:BOTONIMAGEN id="ImgbtnGenerar" runat="server" BorderStyle="None" CambiaValor="False" ImageUrl="imagenes/btngrab2.gif"
												ToolTip="Generar Comprobante" ForeColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
												OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnGrab2.gif" BackColor="Transparent" ImageOver="btnGrab.gif"
												ImageEnable="btnGrab2.gif" ImageDisable="btnGrab0.gif"></CC1:BOTONIMAGEN>
											</div>
											<DIV style="DISPLAY: none" id="divproce" runat="server">
												<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
												<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
											  </asp:panel></DIV>	
										</span>
										<DIV style="DISPLAY: inline" id="divlimpiar" runat="server">
										<CC1:BOTONIMAGEN id="ImgbtnLimpiar" runat="server" BorderStyle="None" CambiaValor="False" ImageUrl="imagenes/btnlimp2.gif"
											ToolTip="Limpiar" ForeColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" OutImage="del.gif"
											BtnImage="edit.gif" ImageBoton="btnlimp2.gif" BackColor="Transparent" ImageOver="btnlimp2.gif"></CC1:BOTONIMAGEN>
										</div>
									</td>
								</tr>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0">
					</td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0">
					</td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0">
					</td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0">
					</td>
				</tr>
			</TABLE>
			<!----------------- FIN RECUADRO ----------------->						
			<DIV style="DISPLAY: none">
				<asp:textbox id="hdnFechaValor" runat="server" AutoPostBack="True"></asp:textbox>				
				<asp:textbox id="hdnListaPrecios" runat="server"></asp:textbox>
				<asp:textbox id="hdnProformaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSobreImpo" runat="server"></asp:textbox>
				<asp:textbox id="hdnSobrePorc" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPopTurnoEqu" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnCria" runat="server"></asp:textbox>
				<asp:textbox id="hdnCotiProf" runat="server"></asp:textbox>
				<asp:textbox id="hdnLoginPop" runat="server" onchange="hdnLoginPop_change();"></asp:textbox>
				<asp:textbox id="hdnDatosTarjPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnDatosPopTurnoBov" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnComprobante" runat="server"></asp:textbox>
				<asp:textbox id="hdnCompGenera" runat="server"></asp:textbox>
				<asp:textbox id="hdnTarjClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnAutoriza" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnNoImprime" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdntexto" runat="server"></asp:textbox>
				<asp:textbox id="hdnAntClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnTasaSobreTasa" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
				<asp:textbox id="hdnTotalSobreTasa" runat="server"></asp:textbox>
				<asp:textbox id="hdnTasaSobreTasaC" runat="server"></asp:textbox>
				<asp:textbox id="hdnTotalSobreTasaC" runat="server"></asp:textbox>
				<asp:textbox id="hdnNoImprimir" runat="server"></asp:textbox>
				<asp:textbox id="hdnTarjVenc" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimio" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="lblMens" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimir" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnProforma" runat="server"></asp:textbox>
				<asp:textbox id="hdnProfCobrar" runat="server"></asp:textbox>
				<asp:textbox id="hdnProfCobro" runat="server"></asp:textbox>
				<asp:textbox id="hdnProfCerrarId" runat="server"></asp:textbox>
				<asp:textbox id="hdnProfCerro" runat="server"></asp:textbox>				
				<asp:textbox id="hdnDatosPopProforma" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnDatosPopProformaFechaIngre" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnProf30Dias" runat="server" AutoPostBack="True"></asp:textbox>								
				<asp:textbox id="hdnDatosCPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnDetaTempId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaTempTipo" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaAranId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaConcepId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCuotasId" runat="server"></asp:textbox>
				<asp:textbox id="hdnAutorizaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>				
				<cc1:numberbox id="txtPUnitAranSIVA" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"	EsDecimal="True"></cc1:numberbox>
				<asp:textbox id="hdnCotDolar" runat="server"></asp:textbox>
				<asp:textbox id="hdnFormaPago" runat="server"></asp:textbox>
				<asp:textbox id="hdnConv" runat="server"></asp:textbox>
				<asp:textbox id="hdnClieDiscIVA" runat="server"></asp:textbox>
				<asp:textbox id="hdnUbic" runat="server"></asp:textbox>
				<asp:textbox id="hdnClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnActiId" runat="server"></asp:textbox>
				<asp:textbox id="hdnFechaIva" runat="server"></asp:textbox>
				<asp:textbox id="hdlImpoIvaAran" runat="server"></asp:textbox>
				<asp:textbox id="hdnTasaIVAAran" runat="server"></asp:textbox>
				<asp:textbox id="hdnTasaIVAConcep" runat="server"></asp:textbox>
				<asp:textbox id="hdnImpoIvaConcep" runat="server"></asp:textbox>
				<asp:textbox id="hdnSocioId" runat="server"></asp:textbox>
				<asp:textbox id="hdnConfTarjVenc" runat="server"></asp:textbox>
				<asp:textbox id="hdnConf" runat="server"></asp:textbox>
				<asp:textbox id="hdnFechaVigenciaProf" runat="server"></asp:textbox>
				<cc1:numberbox id="txtTotIVA" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"	EsDecimal="True"></cc1:numberbox>
				<cc1:numberbox id="txtTotIVARedu" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false"	EsDecimal="True"></cc1:numberbox>
				<cc1:numberbox id="txtTotIVASTasa" runat="server" cssclass="textomonto" Width="100px" AceptaNull="false" EsDecimal="True"></cc1:numberbox>
				<asp:textbox id="hdnCta" runat="server"></asp:textbox>
				<asp:textbox id="hdnComp" runat="server"></asp:textbox>
				<asp:textbox id="hdnSelecProf" runat="server"></asp:textbox>
				<asp:textbox id="hdnIdGenerado" runat="server"></asp:textbox>
				<asp:textbox id="hdnRazaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnFocoSig" runat="server"></asp:textbox>
				<asp:textbox id="hdnActivAnterior" runat="server"></asp:textbox>
				<asp:textbox id="hdnFValorAnterior" runat="server"></asp:textbox>
				<asp:textbox id="hdnLPrecioAnterior" runat="server"></asp:textbox>
				<asp:textbox id="hdnFPagoAnterior" runat="server"></asp:textbox>
				<asp:textbox id="hdnCotiAnterior" runat="server"></asp:textbox>
				<asp:textbox id="hdnCobranzaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCobranzaTipo" runat="server"></asp:textbox>
				<asp:textbox id="hdnCobranzaFac" runat="server"></asp:textbox>
				<asp:textbox id="hdnActividades" runat="server"></asp:textbox>
				<asp:textbox id="hdnMuestraContadorLabiratorio" runat="server"></asp:textbox>
				<asp:textbox id="hdnIdEspecieLaboBonif" runat="server"></asp:textbox>
				<asp:textbox id="hdnGeneraNCAuto" runat="server"></asp:textbox>
				<asp:textbox id="hdnImporteNCAuto" runat="server"></asp:textbox>
				<asp:textbox id="hdnImporteDescuentoAplicadoNCAuto" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimirNC" runat="server"></asp:textbox>
				<asp:textbox id="hdnIdGeneradoNC" runat="server"></asp:textbox>
				<asp:textbox id="hdnCompGeneraNC" runat="server"></asp:textbox>
				
			
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT>
				</OBJECT>
			</DIV>
		</form>
		<script language="javascript">
		function mExportar()
		{
			document.all("divgraba").style.display ="none";
			document.all("divlimpiar").style.display ="none";
			document.all("divproce").style.display ="inline";
		}
		
		 function btnBuscar_click(pstrTabla,pTitu)
		{
		  var filtro;
		  var filClieGene;
		  var filRazaId;
		  var campo;  
		  filClieGene =0;
		  if(mClienteGenerico()=="True")
                 {
                   filClieGene="1";
                 }  
		  if (pstrTabla=='precios_aran')
		     {		      
              filRazaId = mRaza();
              campo="aran_desc";
              combo="cmbAran"; 
              filtro='@peli_acti_id='+ document.all("hdnActiId").value + ',@aran_clie_gene=' + filClieGene + ',@aran_raza_id=' + filRazaId
		     }
		    else
		     {
		       campo="conc_desc"
		       combo="cmbConcep"; 
		       filtro='@conc_acti_id='+ document.all("hdnActiId").value + ',@conc_clie_gene=' + filClieGene 
		     } 
		     BusqCombo(pstrTabla,filtro,campo,combo,pTitu)
		    // gAbrirVentanas("consulta_combo_Pop.aspx?EsConsul=0&tabla=" + pstrTabla + "&filtros=" + filtro + "&campoDesc=" + campo, 1, "600","300");
	    }
		 
			
	     function NoImprime()
	      {
	       if(document.all("hdnNoImprimir").value!="")
			{
			 alert(document.all("hdnNoImprimir").value );
			 document.all("hdnNoImprime").value="1";
			 __doPostBack('hdnNoImprime','');
            }
          }  

	     function CerrarProforma()
	      {
	       if(document.all("hdnProforma").value!="")
			{
			if (window.confirm(document.all("hdnProforma").value))
			 {
				document.all("hdnProfCerro").value="1";
				__doPostBack('hdnProfCerro','');	
			 }
            }
           }  
	     function CerrarProformaCobranza()
	      {
	       if(document.all("hdnProfCobrar").value!="")
			{
			if (window.confirm(document.all("hdnProfCobrar").value))
			 {
				document.all("hdnProfCobro").value="1";
				__doPostBack('hdnProfCobro','');	
			 }
			 else 
             {
				document.all("hdnProfCobro").value="0";
				__doPostBack('hdnProfCobro','');	

			 }
            }            
           } 
		 function Imprimir()
		 {
			if(document.all("hdnImprimir").value!="")
			{
			    var sRet = '';
			    
			    if (document.all("hdnComprobante").value !='Proforma')
			      sRet=EjecutarMetodoXML("Utiles.LetraComprob", document.all("hdnClieId").value);

				if (window.confirm("�Desea imprimir la " + document.all("hdnComprobante").value + " " + sRet + " "+ document.all("hdnCompGenera").value + " ?"))
				{
				 try{
						var pRepo = "Factura";
						
						var pProforma = "";
						if (document.all("hdnComprobante").value == "Proforma")	
						{
							pRepo = "Proforma";
							pProforma = ",@proforma='S'";
						}
						else
						{
							document.all("hdnComprobante").value = "Factura";
						}

				        sRet = mImprimirCopias(2,'O',document.all("hdnImprimir").value,pRepo, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original
			          
					 	if (window.confirm("�Se imprimi� la " + document.all("hdnComprobante").value + " correctamente?"))
						{
							var lstrCopias = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimir").value+pProforma, "copias");

						    if (lstrCopias != "0" && lstrCopias != "1" && pRepo != "Proforma")
								sRet = mImprimirCopias(lstrCopias,'D',document.all("hdnImprimir").value,pRepo,"<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias
								
							document.all("hdnImprId").value = '';
							document.all("hdnImprimio").value="1";
							__doPostBack('hdnImprimio','');	
							document.all("hdnImprimir").value="";			
						}
						else
						{
							document.all("hdnImprId").value = '';
							Imprimir();
						}
					}
					catch(e)
					{
						alert("Error al intentar efectuar la impresi�n");
					}
				}
				else
				{ 	document.all("hdnImprimio").value="0";
					document.all("hdnImprimir").value=""
					__doPostBack('hdnImprimio','');		
				}
			}
		}
		
		/* Dario 2013-10-25 funcion que imprime la nota de credito si corresponde*/
		function ImprimirNC()
		{
			if(document.all("hdnImprimirNC").value !="" && document.all("hdnGeneraNCAuto").value=="Si")
			{
			    var sRet = '';
   		        sRet=EjecutarMetodoXML("Utiles.LetraComprob", document.all("hdnClieId").value);

				if (window.confirm("�Desea imprimir la Nota de Credito " + sRet + " "+ document.all("hdnCompGeneraNC").value + " ?"))
				{
				 try{
						var pRepo = "Factura"; 
						
						document.all("hdnComprobante").value = "Factura";
						
				        sRet = mImprimirCopias(2,'O',document.all("hdnIdGeneradoNC").value,pRepo, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original
			          
					 	if (window.confirm("�Se imprimi� la Nota de Credito correctamente?"))
						{
							var lstrCopias = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimirNC").value, "copias");

						    if (lstrCopias != "0" && lstrCopias != "1")
								sRet = mImprimirCopias(lstrCopias,'D',document.all("hdnIdGeneradoNC").value,pRepo,"<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias
								
							document.all("hdnImprId").value = '';
							document.all("hdnIdGeneradoNC").value="";			
						}
						else
						{
							document.all("hdnIdGeneradoNC").value = '';
							ImprimirNC();
						}
					}
					catch(e)
					{
						alert("Error al intentar efectuar la impresi�n");
					}
				}
				else
				{ 	document.all("hdnImprimio").value="0";
					document.all("hdnImprimir").value=""
					__doPostBack('hdnImprimio','');		
				}
			}
		}

		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		try{	
		     if (document.all["cmbAran"]!= null && document.all["cmbAran"].value=="")
			   document.all["txtcmbAran"].focus();		
		    } catch(e){}
		try{     
		     if (document.all["cmbConcep"]!= null && document.all["cmbConcep"].value=="")
			   document.all["txtcmbConcep"].focus();
	       } catch(e){}
	       
	    try{     
		     mSetearEventoGenerarComprobante()
		   } catch(e){}
	  
	  try{   
	        
		    mCargarCriadores()
		   } catch(e){}      


    function GrabarUnaVez(pCtrl)
		{  
		if(pCtrl.Grabo==null)
	        pCtrl.Grabo="1";
	    else
			return false;
		   } 
		   
		   try{
			if (document.all("hdnFocoSig").value!="")  
		    { 
		     //onactivate="this.ImageUrl='imagenes/fle_foco.jpg'"
		     document.all("hdnFocoSig").value = "";
		     document.all["btnSigCabe"].setActive();
		    
		    }
		   }
		 catch(e)
		   {
		   }

		mSetearCuadros();
  
		function mSetearCuadros(pCtrl)
		{  
			if(document.all("usrClie:txtId")!=null)
			{
				if(document.all("usrClie:txtId").value!='')
				{
					if(document.all("tblEstadoSaldos")!=null)
					{	// Dario 2013-07-02 comentado se una el de cobranzas
						//vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClie:txtId").value, "saldo_cta_cte_venc,saldo_social_venc,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras").split("|");
						vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClie:txtId").value, "saldo_cta_cte,saldo_social,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras").split("|");
						
						/*Dario 2013-07-02*/							
						document.all("txtSaldoCaCte").value  = vstrRet[0];
						document.all("txtSaldoCtaSoci").value  = vstrRet[1];
						document.all("txtImpCuentaCtaCte").value  = vstrRet[2];
						document.all("txtImpCuentaCtaSocial").value  = vstrRet[3];
						document.all("txtTotalProfRRGG").value  = vstrRet[4];
						document.all("txtTotalProfLabo").value  = vstrRet[5];
						document.all("txtTotalProfExpo").value  = vstrRet[6];
						document.all("txtTotalProfOtras").value  = vstrRet[7];	
						/* fin */
						
						if(vstrRet[0]!=0||vstrRet[1]!=0||vstrRet[2]!=0||vstrRet[3]!=0)
							document.all("divEstadoSaldos").style.display="inline";
						else
							document.all("divEstadoSaldos").style.display="none";
						if(vstrRet[4]!=0||vstrRet[5]!=0||vstrRet[6]!=0||vstrRet[7]!=0)
							document.all("divProformas").style.display="inline";
						else
							document.all("divProformas").style.display="none";

						/*Dario 2013-07-02*/							
						var vstrRet2 = LeerCamposXML("acuses_total", document.all("usrClie:txtId").value, "total_acuses").split("|");
						document.all("txtImpAcuses").value  = vstrRet2[0];							
						/* fin */
						
						/*	
						vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClie:txtId").value, "saldo_cta_cte_venc,saldo_social_venc,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras").split("|");					
						vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClie:txtId").value, "saldo_cta_cte     ,saldo_social     ,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras").split("|");
						
						document.all("txtSaldoCaCte").value  = vstrRet[0];
						document.all("txtSaldoCtaSoci").value  = vstrRet[1];
						document.all("txtImpCuentaCtaCte").value  = vstrRet[2];
						document.all("txtImpCuentaCtaSocial").value  = vstrRet[3];
						document.all("txtTotalProfRRGG").value  = vstrRet[4];
						document.all("txtTotalProfLabo").value  = vstrRet[5];
						document.all("txtTotalProfExpo").value  = vstrRet[6];
						document.all("txtTotalProfOtras").value  = vstrRet[7];				
						
						if(vstrRet[0]!=0||vstrRet[1]!=0||vstrRet[2]!=0||vstrRet[3]!=0)
							document.all("divEstadoSaldos").style.display="inline";
						else
							document.all("divEstadoSaldos").style.display="none";
						if(vstrRet[4]!=0||vstrRet[5]!=0||vstrRet[6]!=0||vstrRet[7]!=0)
							document.all("divProformas").style.display="inline";
						else
							document.all("divProformas").style.display="none";
							
						var vstrRet2 = LeerCamposXML("acuses_total", document.all("usrClie:txtId").value, "total_acuses").split("|");
						document.all("txtImpAcuses").value  = vstrRet2[0];
						*/
							
					}
				}
				else
				{
					document.all("divEstadoSaldos").style.display="none";
					document.all("divProformas").style.display="none";
				}
			}
		}
		mVerificarChecks();
		if (document.all["cmbCCosConcep"]!= null)
		{
			document.all('cmbCCosConcep').disabled = (document.all('cmbCCosConcep').options.length <= 1);
			document.all('txtcmbCCosConcep').disabled = (document.all('cmbCCosConcep').options.length <= 1);
		}
		if (document.all("usrClie:txtId")!=null && document.all("DivimgTarj")!= null)
		{
			mTarjetas();
			mEspeciales();
			mEstadoSaldos();
		}
		if (document.all('cmbListaPrecios')!=null)
		{
			if (document.all('cmbListaPrecios').length > 1)
				document.all('cmbListaPrecios').style.color = 'red';
			else
				document.all('cmbListaPrecios').style.color = 'black';
		}		
		</script>
		</FORM>
	</BODY>
</HTML>
