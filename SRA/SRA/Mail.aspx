<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Mail" CodeFile="Mail.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function mCargarTextoMail()
		{
	      document.all('txtAsun').value = opener.document.all('hdnMailAsun').value;
	      document.all('txtPara').value = opener.document.all('hdnMailDire').value;
	      document.all('hdnClieId').value = opener.document.all('usrClieFil_txtCodi').value;
		}	
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');mCargarTextoMail();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right">
									<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD>
												<asp:label id="lblTitu" runat="server" cssclass="titulo" Font-Size="X-Small"></asp:label></TD>
											<TD align="right" width="50"><asp:imagebutton id="btnCerrar" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes/close3.bmp"></asp:imagebutton></TD>
										</TR>
									</TABLE>
									&nbsp;
								</TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center" colSpan="2"><asp:panel id="panEspeciales" runat="server" Visible="true" cssclass="titulo" Width="100%"
										BorderWidth="1px" BorderStyle="Solid" height="100%">
										<TABLE class="FdoFld" id="tblMail" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 5.79%" align="right" height="4"></TD>
												<TD style="WIDTH: 30%" height="4"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 5.79%" align="right">
													<asp:label id="lblPara" runat="server" cssclass="titulo">Para:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 30%">
													<CC1:TEXTBOXTAB id="txtPara" runat="server" cssclass="cuadrotexto" Width="98%"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 5.79%" align="right">
													<asp:label id="lblAsun" runat="server" cssclass="titulo">Asunto:</asp:label>&nbsp;
												</TD>
												<TD style="WIDTH: 30%">
													<CC1:TEXTBOXTAB id="txtAsun" runat="server" cssclass="cuadrotexto" Width="98%"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 70%" align="left" colSpan="2">
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="6"></TD>
															<TD>
																<asp:Label id="lblMsg" runat="server" cssclass="titulo">Mensaje:</asp:Label></TD>
														</TR>
														<TR>
															<TD></TD>
															<TD>
																<asp:datagrid id="grdDatoBusq" runat="server" BorderStyle="None" BorderWidth="1px" ShowFooter="True"
																	AllowPaging="False" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																	AutoGenerateColumns="False" PageSize="2000" width="100%">
																	<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																	<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
																	<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
																	<FooterStyle cssclass="footer"></FooterStyle>
																	<Columns>
																		<asp:BoundColumn DataField="comp_id" Visible="False"></asp:BoundColumn>
																		<asp:BoundColumn DataField="fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																		<asp:BoundColumn DataField="numero" HeaderText="Comprobante"></asp:BoundColumn>
																		<asp:BoundColumn DataField="impo_ori" HeaderText="Importe Original" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																			ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="debe" HeaderText="Debe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																			ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="haber" HeaderText="Haber" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																			ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="aplicado" HeaderText="Sin Aplicar" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																			ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		<asp:BoundColumn DataField="fecha_vto" HeaderText="Fecha Vto." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																	</Columns>
																	<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
																</asp:datagrid></TD>
														</TR>
														<TR>
															<TD></TD>
															<TD>
																<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 220px" align="right"></TD>
																		<TD style="WIDTH: 116px"></TD>
																		<TD style="WIDTH: 169px" align="right">&nbsp;</TD>
																		<TD></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px" align="right">
																			<asp:label id="lblCtaCteVenc" runat="server" cssclass="titulo">Saldo de Cta.Cte.:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 116px">
																			<asp:label id="txtCtaCte" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 169px" align="right">
																			<asp:label id="lblProfRRGG" runat="server" cssclass="titulo">Total Proformas RRGG:</asp:label>&nbsp;</TD>
																		<TD>
																			<asp:label id="txtProfRRGG" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px" align="right">
																			<asp:label id="lblCtaSocVenc" runat="server" cssclass="titulo">Saldo de Cta. Social:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 116px">
																			<asp:label id="txtCtaSoc" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 169px" align="right">
																			<asp:label id="lblProfLabo" runat="server" cssclass="titulo">Total Prof.Laboratorio:</asp:label>&nbsp;</TD>
																		<TD>
																			<asp:label id="txtProfLabo" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px" noWrap align="right">
																			<asp:label id="lblImpoCtaCte" runat="server" cssclass="titulo">Importe a cuenta en Cta. Cte.: </asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 116px">
																			<asp:label id="txtImpoCtaCte" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 169px" align="right">
																			<asp:label id="lblProfExpo" runat="server" cssclass="titulo">Total Prof.Exposiciones:</asp:label>&nbsp;</TD>
																		<TD>
																			<asp:label id="txtProfExpo" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px" align="right">
																			<asp:label id="lblImpoCtaSoc" runat="server" cssclass="titulo">Importe a cuenta en Cta. Social:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 116px">
																			<asp:label id="txtImpoCtaSoc" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 169px" align="right">
																			<asp:label id="lblProfOtra" runat="server" cssclass="titulo">Total Otras Proformas:</asp:label>&nbsp;</TD>
																		<TD>
																			<asp:label id="txtProfOtra" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px" align="right">
																			<asp:label id="lblInteresSoc" runat="server" cssclass="titulo">Interes en Cta. Social:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 116px">
																			<asp:label id="txtInteresSoc" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 220px" align="right">
																			<asp:label id="Label1" runat="server" cssclass="titulo">Acuses de Recibo pendientes:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 116px">
																			<asp:label id="txtTotalAcuses" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px" align="right">
																			<asp:label id="lblInteresCte" runat="server" cssclass="titulo">Interes en Cta. Cte.:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 116px">
																			<asp:label id="txtInteresCte" runat="server" cssclass="titulo"></asp:label>&nbsp;</TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD align="right" background="imagenes/formfdofields.jpg" colSpan="2" height="30">
													<asp:Button id="btnEnvi" runat="server" cssclass="boton" Width="80px" Text="Enviar"></asp:Button>&nbsp;&nbsp;
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
							<ASP:TEXTBOX id="hdnClieId" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
