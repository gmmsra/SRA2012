<%--<%@ Page Language="vb" AutoEventWireup="false" Codebehind="empresas.aspx.vb" Inherits="SRA.empresas" %>--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/empresas.aspx.vb" Inherits="empresas" %>

<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EMPRESAS</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Empresas</asp:label>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center">
									<asp:datagrid id="grdDato" runat="server" width="95%" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="empr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="empr_desc" ReadOnly="True" HeaderText="Descripción" HeaderStyle-Width="60%"></asp:BoundColumn>
											<asp:BoundColumn DataField="empr_factu" ReadOnly="True" HeaderText="Facturación" HeaderStyle-Width="10%"></asp:BoundColumn>
											<asp:BoundColumn DataField="empr_pilo" ReadOnly="True" HeaderText="SRA" HeaderStyle-Width="8%"></asp:BoundColumn>
											<asp:BoundColumn DataField="empr_despa" ReadOnly="True" HeaderText="Despachos" HeaderStyle-Width="10%"></asp:BoundColumn>
											<asp:BoundColumn DataField="empr_presu" ReadOnly="True" HeaderText="Presupuestos" HeaderStyle-Width="10%"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR HEIGHT="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<asp:button cssclass="boton" id="btnAgre" runat="server" Width="170px" CausesValidation="False"
										Text="Nueva Empresa >>"></asp:button>
								</TD>
								<TD align="right">
									<table id="tabLinks" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="1"><img src="imagenes/tab_a.bmp" border="0" WIDTH="9" HEIGHT="28"></td>
											<td background="imagenes/tab_b.bmp"><img src="imagenes/tab_b.bmp" border="0" WIDTH="8" HEIGHT="28"></td>
											<td width="1"><img src="imagenes/tab_c.bmp" border="0" WIDTH="31" HEIGHT="28"></td>
											<td background="imagenes/tab_fondo.bmp" width="1">
												<asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Height="21px" CausesValidation="False" Width="95px"> Empresas</asp:linkbutton></td>
											<td width="1"><img src="imagenes/tab_f.bmp" border="0" WIDTH="27" HEIGHT="28"></td>
											<td background="imagenes/tab_fondo.bmp" width="1">
												<asp:linkbutton id="lnkDeta" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Height="21px" CausesValidation="False"> Acceso</asp:linkbutton></td>
											<td width="1"><img src="imagenes/tab_fondo_fin.bmp" border="0" WIDTH="31" HEIGHT="28"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
											BorderWidth="1px" Visible="False">
											<P align="right">
												<TABLE class="fdofld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" width="219px" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="images\Close.bmp"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="HEIGHT: 16px" align="right" width="20%">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="229px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblTele" runat="server" cssclass="titulo">Tel:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtTele" runat="server" cssclass="cuadrotexto" Width="189px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblCUIT" runat="server" cssclass="titulo">CUIT:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:CUITBOX id="txtCUIT" runat="server" cssclass="cuadrotexto" Width="199px"></CC1:CUITBOX></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblSucu" runat="server" cssclass="titulo">Sucursal:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:NUMBERBOX id="txtSucu" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="True"
																				EsDecimal="False"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblDivi" runat="server" cssclass="titulo">División:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:NUMBERBOX id="txtDivi" runat="server" cssclass="cuadrotexto" Width="140px" Obligatorio="True"
																				EsDecimal="False"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="15%">
																			<asp:Label id="lblFile" runat="server" cssclass="titulo">Ubicación:</asp:Label>&nbsp;
																		</TD>
																		<TD><INPUT class="cuadrotexto" id="txtFile" type="file" size="60" name="txtFile" runat="server"
																				Width="100%">&nbsp;&nbsp;
																			<asp:Button id="btnSubir" runat="server" cssclass="boton" Text="Actualizar" Width="70px"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblLogo" runat="server" cssclass="titulo">Archivo:</asp:Label>&nbsp;</TD>
																		<TD align="left">
																			<cc1:textboxtab class="cuadrotextodeshab" id="txtLogo" runat="server" Width="100%" enabled="false"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right">
																			<asp:Label id="lblImagen" runat="server" cssclass="titulo">Imagen:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<asp:Image id="imgImagen" Height="100px" Visible="False" Runat="server"></asp:Image></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDetalle" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="lblFactu" runat="server" cssclass="titulo">Factura:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:combobox class="combo" id="cmbFactu" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblPilo" runat="server" cssclass="titulo">SRA:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:combobox class="combo" id="cmbPilo" runat="server" Width="210px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDespa" runat="server" cssclass="titulo">Despachos:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:combobox class="combo" id="cmbDespa" runat="server" Width="210px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblPresu" runat="server" cssclass="titulo">Presupuestos:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:combobox class="combo" id="cmbPresu" runat="server" Width="210px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblImgPrin" runat="server" cssclass="titulo">Imagen Principal:</asp:Label>&nbsp;</TD>
																		<TD colSpan="2">
																			<cc1:combobox class="combo" id="cmbImgPrin" runat="server" Width="210px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="3" height="16"></TD>
																	</TR>
																</TABLE>
															</asp:panel><BR>
														</TD>
													</TR>
													<TR>
														<TD width="20%"><A id="editar" name="editar"></A></TD>
														<TD vAlign="bottom" colSpan="2" height="16">
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Text="Alta" Width="80px"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Text="Baja" CausesValidation="False"
																Width="80px"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Text="Modificar" CausesValidation="False"
																Width="80px"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnReset" runat="server" cssclass="boton" Text="Renumerar" CausesValidation="False"
																Width="80px"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Text="Limpiar" CausesValidation="False"
																Width="80px"></asp:Button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
