<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ComprobantesAnul" CodeFile="ComprobantesAnul.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitu%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mConfirmarBaja() 
		{
			if(!confirm('Confirma la baja del registro?')) 
			{
				return false;
			}
			else
			{
				var strRet = LeerCamposXML("comprobante_proforma", document.all('hdnId').value, "prfr_id");
				if (strRet != '' && strRet != '0')
				{
					document.all('hdnPrfrNume').value = strRet;
					if (confirm('El comprobante posee proforma asociada. Desea tambi�n darla de baja?')) 
						document.all('hdnPrfrBaja').value = 'S';
					else
						document.all('hdnPrfrBaja').value = 'N';  // reactivar la proforma
				}
				else				
				{
					return true;
				}
			}
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Anulaci�n de Comprobantes</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="100%"
										BorderWidth="0" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" width="7" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" background="imagenes/formfdofields.jpg">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" Ancho="800" MuestraDesc="False" FilSociNume="True"
																				AceptaNull="False" Tabla="Clientes" Autopostback="false"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblActiFil" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbActiFil" class="combo" runat="server" Width="250px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha Valor:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblNroCompFil" runat="server" cssclass="titulo">Comprobante:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtNroCompFil" runat="server" cssclass="cuadrotexto" Width="180px" AceptaNull="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblCoti" runat="server" cssclass="titulo">Tipo:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbCoti" class="combo" runat="server" Width="250px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD height="10" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="false" DataField="comp_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_fecha" HeaderText="Fecha Valor" DataFormatString="{0:dd/MM/yyyy}">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="coti_desc" HeaderText="Tipo Comp."></asp:BoundColumn>
											<asp:BoundColumn DataField="numero" HeaderText="Nro"></asp:BoundColumn>
											<asp:BoundColumn DataField="acti_desc" HeaderText="Actividad"></asp:BoundColumn>
											<asp:BoundColumn DataField="cliente" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_neto" HeaderText="Importe" DataFormatString="{0:F2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False" Height="116px">
											<P align="right">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:label id="lblTitu" runat="server" cssclass="titulo" width="412px"></asp:label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD colSpan="2"></TD>
																		<TD height="5" vAlign="top" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblFechaComp" runat="server" cssclass="titulo">Fecha Ingreso:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
																				<TR>
																					<TD>
																						<cc1:DateBox id="txtFechaComp" runat="server" cssclass="cuadrotextodeshab" Width="80px" Enabled="False"
																							Obligatorio="true"></cc1:DateBox></TD>
																					<TD align="right"><BUTTON style="WIDTH: 110px" id="btnVista" class="boton" onclick="btnVista_click();" runat="server"
																							value="Vista Previa">Vista Previa</BUTTON></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<cc1:combobox id="cmbActi" class="combo" runat="server" Width="250px" enabled="false"></cc1:combobox></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		<TD style="WIDTH: 5%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" vAlign="top" background="imagenes/formfdofields.jpg"
																			align="right">
																			<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<UC1:CLIE id="usrClie" runat="server" Ancho="800" FilSociNume="True" Tabla="Clientes" Autopostback="true"
																				Saltos="1,1,1,1" Activo="false" FilFanta="true" FilLegaNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD colSpan="2" align="left">
																			<CC1:TEXTBOXTAB id="txtNyap" runat="server" cssclass="cuadrotextodeshab" Width="100%" Enabled="False"
																				Obligatorio="True" onchange="mSetearClie();"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD colSpan="3" align="center"></TD>
																		<TD style="WIDTH: 10%" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblNroComp" runat="server" cssclass="titulo">Comprobante:</asp:Label>&nbsp;</TD>
																		<TD colSpan="3">
																			<CC1:TEXTBOXTAB id="txtNroComp" runat="server" cssclass="cuadrotexto" Width="160px" Enabled="False"
																				Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		<TD style="WIDTH: 5%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblTPers" runat="server" cssclass="titulo">Tr�mite personal:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																				<TR>
																					<TD width="75">
																						<asp:CheckBox id="chkTPers" Enabled="False" Checked="True" CssClass="titulo" Runat="server"></asp:CheckBox></TD>
																					<TD width="90" align="right">
																						<asp:label id="lblFechaValor" runat="server" cssclass="titulo">Fecha Valor:</asp:label>&nbsp;
																					</TD>
																					<TD width="95">
																						<cc1:DateBox id="txtFechaValor" runat="server" cssclass="cuadrotextodeshab" Width="66px" Enabled="False"
																							Obligatorio="true"></cc1:DateBox></TD>
																					<TD width="70" align="right">
																						<asp:label id="lblCotDolar" runat="server" cssclass="titulo">Cot.Dolar:</asp:label>&nbsp;
																					</TD>
																					<TD>
																						<cc1:numberbox id="txtCotDolar" runat="server" cssclass="textomontodeshab" Width="60px" CambiaValor="True"
																							Enabled="False" CantMax="4" EsDecimal="True"></cc1:numberbox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																		<TD style="WIDTH: 5%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		<TD style="WIDTH: 5%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblNeto" runat="server" cssclass="titulo">Importe Neto:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																				<TR>
																					<TD width="75">
																						<cc1:numberbox id="txtBruto" runat="server" cssclass="textomontodeshab" Width="60px" CambiaValor="True"
																							AceptaNull="False" Enabled="False" CantMax="4" EsDecimal="True"></cc1:numberbox></TD>
																					<TD width="90" noWrap align="right">
																						<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																							<TR>
																								<TD>
																									<asp:label id="lblIva" runat="server" cssclass="titulo">Iva:</asp:label></TD>
																								<TD>
																									<cc1:numberbox id="txtIva" runat="server" cssclass="textomontodeshab" Width="60px" CambiaValor="True"
																										AceptaNull="False" Enabled="False" CantMax="4" EsDecimal="True"></cc1:numberbox></TD>
																							</TR>
																						</TABLE>
																					</TD>
																					<TD width="95" noWrap>
																						<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
																							<TR>
																								<TD>
																									<asp:label id="lblST" runat="server" cssclass="titulo">Sobretasa:</asp:label></TD>
																								<TD>
																									<cc1:numberbox id="txtST" runat="server" cssclass="textomontodeshab" Width="60px" CambiaValor="True"
																										AceptaNull="False" Enabled="False" CantMax="4" EsDecimal="True"></cc1:numberbox></TD>
																							</TR>
																						</TABLE>
																					</TD>
																					<TD width="70" align="right">
																						<asp:label id="lblBruto" runat="server" cssclass="titulo">Bruto:</asp:label>&nbsp;
																					</TD>
																					<TD>
																						<cc1:numberbox id="txtNeto" runat="server" cssclass="textomontodeshab" Width="60px" CambiaValor="True"
																							AceptaNull="False" Enabled="False" CantMax="4" EsDecimal="True"></cc1:numberbox>&nbsp;
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																		<TD style="WIDTH: 5%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label15" runat="server" cssclass="titulo">Leyenda:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtObser" runat="server" cssclass="textolibredeshab" Width="100%" Height="54px"
																				Enabled="False" TextMode="MultiLine" EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR> <!--- detalle - Aplic-->
																		<TD vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Aranceles:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<asp:datagrid id="grdAran" runat="server" width="100%" Visible="true" BorderWidth="1px" BorderStyle="None"
																				OnPageIndexChanged="grdAran_Page" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Left" AllowPaging="True" PageSize="5">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="_descrip" HeaderText="Descripci&#243;n"></asp:BoundColumn>
																					<asp:BoundColumn DataField="coan_impo" HeaderText="Importe" DataFormatString="{0:F2}">
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="5" vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Conceptos:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<asp:datagrid id="grdConcep" runat="server" width="100%" Visible="true" BorderWidth="1px" BorderStyle="None"
																				OnPageIndexChanged="grdConcep_Page" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Left" AllowPaging="True" PageSize="5">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="_descrip" HeaderText="Descripci&#243;n"></asp:BoundColumn>
																					<asp:BoundColumn DataField="coco_impo_ivai" HeaderText="Importe" DataFormatString="{0:F2}">
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR> <!--- detalle - cuotas vencimientos-->
																		<TD vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label3" runat="server" cssclass="titulo">Coutas Vencimientos:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<asp:datagrid id="grdVtos" runat="server" width="100%" Visible="true" BorderWidth="1px" BorderStyle="None"
																				OnPageIndexChanged="grdVtos_Page" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Left" AllowPaging="True" PageSize="5">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="covt_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="covt_impo" HeaderText="Importe" DataFormatString="{0:F2}">
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR> <!--- detalle - vencimientos-->
																		<TD vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label5" runat="server" cssclass="titulo">Vencimientos Retrasados:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<asp:datagrid id="grdVtosRetrasados" runat="server" width="100%" Visible="true" BorderWidth="1px"
																				BorderStyle="None" OnPageIndexChanged="grdVtosRetrasados_Page" AutoGenerateColumns="False"
																				CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Left" AllowPaging="True"
																				PageSize="5">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="coso_fecha_VtoAplica" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="coso_impo" HeaderText="Importe" DataFormatString="{0:F2}">
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="16" vAlign="bottom" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR> <!--- detalle - autorizaciones-->
																		<TD vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="Label4" runat="server" cssclass="titulo">Autorizaciones:</asp:Label>&nbsp;
																		</TD>
																		<TD>
																			<asp:datagrid id="grdAutor" runat="server" width="100%" Visible="true" BorderWidth="1px" BorderStyle="None"
																				OnPageIndexChanged="grdAutor_Page" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Left" AllowPaging="True" PageSize="5">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="_auti_desc" HeaderText="Descripci&#243;n"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Visible="False" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR>
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Anular"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnPrfrBaja" runat="server"></asp:textbox>
				<asp:textbox id="hdnPrfrNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnPagina" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
			
		function btnVista_click()
		{
			window.showModalDialog(document.all("hdnPagina").value, "", "dialogHeight:560px;dialogWidth:750px;status=no,resizable=no");
	 	}
		</SCRIPT>
	</BODY>
</HTML>
