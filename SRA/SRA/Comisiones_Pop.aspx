<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Comisiones_Pop" CodeFile="Comisiones_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Comisiones por Criador</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="javascript">
		function mAbrirActa(pActaId)
		{
			gAbrirVentanas("ComisionesActas.aspx?cdoc_id=" + pActaId, 1, 750, 500, 100, 100)
		}
		function ValidaControl(pHab,pId)
		{
			if (pHab == "1") 
				document.all["divImagen"+pId].style.display="inline";
			else 
				document.all["divImagen"+pId].style.display="none";
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onload="gSetearTituloFrame('')">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR>
					<TD style="HEIGHT: 15px" vAlign="bottom" colSpan="6" height="15"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Comisiones por Criador</asp:label></TD>
				</TR>
				<TR height="100%">
					<TD vAlign="top" colspan="6"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px"
							PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
							BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="cria_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="cdoc_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
								<asp:BoundColumn DataField="cdoc_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
									ItemStyle-HorizontalAlign="Center">
									<HeaderStyle Width="10%"></HeaderStyle></asp:BoundColumn>
								<asp:BoundColumn DataField="cdoc_acta" HeaderText="Acta"><HeaderStyle Width="8%"></HeaderStyle></asp:BoundColumn>
								<asp:BoundColumn DataField="crac_tema" HeaderText="Tema"></asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<div style="DISPLAY: none" id="divImagen<%#DataBinder.Eval(Container, "DataItem.cria_id")%>">
											<A href="javascript:mAbrirActa(<%#DataBinder.Eval(Container, "DataItem.cdoc_id")%>);">
												<img onload="javascript:ValidaControl(1,<%#DataBinder.Eval(Container, "DataItem.cria_id")%>);" src='imagenes/Buscar16.gif' style="cursor:hand; VISIBILITY: " border="0" alt="Ver Detalle Acta" />
											</A>
										</div>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
