Namespace SRA

Partial Class Proforma_detalle_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblExenta As System.Web.UI.WebControls.Label
   Protected WithEvents lblPercep As System.Web.UI.WebControls.Label
   Protected WithEvents lblPrecSoci As System.Web.UI.WebControls.Label
   Protected WithEvents lblMiemCD As System.Web.UI.WebControls.Label
   Protected WithEvents lblCuitT As System.Web.UI.WebControls.Label
   Protected WithEvents lblNroSocio As System.Web.UI.WebControls.Label
   Protected WithEvents lblCondIVAT As System.Web.UI.WebControls.Label
   Protected WithEvents lblestado As System.Web.UI.WebControls.Label
   Protected WithEvents lblCatego As System.Web.UI.WebControls.Label
   Protected WithEvents lblISEAT As System.Web.UI.WebControls.Label
   Protected WithEvents lblCEIDAT As System.Web.UI.WebControls.Label
   Protected WithEvents btnGenerarCuot As System.Web.UI.WebControls.ImageButton
   Protected WithEvents panDato As System.Web.UI.WebControls.Panel
   Protected WithEvents txtNroSocio As NixorControls.NumberBox
   Protected WithEvents txtestado As NixorControls.TextBoxTab
   Protected WithEvents txtCatego As NixorControls.TextBoxTab
   Protected WithEvents txtcuit As NixorControls.TextBoxTab
   Protected WithEvents txtISEA As NixorControls.NumberBox
   Protected WithEvents txtCEIDA As NixorControls.NumberBox
   Protected WithEvents txtCondIVA As NixorControls.TextBoxTab

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrId As String
   Private mstrConn As String
   Dim mdsDatos As DataSet
   Dim mstrCmd As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultar()
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()

      mstrId = Request.QueryString("id")
      lbltitulo.Text = "Detalle de la proforma " + Request.QueryString("nro")
   End Sub
#End Region

   Public Sub mConsultar()
      Try
         Dim ds As DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.proforma_detalle_anul_consul " + clsSQLServer.gFormatArg(mstrId, SqlDbType.Int))
         grdConsulta.DataSource = ds
         grdConsulta.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
     mListar()
   End Sub
   Private Sub mListar()
      Dim lstrRptName As String = "Proformas"

      Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

      lstrRpt += "&prfr_id=" + mstrId
      lstrRpt += "&prfr_acti_id%3aisnull=True"
      lstrRpt += "&clie_id%3aisnull=True"
      lstrRpt += "&fecha%3aisnull=True"

      lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
      Response.Redirect(lstrRpt)
   End Sub

End Class

End Namespace
