Namespace SRA

Public Class header
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

   Protected WithEvents lblUser As System.Web.UI.WebControls.Label
   Protected WithEvents lblBase As System.Web.UI.WebControls.Label
   Protected WithEvents lblDate As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      If Not IsPostBack Then
         lblUser.Text = Session("sUser").ToString()
         lblBase.Text = Session("sBase").ToString()
         lblBase.Text = lblBase.Text.ToUpper()
         lblDate.Text = clsFormatear.gFormatFecha(DateTime.Now, "es-AR")
      End If
   End Sub

End Class

End Namespace
