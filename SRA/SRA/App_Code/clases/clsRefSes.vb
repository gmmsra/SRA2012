Namespace SRA

Public Class clsRefSes
   Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState

   Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
      Get
         Return True
      End Get
   End Property

   Public Sub ProcessRequest(ByVal pContext As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
      pContext.Response.AddHeader("Refresh", "300")
   End Sub
End Class

End Namespace

