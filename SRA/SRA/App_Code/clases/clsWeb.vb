Imports System.data.SqlClient
Imports System.Web.SessionState
Imports System.IO
Imports System.XML
Imports System.Web.UI.WebControls
Imports System.Collections
Imports System.Data

Namespace SRA

    Public Class clsWeb
        Public Shared Function gVerificarConexion(ByVal pPage As Page) As String
            If (pPage.Session("sConn") Is Nothing) Then
                pPage.Response.Redirect("login.aspx")
            Else
                If System.Configuration.ConfigurationManager.AppSettings("conCulture") Is Nothing Then
                    System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo(ConfigurationManager.AppSettings("conCulture"))
                End If
                Return (pPage.Session("sConn").ToString)
            End If
            Return String.Empty
        End Function

        Public Shared Function gFormatCheck(ByVal pstrValor As String, ByVal pstrTrue As String) As Boolean
            If Not pstrValor Is System.DBNull.Value Then
                If pstrValor = pstrTrue Then
                    Return (True)
                Else
                    Return (False)
                End If
            Else
                Return (False)
            End If
        End Function

        Public Shared Sub gCargarRefeCmb(ByVal pstrConn As String, ByVal pstrTable As String, ByVal pcmbCmb As System.Web.UI.WebControls.ListControl, ByVal pstrOpc As String)
            gCargarRefeCmb(pstrConn, pstrTable, pcmbCmb, pstrOpc, "")
        End Sub

        Public Shared Sub gCargarRefeCmb(ByVal pstrConn As String, ByVal pstrTable As String, ByVal pcmbCmb As System.Web.UI.WebControls.ListControl, ByVal pstrOpc As String, ByVal pstrFiltro As String, Optional ByVal pbooIgnoraDefa As Boolean = False)
            Dim combos As New ArrayList
            combos.Add(pcmbCmb)
            gCargarRefeCmb(pstrConn, pstrTable, combos, pstrOpc, pstrFiltro, pbooIgnoraDefa)
            'Dim lstrCmd As String = "exec " + pstrTable + "_cargar " + pstrFiltro
            'gCargarCombo(pstrConn, lstrCmd, pcmbCmb, "id", "descrip", pstrOpc, pbooIgnoraDefa)
        End Sub

        Public Shared Sub gCargarRefeCmb(ByVal pstrConn As String, ByVal pstrTable As String, _
            ByVal pcmbCombos As ArrayList, ByVal pstrOpc As String)

            gCargarRefeCmb(pstrConn, pstrTable, pcmbCombos, pstrOpc, "")
        End Sub

        Public Shared Sub gCargarRefeCmb(ByVal pstrConn As String, _
            ByVal pstrTable As String, _
            ByVal pcmbCombos As ArrayList, _
            ByVal pstrOpc As String, _
            ByVal pstrFiltro As String, _
            Optional ByVal pbooIgnoraDefa As Boolean = False)

            Dim lstrCmd As String = "exec " + pstrTable + "_cargar " + pstrFiltro
            mCargarCombosConDs(pcmbCombos, mCargaDS(pstrConn, lstrCmd), "id", "descrip", pstrOpc, lstrCmd, pbooIgnoraDefa)
        End Sub

        Private Shared Function mCargaDS(ByVal pstrConn As String, ByVal lstrCmd As String) As DataSet
            Return clsSQLServer.gExecuteQuery(pstrConn, lstrCmd)
        End Function

        Private Shared Sub mSetearCombo(ByVal pstrCmd As String, _
            ByVal pcmb As ListControl, _
            ByVal pstrCodi As String, _
            ByVal pstrDesc As String, _
            ByVal pstrOpc As String, _
            Optional ByVal pbooIgnoraDefa As Boolean = False)
            If IsNothing(pcmb) Then

                'clsError.gGenerarMensajes("mSetearCombo", "DEBUG", _
                '          "mSetearCombo;" + "pstrCodi-" + pstrCodi + _
                '    ";pstrDesc-" + pstrDesc + "pstrOpc-" + pstrOpc)
                'clsError.gGenerarMensajes("mCargarComboConDs", "DEBUG", "pcmb isNothing")
                Exit Sub
            End If

            With pcmb
                If (.Items.Count <> 1 Or pstrOpc <> "S") Then
                    Select Case (pstrOpc)
                        Case "S"
                            .Items.Insert(0, "(Seleccione)")
                            .Items(0).Value = ""
                        Case "N"
                            .Items.Insert(0, "(Ninguno)")
                            .Items(0).Value = ""
                        Case "T"
                            .Items.Insert(0, "(Todos)")
                            .Items(0).Value = ""
                        Case "Z"
                            '.Items.Insert(0, "(No Retira)")
                            .Items.Insert(0, "No")
                            .Items(0).Value = ""
                        Case "R"
                            .Items.Insert(0, "RESUMIDO")
                            .Items(0).Value = 0
                    End Select

                    .SelectedIndex = -1
                End If

                If Not pbooIgnoraDefa Then
                    'le pone el valor default

                    Dim vlErr1 As Boolean = False
                    Dim vlErr2 As Boolean = False

                    If Not IsNothing(.DataSource) Then
                        If pcmb.Items.Count > 0 Then
                            If pcmb.Items(0).Value.ToString() = "" Then
                                vlErr1 = True
                            End If
                            If pcmb.Items.Count = 2 Then
                                If pcmb.Items(pcmb.Items.Count - 1).Value.ToString() = "" Then
                                    vlErr2 = True
                                End If
                            End If
                        End If


                        ' se agrego esta validacion porque si el combo esta vacio y no es nothing da error
                        If Not vlErr1 And Not vlErr2 Then
                            With CType(.DataSource, DataSet).Tables(0)
                                If .Rows.Count > 0 And .Columns.Contains("defa") Then
                                    With .Select("defa=true")
                                        If .GetUpperBound(0) > -1 Then
                                            pcmb.SelectedIndex = gBuscarValorCombo(pcmb, DirectCast(.GetValue(0), DataRow).Item(pstrCodi))
                                            pcmb.Page.Session.Add(pcmb.ClientID + "_defa", pcmb.SelectedValue)
                                        End If
                                    End With
                                End If
                            End With
                        End If

                    End If



                End If
            End With


            If pcmb.GetType().Name = "ComboBox" Then
                pstrCmd = Replace(pstrCmd, "exec ", "")
                If pstrCmd.IndexOf(" ") > -1 Then pstrCmd = Left(pstrCmd, pstrCmd.IndexOf(" "))
                DirectCast(pcmb, NixorControls.ComboBox).NomOper = pstrCmd
            End If




        End Sub

        Private Shared Sub mCargarCombosConDs(ByVal pcmbCombos As ArrayList,
            ByVal ds As DataSet, ByVal pstrCodi As String,
            ByVal pstrDesc As String, ByVal pstrOpc As String,
            ByVal pstrCmd As String, ByVal pbooIgnoraDefa As Boolean)

            Dim ie As IEnumerator
            Dim cmb As ListControl
            ie = pcmbCombos.GetEnumerator()
            ie.Reset()
            While (ie.MoveNext())
                cmb = ie.Current

                ' Dario 2013-04-29 Comentado por error de casteo en los combos que no retornan datos
                'If ds.Tables(0).Rows.Count > 0 Then
                mCargarComboConDs(cmb, ds, pstrCodi, pstrDesc)
                'Else
                ' clsError.gGenerarMensajes("mCargarCombosConDs-pstrDesc", "DEBUG", "mCargarCombosConDs-DATASET VACIO" + ds.Tables(0).TableName)

                'End If

                mSetearCombo(pstrCmd, cmb, pstrCodi, pstrDesc, pstrOpc, pbooIgnoraDefa)
            End While
        End Sub

        Private Shared Sub mCargarComboConDs(ByVal pcmb As ListControl,
            ByVal ds As DataSet,
            ByVal pstrCodi As String,
            ByVal pstrDesc As String)

            If IsNothing(pcmb) Then
                ' clsError.gGenerarMensajes("mCargarComboConDs-pstrDesc", "DEBUG", _
                '      "mCargarComboConDs-DATASET VACIO;" + "pstrCodi-" + pstrCodi + _
                '";pstrDesc-" + pstrDesc)
                '      clsError.gGenerarMensajes("mCargarComboConDs", "DEBUG", "pcmb isNothing")
                Exit Sub


            End If

            ' Dario 2013-04-29 Comentado por error de casteo en los combos que no retornan datos
            'If ds.Tables(0).Rows.Count > 0 Then
            With pcmb
                .DataSource = ds
                .DataValueField = pstrCodi
                .DataTextField = pstrDesc
                .DataBind()
            End With
            'Else
            'clsError.gGenerarMensajes("mCargarComboConDs-pstrDesc", "DEBUG", "mCargarComboConDs-DATASET VACIO" + ds.Tables(0).TableName)
            'End If

        End Sub
        Public Shared Sub gCargarCombo(ByVal pstrConn As String,
           ByVal pstrCmd As String,
           ByVal pcmbCombo As ListControl,
           ByVal pstrCodi As String,
           ByVal pstrDesc As String,
           ByVal pstrOpc As String,
           Optional ByVal pbooIgnoraDefa As Boolean = False)

            Dim ds As New DataSet

            ds = mCargaDS(pstrConn, pstrCmd)
            If ds.Tables(0).Rows.Count > 0 Then
                mCargarComboConDs(pcmbCombo, ds, pstrCodi, pstrDesc)
            Else
                '  clsError.gGenerarMensajes("gcargarCombo", "DEBUG", "DATASET VACIO-pstrCmd=" + pstrCmd)

                mCargarComboConDs(pcmbCombo, ds, pstrCodi, pstrDesc)
            End If
            mSetearCombo(pstrCmd, pcmbCombo, pstrCodi, pstrDesc, pstrOpc, pbooIgnoraDefa)
        End Sub
        ' Dario 2014-06-02 sobrecarga que permite cargar incluso nada
        Public Shared Sub gCargarCombo(ByVal pstrConn As String,
        ByVal pstrCmd As String,
        ByVal pcmbCombo As ListControl,
        ByVal pstrCodi As String,
        ByVal pstrDesc As String,
        ByVal pstrOpc As String,
        ByVal pbooIgnoraDefa As Boolean,
        ByVal pbooCargarDsVacio As Boolean)

            Dim ds As New DataSet

            ds = mCargaDS(pstrConn, pstrCmd)
            If ds.Tables(0).Rows.Count > 0 Then
                mCargarComboConDs(pcmbCombo, ds, pstrCodi, pstrDesc)
            Else
                '  clsError.gGenerarMensajes("gcargarCombo", "DEBUG", "DATASET VACIO-pstrCmd=" + pstrCmd)
                ' Dario 2014-06-02 para cargar el combo con lo que venta incluso nada
                If (pbooCargarDsVacio = True) Then
                    mCargarComboConDs(pcmbCombo, ds, pstrCodi, pstrDesc)
                End If
            End If

            mSetearCombo(pstrCmd, pcmbCombo, pstrCodi, pstrDesc, pstrOpc, pbooIgnoraDefa)
        End Sub
        Public Shared Sub gCargarCombo(ByVal pstrConn As String,
          ByVal pstrCmd As String,
          ByVal pcmbCombo As ListControl,
          ByVal pstrCodi As String,
          ByVal pstrDesc As String,
          ByVal pstrOpc As String,
          ByVal RazaId As String,
          ByVal Codi As String,
          Optional ByVal pbooIgnoraDefa As Boolean = False)


            Dim ds As New DataSet

            ds = mCargaDS(pstrConn, pstrCmd)
            If ds.Tables(0).Rows.Count > 0 Then
                mCargarComboConDs(pcmbCombo, ds, pstrCodi, pstrDesc)
            Else
                '  clsError.gGenerarMensajes("gcargarCombo", "DEBUG", "DATASET VACIO-pstrCmd=" + pstrCmd)

            End If





            mSetearCombo(pstrCmd, pcmbCombo, pstrCodi, pstrDesc, pstrOpc, pbooIgnoraDefa)
        End Sub

        Public Shared Sub gCargarCombo(ByVal pstrConn As String,
        ByVal pstrCmd As String,
        ByVal pcmbCombo As ListControl,
        ByVal pstrCodi As String,
        ByVal pstrDesc As String)

            ' clsError.gGenerarMensajes("gCargarCombo-gCargarCombo", "DEBUG", "5")
            gCargarCombo(pstrConn, pstrCmd, pcmbCombo, pstrCodi, pstrDesc, "")
        End Sub

        Public Shared Sub gCargarComboBool(ByVal pCombo As NixorControls.ComboBox, ByVal pstrOpc As String)
            With pCombo
                .Items.Clear()
                If pstrOpc = "S" Then
                    .Items.Add("(Sel)")
                    .Items(.Items.Count - 1).Value = ""
                ElseIf pstrOpc = "N" Then
                    .Items.Add("(Ninguno)")
                    .Items(.Items.Count - 1).Value = ""
                ElseIf pstrOpc = "T" Then
                    .Items.Add("(Todos)")
                    .Items(.Items.Count - 1).Value = ""
                ElseIf pstrOpc = "Z" Then
                    '.Items.Add("(No Retira)")
                    .Items.Add("No")
                    .Items(.Items.Count - 1).Value = ""
                ElseIf pstrOpc = "R" Then
                    .Items.Add("RESUMIDO")
                    .Items(.Items.Count - 1).Value = 0
                ElseIf pstrOpc = "SS" Then
                    .Items.Add("No")
                    .Items(.Items.Count - 1).Value = ""
                End If

                .Items.Add("Si")
                .Items(.Items.Count - 1).Value = "1"
                .Items.Add("No")
                .Items(.Items.Count - 1).Value = "0"
                .Width = New Unit(50)
            End With
        End Sub

        Public Shared Sub gCargarDataGrid(ByVal pstrConn As String, ByVal pstrCmd As String, ByVal pgrdGrid As DataGrid)

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(pstrConn, pstrCmd, 99999)

            pgrdGrid.DataSource = ds
            If pgrdGrid.CurrentPageIndex > pgrdGrid.PageCount - 1 Then
                pgrdGrid.CurrentPageIndex = 0
            End If

            pgrdGrid.DataBind()

            ds.Dispose()
        End Sub

        Public Shared Sub gCargarDataGrid(ByVal pstrConn As String, ByVal pstrCmd As String, ByVal pgrdGrid As DataGrid, ByVal intTimeOut As Int16)

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(pstrConn, pstrCmd, intTimeOut)

            pgrdGrid.DataSource = ds
            If pgrdGrid.CurrentPageIndex > pgrdGrid.PageCount - 1 Then
                pgrdGrid.CurrentPageIndex = 0
            End If

            pgrdGrid.DataBind()

            ds.Dispose()
        End Sub

        Public Shared Function gCargarDataSet(ByVal pstrConn As String, ByVal pstrCmd As String) As DataSet

            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(pstrConn, pstrCmd)
            Return ds

        End Function



        Public Shared Sub gCargarDataGrid(ByVal pstrConn As String, _
            ByVal pstrCmd As String, _
            ByVal pgrdGrid As DataGrid, ByVal pintTimeout As Integer)

            Dim ds As New DataSet

            ds = clsSQLServer.gExecuteQuery(pstrConn, pstrCmd, pintTimeout)
            pgrdGrid.DataSource = ds
            pgrdGrid.DataBind()
            ds.Dispose()
        End Sub

        Public Shared Function gBuscarValorCombo(ByVal pobjCombo As ListControl, ByVal pstrValor As String) As Integer

            Return (pobjCombo.Items.IndexOf(pobjCombo.Items.FindByValue(pstrValor)))
        End Function

        Public Shared Function gValorCombo(ByVal pobjCombo As NixorControls.ComboBox) As String

            If (pobjCombo.Items.Count = 0) Then
                Return ("")
            Else

                Return (pobjCombo.SelectedItem.Value.ToString())
            End If
        End Function
        Public Shared Function gTextoCombo(ByVal pobjCombo As NixorControls.ComboBox) As String

            If (pobjCombo.Items.Count = 0) Then
                Return ("")
            Else

                Return (pobjCombo.SelectedItem.Text)
            End If
        End Function


        Public Shared Function gValorCheckBox(ByVal pchkObje As CheckBox, ByVal pstrChecked As String, ByVal pstrUnChecked As String) As String
            If (pchkObje.Checked) Then
                Return (pstrChecked)
            Else
                Return (pstrUnChecked)
            End If
        End Function

        Public Shared Sub gGenerarVentana(ByVal pPagina As Page, ByVal pstrTabla As String, ByVal pstrFiltros As String, Optional ByVal pintAncho As Integer = 500, Optional ByVal pintAlto As Integer = 400, Optional ByVal pbooMultiple As Boolean = False)
            Dim lsbMsg As New StringBuilder
            lsbMsg.Append("<SCRIPT language='javascript' for=window event=onload>")
            lsbMsg.Append(vbCrLf)

            'abre la ventana
            If Not pbooMultiple Then
                lsbMsg.Append("var Ventana = window.open (""consulta_pop.aspx?tabla=")
            Else
                lsbMsg.Append("var Ventana = window.open (""consulta_pop_mul.aspx?tabla=")
            End If

            lsbMsg.Append(pstrTabla)
            lsbMsg.Append("&filtros=")
            lsbMsg.Append(pstrFiltros)
            lsbMsg.Append(""",""")
            lsbMsg.Append(pPagina.Session.SessionID)
            lsbMsg.Append(String.Format(""",""location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no,width={0}px,height={1}px"");", pintAncho.ToString, pintAlto.ToString))

            'mantiene el foco
            lsbMsg.Append(vbCrLf)
            lsbMsg.Append("function window_onfocus() {")
            lsbMsg.Append(vbCrLf)
            lsbMsg.Append("if (Ventana!= null){")
            lsbMsg.Append(vbCrLf)
            lsbMsg.Append("try")
            lsbMsg.Append("{Ventana.focus();}")
            lsbMsg.Append(vbCrLf)
            lsbMsg.Append("catch(e)")
            lsbMsg.Append("{Ventana=null;}")
            lsbMsg.Append(vbCrLf)
            lsbMsg.Append("finally")
            lsbMsg.Append("{;}")
            lsbMsg.Append("}")
            lsbMsg.Append("}")
            lsbMsg.Append(vbCrLf)
            lsbMsg.Append("window.attachEvent('onfocus',window_onfocus);")
            lsbMsg.Append(vbCrLf)
            lsbMsg.Append("</SCRIPT>")
            'pPagina.RegisterClientScriptBlock("Busqueda", lsbMsg.ToString)
            pPagina.ClientScript.RegisterClientScriptBlock(GetType(Page), "Busqueda", lsbMsg.ToString)

        End Sub
        Public Shared Sub gGenerarPopUp(ByVal pPagina As Page, _
                ByVal pstrPagina As String, _
                Optional ByVal pintAncho As Integer = 500, _
                Optional ByVal pintAlto As Integer = 400, _
                Optional ByVal pintArriba As Integer = 0, _
                Optional ByVal pintIzq As Integer = 0, _
                Optional ByVal pboolScroll As Boolean = False)


            Dim lsbMsg As New StringBuilder
            With lsbMsg
                .Append("<SCRIPT language='javascript' for=window event=onload>")
                .Append(vbCrLf)
                'abre la ventana
                .Append("var Ventana = window.open (""" + pstrPagina)
                .Append(""",""")
                .Append(pPagina.Session.SessionID & System.DateTime.Now.Second)
                .Append(String.Format(""",""location=no,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,width={0}px,height={1}px,top={2}px,left={3}px,scrollbars={4}"");", pintAncho.ToString, pintAlto.ToString, pintArriba.ToString, pintIzq.ToString, IIf(pboolScroll, "yes", "no")))

                'mantiene el foco
                .Append(vbCrLf)
                .Append("function window_onfocus() {")
                .Append(vbCrLf)
                .Append("if (Ventana!= null){")
                .Append(vbCrLf)
                .Append("try")
                .Append("{Ventana.focus();}")
                .Append(vbCrLf)
                .Append("catch(e)")
                .Append("{Ventana=null;}")
                .Append(vbCrLf)
                .Append("finally")
                .Append("{;}")
                .Append("}")
                .Append("}")
                .Append(vbCrLf)
                .Append("window.attachEvent('onfocus',window_onfocus);")
                .Append("window.document.body.attachEvent('onfocus',window_onfocus);")
                .Append(vbCrLf)
                .Append("</SCRIPT>")
                'pPagina.RegisterClientScriptBlock("Busqueda", .ToString)
                pPagina.ClientScript.RegisterClientScriptBlock(GetType(Page), "Busqueda", .ToString)

            End With
        End Sub

        Public Shared Sub gValidarControles(ByVal pPage As Page)
            For Each oCtrl As Control In pPage.Controls
                Select Case oCtrl.GetType().Name
                    Case "HtmlForm", "Panel", "HtmlTable", "HtmlTableRow", "HtmlTableCell"
                        For Each oSubCtrl As Control In oCtrl.Controls
                            gValidarControl(oSubCtrl)
                        Next

                    Case "TextBoxTab", "NumberBox", "CosPostalBox", "ComboBox", "CheckListBox", "CuitBox", "DateBox", "HourBox"
                        gValidarControl(oCtrl)
                End Select
            Next
            If (pPage.Session("sConn") Is Nothing) Then
                pPage.Response.Redirect("login.aspx")
            End If
        End Sub

        Private Shared Sub gValidarControl(ByVal pControl As Control)
            Select Case pControl.GetType().Name
                Case "HtmlForm", "Panel", "HtmlTable", "HtmlTableRow", "HtmlTableCell"
                    For Each oSubCtrl As Control In pControl.Controls
                        gValidarControl(oSubCtrl)
                    Next

                Case "TextBoxTab", "NumberBox", "DateBox", "HourBox", "CUITBox", "CosPostalBox"
                    Dim oText As NixorControls.TextBoxControls = DirectCast(pControl, NixorControls.TextBoxControls)
                    If oText.Enabled And oText.Obligatorio And oText.Text.Length = 0 Then
                        If (oText.Panel.Length > 0) And pControl.Parent.GetType().Name.IndexOf("_ascx") = -1 Then
                            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + oText.CampoVal + " del panel " + oText.Panel)
                        Else
                            Throw New AccesoBD.clsErrNeg("Debe ingresar el campo " + oText.CampoVal)
                        End If
                    End If

                Case "ComboBox"
                    Dim oText As NixorControls.ComboBox = DirectCast(pControl, NixorControls.ComboBox)
                    If oText.Enabled And oText.Obligatorio And (oText.Valor Is DBNull.Value Or oText.Valor.Trim.Length = 0) Then
                        Throw New AccesoBD.clsErrNeg("Debe seleccionar el campo " + oText.CampoVal)
                    End If

                Case "CheckListBox"
                    Dim oText As NixorControls.CheckListBox = DirectCast(pControl, NixorControls.CheckListBox)
                    If oText.Enabled And oText.Obligatorio And oText.ObtenerIDs = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe seleccionar el campo " + oText.CampoVal)
                    End If

                Case Else
                    If pControl.GetType().Name.IndexOf("_ascx") > 0 Then
                        For Each oSubCtrl As Control In pControl.Controls
                            gValidarControl(oSubCtrl)
                        Next
                    End If
            End Select
        End Sub

        Public Shared Sub gInicializarControles(ByRef pPage As Page, ByVal pSession As String)
            For Each oCtrl As Control In pPage.Controls
                Select Case oCtrl.GetType().Name
                    Case "HtmlForm", "Panel", "HtmlTable", "HtmlTableRow", "HtmlTableCell"
                        For Each oSubCtrl As Control In oCtrl.Controls
                            gInicializarControl(oSubCtrl, pSession, oCtrl.ClientID)
                        Next

                    Case "TextBoxTab", "NumberBox", "CosPostalBox", "ComboBox", "CheckListBox", "CuitBox", "DateBox", "HourBox"
                        gInicializarControl(oCtrl, pSession, "")
                End Select
            Next
            If (pPage.Session("sConn") Is Nothing) Then
                pPage.Response.Redirect("login.aspx")
            End If
        End Sub

        Private Shared Sub gLeerControl(ByVal pPagiNombre As String, ByVal pControlNombre As String, ByRef pCampoNombre As String, ByRef pCampoTooltip As String, ByRef pPanelNombre As String, ByRef pLabel As String, ByVal pSession As String, ByVal pPanel As String, ByVal pPagina As Control, ByVal pPrefijo As String)
            Dim lstrCmd As New StringBuilder

            lstrCmd.Append("exec Controles_consul")
            lstrCmd.Append(" ")
            lstrCmd.Append(pPagiNombre.Remove(0, 4).Replace("_aspx", ""))   ' Quitar "ASP."
            lstrCmd.Append(", ")
            lstrCmd.Append(pControlNombre)

            Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(pSession, lstrCmd.ToString)
            Dim lbl As System.Web.UI.WebControls.Label
            If ldsDatos.Tables(0).Rows.Count > 0 Then
                With ldsDatos.Tables(0).Rows(0)
                    pCampoNombre = clsFormatear.gFormatCadena(.Item("control_campo_nombre").ToString())
                    pCampoTooltip = clsFormatear.gFormatCadena(.Item("control_tooltip").ToString())
                    pPanelNombre = clsFormatear.gFormatCadena(.Item("control_panel_nombre").ToString())
                    pLabel = .Item("control_label")     'clsFormatear.gFormatCadena(.Item("esta_codi").ToString())
                End With
            Else
                lstrCmd.Length = 0
                'si existe un label con el mismo nombre que el control graba 
                'el texto y prende el flag 

                ' **************************************************************************
                ' 12/05/2021 Se intenta castear el control TextBoxTab de NIXOR a Label.
                ' En todos casos esto no es posible. Se atrapa el error y manipula el mismo
                ' para que el flujo del programa contin�e.

                ' �apturo el control para verificar que es un NIXOR realemnte.
                'Dim vcontrol = pPagina.FindControl(pControlNombre.Replace(pPrefijo, "lbl"))

                Try
                    lbl = pPagina.FindControl(pControlNombre.Replace(pPrefijo, "lbl"))
                Catch ex As Exception
                    lbl = Nothing
                End Try
                ' **************************************************************************

                'lbl = pPagina.FindControl(pControlNombre.Replace(pPrefijo, "lbl"))
                lstrCmd.Append("exec Controles_Alta")
                lstrCmd.Append(" ")
                lstrCmd.Append(pPagiNombre.Remove(0, 4).Replace("_aspx", ""))   ' Quitar "ASP."
                lstrCmd.Append(", ")
                lstrCmd.Append(pControlNombre)
                lstrCmd.Append(", ")
                If (Not (lbl Is Nothing)) Then
                    lstrCmd.Append("'" + lbl.Text.Replace(":", "") + "'")
                Else
                    lstrCmd.Append(pControlNombre)
                End If
                lstrCmd.Append(", ")
                If (pPanel.Length > 0) Then
                    lstrCmd.Append(pPanel.Remove(0, 3))
                Else
                    lstrCmd.Append("null")
                End If

                If (Not (lbl Is Nothing)) Then
                    lstrCmd.Append(", 1 ")
                Else
                    lstrCmd.Append(", 0 ")
                End If

                lstrCmd.Append(", ''")

                Dim lintExec As Integer = clsSQLServer.gExecute(pSession, lstrCmd.ToString)

            End If
        End Sub

        Private Shared Sub gInicializarControl(ByRef pControl As Control, _
                ByVal pSession As String, ByVal pPanel As String)

            'Obtener datos de la BD para el control
            Dim CampoNombre As String = Nothing
            Dim CampoTooltip As String = Nothing
            Dim PanelNombre As String = Nothing
            Dim Label As Boolean

            'Asignarlos al control
            Select Case pControl.GetType().Name
                Case "HtmlForm", "Panel", "HtmlTable", "HtmlTableRow", "HtmlTableCell"
                    For Each oSubCtrl As Control In pControl.Controls
                        gInicializarControl(oSubCtrl, pSession, pControl.ClientID)
                    Next

                Case "TextBoxTab", "NumberBox", "CUITBox", "CosPostalBox", "DateBox", "HourBox"
                    gLeerControl(pControl.Page.ToString(), pControl.ID, CampoNombre, CampoTooltip, PanelNombre, Label, pSession, pPanel, pControl.Parent, "txt")
                    Dim oText As NixorControls.TextBoxControls = DirectCast(pControl, NixorControls.TextBoxControls)

                    If oText.UniqueID.IndexOf(":") = -1 Then    'si no es un usercontrol
                        oText.CampoVal = CampoNombre
                        oText.ToolTip = CampoTooltip
                        oText.Panel = PanelNombre
                    End If

                    'si existe un label con el mismo nombre que el cotrol y se usa label se asigna el nombre
                    If (Label) Then
                        Dim lbl As System.Web.UI.WebControls.Label
                        lbl = oText.Parent.FindControl(oText.ID.Replace("txt", "lbl"))
                        If (Not (lbl Is Nothing)) Then
                            lbl.Text = CampoNombre + ":"
                        End If
                    End If

                Case "ComboBox"
                    gLeerControl(pControl.Page.ToString(), pControl.ClientID, CampoNombre, CampoTooltip, PanelNombre, Label, pSession, pPanel, pControl.Page, "cmb")
                    Dim oText As NixorControls.ComboBox = DirectCast(pControl, NixorControls.ComboBox)
                    oText.CampoVal = CampoNombre
                    oText.ToolTip = CampoTooltip
                    'si existe un label con el mismo nombre que el cotrol y se usa label se asigna el nombre
                    If (Label) Then
                        Dim lbl As System.Web.UI.WebControls.Label
                        lbl = oText.Parent.FindControl(oText.ID.Replace("cmb", "lbl"))
                        If (Not (lbl Is Nothing)) Then
                            lbl.Text = CampoNombre + ":"
                        End If
                    End If

                Case "CheckListBox"
                    gLeerControl(pControl.Page.ToString(), pControl.ClientID, CampoNombre, CampoTooltip, PanelNombre, Label, pSession, pPanel, pControl.Page, "lst")
                    Dim oText As NixorControls.CheckListBox = DirectCast(pControl, NixorControls.CheckListBox)
                    oText.CampoVal = CampoNombre
                    oText.ToolTip = CampoTooltip
                    'si existe un label con el mismo nombre que el cotrol y se usa label se asigna el nombre
                    If (Label) Then
                        Dim lbl As System.Web.UI.WebControls.Label
                        lbl = oText.Parent.FindControl(oText.ID.Replace("lst", "lbl"))
                        If (Not (lbl Is Nothing)) Then
                            lbl.Text = CampoNombre + ":"
                        End If
                    End If


                Case Else
                    If pControl.GetType().Name.IndexOf("_ascx") > 0 And Not pControl.GetType().Name.StartsWith("usr") Then
                        For Each oSubCtrl As Control In pControl.Controls
                            gInicializarControl(oSubCtrl, pSession, "")
                        Next
                    End If
            End Select

        End Sub
        Public Shared Sub gDeshabilitarBotones(ByRef pPage As Page)

            ' Si la pagina tiene un label llamado lblReadOnly lo muestra
            Dim lbl As System.Web.UI.WebControls.Label
            lbl = pPage.FindControl("lblReadOnly")
            If (Not (lbl Is Nothing)) Then
                lbl.Visible = True
            End If

            'recorre los botones y llama a gDeshabilitarBoton 
            ' que deshabilita los que tienen TAB en blanco
            For Each oCtrl As Control In pPage.Controls
                Select Case oCtrl.GetType().Name
                    Case "HtmlForm", "Panel", "HtmlTable", "HtmlTableRow", "HtmlTableCell"
                        For Each oSubCtrl As Control In oCtrl.Controls
                            gDeshabilitarBoton(oSubCtrl)
                        Next

                    Case "Button"
                        gDeshabilitarBoton(oCtrl)

                End Select
            Next
            If (pPage.Session("sConn") Is Nothing) Then
                pPage.Response.Redirect("login.aspx")
            End If
        End Sub
        Private Shared Sub gDeshabilitarBoton(ByRef pControl As Control)

            Select Case pControl.GetType().Name
                Case "HtmlForm", "Panel", "HtmlTable", "HtmlTableRow", "HtmlTableCell"
                    For Each oSubCtrl As Control In pControl.Controls
                        gDeshabilitarBoton(oSubCtrl)
                    Next

                Case "Button"
                    ' deshabilita los botones que tienen TAB en blanco
                    Dim oButton As Button = DirectCast(pControl, Button)
                    If (oButton.CommandName.Length = 0) Then
                        oButton.Enabled = False
                    End If
            End Select

        End Sub

        Public Shared Sub gActivarControl(ByVal ptxtObj As System.Web.UI.WebControls.TextBox, ByVal pbooActivo As Boolean)
            If pbooActivo Then
                ptxtObj.Enabled = True
                If ptxtObj.TextMode = TextBoxMode.MultiLine Then
                    ptxtObj.CssClass = "cuadrotextolibre"
                Else
                    ptxtObj.CssClass = "cuadrotexto"
                End If

            Else
                ptxtObj.Enabled = False
                ptxtObj.CssClass = "cuadrotextodeshab"
            End If
        End Sub

        Public Shared Sub gActivarFileControl(ByVal ptxtObj As System.Web.UI.HtmlControls.HtmlInputFile, ByVal pbooActivo As Boolean)
            If pbooActivo Then
                ptxtObj.Disabled = False
                'ptxtObj.Style = "cuadrotexto"
            Else
                ptxtObj.Disabled = True
                'ptxtObj.Style = "cuadrotextodeshab"
            End If
        End Sub

        Public Shared Function gSumarFechas(ByVal Fecha As Object, ByVal Hora As Object) As Object
            If Fecha Is DBNull.Value Then
                Return DBNull.Value
            Else
                If Hora Is DBNull.Value Or Hora.ToString.Trim.Length = 0 Then
                    Return Fecha
                Else
                    Return (CDate(Fecha).AddHours(CDate(Hora).Hour).AddMinutes(CDate(Hora).Minute))
                End If
            End If
        End Function

        Public Shared Sub gCargarMeses(ByVal pcmbCmb As System.Web.UI.WebControls.ListControl)
            With pcmbCmb
                .Items.Clear()
                .Items.Add("Enero")
                .Items(.Items.Count - 1).Value = "1"
                .Items.Add("Febrero")
                .Items(.Items.Count - 1).Value = "2"
                .Items.Add("Marzo")
                .Items(.Items.Count - 1).Value = "3"
                .Items.Add("Abril")
                .Items(.Items.Count - 1).Value = "4"
                .Items.Add("Mayo")
                .Items(.Items.Count - 1).Value = "5"
                .Items.Add("Junio")
                .Items(.Items.Count - 1).Value = "6"
                .Items.Add("Julio")
                .Items(.Items.Count - 1).Value = "7"
                .Items.Add("Agosto")
                .Items(.Items.Count - 1).Value = "8"
                .Items.Add("Septiembre")
                .Items(.Items.Count - 1).Value = "9"
                .Items.Add("Octubre")
                .Items(.Items.Count - 1).Value = "10"
                .Items.Add("Noviembre")
                .Items(.Items.Count - 1).Value = "11"
                .Items.Add("Diciembre")
                .Items(.Items.Count - 1).Value = "12"
            End With
        End Sub

        Public Shared Sub gSetearActivos(ByRef pPage As Page, Optional ByVal bHabilita As Boolean = True)
            ' Si la pagina tiene un label llamado lblReadOnly lo muestra
            Dim lbl As Label
            lbl = pPage.FindControl("lblReadOnly")
            If (Not (lbl Is Nothing)) Then
                lbl.Visible = True
            End If

            'recorre los botones y llama a gDeshabilitarBoton 
            ' que deshabilita los que tienen TAB en blanco
            For Each oCtrl As Control In pPage.Controls
                Select Case oCtrl.GetType().Name
                    Case "HtmlForm", "Panel"
                        For Each oSubCtrl As Control In oCtrl.Controls
                            gSetearActivo(oSubCtrl, bHabilita)
                        Next

                    Case "TextBoxTab", "NumberBox", "CUITBox", "CosPostalBox", "DateBox", "HourBox"
                        gSetearActivo(oCtrl, bHabilita)

                    Case "ComboBox", "CheckListBox", "RadioButtonList", "Button"
                        Dim oWebCtrl As WebControl = DirectCast(oCtrl, WebControl)
                        oWebCtrl.Enabled = bHabilita
                    Case "HtmlInputFile"
                        DirectCast(oCtrl, HtmlInputFile).Disabled = Not bHabilita
                End Select
            Next
            If (pPage.Session("sConn") Is Nothing) Then
                pPage.Response.Redirect("login.aspx")
            End If
        End Sub

        Public Shared Sub gSetearActivo(ByRef pControl As Control, ByVal bHabilita As Boolean)
            Select Case pControl.GetType().Name
                Case "HtmlForm", "Panel"
                    For Each oSubCtrl As Control In pControl.Controls
                        gSetearActivo(oSubCtrl, bHabilita)
                    Next

                Case "TextBoxTab", "NumberBox", "CUITBox", "CodPostalBox", "DateBox", "HourBox"
                    ' deshabilita los botones que tienen TAB en blanco
                    Dim oTexto As NixorControls.TextBoxControls = DirectCast(pControl, NixorControls.TextBoxControls)
                    gActivarControl(oTexto, bHabilita)

                Case "ComboBox", "CheckListBox", "RadioButtonList", "Button"
                    Dim oWebCtrl As WebControl = DirectCast(pControl, WebControl)
                    oWebCtrl.Enabled = bHabilita

                Case "HtmlInputFile"
                    DirectCast(pControl, HtmlInputFile).Disabled = Not bHabilita
            End Select
        End Sub

        Public Shared Function gValorParametro(ByVal pstrPara As String) As String
            Dim lstrPara As String
            If pstrPara Is Nothing Then
                lstrPara = ""
            Else
                If pstrPara Is System.DBNull.Value Then
                    lstrPara = ""
                Else
                    lstrPara = pstrPara
                End If
            End If
            Return (lstrPara)
        End Function
        Public Shared Function ConvertDsToXML(ByVal pDs As DataSet) As Boolean
            Dim strFileName As String
            Dim modoEjecucion As String

            If pDs Is Nothing Then
                Return False

            End If
            modoEjecucion = ConfigurationManager.AppSettings("ModoEjecucion").ToString()
            If (modoEjecucion = "DEBUG") Then
                strFileName = "C:\TEMP\DIF.XML"
                Dim fs As New FileStream(strFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                Dim writer As New StreamWriter(fs, Encoding.UTF8)
                pDs.WriteXml(writer, XmlWriteMode.WriteSchema)
                fs.Close()
            End If

            Return True

        End Function

        Public Shared Function ConvertDsToXML(ByVal pDs As DataSet, ByVal pLogFile As String) As Boolean
            Dim strFileName As String
            Dim modoEjecucion As String

            If pDs Is Nothing Then
                Return False

            End If

            modoEjecucion = ConfigurationManager.AppSettings("ModoEjecucion").ToString()
            If (modoEjecucion = "DEBUG") Then
                strFileName = "C:\TEMP\" + pLogFile + ".XML"
                Dim fs As New FileStream(strFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                Dim writer As New StreamWriter(fs, Encoding.UTF8)
                pDs.WriteXml(writer, XmlWriteMode.WriteSchema)
                fs.Close()
            End If



            Return True
        End Function


        Public Shared Function gImprimirReporte(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrReporte As String, ByVal pstrImprId As String, ByVal pstrParamNom As String, ByVal pstrParamVal As String, ByVal pRequest As HttpRequest) As Integer
            Dim lvstrParamNom() As String = pstrParamNom.Split(";")
            Dim lvstrParamVal() As String = pstrParamVal.Split(";")
            Dim lbooRet As Boolean
            Dim lrptPrint As clsrptPrint = New clsrptPrint
            Dim lstrRpt As String
            Dim lstrImpresora As String
            Dim lDs As DataSet

            Try
                lDs = clsSQLServer.gObtenerEstruc(pstrConn, "impresoras", pstrImprId)

                'lstrImpresora = System.Drawing.Printing.PrinterSettings.InstalledPrinters.Item(0)
                lstrImpresora = lDs.Tables(0).Rows(0).Item("impr_dire")

                lstrRpt = "/" & ConfigurationManager.AppSettings("conRepoDire").ToString() + "/" + pstrReporte

                Dim parameters(UBound(lvstrParamNom)) As ReportService.ParameterValue

                For i As Integer = 0 To UBound(lvstrParamNom)
                    parameters(i) = New ReportService.ParameterValue
                    parameters(i).Name = lvstrParamNom(i)
                    parameters(i).Value = lvstrParamVal(i)
                Next

                lbooRet = lrptPrint.PrintReport(lstrImpresora, lstrRpt, parameters, System.Net.CredentialCache.DefaultCredentials)

                Return (Math.Abs(CInt(lbooRet)))

            Catch ex As Exception
                clsError.gManejarError(ex)
                Return (0)
            End Try
        End Function
    End Class

End Namespace
