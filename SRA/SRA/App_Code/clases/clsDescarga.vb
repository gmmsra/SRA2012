Imports System.IO


Namespace SRA


Public Class clsDescarga
   Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState

   Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
      Get
         Return True
      End Get
   End Property

   Public Sub ProcessRequest(ByVal pContext As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
      Dim strFileName As String
      Dim ldsDatos As DataSet
      Dim lstrId As String
      Dim lstrDocuId As String
            Dim lstrPath As String = Nothing
      Dim lstrCarpeta As String
      Dim lstrTabla As String

      Try
         strFileName = pContext.Request("archivo")
         If strFileName Is Nothing Then
            lstrId = pContext.Request("id")
            lstrTabla = pContext.Request("tabla")
            ldsDatos = clsSQLServer.gObtenerEstruc(pContext.Session("sConn").ToString(), lstrTabla, lstrId)

            Select Case lstrTabla
               Case "clientes_docum"
                  With ldsDatos.Tables(0).Rows(0)
                     lstrDocuId = .Item("cldo_clie_id")
                     lstrPath = .Item("cldo_path")
                  End With
                  lstrCarpeta = clsSQLServer.gParametroValorConsul((pContext.Session("sConn").ToString()), "para_clie_docu_path")
                  strFileName = pContext.Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrDocuId + "\" + lstrPath
               Case "rg_productos_inscrip_docum"
                  With ldsDatos.Tables(0).Rows(0)
                     lstrDocuId = .Item("pido_id")
                     lstrPath = .Item("pido_docu_path")
                  End With
                  lstrCarpeta = clsSQLServer.gParametroValorConsul((pContext.Session("sConn").ToString()), "para_prdt_docu_path")
                  strFileName = pContext.Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrDocuId + "_" + lstrTabla + "_" + lstrPath
               Case "tramites_docum"
                  With ldsDatos.Tables(0).Rows(0)
                     lstrDocuId = .Item("trdo_id")
                     lstrPath = .Item("trdo_path")
                  End With
                  lstrCarpeta = clsSQLServer.gParametroValorConsul((pContext.Session("sConn").ToString()), "para_tram_docu_path")
                  strFileName = pContext.Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrDocuId + "_" + lstrPath
               Case "comisiones_docum"
                  With ldsDatos.Tables(0).Rows(0)
                     lstrDocuId = .Item("cdoc_id")
                     lstrPath = .Item("cdoc_path")
                  End With
                  lstrCarpeta = clsSQLServer.gParametroValorConsul((pContext.Session("sConn").ToString()), "para_craz_docu_path")
                  strFileName = pContext.Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrDocuId + "_" + lstrTabla + "_" + lstrPath
               Case "comisiones_agenda"
                  With ldsDatos.Tables(0).Rows(0)
                     lstrDocuId = .Item("cage_id")
                     lstrPath = .Item("cage_path")
                  End With
                  lstrCarpeta = clsSQLServer.gParametroValorConsul((pContext.Session("sConn").ToString()), "para_craz_docu_path")
                  strFileName = pContext.Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrDocuId + "_" + lstrTabla + "_" + lstrPath
            End Select
         Else
            lstrPath = strFileName.Substring(strFileName.LastIndexOf("\") + 1)
            strFileName = pContext.Server.MapPath("") + "\" + strFileName
         End If

         Dim myFile As New FileInfo(strFileName)
         Dim fs As FileStream
         fs = File.Open(myFile.FullName, FileMode.Open, FileAccess.Read)

         Dim lBuff(fs.Length - 1) As Byte
         fs.Read(lBuff, 0, fs.Length)
         pContext.Response.AppendHeader("Content-Disposition", "attachment; filename=" + lstrPath)
         pContext.Response.ContentType = "application/octet-stream"
         pContext.Response.BinaryWrite(lBuff)
         fs.Close()

         pContext.Response.Flush()
         pContext.Response.End()

      Catch ex As Exception
         clsError.gManejarError(ex)
      End Try
   End Sub
End Class

End Namespace

