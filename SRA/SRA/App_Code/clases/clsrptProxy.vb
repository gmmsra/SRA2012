Imports System.Net
Imports System.Text
Imports System.IO


Namespace SRA


    Public Class clsRPTPROXY
        Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState

        Private mAddress As String

        Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
            Get
                Return True
            End Get
        End Property

        Public Sub ProcessRequest(ByVal pContext As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
            Dim sDirRep As String = Nothing
            Dim sProxy As String = Nothing
            Dim sRepSvr As String = Nothing

            Try
                sRepSvr = clsReportingServices.clsReportingProxy.gReportServer()

                sDirRep = "http://" + sRepSvr + "/ReportServer?"
                sDirRep += pContext.Request.ServerVariables("QUERY_STRING").ToString()

                'sProxy = Request.ServerVariables("SERVER_NAME").ToString()

                If (pContext.Request.Headers.GetValues("Referer") Is Nothing) Then
                    If pContext.Session("Server") Is Nothing Then
                        If IsArray(pContext.Request.Headers.GetValues("host")) AndAlso pContext.Request.Headers.GetValues("host").GetUpperBound(0) > -1 Then
                            sProxy = pContext.Request.Headers.GetValues("host")(0).ToString
                        Else
                            sProxy = pContext.Request.Headers.GetValues("host").ToString
                        End If
                        pContext.Session("Server") = sProxy
                    Else
                        sProxy = pContext.Session("Server").ToString()
                    End If
                Else
                    sProxy = pContext.Request.Headers.GetValues("Referer")(0).ToString()
                    sProxy = sProxy.Replace("\\", "/")
                    sProxy = sProxy.Substring(7, sProxy.Substring(7).IndexOf("/"))
                    pContext.Session("Server") = sProxy
                End If

                sProxy += pContext.Request.ApplicationPath
                sProxy += "/rptproxy.aspx"

                Dim lstrRefe As String = Nothing

                If sDirRep.IndexOf("Command=Render") > 0 Then
                    If Not (pContext.Request.ServerVariables("http_referer") Is Nothing) Then
                        lstrRefe = pContext.Request.ServerVariables("http_referer")
                        If lstrRefe.IndexOf("rptproxy") < 1 Then
                            pContext.Session("RsRefe") = lstrRefe
                        Else
                            lstrRefe = ""
                        End If
                    End If

                    If lstrRefe = "" AndAlso Not pContext.Session("RsRefe") Is Nothing Then
                        lstrRefe = pContext.Session("RsRefe")
                    End If
                End If

                ObtenerString(sDirRep, sRepSvr, sProxy, pContext, lstrRefe)

            Catch ex As System.Threading.ThreadAbortException

            Catch ex As Exception
                If mAddress <> "" Then
                    mEscribir(mAddress)
                End If

                clsError.gManejarError(ex)
            End Try
        End Sub

        Private Sub ObtenerString(ByVal lcUrl As String, ByVal sRepSvr As String, ByVal sProxy As String, ByVal pContext As System.Web.HttpContext, ByVal pstrPagBack As String)
            Dim loHttp As HttpWebRequest = WebRequest.Create(lcUrl)
            Dim lbooRS2000 As Boolean = False
            Dim lstrUbiBack As String = ""

            If ConfigurationManager.AppSettings("conVersionRS") = "2000" Then
                lbooRS2000 = True
                lstrUbiBack = "conRptUbiBack2000"
            Else
                lstrUbiBack = "conRptUbiBack"
            End If

            'loHttp.Timeout = 10000     
            loHttp.UserAgent = pContext.Request.Headers.GetValues("User-Agent")(0)
            loHttp.Headers.Add("Accept-Language:es")
            loHttp.Headers.Add("Accept-Encoding: gzip, deflate")
            'loHttp.Referer = Request.Headers.GetValues("Referer")(0)
            loHttp.KeepAlive = False

            'loHttp.Credentials = new NetworkCredential("usuario","password","dominio")

            For i As Integer = 0 To pContext.Request.Cookies.Count - 1
                If (pContext.Request.Cookies(i).Name.Replace("%2f", "/") = pContext.Request.QueryString(0)) Then
                    loHttp.Headers.Add("Cookie: " + pContext.Request.Cookies(i).Name + "=" + pContext.Request.Cookies(i).Value)
                End If
                'if(Request.Cookies.Count>0 && Request.Cookies(Request.Cookies.Count-1).Name.IndexOf("ASP")==-1) loHttp.Headers.Add("Cookie: " + Request.Cookies(Request.Cookies.Count-1).Name + "=" + Request.Cookies(Request.Cookies.Count-1).Value)
            Next

            If Not ConfigurationManager.AppSettings("conRptUsr") Is Nothing Then
                Dim credential As NetworkCredential

                loHttp.PreAuthenticate = True
                credential = New NetworkCredential(ConfigurationManager.AppSettings("conRptUsr").ToString, ConfigurationManager.AppSettings("conRptPass").ToString, ConfigurationManager.AppSettings("conRptDom").ToString)
                loHttp.Credentials = credential
            End If

            mAddress = loHttp.Address.ToString

            'mEscribir(loHttp.Address.ToString)
            Dim loWebResponse As HttpWebResponse = loHttp.GetResponse()
            Dim oStream As Stream = loWebResponse.GetResponseStream()

            pContext.Response.Clear()
            pContext.Response.ContentType = loWebResponse.ContentType
            'Response.AddHeader("Referer",Request.Headers.GetValues("Referer")(0))

            If Not loWebResponse.Headers.GetValues("Set-Cookie") Is Nothing Then
                pContext.Response.AddHeader("Set-Cookie", loWebResponse.Headers.GetValues("Set-Cookie")(0) + "; path=/")
            End If

            If Not loWebResponse.Headers.GetValues("Content-Disposition") Is Nothing Then
                pContext.Response.AddHeader("Content-Disposition", loWebResponse.Headers.GetValues("Content-Disposition")(0))
            End If

            If pContext.Response.ContentType.ToString = "image/emf" Then
                pContext.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            End If

            'For Each lstrval As String In loWebResponse.Headers.AllKeys
            '   If lstrval <> "Transfer-Encoding" Then
            '      If lstrval = "Set-Cookie" Then
            '         pContext.Response.AddHeader("Set-Cookie", loWebResponse.Headers.GetValues("Set-Cookie")(0) + "; path=/")
            '      Else
            '         pContext.Response.AddHeader(lstrval, loWebResponse.Headers.GetValues(lstrval)(0))
            '      End If
            '   End If
            'Next


            If ((pContext.Response.ContentType = "") Or (pContext.Response.ContentType.IndexOf("text/") <> -1)) Then
                Dim enc As Encoding = Encoding.GetEncoding("UTF-8")
                'Encoding enc= Encoding.GetEncoding ("Windows-1251")

                Dim loResponseStream As StreamReader = New StreamReader(oStream, enc)
                Dim lcHtml As String = loResponseStream.ReadToEnd()
                loResponseStream.Close()

                lcHtml = lcHtml.Replace(sRepSvr + "/ReportServer", sProxy)
                lcHtml = lcHtml.Replace("""?rs", """" + "http://" + sProxy + "?rs")

                If pstrPagBack <> "" Then
                    Dim [oF] As New System.IO.StreamReader(ConfigurationManager.AppSettings(lstrUbiBack).ToString(), System.Text.Encoding.UTF8)
                    Dim lstrBack As String = [oF].ReadToEnd
                    [oF].Close()
                    If lbooRS2000 Then
                        'reporting services 2000
                        lcHtml = lcHtml.Replace("<div class=""ToolbarHelp WidgetSet"">", lstrBack.Replace("pagBack", pstrPagBack))
                    Else
                        'reporting services 2005
                        lcHtml = lcHtml.Replace("<td class=""WidgetSetSpacer ToolbarRefresh""></td>", lstrBack.Replace("pagBack", pstrPagBack))
                    End If
                End If

                pContext.Response.Write(lcHtml)
                oStream.Close()
                loWebResponse.Close()
                loResponseStream.Close()
                pContext.Response.End()
            Else
                If loHttp.Address.ToString.IndexOf("rsclientprint.cab") > 0 Then
                    Dim fs As FileStream
                    fs = File.Open(ConfigurationManager.AppSettings("conUbiActiveX").ToString(), FileMode.Open, FileAccess.Read)
                    Dim lBuff(fs.Length) As Byte
                    fs.Read(lBuff, 0, fs.Length)
                    pContext.Response.ContentType = "application/octet-stream"
                    pContext.Response.AddHeader("Content-disposition", "attachment; filename=ReportServer.cab")
                    pContext.Response.BinaryWrite(lBuff)
                    fs.Close()
                Else
                    Dim lSize As Long = loWebResponse.ContentLength
                    'Dim lInd As Integer = pContext.Request.RawUrl.LastIndexOf(".00")
                    'Dim sNomArch = pContext.Request.RawUrl.Substring(lInd)
                    'pContext.Response.AddHeader("Content-disposition", "attachment; filename=" & sNomArch)
                    If lSize = -1 Then
                        lSize = 65617
                    End If

                    Dim lBuff(lSize - 1) As Byte
                    Dim i As Integer
                    Dim iByte As Integer
                    iByte = oStream.ReadByte
                    While iByte <> -1
                        If i > lBuff.GetUpperBound(0) Then
                            ReDim Preserve lBuff(i + 65617)
                        End If
                        lBuff(i) = iByte
                        i += 1
                        iByte = oStream.ReadByte
                    End While

                    If i < lBuff.GetUpperBound(0) Then
                        ReDim Preserve lBuff(i)
                    End If

                    'oStream.Read(lBuff, 0, lBuff.GetUpperBound(0))
                    pContext.Response.BinaryWrite(lBuff)
                    'Else
                    'Dim lBuff(0) As Byte
                    'Dim i As Integer
                    'Dim iByte As Integer
                    'iByte = oStream.ReadByte
                    'While iByte <> -1
                    '   ReDim lBuff(i)
                    '   lBuff(i) = iByte
                    '   i += 1
                    '   iByte = oStream.ReadByte
                    'End While
                    'End If

                    'Dim fs As FileStream
                    'fs = File.Open("C:\prueba.txt", FileMode.Create, FileAccess.Write)
                    'fs.Write(lBuff, 0, lSize)
                    'fs.Close()
                End If

                oStream.Close()
                loWebResponse.Close()
                pContext.Response.End()
            End If
        End Sub

        Private Sub mEscribir(ByVal pDatos As String)
            Dim lstrArchLog As String = ConfigurationManager.AppSettings("conArchLog").ToString
            Dim fs As New FileStream(lstrArchLog, FileMode.OpenOrCreate, FileAccess.ReadWrite)

            Dim w = New StreamWriter(fs)
            w.BaseStream.Seek(0, SeekOrigin.End)

            w.Write(clsFormatear.CrLf() & "Fecha: " & DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") & clsFormatear.CrLf())
            w.Write(pDatos & clsFormatear.CrLf())
            w.Write("------------------------------------------------------------------------" & clsFormatear.CrLf())

            w.Flush()
            w.Close()
        End Sub
    End Class
End Namespace
