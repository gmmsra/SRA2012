﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Entities
Imports System

' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
<System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
 Public Class TransferenciaProductos
    Inherits System.Web.Services.WebService

    <WebMethod(True)> _
    Public Function ValidarTramite(ByVal cria_nume As Integer, ByVal tram_vc_tran_tipo As String, ByVal porcentaje As Decimal, ByVal totalPorcentaje As Decimal, ByVal tram_vc_id As Integer, ByVal porcentajeRow As Decimal) As String

        Dim obj As New List(Of TramitesVendedoresCompradoresEntity)
        Dim msg As String = String.Empty

        Select Case tram_vc_tran_tipo
            Case "V"
                obj = Session("objVendedores")
                If Not IsDBNull(obj) Then
                    If (obj.Where(Function(x) x.cria_nume = cria_nume And x.tram_vc_id <> tram_vc_id).Count > 0) Then
                        msg = "Ya existe Nº Prop " + cria_nume.ToString
                        Return msg
                    End If
                End If
                If controlPorcentaje(obj, cria_nume, porcentaje, totalPorcentaje, porcentajeRow) = False Then
                    msg = "La suma de porcentajes ha superado el 100%"
                End If

            Case "C"
                obj = Session("objCompradores")
                If Not IsDBNull(obj) Then
                    If (obj.Where(Function(x) x.cria_nume = cria_nume And x.tram_vc_id <> tram_vc_id).Count > 0) Then
                        msg = "Ya existe Nº Prop " + cria_nume.ToString
                        Return msg
                    End If
                End If
                If controlPorcentaje(obj, cria_nume, porcentaje, totalPorcentaje, porcentajeRow) = False Then
                    msg = "La suma de porcentajes ha superado el 100%"
                End If
        End Select

        Return msg

    End Function

    Private Function controlPorcentaje(ByVal obj As List(Of TramitesVendedoresCompradoresEntity), ByVal cria_nume As Integer, ByVal porcentaje As Decimal, ByVal totalPorcentaje As Decimal, ByVal porcentajeRow As Decimal) As Boolean
        Dim porc = totalPorcentaje - porcentajeRow
        porc += porcentaje
        If (porc > 100) Then
            Return False
        End If
        Return True
    End Function

End Class