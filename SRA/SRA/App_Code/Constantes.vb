Namespace SRA

Public Module Constantes
   Public Enum Opciones As Integer
      'Menu de Clientes
      'Administracion de Clientes
      Clientes = 2002
      Clientes_Alta = 8092
      Clientes_Baja = 8093
      Clientes_Modificacion = 8091
      'Agrupaciones 
      Agrupaciones = 2001
      Agrupaciones_Alta = 8094
      Agrupaciones_Baja = 8095
      Agrupaciones_Modificacion = 8096
      'Datos por Actividad
      DatosActividad = 2002
      DatosActividad_Alta = 8092
      DatosActividad_Baja = 8093
      DatosActividad_Modificacion = 8091
      'Socios
      Socios = 5002
      Socios_Modificacion = 8097
      'Alumnos ISEA
      Alumnos_Alta_ISEA = 8098
      Alumnos_Baja_ISEA = 8099
      Alumnos_Modificacion_ISEA = 8100
      'Alumnos CEIDA
      Alumnos_Alta_CEIDA = 8101
      Alumnos_Baja_CEIDA = 8102
      Alumnos_Modificacion_CEIDA = 8103
      'Alumnos EGEA
      Alumnos_Alta_EGEA = 8104
      Alumnos_Baja_EGEA = 8105
      Alumnos_Modificacion_EGEA = 8106
      'Cotizaciones
      Cotizaciones = 6615
      Cotizaciones_Alta = 8107
      Cotizaciones_Baja = 8108
      Cotizaciones_Modificacion = 8109
      ProcesamientoLote_Procesar = 8111


      Perfiles = 1
      Perfiles_Alta = 2
      Perfiles_Baja = 3
      Perfiles_Modificación = 4
      Permisos = 5
      Permisos_Alta = 6
      Permisos_Baja = 7
      Usuarios = 8
      Usuarios_Alta = 9
      Usuarios_Baja = 10
      Usuarios_Modificación = 11
      Tarifas = 12
      Tarifas_Alta = 13
      Tarifas_Baja = 14
      Tarifas_Modificacion = 15
      Clientes_Tele = 20
      Clientes_Tele_Alta = 21
      Clientes_Tele_Baja = 22
      Clientes_Tele_Modificacion = 23
      Clientes_Dire = 24
      Clientes_Dire_Alta = 25
      Clientes_Dire_Baja = 26
      Clientes_Dire_Modificacion = 27
      Clientes_Mail = 28
      Clientes_Mail_Alta = 29
      Clientes_Mail_Baja = 30
      Clientes_Mail_Modificacion = 31
      Clientes_Docu = 32
      Clientes_Docu_Alta = 33
      Clientes_Docu_Baja = 34
      Clientes_Docu_Modificacion = 35
      Facturacion = 6101
      Cobranzas = 6201
        Arqueos_Abrir = 8195
        Arqueos_Cerrar = 8196

            Empresas

            Empresas_Alta

            Empresas_Baja

            Empresas_Modificación

            Empresas_Alcance

    End Enum
End Module
End Namespace
