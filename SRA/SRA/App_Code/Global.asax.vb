Imports System.Web
Imports System.Web.SessionState
Imports System


Namespace SRA

'using System.IO;
'using System;
'using System.Text;

Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
      'Atrapo la excepcion que no fue atrapada por un  Try/Catch y genero el error para escribirlo en el log
      Dim ex As Exception = Server.GetLastError.GetBaseException
      clsError.gManejarError(ex)

      'Esta linea se usa para cuando quiero limpiar el error ocurrido y seguir con la aplicacion
      'Server.ClearError()
   End Sub

   Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
      ' Fires when the session ends
   End Sub

   Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
      ' Fires when the application ends
   End Sub

End Class

End Namespace
