'===========================================================================
' Este archivo se gener� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' Se cre� este archivo de c�digo 'App_Code\Migrated\Stub_Productos_aspx_vb.vb' que contiene la clase abstracta 
' utilizada como clase base para la clase 'Migrated_Productos' en el archivo 'Productos.aspx.vb'.
' De esta forma, todos los archivos de c�digo del proyecto pueden hacer referencia a la clase base.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================


Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports SRA_Neg.Utiles


Namespace SRA


MustInherit Public Class Productos
    Inherits FormGenerico
    Public mstrProdCtrl As String
    Public mbooActi As Boolean
    Public mintProce As Integer
    Public mintPdinId As Integer
    Public MustOverride Sub mInicializar()

    Public MustOverride Sub mInicializarControl()

    Public MustOverride Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mConsultar(ByVal pbooVisi As Boolean)

    Public MustOverride Sub mSeleccionarProducto(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mCargarDatos(ByVal pstrId As String)

    Public MustOverride Sub grdAna_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub BloquearBotonCerficado(ByVal boolEstado As Boolean)

    Public MustOverride Sub BloquearBotones(ByVal boolEstado As Boolean)

    Public MustOverride Sub DataGridObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub DataGridDocum_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub DataGridAsociacion_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mCrearDataSet(ByVal pstrId As String)

    Public MustOverride Sub mEditarDatosAnalisis(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosAsociacion(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosDocum(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public Shared Function ObtenerSql( _
       ByVal pstrUserId As String, _
       ByVal pintBusc As Integer, _
       ByVal pstrCriaId As String, _
       ByVal pintBaja As Integer, _
       ByVal pstrCriaCtrl As String, _
       ByVal pstrNomb As String, _
       ByVal pstrRaza As String, _
       ByVal pstrSraNume As String, _
       ByVal pstrRP As String, _
       ByVal pstrSexo As String, _
       ByVal pstrNaciDesde As String, _
       ByVal pstrNaciHasta As String, _
       ByVal pstrAsoc As String, _
       ByVal pstrAsocNume As String, _
       ByVal pstrLaboNume As String, _
       ByVal pstrMadre As String, _
       ByVal pstrPadre As String, _
       ByVal pstrRece As String, _
       ByVal pstrObservacion As String, _
       ByVal pstrSraNumDesde As String, _
       ByVal pstrSraNumHasta As String, _
       ByVal pstrApodo As String, _
       ByVal pintInscrip As Integer, _
       Optional ByVal pstrCuig As String = "", _
       Optional ByVal pstrTram As String = "", _
       Optional ByVal pstrInscDesde As String = "", _
       Optional ByVal pstrInscHasta As String = "", _
       Optional ByVal pstrRPDesde As String = "", _
       Optional ByVal pstrRPHasta As String = "", _
       Optional ByVal pstrPropClie As String = "", _
       Optional ByVal pstrPropCria As String = "", _
       Optional ByVal pstrEsta As String = "", _
       Optional ByVal pstrNacio As String = "", _
       Optional ByVal mbooInclDeshab As Boolean = False, _
       Optional ByVal pstrRegiTipo As Integer = 0, _
       Optional ByVal pstrOrdenarPor As Integer = 0, _
       Optional ByVal pstrCondicionalDesde As String = "", _
       Optional ByVal pstrCondicionalHasta As String = "", _
       Optional ByVal pstrRpExtranjero As String = "", _
       Optional ByVal pboolAscendente As Boolean = True, _
       Optional ByVal pstrDadorNroSra As String = "", _
       Optional ByVal pstrDadorNroSenasa As String = "", _
        Optional ByVal pstrCastrado As String = "", _
         Optional ByVal pstrDescorne As String = "" _
) As String

        Dim lstrCmd As New StringBuilder
        'GSZ 20/04/2015  se agrego Castrado  y descorne
        'esta funci�n se llama tambi�n desde clsXMLHTTP
        lstrCmd.Append("exec productos_busq ")
        lstrCmd.Append(" @buscar_en=" + pintBusc.ToString)
        lstrCmd.Append(" , @prdt_cria_id=" + clsSQLServer.gFormatArg(pstrCriaId, SqlDbType.Int))
        If mbooInclDeshab Then
            lstrCmd.Append(" , @incluir_bajas = 1")
        Else
            lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)
        End If
        pstrNomb = Replace(pstrNomb, "'", "''")
        pstrNomb = Replace(pstrNomb, "~", "''")

        lstrCmd.Append(" , @prdt_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
        lstrCmd.Append(" , @prdt_apodo=" + clsSQLServer.gFormatArg(pstrApodo, SqlDbType.VarChar))
        lstrCmd.Append(" , @prdt_sra_nume=" + clsSQLServer.gFormatArg(pstrSraNume, SqlDbType.VarChar))
        lstrCmd.Append(" , @prdt_rp=" + clsSQLServer.gFormatArg(pstrRP, SqlDbType.VarChar))
        lstrCmd.Append(" , @prdt_cuig=" + clsSQLServer.gFormatArg(pstrCuig, SqlDbType.VarChar))
        lstrCmd.Append(" , @rp_desde=" + clsSQLServer.gFormatArg(pstrRPDesde, SqlDbType.VarChar))
        lstrCmd.Append(" , @rp_hasta=" + clsSQLServer.gFormatArg(pstrRPHasta, SqlDbType.VarChar))
        lstrCmd.Append(" , @prop_clie_id=" + clsSQLServer.gFormatArg(pstrPropClie, SqlDbType.Int))
        lstrCmd.Append(" , @prop_cria_id=" + clsSQLServer.gFormatArg(pstrPropCria, SqlDbType.Int))
        lstrCmd.Append(" , @esta_id=" + clsSQLServer.gFormatArg(pstrEsta, SqlDbType.Int))
        If pstrNacio <> "T" Then
            lstrCmd.Append(" , @prdt_ndad=" + clsSQLServer.gFormatArg(pstrNacio, SqlDbType.VarChar))
        End If
        If pstrSexo = "" Then
            lstrCmd.Append(" , @prdt_sexo=null")
        Else
            If pstrSexo.IndexOf(",") <> -1 Then
                pstrSexo = pstrSexo.Substring(pstrSexo.IndexOf(",") + 1)
            End If
            lstrCmd.Append(" , @prdt_sexo=" + clsSQLServer.gFormatArg(pstrSexo, SqlDbType.Int))
        End If

        lstrCmd.Append(" , @prdt_naci_fecha_desde=" + clsSQLServer.gFormatArg(pstrNaciDesde, SqlDbType.SmallDateTime))
        lstrCmd.Append(" , @prdt_naci_fecha_hasta=" + clsSQLServer.gFormatArg(pstrNaciHasta, SqlDbType.SmallDateTime))
        lstrCmd.Append(" , @prdt_insc_fecha_desde=" + clsSQLServer.gFormatArg(pstrInscDesde, SqlDbType.SmallDateTime))
        lstrCmd.Append(" , @prdt_insc_fecha_hasta=" + clsSQLServer.gFormatArg(pstrInscHasta, SqlDbType.SmallDateTime))
        lstrCmd.Append(" , @prdt_asoc_id=" + clsSQLServer.gFormatArg(pstrAsoc, SqlDbType.Int))
        lstrCmd.Append(" , @prdt_asoc_nume=" + clsSQLServer.gFormatArg(pstrAsocNume, SqlDbType.VarChar))
        lstrCmd.Append(" , @prta_nume=" + clsSQLServer.gFormatArg(pstrLaboNume, SqlDbType.Int))
        lstrCmd.Append(" , @madre_prdt_id=" + clsSQLServer.gFormatArg(pstrMadre, SqlDbType.Int))
        lstrCmd.Append(" , @padre_prdt_id=" + clsSQLServer.gFormatArg(pstrPadre, SqlDbType.Int))
        lstrCmd.Append(" , @rece_prdt_id=" + clsSQLServer.gFormatArg(pstrRece, SqlDbType.Int))
        lstrCmd.Append(" , @prdt_raza_id=" + clsSQLServer.gFormatArg(pstrRaza, SqlDbType.Int))
        lstrCmd.Append(" , @prdo_refe=" + clsSQLServer.gFormatArg(pstrObservacion, SqlDbType.VarChar))
        lstrCmd.Append(" , @inscrip = " + pintInscrip.ToString)
        lstrCmd.Append(" , @tram_nume=" + clsSQLServer.gFormatArg(pstrTram, SqlDbType.Int))
        lstrCmd.Append(" , @prdt_sra_nume_desde=" + clsSQLServer.gFormatArg(pstrSraNumDesde, SqlDbType.Int))
        lstrCmd.Append(" , @prdt_sra_nume_hasta=" + clsSQLServer.gFormatArg(pstrSraNumHasta, SqlDbType.Int))
        lstrCmd.Append(" , @prdt_regt_id=" + clsSQLServer.gFormatArg(pstrRegiTipo, SqlDbType.Int))
        lstrCmd.Append(" , @ordenar_por=" + clsSQLServer.gFormatArg(pstrOrdenarPor, SqlDbType.Int)) '0: HBA / 1: RP / 2: Criador.
        lstrCmd.Append(" , @prdt_condicional_desde=" + clsSQLServer.gFormatArg(pstrCondicionalDesde, SqlDbType.Int)) 'Condicional Desde.
        lstrCmd.Append(" , @prdt_condicional_hasta=" + clsSQLServer.gFormatArg(pstrCondicionalHasta, SqlDbType.Int)) 'Condicional Hasta.
        lstrCmd.Append(" , @prdt_rp_extr=" + clsSQLServer.gFormatArg(pstrRpExtranjero, SqlDbType.VarChar)) 'RP Extranjero.
        lstrCmd.Append(" , @Ascendente=" + clsSQLServer.gFormatArg(pboolAscendente, SqlDbType.Bit)) 'Ordena por: Ascendente / Descendente.
        lstrCmd.Append(" , @prdt_dona_nume=" + clsSQLServer.gFormatArg(pstrDadorNroSra, SqlDbType.Int)) 'Nro.Dador SRA
        lstrCmd.Append(" , @prdt_dona_sena_nume=" + clsSQLServer.gFormatArg(pstrDadorNroSenasa, SqlDbType.VarChar)) 'Nro.Dador Senansa


        If pstrCastrado.ToLower() = "(todos)" Then
            lstrCmd.Append(" , @prdt_castrado=null")
        Else
            If pstrCastrado.IndexOf(",") <> -1 Then
                pstrCastrado = pstrCastrado.Substring(pstrCastrado.IndexOf(",") + 1)
            End If
            lstrCmd.Append(" , @prdt_castrado=" + clsSQLServer.gFormatArg(pstrCastrado, SqlDbType.Int))
        End If

        If pstrDescorne.ToLower() = "(todos)" Then
            lstrCmd.Append(" , @prdt_descorne=null")
        Else
            If pstrDescorne.IndexOf(",") <> -1 Then
                pstrDescorne = pstrDescorne.Substring(pstrDescorne.IndexOf(",") + 1)
            End If
            lstrCmd.Append(" , @prdt_descorne=" + clsSQLServer.gFormatArg(pstrDescorne, SqlDbType.Int))
        End If




        Return (lstrCmd.ToString)
    End Function


End Class


End Namespace