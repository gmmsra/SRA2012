'===========================================================================
' Este archivo se gener� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' Se cre� este archivo de c�digo 'App_Code\Migrated\Stub_alumnos_aspx_vb.vb' que contiene la clase abstracta 
' utilizada como clase base para la clase 'Migrated_alumnos' en el archivo 'alumnos.aspx.vb'.
' De esta forma, todos los archivos de c�digo del proyecto pueden hacer referencia a la clase base.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================




Namespace SRA


MustInherit Public Class alumnos
    Inherits FormGenerico
    Public MustOverride Sub mInicializarControl()

    Public MustOverride Sub mCargarDatos(ByVal pstrId As String)

    Public MustOverride Sub mCrearDataSet(ByVal pstrId As String)

    Public MustOverride Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mSeleccionar(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mRetorno(ByVal pstrLega As String)

    Public MustOverride Sub mConsultar()

    Public Shared Function ObtenerSql(ByVal pstrValorId As String, ByVal pstrApel As String, ByVal pstrClieId As String, ByVal pstrLega As String, ByVal pstrCuit As String, ByVal pstrDocuTipo As String, ByVal pstrDocuNume As String, ByVal pstrInse As String, ByVal pstrClieDerivCtrl As String) As String
        Dim lstrCmd As New StringBuilder

        lstrCmd.Append("exec alumnos_consul")

        If pstrValorId <> "" Then
            lstrCmd.Append(" @alum_id=" + pstrValorId)
        Else

            lstrCmd.Append(" @alum_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
            lstrCmd.Append(", @alum_clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
            lstrCmd.Append(", @alum_lega=" + clsSQLServer.gFormatArg(pstrLega, SqlDbType.Int))
            lstrCmd.Append(", @alum_inse_id=" + clsSQLServer.gFormatArg(pstrInse, SqlDbType.Int))
            lstrCmd.Append(", @clie_cuit=" + clsSQLServer.gFormatArg(pstrCuit, SqlDbType.Int))
            lstrCmd.Append(", @clie_doti_id=" + clsSQLServer.gFormatArg(pstrDocuTipo, SqlDbType.Int))
            lstrCmd.Append(", @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNume, SqlDbType.Int))

            If pstrClieDerivCtrl <> "" Then
                lstrCmd.Append(", @alum_esta_id=" + CStr(SRA_Neg.Constantes.Estados.Alumnos_Vigente))
            End If
        End If

        Return (lstrCmd.ToString)
    End Function
    Public MustOverride Sub mEditarDatosEstu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosOcup(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosExpl(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosFunc(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub grdEstu_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub grdOcup_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub grdFunc_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub grdExpl_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)



End Class


End Namespace