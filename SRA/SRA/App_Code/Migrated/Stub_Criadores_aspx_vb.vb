'===========================================================================
' Este archivo se gener� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' Se cre� este archivo de c�digo 'App_Code\Migrated\Stub_Criadores_aspx_vb.vb' que contiene la clase abstracta 
' utilizada como clase base para la clase 'Migrated_Criadores' en el archivo 'Criadores.aspx.vb'.
' De esta forma, todos los archivos de c�digo del proyecto pueden hacer referencia a la clase base.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================




Namespace SRA


MustInherit Public Class Criadores
    Inherits FormGenerico
    Public mstrCriaCtrl As String
    Public mbooActi As Boolean
    Public mintActiDefa As Integer
    Public MustOverride Sub mInicializar()

    Public MustOverride Sub mInicializarControl()

    Public MustOverride Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mValidarConsulta()

    Public MustOverride Sub mConsultar()

    Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pstrClieId As String, _
                                      ByVal pintBaja As Integer, ByVal pstrCriaCtrl As String, ByVal pstrApel As String, _
                                      ByVal pstrNomb As String, _
                                      ByVal pstrSociNume As String, ByVal pstrFilCriaNume As String, ByVal pstrRazaFil As String, _
                                      ByVal pstrCriaNumeFil As String, ByVal pstrFilRepreCriaNume As String, ByVal pstrRepreRazaFil As String, _
                                      ByVal pstrRepreCriaNumeFil As String, ByVal pstrFilCuit As String, ByVal pstrCuitFil As String, _
                                      ByVal pstrFilDocu As String, ByVal pstrDocuTipoFil As String, ByVal pstrDocuNumeFil As String, _
                                      ByVal pstrFilClaveUnica As String, ByVal pstrCunicaFil As String, _
                                      ByVal pstrFilRespa As String, ByVal pstrRespa As String, _
                                      ByVal pstrFilPais As String, ByVal pstrFilprov As String, _
                                      ByVal pintBuscFil As Integer, ByVal pstrEstab As String) As String


        Dim lstrCmd As New StringBuilder

        'esta funci�n se llama tambi�n desde clsXMLHTTP
        lstrCmd.Append("exec criadores_busq")
        lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
        lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
        lstrCmd.Append(" , @clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
        lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)

        lstrCmd.Append(" , @clie_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
        lstrCmd.Append(" , @clie_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
        lstrCmd.Append(" , @clie_soci_nume=" + clsSQLServer.gFormatArg(pstrSociNume, SqlDbType.Int))

        If pstrFilCriaNume = "1" Then
            lstrCmd.Append(" , @clie_raza=" + clsSQLServer.gFormatArg(pstrRazaFil, SqlDbType.Int))
            lstrCmd.Append(" , @clie_cria_nume=" + clsSQLServer.gFormatArg(pstrCriaNumeFil, SqlDbType.Int))
        End If
        If pstrFilRepreCriaNume = "1" Then
            lstrCmd.Append(" , @repre_clie_raza=" + clsSQLServer.gFormatArg(pstrRepreRazaFil, SqlDbType.Int))
            lstrCmd.Append(" , @repre_clie_cria_nume=" + clsSQLServer.gFormatArg(pstrRepreCriaNumeFil, SqlDbType.Int))
        End If
        If pstrFilCuit = "1" And pstrCuitFil <> "" Then
            lstrCmd.Append(" , @clie_cuit=" + clsSQLServer.gFormatArg(pstrCuitFil, SqlDbType.Int))
        End If
        If pstrFilRespa = "1" And pstrRespa <> "" Then
            lstrCmd.Append(" , @clie_respa=" + clsSQLServer.gFormatArg(pstrRespa, SqlDbType.VarChar))
        End If
        If pstrFilDocu = "1" Then
            lstrCmd.Append(" , @clie_docu_tipo=" + clsSQLServer.gFormatArg(pstrDocuTipoFil, SqlDbType.Int))
            lstrCmd.Append(" , @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNumeFil, SqlDbType.Int))
        End If
        If pstrFilClaveUnica = "1" Then
            lstrCmd.Append(" , @clie_cunica=" + clsSQLServer.gFormatArg(pstrCunicaFil, SqlDbType.VarChar))
        End If

        lstrCmd.Append(" , @pais_id=" + clsSQLServer.gFormatArg(pstrFilPais, SqlDbType.Int))
        lstrCmd.Append(" , @prov_id=" + clsSQLServer.gFormatArg(pstrFilprov, SqlDbType.Int))
        lstrCmd.Append(" , @buscar_en_estab=" + pintBuscFil.ToString)
        lstrCmd.Append(" , @estab=" + clsSQLServer.gFormatArg(pstrEstab, SqlDbType.VarChar))

        Return (lstrCmd.ToString)
    End Function
    Public MustOverride Sub mSeleccionarCriador(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mCargarDatos(ByVal pstrId As String)

    Public MustOverride Sub mCrearDataSet(ByVal pstrId As String)



End Class


End Namespace