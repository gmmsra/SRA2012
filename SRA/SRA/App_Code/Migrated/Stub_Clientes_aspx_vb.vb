'===========================================================================
' Este archivo se gener� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' Se cre� este archivo de c�digo 'App_Code\Migrated\Stub_Clientes_aspx_vb.vb' que contiene la clase abstracta 
' utilizada como clase base para la clase 'Migrated_Clientes' en el archivo 'Clientes.aspx.vb'.
' De esta forma, todos los archivos de c�digo del proyecto pueden hacer referencia a la clase base.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================


Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports SRA_Neg.Utiles


Namespace SRA


MustInherit Public Class Clientes
    Inherits FormGenerico
    Public mstrClieCtrl As String
    Public mbooAgru As Boolean
    Public mbooActi As Boolean
    Public mintActiDefa As Integer
    Public MustOverride Sub mInicializar()

    Public MustOverride Sub mSetearCliente(ByVal pbooAgru As Boolean)

    Public MustOverride Sub mInicializarControl()

    Public MustOverride Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mValidarConsulta()

    Public MustOverride Sub mConsultar()

    Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pstrClieId As String, _
                ByVal pintBaja As Integer, ByVal pstrClieCtrl As String, ByVal pbooFilAgru As Boolean, _
                ByVal pbooSoloAgrupa As Boolean, ByVal pintInclAgru As Integer, ByVal pstrValorId As String, _
                ByVal pstrApel As String, ByVal pstrNomb As String, ByVal pstrSociNume As String, ByVal pstrFilCriaNume As String, _
                ByVal pstrRazaFil As String, ByVal pstrCriaNumeFil As String, ByVal pstrFilNoCriaNume As String, _
                ByVal pstrNoCriaNumeFil As String, ByVal pstrFilCuit As String, ByVal pstrCuitFil As String, _
                ByVal pstrFilDocu As String, ByVal pstrDocuTipoFil As String, ByVal pstrDocuNumeFil As String, _
                ByVal pstrFilTarj As String, ByVal pstrTarjTipoFil As String, ByVal pstrTarjNumeFil As String, _
                ByVal pstrFilEnti As String, ByVal pstrEntiFil As String, ByVal pstrFilMedioPago As String, _
                ByVal pstrPagoFil As String, ByVal pstrFilEmpr As String, ByVal pstrEmprFil As String, _
                ByVal pstrFilClaveUnica As String, ByVal pstrCunicaFil As String, ByVal pstrFilLegaNume As String, _
                ByVal pstrInseFil As String, ByVal pstrExpo As String, ByVal pstrLegaNumeFil As String, ByVal pstrFilClieTipo As String, _
                Optional ByVal mbooInclDeshab As Boolean = False) As String
        Dim lstrCmd As New StringBuilder

        'esta funci�n se llama tambi�n desde clsXMLHTTP
        lstrCmd.Append("exec clientes_busq")
        lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
        lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
        lstrCmd.Append(" , @clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
        If mbooInclDeshab Then
            lstrCmd.Append(" , @incluir_bajas = 1")
        Else
            lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)
        End If
        lstrCmd.Append(" , @incluir_agru = " + pintInclAgru.ToString)

        If pstrClieCtrl = "" Then
            lstrCmd.Append(" , @clie_agru = " + IIf(pbooSoloAgrupa, "1", "0"))
        Else
            If pbooFilAgru = "1" Then
                lstrCmd.Append(" , @clie_agru = " + IIf(pbooSoloAgrupa, "null", "0"))
            Else
                lstrCmd.Append(" , @clie_agru = 0")
            End If
        End If

        If pstrValorId = "" Then
            lstrCmd.Append(" , @clie_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
            lstrCmd.Append(" , @clie_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
            lstrCmd.Append(" , @clie_soci_nume=" + clsSQLServer.gFormatArg(pstrSociNume, SqlDbType.Int))
            lstrCmd.Append(" , @exps_nume=" + clsSQLServer.gFormatArg(pstrExpo, SqlDbType.Int))

            If pstrFilCriaNume = "1" Then
                lstrCmd.Append(" , @clie_raza=" + clsSQLServer.gFormatArg(pstrRazaFil, SqlDbType.Int))
                lstrCmd.Append(" , @clie_cria_nume=" + clsSQLServer.gFormatArg(pstrCriaNumeFil, SqlDbType.Int))
            End If
            If pstrFilNoCriaNume = "1" Then
                lstrCmd.Append(" , @clie_nocria_nume=" + clsSQLServer.gFormatArg(pstrNoCriaNumeFil, SqlDbType.Int))
            End If
            If pstrFilCuit = "1" And pstrCuitFil <> "" Then
                lstrCmd.Append(" , @clie_cuit=" + clsSQLServer.gFormatArg(Replace(pstrCuitFil, "-", ""), SqlDbType.Int))
            End If
            If pstrFilDocu = "1" Then
                lstrCmd.Append(" , @clie_docu_tipo=" + clsSQLServer.gFormatArg(pstrDocuTipoFil, SqlDbType.Int))
                lstrCmd.Append(" , @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNumeFil, SqlDbType.Int))
            End If
            If pstrFilTarj = "1" Then
                lstrCmd.Append(" , @tarj_id=" + clsSQLServer.gFormatArg(pstrTarjTipoFil, SqlDbType.Int))
                lstrCmd.Append(" , @tarj_nume=" + clsSQLServer.gFormatArg(pstrTarjNumeFil, SqlDbType.VarChar))
            End If
            If pstrFilEnti = "1" Then
                lstrCmd.Append(" , @clie_enti=" + clsSQLServer.gFormatArg(pstrEntiFil, SqlDbType.Int))
            End If
            If pstrFilMedioPago = "1" Then
                lstrCmd.Append(" , @clie_medio_pago=" + clsSQLServer.gFormatArg(pstrPagoFil, SqlDbType.Int))
            End If
            If pstrFilEmpr = "1" Then
                lstrCmd.Append(" , @clie_empr=" + clsSQLServer.gFormatArg(pstrEmprFil, SqlDbType.Int))
            End If
            ' Dario 2013-05-02 Cambio por error en la recarga de la grilla despues de guardar
            If pstrFilClaveUnica = "1" Then
                If (pstrCunicaFil = "0") Then
                    pstrCunicaFil = ""
                End If
                lstrCmd.Append(" , @clie_cunica=" + clsSQLServer.gFormatArg(Replace(pstrCunicaFil, "-", ""), SqlDbType.VarChar))
            End If
            'If pstrFilClaveUnica = "1" Then
            '    lstrCmd.Append(" , @clie_cunica=" + clsSQLServer.gFormatArg(Replace(pstrCunicaFil, "-", ""), SqlDbType.VarChar))
            'End If
            ' fin cambio
            If pstrFilLegaNume = "1" Then
                lstrCmd.Append(" , @clie_inse_id=" + clsSQLServer.gFormatArg(pstrInseFil, SqlDbType.Int))
                lstrCmd.Append(" , @clie_lega_nume=" + clsSQLServer.gFormatArg(pstrLegaNumeFil, SqlDbType.Int))
            End If
        End If
        If pstrFilClieTipo <> "" Then
                lstrCmd.Append(" , @clie_tipo=" + clsSQLServer.gFormatArg(pstrFilClieTipo, SqlDbType.VarChar))
        End If

        Return (lstrCmd.ToString)
    End Function
    Public MustOverride Sub mSeleccionarCliente(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mCargarDatos(ByVal pstrId As String)

    Public MustOverride Sub mCrearDataSet(ByVal pstrId As String)

    Public MustOverride Sub grdTele_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub grdDire_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub grdMail_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub grdIntegr_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosDire(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosInte(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEditarDatosIntegr(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mEliminarIntegrante(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)



End Class


End Namespace