'===========================================================================
' Este archivo se gener� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' Se cre� este archivo de c�digo 'App_Code\Migrated\Stub_Criaderos_aspx_vb.vb' que contiene la clase abstracta 
' utilizada como clase base para la clase 'Migrated_Criaderos' en el archivo 'Criaderos.aspx.vb'.
' De esta forma, todos los archivos de c�digo del proyecto pueden hacer referencia a la clase base.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================


Imports System.Data.SqlClient


Namespace SRA


MustInherit Public Class Criaderos
    Inherits FormGenerico
   Public mbooActi As Boolean
   Public mintActiDefa As Integer
   Public mstrCriaCtrl As String
   Public MustOverride Sub mInicializarControl()

   Public MustOverride Sub mInicializar()

   Public MustOverride Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

   Public MustOverride Sub mValidarConsulta()

   Public MustOverride Sub mSeleccionarCriadero(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

   Public MustOverride Sub mConsultar()

   Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pintBaja As Integer, ByVal pstrClieId As String, ByVal pstrNume As String, _
                                     ByVal pstrPais As String, ByVal pstrProv As String, ByVal pstrLoca As String, ByVal pstrDesc As String, ByVal pstrRaza As String) As String
      Dim lstrCmd As New StringBuilder

      'esta funci�n se llama tambi�n desde clsXMLHTTP
      lstrCmd.Append("exec criaderos_busq")
      lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
      lstrCmd.Append(" , @crdr_clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
      lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)
      lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
      lstrCmd.Append(" , @crdr_nume=" + clsSQLServer.gFormatArg(pstrNume, SqlDbType.Int))
      lstrCmd.Append(" , @crdr_desc=" + clsSQLServer.gFormatArg(pstrDesc, SqlDbType.VarChar))
      lstrCmd.Append(" , @pais_id=" + clsSQLServer.gFormatArg(pstrPais, SqlDbType.Int))
      lstrCmd.Append(" , @prov_id=" + clsSQLServer.gFormatArg(pstrProv, SqlDbType.Int))
      lstrCmd.Append(" , @loca_id=" + clsSQLServer.gFormatArg(pstrLoca, SqlDbType.Int))
      lstrCmd.Append(" , @raza_id=" + clsSQLServer.gFormatArg(pstrRaza, SqlDbType.Int))

      Return (lstrCmd.ToString)
   End Function
   Public MustOverride Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

   Public MustOverride Sub mCargarDatos(ByVal pstrId As String)

   Public MustOverride Sub mCrearDataSet(ByVal pstrId As String)

   Public MustOverride Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

   Public MustOverride Sub mEditarDatosDire(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

   Public MustOverride Sub mEditarDatosRaza(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

   Public MustOverride Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

   Public MustOverride Sub grdTele_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

   Public MustOverride Sub grdDire_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

   Public MustOverride Sub grdRaza_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

   Public MustOverride Sub grdMail_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)



End Class


End Namespace