'===========================================================================
' Este archivo se gener� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' Se cre� este archivo de c�digo 'App_Code\Migrated\Stub_DebiAutoEnvio_aspx_vb.vb' que contiene la clase abstracta 
' utilizada como clase base para la clase 'Migrated_DebiAutoEnvio' en el archivo 'DebiAutoEnvio.aspx.vb'.
' De esta forma, todos los archivos de c�digo del proyecto pueden hacer referencia a la clase base.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================




Namespace SRA


MustInherit Public Class DebiAutoEnvio
    Inherits FormGenerico
    Public mstrTitulo As String
    Public MustOverride Sub mCargarDatos(ByVal pstrId As String)

    Public Shared Function gObtenerExiste(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim lstrCmd As String
        Dim lstrRet As String
        Dim lds As DataSet
        Dim lvstr() As String
        lvstr = pstrArgs.Split(";")

        lstrCmd = "exec debitos_cabe_consul"
        lstrCmd += " @deca_anio=" + lvstr(0)
        lstrCmd += ",@deca_perio=" + lvstr(1)
        lstrCmd += ",@deca_peti_id=" + lvstr(2)
        lstrCmd += ",@regeneracion=1"
        If Len(lvstr(3)) > 0 Then 'mstrInstituto
            lstrCmd += ",@inse_id=" + lvstr(3)
        End If

        lds = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd)

        If lds.Tables(0).Rows.Count > 0 Then
            lstrRet = "Ya se gener� el d�bito autom�tico para este per�odo. Fecha: " + CDate(lds.Tables(0).Rows(0).Item("deca_audi_fecha")).ToString("dd/MM/yyyy HH:mm")
        Else
            lstrRet = ""
        End If
        Return (lstrRet)
    End Function
    Public MustOverride Sub mCrearDataSet(ByVal pstrId As String)

    Public MustOverride Sub mConsultar()

    Public MustOverride Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

    Public MustOverride Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mConsultarDeta(Optional ByVal pstrSociId As String = "")

    Public MustOverride Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)



End Class


End Namespace