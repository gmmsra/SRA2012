'===========================================================================
' Este archivo se gener� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' Se cre� este archivo de c�digo 'App_Code\Migrated\Stub_Socios_aspx_vb.vb' que contiene la clase abstracta 
' utilizada como clase base para la clase 'Migrated_Socios' en el archivo 'Socios.aspx.vb'.
' De esta forma, todos los archivos de c�digo del proyecto pueden hacer referencia a la clase base.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================


Imports ReglasValida.Validaciones


Namespace SRA


MustInherit Public Class Socios
    Inherits FormGenerico
    Public MustOverride Sub mInicializarControl()

    Public MustOverride Sub mCargarDatos(ByVal pstrId As String)

    Public MustOverride Sub mCrearDataSet(ByVal pstrId As String)

    Public MustOverride Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)

    Public MustOverride Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mSeleccionar(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

    Public MustOverride Sub mRetorno(ByVal pstrnume As String)

    Public MustOverride Sub mConsultar()

    Public Shared Function ObtenerSql(ByVal pintBaja As Integer, ByVal pstrValorId As String, ByVal pstrInclBloq As String, ByVal pstrApel As String, ByVal pstrClieId As String, ByVal pstrSociNume As String, ByVal pstrCuit As String, ByVal pstrDocuTipo As String, ByVal pstrDocuNume As String, ByVal pstrFirmanteNume As String, ByVal pstrFirmante As String, ByVal pstrCateTitu As String, ByVal pstrCatePode As String, ByVal pstrEstaId As String) As String
        Dim lstrCmd As New StringBuilder
        lstrCmd.Append("exec socios_busq ")
        lstrCmd.Append("@incluir_bajas=" + pintBaja.ToString)

        If pstrValorId <> "" Then
            lstrCmd.Append(", @soci_id=" + pstrValorId)
        Else
            lstrCmd.Append(",@bloq=")
            Select Case pstrInclBloq
                Case "S"
                    lstrCmd.Append("1")
                Case "N"
                    lstrCmd.Append("0")
                Case "T", ""
                    lstrCmd.Append("NULL")
            End Select

            lstrCmd.Append(", @soci_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
            lstrCmd.Append(", @soci_clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
            lstrCmd.Append(", @soci_nume=" + clsSQLServer.gFormatArg(pstrSociNume, SqlDbType.Int))
            lstrCmd.Append(", @clie_cuit=" + clsSQLServer.gFormatArg(pstrCuit, SqlDbType.Int))
            lstrCmd.Append(", @clie_doti_id=" + clsSQLServer.gFormatArg(pstrDocuTipo, SqlDbType.Int))
            lstrCmd.Append(", @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNume, SqlDbType.Int))
            lstrCmd.Append(", @firm_soci_nume=" + clsSQLServer.gFormatArg(pstrFirmanteNume, SqlDbType.Int))
            lstrCmd.Append(", @firm_nyap=" + clsSQLServer.gFormatArg(pstrFirmante, SqlDbType.VarChar))

            If pstrCateTitu <> "" Then
                lstrCmd.Append(", @cate_titu=" + pstrCateTitu)
            End If

            If pstrCatePode <> "" Then
                lstrCmd.Append(", @cate_pode=" + pstrCatePode)
            End If

            If pstrEstaId <> "" Then
                lstrCmd.Append(", @esta_id=" + pstrEstaId)
            End If
        End If

        Return (lstrCmd.ToString)
    End Function


End Class


End Namespace