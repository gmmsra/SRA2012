Imports System.Data.SqlClient


Namespace SRA


Partial Class ClienDocu
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable

   Protected WithEvents txtRefeFil As NixorControls.TextBoxTab
   Protected WithEvents btnLimpFil As System.Web.UI.WebControls.ImageButton

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "clientes_docum"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String

   Private Enum Columnas As Integer
      ClieEdit = 0
      ClieDocuId = 1
      ClieRefe = 2
      ClieDocu = 3
      ClieActi = 4
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar()
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
       clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If

      'clientes
      lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      btnAlta.Visible = lbooPermiAlta
      btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Clientes_Docu_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = lbooPermiModi
      btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)

   End Sub

   Private Sub mSetearMaxLength()

      Dim lstrLong As Object
      Dim lintCol As Integer

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cldo_refe")

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      usrClieFil.Tabla = "clientes"
      usrClieFil.AutoPostback = True
      usrClieFil.Ancho = 790
      usrClieFil.Alto = 510

      'Clave unica
      usrClieFil.ColClaveUnica = True
      usrClieFil.FilClaveUnica = True
      'Numero de cliente
      usrClieFil.ColClieNume = True

      'Fantasia
      'usrClieFil.ColFanta = False
      'usrClieFil.FilFanta = False
      'Numero de CUIT
      usrClieFil.ColCUIT = True
      usrClieFil.FilCUIT = True
      'Numero de documento
      usrClieFil.ColDocuNume = True
      usrClieFil.FilDocuNume = True
      'Numero de socio
      usrClieFil.ColSociNume = True
      usrClieFil.FilSociNume = True
      
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()

      Try

         mstrCmd = "exec " + mstrTabla & "_consul "
         mstrCmd = mstrCmd + "@cldo_clie_id = " & usrClieFil.Valor

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         hdnClieId.Text = usrClieFil.Valor

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnDesc.Disabled = pbooAlta
        btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.ClieDocuId).Text)
      lblTitu.Text = "Cliente: " & usrClieFil.Valor & " - " & usrClieFil.Apel

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtRefe.Valor = .Item("cldo_refe")
            cmbActi.Valor = .Item("cldo_acti_id")
            If .IsNull("cldo_path") Then
                txtDocu.Text = ""
            Else
                txtDocu.Text = .Item("cldo_path")
            End If
         End With

         mSetearEditor(False)
         mMostrarPanel(True)

      End If
   End Sub

   Private Sub mAgregar()
     Try
        If Me.usrClieFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
        Else
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnDesc.Disabled = True
            btnAlta.Enabled = True
            mMostrarPanel(True)
        End If
     Catch ex As Exception
        clsError.gManejarError(Me, ex)
     End Try
   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      txtDocu.Text = ""
      txtRefe.Text = ""
      cmbActi.Limpiar()
      lblTitu.Text = ""

      mCrearDataSet("")
      mSetearEditor(True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      panDato.Visible = pbooVisi
      'panFiltros.Visible = Not panDato.Visible
      'grdDato.Visible = Not panDato.Visible
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.ClienteDocu(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, filDocu, Server)

         lobjCliente.Alta()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub



   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.ClienteDocu(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, filDocu, Server)

         lobjCliente.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      With mdsDatos.Tables(0).Rows(0)
         .Item("cldo_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("cldo_refe") = txtRefe.Valor
         .Item("cldo_clie_id") = hdnClieId.Text
         .Item("cldo_acti_id") = cmbActi.Valor
         If filDocu.Value <> "" Then
            .Item("cldo_path") = filDocu.Value
         Else
            .Item("cldo_path") = txtDocu.Text
         End If

         .Item("cldo_path") = .Item("cldo_path").Substring(.Item("cldo_path").LastIndexOf("\") + 1)
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      Session(mstrTabla) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region


   Private Sub usrClieFil_Cambio(ByVal sender As Object) Handles usrClieFil.Cambio
      mConsultar()
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      Try
         If Me.usrClieFil.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
         Else
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
End Class
End Namespace
