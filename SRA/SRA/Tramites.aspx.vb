Imports System.Data.SqlClient
Imports SRA



Public Class Tramites
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents usrCriaFil As usrClieDeriv
    'Protected WithEvents usrProd As usrProdNuevo
    'Protected WithEvents usrProdFil As usrProdDeriv
    'Protected WithEvents usrClieVend As usrClieDeriv
    'Protected WithEvents usrClieProp As usrClieDeriv
    'Protected WithEvents usrClieComp As usrClieDeriv
    'Protected WithEvents rowDivPais As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowDivImpo As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowImpo As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowDivProp As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowProp As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowDivPaisFil As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowPaisFil As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowPais As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowDosisCria As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents rowCantEmbr As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents lblRegiTipo As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbRegiTipo As NixorControls.ComboBox
    'Protected WithEvents lblVariedad As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As NixorControls.BotonImagen
    'Protected WithEvents btnLimpiarFil As NixorControls.BotonImagen
    'Protected WithEvents lblNumeFil As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNumeFil As NixorControls.NumberBox
    'Protected WithEvents lblEstadoFil As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbEstadoFil As NixorControls.ComboBox
    'Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents cmbRazaCria As NixorControls.ComboBox

    'Protected WithEvents lblSraNumeFil As System.Web.UI.WebControls.Label
    'Protected WithEvents txtSraNumeFil As NixorControls.NumberBox
    'Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaDesdeFil As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaDesdeFil As NixorControls.DateBox
    'Protected WithEvents lblFechaHastaFil As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaHastaFil As NixorControls.DateBox
    'Protected WithEvents lblProdFil As System.Web.UI.WebControls.Label
    'Protected WithEvents panFiltros As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnAgre As NixorControls.BotonImagen
    'Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkRequ As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkDocu As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkObse As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkVend As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkComp As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents panLinks As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    'Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents lblInicFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents txtInicFecha As NixorControls.DateBox
    'Protected WithEvents txtFechaTram As NixorControls.DateBox
    'Protected WithEvents txtFechaTransf As NixorControls.DateBox
    'Protected WithEvents lblFinaFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFinaFecha As NixorControls.DateBox
    'Protected WithEvents lblEsta As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbEsta As NixorControls.ComboBox
    'Protected WithEvents lblDosiCant As System.Web.UI.WebControls.Label
    'Protected WithEvents txtDosiCant As NixorControls.NumberBox
    'Protected WithEvents lblCriaCant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCantEmbr As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCantEmbr As NixorControls.NumberBox
    'Protected WithEvents txtCriaCant As NixorControls.NumberBox
    'Protected WithEvents lblTranPlan As System.Web.UI.WebControls.Label
    'Protected WithEvents lblClieComp As System.Web.UI.WebControls.Label
    'Protected WithEvents lblClieProp As System.Web.UI.WebControls.Label
    'Protected WithEvents lblClieVend As System.Web.UI.WebControls.Label
    'Protected WithEvents panCabecera As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdRequ As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblRequRequ As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbRequRequ As NixorControls.ComboBox
    'Protected WithEvents chkRequPend As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents lblRequManuTit As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRequManu As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRequFinaFechaTit As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRequFinaFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents btnAltaRequ As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBajaRequ As System.Web.UI.WebControls.Button
    'Protected WithEvents btnModiRequ As System.Web.UI.WebControls.Button
    'Protected WithEvents btnLimpRequ As System.Web.UI.WebControls.Button
    'Protected WithEvents panRequ As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdDocu As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblDocuDocu As System.Web.UI.WebControls.Label
    'Protected WithEvents txtDocuDocu As NixorControls.TextBoxTab
    'Protected WithEvents lblDocuObse As System.Web.UI.WebControls.Label
    'Protected WithEvents txtDocuObse As NixorControls.TextBoxTab
    'Protected WithEvents btnAltaDocu As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBajaDocu As System.Web.UI.WebControls.Button
    'Protected WithEvents btnModiDocu As System.Web.UI.WebControls.Button
    'Protected WithEvents btnLimpDocu As System.Web.UI.WebControls.Button
    'Protected WithEvents panDocu As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdObse As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblObseFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents txtObseFecha As NixorControls.DateBox
    'Protected WithEvents lblObseRequ As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbObseRequ As NixorControls.ComboBox
    'Protected WithEvents lblObseObse As System.Web.UI.WebControls.Label
    'Protected WithEvents txtObseObse As NixorControls.TextBoxTab
    'Protected WithEvents btnAltaObse As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBajaObse As System.Web.UI.WebControls.Button
    'Protected WithEvents btnModiObse As System.Web.UI.WebControls.Button
    'Protected WithEvents btnLimpObse As System.Web.UI.WebControls.Button
    'Protected WithEvents panObse As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdVend As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents panVend As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdComp As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblPorcComp As System.Web.UI.WebControls.Label
    'Protected WithEvents txtPorcComp As NixorControls.NumberBox
    'Protected WithEvents lblObseComp As System.Web.UI.WebControls.Label
    'Protected WithEvents txtObseComp As NixorControls.TextBoxTab
    'Protected WithEvents btnModiComp As System.Web.UI.WebControls.Button
    'Protected WithEvents btnLimpComp As System.Web.UI.WebControls.Button
    'Protected WithEvents panComp As System.Web.UI.WebControls.Panel
    'Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblBaja As System.Web.UI.WebControls.Label
    'Protected WithEvents btnAlta As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
    'Protected WithEvents btnModi As System.Web.UI.WebControls.Button
    'Protected WithEvents btnLimp As System.Web.UI.WebControls.Button
    'Protected WithEvents panBotones As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnRazaId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnRequId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnDocuId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnObseId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnDatosPop As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblAltaId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnMultiProd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnTramNro As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnMsgError As System.Web.UI.WebControls.TextBox
    'Protected WithEvents imgDelDocuDoc As System.Web.UI.HtmlControls.HtmlImage
    'Protected WithEvents filDocuDocu As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents btnDescDocu As System.Web.UI.HtmlControls.HtmlButton
    'Protected WithEvents hdnVendOrig As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnCompOrig As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblImpo As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbImpo As NixorControls.ComboBox
    'Protected WithEvents lblPais As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbPais As NixorControls.ComboBox
    'Protected WithEvents hdnCompId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblNombComp As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPaisFil As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbPaisFil As NixorControls.ComboBox
    'Protected WithEvents lblFechaTram As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRecuFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents txtRecuFecha As NixorControls.DateBox
    'Protected WithEvents btnTeDenu As System.Web.UI.WebControls.Button
    'Protected WithEvents hdnDatosTEPop As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnDetaId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnTEId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtTeDesc As NixorControls.TextBoxTab
    'Protected WithEvents lblCriaComp As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCriaVend As System.Web.UI.WebControls.Label
    'Protected WithEvents cmbCriaComp As NixorControls.ComboBox
    'Protected WithEvents cmbCriaVend As NixorControls.ComboBox
    'Protected WithEvents hdnCriaComp As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnCriaVend As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnErr As System.Web.UI.WebControls.Button
    'Protected WithEvents chkVisto As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents btnStock As System.Web.UI.WebControls.Button
    'Protected WithEvents cmbVari As NixorControls.ComboBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Tramites
    Private mstrTablaRequisitos As String = SRA_Neg.Constantes.gTab_Tramites_Deta
    Private mstrTablaDocumentos As String = SRA_Neg.Constantes.gTab_Tramites_Docum
    Private mstrTablaObservaciones As String = SRA_Neg.Constantes.gTab_Tramites_Obse
    'Private mstrTablaCompradores As String = SRA_Neg.Constantes.gTab_Tramites_Personas & "_Compradores"
    'Private mstrTablaVendedores As String = SRA_Neg.Constantes.gTab_Tramites_Personas & "_Vendedores"
    Private mstrTablaProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
    Private mstrParaPageSize As Integer
    Private mdsDatosPadre As DataSet
    Private mdsDatosMadre As DataSet
    Private dsVali As DataSet
    Private mstrCmd As String
    Public mintProce As Integer
    Private mdsDatos As DataSet
    Private mstrConn As String
    Private mstrTrapId As String
    Public mstrTitulo As String
    Public mintTtraId As Integer
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                clsWeb.gInicializarControles(Me, mstrConn)

                Session(mSess(mstrTabla)) = Nothing

                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mMostrarPanel(False)
            Else
                mCargarCriadores(cmbCriaComp, usrClieComp.Valor.ToString, usrProd.usrMadreExt.cmbProdRazaExt.Valor.ToString)
                If Not Request.Form("cmbCriaComp") Is Nothing Then
                    cmbCriaComp.Valor = Request.Form("cmbCriaComp")
                    If Not cmbCriaComp.Valor Is DBNull.Value Then
                        hdnCriaComp.Text = cmbCriaComp.Valor
                    End If
                Else
                    cmbCriaComp.Valor = hdnCriaComp.Text
                End If
                mCargarCriadores(cmbCriaVend, usrClieVend.Valor.ToString, usrProd.usrMadreExt.cmbProdRazaExt.Valor.ToString)
                If Not Request.Form("cmbCriaVend") Is Nothing Then
                    cmbCriaVend.Valor = Request.Form("cmbCriaVend")
                    If Not cmbCriaVend.Valor Is DBNull.Value Then
                        hdnCriaVend.Text = cmbCriaVend.Valor
                    End If
                Else
                    cmbCriaVend.Valor = hdnCriaVend.Text
                End If
                mdsDatos = Session(mSess(mstrTabla))
                mstrTrapId = Session("mstrTrapId")
            End If

            usrCriaFil.cmbCriaRazaExt.Valor = cmbRazaFil.Valor.ToString
            usrProdFil.cmbProdRazaExt.Valor = cmbRazaFil.Valor.ToString
            If cmbRazaFil.Valor.ToString <> "0" Then
                usrCriaFil.cmbCriaRazaExt.Enabled = False
                usrProdFil.cmbProdRazaExt.Enabled = False
            Else
                usrCriaFil.cmbCriaRazaExt.Enabled = True
                usrProdFil.cmbProdRazaExt.Enabled = True
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mCargarCombos()
        Dim lstrSRA As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Trámites) + ",@defa=1")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Trámites) + ",@defa=1")

        clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazaFil, "T")




        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
        clsWeb.gCargarCombo(mstrConn, "rg_requisitos_cargar", cmbRequRequ, "id", "descrip_codi", "S")

        clsWeb.gCargarCombo(mstrConn, "importadores_cargar", cmbImpo, "id", "descrip_codi", "S")

        clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar", cmbRegiTipo, "id", "descrip", "T")
        cmbVari.Items.Clear()

        clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar", cmbVari, "id", "descrip", "S")


        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "S", "@inc_defa = null", True) ' & IIf(mintTtraId = SRA_Neg.Constantes.TramitesTipos.Exportación Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportaciónSemen, "null", "1")
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPaisFil, "S", "", True)
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnAgre.Attributes.Add("onclick", "return btnAgre_click();")
        btnModi.Attributes.Add("onclick", "return btnModi_click(" & SRA_Neg.Constantes.Estados.Tramites_Baja & ");")
        btnTeDenu.Attributes.Add("onclick", "mCargarTE();return(false);")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
        Select Case mintTtraId
            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen, SRA_Neg.Constantes.TramitesTipos.ImportacionSemen, SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
                btnStock.Visible = True
                btnStock.Text = "Stock Semen"
                btnStock.Attributes.Add("onclick", "mVerStockSemen(); return false;")
            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones, SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones, SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones
                btnStock.Visible = True
                btnStock.Text = "Stock Embriones"
                btnStock.Attributes.Add("onclick", "mVerStockEmbriones(); return false;")
        End Select


    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nume")
        txtDosiCant.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_dosi_cant")
        txtCriaCant.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_cria_cant")
        txtCantEmbr.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, SRA_Neg.Constantes.gTab_Productos)
        txtSraNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_sra_nume")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocumentos)
        txtDocuObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trdo_refe")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaObservaciones)
        txtObseObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trao_obse")

        'lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaVendedores)
        'txtObseComp.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trpe_obse")
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mHabilitarDatosTE(ByVal pbooHabi As Boolean)
        usrClieVend.Activo = pbooHabi
        cmbCriaVend.Enabled = pbooHabi
        'txtRecuFecha.Enabled = pbooHabi
        'usrProd.Habilitar = False
        'usrProd.usrMadreExt.Activo = pbooHabi
        'usrProd.usrMadreExt.cmbProdRazaExt.Enabled = False
        'usrProd.usrPadreExt.Activo = pbooHabi
    End Sub
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdRequ.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdDocu.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdObse.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdVend.PageSize = Convert.ToInt32(mstrParaPageSize)

        mintTtraId = Request("ttra_id")

        Select Case mintTtraId
            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos
                mintProce = ReglasValida.Validaciones.Procesos.TransferenciaProductos
            Case SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
                mintProce = ReglasValida.Validaciones.Procesos.ImportacionProductos
            Case SRA_Neg.Constantes.TramitesTipos.ExportacionProductos
                mintProce = ReglasValida.Validaciones.Procesos.ExportacionProductos
            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones
                mintProce = ReglasValida.Validaciones.Procesos.TransferenciaEmbriones
            Case SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones
                mintProce = ReglasValida.Validaciones.Procesos.ImportacionEmbriones
            Case SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones
                mintProce = ReglasValida.Validaciones.Procesos.ExportacionEmbriones
            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen
                mintProce = ReglasValida.Validaciones.Procesos.TransferenciaSemen
            Case SRA_Neg.Constantes.TramitesTipos.ImportacionSemen
                mintProce = ReglasValida.Validaciones.Procesos.ImportacionSemen
            Case SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
                mintProce = ReglasValida.Validaciones.Procesos.ExportacionSemen
        End Select

        'lblCriaComp.Visible = False
        'cmbCriaComp.Visible = False
        'lblCriaVend.Visible = False
        'cmbCriaVend.Visible = False

        Select Case mintTtraId
            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos, _
                    SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen
                If mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos Then
                    mstrTitulo = "Transferencia de Productos"
                    lblDosiCant.Text = "Reserva Semen Dosis"
                Else
                    mstrTitulo = "Transferencia de Semen"
                End If

                rowPaisFil.Style.Add("display", "none")
                rowDivPaisFil.Style.Add("display", "none")

                rowPais.Style.Add("display", "none")
                rowDivPais.Style.Add("display", "none")

                rowImpo.Style.Add("display", "none")
                rowDivImpo.Style.Add("display", "none")

                usrProd.MostrarNumeroExtranjero = False
                usrProd.usrProductoExt.FilRpNume = True
                usrProd.usrProductoExt.IgnogaInexistente = True

                usrProd.Habilitar = False
                usrProd.MostrarCriador = False
                usrProd.MostrarResultado = False

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionProductos, _
                SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
                If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionProductos Then
                    mstrTitulo = "Exportación de Productos"
                    lblDosiCant.Text = "Reserva Semen Dosis:"
                Else
                    mstrTitulo = "Exportación de Semen"
                End If

                usrProd.Habilitar = False
                usrProd.MostrarNumeroExtranjero = False
                usrProd.usrProductoExt.FilRpNume = True
                usrProd.usrProductoExt.IgnogaInexistente = True
                usrProd.MostrarCriador = False
                usrProd.MostrarResultado = False

            Case SRA_Neg.Constantes.TramitesTipos.ImportacionProductos, SRA_Neg.Constantes.TramitesTipos.ImportacionSemen
                If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos Then
                    mstrTitulo = "Importación de Productos"
                    lblDosiCant.Text = "Reserva Semen Dosis:"
                    usrProd.MostrarFechaNacimiento = True
                Else
                    mstrTitulo = "Importación de Semen"
                    usrProd.MostrarFechaNacimiento = True
                End If

                lnkVend.Text = "Vendedores"
                grdVend.Columns(2).Visible = False

                usrClieVend.Activo = (hdnId.Text = "")

                rowProp.Style.Add("display", "none")
                rowDivProp.Style.Add("display", "none")

                usrProd.usrProductoExt.FilRpNume = True
                usrProd.Adicionales = True
                usrProd.MostrarPedigree = True
                usrProd.MostrarCriador = False
                usrProd.MostrarResultado = True

                usrProd.usrProductoExt.IgnogaInexistente = True

            Case SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones
                mstrTitulo = "Importación de Embriones"

                'lblCriaComp.Visible = True
                'cmbCriaComp.Visible = True
                'lblCriaVend.Visible = True
                'cmbCriaVend.Visible = True

                lnkVend.Text = "Vendedores"
                grdDato.Columns(5).Visible = False
                grdVend.Columns(2).Visible = False
                grdComp.Columns(0).Visible = False
                grdComp.Columns(3).Visible = False
                lblPorcComp.Visible = False
                txtPorcComp.Visible = False
                lblObseComp.Visible = False
                txtObseComp.Visible = False
                btnLimpComp.Visible = False
                btnModiComp.Visible = False

                rowProp.Style.Add("display", "none")
                rowDivProp.Style.Add("display", "none")

                rowCantEmbr.Style.Add("display", "inline")

                usrClieVend.Activo = (hdnId.Text = "")

                usrProd.MostrarProducto = False
                usrProd.MostrarResultado = False
                usrProd.MostrarCriador = False

                usrProd.MostrarPadre = True
                usrProd.MostrarPedigreePadre = True
                usrProd.usrPadreExt.FilRpNume = True
                usrProd.usrPadreExt.IgnogaInexistente = True

                usrProd.MostrarMadre = True
                usrProd.MostrarPedigreeMadre = True
                usrProd.usrMadreExt.FilRpNume = True
                usrProd.usrMadreExt.IgnogaInexistente = True

            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones
                mstrTitulo = "Transferencia de Embriones"
                btnTeDenu.Visible = True
                txtTeDesc.Visible = True

                'lblCriaComp.Visible = True
                'cmbCriaComp.Visible = True
                'lblCriaVend.Visible = True
                'cmbCriaVend.Visible = True

                grdComp.Columns(0).Visible = False
                grdComp.Columns(3).Visible = False
                grdVend.Columns(2).Visible = False
                lblPorcComp.Visible = False
                txtPorcComp.Visible = False
                lblObseComp.Visible = False
                txtObseComp.Visible = False
                btnLimpComp.Visible = False
                btnModiComp.Visible = False

                mHabilitarDatosTE(False)

                rowPaisFil.Style.Add("display", "none")
                rowDivPaisFil.Style.Add("display", "none")

                rowPais.Style.Add("display", "none")
                rowDivPais.Style.Add("display", "none")

                rowProp.Style.Add("display", "none")
                rowDivProp.Style.Add("display", "none")

                rowCantEmbr.Style.Add("display", "inline")

                rowImpo.Style.Add("display", "none")
                rowDivImpo.Style.Add("display", "none")

                usrProd.MostrarProducto = False
                usrProd.MostrarResultado = False
                usrProd.MostrarCriador = False

                usrProd.MostrarPadre = True
                usrProd.MostrarPedigreePadre = True
                usrProd.usrPadreExt.FilRpNume = True

                usrProd.MostrarMadre = True
                usrProd.MostrarPedigreeMadre = True
                usrProd.usrMadreExt.FilRpNume = True

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones
                mstrTitulo = "Exportación de Embriones"
                btnTeDenu.Visible = True
                txtTeDesc.Visible = True

                'lblCriaComp.Visible = True
                'cmbCriaComp.Visible = True
                'lblCriaVend.Visible = True
                'cmbCriaVend.Visible = True

                grdComp.Columns(0).Visible = False
                grdComp.Columns(3).Visible = False
                grdVend.Columns(2).Visible = False
                lblPorcComp.Visible = False
                txtPorcComp.Visible = False
                lblObseComp.Visible = False
                txtObseComp.Visible = False
                btnLimpComp.Visible = False
                btnModiComp.Visible = False

                mHabilitarDatosTE(False)

                rowCantEmbr.Style.Add("display", "inline")

                rowProp.Style.Add("display", "none")
                rowDivProp.Style.Add("display", "none")

                usrProd.MostrarProducto = False
                usrProd.MostrarResultado = False
                usrProd.MostrarCriador = False

                usrProd.MostrarPadre = True
                usrProd.MostrarPedigreePadre = True
                usrProd.usrPadreExt.FilRpNume = True

                usrProd.MostrarMadre = True
                usrProd.MostrarPedigreeMadre = True
                usrProd.usrMadreExt.FilRpNume = True

        End Select

        btnAgre.AlternateText = "Nueva " & mstrTitulo

        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
            usrProdFil.Sexo = "1"
            usrProdFil.FilSexo = False
            usrProd.usrProductoExt.Sexo = "1"
            usrProd.usrProductoExt.FilSexo = False
        End If

        'If (usrProd.usrProductoExt.cmbSexoProdExt.Valor.ToString = "1" And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionProductos) Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
            rowDosisCria.Style.Add("display", "inline")
        Else
            rowDosisCria.Style.Add("display", "none")
        End If

        lblTituAbm.Text = mstrTitulo
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub grdDato_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdRequ_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdRequ.EditItemIndex = -1
            If (grdRequ.CurrentPageIndex < 0 Or grdRequ.CurrentPageIndex >= grdRequ.PageCount) Then
                grdRequ.CurrentPageIndex = 0
            Else
                grdRequ.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdDocu_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDocu.EditItemIndex = -1
            If (grdDocu.CurrentPageIndex < 0 Or grdDocu.CurrentPageIndex >= grdDocu.PageCount) Then
                grdDocu.CurrentPageIndex = 0
            Else
                grdDocu.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarDocu()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdObse.EditItemIndex = -1
            If (grdObse.CurrentPageIndex < 0 Or grdObse.CurrentPageIndex >= grdObse.PageCount) Then
                grdObse.CurrentPageIndex = 0
            Else
                grdObse.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdVend_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        '   Try
        '      grdVend.EditItemIndex = -1
        '      If (grdVend.CurrentPageIndex < 0 Or grdVend.CurrentPageIndex >= grdVend.PageCount) Then
        '         grdVend.CurrentPageIndex = 0
        '      Else
        '         grdVend.CurrentPageIndex = E.NewPageIndex
        '      End If
        '      mConsultarVend()
        '   Catch ex As Exception
        '      clsError.gManejarError(Me, ex)
        '   End Try
    End Sub
    Public Sub grdComp_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        '   Try
        '      grdComp.EditItemIndex = -1
        '      If (grdComp.CurrentPageIndex < 0 Or grdComp.CurrentPageIndex >= grdComp.PageCount) Then
        '         grdComp.CurrentPageIndex = 0
        '      Else
        '         grdComp.CurrentPageIndex = E.NewPageIndex
        '      End If
        '      mConsultarComp()
        '   Catch ex As Exception
        '      clsError.gManejarError(Me, ex)
        '   End Try
    End Sub

    Private Sub mConsultar()
        Try
            Dim lstrCmd As String

            lstrCmd = "exec " & SRA_Neg.Constantes.gTab_Tramites & "_busq"
            lstrCmd += " @tram_nume=" + clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int)
            lstrCmd += ",@tram_raza_id=" + clsSQLServer.gFormatArg(cmbRazaFil.Valor, SqlDbType.Int)


            lstrCmd += ",@sra_nume=" + clsSQLServer.gFormatArg(txtSraNumeFil.Valor, SqlDbType.Int)
            lstrCmd += ",@cria_id=" + clsSQLServer.gFormatArg(usrCriaFil.Valor, SqlDbType.Int)
            lstrCmd += ",@prod_id=" + clsSQLServer.gFormatArg(usrProdFil.Valor, SqlDbType.Int)
            lstrCmd += ",@tram_fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
            lstrCmd += ",@tram_fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)
            lstrCmd += ",@tram_ttra_id=" + mintTtraId.ToString
            lstrCmd += ",@tram_esta_id=" + cmbEstadoFil.Valor.ToString
            lstrCmd += ",@tram_pais_id=" + cmbPaisFil.Valor.ToString
            lstrCmd += ",@mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")
            If mintTtraId.ToString = "10" Or mintTtraId.ToString = "12" Then
                lstrCmd = lstrCmd & " ,@prod_nacionalidad='I'"

            End If


            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)
            grdDato.Visible = True

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mConsultarRequ()
        grdRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
        grdRequ.DataBind()
    End Sub
    Private Sub mConsultarDocu()
        grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocumentos)
        grdDocu.DataBind()
    End Sub
    Private Sub mConsultarObse()
        mdsDatos.Tables(mstrTablaObservaciones).DefaultView.Sort = "trao_fecha desc,_requ_desc"
        grdObse.DataSource = mdsDatos.Tables(mstrTablaObservaciones)
        grdObse.DataBind()
    End Sub
    'Private Sub mConsultarVend()
    '   grdVend.DataSource = mdsDatos.Tables(mstrTablaVendedores)
    '   grdVend.DataBind()
    'End Sub
    'Private Sub mConsultarComp()
    '   grdComp.DataSource = mdsDatos.Tables(mstrTablaCompradores)
    '   grdComp.DataBind()
    'End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaRequisitos
        mdsDatos.Tables(2).TableName = mstrTablaDocumentos
        mdsDatos.Tables(3).TableName = mstrTablaObservaciones
        mdsDatos.Tables(4).TableName = mstrTablaProductos
        'mdsDatos.Tables(5).TableName = mstrTablaVendedores
        'mdsDatos.Tables(6).TableName = mstrTablaCompradores

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdRequ.CurrentPageIndex = 0
        grdDato.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0
        grdVend.CurrentPageIndex = 0
        grdComp.CurrentPageIndex = 0

        mConsultarRequ()
        mConsultarDocu()
        mConsultarObse()
        'mConsultarVend()
        'mConsultarComp()

        Session(mSess(mstrTabla)) = mdsDatos
    End Sub
#End Region

#Region "Seteo de Controles"
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mEditarTramite(E.Item.Cells(2).Text)
    End Sub

    Private Sub mCargarCriadores(ByVal pobjCombo As NixorControls.ComboBox, ByVal pstrClieId As String, ByVal pstrRaza As String)
        If pobjCombo.Visible Then
            mstrCmd = "criadores_cliente_cargar "
            If pstrClieId <> "" Then
                mstrCmd = mstrCmd & " @clie_id=" & pstrClieId
            Else
                mstrCmd = mstrCmd & " @clie_id=0"
            End If
            If pstrRaza <> "" Then
                mstrCmd = mstrCmd & ",@raza_id=" & pstrRaza
            End If
            clsWeb.gCargarCombo(mstrConn, mstrCmd, pobjCombo, "id", "descrip", "S")
        End If
    End Sub

    Public Sub mEditarTramite(ByVal pstrTram As String)
        Try
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(pstrTram)

            mCrearDataSet(hdnId.Text)


            With mdsDatos.Tables(mstrTabla).Rows(0)
                hdnTramNro.Text = "Trámite Nro: " & .Item("tram_nume")
                lblTitu.Text = hdnTramNro.Text
                hdnRazaId.Text = .Item("tram_raza_id")
                txtInicFecha.Fecha = .Item("tram_inic_fecha")
                txtFinaFecha.Fecha = .Item("tram_fina_fecha")
                txtFechaTram.Fecha = .Item("tram_pres_fecha")
                txtDosiCant.Valor = .Item("tram_dosi_cant")
                txtCriaCant.Valor = .Item("tram_cria_cant")
                cmbEsta.Valor = .Item("tram_esta_id")

                cmbImpo.Valor = .Item("tram_impr_id")
                cmbPais.Valor = .Item("tram_pais_id")

                usrClieComp.Valor = .Item("tram_comp_clie_id")



                hdnCompOrig.Text = usrClieComp.Valor.ToString

                usrClieVend.Valor = .Item("tram_vend_clie_id")
                If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
                    usrClieVend.Activo = mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
                End If

                usrClieProp.Valor = .Item("tram_prop_clie_id")
                usrClieProp.Activo = .IsNull("tram_prop_clie_id")

                hdnVendOrig.Text = IIf(mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos, usrClieVend.Valor.ToString, usrClieProp.Valor.ToString)


                If Not .IsNull("tram_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("tram_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

                If mdsDatos.Tables(mstrTablaProductos).Rows.Count = 1 Then
                    With mdsDatos.Tables(mstrTablaProductos).Rows(0)
                        hdnDetaId.Text = .Item("trpr_id")
                    End With
                Else
                    hdnDetaId.Text = "0"
                End If

                If mdsDatos.Tables(mstrTablaProductos).Rows.Count > 1 Then
                    usrProd.Visible = False
                    rowDosisCria.Style.Add("display", "none")
                    rowCantEmbr.Style.Add("display", "none")
                    lblTranPlan.Visible = True
                    hdnMultiProd.Text = "S"
                Else
                    With mdsDatos.Tables(mstrTablaProductos).Rows(0)
                        usrProd.Tramite = hdnId.Text
                        usrProd.Valor = .Item("trpr_prdt_id")
                    End With
                    usrProd.Visible = True
                    'If (usrProd.usrProductoExt.cmbSexoProdExt.Valor.ToString = "1" And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.Importacionproductos) Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportaciónSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
                    If mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
                        rowDosisCria.Style.Add("display", "inline")
                    Else
                        rowDosisCria.Style.Add("display", "none")
                    End If
                    If mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones Then
                        rowCantEmbr.Style.Add("display", "inline")
                    Else
                        rowCantEmbr.Style.Add("display", "none")
                    End If
                    lblTranPlan.Visible = False
                    hdnMultiProd.Text = ""
                End If

                usrProd.usrMadreExt.Valor = .Item("tram_embr_madre_id")
                usrProd.usrPadreExt.Valor = .Item("tram_embr_padre_id")
                mCargarCriadores(cmbCriaComp, usrClieComp.Valor.ToString, hdnRazaId.Text)
                cmbCriaComp.Valor = .Item("tram_comp_cria_id")
                hdnCriaComp.Text = cmbCriaComp.Valor.ToString
                mCargarCriadores(cmbCriaVend, usrClieVend.Valor.ToString, hdnRazaId.Text)
                cmbCriaVend.Valor = .Item("tram_vend_cria_id")
                hdnCriaVend.Text = cmbCriaVend.Valor.ToString
                If .IsNull("tram_embr_cant") Then
                    txtCantEmbr.Text = ""
                Else
                    txtCantEmbr.Text = .Item("tram_embr_cant")
                End If
                txtRecuFecha.Fecha = .Item("tram_embr_recu_fecha")
                If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones Then
                    If .IsNull("tram_embr_tede_comp_id") Then
                        hdnTEId.Text = ""
                    Else
                        hdnTEId.Text = .Item("tram_embr_tede_comp_id")
                    End If
                Else
                    If .IsNull("tram_embr_tede_vend_id") Then
                        hdnTEId.Text = ""
                    Else
                        hdnTEId.Text = .Item("tram_embr_tede_vend_id")
                    End If
                End If
                If .IsNull("_te_denun_desc") Then
                    txtTeDesc.Text = ""
                Else
                    txtTeDesc.Text = .Item("_te_denun_desc")
                End If


                'If (usrProd.usrProductoExt.cmbSexoProdExt.Valor.ToString = "1" And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionProductos) Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportaciónSemen Then
                '   rowDosisCria.Style.Add("display", "inline")
                'Else
                '   rowDosisCria.Style.Add("display", "none")
                'End If

                btnTeDenu.Visible = False
                txtTeDesc.Visible = False
                mSetearEditor(mstrTabla, False)
                mMostrarPanel(True)
            End With
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosRequ(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDeta As DataRow

            hdnRequId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)

            With ldrDeta
                cmbRequRequ.Valor = .Item("trad_requ_id")
                chkRequPend.Checked = .Item("trad_pend")
                lblRequManu.Text = .Item("_manu")
            End With

            mSetearEditor(mstrTablaRequisitos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosDocu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDoc As DataRow

            hdnDocuId.Text = E.Item.Cells(1).Text
            ldrDoc = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)

            With ldrDoc
                If .IsNull("trdo_path") Then
                    txtDocuDocu.Valor = ""
                    imgDelDocuDoc.Visible = False
                Else
                    imgDelDocuDoc.Visible = True
                    txtDocuDocu.Valor = .Item("trdo_path")
                End If
                txtDocuObse.Valor = .Item("trdo_refe")
            End With

            mSetearEditor(mstrTablaDocumentos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrObse As DataRow

            hdnObseId.Text = E.Item.Cells(1).Text
            ldrObse = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)

            With ldrObse
                cmbObseRequ.Valor = .Item("_trad_requ_id")
                txtObseObse.Valor = .Item("trao_obse")
                lblObseFecha.Text = CDate(.Item("trao_fecha")).ToString("dd/MM/yyyy")
            End With

            mSetearEditor(mstrTablaObservaciones, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosComp(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        '   Try
        '      Dim ldrDoc As DataRow

        '      hdnCompId.Text = E.Item.Cells(1).Text
        '      lblNombComp.Text = "Cliente: " & E.Item.Cells(2).Text

        '      ldrDoc = mdsDatos.Tables(mstrTablaCompradores).Select("trpe_id=" & hdnCompId.Text)(0)

        '      With ldrDoc
        '         txtObseComp.Valor = .Item("trpe_obse")
        '         txtPorcComp.Valor = .Item("trpe_porc")
        '      End With

        '      mSetearEditor(mstrTablaCompradores, False)

        '   Catch ex As Exception
        '      clsError.gManejarError(Me, ex)
        '   End Try
    End Sub

    Private Sub mLimpiarFiltros()
        txtNumeFil.Text = ""
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""

        cmbRazaFil.Limpiar()
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
        cmbEstadoFil.Limpiar()
        cmbPaisFil.Limpiar()
        cmbRazaFil.Limpiar()
        txtNumeFil.Text = ""
        usrCriaFil.cmbCriaRazaExt.Enabled = True
        usrProdFil.cmbProdRazaExt.Enabled = True
        usrCriaFil.Limpiar()
        usrProdFil.Limpiar()

        If cmbRazaFil.Valor.ToString <> "0" Then
            usrCriaFil.cmbCriaRazaExt.Enabled = False
            usrProdFil.cmbProdRazaExt.Enabled = False
        Else
            usrCriaFil.cmbCriaRazaExt.Enabled = True
            usrProdFil.cmbProdRazaExt.Enabled = True
        End If

        grdDato.Visible = False
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnRazaId.Text = ""
        lblBaja.Text = ""
        hdnMultiProd.Text = ""
        txtTeDesc.Text = ""
        hdnTEId.Text = ""
        hdnCriaComp.Text = ""
        hdnCriaVend.Text = ""
        txtInicFecha.Fecha = Now
        txtFechaTram.Fecha = Now
        txtFechaTransf.Fecha = Now
        txtFinaFecha.Text = ""
        txtCriaCant.Text = ""
        txtDosiCant.Text = ""
        txtCantEmbr.Text = ""
        txtRecuFecha.Text = ""
        cmbEsta.Limpiar()

        usrProd.Limpiar()
        Select Case mintTtraId
            Case SRA_Neg.Constantes.TramitesTipos.ImportacionSemen
                'usrProd.usrProductoExt.txtRPExt.Text = "SI "
            Case SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones
                usrProd.usrMadreExt.txtRPExt.Text = "DEI "
                usrProd.usrPadreExt.txtRPExt.Text = "DEI "
        End Select
        lblTranPlan.Visible = False
        usrProd.Visible = True
        btnTeDenu.Enabled = True

        usrClieComp.Limpiar()
        usrClieVend.Limpiar()
        If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
            usrClieVend.Activo = True
        End If
        usrClieProp.Limpiar()
        usrClieProp.Activo = mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
        cmbPais.Limpiar()
        cmbImpo.Limpiar()
        cmbCriaComp.Items.Clear()
        cmbCriaVend.Items.Clear()
        hdnCompOrig.Text = ""
        hdnVendOrig.Text = ""

        lblTitu.Text = ""
        hdnTramNro.Text = ""

        grdRequ.CurrentPageIndex = 0
        grdDocu.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0
        grdVend.CurrentPageIndex = 0
        grdComp.CurrentPageIndex = 0

        mLimpiarRequ()
        mLimpiarDocu()
        mLimpiarObse()
        'mLimpiarComp()

        mCrearDataSet("")
        mSetearEditor(mstrTabla, True)
        mShowTabs(1)
        mCargarPlantilla()
    End Sub
    Private Sub mLimpiarRequ()
        hdnRequId.Text = ""
        cmbRequRequ.Limpiar()
        chkRequPend.Checked = False
        lblRequManu.Text = "Sí"

        mSetearEditor(mstrTablaRequisitos, True)
    End Sub
    Private Sub mLimpiarDocu()
        hdnDocuId.Text = ""
        txtDocuObse.Valor = ""
        txtDocuDocu.Valor = ""
        imgDelDocuDoc.Visible = False

        mSetearEditor(mstrTablaDocumentos, True)
    End Sub
    Private Sub mLimpiarObse()
        hdnObseId.Text = ""
        cmbObseRequ.Limpiar()
        txtObseObse.Text = ""
        txtObseFecha.Fecha = Now

        mSetearEditor(mstrTablaObservaciones, True)
    End Sub
    'Private Sub mLimpiarComp()
    '   hdnCompId.Text = ""
    '   txtObseComp.Text = ""
    '   txtPorcComp.Text = ""
    '   lblNombComp.Text = ""

    '   mSetearEditor(mstrTablaCompradores, True)
    'End Sub

    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTabla
                btnAlta.Enabled = pbooAlta
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                'btnPedigree.Disabled = pbooAlta
                btnErr.Visible = Not (pbooAlta)
            Case mstrTablaRequisitos
                btnAltaRequ.Enabled = pbooAlta
                btnBajaRequ.Enabled = Not (pbooAlta)
                btnModiRequ.Enabled = Not (pbooAlta)
            Case mstrTablaDocumentos
                btnAltaDocu.Enabled = pbooAlta
                btnBajaDocu.Enabled = Not (pbooAlta)
                btnModiDocu.Enabled = Not (pbooAlta)
            Case mstrTablaObservaciones
                btnAltaObse.Enabled = pbooAlta
                btnBajaObse.Enabled = Not (pbooAlta)
                btnModiObse.Enabled = Not (pbooAlta)
                'Case mstrTablaCompradores
                '   btnModiComp.Enabled = Not (pbooAlta)
        End Select
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        lnkCabecera.Font.Bold = True
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False
        lnkVend.Font.Bold = False
        lnkComp.Font.Bold = False

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False
        panComp.Visible = False
        panVend.Visible = False

        panLinks.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet
            Dim ldsDatosProd As DataSet
            Dim lstrId As String

            ldsDatos = mGuardarDatos()
            If Not ldsDatos Is Nothing Then
                ldsDatosProd = ldsDatos.Copy
            End If

            Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy, ldsDatosProd, Context, mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones, mintTtraId, mdsDatosMadre, mdsDatosPadre)
            lstrId = lTramites.Alta()
            lblAltaId.Text = mObtenerTramite(lstrId)

            mAplicarReglas(lstrId)

            mConsultar()

            If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                mEditarTramite(lstrId)
            Else
                mMostrarPanel(False)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet
            Dim ldsDatosProd As DataSet

            ldsDatos = mGuardarDatos()

            If Not ldsDatos Is Nothing Then
                ldsDatosProd = ldsDatos.Copy
            End If

            Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy, ldsDatosProd, Context, mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones, mintTtraId, mdsDatosMadre, mdsDatosPadre)
            lTramites.Modi()

            mAplicarReglas(hdnId.Text)

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            lobjGenerica.Baja(hdnId.Text)

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDatos(ByVal pbooCierre As Boolean, ByVal pbooCierreBaja As Boolean)
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)


        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
            If hdnId.Text = "" And hdnTEId.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar una Denuncia de TE.")
            End If
        End If

        If usrClieComp.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el comprador.")
        End If

        If usrClieVend.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el vendedor.")
        End If


        If Not pbooCierreBaja Then
            Select Case mintTtraId
                Case SRA_Neg.Constantes.TramitesTipos.ImportacionProductos, SRA_Neg.Constantes.TramitesTipos.ImportacionSemen
                    If usrProd.usrProductoExt.cmbProdRazaExt.Valor.ToString = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar la Raza del Producto para el Trámite.")
                    End If

                    If usrProd.usrProductoExt.cmbSexoProdExt.Valor.ToString = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar el Sexo del Producto para el Trámite.")
                    End If

                    If clsSQLServer.gCampoValorConsul(mstrConn, "razas_consul @raza_id=" + usrProd.usrProductoExt.cmbProdRazaExt.Valor.ToString, "raza_espe_id") <> SRA_Neg.Constantes.Especies.Peliferos Then
                        If usrProd.usrProductoExt.txtProdNombExt.Valor.ToString = "" Then
                            Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre del Producto para el Trámite.")
                        End If
                    End If
                Case SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones, SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones, SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones
                    If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
                        If usrProd.usrPadreExt.Valor.ToString = "" Then
                            Throw New AccesoBD.clsErrNeg("Debe ingresar el Padre para el Trámite.")
                        End If
                        If usrProd.usrMadreExt.Valor.ToString = "" Then
                            Throw New AccesoBD.clsErrNeg("Debe ingresar la Madre para el Trámite.")
                        End If
                    End If
                    If txtCantEmbr.Valor.ToString <= "0" Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar la Cantidad de Embriones.")
                    End If
                    If txtRecuFecha.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de recuperación.")
                    End If
                    If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones And hdnId.Text = "" And hdnTEId.Text = "" Then
                        Throw New AccesoBD.clsErrNeg("Debe indicar la denuncia de transplante embrionario.")
                    End If
                Case SRA_Neg.Constantes.TramitesTipos.ExportacionProductos, SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos, _
                SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen, SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
                    If usrProd.usrProductoExt.Valor.ToString = "" And hdnMultiProd.Text <> "S" Then
                        Throw New AccesoBD.clsErrNeg("Debe ingresar el Producto para el Trámite.")
                    End If
                    Dim clie_id As Integer
                    If usrClieProp.Valor = "" Then
                        clie_id = TraerPropietario(clsSQLServer.gFormatArg(usrProd.usrProductoExt.Valor.ToString, SqlDbType.Int))
                    Else
                        clie_id = usrClieProp.Valor
                    End If
                    If clie_id = 0 Then

                        Throw New AccesoBD.clsErrNeg("Propietario no existe, porfavor verificar los datos del propietario.")
                    End If

                    If clie_id.ToString() <> usrClieVend.Valor.ToString() Then

                        Throw New AccesoBD.clsErrNeg("El vendedor no figura como propietario del Animal.")
                    End If
                    If usrClieComp.Valor = usrClieVend.Valor.ToString() Then

                        Throw New AccesoBD.clsErrNeg("El vendedor y el Comprador deben ser distintos.")
                    End If
                    If usrClieComp.Valor = clie_id Then

                        Throw New AccesoBD.clsErrNeg("El Comprador y el propietario deben ser distintos.")
                    End If
            End Select

            If pbooCierre And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos And _
                            mintTtraId <> SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen And _
                            mintTtraId <> SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
                If cmbPais.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el País para cerrar el Trámite.")
                End If


                If cmbImpo.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Despachante para cerrar el Trámite.")
                End If
            End If

            Dim lintPorcVende As Integer = 0
            Dim lintPorcCompra As Integer = 0

            'For Each ldr As DataRow In mdsDatos.Tables(mstrTablaCompradores).Select
            '   With ldr
            '      lintPorcCompra += IIf(.IsNull("trpe_porc"), 0, .Item("trpe_porc"))
            '   End With
            'Next

            'If mintTtraId = SRA_Neg.Constantes.TramitesTipos.Transferencia Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Then
            '   For Each ldr As DataRow In mdsDatos.Tables(mstrTablaVendedores).Select
            '      With ldr
            '         lintPorcVende += IIf(.IsNull("trpe_porc"), 0, .Item("trpe_porc"))
            '      End With
            '   Next
            '   If lintPorcVende <> lintPorcCompra And lintPorcVende <> 0 Then
            '      clsError.gGenerarMensajes(Me, "El porcentaje de participación de los Propietarios debe ser igual al de los Compradores.")
            '   End If
            'Else
            '   If lintPorcCompra <> 100 And lintPorcCompra <> 0 Then
            '      Throw New AccesoBD.clsErrNeg("Debe indicar un porcentaje para cada uno de los Compradores y la suma de los mismos debe ser igual a 100.")
            '   End If
            'End If

            If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionSemen Then
                If txtDosiCant.Valor.ToString = "" AndAlso txtCriaCant.Valor.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la cantidad de Crias y/o Dosis.")
                End If
            End If
        End If
    End Sub
    Private Function mObtenerTramite(ByVal pstrId) As String
        Dim lstrId As String = ""
        Try
            mstrCmd = "exec tramites_consul " & pstrId
            Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), mstrCmd)
            While (dr.Read())
                lstrId = dr.GetValue(dr.GetOrdinal("tram_nume")).ToString().Trim()
            End While
            dr.Close()
            Return (lstrId)
        Catch ex As Exception
            Return (lstrId)
        End Try
    End Function
    Private Function TraerPropietario(ByVal prod_id As Integer) As Integer
        Dim lstrCmd As String = ""
        Dim lbooResult As Boolean = False
        Dim lstrId As Integer
        Try
            lstrCmd = " exec productoPorId "
            lstrCmd += "@prdt_id=" + clsSQLServer.gFormatArg(prod_id, SqlDbType.Int)

            Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(mstrConn, lstrCmd)
            While (dr.Read())
                lstrId = dr.GetValue(dr.GetOrdinal("prdt_prop_clie_id")).ToString().Trim()

            End While
            dr.Close()

            Return lstrId
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            Return (0)
        End Try

    End Function
    Private Sub mAplicarReglas(ByVal pstrTramId As String)
        Try
            Dim lstrId As String = ""
            Dim lstrRaza As String = ""

            ''dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, mintProce)
            dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, mintProce.ToString(), "W", txtFechaTram.Text, txtFechaTram.Text)

            Select Case mintTtraId
                Case SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones, SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones, SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones
                    lstrId = pstrTramId
                    lstrRaza = usrProd.usrPadreExt.cmbProdRazaExt.Valor.ToString
                    'Limpiar errores anteriores.
                    ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTabla, lstrId)
                    Select Case mintTtraId
                        Case SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.TransferenciaEmbriones.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.TransferenciaEmbriones.ToString(), 0, 0)
                        Case SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImportacionEmbriones.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImportacionEmbriones.ToString(), 0, 0)
                        Case SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ExportacionEmbriones.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ExportacionEmbriones.ToString(), 0, 0)
                    End Select

                Case Else
                    mstrCmd = "exec " & mstrTablaProductos & "_consul @trpr_tram_id = " & pstrTramId
                    Dim dr As SqlDataReader = clsSQLServer.gExecuteQueryDR(mstrConn, mstrCmd)
                    Do While dr.Read
                        lstrId = dr.GetValue(dr.GetOrdinal("trpr_id"))
                    Loop
                    dr.Close()
                    lstrRaza = usrProd.usrProductoExt.cmbProdRazaExt.Valor.ToString()
                    'Limpiar errores anteriores.
                    ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTablaProductos, lstrId)
                    Select Case mintTtraId
                        Case SRA_Neg.Constantes.TramitesTipos.ExportacionProductos
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ExportacionProductos.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ExportacionProductos.ToString(), 0, 0)
                        Case SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImportacionProductos.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImportacionProductos.ToString(), 0, 0)
                        Case SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.TransferenciaProductos.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.TransferenciaProductos.ToString(), 0, 0)
                        Case SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ExportacionSemen.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ExportacionSemen.ToString(), 0, 0)
                        Case SRA_Neg.Constantes.TramitesTipos.ImportacionSemen
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImportacionSemen.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.ImportacionSemen.ToString(), 0, 0)
                        Case SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen
                            'Aplicar Reglas.
                            'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.TransferenciaSemen.ToString())
                            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaProductos, lstrId, lstrRaza, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.TransferenciaSemen.ToString(), 0, 0)
                    End Select
            End Select

            If hdnMsgError.Text <> "" Then
                AccesoBD.clsError.gGenerarMensajes(Me, hdnMsgError.Text)
            Else
                mstrCmd = "exec " & mstrTabla & "_aprobar @tram_id = " & pstrTramId & ", @audi_user=" & Session("sUserId").ToString()
                clsSQLServer.gExecute(mstrConn, mstrCmd)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Function mGuardarDatos() As DataSet
        Dim ldsDatosProd As DataSet
        Dim lbooCierre As Boolean = False
        Dim lbooCierreBaja As Boolean = False

        'If hdnCompOrig.Text <> usrClieComp.Valor.ToString And usrClieComp.Valor.ToString <> "" Then
        '   mCargarPersonas(mstrTablaCompradores, usrClieComp.Valor.ToString, grdComp, hdnCompOrig)
        'End If

        'If hdnVendOrig.Text <> usrClieVend.Valor.ToString And usrClieVend.Valor.ToString <> "" Then
        '   mCargarPersonas(mstrTablaVendedores, usrClieVend.Valor.ToString, grdVend, hdnVendOrig)
        'End If

        If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_pend = 1").GetUpperBound(0) < 0 And grdRequ.Items.Count > 0 Then
            lbooCierre = True
        End If

        If cmbEsta.Valor.ToString = SRA_Neg.Constantes.Estados.Tramites_Baja Then
            lbooCierreBaja = True
        End If

        mValidarDatos(lbooCierre, lbooCierreBaja)

        'mGuardarDatosCompFinal()
        'mGuardarDatosVendFinal()

        If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then
            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("tram_ttra_id") = mintTtraId
                .Item("tram_dosi_cant") = txtDosiCant.Valor
                .Item("tram_cria_cant") = txtCriaCant.Valor
                If hdnMultiProd.Text <> "S" Then
                    .Item("tram_raza_id") = IIf(usrProd.usrProductoExt.cmbProdRazaExt.Valor.ToString = "", usrProd.usrPadreExt.cmbProdRazaExt.Valor, usrProd.usrProductoExt.cmbProdRazaExt.Valor)
                End If
                .Item("tram_pres_fecha") = txtFechaTram.Fecha
                .Item("tram_esta_id") = cmbEsta.Valor
                .Item("tram_impr_id") = cmbImpo.Valor
                .Item("tram_pais_id") = cmbPais.Valor
                .Item("tram_comp_clie_id") = usrClieComp.Valor
                If hdnCriaComp.Text <> "" Then
                    .Item("tram_comp_cria_id") = hdnCriaComp.Text
                Else
                    .Item("tram_comp_cria_id") = DBNull.Value
                End If
                .Item("tram_vend_clie_id") = usrClieVend.Valor
                .Item("tram_vend_cria_id") = DBNull.Value
                If Not cmbCriaVend.Valor Is DBNull.Value Then
                    If cmbCriaVend.Valor <> "" Then
                        .Item("tram_vend_cria_id") = cmbCriaVend.Valor
                    End If
                End If
                .Item("tram_prop_clie_id") = usrClieProp.Valor

                If .IsNull("tram_inic_fecha") Then
                    .Item("tram_inic_fecha") = Today
                End If

                .Item("tram_baja_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                .Item("tram_fina_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)

                If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
                    .Item("tram_embr_madre_id") = usrProd.usrMadreExt.Valor
                    .Item("tram_embr_padre_id") = usrProd.usrPadreExt.Valor
                    .Item("tram_embr_cant") = txtCantEmbr.Text
                    .Item("tram_embr_recu_fecha") = txtRecuFecha.Fecha
                    'If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                    '    .Item("tram_embr_tede_vend_id") = hdnTEId.Text
                    'Else
                    '    If hdnTEId.Text <> "" Then
                    '        .Item("tram_embr_tede_comp_id") = hdnTEId.Text
                    '    End If
                    'End If
                    If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones Then
                        .Item("tram_embr_tede_vend_id") = usrClieVend.Valor
                    End If
                Else
                    If hdnMultiProd.Text <> "S" And mNuevoProducto(usrProd.usrProductoExt, hdnId.Text) Then
                        usrProd.GenerarSRANume = lbooCierre
                        ldsDatosProd = mGuardarDatosProd()
                    End If
                End If

            End With
        End If

        Dim ldrDatos As DataRow

        'If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
        'GUARDA LOS DATOS DE TRAMITES_PRODUCTOS
        If mdsDatos.Tables(mstrTablaProductos).Rows.Count = 0 Then
            ldrDatos = mdsDatos.Tables(mstrTablaProductos).NewRow
            ldrDatos.Item("trpr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaProductos), "trpr_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaProductos).Select()(0)
        End If

        With ldrDatos
            .Item("trpr_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones And mintTtraId <> SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
                .Item("trpr_prdt_id") = IIf(usrProd.usrProductoExt.Valor.ToString = "", DBNull.Value, usrProd.usrProductoExt.Valor)
            Else
                .Item("trpr_prdt_id") = usrProd.usrPadreExt.Valor
            End If
            .Item("trpr_asoc_id") = usrProd.usrProductoExt.cmbProdAsocExt.Valor
            .Item("trpr_padre_asoc_id") = usrProd.usrPadreExt.Valor
            .Item("trpr_madre_asoc_id") = usrProd.usrMadreExt.Valor
            .Item("trpr_audi_user") = Session("sUserId").ToString()
            .Item("trpr_baja_fecha") = DBNull.Value
            If mdsDatos.Tables(mstrTablaProductos).Rows.Count = 0 Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
        'End If

        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
            'solo para importacion de embriones si no existen los padres los da de alta.
            mdsDatosMadre = Nothing
            mdsDatosPadre = Nothing
            If mNuevoProducto(usrProd.usrMadreExt, hdnId.Text) Then
                mdsDatosMadre = mGuardarDatosPadre(usrProd.usrMadreExt, usrProd.usrMadreExt.Valor.ToString, lbooCierre)
            End If
            If mNuevoProducto(usrProd.usrPadreExt, hdnId.Text) Then
                mdsDatosPadre = mGuardarDatosPadre(usrProd.usrPadreExt, usrProd.usrPadreExt.Valor.ToString, lbooCierre)
            End If
        End If

        Return ldsDatosProd
    End Function

    Private Function mNuevoProducto(ByVal pusrProd As SRA.usrProdDeriv, ByVal pstrTramId As String) As Boolean
        Dim lstrNuevo As String
        If pstrTramId = "" Then
            If pusrProd.Valor.ToString = "" Then
                Return (True)
            Else
                Return (False)
            End If
        Else
            If pusrProd.Valor.ToString = "" Then
                Return (True)
            Else
                mstrCmd = "producto_tramite_determinar @prdt_id=" & pusrProd.Valor.ToString & ",@tram_id=" & pstrTramId
                lstrNuevo = clsSQLServer.gCampoValorConsul(mstrConn, mstrCmd, "resul")
                If lstrNuevo = "S" Then
                    Return (True)
                Else
                    Return (False)
                End If
            End If
        End If
    End Function

    Public Function mGuardarDatosPadre(ByVal pusrProd As SRA.usrProdDeriv, ByVal pstrId As String, ByVal pbooCierre As Boolean) As DataSet
        Dim ldsDatos As DataSet
        Dim lstrId As String
        Dim ldrProd As DataRow
        Dim ldrNume As DataRow

        ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, SRA_Neg.Constantes.gTab_Productos, "", "")
        With ldsDatos
            .Tables(0).TableName = SRA_Neg.Constantes.gTab_Productos
            .Tables(1).TableName = SRA_Neg.Constantes.gTab_ProductosAnalisis
            .Tables(2).TableName = SRA_Neg.Constantes.gTab_ProductosNumeros
        End With

        If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select.GetUpperBound(0) = -1 Then
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).NewRow
            ldrProd.Table.Rows.Add(ldrProd)
        Else
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select()(0)
        End If

        'productos
        With ldrProd
            lstrId = clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
            .Item("prdt_id") = lstrId
            .Item("prdt_raza_id") = pusrProd.cmbProdRazaExt.Valor
            .Item("prdt_sexo") = pusrProd.cmbSexoProdExt.ValorBool
            .Item("prdt_nomb") = pusrProd.txtProdNombExt.Valor
            '.Item("prdt_px") = txtPX.Text
            '.Item("prdt_naci_fecha") = txtFechaNac.Fecha
            .Item("prdt_tran_fecha") = txtFechaTransf.Fecha

            .Item("prdt_rp_nume") = IIf(IsNumeric(Trim(pusrProd.txtRPExt.Text)), Trim(pusrProd.txtRPExt.Text), DBNull.Value)
            .Item("prdt_rp") = IIf(Trim(pusrProd.txtRPExt.Text) = "", DBNull.Value, IIf(pusrProd.txtRPExt.Text.ToUpper.IndexOf("DEI") = 0, "", "DEI ") & Trim(pusrProd.txtRPExt.Text))
            .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", "E", .Item("prdt_ndad"))
            .Item("prdt_ori_asoc_id") = pusrProd.cmbProdAsocExt.Valor
            .Item("prdt_ori_asoc_nume") = pusrProd.txtCodiExt.Valor
            ' .Item("prdt_ori_asoc_nume_lab") = pusrProd.txtCodiExtLab.Valor
            .Item("prdt_prop_clie_id") = usrClieVend.Valor
            .Item("generar_numero") = pbooCierre
            .Item("prdt_vari_id") = cmbVari.Valor
        End With

        'productos_numeros
        If pusrProd.txtCodiExt.Text <> "" Then
            If ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select.GetUpperBound(0) = -1 Then
                ldrNume = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).NewRow
                ldrNume.Table.Rows.Add(ldrNume)
            Else
                ldrNume = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select()(0)
            End If
            With ldrNume
                .Item("ptnu_id") = clsSQLServer.gFormatArg("", SqlDbType.Int)
                .Item("ptnu_prdt_id") = lstrId
                .Item("ptnu_asoc_id") = pusrProd.cmbProdAsocExt.Valor
                .Item("ptnu_nume") = pusrProd.txtCodiExt.Text
            End With
        End If

        Return ldsDatos
    End Function

    'REQUISISTOS
    Private Sub mActualizarRequ(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosRequ(pbooAlta)
            mLimpiarRequ()
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarRequ(ByVal pbooAlta As Boolean)
        If cmbRequRequ.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
        End If
        If pbooAlta Then
            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Requisito existente.")
            End If
        End If
    End Sub
    Private Sub mGuardarDatosRequ(ByVal pbooAlta As Boolean)
        Dim ldrDatos As DataRow
        mValidarRequ(pbooAlta)

        If hdnRequId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
            ldrDatos.Item("trad_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRequisitos), "trad_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)
        End If

        With ldrDatos
            .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("trad_requ_id") = cmbRequRequ.Valor
            .Item("trad_pend") = chkRequPend.Checked
            .Item("trad_obli") = True
            .Item("trad_manu") = IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
            .Item("trad_baja_fecha") = DBNull.Value
            .Item("trad_audi_user") = Session("sUserId").ToString()
            .Item("_requ_desc") = cmbRequRequ.SelectedItem.Text
            .Item("_pend") = IIf(chkRequPend.Checked, "Sí", "No")
            .Item("_manu") = "Sí"
            .Item("_estado") = "Activo"

            If hdnRequId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With

    End Sub
    'DOCUMENTOS
    Private Sub mActualizarDocu()
        Try
            mGuardarDatosDocu()
            mLimpiarDocu()
            mConsultarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDocu()
        If txtDocuDocu.Valor.ToString = "" And filDocuDocu.Value = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
        End If

        If txtDocuObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
        End If
    End Sub
    Private Sub mGuardarDatosDocu()
        Dim ldrDatos As DataRow
        Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_tram_docu_path")

        mValidarDocu()

        If hdnDocuId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).NewRow
            ldrDatos.Item("trdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocumentos), "trdo_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)
        End If

        With ldrDatos
            .Item("trdo_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            If filDocuDocu.Value <> "" Then
                .Item("trdo_path") = filDocuDocu.Value
            Else
                .Item("trdo_path") = txtDocuDocu.Valor
            End If
            If Not .IsNull("trdo_path") Then
                .Item("trdo_path") = .Item("trdo_path").Substring(.Item("trdo_path").LastIndexOf("\") + 1)
            End If
            .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(.Item("trdo_id"), "-", "m") + "__" + .Item("trdo_path")
            .Item("trdo_refe") = txtDocuObse.Valor
            .Item("trdo_baja_fecha") = DBNull.Value
            .Item("trdo_audi_user") = Session("sUserId").ToString()
            .Item("_estado") = "Activo"

            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocuDocu)

            If hdnDocuId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub
    'OBSERVACIONES
    Private Sub mActualizarObse()
        Try
            mGuardarDatosObse()
            mLimpiarObse()
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarObse()
        If txtObseObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Observación.")
        End If
        If txtObseFecha.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de la Observación.")
        End If
    End Sub
    Private Sub mGuardarDatosObse()
        Dim ldrDatos As DataRow
        mValidarObse()

        If hdnObseId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).NewRow
            ldrDatos.Item("trao_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaObservaciones), "trao_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)
        End If

        With ldrDatos
            .Item("trao_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            If cmbObseRequ.Valor.ToString = "" Then
                .Item("trao_trad_id") = DBNull.Value
            Else
                .Item("trao_trad_id") = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_requ_id=" + cmbObseRequ.Valor.ToString)(0).Item("trad_id")
            End If
            .Item("_requ_desc") = IIf(cmbObseRequ.Valor.ToString = "", "COMENTARIO GENERAL", cmbObseRequ.SelectedItem.Text)
            .Item("_trad_requ_id") = IIf(cmbObseRequ.Valor.ToString = "", "0", cmbObseRequ.Valor)
            .Item("trao_obse") = txtObseObse.Valor
            .Item("trao_fecha") = txtObseFecha.Fecha
            .Item("trao_audi_user") = Session("sUserId").ToString()

            If hdnObseId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub
    'VENDEDORES
    'Private Sub mGuardarDatosVendFinal()
    '   For Each lDr As DataRow In mdsDatos.Tables(mstrTablaVendedores).Select()
    '      With lDr
    '         .Item("trpe_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
    '         .Item("trpe_audi_user") = Session("sUserId").ToString()
    '         .Item("trpe_rol") = SRA_Neg.Constantes.Roles.Vendedor
    '         .Item("trpe_clie_id") = IIf(lDr.IsNull("_clie_id"), .Item("trpe_clie_id"), .Item("_clie_id"))
    '      End With
    '   Next
    'End Sub
    'COMPRADORES
    'Private Sub mActualizarComp()
    '   Try
    '      mGuardarDatosComp()
    '      mLimpiarComp()
    '      mConsultarComp()
    '   Catch ex As Exception
    '      clsError.gManejarError(Me, ex)
    '   End Try
    'End Sub
    'Private Sub mValidarComp()

    '   If txtPorcComp.Valor.ToString = "" Then
    '      Throw New AccesoBD.clsErrNeg("Debe indicar el Porcentaje.")
    '   End If
    '   Select Case mintTtraId
    '      Case SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
    '      Case SRA_Neg.Constantes.TramitesTipos.Exportación, _
    '           SRA_Neg.Constantes.TramitesTipos.ExportaciónSemen
    '      Case SRA_Neg.Constantes.TramitesTipos.Transferencia, _
    '           SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen
    '   End Select
    'End Sub
    'Private Sub mGuardarDatosCompFinal()
    '   For Each lDr As DataRow In mdsDatos.Tables(mstrTablaCompradores).Select()
    '      With lDr
    '         .Item("trpe_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
    '         .Item("trpe_audi_user") = Session("sUserId").ToString()
    '         .Item("trpe_rol") = SRA_Neg.Constantes.Roles.Comprador
    '         .Item("trpe_clie_id") = IIf(lDr.IsNull("_clie_id"), .Item("trpe_clie_id"), .Item("_clie_id"))
    '      End With
    '   Next
    'End Sub
    'Private Sub mGuardarDatosComp()
    '   Dim ldrDatos As DataRow

    '   mValidarComp()

    '   If hdnCompId.Text = "" Then
    '      ldrDatos = mdsDatos.Tables(mstrTablaCompradores).NewRow
    '      ldrDatos.Item("trpe_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaCompradores), "trpe_id")
    '   Else
    '      ldrDatos = mdsDatos.Tables(mstrTablaCompradores).Select("trpe_id=" & hdnCompId.Text)(0)
    '   End If

    '   With ldrDatos
    '      .Item("trpe_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
    '      .Item("trpe_clie_id") = IIf(.IsNull("_clie_id"), .Item("trpe_clie_id"), .Item("_clie_id"))
    '      .Item("trpe_obse") = txtObseComp.Valor
    '      .Item("trpe_porc") = txtPorcComp.Valor
    '      .Item("trpe_audi_user") = Session("sUserId").ToString()
    '      If hdnCompId.Text = "" Then
    '         .Table.Rows.Add(ldrDatos)
    '      End If
    '   End With
    'End Sub
    'PRODUCTOS
    Private Function mGuardarDatosProd() As DataSet
        Dim ldsDatosProd As DataSet
        Dim ldrDatos As DataRow

        usrProd.NdadProd = "I"
        usrProd.NdadPadres = "E"
        If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_pend = 1").GetUpperBound(0) < 0 And usrClieComp.Valor.ToString <> "" Then
            usrProd.Propietario = usrClieComp.Valor
        End If

        ''usrProd.Tramite = mintTtraId
        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen And usrProd.usrProductoExt.txtRPExt.Text <> "" Then
            If usrProd.usrProductoExt.txtRPExt.Text.Substring(0, 3).ToUpper() <> "SI " Then
                usrProd.usrProductoExt.txtRPExt.Text = "SI " + usrProd.usrProductoExt.txtRPExt.Text
            End If
        End If
        ldsDatosProd = usrProd.GuardarDatos

        Return (ldsDatosProd)
    End Function
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panCabecera.Visible = False
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False
        panComp.Visible = False
        panVend.Visible = False

        lnkCabecera.Font.Bold = False
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False
        lnkVend.Font.Bold = False
        lnkComp.Font.Bold = False

        lblTitu.Text = ""

        If origen <> 1 And usrProd.usrProductoExt.Valor.ToString <> "" Then
            lblTitu.Text = "Producto: " & usrProd.usrProductoExt.cmbProdRazaExt.SelectedItem.Text & " - " & usrProd.usrProductoExt.cmbSexoProdExt.SelectedItem.Text & " - " & usrProd.usrProductoExt.txtProdNombExt.Valor.ToString
        End If

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Text = hdnTramNro.Text
            Case 2
                panRequ.Visible = True
                lnkRequ.Font.Bold = True
            Case 3
                panDocu.Visible = True
                lnkDocu.Font.Bold = True
            Case 4
                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = "trad_baja_fecha is null"

                cmbObseRequ.DataTextField = "_requ_desc"
                cmbObseRequ.DataValueField = "trad_requ_id"
                cmbObseRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
                cmbObseRequ.DataBind()
                cmbObseRequ.Items.Insert(0, "(Seleccione)")
                cmbObseRequ.Items(0).Value = ""
                cmbObseRequ.Valor = ""

                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = ""

                panObse.Visible = True
                lnkObse.Font.Bold = True
                'Case 5
                '   panVend.Visible = True
                '   lnkVend.Font.Bold = True
                '   If hdnVendOrig.Text <> IIf(mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos, usrClieVend.Valor.ToString, usrClieProp.Valor.ToString) Or mdsDatos.Tables(mstrTablaVendedores).Select().GetUpperBound(0) = -1 Then
                '      If IIf(mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos, usrClieVend.Valor.ToString, usrClieProp.Valor.ToString) <> "" Then
                '         mCargarPersonas(mstrTablaVendedores, IIf(mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos, usrClieVend.Valor.ToString, usrClieProp.Valor.ToString), grdVend, hdnVendOrig)
                '      Else
                '         mLimpiarPersonas(mstrTablaVendedores, grdVend, hdnVendOrig)
                '      End If
                '      If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                '         If usrClieVend.Valor.ToString <> "" Then
                '           mCargarPersonas(mstrTablaVendedores, usrClieVend.Valor.ToString, grdVend, hdnVendOrig)
                '         Else
                '           mLimpiarPersonas(mstrTablaVendedores, grdVend, hdnVendOrig)
                '         End If
                '      End If
                '   End If
                'Case 6
                '   panComp.Visible = True
                '   lnkComp.Font.Bold = True
                '   If hdnCompOrig.Text <> usrClieComp.Valor.ToString Then
                '      If usrClieComp.Valor.ToString <> "" Then
                '         mCargarPersonas(mstrTablaCompradores, usrClieComp.Valor.ToString, grdComp, hdnCompOrig)
                '      Else
                '         mLimpiarPersonas(mstrTablaCompradores, grdComp, hdnCompOrig)
                '      End If
                '   End If
        End Select
    End Sub
    Private Sub mLimpiarPersonas(ByVal pstrTabla As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
        For Each lDr As DataRow In mdsDatos.Tables(pstrTabla).Select()
            If lDr.Item("trpe_id") > 0 Then
                lDr.Delete()
            Else
                mdsDatos.Tables(pstrTabla).Rows.Remove(lDr)
            End If
        Next

        If mdsDatos.Tables(pstrTabla).Select.GetUpperBound(0) = -1 Then pctrHdnOrig.Text = ""

        pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
        pgrdGrilla.DataBind()
    End Sub
    'Private Sub mCargarPersonas(ByVal pstrTabla As String, ByVal pstrCliente As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
    '   Dim ldsDatos As DataSet

    '   mstrCmd = "@clie_id=" & pstrCliente
    '   If usrProd.usrProductoExt.Valor.ToString <> "" Then
    '      mstrCmd += ",@prdt_id=" & IIf(pstrTabla = mstrTablaCompradores, "null", usrProd.usrProductoExt.Valor.ToString)
    '   End If

    '   ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, "tramites_personas", mstrCmd)

    '   mLimpiarPersonas(pstrTabla, pgrdGrilla, pctrHdnOrig)

    '   For Each lDr As DataRow In ldsDatos.Tables(0).Select()
    '      lDr.Item("trpe_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(pstrTabla), "trpe_id")
    '      mdsDatos.Tables(pstrTabla).ImportRow(lDr)
    '   Next

    '   If pstrTabla = mstrTablaCompradores Then
    '      If mdsDatos.Tables(pstrTabla).Select().GetUpperBound(0) = 0 Then
    '         Dim ldr As DataRow = mdsDatos.Tables(pstrTabla).Select().GetValue(0)
    '         ldr.Item("trpe_porc") = 100
    '      End If
    '   End If

    '   pctrHdnOrig.Text = pstrCliente

    '   pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
    '   pgrdGrilla.DataBind()
    '   Session(mSess(mstrTabla)) = mdsDatos
    '   mLimpiarComp()
    'End Sub
    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    'REQUISISTOS
    Private Sub btnAltaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRequ.Click
        mActualizarRequ(True)
    End Sub
    Private Sub btnLimpRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRequ.Click
        mLimpiarRequ()
    End Sub
    Private Sub btnBajaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRequ.Click
        Try
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaObservaciones).Select("trao_trad_id=" + hdnRequId.Text)
                odrDeta.Delete()
            Next

            mConsultarObse()
            mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0).Delete()
            grdRequ.CurrentPageIndex = 0
            mConsultarRequ()
            mLimpiarRequ()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRequ.Click
        mActualizarRequ(False)
    End Sub
    'DOCUMENTOS
    Private Sub btnAltaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocu.Click
        mActualizarDocu()
    End Sub
    Private Sub btnLimpDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocu.Click
        mLimpiarDocu()
    End Sub
    Private Sub btnBajaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocu.Click
        Try
            mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0).Delete()
            grdDocu.CurrentPageIndex = 0
            mConsultarDocu()
            mLimpiarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocu.Click
        mActualizarDocu()
    End Sub
    'OBSERVACIONES
    Private Sub btnAltaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaObse.Click
        mActualizarObse()
    End Sub
    Private Sub btnLimpObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpObse.Click
        mLimpiarObse()
    End Sub
    Private Sub btnBajaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaObse.Click
        Try
            mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0).Delete()
            grdObse.CurrentPageIndex = 0
            mConsultarObse()
            mLimpiarObse()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiObse.Click
        mActualizarObse()
    End Sub
    'COMPRADORES
    Private Sub btnModiComp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiComp.Click
        'mActualizarComp()
    End Sub
    Private Sub btnLimpComp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpComp.Click
        'mLimpiarComp()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRequ.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkObse.Click
        mShowTabs(4)
    End Sub
    Private Sub lnkVend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkVend.Click
        mShowTabs(5)
    End Sub
    Private Sub lnkComp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkComp.Click
        mShowTabs(6)
    End Sub
#End Region

#Region "Opciones de POP"
    Private Sub mCargarPlantilla()
        If mstrTrapId <> "" Then

            Dim lDs As New DataSet
            lDs = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Tramites_Plantilla, "@SinBaja=1,@trap_id=" + mstrTrapId)
            Dim ldrDatos As DataRow

            mdsDatos.Tables(mstrTablaRequisitos).Clear()

            For Each ldrOri As DataRow In lDs.Tables(1).Select
                ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                With ldrDatos
                    .Item("trad_id") = clsSQLServer.gObtenerId(.Table, "trad_id")
                    .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    .Item("trad_baja_fecha") = DBNull.Value
                    .Item("trad_requ_id") = ldrOri.Item("trpd_requ_id")
                    .Item("trad_obli") = ldrOri.Item("trpd_obli")
                    .Item("trad_pend") = True
                    .Item("trad_manu") = False

                    .Item("_requ_desc") = ldrOri.Item("_requisito")
                    .Item("_obli") = ldrOri.Item("_obligatorio")
                    .Item("_pend") = "Sí"
                    .Item("_manu") = "No"
                    .Item("_estado") = ldrOri.Item("_estado")

                    .Table.Rows.Add(ldrDatos)
                End With
            Next

            Dim lstrRazas As String = ""
            Dim lstrEspecies As String = ""

            For Each ldrOri As DataRow In lDs.Tables(2).Select
                With ldrOri
                    If .IsNull("trdr_raza_id") Then
                        If lstrEspecies.Length > 0 Then lstrEspecies += ","
                        lstrEspecies += .Item("trdr_espe_id").ToString
                    Else
                        If lstrRazas.Length > 0 Then lstrRazas += ","
                        lstrRazas += .Item("trdr_raza_id").ToString
                    End If
                End With
            Next

            usrProd.usrProductoExt.FiltroRazas = "@raza_ids = " & IIf(lstrRazas = "", "null", "'" & lstrRazas & "'") & ",@raza_espe_ids = " & IIf(lstrEspecies = "", "null", "'" & lstrEspecies & "'")
            usrProd.usrProductoExt.mCargarRazas()

            mConsultarRequ()
        End If
    End Sub
    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            mstrTrapId = hdnDatosPop.Text
            mAgregar()
            hdnDatosPop.Text = ""
            Session("mstrTrapId") = mstrTrapId

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub hdnDatosTEPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosTEPop.TextChanged
        Try
            hdnTEId.Text = ""
            txtRecuFecha.Text = ""
            txtTeDesc.Text = ""
            txtCantEmbr.Text = ""
            usrProd.usrMadreExt.Valor = ""
            usrProd.usrPadreExt.Valor = ""

            If (hdnDatosTEPop.Text <> "") Then
                hdnTEId.Text = hdnDatosTEPop.Text
                mDescripTE(hdnDatosTEPop.Text)
                hdnDatosTEPop.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mDescripTE(ByVal pstrId As String)
        Dim lsrtFiltro As String = " @tede_id =" + clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)

        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.te_denun_tramite_busq " & lsrtFiltro)
        For Each ldr As DataRow In ds.Tables(0).Select
            With ldr
                txtTeDesc.Text = "Denuncia TE Nro.: " + Convert.ToString(.Item("tede_nume"))
                txtCantEmbr.Text = .Item("tede_embr")
                usrProd.usrMadreExt.Valor = .Item("tede_madr_prdt_id")
                usrProd.usrPadreExt.Valor = .Item("tede_pad1_prdt_id")
                txtRecuFecha.Fecha = .Item("tede_recu_fecha")
                usrClieVend.Valor = .Item("tede_clie_id")
                mCargarCriadores(cmbCriaVend, usrClieVend.Valor.ToString, usrProd.usrMadreExt.cmbProdRazaExt.Valor.ToString)
                cmbCriaVend.Valor = .Item("tede_cria_id")
                If .IsNull("tede_cria_id") Then
                    hdnCriaVend.Text = ""
                Else
                    hdnCriaVend.Text = .Item("tede_cria_id")
                End If
            End With
        Next

    End Sub

End Class