<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CentrosImplantes" CodeFile="CentrosImplantes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Centros de Implantes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/direcciones.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('')" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Centros de Implantes</asp:label></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderStyle="Solid"
												BorderWidth="0">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblCodigoFil" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:numberbox id="txtCodigoFil" runat="server" cssclass="cuadrotexto" Width="72px"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 14px" vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																					<asp:label id="Label1" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																				<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																					<UC1:CLIE id="usrClieFilt" runat="server" Saltos="" Ancho="800" FilFanta="true" FilClaveUnica="True"
																						FilSociNume="False" FilDocuNume="true" FilLegaNume="True" FilCriaNume="False" FilCUIT="True"
																						Tabla="Clientes"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblDenominacionFil" runat="server" cssclass="titulo">Denominaci�n:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:TextBoxtab id="txtDenominacionFil" runat="server" cssclass="cuadrotexto" Width="568px"></cc1:TextBoxtab></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblVetACargoFil" runat="server" cssclass="titulo">Veterinario a Cargo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:TextBoxtab id="txtVeterinarioCargoFil" runat="server" cssclass="cuadrotexto" Width="180px"></cc1:TextBoxtab></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:label id="lblPaisFil" runat="server" cssclass="titulo">Pa�s:&nbsp;</asp:label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbDirePaisFil" class="combo" runat="server" Width="260px" AceptaNull="false"
																						onchange="mCargarProvincias('Fil')" NomOper="paises_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:label id="lblProvFil" runat="server" cssclass="titulo">Provincia:&nbsp;</asp:label>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbDirePciaFil" class="combo" runat="server" Width="260px" AceptaNull="false"
																						onchange="mCargarLocalidades('Fil')" NomOper="provincias_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 25%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:label id="lblLocaFil" runat="server" cssclass="titulo">Localidad:&nbsp;</asp:label></TD>
																				<TD style="WIDTH: 75%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbDireLocaFil" class="combo" runat="server" Width="260px" AceptaNull="false"
																						NomOper="localidades_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="2" align="center"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
												AutoGenerateColumns="False">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="imcr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="imcr_codi" ReadOnly="True" ItemStyle-HorizontalAlign="left" HeaderText="C&#243;digo">
														<HeaderStyle HorizontalAlign="center"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="imcr_deno" ReadOnly="True" HeaderText="Denominaci&#243;n">
														<HeaderStyle Width="45%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="imcr_vete" HeaderText="Veterinario a Cargo">
														<HeaderStyle Width="40%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="imcr_clie_id" HeaderText="Nro Cliente" ItemStyle-HorizontalAlign="center">
														<HeaderStyle Width="30%" HorizontalAlign="center"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado">
														<HeaderStyle Width="7%"></HeaderStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<tr>
										<TD colSpan="2"></TD>
									</tr>
									<TR>
										<TD style="HEIGHT: 9px" height="9"></TD>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
												IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
												ImageOver="btnNuev2.gif" ForeColor="Transparent" ToolTip="Nuevo Centro de Implantes" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									</TR>
									<tr>
										<TD colSpan="2"></TD>
									</tr>
									<TR>
										<TD></TD>
										<TD colSpan="2" align="center"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False" Font-Bold="True"> General</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDirecciones" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Direcciones</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkTelefonos" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Tel�fonos</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkMails" runat="server" cssclass="solapa"
																Width="80px" Height="21px" CausesValidation="False"> Mails</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkAutorizados" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Autorizados</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="2" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:numberbox id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="100px"></CC1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDenominacion" runat="server" cssclass="titulo">Denominaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtDenominacion" runat="server" cssclass="cuadrotexto" Width="100%" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 22px" align="right">
																			<asp:Label id="lblVeterinarioCargo" runat="server" cssclass="titulo">Veterinario a Cargo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 22px">
																			<CC1:TEXTBOXTAB id="txtVeterinarioCargo" runat="server" cssclass="cuadrotexto" Width="100%" obligatorio="true"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblNroHabSenasa" runat="server" cssclass="titulo">Nro Habilitacion SENASA:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtNroHabSenasa" runat="server" cssclass="cuadrotexto" Width="100%" Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaAlta" runat="server" cssclass="titulo">Fecha de Alta:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaAlta" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaBaja" runat="server" cssclass="titulo">Fecha de Baja:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaBaja" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblHabitado" runat="server" cssclass="titulo">Habilitado desde:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaHabiDesde" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label id="lblHabitadohasta" runat="server" cssclass="titulo">Hasta:</asp:Label>
																			<cc1:DateBox id="txtFechaHabihasta" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" vAlign="top" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																			<UC1:CLIE id="usrClie" runat="server" Saltos="" Ancho="800" FilFanta="true" FilClaveUnica="True"
																				FilSociNume="False" FilDocuNume="true" FilLegaNume="True" FilCriaNume="False" FilCUIT="True"
																				Tabla="Clientes"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblEstado" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox id="cmbEstado" class="combo" runat="server" Width="248px" obligatorio="true" nomoper="estados_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDirecciones" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TablaDirecciones" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdDire" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosDire" OnPageIndexChanged="DataGridDire_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="diim_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="diim_dire" ReadOnly="True" HeaderText="Direcci�n">
																						<HeaderStyle Width="40%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_localidad" ReadOnly="True" HeaderText="Localidad">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="diim_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="30%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_default" ReadOnly="True" HeaderText="Default" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDirePais" runat="server" cssclass="titulo">Pais:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox id="cmbDirePais" class="combo" runat="server" Width="260px" onchange="mCargarProvincias('')"
																				NomOper="paises_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																			<asp:Label id="llbDirePcia" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox id="cmbDirePcia" class="combo" runat="server" Width="260px" onchange="mCargarLocalidades('')"
																				NomOper="provincias_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblDireLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox id="cmbDireLoca" class="combo" runat="server" Width="260px" onchange="mSelecCP('');"
																				nomoper="localidades_cargar"></cc1:combobox>&nbsp;<BUTTON style="WIDTH: 25%" id="btnLoca" class="boton" onclick="btnLoca_click();" runat="server"
																				value="Detalles">Localidades</BUTTON></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblDireCpos" runat="server" cssclass="titulo">C�digo Postal:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:CODPOSTALBOX id="txtDireCP" runat="server" cssclass="cuadrotexto" Width="77px" Obligatorio="True"></CC1:CODPOSTALBOX></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 220px; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblDireDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:textboxtab id="txtDireDire" runat="server" cssclass="textolibre" Width="400px" height="46px"
																				Rows="2" TextMode="MultiLine"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblDirRef" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtDireRef" runat="server" cssclass="cuadrotexto" Width="500px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																		<TD style="HEIGHT: 16px">
																			<asp:CheckBox id="chkDireDefalt" Runat="server" Checked="false" CssClass="titulo" Text="Tomar por Default"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBajaDire" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnAltaDire" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaDire" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiDire" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpDire" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panTelefonos" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TablaTelefonos" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdTele" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosTele" OnPageIndexChanged="DataGridTele_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="teim_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_TipoTele" ReadOnly="True" HeaderText="Tipo">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="teim_area_code" ReadOnly="True" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					<asp:BoundColumn DataField="teim_tele" ReadOnly="True" HeaderText="Tel�fono">
																						<HeaderStyle Width="30%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="teim_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="45%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 208px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblTeleTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:combobox id="cmbTeleTipo" class="combo" runat="server" Width="176px" nomoper="tele_tipos_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 208px; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblTeleAreaCode" runat="server" cssclass="titulo">C�digo �rea:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtTeleAreaCode" runat="server" cssclass="cuadrotexto" Width="50px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 208px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblTeleTele" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtTeleTele" runat="server" cssclass="cuadrotexto" Width="376px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 208px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblTeleRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtTeleRefe" runat="server" cssclass="cuadrotexto" Width="376px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBajaTele" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnAltaTele" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaTele" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiTele" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpTele" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panMails" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TablaMails" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdMail" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosMail" OnPageIndexChanged="DataGridMail_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="maim_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="maim_mail" ReadOnly="True" HeaderText="Mail">
																						<HeaderStyle Width="40%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="maim_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 211px; HEIGHT: 16px" align="right">
																			<asp:Label id="lblMailMail" runat="server" cssclass="titulo">Direcci�n Mail:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtMailMail" runat="server" cssclass="cuadrotexto" Width="392px" EsMail="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 211px; HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblMailRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<CC1:TEXTBOXTAB id="txtMailRefe" runat="server" cssclass="cuadrotexto" Width="420px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBajaMail" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnAltaMail" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaMail" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiMail" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpMail" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panAutorizados" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE style="WIDTH: 100%" id="TablaAutorizados" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdAutor" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosAutor" OnPageIndexChanged="DataGridAutor_Page" CellPadding="1"
																				GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="auim_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="auim_nyap" ReadOnly="True" HeaderText="Apellido y Nombre">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="auim_obse" ReadOnly="True" HeaderText="Observaciones">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblAutorNomAp" runat="server" cssclass="titulo">Nombre y Apellido:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:textboxtab id="txtAutorNomAp" runat="server" cssclass="textolibre" Width="400px" height="46px"
																				Rows="2" TextMode="MultiLine"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" vAlign="top" align="right">
																			<asp:Label id="lblAutorObs" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px">
																			<cc1:textboxtab id="txtAutorObs" runat="server" cssclass="textolibre" Width="400px" height="46px"
																				Rows="2" TextMode="MultiLine"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBajaAutor" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnAltaAutor" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaAutor" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Baja"></asp:Button>
																			<asp:Button id="btnModiAutor" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Modificar"></asp:Button>
																			<asp:Button id="btnLimpAutor" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																				Text="Limpiar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																			align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Visible="False" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
						<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
						<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnLocaPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnAutorId" runat="server"></asp:textbox><asp:textbox id="hdnDireId" runat="server"></asp:textbox><asp:textbox id="hdnTeleId" runat="server"></asp:textbox><asp:textbox id="hdnMailId" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
				
		function btnLoca_click()
	   	{
	   	  if (document.all("cmbDirePcia").value == "" || document.all("cmbDirePais").value == "" )
			alert('Debe seleccionar Pa�s y Provincia.')
		  else	
	   	   gAbrirVentanas("localidades.aspx?EsConsul=1&t=popup&pa=" + document.all("cmbDirePais").value + "&pr=" + document.all("cmbDirePcia").value,1);
		}	
		/*
		function mSelecCP()
		{
		    document.all["txtDireCpos"].value = LeerCamposXML("localidades", document.all["cmbDireLoca"].value, "loca_cpos");
		}
		 */  
		function mCargarProvincias(pstrTipo)
		{
			var sFiltro = "0" + document.all("cmbDirePais" + pstrTipo).value;
			LoadComboXML("provincias_cargar", sFiltro, "cmbDirePcia"  + pstrTipo, "S");
			if (document.all('hdnValCodPostalPais') != null)
			{
				if (document.all("cmbDirePais" + pstrTipo).value == '<%=Session("sPaisDefaId")%>')
					document.all('hdnValCodPostalPais').value = 'S';
				else
					document.all('hdnValCodPostalPais').value = 'N';	
			}			    
			document.all("cmbDirePcia"  + pstrTipo).onchange();
		}
		/*
		function mCargarLocalidades(pstrTipo)
		{
			var sFiltro = "0" + document.all("cmbDirePcia"  + pstrTipo).value 
		    LoadComboXML("localidades_cargar", sFiltro, "cmbDireLoca"  + pstrTipo, "S");
		}*/
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
