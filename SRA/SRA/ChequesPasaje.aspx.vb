Namespace SRA

Partial Class ChequesPasaje
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Cheques
   Private mstrTablaAsiento As String = "pagos_pasaje_proceso"
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      chkSel = 0
      cheq_id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mEstablecerPerfil()
            mInicializar()

                    txtFechaFil.Fecha = Today

                    mSetearEventos()

            mCrearDataSet()

            mConsultarBusc()

            clsWeb.gInicializarControles(sender, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
            mGuardarDatosGrilla()
         End If
            If Session("sCentroEmisorCentral") <> "S" Then
                btnBuscar.Visible = False
                grdDato.Enabled = False
                Throw New AccesoBD.clsErrNeg("Opci�n No Disponible Solo Se Puede Ejecutar Desde Administraci�n Central")
            End If

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mEstablecerPerfil()
   End Sub

   Private Sub mSetearEventos()
      btnAlta.Attributes.Add("onclick", "if(!confirm('Confirma el pasaje de los cheques y los acuses?')) return false;")
   End Sub

   Public Sub mInicializar()
      Dim lstrParaPageSize As String
      clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(lstrParaPageSize)

   End Sub
#End Region

   Private Sub btnBuscar_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
      Try
         mCrearDataSet()

         grdDato.CurrentPageIndex = 0
         mConsultarBusc()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultarBusc()

         mChequearGrilla()

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Public Sub mConsultarBusc()
      Dim lstrFiltros As New System.text.StringBuilder

      With lstrFiltros
         .Append("exec cheques_busq")
         .Append(" @cheq_post_busq_fecha=")
         .Append(clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
         .Append(",@cheq_chti_id=")
         .Append(SRA_Neg.Constantes.ChequesTipos.Postdatado)
         .Append(",@depositado=0")

         clsWeb.gCargarDataGrid(mstrConn, .ToString, grdDato)
      End With
   End Sub

   Private Sub mChequearGrilla()
      For Each oItem As DataGridItem In grdDato.Items
         CType(oItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = mdsDatos.Tables(mstrTabla).Select("cheq_id=" & oItem.Cells(Columnas.cheq_id).Text).GetUpperBound(0) <> -1
      Next
   End Sub
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub mAlta()
      Try
         Dim lstrReciNumes As String
         Dim lobjNeg As New SRA_Neg.Cheques(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjNeg.PasarChequesPost()

         With mdsDatos.Tables(mstrTabla).Select()
            If .GetUpperBound(0) > -1 Then
               lstrReciNumes = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, .GetValue(0).Item("cheq_id")).Tables(0).Rows(0).Item("_comp_nume")
               If .GetUpperBound(0) = 0 Then
                  clsError.gGenerarMensajes(Me, "Se gener� el Recibo " + lstrReciNumes)
               Else
                  clsError.gGenerarMensajes(Me, "Se generaron " + (.GetUpperBound(0) + 1).ToString + " recibos a partir del Nro " + lstrReciNumes)
               End If
            End If
         End With

         mConsultarBusc()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosGrilla()
      Dim ldrDatos As DataRow
      Dim lbooSelec As Boolean
      Dim lstrFiltro As String
      Dim ldtOri As DataTable

      For Each oItem As DataGridItem In grdDato.Items
         lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked

         lstrFiltro = "cheq_id=" & oItem.Cells(Columnas.cheq_id).Text

         With mdsDatos.Tables(mstrTabla).Select(lstrFiltro)
            If .GetUpperBound(0) <> -1 Then
               ldrDatos = .GetValue(0)
            End If
         End With

         If lbooSelec Then
            If ldrDatos Is Nothing Then
               'obtengo el registro de la base, as� de paso tengo el timestmp
               ldtOri = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, oItem.Cells(Columnas.cheq_id).Text).Tables(0)
               ldrDatos = ldtOri.Rows(0)
               With mdsDatos.Tables(mstrTabla).Rows.Add(ldrDatos.ItemArray)
                  'le cambio el tipo al cheque
                  .Item("cheq_chti_id") = SRA_Neg.Constantes.ChequesTipos.Comun
               End With
            End If
         Else
            If Not ldrDatos Is Nothing Then
               ldrDatos.Delete()
            End If
         End If
         ldrDatos = Nothing
      Next
   End Sub

   Private Sub mCrearDataSet()
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla)
      mdsDatos.Tables(0).Rows(0).Delete()

      mdsDatos.Tables.Add(mstrTablaAsiento)
      With mdsDatos.Tables(mstrTablaAsiento)
         .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_paco_ids", System.Type.GetType("System.String"))
         .Columns.Add("proc_modu_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))

         .Rows.Add(.NewRow)
         .Rows(0).Item("proc_id") = -1
      End With

      Session(mstrTabla) = mdsDatos
   End Sub
End Class
End Namespace
